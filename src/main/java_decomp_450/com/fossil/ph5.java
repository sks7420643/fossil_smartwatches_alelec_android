package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph5 {
    @DexIgnore
    public /* final */ ch5 a;
    @DexIgnore
    public /* final */ ll4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$1", f = "MigrationHelper.kt", l = {22}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $version;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ph5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ph5 ph5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ph5;
            this.$version = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$version, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                String p = this.this$0.a.p();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MigrationHelper", "start migration for " + this.$version + " lastVersion " + p);
                ll4 a2 = this.this$0.b;
                this.L$0 = yi7;
                this.L$1 = p;
                this.label = 1;
                if (a2.b(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ph5(ch5 ch5, ll4 ll4) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(ll4, "mMigrationManager");
        this.a = ch5;
        this.b = ll4;
    }

    @DexIgnore
    public final boolean a(String str) {
        ee7.b(str, "version");
        return this.a.l(str);
    }

    @DexIgnore
    public final ik7 b(String str) {
        ee7.b(str, "version");
        return xh7.b(zi7.a(qj7.a()), null, null, new b(this, str, null), 3, null);
    }
}
