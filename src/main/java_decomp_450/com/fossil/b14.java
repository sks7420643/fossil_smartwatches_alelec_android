package com.fossil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b14 {
    @DexIgnore
    public static <T> T a(Class<T> cls, InvocationHandler invocationHandler) {
        jw3.a(invocationHandler);
        jw3.a(cls.isInterface(), "%s is not an interface", cls);
        return cls.cast(Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, invocationHandler));
    }
}
