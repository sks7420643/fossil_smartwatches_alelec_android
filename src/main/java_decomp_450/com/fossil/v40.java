package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class v40<Z> implements c50<Z> {
    @DexIgnore
    @Override // com.fossil.c50
    public void a(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void b(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void c(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStart() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStop() {
    }
}
