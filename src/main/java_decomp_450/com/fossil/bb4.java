package com.fossil;

import java.util.concurrent.ScheduledFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bb4 implements ho3 {
    @DexIgnore
    public /* final */ db4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ScheduledFuture c;

    @DexIgnore
    public bb4(db4 db4, String str, ScheduledFuture scheduledFuture) {
        this.a = db4;
        this.b = str;
        this.c = scheduledFuture;
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3 no3) {
        this.a.a(this.b, this.c, no3);
    }
}
