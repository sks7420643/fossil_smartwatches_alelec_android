package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.h9;
import com.fossil.w1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r1 implements w7 {
    @DexIgnore
    public View A;
    @DexIgnore
    public h9 B;
    @DexIgnore
    public MenuItem.OnActionExpandListener C;
    @DexIgnore
    public boolean D; // = false;
    @DexIgnore
    public ContextMenu.ContextMenuInfo E;
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public CharSequence e;
    @DexIgnore
    public CharSequence f;
    @DexIgnore
    public Intent g;
    @DexIgnore
    public char h;
    @DexIgnore
    public int i; // = 4096;
    @DexIgnore
    public char j;
    @DexIgnore
    public int k; // = 4096;
    @DexIgnore
    public Drawable l;
    @DexIgnore
    public int m; // = 0;
    @DexIgnore
    public p1 n;
    @DexIgnore
    public a2 o;
    @DexIgnore
    public Runnable p;
    @DexIgnore
    public MenuItem.OnMenuItemClickListener q;
    @DexIgnore
    public CharSequence r;
    @DexIgnore
    public CharSequence s;
    @DexIgnore
    public ColorStateList t; // = null;
    @DexIgnore
    public PorterDuff.Mode u; // = null;
    @DexIgnore
    public boolean v; // = false;
    @DexIgnore
    public boolean w; // = false;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public int y; // = 16;
    @DexIgnore
    public int z; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements h9.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.h9.b
        public void onActionProviderVisibilityChanged(boolean z) {
            r1 r1Var = r1.this;
            r1Var.n.d(r1Var);
        }
    }

    @DexIgnore
    public r1(p1 p1Var, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.n = p1Var;
        this.a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.e = charSequence;
        this.z = i6;
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i2, int i3, String str) {
        if ((i2 & i3) == i3) {
            sb.append(str);
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        int i2 = this.y;
        int i3 = (z2 ? 2 : 0) | (i2 & -3);
        this.y = i3;
        if (i2 != i3) {
            this.n.c(false);
        }
    }

    @DexIgnore
    public int c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public boolean collapseActionView() {
        if ((this.z & 8) == 0) {
            return false;
        }
        if (this.A == null) {
            return true;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.C;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionCollapse(this)) {
            return this.n.a(this);
        }
        return false;
    }

    @DexIgnore
    public char d() {
        return this.n.p() ? this.j : this.h;
    }

    @DexIgnore
    public String e() {
        char d2 = d();
        if (d2 == 0) {
            return "";
        }
        Resources resources = this.n.e().getResources();
        StringBuilder sb = new StringBuilder();
        if (ViewConfiguration.get(this.n.e()).hasPermanentMenuKey()) {
            sb.append(resources.getString(f0.abc_prepend_shortcut_label));
        }
        int i2 = this.n.p() ? this.k : this.i;
        a(sb, i2, 65536, resources.getString(f0.abc_menu_meta_shortcut_label));
        a(sb, i2, 4096, resources.getString(f0.abc_menu_ctrl_shortcut_label));
        a(sb, i2, 2, resources.getString(f0.abc_menu_alt_shortcut_label));
        a(sb, i2, 1, resources.getString(f0.abc_menu_shift_shortcut_label));
        a(sb, i2, 4, resources.getString(f0.abc_menu_sym_shortcut_label));
        a(sb, i2, 8, resources.getString(f0.abc_menu_function_shortcut_label));
        if (d2 == '\b') {
            sb.append(resources.getString(f0.abc_menu_delete_shortcut_label));
        } else if (d2 == '\n') {
            sb.append(resources.getString(f0.abc_menu_enter_shortcut_label));
        } else if (d2 != ' ') {
            sb.append(d2);
        } else {
            sb.append(resources.getString(f0.abc_menu_space_shortcut_label));
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.w7
    public boolean expandActionView() {
        if (!f()) {
            return false;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.C;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionExpand(this)) {
            return this.n.b(this);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        h9 h9Var;
        if ((this.z & 8) == 0) {
            return false;
        }
        if (this.A == null && (h9Var = this.B) != null) {
            this.A = h9Var.onCreateActionView(this);
        }
        if (this.A != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean g() {
        MenuItem.OnMenuItemClickListener onMenuItemClickListener = this.q;
        if (onMenuItemClickListener != null && onMenuItemClickListener.onMenuItemClick(this)) {
            return true;
        }
        p1 p1Var = this.n;
        if (p1Var.a(p1Var, this)) {
            return true;
        }
        Runnable runnable = this.p;
        if (runnable != null) {
            runnable.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.n.e().startActivity(this.g);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        h9 h9Var = this.B;
        if (h9Var == null || !h9Var.onPerformDefaultAction()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    @DexIgnore
    @Override // com.fossil.w7
    public View getActionView() {
        View view = this.A;
        if (view != null) {
            return view;
        }
        h9 h9Var = this.B;
        if (h9Var == null) {
            return null;
        }
        View onCreateActionView = h9Var.onCreateActionView(this);
        this.A = onCreateActionView;
        return onCreateActionView;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public int getAlphabeticModifiers() {
        return this.k;
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public CharSequence getContentDescription() {
        return this.r;
    }

    @DexIgnore
    public int getGroupId() {
        return this.b;
    }

    @DexIgnore
    public Drawable getIcon() {
        Drawable drawable = this.l;
        if (drawable != null) {
            return a(drawable);
        }
        if (this.m == 0) {
            return null;
        }
        Drawable c2 = t0.c(this.n.e(), this.m);
        this.m = 0;
        this.l = c2;
        return a(c2);
    }

    @DexIgnore
    @Override // com.fossil.w7
    public ColorStateList getIconTintList() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public PorterDuff.Mode getIconTintMode() {
        return this.u;
    }

    @DexIgnore
    public Intent getIntent() {
        return this.g;
    }

    @DexIgnore
    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.a;
    }

    @DexIgnore
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public int getNumericModifiers() {
        return this.i;
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.h;
    }

    @DexIgnore
    public int getOrder() {
        return this.c;
    }

    @DexIgnore
    public SubMenu getSubMenu() {
        return this.o;
    }

    @DexIgnore
    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.e;
    }

    @DexIgnore
    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f;
        if (charSequence == null) {
            charSequence = this.e;
        }
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    @DexIgnore
    @Override // com.fossil.w7
    public CharSequence getTooltipText() {
        return this.s;
    }

    @DexIgnore
    public boolean h() {
        return (this.y & 32) == 32;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return this.o != null;
    }

    @DexIgnore
    public boolean i() {
        return (this.y & 4) != 0;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public boolean isActionViewExpanded() {
        return this.D;
    }

    @DexIgnore
    public boolean isCheckable() {
        return (this.y & 1) == 1;
    }

    @DexIgnore
    public boolean isChecked() {
        return (this.y & 2) == 2;
    }

    @DexIgnore
    public boolean isEnabled() {
        return (this.y & 16) != 0;
    }

    @DexIgnore
    public boolean isVisible() {
        h9 h9Var = this.B;
        if (h9Var == null || !h9Var.overridesItemVisibility()) {
            if ((this.y & 8) == 0) {
                return true;
            }
            return false;
        } else if ((this.y & 8) != 0 || !this.B.isVisible()) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    public boolean j() {
        return (this.z & 1) == 1;
    }

    @DexIgnore
    public boolean k() {
        return (this.z & 2) == 2;
    }

    @DexIgnore
    public boolean l() {
        return this.n.k();
    }

    @DexIgnore
    public boolean m() {
        return this.n.q() && d() != 0;
    }

    @DexIgnore
    public boolean n() {
        return (this.z & 4) == 4;
    }

    @DexIgnore
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.j == c2) {
            return this;
        }
        this.j = Character.toLowerCase(c2);
        this.n.c(false);
        return this;
    }

    @DexIgnore
    public MenuItem setCheckable(boolean z2) {
        int i2 = this.y;
        int i3 = z2 | (i2 & -2);
        this.y = i3;
        if (i2 != i3) {
            this.n.c(false);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setChecked(boolean z2) {
        if ((this.y & 4) != 0) {
            this.n.a((MenuItem) this);
        } else {
            b(z2);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.y |= 16;
        } else {
            this.y &= -17;
        }
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.m = 0;
        this.l = drawable;
        this.x = true;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.t = colorStateList;
        this.v = true;
        this.x = true;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.u = mode;
        this.w = true;
        this.x = true;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    public MenuItem setIntent(Intent intent) {
        this.g = intent;
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2) {
        if (this.h == c2) {
            return this;
        }
        this.h = c2;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.C = onActionExpandListener;
        return this;
    }

    @DexIgnore
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.q = onMenuItemClickListener;
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3) {
        this.h = c2;
        this.j = Character.toLowerCase(c3);
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public void setShowAsAction(int i2) {
        int i3 = i2 & 3;
        if (i3 == 0 || i3 == 1 || i3 == 2) {
            this.z = i2;
            this.n.c(this);
            return;
        }
        throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.e = charSequence;
        this.n.c(false);
        a2 a2Var = this.o;
        if (a2Var != null) {
            a2Var.setHeaderTitle(charSequence);
        }
        return this;
    }

    @DexIgnore
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f = charSequence;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    public MenuItem setVisible(boolean z2) {
        if (e(z2)) {
            this.n.d(this);
        }
        return this;
    }

    @DexIgnore
    public String toString() {
        CharSequence charSequence = this.e;
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    @DexIgnore
    public void a(a2 a2Var) {
        this.o = a2Var;
        a2Var.setHeaderTitle(getTitle());
    }

    @DexIgnore
    public void c(boolean z2) {
        this.y = (z2 ? 4 : 0) | (this.y & -5);
    }

    @DexIgnore
    public void d(boolean z2) {
        if (z2) {
            this.y |= 32;
        } else {
            this.y &= -33;
        }
    }

    @DexIgnore
    @Override // com.fossil.w7
    public w7 setContentDescription(CharSequence charSequence) {
        this.r = charSequence;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public w7 setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public w7 setTooltipText(CharSequence charSequence) {
        this.s = charSequence;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7, android.view.MenuItem
    public w7 setActionView(View view) {
        int i2;
        this.A = view;
        this.B = null;
        if (view != null && view.getId() == -1 && (i2 = this.a) > 0) {
            view.setId(i2);
        }
        this.n.c(this);
        return this;
    }

    @DexIgnore
    public CharSequence a(w1.a aVar) {
        if (aVar == null || !aVar.c()) {
            return getTitle();
        }
        return getTitleCondensed();
    }

    @DexIgnore
    public void b() {
        this.n.c(this);
    }

    @DexIgnore
    @Override // com.fossil.w7
    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        if (this.j == c2 && this.k == i2) {
            return this;
        }
        this.j = Character.toLowerCase(c2);
        this.k = KeyEvent.normalizeMetaState(i2);
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public MenuItem setNumericShortcut(char c2, int i2) {
        if (this.h == c2 && this.i == i2) {
            return this;
        }
        this.h = c2;
        this.i = KeyEvent.normalizeMetaState(i2);
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.h = c2;
        this.i = KeyEvent.normalizeMetaState(i2);
        this.j = Character.toLowerCase(c3);
        this.k = KeyEvent.normalizeMetaState(i3);
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(int i2) {
        this.l = null;
        this.m = i2;
        this.x = true;
        this.n.c(false);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(int i2) {
        setTitle(this.n.e().getString(i2));
        return this;
    }

    @DexIgnore
    public final Drawable a(Drawable drawable) {
        if (drawable != null && this.x && (this.v || this.w)) {
            drawable = p7.i(drawable).mutate();
            if (this.v) {
                p7.a(drawable, this.t);
            }
            if (this.w) {
                p7.a(drawable, this.u);
            }
            this.x = false;
        }
        return drawable;
    }

    @DexIgnore
    @Override // com.fossil.w7, android.view.MenuItem
    public w7 setActionView(int i2) {
        Context e2 = this.n.e();
        setActionView(LayoutInflater.from(e2).inflate(i2, (ViewGroup) new LinearLayout(e2), false));
        return this;
    }

    @DexIgnore
    public void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.E = contextMenuInfo;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public h9 a() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.w7
    public w7 a(h9 h9Var) {
        h9 h9Var2 = this.B;
        if (h9Var2 != null) {
            h9Var2.reset();
        }
        this.A = null;
        this.B = h9Var;
        this.n.c(true);
        h9 h9Var3 = this.B;
        if (h9Var3 != null) {
            h9Var3.setVisibilityListener(new a());
        }
        return this;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.D = z2;
        this.n.c(false);
    }

    @DexIgnore
    public boolean e(boolean z2) {
        int i2 = this.y;
        int i3 = (z2 ? 0 : 8) | (i2 & -9);
        this.y = i3;
        if (i2 != i3) {
            return true;
        }
        return false;
    }
}
