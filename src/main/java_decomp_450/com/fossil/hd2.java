package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.RawDataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd2 implements Parcelable.Creator<RawDataPoint> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ RawDataPoint createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        mc2[] mc2Arr = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    j = j72.s(parcel, a);
                    break;
                case 2:
                    j2 = j72.s(parcel, a);
                    break;
                case 3:
                    mc2Arr = (mc2[]) j72.b(parcel, a, mc2.CREATOR);
                    break;
                case 4:
                    i = j72.q(parcel, a);
                    break;
                case 5:
                    i2 = j72.q(parcel, a);
                    break;
                case 6:
                    j3 = j72.s(parcel, a);
                    break;
                case 7:
                    j4 = j72.s(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new RawDataPoint(j, j2, mc2Arr, i, i2, j3, j4);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ RawDataPoint[] newArray(int i) {
        return new RawDataPoint[i];
    }
}
