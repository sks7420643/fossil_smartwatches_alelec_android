package com.fossil;

import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.be5;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class is6 extends fl4<es6, fs6, ds6> {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ MicroAppRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ NotificationsRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ AlarmsRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1", f = "GetHybridDeviceSettingUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SparseArray sparseArray, boolean z, String str, fb7 fb7) {
            super(2, fb7);
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$data, this.$isMovemberModel, this.$serialNumber, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                AppNotificationFilterSettings a = nx6.b.a(this.$data, this.$isMovemberModel);
                PortfolioApp.g0.c().a(a, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = is6.k;
                local.d(e, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1", f = "GetHybridDeviceSettingUseCase.kt", l = {60, 61, 62, 63, 64, 76, 84, 100}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ is6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(is6 is6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = is6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:119:0x03d6  */
        /* JADX WARNING: Removed duplicated region for block: B:121:0x03da  */
        /* JADX WARNING: Removed duplicated region for block: B:125:0x03e2  */
        /* JADX WARNING: Removed duplicated region for block: B:127:0x03e6  */
        /* JADX WARNING: Removed duplicated region for block: B:129:0x03ea  */
        /* JADX WARNING: Removed duplicated region for block: B:131:0x03ee  */
        /* JADX WARNING: Removed duplicated region for block: B:135:0x0214 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00b5  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00cf  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00e9  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0106 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0123  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x019e  */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x01fe  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0241  */
        /* JADX WARNING: Removed duplicated region for block: B:80:0x02b3  */
        /* JADX WARNING: Removed duplicated region for block: B:94:0x0325  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                r13 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r13.label
                r2 = 0
                r3 = 1
                r4 = 0
                switch(r1) {
                    case 0: goto L_0x0091;
                    case 1: goto L_0x0089;
                    case 2: goto L_0x0081;
                    case 3: goto L_0x0079;
                    case 4: goto L_0x0070;
                    case 5: goto L_0x0067;
                    case 6: goto L_0x004e;
                    case 7: goto L_0x0031;
                    case 8: goto L_0x0014;
                    default: goto L_0x000c;
                }
            L_0x000c:
                java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r14.<init>(r0)
                throw r14
            L_0x0014:
                java.lang.Object r0 = r13.L$5
                com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
                java.lang.Object r1 = r13.L$4
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                java.lang.Object r1 = r13.L$3
                com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
                java.lang.Object r1 = r13.L$2
                com.fossil.qe7 r1 = (com.fossil.qe7) r1
                java.lang.Object r1 = r13.L$1
                com.fossil.qe7 r1 = (com.fossil.qe7) r1
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x0286
            L_0x0031:
                java.lang.Object r1 = r13.L$5
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r1 = r13.L$4
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                java.lang.Object r5 = r13.L$3
                com.portfolio.platform.data.model.Device r5 = (com.portfolio.platform.data.model.Device) r5
                java.lang.Object r6 = r13.L$2
                com.fossil.qe7 r6 = (com.fossil.qe7) r6
                java.lang.Object r7 = r13.L$1
                com.fossil.qe7 r7 = (com.fossil.qe7) r7
                java.lang.Object r8 = r13.L$0
                com.fossil.yi7 r8 = (com.fossil.yi7) r8
                com.fossil.t87.a(r14)
                goto L_0x01f4
            L_0x004e:
                java.lang.Object r1 = r13.L$3
                com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
                java.lang.Object r5 = r13.L$2
                com.fossil.qe7 r5 = (com.fossil.qe7) r5
                java.lang.Object r6 = r13.L$1
                com.fossil.qe7 r6 = (com.fossil.qe7) r6
                java.lang.Object r7 = r13.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r14)
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r1
                goto L_0x0190
            L_0x0067:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x0107
            L_0x0070:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00f5
            L_0x0079:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00db
            L_0x0081:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00c1
            L_0x0089:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00a7
            L_0x0091:
                com.fossil.t87.a(r14)
                com.fossil.yi7 r1 = r13.p$
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.CategoryRepository r14 = r14.i
                r13.L$0 = r1
                r13.label = r3
                java.lang.Object r14 = r14.downloadCategories(r13)
                if (r14 != r0) goto L_0x00a7
                return r0
            L_0x00a7:
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.MicroAppRepository r14 = r14.f
                com.fossil.is6 r5 = r13.this$0
                java.lang.String r5 = r5.d
                if (r5 == 0) goto L_0x03ee
                r13.L$0 = r1
                r6 = 2
                r13.label = r6
                java.lang.Object r14 = r14.downloadAllMicroApp(r5, r13)
                if (r14 != r0) goto L_0x00c1
                return r0
            L_0x00c1:
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r14 = r14.e
                com.fossil.is6 r5 = r13.this$0
                java.lang.String r5 = r5.d
                if (r5 == 0) goto L_0x03ea
                r13.L$0 = r1
                r6 = 3
                r13.label = r6
                java.lang.Object r14 = r14.downloadRecommendPresetList(r5, r13)
                if (r14 != r0) goto L_0x00db
                return r0
            L_0x00db:
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r14 = r14.e
                com.fossil.is6 r5 = r13.this$0
                java.lang.String r5 = r5.d
                if (r5 == 0) goto L_0x03e6
                r13.L$0 = r1
                r6 = 4
                r13.label = r6
                java.lang.Object r14 = r14.downloadPresetList(r5, r13)
                if (r14 != r0) goto L_0x00f5
                return r0
            L_0x00f5:
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.AlarmsRepository r14 = r14.j
                r13.L$0 = r1
                r5 = 5
                r13.label = r5
                java.lang.Object r14 = r14.downloadAlarms(r13)
                if (r14 != r0) goto L_0x0107
                return r0
            L_0x0107:
                com.fossil.qe7 r14 = new com.fossil.qe7
                r14.<init>()
                r14.element = r3
                com.fossil.qe7 r5 = new com.fossil.qe7
                r5.<init>()
                r5.element = r2
                com.fossil.is6 r6 = r13.this$0
                com.portfolio.platform.data.source.DeviceRepository r6 = r6.g
                com.fossil.is6 r7 = r13.this$0
                java.lang.String r7 = r7.d
                if (r7 == 0) goto L_0x03e2
                com.portfolio.platform.data.model.Device r6 = r6.getDeviceBySerial(r7)
                if (r6 == 0) goto L_0x0135
                int r7 = r6.getMajor()
                r14.element = r7
                int r7 = r6.getMinor()
                r5.element = r7
            L_0x0135:
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                java.lang.String r8 = com.fossil.is6.k
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "download variant with major "
                r9.append(r10)
                int r10 = r14.element
                r9.append(r10)
                java.lang.String r10 = " minor "
                r9.append(r10)
                int r10 = r5.element
                r9.append(r10)
                java.lang.String r9 = r9.toString()
                r7.d(r8, r9)
                com.fossil.is6 r7 = r13.this$0
                com.portfolio.platform.data.source.MicroAppRepository r7 = r7.f
                com.fossil.is6 r8 = r13.this$0
                java.lang.String r8 = r8.d
                if (r8 == 0) goto L_0x03de
                int r9 = r14.element
                java.lang.String r9 = java.lang.String.valueOf(r9)
                int r10 = r5.element
                java.lang.String r10 = java.lang.String.valueOf(r10)
                r13.L$0 = r1
                r13.L$1 = r14
                r13.L$2 = r5
                r13.L$3 = r6
                r11 = 6
                r13.label = r11
                java.lang.Object r7 = r7.downloadMicroAppVariant(r8, r9, r10, r13)
                if (r7 != r0) goto L_0x018b
                return r0
            L_0x018b:
                r7 = r14
                r8 = r1
                r12 = r6
                r6 = r5
                r5 = r12
            L_0x0190:
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r14 = r14.e
                com.fossil.is6 r1 = r13.this$0
                java.lang.String r1 = r1.d
                if (r1 == 0) goto L_0x03da
                java.util.ArrayList r1 = r14.getPresetList(r1)
                boolean r14 = r1.isEmpty()
                if (r14 == 0) goto L_0x01f4
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r14 = r14.e
                com.fossil.is6 r9 = r13.this$0
                java.lang.String r9 = r9.d
                if (r9 == 0) goto L_0x01f0
                java.util.List r14 = r14.getHybridRecommendPresetList(r9)
                java.util.Iterator r9 = r14.iterator()
            L_0x01be:
                boolean r10 = r9.hasNext()
                if (r10 == 0) goto L_0x01d4
                java.lang.Object r10 = r9.next()
                com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset r10 = (com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset) r10
                com.portfolio.platform.data.model.room.microapp.HybridPreset$Companion r11 = com.portfolio.platform.data.model.room.microapp.HybridPreset.Companion
                com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = r11.cloneFromDefault(r10)
                r1.add(r10)
                goto L_0x01be
            L_0x01d4:
                com.fossil.is6 r9 = r13.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r9 = r9.e
                r13.L$0 = r8
                r13.L$1 = r7
                r13.L$2 = r6
                r13.L$3 = r5
                r13.L$4 = r1
                r13.L$5 = r14
                r14 = 7
                r13.label = r14
                java.lang.Object r14 = r9.upsertHybridPresetList(r1, r13)
                if (r14 != r0) goto L_0x01f4
                return r0
            L_0x01f0:
                com.fossil.ee7.a()
                throw r4
            L_0x01f4:
                java.util.Iterator r14 = r1.iterator()
            L_0x01f8:
                boolean r9 = r14.hasNext()
                if (r9 == 0) goto L_0x0214
                java.lang.Object r9 = r14.next()
                r10 = r9
                com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r10
                boolean r10 = r10.isActive()
                java.lang.Boolean r10 = com.fossil.pb7.a(r10)
                boolean r10 = r10.booleanValue()
                if (r10 == 0) goto L_0x01f8
                goto L_0x0215
            L_0x0214:
                r9 = r4
            L_0x0215:
                com.portfolio.platform.data.model.room.microapp.HybridPreset r9 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r9
                if (r9 != 0) goto L_0x023f
                boolean r14 = r1.isEmpty()
                if (r14 == 0) goto L_0x023f
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                java.lang.String r0 = com.fossil.is6.k
                java.lang.String r1 = "activePreset is null, preset list is emptyObjectObject?"
                r14.d(r0, r1)
                com.fossil.is6 r14 = r13.this$0
                com.fossil.ds6 r0 = new com.fossil.ds6
                r1 = 600(0x258, float:8.41E-43)
                java.lang.String r2 = ""
                r0.<init>(r1, r2)
                r14.a(r0)
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            L_0x023f:
                if (r9 != 0) goto L_0x0287
                java.lang.Object r14 = r1.get(r2)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r14 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r14
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.String r9 = com.fossil.is6.k
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "Active preset is null ,pick "
                r10.append(r11)
                r10.append(r14)
                java.lang.String r10 = r10.toString()
                r2.d(r9, r10)
                r14.setActive(r3)
                com.fossil.is6 r2 = r13.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r2 = r2.e
                r13.L$0 = r8
                r13.L$1 = r7
                r13.L$2 = r6
                r13.L$3 = r5
                r13.L$4 = r1
                r13.L$5 = r14
                r1 = 8
                r13.label = r1
                java.lang.Object r1 = r2.upsertHybridPreset(r14, r13)
                if (r1 != r0) goto L_0x0285
                return r0
            L_0x0285:
                r0 = r14
            L_0x0286:
                r9 = r0
            L_0x0287:
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                java.lang.String r0 = com.fossil.is6.k
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "activePreset="
                r1.append(r2)
                r1.append(r9)
                java.lang.String r1 = r1.toString()
                r14.d(r0, r1)
                java.util.ArrayList r14 = r9.getButtons()
                java.util.Iterator r14 = r14.iterator()
            L_0x02ad:
                boolean r0 = r14.hasNext()
                if (r0 == 0) goto L_0x031d
                java.lang.Object r0 = r14.next()
                com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r0
                java.lang.String r1 = r0.getAppId()
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
                java.lang.String r2 = r2.getValue()
                boolean r1 = com.fossil.ee7.a(r1, r2)
                if (r1 == 0) goto L_0x02ad
                java.lang.String r0 = r0.getSettings()
                boolean r1 = android.text.TextUtils.isEmpty(r0)
                if (r1 != 0) goto L_0x02ad
                com.google.gson.Gson r1 = new com.google.gson.Gson     // Catch:{ Exception -> 0x02fa }
                r1.<init>()     // Catch:{ Exception -> 0x02fa }
                java.lang.Class<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> r2 = com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class
                java.lang.Object r0 = r1.a(r0, r2)     // Catch:{ Exception -> 0x02fa }
                com.portfolio.platform.data.model.setting.SecondTimezoneSetting r0 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r0     // Catch:{ Exception -> 0x02fa }
                if (r0 == 0) goto L_0x02ad
                java.lang.String r1 = r0.getTimeZoneId()     // Catch:{ Exception -> 0x02fa }
                boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x02fa }
                if (r1 != 0) goto L_0x02ad
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x02fa }
                com.portfolio.platform.PortfolioApp r1 = r1.c()     // Catch:{ Exception -> 0x02fa }
                java.lang.String r0 = r0.getTimeZoneId()     // Catch:{ Exception -> 0x02fa }
                r1.o(r0)     // Catch:{ Exception -> 0x02fa }
                goto L_0x02ad
            L_0x02fa:
                r0 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                java.lang.String r2 = com.fossil.is6.k
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "parse secondTimezone, ex="
                r5.append(r6)
                r5.append(r0)
                java.lang.String r5 = r5.toString()
                r1.e(r2, r5)
                r0.printStackTrace()
                goto L_0x02ad
            L_0x031d:
                com.fossil.is6 r14 = r13.this$0
                java.lang.String r14 = r14.d
                if (r14 == 0) goto L_0x03d6
                com.fossil.is6 r0 = r13.this$0
                com.portfolio.platform.data.source.DeviceRepository r0 = r0.g
                com.fossil.is6 r1 = r13.this$0
                com.portfolio.platform.data.source.MicroAppRepository r1 = r1.f
                java.util.List r14 = com.fossil.xc5.a(r9, r14, r0, r1)
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.is6.k
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r5 = "bleMappingList "
                r2.append(r5)
                r2.append(r14)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                boolean r0 = r14.isEmpty()
                r0 = r0 ^ r3
                if (r0 == 0) goto L_0x0370
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                com.fossil.is6 r1 = r13.this$0
                java.lang.String r1 = r1.d
                if (r1 == 0) goto L_0x036c
                r0.b(r1, r14)
                goto L_0x0370
            L_0x036c:
                com.fossil.ee7.a()
                throw r4
            L_0x0370:
                com.fossil.is6 r14 = r13.this$0
                com.portfolio.platform.data.source.NotificationsRepository r14 = r14.h
                com.fossil.is6 r0 = r13.this$0
                java.lang.String r0 = r0.d
                if (r0 == 0) goto L_0x03d2
                com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r1 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
                int r1 = r1.getValue()
                android.util.SparseArray r14 = r14.getAllNotificationsByHour(r0, r1)
                com.fossil.is6 r0 = r13.this$0
                com.portfolio.platform.data.source.DeviceRepository r0 = r0.g
                com.fossil.be5$a r1 = com.fossil.be5.o
                com.fossil.is6 r2 = r13.this$0
                java.lang.String r2 = r2.d
                if (r2 == 0) goto L_0x03ce
                java.lang.String r1 = r1.b(r2)
                com.portfolio.platform.data.model.SKUModel r0 = r0.getSkuModelBySerialPrefix(r1)
                com.fossil.nx6 r1 = com.fossil.nx6.b
                com.fossil.is6 r2 = r13.this$0
                java.lang.String r2 = r2.d
                if (r2 == 0) goto L_0x03ca
                boolean r0 = r1.a(r0, r2)
                com.fossil.is6 r1 = r13.this$0
                java.lang.String r2 = r1.d
                if (r2 == 0) goto L_0x03c6
                com.fossil.ik7 unused = r1.a(r14, r2, r0)
                com.fossil.is6 r14 = r13.this$0
                com.fossil.fs6 r0 = new com.fossil.fs6
                r0.<init>()
                r14.a(r0)
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            L_0x03c6:
                com.fossil.ee7.a()
                throw r4
            L_0x03ca:
                com.fossil.ee7.a()
                throw r4
            L_0x03ce:
                com.fossil.ee7.a()
                throw r4
            L_0x03d2:
                com.fossil.ee7.a()
                throw r4
            L_0x03d6:
                com.fossil.ee7.a()
                throw r4
            L_0x03da:
                com.fossil.ee7.a()
                throw r4
            L_0x03de:
                com.fossil.ee7.a()
                throw r4
            L_0x03e2:
                com.fossil.ee7.a()
                throw r4
            L_0x03e6:
                com.fossil.ee7.a()
                throw r4
            L_0x03ea:
                com.fossil.ee7.a()
                throw r4
            L_0x03ee:
                com.fossil.ee7.a()
                throw r4
                switch-data {0->0x0091, 1->0x0089, 2->0x0081, 3->0x0079, 4->0x0070, 5->0x0067, 6->0x004e, 7->0x0031, 8->0x0014, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.is6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = is6.class.getSimpleName();
        ee7.a((Object) simpleName, "GetHybridDeviceSettingUs\u2026se::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public is6(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        ee7.b(hybridPresetRepository, "mPresetRepository");
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(notificationsRepository, "mNotificationsRepository");
        ee7.b(categoryRepository, "mCategoryRepository");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        this.e = hybridPresetRepository;
        this.f = microAppRepository;
        this.g = deviceRepository;
        this.h = notificationsRepository;
        this.i = categoryRepository;
        this.j = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return k;
    }

    @DexIgnore
    public final ik7 d() {
        return xh7.b(b(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(es6 es6, fb7 fb7) {
        return a(es6, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    public Object a(es6 es6, fb7<? super i97> fb7) {
        if (es6 == null) {
            a(new ds6(600, ""));
            return i97.a;
        }
        this.d = es6.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .GetHybridDeviceSettingUseCase executing!!! with serial=");
        String str2 = this.d;
        if (str2 != null) {
            sb.append(str2);
            local.d(str, sb.toString());
            if (!xw6.b(PortfolioApp.g0.c())) {
                a(new ds6(601, ""));
                return i97.a;
            }
            if (!TextUtils.isEmpty(this.d)) {
                be5.a aVar = be5.o;
                String str3 = this.d;
                if (str3 == null) {
                    ee7.a();
                    throw null;
                } else if (aVar.e(str3)) {
                    d();
                    return i97.a;
                }
            }
            a(new ds6(600, ""));
            return i97.a;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ik7 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return xh7.b(zi7.a(qj7.a()), null, null, new b(sparseArray, z, str, null), 3, null);
    }
}
