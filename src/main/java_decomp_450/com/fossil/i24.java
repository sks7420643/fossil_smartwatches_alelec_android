package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class i24 implements ob4 {
    @DexIgnore
    public /* final */ Set a;

    @DexIgnore
    public i24(Set set) {
        this.a = set;
    }

    @DexIgnore
    public static ob4 a(Set set) {
        return new i24(set);
    }

    @DexIgnore
    @Override // com.fossil.ob4
    public Object get() {
        return k24.a(this.a);
    }
}
