package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm1 extends xp0 {
    @DexIgnore
    public static /* final */ nk1 CREATOR; // = new nk1(null);
    @DexIgnore
    public /* final */ k90 d;

    @DexIgnore
    public lm1(byte b, k90 k90) {
        super(ru0.MUSIC_EVENT, b, false, 4);
        this.d = k90;
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }

    @DexIgnore
    public /* synthetic */ lm1(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            this.d = k90.valueOf(readString);
            return;
        }
        ee7.a();
        throw null;
    }
}
