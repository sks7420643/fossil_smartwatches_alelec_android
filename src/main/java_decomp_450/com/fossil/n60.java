package com.fossil;

import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum n60 {
    UNKNOWN(false, yp0.f.i()),
    DIANA(false, yp0.f.b()),
    SE1(true, yp0.f.g()),
    SLIM(true, yp0.f.h()),
    MINI(true, yp0.f.e()),
    HELLAS(false, yp0.f.c()),
    SE0(true, yp0.f.g()),
    WEAR_OS(false, new Type[0]),
    IVY(false, yp0.f.b());
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Type[] b;

    @DexIgnore
    public n60(boolean z, Type[] typeArr) {
        this.a = z;
        this.b = typeArr;
    }

    @DexIgnore
    public final Type[] a() {
        return this.b;
    }

    @DexIgnore
    public final boolean b() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final n60 a(String str) {
            if (!m60.t.a(str)) {
                return n60.UNKNOWN;
            }
            if (mh7.c(str, "D", false, 2, null)) {
                return n60.DIANA;
            }
            if (mh7.c(str, "W", false, 2, null)) {
                return n60.SE1;
            }
            if (mh7.c(str, "L", false, 2, null)) {
                return n60.SLIM;
            }
            if (mh7.c(str, "M", false, 2, null)) {
                return n60.MINI;
            }
            if (mh7.c(str, "Y", false, 2, null)) {
                return n60.HELLAS;
            }
            if (mh7.c(str, "Z", false, 2, null)) {
                return n60.SE0;
            }
            if (mh7.c(str, "V", false, 2, null)) {
                return n60.IVY;
            }
            return n60.UNKNOWN;
        }

        @DexIgnore
        public final n60 a(String str, String str2) {
            if (mh7.c(str, "DN", false, 2, null)) {
                return n60.DIANA;
            }
            if (mh7.c(str, "HW", false, 2, null)) {
                n60 a = a(str2);
                if (a != n60.UNKNOWN) {
                    return a;
                }
                return n60.SE1;
            } else if (mh7.c(str, "HL", false, 2, null)) {
                return n60.SLIM;
            } else {
                if (mh7.c(str, "HM", false, 2, null)) {
                    return n60.MINI;
                }
                if (mh7.c(str, "AW", false, 2, null)) {
                    return n60.HELLAS;
                }
                if (mh7.c(str, "IV", false, 2, null)) {
                    return n60.IVY;
                }
                return a(str2);
            }
        }
    }
}
