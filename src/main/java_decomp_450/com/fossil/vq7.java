package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq7 {
    @DexIgnore
    public static final int a(int i) {
        return ((i & 255) << 24) | ((-16777216 & i) >>> 24) | ((16711680 & i) >>> 8) | ((65280 & i) << 8);
    }

    @DexIgnore
    public static final short a(short s) {
        short s2 = s & 65535;
        return (short) (((s2 & 255) << 8) | ((65280 & s2) >>> 8));
    }

    @DexIgnore
    public static final void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException("size=" + j + " offset=" + j2 + " byteCount=" + j3);
        }
    }

    @DexIgnore
    public static final String b(int i) {
        if (i == 0) {
            return "0";
        }
        int i2 = 0;
        char[] cArr = {vr7.a()[(i >> 28) & 15], vr7.a()[(i >> 24) & 15], vr7.a()[(i >> 20) & 15], vr7.a()[(i >> 16) & 15], vr7.a()[(i >> 12) & 15], vr7.a()[(i >> 8) & 15], vr7.a()[(i >> 4) & 15], vr7.a()[i & 15]};
        while (i2 < 8 && cArr[i2] == '0') {
            i2++;
        }
        return new String(cArr, i2, 8 - i2);
    }

    @DexIgnore
    public static final boolean a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        ee7.b(bArr, "a");
        ee7.b(bArr2, "b");
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static final String a(byte b) {
        return new String(new char[]{vr7.a()[(b >> 4) & 15], vr7.a()[b & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]});
    }
}
