package com.fossil;

import android.os.RemoteException;
import com.fossil.v02;
import com.fossil.v02.b;
import com.fossil.y12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h22<A extends v02.b, L> {
    @DexIgnore
    public /* final */ y12.a<L> a;

    @DexIgnore
    public h22(y12.a<L> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public y12.a<L> a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(A a2, oo3<Boolean> oo3) throws RemoteException;
}
