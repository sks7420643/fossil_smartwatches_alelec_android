package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l01 extends xp0 {
    @DexIgnore
    public static /* final */ uy0 CREATOR; // = new uy0(null);
    @DexIgnore
    public /* final */ sf0 d;
    @DexIgnore
    public /* final */ byte e;

    @DexIgnore
    public l01(byte b, sf0 sf0, byte b2) {
        super(ru0.BATTERY_EVENT, b, false, 4);
        this.d = sf0;
        this.e = b2;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.l1, yz0.a(this.d)), r51.a5, Byte.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
    }

    @DexIgnore
    public l01(Parcel parcel) {
        super(parcel);
        sf0 a = sf0.c.a(parcel.readByte());
        this.d = a == null ? sf0.LOW : a;
        this.e = parcel.readByte();
    }
}
