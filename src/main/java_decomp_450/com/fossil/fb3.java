package com.fossil;

import android.os.Bundle;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb3 extends ie3 {
    @DexIgnore
    public /* final */ Map<String, Long> b; // = new n4();
    @DexIgnore
    public /* final */ Map<String, Integer> c; // = new n4();
    @DexIgnore
    public long d;

    @DexIgnore
    public fb3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public final void a(String str, long j) {
        if (str == null || str.length() == 0) {
            e().t().a("Ad unit id must be a non-empty string");
        } else {
            c().a(new hd3(this, str, j));
        }
    }

    @DexIgnore
    public final void b(String str, long j) {
        if (str == null || str.length() == 0) {
            e().t().a("Ad unit id must be a non-empty string");
        } else {
            c().a(new gc3(this, str, j));
        }
    }

    @DexIgnore
    public final void c(String str, long j) {
        a();
        g();
        a72.b(str);
        if (this.c.isEmpty()) {
            this.d = j;
        }
        Integer num = this.c.get(str);
        if (num != null) {
            this.c.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (this.c.size() >= 100) {
            e().w().a("Too many ads visible");
        } else {
            this.c.put(str, 1);
            this.b.put(str, Long.valueOf(j));
        }
    }

    @DexIgnore
    public final void d(String str, long j) {
        a();
        g();
        a72.b(str);
        Integer num = this.c.get(str);
        if (num != null) {
            wj3 a = r().a(false);
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.c.remove(str);
                Long l = this.b.get(str);
                if (l == null) {
                    e().t().a("First ad unit exposure time was never set");
                } else {
                    this.b.remove(str);
                    a(str, j - l.longValue(), a);
                }
                if (this.c.isEmpty()) {
                    long j2 = this.d;
                    if (j2 == 0) {
                        e().t().a("First ad exposure time was never set");
                        return;
                    }
                    a(j - j2, a);
                    this.d = 0;
                    return;
                }
                return;
            }
            this.c.put(str, Integer.valueOf(intValue));
            return;
        }
        e().t().a("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    @DexIgnore
    public final void a(long j, wj3 wj3) {
        if (wj3 == null) {
            e().B().a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            e().B().a("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            zj3.a(wj3, bundle, true);
            o().a("am", "_xa", bundle);
        }
    }

    @DexIgnore
    public final void b(long j) {
        for (String str : this.b.keySet()) {
            this.b.put(str, Long.valueOf(j));
        }
        if (!this.b.isEmpty()) {
            this.d = j;
        }
    }

    @DexIgnore
    public final void a(String str, long j, wj3 wj3) {
        if (wj3 == null) {
            e().B().a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            e().B().a("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            zj3.a(wj3, bundle, true);
            o().a("am", "_xu", bundle);
        }
    }

    @DexIgnore
    public final void a(long j) {
        wj3 a = r().a(false);
        for (String str : this.b.keySet()) {
            a(str, j - this.b.get(str).longValue(), a);
        }
        if (!this.b.isEmpty()) {
            a(j - this.d, a);
        }
        b(j);
    }
}
