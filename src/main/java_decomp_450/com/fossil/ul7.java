package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ul7<T> extends hm7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(ul7.class, Object.class, "_consensus");
    @DexIgnore
    public volatile Object _consensus; // = tl7.a;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.ul7<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.hm7
    public ul7<?> a() {
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.hm7
    public final Object a(Object obj) {
        Object obj2 = this._consensus;
        if (obj2 == tl7.a) {
            obj2 = b(c(obj));
        }
        a(obj, obj2);
        return obj2;
    }

    @DexIgnore
    public abstract void a(T t, Object obj);

    @DexIgnore
    public long b() {
        return 0;
    }

    @DexIgnore
    public final Object b(Object obj) {
        if (dj7.a()) {
            if (!(obj != tl7.a)) {
                throw new AssertionError();
            }
        }
        Object obj2 = this._consensus;
        if (obj2 != tl7.a) {
            return obj2;
        }
        if (a.compareAndSet(this, tl7.a, obj)) {
            return obj;
        }
        return this._consensus;
    }

    @DexIgnore
    public abstract Object c(T t);
}
