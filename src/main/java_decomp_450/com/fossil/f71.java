package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f71 extends ji {
    @DexIgnore
    public f71(rc1 rc1, ci ciVar) {
        super(ciVar);
    }

    @DexIgnore
    @Override // com.fossil.ji
    public String createQuery() {
        return "delete from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0";
    }
}
