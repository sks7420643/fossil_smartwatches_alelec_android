package com.fossil;

import com.fossil.ut1;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot1 extends ut1 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ Integer b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ xt1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ut1.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Long c;
        @DexIgnore
        public byte[] d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Long f;
        @DexIgnore
        public xt1 g;

        @DexIgnore
        @Override // com.fossil.ut1.a
        public ut1.a a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ut1.a
        public ut1.a b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ut1.a
        public ut1.a c(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ut1.a
        public ut1.a a(Integer num) {
            this.b = num;
            return this;
        }

        @DexIgnore
        public ut1.a a(byte[] bArr) {
            this.d = bArr;
            return this;
        }

        @DexIgnore
        public ut1.a a(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ut1.a
        public ut1.a a(xt1 xt1) {
            this.g = xt1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ut1.a
        public ut1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " eventTimeMs";
            }
            if (this.c == null) {
                str = str + " eventUptimeMs";
            }
            if (this.f == null) {
                str = str + " timezoneOffsetSeconds";
            }
            if (str.isEmpty()) {
                return new ot1(this.a.longValue(), this.b, this.c.longValue(), this.d, this.e, this.f.longValue(), this.g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public /* synthetic */ ot1(long j, Integer num, long j2, byte[] bArr, String str, long j3, xt1 xt1, a aVar) {
        this.a = j;
        this.b = num;
        this.c = j2;
        this.d = bArr;
        this.e = str;
        this.f = j3;
        this.g = xt1;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public Integer a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public long b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public long c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public xt1 d() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public byte[] e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ut1)) {
            return false;
        }
        ut1 ut1 = (ut1) obj;
        if (this.a == ut1.b() && ((num = this.b) != null ? num.equals(((ot1) ut1).b) : ((ot1) ut1).b == null) && this.c == ut1.c()) {
            if (Arrays.equals(this.d, ut1 instanceof ot1 ? ((ot1) ut1).d : ut1.e()) && ((str = this.e) != null ? str.equals(((ot1) ut1).e) : ((ot1) ut1).e == null) && this.f == ut1.g()) {
                xt1 xt1 = this.g;
                if (xt1 == null) {
                    if (((ot1) ut1).g == null) {
                        return true;
                    }
                } else if (xt1.equals(((ot1) ut1).g)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public String f() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ut1
    public long g() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        int i = (((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003;
        Integer num = this.b;
        int i2 = 0;
        int hashCode = num == null ? 0 : num.hashCode();
        long j2 = this.c;
        int hashCode2 = (((((i ^ hashCode) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ Arrays.hashCode(this.d)) * 1000003;
        String str = this.e;
        int hashCode3 = str == null ? 0 : str.hashCode();
        long j3 = this.f;
        int i3 = (((hashCode2 ^ hashCode3) * 1000003) ^ ((int) ((j3 >>> 32) ^ j3))) * 1000003;
        xt1 xt1 = this.g;
        if (xt1 != null) {
            i2 = xt1.hashCode();
        }
        return i3 ^ i2;
    }

    @DexIgnore
    public String toString() {
        return "LogEvent{eventTimeMs=" + this.a + ", eventCode=" + this.b + ", eventUptimeMs=" + this.c + ", sourceExtension=" + Arrays.toString(this.d) + ", sourceExtensionJsonProto3=" + this.e + ", timezoneOffsetSeconds=" + this.f + ", networkConnectionInfo=" + this.g + "}";
    }
}
