package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.j90;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w60 extends v60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ Set<j90> d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w60> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final w60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 3) {
                boolean z = false;
                byte b = bArr[0];
                Set<j90> a = j90.d.a(bArr[0]);
                if (((byte) (((byte) 128) & bArr[1])) != 0) {
                    z = true;
                }
                byte b2 = bArr[1];
                byte b3 = (byte) (bArr[1] & 63);
                byte b4 = (byte) (bArr[2] & 31);
                if (z) {
                    return new d70(b4, b3, a);
                }
                if (a.isEmpty() || a.size() == j90.values().length) {
                    return new z60(b4, b3, null);
                }
                return new z60(b4, b3, (j90) ea7.d((Iterable) a));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Size("), bArr.length, " not equal to 3."));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public w60[] newArray(int i) {
            return new w60[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public w60 createFromParcel(Parcel parcel) {
            parcel.readInt();
            byte readByte = parcel.readByte();
            byte readByte2 = parcel.readByte();
            j90.a aVar = j90.d;
            String[] createStringArray = parcel.createStringArray();
            j90 j90 = null;
            if (createStringArray != null) {
                ee7.a((Object) createStringArray, "parcel.createStringArray()!!");
                LinkedHashSet linkedHashSet = new LinkedHashSet(t97.h(aVar.a(createStringArray)));
                boolean z = false;
                boolean z2 = parcel.readInt() != 0;
                boolean z3 = parcel.readInt() != 0;
                if (parcel.readInt() != 0) {
                    z = true;
                }
                if (z2) {
                    return new d70(readByte, readByte2, linkedHashSet, z3, z);
                }
                if (!linkedHashSet.isEmpty() && linkedHashSet.size() != j90.values().length) {
                    j90 = (j90) ea7.d((Iterable) linkedHashSet);
                }
                return new z60(readByte, readByte2, j90);
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Set<? extends com.fossil.j90> */
    /* JADX WARN: Multi-variable type inference failed */
    public w60(byte b2, byte b3, Set<? extends j90> set, boolean z, boolean z2, boolean z3) throws IllegalArgumentException {
        super(go0.FIRE_TIME);
        this.b = b2;
        this.c = b3;
        this.d = set;
        this.e = z;
        this.f = z2;
        this.g = z3;
        boolean z4 = true;
        if (b2 >= 0 && 23 >= b2) {
            byte b4 = this.c;
            if (!((b4 < 0 || 59 < b4) ? false : z4)) {
                throw new IllegalArgumentException(yh0.a(yh0.b("minute("), this.c, ") is out of range ", "[0, 59]."));
            }
            return;
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("hour("), this.b, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.fossil.v60, com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(yz0.a(super.a(), r51.z, Byte.valueOf(this.b)), r51.A, Byte.valueOf(this.c));
        r51 r51 = r51.B;
        Set<j90> set = this.d;
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = set.iterator();
        while (it.hasNext()) {
            jSONArray.put(yz0.a((Enum<?>) it.next()));
        }
        return yz0.a(yz0.a(yz0.a(yz0.a(a2, r51, jSONArray), r51.C, Boolean.valueOf(this.e)), r51.E, Boolean.valueOf(this.g)), r51.D, Boolean.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.v60
    public byte[] c() {
        byte[] bArr = new byte[3];
        byte b2 = this.e ? (byte) 128 : (byte) 0;
        byte b3 = this.f ? (byte) 64 : (byte) 0;
        bArr[0] = (byte) ((this.g ? (byte) 128 : (byte) 0) | yz0.a(this.d));
        bArr[1] = (byte) (((byte) (b2 | b3)) | this.c);
        bArr[2] = this.b;
        return bArr;
    }

    @DexIgnore
    public final Set<j90> d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(getClass(), obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            w60 w60 = (w60) obj;
            return this.b == w60.b && this.c == w60.c && yz0.a(this.d) == yz0.a(w60.d) && this.e == w60.e && this.f == w60.f && this.g == w60.g;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
    }

    @DexIgnore
    public final byte getHour() {
        return this.b;
    }

    @DexIgnore
    public final byte getMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v60
    public int hashCode() {
        int hashCode = this.d.hashCode();
        int hashCode2 = Boolean.valueOf(this.e).hashCode();
        int hashCode3 = Boolean.valueOf(this.f).hashCode();
        return Boolean.valueOf(this.g).hashCode() + ((hashCode3 + ((hashCode2 + ((hashCode + (((((super.hashCode() * 31) + this.b) * 31) + this.c) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.v60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            Object[] array = this.d.toArray(new j90[0]);
            if (array != null) {
                j90[] j90Arr = (j90[]) array;
                ArrayList arrayList = new ArrayList(j90Arr.length);
                for (j90 j90 : j90Arr) {
                    arrayList.add(j90.name());
                }
                Object[] array2 = arrayList.toArray(new String[0]);
                if (array2 != null) {
                    parcel.writeStringArray((String[]) array2);
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                }
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        if (parcel != null) {
            parcel.writeInt(this.e ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.f ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.g ? 1 : 0);
        }
    }
}
