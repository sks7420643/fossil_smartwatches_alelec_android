package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ db0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jb0 createFromParcel(Parcel parcel) {
            return new jb0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jb0[] newArray(int i) {
            return new jb0[i];
        }
    }

    @DexIgnore
    public jb0(byte b, yr0 yr0) {
        super(cb0.DEVICE_CONFIG_SYNC, b);
        this.c = db0.b.a(yr0);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(jb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((jb0) obj).c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.DeviceConfigSyncNotification");
    }

    @DexIgnore
    public final db0 getAction() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
    }

    @DexIgnore
    public /* synthetic */ jb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.c = db0.values()[parcel.readInt()];
    }
}
