package com.fossil;

import com.fossil.vt1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt1 extends vt1 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ tt1 c;
    @DexIgnore
    public /* final */ Integer d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ List<ut1> f;
    @DexIgnore
    public /* final */ yt1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends vt1.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public tt1 c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public String e;
        @DexIgnore
        public List<ut1> f;
        @DexIgnore
        public yt1 g;

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a b(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a a(tt1 tt1) {
            this.c = tt1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a a(Integer num) {
            this.d = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a a(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a a(List<ut1> list) {
            this.f = list;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1.a a(yt1 yt1) {
            this.g = yt1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vt1.a
        public vt1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " requestTimeMs";
            }
            if (this.b == null) {
                str = str + " requestUptimeMs";
            }
            if (str.isEmpty()) {
                return new pt1(this.a.longValue(), this.b.longValue(), this.c, this.d, this.e, this.f, this.g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public /* synthetic */ pt1(long j, long j2, tt1 tt1, Integer num, String str, List list, yt1 yt1, a aVar) {
        this.a = j;
        this.b = j2;
        this.c = tt1;
        this.d = num;
        this.e = str;
        this.f = list;
        this.g = yt1;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public tt1 a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public List<ut1> b() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public Integer c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public String d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public yt1 e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        tt1 tt1;
        Integer num;
        String str;
        List<ut1> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof vt1)) {
            return false;
        }
        vt1 vt1 = (vt1) obj;
        if (this.a == vt1.f() && this.b == vt1.g() && ((tt1 = this.c) != null ? tt1.equals(((pt1) vt1).c) : ((pt1) vt1).c == null) && ((num = this.d) != null ? num.equals(((pt1) vt1).d) : ((pt1) vt1).d == null) && ((str = this.e) != null ? str.equals(((pt1) vt1).e) : ((pt1) vt1).e == null) && ((list = this.f) != null ? list.equals(((pt1) vt1).f) : ((pt1) vt1).f == null)) {
            yt1 yt1 = this.g;
            if (yt1 == null) {
                if (((pt1) vt1).g == null) {
                    return true;
                }
            } else if (yt1.equals(((pt1) vt1).g)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public long f() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.vt1
    public long g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        int i = (((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003;
        tt1 tt1 = this.c;
        int i2 = 0;
        int hashCode = (i ^ (tt1 == null ? 0 : tt1.hashCode())) * 1000003;
        Integer num = this.d;
        int hashCode2 = (hashCode ^ (num == null ? 0 : num.hashCode())) * 1000003;
        String str = this.e;
        int hashCode3 = (hashCode2 ^ (str == null ? 0 : str.hashCode())) * 1000003;
        List<ut1> list = this.f;
        int hashCode4 = (hashCode3 ^ (list == null ? 0 : list.hashCode())) * 1000003;
        yt1 yt1 = this.g;
        if (yt1 != null) {
            i2 = yt1.hashCode();
        }
        return hashCode4 ^ i2;
    }

    @DexIgnore
    public String toString() {
        return "LogRequest{requestTimeMs=" + this.a + ", requestUptimeMs=" + this.b + ", clientInfo=" + this.c + ", logSource=" + this.d + ", logSourceName=" + this.e + ", logEvents=" + this.f + ", qosTier=" + this.g + "}";
    }
}
