package com.fossil;

import android.app.Activity;
import android.app.Application;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class je {
    @DexIgnore
    public static Application a(Activity activity) {
        Application application = activity.getApplication();
        if (application != null) {
            return application;
        }
        throw new IllegalStateException("Your activity/fragment is not yet attached to Application. You can't request ViewModel before onCreate call.");
    }

    @DexIgnore
    public static Activity a(Fragment fragment) {
        FragmentActivity activity = fragment.getActivity();
        if (activity != null) {
            return activity;
        }
        throw new IllegalStateException("Can't create ViewModelProvider for detached fragment");
    }

    @DexIgnore
    public static ViewModelProvider a(FragmentActivity fragmentActivity) {
        return a(fragmentActivity, (ViewModelProvider.Factory) null);
    }

    @DexIgnore
    public static ViewModelProvider a(Fragment fragment, ViewModelProvider.Factory factory) {
        Application a = a(a(fragment));
        if (factory == null) {
            factory = ViewModelProvider.a.a(a);
        }
        return new ViewModelProvider(fragment.getViewModelStore(), factory);
    }

    @DexIgnore
    public static ViewModelProvider a(FragmentActivity fragmentActivity, ViewModelProvider.Factory factory) {
        Application a = a((Activity) fragmentActivity);
        if (factory == null) {
            factory = ViewModelProvider.a.a(a);
        }
        return new ViewModelProvider(fragmentActivity.getViewModelStore(), factory);
    }
}
