package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m5 extends i5 {
    @DexIgnore
    public i5[] k0; // = new i5[4];
    @DexIgnore
    public int l0; // = 0;

    @DexIgnore
    public void K() {
        this.l0 = 0;
    }

    @DexIgnore
    public void b(i5 i5Var) {
        int i = this.l0 + 1;
        i5[] i5VarArr = this.k0;
        if (i > i5VarArr.length) {
            this.k0 = (i5[]) Arrays.copyOf(i5VarArr, i5VarArr.length * 2);
        }
        i5[] i5VarArr2 = this.k0;
        int i2 = this.l0;
        i5VarArr2[i2] = i5Var;
        this.l0 = i2 + 1;
    }
}
