package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q11 extends uh1 {
    @DexIgnore
    public /* final */ qk1 G;
    @DexIgnore
    public /* final */ qk1 H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ q11(short s, ri1 ri1, int i, int i2) {
        super(qa1.X, ri1, (i2 & 4) != 0 ? 3 : i);
        this.K = s;
        qk1 qk1 = qk1.FTC;
        this.G = qk1;
        this.H = qk1;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(g51.LEGACY_ERASE_FILE.a).putShort(this.K).array();
        ee7.a((Object) array, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(g51.LEGACY_ERASE_FILE.a()).putShort(this.K).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.J = array2;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public mw0 a(byte b) {
        return tu0.j.a(b);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.y0, yz0.a(this.K));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public qk1 l() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] n() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public qk1 o() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] q() {
        return this.J;
    }
}
