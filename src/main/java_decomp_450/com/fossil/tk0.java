package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk0 extends zk0 {
    @DexIgnore
    public m01 C;

    @DexIgnore
    public tk0(ri1 ri1, en0 en0, String str) {
        super(ri1, en0, wm0.e0, str, false, 16);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        m01 m01 = this.C;
        return m01 != null ? m01 : m01.BOND_NONE;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        if (((zk0) this).x.a().getDeviceType().b()) {
            zk0.a(this, new a51(((zk0) this).w), new uq1(this), ss1.a, (kd7) null, new wi0(this), (gd7) null, 40, (Object) null);
        } else {
            a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.m1, yz0.a(((zk0) this).w.getBondState()));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.B0;
        m01 m01 = this.C;
        return yz0.a(k, r51, m01 != null ? yz0.a(m01) : null);
    }
}
