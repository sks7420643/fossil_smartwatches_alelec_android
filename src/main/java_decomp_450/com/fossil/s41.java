package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s41 {
    @DexIgnore
    public /* synthetic */ s41(zd7 zd7) {
    }

    @DexIgnore
    public final JSONObject a(long j) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("push", new JSONObject().put("set", new JSONObject().put("buddyChallengeApp._.config.step_diff", j)));
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }
}
