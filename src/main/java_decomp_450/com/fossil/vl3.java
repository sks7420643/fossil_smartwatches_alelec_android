package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vl3 extends ii3 implements ki3 {
    @DexIgnore
    public /* final */ xl3 b;

    @DexIgnore
    public vl3(xl3 xl3) {
        super(xl3.u());
        a72.a(xl3);
        this.b = xl3;
    }

    @DexIgnore
    public fm3 m() {
        return this.b.n();
    }

    @DexIgnore
    public jb3 n() {
        return this.b.k();
    }

    @DexIgnore
    public ih3 o() {
        return this.b.i();
    }
}
