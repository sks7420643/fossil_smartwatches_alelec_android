package com.fossil;

import android.content.Context;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s47 implements Runnable {
    @DexIgnore
    public Context a; // = null;
    @DexIgnore
    public Map<String, Integer> b; // = null;
    @DexIgnore
    public a47 c; // = null;

    @DexIgnore
    public s47(Context context, Map<String, Integer> map, a47 a47) {
        this.a = context;
        this.c = a47;
        if (map != null) {
            this.b = map;
        }
    }

    @DexIgnore
    public final v37 a(String str, int i) {
        int i2;
        v37 v37 = new v37();
        Socket socket = new Socket();
        try {
            v37.a(str);
            v37.a(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            v37.a(System.currentTimeMillis() - currentTimeMillis);
            v37.b(inetSocketAddress.getAddress().getHostAddress());
            socket.close();
            try {
                socket.close();
            } catch (Throwable th) {
                z37.m.a(th);
            }
            i2 = 0;
        } catch (IOException e) {
            try {
                z37.m.a((Throwable) e);
                socket.close();
            } catch (Throwable th2) {
                z37.m.a(th2);
            }
        } catch (Throwable th3) {
            z37.m.a(th3);
        }
        v37.b(i2);
        return v37;
        i2 = -1;
        v37.b(i2);
        return v37;
        throw th;
    }

    @DexIgnore
    public final Map<String, Integer> a() {
        String str;
        HashMap hashMap = new HashMap();
        String a2 = w37.a("__MTA_TEST_SPEED__", (String) null);
        if (!(a2 == null || a2.trim().length() == 0)) {
            for (String str2 : a2.split(";")) {
                String[] split = str2.split(",");
                if (!(split == null || split.length != 2 || (str = split[0]) == null || str.trim().length() == 0)) {
                    try {
                        hashMap.put(str, Integer.valueOf(Integer.valueOf(split[1]).intValue()));
                    } catch (NumberFormatException e) {
                        z37.m.a((Throwable) e);
                    }
                }
            }
        }
        return hashMap;
    }

    @DexIgnore
    public void run() {
        k57 f;
        String str;
        try {
            if (this.b == null) {
                this.b = a();
            }
            if (this.b != null) {
                if (this.b.size() != 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (Map.Entry<String, Integer> entry : this.b.entrySet()) {
                        String key = entry.getKey();
                        if (key != null) {
                            if (key.length() != 0) {
                                if (entry.getValue() == null) {
                                    f = z37.m;
                                    str = "port is null for " + key;
                                    f.g(str);
                                } else {
                                    jSONArray.put(a(entry.getKey(), entry.getValue().intValue()).a());
                                }
                            }
                        }
                        f = z37.m;
                        str = "empty domain name.";
                        f.g(str);
                    }
                    if (jSONArray.length() != 0) {
                        h47 h47 = new h47(this.a, z37.a(this.a, false, this.c), this.c);
                        h47.a(jSONArray.toString());
                        new t47(h47).a();
                        return;
                    }
                    return;
                }
            }
            z37.m.e("empty domain list.");
        } catch (Throwable th) {
            z37.m.a(th);
        }
    }
}
