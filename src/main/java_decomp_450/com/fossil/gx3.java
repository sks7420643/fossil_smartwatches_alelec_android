package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gx3<E> extends ny3<E> {
    @DexIgnore
    public /* final */ ny3<E> forward;

    @DexIgnore
    public gx3(ny3<E> ny3) {
        super(jz3.from(ny3.comparator()).reverse());
        this.forward = ny3;
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E ceiling(E e) {
        return this.forward.floor(e);
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        return this.forward.contains(obj);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> createDescendingSet() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E floor(E e) {
        return this.forward.ceiling(e);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> headSetImpl(E e, boolean z) {
        return this.forward.tailSet((Object) e, z).descendingSet();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E higher(E e) {
        return this.forward.lower(e);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public int indexOf(Object obj) {
        int indexOf = this.forward.indexOf(obj);
        if (indexOf == -1) {
            return indexOf;
        }
        return (size() - 1) - indexOf;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return this.forward.isPartialView();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E lower(E e) {
        return this.forward.higher(e);
    }

    @DexIgnore
    public int size() {
        return this.forward.size();
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> subSetImpl(E e, boolean z, E e2, boolean z2) {
        return this.forward.subSet((Object) e2, z2, (Object) e, z).descendingSet();
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> tailSetImpl(E e, boolean z) {
        return this.forward.headSet((Object) e, z).descendingSet();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3, com.fossil.ny3
    public j04<E> descendingIterator() {
        return this.forward.iterator();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3, com.fossil.ny3
    public ny3<E> descendingSet() {
        return this.forward;
    }

    @DexIgnore
    @Override // com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.util.NavigableSet, java.lang.Iterable, java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.ny3, com.fossil.ny3
    public j04<E> iterator() {
        return this.forward.descendingIterator();
    }
}
