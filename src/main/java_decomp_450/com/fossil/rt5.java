package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.xp5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt5 extends ho5 implements qt5, cy6.g {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public pt5 g;
    @DexIgnore
    public qw6<s35> h;
    @DexIgnore
    public xp5 i;
    @DexIgnore
    public ou5 j;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public su5 q;
    @DexIgnore
    public ju5 r;
    @DexIgnore
    public rj4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rt5.u;
        }

        @DexIgnore
        public final rt5 b() {
            return new rt5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements xp5.b {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public b(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        @Override // com.fossil.xp5.b
        public void a() {
            NotificationContactsActivity.z.a(this.a);
        }

        @DexIgnore
        @Override // com.fossil.xp5.b
        public void a(ContactGroup contactGroup) {
            ee7.b(contactGroup, "contactGroup");
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, contactGroup);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public c(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.y;
            Context requireContext = this.a.requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public d(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (this.a.g == null) {
                return;
            }
            if (!z) {
                rt5.b(this.a).a(false);
            } else if (!this.a.f1()) {
                rt5.b(this.a).b(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public e(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(rt5.v.a(), "press on button back");
            rt5.b(this.a).c(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public f(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rt5.b(this.a).d(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public g(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rt5.b(this.a).d(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public h(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsActivity.z.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public i(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.y;
            Context requireContext = this.a.requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rt5 a;

        @DexIgnore
        public j(rt5 rt5) {
            this.a = rt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rt5.b(this.a).h();
        }
    }

    /*
    static {
        String simpleName = rt5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationCallsAndMess\u2026nt::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pt5 b(rt5 rt5) {
        pt5 pt5 = rt5.g;
        if (pt5 != null) {
            return pt5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void B(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "settingsTypeName");
        qw6<s35> qw6 = this.h;
        if (qw6 != null) {
            s35 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void F0() {
        ou5 ou5;
        FLogger.INSTANCE.getLocal().d(u, "showNotificationSettingDialog");
        if (isActive() && (ou5 = this.j) != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            ou5.show(childFragmentManager, ou5.v.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public List<ContactGroup> H0() {
        xp5 xp5 = this.i;
        if (xp5 != null) {
            return xp5.c();
        }
        ee7.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public boolean P0() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public int T() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        qw6<s35> qw6 = this.h;
        CharSequence charSequence = null;
        if (qw6 != null) {
            s35 a2 = qw6.a();
            if (ee7.a((Object) ((a2 == null || (flexibleTextView2 = a2.r) == null) ? null : flexibleTextView2.getText()), (Object) ig5.a(PortfolioApp.g0.c(), 2131886089))) {
                return 0;
            }
            qw6<s35> qw62 = this.h;
            if (qw62 != null) {
                s35 a3 = qw62.a();
                if (!(a3 == null || (flexibleTextView = a3.r) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return ee7.a(charSequence, ig5.a(PortfolioApp.g0.c(), 2131886090)) ? 1 : 2;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void U0() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.setFlags(16, 16);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void d(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            qw6<s35> qw6 = this.h;
            if (qw6 != null) {
                s35 a2 = qw6.a();
                if (a2 != null && (rTLImageView2 = a2.y) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<s35> qw62 = this.h;
        if (qw62 != null) {
            s35 a3 = qw62.a();
            if (a3 != null && (rTLImageView = a3.y) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void e0() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.T(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d(u, "onActivityBackPressed -");
        pt5 pt5 = this.g;
        if (pt5 != null) {
            pt5.c(true);
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final boolean f1() {
        pt5 pt5 = this.g;
        if (pt5 != null) {
            return pt5.i();
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public int h0() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        qw6<s35> qw6 = this.h;
        CharSequence charSequence = null;
        if (qw6 != null) {
            s35 a2 = qw6.a();
            if (ee7.a((Object) ((a2 == null || (flexibleTextView2 = a2.s) == null) ? null : flexibleTextView2.getText()), (Object) ig5.a(PortfolioApp.g0.c(), 2131886089))) {
                return 0;
            }
            qw6<s35> qw62 = this.h;
            if (qw62 != null) {
                s35 a3 = qw62.a();
                if (!(a3 == null || (flexibleTextView = a3.s) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return ee7.a(charSequence, ig5.a(PortfolioApp.g0.c(), 2131886090)) ? 1 : 2;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void m(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "settingsTypeName");
        qw6<s35> qw6 = this.h;
        if (qw6 != null) {
            s35 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.s) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        tj4 f2 = PortfolioApp.g0.c().f();
        ou5 ou5 = this.j;
        if (ou5 != null) {
            f2.a(new qu5(ou5)).a(this);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
                rj4 rj4 = this.s;
                if (rj4 != null) {
                    he a2 = je.a(notificationCallsAndMessagesActivity, rj4).a(ju5.class);
                    ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                    ju5 ju5 = (ju5) a2;
                    this.r = ju5;
                    pt5 pt5 = this.g;
                    if (pt5 == null) {
                        ee7.d("mPresenter");
                        throw null;
                    } else if (ju5 != null) {
                        pt5.a(ju5);
                    } else {
                        ee7.d("mNotificationSettingViewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModelFactory");
                    throw null;
                }
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == 1) {
            this.p = true;
            if (i2 == 112) {
                pt5 pt5 = this.g;
                if (pt5 != null) {
                    pt5.b(false);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else {
                pt5 pt52 = this.g;
                if (pt52 != null) {
                    pt52.c(false);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == 2 && i2 == 111) {
            pt5 pt53 = this.g;
            if (pt53 != null) {
                pt53.k();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        s35 s35 = (s35) qb.a(layoutInflater, 2131558588, viewGroup, false, a1());
        String b2 = eh5.l.a().b("onPrimaryButton");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230982);
            if (drawable != null) {
                drawable.setTint(parseColor);
                s35.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        ou5 ou5 = (ou5) getChildFragmentManager().b(ou5.v.a());
        this.j = ou5;
        if (ou5 == null) {
            this.j = ou5.v.b();
        }
        s35.C.setOnClickListener(new c(this));
        s35.F.setOnCheckedChangeListener(new d(this));
        s35.x.setOnClickListener(new e(this));
        s35.A.setOnClickListener(new f(this));
        s35.B.setOnClickListener(new g(this));
        s35.q.setOnClickListener(new h(this));
        FlexibleSwitchCompat flexibleSwitchCompat = s35.F;
        ee7.a((Object) flexibleSwitchCompat, "binding.swQrEnabled");
        flexibleSwitchCompat.setChecked(f1());
        s35.C.setOnClickListener(new i(this));
        s35.y.setOnClickListener(new j(this));
        xp5 xp5 = new xp5();
        xp5.a(new b(this));
        this.i = xp5;
        RecyclerView recyclerView = s35.E;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        xp5 xp52 = this.i;
        if (xp52 != null) {
            recyclerView.setAdapter(xp52);
            recyclerView.setHasFixedSize(true);
            tj4 f2 = PortfolioApp.g0.c().f();
            ou5 ou52 = this.j;
            if (ou52 != null) {
                f2.a(new qu5(ou52)).a(this);
                this.h = new qw6<>(this, s35);
                V("call_message_view");
                ee7.a((Object) s35, "binding");
                return s35.d();
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
        ee7.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        pt5 pt5 = this.g;
        if (pt5 != null) {
            pt5.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
            }
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FlexibleSwitchCompat flexibleSwitchCompat;
        super.onResume();
        pt5 pt5 = this.g;
        if (pt5 != null) {
            if (pt5 != null) {
                pt5.f();
                qw6<s35> qw6 = this.h;
                if (qw6 != null) {
                    s35 a2 = qw6.a();
                    if (!(a2 == null || (flexibleSwitchCompat = a2.F) == null)) {
                        flexibleSwitchCompat.setChecked(f1());
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void r(List<ContactGroup> list) {
        ee7.b(list, "mListContactGroup");
        xp5 xp5 = this.i;
        if (xp5 != null) {
            xp5.a(list);
        } else {
            ee7.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void u(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        pt5 pt5 = this.g;
        if (pt5 != null) {
            pt5.a(z);
            qw6<s35> qw6 = this.h;
            if (qw6 != null) {
                s35 a2 = qw6.a();
                if (a2 != null && (flexibleSwitchCompat = a2.F) != null) {
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void w0() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.clearFlags(16);
        }
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(pt5 pt5) {
        ee7.b(pt5, "presenter");
        this.g = pt5;
    }

    @DexIgnore
    @Override // com.fossil.qt5
    public void a(ContactGroup contactGroup) {
        ee7.b(contactGroup, "contactGroup");
        xp5 xp5 = this.i;
        if (xp5 != null) {
            xp5.a(contactGroup);
        } else {
            ee7.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ContactGroup contactGroup;
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -638196028) {
            if (hashCode != -4398250) {
                if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363307) {
                    pt5 pt5 = this.g;
                    if (pt5 != null) {
                        pt5.j();
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("SET TO WATCH FAIL")) {
            } else {
                if (i2 == 2131363229) {
                    u(false);
                } else if (i2 == 2131363307) {
                    pt5 pt52 = this.g;
                    if (pt52 != null) {
                        pt52.b(true);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("CONFIRM_REMOVE_CONTACT") && i2 == 2131363307) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (contactGroup = (ContactGroup) extras.getSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE")) != null) {
                pt5 pt53 = this.g;
                if (pt53 != null) {
                    pt53.a(contactGroup);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }
}
