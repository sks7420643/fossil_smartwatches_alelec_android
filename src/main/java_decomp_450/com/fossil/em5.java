package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em5 implements Factory<dm5> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;

    @DexIgnore
    public em5(Provider<QuickResponseRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static em5 a(Provider<QuickResponseRepository> provider) {
        return new em5(provider);
    }

    @DexIgnore
    public static dm5 a(QuickResponseRepository quickResponseRepository) {
        return new dm5(quickResponseRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public dm5 get() {
        return a(this.a.get());
    }
}
