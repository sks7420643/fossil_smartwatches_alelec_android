package com.fossil;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hi implements xi {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ xi e;
    @DexIgnore
    public th f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public hi(Context context, String str, File file, int i, xi xiVar) {
        this.a = context;
        this.b = str;
        this.c = file;
        this.d = i;
        this.e = xiVar;
    }

    @DexIgnore
    public void a(th thVar) {
        this.f = thVar;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.xi, java.lang.AutoCloseable
    public synchronized void close() {
        this.e.close();
        this.g = false;
    }

    @DexIgnore
    @Override // com.fossil.xi
    public String getDatabaseName() {
        return this.e.getDatabaseName();
    }

    @DexIgnore
    @Override // com.fossil.xi
    public synchronized wi getWritableDatabase() {
        if (!this.g) {
            a();
            this.g = true;
        }
        return this.e.getWritableDatabase();
    }

    @DexIgnore
    @Override // com.fossil.xi
    public void setWriteAheadLoggingEnabled(boolean z) {
        this.e.setWriteAheadLoggingEnabled(z);
    }

    @DexIgnore
    public final void a() {
        String databaseName = getDatabaseName();
        File databasePath = this.a.getDatabasePath(databaseName);
        th thVar = this.f;
        ni niVar = new ni(databaseName, this.a.getFilesDir(), thVar == null || thVar.j);
        try {
            niVar.a();
            if (!databasePath.exists()) {
                try {
                    a(databasePath);
                } catch (IOException e2) {
                    throw new RuntimeException("Unable to copy database file.", e2);
                }
            } else if (this.f == null) {
                niVar.b();
            } else {
                try {
                    int a2 = pi.a(databasePath);
                    if (a2 == this.d) {
                        niVar.b();
                    } else if (this.f.a(a2, this.d)) {
                        niVar.b();
                    } else {
                        if (this.a.deleteDatabase(databaseName)) {
                            try {
                                a(databasePath);
                            } catch (IOException e3) {
                                Log.w("ROOM", "Unable to copy database file.", e3);
                            }
                        } else {
                            Log.w("ROOM", "Failed to delete database file (" + databaseName + ") for a copy destructive migration.");
                        }
                        niVar.b();
                    }
                } catch (IOException e4) {
                    Log.w("ROOM", "Unable to read database version.", e4);
                    niVar.b();
                }
            }
        } finally {
            niVar.b();
        }
    }

    @DexIgnore
    public final void a(File file) throws IOException {
        ReadableByteChannel readableByteChannel;
        if (this.b != null) {
            readableByteChannel = Channels.newChannel(this.a.getAssets().open(this.b));
        } else if (this.c != null) {
            readableByteChannel = new FileInputStream(this.c).getChannel();
        } else {
            throw new IllegalStateException("copyFromAssetPath and copyFromFile == null!");
        }
        File createTempFile = File.createTempFile("room-copy-helper", ".tmp", this.a.getCacheDir());
        createTempFile.deleteOnExit();
        qi.a(readableByteChannel, new FileOutputStream(createTempFile).getChannel());
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Failed to create directories for " + file.getAbsolutePath());
        } else if (!createTempFile.renameTo(file)) {
            throw new IOException("Failed to move intermediate file (" + createTempFile.getAbsolutePath() + ") to destination (" + file.getAbsolutePath() + ").");
        }
    }
}
