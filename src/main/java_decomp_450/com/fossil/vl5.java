package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl5 implements Factory<ul5> {
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> e;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> f;
    @DexIgnore
    public /* final */ Provider<qd5> g;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> h;

    @DexIgnore
    public vl5(Provider<MicroAppRepository> provider, Provider<ch5> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<qd5> provider7, Provider<AlarmsRepository> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static vl5 a(Provider<MicroAppRepository> provider, Provider<ch5> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<qd5> provider7, Provider<AlarmsRepository> provider8) {
        return new vl5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static ul5 a(MicroAppRepository microAppRepository, ch5 ch5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, qd5 qd5, AlarmsRepository alarmsRepository) {
        return new ul5(microAppRepository, ch5, deviceRepository, portfolioApp, hybridPresetRepository, notificationsRepository, qd5, alarmsRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ul5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get());
    }
}
