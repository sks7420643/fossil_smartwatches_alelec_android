package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface i94 {
    @DexIgnore
    <T> void a(Class<T> cls, g94<? super T> g94);

    @DexIgnore
    <T> void a(Class<T> cls, Executor executor, g94<? super T> g94);
}
