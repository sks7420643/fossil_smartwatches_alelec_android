package com.fossil;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.an5;
import com.fossil.bn5;
import com.fossil.fl4;
import com.fossil.in5;
import com.fossil.jn5;
import com.fossil.jw6;
import com.fossil.kn5;
import com.fossil.ln5;
import com.fossil.mn5;
import com.fossil.rn5;
import com.fossil.un5;
import com.fossil.vn5;
import com.fossil.wn5;
import com.fossil.xn5;
import com.fossil.xv6;
import com.fossil.yl5;
import com.fossil.ym5;
import com.fossil.yn5;
import com.fossil.yv6;
import com.fossil.zm5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et6 extends ys6 {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public static /* final */ Pattern T;
    @DexIgnore
    public static /* final */ a U; // = new a(null);
    @DexIgnore
    public qd5 A;
    @DexIgnore
    public mn5 B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public ym5 F;
    @DexIgnore
    public zm5 G;
    @DexIgnore
    public jw6 H;
    @DexIgnore
    public fw6 I;
    @DexIgnore
    public WatchLocalizationRepository J;
    @DexIgnore
    public ch5 K;
    @DexIgnore
    public to4 L;
    @DexIgnore
    public lm4 M;
    @DexIgnore
    public WorkoutSettingRepository N;
    @DexIgnore
    public String O;
    @DexIgnore
    public String P;
    @DexIgnore
    public /* final */ zs6 Q;
    @DexIgnore
    public /* final */ cl5 R;
    @DexIgnore
    public un5 e;
    @DexIgnore
    public lh5 f;
    @DexIgnore
    public vn5 g;
    @DexIgnore
    public yn5 h;
    @DexIgnore
    public xn5 i;
    @DexIgnore
    public wn5 j;
    @DexIgnore
    public UserRepository k;
    @DexIgnore
    public DeviceRepository l;
    @DexIgnore
    public rl4 m;
    @DexIgnore
    public kn5 n;
    @DexIgnore
    public ln5 o;
    @DexIgnore
    public in5 p;
    @DexIgnore
    public jn5 q;
    @DexIgnore
    public bn5 r;
    @DexIgnore
    public an5 s;
    @DexIgnore
    public AlarmsRepository t;
    @DexIgnore
    public yl5 u;
    @DexIgnore
    public ad5 v;
    @DexIgnore
    public rn5 w;
    @DexIgnore
    public ch5 x;
    @DexIgnore
    public xv6 y;
    @DexIgnore
    public yv6 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return et6.S;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<mn5.a, fl4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mn5.a aVar) {
            ee7.b(aVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Get current user success: " + aVar.a());
            MFUser a3 = aVar.a();
            if (a3 != null) {
                this.a.s().b(a3.getUserId());
            }
        }

        @DexIgnore
        public void a(fl4.a aVar) {
            ee7.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(et6.U.a(), "Get current user failed");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1", f = "SignUpPresenter.kt", l = {573, 574, 578, 583, 586}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ et6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1$currentUser$1", f = "SignUpPresenter.kt", l = {586}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository B = this.this$0.this$0.B();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = B.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(et6 et6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = et6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00d1 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00d2  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x011d A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x011e  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x013c A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0161  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                r13 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r13.label
                r2 = 5
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                if (r1 == 0) goto L_0x0056
                if (r1 == r6) goto L_0x004e
                if (r1 == r5) goto L_0x0046
                if (r1 == r4) goto L_0x003b
                if (r1 == r3) goto L_0x002c
                if (r1 != r2) goto L_0x0024
                java.lang.Object r0 = r13.L$1
                com.fossil.ko4 r0 = (com.fossil.ko4) r0
                java.lang.Object r0 = r13.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r14)
                goto L_0x013d
            L_0x0024:
                java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r14.<init>(r0)
                throw r14
            L_0x002c:
                java.lang.Object r1 = r13.L$1
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                boolean r3 = r13.Z$0
                java.lang.Object r4 = r13.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r14)
                goto L_0x0120
            L_0x003b:
                boolean r1 = r13.Z$0
                java.lang.Object r4 = r13.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r14)
                goto L_0x00d6
            L_0x0046:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x0099
            L_0x004e:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x0088
            L_0x0056:
                com.fossil.t87.a(r14)
                com.fossil.yi7 r14 = r13.p$
                com.fossil.et6 r1 = r13.this$0
                com.fossil.lm4 r1 = r1.q()
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                java.lang.String r7 = r7.c()
                com.fossil.xe5 r8 = com.fossil.xe5.b
                java.lang.String r8 = r8.a()
                java.lang.String[] r9 = new java.lang.String[r6]
                r10 = 0
                com.fossil.gm4 r11 = com.fossil.gm4.BUDDY_CHALLENGE
                java.lang.String r11 = r11.getStrType()
                r9[r10] = r11
                r13.L$0 = r14
                r13.label = r6
                java.lang.Object r1 = r1.a(r7, r8, r9, r13)
                if (r1 != r0) goto L_0x0087
                return r0
            L_0x0087:
                r1 = r14
            L_0x0088:
                com.fossil.et6 r14 = r13.this$0
                com.fossil.lm4 r14 = r14.q()
                r13.L$0 = r1
                r13.label = r5
                java.lang.Object r14 = r14.a(r13)
                if (r14 != r0) goto L_0x0099
                return r0
            L_0x0099:
                java.lang.Boolean r14 = (java.lang.Boolean) r14
                boolean r14 = r14.booleanValue()
                com.fossil.et6 r5 = r13.this$0
                com.fossil.ch5 r5 = r5.x()
                java.lang.Boolean r7 = com.fossil.pb7.a(r14)
                r5.b(r7)
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                r5.a(r14)
                com.fossil.et6 r5 = r13.this$0
                com.fossil.to4 r5 = r5.p()
                com.fossil.et6 r7 = r13.this$0
                com.fossil.ch5 r7 = r7.x()
                java.lang.String r7 = r7.j()
                r13.L$0 = r1
                r13.Z$0 = r14
                r13.label = r4
                java.lang.Object r4 = r5.a(r7, r13)
                if (r4 != r0) goto L_0x00d2
                return r0
            L_0x00d2:
                r12 = r1
                r1 = r14
                r14 = r4
                r4 = r12
            L_0x00d6:
                com.fossil.ko4 r14 = (com.fossil.ko4) r14
                com.portfolio.platform.data.model.ServerError r5 = r14.a()
                if (r5 == 0) goto L_0x0122
                com.portfolio.platform.data.model.ServerError r5 = r14.a()
                java.lang.Integer r5 = r5.getCode()
                if (r5 != 0) goto L_0x00e9
                goto L_0x0122
            L_0x00e9:
                int r5 = r5.intValue()
                if (r5 != 0) goto L_0x0122
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                com.fossil.et6$a r7 = com.fossil.et6.U
                java.lang.String r7 = r7.a()
                java.lang.String r8 = "reset device Id"
                r5.e(r7, r8)
                com.fossil.et6 r5 = r13.this$0
                com.fossil.ch5 r5 = r5.x()
                r5.x(r6)
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                r13.L$0 = r4
                r13.Z$0 = r1
                r13.L$1 = r14
                r13.label = r3
                java.lang.Object r3 = r5.b(r13)
                if (r3 != r0) goto L_0x011e
                return r0
            L_0x011e:
                r3 = r1
                r1 = r14
            L_0x0120:
                r14 = r1
                r1 = r3
            L_0x0122:
                com.fossil.et6 r3 = r13.this$0
                com.fossil.ti7 r3 = r3.b()
                com.fossil.et6$c$a r5 = new com.fossil.et6$c$a
                r6 = 0
                r5.<init>(r13, r6)
                r13.L$0 = r4
                r13.Z$0 = r1
                r13.L$1 = r14
                r13.label = r2
                java.lang.Object r14 = com.fossil.vh7.a(r3, r5, r13)
                if (r14 != r0) goto L_0x013d
                return r0
            L_0x013d:
                com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.et6$a r1 = com.fossil.et6.U
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "checkOnboarding currentUser="
                r2.append(r3)
                r2.append(r14)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                if (r14 == 0) goto L_0x0173
                com.fossil.et6 r0 = r13.this$0
                com.fossil.zs6 r0 = r0.Q
                r0.f()
                com.fossil.et6 r0 = r13.this$0
                com.fossil.zs6 r0 = r0.Q
                r0.i()
            L_0x0173:
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r0.a(r14)
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.et6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<yv6.d, yv6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public d(et6 et6, SignUpSocialAuth signUpSocialAuth) {
            this.a = et6;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(yv6.d dVar) {
            ee7.b(dVar, "responseValue");
            boolean a2 = dVar.a();
            if (a2) {
                this.a.c(this.b);
            } else if (!a2) {
                this.a.Q.b(this.b);
            }
        }

        @DexIgnore
        public void a(yv6.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.Q.f();
            this.a.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1", f = "SignUpPresenter.kt", l = {525, 536}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ et6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1", f = "SignUpPresenter.kt", l = {526, 527, 528, 529, 530, HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 532}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x007a A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x008e A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x00a2 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00b6 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x00ca A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x00df A[RETURN] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r6) {
                /*
                    r5 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r5.label
                    r2 = 1
                    r3 = 0
                    switch(r1) {
                        case 0: goto L_0x004e;
                        case 1: goto L_0x0046;
                        case 2: goto L_0x003e;
                        case 3: goto L_0x0036;
                        case 4: goto L_0x002e;
                        case 5: goto L_0x0025;
                        case 6: goto L_0x001c;
                        case 7: goto L_0x0013;
                        default: goto L_0x000b;
                    }
                L_0x000b:
                    java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r6.<init>(r0)
                    throw r6
                L_0x0013:
                    java.lang.Object r0 = r5.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r6)
                    goto L_0x00e0
                L_0x001c:
                    java.lang.Object r1 = r5.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r6)
                    goto L_0x00cb
                L_0x0025:
                    java.lang.Object r1 = r5.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r6)
                    goto L_0x00b7
                L_0x002e:
                    java.lang.Object r1 = r5.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r6)
                    goto L_0x00a3
                L_0x0036:
                    java.lang.Object r1 = r5.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r6)
                    goto L_0x008f
                L_0x003e:
                    java.lang.Object r1 = r5.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r6)
                    goto L_0x007b
                L_0x0046:
                    java.lang.Object r1 = r5.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r6)
                    goto L_0x0067
                L_0x004e:
                    com.fossil.t87.a(r6)
                    com.fossil.yi7 r6 = r5.p$
                    com.fossil.et6$e r1 = r5.this$0
                    com.fossil.et6 r1 = r1.this$0
                    com.portfolio.platform.data.source.WatchLocalizationRepository r1 = r1.C()
                    r5.L$0 = r6
                    r5.label = r2
                    java.lang.Object r1 = r1.getWatchLocalizationFromServer(r3, r5)
                    if (r1 != r0) goto L_0x0066
                    return r0
                L_0x0066:
                    r1 = r6
                L_0x0067:
                    com.fossil.et6$e r6 = r5.this$0
                    com.fossil.et6 r6 = r6.this$0
                    com.portfolio.platform.data.source.SummariesRepository r6 = r6.A()
                    r5.L$0 = r1
                    r4 = 2
                    r5.label = r4
                    java.lang.Object r6 = r6.fetchActivitySettings(r5)
                    if (r6 != r0) goto L_0x007b
                    return r0
                L_0x007b:
                    com.fossil.et6$e r6 = r5.this$0
                    com.fossil.et6 r6 = r6.this$0
                    com.portfolio.platform.data.source.SleepSummariesRepository r6 = r6.z()
                    r5.L$0 = r1
                    r4 = 3
                    r5.label = r4
                    java.lang.Object r6 = r6.fetchLastSleepGoal(r5)
                    if (r6 != r0) goto L_0x008f
                    return r0
                L_0x008f:
                    com.fossil.et6$e r6 = r5.this$0
                    com.fossil.et6 r6 = r6.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository r6 = r6.w()
                    r5.L$0 = r1
                    r4 = 4
                    r5.label = r4
                    java.lang.Object r6 = r6.fetchGoalSetting(r5)
                    if (r6 != r0) goto L_0x00a3
                    return r0
                L_0x00a3:
                    com.fossil.et6$e r6 = r5.this$0
                    com.fossil.et6 r6 = r6.this$0
                    com.portfolio.platform.data.source.AlarmsRepository r6 = r6.r()
                    r5.L$0 = r1
                    r4 = 5
                    r5.label = r4
                    java.lang.Object r6 = r6.downloadAlarms(r5)
                    if (r6 != r0) goto L_0x00b7
                    return r0
                L_0x00b7:
                    com.fossil.et6$e r6 = r5.this$0
                    com.fossil.et6 r6 = r6.this$0
                    com.portfolio.platform.data.source.WorkoutSettingRepository r6 = r6.D()
                    r5.L$0 = r1
                    r4 = 6
                    r5.label = r4
                    java.lang.Object r6 = r6.downloadWorkoutSettings(r5)
                    if (r6 != r0) goto L_0x00cb
                    return r0
                L_0x00cb:
                    com.fossil.et6$e r6 = r5.this$0
                    com.fossil.et6 r6 = r6.this$0
                    com.portfolio.platform.data.source.DeviceRepository r6 = r6.t()
                    r4 = 0
                    r5.L$0 = r1
                    r1 = 7
                    r5.label = r1
                    java.lang.Object r6 = com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r6, r3, r5, r2, r4)
                    if (r6 != r0) goto L_0x00e0
                    return r0
                L_0x00e0:
                    com.fossil.i97 r6 = com.fossil.i97.a
                    return r6
                    switch-data {0->0x004e, 1->0x0046, 2->0x003e, 3->0x0036, 4->0x002e, 5->0x0025, 6->0x001c, 7->0x0013, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.et6.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$alarms$1", f = "SignUpPresenter.kt", l = {536}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<Alarm>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<Alarm>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    AlarmsRepository r = this.this$0.this$0.r();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = r.getActiveAlarms(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(et6 et6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = et6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x0027
                if (r1 == r4) goto L_0x001f
                if (r1 != r3) goto L_0x0017
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x0058
            L_0x0017:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x001f:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0042
            L_0x0027:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r1 = r6.p$
                com.fossil.et6 r7 = r6.this$0
                com.fossil.ti7 r7 = r7.c()
                com.fossil.et6$e$a r5 = new com.fossil.et6$e$a
                r5.<init>(r6, r2)
                r6.L$0 = r1
                r6.label = r4
                java.lang.Object r7 = com.fossil.vh7.a(r7, r5, r6)
                if (r7 != r0) goto L_0x0042
                return r0
            L_0x0042:
                com.fossil.et6 r7 = r6.this$0
                com.fossil.ti7 r7 = r7.b()
                com.fossil.et6$e$b r4 = new com.fossil.et6$e$b
                r4.<init>(r6, r2)
                r6.L$0 = r1
                r6.label = r3
                java.lang.Object r7 = com.fossil.vh7.a(r7, r4, r6)
                if (r7 != r0) goto L_0x0058
                return r0
            L_0x0058:
                java.util.List r7 = (java.util.List) r7
                if (r7 != 0) goto L_0x0061
                java.util.ArrayList r7 = new java.util.ArrayList
                r7.<init>()
            L_0x0061:
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                java.util.List r7 = com.fossil.rc5.a(r7)
                r0.a(r7)
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.et6.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements fl4.e<wn5.d, wn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(wn5.d dVar) {
            ee7.b(dVar, "responseValue");
            PortfolioApp.g0.c().f().a(this.a);
            this.a.G();
        }

        @DexIgnore
        public void a(wn5.b bVar) {
            ee7.b(bVar, "errorValue");
            this.a.Q.f();
            this.a.a(bVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<rn5.d, rn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onError$1", f = "SignUpPresenter.kt", l = {513}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ rn5.b $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, rn5.b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$errorValue = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$errorValue, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository B = this.this$0.a.B();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (B.clearAllUser(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.Q.f();
                this.this$0.a.a(this.$errorValue.a(), this.$errorValue.b());
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1", f = "SignUpPresenter.kt", l = {426, 435, 440, 447, 463, 471, Action.Apps.IF}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $currentUser;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$1", f = "SignUpPresenter.kt", l = {426}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        UserRepository B = this.this$0.this$0.a.B();
                        this.L$0 = yi7;
                        this.label = 1;
                        if (B.clearAllUser(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.et6$g$b$b")
            @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$2", f = "SignUpPresenter.kt", l = {HelpSearchRecyclerViewAdapter.TYPE_NO_RESULTS, 442}, m = "invokeSuspend")
            /* renamed from: com.fossil.et6$g$b$b  reason: collision with other inner class name */
            public static final class C0061b extends zb7 implements kd7<yi7, fb7<? super zi5<UserSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0061b(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0061b bVar = new C0061b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super zi5<UserSettings>> fb7) {
                    return ((C0061b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        PortfolioApp c = PortfolioApp.g0.c();
                        this.L$0 = yi7;
                        this.label = 1;
                        if (c.m(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    UserRepository B = this.this$0.this$0.a.B();
                    this.L$0 = yi7;
                    this.label = 2;
                    obj = B.getUserSettingFromServer(this);
                    return obj == a ? a : obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class c implements fl4.e<fs6, ds6> {
                @DexIgnore
                public /* final */ /* synthetic */ b a;
                @DexIgnore
                public /* final */ /* synthetic */ se7 b;
                @DexIgnore
                public /* final */ /* synthetic */ se7 c;
                @DexIgnore
                public /* final */ /* synthetic */ List d;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1", f = "SignUpPresenter.kt", l = {490}, m = "invokeSuspend")
                public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ ds6 $errorValue;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ c this$0;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.et6$g$b$c$a$a")
                    @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1$1", f = "SignUpPresenter.kt", l = {490}, m = "invokeSuspend")
                    /* renamed from: com.fossil.et6$g$b$c$a$a  reason: collision with other inner class name */
                    public static final class C0062a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                        @DexIgnore
                        public Object L$0;
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public yi7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ a this$0;

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public C0062a(a aVar, fb7 fb7) {
                            super(2, fb7);
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.ob7
                        public final fb7<i97> create(Object obj, fb7<?> fb7) {
                            ee7.b(fb7, "completion");
                            C0062a aVar = new C0062a(this.this$0, fb7);
                            aVar.p$ = (yi7) obj;
                            return aVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.kd7
                        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                            return ((C0062a) create(yi7, fb7)).invokeSuspend(i97.a);
                        }

                        @DexIgnore
                        @Override // com.fossil.ob7
                        public final Object invokeSuspend(Object obj) {
                            Object a = nb7.a();
                            int i = this.label;
                            if (i == 0) {
                                t87.a(obj);
                                yi7 yi7 = this.p$;
                                UserRepository B = this.this$0.this$0.a.this$0.a.B();
                                this.L$0 = yi7;
                                this.label = 1;
                                if (B.clearAllUser(this) == a) {
                                    return a;
                                }
                            } else if (i == 1) {
                                yi7 yi72 = (yi7) this.L$0;
                                t87.a(obj);
                            } else {
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                            }
                            return i97.a;
                        }
                    }

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public a(c cVar, ds6 ds6, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = cVar;
                        this.$errorValue = ds6;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        a aVar = new a(this.this$0, this.$errorValue, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            ti7 a2 = this.this$0.a.this$0.a.b();
                            C0062a aVar = new C0062a(this, null);
                            this.L$0 = yi7;
                            this.label = 1;
                            if (vh7.a(a2, aVar, this) == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        this.this$0.a.this$0.a.Q.f();
                        this.this$0.a.this$0.a.a(this.$errorValue.a(), this.$errorValue.b());
                        return i97.a;
                    }
                }

                @DexIgnore
                public c(b bVar, se7 se7, se7 se72, List list) {
                    this.a = bVar;
                    this.b = se7;
                    this.c = se72;
                    this.d = list;
                }

                @DexIgnore
                /* renamed from: a */
                public void onSuccess(fs6 fs6) {
                    ee7.b(fs6, "responseValue");
                    PortfolioApp.g0.c().c((String) this.b.element, (String) this.c.element);
                    FLogger.INSTANCE.getLocal().d(et6.U.a(), "onLoginSuccess download device setting success");
                    for (r87 r87 : this.d) {
                        PortfolioApp.g0.c().d((String) r87.getFirst(), (String) r87.getSecond());
                    }
                    this.a.this$0.a.c((String) this.b.element);
                    PortfolioApp.g0.c().a(this.a.this$0.a.u(), false, 13);
                    this.a.this$0.a.n();
                }

                @DexIgnore
                public void a(ds6 ds6) {
                    ee7.b(ds6, "errorValue");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = et6.U.a();
                    local.d(a2, "onLoginSuccess download device setting fail " + ds6.a());
                    ik7 unused = xh7.b(this.a.this$0.a.e(), null, null, new a(this, ds6, null), 3, null);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$response$1", f = "SignUpPresenter.kt", l = {447}, m = "invokeSuspend")
            public static final class d extends zb7 implements kd7<yi7, fb7<? super zi5<ApiResponse<Device>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    d dVar = new d(this.this$0, fb7);
                    dVar.p$ = (yi7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super zi5<ApiResponse<Device>>> fb7) {
                    return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        DeviceRepository t = this.this$0.this$0.a.t();
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = t.downloadDeviceList(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(g gVar, MFUser mFUser, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$currentUser = mFUser;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$currentUser, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0150 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x017a A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0181  */
            /* JADX WARNING: Removed duplicated region for block: B:37:0x01da  */
            /* JADX WARNING: Removed duplicated region for block: B:50:0x0240  */
            /* JADX WARNING: Removed duplicated region for block: B:58:0x02d7  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r18) {
                /*
                    r17 = this;
                    r0 = r17
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 1
                    r4 = 0
                    java.lang.String r5 = ""
                    switch(r2) {
                        case 0: goto L_0x0091;
                        case 1: goto L_0x0089;
                        case 2: goto L_0x0080;
                        case 3: goto L_0x0077;
                        case 4: goto L_0x006c;
                        case 5: goto L_0x0042;
                        case 6: goto L_0x0024;
                        case 7: goto L_0x0017;
                        default: goto L_0x000f;
                    }
                L_0x000f:
                    java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                    java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                    r1.<init>(r2)
                    throw r1
                L_0x0017:
                    java.lang.Object r1 = r0.L$1
                    com.fossil.zi5 r1 = (com.fossil.zi5) r1
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r18)
                    goto L_0x0319
                L_0x0024:
                    java.lang.Object r1 = r0.L$5
                    java.util.List r1 = (java.util.List) r1
                    java.lang.Object r2 = r0.L$4
                    com.fossil.se7 r2 = (com.fossil.se7) r2
                    java.lang.Object r3 = r0.L$3
                    com.fossil.se7 r3 = (com.fossil.se7) r3
                    java.lang.Object r4 = r0.L$2
                    java.util.List r4 = (java.util.List) r4
                    java.lang.Object r4 = r0.L$1
                    com.fossil.zi5 r4 = (com.fossil.zi5) r4
                    java.lang.Object r4 = r0.L$0
                    com.fossil.yi7 r4 = (com.fossil.yi7) r4
                    com.fossil.t87.a(r18)
                    r12 = r0
                    goto L_0x029c
                L_0x0042:
                    java.lang.Object r2 = r0.L$8
                    com.portfolio.platform.data.model.Device r2 = (com.portfolio.platform.data.model.Device) r2
                    java.lang.Object r2 = r0.L$7
                    java.util.Iterator r2 = (java.util.Iterator) r2
                    java.lang.Object r4 = r0.L$6
                    java.util.List r4 = (java.util.List) r4
                    java.lang.Object r6 = r0.L$5
                    java.util.List r6 = (java.util.List) r6
                    java.lang.Object r7 = r0.L$4
                    com.fossil.se7 r7 = (com.fossil.se7) r7
                    java.lang.Object r8 = r0.L$3
                    com.fossil.se7 r8 = (com.fossil.se7) r8
                    java.lang.Object r9 = r0.L$2
                    java.util.List r9 = (java.util.List) r9
                    java.lang.Object r10 = r0.L$1
                    com.fossil.zi5 r10 = (com.fossil.zi5) r10
                    java.lang.Object r11 = r0.L$0
                    com.fossil.yi7 r11 = (com.fossil.yi7) r11
                    com.fossil.t87.a(r18)
                    r12 = r0
                    goto L_0x0228
                L_0x006c:
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r18)
                    r6 = r18
                    goto L_0x017b
                L_0x0077:
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r18)
                    goto L_0x0151
                L_0x0080:
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r18)
                    goto L_0x0131
                L_0x0089:
                    java.lang.Object r1 = r0.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r18)
                    goto L_0x00d4
                L_0x0091:
                    com.fossil.t87.a(r18)
                    com.fossil.yi7 r2 = r0.p$
                    com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                    com.fossil.et6$a r7 = com.fossil.et6.U
                    java.lang.String r7 = r7.a()
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder
                    r8.<init>()
                    java.lang.String r9 = "onLoginSuccess download userInfo success user "
                    r8.append(r9)
                    com.portfolio.platform.data.model.MFUser r9 = r0.$currentUser
                    r8.append(r9)
                    java.lang.String r8 = r8.toString()
                    r6.d(r7, r8)
                    com.portfolio.platform.data.model.MFUser r6 = r0.$currentUser
                    if (r6 != 0) goto L_0x00eb
                    com.fossil.et6$g r6 = r0.this$0
                    com.fossil.et6 r6 = r6.a
                    com.fossil.ti7 r6 = r6.c()
                    com.fossil.et6$g$b$a r7 = new com.fossil.et6$g$b$a
                    r7.<init>(r0, r4)
                    r0.L$0 = r2
                    r0.label = r3
                    java.lang.Object r2 = com.fossil.vh7.a(r6, r7, r0)
                    if (r2 != r1) goto L_0x00d4
                    return r1
                L_0x00d4:
                    com.fossil.et6$g r1 = r0.this$0
                    com.fossil.et6 r1 = r1.a
                    com.fossil.zs6 r1 = r1.Q
                    r1.f()
                    com.fossil.et6$g r1 = r0.this$0
                    com.fossil.et6 r1 = r1.a
                    r2 = 600(0x258, float:8.41E-43)
                    r1.a(r2, r5)
                    com.fossil.i97 r1 = com.fossil.i97.a
                    return r1
                L_0x00eb:
                    com.fossil.ah5$a r6 = com.fossil.ah5.p
                    com.fossil.ah5 r6 = r6.a()
                    com.portfolio.platform.data.model.MFUser r7 = r0.$currentUser
                    java.lang.String r7 = r7.getUserId()
                    r6.b(r7)
                    com.fossil.pg5 r6 = com.fossil.pg5.i
                    r6.c()
                    com.fossil.et6$g r6 = r0.this$0
                    com.fossil.et6 r6 = r6.a
                    com.fossil.qd5 r6 = r6.s()
                    com.portfolio.platform.data.model.MFUser r7 = r0.$currentUser
                    java.lang.String r7 = r7.getUserId()
                    r6.b(r7)
                    com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r6 = r6.c()
                    com.portfolio.platform.data.model.MFUser r7 = r0.$currentUser
                    java.lang.String r7 = r7.getUserId()
                    r6.q(r7)
                    com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r6 = r6.c()
                    r0.L$0 = r2
                    r7 = 2
                    r0.label = r7
                    java.lang.Object r6 = r6.l(r0)
                    if (r6 != r1) goto L_0x0131
                    return r1
                L_0x0131:
                    com.fossil.et6$g r6 = r0.this$0
                    com.fossil.et6 r6 = r6.a
                    r6.o()
                    com.fossil.et6$g r6 = r0.this$0
                    com.fossil.et6 r6 = r6.a
                    com.fossil.ti7 r6 = r6.b()
                    com.fossil.et6$g$b$b r7 = new com.fossil.et6$g$b$b
                    r7.<init>(r0, r4)
                    r0.L$0 = r2
                    r8 = 3
                    r0.label = r8
                    java.lang.Object r6 = com.fossil.vh7.a(r6, r7, r0)
                    if (r6 != r1) goto L_0x0151
                    return r1
                L_0x0151:
                    com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                    com.fossil.et6$a r7 = com.fossil.et6.U
                    java.lang.String r7 = r7.a()
                    java.lang.String r8 = "onLoginSuccess download require device and preset"
                    r6.d(r7, r8)
                    com.fossil.et6$g r6 = r0.this$0
                    com.fossil.et6 r6 = r6.a
                    com.fossil.ti7 r6 = r6.b()
                    com.fossil.et6$g$b$d r7 = new com.fossil.et6$g$b$d
                    r7.<init>(r0, r4)
                    r0.L$0 = r2
                    r8 = 4
                    r0.label = r8
                    java.lang.Object r6 = com.fossil.vh7.a(r6, r7, r0)
                    if (r6 != r1) goto L_0x017b
                    return r1
                L_0x017b:
                    com.fossil.zi5 r6 = (com.fossil.zi5) r6
                    boolean r7 = r6 instanceof com.fossil.bj5
                    if (r7 == 0) goto L_0x02d7
                    r7 = r6
                    com.fossil.bj5 r7 = (com.fossil.bj5) r7
                    java.lang.Object r7 = r7.a()
                    com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
                    if (r7 == 0) goto L_0x0190
                    java.util.List r4 = r7.get_items()
                L_0x0190:
                    com.fossil.se7 r7 = new com.fossil.se7
                    r7.<init>()
                    r7.element = r5
                    com.fossil.se7 r8 = new com.fossil.se7
                    r8.<init>()
                    r8.element = r5
                    java.util.ArrayList r9 = new java.util.ArrayList
                    r9.<init>()
                    com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                    com.fossil.et6$a r11 = com.fossil.et6.U
                    java.lang.String r11 = r11.a()
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder
                    r12.<init>()
                    java.lang.String r13 = "onLoginSuccess allDevices "
                    r12.append(r13)
                    r12.append(r4)
                    java.lang.String r12 = r12.toString()
                    r10.d(r11, r12)
                    if (r4 == 0) goto L_0x0230
                    java.util.Iterator r10 = r4.iterator()
                    r12 = r0
                    r11 = r2
                    r2 = r10
                    r10 = r6
                    r6 = r9
                    r9 = r4
                    r16 = r8
                    r8 = r7
                    r7 = r16
                L_0x01d4:
                    boolean r13 = r2.hasNext()
                    if (r13 == 0) goto L_0x022a
                    java.lang.Object r13 = r2.next()
                    com.portfolio.platform.data.model.Device r13 = (com.portfolio.platform.data.model.Device) r13
                    com.fossil.r87 r14 = new com.fossil.r87
                    java.lang.String r15 = r13.getDeviceId()
                    java.lang.String r3 = r13.getMacAddress()
                    r14.<init>(r15, r3)
                    r6.add(r14)
                    boolean r3 = r13.isActive()
                    if (r3 == 0) goto L_0x0228
                    java.lang.String r3 = r13.getDeviceId()
                    r8.element = r3
                    java.lang.String r3 = r13.getMacAddress()
                    if (r3 == 0) goto L_0x0203
                    goto L_0x0204
                L_0x0203:
                    r3 = r5
                L_0x0204:
                    r7.element = r3
                    com.fossil.yw6$a r3 = com.fossil.yw6.h
                    com.fossil.yw6 r3 = r3.a()
                    r12.L$0 = r11
                    r12.L$1 = r10
                    r12.L$2 = r9
                    r12.L$3 = r8
                    r12.L$4 = r7
                    r12.L$5 = r6
                    r12.L$6 = r4
                    r12.L$7 = r2
                    r12.L$8 = r13
                    r13 = 5
                    r12.label = r13
                    java.lang.Object r3 = r3.a(r12)
                    if (r3 != r1) goto L_0x0228
                    return r1
                L_0x0228:
                    r3 = 1
                    goto L_0x01d4
                L_0x022a:
                    r2 = r1
                    r1 = r6
                    r3 = r8
                    r4 = r9
                    r6 = r10
                    goto L_0x0236
                L_0x0230:
                    r12 = r0
                    r11 = r2
                    r3 = r7
                    r7 = r8
                    r2 = r1
                    r1 = r9
                L_0x0236:
                    T r5 = r3.element
                    java.lang.String r5 = (java.lang.String) r5
                    boolean r5 = android.text.TextUtils.isEmpty(r5)
                    if (r5 != 0) goto L_0x02cf
                    com.fossil.be5$a r5 = com.fossil.be5.o
                    T r8 = r3.element
                    java.lang.String r8 = (java.lang.String) r8
                    boolean r5 = r5.e(r8)
                    if (r5 == 0) goto L_0x02cf
                    com.fossil.et6$g r5 = r12.this$0
                    com.fossil.et6 r5 = r5.a
                    com.fossil.ch5 r5 = r5.y()
                    T r8 = r3.element
                    java.lang.String r8 = (java.lang.String) r8
                    r9 = 0
                    r13 = 0
                    r5.a(r8, r9, r13)
                    com.fossil.et6$g r5 = r12.this$0
                    com.fossil.et6 r5 = r5.a
                    com.fossil.ch5 r5 = r5.y()
                    T r8 = r3.element
                    java.lang.String r8 = (java.lang.String) r8
                    r9 = 1
                    r5.a(r8, r9)
                    com.fossil.et6$g r5 = r12.this$0
                    com.fossil.et6 r5 = r5.a
                    com.fossil.fw6 r5 = r5.v()
                    com.fossil.fw6$b r8 = new com.fossil.fw6$b
                    T r9 = r3.element
                    java.lang.String r9 = (java.lang.String) r9
                    com.portfolio.platform.data.model.MFUser r10 = r12.$currentUser
                    java.lang.String r10 = r10.getUserId()
                    r8.<init>(r9, r10)
                    r12.L$0 = r11
                    r12.L$1 = r6
                    r12.L$2 = r4
                    r12.L$3 = r3
                    r12.L$4 = r7
                    r12.L$5 = r1
                    r4 = 6
                    r12.label = r4
                    java.lang.Object r4 = com.fossil.gl4.a(r5, r8, r12)
                    if (r4 != r2) goto L_0x029b
                    return r2
                L_0x029b:
                    r2 = r7
                L_0x029c:
                    com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r4 = r4.c()
                    T r5 = r3.element
                    java.lang.String r5 = (java.lang.String) r5
                    T r6 = r2.element
                    java.lang.String r6 = (java.lang.String) r6
                    r4.c(r5, r6)
                    com.fossil.et6$g r4 = r12.this$0
                    com.fossil.et6 r4 = r4.a
                    com.fossil.ad5 r4 = r4.u()
                    T r5 = r3.element
                    java.lang.String r5 = (java.lang.String) r5
                    com.fossil.fl4 r4 = r4.a(r5)
                    com.fossil.es6 r5 = new com.fossil.es6
                    T r6 = r3.element
                    java.lang.String r6 = (java.lang.String) r6
                    r5.<init>(r6)
                    com.fossil.et6$g$b$c r6 = new com.fossil.et6$g$b$c
                    r6.<init>(r12, r3, r2, r1)
                    r4.a(r5, r6)
                    goto L_0x033e
                L_0x02cf:
                    com.fossil.et6$g r1 = r12.this$0
                    com.fossil.et6 r1 = r1.a
                    r1.n()
                    goto L_0x033e
                L_0x02d7:
                    boolean r3 = r6 instanceof com.fossil.yi5
                    if (r3 == 0) goto L_0x033e
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                    com.fossil.et6$a r4 = com.fossil.et6.U
                    java.lang.String r4 = r4.a()
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    java.lang.String r8 = "onLoginSuccess get all device fail "
                    r7.append(r8)
                    r8 = r6
                    com.fossil.yi5 r8 = (com.fossil.yi5) r8
                    int r8 = r8.a()
                    r7.append(r8)
                    java.lang.String r7 = r7.toString()
                    r3.d(r4, r7)
                    com.fossil.et6$g r3 = r0.this$0
                    com.fossil.et6 r3 = r3.a
                    com.portfolio.platform.data.source.UserRepository r3 = r3.B()
                    r0.L$0 = r2
                    r0.L$1 = r6
                    r2 = 7
                    r0.label = r2
                    java.lang.Object r2 = r3.clearAllUser(r0)
                    if (r2 != r1) goto L_0x0318
                    return r1
                L_0x0318:
                    r1 = r6
                L_0x0319:
                    com.fossil.et6$g r2 = r0.this$0
                    com.fossil.et6 r2 = r2.a
                    com.fossil.zs6 r2 = r2.Q
                    r2.f()
                    com.fossil.et6$g r2 = r0.this$0
                    com.fossil.et6 r2 = r2.a
                    com.fossil.yi5 r1 = (com.fossil.yi5) r1
                    int r3 = r1.a()
                    com.portfolio.platform.data.model.ServerError r1 = r1.c()
                    if (r1 == 0) goto L_0x033b
                    java.lang.String r1 = r1.getMessage()
                    if (r1 == 0) goto L_0x033b
                    r5 = r1
                L_0x033b:
                    r2.a(r3, r5)
                L_0x033e:
                    com.fossil.i97 r1 = com.fossil.i97.a
                    return r1
                    switch-data {0->0x0091, 1->0x0089, 2->0x0080, 3->0x0077, 4->0x006c, 5->0x0042, 6->0x0024, 7->0x0017, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.et6.g.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rn5.d dVar) {
            ee7.b(dVar, "responseValue");
            ik7 unused = xh7.b(this.a.e(), null, null, new b(this, dVar.a(), null), 3, null);
        }

        @DexIgnore
        public void a(rn5.b bVar) {
            ee7.b(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + bVar.a());
            ik7 unused = xh7.b(this.a.e(), null, null, new a(this, bVar, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements fl4.e<yl5.e, yl5.d> {
        @DexIgnore
        public void a(yl5.d dVar) {
            ee7.b(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(yl5.e eVar) {
            ee7.b(eVar, "responseValue");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements fl4.e<jw6.d, jw6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth b;

        @DexIgnore
        public i(et6 et6, SignUpEmailAuth signUpEmailAuth) {
            this.a = et6;
            this.b = signUpEmailAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(jw6.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.Q.f();
            this.a.Q.b(this.b);
        }

        @DexIgnore
        public void a(jw6.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.Q.f();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "requestOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.Q.e(cVar.a(), cVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements fl4.e<xv6.d, xv6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public j(et6 et6, String str, String str2) {
            this.a = et6;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(xv6.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.o();
            boolean a2 = dVar.a();
            if (a2) {
                this.a.Q.f();
                zs6 c2 = this.a.Q;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886953);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026_ThisEmailIsAlreadyInUse)");
                c2.S(a3);
                this.a.m();
            } else if (!a2) {
                SignUpEmailAuth signUpEmailAuth = new SignUpEmailAuth();
                signUpEmailAuth.setEmail(this.b);
                signUpEmailAuth.setPassword(this.c);
                this.a.a(signUpEmailAuth);
            }
        }

        @DexIgnore
        public void a(xv6.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.Q.f();
            this.a.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements fl4.e<un5.d, un5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(un5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(un5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.a.Q.f();
            if (2 != cVar.a()) {
                this.a.a(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements fl4.e<vn5.d, vn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public l(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(vn5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(vn5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.a.Q.f();
            if (2 != cVar.a()) {
                this.a.a(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements fl4.e<xn5.d, xn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public m(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(xn5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(xn5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.a.Q.f();
            this.a.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements fl4.e<yn5.d, yn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ et6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public n(et6 et6) {
            this.a = et6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(yn5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(yn5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.a.Q.f();
            this.a.a(cVar.a(), "");
        }
    }

    /*
    static {
        String simpleName = et6.class.getSimpleName();
        ee7.a((Object) simpleName, "SignUpPresenter::class.java.simpleName");
        S = simpleName;
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            T = compile;
        } else {
            ee7.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public et6(zs6 zs6, cl5 cl5) {
        ee7.b(zs6, "mView");
        ee7.b(cl5, "mContext");
        this.Q = zs6;
        this.R = cl5;
    }

    @DexIgnore
    public final SummariesRepository A() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        ee7.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository B() {
        UserRepository userRepository = this.k;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository C() {
        WatchLocalizationRepository watchLocalizationRepository = this.J;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        ee7.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutSettingRepository D() {
        WorkoutSettingRepository workoutSettingRepository = this.N;
        if (workoutSettingRepository != null) {
            return workoutSettingRepository;
        }
        ee7.d("mWorkoutSettingRepository");
        throw null;
    }

    @DexIgnore
    public final boolean E() {
        String str = this.O;
        if (str == null || str.length() == 0) {
            this.Q.a(false, false, "");
        } else if (!kx6.a(this.O)) {
            zs6 zs6 = this.Q;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886955);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            zs6.a(false, true, a2);
        } else {
            this.Q.a(true, false, "");
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean F() {
        if (!TextUtils.isEmpty(this.P)) {
            String str = this.P;
            if (str != null) {
                boolean z2 = str.length() >= 7;
                boolean matches = T.matcher(this.P).matches();
                this.Q.b(z2, matches);
                if (!z2 || !matches) {
                    return false;
                }
                return true;
            }
            ee7.a();
            throw null;
        }
        this.Q.b(false, false);
        return false;
    }

    @DexIgnore
    public final void G() {
        FLogger.INSTANCE.getLocal().d(S, "onLoginSuccess download user info");
        rn5 rn5 = this.w;
        if (rn5 != null) {
            rn5.a(new rn5.c(), new g(this));
        } else {
            ee7.d("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void H() {
        this.Q.a(this);
    }

    @DexIgnore
    public final void I() {
        Locale locale = Locale.getDefault();
        ee7.a((Object) locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            ee7.a((Object) locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                ee7.a((Object) locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append(LocaleConverter.LOCALE_DELIMITER);
                Locale locale4 = Locale.getDefault();
                ee7.a((Object) locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = S;
                local.d(str, "language: " + sb2);
                if (mh7.b(sb2, "zh_CN", true) || mh7.b(sb2, "zh_SG", true) || mh7.b(sb2, "zh_TW", true)) {
                    this.Q.y(true);
                    return;
                } else {
                    this.Q.y(false);
                    return;
                }
            }
        }
        this.Q.y(false);
    }

    @DexIgnore
    public final void J() {
        boolean z2 = TextUtils.isEmpty(this.O) || TextUtils.isEmpty(this.P);
        boolean E2 = E();
        boolean F2 = F();
        if (z2 || !F2 || !E2) {
            this.Q.C0();
        } else {
            this.Q.c0();
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        this.Q.e();
        J();
        I();
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void h() {
        if (PortfolioApp.g0.c().z()) {
            this.Q.q();
        } else {
            a(601, "");
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void i() {
        this.Q.g();
        un5 un5 = this.e;
        if (un5 != null) {
            un5.a(new un5.b(new WeakReference(this.R)), new k(this));
        } else {
            ee7.d("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void j() {
        this.Q.g();
        vn5 vn5 = this.g;
        if (vn5 != null) {
            vn5.a(new vn5.b(new WeakReference(this.R)), new l(this));
        } else {
            ee7.d("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void k() {
        if (!rd5.g.a(this.R, "com.tencent.mm")) {
            rd5.g.b(this.R, "com.tencent.mm");
            return;
        }
        this.Q.g();
        xn5 xn5 = this.i;
        if (xn5 != null) {
            xn5.a(new xn5.b(new WeakReference(this.R)), new m(this));
        } else {
            ee7.d("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void l() {
        this.Q.g();
        yn5 yn5 = this.h;
        if (yn5 != null) {
            yn5.a(new yn5.b(new WeakReference(this.R)), new n(this));
        } else {
            ee7.d("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void m() {
        mn5 mn5 = this.B;
        if (mn5 != null) {
            mn5.a((fl4.b) null, new b(this));
        } else {
            ee7.d("mGetUser");
            throw null;
        }
    }

    @DexIgnore
    public final ik7 n() {
        return xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void o() {
        FLogger.INSTANCE.getLocal().d(no6.R.a(), "downloadOptionalsResources");
        Date date = new Date();
        ik7 unused = xh7.b(e(), null, null, new e(this, null), 3, null);
        jn5 jn5 = this.q;
        if (jn5 != null) {
            jn5.a(new jn5.b(date), (fl4.e) null);
            in5 in5 = this.p;
            if (in5 != null) {
                in5.a(new in5.b(date), (fl4.e) null);
                kn5 kn5 = this.n;
                if (kn5 != null) {
                    kn5.a(new kn5.b(date), (fl4.e) null);
                    ln5 ln5 = this.o;
                    if (ln5 != null) {
                        ln5.a(new ln5.b(date), (fl4.e) null);
                        bn5 bn5 = this.r;
                        if (bn5 != null) {
                            bn5.a(new bn5.b(date), (fl4.e) null);
                            an5 an5 = this.s;
                            if (an5 != null) {
                                an5.a(new an5.b(date), (fl4.e) null);
                                zm5 zm5 = this.G;
                                if (zm5 != null) {
                                    zm5.a(new zm5.b(date), (fl4.e) null);
                                    ym5 ym5 = this.F;
                                    if (ym5 != null) {
                                        ym5.a(new ym5.b(date), (fl4.e) null);
                                    } else {
                                        ee7.d("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                ee7.d("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            ee7.d("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        ee7.d("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    ee7.d("mFetchSleepSessions");
                    throw null;
                }
            } else {
                ee7.d("mFetchActivities");
                throw null;
            }
        } else {
            ee7.d("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final to4 p() {
        to4 to4 = this.L;
        if (to4 != null) {
            return to4;
        }
        ee7.d("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final lm4 q() {
        lm4 lm4 = this.M;
        if (lm4 != null) {
            return lm4;
        }
        ee7.d("flagRepository");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository r() {
        AlarmsRepository alarmsRepository = this.t;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        ee7.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final qd5 s() {
        qd5 qd5 = this.A;
        if (qd5 != null) {
            return qd5;
        }
        ee7.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository t() {
        DeviceRepository deviceRepository = this.l;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        ee7.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final ad5 u() {
        ad5 ad5 = this.v;
        if (ad5 != null) {
            return ad5;
        }
        ee7.d("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final fw6 v() {
        fw6 fw6 = this.I;
        if (fw6 != null) {
            return fw6;
        }
        ee7.d("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository w() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        ee7.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final ch5 x() {
        ch5 ch5 = this.K;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final ch5 y() {
        ch5 ch5 = this.x;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository z() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        ee7.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void a(boolean z2) {
        J();
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void b(String str) {
        ee7.b(str, "password");
        this.P = str;
        J();
    }

    @DexIgnore
    public final void c(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "auth");
        wn5 wn5 = this.j;
        if (wn5 != null) {
            wn5.a(new wn5.c(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new f(this));
        } else {
            ee7.d("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void a(String str) {
        ee7.b(str, Constants.EMAIL);
        this.O = str;
        E();
    }

    @DexIgnore
    public final void b(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.d(str, "checkSocialAccountIsExisted " + signUpSocialAuth);
        yv6 yv6 = this.z;
        if (yv6 != null) {
            yv6.a(new yv6.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new d(this, signUpSocialAuth));
        } else {
            ee7.d("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void a(String str, String str2) {
        ee7.b(str, Constants.EMAIL);
        ee7.b(str2, "password");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = S;
        local.d(str3, "signupEmail " + str + ' ' + str2);
        if (E()) {
            this.Q.g();
            xv6 xv6 = this.y;
            if (xv6 != null) {
                xv6.a(new xv6.b(str), new j(this, str, str2));
            } else {
                ee7.d("mCheckAuthenticationEmailExisting");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, "activeSerial");
        yl5 yl5 = this.u;
        if (yl5 != null) {
            yl5.a(new yl5.c(str), new h());
        } else {
            ee7.d("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ys6
    public void a(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "auth");
        this.Q.g();
        b(signUpSocialAuth);
    }

    @DexIgnore
    public final void a(int i2, String str) {
        ee7.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = S;
        local.d(str2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.Q.b(i2, str);
        } else if (!xw6.b(PortfolioApp.g0.c())) {
            this.Q.b(601, "");
        } else {
            this.Q.b(i2, "");
        }
    }

    @DexIgnore
    public final void a(SignUpEmailAuth signUpEmailAuth) {
        ee7.b(signUpEmailAuth, "emailAuth");
        jw6 jw6 = this.H;
        if (jw6 != null) {
            jw6.a(new jw6.b(signUpEmailAuth.getEmail()), new i(this, signUpEmailAuth));
        } else {
            ee7.d("mRequestEmailOtp");
            throw null;
        }
    }
}
