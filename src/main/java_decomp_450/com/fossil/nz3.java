package com.fossil;

import com.google.j2objc.annotations.Weak;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nz3<E> extends tx3<E> {
    @DexIgnore
    @Weak
    public /* final */ vx3<E> delegate;
    @DexIgnore
    public /* final */ zx3<? extends E> delegateList;

    @DexIgnore
    public nz3(vx3<E> vx3, zx3<? extends E> zx3) {
        this.delegate = vx3;
        this.delegateList = zx3;
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.zx3
    public int copyIntoArray(Object[] objArr, int i) {
        return this.delegateList.copyIntoArray(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.tx3
    public vx3<E> delegateCollection() {
        return this.delegate;
    }

    @DexIgnore
    public zx3<? extends E> delegateList() {
        return this.delegateList;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return (E) this.delegateList.get(i);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r2v1. Raw type applied. Possible types: com.fossil.k04<? extends E>, com.fossil.k04<E> */
    @Override // java.util.List, com.fossil.zx3, com.fossil.zx3
    public k04<E> listIterator(int i) {
        return (k04<? extends E>) this.delegateList.listIterator(i);
    }

    @DexIgnore
    public nz3(vx3<E> vx3, Object[] objArr) {
        this(vx3, zx3.asImmutableList(objArr));
    }
}
