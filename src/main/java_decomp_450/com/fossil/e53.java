package com.fossil;

import android.content.Context;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e53 {
    @DexIgnore
    public static /* final */ v02.g<yl2> a; // = new v02.g<>();
    @DexIgnore
    public static /* final */ v02.a<yl2, v02.d.C0203d> b;
    @DexIgnore
    public static /* final */ v02<v02.d.C0203d> c;
    @DexIgnore
    @Deprecated
    public static /* final */ a53 d; // = new qm2();
    @DexIgnore
    @Deprecated
    public static /* final */ j53 e; // = new gm2();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<R extends i12> extends r12<R, yl2> {
        @DexIgnore
        public a(a12 a12) {
            super(e53.c, a12);
        }
    }

    /*
    static {
        o53 o53 = new o53();
        b = o53;
        c = new v02<>("LocationServices.API", o53, a);
        new jl2();
    }
    */

    @DexIgnore
    public static b53 a(Context context) {
        return new b53(context);
    }

    @DexIgnore
    public static k53 b(Context context) {
        return new k53(context);
    }
}
