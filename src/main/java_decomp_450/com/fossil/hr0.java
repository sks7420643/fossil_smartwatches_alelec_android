package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr0 extends fe7 implements gd7<eu0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hr0(BluetoothDevice bluetoothDevice) {
        super(1);
        this.a = bluetoothDevice;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(eu0 eu0) {
        f60 f60 = f60.k;
        f60.d.remove(this.a.getAddress());
        return i97.a;
    }
}
