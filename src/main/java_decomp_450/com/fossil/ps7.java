package com.fossil;

import com.zendesk.sdk.network.impl.DeviceInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ps7 {
    @DexIgnore
    public static /* final */ BigInteger a;
    @DexIgnore
    public static /* final */ BigInteger b;
    @DexIgnore
    public static /* final */ BigInteger c;
    @DexIgnore
    public static /* final */ BigInteger d;
    @DexIgnore
    public static /* final */ BigInteger e;
    @DexIgnore
    public static /* final */ BigInteger f;

    /*
    static {
        BigInteger valueOf = BigInteger.valueOf(DeviceInfo.BYTES_MULTIPLIER);
        a = valueOf;
        BigInteger multiply = valueOf.multiply(valueOf);
        b = multiply;
        BigInteger multiply2 = a.multiply(multiply);
        c = multiply2;
        BigInteger multiply3 = a.multiply(multiply2);
        d = multiply3;
        BigInteger multiply4 = a.multiply(multiply3);
        e = multiply4;
        a.multiply(multiply4);
        BigInteger multiply5 = BigInteger.valueOf(DeviceInfo.BYTES_MULTIPLIER).multiply(BigInteger.valueOf(1152921504606846976L));
        f = multiply5;
        a.multiply(multiply5);
        Charset.forName("UTF-8");
    }
    */

    @DexIgnore
    public static FileInputStream a(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException("File '" + file + "' does not exist");
        } else if (file.isDirectory()) {
            throw new IOException("File '" + file + "' exists but is a directory");
        } else if (file.canRead()) {
            return new FileInputStream(file);
        } else {
            throw new IOException("File '" + file + "' cannot be read");
        }
    }

    @DexIgnore
    public static byte[] b(File file) throws IOException {
        FileInputStream fileInputStream;
        try {
            fileInputStream = a(file);
            try {
                byte[] a2 = rs7.a(fileInputStream, file.length());
                rs7.a((InputStream) fileInputStream);
                return a2;
            } catch (Throwable th) {
                th = th;
                rs7.a((InputStream) fileInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            rs7.a((InputStream) fileInputStream);
            throw th;
        }
    }

    @DexIgnore
    public static FileOutputStream a(File file, boolean z) throws IOException {
        if (!file.exists()) {
            File parentFile = file.getParentFile();
            if (parentFile != null && !parentFile.mkdirs() && !parentFile.isDirectory()) {
                throw new IOException("Directory '" + parentFile + "' could not be created");
            }
        } else if (file.isDirectory()) {
            throw new IOException("File '" + file + "' exists but is a directory");
        } else if (!file.canWrite()) {
            throw new IOException("File '" + file + "' cannot be written to");
        }
        return new FileOutputStream(file, z);
    }

    @DexIgnore
    public static void a(File file, byte[] bArr) throws IOException {
        a(file, bArr, false);
    }

    @DexIgnore
    public static void a(File file, byte[] bArr, boolean z) throws IOException {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = a(file, z);
            try {
                fileOutputStream.write(bArr);
                fileOutputStream.close();
                rs7.a((OutputStream) fileOutputStream);
            } catch (Throwable th) {
                th = th;
                rs7.a((OutputStream) fileOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            rs7.a((OutputStream) fileOutputStream);
            throw th;
        }
    }
}
