package com.fossil;

import com.fossil.s87;
import java.util.ArrayDeque;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km7 {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        Object obj;
        Object obj2;
        try {
            s87.a aVar = s87.Companion;
            obj = s87.m60constructorimpl(Class.forName("com.fossil.ob7").getCanonicalName());
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            obj = s87.m60constructorimpl(t87.a(th));
        }
        if (s87.m63exceptionOrNullimpl(obj) != null) {
            obj = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        a = (String) obj;
        try {
            s87.a aVar3 = s87.Companion;
            obj2 = s87.m60constructorimpl(Class.forName("com.fossil.km7").getCanonicalName());
        } catch (Throwable th2) {
            s87.a aVar4 = s87.Companion;
            obj2 = s87.m60constructorimpl(t87.a(th2));
        }
        if (s87.m63exceptionOrNullimpl(obj2) != null) {
            obj2 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        String str = (String) obj2;
    }
    */

    @DexIgnore
    public static final <E extends Throwable> E b(E e, sb7 sb7) {
        r87 a2 = a(e);
        Throwable th = (Throwable) a2.component1();
        StackTraceElement[] stackTraceElementArr = (StackTraceElement[]) a2.component2();
        E e2 = (E) wl7.a(th);
        if (e2 == null || (!ee7.a((Object) e2.getMessage(), (Object) th.getMessage()))) {
            return e;
        }
        ArrayDeque<StackTraceElement> a3 = a(sb7);
        if (a3.isEmpty()) {
            return e;
        }
        if (th != e) {
            a(stackTraceElementArr, a3);
        }
        a(th, e2, a3);
        return e2;
    }

    @DexIgnore
    public static final <E extends Throwable> E a(E e, E e2, ArrayDeque<StackTraceElement> arrayDeque) {
        arrayDeque.addFirst(a("Coroutine boundary"));
        StackTraceElement[] stackTrace = e.getStackTrace();
        int a2 = a(stackTrace, a);
        int i = 0;
        if (a2 == -1) {
            Object[] array = arrayDeque.toArray(new StackTraceElement[0]);
            if (array != null) {
                e2.setStackTrace((StackTraceElement[]) array);
                return e2;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        StackTraceElement[] stackTraceElementArr = new StackTraceElement[(arrayDeque.size() + a2)];
        for (int i2 = 0; i2 < a2; i2++) {
            stackTraceElementArr[i2] = stackTrace[i2];
        }
        Iterator<T> it = arrayDeque.iterator();
        while (it.hasNext()) {
            stackTraceElementArr[a2 + i] = it.next();
            i++;
        }
        e2.setStackTrace(stackTraceElementArr);
        return e2;
    }

    @DexIgnore
    public static final <E extends Throwable> E b(E e) {
        E e2 = (E) e.getCause();
        if (e2 != null) {
            boolean z = true;
            if (!(!ee7.a(e2.getClass(), e.getClass()))) {
                StackTraceElement[] stackTrace = e.getStackTrace();
                int length = stackTrace.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = false;
                        break;
                    } else if (a(stackTrace[i])) {
                        break;
                    } else {
                        i++;
                    }
                }
                if (z) {
                    return e2;
                }
            }
        }
        return e;
    }

    @DexIgnore
    public static final <E extends Throwable> r87<E, StackTraceElement[]> a(E e) {
        boolean z;
        Throwable cause = e.getCause();
        if (cause == null || !ee7.a(cause.getClass(), e.getClass())) {
            return w87.a(e, new StackTraceElement[0]);
        }
        StackTraceElement[] stackTrace = e.getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (a(stackTrace[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            return w87.a(cause, stackTrace);
        }
        return w87.a(e, new StackTraceElement[0]);
    }

    @DexIgnore
    public static final ArrayDeque<StackTraceElement> a(sb7 sb7) {
        ArrayDeque<StackTraceElement> arrayDeque = new ArrayDeque<>();
        StackTraceElement stackTraceElement = sb7.getStackTraceElement();
        if (stackTraceElement != null) {
            arrayDeque.add(stackTraceElement);
        }
        while (true) {
            if (!(sb7 instanceof sb7)) {
                sb7 = null;
            }
            if (sb7 == null || (sb7 = sb7.getCallerFrame()) == null) {
                return arrayDeque;
            }
            StackTraceElement stackTraceElement2 = sb7.getStackTraceElement();
            if (stackTraceElement2 != null) {
                arrayDeque.add(stackTraceElement2);
            }
        }
        return arrayDeque;
    }

    @DexIgnore
    public static final StackTraceElement a(String str) {
        return new StackTraceElement("\b\b\b(" + str, "\b", "\b", -1);
    }

    @DexIgnore
    public static final boolean a(StackTraceElement stackTraceElement) {
        return mh7.c(stackTraceElement.getClassName(), "\b\b\b", false, 2, null);
    }

    @DexIgnore
    public static final boolean a(StackTraceElement stackTraceElement, StackTraceElement stackTraceElement2) {
        return stackTraceElement.getLineNumber() == stackTraceElement2.getLineNumber() && ee7.a(stackTraceElement.getMethodName(), stackTraceElement2.getMethodName()) && ee7.a(stackTraceElement.getFileName(), stackTraceElement2.getFileName()) && ee7.a(stackTraceElement.getClassName(), stackTraceElement2.getClassName());
    }

    @DexIgnore
    public static final void a(StackTraceElement[] stackTraceElementArr, ArrayDeque<StackTraceElement> arrayDeque) {
        int length = stackTraceElementArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (a(stackTraceElementArr[i])) {
                break;
            } else {
                i++;
            }
        }
        int i2 = i + 1;
        int length2 = stackTraceElementArr.length - 1;
        if (length2 >= i2) {
            while (true) {
                if (a(stackTraceElementArr[length2], arrayDeque.getLast())) {
                    arrayDeque.removeLast();
                }
                arrayDeque.addFirst(stackTraceElementArr[length2]);
                if (length2 != i2) {
                    length2--;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public static final int a(StackTraceElement[] stackTraceElementArr, String str) {
        int length = stackTraceElementArr.length;
        for (int i = 0; i < length; i++) {
            if (ee7.a((Object) str, (Object) stackTraceElementArr[i].getClassName())) {
                return i;
            }
        }
        return -1;
    }
}
