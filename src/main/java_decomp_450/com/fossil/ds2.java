package com.fossil;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds2<K, V> extends AbstractMap<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    @NullableDecl
    public transient Object a;
    @DexIgnore
    public transient int b; // = bu2.a(3, 1, 1073741823);
    @DexIgnore
    public transient int c;
    @DexIgnore
    @NullableDecl
    public transient Set<K> d;
    @DexIgnore
    @NullableDecl
    public transient Set<Map.Entry<K, V>> e;
    @DexIgnore
    @NullableDecl
    public transient Collection<V> f;
    @DexIgnore
    @NullableDecl
    public transient int[] zza;
    @DexIgnore
    @NullableDecl
    public transient Object[] zzb;
    @DexIgnore
    @NullableDecl
    public transient Object[] zzc;

    @DexIgnore
    public ds2() {
        or2.a(true, (Object) "Expected size must be >= 0");
    }

    @DexIgnore
    public static int zzb(int i, int i2) {
        return i - 1;
    }

    @DexIgnore
    public final void a(int i) {
        this.b = ks2.a(this.b, 32 - Integer.numberOfLeadingZeros(i), 31);
    }

    @DexIgnore
    public final int b(@NullableDecl Object obj) {
        if (zza()) {
            return -1;
        }
        int a2 = ms2.a(obj);
        int a3 = a();
        int a4 = ks2.a(this.a, a2 & a3);
        if (a4 == 0) {
            return -1;
        }
        int i = ~a3;
        int i2 = a2 & i;
        do {
            int i3 = a4 - 1;
            int i4 = this.zza[i3];
            if ((i4 & i) == i2 && mr2.a(obj, this.zzb[i3])) {
                return i3;
            }
            a4 = i4 & a3;
        } while (a4 != 0);
        return -1;
    }

    @DexIgnore
    @NullableDecl
    public final Object c(@NullableDecl Object obj) {
        if (zza()) {
            return g;
        }
        int a2 = a();
        int a3 = ks2.a(obj, null, a2, this.a, this.zza, this.zzb, null);
        if (a3 == -1) {
            return g;
        }
        Object obj2 = this.zzc[a3];
        zza(a3, a2);
        this.c--;
        zzc();
        return obj2;
    }

    @DexIgnore
    public final void clear() {
        if (!zza()) {
            zzc();
            Map<K, V> zzb2 = zzb();
            if (zzb2 != null) {
                this.b = bu2.a(size(), 3, 1073741823);
                zzb2.clear();
                this.a = null;
                this.c = 0;
                return;
            }
            Arrays.fill(this.zzb, 0, this.c, (Object) null);
            Arrays.fill(this.zzc, 0, this.c, (Object) null);
            Object obj = this.a;
            if (obj instanceof byte[]) {
                Arrays.fill((byte[]) obj, (byte) 0);
            } else if (obj instanceof short[]) {
                Arrays.fill((short[]) obj, (short) 0);
            } else {
                Arrays.fill((int[]) obj, 0);
            }
            Arrays.fill(this.zza, 0, this.c, 0);
            this.c = 0;
        }
    }

    @DexIgnore
    public final boolean containsKey(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.containsKey(obj);
        }
        return b(obj) != -1;
    }

    @DexIgnore
    public final boolean containsValue(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.containsValue(obj);
        }
        for (int i = 0; i < this.c; i++) {
            if (mr2.a(obj, this.zzc[i])) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.e;
        if (set != null) {
            return set;
        }
        hs2 hs2 = new hs2(this);
        this.e = hs2;
        return hs2;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final V get(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.get(obj);
        }
        int b2 = b(obj);
        if (b2 == -1) {
            return null;
        }
        return (V) this.zzc[b2];
    }

    @DexIgnore
    public final boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final Set<K> keySet() {
        Set<K> set = this.d;
        if (set != null) {
            return set;
        }
        js2 js2 = new js2(this);
        this.d = js2;
        return js2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v4, resolved type: java.util.LinkedHashMap */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    @NullableDecl
    public final V put(@NullableDecl K k, @NullableDecl V v) {
        int min;
        if (zza()) {
            or2.b(zza(), "Arrays already allocated");
            int i = this.b;
            int max = Math.max(i + 1, 2);
            int highestOneBit = Integer.highestOneBit(max);
            int max2 = Math.max(4, (max <= ((int) (((double) highestOneBit) * 1.0d)) || (highestOneBit = highestOneBit << 1) > 0) ? highestOneBit : 1073741824);
            this.a = ks2.a(max2);
            a(max2 - 1);
            this.zza = new int[i];
            this.zzb = new Object[i];
            this.zzc = new Object[i];
        }
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.put(k, v);
        }
        int[] iArr = this.zza;
        Object[] objArr = this.zzb;
        Object[] objArr2 = this.zzc;
        int i2 = this.c;
        int i3 = i2 + 1;
        int a2 = ms2.a(k);
        int a3 = a();
        int i4 = a2 & a3;
        int a4 = ks2.a(this.a, i4);
        if (a4 != 0) {
            int i5 = ~a3;
            int i6 = a2 & i5;
            int i7 = 0;
            while (true) {
                int i8 = a4 - 1;
                int i9 = iArr[i8];
                if ((i9 & i5) != i6 || !mr2.a(k, objArr[i8])) {
                    int i10 = i9 & a3;
                    int i11 = i7 + 1;
                    if (i10 != 0) {
                        i7 = i11;
                        a4 = i10;
                        objArr = objArr;
                    } else if (i11 >= 9) {
                        LinkedHashMap linkedHashMap = new LinkedHashMap(a() + 1, 1.0f);
                        int zzd = zzd();
                        while (zzd >= 0) {
                            linkedHashMap.put(this.zzb[zzd], this.zzc[zzd]);
                            zzd = zza(zzd);
                        }
                        this.a = linkedHashMap;
                        this.zza = null;
                        this.zzb = null;
                        this.zzc = null;
                        zzc();
                        return (V) linkedHashMap.put(k, v);
                    } else if (i3 > a3) {
                        a3 = a(a3, ks2.b(a3), a2, i2);
                    } else {
                        iArr[i8] = ks2.a(i9, i3, a3);
                    }
                } else {
                    V v2 = (V) objArr2[i8];
                    objArr2[i8] = v;
                    return v2;
                }
            }
        } else if (i3 > a3) {
            a3 = a(a3, ks2.b(a3), a2, i2);
        } else {
            ks2.a(this.a, i4, i3);
        }
        int length = this.zza.length;
        if (i3 > length && (min = Math.min(1073741823, 1 | (Math.max(1, length >>> 1) + length))) != length) {
            this.zza = Arrays.copyOf(this.zza, min);
            this.zzb = Arrays.copyOf(this.zzb, min);
            this.zzc = Arrays.copyOf(this.zzc, min);
        }
        this.zza[i2] = ks2.a(a2, 0, a3);
        this.zzb[i2] = k;
        this.zzc[i2] = v;
        this.c = i3;
        zzc();
        return null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @NullableDecl
    public final V remove(@NullableDecl Object obj) {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.remove(obj);
        }
        V v = (V) c(obj);
        if (v == g) {
            return null;
        }
        return v;
    }

    @DexIgnore
    public final int size() {
        Map<K, V> zzb2 = zzb();
        return zzb2 != null ? zzb2.size() : this.c;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public final Collection<V> values() {
        Collection<V> collection = this.f;
        if (collection != null) {
            return collection;
        }
        ls2 ls2 = new ls2(this);
        this.f = ls2;
        return ls2;
    }

    @DexIgnore
    public final boolean zza() {
        return this.a == null;
    }

    @DexIgnore
    @NullableDecl
    public final Map<K, V> zzb() {
        Object obj = this.a;
        if (obj instanceof Map) {
            return (Map) obj;
        }
        return null;
    }

    @DexIgnore
    public final void zzc() {
        this.b += 32;
    }

    @DexIgnore
    public final int zzd() {
        return isEmpty() ? -1 : 0;
    }

    @DexIgnore
    public final Iterator<K> zze() {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.keySet().iterator();
        }
        return new cs2(this);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> zzf() {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.entrySet().iterator();
        }
        return new fs2(this);
    }

    @DexIgnore
    public final Iterator<V> zzg() {
        Map<K, V> zzb2 = zzb();
        if (zzb2 != null) {
            return zzb2.values().iterator();
        }
        return new es2(this);
    }

    @DexIgnore
    public static /* synthetic */ int zzd(ds2 ds2) {
        int i = ds2.c;
        ds2.c = i - 1;
        return i;
    }

    @DexIgnore
    public final void zza(int i, int i2) {
        int size = size() - 1;
        if (i < size) {
            Object[] objArr = this.zzb;
            Object obj = objArr[size];
            objArr[i] = obj;
            Object[] objArr2 = this.zzc;
            objArr2[i] = objArr2[size];
            objArr[size] = null;
            objArr2[size] = null;
            int[] iArr = this.zza;
            iArr[i] = iArr[size];
            iArr[size] = 0;
            int a2 = ms2.a(obj) & i2;
            int a3 = ks2.a(this.a, a2);
            int i3 = size + 1;
            if (a3 == i3) {
                ks2.a(this.a, a2, i + 1);
                return;
            }
            while (true) {
                int i4 = a3 - 1;
                int[] iArr2 = this.zza;
                int i5 = iArr2[i4];
                int i6 = i5 & i2;
                if (i6 == i3) {
                    iArr2[i4] = ks2.a(i5, i + 1, i2);
                    return;
                }
                a3 = i6;
            }
        } else {
            this.zzb[i] = null;
            this.zzc[i] = null;
            this.zza[i] = 0;
        }
    }

    @DexIgnore
    public final int a() {
        return (1 << (this.b & 31)) - 1;
    }

    @DexIgnore
    public final int a(int i, int i2, int i3, int i4) {
        Object a2 = ks2.a(i2);
        int i5 = i2 - 1;
        if (i4 != 0) {
            ks2.a(a2, i3 & i5, i4 + 1);
        }
        Object obj = this.a;
        int[] iArr = this.zza;
        for (int i6 = 0; i6 <= i; i6++) {
            int a3 = ks2.a(obj, i6);
            while (a3 != 0) {
                int i7 = a3 - 1;
                int i8 = iArr[i7];
                int i9 = ((~i) & i8) | i6;
                int i10 = i9 & i5;
                int a4 = ks2.a(a2, i10);
                ks2.a(a2, i10, a3);
                iArr[i7] = ks2.a(i9, a4, i5);
                a3 = i8 & i;
            }
        }
        this.a = a2;
        a(i5);
        return i5;
    }

    @DexIgnore
    public final int zza(int i) {
        int i2 = i + 1;
        if (i2 < this.c) {
            return i2;
        }
        return -1;
    }
}
