package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa0 extends na0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<fa0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public fa0 createFromParcel(Parcel parcel) {
            return new fa0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public fa0[] newArray(int i) {
            return new fa0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public fa0 m18createFromParcel(Parcel parcel) {
            return new fa0(parcel, null);
        }
    }

    @DexIgnore
    public fa0() {
        super(pa0.BUDDY_CHALLENGE, null, null, 6);
    }

    @DexIgnore
    public fa0(zg0 zg0) {
        super(pa0.BUDDY_CHALLENGE, zg0, null, 4);
    }

    @DexIgnore
    public /* synthetic */ fa0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
