package com.fossil;

import androidx.lifecycle.MutableLiveData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class el4 extends he {
    @DexIgnore
    public /* final */ n87 a; // = o87.a(c.INSTANCE);
    @DexIgnore
    public /* final */ n87 b; // = o87.a(d.INSTANCE);
    @DexIgnore
    public /* final */ n87 c; // = o87.a(e.INSTANCE);
    @DexIgnore
    public /* final */ n87 d; // = o87.a(f.INSTANCE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a() {
            this(false, false, 3, null);
        }

        @DexIgnore
        public a(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.b == aVar.b;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            boolean z = this.a;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = i2 * 31;
            boolean z2 = this.b;
            if (z2 == 0) {
                i = z2;
            }
            return i4 + i;
        }

        @DexIgnore
        public String toString() {
            return "LoadingState(mStartLoading=" + this.a + ", mStopLoading=" + this.b + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(boolean z, boolean z2, int i, zd7 zd7) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2);
        }

        @DexIgnore
        public final void a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ List<ib5> a;

        @DexIgnore
        public b() {
            this(null, 1, null);
        }

        @DexIgnore
        public b(List<ib5> list) {
            ee7.b(list, "permissionCodes");
            this.a = list;
        }

        @DexIgnore
        public final List<ib5> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && ee7.a(this.a, ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<ib5> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PermissionState(permissionCodes=" + this.a + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(List list, int i, zd7 zd7) {
            this((i & 1) != 0 ? new ArrayList() : list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements vc7<a> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final a invoke() {
            return new a(false, false, 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements vc7<MutableLiveData<a>> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final MutableLiveData<a> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends fe7 implements vc7<b> {
        @DexIgnore
        public static /* final */ e INSTANCE; // = new e();

        @DexIgnore
        public e() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final b invoke() {
            return new b(null, 1, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends fe7 implements vc7<MutableLiveData<b>> {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final MutableLiveData<b> invoke() {
            return new MutableLiveData<>();
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(el4 el4, boolean z, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            if ((i & 2) != 0) {
                z2 = false;
            }
            el4.a(z, z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: emitLoadingState");
    }

    @DexIgnore
    public final a a() {
        return (a) this.a.getValue();
    }

    @DexIgnore
    public final MutableLiveData<a> b() {
        return (MutableLiveData) this.b.getValue();
    }

    @DexIgnore
    public final b c() {
        return (b) this.c.getValue();
    }

    @DexIgnore
    public final MutableLiveData<b> d() {
        return (MutableLiveData) this.d.getValue();
    }

    @DexIgnore
    public final void a(boolean z, boolean z2) {
        a().a(z);
        a().b(z2);
        b().a(a());
    }

    @DexIgnore
    public final void a(ib5... ib5Arr) {
        ee7.b(ib5Arr, "permissionCodes");
        c().a().clear();
        c().a().addAll(t97.h(ib5Arr));
        d().a(c());
    }
}
