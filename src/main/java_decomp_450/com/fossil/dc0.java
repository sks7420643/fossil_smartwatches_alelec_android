package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dc0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ zf0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<dc0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public dc0 createFromParcel(Parcel parcel) {
            return new dc0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public dc0[] newArray(int i) {
            return new dc0[i];
        }
    }

    @DexIgnore
    public dc0(byte b, int i, zf0 zf0) {
        super(cb0.RING_PHONE, b, i);
        this.d = zf0;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(dc0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((dc0) obj).d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.RingPhoneRequest");
    }

    @DexIgnore
    public final zf0 getAction() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public /* synthetic */ dc0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.d = zf0.values()[parcel.readInt()];
    }
}
