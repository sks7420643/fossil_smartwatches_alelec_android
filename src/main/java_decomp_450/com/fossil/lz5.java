package com.fossil;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.DragEvent;
import android.view.View;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lz5 implements View.OnDragListener {
    @DexIgnore
    public Handler a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);

        @DexIgnore
        boolean a(View view, String str);

        @DexIgnore
        void b(String str);

        @DexIgnore
        boolean c(String str);
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public lz5(b bVar) {
        this.b = bVar;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.DragEvent r2) {
        /*
            r1 = this;
            android.content.ClipDescription r2 = r2.getClipDescription()
            if (r2 == 0) goto L_0x0047
            java.lang.CharSequence r2 = r2.getLabel()
            java.lang.String r2 = r2.toString()
            int r0 = r2.hashCode()
            switch(r0) {
                case -1787753871: goto L_0x0038;
                case 137912893: goto L_0x0028;
                case 333273330: goto L_0x001f;
                case 672879678: goto L_0x0016;
                default: goto L_0x0015;
            }
        L_0x0015:
            goto L_0x0047
        L_0x0016:
            java.lang.String r0 = "COMPLICATION"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0047
            goto L_0x0040
        L_0x001f:
            java.lang.String r0 = "SWAP_PRESET_COMPLICATION"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0047
            goto L_0x0030
        L_0x0028:
            java.lang.String r0 = "SWAP_PRESET_WATCH_APP"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0047
        L_0x0030:
            com.fossil.lz5$b r0 = r1.b
            if (r0 == 0) goto L_0x0047
            r0.b(r2)
            goto L_0x0047
        L_0x0038:
            java.lang.String r0 = "WATCH_APP"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0047
        L_0x0040:
            com.fossil.lz5$b r0 = r1.b
            if (r0 == 0) goto L_0x0047
            r0.b(r2)
        L_0x0047:
            return
            switch-data {-1787753871->0x0038, 137912893->0x0028, 333273330->0x001f, 672879678->0x0016, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lz5.a(android.view.DragEvent):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(android.view.DragEvent r3) {
        /*
            r2 = this;
            android.content.ClipDescription r3 = r3.getClipDescription()
            if (r3 == 0) goto L_0x004f
            java.lang.CharSequence r3 = r3.getLabel()
            java.lang.String r3 = r3.toString()
            int r0 = r3.hashCode()
            switch(r0) {
                case -1787753871: goto L_0x0040;
                case 137912893: goto L_0x0028;
                case 333273330: goto L_0x001f;
                case 672879678: goto L_0x0016;
                default: goto L_0x0015;
            }
        L_0x0015:
            goto L_0x004f
        L_0x0016:
            java.lang.String r0 = "COMPLICATION"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x004f
            goto L_0x0048
        L_0x001f:
            java.lang.String r0 = "SWAP_PRESET_COMPLICATION"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x004f
            goto L_0x0030
        L_0x0028:
            java.lang.String r0 = "SWAP_PRESET_WATCH_APP"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x004f
        L_0x0030:
            android.os.Handler r0 = r2.a
            if (r0 == 0) goto L_0x0038
            r1 = 0
            r0.removeCallbacksAndMessages(r1)
        L_0x0038:
            com.fossil.lz5$b r0 = r2.b
            if (r0 == 0) goto L_0x004f
            r0.a(r3)
            goto L_0x004f
        L_0x0040:
            java.lang.String r0 = "WATCH_APP"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x004f
        L_0x0048:
            com.fossil.lz5$b r0 = r2.b
            if (r0 == 0) goto L_0x004f
            r0.a(r3)
        L_0x004f:
            return
            switch-data {-1787753871->0x0040, 137912893->0x0028, 333273330->0x001f, 672879678->0x0016, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lz5.b(android.view.DragEvent):void");
    }

    @DexIgnore
    public boolean onDrag(View view, DragEvent dragEvent) {
        b bVar;
        if (dragEvent != null) {
            int action = dragEvent.getAction();
            ClipData clipData = dragEvent.getClipData();
            ClipDescription clipDescription = dragEvent.getClipDescription();
            Integer num = null;
            CharSequence label = clipDescription != null ? clipDescription.getLabel() : null;
            if (clipData != null) {
                int i = 0;
                int itemCount = clipData.getItemCount() - 1;
                if (itemCount >= 0) {
                    while (true) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("onDrag - v=");
                        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
                        sb.append(", action=");
                        sb.append(action);
                        sb.append(", labelDesc=");
                        sb.append(label);
                        sb.append(", item=");
                        sb.append(clipData.getItemAt(i));
                        local.d("CustomizeDragListener", sb.toString());
                        if (i == itemCount) {
                            break;
                        }
                        i++;
                    }
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("onDrag - v=");
                if (view != null) {
                    num = Integer.valueOf(view.getId());
                }
                sb2.append(num);
                sb2.append(", action=");
                sb2.append(action);
                sb2.append(", labelDesc=");
                sb2.append(label);
                local2.d("CustomizeDragListener", sb2.toString());
            }
            if (action != 1) {
                if (action != 3) {
                    if (action != 4) {
                        if (action == 5) {
                            a(dragEvent);
                        } else if (action == 6) {
                            b(dragEvent);
                        }
                    } else if (!dragEvent.getResult() && (bVar = this.b) != null) {
                        bVar.a();
                    }
                } else if (view != null) {
                    a(view, dragEvent);
                }
            } else if (this.a == null) {
                this.a = new Handler(Looper.getMainLooper());
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0033, code lost:
        if (r1.equals("COMPLICATION") != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003c, code lost:
        if (r1.equals("SWAP_PRESET_COMPLICATION") != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        if (r1.equals("SWAP_PRESET_WATCH_APP") != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r4 = r5.getItemAt(0);
        com.fossil.ee7.a((java.lang.Object) r4, "clipData.getItemAt(0)");
        r4 = r4.getIntent();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0056, code lost:
        r5 = r3.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        if (r5 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r4 = r4.getStringExtra("KEY_POSITION");
        com.fossil.ee7.a((java.lang.Object) r4, "it.getStringExtra(KEY_POSITION)");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006a, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
        r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r5.e("CustomizeDragListener", "processDrop - e=" + r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0087, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008e, code lost:
        if (r1.equals("WATCH_APP") != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0094, code lost:
        return a(r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return r5.c(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return false;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.view.View r4, android.view.DragEvent r5) {
        /*
            r3 = this;
            android.os.Handler r0 = r3.a
            if (r0 == 0) goto L_0x0008
            r1 = 0
            r0.removeCallbacksAndMessages(r1)
        L_0x0008:
            android.content.ClipData r5 = r5.getClipData()
            r0 = 0
            if (r5 == 0) goto L_0x0095
            int r1 = r5.getItemCount()
            if (r1 != 0) goto L_0x0017
            goto L_0x0095
        L_0x0017:
            android.content.ClipDescription r1 = r5.getDescription()
            if (r1 == 0) goto L_0x0095
            java.lang.CharSequence r1 = r1.getLabel()
            java.lang.String r1 = r1.toString()
            int r2 = r1.hashCode()
            switch(r2) {
                case -1787753871: goto L_0x0088;
                case 137912893: goto L_0x003f;
                case 333273330: goto L_0x0036;
                case 672879678: goto L_0x002d;
                default: goto L_0x002c;
            }
        L_0x002c:
            goto L_0x0095
        L_0x002d:
            java.lang.String r2 = "COMPLICATION"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0095
            goto L_0x0090
        L_0x0036:
            java.lang.String r4 = "SWAP_PRESET_COMPLICATION"
            boolean r4 = r1.equals(r4)
            if (r4 == 0) goto L_0x0095
            goto L_0x0047
        L_0x003f:
            java.lang.String r4 = "SWAP_PRESET_WATCH_APP"
            boolean r4 = r1.equals(r4)
            if (r4 == 0) goto L_0x0095
        L_0x0047:
            android.content.ClipData$Item r4 = r5.getItemAt(r0)     // Catch:{ Exception -> 0x006a }
            java.lang.String r5 = "clipData.getItemAt(0)"
            com.fossil.ee7.a(r4, r5)     // Catch:{ Exception -> 0x006a }
            android.content.Intent r4 = r4.getIntent()     // Catch:{ Exception -> 0x006a }
            if (r4 == 0) goto L_0x0069
            com.fossil.lz5$b r5 = r3.b     // Catch:{ Exception -> 0x006a }
            if (r5 == 0) goto L_0x0069
            java.lang.String r1 = "KEY_POSITION"
            java.lang.String r4 = r4.getStringExtra(r1)     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = "it.getStringExtra(KEY_POSITION)"
            com.fossil.ee7.a(r4, r1)     // Catch:{ Exception -> 0x006a }
            boolean r0 = r5.c(r4)     // Catch:{ Exception -> 0x006a }
        L_0x0069:
            return r0
        L_0x006a:
            r4 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "processDrop - e="
            r1.append(r2)
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            java.lang.String r1 = "CustomizeDragListener"
            r5.e(r1, r4)
            return r0
        L_0x0088:
            java.lang.String r2 = "WATCH_APP"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0095
        L_0x0090:
            boolean r4 = r3.a(r4, r5)
            return r4
        L_0x0095:
            return r0
            switch-data {-1787753871->0x0088, 137912893->0x003f, 333273330->0x0036, 672879678->0x002d, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lz5.a(android.view.View, android.view.DragEvent):boolean");
    }

    @DexIgnore
    public final boolean a(View view, ClipData clipData) {
        String str;
        Intent intent;
        ClipData.Item itemAt = clipData.getItemAt(0);
        if (itemAt == null || (intent = itemAt.getIntent()) == null || (str = intent.getStringExtra("KEY_ID")) == null) {
            str = "";
        }
        if (itemAt == null) {
            return false;
        }
        b bVar = this.b;
        if (bVar != null ? bVar.a(view, str) : false) {
            return true;
        }
        return false;
    }
}
