package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.c44;
import com.fossil.d74;
import com.fossil.g74;
import com.fossil.x44;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w34 {
    @DexIgnore
    public static /* final */ Comparator<File> A; // = new p();
    @DexIgnore
    public static /* final */ Comparator<File> B; // = new q();
    @DexIgnore
    public static /* final */ Pattern C; // = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    @DexIgnore
    public static /* final */ Map<String, String> D; // = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    @DexIgnore
    public static /* final */ String[] E; // = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
    @DexIgnore
    public static /* final */ FilenameFilter x; // = new j("BeginSession");
    @DexIgnore
    public static /* final */ FilenameFilter y; // = v34.a();
    @DexIgnore
    public static /* final */ FilenameFilter z; // = new o();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ e44 b;
    @DexIgnore
    public /* final */ z34 c;
    @DexIgnore
    public /* final */ u44 d;
    @DexIgnore
    public /* final */ u34 e;
    @DexIgnore
    public /* final */ m64 f;
    @DexIgnore
    public /* final */ j44 g;
    @DexIgnore
    public /* final */ v64 h;
    @DexIgnore
    public /* final */ n34 i;
    @DexIgnore
    public /* final */ d74.b j;
    @DexIgnore
    public /* final */ a0 k;
    @DexIgnore
    public /* final */ x44 l;
    @DexIgnore
    public /* final */ c74 m;
    @DexIgnore
    public /* final */ d74.a n;
    @DexIgnore
    public /* final */ y24 o;
    @DexIgnore
    public /* final */ m84 p;
    @DexIgnore
    public /* final */ String q;
    @DexIgnore
    public /* final */ d34 r;
    @DexIgnore
    public /* final */ s44 s;
    @DexIgnore
    public c44 t;
    @DexIgnore
    public oo3<Boolean> u; // = new oo3<>();
    @DexIgnore
    public oo3<Boolean> v; // = new oo3<>();
    @DexIgnore
    public oo3<Void> w; // = new oo3<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ long a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(long j, String str) {
            this.a = j;
            this.b = str;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            if (w34.this.k()) {
                return null;
            }
            w34.this.l.a(this.a, this.b);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0 implements x44.b {
        @DexIgnore
        public /* final */ v64 a;

        @DexIgnore
        public a0(v64 v64) {
            this.a = v64;
        }

        @DexIgnore
        @Override // com.fossil.x44.b
        public File a() {
            File file = new File(this.a.b(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ u44 a;

        @DexIgnore
        public b(u44 u44) {
            this.a = u44;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            w34.this.s.b();
            new m44(w34.this.h()).a(w34.this.f(), this.a);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements d74.c {
        @DexIgnore
        public b0() {
        }

        @DexIgnore
        @Override // com.fossil.d74.c
        public File[] a() {
            return w34.this.n();
        }

        @DexIgnore
        @Override // com.fossil.d74.c
        public File[] b() {
            return w34.this.m();
        }

        @DexIgnore
        public /* synthetic */ b0(w34 w34, j jVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Map a;

        @DexIgnore
        public c(Map map) {
            this.a = map;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            new m44(w34.this.h()).a(w34.this.f(), this.a);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements d74.a {
        @DexIgnore
        public c0() {
        }

        @DexIgnore
        @Override // com.fossil.d74.a
        public boolean a() {
            return w34.this.k();
        }

        @DexIgnore
        public /* synthetic */ c0(w34 w34, j jVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Void> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            w34.this.d();
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d0 implements Runnable {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ g74 b;
        @DexIgnore
        public /* final */ d74 c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public d0(Context context, g74 g74, d74 d74, boolean z) {
            this.a = context;
            this.b = g74;
            this.c = d74;
            this.d = z;
        }

        @DexIgnore
        public void run() {
            if (t34.b(this.a)) {
                z24.a().a("Attempting to send crash report at time of crash...");
                this.c.a(this.b, this.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            w34 w34 = w34.this;
            w34.a(w34.a(new z()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e0 implements FilenameFilter {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public e0(String str) {
            this.a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            if (!str.equals(this.a + ".cls") && str.contains(this.a) && !str.endsWith(".cls_temp")) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements FilenameFilter {
        @DexIgnore
        public /* final */ /* synthetic */ Set a;

        @DexIgnore
        public f(w34 w34, Set set) {
            this.a = set;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            if (str.length() < 35) {
                return false;
            }
            return this.a.contains(str.substring(0, 35));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements x {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        public g(w34 w34, String str, String str2, long j) {
            this.a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.w34.x
        public void a(z64 z64) throws Exception {
            a74.a(z64, this.a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements x {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        public h(String str, String str2, String str3, String str4, int i) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = i;
        }

        @DexIgnore
        @Override // com.fossil.w34.x
        public void a(z64 z64) throws Exception {
            a74.a(z64, this.a, this.b, this.c, this.d, this.e, w34.this.q);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements x {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public i(w34 w34, String str, String str2, boolean z) {
            this.a = str;
            this.b = str2;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.w34.x
        public void a(z64 z64) throws Exception {
            a74.a(z64, this.a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends y {
        @DexIgnore
        public j(String str) {
            super(str);
        }

        @DexIgnore
        @Override // com.fossil.w34.y
        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k implements x {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ long e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ String h;
        @DexIgnore
        public /* final */ /* synthetic */ String i;

        @DexIgnore
        public k(w34 w34, int i2, String str, int i3, long j, long j2, boolean z, int i4, String str2, String str3) {
            this.a = i2;
            this.b = str;
            this.c = i3;
            this.d = j;
            this.e = j2;
            this.f = z;
            this.g = i4;
            this.h = str2;
            this.i = str3;
        }

        @DexIgnore
        @Override // com.fossil.w34.x
        public void a(z64 z64) throws Exception {
            a74.a(z64, this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements x {
        @DexIgnore
        public /* final */ /* synthetic */ u44 a;

        @DexIgnore
        public l(w34 w34, u44 u44) {
            this.a = u44;
        }

        @DexIgnore
        @Override // com.fossil.w34.x
        public void a(z64 z64) throws Exception {
            a74.a(z64, this.a.b(), (String) null, (String) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements x {
        @DexIgnore
        public /* final */ /* synthetic */ String a;

        @DexIgnore
        public m(String str) {
            this.a = str;
        }

        @DexIgnore
        @Override // com.fossil.w34.x
        public void a(z64 z64) throws Exception {
            a74.a(z64, this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ long a;

        @DexIgnore
        public n(long j) {
            this.a = j;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            Bundle bundle = new Bundle();
            bundle.putInt("fatal", 1);
            bundle.putLong("timestamp", this.a);
            w34.this.r.b("_ae", bundle);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class o implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.length() == 39 && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class p implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class q implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class r implements c44.a {
        @DexIgnore
        public r() {
        }

        @DexIgnore
        @Override // com.fossil.c44.a
        public void a(t74 t74, Thread thread, Throwable th) {
            w34.this.a(t74, thread, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class s implements Callable<no3<Void>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date a;
        @DexIgnore
        public /* final */ /* synthetic */ Throwable b;
        @DexIgnore
        public /* final */ /* synthetic */ Thread c;
        @DexIgnore
        public /* final */ /* synthetic */ t74 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements mo3<y74, Void> {
            @DexIgnore
            public /* final */ /* synthetic */ Executor a;

            @DexIgnore
            public a(Executor executor) {
                this.a = executor;
            }

            @DexIgnore
            /* renamed from: a */
            public no3<Void> then(y74 y74) throws Exception {
                if (y74 == null) {
                    z24.a().d("Received null app settings, cannot send reports at crash time.");
                    return qo3.a((Object) null);
                }
                w34.this.a(y74, true);
                return qo3.a((no3<?>[]) new no3[]{w34.this.q(), w34.this.s.a(this.a, f44.getState(y74))});
            }
        }

        @DexIgnore
        public s(Date date, Throwable th, Thread thread, t74 t74) {
            this.a = date;
            this.b = th;
            this.c = thread;
            this.d = t74;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public no3<Void> call() throws Exception {
            w34.this.c.a();
            long a2 = w34.b(this.a);
            w34.this.s.a(this.b, this.c, a2);
            w34.this.a(this.c, this.b, a2);
            w34.this.b(this.a.getTime());
            b84 b2 = this.d.b();
            int i = b2.b().a;
            int i2 = b2.b().b;
            w34.this.a(i);
            w34.this.d();
            w34.this.d(i2);
            if (!w34.this.b.a()) {
                return qo3.a((Object) null);
            }
            Executor b3 = w34.this.e.b();
            return this.d.a().a(b3, new a(b3));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class t implements mo3<Void, Boolean> {
        @DexIgnore
        public t(w34 w34) {
        }

        @DexIgnore
        /* renamed from: a */
        public no3<Boolean> then(Void r1) throws Exception {
            return qo3.a((Object) true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class u implements mo3<Boolean, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ no3 a;
        @DexIgnore
        public /* final */ /* synthetic */ float b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Callable<no3<Void>> {
            @DexIgnore
            public /* final */ /* synthetic */ Boolean a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w34$u$a$a")
            /* renamed from: com.fossil.w34$u$a$a  reason: collision with other inner class name */
            public class C0226a implements mo3<y74, Void> {
                @DexIgnore
                public /* final */ /* synthetic */ List a;
                @DexIgnore
                public /* final */ /* synthetic */ boolean b;
                @DexIgnore
                public /* final */ /* synthetic */ Executor c;

                @DexIgnore
                public C0226a(List list, boolean z, Executor executor) {
                    this.a = list;
                    this.b = z;
                    this.c = executor;
                }

                @DexIgnore
                /* renamed from: a */
                public no3<Void> then(y74 y74) throws Exception {
                    if (y74 == null) {
                        z24.a().d("Received null app settings, cannot send reports during app startup.");
                        return qo3.a((Object) null);
                    }
                    for (g74 g74 : this.a) {
                        if (g74.getType() == g74.a.JAVA) {
                            w34.b(y74.e, g74.c());
                        }
                    }
                    no3 unused = w34.this.q();
                    w34.this.j.a(y74).a(this.a, this.b, u.this.b);
                    w34.this.s.a(this.c, f44.getState(y74));
                    w34.this.w.b((Void) null);
                    return qo3.a((Object) null);
                }
            }

            @DexIgnore
            public a(Boolean bool) {
                this.a = bool;
            }

            @DexIgnore
            @Override // java.util.concurrent.Callable
            public no3<Void> call() throws Exception {
                List<g74> b2 = w34.this.m.b();
                if (!this.a.booleanValue()) {
                    z24.a().a("Reports are being deleted.");
                    w34.d(w34.this.l());
                    w34.this.m.a(b2);
                    w34.this.s.c();
                    w34.this.w.b((Void) null);
                    return qo3.a((Object) null);
                }
                z24.a().a("Reports are being sent.");
                boolean booleanValue = this.a.booleanValue();
                w34.this.b.a(booleanValue);
                Executor b3 = w34.this.e.b();
                return u.this.a.a(b3, new C0226a(b2, booleanValue, b3));
            }
        }

        @DexIgnore
        public u(no3 no3, float f) {
            this.a = no3;
            this.b = f;
        }

        @DexIgnore
        /* renamed from: a */
        public no3<Void> then(Boolean bool) throws Exception {
            return w34.this.e.c(new a(bool));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class v implements d74.b {
        @DexIgnore
        public v() {
        }

        @DexIgnore
        @Override // com.fossil.d74.b
        public d74 a(y74 y74) {
            String str = y74.c;
            String str2 = y74.d;
            return new d74(y74.e, w34.this.i.a, f44.getState(y74), w34.this.m, w34.this.a(str, str2), w34.this.n);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class w implements FilenameFilter {
        @DexIgnore
        public w() {
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return !w34.z.accept(file, str) && w34.C.matcher(str).matches();
        }

        @DexIgnore
        public /* synthetic */ w(j jVar) {
            this();
        }
    }

    @DexIgnore
    public interface x {
        @DexIgnore
        void a(z64 z64) throws Exception;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class y implements FilenameFilter {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public y(String str) {
            this.a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return str.contains(this.a) && !str.endsWith(".cls_temp");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class z implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return y64.d.accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    @DexIgnore
    public w34(Context context, u34 u34, m64 m64, j44 j44, e44 e44, v64 v64, z34 z34, n34 n34, c74 c74, d74.b bVar, y24 y24, p84 p84, d34 d34, t74 t74) {
        new AtomicInteger(0);
        new AtomicBoolean(false);
        this.a = context;
        this.e = u34;
        this.f = m64;
        this.g = j44;
        this.b = e44;
        this.h = v64;
        this.c = z34;
        this.i = n34;
        if (bVar != null) {
            this.j = bVar;
        } else {
            this.j = b();
        }
        this.o = y24;
        this.q = p84.a();
        this.r = d34;
        this.d = new u44();
        this.k = new a0(v64);
        this.l = new x44(context, this.k);
        this.m = c74 == null ? new c74(new b0(this, null)) : c74;
        this.n = new c0(this, null);
        j84 j84 = new j84(1024, new l84(10));
        this.p = j84;
        this.s = s44.a(context, j44, v64, n34, this.l, this.d, j84, t74);
    }

    @DexIgnore
    public static boolean u() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public static long v() {
        return b(new Date());
    }

    @DexIgnore
    public final File[] b(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    @DexIgnore
    public File[] o() {
        return a(x);
    }

    @DexIgnore
    public final File[] p() {
        File[] o2 = o();
        Arrays.sort(o2, A);
        return o2;
    }

    @DexIgnore
    public final no3<Void> q() {
        ArrayList arrayList = new ArrayList();
        File[] l2 = l();
        for (File file : l2) {
            try {
                arrayList.add(a(Long.parseLong(file.getName().substring(3))));
            } catch (NumberFormatException unused) {
                z24.a().a("Could not parse timestamp from file " + file.getName());
            }
            file.delete();
        }
        return qo3.a((Collection<? extends no3<?>>) arrayList);
    }

    @DexIgnore
    public void r() {
        this.e.b(new d());
    }

    @DexIgnore
    public final no3<Boolean> s() {
        if (this.b.a()) {
            z24.a().a("Automatic data collection is enabled. Allowing upload.");
            this.u.b((Boolean) false);
            return qo3.a((Object) true);
        }
        z24.a().a("Automatic data collection is disabled.");
        z24.a().a("Notifying that unsent reports are available.");
        this.u.b((Boolean) true);
        no3<TContinuationResult> a2 = this.b.b().a(new t(this));
        z24.a().a("Waiting for send/deleteUnsentReports to be called.");
        return v44.a(a2, this.v.a());
    }

    @DexIgnore
    public static String i(String str) {
        return str.replaceAll(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "");
    }

    @DexIgnore
    public final d74.b b() {
        return new v();
    }

    @DexIgnore
    public void d(String str) {
        this.d.a(str);
        a(this.d);
    }

    @DexIgnore
    public final Context e() {
        return this.a;
    }

    @DexIgnore
    public final String f() {
        File[] p2 = p();
        if (p2.length > 0) {
            return a(p2[0]);
        }
        return null;
    }

    @DexIgnore
    public final void g(String str) throws Exception {
        String str2 = Build.VERSION.RELEASE;
        String str3 = Build.VERSION.CODENAME;
        boolean k2 = t34.k(e());
        a(str, "SessionOS", new i(this, str2, str3, k2));
        this.o.a(str, str2, str3, k2);
    }

    @DexIgnore
    public final void h(String str) throws Exception {
        a(str, "SessionUser", new l(this, b(str)));
    }

    @DexIgnore
    public File j() {
        return new File(h(), "nonfatal-sessions");
    }

    @DexIgnore
    public boolean k() {
        c44 c44 = this.t;
        return c44 != null && c44.a();
    }

    @DexIgnore
    public File[] l() {
        return a(y);
    }

    @DexIgnore
    public File[] m() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, a(g(), z));
        Collections.addAll(linkedList, a(j(), z));
        Collections.addAll(linkedList, a(h(), z));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    @DexIgnore
    public File[] n() {
        return b(i().listFiles());
    }

    @DexIgnore
    public void b(String str, String str2) {
        try {
            this.d.a(str, str2);
            a(this.d.a());
        } catch (IllegalArgumentException e2) {
            Context context = this.a;
            if (context == null || !t34.i(context)) {
                z24.a().b("Attempting to set custom attribute with null key, ignoring.");
                return;
            }
            throw e2;
        }
    }

    @DexIgnore
    public boolean c() {
        if (!this.c.c()) {
            String f2 = f();
            return f2 != null && this.o.c(f2);
        }
        z24.a().a("Found previous crash marker.");
        this.c.d();
        return Boolean.TRUE.booleanValue();
    }

    @DexIgnore
    public final void e(String str) throws Exception {
        String b2 = this.g.b();
        n34 n34 = this.i;
        String str2 = n34.e;
        String str3 = n34.f;
        String a2 = this.g.a();
        int id = g44.determineFrom(this.i.c).getId();
        a(str, "SessionApp", new h(b2, str2, str3, a2, id));
        this.o.a(str, b2, str2, str3, a2, id, this.q);
    }

    @DexIgnore
    public File i() {
        return new File(h(), "native-sessions");
    }

    @DexIgnore
    public final void d() throws Exception {
        long v2 = v();
        String s34 = new s34(this.g).toString();
        z24 a2 = z24.a();
        a2.a("Opening a new session with ID " + s34);
        this.o.d(s34);
        a(s34, v2);
        e(s34);
        g(s34);
        f(s34);
        this.l.b(s34);
        this.s.a(i(s34), v2);
    }

    @DexIgnore
    public File h() {
        return this.h.b();
    }

    @DexIgnore
    public final void f(String str) throws Exception {
        Context e2 = e();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int a2 = t34.a();
        String str2 = Build.MODEL;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long b2 = t34.b();
        long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        boolean j2 = t34.j(e2);
        int c2 = t34.c(e2);
        String str3 = Build.MANUFACTURER;
        String str4 = Build.PRODUCT;
        a(str, "SessionDevice", new k(this, a2, str2, availableProcessors, b2, blockCount, j2, c2, str3, str4));
        this.o.a(str, a2, str2, availableProcessors, b2, blockCount, j2, c2, str3, str4);
    }

    @DexIgnore
    public File g() {
        return new File(h(), "fatal-sessions");
    }

    @DexIgnore
    public boolean b(int i2) {
        this.e.a();
        if (k()) {
            z24.a().a("Skipping session finalization because a crash has already occurred.");
            return Boolean.FALSE.booleanValue();
        }
        z24.a().a("Finalizing previously open sessions.");
        try {
            a(i2, false);
            z24.a().a("Closed all previously open sessions");
            return true;
        } catch (Exception e2) {
            z24.a().b("Unable to finalize previously open sessions.", e2);
            return false;
        }
    }

    @DexIgnore
    public final File[] c(String str) {
        return a(new e0(str));
    }

    @DexIgnore
    public void a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, t74 t74) {
        r();
        c44 c44 = new c44(new r(), t74, uncaughtExceptionHandler);
        this.t = c44;
        Thread.setDefaultUncaughtExceptionHandler(c44);
    }

    @DexIgnore
    public final void c(int i2) {
        HashSet hashSet = new HashSet();
        File[] p2 = p();
        int min = Math.min(i2, p2.length);
        for (int i3 = 0; i3 < min; i3++) {
            hashSet.add(a(p2[i3]));
        }
        this.l.a(hashSet);
        a(a(new w(null)), hashSet);
    }

    @DexIgnore
    public synchronized void a(t74 t74, Thread thread, Throwable th) {
        z24 a2 = z24.a();
        a2.a("Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
        try {
            v44.a(this.e.c(new s(new Date(), th, thread, t74)));
        } catch (Exception unused) {
        }
    }

    @DexIgnore
    public static long b(Date date) {
        return date.getTime() / 1000;
    }

    @DexIgnore
    public void d(int i2) {
        int a2 = i2 - v44.a(i(), g(), i2, B);
        v44.a(h(), z, a2 - v44.a(j(), a2, B), B);
    }

    @DexIgnore
    public final void b(long j2) {
        try {
            File h2 = h();
            new File(h2, ".ae" + j2).createNewFile();
        } catch (IOException unused) {
            z24.a().a("Could not write app exception marker.");
        }
    }

    @DexIgnore
    public static void b(String str, File file) throws Exception {
        if (str != null) {
            a(file, new m(str));
        }
    }

    @DexIgnore
    public final u44 b(String str) {
        if (k()) {
            return this.d;
        }
        return new m44(h()).c(str);
    }

    @DexIgnore
    public static void d(File[] fileArr) {
        if (fileArr != null) {
            for (File file : fileArr) {
                file.delete();
            }
        }
    }

    @DexIgnore
    public no3<Void> a(float f2, no3<y74> no3) {
        if (!this.m.a()) {
            z24.a().a("No reports are available.");
            this.u.b((Boolean) false);
            return qo3.a((Object) null);
        }
        z24.a().a("Unsent reports are available.");
        return s().a(new u(no3, f2));
    }

    @DexIgnore
    public void a(long j2, String str) {
        this.e.b(new a(j2, str));
    }

    @DexIgnore
    public final void a(u44 u44) {
        this.e.b(new b(u44));
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        this.e.b(new c(map));
    }

    @DexIgnore
    public static String a(File file) {
        return file.getName().substring(0, 35);
    }

    @DexIgnore
    public void a(int i2) throws Exception {
        a(i2, true);
    }

    @DexIgnore
    public final void a(int i2, boolean z2) throws Exception {
        int i3 = !z2;
        c(i3 + 8);
        File[] p2 = p();
        if (p2.length <= i3) {
            z24.a().a("No open sessions to be closed.");
            return;
        }
        String a2 = a(p2[i3]);
        h(a2);
        if (z2) {
            this.s.a();
        } else if (this.o.c(a2)) {
            a(a2);
            if (!this.o.a(a2)) {
                z24.a().a("Could not finalize native session: " + a2);
            }
        }
        a(p2, i3, i2);
        this.s.a(v());
    }

    @DexIgnore
    public final void a(File[] fileArr, int i2, int i3) {
        z24.a().a("Closing open sessions.");
        while (i2 < fileArr.length) {
            File file = fileArr[i2];
            String a2 = a(file);
            z24 a3 = z24.a();
            a3.a("Closing session: " + a2);
            a(file, a2, i3);
            i2++;
        }
    }

    @DexIgnore
    public final void a(y64 y64) {
        if (y64 != null) {
            try {
                y64.a();
            } catch (IOException e2) {
                z24.a().b("Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    @DexIgnore
    public final File[] a(FilenameFilter filenameFilter) {
        return a(h(), filenameFilter);
    }

    @DexIgnore
    public final File[] a(File file, FilenameFilter filenameFilter) {
        return b(file.listFiles(filenameFilter));
    }

    @DexIgnore
    public final void a(String str, int i2) {
        File h2 = h();
        v44.a(h2, new y(str + "SessionEvent"), i2, B);
    }

    @DexIgnore
    public final void a(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = C.matcher(name);
            if (!matcher.matches()) {
                z24.a().a("Deleting unknown file: " + name);
                file.delete();
            } else if (!set.contains(matcher.group(1))) {
                z24.a().a("Trimming session file: " + name);
                file.delete();
            }
        }
    }

    @DexIgnore
    public final File[] a(String str, File[] fileArr, int i2) {
        if (fileArr.length <= i2) {
            return fileArr;
        }
        z24.a().a(String.format(Locale.US, "Trimming down to %d logged exceptions.", Integer.valueOf(i2)));
        a(str, i2);
        return a(new y(str + "SessionEvent"));
    }

    @DexIgnore
    public void a() {
        this.e.a(new e());
    }

    @DexIgnore
    public void a(File[] fileArr) {
        HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            z24.a().a("Found invalid session part file: " + file);
            hashSet.add(a(file));
        }
        if (!hashSet.isEmpty()) {
            File[] a2 = a(new f(this, hashSet));
            for (File file2 : a2) {
                z24.a().a("Deleting invalid session file: " + file2);
                file2.delete();
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        z24 a2 = z24.a();
        a2.a("Finalizing native report for session " + str);
        b34 b2 = this.o.b(str);
        File e2 = b2.e();
        if (e2 == null || !e2.exists()) {
            z24 a3 = z24.a();
            a3.d("No minidump data found for session " + str);
            return;
        }
        long lastModified = e2.lastModified();
        x44 x44 = new x44(this.a, this.k, str);
        File file = new File(i(), str);
        if (!file.mkdirs()) {
            z24.a().a("Couldn't create native sessions directory");
            return;
        }
        b(lastModified);
        List<n44> a4 = a(b2, str, e(), h(), x44.b());
        o44.a(file, a4);
        this.s.a(i(str), a4);
        x44.a();
    }

    @DexIgnore
    public final void a(Thread thread, Throwable th, long j2) {
        y64 y64;
        z64 z64 = null;
        try {
            String f2 = f();
            if (f2 == null) {
                z24.a().b("Tried to write a fatal exception while no session was open.");
                t34.a((Flushable) null, "Failed to flush to session begin file.");
                t34.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            y64 = new y64(h(), f2 + "SessionCrash");
            try {
                z64 = z64.a(y64);
                a(z64, thread, th, j2, CrashDumperPlugin.NAME, true);
            } catch (Exception e2) {
                e = e2;
            }
            t34.a(z64, "Failed to flush to session begin file.");
            t34.a((Closeable) y64, "Failed to close fatal exception file output stream.");
        } catch (Exception e3) {
            e = e3;
            y64 = null;
            try {
                z24.a().b("An error occurred in the fatal exception logger", e);
                t34.a(z64, "Failed to flush to session begin file.");
                t34.a((Closeable) y64, "Failed to close fatal exception file output stream.");
            } catch (Throwable th2) {
                th = th2;
                t34.a(z64, "Failed to flush to session begin file.");
                t34.a((Closeable) y64, "Failed to close fatal exception file output stream.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            y64 = null;
            t34.a(z64, "Failed to flush to session begin file.");
            t34.a((Closeable) y64, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, x xVar) throws Exception {
        y64 y64;
        z64 z64 = null;
        try {
            y64 = new y64(h(), str + str2);
            try {
                z64 = z64.a(y64);
                xVar.a(z64);
                t34.a(z64, "Failed to flush to session " + str2 + " file.");
                t34.a((Closeable) y64, "Failed to close session " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                t34.a(z64, "Failed to flush to session " + str2 + " file.");
                t34.a((Closeable) y64, "Failed to close session " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            y64 = null;
            t34.a(z64, "Failed to flush to session " + str2 + " file.");
            t34.a((Closeable) y64, "Failed to close session " + str2 + " file.");
            throw th;
        }
    }

    @DexIgnore
    public static void a(File file, x xVar) throws Exception {
        FileOutputStream fileOutputStream;
        z64 z64 = null;
        try {
            fileOutputStream = new FileOutputStream(file, true);
            try {
                z64 = z64.a(fileOutputStream);
                xVar.a(z64);
                t34.a(z64, "Failed to flush to append to " + file.getPath());
                t34.a((Closeable) fileOutputStream, "Failed to close " + file.getPath());
            } catch (Throwable th) {
                th = th;
                t34.a(z64, "Failed to flush to append to " + file.getPath());
                t34.a((Closeable) fileOutputStream, "Failed to close " + file.getPath());
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            t34.a(z64, "Failed to flush to append to " + file.getPath());
            t34.a((Closeable) fileOutputStream, "Failed to close " + file.getPath());
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str, long j2) throws Exception {
        String format = String.format(Locale.US, "Crashlytics Android SDK/%s", y34.e());
        a(str, "BeginSession", new g(this, str, format, j2));
        this.o.a(str, format, j2);
    }

    @DexIgnore
    public final void a(z64 z64, Thread thread, Throwable th, long j2, String str, boolean z2) throws Exception {
        Thread[] threadArr;
        TreeMap treeMap;
        Map<String, String> map;
        n84 n84 = new n84(th, this.p);
        Context e2 = e();
        q34 a2 = q34.a(e2);
        Float a3 = a2.a();
        int b2 = a2.b();
        boolean f2 = t34.f(e2);
        int i2 = e2.getResources().getConfiguration().orientation;
        long b3 = t34.b() - t34.a(e2);
        long a4 = t34.a(Environment.getDataDirectory().getPath());
        ActivityManager.RunningAppProcessInfo a5 = t34.a(e2.getPackageName(), e2);
        LinkedList linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = n84.c;
        String str2 = this.i.b;
        String b4 = this.g.b();
        int i3 = 0;
        if (z2) {
            Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
            Thread[] threadArr2 = new Thread[allStackTraces.size()];
            for (Map.Entry<Thread, StackTraceElement[]> entry : allStackTraces.entrySet()) {
                threadArr2[i3] = entry.getKey();
                linkedList.add(this.p.a(entry.getValue()));
                i3++;
            }
            threadArr = threadArr2;
        } else {
            threadArr = new Thread[0];
        }
        if (!t34.a(e2, "com.crashlytics.CollectCustomKeys", true)) {
            map = new TreeMap<>();
        } else {
            map = this.d.a();
            if (map != null && map.size() > 1) {
                treeMap = new TreeMap(map);
                a74.a(z64, j2, str, n84, thread, stackTraceElementArr, threadArr, linkedList, 8, treeMap, this.l.b(), a5, i2, b4, str2, a3, b2, f2, b3, a4);
                this.l.a();
            }
        }
        treeMap = map;
        a74.a(z64, j2, str, n84, thread, stackTraceElementArr, threadArr, linkedList, 8, treeMap, this.l.b(), a5, i2, b4, str2, a3, b2, f2, b3, a4);
        this.l.a();
    }

    @DexIgnore
    public final void a(File file, String str, int i2) {
        z24 a2 = z24.a();
        a2.a("Collecting session parts for ID " + str);
        File[] a3 = a(new y(str + "SessionCrash"));
        boolean z2 = a3 != null && a3.length > 0;
        z24.a().a(String.format(Locale.US, "Session %s has fatal exception: %s", str, Boolean.valueOf(z2)));
        File[] a4 = a(new y(str + "SessionEvent"));
        boolean z3 = a4 != null && a4.length > 0;
        z24.a().a(String.format(Locale.US, "Session %s has non-fatal exceptions: %s", str, Boolean.valueOf(z3)));
        if (z2 || z3) {
            a(file, str, a(str, a4, i2), z2 ? a3[0] : null);
        } else {
            z24 a5 = z24.a();
            a5.a("No events present for session ID " + str);
        }
        z24 a6 = z24.a();
        a6.a("Removing session part files for ID " + str);
        d(c(str));
    }

    @DexIgnore
    public final void a(File file, String str, File[] fileArr, File file2) {
        y64 y64;
        boolean z2 = file2 != null;
        File g2 = z2 ? g() : j();
        if (!g2.exists()) {
            g2.mkdirs();
        }
        try {
            y64 = new y64(g2, str);
            try {
                z64 a2 = z64.a(y64);
                z24.a().a("Collecting SessionStart data for session ID " + str);
                a(a2, file);
                a2.a(4, v());
                a2.a(5, z2);
                a2.d(11, 1);
                a2.a(12, 3);
                a(a2, str);
                a(a2, fileArr, str);
                if (z2) {
                    a(a2, file2);
                }
                t34.a(a2, "Error flushing session file stream");
                t34.a((Closeable) y64, "Failed to close CLS file");
            } catch (Exception e2) {
                e = e2;
                try {
                    z24.a().b("Failed to write session file for session ID: " + str, e);
                    t34.a((Flushable) null, "Error flushing session file stream");
                    a(y64);
                } catch (Throwable th) {
                    th = th;
                    t34.a((Flushable) null, "Error flushing session file stream");
                    t34.a((Closeable) y64, "Failed to close CLS file");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            y64 = null;
            z24.a().b("Failed to write session file for session ID: " + str, e);
            t34.a((Flushable) null, "Error flushing session file stream");
            a(y64);
        } catch (Throwable th2) {
            th = th2;
            y64 = null;
            t34.a((Flushable) null, "Error flushing session file stream");
            t34.a((Closeable) y64, "Failed to close CLS file");
            throw th;
        }
    }

    @DexIgnore
    public static void a(z64 z64, File[] fileArr, String str) {
        Arrays.sort(fileArr, t34.c);
        for (File file : fileArr) {
            try {
                z24.a().a(String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", str, file.getName()));
                a(z64, file);
            } catch (Exception e2) {
                z24.a().b("Error writting non-fatal to session.", e2);
            }
        }
    }

    @DexIgnore
    public final void a(z64 z64, String str) throws IOException {
        String[] strArr = E;
        for (String str2 : strArr) {
            File[] a2 = a(new y(str + str2 + ".cls"));
            if (a2.length == 0) {
                z24.a().a("Can't find " + str2 + " data for session ID " + str);
            } else {
                z24.a().a("Collecting " + str2 + " data for session ID " + str);
                a(z64, a2[0]);
            }
        }
    }

    @DexIgnore
    public static void a(z64 z64, File file) throws IOException {
        if (!file.exists()) {
            z24 a2 = z24.a();
            a2.b("Tried to include a file that doesn't exist: " + file.getName());
            return;
        }
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                a(fileInputStream2, z64, (int) file.length());
                t34.a((Closeable) fileInputStream2, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                fileInputStream = fileInputStream2;
                t34.a((Closeable) fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            t34.a((Closeable) fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    @DexIgnore
    public static void a(InputStream inputStream, z64 z64, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < i2) {
            int read = inputStream.read(bArr, i3, i2 - i3);
            if (read < 0) {
                break;
            }
            i3 += read;
        }
        z64.a(bArr);
    }

    @DexIgnore
    public final j74 a(String str, String str2) {
        String b2 = t34.b(e(), "com.crashlytics.ApiEndpoint");
        return new i74(new k74(b2, str, this.f, y34.e()), new l74(b2, str2, this.f, y34.e()));
    }

    @DexIgnore
    public final void a(y74 y74, boolean z2) throws Exception {
        Context e2 = e();
        d74 a2 = this.j.a(y74);
        File[] m2 = m();
        for (File file : m2) {
            b(y74.e, file);
            this.e.a(new d0(e2, new h74(file, D), a2, z2));
        }
    }

    @DexIgnore
    public final no3<Void> a(long j2) {
        if (!u()) {
            return qo3.a(new ScheduledThreadPoolExecutor(1), new n(j2));
        }
        z24.a().a("Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
        return qo3.a((Object) null);
    }

    @DexIgnore
    public static List<n44> a(b34 b34, String str, Context context, File file, byte[] bArr) {
        byte[] bArr2;
        m44 m44 = new m44(file);
        File b2 = m44.b(str);
        File a2 = m44.a(str);
        try {
            bArr2 = g64.a(b34.d(), context);
        } catch (Exception unused) {
            bArr2 = null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new r34("logs_file", FileLogWriter.LOG_FOLDER, bArr));
        arrayList.add(new r34("binary_images_file", "binaryImages", bArr2));
        arrayList.add(new i44("crash_meta_file", "metadata", b34.g()));
        arrayList.add(new i44("session_meta_file", Constants.SESSION, b34.f()));
        arrayList.add(new i44("app_meta_file", "app", b34.a()));
        arrayList.add(new i44("device_meta_file", "device", b34.c()));
        arrayList.add(new i44("os_meta_file", "os", b34.b()));
        arrayList.add(new i44("minidump_file", "minidump", b34.e()));
        arrayList.add(new i44("user_meta_file", "user", b2));
        arrayList.add(new i44("keys_file", "keys", a2));
        return arrayList;
    }
}
