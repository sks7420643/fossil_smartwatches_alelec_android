package com.fossil;

import android.util.Log;
import android.util.Pair;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xa4 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Map<Pair<String, String>, no3<ka4>> b; // = new n4();

    @DexIgnore
    public interface a {
        @DexIgnore
        no3<ka4> start();
    }

    @DexIgnore
    public xa4(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public synchronized no3<ka4> a(String str, String str2, a aVar) {
        Pair<String, String> pair = new Pair<>(str, str2);
        no3<ka4> no3 = this.b.get(pair);
        if (no3 != null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(pair);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 29);
                sb.append("Joining ongoing request for: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            return no3;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(pair);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 24);
            sb2.append("Making new request for: ");
            sb2.append(valueOf2);
            Log.d("FirebaseInstanceId", sb2.toString());
        }
        no3<TContinuationResult> b2 = aVar.start().b(this.a, new wa4(this, pair));
        this.b.put(pair, b2);
        return b2;
    }

    @DexIgnore
    public final /* synthetic */ no3 a(Pair pair, no3 no3) throws Exception {
        synchronized (this) {
            this.b.remove(pair);
        }
        return no3;
    }
}
