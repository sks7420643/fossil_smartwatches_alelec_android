package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s74 implements t74 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ d84 b;
    @DexIgnore
    public /* final */ u74 c;
    @DexIgnore
    public /* final */ d44 d;
    @DexIgnore
    public /* final */ p74 e;
    @DexIgnore
    public /* final */ h84 f;
    @DexIgnore
    public /* final */ e44 g;
    @DexIgnore
    public /* final */ AtomicReference<b84> h; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ AtomicReference<oo3<y74>> i; // = new AtomicReference<>(new oo3());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements mo3<Void, Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: a */
        public no3<Void> then(Void r5) throws Exception {
            JSONObject a2 = s74.this.f.a(s74.this.b, true);
            if (a2 != null) {
                c84 a3 = s74.this.c.a(a2);
                s74.this.e.a(a3.d(), a2);
                s74.this.a(a2, "Loaded settings: ");
                s74 s74 = s74.this;
                boolean unused = s74.a(s74.b.f);
                s74.this.h.set(a3);
                ((oo3) s74.this.i.get()).b(a3.c());
                oo3 oo3 = new oo3();
                oo3.b(a3.c());
                s74.this.i.set(oo3);
            }
            return qo3.a((Object) null);
        }
    }

    @DexIgnore
    public s74(Context context, d84 d84, d44 d44, u74 u74, p74 p74, h84 h84, e44 e44) {
        this.a = context;
        this.b = d84;
        this.d = d44;
        this.c = u74;
        this.e = p74;
        this.f = h84;
        this.g = e44;
        this.h.set(q74.a(d44));
    }

    @DexIgnore
    @Override // com.fossil.t74
    public b84 b() {
        return this.h.get();
    }

    @DexIgnore
    public boolean c() {
        return !d().equals(this.b.f);
    }

    @DexIgnore
    public final String d() {
        return t34.h(this.a).getString("existing_instance_identifier", "");
    }

    @DexIgnore
    public static s74 a(Context context, String str, j44 j44, m64 m64, String str2, String str3, String str4, e44 e44) {
        String c2 = j44.c();
        t44 t44 = new t44();
        return new s74(context, new d84(str, j44.d(), j44.e(), j44.f(), j44, t34.a(t34.e(context), str, str3, str2), str3, str2, g44.determineFrom(c2).getId()), t44, new u74(t44), new p74(context), new g84(str4, String.format(Locale.US, "https://firebase-settings.crashlytics.com/spi/v2/platforms/android/gmp/%s/settings", str), m64), e44);
    }

    @DexIgnore
    @Override // com.fossil.t74
    public no3<y74> a() {
        return this.i.get().a();
    }

    @DexIgnore
    public no3<Void> a(Executor executor) {
        return a(r74.USE_CACHE, executor);
    }

    @DexIgnore
    public no3<Void> a(r74 r74, Executor executor) {
        c84 a2;
        if (c() || (a2 = a(r74)) == null) {
            c84 a3 = a(r74.IGNORE_CACHE_EXPIRATION);
            if (a3 != null) {
                this.h.set(a3);
                this.i.get().b(a3.c());
            }
            return this.g.c().a(executor, new a());
        }
        this.h.set(a2);
        this.i.get().b(a2.c());
        return qo3.a((Object) null);
    }

    @DexIgnore
    public final c84 a(r74 r74) {
        c84 c84 = null;
        try {
            if (r74.SKIP_CACHE_LOOKUP.equals(r74)) {
                return null;
            }
            JSONObject b2 = this.e.b();
            if (b2 != null) {
                c84 a2 = this.c.a(b2);
                if (a2 != null) {
                    a(b2, "Loaded cached settings: ");
                    long a3 = this.d.a();
                    if (!r74.IGNORE_CACHE_EXPIRATION.equals(r74)) {
                        if (a2.a(a3)) {
                            z24.a().a("Cached settings have expired.");
                            return null;
                        }
                    }
                    try {
                        z24.a().a("Returning cached settings.");
                        return a2;
                    } catch (Exception e2) {
                        e = e2;
                        c84 = a2;
                        z24.a().b("Failed to get cached settings", e);
                        return c84;
                    }
                } else {
                    z24.a().b("Failed to parse cached settings data.", null);
                    return null;
                }
            } else {
                z24.a().a("No cached settings data found.");
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            z24.a().b("Failed to get cached settings", e);
            return c84;
        }
    }

    @DexIgnore
    public final void a(JSONObject jSONObject, String str) throws JSONException {
        z24 a2 = z24.a();
        a2.a(str + jSONObject.toString());
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final boolean a(String str) {
        SharedPreferences.Editor edit = t34.h(this.a).edit();
        edit.putString("existing_instance_identifier", str);
        edit.apply();
        return true;
    }
}
