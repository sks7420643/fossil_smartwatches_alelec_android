package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn3 implements Parcelable.Creator<in3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ in3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        int i = 0;
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                i2 = j72.q(parcel, a);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                intent = (Intent) j72.a(parcel, a, Intent.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new in3(i, i2, intent);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ in3[] newArray(int i) {
        return new in3[i];
    }
}
