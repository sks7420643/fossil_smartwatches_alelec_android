package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n36 extends dl4<m36> {
    @DexIgnore
    void H(String str);

    @DexIgnore
    void R(String str);

    @DexIgnore
    void a(String str, String str2, String str3);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, String str3);

    @DexIgnore
    void b(WatchApp watchApp);

    @DexIgnore
    void c(String str);

    @DexIgnore
    void d(String str);

    @DexIgnore
    void f(String str);

    @DexIgnore
    void k(boolean z);

    @DexIgnore
    void v(List<WatchApp> list);
}
