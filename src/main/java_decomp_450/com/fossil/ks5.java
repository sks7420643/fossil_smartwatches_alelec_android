package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks5 implements Factory<d06> {
    @DexIgnore
    public static d06 a(gs5 gs5) {
        d06 d = gs5.d();
        c87.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
