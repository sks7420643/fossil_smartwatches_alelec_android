package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt2 extends os2<Object> {
    @DexIgnore
    public /* final */ transient Object[] c;
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ transient int e;

    @DexIgnore
    public pt2(Object[] objArr, int i, int i2) {
        this.c = objArr;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    @Override // java.util.List
    public final Object get(int i) {
        or2.a(i, this.e);
        return this.c[(i * 2) + this.d];
    }

    @DexIgnore
    public final int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return true;
    }
}
