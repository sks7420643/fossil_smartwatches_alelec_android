package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.am4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Ringtone;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p16 extends go5 implements o16, am4.b {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<aw4> f;
    @DexIgnore
    public am4 g;
    @DexIgnore
    public n16 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return p16.j;
        }

        @DexIgnore
        public final p16 b() {
            return new p16();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p16 a;

        @DexIgnore
        public b(p16 p16) {
            this.a = p16;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    /*
    static {
        String simpleName = p16.class.getSimpleName();
        ee7.a((Object) simpleName, "SearchRingPhoneFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return false;
        }
        n16 n16 = this.h;
        if (n16 != null) {
            n16.i();
            Intent intent = new Intent();
            n16 n162 = this.h;
            if (n162 != null) {
                intent.putExtra("KEY_SELECTED_RINGPHONE", n162.h());
                activity.setResult(-1, intent);
                activity.finish();
                return false;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<aw4> qw6 = new qw6<>(this, (aw4) qb.a(layoutInflater, 2131558434, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            aw4 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        n16 n16 = this.h;
        if (n16 != null) {
            n16.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        n16 n16 = this.h;
        if (n16 != null) {
            n16.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<aw4> qw6 = this.f;
        if (qw6 != null) {
            aw4 a2 = qw6.a();
            if (a2 != null) {
                this.g = new am4(this);
                RecyclerView recyclerView = a2.s;
                ee7.a((Object) recyclerView, "it.rvRingphones");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.s;
                ee7.a((Object) recyclerView2, "it.rvRingphones");
                am4 am4 = this.g;
                if (am4 != null) {
                    recyclerView2.setAdapter(am4);
                    a2.q.setOnClickListener(new b(this));
                    return;
                }
                ee7.d("mSearchRingPhoneAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.o16
    public void p(List<Ringtone> list) {
        ee7.b(list, "data");
        am4 am4 = this.g;
        if (am4 != null) {
            n16 n16 = this.h;
            if (n16 != null) {
                am4.a(list, n16.h());
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            ee7.d("mSearchRingPhoneAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.am4.b
    public void a(Ringtone ringtone) {
        ee7.b(ringtone, Constants.RINGTONE);
        n16 n16 = this.h;
        if (n16 != null) {
            n16.a(ringtone);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(n16 n16) {
        ee7.b(n16, "presenter");
        this.h = n16;
    }
}
