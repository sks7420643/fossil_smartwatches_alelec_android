package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.FitnessData;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dn0 extends zk0 {
    @DexIgnore
    public /* final */ boolean C; // = true;
    @DexIgnore
    public FitnessData[] D; // = new FitnessData[0];
    @DexIgnore
    public byte[][] E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public float I; // = 0.5f;
    @DexIgnore
    public float J; // = 0.5f;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ int N;
    @DexIgnore
    public /* final */ ArrayList<ul0> O;
    @DexIgnore
    public /* final */ c80 P;
    @DexIgnore
    public /* final */ HashMap<xf0, Object> Q;

    @DexIgnore
    public dn0(ri1 ri1, en0 en0, c80 c80, HashMap<xf0, Object> hashMap) {
        super(ri1, en0, wm0.m0, null, false, 24);
        this.P = c80;
        this.Q = hashMap;
        Boolean bool = (Boolean) hashMap.get(xf0.SKIP_LIST);
        this.K = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) this.Q.get(xf0.SKIP_ERASE);
        this.L = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) this.Q.get(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.M = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) this.Q.get(xf0.NUMBER_OF_FILE_REQUIRED);
        this.N = num != null ? num.intValue() : 0;
        this.O = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.FILE_CONFIG, ul0.TRANSFER_DATA}));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.O;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        if (yp0.f.b(((zk0) this).x.a())) {
            this.I = 1.0f;
            this.J = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            n();
            return;
        }
        zk0.a(this, new jp0(gq0.b.a(((zk0) this).w.u, pb1.ACTIVITY_FILE), ((zk0) this).w, 0, 4), new yd1(this), new sf1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        JSONObject put = super.i().put(o80.BIOMETRIC_PROFILE.b(), this.P.d()).put(yz0.a(xf0.SKIP_LIST), this.K).put(yz0.a(xf0.SKIP_ERASE), this.L).put(yz0.a(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.M).put(yz0.a(xf0.NUMBER_OF_FILE_REQUIRED), this.N);
        ee7.a((Object) put, "super.optionDescription(\u2026me, numberOfFileRequired)");
        return put;
    }

    @DexIgnore
    public final void m() {
        zk0.a(this, new jp0(gq0.b.a(((zk0) this).w.u, pb1.HARDWARE_LOG), ((zk0) this).w, 0, 4), new oh1(this), new mj1(this), (kd7) null, new jl1(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    public final void n() {
        zk0.a(this, new b31(((zk0) this).w, ((zk0) this).x, this.P, this.Q, ((zk0) this).z), new mh0(this), new jj0(this), new hl0(this), (gd7) null, (gd7) null, 48, (Object) null);
    }
}
