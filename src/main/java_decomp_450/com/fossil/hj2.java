package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj2 extends th2 {
    @DexIgnore
    public /* final */ /* synthetic */ DataSet s;
    @DexIgnore
    public /* final */ /* synthetic */ boolean t; // = false;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hj2(ij2 ij2, a12 a12, DataSet dataSet, boolean z) {
        super(a12);
        this.s = dataSet;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.v02$b] */
    @Override // com.fossil.r12
    public final /* synthetic */ void a(mh2 mh2) throws RemoteException {
        ((si2) mh2.A()).a(new od2(this.s, (zi2) new nj2(this), this.t));
    }
}
