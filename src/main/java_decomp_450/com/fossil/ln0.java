package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ln0 extends Enum<ln0> {
    @DexIgnore
    public static /* final */ ln0 b;
    @DexIgnore
    public static /* final */ ln0 c;
    @DexIgnore
    public static /* final */ ln0 d;
    @DexIgnore
    public static /* final */ ln0 e;
    @DexIgnore
    public static /* final */ ln0 f;
    @DexIgnore
    public static /* final */ ln0 g;
    @DexIgnore
    public static /* final */ ln0 h;
    @DexIgnore
    public static /* final */ ln0 i;
    @DexIgnore
    public static /* final */ ln0 j;
    @DexIgnore
    public static /* final */ ln0 k;
    @DexIgnore
    public static /* final */ /* synthetic */ ln0[] l;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        ln0 ln0 = new ln0("GET_FILE", 0, (byte) 1);
        b = ln0;
        ln0 ln02 = new ln0("LIST_FILE", 1, (byte) 2);
        c = ln02;
        ln0 ln03 = new ln0("PUT_FILE", 2, (byte) 3);
        d = ln03;
        ln0 ln04 = new ln0("VERIFY_FILE", 3, (byte) 4);
        e = ln04;
        ln0 ln05 = new ln0("GET_SIZE_WRITTEN", 4, (byte) 5);
        f = ln05;
        ln0 ln06 = new ln0("VERIFY_DATA", 5, (byte) 6);
        g = ln06;
        ln0 ln07 = new ln0("EOF_REACH", 7, (byte) 8);
        h = ln07;
        ln0 ln08 = new ln0("ABORT_FILE", 8, (byte) 9);
        i = ln08;
        ln0 ln09 = new ln0("WAITING_REQUEST", 9, (byte) 10);
        j = ln09;
        ln0 ln010 = new ln0("DELETE_FILE", 10, (byte) 11);
        k = ln010;
        l = new ln0[]{ln0, ln02, ln03, ln04, ln05, ln06, new ln0("ERASE_DATA", 6, (byte) 7), ln07, ln08, ln09, ln010};
    }
    */

    @DexIgnore
    public ln0(String str, int i2, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static ln0 valueOf(String str) {
        return (ln0) Enum.valueOf(ln0.class, str);
    }

    @DexIgnore
    public static ln0[] values() {
        return (ln0[]) l.clone();
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.a | Byte.MIN_VALUE);
    }
}
