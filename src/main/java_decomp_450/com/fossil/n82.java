package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n82 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<n82> CREATOR; // = new m82();
    @DexIgnore
    public Bundle a;
    @DexIgnore
    public k02[] b;
    @DexIgnore
    public int c;

    @DexIgnore
    public n82(Bundle bundle, k02[] k02Arr, int i) {
        this.a = bundle;
        this.b = k02Arr;
        this.c = i;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a, false);
        k72.a(parcel, 2, (Parcelable[]) this.b, i, false);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public n82() {
    }
}
