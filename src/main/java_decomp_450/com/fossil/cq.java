package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cq<TTaskResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(eq<TTaskResult> eqVar) throws Exception;
}
