package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Context g;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle h;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vn2(sn2 sn2, String str, String str2, Context context, Bundle bundle) {
        super(sn2);
        this.i = sn2;
        this.e = str;
        this.f = str2;
        this.g = context;
        this.h = bundle;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0054 A[Catch:{ Exception -> 0x00a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0060 A[Catch:{ Exception -> 0x00a0 }] */
    @Override // com.fossil.sn2.a
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r14 = this;
            r0 = 0
            r1 = 1
            com.fossil.sn2 r2 = r14.i     // Catch:{ Exception -> 0x00a0 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00a0 }
            r3.<init>()     // Catch:{ Exception -> 0x00a0 }
            java.util.List unused = r2.e = r3     // Catch:{ Exception -> 0x00a0 }
            com.fossil.sn2 r2 = r14.i     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r3 = r14.e     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r4 = r14.f     // Catch:{ Exception -> 0x00a0 }
            boolean r2 = com.fossil.sn2.c(r3, r4)     // Catch:{ Exception -> 0x00a0 }
            r3 = 0
            if (r2 == 0) goto L_0x0027
            java.lang.String r3 = r14.f     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r2 = r14.e     // Catch:{ Exception -> 0x00a0 }
            com.fossil.sn2 r4 = r14.i     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r4 = r4.a     // Catch:{ Exception -> 0x00a0 }
            r10 = r2
            r11 = r3
            r9 = r4
            goto L_0x002a
        L_0x0027:
            r9 = r3
            r10 = r9
            r11 = r10
        L_0x002a:
            android.content.Context r2 = r14.g     // Catch:{ Exception -> 0x00a0 }
            com.fossil.sn2.h(r2)     // Catch:{ Exception -> 0x00a0 }
            java.lang.Boolean r2 = com.fossil.sn2.j     // Catch:{ Exception -> 0x00a0 }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x00a0 }
            if (r2 != 0) goto L_0x003e
            if (r10 == 0) goto L_0x003c
            goto L_0x003e
        L_0x003c:
            r2 = 0
            goto L_0x003f
        L_0x003e:
            r2 = 1
        L_0x003f:
            com.fossil.sn2 r3 = r14.i     // Catch:{ Exception -> 0x00a0 }
            com.fossil.sn2 r4 = r14.i     // Catch:{ Exception -> 0x00a0 }
            android.content.Context r5 = r14.g     // Catch:{ Exception -> 0x00a0 }
            com.fossil.q43 r4 = r4.a(r5, r2)     // Catch:{ Exception -> 0x00a0 }
            com.fossil.q43 unused = r3.h = r4     // Catch:{ Exception -> 0x00a0 }
            com.fossil.sn2 r3 = r14.i     // Catch:{ Exception -> 0x00a0 }
            com.fossil.q43 r3 = r3.h     // Catch:{ Exception -> 0x00a0 }
            if (r3 != 0) goto L_0x0060
            com.fossil.sn2 r2 = r14.i     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r2 = r2.a     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r3 = "Failed to connect to measurement client."
            android.util.Log.w(r2, r3)     // Catch:{ Exception -> 0x00a0 }
            return
        L_0x0060:
            android.content.Context r3 = r14.g     // Catch:{ Exception -> 0x00a0 }
            int r3 = com.fossil.sn2.g(r3)     // Catch:{ Exception -> 0x00a0 }
            android.content.Context r4 = r14.g     // Catch:{ Exception -> 0x00a0 }
            int r4 = com.fossil.sn2.f(r4)     // Catch:{ Exception -> 0x00a0 }
            if (r2 == 0) goto L_0x0079
            int r2 = java.lang.Math.max(r3, r4)     // Catch:{ Exception -> 0x00a0 }
            if (r4 >= r3) goto L_0x0076
            r3 = 1
            goto L_0x0077
        L_0x0076:
            r3 = 0
        L_0x0077:
            r8 = r3
            goto L_0x0083
        L_0x0079:
            if (r3 <= 0) goto L_0x007c
            r4 = r3
        L_0x007c:
            if (r3 <= 0) goto L_0x0080
            r2 = 1
            goto L_0x0081
        L_0x0080:
            r2 = 0
        L_0x0081:
            r8 = r2
            r2 = r4
        L_0x0083:
            com.fossil.qn2 r13 = new com.fossil.qn2     // Catch:{ Exception -> 0x00a0 }
            r4 = 31000(0x7918, double:1.5316E-319)
            long r6 = (long) r2     // Catch:{ Exception -> 0x00a0 }
            android.os.Bundle r12 = r14.h     // Catch:{ Exception -> 0x00a0 }
            r3 = r13
            r3.<init>(r4, r6, r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x00a0 }
            com.fossil.sn2 r2 = r14.i     // Catch:{ Exception -> 0x00a0 }
            com.fossil.q43 r2 = r2.h     // Catch:{ Exception -> 0x00a0 }
            android.content.Context r3 = r14.g     // Catch:{ Exception -> 0x00a0 }
            com.fossil.ab2 r3 = com.fossil.cb2.a(r3)     // Catch:{ Exception -> 0x00a0 }
            long r4 = r14.a     // Catch:{ Exception -> 0x00a0 }
            r2.initialize(r3, r13, r4)     // Catch:{ Exception -> 0x00a0 }
            return
        L_0x00a0:
            r2 = move-exception
            com.fossil.sn2 r3 = r14.i
            r3.a(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vn2.a():void");
    }
}
