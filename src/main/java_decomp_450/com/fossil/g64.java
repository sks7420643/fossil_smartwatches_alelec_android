package com.fossil;

import android.content.Context;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g64 {
    @DexIgnore
    public static byte[] a(File file, Context context) throws IOException {
        if (file == null || !file.exists()) {
            return new byte[0];
        }
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file));
            try {
                byte[] a = new f64(context, new j64()).a(bufferedReader2);
                t34.a(bufferedReader2);
                return a;
            } catch (Throwable th) {
                th = th;
                bufferedReader = bufferedReader2;
                t34.a(bufferedReader);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            t34.a(bufferedReader);
            throw th;
        }
    }
}
