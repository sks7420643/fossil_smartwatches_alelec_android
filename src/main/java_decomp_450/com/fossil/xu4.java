package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<HybridPresetAppSetting>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<HybridPresetAppSetting>> {
    }

    @DexIgnore
    public final String a(ArrayList<HybridPresetAppSetting> arrayList) {
        ee7.b(arrayList, "configurationList");
        if (arrayList.isEmpty()) {
            return "";
        }
        return new Gson().a(arrayList, new a().getType());
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> a(String str) {
        ee7.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object a2 = new Gson().a(str, new b().getType());
        ee7.a(a2, "Gson().fromJson(data, type)");
        return (ArrayList) a2;
    }
}
