package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iw3<T> extends aw3<Iterable<T>> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ aw3<? super T> elementEquivalence;

    @DexIgnore
    public iw3(aw3<? super T> aw3) {
        jw3.a(aw3);
        this.elementEquivalence = aw3;
    }

    @DexIgnore
    @Override // com.fossil.aw3
    public /* bridge */ /* synthetic */ boolean doEquivalent(Object obj, Object obj2) {
        return doEquivalent((Iterable) ((Iterable) obj), (Iterable) ((Iterable) obj2));
    }

    @DexIgnore
    @Override // com.fossil.aw3
    public /* bridge */ /* synthetic */ int doHash(Object obj) {
        return doHash((Iterable) ((Iterable) obj));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof iw3) {
            return this.elementEquivalence.equals(((iw3) obj).elementEquivalence);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.elementEquivalence.hashCode() ^ 1185147655;
    }

    @DexIgnore
    public String toString() {
        return this.elementEquivalence + ".pairwise()";
    }

    @DexIgnore
    public boolean doEquivalent(Iterable<T> iterable, Iterable<T> iterable2) {
        Iterator<T> it = iterable.iterator();
        Iterator<T> it2 = iterable2.iterator();
        while (it.hasNext() && it2.hasNext()) {
            if (!this.elementEquivalence.equivalent(it.next(), it2.next())) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int doHash(Iterable<T> iterable) {
        int i = 78721;
        for (T t : iterable) {
            i = (i * 24943) + this.elementEquivalence.hash(t);
        }
        return i;
    }
}
