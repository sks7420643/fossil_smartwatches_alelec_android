package com.fossil;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import com.facebook.internal.Utility;
import com.fossil.i17;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c17 extends i17 {
    @DexIgnore
    public /* final */ Downloader a;
    @DexIgnore
    public /* final */ k17 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends IOException {
        @DexIgnore
        public a(String str) {
            super(str);
        }
    }

    @DexIgnore
    public c17(Downloader downloader, k17 k17) {
        this.a = downloader;
        this.b = k17;
    }

    @DexIgnore
    @Override // com.fossil.i17
    public int a() {
        return 2;
    }

    @DexIgnore
    @Override // com.fossil.i17
    public boolean a(g17 g17) {
        String scheme = g17.d.getScheme();
        return "http".equals(scheme) || Utility.URL_SCHEME.equals(scheme);
    }

    @DexIgnore
    @Override // com.fossil.i17
    public boolean b() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.i17
    public i17.a a(g17 g17, int i) throws IOException {
        Downloader.Response load = this.a.load(g17.d, g17.c);
        if (load == null) {
            return null;
        }
        Picasso.LoadedFrom loadedFrom = load.c ? Picasso.LoadedFrom.DISK : Picasso.LoadedFrom.NETWORK;
        Bitmap a2 = load.a();
        if (a2 != null) {
            return new i17.a(a2, loadedFrom);
        }
        InputStream c = load.c();
        if (c == null) {
            return null;
        }
        if (loadedFrom == Picasso.LoadedFrom.DISK && load.b() == 0) {
            o17.a(c);
            throw new a("Received response with 0 content-length header.");
        }
        if (loadedFrom == Picasso.LoadedFrom.NETWORK && load.b() > 0) {
            this.b.a(load.b());
        }
        return new i17.a(c, loadedFrom);
    }

    @DexIgnore
    @Override // com.fossil.i17
    public boolean a(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }
}
