package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class az0 extends wi1<i97, m60> {
    @DexIgnore
    public static /* final */ y71<i97>[] b; // = new y71[0];
    @DexIgnore
    public static /* final */ uk1<m60>[] c; // = {new qv0(pb1.DEVICE_INFO), new ix0(pb1.DEVICE_INFO)};
    @DexIgnore
    public static /* final */ az0 d; // = new az0();

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<m60>[] b() {
        return c;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<i97>[] a() {
        return b;
    }

    @DexIgnore
    public final m60 b(byte[] bArr) {
        return m60.t.a(s97.a(bArr, 12, bArr.length - 4));
    }
}
