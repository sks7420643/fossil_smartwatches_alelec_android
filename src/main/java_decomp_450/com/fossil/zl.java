package com.fossil;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ tm c;
    @DexIgnore
    public /* final */ hm d;
    @DexIgnore
    public /* final */ om e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Executor a;
        @DexIgnore
        public tm b;
        @DexIgnore
        public hm c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public om e;
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = Integer.MAX_VALUE;
        @DexIgnore
        public int i; // = 20;

        @DexIgnore
        public a a(tm tmVar) {
            this.b = tmVar;
            return this;
        }

        @DexIgnore
        public zl a() {
            return new zl(this);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        zl a();
    }

    @DexIgnore
    public zl(a aVar) {
        Executor executor = aVar.a;
        if (executor == null) {
            this.a = a();
        } else {
            this.a = executor;
        }
        Executor executor2 = aVar.d;
        if (executor2 == null) {
            this.b = a();
        } else {
            this.b = executor2;
        }
        tm tmVar = aVar.b;
        if (tmVar == null) {
            this.c = tm.a();
        } else {
            this.c = tmVar;
        }
        hm hmVar = aVar.c;
        if (hmVar == null) {
            this.d = hm.a();
        } else {
            this.d = hmVar;
        }
        om omVar = aVar.e;
        if (omVar == null) {
            this.e = new um();
        } else {
            this.e = omVar;
        }
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
    }

    @DexIgnore
    public final Executor a() {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    @DexIgnore
    public Executor b() {
        return this.a;
    }

    @DexIgnore
    public hm c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.h;
    }

    @DexIgnore
    public int e() {
        if (Build.VERSION.SDK_INT == 23) {
            return this.i / 2;
        }
        return this.i;
    }

    @DexIgnore
    public int f() {
        return this.g;
    }

    @DexIgnore
    public int g() {
        return this.f;
    }

    @DexIgnore
    public om h() {
        return this.e;
    }

    @DexIgnore
    public Executor i() {
        return this.b;
    }

    @DexIgnore
    public tm j() {
        return this.c;
    }
}
