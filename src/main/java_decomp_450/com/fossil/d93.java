package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<d93> CREATOR; // = new y93();
    @DexIgnore
    public LatLng a; // = null;
    @DexIgnore
    public double b; // = 0.0d;
    @DexIgnore
    public float c; // = 10.0f;
    @DexIgnore
    public int d; // = -16777216;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public float f; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public List<l93> i; // = null;

    @DexIgnore
    public d93() {
    }

    @DexIgnore
    public final boolean A() {
        return this.h;
    }

    @DexIgnore
    public final boolean B() {
        return this.g;
    }

    @DexIgnore
    public final d93 a(LatLng latLng) {
        this.a = latLng;
        return this;
    }

    @DexIgnore
    public final d93 b(int i2) {
        this.d = i2;
        return this;
    }

    @DexIgnore
    public final LatLng e() {
        return this.a;
    }

    @DexIgnore
    public final int g() {
        return this.e;
    }

    @DexIgnore
    public final double v() {
        return this.b;
    }

    @DexIgnore
    public final int w() {
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, (Parcelable) e(), i2, false);
        k72.a(parcel, 3, v());
        k72.a(parcel, 4, y());
        k72.a(parcel, 5, w());
        k72.a(parcel, 6, g());
        k72.a(parcel, 7, z());
        k72.a(parcel, 8, B());
        k72.a(parcel, 9, A());
        k72.c(parcel, 10, x(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final List<l93> x() {
        return this.i;
    }

    @DexIgnore
    public final float y() {
        return this.c;
    }

    @DexIgnore
    public final float z() {
        return this.f;
    }

    @DexIgnore
    public final d93 a(double d2) {
        this.b = d2;
        return this;
    }

    @DexIgnore
    public final d93 b(float f2) {
        this.f = f2;
        return this;
    }

    @DexIgnore
    public final d93 a(float f2) {
        this.c = f2;
        return this;
    }

    @DexIgnore
    public final d93 b(boolean z) {
        this.g = z;
        return this;
    }

    @DexIgnore
    public final d93 a(int i2) {
        this.e = i2;
        return this;
    }

    @DexIgnore
    public final d93 a(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public d93(LatLng latLng, double d2, float f2, int i2, int i3, float f3, boolean z, boolean z2, List<l93> list) {
        this.a = latLng;
        this.b = d2;
        this.c = f2;
        this.d = i2;
        this.e = i3;
        this.f = f3;
        this.g = z;
        this.h = z2;
        this.i = list;
    }
}
