package com.fossil;

import com.fossil.ap2;
import com.fossil.bw2;
import com.fossil.dp2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo2 extends bw2<zo2, a> implements lx2 {
    @DexIgnore
    public static /* final */ zo2 zzi;
    @DexIgnore
    public static volatile wx2<zo2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public jw2<dp2> zze; // = bw2.o();
    @DexIgnore
    public jw2<ap2> zzf; // = bw2.o();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<zo2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(zo2.zzi);
        }

        @DexIgnore
        public final dp2 a(int i) {
            return ((zo2) ((bw2.a) this).b).b(i);
        }

        @DexIgnore
        public final ap2 b(int i) {
            return ((zo2) ((bw2.a) this).b).c(i);
        }

        @DexIgnore
        public final int o() {
            return ((zo2) ((bw2.a) this).b).t();
        }

        @DexIgnore
        public final int zza() {
            return ((zo2) ((bw2.a) this).b).r();
        }

        @DexIgnore
        public /* synthetic */ a(yo2 yo2) {
            this();
        }

        @DexIgnore
        public final a a(int i, dp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zo2) ((bw2.a) this).b).a(i, (dp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a a(int i, ap2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zo2) ((bw2.a) this).b).a(i, (ap2) ((bw2) aVar.g()));
            return this;
        }
    }

    /*
    static {
        zo2 zo2 = new zo2();
        zzi = zo2;
        bw2.a(zo2.class, zo2);
    }
    */

    @DexIgnore
    public final void a(int i, dp2 dp2) {
        dp2.getClass();
        jw2<dp2> jw2 = this.zze;
        if (!jw2.zza()) {
            this.zze = bw2.a(jw2);
        }
        this.zze.set(i, dp2);
    }

    @DexIgnore
    public final dp2 b(int i) {
        return this.zze.get(i);
    }

    @DexIgnore
    public final ap2 c(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final int p() {
        return this.zzd;
    }

    @DexIgnore
    public final List<dp2> q() {
        return this.zze;
    }

    @DexIgnore
    public final int r() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<ap2> s() {
        return this.zzf;
    }

    @DexIgnore
    public final int t() {
        return this.zzf.size();
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final void a(int i, ap2 ap2) {
        ap2.getClass();
        jw2<ap2> jw2 = this.zzf;
        if (!jw2.zza()) {
            this.zzf = bw2.a(jw2);
        }
        this.zzf.set(i, ap2);
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (yo2.a[i - 1]) {
            case 1:
                return new zo2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001\u1004\u0000\u0002\u001b\u0003\u001b\u0004\u1007\u0001\u0005\u1007\u0002", new Object[]{"zzc", "zzd", "zze", dp2.class, "zzf", ap2.class, "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                wx2<zo2> wx2 = zzj;
                if (wx2 == null) {
                    synchronized (zo2.class) {
                        wx2 = zzj;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzi);
                            zzj = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
