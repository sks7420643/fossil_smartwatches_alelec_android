package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p13 implements m13 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.service.database_return_empty_collection", true);

    @DexIgnore
    @Override // com.fossil.m13
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m13
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
