package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ab0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ WorkoutType b;
    @DexIgnore
    public /* final */ WorkoutState c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ short i;
    @DexIgnore
    public /* final */ short j;
    @DexIgnore
    public /* final */ short k;
    @DexIgnore
    public /* final */ boolean l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ab0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ab0 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                WorkoutType valueOf = WorkoutType.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    return new ab0(readLong, valueOf, WorkoutState.valueOf(readString2), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), (short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt(), parcel.readInt() == 1);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ab0[] newArray(int i) {
            return new ab0[i];
        }
    }

    @DexIgnore
    public ab0(long j2, WorkoutType workoutType, WorkoutState workoutState, long j3, long j4, long j5, long j6, long j7, short s, short s2, short s3, boolean z) {
        this.a = j2;
        this.b = workoutType;
        this.c = workoutState;
        this.d = j3;
        this.e = j4;
        this.f = j5;
        this.g = j6;
        this.h = j7;
        this.i = s;
        this.j = s2;
        this.k = s3;
        this.l = z;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put(Constants.SESSION_ID, this.a).put("type", yz0.a(this.b)).put("state", yz0.a(this.c)).put("duration_in_second", this.d).put("number_of_step", this.e).put("distance_in_meter", this.f).put("active_calorie", this.g).put("total_calorie", this.h).put("current_heart_rate", Short.valueOf(this.i)).put("average_heart_rate", Short.valueOf(this.j)).put("maximum_heart_rate", Short.valueOf(this.k)).put("gps", this.l);
        ee7.a((Object) put, "JSONObject()\n           \u2026put(\"gps\", isRequiredGPS)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ab0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ab0 ab0 = (ab0) obj;
            return this.a == ab0.a && this.b == ab0.b && this.c == ab0.c && this.d == ab0.d && this.e == ab0.e && this.f == ab0.f && this.g == ab0.g && this.h == ab0.h && this.i == ab0.i && this.j == ab0.j && this.k == ab0.k && this.l == ab0.l;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutSession");
    }

    @DexIgnore
    public final long getActiveCaloriesInCalorie() {
        return this.g;
    }

    @DexIgnore
    public final short getAverageHeartRate() {
        return this.j;
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.i;
    }

    @DexIgnore
    public final long getDistanceInMeter() {
        return this.f;
    }

    @DexIgnore
    public final long getDurationInSecond() {
        return this.d;
    }

    @DexIgnore
    public final short getMaximumHeartRate() {
        return this.k;
    }

    @DexIgnore
    public final long getNumberOfStep() {
        return this.e;
    }

    @DexIgnore
    public final long getSessionId() {
        return this.a;
    }

    @DexIgnore
    public final long getTotalCaloriesInCalorie() {
        return this.h;
    }

    @DexIgnore
    public final WorkoutState getWorkoutState() {
        return this.c;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        int hashCode3 = Long.valueOf(this.d).hashCode();
        int hashCode4 = Long.valueOf(this.e).hashCode();
        int hashCode5 = Long.valueOf(this.f).hashCode();
        int hashCode6 = Long.valueOf(this.g).hashCode();
        int hashCode7 = Long.valueOf(this.h).hashCode();
        return Boolean.valueOf(this.l).hashCode() + ((((((((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (Long.valueOf(this.a).hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + this.i) * 31) + this.j) * 31) + this.k) * 31);
    }

    @DexIgnore
    public final boolean isRequiredGPS() {
        return this.l;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.i));
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.j));
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.k));
        }
        if (parcel != null) {
            parcel.writeInt(this.l ? 1 : 0);
        }
    }
}
