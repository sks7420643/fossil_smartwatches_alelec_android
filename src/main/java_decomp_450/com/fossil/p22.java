package com.fossil;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.fossil.v02;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p22 implements m32 {
    @DexIgnore
    public /* final */ l32 a;
    @DexIgnore
    public boolean b; // = false;

    @DexIgnore
    public p22(l32 l32) {
        this.a = l32;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(T t) {
        try {
            this.a.s.y.a(t);
            c32 c32 = this.a.s;
            v02.f fVar = c32.p.get(t.i());
            a72.a(fVar, "Appropriate Api was not requested.");
            if (fVar.c() || !this.a.g.containsKey(t.i())) {
                boolean z = fVar instanceof f72;
                v02.h hVar = fVar;
                if (z) {
                    hVar = ((f72) fVar).I();
                }
                t.b(hVar);
                return t;
            }
            t.c(new Status(17));
            return t;
        } catch (DeadObjectException unused) {
            this.a.a(new o22(this, this));
        }
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void a(i02 i02, v02<?> v02, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t) {
        a(t);
        return t;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void b(Bundle bundle) {
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void c() {
    }

    @DexIgnore
    public final void d() {
        if (this.b) {
            this.b = false;
            this.a.s.y.a();
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void b() {
        if (this.b) {
            this.b = false;
            this.a.a(new r22(this, this));
        }
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final boolean a() {
        if (this.b) {
            return false;
        }
        if (this.a.s.p()) {
            this.b = true;
            for (m42 m42 : this.a.s.x) {
                m42.a();
            }
            return false;
        }
        this.a.a((i02) null);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void a(int i) {
        this.a.a((i02) null);
        this.a.t.a(i, this.b);
    }
}
