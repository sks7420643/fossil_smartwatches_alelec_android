package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr0 extends nw0 {
    @DexIgnore
    public boolean M;

    @DexIgnore
    public fr0(short s, ri1 ri1) {
        super(s, qa1.m, ri1, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(pm1 pm1) {
        if (pm1.a != dd1.DISCONNECTED) {
            a(sz0.a(((v81) this).v, null, null, ay0.t, null, null, 27));
        } else if (pm1.b == 19 || this.M) {
            a(((v81) this).v);
        } else {
            a(sz0.a(((v81) this).v, null, null, ay0.f, null, null, 27));
        }
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        if (((v81) this).v.c != ay0.a) {
            ((uh1) this).E = true;
        } else {
            this.M = true;
        }
        return new JSONObject();
    }
}
