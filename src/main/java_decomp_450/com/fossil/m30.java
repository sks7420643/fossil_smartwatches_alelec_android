package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.fossil.k30;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m30 implements k30 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ k30.a b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ BroadcastReceiver e; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            m30 m30 = m30.this;
            boolean z = m30.c;
            m30.c = m30.a(context);
            if (z != m30.this.c) {
                if (Log.isLoggable("ConnectivityMonitor", 3)) {
                    Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + m30.this.c);
                }
                m30 m302 = m30.this;
                m302.b.a(m302.c);
            }
        }
    }

    @DexIgnore
    public m30(Context context, k30.a aVar) {
        this.a = context.getApplicationContext();
        this.b = aVar;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        u50.a(connectivityManager);
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (RuntimeException e2) {
            if (Log.isLoggable("ConnectivityMonitor", 5)) {
                Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e2);
            }
            return true;
        }
    }

    @DexIgnore
    public final void b() {
        if (!this.d) {
            this.c = a(this.a);
            try {
                this.a.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.d = true;
            } catch (SecurityException e2) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register", e2);
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        if (this.d) {
            this.a.unregisterReceiver(this.e);
            this.d = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStart() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStop() {
        c();
    }
}
