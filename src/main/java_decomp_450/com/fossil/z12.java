package com.fossil;

import android.os.Looper;
import com.fossil.y12;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z12 {
    @DexIgnore
    public /* final */ Set<y12<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public final void a() {
        for (y12<?> y12 : this.a) {
            y12.a();
        }
        this.a.clear();
    }

    @DexIgnore
    public static <L> y12<L> a(L l, Looper looper, String str) {
        a72.a((Object) l, (Object) "Listener must not be null");
        a72.a(looper, "Looper must not be null");
        a72.a((Object) str, (Object) "Listener type must not be null");
        return new y12<>(looper, l, str);
    }

    @DexIgnore
    public static <L> y12.a<L> a(L l, String str) {
        a72.a((Object) l, (Object) "Listener must not be null");
        a72.a((Object) str, (Object) "Listener type must not be null");
        a72.a(str, (Object) "Listener type must not be empty");
        return new y12.a<>(l, str);
    }
}
