package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class un extends vn<Boolean> {
    @DexIgnore
    public un(Context context, vp vpVar) {
        super(ho.a(context, vpVar).b());
    }

    @DexIgnore
    @Override // com.fossil.vn
    public boolean a(zo zoVar) {
        return zoVar.j.f();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
