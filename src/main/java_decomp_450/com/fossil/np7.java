package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np7 extends mo7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ ar7 c;

    @DexIgnore
    public np7(String str, long j, ar7 ar7) {
        this.a = str;
        this.b = j;
        this.c = ar7;
    }

    @DexIgnore
    @Override // com.fossil.mo7
    public long contentLength() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.mo7
    public ho7 contentType() {
        String str = this.a;
        if (str != null) {
            return ho7.b(str);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.mo7
    public ar7 source() {
        return this.c;
    }
}
