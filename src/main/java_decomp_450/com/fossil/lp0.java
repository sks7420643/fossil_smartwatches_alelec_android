package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp0 extends fe7 implements gd7<m60, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice a;
    @DexIgnore
    public /* final */ /* synthetic */ km1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lp0(BluetoothDevice bluetoothDevice, km1 km1) {
        super(1);
        this.a = bluetoothDevice;
        this.b = km1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(m60 m60) {
        sx0 sx0 = sx0.c;
        String serialNumber = m60.getSerialNumber();
        String address = this.a.getAddress();
        ee7.a((Object) address, "bluetoothDevice.address");
        sx0.a(serialNumber, address);
        f60 f60 = f60.k;
        f60.d.remove(this.a.getAddress());
        f60.a(f60.k, this.b);
        return i97.a;
    }
}
