package com.fossil;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h1 {
    @DexIgnore
    public /* final */ ArrayList<ha> a; // = new ArrayList<>();
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public Interpolator c;
    @DexIgnore
    public ia d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ ja f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ja {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a() {
            this.b = 0;
            this.a = false;
            h1.this.b();
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void b(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == h1.this.a.size()) {
                ia iaVar = h1.this.d;
                if (iaVar != null) {
                    iaVar.b(null);
                }
                a();
            }
        }

        @DexIgnore
        @Override // com.fossil.ja, com.fossil.ia
        public void c(View view) {
            if (!this.a) {
                this.a = true;
                ia iaVar = h1.this.d;
                if (iaVar != null) {
                    iaVar.c(null);
                }
            }
        }
    }

    @DexIgnore
    public h1 a(ha haVar) {
        if (!this.e) {
            this.a.add(haVar);
        }
        return this;
    }

    @DexIgnore
    public void b() {
        this.e = false;
    }

    @DexIgnore
    public void c() {
        if (!this.e) {
            Iterator<ha> it = this.a.iterator();
            while (it.hasNext()) {
                ha next = it.next();
                long j = this.b;
                if (j >= 0) {
                    next.a(j);
                }
                Interpolator interpolator = this.c;
                if (interpolator != null) {
                    next.a(interpolator);
                }
                if (this.d != null) {
                    next.a(this.f);
                }
                next.c();
            }
            this.e = true;
        }
    }

    @DexIgnore
    public h1 a(ha haVar, ha haVar2) {
        this.a.add(haVar);
        haVar2.b(haVar.b());
        this.a.add(haVar2);
        return this;
    }

    @DexIgnore
    public void a() {
        if (this.e) {
            Iterator<ha> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            this.e = false;
        }
    }

    @DexIgnore
    public h1 a(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    @DexIgnore
    public h1 a(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    @DexIgnore
    public h1 a(ia iaVar) {
        if (!this.e) {
            this.d = iaVar;
        }
        return this;
    }
}
