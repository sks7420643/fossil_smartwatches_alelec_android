package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface p07 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements p07 {
        @DexIgnore
        @Override // com.fossil.p07
        public int a() {
            return 0;
        }

        @DexIgnore
        @Override // com.fossil.p07
        public Bitmap a(String str) {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.p07
        public void a(String str, Bitmap bitmap) {
        }

        @DexIgnore
        @Override // com.fossil.p07
        public int size() {
            return 0;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    int a();

    @DexIgnore
    Bitmap a(String str);

    @DexIgnore
    void a(String str, Bitmap bitmap);

    @DexIgnore
    int size();
}
