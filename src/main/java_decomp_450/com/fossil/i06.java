package com.fossil;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i06 extends cl4 {
    @DexIgnore
    public abstract void a(a06 a06);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, Parcelable parcelable);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();
}
