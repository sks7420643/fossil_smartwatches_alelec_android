package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tj0 extends rr1 {
    @DexIgnore
    public long M;
    @DexIgnore
    public byte[] N; // = new byte[0];
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public int R; // = -1;
    @DexIgnore
    public float S;
    @DexIgnore
    public qk1 T;
    @DexIgnore
    public long U;
    @DexIgnore
    public /* final */ zq0 V;
    @DexIgnore
    public /* final */ vc7<i97> W;

    @DexIgnore
    public tj0(ln0 ln0, short s, qa1 qa1, ri1 ri1, int i) {
        super(ln0, s, qa1, ri1, i);
        qk1 qk1 = qk1.UNKNOWN;
        this.T = qk1;
        this.V = new zq0(0, qk1, new byte[0], null, 9);
        this.W = new wh0(this, ri1);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final JSONObject a(byte[] bArr) {
        qk1 qk1;
        boolean z = true;
        if (!this.Q) {
            JSONObject jSONObject = new JSONObject();
            if (bArr.length >= 4) {
                long b = yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                this.M = b;
                yz0.a(jSONObject, r51.D0, Long.valueOf(b));
                if (this.M == 0) {
                    a(sz0.a(((v81) this).v, null, null, ay0.a, null, null, 27));
                } else {
                    if (bArr.length >= 5) {
                        qk1 = qk1.p.a(bArr[4]);
                    } else {
                        qk1 = qk1.FTD;
                    }
                    this.T = qk1;
                    yz0.a(jSONObject, r51.E0, qk1.a);
                    this.V.b = this.T;
                    if (((v81) this).s) {
                        mp0.b.a(((v81) this).y.u).b(this.T);
                    }
                }
            } else {
                ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.j, null, null, 27);
            }
            this.Q = true;
            if (((v81) this).v.c == ay0.a) {
                z = false;
            }
            ((uh1) this).E = z;
            byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(ln0.h.a()).putShort(((rr1) this).L).array();
            ee7.a((Object) array, "ByteBuffer.allocate(1 + \u2026                 .array()");
            ((rr1) this).H = array;
            return jSONObject;
        }
        i();
        JSONObject jSONObject2 = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        c(yz0.b(order.getInt(0)));
        b(yz0.b(order.getInt(4)));
        yz0.a(yz0.a(jSONObject2, r51.H, Long.valueOf(s())), r51.I, Long.valueOf(r()));
        u();
        ((uh1) this).E = true;
        return jSONObject2;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.N = bArr;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final boolean c(rk1 rk1) {
        return rk1.a == this.T;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final void f(rk1 rk1) {
        byte[] bArr;
        if (((v81) this).s) {
            bArr = ir0.b.a(((v81) this).y.u, this.T, rk1.b);
        } else {
            bArr = rk1.b;
        }
        boolean z = false;
        if (yz0.b(yz0.b((byte) (bArr[0] & 63))) == (this.R + 1) % 64) {
            if (((byte) (bArr[0] & ((byte) 128))) != ((byte) 0)) {
                z = true;
            }
            if (z) {
                a(5000L);
                a(this.W);
            } else {
                a(((v81) this).p);
            }
            this.R++;
            b(yz0.a(t(), s97.a(bArr, 1, bArr.length)));
            float length = (((float) t().length) * 1.0f) / ((float) this.M);
            if (length - this.S > 0.001f || length == 1.0f) {
                this.S = length;
                a(length);
            }
            if (this.U == 0) {
                a(this.V);
            }
            this.U += (long) bArr.length;
            yz0.a(yz0.a(yz0.a(this.V.d, r51.T0, Integer.valueOf(this.R + 1)), r51.o3, Long.valueOf(this.U)), r51.F0, Integer.valueOf(t().length));
            return;
        }
        ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.g, null, null, 27);
        ((uh1) this).E = true;
    }

    @DexIgnore
    public long r() {
        return this.P;
    }

    @DexIgnore
    public long s() {
        return this.O;
    }

    @DexIgnore
    public byte[] t() {
        return this.N;
    }

    @DexIgnore
    public void u() {
    }

    @DexIgnore
    public void b(long j) {
        this.P = j;
    }

    @DexIgnore
    public void c(long j) {
        this.O = j;
    }
}
