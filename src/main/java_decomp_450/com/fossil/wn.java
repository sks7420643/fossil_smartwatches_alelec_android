package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wn extends vn<qn> {
    @DexIgnore
    public wn(Context context, vp vpVar) {
        super(ho.a(context, vpVar).c());
    }

    @DexIgnore
    @Override // com.fossil.vn
    public boolean a(zo zoVar) {
        return zoVar.j.b() == jm.CONNECTED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(qn qnVar) {
        if (Build.VERSION.SDK_INT < 26) {
            return !qnVar.a();
        }
        if (!qnVar.a() || !qnVar.d()) {
            return true;
        }
        return false;
    }
}
