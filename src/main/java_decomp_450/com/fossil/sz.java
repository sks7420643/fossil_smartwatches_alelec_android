package com.fossil;

import android.content.Context;
import com.fossil.qz;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz extends qz {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements qz.a {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.qz.a
        public File a() {
            File cacheDir = this.a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            return this.b != null ? new File(cacheDir, this.b) : cacheDir;
        }
    }

    @DexIgnore
    public sz(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    @DexIgnore
    public sz(Context context, String str, long j) {
        super(new a(context, str), j);
    }
}
