package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ ch5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return pm5.f;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final FirmwareData a(ch5 ch5, String str) {
            ee7.b(ch5, "sharedPreferencesManager");
            ee7.b(str, "deviceSKU");
            if (mh7.b("release", "release", true) || !ch5.H()) {
                Firmware a = ah5.p.a().d().a(str);
                if (a != null) {
                    return FirmwareFactory.getInstance().createFirmwareData(a.getVersionNumber(), a.getDeviceModel(), a.getChecksum());
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = a();
                local.e(a2, "Error when update firmware, can't find latest fw of model=" + str);
                return null;
            }
            Firmware a3 = ch5.a(str);
            if (a3 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(a3.getVersionNumber(), a3.getDeviceModel(), a3.getChecksum());
            }
            Firmware a4 = ah5.p.a().d().a(str);
            if (a4 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(a4.getVersionNumber(), a4.getDeviceModel(), a4.getChecksum());
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a5 = a();
            local2.e(a5, "Error when update firmware, can't find latest fw of model=" + str);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(String str, boolean z) {
            ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(String str, boolean z, int i, zd7 zd7) {
            this(str, (i & 2) != 0 ? false : z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase", f = "UpdateFirmwareUsecase.kt", l = {45, 54}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(pm5 pm5, fb7 fb7) {
            super(fb7);
            this.this$0 = pm5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        String simpleName = pm5.class.getSimpleName();
        ee7.a((Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public pm5(DeviceRepository deviceRepository, ch5 ch5) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0110 A[Catch:{ Exception -> 0x01a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0111 A[Catch:{ Exception -> 0x01a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0118 A[Catch:{ Exception -> 0x01a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x016c A[Catch:{ Exception -> 0x01a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0191 A[Catch:{ Exception -> 0x01a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x019c A[Catch:{ Exception -> 0x01a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.pm5.b r13, com.fossil.fb7<java.lang.Object> r14) {
        /*
            r12 = this;
            boolean r0 = r14 instanceof com.fossil.pm5.e
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.fossil.pm5$e r0 = (com.fossil.pm5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pm5$e r0 = new com.fossil.pm5$e
            r0.<init>(r12, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = ""
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L_0x0061
            if (r2 == r5) goto L_0x004c
            if (r2 != r4) goto L_0x0044
            java.lang.Object r13 = r0.L$3
            com.misfit.frameworks.buttonservice.model.FirmwareData r13 = (com.misfit.frameworks.buttonservice.model.FirmwareData) r13
            java.lang.Object r1 = r0.L$2
            com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.pm5$b r1 = (com.fossil.pm5.b) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.pm5 r0 = (com.fossil.pm5) r0
            com.fossil.t87.a(r14)     // Catch:{ Exception -> 0x0041 }
            r2 = r13
            r13 = r1
            goto L_0x0168
        L_0x0041:
            r13 = move-exception
            goto L_0x01a5
        L_0x0044:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r14 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r14)
            throw r13
        L_0x004c:
            java.lang.Object r13 = r0.L$2
            com.portfolio.platform.data.model.Device r13 = (com.portfolio.platform.data.model.Device) r13
            java.lang.Object r2 = r0.L$1
            com.fossil.pm5$b r2 = (com.fossil.pm5.b) r2
            java.lang.Object r5 = r0.L$0
            com.fossil.pm5 r5 = (com.fossil.pm5) r5
            com.fossil.t87.a(r14)     // Catch:{ Exception -> 0x005d }
            goto L_0x0102
        L_0x005d:
            r13 = move-exception
            r1 = r2
            goto L_0x01a5
        L_0x0061:
            com.fossil.t87.a(r14)
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r2 = com.fossil.pm5.f
            java.lang.String r6 = "running UseCase"
            r14.d(r2, r6)
            if (r13 != 0) goto L_0x0086
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r0 = com.fossil.pm5.f     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r1 = "Error when update firmware, requestValues is NULL"
            r14.e(r0, r1)     // Catch:{ Exception -> 0x01a2 }
            com.fossil.pm5$c r14 = new com.fossil.pm5$c     // Catch:{ Exception -> 0x01a2 }
            r14.<init>()     // Catch:{ Exception -> 0x01a2 }
            return r14
        L_0x0086:
            com.portfolio.platform.data.source.DeviceRepository r14 = r12.d     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r2 = r13.a()     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.data.model.Device r14 = r14.getDeviceBySerial(r2)     // Catch:{ Exception -> 0x01a2 }
            if (r14 != 0) goto L_0x00b8
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r0 = com.fossil.pm5.f     // Catch:{ Exception -> 0x01a2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a2 }
            r1.<init>()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r2 = "Error when update firmware, can't find latest device on db serial="
            r1.append(r2)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r2 = r13.a()     // Catch:{ Exception -> 0x01a2 }
            r1.append(r2)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01a2 }
            r14.e(r0, r1)     // Catch:{ Exception -> 0x01a2 }
            com.fossil.pm5$c r14 = new com.fossil.pm5$c     // Catch:{ Exception -> 0x01a2 }
            r14.<init>()     // Catch:{ Exception -> 0x01a2 }
            return r14
        L_0x00b8:
            boolean r2 = r13.b()     // Catch:{ Exception -> 0x01a2 }
            if (r2 == 0) goto L_0x0105
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r6 = com.fossil.pm5.f     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r7 = "force download latest fw before udpate fw"
            r2.d(r6, r7)     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r6 = r2.getRemote()     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.FLogger$Component r7 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.FLogger$Session r8 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.PortfolioApp r2 = r2.c()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r9 = r2.c()     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.ApplicationEventListener$a r2 = com.portfolio.platform.ApplicationEventListener.F     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r10 = r2.a()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r11 = "[OTA Start] Download FW"
            r6.i(r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x01a2 }
            com.fossil.yw6$a r2 = com.fossil.yw6.h     // Catch:{ Exception -> 0x01a2 }
            com.fossil.yw6 r2 = r2.a()     // Catch:{ Exception -> 0x01a2 }
            r0.L$0 = r12     // Catch:{ Exception -> 0x01a2 }
            r0.L$1 = r13     // Catch:{ Exception -> 0x01a2 }
            r0.L$2 = r14     // Catch:{ Exception -> 0x01a2 }
            r0.label = r5     // Catch:{ Exception -> 0x01a2 }
            java.lang.Object r2 = r2.a(r0)     // Catch:{ Exception -> 0x01a2 }
            if (r2 != r1) goto L_0x00ff
            return r1
        L_0x00ff:
            r5 = r12
            r2 = r13
            r13 = r14
        L_0x0102:
            r14 = r13
            r13 = r2
            goto L_0x0106
        L_0x0105:
            r5 = r12
        L_0x0106:
            com.fossil.pm5$a r2 = com.fossil.pm5.g     // Catch:{ Exception -> 0x01a2 }
            com.fossil.ch5 r6 = r5.e     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r7 = r14.getSku()     // Catch:{ Exception -> 0x01a2 }
            if (r7 == 0) goto L_0x0111
            goto L_0x0112
        L_0x0111:
            r7 = r3
        L_0x0112:
            com.misfit.frameworks.buttonservice.model.FirmwareData r2 = r2.a(r6, r7)     // Catch:{ Exception -> 0x01a2 }
            if (r2 == 0) goto L_0x019c
            com.fossil.ch5 r6 = r5.e     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r7 = r14.getDeviceId()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r8 = r14.getFirmwareRevision()     // Catch:{ Exception -> 0x01a2 }
            r6.a(r7, r8)     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r7 = com.fossil.pm5.f     // Catch:{ Exception -> 0x01a2 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a2 }
            r8.<init>()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r9 = "Start update firmware with version="
            r8.append(r9)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r9 = r2.getFirmwareVersion()     // Catch:{ Exception -> 0x01a2 }
            r8.append(r9)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r9 = ", currentVersion="
            r8.append(r9)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r9 = r14.getFirmwareRevision()     // Catch:{ Exception -> 0x01a2 }
            r8.append(r9)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x01a2 }
            r6.d(r7, r8)     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.PortfolioApp r6 = r6.c()     // Catch:{ Exception -> 0x01a2 }
            r0.L$0 = r5     // Catch:{ Exception -> 0x01a2 }
            r0.L$1 = r13     // Catch:{ Exception -> 0x01a2 }
            r0.L$2 = r14     // Catch:{ Exception -> 0x01a2 }
            r0.L$3 = r2     // Catch:{ Exception -> 0x01a2 }
            r0.label = r4     // Catch:{ Exception -> 0x01a2 }
            java.lang.Object r14 = r6.d(r0)     // Catch:{ Exception -> 0x01a2 }
            if (r14 != r1) goto L_0x0168
            return r1
        L_0x0168:
            com.misfit.frameworks.buttonservice.model.UserProfile r14 = (com.misfit.frameworks.buttonservice.model.UserProfile) r14     // Catch:{ Exception -> 0x01a2 }
            if (r14 == 0) goto L_0x0191
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r4 = r0.getRemote()     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.FLogger$Component r5 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x01a2 }
            com.misfit.frameworks.buttonservice.log.FLogger$Session r6 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r7 = r13.a()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r8 = com.fossil.pm5.f     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r9 = "[OTA Start] Calling OTA from SDK"
            r4.i(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x01a2 }
            com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r1 = r13.a()     // Catch:{ Exception -> 0x01a2 }
            r0.a(r1, r2, r14)     // Catch:{ Exception -> 0x01a2 }
            com.fossil.i97 r14 = com.fossil.i97.a     // Catch:{ Exception -> 0x01a2 }
            goto L_0x0196
        L_0x0191:
            com.fossil.pm5$c r14 = new com.fossil.pm5$c     // Catch:{ Exception -> 0x01a2 }
            r14.<init>()     // Catch:{ Exception -> 0x01a2 }
        L_0x0196:
            com.fossil.pm5$d r14 = new com.fossil.pm5$d     // Catch:{ Exception -> 0x01a2 }
            r14.<init>()     // Catch:{ Exception -> 0x01a2 }
            return r14
        L_0x019c:
            com.fossil.pm5$c r14 = new com.fossil.pm5$c     // Catch:{ Exception -> 0x01a2 }
            r14.<init>()     // Catch:{ Exception -> 0x01a2 }
            return r14
        L_0x01a2:
            r14 = move-exception
            r1 = r13
            r13 = r14
        L_0x01a5:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r4 = r14.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r5 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r6 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            if (r1 == 0) goto L_0x01b9
            java.lang.String r14 = r1.a()
            if (r14 == 0) goto L_0x01b9
            r7 = r14
            goto L_0x01ba
        L_0x01b9:
            r7 = r3
        L_0x01ba:
            java.lang.String r8 = com.fossil.pm5.f
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r0 = "[OTA Start] Exception "
            r14.append(r0)
            r14.append(r13)
            java.lang.String r9 = r14.toString()
            r4.i(r5, r6, r7, r8, r9)
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.fossil.pm5.f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Inside .run failed with exception="
            r1.append(r2)
            r1.append(r13)
            java.lang.String r1 = r1.toString()
            r14.d(r0, r1)
            r13.printStackTrace()
            com.fossil.pm5$c r13 = new com.fossil.pm5$c
            r13.<init>()
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pm5.a(com.fossil.pm5$b, com.fossil.fb7):java.lang.Object");
    }
}
