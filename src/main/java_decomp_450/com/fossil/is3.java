package com.fossil;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import com.fossil.hs3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class is3 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT < 18);

    @DexIgnore
    public static void a(Rect rect, float f, float f2, float f3, float f4) {
        rect.set((int) (f - f3), (int) (f2 - f4), (int) (f + f3), (int) (f2 + f4));
    }

    @DexIgnore
    public static void b(hs3 hs3, View view, FrameLayout frameLayout) {
        if (hs3 != null) {
            if (a) {
                frameLayout.setForeground(null);
            } else {
                view.getOverlay().remove(hs3);
            }
        }
    }

    @DexIgnore
    public static void c(hs3 hs3, View view, FrameLayout frameLayout) {
        Rect rect = new Rect();
        (a ? frameLayout : view).getDrawingRect(rect);
        hs3.setBounds(rect);
        hs3.a(view, frameLayout);
    }

    @DexIgnore
    public static void a(hs3 hs3, View view, FrameLayout frameLayout) {
        c(hs3, view, frameLayout);
        if (a) {
            frameLayout.setForeground(hs3);
        } else {
            view.getOverlay().add(hs3);
        }
    }

    @DexIgnore
    public static gu3 a(SparseArray<hs3> sparseArray) {
        gu3 gu3 = new gu3();
        int i = 0;
        while (i < sparseArray.size()) {
            int keyAt = sparseArray.keyAt(i);
            hs3 valueAt = sparseArray.valueAt(i);
            if (valueAt != null) {
                gu3.put(keyAt, valueAt.f());
                i++;
            } else {
                throw new IllegalArgumentException("badgeDrawable cannot be null");
            }
        }
        return gu3;
    }

    @DexIgnore
    public static SparseArray<hs3> a(Context context, gu3 gu3) {
        SparseArray<hs3> sparseArray = new SparseArray<>(gu3.size());
        int i = 0;
        while (i < gu3.size()) {
            int keyAt = gu3.keyAt(i);
            hs3.a aVar = (hs3.a) gu3.valueAt(i);
            if (aVar != null) {
                sparseArray.put(keyAt, hs3.a(context, aVar));
                i++;
            } else {
                throw new IllegalArgumentException("BadgeDrawable's savedState cannot be null");
            }
        }
        return sparseArray;
    }
}
