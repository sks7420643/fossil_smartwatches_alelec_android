package com.fossil;

import com.fossil.fl4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ HybridPresetRepository f;
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ WatchFaceRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mm5.j;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(String str, ArrayList<Integer> arrayList, String str2) {
            ee7.b(str, "lastErrorCode");
            this.a = str;
            this.b = arrayList;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1", f = "UnlinkDeviceUseCase.kt", l = {59}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(mm5 mm5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mm5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Device device;
            String userMessage;
            Object a = nb7.a();
            int i = this.label;
            String str = "";
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                DeviceRepository a2 = this.this$0.e;
                b d = this.this$0.d;
                String str2 = null;
                String a3 = d != null ? d.a() : null;
                if (a3 != null) {
                    Device deviceBySerial = a2.getDeviceBySerial(a3);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a4 = mm5.k.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("doRemoveDevice ");
                    if (deviceBySerial != null) {
                        str2 = deviceBySerial.getDeviceId();
                    }
                    sb.append(str2);
                    local.d(a4, sb.toString());
                    if (deviceBySerial != null) {
                        DeviceRepository a5 = this.this$0.e;
                        this.L$0 = yi7;
                        this.L$1 = deviceBySerial;
                        this.label = 1;
                        obj = a5.removeDevice(deviceBySerial, this);
                        if (obj == a) {
                            return a;
                        }
                        device = deviceBySerial;
                    } else {
                        this.this$0.a(new c("UNLINK_FAIL_ON_SERVER", w97.a((Object[]) new Integer[]{pb7.a(600)}), str));
                        return i97.a;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (i == 1) {
                device = (Device) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            zi5 zi5 = (zi5) obj;
            if (zi5 instanceof bj5) {
                String deviceId = device.getDeviceId();
                PortfolioApp.g0.c().r(deviceId);
                if (be5.o.f(deviceId)) {
                    this.this$0.i.deleteWatchFacesWithSerial(deviceId);
                    this.this$0.g.deleteAllPresetBySerial(deviceId);
                } else {
                    this.this$0.f.deleteAllPresetBySerial(deviceId);
                }
                this.this$0.a(new d());
            } else if (zi5 instanceof yi5) {
                mm5 mm5 = this.this$0;
                yi5 yi5 = (yi5) zi5;
                ArrayList a6 = w97.a((Object[]) new Integer[]{pb7.a(yi5.a())});
                ServerError c = yi5.c();
                if (!(c == null || (userMessage = c.getUserMessage()) == null)) {
                    str = userMessage;
                }
                mm5.a(new c("UNLINK_FAIL_ON_SERVER", a6, str));
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = mm5.class.getSimpleName();
        ee7.a((Object) simpleName, "UnlinkDeviceUseCase::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public mm5(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(hybridPresetRepository, "mHybridPresetRepository");
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(portfolioApp, "mApp");
        ee7.b(watchFaceRepository, "watchFaceRepository");
        this.e = deviceRepository;
        this.f = hybridPresetRepository;
        this.g = dianaPresetRepository;
        this.h = portfolioApp;
        this.i = watchFaceRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return j;
    }

    @DexIgnore
    public final ik7 d() {
        return xh7.b(b(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        FLogger.INSTANCE.getLocal().d(j, "running UseCase");
        if (bVar == null) {
            return new c("", null, "");
        }
        this.d = bVar;
        String a2 = bVar.a();
        String c2 = this.h.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "remove device " + a2 + " currentActive " + c2);
        d();
        return new Object();
    }
}
