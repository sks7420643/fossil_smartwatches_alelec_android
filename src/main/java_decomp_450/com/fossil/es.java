package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.lifecycle.Lifecycle;
import coil.memory.BaseRequestDelegate;
import coil.memory.RequestDelegate;
import coil.memory.ViewTargetRequestDelegate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es {
    @DexIgnore
    public /* final */ lq a;
    @DexIgnore
    public /* final */ ds b;

    @DexIgnore
    public es(lq lqVar, ds dsVar) {
        ee7.b(lqVar, "imageLoader");
        ee7.b(dsVar, "referenceCounter");
        this.a = lqVar;
        this.b = dsVar;
    }

    @DexIgnore
    public final ts a(it itVar) {
        ts tsVar;
        ee7.b(itVar, "request");
        if (itVar instanceof et) {
            vt u = itVar.u();
            if (u == null) {
                return gs.a;
            }
            if (u instanceof ut) {
                tsVar = new ps((ut) u, this.b);
            } else {
                tsVar = new ls(u, this.b);
            }
            return tsVar;
        }
        throw new p87();
    }

    @DexIgnore
    public final RequestDelegate a(it itVar, ts tsVar, Lifecycle lifecycle, ti7 ti7, hj7<? extends Drawable> hj7) {
        ee7.b(itVar, "request");
        ee7.b(tsVar, "targetDelegate");
        ee7.b(lifecycle, "lifecycle");
        ee7.b(ti7, "mainDispatcher");
        ee7.b(hj7, "deferred");
        if (itVar instanceof et) {
            vt u = itVar.u();
            if (u instanceof wt) {
                ViewTargetRequestDelegate viewTargetRequestDelegate = new ViewTargetRequestDelegate(this.a, (et) itVar, tsVar, lifecycle, ti7, hj7);
                lifecycle.a(viewTargetRequestDelegate);
                iu.a(((wt) u).getView()).a(viewTargetRequestDelegate);
                return viewTargetRequestDelegate;
            }
            BaseRequestDelegate baseRequestDelegate = new BaseRequestDelegate(lifecycle, ti7, hj7);
            lifecycle.a(baseRequestDelegate);
            return baseRequestDelegate;
        }
        throw new p87();
    }
}
