package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg4 extends bh4 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public wg4(bh4 bh4, int i, int i2) {
        super(bh4);
        this.c = (short) i;
        this.d = (short) i2;
    }

    @DexIgnore
    @Override // com.fossil.bh4
    public void a(ch4 ch4, byte[] bArr) {
        int i = 0;
        while (true) {
            short s = this.d;
            if (i < s) {
                if (i == 0 || (i == 31 && s <= 62)) {
                    ch4.a(31, 5);
                    short s2 = this.d;
                    if (s2 > 62) {
                        ch4.a(s2 - 31, 16);
                    } else if (i == 0) {
                        ch4.a(Math.min((int) s2, 31), 5);
                    } else {
                        ch4.a(s2 - 31, 5);
                    }
                }
                ch4.a(bArr[this.c + i], 8);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(SimpleComparison.LESS_THAN_OPERATION);
        sb.append((int) this.c);
        sb.append("::");
        sb.append((this.c + this.d) - 1);
        sb.append('>');
        return sb.toString();
    }
}
