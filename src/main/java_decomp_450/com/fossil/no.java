package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class no {
    @DexIgnore
    public String a;
    @DexIgnore
    public Long b;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public no(String str, boolean z) {
        this(str, z ? 1 : 0);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof no)) {
            return false;
        }
        no noVar = (no) obj;
        if (!this.a.equals(noVar.a)) {
            return false;
        }
        Long l = this.b;
        Long l2 = noVar.b;
        if (l != null) {
            return l.equals(l2);
        }
        if (l2 == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        Long l = this.b;
        return hashCode + (l != null ? l.hashCode() : 0);
    }

    @DexIgnore
    public no(String str, long j) {
        this.a = str;
        this.b = Long.valueOf(j);
    }
}
