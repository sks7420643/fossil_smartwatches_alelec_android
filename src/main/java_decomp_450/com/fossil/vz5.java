package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz5 implements Factory<rz5> {
    @DexIgnore
    public static rz5 a(uz5 uz5) {
        rz5 b = uz5.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
