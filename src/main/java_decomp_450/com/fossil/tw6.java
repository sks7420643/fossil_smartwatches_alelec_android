package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tw6 {
    @DexIgnore
    public static final void a(Canvas canvas, RectF rectF, Paint paint, float f) {
        ee7.b(canvas, "$this$drawTopLeftRoundRect");
        ee7.b(rectF, "rect");
        ee7.b(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopLeftRoundRect, rect: " + rectF + ", radius: " + f);
        canvas.drawRoundRect(rectF, f, f, paint);
        canvas.drawRect((rectF.width() / ((float) 2)) + rectF.left, rectF.top, rectF.right, rectF.bottom, paint);
        canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
    }

    @DexIgnore
    public static final void b(Canvas canvas, RectF rectF, Paint paint, float f) {
        ee7.b(canvas, "$this$drawTopRightRoundRect");
        ee7.b(rectF, "rect");
        ee7.b(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopRightRoundRect, rect: " + rectF + ", radius: " + f);
        canvas.drawRoundRect(rectF, f, f, paint);
        canvas.drawRect(rectF.left, rectF.top, rectF.right - (rectF.width() / ((float) 2)), rectF.bottom, paint);
        canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
    }

    @DexIgnore
    public static final void c(Canvas canvas, RectF rectF, Paint paint, float f) {
        ee7.b(canvas, "$this$drawTopRoundRect");
        ee7.b(rectF, "rect");
        ee7.b(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopRoundRect, rect: " + rectF + ", radius: " + f);
        if (rectF.height() >= f) {
            canvas.drawRoundRect(rectF, f, f, paint);
            canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
        }
    }

    @DexIgnore
    public static final void a(Canvas canvas, float f, float f2, float f3, float f4, float f5, float f6, boolean z, boolean z2, boolean z3, boolean z4, Paint paint) {
        ee7.b(canvas, "$this$drawRoundedPath");
        ee7.b(paint, "paint");
        Path path = new Path();
        float f7 = (float) 0;
        float f8 = f5 < f7 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f5;
        float f9 = f6 < f7 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f6;
        float f10 = f3 - f;
        float f11 = f4 - f2;
        float f12 = (float) 2;
        float f13 = f10 / f12;
        if (f8 > f13) {
            f8 = f13;
        }
        if (f9 > f13) {
            f9 = f13;
        }
        float f14 = f10 - (f12 * f8);
        float f15 = f11 - (f12 * f9);
        path.moveTo(f3, f2 + f9);
        if (z2) {
            float f16 = -f9;
            path.rQuadTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f16, -f8, f16);
        } else {
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f9);
            path.rLineTo(-f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        path.rLineTo(-f14, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        if (z) {
            float f17 = -f8;
            path.rQuadTo(f17, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f17, f9);
        } else {
            path.rLineTo(-f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9);
        }
        path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f15);
        if (z4) {
            path.rQuadTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9, f8, f9);
        } else {
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9);
            path.rLineTo(f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        path.rLineTo(f14, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        if (z3) {
            path.rQuadTo(f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f8, -f9);
        } else {
            path.rLineTo(f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f9);
        }
        path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f15);
        path.close();
        canvas.drawPath(path, paint);
    }
}
