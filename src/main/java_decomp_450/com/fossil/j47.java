package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j47 extends e47 {
    @DexIgnore
    public m57 m;
    @DexIgnore
    public JSONObject n; // = null;

    @DexIgnore
    public j47(Context context, int i, JSONObject jSONObject, a47 a47) {
        super(context, i, a47);
        this.m = new m57(context);
        this.n = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public f47 a() {
        return f47.b;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public boolean a(JSONObject jSONObject) {
        l57 l57 = ((e47) this).d;
        if (l57 != null) {
            jSONObject.put("ut", l57.d());
        }
        JSONObject jSONObject2 = this.n;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (v57.B(((e47) this).j)) {
            jSONObject.put("ncts", 1);
        }
        this.m.a(jSONObject, null);
        return true;
    }
}
