package com.fossil;

import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr extends or {
    @DexIgnore
    public /* final */ Drawable a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ br c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nr(Drawable drawable, boolean z, br brVar) {
        super(null);
        ee7.b(drawable, ResourceManager.DRAWABLE);
        ee7.b(brVar, "dataSource");
        this.a = drawable;
        this.b = z;
        this.c = brVar;
    }

    @DexIgnore
    public static /* synthetic */ nr a(nr nrVar, Drawable drawable, boolean z, br brVar, int i, Object obj) {
        if ((i & 1) != 0) {
            drawable = nrVar.a;
        }
        if ((i & 2) != 0) {
            z = nrVar.b;
        }
        if ((i & 4) != 0) {
            brVar = nrVar.c;
        }
        return nrVar.a(drawable, z, brVar);
    }

    @DexIgnore
    public final Drawable a() {
        return this.a;
    }

    @DexIgnore
    public final nr a(Drawable drawable, boolean z, br brVar) {
        ee7.b(drawable, ResourceManager.DRAWABLE);
        ee7.b(brVar, "dataSource");
        return new nr(drawable, z, brVar);
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final br c() {
        return this.c;
    }

    @DexIgnore
    public final Drawable d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof nr)) {
            return false;
        }
        nr nrVar = (nr) obj;
        return ee7.a(this.a, nrVar.a) && this.b == nrVar.b && ee7.a(this.c, nrVar.c);
    }

    @DexIgnore
    public int hashCode() {
        Drawable drawable = this.a;
        int i = 0;
        int hashCode = (drawable != null ? drawable.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode + i2) * 31;
        br brVar = this.c;
        if (brVar != null) {
            i = brVar.hashCode();
        }
        return i4 + i;
    }

    @DexIgnore
    public String toString() {
        return "DrawableResult(drawable=" + this.a + ", isSampled=" + this.b + ", dataSource=" + this.c + ")";
    }
}
