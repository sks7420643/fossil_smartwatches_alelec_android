package com.fossil;

import com.fossil.d74;
import java.io.File;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c74 {
    @DexIgnore
    public /* final */ d74.c a;

    @DexIgnore
    public c74(d74.c cVar) {
        this.a = cVar;
    }

    @DexIgnore
    public boolean a() {
        File[] b = this.a.b();
        File[] a2 = this.a.a();
        if (b != null && b.length > 0) {
            return true;
        }
        if (a2 == null || a2.length <= 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public List<g74> b() {
        z24.a().a("Checking for crash reports...");
        File[] b = this.a.b();
        File[] a2 = this.a.a();
        LinkedList linkedList = new LinkedList();
        if (b != null) {
            for (File file : b) {
                z24.a().a("Found crash report " + file.getPath());
                linkedList.add(new h74(file));
            }
        }
        if (a2 != null) {
            for (File file2 : a2) {
                linkedList.add(new f74(file2));
            }
        }
        if (linkedList.isEmpty()) {
            z24.a().a("No reports found.");
        }
        return linkedList;
    }

    @DexIgnore
    public void a(g74 g74) {
        g74.remove();
    }

    @DexIgnore
    public void a(List<g74> list) {
        for (g74 g74 : list) {
            a(g74);
        }
    }
}
