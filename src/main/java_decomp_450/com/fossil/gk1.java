package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk1 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ ii1 CREATOR; // = new ii1(null);
    @DexIgnore
    public /* final */ cs1 a;
    @DexIgnore
    public /* final */ r60 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public gk1(cs1 cs1, r60 r60, boolean z, byte[] bArr) {
        this.a = cs1;
        this.b = r60;
        this.c = z;
        this.d = bArr;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.d, yz0.a(this.a)), r51.I2, this.b.a()), r51.N4, Boolean.valueOf(this.c)), r51.O4, yz0.a(this.d, (String) null, 1));
    }

    @DexIgnore
    public final cs1 b() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeParcelable(this.b, i);
        parcel.writeByte(this.c ? (byte) 1 : 0);
        parcel.writeByteArray(this.d);
    }
}
