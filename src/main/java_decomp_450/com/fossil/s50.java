package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s50 extends FilterInputStream {
    @DexIgnore
    public int a; // = RecyclerView.UNDEFINED_DURATION;

    @DexIgnore
    public s50(InputStream inputStream) {
        super(inputStream);
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() throws IOException {
        int i = this.a;
        if (i == Integer.MIN_VALUE) {
            return super.available();
        }
        return Math.min(i, super.available());
    }

    @DexIgnore
    public final long e(long j) {
        int i = this.a;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    @DexIgnore
    public final void g(long j) {
        int i = this.a;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.a = (int) (((long) i) - j);
        }
    }

    @DexIgnore
    public synchronized void mark(int i) {
        super.mark(i);
        this.a = i;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        if (e(1) == -1) {
            return -1;
        }
        int read = super.read();
        g(1);
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized void reset() throws IOException {
        super.reset();
        this.a = RecyclerView.UNDEFINED_DURATION;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) throws IOException {
        long e = e(j);
        if (e == -1) {
            return 0;
        }
        long skip = super.skip(e);
        g(skip);
        return skip;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int e = (int) e((long) i2);
        if (e == -1) {
            return -1;
        }
        int read = super.read(bArr, i, e);
        g((long) read);
        return read;
    }
}
