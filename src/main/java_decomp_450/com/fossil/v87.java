package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v87<A, B, C> implements Serializable {
    @DexIgnore
    public /* final */ A first;
    @DexIgnore
    public /* final */ B second;
    @DexIgnore
    public /* final */ C third;

    @DexIgnore
    public v87(A a, B b, C c) {
        this.first = a;
        this.second = b;
        this.third = c;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.v87 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ v87 copy$default(v87 v87, Object obj, Object obj2, Object obj3, int i, Object obj4) {
        if ((i & 1) != 0) {
            obj = v87.first;
        }
        if ((i & 2) != 0) {
            obj2 = v87.second;
        }
        if ((i & 4) != 0) {
            obj3 = v87.third;
        }
        return v87.copy(obj, obj2, obj3);
    }

    @DexIgnore
    public final A component1() {
        return this.first;
    }

    @DexIgnore
    public final B component2() {
        return this.second;
    }

    @DexIgnore
    public final C component3() {
        return this.third;
    }

    @DexIgnore
    public final v87<A, B, C> copy(A a, B b, C c) {
        return new v87<>(a, b, c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof v87)) {
            return false;
        }
        v87 v87 = (v87) obj;
        return ee7.a(this.first, v87.first) && ee7.a(this.second, v87.second) && ee7.a(this.third, v87.third);
    }

    @DexIgnore
    public final A getFirst() {
        return this.first;
    }

    @DexIgnore
    public final B getSecond() {
        return this.second;
    }

    @DexIgnore
    public final C getThird() {
        return this.third;
    }

    @DexIgnore
    public int hashCode() {
        A a = this.first;
        int i = 0;
        int hashCode = (a != null ? a.hashCode() : 0) * 31;
        B b = this.second;
        int hashCode2 = (hashCode + (b != null ? b.hashCode() : 0)) * 31;
        C c = this.third;
        if (c != null) {
            i = c.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return '(' + ((Object) this.first) + ", " + ((Object) this.second) + ", " + ((Object) this.third) + ')';
    }
}
