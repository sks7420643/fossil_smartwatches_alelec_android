package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn4 {
    @DexIgnore
    @te4("challengeId")
    public String a;
    @DexIgnore
    @te4("numberOfPlayers")
    public Integer b;
    @DexIgnore
    @te4("topPlayers")
    public List<jn4> c;
    @DexIgnore
    @te4("nearPlayers")
    public List<jn4> d;

    @DexIgnore
    public hn4(String str, Integer num, List<jn4> list, List<jn4> list2) {
        ee7.b(str, "challengeId");
        this.a = str;
        this.b = num;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final List<jn4> b() {
        return this.d;
    }

    @DexIgnore
    public final Integer c() {
        return this.b;
    }

    @DexIgnore
    public final List<jn4> d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hn4)) {
            return false;
        }
        hn4 hn4 = (hn4) obj;
        return ee7.a(this.a, hn4.a) && ee7.a(this.b, hn4.b) && ee7.a(this.c, hn4.c) && ee7.a(this.d, hn4.d);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Integer num = this.b;
        int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
        List<jn4> list = this.c;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        List<jn4> list2 = this.d;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "BCDisplayPlayer(challengeId=" + this.a + ", numberOfPlayers=" + this.b + ", topPlayers=" + this.c + ", nearPlayers=" + this.d + ")";
    }
}
