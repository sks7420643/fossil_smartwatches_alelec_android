package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e71 extends uh1 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ qk1 I;
    @DexIgnore
    public /* final */ qk1 J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    public e71(g51 g51, short s, qa1 qa1, ri1 ri1, int i) {
        super(qa1, ri1, i);
        this.K = s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(g51.a).putShort(this.K).array();
        ee7.a((Object) array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(g51.a()).putShort(this.K).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        qk1 qk1 = qk1.FTC;
        this.I = qk1;
        this.J = qk1;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final mw0 a(byte b) {
        return tu0.j.a(b);
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.H = bArr;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.y0, yz0.a(this.K));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 l() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] n() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 o() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] q() {
        return this.H;
    }

    @DexIgnore
    public final short r() {
        return this.K;
    }
}
