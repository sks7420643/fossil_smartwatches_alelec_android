package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.fossil.i17;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n07 extends i17 {
    @DexIgnore
    public static /* final */ int b; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;

    @DexIgnore
    public n07(Context context) {
        this.a = context.getAssets();
    }

    @DexIgnore
    public static String c(g17 g17) {
        return g17.d.toString().substring(b);
    }

    @DexIgnore
    @Override // com.fossil.i17
    public boolean a(g17 g17) {
        Uri uri = g17.d;
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.i17
    public i17.a a(g17 g17, int i) throws IOException {
        return new i17.a(this.a.open(c(g17)), Picasso.LoadedFrom.DISK);
    }
}
