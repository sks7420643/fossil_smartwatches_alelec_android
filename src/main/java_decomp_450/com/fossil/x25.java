package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x25 extends w25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i G0; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H0;
    @DexIgnore
    public long F0;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H0 = sparseIntArray;
        sparseIntArray.put(2131362679, 1);
        H0.put(2131362734, 2);
        H0.put(2131362735, 3);
        H0.put(2131363349, 4);
        H0.put(2131363334, 5);
        H0.put(2131363276, 6);
        H0.put(2131363297, 7);
        H0.put(2131362557, 8);
        H0.put(2131363318, 9);
        H0.put(2131361985, 10);
        H0.put(2131363403, 11);
        H0.put(2131362558, 12);
        H0.put(2131362030, 13);
        H0.put(2131362583, 14);
        H0.put(2131363211, 15);
        H0.put(2131363213, 16);
        H0.put(2131363212, 17);
        H0.put(2131362026, 18);
        H0.put(2131362582, 19);
        H0.put(2131363202, 20);
        H0.put(2131363204, 21);
        H0.put(2131363203, 22);
        H0.put(2131362031, 23);
        H0.put(2131362584, 24);
        H0.put(2131363205, 25);
        H0.put(2131363207, 26);
        H0.put(2131363206, 27);
        H0.put(2131362041, 28);
        H0.put(2131362585, 29);
        H0.put(2131363208, 30);
        H0.put(2131363210, 31);
        H0.put(2131363209, 32);
        H0.put(2131362074, 33);
        H0.put(2131362520, 34);
        H0.put(2131362383, 35);
        H0.put(2131361951, 36);
        H0.put(2131363256, 37);
        H0.put(2131362747, 38);
        H0.put(2131362272, 39);
        H0.put(2131362985, 40);
        H0.put(2131362168, 41);
        H0.put(2131361921, 42);
        H0.put(2131363328, 43);
        H0.put(2131362275, 44);
        H0.put(2131362789, 45);
        H0.put(2131361952, 46);
        H0.put(2131362720, 47);
        H0.put(2131361930, 48);
        H0.put(2131362634, 49);
        H0.put(2131363380, 50);
        H0.put(2131361932, 51);
        H0.put(2131362646, 52);
        H0.put(2131363385, 53);
        H0.put(2131361957, 54);
        H0.put(2131362733, 55);
        H0.put(2131361935, 56);
        H0.put(2131362659, 57);
        H0.put(2131361946, 58);
        H0.put(2131362709, 59);
        H0.put(2131361940, 60);
        H0.put(2131362693, 61);
        H0.put(2131361928, 62);
        H0.put(2131362626, 63);
        H0.put(2131361955, 64);
        H0.put(2131362726, 65);
        H0.put(2131363294, 66);
    }
    */

    @DexIgnore
    public x25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 67, G0, H0));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.F0 = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.F0 != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.F0 = 1;
        }
        g();
    }

    @DexIgnore
    public x25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (Barrier) objArr[42], (RelativeLayout) objArr[62], (RelativeLayout) objArr[48], (RelativeLayout) objArr[51], (RelativeLayout) objArr[56], (RelativeLayout) objArr[60], (RelativeLayout) objArr[58], (FlexibleButton) objArr[36], (RelativeLayout) objArr[46], (RelativeLayout) objArr[64], (RelativeLayout) objArr[54], (ConstraintLayout) objArr[10], (ConstraintLayout) objArr[18], (ConstraintLayout) objArr[13], (ConstraintLayout) objArr[23], (ConstraintLayout) objArr[28], (ConstraintLayout) objArr[33], (FlexibleButton) objArr[41], (FlexibleButton) objArr[39], (FlexibleButton) objArr[44], (FlexibleTextView) objArr[35], (FlexibleTextView) objArr[34], (Barrier) objArr[8], (Guideline) objArr[12], (ImageView) objArr[19], (ImageView) objArr[14], (ImageView) objArr[24], (ImageView) objArr[29], (RTLImageView) objArr[63], (RTLImageView) objArr[49], (RTLImageView) objArr[52], (RTLImageView) objArr[57], (RTLImageView) objArr[1], (RTLImageView) objArr[61], (RTLImageView) objArr[59], (RTLImageView) objArr[47], (RTLImageView) objArr[65], (RTLImageView) objArr[55], (FossilCircleImageView) objArr[2], (FossilCircleImageView) objArr[3], (ConstraintLayout) objArr[38], (LinearLayout) objArr[45], (NestedScrollView) objArr[0], (RecyclerView) objArr[40], (AutoResizeTextView) objArr[20], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[21], (AutoResizeTextView) objArr[25], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[26], (AutoResizeTextView) objArr[30], (FlexibleTextView) objArr[32], (FlexibleTextView) objArr[31], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[37], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[66], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[43], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (View) objArr[50], (View) objArr[53], (Guideline) objArr[11]);
        this.F0 = -1;
        ((w25) this).g0.setTag(null);
        a(view);
        f();
    }
}
