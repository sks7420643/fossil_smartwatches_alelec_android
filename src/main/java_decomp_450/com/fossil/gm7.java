package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gm7 extends tk7 implements jj7 {
    @DexIgnore
    public /* final */ Throwable b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public gm7(Throwable th, String str) {
        this.b = th;
        this.c = str;
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public boolean b(ib7 ib7) {
        k();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.tk7
    public tk7 g() {
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r1 != null) goto L_0x0028;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Void k() {
        /*
            r4 = this;
            java.lang.Throwable r0 = r4.b
            if (r0 == 0) goto L_0x0037
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Module with the Main dispatcher had failed to initialize"
            r0.append(r1)
            java.lang.String r1 = r4.c
            if (r1 == 0) goto L_0x0026
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ". "
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            if (r1 == 0) goto L_0x0026
            goto L_0x0028
        L_0x0026:
            java.lang.String r1 = ""
        L_0x0028:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.Throwable r2 = r4.b
            r1.<init>(r0, r2)
            throw r1
        L_0x0037:
            com.fossil.fm7.a()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gm7.k():java.lang.Void");
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Main[missing");
        if (this.b != null) {
            str = ", cause=" + this.b;
        } else {
            str = "";
        }
        sb.append(str);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.jj7
    public rj7 a(long j, Runnable runnable) {
        k();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public Void a(ib7 ib7, Runnable runnable) {
        k();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jj7
    public Void a(long j, ai7<? super i97> ai7) {
        k();
        throw null;
    }
}
