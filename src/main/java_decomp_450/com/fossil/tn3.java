package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<tn3> CREATOR; // = new sn3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ i02 b;
    @DexIgnore
    public /* final */ c72 c;

    @DexIgnore
    public tn3(int i, i02 i02, c72 c72) {
        this.a = i;
        this.b = i02;
        this.c = c72;
    }

    @DexIgnore
    public final i02 e() {
        return this.b;
    }

    @DexIgnore
    public final c72 g() {
        return this.c;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, (Parcelable) this.b, i, false);
        k72.a(parcel, 3, (Parcelable) this.c, i, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public tn3(int i) {
        this(new i02(8, null), null);
    }

    @DexIgnore
    public tn3(i02 i02, c72 c72) {
        this(1, i02, null);
    }
}
