package com.fossil;

import android.util.Log;
import com.facebook.internal.Utility;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class va2 {
    @DexIgnore
    public static /* final */ va2 d; // = new va2(true, null, null);
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Throwable c;

    @DexIgnore
    public va2(boolean z, String str, Throwable th) {
        this.a = z;
        this.b = str;
        this.c = th;
    }

    @DexIgnore
    public static va2 a(Callable<String> callable) {
        return new xa2(callable);
    }

    @DexIgnore
    public static va2 c() {
        return d;
    }

    @DexIgnore
    public final void b() {
        if (!this.a && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (this.c != null) {
                Log.d("GoogleCertificatesRslt", a(), this.c);
            } else {
                Log.d("GoogleCertificatesRslt", a());
            }
        }
    }

    @DexIgnore
    public static va2 a(String str) {
        return new va2(false, str, null);
    }

    @DexIgnore
    public static va2 a(String str, Throwable th) {
        return new va2(false, str, th);
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public static String a(String str, na2 na2, boolean z, boolean z2) {
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", z2 ? "debug cert rejected" : "not whitelisted", str, s92.a(j92.a(Utility.HASH_ALGORITHM_SHA1).digest(na2.E())), Boolean.valueOf(z), "12451009.false");
    }
}
