package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.WaveView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x45 extends w45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        y.put(2131362927, 2);
        y.put(2131362517, 3);
        y.put(2131362662, 4);
        y.put(2131362377, 5);
    }
    */

    @DexIgnore
    public x45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 6, x, y));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public x45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (RTLImageView) objArr[1], (WaveView) objArr[4], (DashBar) objArr[2], (ConstraintLayout) objArr[0]);
        this.w = -1;
        ((w45) this).v.setTag(null);
        a(view);
        f();
    }
}
