package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rh5 implements MembersInjector<NotificationReceiver> {
    @DexIgnore
    public static void a(NotificationReceiver notificationReceiver, ch5 ch5) {
        notificationReceiver.a = ch5;
    }

    @DexIgnore
    public static void a(NotificationReceiver notificationReceiver, ad5 ad5) {
        notificationReceiver.b = ad5;
    }

    @DexIgnore
    public static void a(NotificationReceiver notificationReceiver, UserRepository userRepository) {
        notificationReceiver.c = userRepository;
    }
}
