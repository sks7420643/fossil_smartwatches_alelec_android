package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nv7 implements tu7<mo7, Boolean> {
    @DexIgnore
    public static /* final */ nv7 a; // = new nv7();

    @DexIgnore
    public Boolean a(mo7 mo7) throws IOException {
        return Boolean.valueOf(mo7.string());
    }
}
