package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kz {
    @DexIgnore
    Bitmap a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void a(Bitmap bitmap);

    @DexIgnore
    String b(int i, int i2, Bitmap.Config config);

    @DexIgnore
    String b(Bitmap bitmap);

    @DexIgnore
    int c(Bitmap bitmap);

    @DexIgnore
    Bitmap removeLast();
}
