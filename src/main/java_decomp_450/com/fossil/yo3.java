package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wo3 a;

    @DexIgnore
    public yo3(wo3 wo3) {
        this.a = wo3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a.b) {
            if (this.a.c != null) {
                this.a.c.onCanceled();
            }
        }
    }
}
