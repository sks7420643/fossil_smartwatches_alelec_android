package com.fossil;

import android.util.Log;
import com.fossil.gy;
import com.fossil.nz;
import com.fossil.oy;
import com.fossil.uz;
import com.fossil.w50;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jy implements ly, uz.a, oy.a {
    @DexIgnore
    public static /* final */ boolean i; // = Log.isLoggable("Engine", 2);
    @DexIgnore
    public /* final */ ry a;
    @DexIgnore
    public /* final */ ny b;
    @DexIgnore
    public /* final */ uz c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ xy e;
    @DexIgnore
    public /* final */ c f;
    @DexIgnore
    public /* final */ a g;
    @DexIgnore
    public /* final */ zx h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ gy.e a;
        @DexIgnore
        public /* final */ b9<gy<?>> b; // = w50.a(150, new C0090a());
        @DexIgnore
        public int c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jy$a$a")
        /* renamed from: com.fossil.jy$a$a  reason: collision with other inner class name */
        public class C0090a implements w50.d<gy<?>> {
            @DexIgnore
            public C0090a() {
            }

            @DexIgnore
            @Override // com.fossil.w50.d
            public gy<?> create() {
                a aVar = a.this;
                return new gy<>(aVar.a, aVar.b);
            }
        }

        @DexIgnore
        public a(gy.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public <R> gy<R> a(cw cwVar, Object obj, my myVar, yw ywVar, int i, int i2, Class<?> cls, Class<R> cls2, ew ewVar, iy iyVar, Map<Class<?>, ex<?>> map, boolean z, boolean z2, boolean z3, ax axVar, gy.b<R> bVar) {
            gy<?> a2 = this.b.a();
            u50.a(a2);
            gy<R> gyVar = (gy<R>) a2;
            int i3 = this.c;
            this.c = i3 + 1;
            gyVar.a(cwVar, obj, myVar, ywVar, i, i2, cls, cls2, ewVar, iyVar, map, z, z2, z3, axVar, bVar, i3);
            return gyVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ xz a;
        @DexIgnore
        public /* final */ xz b;
        @DexIgnore
        public /* final */ xz c;
        @DexIgnore
        public /* final */ xz d;
        @DexIgnore
        public /* final */ ly e;
        @DexIgnore
        public /* final */ oy.a f;
        @DexIgnore
        public /* final */ b9<ky<?>> g; // = w50.a(150, new a());

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements w50.d<ky<?>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.w50.d
            public ky<?> create() {
                b bVar = b.this;
                return new ky<>(bVar.a, bVar.b, bVar.c, bVar.d, bVar.e, bVar.f, bVar.g);
            }
        }

        @DexIgnore
        public b(xz xzVar, xz xzVar2, xz xzVar3, xz xzVar4, ly lyVar, oy.a aVar) {
            this.a = xzVar;
            this.b = xzVar2;
            this.c = xzVar3;
            this.d = xzVar4;
            this.e = lyVar;
            this.f = aVar;
        }

        @DexIgnore
        public <R> ky<R> a(yw ywVar, boolean z, boolean z2, boolean z3, boolean z4) {
            ky<?> a2 = this.g.a();
            u50.a(a2);
            ky<R> kyVar = (ky<R>) a2;
            kyVar.a(ywVar, z, z2, z3, z4);
            return kyVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements gy.e {
        @DexIgnore
        public /* final */ nz.a a;
        @DexIgnore
        public volatile nz b;

        @DexIgnore
        public c(nz.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.gy.e
        public nz a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.a.build();
                    }
                    if (this.b == null) {
                        this.b = new oz();
                    }
                }
            }
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d {
        @DexIgnore
        public /* final */ ky<?> a;
        @DexIgnore
        public /* final */ s40 b;

        @DexIgnore
        public d(s40 s40, ky<?> kyVar) {
            this.b = s40;
            this.a = kyVar;
        }

        @DexIgnore
        public void a() {
            synchronized (jy.this) {
                this.a.c(this.b);
            }
        }
    }

    @DexIgnore
    public jy(uz uzVar, nz.a aVar, xz xzVar, xz xzVar2, xz xzVar3, xz xzVar4, boolean z) {
        this(uzVar, aVar, xzVar, xzVar2, xzVar3, xzVar4, null, null, null, null, null, null, z);
    }

    @DexIgnore
    public <R> d a(cw cwVar, Object obj, yw ywVar, int i2, int i3, Class<?> cls, Class<R> cls2, ew ewVar, iy iyVar, Map<Class<?>, ex<?>> map, boolean z, boolean z2, ax axVar, boolean z3, boolean z4, boolean z5, boolean z6, s40 s40, Executor executor) {
        long a2 = i ? q50.a() : 0;
        my a3 = this.b.a(obj, ywVar, i2, i3, map, cls, cls2, axVar);
        synchronized (this) {
            oy<?> a4 = a(a3, z3, a2);
            if (a4 == null) {
                return a(cwVar, obj, ywVar, i2, i3, cls, cls2, ewVar, iyVar, map, z, z2, axVar, z3, z4, z5, z6, s40, executor, a3, a2);
            }
            s40.a(a4, sw.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    public final oy<?> b(yw ywVar) {
        oy<?> b2 = this.h.b(ywVar);
        if (b2 != null) {
            b2.a();
        }
        return b2;
    }

    @DexIgnore
    public final oy<?> c(yw ywVar) {
        oy<?> a2 = a(ywVar);
        if (a2 != null) {
            a2.a();
            this.h.a(ywVar, a2);
        }
        return a2;
    }

    @DexIgnore
    public jy(uz uzVar, nz.a aVar, xz xzVar, xz xzVar2, xz xzVar3, xz xzVar4, ry ryVar, ny nyVar, zx zxVar, b bVar, a aVar2, xy xyVar, boolean z) {
        this.c = uzVar;
        this.f = new c(aVar);
        zx zxVar2 = zxVar == null ? new zx(z) : zxVar;
        this.h = zxVar2;
        zxVar2.a(this);
        this.b = nyVar == null ? new ny() : nyVar;
        this.a = ryVar == null ? new ry() : ryVar;
        this.d = bVar == null ? new b(xzVar, xzVar2, xzVar3, xzVar4, this, this) : bVar;
        this.g = aVar2 == null ? new a(this.f) : aVar2;
        this.e = xyVar == null ? new xy() : xyVar;
        uzVar.a(this);
    }

    @DexIgnore
    public void b(uy<?> uyVar) {
        if (uyVar instanceof oy) {
            ((oy) uyVar).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    public final <R> d a(cw cwVar, Object obj, yw ywVar, int i2, int i3, Class<?> cls, Class<R> cls2, ew ewVar, iy iyVar, Map<Class<?>, ex<?>> map, boolean z, boolean z2, ax axVar, boolean z3, boolean z4, boolean z5, boolean z6, s40 s40, Executor executor, my myVar, long j) {
        ky<?> a2 = this.a.a(myVar, z6);
        if (a2 != null) {
            a2.a(s40, executor);
            if (i) {
                a("Added to existing load", j, myVar);
            }
            return new d(s40, a2);
        }
        ky<R> a3 = this.d.a(myVar, z3, z4, z5, z6);
        gy<R> a4 = this.g.a(cwVar, obj, myVar, ywVar, i2, i3, cls, cls2, ewVar, iyVar, map, z, z2, z6, axVar, a3);
        this.a.a((yw) myVar, (ky<?>) a3);
        a3.a(s40, executor);
        a3.b(a4);
        if (i) {
            a("Started new load", j, myVar);
        }
        return new d(s40, a3);
    }

    @DexIgnore
    public final oy<?> a(my myVar, boolean z, long j) {
        if (!z) {
            return null;
        }
        oy<?> b2 = b(myVar);
        if (b2 != null) {
            if (i) {
                a("Loaded resource from active resources", j, myVar);
            }
            return b2;
        }
        oy<?> c2 = c(myVar);
        if (c2 == null) {
            return null;
        }
        if (i) {
            a("Loaded resource from cache", j, myVar);
        }
        return c2;
    }

    @DexIgnore
    public static void a(String str, long j, yw ywVar) {
        Log.v("Engine", str + " in " + q50.a(j) + "ms, key: " + ywVar);
    }

    @DexIgnore
    public final oy<?> a(yw ywVar) {
        uy<?> a2 = this.c.a(ywVar);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof oy) {
            return (oy) a2;
        }
        return new oy<>(a2, true, true, ywVar, this);
    }

    @DexIgnore
    @Override // com.fossil.ly
    public synchronized void a(ky<?> kyVar, yw ywVar, oy<?> oyVar) {
        if (oyVar != null) {
            if (oyVar.f()) {
                this.h.a(ywVar, oyVar);
            }
        }
        this.a.b(ywVar, kyVar);
    }

    @DexIgnore
    @Override // com.fossil.ly
    public synchronized void a(ky<?> kyVar, yw ywVar) {
        this.a.b(ywVar, kyVar);
    }

    @DexIgnore
    @Override // com.fossil.uz.a
    public void a(uy<?> uyVar) {
        this.e.a(uyVar, true);
    }

    @DexIgnore
    @Override // com.fossil.oy.a
    public void a(yw ywVar, oy<?> oyVar) {
        this.h.a(ywVar);
        if (oyVar.f()) {
            this.c.a(ywVar, oyVar);
        } else {
            this.e.a(oyVar, false);
        }
    }
}
