package com.fossil;

import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko4<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ Range b;
    @DexIgnore
    public /* final */ ServerError c;

    @DexIgnore
    public ko4(T t, Range range, ServerError serverError) {
        this.a = t;
        this.b = range;
        this.c = serverError;
    }

    @DexIgnore
    public final ServerError a() {
        return this.c;
    }

    @DexIgnore
    public final Range b() {
        return this.b;
    }

    @DexIgnore
    public final T c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ko4)) {
            return false;
        }
        ko4 ko4 = (ko4) obj;
        return ee7.a(this.a, ko4.a) && ee7.a(this.b, ko4.b) && ee7.a(this.c, ko4.c);
    }

    @DexIgnore
    public int hashCode() {
        T t = this.a;
        int i = 0;
        int hashCode = (t != null ? t.hashCode() : 0) * 31;
        Range range = this.b;
        int hashCode2 = (hashCode + (range != null ? range.hashCode() : 0)) * 31;
        ServerError serverError = this.c;
        if (serverError != null) {
            i = serverError.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "ResultWrapper(value=" + ((Object) this.a) + ", range=" + this.b + ", error=" + this.c + ")";
    }

    @DexIgnore
    public ko4(T t, Range range) {
        this(t, range, null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ko4(Object obj, Range range, int i, zd7 zd7) {
        this(obj, (i & 2) != 0 ? null : range);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ko4(ServerError serverError) {
        this(null, null, serverError);
        ee7.b(serverError, "error");
    }
}
