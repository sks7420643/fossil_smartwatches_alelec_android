package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vy0 {
    @DexIgnore
    public /* synthetic */ vy0(zd7 zd7) {
    }

    @DexIgnore
    public final m01 a(int i) {
        switch (i) {
            case 10:
                return m01.BOND_NONE;
            case 11:
                return m01.BONDING;
            case 12:
                return m01.BONDED;
            default:
                return m01.BOND_NONE;
        }
    }
}
