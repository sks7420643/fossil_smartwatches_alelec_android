package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mb4 implements ServiceConnection {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ ScheduledExecutorService c;
    @DexIgnore
    public /* final */ Queue<a> d;
    @DexIgnore
    public jb4 e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public mb4(Context context, String str) {
        this(context, str, new ScheduledThreadPoolExecutor(0, new ba2("Firebase-FirebaseInstanceIdServiceConnection")));
    }

    @DexIgnore
    public synchronized no3<Void> a(Intent intent) {
        a aVar;
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "new intent queued in the bind-strategy delivery");
        }
        aVar = new a(intent);
        aVar.a(this.c);
        this.d.add(aVar);
        b();
        return aVar.b();
    }

    @DexIgnore
    public final synchronized void b() {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "flush queue called");
        }
        while (!this.d.isEmpty()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "found intent to be delivered");
            }
            if (this.e == null || !this.e.isBinderAlive()) {
                c();
                return;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "binder is alive, sending the intent.");
            }
            this.e.a(this.d.poll());
        }
    }

    @DexIgnore
    public final void c() {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            StringBuilder sb = new StringBuilder(39);
            sb.append("binder is dead. start connection? ");
            sb.append(!this.f);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        if (!this.f) {
            this.f = true;
            try {
                if (!e92.a().a(this.a, this.b, this, 65)) {
                    Log.e("FirebaseInstanceId", "binding to the service failed");
                    this.f = false;
                    a();
                }
            } catch (SecurityException e2) {
                Log.e("FirebaseInstanceId", "Exception while binding the service", e2);
            }
        }
    }

    @DexIgnore
    public synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20);
            sb.append("onServiceConnected: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        this.f = false;
        if (!(iBinder instanceof jb4)) {
            String valueOf2 = String.valueOf(iBinder);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 28);
            sb2.append("Invalid service connection: ");
            sb2.append(valueOf2);
            Log.e("FirebaseInstanceId", sb2.toString());
            a();
            return;
        }
        this.e = (jb4) iBinder;
        b();
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        b();
    }

    @DexIgnore
    public mb4(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
        this.d = new ArrayDeque();
        this.f = false;
        this.a = context.getApplicationContext();
        this.b = new Intent(str).setPackage(this.a.getPackageName());
        this.c = scheduledExecutorService;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public /* final */ oo3<Void> b; // = new oo3<>();

        @DexIgnore
        public a(Intent intent) {
            this.a = intent;
        }

        @DexIgnore
        public void a(ScheduledExecutorService scheduledExecutorService) {
            b().a(scheduledExecutorService, new lb4(scheduledExecutorService.schedule(new kb4(this), 9000, TimeUnit.MILLISECONDS)));
        }

        @DexIgnore
        public no3<Void> b() {
            return this.b.a();
        }

        @DexIgnore
        public final /* synthetic */ void c() {
            String action = this.a.getAction();
            StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
            sb.append("Service took too long to process intent: ");
            sb.append(action);
            sb.append(" App may get closed.");
            Log.w("FirebaseInstanceId", sb.toString());
            a();
        }

        @DexIgnore
        public void a() {
            this.b.b((Void) null);
        }
    }

    @DexIgnore
    public final void a() {
        while (!this.d.isEmpty()) {
            this.d.poll().a();
        }
    }
}
