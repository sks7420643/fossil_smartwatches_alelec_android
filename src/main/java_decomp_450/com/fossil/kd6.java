package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ov3;
import com.fossil.sg6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd6 extends go5 implements jd6 {
    @DexIgnore
    public qw6<e65> f;
    @DexIgnore
    public id6 g;
    @DexIgnore
    public sg6 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ String j; // = eh5.l.a().b("nonBrandSurface");
    @DexIgnore
    public /* final */ String p; // = eh5.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String q; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.A;
            Date date = new Date();
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.A;
            Date date = new Date();
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ kd6 a;
        @DexIgnore
        public /* final */ /* synthetic */ e65 b;

        @DexIgnore
        public d(kd6 kd6, e65 e65) {
            this.a = kd6;
            this.b = e65;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            super.a(i, f, i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayFragment", "onPageScrolled " + i);
            if (!TextUtils.isEmpty(this.a.q)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepOverviewDayFragment", "set icon color " + this.a.q);
                int parseColor = Color.parseColor(this.a.q);
                TabLayout.g b4 = this.b.r.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.p) && this.a.i != i) {
                int parseColor2 = Color.parseColor(this.a.p);
                TabLayout.g b5 = this.b.r.b(this.a.i);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.i = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ kd6 a;

        @DexIgnore
        public e(kd6 kd6) {
            this.a = kd6;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.p) && !TextUtils.isEmpty(this.a.q)) {
                int parseColor = Color.parseColor(this.a.p);
                int parseColor2 = Color.parseColor(this.a.q);
                gVar.b(2131230963);
                if (i == this.a.i) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "SleepOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        e65 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onCreateView");
        e65 e65 = (e65) qb.a(layoutInflater, 2131558624, viewGroup, false, a1());
        ee7.a((Object) e65, "binding");
        a(e65);
        qw6<e65> qw6 = new qw6<>(this, e65);
        this.f = qw6;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onResume");
        id6 id6 = this.g;
        if (id6 != null) {
            id6.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onStop");
        id6 id6 = this.g;
        if (id6 != null) {
            id6.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.jd6
    public void s(List<sg6.b> list) {
        e65 a2;
        TabLayout tabLayout;
        e65 a3;
        TabLayout tabLayout2;
        e65 a4;
        FlexibleTextView flexibleTextView;
        e65 a5;
        FlexibleTextView flexibleTextView2;
        ee7.b(list, "listOfSleepSessionUIData");
        if (list.isEmpty()) {
            qw6<e65> qw6 = this.f;
            if (qw6 != null && (a5 = qw6.a()) != null && (flexibleTextView2 = a5.v) != null) {
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        qw6<e65> qw62 = this.f;
        if (!(qw62 == null || (a4 = qw62.a()) == null || (flexibleTextView = a4.v) == null)) {
            flexibleTextView.setVisibility(8);
        }
        if (list.size() > 1) {
            qw6<e65> qw63 = this.f;
            if (!(qw63 == null || (a3 = qw63.a()) == null || (tabLayout2 = a3.r) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            qw6<e65> qw64 = this.f;
            if (!(qw64 == null || (a2 = qw64.a()) == null || (tabLayout = a2.r) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        sg6 sg6 = this.h;
        if (sg6 != null) {
            sg6.a(list);
        }
    }

    @DexIgnore
    public void a(id6 id6) {
        ee7.b(id6, "presenter");
        this.g = id6;
    }

    @DexIgnore
    public final void a(e65 e65) {
        e65.s.setOnClickListener(b.a);
        String str = this.j;
        if (str != null) {
            e65.v.setBackgroundColor(Color.parseColor(str));
            e65.q.setBackgroundColor(Color.parseColor(str));
        }
        e65.t.setOnClickListener(c.a);
        this.h = new sg6(new ArrayList());
        ViewPager2 viewPager2 = e65.u;
        ee7.a((Object) viewPager2, "binding.rvSleeps");
        viewPager2.setAdapter(this.h);
        e65.u.a(new d(this, e65));
        new ov3(e65.r, e65.u, new e(this)).a();
        ViewPager2 viewPager22 = e65.u;
        ee7.a((Object) viewPager22, "binding.rvSleeps");
        viewPager22.setCurrentItem(this.i);
    }
}
