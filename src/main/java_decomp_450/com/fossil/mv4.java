package com.fossil;

import android.text.TextUtils;
import com.fossil.ac5;
import com.fossil.xb5;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutCadence;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutPace;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv4 {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<WorkoutStateChange>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<List<WorkoutGpsPoint>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<List<WorkoutStateChange>> {
    }

    /*
    static {
        new a(null);
        String simpleName = mv4.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutTypeConverter::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(WorkoutCalorie workoutCalorie) {
        if (workoutCalorie == null) {
            return null;
        }
        try {
            return new Gson().a(workoutCalorie);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutCadence b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutCadence) new Gson().a(str, WorkoutCadence.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutCadence ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutCalorie c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutCalorie) new Gson().a(str, WorkoutCalorie.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutDistance d(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutDistance) new Gson().a(str, WorkoutDistance.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutHeartRate e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutHeartRate) new Gson().a(str, WorkoutHeartRate.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ub5 f(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str != null) {
            return ub5.valueOf(str);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutPace g(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutPace) new Gson().a(str, WorkoutPace.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutPace ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final xb5 h(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        xb5.a aVar = xb5.Companion;
        if (str != null) {
            return aVar.a(str);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSpeed i(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutSpeed) new Gson().a(str, WorkoutSpeed.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final List<WorkoutStateChange> j(String str) {
        ee7.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList();
        }
        try {
            Object a2 = new Gson().a(str, new d().getType());
            ee7.a(a2, "Gson().fromJson(data, type)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final WorkoutStep k(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutStep) new Gson().a(str, WorkoutStep.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStep ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ac5 l(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        ac5.a aVar = ac5.Companion;
        if (str != null) {
            return aVar.a(str);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final String a(WorkoutDistance workoutDistance) {
        if (workoutDistance == null) {
            return null;
        }
        try {
            return new Gson().a(workoutDistance);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String b(List<WorkoutStateChange> list) {
        ee7.b(list, "workoutStateChangeList");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = new Gson().a(list, new b().getType());
            ee7.a((Object) a2, "Gson().toJson(workoutStateChangeList, type)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String a(WorkoutHeartRate workoutHeartRate) {
        if (workoutHeartRate == null) {
            return null;
        }
        try {
            return new Gson().a(workoutHeartRate);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutStep workoutStep) {
        if (workoutStep == null) {
            return null;
        }
        try {
            return new Gson().a(workoutStep);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStep ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(xb5 xb5) {
        if (xb5 != null) {
            return xb5.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final String a(ac5 ac5) {
        if (ac5 != null) {
            return ac5.name();
        }
        return null;
    }

    @DexIgnore
    public final String a(ub5 ub5) {
        if (ub5 != null) {
            return ub5.name();
        }
        return null;
    }

    @DexIgnore
    public final String a(WorkoutPace workoutPace) {
        try {
            return new Gson().a(workoutPace);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutPace ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutCadence workoutCadence) {
        try {
            return new Gson().a(workoutCadence);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutCadence ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutSpeed workoutSpeed) {
        try {
            return new Gson().a(workoutSpeed);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(List<WorkoutGpsPoint> list) {
        if (list == null) {
            return null;
        }
        try {
            return new Gson().a(list);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromGpsPoints ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final List<WorkoutGpsPoint> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (List) new Gson().a(str, new c().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toGpsPoints ex:");
            e.printStackTrace();
            sb.append(i97.a);
            local.d(str2, sb.toString());
            return null;
        }
    }
}
