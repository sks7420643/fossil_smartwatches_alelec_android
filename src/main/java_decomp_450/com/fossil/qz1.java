package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz1 extends hf2 implements pz1 {
    @DexIgnore
    public qz1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    @DexIgnore
    @Override // com.fossil.pz1
    public final void a(nz1 nz1, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel E = E();
        jf2.a(E, nz1);
        jf2.a(E, googleSignInOptions);
        a(102, E);
    }

    @DexIgnore
    @Override // com.fossil.pz1
    public final void b(nz1 nz1, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel E = E();
        jf2.a(E, nz1);
        jf2.a(E, googleSignInOptions);
        a(103, E);
    }
}
