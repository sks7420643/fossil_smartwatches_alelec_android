package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bz3<K0, V0> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends e<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        @Override // com.fossil.bz3.e
        public <K, V> Map<K, Collection<V>> b() {
            return yy3.b(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<V> implements nw3<List<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public b(int i) {
            bx3.a(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        @Override // com.fossil.nw3
        public List<V> get() {
            return new ArrayList(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<V> implements nw3<Set<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public c(int i) {
            bx3.a(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        @Override // com.fossil.nw3
        public Set<V> get() {
            return yz3.b(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K0, V0> extends bz3<K0, V0> {
        @DexIgnore
        public d() {
            super(null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> ty3<K, V> b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<K0> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends d<K0, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public a(int i) {
                this.a = i;
            }

            @DexIgnore
            @Override // com.fossil.bz3.d
            public <K extends K0, V> ty3<K, V> b() {
                return cz3.a(e.this.b(), new b(this.a));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends f<K0, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public b(int i) {
                this.a = i;
            }

            @DexIgnore
            @Override // com.fossil.bz3.f
            public <K extends K0, V> xz3<K, V> b() {
                return cz3.b(e.this.b(), new c(this.a));
            }
        }

        @DexIgnore
        public d<K0, Object> a() {
            return a(2);
        }

        @DexIgnore
        public f<K0, Object> b(int i) {
            bx3.a(i, "expectedValuesPerKey");
            return new b(i);
        }

        @DexIgnore
        public abstract <K extends K0, V> Map<K, Collection<V>> b();

        @DexIgnore
        public f<K0, Object> c() {
            return b(2);
        }

        @DexIgnore
        public d<K0, Object> a(int i) {
            bx3.a(i, "expectedValuesPerKey");
            return new a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f<K0, V0> extends bz3<K0, V0> {
        @DexIgnore
        public f() {
            super(null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> xz3<K, V> b();
    }

    @DexIgnore
    public /* synthetic */ bz3(az3 az3) {
        this();
    }

    @DexIgnore
    public static e<Object> a() {
        return a(8);
    }

    @DexIgnore
    public bz3() {
    }

    @DexIgnore
    public static e<Object> a(int i) {
        bx3.a(i, "expectedKeys");
        return new a(i);
    }
}
