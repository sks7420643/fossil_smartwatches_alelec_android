package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pd {
    @DexIgnore
    public static AtomicBoolean a; // = new AtomicBoolean(false);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jd {
        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            ce.b(activity);
        }

        @DexIgnore
        @Override // com.fossil.jd
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static void a(Context context) {
        if (!a.getAndSet(true)) {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new a());
        }
    }
}
