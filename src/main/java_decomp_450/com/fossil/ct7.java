package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;

public class ct7 {
    public static final ByteArrayBuffer e = a(et7.a, ": ");
    public static final ByteArrayBuffer f = a(et7.a, "\r\n");
    public static final ByteArrayBuffer g = a(et7.a, "--");
    public final Charset a;
    public final String b;
    public final List<at7> c;
    public final dt7 d;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /*
        static {
            /*
                com.fossil.dt7[] r0 = com.fossil.dt7.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.ct7.a.a = r0
                com.fossil.dt7 r1 = com.fossil.dt7.STRICT     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.ct7.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.dt7 r1 = com.fossil.dt7.BROWSER_COMPATIBLE     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ct7.a.<clinit>():void");
        }
        */
    }

    public ct7(String str, Charset charset, String str2, dt7 dt7) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 != null) {
            this.a = charset == null ? et7.a : charset;
            this.b = str2;
            this.c = new ArrayList();
            this.d = dt7;
        } else {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        }
    }

    public static ByteArrayBuffer a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    public String b() {
        return this.b;
    }

    public long c() {
        long j = 0;
        for (at7 at7 : this.c) {
            long contentLength = at7.a().getContentLength();
            if (contentLength < 0) {
                return -1;
            }
            j += contentLength;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.d, (OutputStream) byteArrayOutputStream, false);
            return j + ((long) byteArrayOutputStream.toByteArray().length);
        } catch (IOException unused) {
            return -1;
        }
    }

    public static void a(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) throws IOException {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    public static void a(String str, Charset charset, OutputStream outputStream) throws IOException {
        a(a(charset, str), outputStream);
    }

    public static void a(String str, OutputStream outputStream) throws IOException {
        a(a(et7.a, str), outputStream);
    }

    public static void a(ft7 ft7, OutputStream outputStream) throws IOException {
        a(ft7.b(), outputStream);
        a(e, outputStream);
        a(ft7.a(), outputStream);
        a(f, outputStream);
    }

    public static void a(ft7 ft7, Charset charset, OutputStream outputStream) throws IOException {
        a(ft7.b(), charset, outputStream);
        a(e, outputStream);
        a(ft7.a(), charset, outputStream);
        a(f, outputStream);
    }

    public List<at7> a() {
        return this.c;
    }

    public void a(at7 at7) {
        if (at7 != null) {
            this.c.add(at7);
        }
    }

    public final void a(dt7 dt7, OutputStream outputStream, boolean z) throws IOException {
        ByteArrayBuffer a2 = a(this.a, b());
        for (at7 at7 : this.c) {
            a(g, outputStream);
            a(a2, outputStream);
            a(f, outputStream);
            bt7 b2 = at7.b();
            int i = a.a[dt7.ordinal()];
            if (i == 1) {
                Iterator<ft7> it = b2.iterator();
                while (it.hasNext()) {
                    a(it.next(), outputStream);
                }
            } else if (i == 2) {
                a(at7.b().a("Content-Disposition"), this.a, outputStream);
                if (at7.a().d() != null) {
                    a(at7.b().a("Content-Type"), this.a, outputStream);
                }
            }
            a(f, outputStream);
            if (z) {
                at7.a().writeTo(outputStream);
            }
            a(f, outputStream);
        }
        a(g, outputStream);
        a(a2, outputStream);
        a(g, outputStream);
        a(f, outputStream);
    }

    public void a(OutputStream outputStream) throws IOException {
        a(this.d, outputStream, true);
    }
}
