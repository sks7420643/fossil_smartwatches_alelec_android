package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class go extends Cdo<Boolean> {
    @DexIgnore
    public static /* final */ String i; // = im.a("StorageNotLowTracker");

    @DexIgnore
    public go(Context context, vp vpVar) {
        super(context, vpVar);
    }

    @DexIgnore
    @Override // com.fossil.Cdo
    public IntentFilter d() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }

    @DexIgnore
    @Override // com.fossil.eo
    public Boolean a() {
        Intent registerReceiver = ((eo) this).b.registerReceiver(null, d());
        if (!(registerReceiver == null || registerReceiver.getAction() == null)) {
            String action = registerReceiver.getAction();
            char c = '\uffff';
            int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    c = 0;
                }
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                c = 1;
            }
            if (c != 0) {
                if (c != 1) {
                    return null;
                }
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Cdo
    public void a(Context context, Intent intent) {
        if (intent.getAction() != null) {
            im.a().a(i, String.format("Received %s", intent.getAction()), new Throwable[0]);
            String action = intent.getAction();
            char c = '\uffff';
            int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    c = 0;
                }
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                c = 1;
            }
            if (c == 0) {
                a((Object) true);
            } else if (c == 1) {
                a((Object) false);
            }
        }
    }
}
