package com.fossil;

import com.fossil.qu7;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@IgnoreJRERequirement
public final class su7 extends qu7.a {
    @DexIgnore
    public static /* final */ qu7.a a; // = new su7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class a<R> implements qu7<R, CompletableFuture<R>> {
        @DexIgnore
        public /* final */ Type a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.su7$a$a")
        /* renamed from: com.fossil.su7$a$a  reason: collision with other inner class name */
        public class C0177a extends CompletableFuture<R> {
            @DexIgnore
            public /* final */ /* synthetic */ Call a;

            @DexIgnore
            public C0177a(a aVar, Call call) {
                this.a = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.a.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements ru7<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture a;

            @DexIgnore
            public b(a aVar, CompletableFuture completableFuture) {
                this.a = completableFuture;
            }

            @DexIgnore
            @Override // com.fossil.ru7
            public void onFailure(Call<R> call, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @DexIgnore
            @Override // com.fossil.ru7
            public void onResponse(Call<R> call, fv7<R> fv7) {
                if (fv7.d()) {
                    this.a.complete(fv7.a());
                } else {
                    this.a.completeExceptionally(new vu7(fv7));
                }
            }
        }

        @DexIgnore
        public a(Type type) {
            this.a = type;
        }

        @DexIgnore
        @Override // com.fossil.qu7
        public Type a() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.qu7
        public CompletableFuture<R> a(Call<R> call) {
            C0177a aVar = new C0177a(this, call);
            call.a(new b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class b<R> implements qu7<R, CompletableFuture<fv7<R>>> {
        @DexIgnore
        public /* final */ Type a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends CompletableFuture<fv7<R>> {
            @DexIgnore
            public /* final */ /* synthetic */ Call a;

            @DexIgnore
            public a(b bVar, Call call) {
                this.a = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.a.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.su7$b$b")
        /* renamed from: com.fossil.su7$b$b  reason: collision with other inner class name */
        public class C0178b implements ru7<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture a;

            @DexIgnore
            public C0178b(b bVar, CompletableFuture completableFuture) {
                this.a = completableFuture;
            }

            @DexIgnore
            @Override // com.fossil.ru7
            public void onFailure(Call<R> call, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @DexIgnore
            @Override // com.fossil.ru7
            public void onResponse(Call<R> call, fv7<R> fv7) {
                this.a.complete(fv7);
            }
        }

        @DexIgnore
        public b(Type type) {
            this.a = type;
        }

        @DexIgnore
        @Override // com.fossil.qu7
        public Type a() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.qu7
        public CompletableFuture<fv7<R>> a(Call<R> call) {
            a aVar = new a(this, call);
            call.a(new C0178b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore
    @Override // com.fossil.qu7.a
    public qu7<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (qu7.a.a(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type a2 = qu7.a.a(0, (ParameterizedType) type);
            if (qu7.a.a(a2) != fv7.class) {
                return new a(a2);
            }
            if (a2 instanceof ParameterizedType) {
                return new b(qu7.a.a(0, (ParameterizedType) a2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
