package com.fossil;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji7 extends ak7 {
    @DexIgnore
    public static /* final */ int b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static /* final */ ji7 d; // = new ji7();
    @DexIgnore
    public static volatile Executor pool;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicInteger a;

        @DexIgnore
        public a(AtomicInteger atomicInteger) {
            this.a = atomicInteger;
        }

        @DexIgnore
        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "CommonPool-worker-" + this.a.incrementAndGet());
            thread.setDaemon(true);
            return thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void run() {
        }
    }

    /*
    static {
        String str;
        int i;
        try {
            str = System.getProperty("kotlinx.coroutines.default.parallelism");
        } catch (Throwable unused) {
            str = null;
        }
        if (str != null) {
            Integer b2 = lh7.b(str);
            if (b2 == null || b2.intValue() < 1) {
                throw new IllegalStateException(("Expected positive number in kotlinx.coroutines.default.parallelism, but has " + str).toString());
            }
            i = b2.intValue();
        } else {
            i = -1;
        }
        b = i;
    }
    */

    @DexIgnore
    public final boolean a(Class<?> cls, ExecutorService executorService) {
        executorService.submit(b.a);
        Integer num = null;
        try {
            Object invoke = cls.getMethod("getPoolSize", new Class[0]).invoke(executorService, new Object[0]);
            if (!(invoke instanceof Integer)) {
                invoke = null;
            }
            num = (Integer) invoke;
        } catch (Throwable unused) {
        }
        if (num == null || num.intValue() < 1) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }

    @DexIgnore
    public final ExecutorService g() {
        return Executors.newFixedThreadPool(n(), new a(new AtomicInteger()));
    }

    @DexIgnore
    public final ExecutorService k() {
        Class<?> cls;
        ExecutorService executorService;
        if (System.getSecurityManager() != null) {
            return g();
        }
        ExecutorService executorService2 = null;
        try {
            cls = Class.forName("java.util.concurrent.ForkJoinPool");
        } catch (Throwable unused) {
            cls = null;
        }
        if (cls == null) {
            return g();
        }
        if (!c && b < 0) {
            try {
                Method method = cls.getMethod("commonPool", new Class[0]);
                Object invoke = method != null ? method.invoke(null, new Object[0]) : null;
                if (!(invoke instanceof ExecutorService)) {
                    invoke = null;
                }
                executorService = (ExecutorService) invoke;
            } catch (Throwable unused2) {
                executorService = null;
            }
            if (executorService != null) {
                if (!d.a(cls, executorService)) {
                    executorService = null;
                }
                if (executorService != null) {
                    return executorService;
                }
            }
        }
        try {
            Object newInstance = cls.getConstructor(Integer.TYPE).newInstance(Integer.valueOf(d.n()));
            if (!(newInstance instanceof ExecutorService)) {
                newInstance = null;
            }
            executorService2 = (ExecutorService) newInstance;
        } catch (Throwable unused3) {
        }
        if (executorService2 != null) {
            return executorService2;
        }
        return g();
    }

    @DexIgnore
    public final synchronized Executor l() {
        Executor executor;
        executor = pool;
        if (executor == null) {
            executor = k();
            pool = executor;
        }
        return executor;
    }

    @DexIgnore
    public final int n() {
        Integer valueOf = Integer.valueOf(b);
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return qf7.a(Runtime.getRuntime().availableProcessors() - 1, 1);
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public String toString() {
        return "CommonPool";
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public void a(ib7 ib7, Runnable runnable) {
        Runnable runnable2;
        try {
            Executor executor = pool;
            if (executor == null) {
                executor = l();
            }
            gl7 a2 = hl7.a();
            if (a2 == null || (runnable2 = a2.a(runnable)) == null) {
                runnable2 = runnable;
            }
            executor.execute(runnable2);
        } catch (RejectedExecutionException unused) {
            gl7 a3 = hl7.a();
            if (a3 != null) {
                a3.c();
            }
            fj7.h.a(runnable);
        }
    }
}
