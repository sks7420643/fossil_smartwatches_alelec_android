package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.ep5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rf6 extends go5 implements qf6, View.OnClickListener, ep5.b, cy6.g {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public ep5 g;
    @DexIgnore
    public qw6<k15> h;
    @DexIgnore
    public pf6 i;
    @DexIgnore
    public pz6 j;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ String x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final rf6 a(Date date) {
            ee7.b(date, "date");
            rf6 rf6 = new rf6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            rf6.setArguments(bundle);
            return rf6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends pz6 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewEmptySupport e;
        @DexIgnore
        public /* final */ /* synthetic */ rf6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerViewEmptySupport recyclerViewEmptySupport, LinearLayoutManager linearLayoutManager, rf6 rf6, LinearLayoutManager linearLayoutManager2, k15 k15) {
            super(linearLayoutManager);
            this.e = recyclerViewEmptySupport;
            this.f = rf6;
        }

        @DexIgnore
        @Override // com.fossil.pz6
        public void a(int i) {
            pf6 a = this.f.i;
            if (a != null) {
                a.i();
            }
        }

        @DexIgnore
        @Override // com.fossil.pz6
        public void a(int i, int i2) {
        }
    }

    @DexIgnore
    public rf6() {
        String b2 = eh5.l.a().b("nonBrandSurface");
        String str = "#FFFFFF";
        this.s = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("secondaryText");
        this.t = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("primaryText");
        this.u = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("nonBrandDisableCalendarDay");
        this.v = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("nonBrandNonReachGoal");
        this.w = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandSurface");
        this.x = b7 != null ? b7 : str;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.qf6
    public void c(qf<GoalTrackingData> qfVar) {
        ep5 ep5;
        ee7.b(qfVar, "goalTrackingDataList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailData - goalTrackingDataList=" + qfVar);
        qw6<k15> qw6 = this.h;
        if (qw6 != null && qw6.a() != null && (ep5 = this.g) != null) {
            ep5.b(qfVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.qf6
    public void d() {
        pz6 pz6 = this.j;
        if (pz6 != null) {
            pz6.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "GoalTrackingDetailFragment";
    }

    @DexIgnore
    public final void f1() {
        k15 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        qw6<k15> qw6 = this.h;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayGoalChart = a2.y) != null) {
            overviewDayGoalChart.a("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int i2;
        int i3;
        int i4;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("GoalTrackingDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131361881:
                    Boolean w2 = zd5.w(this.f);
                    ee7.a((Object) w2, "DateHelper.isToday(mDate)");
                    if (w2.booleanValue()) {
                        Calendar instance = Calendar.getInstance();
                        int i5 = instance.get(10);
                        int i6 = instance.get(12);
                        i2 = instance.get(9);
                        if (i5 == 0) {
                            i3 = i6;
                            i4 = 12;
                        } else {
                            i4 = i5;
                            i3 = i6;
                        }
                    } else {
                        i4 = 12;
                        i3 = 0;
                        i2 = 0;
                    }
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    String[] strArr = new String[2];
                    String a2 = ig5.a(PortfolioApp.g0.c(), 2131886102);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    if (a2 != null) {
                        String upperCase = a2.toUpperCase();
                        ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        strArr[0] = upperCase;
                        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886104);
                        ee7.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                        if (a3 != null) {
                            String upperCase2 = a3.toUpperCase();
                            ee7.a((Object) upperCase2, "(this as java.lang.String).toUpperCase()");
                            strArr[1] = upperCase2;
                            bx6.a(childFragmentManager, i4, i3, i2, strArr);
                            return;
                        }
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                case 2131362636:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362637:
                    pf6 pf6 = this.i;
                    if (pf6 != null) {
                        pf6.k();
                        return;
                    }
                    return;
                case 2131362705:
                    pf6 pf62 = this.i;
                    if (pf62 != null) {
                        pf62.j();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        k15 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        k15 k15 = (k15) qb.a(layoutInflater, 2131558557, viewGroup, false, a1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.f = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.f = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        this.g = new ep5(this, new kl4());
        ee7.a((Object) k15, "binding");
        a(k15);
        pf6 pf6 = this.i;
        if (pf6 != null) {
            pf6.b(this.f);
        }
        this.h = new qw6<>(this, k15);
        f1();
        qw6<k15> qw6 = this.h;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        pf6 pf6 = this.i;
        if (pf6 != null) {
            pf6.h();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        pf6 pf6 = this.i;
        if (pf6 != null) {
            pf6.g();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f1();
        pf6 pf6 = this.i;
        if (pf6 != null) {
            pf6.c(this.f);
        }
        pf6 pf62 = this.i;
        if (pf62 != null) {
            pf62.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        pf6 pf6 = this.i;
        if (pf6 != null) {
            pf6.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public final void a(k15 k15) {
        k15.F.setOnClickListener(this);
        k15.G.setOnClickListener(this);
        k15.H.setOnClickListener(this);
        if (!TextUtils.isEmpty(this.x)) {
            int parseColor = Color.parseColor(this.x);
            Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230982);
            if (drawable != null) {
                drawable.setTint(parseColor);
                k15.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        k15.q.setOnClickListener(this);
        this.p = eh5.l.a().b("hybridGoalTrackingTab");
        this.q = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.r = eh5.l.a().b("onHybridGoalTrackingTab");
        k15.r.setBackgroundColor(this.s);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        RecyclerViewEmptySupport recyclerViewEmptySupport = k15.N;
        ee7.a((Object) recyclerViewEmptySupport, "it");
        recyclerViewEmptySupport.setLayoutManager(linearLayoutManager);
        RecyclerView.m layoutManager = recyclerViewEmptySupport.getLayoutManager();
        if (layoutManager != null) {
            this.j = new b(recyclerViewEmptySupport, (LinearLayoutManager) layoutManager, this, linearLayoutManager, k15);
            recyclerViewEmptySupport.setAdapter(this.g);
            FlexibleTextView flexibleTextView = k15.O;
            ee7.a((Object) flexibleTextView, "binding.tvNotFound");
            recyclerViewEmptySupport.setEmptyView(flexibleTextView);
            pz6 pz6 = this.j;
            if (pz6 != null) {
                recyclerViewEmptySupport.addOnScrollListener(pz6);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        GoalTrackingData goalTrackingData;
        pf6 pf6;
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        Bundle bundle = null;
        Serializable serializable = null;
        if (hashCode != 193266439) {
            if (hashCode == 1983205541 && str.equals("GOAL_TRACKING_ADD") && i2 == 2131362243) {
                if (intent != null) {
                    serializable = intent.getSerializableExtra("EXTRA_NUMBER_PICKER_RESULTS");
                }
                if (serializable != null) {
                    HashMap hashMap = (HashMap) serializable;
                    Integer num = (Integer) hashMap.get(2131362853);
                    Integer num2 = (Integer) hashMap.get(2131362854);
                    Integer num3 = (Integer) hashMap.get(2131362857);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("GoalTrackingDetailFragment", "onDialogFragmentResult GOAL_TRACKING_ADD: hour=" + num + ", minute=" + num2 + ", suffix=" + num3);
                    Calendar instance = Calendar.getInstance();
                    ee7.a((Object) instance, "calendar");
                    instance.setTime(this.f);
                    if (num != null && num2 != null && num3 != null) {
                        if (num.intValue() == 12) {
                            num = 0;
                        }
                        instance.set(9, num3.intValue());
                        instance.set(10, num.intValue());
                        instance.set(12, num2.intValue());
                        Calendar instance2 = Calendar.getInstance();
                        ee7.a((Object) instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        ee7.a((Object) time, "Calendar.getInstance().time");
                        long time2 = time.getTime();
                        Date time3 = instance.getTime();
                        ee7.a((Object) time3, "calendar.time");
                        if (time2 > time3.getTime()) {
                            pf6 pf62 = this.i;
                            if (pf62 != null) {
                                Date time4 = instance.getTime();
                                ee7.a((Object) time4, "calendar.time");
                                pf62.a(time4);
                                return;
                            }
                            return;
                        }
                        FragmentManager fragmentManager = getFragmentManager();
                        if (fragmentManager != null) {
                            bx6 bx6 = bx6.c;
                            ee7.a((Object) fragmentManager, "this");
                            bx6.p(fragmentManager);
                            return;
                        }
                        return;
                    }
                    return;
                }
                throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Int, kotlin.Int>");
            }
        } else if (str.equals("GOAL_TRACKING_DELETE") && i2 == 2131363307) {
            if (intent != null) {
                bundle = intent.getExtras();
            }
            if (bundle != null && (goalTrackingData = (GoalTrackingData) bundle.getSerializable("GOAL_TRACKING_DELETE_BUNDLE")) != null && (pf6 = this.i) != null) {
                pf6.a(goalTrackingData);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ep5.b
    public void a(GoalTrackingData goalTrackingData) {
        ee7.b(goalTrackingData, "item");
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.a(childFragmentManager, goalTrackingData);
    }

    @DexIgnore
    public void a(pf6 pf6) {
        ee7.b(pf6, "presenter");
        this.i = pf6;
    }

    @DexIgnore
    @Override // com.fossil.qf6
    public void a(Date date, boolean z2, boolean z3, boolean z4) {
        k15 a2;
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4);
        this.f = date;
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        int i2 = instance.get(7);
        qw6<k15> qw6 = this.h;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            FlexibleTextView flexibleTextView = a2.B;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(instance.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.G;
                ee7.a((Object) rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.G;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.H;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(ig5.a(getContext(), 2131886616));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.C;
                ee7.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(xe5.b.b(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.H;
            ee7.a((Object) rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.C;
            ee7.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(xe5.b.b(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.qf6
    public void a(GoalTrackingSummary goalTrackingSummary) {
        k15 a2;
        int i2;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetail - goalTrackingSummary=" + goalTrackingSummary);
        qw6<k15> qw6 = this.h;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            ee7.a((Object) a2, "binding");
            View d = a2.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (goalTrackingSummary != null) {
                i2 = goalTrackingSummary.getTotalTracked();
                i3 = goalTrackingSummary.getGoalTarget();
            } else {
                i3 = 0;
                i2 = 0;
            }
            FlexibleTextView flexibleTextView = a2.A;
            ee7.a((Object) flexibleTextView, "binding.ftvDailyValue");
            flexibleTextView.setText(re5.b((float) i2, 1));
            FlexibleTextView flexibleTextView2 = a2.z;
            ee7.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
            flexibleTextView2.setText("/ " + i3 + " " + " " + ig5.a(PortfolioApp.g0.c(), 2131886694));
            int i4 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.C.setTextColor(this.s);
                a2.B.setTextColor(this.s);
                a2.z.setTextColor(this.s);
                a2.A.setTextColor(this.s);
                RTLImageView rTLImageView = a2.H;
                ee7.a((Object) rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.G;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                ee7.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView3 = a2.C;
                ee7.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setSelected(true);
                FlexibleTextView flexibleTextView4 = a2.B;
                ee7.a((Object) flexibleTextView4, "binding.ftvDayOfMonth");
                flexibleTextView4.setSelected(true);
                View view = a2.J;
                ee7.a((Object) view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView5 = a2.A;
                ee7.a((Object) flexibleTextView5, "binding.ftvDailyValue");
                flexibleTextView5.setSelected(true);
                FlexibleTextView flexibleTextView6 = a2.z;
                ee7.a((Object) flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.O;
                ee7.a((Object) flexibleTextView7, "binding.tvNotFound");
                flexibleTextView7.setVisibility(4);
                String str = this.r;
                if (str != null) {
                    a2.C.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.J.setBackgroundColor(Color.parseColor(str));
                    a2.H.setColorFilter(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.p;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.B.setTextColor(this.u);
                a2.C.setTextColor(this.t);
                a2.z.setTextColor(this.w);
                View view2 = a2.J;
                ee7.a((Object) view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.H;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.G;
                ee7.a((Object) rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                a2.A.setTextColor(this.u);
                FlexibleTextView flexibleTextView8 = a2.O;
                ee7.a((Object) flexibleTextView8, "binding.tvNotFound");
                flexibleTextView8.setVisibility(4);
                int i5 = this.w;
                a2.J.setBackgroundColor(i5);
                a2.H.setColorFilter(i5);
                a2.G.setColorFilter(i5);
                String str3 = this.q;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.B.setTextColor(this.u);
                a2.C.setTextColor(this.t);
                a2.A.setTextColor(this.u);
                a2.z.setTextColor(this.v);
                RTLImageView rTLImageView5 = a2.H;
                ee7.a((Object) rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.G;
                ee7.a((Object) rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                ee7.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView9 = a2.C;
                ee7.a((Object) flexibleTextView9, "binding.ftvDayOfWeek");
                flexibleTextView9.setSelected(false);
                FlexibleTextView flexibleTextView10 = a2.B;
                ee7.a((Object) flexibleTextView10, "binding.ftvDayOfMonth");
                flexibleTextView10.setSelected(false);
                View view3 = a2.J;
                ee7.a((Object) view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView11 = a2.A;
                ee7.a((Object) flexibleTextView11, "binding.ftvDailyValue");
                flexibleTextView11.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.z;
                ee7.a((Object) flexibleTextView12, "binding.ftvDailyUnit");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.O;
                ee7.a((Object) flexibleTextView13, "binding.tvNotFound");
                flexibleTextView13.setVisibility(0);
                int i6 = this.w;
                a2.J.setBackgroundColor(i6);
                a2.H.setColorFilter(i6);
                a2.G.setColorFilter(i6);
                String str4 = this.q;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.L;
                ee7.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView14 = a2.E;
                ee7.a((Object) flexibleTextView14, "binding.ftvProgressValue");
                flexibleTextView14.setText(ig5.a(context, 2131887288));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.L;
                ee7.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView15 = a2.E;
                ee7.a((Object) flexibleTextView15, "binding.ftvProgressValue");
                flexibleTextView15.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView16 = a2.D;
            ee7.a((Object) flexibleTextView16, "binding.ftvGoalValue");
            we7 we7 = we7.a;
            String a3 = ig5.a(context, 2131886696);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026age_Title__OfNumberTimes)");
            String format = String.format(a3, Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView16.setText(format);
        }
    }

    @DexIgnore
    @Override // com.fossil.qf6
    public void a(do5 do5, ArrayList<String> arrayList) {
        k15 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailChart - baseModel=" + do5);
        qw6<k15> qw6 = this.h;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayGoalChart = a2.y) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayGoalChart.a(do5);
        }
    }
}
