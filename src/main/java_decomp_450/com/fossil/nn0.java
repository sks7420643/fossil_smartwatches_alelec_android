package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn0 extends rr1 {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ nn0(short s, ri1 ri1, int i, int i2) {
        super(ln0.f, s, qa1.n, ri1, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = yz0.b(order.getInt(0));
            this.N = yz0.b(order.getInt(4));
            yz0.a(yz0.a(jSONObject, r51.G0, Long.valueOf(this.M)), r51.H0, Long.valueOf(this.N));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(yz0.a(super.h(), r51.G0, Long.valueOf(this.M)), r51.H0, Long.valueOf(this.N));
    }
}
