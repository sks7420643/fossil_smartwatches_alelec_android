package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.ui.debug.DebugActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il5 implements MembersInjector<DebugActivity> {
    @DexIgnore
    public static void a(DebugActivity debugActivity, ch5 ch5) {
        debugActivity.y = ch5;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, om5 om5) {
        debugActivity.z = om5;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, FirmwareFileRepository firmwareFileRepository) {
        debugActivity.A = firmwareFileRepository;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, GuestApiService guestApiService) {
        debugActivity.B = guestApiService;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, DianaPresetRepository dianaPresetRepository) {
        debugActivity.C = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, sj5 sj5) {
        debugActivity.D = sj5;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, ad5 ad5) {
        debugActivity.E = ad5;
    }
}
