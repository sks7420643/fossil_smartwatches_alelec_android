package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r14 implements f24 {
    @DexIgnore
    public static /* final */ f24 a; // = new r14();

    @DexIgnore
    @Override // com.fossil.f24
    public final Object a(d24 d24) {
        return p14.a((l14) d24.get(l14.class), (Context) d24.get(Context.class), (i94) d24.get(i94.class));
    }
}
