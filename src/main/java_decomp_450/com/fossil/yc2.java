package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yc2 implements Parcelable.Creator<gc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ gc2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        DataType dataType = null;
        String str = null;
        hc2 hc2 = null;
        uc2 uc2 = null;
        String str2 = null;
        int[] iArr = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    dataType = (DataType) j72.a(parcel, a, DataType.CREATOR);
                    break;
                case 2:
                    str = j72.e(parcel, a);
                    break;
                case 3:
                    i = j72.q(parcel, a);
                    break;
                case 4:
                    hc2 = (hc2) j72.a(parcel, a, hc2.CREATOR);
                    break;
                case 5:
                    uc2 = (uc2) j72.a(parcel, a, uc2.CREATOR);
                    break;
                case 6:
                    str2 = j72.e(parcel, a);
                    break;
                case 7:
                default:
                    j72.v(parcel, a);
                    break;
                case 8:
                    iArr = j72.d(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new gc2(dataType, str, i, hc2, uc2, str2, iArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ gc2[] newArray(int i) {
        return new gc2[i];
    }
}
