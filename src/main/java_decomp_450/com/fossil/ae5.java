package com.fossil;

import com.fossil.be5;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ae5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[be5.b.values().length];
        a = iArr;
        iArr[be5.b.SMALL.ordinal()] = 1;
        a[be5.b.NORMAL.ordinal()] = 2;
        a[be5.b.LARGE.ordinal()] = 3;
        a[be5.b.WATCH_COMPLETED.ordinal()] = 4;
        a[be5.b.HYBRID_WATCH_HOUR.ordinal()] = 5;
        a[be5.b.HYBRID_WATCH_MINUTE.ordinal()] = 6;
        a[be5.b.HYBRID_WATCH_SUBEYE.ordinal()] = 7;
        a[be5.b.DIANA_WATCH_HOUR.ordinal()] = 8;
        a[be5.b.DIANA_WATCH_MINUTE.ordinal()] = 9;
        int[] iArr2 = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
        b = iArr2;
        iArr2[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        b[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
        b[FossilDeviceSerialPatternUtil.DEVICE.IVY.ordinal()] = 5;
    }
    */
}
