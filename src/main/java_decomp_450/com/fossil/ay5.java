package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.fossil.dy5;
import com.fossil.ql4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay5 extends ux5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ List<at5> e; // = new ArrayList();
    @DexIgnore
    public /* final */ vx5 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<String> h;
    @DexIgnore
    public /* final */ rl4 i;
    @DexIgnore
    public /* final */ dy5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ay5.k;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<at5, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ay5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ay5 ay5) {
            super(1);
            this.this$0 = ay5;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(at5 at5) {
            return Boolean.valueOf(invoke(at5));
        }

        @DexIgnore
        public final boolean invoke(at5 at5) {
            ee7.b(at5, "it");
            InstalledApp installedApp = at5.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected != null) {
                return isSelected.booleanValue() && at5.getCurrentHandGroup() == this.this$0.g;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<at5, String> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public final String invoke(at5 at5) {
            ee7.b(at5, "it");
            return String.valueOf(at5.getUri());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ql4.d<dy5.b, ql4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ ay5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(ay5 ay5) {
            this.a = ay5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dy5.b bVar) {
            InstalledApp installedApp;
            ee7.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(ay5.l.a(), "mGetApps onSuccess");
            ArrayList<at5> arrayList = new ArrayList(bVar.a());
            for (at5 at5 : arrayList) {
                if (at5.getUri() != null) {
                    if (this.a.h.contains(String.valueOf(at5.getUri()))) {
                        InstalledApp installedApp2 = at5.getInstalledApp();
                        if (installedApp2 != null) {
                            installedApp2.setSelected(true);
                        }
                        at5.setCurrentHandGroup(this.a.g);
                    } else if (at5.getCurrentHandGroup() == this.a.g && (installedApp = at5.getInstalledApp()) != null) {
                        installedApp.setSelected(false);
                    }
                }
            }
            this.a.j().addAll(arrayList);
            this.a.f.a(this.a.j(), this.a.g);
            this.a.f.h();
        }

        @DexIgnore
        public void a(ql4.a aVar) {
            ee7.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(ay5.l.a(), "mGetApps onError");
            this.a.f.h();
        }
    }

    /*
    static {
        String simpleName = ay5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationHybridAppPre\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public ay5(vx5 vx5, int i2, ArrayList<String> arrayList, rl4 rl4, dy5 dy5) {
        ee7.b(vx5, "mView");
        ee7.b(arrayList, "mListAppsSelected");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(dy5, "mGetHybridApp");
        this.f = vx5;
        this.g = i2;
        this.h = arrayList;
        this.i = rl4;
        this.j = dy5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (this.e.isEmpty()) {
            this.f.j();
            this.i.a(this.j, null, new d(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.ux5
    public int h() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ux5
    public void i() {
        this.f.a(new ArrayList<>(og7.f(og7.c(og7.a(ea7.b((Iterable) this.e), new b(this)), c.INSTANCE))));
    }

    @DexIgnore
    public final List<at5> j() {
        return this.e;
    }

    @DexIgnore
    public void k() {
        this.f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.ux5
    public void a(at5 at5, boolean z) {
        T t;
        int i2;
        ee7.b(at5, "appWrapper");
        FLogger.INSTANCE.getLocal().d(k, "setAppState: appWrapper=" + at5 + ", selected=" + z);
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a(t.getUri(), at5.getUri())) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            InstalledApp installedApp = t2.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(z);
            }
            InstalledApp installedApp2 = t2.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected == null) {
                ee7.a();
                throw null;
            } else if (isSelected.booleanValue() && t2.getCurrentHandGroup() != (i2 = this.g)) {
                t2.setCurrentHandGroup(i2);
            }
        }
    }
}
