package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pa2 extends na2 {
    @DexIgnore
    public static /* final */ WeakReference<byte[]> c; // = new WeakReference<>(null);
    @DexIgnore
    public WeakReference<byte[]> b; // = c;

    @DexIgnore
    public pa2(byte[] bArr) {
        super(bArr);
    }

    @DexIgnore
    @Override // com.fossil.na2
    public final byte[] E() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.b.get();
            if (bArr == null) {
                bArr = F();
                this.b = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    @DexIgnore
    public abstract byte[] F();
}
