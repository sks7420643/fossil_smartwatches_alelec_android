package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dx6<T> {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public dx6(T t) {
        this.b = t;
    }

    @DexIgnore
    public final T a() {
        if (this.a) {
            return null;
        }
        this.a = true;
        return this.b;
    }
}
