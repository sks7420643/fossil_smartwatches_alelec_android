package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.wp5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt5 extends ho5 implements ft5, cy6.g {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public et5 g;
    @DexIgnore
    public qw6<q35> h;
    @DexIgnore
    public ou5 i;
    @DexIgnore
    public wp5 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gt5.q;
        }

        @DexIgnore
        public final gt5 b() {
            return new gt5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements wp5.b {
        @DexIgnore
        public /* final */ /* synthetic */ gt5 a;

        @DexIgnore
        public b(gt5 gt5) {
            this.a = gt5;
        }

        @DexIgnore
        @Override // com.fossil.wp5.b
        public void a(at5 at5, boolean z) {
            ee7.b(at5, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = gt5.r.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = at5.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            gt5.b(this.a).a(at5, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gt5 a;

        @DexIgnore
        public c(gt5 gt5) {
            this.a = gt5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(gt5.r.a(), "press on close button");
            gt5.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ gt5 a;

        @DexIgnore
        public d(gt5 gt5) {
            this.a = gt5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            gt5.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ gt5 a;
        @DexIgnore
        public /* final */ /* synthetic */ q35 b;

        @DexIgnore
        public e(gt5 gt5, q35 q35) {
            this.a = gt5;
            this.b = q35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.b.u;
            ee7.a((Object) rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 == 0 ? 4 : 0);
            gt5.a(this.a).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ q35 a;

        @DexIgnore
        public f(q35 q35) {
            this.a = q35;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                q35 q35 = this.a;
                ee7.a((Object) q35, "binding");
                q35.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q35 a;

        @DexIgnore
        public g(q35 q35) {
            this.a = q35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.r.setText("");
        }
    }

    /*
    static {
        String simpleName = gt5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationAppsFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ wp5 a(gt5 gt5) {
        wp5 wp5 = gt5.j;
        if (wp5 != null) {
            return wp5;
        }
        ee7.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ et5 b(gt5 gt5) {
        et5 et5 = gt5.g;
        if (et5 != null) {
            return et5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ft5
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ft5
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d(q, "onActivityBackPressed -");
        et5 et5 = this.g;
        if (et5 != null) {
            et5.h();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ft5
    public void j(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<q35> qw6 = this.h;
        if (qw6 != null) {
            q35 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ft5
    public void k(List<at5> list) {
        ee7.b(list, "listAppWrapper");
        wp5 wp5 = this.j;
        if (wp5 != null) {
            wp5.a(list);
        } else {
            ee7.d("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ft5
    public void l() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            Context requireContext = requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            TroubleshootingActivity.a.a(aVar, requireContext, PortfolioApp.g0.c().c(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        q35 q35 = (q35) qb.a(layoutInflater, 2131558587, viewGroup, false, a1());
        q35.v.setOnClickListener(new c(this));
        q35.y.setOnCheckedChangeListener(new d(this));
        q35.r.addTextChangedListener(new e(this, q35));
        q35.r.setOnFocusChangeListener(new f(q35));
        q35.u.setOnClickListener(new g(q35));
        wp5 wp5 = new wp5();
        wp5.a(new b(this));
        this.j = wp5;
        ou5 ou5 = (ou5) getChildFragmentManager().b(ou5.v.a());
        this.i = ou5;
        if (ou5 == null) {
            this.i = ou5.v.b();
        }
        RecyclerView recyclerView = q35.x;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        wp5 wp52 = this.j;
        if (wp52 != null) {
            recyclerView.setAdapter(wp52);
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                ((mg) itemAnimator).setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                tj4 f2 = PortfolioApp.g0.c().f();
                ou5 ou52 = this.i;
                if (ou52 != null) {
                    f2.a(new qu5(ou52)).a(this);
                    this.h = new qw6<>(this, q35);
                    V("app_notification_view");
                    ee7.a((Object) q35, "binding");
                    return q35.d();
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        ee7.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        et5 et5 = this.g;
        if (et5 != null) {
            et5.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
            }
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        et5 et5 = this.g;
        if (et5 != null) {
            et5.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(et5 et5) {
        ee7.b(et5, "presenter");
        this.g = et5;
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363307) {
                et5 et5 = this.g;
                if (et5 != null) {
                    et5.i();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i2 == 2131362268) {
                et5 et52 = this.g;
                if (et52 != null) {
                    et52.h();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (i2 != 2131362359) {
                if (i2 == 2131362656) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.z;
                FragmentActivity requireActivity = requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }
}
