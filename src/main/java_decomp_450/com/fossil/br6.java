package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentActivity;
import com.fossil.gx6;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br6 extends mr6 implements sr6 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public qw6<w45> g;
    @DexIgnore
    public rr6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final br6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            br6 br6 = new br6();
            br6.setArguments(bundle);
            return br6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity a;

        @DexIgnore
        public b(FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.a, 2130772009);
            FlexibleTextView flexibleTextView = (FlexibleTextView) this.a.findViewById(2131362517);
            if (flexibleTextView != null) {
                flexibleTextView.startAnimation(loadAnimation);
            }
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) this.a.findViewById(2131362377);
            if (flexibleTextView2 != null) {
                flexibleTextView2.startAnimation(loadAnimation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ br6 a;

        @DexIgnore
        public c(br6 br6) {
            this.a = br6;
        }

        @DexIgnore
        public final void onClick(View view) {
            br6.a(this.a).a(1);
        }
    }

    @DexIgnore
    public static final /* synthetic */ rr6 a(br6 br6) {
        rr6 rr6 = br6.h;
        if (rr6 != null) {
            return rr6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        FragmentActivity activity;
        if (z || (activity = getActivity()) == null) {
            return null;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(activity, 2130772005);
        if (loadAnimation == null) {
            return loadAnimation;
        }
        loadAnimation.setAnimationListener(new b(activity));
        return loadAnimation;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        w45 w45 = (w45) qb.a(layoutInflater, 2131558604, viewGroup, false, a1());
        this.g = new qw6<>(this, w45);
        ee7.a((Object) w45, "binding");
        return w45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        RTLImageView rTLImageView;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<w45> qw6 = this.g;
        if (qw6 != null) {
            w45 a2 = qw6.a();
            if (!(a2 == null || (rTLImageView = a2.s) == null)) {
                rTLImageView.setOnClickListener(new c(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                qw6<w45> qw62 = this.g;
                if (qw62 != null) {
                    w45 a3 = qw62.a();
                    if (a3 != null && (dashBar = a3.u) != null) {
                        gx6.a aVar = gx6.a;
                        ee7.a((Object) dashBar, "this");
                        aVar.d(dashBar, z, 500);
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(rr6 rr6) {
        ee7.b(rr6, "presenter");
        this.h = rr6;
    }
}
