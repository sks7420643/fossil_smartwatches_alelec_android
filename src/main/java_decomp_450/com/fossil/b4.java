package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b4 {
    @DexIgnore
    public static /* final */ int cardBackgroundColor; // = 2130968874;
    @DexIgnore
    public static /* final */ int cardCornerRadius; // = 2130968875;
    @DexIgnore
    public static /* final */ int cardElevation; // = 2130968876;
    @DexIgnore
    public static /* final */ int cardMaxElevation; // = 2130968878;
    @DexIgnore
    public static /* final */ int cardPreventCornerOverlap; // = 2130968879;
    @DexIgnore
    public static /* final */ int cardUseCompatPadding; // = 2130968880;
    @DexIgnore
    public static /* final */ int cardViewStyle; // = 2130968881;
    @DexIgnore
    public static /* final */ int contentPadding; // = 2130968985;
    @DexIgnore
    public static /* final */ int contentPaddingBottom; // = 2130968986;
    @DexIgnore
    public static /* final */ int contentPaddingLeft; // = 2130968987;
    @DexIgnore
    public static /* final */ int contentPaddingRight; // = 2130968988;
    @DexIgnore
    public static /* final */ int contentPaddingTop; // = 2130968989;
}
