package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m83 extends tm2 implements l83 {
    @DexIgnore
    public m83(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    @DexIgnore
    @Override // com.fossil.l83
    public final y63 a(ab2 ab2, GoogleMapOptions googleMapOptions) throws RemoteException {
        y63 y63;
        Parcel zza = zza();
        xm2.a(zza, ab2);
        xm2.a(zza, googleMapOptions);
        Parcel a = a(3, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            y63 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
            if (queryLocalInterface instanceof y63) {
                y63 = (y63) queryLocalInterface;
            } else {
                y63 = new p83(readStrongBinder);
            }
        }
        a.recycle();
        return y63;
    }

    @DexIgnore
    @Override // com.fossil.l83
    public final x63 f(ab2 ab2) throws RemoteException {
        x63 x63;
        Parcel zza = zza();
        xm2.a(zza, ab2);
        Parcel a = a(2, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            x63 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapFragmentDelegate");
            if (queryLocalInterface instanceof x63) {
                x63 = (x63) queryLocalInterface;
            } else {
                x63 = new o83(readStrongBinder);
            }
        }
        a.recycle();
        return x63;
    }

    @DexIgnore
    @Override // com.fossil.l83
    public final v63 zze() throws RemoteException {
        v63 v63;
        Parcel a = a(4, zza());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            v63 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
            if (queryLocalInterface instanceof v63) {
                v63 = (v63) queryLocalInterface;
            } else {
                v63 = new u73(readStrongBinder);
            }
        }
        a.recycle();
        return v63;
    }

    @DexIgnore
    @Override // com.fossil.l83
    public final ym2 zzf() throws RemoteException {
        Parcel a = a(5, zza());
        ym2 a2 = zm2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.l83
    public final void a(ab2 ab2, int i) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        zza.writeInt(i);
        b(6, zza);
    }

    @DexIgnore
    @Override // com.fossil.l83
    public final b73 a(ab2 ab2, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException {
        b73 b73;
        Parcel zza = zza();
        xm2.a(zza, ab2);
        xm2.a(zza, streetViewPanoramaOptions);
        Parcel a = a(7, zza);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            b73 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
            if (queryLocalInterface instanceof b73) {
                b73 = (b73) queryLocalInterface;
            } else {
                b73 = new f83(readStrongBinder);
            }
        }
        a.recycle();
        return b73;
    }
}
