package com.fossil;

import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd1 extends fe7 implements gd7<i97, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ xp0 a;
    @DexIgnore
    public /* final */ /* synthetic */ km1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kd1(xp0 xp0, km1 km1) {
        super(1);
        this.a = xp0;
        this.b = km1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(i97 i97) {
        ov0[] ov0Arr = ((bx0) this.a).d;
        for (ov0 ov0 : ov0Arr) {
            pb1 a2 = pb1.z.a(ov0.b);
            if (a2 != null) {
                switch (w91.a[a2.ordinal()]) {
                    case 1:
                        if (ov0.a != yr0.GET) {
                            break;
                        } else {
                            km1.b(this.b, false, true, null, 4);
                            continue;
                        }
                    case 2:
                        km1 km1 = this.b;
                        eb0 eb0 = new eb0(this.a.b, ov0.a);
                        l60.b A = km1.A();
                        if (A != null) {
                            A.onEventReceived(km1, eb0);
                            break;
                        } else {
                            continue;
                        }
                    case 3:
                        if (ov0.a == yr0.GET) {
                            this.b.y();
                            break;
                        } else {
                            continue;
                        }
                    case 4:
                        km1 km12 = this.b;
                        nb0 nb0 = new nb0(this.a.b, ov0.a);
                        l60.b A2 = km12.A();
                        if (A2 != null) {
                            A2.onEventReceived(km12, nb0);
                            break;
                        } else {
                            continue;
                        }
                    case 5:
                        km1 km13 = this.b;
                        jb0 jb0 = new jb0(this.a.b, ov0.a);
                        l60.b A3 = km13.A();
                        if (A3 != null) {
                            A3.onEventReceived(km13, jb0);
                            break;
                        } else {
                            continue;
                        }
                    case 6:
                        if (ov0.a == yr0.GET) {
                            km1.a(this.b, false, true, (a61) null, 4);
                            break;
                        } else {
                            continue;
                        }
                }
            }
        }
        return i97.a;
    }
}
