package com.fossil;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ql4;
import com.fossil.qy5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ny5 extends gy5 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public /* final */ List<bt5> e; // = new ArrayList();
    @DexIgnore
    public /* final */ hy5 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<bt5> h;
    @DexIgnore
    public /* final */ LoaderManager i;
    @DexIgnore
    public /* final */ rl4 j;
    @DexIgnore
    public /* final */ qy5 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ny5.l;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1", f = "NotificationHybridContactPresenter.kt", l = {47}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ny5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1", f = "NotificationHybridContactPresenter.kt", l = {48}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ny5$b$b")
        /* renamed from: com.fossil.ny5$b$b  reason: collision with other inner class name */
        public static final class C0144b implements ql4.d<qy5.d, qy5.b> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ny5$b$b$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1", f = "NotificationHybridContactPresenter.kt", l = {99}, m = "invokeSuspend")
            /* renamed from: com.fossil.ny5$b$b$a */
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ qy5.d $successResponse;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0144b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ny5$b$b$a$a")
                @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1", f = "NotificationHybridContactPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.ny5$b$b$a$a  reason: collision with other inner class name */
                public static final class C0145a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0145a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0145a aVar = new C0145a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0145a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            for (T t : this.this$0.$successResponse.a()) {
                                for (Contact contact : t.getContacts()) {
                                    bt5 bt5 = new bt5(contact, null, 2, null);
                                    bt5.setAdded(true);
                                    Contact contact2 = bt5.getContact();
                                    if (contact2 != null) {
                                        ee7.a((Object) contact, "contact");
                                        contact2.setDbRowId(contact.getDbRowId());
                                        contact2.setUseSms(contact.isUseSms());
                                        contact2.setUseCall(contact.isUseCall());
                                    }
                                    bt5.setCurrentHandGroup(t.getHour());
                                    ee7.a((Object) contact, "contact");
                                    List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                                    ee7.a((Object) phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                        ee7.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                                        String number = phoneNumber.getNumber();
                                        if (!TextUtils.isEmpty(number)) {
                                            bt5.setHasPhoneNumber(true);
                                            bt5.setPhoneNumber(number);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a = ny5.m.a();
                                            local.d(a, " filter selected contact, phoneNumber=" + number);
                                        }
                                    }
                                    Iterator it = this.this$0.this$0.a.this$0.h.iterator();
                                    int i = 0;
                                    while (true) {
                                        if (!it.hasNext()) {
                                            i = -1;
                                            break;
                                        }
                                        Contact contact3 = ((bt5) it.next()).getContact();
                                        if (pb7.a(contact3 != null && contact3.getContactId() == contact.getContactId()).booleanValue()) {
                                            break;
                                        }
                                        i++;
                                    }
                                    if (i != -1) {
                                        bt5.setCurrentHandGroup(this.this$0.this$0.a.this$0.g);
                                        ee7.a(this.this$0.this$0.a.this$0.h.remove(i), "mContactWrappersSelected\u2026moveAt(indexContactFound)");
                                    } else if (bt5.getCurrentHandGroup() == this.this$0.this$0.a.this$0.g) {
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a2 = ny5.m.a();
                                    local2.d(a2, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                                    this.this$0.this$0.a.this$0.j().add(bt5);
                                }
                            }
                            return i97.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(C0144b bVar, qy5.d dVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                    this.$successResponse = dVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$successResponse, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        hj7 a2 = xh7.a(yi7, this.this$0.a.this$0.b(), null, new C0145a(this, null), 2, null);
                        this.L$0 = yi7;
                        this.L$1 = a2;
                        this.label = 1;
                        if (a2.c(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        hj7 hj7 = (hj7) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.a.this$0.h.isEmpty()) {
                        for (bt5 bt5 : this.this$0.a.this$0.h) {
                            this.this$0.a.this$0.j().add(bt5);
                        }
                    }
                    this.this$0.a.this$0.f.a(this.this$0.a.this$0.j(), fx6.a.a(), this.this$0.a.this$0.g);
                    this.this$0.a.this$0.i.a(0, new Bundle(), this.this$0.a.this$0);
                    return i97.a;
                }
            }

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public C0144b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(qy5.d dVar) {
                ee7.b(dVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(ny5.m.a(), "GetAllContactGroup onSuccess");
                ik7 unused = xh7.b(this.a.this$0.e(), null, null, new a(this, dVar, null), 3, null);
            }

            @DexIgnore
            public void a(qy5.b bVar) {
                ee7.b(bVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(ny5.m.a(), "GetAllContactGroup onError");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ny5 ny5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ny5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (!PortfolioApp.g0.c().v().U()) {
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(a3, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.j().isEmpty()) {
                this.this$0.j.a(this.this$0.k, null, new C0144b(this));
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = ny5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationHybridContac\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public ny5(hy5 hy5, int i2, ArrayList<bt5> arrayList, LoaderManager loaderManager, rl4 rl4, qy5 qy5) {
        ee7.b(hy5, "mView");
        ee7.b(arrayList, "mContactWrappersSelected");
        ee7.b(loaderManager, "mLoaderManager");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(qy5, "mGetAllHybridContactGroups");
        this.f = hy5;
        this.g = i2;
        this.h = arrayList;
        this.i = loaderManager;
        this.j = rl4;
        this.k = qy5;
    }

    @DexIgnore
    @Override // com.fossil.gy5
    public void i() {
        ArrayList<bt5> arrayList = new ArrayList<>();
        for (T t : this.e) {
            if (t.isAdded() && t.getCurrentHandGroup() == this.g) {
                arrayList.add(t);
            }
        }
        this.f.a(arrayList);
    }

    @DexIgnore
    public final List<bt5> j() {
        return this.e;
    }

    @DexIgnore
    public void k() {
        this.f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    @Override // com.fossil.gy5
    public int h() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.gy5
    public void a(bt5 bt5) {
        Integer num;
        ee7.b(bt5, "contactWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "reassignContact: contactWrapper=" + bt5);
        Iterator<T> it = this.e.iterator();
        while (true) {
            num = null;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            Contact contact = next.getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = bt5.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (ee7.a(valueOf, num)) {
                num = next;
                break;
            }
        }
        bt5 bt52 = (bt5) num;
        if (bt52 != null) {
            bt52.setAdded(true);
            bt52.setCurrentHandGroup(this.g);
            this.f.u0();
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public oe<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new ne(PortfolioApp.g0.c(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(oe<Cursor> oeVar, Cursor cursor) {
        ee7.b(oeVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.f.a(cursor);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void a(oe<Cursor> oeVar) {
        ee7.b(oeVar, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.f.s();
    }
}
