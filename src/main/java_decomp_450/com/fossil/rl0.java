package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl0 extends tj0 {
    @DexIgnore
    public byte[] X;
    @DexIgnore
    public long Y;
    @DexIgnore
    public long Z;
    @DexIgnore
    public /* final */ long a0;
    @DexIgnore
    public /* final */ long b0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ rl0(short s, long j, long j2, ri1 ri1, int i, int i2) {
        super(ln0.b, s, qa1.v, ri1, (i2 & 16) != 0 ? 3 : i);
        this.a0 = j;
        this.b0 = j2;
        this.X = new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public void b(byte[] bArr) {
        this.X = bArr;
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public void c(long j) {
        this.Z = j;
    }

    @DexIgnore
    @Override // com.fossil.v81, com.fossil.rr1
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.a1, Long.valueOf(this.a0)), r51.b1, Long.valueOf(this.b0));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a0).putInt((int) this.b0).array();
        ee7.a((Object) array, "ByteBuffer.allocate(8).o\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public long r() {
        return this.Y;
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public long s() {
        return this.Z;
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public byte[] t() {
        return this.X;
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public void b(long j) {
        this.Y = j;
    }
}
