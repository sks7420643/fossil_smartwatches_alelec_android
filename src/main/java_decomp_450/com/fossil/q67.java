package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.zendesk.belvedere.Belvedere;
import java.util.Arrays;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q67 {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public String f;
    @DexIgnore
    public u67 g;
    @DexIgnore
    public TreeSet<w67> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public Context a;
        @DexIgnore
        public String b; // = "belvedere-data";
        @DexIgnore
        public int c; // = FailureCode.USER_CANCELLED_BUT_USER_DID_NOT_SELECT_ANY_DEVICE;
        @DexIgnore
        public int d; // = FailureCode.USER_CANCELLED;
        @DexIgnore
        public int e; // = 1653;
        @DexIgnore
        public boolean f; // = true;
        @DexIgnore
        public String g; // = "*/*";
        @DexIgnore
        public u67 h; // = new y67();
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public TreeSet<w67> j; // = new TreeSet<>(Arrays.asList(w67.Camera, w67.Gallery));

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public a a(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public a b(boolean z) {
            this.i = z;
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public a a(u67 u67) {
            if (u67 != null) {
                this.h = u67;
                return this;
            }
            throw new IllegalArgumentException("Invalid logger provided");
        }

        @DexIgnore
        public Belvedere a() {
            this.h.setLoggable(this.i);
            return new Belvedere(this.a, new q67(this));
        }
    }

    @DexIgnore
    public q67(a aVar) {
        this.a = aVar.b;
        this.b = aVar.c;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.j;
    }

    @DexIgnore
    public boolean a() {
        return this.e;
    }

    @DexIgnore
    public u67 b() {
        return this.g;
    }

    @DexIgnore
    public TreeSet<w67> c() {
        return this.h;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public String f() {
        return this.f;
    }

    @DexIgnore
    public String g() {
        return this.a;
    }

    @DexIgnore
    public int h() {
        return this.b;
    }
}
