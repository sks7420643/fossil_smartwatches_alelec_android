package com.fossil;

import com.fossil.dw1;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aw1 extends dw1.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Set<dw1.c> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends dw1.b.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Set<dw1.c> c;

        @DexIgnore
        @Override // com.fossil.dw1.b.a
        public dw1.b.a a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.dw1.b.a
        public dw1.b.a b(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.dw1.b.a
        public dw1.b.a a(Set<dw1.c> set) {
            if (set != null) {
                this.c = set;
                return this;
            }
            throw new NullPointerException("Null flags");
        }

        @DexIgnore
        @Override // com.fossil.dw1.b.a
        public dw1.b a() {
            String str = "";
            if (this.a == null) {
                str = str + " delta";
            }
            if (this.b == null) {
                str = str + " maxAllowedDelay";
            }
            if (this.c == null) {
                str = str + " flags";
            }
            if (str.isEmpty()) {
                return new aw1(this.a.longValue(), this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.dw1.b
    public long a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.dw1.b
    public Set<dw1.c> b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.dw1.b
    public long c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof dw1.b)) {
            return false;
        }
        dw1.b bVar = (dw1.b) obj;
        if (this.a == bVar.a() && this.b == bVar.c() && this.c.equals(bVar.b())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        return this.c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "ConfigValue{delta=" + this.a + ", maxAllowedDelay=" + this.b + ", flags=" + this.c + "}";
    }

    @DexIgnore
    public aw1(long j, long j2, Set<dw1.c> set) {
        this.a = j;
        this.b = j2;
        this.c = set;
    }
}
