package com.fossil;

import android.os.Build;
import android.os.LocaleList;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g8 {
    @DexIgnore
    public i8 a;

    /*
    static {
        a(new Locale[0]);
    }
    */

    @DexIgnore
    public g8(i8 i8Var) {
        this.a = i8Var;
    }

    @DexIgnore
    public static g8 a(LocaleList localeList) {
        return new g8(new j8(localeList));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof g8) && this.a.equals(((g8) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public static g8 a(Locale... localeArr) {
        if (Build.VERSION.SDK_INT >= 24) {
            return a(new LocaleList(localeArr));
        }
        return new g8(new h8(localeArr));
    }

    @DexIgnore
    public Locale a(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public static Locale a(String str) {
        if (str.contains(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
            String[] split = str.split(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, -1);
            if (split.length > 2) {
                return new Locale(split[0], split[1], split[2]);
            }
            if (split.length > 1) {
                return new Locale(split[0], split[1]);
            }
            if (split.length == 1) {
                return new Locale(split[0]);
            }
        } else if (!str.contains(LocaleConverter.LOCALE_DELIMITER)) {
            return new Locale(str);
        } else {
            String[] split2 = str.split(LocaleConverter.LOCALE_DELIMITER, -1);
            if (split2.length > 2) {
                return new Locale(split2[0], split2[1], split2[2]);
            }
            if (split2.length > 1) {
                return new Locale(split2[0], split2[1]);
            }
            if (split2.length == 1) {
                return new Locale(split2[0]);
            }
        }
        throw new IllegalArgumentException("Can not parse language tag: [" + str + "]");
    }
}
