package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x55 extends w55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362776, 1);
        Q.put(2131362656, 2);
        Q.put(2131363269, 3);
        Q.put(2131363342, 4);
        Q.put(2131363100, 5);
        Q.put(2131362608, 6);
        Q.put(2131362223, 7);
        Q.put(2131362649, 8);
        Q.put(2131362614, 9);
        Q.put(2131362231, 10);
        Q.put(2131363266, 11);
        Q.put(2131363265, 12);
        Q.put(2131361920, 13);
        Q.put(2131361922, 14);
        Q.put(2131363330, 15);
        Q.put(2131362684, 16);
        Q.put(2131362689, 17);
        Q.put(2131362631, 18);
        Q.put(2131362740, 19);
        Q.put(2131362739, 20);
        Q.put(2131363275, 21);
        Q.put(2131363329, 22);
        Q.put(2131361936, 23);
    }
    */

    @DexIgnore
    public x55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 24, P, Q));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.O != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.O = 1;
        }
        g();
    }

    @DexIgnore
    public x55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (Barrier) objArr[13], (Barrier) objArr[14], (FlexibleButton) objArr[23], (FlexibleTextInputEditText) objArr[7], (FlexibleTextInputEditText) objArr[10], (FlexibleTextInputLayout) objArr[6], (FlexibleTextInputLayout) objArr[9], (FloatingActionButton) objArr[18], (RTLImageView) objArr[8], (ImageView) objArr[2], (ImageView) objArr[16], (ImageView) objArr[17], (ConstraintLayout) objArr[20], (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[0], (ScrollView) objArr[5], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[21], (FlexibleButton) objArr[22], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[4]);
        this.O = -1;
        ((w55) this).F.setTag(null);
        a(view);
        f();
    }
}
