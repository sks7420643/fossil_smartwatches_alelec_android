package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements u84<zc4> {
        @DexIgnore
        public void a(zc4 zc4, v84 v84) throws t84, IOException {
            Intent b = zc4.b();
            v84.a("ttl", cd4.l(b));
            v84.a(Constants.EVENT, zc4.a());
            v84.a("instanceId", cd4.b());
            v84.a("priority", cd4.j(b));
            v84.a("packageName", cd4.c());
            v84.a("sdkPlatform", "ANDROID");
            v84.a("messageType", cd4.h(b));
            String e = cd4.e(b);
            if (e != null) {
                v84.a("messageId", e);
            }
            String k = cd4.k(b);
            if (k != null) {
                v84.a("topic", k);
            }
            String a = cd4.a(b);
            if (a != null) {
                v84.a("collapseKey", a);
            }
            if (cd4.f(b) != null) {
                v84.a("analyticsLabel", cd4.f(b));
            }
            if (cd4.c(b) != null) {
                v84.a("composerLabel", cd4.c(b));
            }
            String d = cd4.d();
            if (d != null) {
                v84.a("projectNumber", d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ zc4 a;

        @DexIgnore
        public b(zc4 zc4) {
            a72.a(zc4);
            this.a = zc4;
        }

        @DexIgnore
        public final zc4 a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements u84<b> {
        @DexIgnore
        public final void a(b bVar, v84 v84) throws t84, IOException {
            v84.a("messaging_client_event", bVar.a());
        }
    }

    @DexIgnore
    public zc4(String str, Intent intent) {
        a72.a(str, (Object) "evenType must be non-null");
        this.a = str;
        a72.a(intent, "intent must be non-null");
        this.b = intent;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final Intent b() {
        return this.b;
    }
}
