package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.be5;
import com.fossil.do5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l96 extends go5 implements k96 {
    @DexIgnore
    public qw6<ex4> f;
    @DexIgnore
    public j96 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ActivityOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        ex4 a2;
        OverviewWeekChart overviewWeekChart;
        qw6<ex4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewWeekChart = a2.q) != null) {
            be5.a aVar = be5.o;
            j96 j96 = this.g;
            if (aVar.a(j96 != null ? j96.h() : null)) {
                overviewWeekChart.a("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.a("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ex4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onCreateView");
        this.f = new qw6<>(this, (ex4) qb.a(layoutInflater, 2131558500, viewGroup, false, a1()));
        f1();
        qw6<ex4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onResume");
        f1();
        j96 j96 = this.g;
        if (j96 != null) {
            j96.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onStop");
        j96 j96 = this.g;
        if (j96 != null) {
            j96.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.k96
    public void a(do5 do5) {
        ex4 a2;
        OverviewWeekChart overviewWeekChart;
        ee7.b(do5, "baseModel");
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "showWeekDetails");
        qw6<ex4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            do5.a aVar = do5.a;
            ee7.a((Object) overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            ee7.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewWeekChart.a(do5);
        }
    }

    @DexIgnore
    public void a(j96 j96) {
        ee7.b(j96, "presenter");
        this.g = j96;
    }
}
