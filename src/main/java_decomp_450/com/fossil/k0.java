package com.fossil;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle$Delegate;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.collection.SimpleArrayMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.f1;
import com.fossil.m9;
import com.fossil.p1;
import com.fossil.s2;
import com.fossil.v1;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.lang.Thread;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k0 extends AppCompatDelegate implements p1.a, LayoutInflater.Factory2 {
    @DexIgnore
    public static /* final */ SimpleArrayMap<String, Integer> f0; // = new SimpleArrayMap<>();
    @DexIgnore
    public static /* final */ boolean g0; // = (Build.VERSION.SDK_INT < 21);
    @DexIgnore
    public static /* final */ int[] h0; // = {16842836};
    @DexIgnore
    public static /* final */ boolean i0; // = (!"robolectric".equals(Build.FINGERPRINT));
    @DexIgnore
    public static /* final */ boolean j0;
    @DexIgnore
    public static boolean k0; // = true;
    @DexIgnore
    public ViewGroup A;
    @DexIgnore
    public TextView B;
    @DexIgnore
    public View C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public t[] L;
    @DexIgnore
    public t M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public boolean R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public m W;
    @DexIgnore
    public m X;
    @DexIgnore
    public boolean Y;
    @DexIgnore
    public int Z;
    @DexIgnore
    public /* final */ Runnable a0;
    @DexIgnore
    public boolean b0;
    @DexIgnore
    public Rect c0;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public Rect d0;
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public n0 e0;
    @DexIgnore
    public Window f;
    @DexIgnore
    public k g;
    @DexIgnore
    public /* final */ j0 h;
    @DexIgnore
    public ActionBar i;
    @DexIgnore
    public MenuInflater j;
    @DexIgnore
    public CharSequence p;
    @DexIgnore
    public o2 q;
    @DexIgnore
    public i r;
    @DexIgnore
    public u s;
    @DexIgnore
    public ActionMode t;
    @DexIgnore
    public ActionBarContextView u;
    @DexIgnore
    public PopupWindow v;
    @DexIgnore
    public Runnable w;
    @DexIgnore
    public ha x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ Thread.UncaughtExceptionHandler a;

        @DexIgnore
        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.a = uncaughtExceptionHandler;
        }

        @DexIgnore
        public final boolean a(Throwable th) {
            String message;
            if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                return false;
            }
            if (message.contains(ResourceManager.DRAWABLE) || message.contains("Drawable")) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public void uncaughtException(Thread thread, Throwable th) {
            if (a(th)) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.a.uncaughtException(thread, notFoundException);
                return;
            }
            this.a.uncaughtException(thread, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            k0 k0Var = k0.this;
            if ((k0Var.Z & 1) != 0) {
                k0Var.f(0);
            }
            k0 k0Var2 = k0.this;
            if ((k0Var2.Z & 4096) != 0) {
                k0Var2.f(108);
            }
            k0 k0Var3 = k0.this;
            k0Var3.Y = false;
            k0Var3.Z = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements z9 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.z9
        public la a(View view, la laVar) {
            int g = laVar.g();
            int a2 = k0.this.a(laVar, (Rect) null);
            if (g != a2) {
                laVar = laVar.b(laVar.e(), a2, laVar.f(), laVar.d());
            }
            return da.b(view, laVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements s2.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.s2.a
        public void a(Rect rect) {
            rect.top = k0.this.a((la) null, rect);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements ContentFrameLayout.a {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ContentFrameLayout.a
        public void a() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ContentFrameLayout.a
        public void onDetachedFromWindow() {
            k0.this.q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ja {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.ia
            public void b(View view) {
                k0.this.u.setAlpha(1.0f);
                k0.this.x.a((ia) null);
                k0.this.x = null;
            }

            @DexIgnore
            @Override // com.fossil.ja, com.fossil.ia
            public void c(View view) {
                k0.this.u.setVisibility(0);
            }
        }

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void run() {
            k0 k0Var = k0.this;
            k0Var.v.showAtLocation(k0Var.u, 55, 0, 0);
            k0.this.r();
            if (k0.this.C()) {
                k0.this.u.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                k0 k0Var2 = k0.this;
                ha a2 = da.a(k0Var2.u);
                a2.a(1.0f);
                k0Var2.x = a2;
                k0.this.x.a(new a());
                return;
            }
            k0.this.u.setAlpha(1.0f);
            k0.this.u.setVisibility(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends ja {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void b(View view) {
            k0.this.u.setAlpha(1.0f);
            k0.this.x.a((ia) null);
            k0.this.x = null;
        }

        @DexIgnore
        @Override // com.fossil.ja, com.fossil.ia
        public void c(View view) {
            k0.this.u.setVisibility(0);
            k0.this.u.sendAccessibilityEvent(32);
            if (k0.this.u.getParent() instanceof View) {
                da.L((View) k0.this.u.getParent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements ActionBarDrawerToggle$Delegate {
        @DexIgnore
        public h(k0 k0Var) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements ActionMode.Callback {
        @DexIgnore
        public ActionMode.Callback a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ja {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.ia
            public void b(View view) {
                k0.this.u.setVisibility(8);
                k0 k0Var = k0.this;
                PopupWindow popupWindow = k0Var.v;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (k0Var.u.getParent() instanceof View) {
                    da.L((View) k0.this.u.getParent());
                }
                k0.this.u.removeAllViews();
                k0.this.x.a((ia) null);
                k0 k0Var2 = k0.this;
                k0Var2.x = null;
                da.L(k0Var2.A);
            }
        }

        @DexIgnore
        public j(ActionMode.Callback callback) {
            this.a = callback;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean a(ActionMode actionMode, Menu menu) {
            return this.a.a(actionMode, menu);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean b(ActionMode actionMode, Menu menu) {
            da.L(k0.this.A);
            return this.a.b(actionMode, menu);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.a.a(actionMode, menuItem);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public void a(ActionMode actionMode) {
            this.a.a(actionMode);
            k0 k0Var = k0.this;
            if (k0Var.v != null) {
                k0Var.f.getDecorView().removeCallbacks(k0.this.w);
            }
            k0 k0Var2 = k0.this;
            if (k0Var2.u != null) {
                k0Var2.r();
                k0 k0Var3 = k0.this;
                ha a2 = da.a(k0Var3.u);
                a2.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                k0Var3.x = a2;
                k0.this.x.a(new a());
            }
            k0 k0Var4 = k0.this;
            j0 j0Var = k0Var4.h;
            if (j0Var != null) {
                j0Var.onSupportActionModeFinished(k0Var4.t);
            }
            k0 k0Var5 = k0.this;
            k0Var5.t = null;
            da.L(k0Var5.A);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends m {
        @DexIgnore
        public /* final */ PowerManager c;

        @DexIgnore
        public l(Context context) {
            super();
            this.c = (PowerManager) context.getApplicationContext().getSystemService("power");
        }

        @DexIgnore
        @Override // com.fossil.k0.m
        public IntentFilter b() {
            if (Build.VERSION.SDK_INT < 21) {
                return null;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }

        @DexIgnore
        @Override // com.fossil.k0.m
        public int c() {
            if (Build.VERSION.SDK_INT < 21 || !this.c.isPowerSaveMode()) {
                return 1;
            }
            return 2;
        }

        @DexIgnore
        @Override // com.fossil.k0.m
        public void d() {
            k0.this.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class m {
        @DexIgnore
        public BroadcastReceiver a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends BroadcastReceiver {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                m.this.d();
            }
        }

        @DexIgnore
        public m() {
        }

        @DexIgnore
        public void a() {
            BroadcastReceiver broadcastReceiver = this.a;
            if (broadcastReceiver != null) {
                try {
                    k0.this.e.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
                this.a = null;
            }
        }

        @DexIgnore
        public abstract IntentFilter b();

        @DexIgnore
        public abstract int c();

        @DexIgnore
        public abstract void d();

        @DexIgnore
        public void e() {
            a();
            IntentFilter b2 = b();
            if (b2 != null && b2.countActions() != 0) {
                if (this.a == null) {
                    this.a = new a();
                }
                k0.this.e.registerReceiver(this.a, b2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n extends m {
        @DexIgnore
        public /* final */ r0 c;

        @DexIgnore
        public n(r0 r0Var) {
            super();
            this.c = r0Var;
        }

        @DexIgnore
        @Override // com.fossil.k0.m
        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        @DexIgnore
        @Override // com.fossil.k0.m
        public int c() {
            return this.c.b() ? 2 : 1;
        }

        @DexIgnore
        @Override // com.fossil.k0.m
        public void d() {
            k0.this.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class o {
        @DexIgnore
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            int i = configuration.densityDpi;
            int i2 = configuration2.densityDpi;
            if (i != i2) {
                configuration3.densityDpi = i2;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class p {
        @DexIgnore
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            LocaleList locales = configuration.getLocales();
            LocaleList locales2 = configuration2.getLocales();
            if (!locales.equals(locales2)) {
                configuration3.setLocales(locales2);
                configuration3.locale = configuration2.locale;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class q {
        @DexIgnore
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            int i = configuration.colorMode & 3;
            int i2 = configuration2.colorMode;
            if (i != (i2 & 3)) {
                configuration3.colorMode |= i2 & 3;
            }
            int i3 = configuration.colorMode & 12;
            int i4 = configuration2.colorMode;
            if (i3 != (i4 & 12)) {
                configuration3.colorMode |= i4 & 12;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class r {
        @DexIgnore
        public static void a(ContextThemeWrapper contextThemeWrapper, Configuration configuration) {
            contextThemeWrapper.applyOverrideConfiguration(configuration);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class s extends ContentFrameLayout {
        @DexIgnore
        public s(Context context) {
            super(context);
        }

        @DexIgnore
        public final boolean a(int i2, int i3) {
            return i2 < -5 || i3 < -5 || i2 > getWidth() + 5 || i3 > getHeight() + 5;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return k0.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            k0.this.e(0);
            return true;
        }

        @DexIgnore
        public void setBackgroundResource(int i2) {
            setBackgroundDrawable(t0.c(getContext(), i2));
        }
    }

    /*
    static {
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 17) {
            z2 = true;
        }
        j0 = z2;
        if (g0 && !k0) {
            Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }
    */

    @DexIgnore
    public k0(Activity activity, j0 j0Var) {
        this(activity, null, j0Var, activity);
    }

    @DexIgnore
    public boolean A() {
        ActionMode actionMode = this.t;
        if (actionMode != null) {
            actionMode.a();
            return true;
        }
        ActionBar d2 = d();
        if (d2 == null || !d2.f()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ActionBar B() {
        return this.i;
    }

    @DexIgnore
    public final boolean C() {
        ViewGroup viewGroup;
        return this.z && (viewGroup = this.A) != null && da.G(viewGroup);
    }

    @DexIgnore
    public final void D() {
        if (this.z) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    @DexIgnore
    public final AppCompatActivity E() {
        Context context = this.e;
        while (context != null) {
            if (!(context instanceof AppCompatActivity)) {
                if (!(context instanceof ContextWrapper)) {
                    break;
                }
                context = ((ContextWrapper) context).getBaseContext();
            } else {
                return (AppCompatActivity) context;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void a(Bundle bundle) {
        this.O = true;
        a(false);
        t();
        Object obj = this.d;
        if (obj instanceof Activity) {
            String str = null;
            try {
                str = m6.b((Activity) obj);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                ActionBar B2 = B();
                if (B2 == null) {
                    this.b0 = true;
                } else {
                    B2.c(true);
                }
            }
        }
        AppCompatDelegate.a(this);
        this.P = true;
    }

    @DexIgnore
    public void a(ViewGroup viewGroup) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public Context b(Context context) {
        boolean z2 = true;
        this.O = true;
        int a2 = a(context, n());
        Configuration configuration = null;
        if (j0 && (context instanceof ContextThemeWrapper)) {
            try {
                r.a((ContextThemeWrapper) context, a(context, a2, (Configuration) null));
                return context;
            } catch (IllegalStateException unused) {
            }
        }
        if (context instanceof d1) {
            try {
                ((d1) context).a(a(context, a2, (Configuration) null));
                return context;
            } catch (IllegalStateException unused2) {
            }
        }
        if (!i0) {
            super.b(context);
            return context;
        }
        try {
            Configuration configuration2 = context.getPackageManager().getResourcesForApplication(context.getApplicationInfo()).getConfiguration();
            Configuration configuration3 = context.getResources().getConfiguration();
            if (!configuration2.equals(configuration3)) {
                configuration = a(configuration2, configuration3);
            }
            Configuration a3 = a(context, a2, configuration);
            d1 d1Var = new d1(context, g0.Theme_AppCompat_Empty);
            d1Var.a(a3);
            boolean z3 = false;
            try {
                if (context.getTheme() == null) {
                    z2 = false;
                }
                z3 = z2;
            } catch (NullPointerException unused3) {
            }
            if (z3) {
                c7.b.a(d1Var.getTheme());
            }
            super.b(d1Var);
            return d1Var;
        } catch (PackageManager.NameNotFoundException e2) {
            throw new RuntimeException("Application failed to obtain resources from itself", e2);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public MenuInflater c() {
        if (this.j == null) {
            x();
            ActionBar actionBar = this.i;
            this.j = new g1(actionBar != null ? actionBar.h() : this.e);
        }
        return this.j;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void c(Bundle bundle) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public ActionBar d() {
        x();
        return this.i;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void e() {
        LayoutInflater from = LayoutInflater.from(this.e);
        if (from.getFactory() == null) {
            n9.b(from, this);
        } else if (!(from.getFactory2() instanceof k0)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void f() {
        ActionBar d2 = d();
        if (d2 == null || !d2.i()) {
            g(0);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0055  */
    @Override // androidx.appcompat.app.AppCompatDelegate
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r3 = this;
            androidx.appcompat.app.AppCompatDelegate.b(r3)
            boolean r0 = r3.Y
            if (r0 == 0) goto L_0x0012
            android.view.Window r0 = r3.f
            android.view.View r0 = r0.getDecorView()
            java.lang.Runnable r1 = r3.a0
            r0.removeCallbacks(r1)
        L_0x0012:
            r0 = 0
            r3.Q = r0
            r0 = 1
            r3.R = r0
            int r0 = r3.S
            r1 = -100
            if (r0 == r1) goto L_0x0042
            java.lang.Object r0 = r3.d
            boolean r1 = r0 instanceof android.app.Activity
            if (r1 == 0) goto L_0x0042
            android.app.Activity r0 = (android.app.Activity) r0
            boolean r0 = r0.isChangingConfigurations()
            if (r0 == 0) goto L_0x0042
            androidx.collection.SimpleArrayMap<java.lang.String, java.lang.Integer> r0 = com.fossil.k0.f0
            java.lang.Object r1 = r3.d
            java.lang.Class r1 = r1.getClass()
            java.lang.String r1 = r1.getName()
            int r2 = r3.S
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0.put(r1, r2)
            goto L_0x0051
        L_0x0042:
            androidx.collection.SimpleArrayMap<java.lang.String, java.lang.Integer> r0 = com.fossil.k0.f0
            java.lang.Object r1 = r3.d
            java.lang.Class r1 = r1.getClass()
            java.lang.String r1 = r1.getName()
            r0.remove(r1)
        L_0x0051:
            androidx.appcompat.app.ActionBar r0 = r3.i
            if (r0 == 0) goto L_0x0058
            r0.j()
        L_0x0058:
            r3.o()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k0.g():void");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void h() {
        ActionBar d2 = d();
        if (d2 != null) {
            d2.e(true);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void i() {
        this.Q = true;
        l();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void j() {
        this.Q = false;
        ActionBar d2 = d();
        if (d2 != null) {
            d2.e(false);
        }
    }

    @DexIgnore
    public boolean l() {
        return a(true);
    }

    @DexIgnore
    public final void m() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.A.findViewById(16908290);
        View decorView = this.f.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.e.obtainStyledAttributes(h0.AppCompatTheme);
        obtainStyledAttributes.getValue(h0.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(h0.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(h0.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(h0.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(h0.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(h0.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(h0.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(h0.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(h0.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(h0.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    @DexIgnore
    public final int n() {
        int i2 = this.S;
        return i2 != -100 ? i2 : AppCompatDelegate.k();
    }

    @DexIgnore
    public final void o() {
        m mVar = this.W;
        if (mVar != null) {
            mVar.a();
        }
        m mVar2 = this.X;
        if (mVar2 != null) {
            mVar2.a();
        }
    }

    @DexIgnore
    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return a(view, str, context, attributeSet);
    }

    @DexIgnore
    public final ViewGroup p() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.e.obtainStyledAttributes(h0.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(h0.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(h0.AppCompatTheme_windowNoTitle, false)) {
                b(1);
            } else if (obtainStyledAttributes.getBoolean(h0.AppCompatTheme_windowActionBar, false)) {
                b(108);
            }
            if (obtainStyledAttributes.getBoolean(h0.AppCompatTheme_windowActionBarOverlay, false)) {
                b(109);
            }
            if (obtainStyledAttributes.getBoolean(h0.AppCompatTheme_windowActionModeOverlay, false)) {
                b(10);
            }
            this.I = obtainStyledAttributes.getBoolean(h0.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            t();
            this.f.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.e);
            if (this.J) {
                viewGroup = this.H ? (ViewGroup) from.inflate(e0.abc_screen_simple_overlay_action_mode, (ViewGroup) null) : (ViewGroup) from.inflate(e0.abc_screen_simple, (ViewGroup) null);
            } else if (this.I) {
                viewGroup = (ViewGroup) from.inflate(e0.abc_dialog_title_material, (ViewGroup) null);
                this.G = false;
                this.F = false;
            } else if (this.F) {
                TypedValue typedValue = new TypedValue();
                this.e.getTheme().resolveAttribute(y.actionBarTheme, typedValue, true);
                if (typedValue.resourceId != 0) {
                    context = new d1(this.e, typedValue.resourceId);
                } else {
                    context = this.e;
                }
                viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(e0.abc_screen_toolbar, (ViewGroup) null);
                o2 o2Var = (o2) viewGroup.findViewById(d0.decor_content_parent);
                this.q = o2Var;
                o2Var.setWindowCallback(w());
                if (this.G) {
                    this.q.a(109);
                }
                if (this.D) {
                    this.q.a(2);
                }
                if (this.E) {
                    this.q.a(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (Build.VERSION.SDK_INT >= 21) {
                    da.a(viewGroup, new c());
                } else if (viewGroup instanceof s2) {
                    ((s2) viewGroup).setOnFitSystemWindowsListener(new d());
                }
                if (this.q == null) {
                    this.B = (TextView) viewGroup.findViewById(d0.title);
                }
                m3.b(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(d0.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.f.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground(null);
                    }
                }
                this.f.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new e());
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.F + ", windowActionBarOverlay: " + this.G + ", android:windowIsFloating: " + this.I + ", windowActionModeOverlay: " + this.H + ", windowNoTitle: " + this.J + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    @DexIgnore
    public void q() {
        p1 p1Var;
        o2 o2Var = this.q;
        if (o2Var != null) {
            o2Var.g();
        }
        if (this.v != null) {
            this.f.getDecorView().removeCallbacks(this.w);
            if (this.v.isShowing()) {
                try {
                    this.v.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.v = null;
        }
        r();
        t a2 = a(0, false);
        if (a2 != null && (p1Var = a2.j) != null) {
            p1Var.close();
        }
    }

    @DexIgnore
    public void r() {
        ha haVar = this.x;
        if (haVar != null) {
            haVar.a();
        }
    }

    @DexIgnore
    public final void s() {
        if (!this.z) {
            this.A = p();
            CharSequence v2 = v();
            if (!TextUtils.isEmpty(v2)) {
                o2 o2Var = this.q;
                if (o2Var != null) {
                    o2Var.setWindowTitle(v2);
                } else if (B() != null) {
                    B().b(v2);
                } else {
                    TextView textView = this.B;
                    if (textView != null) {
                        textView.setText(v2);
                    }
                }
            }
            m();
            a(this.A);
            this.z = true;
            t a2 = a(0, false);
            if (this.R) {
                return;
            }
            if (a2 == null || a2.j == null) {
                g(108);
            }
        }
    }

    @DexIgnore
    public final void t() {
        if (this.f == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                a(((Activity) obj).getWindow());
            }
        }
        if (this.f == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    @DexIgnore
    public final Context u() {
        ActionBar d2 = d();
        Context h2 = d2 != null ? d2.h() : null;
        return h2 == null ? this.e : h2;
    }

    @DexIgnore
    public final CharSequence v() {
        Object obj = this.d;
        if (obj instanceof Activity) {
            return ((Activity) obj).getTitle();
        }
        return this.p;
    }

    @DexIgnore
    public final Window.Callback w() {
        return this.f.getCallback();
    }

    @DexIgnore
    public final void x() {
        s();
        if (this.F && this.i == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                this.i = new s0((Activity) this.d, this.G);
            } else if (obj instanceof Dialog) {
                this.i = new s0((Dialog) this.d);
            }
            ActionBar actionBar = this.i;
            if (actionBar != null) {
                actionBar.c(this.b0);
            }
        }
    }

    @DexIgnore
    public final boolean y() {
        int i2;
        if (!this.V && (this.d instanceof Activity)) {
            PackageManager packageManager = this.e.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            try {
                if (Build.VERSION.SDK_INT >= 29) {
                    i2 = 269221888;
                } else {
                    i2 = Build.VERSION.SDK_INT >= 24 ? 786432 : 0;
                }
                ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(this.e, this.d.getClass()), i2);
                this.U = (activityInfo == null || (activityInfo.configChanges & 512) == 0) ? false : true;
            } catch (PackageManager.NameNotFoundException e2) {
                Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
                this.U = false;
            }
        }
        this.V = true;
        return this.U;
    }

    @DexIgnore
    public boolean z() {
        return this.y;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements v1.a {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public boolean a(p1 p1Var) {
            Window.Callback w = k0.this.w();
            if (w == null) {
                return true;
            }
            w.onMenuOpened(108, p1Var);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public void a(p1 p1Var, boolean z) {
            k0.this.b(p1Var);
        }
    }

    @DexIgnore
    public k0(Dialog dialog, j0 j0Var) {
        this(dialog.getContext(), dialog.getWindow(), j0Var, dialog);
    }

    @DexIgnore
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public ViewGroup g;
        @DexIgnore
        public View h;
        @DexIgnore
        public View i;
        @DexIgnore
        public p1 j;
        @DexIgnore
        public n1 k;
        @DexIgnore
        public Context l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q; // = false;
        @DexIgnore
        public boolean r;
        @DexIgnore
        public Bundle s;

        @DexIgnore
        public t(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public boolean a() {
            if (this.h == null) {
                return false;
            }
            if (this.i == null && this.k.a().getCount() <= 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(y.actionBarPopupTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            }
            newTheme.resolveAttribute(y.panelMenuListTheme, typedValue, true);
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                newTheme.applyStyle(i3, true);
            } else {
                newTheme.applyStyle(g0.Theme_AppCompat_CompactMenu, true);
            }
            d1 d1Var = new d1(context, 0);
            d1Var.getTheme().setTo(newTheme);
            this.l = d1Var;
            TypedArray obtainStyledAttributes = d1Var.obtainStyledAttributes(h0.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(h0.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(h0.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        public void a(p1 p1Var) {
            n1 n1Var;
            p1 p1Var2 = this.j;
            if (p1Var != p1Var2) {
                if (p1Var2 != null) {
                    p1Var2.b(this.k);
                }
                this.j = p1Var;
                if (p1Var != null && (n1Var = this.k) != null) {
                    p1Var.a(n1Var);
                }
            }
        }

        @DexIgnore
        public w1 a(v1.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                n1 n1Var = new n1(this.l, e0.abc_list_menu_item_layout);
                this.k = n1Var;
                n1Var.a(aVar);
                this.j.a(this.k);
            }
            return this.k.a(this.g);
        }
    }

    @DexIgnore
    public k0(Context context, Window window, j0 j0Var, Object obj) {
        Integer num;
        AppCompatActivity E2;
        this.x = null;
        this.y = true;
        this.S = -100;
        this.a0 = new b();
        this.e = context;
        this.h = j0Var;
        this.d = obj;
        if (this.S == -100 && (obj instanceof Dialog) && (E2 = E()) != null) {
            this.S = E2.getDelegate().b();
        }
        if (this.S == -100 && (num = f0.get(this.d.getClass().getName())) != null) {
            this.S = num.intValue();
            f0.remove(this.d.getClass().getName());
        }
        if (window != null) {
            a(window);
        }
        h2.c();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void d(int i2) {
        this.T = i2;
    }

    @DexIgnore
    public void h(int i2) {
        ActionBar d2;
        if (i2 == 108 && (d2 = d()) != null) {
            d2.b(true);
        }
    }

    @DexIgnore
    public void i(int i2) {
        if (i2 == 108) {
            ActionBar d2 = d();
            if (d2 != null) {
                d2.b(false);
            }
        } else if (i2 == 0) {
            t a2 = a(i2, true);
            if (a2.o) {
                a(a2, false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends i1 {
        @DexIgnore
        public k(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public final android.view.ActionMode a(ActionMode.Callback callback) {
            f1.a aVar = new f1.a(k0.this.e, callback);
            androidx.appcompat.view.ActionMode a = k0.this.a(aVar);
            if (a != null) {
                return aVar.b(a);
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.i1
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return k0.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        @Override // com.fossil.i1
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || k0.this.c(keyEvent.getKeyCode(), keyEvent);
        }

        @DexIgnore
        @Override // com.fossil.i1
        public void onContentChanged() {
        }

        @DexIgnore
        @Override // com.fossil.i1
        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof p1)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.i1
        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            k0.this.h(i);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.i1
        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            k0.this.i(i);
        }

        @DexIgnore
        @Override // com.fossil.i1
        public boolean onPreparePanel(int i, View view, Menu menu) {
            p1 p1Var = menu instanceof p1 ? (p1) menu : null;
            if (i == 0 && p1Var == null) {
                return false;
            }
            if (p1Var != null) {
                p1Var.d(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (p1Var != null) {
                p1Var.d(false);
            }
            return onPreparePanel;
        }

        @DexIgnore
        @Override // com.fossil.i1, android.view.Window.Callback
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            p1 p1Var;
            t a = k0.this.a(0, true);
            if (a == null || (p1Var = a.j) == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, p1Var, i);
            }
        }

        @DexIgnore
        @Override // com.fossil.i1
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (k0.this.z()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        @DexIgnore
        @Override // com.fossil.i1
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (!k0.this.z() || i != 0) {
                return super.onWindowStartingActionMode(callback, i);
            }
            return a(callback);
        }
    }

    @DexIgnore
    public boolean d(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z2 = this.N;
            this.N = false;
            t a2 = a(0, false);
            if (a2 != null && a2.o) {
                if (!z2) {
                    a(a2, true);
                }
                return true;
            } else if (A()) {
                return true;
            }
        } else if (i2 == 82) {
            e(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    public void f(int i2) {
        t a2;
        t a3 = a(i2, true);
        if (a3.j != null) {
            Bundle bundle = new Bundle();
            a3.j.e(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.j.s();
            a3.j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i2 == 108 || i2 == 0) && this.q != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    @DexIgnore
    public final int j(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements v1.a {
        @DexIgnore
        public u() {
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public void a(p1 p1Var, boolean z) {
            p1 m = p1Var.m();
            boolean z2 = m != p1Var;
            k0 k0Var = k0.this;
            if (z2) {
                p1Var = m;
            }
            t a2 = k0Var.a((Menu) p1Var);
            if (a2 == null) {
                return;
            }
            if (z2) {
                k0.this.a(a2.a, a2, m);
                k0.this.a(a2, true);
                return;
            }
            k0.this.a(a2, z);
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public boolean a(p1 p1Var) {
            Window.Callback w;
            if (p1Var != p1Var.m()) {
                return true;
            }
            k0 k0Var = k0.this;
            if (!k0Var.F || (w = k0Var.w()) == null || k0.this.R) {
                return true;
            }
            w.onMenuOpened(108, p1Var);
            return true;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void c(int i2) {
        s();
        ViewGroup viewGroup = (ViewGroup) this.A.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.e).inflate(i2, viewGroup);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    public void e(int i2) {
        a(a(i2, true), true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e(int r4, android.view.KeyEvent r5) {
        /*
            r3 = this;
            androidx.appcompat.view.ActionMode r0 = r3.t
            r1 = 0
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            r0 = 1
            com.fossil.k0$t r2 = r3.a(r4, r0)
            if (r4 != 0) goto L_0x0043
            com.fossil.o2 r4 = r3.q
            if (r4 == 0) goto L_0x0043
            boolean r4 = r4.c()
            if (r4 == 0) goto L_0x0043
            android.content.Context r4 = r3.e
            android.view.ViewConfiguration r4 = android.view.ViewConfiguration.get(r4)
            boolean r4 = r4.hasPermanentMenuKey()
            if (r4 != 0) goto L_0x0043
            com.fossil.o2 r4 = r3.q
            boolean r4 = r4.a()
            if (r4 != 0) goto L_0x003c
            boolean r4 = r3.R
            if (r4 != 0) goto L_0x0062
            boolean r4 = r3.b(r2, r5)
            if (r4 == 0) goto L_0x0062
            com.fossil.o2 r4 = r3.q
            boolean r0 = r4.f()
            goto L_0x006a
        L_0x003c:
            com.fossil.o2 r4 = r3.q
            boolean r0 = r4.e()
            goto L_0x006a
        L_0x0043:
            boolean r4 = r2.o
            if (r4 != 0) goto L_0x0064
            boolean r4 = r2.n
            if (r4 == 0) goto L_0x004c
            goto L_0x0064
        L_0x004c:
            boolean r4 = r2.m
            if (r4 == 0) goto L_0x0062
            boolean r4 = r2.r
            if (r4 == 0) goto L_0x005b
            r2.m = r1
            boolean r4 = r3.b(r2, r5)
            goto L_0x005c
        L_0x005b:
            r4 = 1
        L_0x005c:
            if (r4 == 0) goto L_0x0062
            r3.a(r2, r5)
            goto L_0x006a
        L_0x0062:
            r0 = 0
            goto L_0x006a
        L_0x0064:
            boolean r4 = r2.o
            r3.a(r2, r0)
            r0 = r4
        L_0x006a:
            if (r0 == 0) goto L_0x0087
            android.content.Context r4 = r3.e
            android.content.Context r4 = r4.getApplicationContext()
            java.lang.String r5 = "audio"
            java.lang.Object r4 = r4.getSystemService(r5)
            android.media.AudioManager r4 = (android.media.AudioManager) r4
            if (r4 == 0) goto L_0x0080
            r4.playSoundEffect(r1)
            goto L_0x0087
        L_0x0080:
            java.lang.String r4 = "AppCompatDelegate"
            java.lang.String r5 = "Couldn't get audio manager"
            android.util.Log.w(r4, r5)
        L_0x0087:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k0.e(int, android.view.KeyEvent):boolean");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void a(Toolbar toolbar) {
        if (this.d instanceof Activity) {
            ActionBar d2 = d();
            if (!(d2 instanceof s0)) {
                this.j = null;
                if (d2 != null) {
                    d2.j();
                }
                if (toolbar != null) {
                    p0 p0Var = new p0(toolbar, v(), this.g);
                    this.i = p0Var;
                    this.f.setCallback(p0Var.m());
                } else {
                    this.i = null;
                    this.f.setCallback(this.g);
                }
                f();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    @DexIgnore
    public boolean c(int i2, KeyEvent keyEvent) {
        ActionBar d2 = d();
        if (d2 != null && d2.a(i2, keyEvent)) {
            return true;
        }
        t tVar = this.M;
        if (tVar == null || !a(tVar, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.M == null) {
                t a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        }
        t tVar2 = this.M;
        if (tVar2 != null) {
            tVar2.n = true;
        }
        return true;
    }

    @DexIgnore
    public final m d(Context context) {
        if (this.W == null) {
            this.W = new n(r0.a(context));
        }
        return this.W;
    }

    @DexIgnore
    public final void g(int i2) {
        this.Z = (1 << i2) | this.Z;
        if (!this.Y) {
            da.a(this.f.getDecorView(), this.a0);
            this.Y = true;
        }
    }

    @DexIgnore
    public final boolean c(t tVar) {
        Context context = this.e;
        int i2 = tVar.a;
        if ((i2 == 0 || i2 == 108) && this.q != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(y.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(y.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(y.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                d1 d1Var = new d1(context, 0);
                d1Var.getTheme().setTo(theme2);
                context = d1Var;
            }
        }
        p1 p1Var = new p1(context);
        p1Var.a(this);
        tVar.a(p1Var);
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public <T extends View> T a(int i2) {
        s();
        return (T) this.f.findViewById(i2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void a(Configuration configuration) {
        ActionBar d2;
        if (this.F && this.z && (d2 = d()) != null) {
            d2.a(configuration);
        }
        h2.b().a(this.e);
        a(false);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void b(Bundle bundle) {
        s();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        s();
        ViewGroup viewGroup = (ViewGroup) this.A.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void a(View view) {
        s();
        ViewGroup viewGroup = (ViewGroup) this.A.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public boolean b(int i2) {
        int j2 = j(i2);
        if (this.J && j2 == 108) {
            return false;
        }
        if (this.F && j2 == 1) {
            this.F = false;
        }
        if (j2 == 1) {
            D();
            this.J = true;
            return true;
        } else if (j2 == 2) {
            D();
            this.D = true;
            return true;
        } else if (j2 == 5) {
            D();
            this.E = true;
            return true;
        } else if (j2 == 10) {
            D();
            this.H = true;
            return true;
        } else if (j2 == 108) {
            D();
            this.F = true;
            return true;
        } else if (j2 != 109) {
            return this.f.requestFeature(j2);
        } else {
            D();
            this.G = true;
            return true;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        s();
        ((ViewGroup) this.A.findViewById(16908290)).addView(view, layoutParams);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    public final void a(Window window) {
        if (this.f == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof k)) {
                k kVar = new k(callback);
                this.g = kVar;
                window.setCallback(kVar);
                g3 a2 = g3.a(this.e, (AttributeSet) null, h0);
                Drawable c2 = a2.c(0);
                if (c2 != null) {
                    window.setBackgroundDrawable(c2);
                }
                a2.b();
                this.f = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    @DexIgnore
    public final m c(Context context) {
        if (this.X == null) {
            this.X = new l(context);
        }
        return this.X;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public androidx.appcompat.view.ActionMode b(androidx.appcompat.view.ActionMode.Callback r8) {
        /*
            r7 = this;
            r7.r()
            androidx.appcompat.view.ActionMode r0 = r7.t
            if (r0 == 0) goto L_0x000a
            r0.a()
        L_0x000a:
            boolean r0 = r8 instanceof com.fossil.k0.j
            if (r0 != 0) goto L_0x0014
            com.fossil.k0$j r0 = new com.fossil.k0$j
            r0.<init>(r8)
            r8 = r0
        L_0x0014:
            com.fossil.j0 r0 = r7.h
            r1 = 0
            if (r0 == 0) goto L_0x0022
            boolean r2 = r7.R
            if (r2 != 0) goto L_0x0022
            androidx.appcompat.view.ActionMode r0 = r0.onWindowStartingSupportActionMode(r8)     // Catch:{ AbstractMethodError -> 0x0022 }
            goto L_0x0023
        L_0x0022:
            r0 = r1
        L_0x0023:
            if (r0 == 0) goto L_0x0029
            r7.t = r0
            goto L_0x0161
        L_0x0029:
            androidx.appcompat.widget.ActionBarContextView r0 = r7.u
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x00d4
            boolean r0 = r7.I
            if (r0 == 0) goto L_0x00b5
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            android.content.Context r4 = r7.e
            android.content.res.Resources$Theme r4 = r4.getTheme()
            int r5 = com.fossil.y.actionBarTheme
            r4.resolveAttribute(r5, r0, r3)
            int r5 = r0.resourceId
            if (r5 == 0) goto L_0x0068
            android.content.Context r5 = r7.e
            android.content.res.Resources r5 = r5.getResources()
            android.content.res.Resources$Theme r5 = r5.newTheme()
            r5.setTo(r4)
            int r4 = r0.resourceId
            r5.applyStyle(r4, r3)
            com.fossil.d1 r4 = new com.fossil.d1
            android.content.Context r6 = r7.e
            r4.<init>(r6, r2)
            android.content.res.Resources$Theme r6 = r4.getTheme()
            r6.setTo(r5)
            goto L_0x006a
        L_0x0068:
            android.content.Context r4 = r7.e
        L_0x006a:
            androidx.appcompat.widget.ActionBarContextView r5 = new androidx.appcompat.widget.ActionBarContextView
            r5.<init>(r4)
            r7.u = r5
            android.widget.PopupWindow r5 = new android.widget.PopupWindow
            int r6 = com.fossil.y.actionModePopupWindowStyle
            r5.<init>(r4, r1, r6)
            r7.v = r5
            r6 = 2
            com.fossil.za.a(r5, r6)
            android.widget.PopupWindow r5 = r7.v
            androidx.appcompat.widget.ActionBarContextView r6 = r7.u
            r5.setContentView(r6)
            android.widget.PopupWindow r5 = r7.v
            r6 = -1
            r5.setWidth(r6)
            android.content.res.Resources$Theme r5 = r4.getTheme()
            int r6 = com.fossil.y.actionBarSize
            r5.resolveAttribute(r6, r0, r3)
            int r0 = r0.data
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            int r0 = android.util.TypedValue.complexToDimensionPixelSize(r0, r4)
            androidx.appcompat.widget.ActionBarContextView r4 = r7.u
            r4.setContentHeight(r0)
            android.widget.PopupWindow r0 = r7.v
            r4 = -2
            r0.setHeight(r4)
            com.fossil.k0$f r0 = new com.fossil.k0$f
            r0.<init>()
            r7.w = r0
            goto L_0x00d4
        L_0x00b5:
            android.view.ViewGroup r0 = r7.A
            int r4 = com.fossil.d0.action_mode_bar_stub
            android.view.View r0 = r0.findViewById(r4)
            androidx.appcompat.widget.ViewStubCompat r0 = (androidx.appcompat.widget.ViewStubCompat) r0
            if (r0 == 0) goto L_0x00d4
            android.content.Context r4 = r7.u()
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
            r0.setLayoutInflater(r4)
            android.view.View r0 = r0.a()
            androidx.appcompat.widget.ActionBarContextView r0 = (androidx.appcompat.widget.ActionBarContextView) r0
            r7.u = r0
        L_0x00d4:
            androidx.appcompat.widget.ActionBarContextView r0 = r7.u
            if (r0 == 0) goto L_0x0161
            r7.r()
            androidx.appcompat.widget.ActionBarContextView r0 = r7.u
            r0.d()
            com.fossil.e1 r0 = new com.fossil.e1
            androidx.appcompat.widget.ActionBarContextView r4 = r7.u
            android.content.Context r4 = r4.getContext()
            androidx.appcompat.widget.ActionBarContextView r5 = r7.u
            android.widget.PopupWindow r6 = r7.v
            if (r6 != 0) goto L_0x00ef
            goto L_0x00f0
        L_0x00ef:
            r3 = 0
        L_0x00f0:
            r0.<init>(r4, r5, r8, r3)
            android.view.Menu r3 = r0.c()
            boolean r8 = r8.a(r0, r3)
            if (r8 == 0) goto L_0x015f
            r0.i()
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            r8.a(r0)
            r7.t = r0
            boolean r8 = r7.C()
            r0 = 1065353216(0x3f800000, float:1.0)
            if (r8 == 0) goto L_0x0129
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            r1 = 0
            r8.setAlpha(r1)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            com.fossil.ha r8 = com.fossil.da.a(r8)
            r8.a(r0)
            r7.x = r8
            com.fossil.k0$g r0 = new com.fossil.k0$g
            r0.<init>()
            r8.a(r0)
            goto L_0x014f
        L_0x0129:
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            r8.setAlpha(r0)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            r8.setVisibility(r2)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            r0 = 32
            r8.sendAccessibilityEvent(r0)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            android.view.ViewParent r8 = r8.getParent()
            boolean r8 = r8 instanceof android.view.View
            if (r8 == 0) goto L_0x014f
            androidx.appcompat.widget.ActionBarContextView r8 = r7.u
            android.view.ViewParent r8 = r8.getParent()
            android.view.View r8 = (android.view.View) r8
            com.fossil.da.L(r8)
        L_0x014f:
            android.widget.PopupWindow r8 = r7.v
            if (r8 == 0) goto L_0x0161
            android.view.Window r8 = r7.f
            android.view.View r8 = r8.getDecorView()
            java.lang.Runnable r0 = r7.w
            r8.post(r0)
            goto L_0x0161
        L_0x015f:
            r7.t = r1
        L_0x0161:
            androidx.appcompat.view.ActionMode r8 = r7.t
            if (r8 == 0) goto L_0x016c
            com.fossil.j0 r0 = r7.h
            if (r0 == 0) goto L_0x016c
            r0.onSupportActionModeStarted(r8)
        L_0x016c:
            androidx.appcompat.view.ActionMode r8 = r7.t
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k0.b(androidx.appcompat.view.ActionMode$Callback):androidx.appcompat.view.ActionMode");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public final void a(CharSequence charSequence) {
        this.p = charSequence;
        o2 o2Var = this.q;
        if (o2Var != null) {
            o2Var.setWindowTitle(charSequence);
        } else if (B() != null) {
            B().b(charSequence);
        } else {
            TextView textView = this.B;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.p1.a
    public boolean a(p1 p1Var, MenuItem menuItem) {
        t a2;
        Window.Callback w2 = w();
        if (w2 == null || this.R || (a2 = a((Menu) p1Var.m())) == null) {
            return false;
        }
        return w2.onMenuItemSelected(a2.a, menuItem);
    }

    @DexIgnore
    @Override // com.fossil.p1.a
    public void a(p1 p1Var) {
        b(true);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public androidx.appcompat.view.ActionMode a(ActionMode.Callback callback) {
        j0 j0Var;
        if (callback != null) {
            androidx.appcompat.view.ActionMode actionMode = this.t;
            if (actionMode != null) {
                actionMode.a();
            }
            j jVar = new j(callback);
            ActionBar d2 = d();
            if (d2 != null) {
                androidx.appcompat.view.ActionMode a2 = d2.a(jVar);
                this.t = a2;
                if (!(a2 == null || (j0Var = this.h) == null)) {
                    j0Var.onSupportActionModeStarted(a2);
                }
            }
            if (this.t == null) {
                this.t = b(jVar);
            }
            return this.t;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        View decorView;
        Object obj = this.d;
        boolean z2 = true;
        if (((obj instanceof m9.a) || (obj instanceof l0)) && (decorView = this.f.getDecorView()) != null && m9.a(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.g.a().dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? a(keyCode, keyEvent) : d(keyCode, keyEvent);
    }

    @DexIgnore
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        if (i2 == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z2 = false;
            }
            this.N = z2;
        } else if (i2 == 82) {
            b(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean z3 = false;
        if (this.e0 == null) {
            String string = this.e.obtainStyledAttributes(h0.AppCompatTheme).getString(h0.AppCompatTheme_viewInflaterClass);
            if (string == null) {
                this.e0 = new n0();
            } else {
                try {
                    this.e0 = (n0) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.e0 = new n0();
                }
            }
        }
        if (g0) {
            if (!(attributeSet instanceof XmlPullParser)) {
                z3 = a((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                z3 = true;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        return this.e0.createView(view, str, context, attributeSet, z2, g0, true, l3.b());
    }

    @DexIgnore
    public final boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || da.F((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    @DexIgnore
    public final void a(t tVar, KeyEvent keyEvent) {
        int i2;
        ViewGroup.LayoutParams layoutParams;
        if (!tVar.o && !this.R) {
            if (tVar.a == 0) {
                if ((this.e.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback w2 = w();
            if (w2 == null || w2.onMenuOpened(tVar.a, tVar.j)) {
                WindowManager windowManager = (WindowManager) this.e.getSystemService("window");
                if (windowManager != null && b(tVar, keyEvent)) {
                    if (tVar.g == null || tVar.q) {
                        ViewGroup viewGroup = tVar.g;
                        if (viewGroup == null) {
                            if (!b(tVar) || tVar.g == null) {
                                return;
                            }
                        } else if (tVar.q && viewGroup.getChildCount() > 0) {
                            tVar.g.removeAllViews();
                        }
                        if (!a(tVar) || !tVar.a()) {
                            tVar.q = true;
                            return;
                        }
                        ViewGroup.LayoutParams layoutParams2 = tVar.h.getLayoutParams();
                        if (layoutParams2 == null) {
                            layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
                        }
                        tVar.g.setBackgroundResource(tVar.b);
                        ViewParent parent = tVar.h.getParent();
                        if (parent instanceof ViewGroup) {
                            ((ViewGroup) parent).removeView(tVar.h);
                        }
                        tVar.g.addView(tVar.h, layoutParams2);
                        if (!tVar.h.hasFocus()) {
                            tVar.h.requestFocus();
                        }
                    } else {
                        View view = tVar.i;
                        if (!(view == null || (layoutParams = view.getLayoutParams()) == null || layoutParams.width != -1)) {
                            i2 = -1;
                            tVar.n = false;
                            WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i2, -2, tVar.d, tVar.e, 1002, 8519680, -3);
                            layoutParams3.gravity = tVar.c;
                            layoutParams3.windowAnimations = tVar.f;
                            windowManager.addView(tVar.g, layoutParams3);
                            tVar.o = true;
                            return;
                        }
                    }
                    i2 = -2;
                    tVar.n = false;
                    WindowManager.LayoutParams layoutParams32 = new WindowManager.LayoutParams(i2, -2, tVar.d, tVar.e, 1002, 8519680, -3);
                    layoutParams32.gravity = tVar.c;
                    layoutParams32.windowAnimations = tVar.f;
                    windowManager.addView(tVar.g, layoutParams32);
                    tVar.o = true;
                    return;
                }
                return;
            }
            a(tVar, true);
        }
    }

    @DexIgnore
    public final boolean b(t tVar) {
        tVar.a(u());
        tVar.g = new s(tVar.l);
        tVar.c = 81;
        return true;
    }

    @DexIgnore
    public final void b(boolean z2) {
        o2 o2Var = this.q;
        if (o2Var == null || !o2Var.c() || (ViewConfiguration.get(this.e).hasPermanentMenuKey() && !this.q.d())) {
            t a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback w2 = w();
        if (this.q.a() && z2) {
            this.q.e();
            if (!this.R) {
                w2.onPanelClosed(108, a(0, true).j);
            }
        } else if (w2 != null && !this.R) {
            if (this.Y && (this.Z & 1) != 0) {
                this.f.getDecorView().removeCallbacks(this.a0);
                this.a0.run();
            }
            t a3 = a(0, true);
            p1 p1Var = a3.j;
            if (p1Var != null && !a3.r && w2.onPreparePanel(0, a3.i, p1Var)) {
                w2.onMenuOpened(108, a3.j);
                this.q.f();
            }
        }
    }

    @DexIgnore
    public final boolean b(t tVar, KeyEvent keyEvent) {
        o2 o2Var;
        o2 o2Var2;
        o2 o2Var3;
        if (this.R) {
            return false;
        }
        if (tVar.m) {
            return true;
        }
        t tVar2 = this.M;
        if (!(tVar2 == null || tVar2 == tVar)) {
            a(tVar2, false);
        }
        Window.Callback w2 = w();
        if (w2 != null) {
            tVar.i = w2.onCreatePanelView(tVar.a);
        }
        int i2 = tVar.a;
        boolean z2 = i2 == 0 || i2 == 108;
        if (z2 && (o2Var3 = this.q) != null) {
            o2Var3.b();
        }
        if (tVar.i == null && (!z2 || !(B() instanceof p0))) {
            if (tVar.j == null || tVar.r) {
                if (tVar.j == null && (!c(tVar) || tVar.j == null)) {
                    return false;
                }
                if (z2 && this.q != null) {
                    if (this.r == null) {
                        this.r = new i();
                    }
                    this.q.a(tVar.j, this.r);
                }
                tVar.j.s();
                if (!w2.onCreatePanelMenu(tVar.a, tVar.j)) {
                    tVar.a((p1) null);
                    if (z2 && (o2Var2 = this.q) != null) {
                        o2Var2.a(null, this.r);
                    }
                    return false;
                }
                tVar.r = false;
            }
            tVar.j.s();
            Bundle bundle = tVar.s;
            if (bundle != null) {
                tVar.j.c(bundle);
                tVar.s = null;
            }
            if (!w2.onPreparePanel(0, tVar.i, tVar.j)) {
                if (z2 && (o2Var = this.q) != null) {
                    o2Var.a(null, this.r);
                }
                tVar.j.r();
                return false;
            }
            boolean z3 = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            tVar.p = z3;
            tVar.j.setQwertyMode(z3);
            tVar.j.r();
        }
        tVar.m = true;
        tVar.n = false;
        this.M = tVar;
        return true;
    }

    @DexIgnore
    public final boolean a(t tVar) {
        View view = tVar.i;
        if (view != null) {
            tVar.h = view;
            return true;
        } else if (tVar.j == null) {
            return false;
        } else {
            if (this.s == null) {
                this.s = new u();
            }
            View view2 = (View) tVar.a(this.s);
            tVar.h = view2;
            if (view2 != null) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void a(t tVar, boolean z2) {
        ViewGroup viewGroup;
        o2 o2Var;
        if (!z2 || tVar.a != 0 || (o2Var = this.q) == null || !o2Var.a()) {
            WindowManager windowManager = (WindowManager) this.e.getSystemService("window");
            if (!(windowManager == null || !tVar.o || (viewGroup = tVar.g) == null)) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    a(tVar.a, tVar, (Menu) null);
                }
            }
            tVar.m = false;
            tVar.n = false;
            tVar.o = false;
            tVar.h = null;
            tVar.q = true;
            if (this.M == tVar) {
                this.M = null;
                return;
            }
            return;
        }
        b(tVar.j);
    }

    @DexIgnore
    public void a(int i2, t tVar, Menu menu) {
        if (menu == null) {
            if (tVar == null && i2 >= 0) {
                t[] tVarArr = this.L;
                if (i2 < tVarArr.length) {
                    tVar = tVarArr[i2];
                }
            }
            if (tVar != null) {
                menu = tVar.j;
            }
        }
        if ((tVar == null || tVar.o) && !this.R) {
            this.g.a().onPanelClosed(i2, menu);
        }
    }

    @DexIgnore
    public t a(Menu menu) {
        t[] tVarArr = this.L;
        int length = tVarArr != null ? tVarArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            t tVar = tVarArr[i2];
            if (tVar != null && tVar.j == menu) {
                return tVar;
            }
        }
        return null;
    }

    @DexIgnore
    public t a(int i2, boolean z2) {
        t[] tVarArr = this.L;
        if (tVarArr == null || tVarArr.length <= i2) {
            t[] tVarArr2 = new t[(i2 + 1)];
            if (tVarArr != null) {
                System.arraycopy(tVarArr, 0, tVarArr2, 0, tVarArr.length);
            }
            this.L = tVarArr2;
            tVarArr = tVarArr2;
        }
        t tVar = tVarArr[i2];
        if (tVar != null) {
            return tVar;
        }
        t tVar2 = new t(i2);
        tVarArr[i2] = tVar2;
        return tVar2;
    }

    @DexIgnore
    public void b(p1 p1Var) {
        if (!this.K) {
            this.K = true;
            this.q.g();
            Window.Callback w2 = w();
            if (w2 != null && !this.R) {
                w2.onPanelClosed(108, p1Var);
            }
            this.K = false;
        }
    }

    @DexIgnore
    public final boolean a(t tVar, int i2, KeyEvent keyEvent, int i3) {
        p1 p1Var;
        boolean z2 = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((tVar.m || b(tVar, keyEvent)) && (p1Var = tVar.j) != null) {
            z2 = p1Var.performShortcut(i2, keyEvent, i3);
        }
        if (z2 && (i3 & 1) == 0 && this.q == null) {
            a(tVar, true);
        }
        return z2;
    }

    @DexIgnore
    public final boolean b(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        t a2 = a(i2, true);
        if (!a2.o) {
            return b(a2, keyEvent);
        }
        return false;
    }

    @DexIgnore
    public final int a(la laVar, Rect rect) {
        int i2;
        boolean z2;
        int i3;
        int i4;
        boolean z3;
        int i5 = 0;
        if (laVar != null) {
            i2 = laVar.g();
        } else {
            i2 = rect != null ? rect.top : 0;
        }
        ActionBarContextView actionBarContextView = this.u;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.u.getLayoutParams();
            boolean z4 = true;
            if (this.u.isShown()) {
                if (this.c0 == null) {
                    this.c0 = new Rect();
                    this.d0 = new Rect();
                }
                Rect rect2 = this.c0;
                Rect rect3 = this.d0;
                if (laVar == null) {
                    rect2.set(rect);
                } else {
                    rect2.set(laVar.e(), laVar.g(), laVar.f(), laVar.d());
                }
                m3.a(this.A, rect2, rect3);
                int i6 = rect2.top;
                int i7 = rect2.left;
                int i8 = rect2.right;
                la w2 = da.w(this.A);
                if (w2 == null) {
                    i3 = 0;
                } else {
                    i3 = w2.e();
                }
                if (w2 == null) {
                    i4 = 0;
                } else {
                    i4 = w2.f();
                }
                if (marginLayoutParams.topMargin == i6 && marginLayoutParams.leftMargin == i7 && marginLayoutParams.rightMargin == i8) {
                    z3 = false;
                } else {
                    marginLayoutParams.topMargin = i6;
                    marginLayoutParams.leftMargin = i7;
                    marginLayoutParams.rightMargin = i8;
                    z3 = true;
                }
                if (i6 <= 0 || this.C != null) {
                    View view = this.C;
                    if (view != null) {
                        ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                        if (!(marginLayoutParams2.height == marginLayoutParams.topMargin && marginLayoutParams2.leftMargin == i3 && marginLayoutParams2.rightMargin == i4)) {
                            marginLayoutParams2.height = marginLayoutParams.topMargin;
                            marginLayoutParams2.leftMargin = i3;
                            marginLayoutParams2.rightMargin = i4;
                            this.C.setLayoutParams(marginLayoutParams2);
                        }
                    }
                } else {
                    View view2 = new View(this.e);
                    this.C = view2;
                    view2.setVisibility(8);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, marginLayoutParams.topMargin, 51);
                    layoutParams.leftMargin = i3;
                    layoutParams.rightMargin = i4;
                    this.A.addView(this.C, -1, layoutParams);
                }
                if (this.C == null) {
                    z4 = false;
                }
                if (z4 && this.C.getVisibility() != 0) {
                    b(this.C);
                }
                if (!this.H && z4) {
                    i2 = 0;
                }
                z2 = z4;
                z4 = z3;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z2 = false;
            } else {
                z2 = false;
                z4 = false;
            }
            if (z4) {
                this.u.setLayoutParams(marginLayoutParams);
            }
        }
        View view3 = this.C;
        if (view3 != null) {
            if (!z2) {
                i5 = 8;
            }
            view3.setVisibility(i5);
        }
        return i2;
    }

    @DexIgnore
    public final void b(View view) {
        int i2;
        if ((da.z(view) & 8192) != 0) {
            i2 = v6.a(this.e, a0.abc_decor_view_status_guard_light);
        } else {
            i2 = v6.a(this.e, a0.abc_decor_view_status_guard);
        }
        view.setBackgroundColor(i2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public int b() {
        return this.S;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean b(int r7, boolean r8) {
        /*
            r6 = this;
            android.content.Context r0 = r6.e
            r1 = 0
            android.content.res.Configuration r0 = r6.a(r0, r7, r1)
            boolean r2 = r6.y()
            android.content.Context r3 = r6.e
            android.content.res.Resources r3 = r3.getResources()
            android.content.res.Configuration r3 = r3.getConfiguration()
            int r3 = r3.uiMode
            r3 = r3 & 48
            int r0 = r0.uiMode
            r0 = r0 & 48
            r4 = 1
            if (r3 == r0) goto L_0x0047
            if (r8 == 0) goto L_0x0047
            if (r2 != 0) goto L_0x0047
            boolean r8 = r6.O
            if (r8 == 0) goto L_0x0047
            boolean r8 = com.fossil.k0.i0
            if (r8 != 0) goto L_0x0030
            boolean r8 = r6.P
            if (r8 == 0) goto L_0x0047
        L_0x0030:
            java.lang.Object r8 = r6.d
            boolean r5 = r8 instanceof android.app.Activity
            if (r5 == 0) goto L_0x0047
            android.app.Activity r8 = (android.app.Activity) r8
            boolean r8 = r8.isChild()
            if (r8 != 0) goto L_0x0047
            java.lang.Object r8 = r6.d
            android.app.Activity r8 = (android.app.Activity) r8
            com.fossil.g6.d(r8)
            r8 = 1
            goto L_0x0048
        L_0x0047:
            r8 = 0
        L_0x0048:
            if (r8 != 0) goto L_0x0050
            if (r3 == r0) goto L_0x0050
            r6.a(r0, r2, r1)
            goto L_0x0051
        L_0x0050:
            r4 = r8
        L_0x0051:
            if (r4 == 0) goto L_0x005e
            java.lang.Object r8 = r6.d
            boolean r0 = r8 instanceof androidx.appcompat.app.AppCompatActivity
            if (r0 == 0) goto L_0x005e
            androidx.appcompat.app.AppCompatActivity r8 = (androidx.appcompat.app.AppCompatActivity) r8
            r8.onNightModeChanged(r7)
        L_0x005e:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k0.b(int, boolean):boolean");
    }

    @DexIgnore
    public final boolean a(boolean z2) {
        if (this.R) {
            return false;
        }
        int n2 = n();
        boolean b2 = b(a(this.e, n2), z2);
        if (n2 == 0) {
            d(this.e).e();
        } else {
            m mVar = this.W;
            if (mVar != null) {
                mVar.a();
            }
        }
        if (n2 == 3) {
            c(this.e).e();
        } else {
            m mVar2 = this.X;
            if (mVar2 != null) {
                mVar2.a();
            }
        }
        return b2;
    }

    @DexIgnore
    public int a(Context context, int i2) {
        if (i2 == -100) {
            return -1;
        }
        if (i2 != -1) {
            if (i2 != 0) {
                if (!(i2 == 1 || i2 == 2)) {
                    if (i2 == 3) {
                        return c(context).c();
                    }
                    throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
                }
            } else if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) context.getApplicationContext().getSystemService(UiModeManager.class)).getNightMode() != 0) {
                return d(context).c();
            } else {
                return -1;
            }
        }
        return i2;
    }

    @DexIgnore
    public final Configuration a(Context context, int i2, Configuration configuration) {
        int i3 = i2 != 1 ? i2 != 2 ? context.getApplicationContext().getResources().getConfiguration().uiMode & 48 : 32 : 16;
        Configuration configuration2 = new Configuration();
        configuration2.fontScale = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (configuration != null) {
            configuration2.setTo(configuration);
        }
        configuration2.uiMode = i3 | (configuration2.uiMode & -49);
        return configuration2;
    }

    @DexIgnore
    public final void a(int i2, boolean z2, Configuration configuration) {
        Resources resources = this.e.getResources();
        Configuration configuration2 = new Configuration(resources.getConfiguration());
        if (configuration != null) {
            configuration2.updateFrom(configuration);
        }
        configuration2.uiMode = i2 | (resources.getConfiguration().uiMode & -49);
        resources.updateConfiguration(configuration2, null);
        if (Build.VERSION.SDK_INT < 26) {
            o0.a(resources);
        }
        int i3 = this.T;
        if (i3 != 0) {
            this.e.setTheme(i3);
            if (Build.VERSION.SDK_INT >= 23) {
                this.e.getTheme().applyStyle(this.T, true);
            }
        }
        if (z2) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                Activity activity = (Activity) obj;
                if (activity instanceof LifecycleOwner) {
                    if (((LifecycleOwner) activity).getLifecycle().a().isAtLeast(Lifecycle.State.STARTED)) {
                        activity.onConfigurationChanged(configuration2);
                    }
                } else if (this.Q) {
                    activity.onConfigurationChanged(configuration2);
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public final ActionBarDrawerToggle$Delegate a() {
        return new h(this);
    }

    @DexIgnore
    public static Configuration a(Configuration configuration, Configuration configuration2) {
        Configuration configuration3 = new Configuration();
        configuration3.fontScale = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (!(configuration2 == null || configuration.diff(configuration2) == 0)) {
            float f2 = configuration.fontScale;
            float f3 = configuration2.fontScale;
            if (f2 != f3) {
                configuration3.fontScale = f3;
            }
            int i2 = configuration.mcc;
            int i3 = configuration2.mcc;
            if (i2 != i3) {
                configuration3.mcc = i3;
            }
            int i4 = configuration.mnc;
            int i5 = configuration2.mnc;
            if (i4 != i5) {
                configuration3.mnc = i5;
            }
            if (Build.VERSION.SDK_INT >= 24) {
                p.a(configuration, configuration2, configuration3);
            } else if (!z8.a(configuration.locale, configuration2.locale)) {
                configuration3.locale = configuration2.locale;
            }
            int i6 = configuration.touchscreen;
            int i7 = configuration2.touchscreen;
            if (i6 != i7) {
                configuration3.touchscreen = i7;
            }
            int i8 = configuration.keyboard;
            int i9 = configuration2.keyboard;
            if (i8 != i9) {
                configuration3.keyboard = i9;
            }
            int i10 = configuration.keyboardHidden;
            int i11 = configuration2.keyboardHidden;
            if (i10 != i11) {
                configuration3.keyboardHidden = i11;
            }
            int i12 = configuration.navigation;
            int i13 = configuration2.navigation;
            if (i12 != i13) {
                configuration3.navigation = i13;
            }
            int i14 = configuration.navigationHidden;
            int i15 = configuration2.navigationHidden;
            if (i14 != i15) {
                configuration3.navigationHidden = i15;
            }
            int i16 = configuration.orientation;
            int i17 = configuration2.orientation;
            if (i16 != i17) {
                configuration3.orientation = i17;
            }
            int i18 = configuration.screenLayout & 15;
            int i19 = configuration2.screenLayout;
            if (i18 != (i19 & 15)) {
                configuration3.screenLayout |= i19 & 15;
            }
            int i20 = configuration.screenLayout & 192;
            int i21 = configuration2.screenLayout;
            if (i20 != (i21 & 192)) {
                configuration3.screenLayout |= i21 & 192;
            }
            int i22 = configuration.screenLayout & 48;
            int i23 = configuration2.screenLayout;
            if (i22 != (i23 & 48)) {
                configuration3.screenLayout |= i23 & 48;
            }
            int i24 = configuration.screenLayout & 768;
            int i25 = configuration2.screenLayout;
            if (i24 != (i25 & 768)) {
                configuration3.screenLayout |= i25 & 768;
            }
            if (Build.VERSION.SDK_INT >= 26) {
                q.a(configuration, configuration2, configuration3);
            }
            int i26 = configuration.uiMode & 15;
            int i27 = configuration2.uiMode;
            if (i26 != (i27 & 15)) {
                configuration3.uiMode |= i27 & 15;
            }
            int i28 = configuration.uiMode & 48;
            int i29 = configuration2.uiMode;
            if (i28 != (i29 & 48)) {
                configuration3.uiMode |= i29 & 48;
            }
            int i30 = configuration.screenWidthDp;
            int i31 = configuration2.screenWidthDp;
            if (i30 != i31) {
                configuration3.screenWidthDp = i31;
            }
            int i32 = configuration.screenHeightDp;
            int i33 = configuration2.screenHeightDp;
            if (i32 != i33) {
                configuration3.screenHeightDp = i33;
            }
            int i34 = configuration.smallestScreenWidthDp;
            int i35 = configuration2.smallestScreenWidthDp;
            if (i34 != i35) {
                configuration3.smallestScreenWidthDp = i35;
            }
            if (Build.VERSION.SDK_INT >= 17) {
                o.a(configuration, configuration2, configuration3);
            }
        }
        return configuration3;
    }
}
