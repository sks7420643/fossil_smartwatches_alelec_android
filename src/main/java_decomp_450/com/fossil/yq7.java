package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.misfit.frameworks.buttonservice.ButtonService;
import io.flutter.plugin.common.StandardMessageCodec;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yq7 implements ar7, zq7, Cloneable, ByteChannel {
    @DexIgnore
    public nr7 a;
    @DexIgnore
    public long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends OutputStream {
        @DexIgnore
        public /* final */ /* synthetic */ yq7 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(yq7 yq7) {
            this.a = yq7;
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() {
        }

        @DexIgnore
        public String toString() {
            return this.a + ".outputStream()";
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(int i) {
            this.a.writeByte(i);
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) {
            ee7.b(bArr, "data");
            this.a.write(bArr, i, i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public String b(long j) throws EOFException {
        if (j >= 0) {
            long j2 = Long.MAX_VALUE;
            if (j != Long.MAX_VALUE) {
                j2 = j + 1;
            }
            byte b2 = (byte) 10;
            long a2 = a(b2, 0, j2);
            if (a2 != -1) {
                return ur7.a(this, a2);
            }
            if (j2 < x() && e(j2 - 1) == ((byte) 13) && e(j2) == b2) {
                return ur7.a(this, j2);
            }
            yq7 yq7 = new yq7();
            a(yq7, 0, Math.min((long) 32, x()));
            throw new EOFException("\\n not found: limit=" + Math.min(x(), j) + " content=" + yq7.o().hex() + '\u2026');
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7, com.fossil.ar7
    public yq7 buffer() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.qr7, com.fossil.sr7, java.lang.AutoCloseable, java.io.Closeable, java.nio.channels.Channel
    public void close() {
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public boolean d(long j) {
        return this.b >= j;
    }

    @DexIgnore
    public final byte e(long j) {
        vq7.a(x(), j, 1);
        nr7 nr7 = this.a;
        if (nr7 == null) {
            ee7.a();
            throw null;
        } else if (x() - j < j) {
            long x = x();
            while (x > j) {
                nr7 = nr7.g;
                if (nr7 != null) {
                    x -= (long) (nr7.c - nr7.b);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            if (nr7 != null) {
                return nr7.a[(int) ((((long) nr7.b) + j) - x)];
            }
            ee7.a();
            throw null;
        } else {
            long j2 = 0;
            while (true) {
                int i = nr7.c;
                int i2 = nr7.b;
                long j3 = ((long) (i - i2)) + j2;
                if (j3 <= j) {
                    nr7 = nr7.f;
                    if (nr7 != null) {
                        j2 = j3;
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (nr7 != null) {
                    return nr7.a[(int) ((((long) i2) + j) - j2)];
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof yq7) {
                yq7 yq7 = (yq7) obj;
                if (x() == yq7.x()) {
                    if (x() != 0) {
                        nr7 nr7 = this.a;
                        if (nr7 != null) {
                            nr7 nr72 = yq7.a;
                            if (nr72 != null) {
                                int i = nr7.b;
                                int i2 = nr72.b;
                                long j = 0;
                                while (j < x()) {
                                    long min = (long) Math.min(nr7.c - i, nr72.c - i2);
                                    long j2 = 0;
                                    while (j2 < min) {
                                        int i3 = i + 1;
                                        int i4 = i2 + 1;
                                        if (nr7.a[i] == nr72.a[i2]) {
                                            j2++;
                                            i = i3;
                                            i2 = i4;
                                        }
                                    }
                                    if (i == nr7.c) {
                                        nr7 nr73 = nr7.f;
                                        if (nr73 != null) {
                                            i = nr73.b;
                                            nr7 = nr73;
                                        } else {
                                            ee7.a();
                                            throw null;
                                        }
                                    }
                                    if (i2 == nr72.c) {
                                        nr72 = nr72.f;
                                        if (nr72 != null) {
                                            i2 = nr72.b;
                                        } else {
                                            ee7.a();
                                            throw null;
                                        }
                                    }
                                    j += min;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public byte[] f() {
        return f(x());
    }

    @DexIgnore
    @Override // com.fossil.zq7, com.fossil.qr7, java.io.Flushable
    public void flush() {
    }

    @DexIgnore
    public String g(long j) throws EOFException {
        return a(j, sg7.a);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public yq7 getBuffer() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public boolean h() {
        return this.b == 0;
    }

    @DexIgnore
    public int hashCode() {
        nr7 nr7 = this.a;
        if (nr7 == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = nr7.c;
            for (int i3 = nr7.b; i3 < i2; i3++) {
                i = (i * 31) + nr7.a[i3];
            }
            nr7 = nr7.f;
            if (nr7 == null) {
                ee7.a();
                throw null;
            }
        } while (nr7 != this.a);
        return i;
    }

    @DexIgnore
    public boolean isOpen() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 j() {
        return this;
    }

    @DexIgnore
    public final void k() {
        skip(x());
    }

    @DexIgnore
    public final long l() {
        long x = x();
        if (x == 0) {
            return 0;
        }
        nr7 nr7 = this.a;
        if (nr7 != null) {
            nr7 nr72 = nr7.g;
            if (nr72 != null) {
                int i = nr72.c;
                if (i < 8192 && nr72.e) {
                    x -= (long) (i - nr72.b);
                }
                return x;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a9, code lost:
        if (r10 != r11) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ab, code lost:
        r17.a = r16.b();
        com.fossil.or7.c.a(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b9, code lost:
        r16.b = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00bd, code lost:
        if (r7 != false) goto L_0x00c3;
     */
    @DexIgnore
    @Override // com.fossil.ar7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m() throws java.io.EOFException {
        /*
            r17 = this;
            r0 = r17
            long r1 = r17.x()
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00d6
            r1 = -7
            r5 = 0
            r6 = 0
            r7 = 0
        L_0x0011:
            com.fossil.nr7 r8 = r0.a
            if (r8 == 0) goto L_0x00d1
            byte[] r9 = r8.a
            int r10 = r8.b
            int r11 = r8.c
        L_0x001b:
            r12 = 1
            if (r10 >= r11) goto L_0x00a6
            byte r13 = r9[r10]
            r14 = 48
            byte r14 = (byte) r14
            if (r13 < r14) goto L_0x0071
            r15 = 57
            byte r15 = (byte) r15
            if (r13 > r15) goto L_0x0071
            int r14 = r14 - r13
            r15 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r12 = (r3 > r15 ? 1 : (r3 == r15 ? 0 : -1))
            if (r12 < 0) goto L_0x0046
            r15 = r7
            r16 = r8
            if (r12 != 0) goto L_0x003f
            long r7 = (long) r14
            int r12 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r12 >= 0) goto L_0x003f
            goto L_0x0046
        L_0x003f:
            r7 = 10
            long r3 = r3 * r7
            long r7 = (long) r14
            long r3 = r3 + r7
            goto L_0x007f
        L_0x0046:
            com.fossil.yq7 r1 = new com.fossil.yq7
            r1.<init>()
            r1.i(r3)
            r1.writeByte(r13)
            if (r6 != 0) goto L_0x0056
            r1.readByte()
        L_0x0056:
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Number too large: "
            r3.append(r4)
            java.lang.String r1 = r1.v()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x0071:
            r15 = r7
            r16 = r8
            r7 = 45
            byte r7 = (byte) r7
            if (r13 != r7) goto L_0x0087
            if (r5 != 0) goto L_0x0087
            r6 = 1
            long r1 = r1 - r6
            r6 = 1
        L_0x007f:
            int r10 = r10 + 1
            int r5 = r5 + 1
            r7 = r15
            r8 = r16
            goto L_0x001b
        L_0x0087:
            if (r5 == 0) goto L_0x008b
            r7 = 1
            goto L_0x00a9
        L_0x008b:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Expected leading [0-9] or '-' character but was 0x"
            r2.append(r3)
            java.lang.String r3 = com.fossil.vq7.a(r13)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x00a6:
            r15 = r7
            r16 = r8
        L_0x00a9:
            if (r10 != r11) goto L_0x00b9
            com.fossil.nr7 r8 = r16.b()
            r0.a = r8
            com.fossil.or7 r8 = com.fossil.or7.c
            r9 = r16
            r8.a(r9)
            goto L_0x00bd
        L_0x00b9:
            r9 = r16
            r9.b = r10
        L_0x00bd:
            if (r7 != 0) goto L_0x00c3
            com.fossil.nr7 r8 = r0.a
            if (r8 != 0) goto L_0x0011
        L_0x00c3:
            long r1 = r17.x()
            long r7 = (long) r5
            long r1 = r1 - r7
            r0.j(r1)
            if (r6 == 0) goto L_0x00cf
            goto L_0x00d0
        L_0x00cf:
            long r3 = -r3
        L_0x00d0:
            return r3
        L_0x00d1:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x00d6:
            java.io.EOFException r1 = new java.io.EOFException
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yq7.m():long");
    }

    @DexIgnore
    public final yq7 n() {
        yq7 yq7 = new yq7();
        if (x() != 0) {
            nr7 nr7 = this.a;
            if (nr7 != null) {
                nr7 c = nr7.c();
                yq7.a = c;
                c.g = c;
                c.f = c;
                nr7 nr72 = nr7.f;
                while (nr72 != nr7) {
                    nr7 nr73 = c.g;
                    if (nr73 == null) {
                        ee7.a();
                        throw null;
                    } else if (nr72 != null) {
                        nr73.a(nr72.c());
                        nr72 = nr72.f;
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                yq7.j(x());
            } else {
                ee7.a();
                throw null;
            }
        }
        return yq7;
    }

    @DexIgnore
    public br7 o() {
        return a(x());
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public String p() throws EOFException {
        return b(Long.MAX_VALUE);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public ar7 peek() {
        return ir7.a(new kr7(this));
    }

    @DexIgnore
    public int q() throws EOFException {
        return vq7.a(readInt());
    }

    @DexIgnore
    public short r() throws EOFException {
        return vq7.a(readShort());
    }

    @DexIgnore
    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) throws IOException {
        ee7.b(byteBuffer, "sink");
        nr7 nr7 = this.a;
        if (nr7 == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), nr7.c - nr7.b);
        byteBuffer.put(nr7.a, nr7.b, min);
        int i = nr7.b + min;
        nr7.b = i;
        this.b -= (long) min;
        if (i == nr7.c) {
            this.a = nr7.b();
            or7.c.a(nr7);
        }
        return min;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public byte readByte() throws EOFException {
        if (x() != 0) {
            nr7 nr7 = this.a;
            if (nr7 != null) {
                int i = nr7.b;
                int i2 = nr7.c;
                int i3 = i + 1;
                byte b2 = nr7.a[i];
                j(x() - 1);
                if (i3 == i2) {
                    this.a = nr7.b();
                    or7.c.a(nr7);
                } else {
                    nr7.b = i3;
                }
                return b2;
            }
            ee7.a();
            throw null;
        }
        throw new EOFException();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public void readFully(byte[] bArr) throws EOFException {
        ee7.b(bArr, "sink");
        int i = 0;
        while (i < bArr.length) {
            int read = read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public int readInt() throws EOFException {
        if (x() >= 4) {
            nr7 nr7 = this.a;
            if (nr7 != null) {
                int i = nr7.b;
                int i2 = nr7.c;
                if (((long) (i2 - i)) < 4) {
                    return ((readByte() & 255) << 24) | ((readByte() & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((readByte() & 255) << 8) | (readByte() & 255);
                }
                byte[] bArr = nr7.a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY);
                int i5 = i4 + 1;
                byte b3 = b2 | ((bArr[i4] & 255) << 8);
                int i6 = i5 + 1;
                byte b4 = b3 | (bArr[i5] & 255);
                j(x() - 4);
                if (i6 == i2) {
                    this.a = nr7.b();
                    or7.c.a(nr7);
                } else {
                    nr7.b = i6;
                }
                return b4;
            }
            ee7.a();
            throw null;
        }
        throw new EOFException();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public short readShort() throws EOFException {
        if (x() >= 2) {
            nr7 nr7 = this.a;
            if (nr7 != null) {
                int i = nr7.b;
                int i2 = nr7.c;
                if (i2 - i < 2) {
                    return (short) (((readByte() & 255) << 8) | (readByte() & 255));
                }
                byte[] bArr = nr7.a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
                j(x() - 2);
                if (i4 == i2) {
                    this.a = nr7.b();
                    or7.c.a(nr7);
                } else {
                    nr7.b = i4;
                }
                return (short) b2;
            }
            ee7.a();
            throw null;
        }
        throw new EOFException();
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public OutputStream s() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public void skip(long j) throws EOFException {
        while (j > 0) {
            nr7 nr7 = this.a;
            if (nr7 != null) {
                int min = (int) Math.min(j, (long) (nr7.c - nr7.b));
                long j2 = (long) min;
                j(x() - j2);
                j -= j2;
                int i = nr7.b + min;
                nr7.b = i;
                if (i == nr7.c) {
                    this.a = nr7.b();
                    or7.c.a(nr7);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0097, code lost:
        if (r8 != r9) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0099, code lost:
        r15.a = r6.b();
        com.fossil.or7.c.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a5, code lost:
        r6.b = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a7, code lost:
        if (r1 != false) goto L_0x00ad;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x007c A[SYNTHETIC] */
    @Override // com.fossil.ar7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long t() throws java.io.EOFException {
        /*
            r15 = this;
            long r0 = r15.x()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x00bc
            r0 = 0
            r4 = r2
            r1 = 0
        L_0x000d:
            com.fossil.nr7 r6 = r15.a
            if (r6 == 0) goto L_0x00b7
            byte[] r7 = r6.a
            int r8 = r6.b
            int r9 = r6.c
        L_0x0017:
            if (r8 >= r9) goto L_0x0097
            byte r10 = r7[r8]
            r11 = 48
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0028
            r12 = 57
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0028
            int r11 = r10 - r11
            goto L_0x0042
        L_0x0028:
            r11 = 97
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0037
            r12 = 102(0x66, float:1.43E-43)
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0037
        L_0x0032:
            int r11 = r10 - r11
            int r11 = r11 + 10
            goto L_0x0042
        L_0x0037:
            r11 = 65
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0078
            r12 = 70
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0078
            goto L_0x0032
        L_0x0042:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r14 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r14 != 0) goto L_0x0052
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r0 = r0 + 1
            goto L_0x0017
        L_0x0052:
            com.fossil.yq7 r0 = new com.fossil.yq7
            r0.<init>()
            r0.c(r4)
            r0.writeByte(r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Number too large: "
            r2.append(r3)
            java.lang.String r0 = r0.v()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x0078:
            if (r0 == 0) goto L_0x007c
            r1 = 1
            goto L_0x0097
        L_0x007c:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected leading [0-9a-fA-F] character but was 0x"
            r1.append(r2)
            java.lang.String r2 = com.fossil.vq7.a(r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0097:
            if (r8 != r9) goto L_0x00a5
            com.fossil.nr7 r7 = r6.b()
            r15.a = r7
            com.fossil.or7 r7 = com.fossil.or7.c
            r7.a(r6)
            goto L_0x00a7
        L_0x00a5:
            r6.b = r8
        L_0x00a7:
            if (r1 != 0) goto L_0x00ad
            com.fossil.nr7 r6 = r15.a
            if (r6 != 0) goto L_0x000d
        L_0x00ad:
            long r1 = r15.x()
            long r6 = (long) r0
            long r1 = r1 - r6
            r15.j(r1)
            return r4
        L_0x00b7:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x00bc:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yq7.t():long");
    }

    @DexIgnore
    public String toString() {
        return y().toString();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public InputStream u() {
        return new a(this);
    }

    @DexIgnore
    public String v() {
        return a(this.b, sg7.a);
    }

    @DexIgnore
    public int w() throws EOFException {
        byte b2;
        int i;
        byte b3;
        if (x() != 0) {
            byte e = e(0);
            int i2 = 1;
            if ((e & 128) == 0) {
                b3 = e & Byte.MAX_VALUE;
                i = 1;
                b2 = 0;
            } else if ((e & 224) == 192) {
                b3 = e & 31;
                i = 2;
                b2 = 128;
            } else if ((e & 240) == 224) {
                b3 = e & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY;
                i = 3;
                b2 = StandardMessageCodec.NULL;
            } else if ((e & 248) == 240) {
                b3 = e & 7;
                i = 4;
                b2 = StandardMessageCodec.NULL;
            } else {
                skip(1);
                return 65533;
            }
            long j = (long) i;
            if (x() >= j) {
                while (i2 < i) {
                    long j2 = (long) i2;
                    byte e2 = e(j2);
                    if ((e2 & 192) == 128) {
                        b3 = (b3 << 6) | (e2 & 63);
                        i2++;
                    } else {
                        skip(j2);
                        return 65533;
                    }
                }
                skip(j);
                if (b3 > 1114111) {
                    return 65533;
                }
                if ((55296 <= b3 && 57343 >= b3) || b3 < b2) {
                    return 65533;
                }
                return b3;
            }
            throw new EOFException("size < " + i + ": " + x() + " (to read code point prefixed 0x" + vq7.a(e) + ')');
        }
        throw new EOFException();
    }

    @DexIgnore
    public final long x() {
        return this.b;
    }

    @DexIgnore
    public final br7 y() {
        if (x() <= ((long) Integer.MAX_VALUE)) {
            return a((int) x());
        }
        throw new IllegalStateException(("size > Int.MAX_VALUE: " + x()).toString());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends InputStream {
        @DexIgnore
        public /* final */ /* synthetic */ yq7 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(yq7 yq7) {
            this.a = yq7;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            return (int) Math.min(this.a.x(), (long) Integer.MAX_VALUE);
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() {
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            if (this.a.x() > 0) {
                return this.a.readByte() & 255;
            }
            return -1;
        }

        @DexIgnore
        public String toString() {
            return this.a + ".inputStream()";
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            ee7.b(bArr, "sink");
            return this.a.read(bArr, i, i2);
        }
    }

    @DexIgnore
    public yq7 c(int i) {
        if (i < 128) {
            writeByte(i);
        } else if (i < 2048) {
            nr7 b2 = b(2);
            byte[] bArr = b2.a;
            int i2 = b2.c;
            bArr[i2] = (byte) ((i >> 6) | 192);
            bArr[i2 + 1] = (byte) ((i & 63) | 128);
            b2.c = i2 + 2;
            j(x() + 2);
        } else if (55296 <= i && 57343 >= i) {
            writeByte(63);
        } else if (i < 65536) {
            nr7 b3 = b(3);
            byte[] bArr2 = b3.a;
            int i3 = b3.c;
            bArr2[i3] = (byte) ((i >> 12) | 224);
            bArr2[i3 + 1] = (byte) (((i >> 6) & 63) | 128);
            bArr2[i3 + 2] = (byte) ((i & 63) | 128);
            b3.c = i3 + 3;
            j(x() + 3);
        } else if (i <= 1114111) {
            nr7 b4 = b(4);
            byte[] bArr3 = b4.a;
            int i4 = b4.c;
            bArr3[i4] = (byte) ((i >> 18) | 240);
            bArr3[i4 + 1] = (byte) (((i >> 12) & 63) | 128);
            bArr3[i4 + 2] = (byte) (((i >> 6) & 63) | 128);
            bArr3[i4 + 3] = (byte) ((i & 63) | 128);
            b4.c = i4 + 4;
            j(x() + 4);
        } else {
            throw new IllegalArgumentException("Unexpected code point: 0x" + vq7.b(i));
        }
        return this;
    }

    @DexIgnore
    @Override // java.lang.Object
    public yq7 clone() {
        return n();
    }

    @DexIgnore
    @Override // com.fossil.qr7, com.fossil.sr7
    public tr7 d() {
        return tr7.d;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public byte[] f(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (x() >= j) {
            byte[] bArr = new byte[((int) j)];
            readFully(bArr);
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public void h(long j) throws EOFException {
        if (this.b < j) {
            throw new EOFException();
        }
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 i(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            writeByte(48);
        } else {
            boolean z = false;
            int i2 = 1;
            if (i < 0) {
                j = -j;
                if (j < 0) {
                    a("-9223372036854775808");
                } else {
                    z = true;
                }
            }
            if (j >= 100000000) {
                i2 = j < 1000000000000L ? j < 10000000000L ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
            } else if (j >= ButtonService.CONNECT_TIMEOUT) {
                i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
            } else if (j >= 100) {
                i2 = j < 1000 ? 3 : 4;
            } else if (j >= 10) {
                i2 = 2;
            }
            if (z) {
                i2++;
            }
            nr7 b2 = b(i2);
            byte[] bArr = b2.a;
            int i3 = b2.c + i2;
            while (j != 0) {
                long j2 = (long) 10;
                i3--;
                bArr[i3] = ur7.a()[(int) (j % j2)];
                j /= j2;
            }
            if (z) {
                bArr[i3 - 1] = (byte) 45;
            }
            b2.c += i2;
            j(x() + ((long) i2));
        }
        return this;
    }

    @DexIgnore
    public final void j(long j) {
        this.b = j;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 writeByte(int i) {
        nr7 b2 = b(1);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        b2.c = i2 + 1;
        bArr[i2] = (byte) i;
        j(x() + 1);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 writeInt(int i) {
        nr7 b2 = b(4);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        b2.c = i5 + 1;
        j(x() + 4);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 writeShort(int i) {
        nr7 b2 = b(2);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        b2.c = i3 + 1;
        j(x() + 2);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public String a(Charset charset) {
        ee7.b(charset, "charset");
        return a(this.b, charset);
    }

    @DexIgnore
    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) throws IOException {
        ee7.b(byteBuffer, "source");
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            nr7 b2 = b(1);
            int min = Math.min(i, 8192 - b2.c);
            byteBuffer.get(b2.a, b2.c, min);
            i -= min;
            b2.c += min;
        }
        this.b += (long) remaining;
        return remaining;
    }

    @DexIgnore
    public String a(long j, Charset charset) throws EOFException {
        ee7.b(charset, "charset");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (this.b < j) {
            throw new EOFException();
        } else if (i == 0) {
            return "";
        } else {
            nr7 nr7 = this.a;
            if (nr7 != null) {
                int i2 = nr7.b;
                if (((long) i2) + j > ((long) nr7.c)) {
                    return new String(f(j), charset);
                }
                int i3 = (int) j;
                String str = new String(nr7.a, i2, i3, charset);
                int i4 = nr7.b + i3;
                nr7.b = i4;
                this.b -= j;
                if (i4 == nr7.c) {
                    this.a = nr7.b();
                    or7.c.a(nr7);
                }
                return str;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) {
        ee7.b(bArr, "sink");
        vq7.a((long) bArr.length, (long) i, (long) i2);
        nr7 nr7 = this.a;
        if (nr7 == null) {
            return -1;
        }
        int min = Math.min(i2, nr7.c - nr7.b);
        byte[] bArr2 = nr7.a;
        int i3 = nr7.b;
        s97.a(bArr2, bArr, i, i3, i3 + min);
        nr7.b += min;
        j(x() - ((long) min));
        if (nr7.b != nr7.c) {
            return min;
        }
        this.a = nr7.b();
        or7.c.a(nr7);
        return min;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 write(byte[] bArr) {
        ee7.b(bArr, "source");
        write(bArr, 0, bArr.length);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 write(byte[] bArr, int i, int i2) {
        ee7.b(bArr, "source");
        long j = (long) i2;
        vq7.a((long) bArr.length, (long) i, j);
        int i3 = i2 + i;
        while (i < i3) {
            nr7 b2 = b(1);
            int min = Math.min(i3 - i, 8192 - b2.c);
            int i4 = i + min;
            s97.a(bArr, b2.a, b2.c, i, i4);
            b2.c += min;
            i = i4;
        }
        j(x() + j);
        return this;
    }

    @DexIgnore
    public final nr7 b(int i) {
        boolean z = true;
        if (i < 1 || i > 8192) {
            z = false;
        }
        if (z) {
            nr7 nr7 = this.a;
            if (nr7 == null) {
                nr7 a2 = or7.c.a();
                this.a = a2;
                a2.g = a2;
                a2.f = a2;
                return a2;
            } else if (nr7 != null) {
                nr7 nr72 = nr7.g;
                if (nr72 == null) {
                    ee7.a();
                    throw null;
                } else if (nr72.c + i <= 8192 && nr72.e) {
                    return nr72;
                } else {
                    nr7 a3 = or7.c.a();
                    nr72.a(a3);
                    return a3;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            throw new IllegalArgumentException("unexpected capacity".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 a(String str) {
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        a(str, 0, str.length());
        return this;
    }

    @DexIgnore
    public yq7 a(String str, Charset charset) {
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        ee7.b(charset, "charset");
        a(str, 0, str.length(), charset);
        return this;
    }

    @DexIgnore
    public yq7 a(String str, int i, int i2, Charset charset) {
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        ee7.b(charset, "charset");
        boolean z = true;
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 > str.length()) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
                } else if (ee7.a(charset, sg7.a)) {
                    a(str, i, i2);
                    return this;
                } else {
                    String substring = str.substring(i, i2);
                    ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    if (substring != null) {
                        byte[] bytes = substring.getBytes(charset);
                        ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        write(bytes, 0, bytes.length);
                        return this;
                    }
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
            }
        } else {
            throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 c(long j) {
        if (j == 0) {
            writeByte(48);
        } else {
            long j2 = (j >>> 1) | j;
            long j3 = j2 | (j2 >>> 2);
            long j4 = j3 | (j3 >>> 4);
            long j5 = j4 | (j4 >>> 8);
            long j6 = j5 | (j5 >>> 16);
            long j7 = j6 | (j6 >>> 32);
            long j8 = j7 - ((j7 >>> 1) & 6148914691236517205L);
            long j9 = ((j8 >>> 2) & 3689348814741910323L) + (j8 & 3689348814741910323L);
            long j10 = ((j9 >>> 4) + j9) & 1085102592571150095L;
            long j11 = j10 + (j10 >>> 8);
            long j12 = j11 + (j11 >>> 16);
            int i = (int) ((((j12 & 63) + ((j12 >>> 32) & 63)) + ((long) 3)) / ((long) 4));
            nr7 b2 = b(i);
            byte[] bArr = b2.a;
            int i2 = b2.c;
            for (int i3 = (i2 + i) - 1; i3 >= i2; i3--) {
                bArr[i3] = ur7.a()[(int) (15 & j)];
                j >>>= 4;
            }
            b2.c += i;
            j(x() + ((long) i));
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public long b(yq7 yq7, long j) {
        ee7.b(yq7, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (x() == 0) {
            return -1;
        } else {
            if (j > x()) {
                j = x();
            }
            yq7.a(this, j);
            return j;
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public boolean a(long j, br7 br7) {
        ee7.b(br7, "bytes");
        return a(j, br7, 0, br7.size());
    }

    @DexIgnore
    public final yq7 a(yq7 yq7, long j, long j2) {
        ee7.b(yq7, "out");
        vq7.a(x(), j, j2);
        if (j2 != 0) {
            yq7.j(yq7.x() + j2);
            nr7 nr7 = this.a;
            while (nr7 != null) {
                int i = nr7.c;
                int i2 = nr7.b;
                if (j >= ((long) (i - i2))) {
                    j -= (long) (i - i2);
                    nr7 = nr7.f;
                } else {
                    while (j2 > 0) {
                        if (nr7 != null) {
                            nr7 c = nr7.c();
                            int i3 = c.b + ((int) j);
                            c.b = i3;
                            c.c = Math.min(i3 + ((int) j2), c.c);
                            nr7 nr72 = yq7.a;
                            if (nr72 == null) {
                                c.g = c;
                                c.f = c;
                                yq7.a = c;
                            } else if (nr72 != null) {
                                nr7 nr73 = nr72.g;
                                if (nr73 != null) {
                                    nr73.a(c);
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                            j2 -= (long) (c.c - c.b);
                            nr7 = nr7.f;
                            j = 0;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                }
            }
            ee7.a();
            throw null;
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public br7 a(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (x() < j) {
            throw new EOFException();
        } else if (j < ((long) 4096)) {
            return new br7(f(j));
        } else {
            br7 a2 = a((int) j);
            skip(j);
            return a2;
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public long a(qr7 qr7) throws IOException {
        ee7.b(qr7, "sink");
        long x = x();
        if (x > 0) {
            qr7.a(this, x);
        }
        return x;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 a(br7 br7) {
        ee7.b(br7, "byteString");
        br7.write$okio(this, 0, br7.size());
        return this;
    }

    @DexIgnore
    public yq7 a(String str, int i, int i2) {
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 <= str.length()) {
                    while (i < i2) {
                        char charAt = str.charAt(i);
                        if (charAt < '\u0080') {
                            nr7 b2 = b(1);
                            byte[] bArr = b2.a;
                            int i3 = b2.c - i;
                            int min = Math.min(i2, 8192 - i3);
                            int i4 = i + 1;
                            bArr[i + i3] = (byte) charAt;
                            while (i4 < min) {
                                char charAt2 = str.charAt(i4);
                                if (charAt2 >= '\u0080') {
                                    break;
                                }
                                bArr[i4 + i3] = (byte) charAt2;
                                i4++;
                            }
                            int i5 = b2.c;
                            int i6 = (i3 + i4) - i5;
                            b2.c = i5 + i6;
                            j(x() + ((long) i6));
                            i = i4;
                        } else {
                            if (charAt < '\u0800') {
                                nr7 b3 = b(2);
                                byte[] bArr2 = b3.a;
                                int i7 = b3.c;
                                bArr2[i7] = (byte) ((charAt >> 6) | 192);
                                bArr2[i7 + 1] = (byte) ((charAt & '?') | '\u0080');
                                b3.c = i7 + 2;
                                j(x() + 2);
                            } else if (charAt < '\ud800' || charAt > '\udfff') {
                                nr7 b4 = b(3);
                                byte[] bArr3 = b4.a;
                                int i8 = b4.c;
                                bArr3[i8] = (byte) ((charAt >> '\f') | 224);
                                bArr3[i8 + 1] = (byte) ((63 & (charAt >> 6)) | 128);
                                bArr3[i8 + 2] = (byte) ((charAt & '?') | '\u0080');
                                b4.c = i8 + 3;
                                j(x() + 3);
                            } else {
                                int i9 = i + 1;
                                char charAt3 = i9 < i2 ? str.charAt(i9) : 0;
                                if (charAt > '\udbff' || '\udc00' > charAt3 || '\udfff' < charAt3) {
                                    writeByte(63);
                                    i = i9;
                                } else {
                                    int i10 = (((charAt & '\u03ff') << '\n') | (charAt3 & '\u03ff')) + 0;
                                    nr7 b5 = b(4);
                                    byte[] bArr4 = b5.a;
                                    int i11 = b5.c;
                                    bArr4[i11] = (byte) ((i10 >> 18) | 240);
                                    bArr4[i11 + 1] = (byte) (((i10 >> 12) & 63) | 128);
                                    bArr4[i11 + 2] = (byte) (((i10 >> 6) & 63) | 128);
                                    bArr4[i11 + 3] = (byte) ((i10 & 63) | 128);
                                    b5.c = i11 + 4;
                                    j(x() + 4);
                                    i += 2;
                                }
                            }
                            i++;
                        }
                    }
                    return this;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public long a(sr7 sr7) throws IOException {
        ee7.b(sr7, "source");
        long j = 0;
        while (true) {
            long b2 = sr7.b(this, (long) 8192);
            if (b2 == -1) {
                return j;
            }
            j += b2;
        }
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public void a(yq7 yq7, long j) {
        nr7 nr7;
        ee7.b(yq7, "source");
        if (yq7 != this) {
            vq7.a(yq7.x(), 0, j);
            while (j > 0) {
                nr7 nr72 = yq7.a;
                if (nr72 != null) {
                    int i = nr72.c;
                    if (nr72 != null) {
                        if (j < ((long) (i - nr72.b))) {
                            nr7 nr73 = this.a;
                            if (nr73 == null) {
                                nr7 = null;
                            } else if (nr73 != null) {
                                nr7 = nr73.g;
                            } else {
                                ee7.a();
                                throw null;
                            }
                            if (nr7 != null && nr7.e) {
                                if ((((long) nr7.c) + j) - ((long) (nr7.d ? 0 : nr7.b)) <= ((long) 8192)) {
                                    nr7 nr74 = yq7.a;
                                    if (nr74 != null) {
                                        nr74.a(nr7, (int) j);
                                        yq7.j(yq7.x() - j);
                                        j(x() + j);
                                        return;
                                    }
                                    ee7.a();
                                    throw null;
                                }
                            }
                            nr7 nr75 = yq7.a;
                            if (nr75 != null) {
                                yq7.a = nr75.a((int) j);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        nr7 nr76 = yq7.a;
                        if (nr76 != null) {
                            long j2 = (long) (nr76.c - nr76.b);
                            yq7.a = nr76.b();
                            nr7 nr77 = this.a;
                            if (nr77 == null) {
                                this.a = nr76;
                                nr76.g = nr76;
                                nr76.f = nr76;
                            } else if (nr77 != null) {
                                nr7 nr78 = nr77.g;
                                if (nr78 != null) {
                                    nr78.a(nr76);
                                    nr76.a();
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                            yq7.j(yq7.x() - j2);
                            j(x() + j2);
                            j -= j2;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    @DexIgnore
    public long a(byte b2, long j, long j2) {
        long j3;
        int i;
        long j4 = j;
        long j5 = j2;
        long j6 = 0;
        if (0 <= j4 && j5 >= j4) {
            if (j5 > x()) {
                j5 = x();
            }
            if (j4 == j5) {
                return -1;
            }
            nr7 nr7 = this.a;
            if (nr7 != null) {
                if (x() - j4 < j4) {
                    j3 = x();
                    while (j3 > j4) {
                        nr7 = nr7.g;
                        if (nr7 != null) {
                            j3 -= (long) (nr7.c - nr7.b);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    if (nr7 != null) {
                        while (j3 < j5) {
                            byte[] bArr = nr7.a;
                            int min = (int) Math.min((long) nr7.c, (((long) nr7.b) + j5) - j3);
                            i = (int) ((((long) nr7.b) + j4) - j3);
                            while (i < min) {
                                if (bArr[i] != b2) {
                                    i++;
                                }
                            }
                            j3 += (long) (nr7.c - nr7.b);
                            nr7 = nr7.f;
                            if (nr7 != null) {
                                j4 = j3;
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    }
                } else {
                    while (true) {
                        long j7 = ((long) (nr7.c - nr7.b)) + j6;
                        if (j7 <= j4) {
                            nr7 = nr7.f;
                            if (nr7 != null) {
                                j6 = j7;
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else if (nr7 != null) {
                            while (j3 < j5) {
                                byte[] bArr2 = nr7.a;
                                int min2 = (int) Math.min((long) nr7.c, (((long) nr7.b) + j5) - j3);
                                int i2 = (int) ((((long) nr7.b) + j4) - j3);
                                while (i < min2) {
                                    if (bArr2[i] != b2) {
                                        i2 = i + 1;
                                    }
                                }
                                j6 = j3 + ((long) (nr7.c - nr7.b));
                                nr7 = nr7.f;
                                if (nr7 != null) {
                                    j4 = j6;
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                        }
                    }
                }
                return ((long) (i - nr7.b)) + j3;
            }
            return -1;
        }
        throw new IllegalArgumentException(("size=" + x() + " fromIndex=" + j4 + " toIndex=" + j5).toString());
    }

    @DexIgnore
    public boolean a(long j, br7 br7, int i, int i2) {
        ee7.b(br7, "bytes");
        if (j < 0 || i < 0 || i2 < 0 || x() - j < ((long) i2) || br7.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (e(((long) i3) + j) != br7.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final br7 a(int i) {
        if (i == 0) {
            return br7.EMPTY;
        }
        vq7.a(x(), 0, (long) i);
        nr7 nr7 = this.a;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            if (nr7 != null) {
                int i5 = nr7.c;
                int i6 = nr7.b;
                if (i5 != i6) {
                    i3 += i5 - i6;
                    i4++;
                    nr7 = nr7.f;
                } else {
                    throw new AssertionError("s.limit == s.pos");
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        byte[][] bArr = new byte[i4][];
        int[] iArr = new int[(i4 * 2)];
        nr7 nr72 = this.a;
        int i7 = 0;
        while (i2 < i) {
            if (nr72 != null) {
                bArr[i7] = nr72.a;
                i2 += nr72.c - nr72.b;
                iArr[i7] = Math.min(i2, i);
                iArr[i7 + i4] = nr72.b;
                nr72.d = true;
                i7++;
                nr72 = nr72.f;
            } else {
                ee7.a();
                throw null;
            }
        }
        return new pr7(bArr, iArr);
    }
}
