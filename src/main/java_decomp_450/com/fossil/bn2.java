package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bn2 extends IInterface {
    @DexIgnore
    int a() throws RemoteException;

    @DexIgnore
    void a(boolean z) throws RemoteException;

    @DexIgnore
    boolean b(bn2 bn2) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    void remove() throws RemoteException;

    @DexIgnore
    void setCenter(LatLng latLng) throws RemoteException;

    @DexIgnore
    void setFillColor(int i) throws RemoteException;

    @DexIgnore
    void setRadius(double d) throws RemoteException;

    @DexIgnore
    void setStrokeColor(int i) throws RemoteException;

    @DexIgnore
    void setStrokeWidth(float f) throws RemoteException;

    @DexIgnore
    void setVisible(boolean z) throws RemoteException;

    @DexIgnore
    void setZIndex(float f) throws RemoteException;
}
