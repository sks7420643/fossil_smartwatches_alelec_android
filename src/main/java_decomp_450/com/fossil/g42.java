package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.a12;
import com.fossil.v02;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g42 extends kn3 implements a12.b, a12.c {
    @DexIgnore
    public static v02.a<? extends xn3, fn3> h; // = un3.c;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ v02.a<? extends xn3, fn3> c;
    @DexIgnore
    public Set<Scope> d;
    @DexIgnore
    public j62 e;
    @DexIgnore
    public xn3 f;
    @DexIgnore
    public j42 g;

    @DexIgnore
    public g42(Context context, Handler handler, j62 j62) {
        this(context, handler, j62, h);
    }

    @DexIgnore
    public final xn3 E() {
        return this.f;
    }

    @DexIgnore
    public final void F() {
        xn3 xn3 = this.f;
        if (xn3 != null) {
            xn3.a();
        }
    }

    @DexIgnore
    public final void a(j42 j42) {
        xn3 xn3 = this.f;
        if (xn3 != null) {
            xn3.a();
        }
        this.e.a(Integer.valueOf(System.identityHashCode(this)));
        v02.a<? extends xn3, fn3> aVar = this.c;
        Context context = this.a;
        Looper looper = this.b.getLooper();
        j62 j62 = this.e;
        this.f = (xn3) aVar.a(context, looper, j62, j62.j(), this, this);
        this.g = j42;
        Set<Scope> set = this.d;
        if (set == null || set.isEmpty()) {
            this.b.post(new i42(this));
        } else {
            this.f.b();
        }
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void b(Bundle bundle) {
        this.f.a(this);
    }

    @DexIgnore
    public g42(Context context, Handler handler, j62 j62, v02.a<? extends xn3, fn3> aVar) {
        this.a = context;
        this.b = handler;
        a72.a(j62, "ClientSettings must not be null");
        this.e = j62;
        this.d = j62.i();
        this.c = aVar;
    }

    @DexIgnore
    public final void b(tn3 tn3) {
        i02 e2 = tn3.e();
        if (e2.x()) {
            c72 g2 = tn3.g();
            i02 g3 = g2.g();
            if (!g3.x()) {
                String valueOf = String.valueOf(g3);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.g.b(g3);
                this.f.a();
                return;
            }
            this.g.a(g2.e(), this.d);
        } else {
            this.g.b(e2);
        }
        this.f.a();
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void a(int i) {
        this.f.a();
    }

    @DexIgnore
    @Override // com.fossil.a22
    public final void a(i02 i02) {
        this.g.b(i02);
    }

    @DexIgnore
    @Override // com.fossil.jn3
    public final void a(tn3 tn3) {
        this.b.post(new h42(this, tn3));
    }
}
