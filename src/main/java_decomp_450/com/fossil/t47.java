package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t47 {
    @DexIgnore
    public static volatile long f;
    @DexIgnore
    public e47 a;
    @DexIgnore
    public x37 b; // = null;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public Context d; // = null;
    @DexIgnore
    public long e; // = System.currentTimeMillis();

    @DexIgnore
    public t47(e47 e47) {
        this.a = e47;
        this.b = w37.o();
        this.c = e47.e();
        this.d = e47.d();
    }

    @DexIgnore
    public void a() {
        if (!d()) {
            if (w37.J > 0 && this.e >= f) {
                z37.f(this.d);
                f = this.e + w37.K;
                if (w37.q()) {
                    k57 f2 = z37.m;
                    f2.e("nextFlushTime=" + f);
                }
            }
            if (k47.a(this.d).f()) {
                if (w37.q()) {
                    k57 f3 = z37.m;
                    f3.e("sendFailedCount=" + z37.p);
                }
                if (!z37.a()) {
                    b();
                    return;
                }
                x47.b(this.d).a(this.a, (g67) null, this.c, false);
                if (this.e - z37.q > 1800000) {
                    z37.d(this.d);
                    return;
                }
                return;
            }
            x47.b(this.d).a(this.a, (g67) null, this.c, false);
        }
    }

    @DexIgnore
    public final void a(g67 g67) {
        h67.b(z37.r).a(this.a, g67);
    }

    @DexIgnore
    public final void b() {
        if (this.a.c() != null && this.a.c().e()) {
            this.b = x37.INSTANT;
        }
        if (w37.z && k47.a(z37.r).e()) {
            this.b = x37.INSTANT;
        }
        if (w37.q()) {
            k57 f2 = z37.m;
            f2.e("strategy=" + this.b.name());
        }
        switch (n47.a[this.b.ordinal()]) {
            case 1:
                c();
                return;
            case 2:
                x47.b(this.d).a(this.a, (g67) null, this.c, false);
                if (w37.q()) {
                    k57 f3 = z37.m;
                    f3.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + z37.s + ",difftime=" + (z37.s - this.e));
                }
                if (z37.s == 0) {
                    z37.s = z57.a(this.d, "last_period_ts", 0L);
                    if (this.e > z37.s) {
                        z37.e(this.d);
                    }
                    long l = this.e + ((long) (w37.l() * 60 * 1000));
                    if (z37.s > l) {
                        z37.s = l;
                    }
                    c67.a(this.d).a();
                }
                if (w37.q()) {
                    k57 f4 = z37.m;
                    f4.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + z37.s + ",difftime=" + (z37.s - this.e));
                }
                if (this.e > z37.s) {
                    z37.e(this.d);
                    return;
                }
                return;
            case 3:
            case 4:
                x47.b(this.d).a(this.a, (g67) null, this.c, false);
                return;
            case 5:
                x47.b(this.d).a(this.a, (g67) new u47(this), this.c, true);
                return;
            case 6:
                if (k47.a(z37.r).c() == 1) {
                    c();
                    return;
                } else {
                    x47.b(this.d).a(this.a, (g67) null, this.c, false);
                    return;
                }
            case 7:
                if (v57.h(this.d)) {
                    a(new v47(this));
                    return;
                }
                return;
            default:
                k57 f5 = z37.m;
                f5.d("Invalid stat strategy:" + w37.o());
                return;
        }
    }

    @DexIgnore
    public final void c() {
        if (x47.i().f <= 0 || !w37.I) {
            a(new w47(this));
            return;
        }
        x47.i().a(this.a, (g67) null, this.c, true);
        x47.i().a(-1);
    }

    @DexIgnore
    public final boolean d() {
        if (w37.w <= 0) {
            return false;
        }
        if (this.e > z37.d) {
            z37.c.clear();
            long unused = z37.d = this.e + w37.x;
            if (w37.q()) {
                k57 f2 = z37.m;
                f2.e("clear methodsCalledLimitMap, nextLimitCallClearTime=" + z37.d);
            }
        }
        Integer valueOf = Integer.valueOf(this.a.a().a());
        Integer num = (Integer) z37.c.get(valueOf);
        if (num != null) {
            z37.c.put(valueOf, Integer.valueOf(num.intValue() + 1));
            if (num.intValue() <= w37.w) {
                return false;
            }
            if (w37.q()) {
                k57 f3 = z37.m;
                f3.c("event " + this.a.f() + " was discard, cause of called limit, current:" + num + ", limit:" + w37.w + ", period:" + w37.x + " ms");
            }
            return true;
        }
        z37.c.put(valueOf, 1);
        return false;
    }
}
