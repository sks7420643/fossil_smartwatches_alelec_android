package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek3 extends kh3 {
    @DexIgnore
    public /* final */ zk3 c;
    @DexIgnore
    public bg3 d;
    @DexIgnore
    public volatile Boolean e;
    @DexIgnore
    public /* final */ mb3 f;
    @DexIgnore
    public /* final */ ul3 g;
    @DexIgnore
    public /* final */ List<Runnable> h; // = new ArrayList();
    @DexIgnore
    public /* final */ mb3 i;

    @DexIgnore
    public ek3(oh3 oh3) {
        super(oh3);
        this.g = new ul3(oh3.zzm());
        this.c = new zk3(this);
        this.f = new hk3(this, oh3);
        this.i = new rk3(this, oh3);
    }

    @DexIgnore
    public final boolean A() {
        g();
        w();
        return this.d != null;
    }

    @DexIgnore
    public final void B() {
        g();
        w();
        a(new qk3(this, a(true)));
    }

    @DexIgnore
    public final void C() {
        g();
        a();
        w();
        nm3 a = a(false);
        if (I()) {
            s().A();
        }
        a(new ik3(this, a));
    }

    @DexIgnore
    public final void D() {
        g();
        w();
        nm3 a = a(true);
        s().B();
        a(new nk3(this, a));
    }

    @DexIgnore
    public final void E() {
        g();
        w();
        if (!A()) {
            if (K()) {
                this.c.b();
            } else if (!l().u()) {
                b();
                List<ResolveInfo> queryIntentServices = f().getPackageManager().queryIntentServices(new Intent().setClassName(f(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    Intent intent = new Intent("com.google.android.gms.measurement.START");
                    Context f2 = f();
                    b();
                    intent.setComponent(new ComponentName(f2, "com.google.android.gms.measurement.AppMeasurementService"));
                    this.c.a(intent);
                    return;
                }
                e().t().a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    @DexIgnore
    public final Boolean F() {
        return this.e;
    }

    @DexIgnore
    public final void G() {
        g();
        w();
        this.c.a();
        try {
            e92.a().a(f(), this.c);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.d = null;
    }

    @DexIgnore
    public final boolean H() {
        g();
        w();
        if (K() && j().u() < 200900) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean I() {
        b();
        return true;
    }

    @DexIgnore
    public final void J() {
        g();
        this.g.a();
        this.f.a(wb3.J.a(null).longValue());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean K() {
        /*
            r5 = this;
            r5.g()
            r5.w()
            java.lang.Boolean r0 = r5.e
            if (r0 != 0) goto L_0x0104
            r5.g()
            r5.w()
            com.fossil.wg3 r0 = r5.k()
            java.lang.Boolean r0 = r0.v()
            r1 = 1
            if (r0 == 0) goto L_0x0023
            boolean r2 = r0.booleanValue()
            if (r2 == 0) goto L_0x0023
            goto L_0x00fe
        L_0x0023:
            r5.b()
            com.fossil.cg3 r2 = r5.p()
            int r2 = r2.F()
            r3 = 0
            if (r2 != r1) goto L_0x0034
        L_0x0031:
            r0 = 1
            goto L_0x00da
        L_0x0034:
            com.fossil.jg3 r2 = r5.e()
            com.fossil.mg3 r2 = r2.B()
            java.lang.String r4 = "Checking service availability"
            r2.a(r4)
            com.fossil.jm3 r2 = r5.j()
            r4 = 12451000(0xbdfcb8, float:1.7447567E-38)
            int r2 = r2.a(r4)
            if (r2 == 0) goto L_0x00cb
            if (r2 == r1) goto L_0x00bb
            r4 = 2
            if (r2 == r4) goto L_0x009b
            r0 = 3
            if (r2 == r0) goto L_0x008c
            r0 = 9
            if (r2 == r0) goto L_0x007e
            r0 = 18
            if (r2 == r0) goto L_0x0070
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.w()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "Unexpected service status"
            r0.a(r2, r1)
            goto L_0x0099
        L_0x0070:
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.w()
            java.lang.String r2 = "Service updating"
            r0.a(r2)
            goto L_0x0031
        L_0x007e:
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.w()
            java.lang.String r1 = "Service invalid"
            r0.a(r1)
            goto L_0x0099
        L_0x008c:
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.w()
            java.lang.String r1 = "Service disabled"
            r0.a(r1)
        L_0x0099:
            r0 = 0
            goto L_0x00c9
        L_0x009b:
            com.fossil.jg3 r2 = r5.e()
            com.fossil.mg3 r2 = r2.A()
            java.lang.String r4 = "Service container out of date"
            r2.a(r4)
            com.fossil.jm3 r2 = r5.j()
            int r2 = r2.u()
            r4 = 17443(0x4423, float:2.4443E-41)
            if (r2 >= r4) goto L_0x00b5
            goto L_0x00c8
        L_0x00b5:
            if (r0 != 0) goto L_0x00b8
            goto L_0x00b9
        L_0x00b8:
            r1 = 0
        L_0x00b9:
            r0 = 0
            goto L_0x00da
        L_0x00bb:
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.B()
            java.lang.String r2 = "Service missing"
            r0.a(r2)
        L_0x00c8:
            r0 = 1
        L_0x00c9:
            r1 = 0
            goto L_0x00da
        L_0x00cb:
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.B()
            java.lang.String r2 = "Service available"
            r0.a(r2)
            goto L_0x0031
        L_0x00da:
            if (r1 != 0) goto L_0x00f4
            com.fossil.ym3 r2 = r5.l()
            boolean r2 = r2.u()
            if (r2 == 0) goto L_0x00f4
            com.fossil.jg3 r0 = r5.e()
            com.fossil.mg3 r0 = r0.t()
            java.lang.String r2 = "No way to upload. Consider using the full version of Analytics"
            r0.a(r2)
            goto L_0x00f5
        L_0x00f4:
            r3 = r0
        L_0x00f5:
            if (r3 == 0) goto L_0x00fe
            com.fossil.wg3 r0 = r5.k()
            r0.a(r1)
        L_0x00fe:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            r5.e = r0
        L_0x0104:
            java.lang.Boolean r0 = r5.e
            boolean r0 = r0.booleanValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ek3.K():boolean");
    }

    @DexIgnore
    public final void L() {
        g();
        if (A()) {
            e().B().a("Inactivity, disconnecting from the service");
            G();
        }
    }

    @DexIgnore
    public final void M() {
        g();
        e().B().a("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable runnable : this.h) {
            try {
                runnable.run();
            } catch (Exception e2) {
                e().t().a("Task exception while flushing queue", e2);
            }
        }
        this.h.clear();
        this.i.c();
    }

    @DexIgnore
    public final void a(bg3 bg3, i72 i72, nm3 nm3) {
        int i2;
        List<i72> a;
        g();
        a();
        w();
        boolean I = I();
        int i3 = 0;
        int i4 = 100;
        while (i3 < 1001 && i4 == 100) {
            ArrayList arrayList = new ArrayList();
            if (!I || (a = s().a(100)) == null) {
                i2 = 0;
            } else {
                arrayList.addAll(a);
                i2 = a.size();
            }
            if (i72 != null && i2 < 100) {
                arrayList.add(i72);
            }
            int size = arrayList.size();
            int i5 = 0;
            while (i5 < size) {
                Object obj = arrayList.get(i5);
                i5++;
                i72 i722 = (i72) obj;
                if (i722 instanceof ub3) {
                    try {
                        bg3.a((ub3) i722, nm3);
                    } catch (RemoteException e2) {
                        e().t().a("Failed to send event to the service", e2);
                    }
                } else if (i722 instanceof em3) {
                    try {
                        bg3.a((em3) i722, nm3);
                    } catch (RemoteException e3) {
                        e().t().a("Failed to send user property to the service", e3);
                    }
                } else if (i722 instanceof wm3) {
                    try {
                        bg3.a((wm3) i722, nm3);
                    } catch (RemoteException e4) {
                        e().t().a("Failed to send conditional user property to the service", e4);
                    }
                } else {
                    e().t().a("Discarding data. Unrecognized parcel type.");
                }
            }
            i3++;
            i4 = i2;
        }
    }

    @DexIgnore
    @Override // com.fossil.kh3
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final void a(ub3 ub3, String str) {
        a72.a(ub3);
        g();
        w();
        boolean I = I();
        a(new tk3(this, I, I && s().a(ub3), ub3, a(true), str));
    }

    @DexIgnore
    public final void a(wm3 wm3) {
        a72.a(wm3);
        g();
        w();
        b();
        a(new sk3(this, true, s().a(wm3), new wm3(wm3), a(true), wm3));
    }

    @DexIgnore
    public final void a(AtomicReference<List<wm3>> atomicReference, String str, String str2, String str3) {
        g();
        w();
        a(new vk3(this, atomicReference, str, str2, str3, a(false)));
    }

    @DexIgnore
    public final void a(r43 r43, String str, String str2) {
        g();
        w();
        a(new uk3(this, str, str2, a(false), r43));
    }

    @DexIgnore
    public final void a(AtomicReference<List<em3>> atomicReference, String str, String str2, String str3, boolean z) {
        g();
        w();
        a(new xk3(this, atomicReference, str, str2, str3, z, a(false)));
    }

    @DexIgnore
    public final void a(r43 r43, String str, String str2, boolean z) {
        g();
        w();
        a(new wk3(this, str, str2, z, a(false), r43));
    }

    @DexIgnore
    public final void a(em3 em3) {
        g();
        w();
        a(new gk3(this, I() && s().a(em3), em3, a(true)));
    }

    @DexIgnore
    public final void a(AtomicReference<String> atomicReference) {
        g();
        w();
        a(new kk3(this, atomicReference, a(false)));
    }

    @DexIgnore
    public final void a(r43 r43) {
        g();
        w();
        a(new jk3(this, a(false), r43));
    }

    @DexIgnore
    public final void a(wj3 wj3) {
        g();
        w();
        a(new mk3(this, wj3));
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        g();
        w();
        a(new pk3(this, bundle, a(false)));
    }

    @DexIgnore
    public final void a(bg3 bg3) {
        g();
        a72.a(bg3);
        this.d = bg3;
        J();
        M();
    }

    @DexIgnore
    public final void a(ComponentName componentName) {
        g();
        if (this.d != null) {
            this.d = null;
            e().B().a("Disconnected from device MeasurementService", componentName);
            g();
            E();
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) throws IllegalStateException {
        g();
        if (A()) {
            runnable.run();
        } else if (((long) this.h.size()) >= 1000) {
            e().t().a("Discarding data. Max runnable queue size reached");
        } else {
            this.h.add(runnable);
            this.i.a(60000);
            E();
        }
    }

    @DexIgnore
    public final nm3 a(boolean z) {
        b();
        return p().a(z ? e().C() : null);
    }

    @DexIgnore
    public final void a(r43 r43, ub3 ub3, String str) {
        g();
        w();
        if (j().a(q02.a) != 0) {
            e().w().a("Not bundling data. Service unavailable or out of date");
            j().a(r43, new byte[0]);
            return;
        }
        a(new ok3(this, ub3, str, r43));
    }
}
