package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qh extends ph {
    @DexIgnore
    public qh(long j, RenderScript renderScript) {
        super(j, renderScript);
        if (j == 0) {
            throw new oh("Loading of ScriptIntrinsic failed.");
        }
    }
}
