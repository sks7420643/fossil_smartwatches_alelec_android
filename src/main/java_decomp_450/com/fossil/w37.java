package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.net.URI;
import java.util.Iterator;
import org.joda.time.DateTimeConstants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w37 {
    @DexIgnore
    public static boolean A; // = true;
    @DexIgnore
    public static volatile String B; // = "pingma.qq.com:80";
    @DexIgnore
    public static volatile String C; // = "http://pingma.qq.com:80/mstat/report";
    @DexIgnore
    public static int D; // = 20;
    @DexIgnore
    public static int E; // = 0;
    @DexIgnore
    public static boolean F; // = false;
    @DexIgnore
    public static String G; // = null;
    @DexIgnore
    public static f67 H; // = null;
    @DexIgnore
    public static boolean I; // = true;
    @DexIgnore
    public static int J; // = 0;
    @DexIgnore
    public static long K; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public static int L; // = 512;
    @DexIgnore
    public static k57 a; // = v57.b();
    @DexIgnore
    public static e67 b; // = new e67(2);
    @DexIgnore
    public static e67 c; // = new e67(1);
    @DexIgnore
    public static x37 d; // = x37.APP_LAUNCH;
    @DexIgnore
    public static boolean e; // = false;
    @DexIgnore
    public static boolean f; // = true;
    @DexIgnore
    public static int g; // = 30000;
    @DexIgnore
    public static int h; // = 100000;
    @DexIgnore
    public static int i; // = 30;
    @DexIgnore
    public static int j; // = 10;
    @DexIgnore
    public static int k; // = 100;
    @DexIgnore
    public static int l; // = 30;
    @DexIgnore
    public static int m; // = 1;
    @DexIgnore
    public static String n; // = "__HIBERNATE__";
    @DexIgnore
    public static String o; // = "__HIBERNATE__TIME";
    @DexIgnore
    public static String p; // = "__MTA_KILL__";
    @DexIgnore
    public static String q;
    @DexIgnore
    public static String r;
    @DexIgnore
    public static String s; // = "mta_channel";
    @DexIgnore
    public static String t; // = "";
    @DexIgnore
    public static int u; // = 180;
    @DexIgnore
    public static boolean v; // = false;
    @DexIgnore
    public static int w; // = 100;
    @DexIgnore
    public static long x; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public static int y; // = 1024;
    @DexIgnore
    public static boolean z; // = true;

    @DexIgnore
    public static int a() {
        return i;
    }

    @DexIgnore
    public static String a(Context context) {
        return a67.a(z57.a(context, "_mta_ky_tag_", (String) null));
    }

    @DexIgnore
    public static String a(String str, String str2) {
        try {
            String string = c.b.getString(str);
            return string != null ? string : str2;
        } catch (Throwable unused) {
            k57 k57 = a;
            k57.g("can't find custom key:" + str);
        }
    }

    @DexIgnore
    public static synchronized void a(int i2) {
        synchronized (w37.class) {
        }
    }

    @DexIgnore
    public static void a(long j2) {
        z57.b(h67.a(), n, j2);
        b(false);
        a.h("MTA is disable for current SDK version");
    }

    @DexIgnore
    public static void a(Context context, e67 e67) {
        int i2 = e67.a;
        if (i2 == c.a) {
            c = e67;
            a(e67.b);
            if (!c.b.isNull("iplist")) {
                k47.a(context).a(c.b.getString("iplist"));
            }
        } else if (i2 == b.a) {
            b = e67;
        }
    }

    @DexIgnore
    public static void a(Context context, e67 e67, JSONObject jSONObject) {
        boolean z2 = false;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase("v")) {
                    int i2 = jSONObject.getInt(next);
                    if (e67.d != i2) {
                        z2 = true;
                    }
                    e67.d = i2;
                } else if (next.equalsIgnoreCase("c")) {
                    String string = jSONObject.getString("c");
                    if (string.length() > 0) {
                        e67.b = new JSONObject(string);
                    }
                } else if (next.equalsIgnoreCase("m")) {
                    e67.c = jSONObject.getString("m");
                }
            }
            if (z2) {
                x47 b2 = x47.b(h67.a());
                if (b2 != null) {
                    b2.a(e67);
                }
                if (e67.a == c.a) {
                    a(e67.b);
                    b(e67.b);
                }
            }
            a(context, e67);
        } catch (JSONException e2) {
            a.a((Throwable) e2);
        }
    }

    @DexIgnore
    public static void a(Context context, String str) {
        if (str != null) {
            z57.b(context, "_mta_ky_tag_", a67.b(str));
        }
    }

    @DexIgnore
    public static void a(Context context, JSONObject jSONObject) {
        JSONObject jSONObject2;
        e67 e67;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(c.a))) {
                    jSONObject2 = jSONObject.getJSONObject(next);
                    e67 = c;
                } else if (next.equalsIgnoreCase(Integer.toString(b.a))) {
                    jSONObject2 = jSONObject.getJSONObject(next);
                    e67 = b;
                } else if (next.equalsIgnoreCase("rs")) {
                    x37 statReportStrategy = x37.getStatReportStrategy(jSONObject.getInt(next));
                    if (statReportStrategy != null) {
                        d = statReportStrategy;
                        if (q()) {
                            k57 k57 = a;
                            k57.a("Change to ReportStrategy:" + statReportStrategy.name());
                        }
                    }
                } else {
                    return;
                }
                a(context, e67, jSONObject2);
            }
        } catch (JSONException e2) {
            a.a((Throwable) e2);
        }
    }

    @DexIgnore
    public static void a(x37 x37) {
        d = x37;
        if (x37 != x37.PERIOD) {
            z37.s = 0;
        }
        if (q()) {
            k57 k57 = a;
            k57.a("Change to statSendStrategy: " + x37);
        }
    }

    @DexIgnore
    public static void a(JSONObject jSONObject) {
        try {
            x37 statReportStrategy = x37.getStatReportStrategy(jSONObject.getInt("rs"));
            if (statReportStrategy != null) {
                a(statReportStrategy);
            }
        } catch (JSONException unused) {
            if (q()) {
                a.e("rs not found.");
            }
        }
    }

    @DexIgnore
    public static void a(boolean z2) {
        z = z2;
    }

    @DexIgnore
    public static boolean a(int i2, int i3, int i4) {
        return i2 >= i3 && i2 <= i4;
    }

    @DexIgnore
    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        String str2 = q;
        if (str2 == null) {
            q = str;
            return true;
        } else if (str2.contains(str)) {
            return false;
        } else {
            q += "|" + str;
            return true;
        }
    }

    @DexIgnore
    public static boolean a(JSONObject jSONObject, String str, String str2) {
        if (jSONObject.isNull(str)) {
            return false;
        }
        String optString = jSONObject.optString(str);
        return v57.c(str2) && v57.c(optString) && str2.equalsIgnoreCase(optString);
    }

    @DexIgnore
    public static synchronized String b(Context context) {
        synchronized (w37.class) {
            if (q != null) {
                return q;
            }
            if (context != null && q == null) {
                q = v57.i(context);
            }
            if (q == null || q.trim().length() == 0) {
                a.d("AppKey can not be null or empty, please read Developer's Guide first!");
            }
            return q;
        }
    }

    @DexIgnore
    public static void b() {
        E++;
    }

    @DexIgnore
    public static void b(int i2) {
        if (i2 >= 0) {
            E = i2;
        }
    }

    @DexIgnore
    public static void b(Context context, String str) {
        k57 k57;
        String str2;
        if (context == null) {
            k57 = a;
            str2 = "ctx in StatConfig.setAppKey() is null";
        } else if (str == null || str.length() > 256) {
            k57 = a;
            str2 = "appkey in StatConfig.setAppKey() is null or exceed 256 bytes";
        } else {
            if (q == null) {
                q = a(context);
            }
            if (a(str) || a(v57.i(context))) {
                a(context, q);
                return;
            }
            return;
        }
        k57.d(str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040 A[Catch:{ Exception -> 0x01b1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(android.content.Context r8, org.json.JSONObject r9) {
        /*
            java.lang.String r0 = "2.0.3"
            java.lang.String r1 = "sm"
            java.lang.String r2 = com.fossil.w37.p     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r9 = r9.optString(r2)     // Catch:{ Exception -> 0x01b1 }
            boolean r2 = com.fossil.v57.c(r9)     // Catch:{ Exception -> 0x01b1 }
            if (r2 == 0) goto L_0x01b0
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x01b1 }
            r2.<init>(r9)     // Catch:{ Exception -> 0x01b1 }
            int r9 = r2.length()     // Catch:{ Exception -> 0x01b1 }
            if (r9 != 0) goto L_0x001c
            return
        L_0x001c:
            boolean r9 = r2.isNull(r1)     // Catch:{ Exception -> 0x01b1 }
            r3 = 0
            if (r9 != 0) goto L_0x0077
            java.lang.Object r9 = r2.get(r1)     // Catch:{ Exception -> 0x01b1 }
            boolean r1 = r9 instanceof java.lang.Integer     // Catch:{ Exception -> 0x01b1 }
            if (r1 == 0) goto L_0x0032
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ Exception -> 0x01b1 }
        L_0x002d:
            int r9 = r9.intValue()     // Catch:{ Exception -> 0x01b1 }
            goto L_0x003e
        L_0x0032:
            boolean r1 = r9 instanceof java.lang.String     // Catch:{ Exception -> 0x01b1 }
            if (r1 == 0) goto L_0x003d
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ Exception -> 0x01b1 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x01b1 }
            goto L_0x002d
        L_0x003d:
            r9 = 0
        L_0x003e:
            if (r9 <= 0) goto L_0x0077
            boolean r1 = q()     // Catch:{ Exception -> 0x01b1 }
            if (r1 == 0) goto L_0x005e
            com.fossil.k57 r1 = com.fossil.w37.a     // Catch:{ Exception -> 0x01b1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r5 = "match sleepTime:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x01b1 }
            r4.append(r9)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r5 = " minutes"
            r4.append(r5)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01b1 }
            r1.e(r4)     // Catch:{ Exception -> 0x01b1 }
        L_0x005e:
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01b1 }
            int r9 = r9 * 60
            int r9 = r9 * 1000
            long r6 = (long) r9     // Catch:{ Exception -> 0x01b1 }
            long r4 = r4 + r6
            java.lang.String r9 = com.fossil.w37.o     // Catch:{ Exception -> 0x01b1 }
            com.fossil.z57.b(r8, r9, r4)     // Catch:{ Exception -> 0x01b1 }
            b(r3)     // Catch:{ Exception -> 0x01b1 }
            com.fossil.k57 r9 = com.fossil.w37.a     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r1 = "MTA is disable for current SDK version"
            r9.h(r1)     // Catch:{ Exception -> 0x01b1 }
        L_0x0077:
            java.lang.String r9 = "sv"
            boolean r9 = a(r2, r9, r0)     // Catch:{ Exception -> 0x01b1 }
            r1 = 1
            if (r9 == 0) goto L_0x0088
            com.fossil.k57 r9 = com.fossil.w37.a     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r3 = "match sdk version:2.0.3"
            r9.e(r3)     // Catch:{ Exception -> 0x01b1 }
            r3 = 1
        L_0x0088:
            java.lang.String r9 = "md"
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ Exception -> 0x01b1 }
            boolean r9 = a(r2, r9, r4)     // Catch:{ Exception -> 0x01b1 }
            if (r9 == 0) goto L_0x00a8
            com.fossil.k57 r9 = com.fossil.w37.a     // Catch:{ Exception -> 0x01b1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = "match MODEL:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ Exception -> 0x01b1 }
            r3.append(r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01b1 }
            r9.e(r3)     // Catch:{ Exception -> 0x01b1 }
            r3 = 1
        L_0x00a8:
            java.lang.String r9 = "av"
            java.lang.String r4 = com.fossil.v57.m(r8)     // Catch:{ Exception -> 0x01b1 }
            boolean r9 = a(r2, r9, r4)     // Catch:{ Exception -> 0x01b1 }
            if (r9 == 0) goto L_0x00cc
            com.fossil.k57 r9 = com.fossil.w37.a     // Catch:{ Exception -> 0x01b1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = "match app version:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = com.fossil.v57.m(r8)     // Catch:{ Exception -> 0x01b1 }
            r3.append(r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01b1 }
            r9.e(r3)     // Catch:{ Exception -> 0x01b1 }
            r3 = 1
        L_0x00cc:
            java.lang.String r9 = "mf"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
            r4.<init>()     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r5 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x01b1 }
            r4.append(r5)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01b1 }
            boolean r9 = a(r2, r9, r4)     // Catch:{ Exception -> 0x01b1 }
            if (r9 == 0) goto L_0x00f8
            com.fossil.k57 r9 = com.fossil.w37.a     // Catch:{ Exception -> 0x01b1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = "match MANUFACTURER:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x01b1 }
            r3.append(r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01b1 }
            r9.e(r3)     // Catch:{ Exception -> 0x01b1 }
            r3 = 1
        L_0x00f8:
            java.lang.String r9 = "osv"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1 }
            r4.<init>()     // Catch:{ Exception -> 0x01b1 }
            int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01b1 }
            r4.append(r5)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01b1 }
            boolean r9 = a(r2, r9, r4)     // Catch:{ Exception -> 0x01b1 }
            java.lang.String r4 = "match android SDK version:"
            if (r9 == 0) goto L_0x0124
            com.fossil.k57 r9 = com.fossil.w37.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r4)
            int r5 = android.os.Build.VERSION.SDK_INT
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r9.e(r3)
            r3 = 1
        L_0x0124:
            java.lang.String r9 = "ov"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            int r6 = android.os.Build.VERSION.SDK_INT
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            boolean r9 = a(r2, r9, r5)
            if (r9 == 0) goto L_0x014e
            com.fossil.k57 r9 = com.fossil.w37.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r4)
            int r4 = android.os.Build.VERSION.SDK_INT
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r9.e(r3)
            r3 = 1
        L_0x014e:
            java.lang.String r9 = "ui"
            com.fossil.x47 r4 = com.fossil.x47.b(r8)
            com.fossil.l57 r4 = r4.a(r8)
            java.lang.String r4 = r4.b()
            boolean r9 = a(r2, r9, r4)
            if (r9 == 0) goto L_0x0182
            com.fossil.k57 r9 = com.fossil.w37.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "match imei:"
            r3.<init>(r4)
            com.fossil.x47 r4 = com.fossil.x47.b(r8)
            com.fossil.l57 r4 = r4.a(r8)
            java.lang.String r4 = r4.b()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r9.e(r3)
            r3 = 1
        L_0x0182:
            java.lang.String r9 = "mid"
            java.lang.String r4 = e(r8)
            boolean r9 = a(r2, r9, r4)
            if (r9 == 0) goto L_0x01a6
            com.fossil.k57 r9 = com.fossil.w37.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "match mid:"
            r2.<init>(r3)
            java.lang.String r8 = e(r8)
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            r9.e(r8)
            goto L_0x01a7
        L_0x01a6:
            r1 = r3
        L_0x01a7:
            if (r1 == 0) goto L_0x01b0
            long r8 = com.fossil.v57.b(r0)
            a(r8)
        L_0x01b0:
            return
        L_0x01b1:
            r8 = move-exception
            com.fossil.k57 r9 = com.fossil.w37.a
            r9.a(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w37.b(android.content.Context, org.json.JSONObject):void");
    }

    @DexIgnore
    public static void b(String str) {
        if (str.length() > 128) {
            a.d("the length of installChannel can not exceed the range of 128 bytes.");
        } else {
            r = str;
        }
    }

    @DexIgnore
    public static void b(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.length() != 0) {
            try {
                b(h67.a(), jSONObject);
                String string = jSONObject.getString(n);
                if (q()) {
                    k57 k57 = a;
                    k57.a("hibernateVer:" + string + ", current version:2.0.3");
                }
                long b2 = v57.b(string);
                if (v57.b("2.0.3") <= b2) {
                    a(b2);
                }
            } catch (JSONException unused) {
                a.a("__HIBERNATE__ not found.");
            }
        }
    }

    @DexIgnore
    public static void b(boolean z2) {
        f = z2;
        if (!z2) {
            a.h("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    @DexIgnore
    public static int c() {
        return E;
    }

    @DexIgnore
    public static String c(Context context) {
        if (context == null) {
            a.d("Context for getCustomUid is null.");
            return null;
        }
        if (G == null) {
            G = z57.a(context, "MTA_CUSTOM_UID", "");
        }
        return G;
    }

    @DexIgnore
    public static void c(int i2) {
        if (!a(i2, 1, (int) DateTimeConstants.MINUTES_PER_WEEK)) {
            a.d("setSendPeriodMinutes can not exceed the range of [1, 7*24*60] minutes.");
        } else {
            u = i2;
        }
    }

    @DexIgnore
    public static void c(Context context, String str) {
        if (str.length() > 128) {
            a.d("the length of installChannel can not exceed the range of 128 bytes.");
            return;
        }
        r = str;
        z57.b(context, s, str);
    }

    @DexIgnore
    public static void c(String str) {
        if (str == null || str.length() == 0) {
            a.d("statReportUrl cannot be null or empty.");
            return;
        }
        C = str;
        try {
            B = new URI(C).getHost();
        } catch (Exception e2) {
            a.g(e2);
        }
        if (q()) {
            k57 k57 = a;
            k57.e("url:" + C + ", domain:" + B);
        }
    }

    @DexIgnore
    public static f67 d() {
        return H;
    }

    @DexIgnore
    public static synchronized String d(Context context) {
        synchronized (w37.class) {
            if (r != null) {
                return r;
            }
            String a2 = z57.a(context, s, "");
            r = a2;
            if (a2 == null || a2.trim().length() == 0) {
                r = v57.j(context);
            }
            if (r == null || r.trim().length() == 0) {
                a.g("installChannel can not be null or empty, please read Developer's Guide first!");
            }
            return r;
        }
    }

    @DexIgnore
    public static int e() {
        return l;
    }

    @DexIgnore
    public static String e(Context context) {
        return context != null ? i27.a(context).a().a() : "0";
    }

    @DexIgnore
    public static int f() {
        return D;
    }

    @DexIgnore
    public static String f(Context context) {
        return z57.a(context, "mta.acc.qq", t);
    }

    @DexIgnore
    public static int g() {
        return k;
    }

    @DexIgnore
    public static int h() {
        return y;
    }

    @DexIgnore
    public static int i() {
        return j;
    }

    @DexIgnore
    public static int j() {
        return h;
    }

    @DexIgnore
    public static int k() {
        return m;
    }

    @DexIgnore
    public static int l() {
        return u;
    }

    @DexIgnore
    public static int m() {
        return g;
    }

    @DexIgnore
    public static String n() {
        return C;
    }

    @DexIgnore
    public static x37 o() {
        return d;
    }

    @DexIgnore
    public static boolean p() {
        return A;
    }

    @DexIgnore
    public static boolean q() {
        return e;
    }

    @DexIgnore
    public static boolean r() {
        return F;
    }

    @DexIgnore
    public static boolean s() {
        return f;
    }
}
