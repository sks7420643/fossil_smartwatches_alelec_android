package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.v02;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l32 implements b42, e52 {
    @DexIgnore
    public /* final */ Lock a;
    @DexIgnore
    public /* final */ Condition b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ m02 d;
    @DexIgnore
    public /* final */ n32 e;
    @DexIgnore
    public /* final */ Map<v02.c<?>, v02.f> f;
    @DexIgnore
    public /* final */ Map<v02.c<?>, i02> g; // = new HashMap();
    @DexIgnore
    public /* final */ j62 h;
    @DexIgnore
    public /* final */ Map<v02<?>, Boolean> i;
    @DexIgnore
    public /* final */ v02.a<? extends xn3, fn3> j;
    @DexIgnore
    public volatile m32 p;
    @DexIgnore
    public i02 q; // = null;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ c32 s;
    @DexIgnore
    public /* final */ a42 t;

    @DexIgnore
    public l32(Context context, c32 c32, Lock lock, Looper looper, m02 m02, Map<v02.c<?>, v02.f> map, j62 j62, Map<v02<?>, Boolean> map2, v02.a<? extends xn3, fn3> aVar, ArrayList<f52> arrayList, a42 a42) {
        this.c = context;
        this.a = lock;
        this.d = m02;
        this.f = map;
        this.h = j62;
        this.i = map2;
        this.j = aVar;
        this.s = c32;
        this.t = a42;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            f52 f52 = arrayList.get(i2);
            i2++;
            f52.a(this);
        }
        this.e = new n32(this, looper);
        this.b = lock.newCondition();
        this.p = new d32(this);
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(T t2) {
        t2.g();
        return (T) this.p.a(t2);
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final boolean a(c22 c22) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t2) {
        t2.g();
        return (T) this.p.b(t2);
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final boolean c() {
        return this.p instanceof p22;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void d() {
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void e() {
        if (c()) {
            ((p22) this.p).d();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final i02 f() {
        b();
        while (g()) {
            try {
                this.b.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new i02(15, null);
            }
        }
        if (c()) {
            return i02.e;
        }
        i02 i02 = this.q;
        if (i02 != null) {
            return i02;
        }
        return new i02(13, null);
    }

    @DexIgnore
    public final boolean g() {
        return this.p instanceof q22;
    }

    @DexIgnore
    public final void h() {
        this.a.lock();
        try {
            this.p = new q22(this, this.h, this.i, this.d, this.j, this.a, this.c);
            this.p.c();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void i() {
        this.a.lock();
        try {
            this.s.o();
            this.p = new p22(this);
            this.p.c();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void a() {
        if (this.p.a()) {
            this.g.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void b() {
        this.p.b();
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void b(Bundle bundle) {
        this.a.lock();
        try {
            this.p.b(bundle);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void a(i02 i02) {
        this.a.lock();
        try {
            this.q = i02;
            this.p = new d32(this);
            this.p.c();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.e52
    public final void a(i02 i02, v02<?> v02, boolean z) {
        this.a.lock();
        try {
            this.p.a(i02, v02, z);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void a(int i2) {
        this.a.lock();
        try {
            this.p.a(i2);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void a(o32 o32) {
        this.e.sendMessage(this.e.obtainMessage(1, o32));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.e.sendMessage(this.e.obtainMessage(2, runtimeException));
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append("mState=").println(this.p);
        for (v02<?> v02 : this.i.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) v02.b()).println(":");
            this.f.get(v02.a()).a(concat, fileDescriptor, printWriter, strArr);
        }
    }
}
