package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h4 implements k4 {
    @DexIgnore
    @Override // com.fossil.k4
    public void a() {
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a(j4 j4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        j4Var.a(new l4(colorStateList, f));
        View d = j4Var.d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        c(j4Var, f3);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float b(j4 j4Var) {
        return j(j4Var).c();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void c(j4 j4Var, float f) {
        j(j4Var).a(f, j4Var.b(), j4Var.a());
        f(j4Var);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float d(j4 j4Var) {
        return j(j4Var).b();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public ColorStateList e(j4 j4Var) {
        return j(j4Var).a();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void f(j4 j4Var) {
        if (!j4Var.b()) {
            j4Var.a(0, 0, 0, 0);
            return;
        }
        float d = d(j4Var);
        float b = b(j4Var);
        int ceil = (int) Math.ceil((double) m4.a(d, b, j4Var.a()));
        int ceil2 = (int) Math.ceil((double) m4.b(d, b, j4Var.a()));
        j4Var.a(ceil, ceil2, ceil, ceil2);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float g(j4 j4Var) {
        return b(j4Var) * 2.0f;
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float h(j4 j4Var) {
        return b(j4Var) * 2.0f;
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void i(j4 j4Var) {
        c(j4Var, d(j4Var));
    }

    @DexIgnore
    public final l4 j(j4 j4Var) {
        return (l4) j4Var.c();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void b(j4 j4Var, float f) {
        j4Var.d().setElevation(f);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void c(j4 j4Var) {
        c(j4Var, d(j4Var));
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a(j4 j4Var, float f) {
        j(j4Var).a(f);
    }

    @DexIgnore
    @Override // com.fossil.k4
    public float a(j4 j4Var) {
        return j4Var.d().getElevation();
    }

    @DexIgnore
    @Override // com.fossil.k4
    public void a(j4 j4Var, ColorStateList colorStateList) {
        j(j4Var).b(colorStateList);
    }
}
