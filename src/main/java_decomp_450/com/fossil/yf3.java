package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yf3<V> {
    @DexIgnore
    public static /* final */ Object h; // = new Object();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ wf3<V> b;
    @DexIgnore
    public /* final */ V c;
    @DexIgnore
    public /* final */ V d;
    @DexIgnore
    public /* final */ Object e;
    @DexIgnore
    public volatile V f;
    @DexIgnore
    public volatile V g;

    @DexIgnore
    public yf3(String str, V v, V v2, wf3<V> wf3) {
        this.e = new Object();
        this.f = null;
        this.g = null;
        this.a = str;
        this.c = v;
        this.d = v2;
        this.b = wf3;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r4 = com.fossil.wb3.a.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x002f, code lost:
        if (r4.hasNext() == false) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0031, code lost:
        r0 = (com.fossil.yf3) r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003b, code lost:
        if (com.fossil.xm3.a() != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003d, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0040, code lost:
        if (r0.b == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0042, code lost:
        r1 = r0.b.zza();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0059, code lost:
        throw new java.lang.IllegalStateException("Refreshing flag cache must be done on a worker thread.");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V a(V r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.e
            monitor-enter(r0)
            monitor-exit(r0)     // Catch:{ all -> 0x0072 }
            if (r4 == 0) goto L_0x0007
            return r4
        L_0x0007:
            com.fossil.xm3 r4 = com.fossil.zf3.a
            if (r4 != 0) goto L_0x000e
            V r4 = r3.c
            return r4
        L_0x000e:
            java.lang.Object r4 = com.fossil.yf3.h
            monitor-enter(r4)
            boolean r0 = com.fossil.xm3.a()     // Catch:{ all -> 0x006d }
            if (r0 == 0) goto L_0x0022
            V r0 = r3.g     // Catch:{ all -> 0x006d }
            if (r0 != 0) goto L_0x001e
            V r0 = r3.c     // Catch:{ all -> 0x006d }
            goto L_0x0020
        L_0x001e:
            V r0 = r3.g     // Catch:{ all -> 0x006d }
        L_0x0020:
            monitor-exit(r4)     // Catch:{ all -> 0x006d }
            return r0
        L_0x0022:
            monitor-exit(r4)     // Catch:{ all -> 0x006d }
            java.util.List r4 = com.fossil.wb3.a     // Catch:{ SecurityException -> 0x005a }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ SecurityException -> 0x005a }
        L_0x002b:
            boolean r0 = r4.hasNext()     // Catch:{ SecurityException -> 0x005a }
            if (r0 == 0) goto L_0x005b
            java.lang.Object r0 = r4.next()     // Catch:{ SecurityException -> 0x005a }
            com.fossil.yf3 r0 = (com.fossil.yf3) r0     // Catch:{ SecurityException -> 0x005a }
            boolean r1 = com.fossil.xm3.a()     // Catch:{ SecurityException -> 0x005a }
            if (r1 != 0) goto L_0x0052
            r1 = 0
            com.fossil.wf3<V> r2 = r0.b     // Catch:{ IllegalStateException -> 0x0048 }
            if (r2 == 0) goto L_0x0048
            com.fossil.wf3<V> r2 = r0.b     // Catch:{ IllegalStateException -> 0x0048 }
            java.lang.Object r1 = r2.zza()     // Catch:{ IllegalStateException -> 0x0048 }
        L_0x0048:
            java.lang.Object r2 = com.fossil.yf3.h
            monitor-enter(r2)
            r0.g = r1     // Catch:{ all -> 0x004f }
            monitor-exit(r2)     // Catch:{ all -> 0x004f }
            goto L_0x002b
        L_0x004f:
            r4 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004f }
            throw r4
        L_0x0052:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r0 = "Refreshing flag cache must be done on a worker thread."
            r4.<init>(r0)
            throw r4
        L_0x005a:
        L_0x005b:
            com.fossil.wf3<V> r4 = r3.b
            if (r4 != 0) goto L_0x0062
            V r4 = r3.c
            return r4
        L_0x0062:
            java.lang.Object r4 = r4.zza()     // Catch:{ SecurityException -> 0x006a, IllegalStateException -> 0x0067 }
            return r4
        L_0x0067:
            V r4 = r3.c
            return r4
        L_0x006a:
            V r4 = r3.c
            return r4
        L_0x006d:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0070:
            monitor-exit(r0)
            throw r4
        L_0x0072:
            r4 = move-exception
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yf3.a(java.lang.Object):java.lang.Object");
    }
}
