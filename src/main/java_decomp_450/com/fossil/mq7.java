package com.fossil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mq7 {
    @DexIgnore
    public static /* final */ mq7 a; // = c();
    @DexIgnore
    public static /* final */ Logger b; // = Logger.getLogger(OkHttpClient.class.getName());

    @DexIgnore
    public static byte[] b(List<jo7> list) {
        yq7 yq7 = new yq7();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            jo7 jo7 = list.get(i);
            if (jo7 != jo7.HTTP_1_0) {
                yq7.writeByte(jo7.toString().length());
                yq7.a(jo7.toString());
            }
        }
        return yq7.f();
    }

    @DexIgnore
    public static mq7 c() {
        iq7 g;
        mq7 f = hq7.f();
        if (f != null) {
            return f;
        }
        if (e() && (g = iq7.g()) != null) {
            return g;
        }
        jq7 f2 = jq7.f();
        if (f2 != null) {
            return f2;
        }
        mq7 f3 = kq7.f();
        if (f3 != null) {
            return f3;
        }
        return new mq7();
    }

    @DexIgnore
    public static mq7 d() {
        return a;
    }

    @DexIgnore
    public static boolean e() {
        if ("conscrypt".equals(System.getProperty("okhttp.platform"))) {
            return true;
        }
        return "Conscrypt".equals(Security.getProviders()[0].getName());
    }

    @DexIgnore
    public String a() {
        return "OkHttp";
    }

    @DexIgnore
    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket) {
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, String str, List<jo7> list) {
    }

    @DexIgnore
    public void a(SSLSocketFactory sSLSocketFactory) {
    }

    @DexIgnore
    public String b(SSLSocket sSLSocket) {
        return null;
    }

    @DexIgnore
    public boolean b(String str) {
        return true;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName();
    }

    @DexIgnore
    public void a(int i, String str, Throwable th) {
        b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    @DexIgnore
    public Object a(String str) {
        if (b.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    @DexIgnore
    public void a(String str, Object obj) {
        if (obj == null) {
            str = str + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
        }
        a(5, str, (Throwable) obj);
    }

    @DexIgnore
    public static List<String> a(List<jo7> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            jo7 jo7 = list.get(i);
            if (jo7 != jo7.HTTP_1_0) {
                arrayList.add(jo7.toString());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public SSLContext b() {
        if ("1.7".equals(System.getProperty("java.specification.version"))) {
            try {
                return SSLContext.getInstance("TLSv1.2");
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No TLS provider", e);
        }
    }

    @DexIgnore
    public qq7 a(X509TrustManager x509TrustManager) {
        return new oq7(b(x509TrustManager));
    }

    @DexIgnore
    public sq7 b(X509TrustManager x509TrustManager) {
        return new pq7(x509TrustManager.getAcceptedIssuers());
    }
}
