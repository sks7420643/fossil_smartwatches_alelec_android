package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj0 extends s81 {
    @DexIgnore
    public long T;
    @DexIgnore
    public long U;
    @DexIgnore
    public String V;
    @DexIgnore
    public String W;
    @DexIgnore
    public m60 X;
    @DexIgnore
    public int Y;
    @DexIgnore
    public boolean Z;
    @DexIgnore
    public /* final */ String a0;

    @DexIgnore
    public aj0(ri1 ri1, en0 en0, byte[] bArr, String str) {
        super(ri1, en0, wm0.g, false, gq0.b.b(ri1.u, pb1.OTA), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.a0 = str;
        String firmwareVersion = en0.a().getFirmwareVersion();
        this.V = firmwareVersion.length() == 0 ? "unknown" : firmwareVersion;
        this.W = "unknown";
        this.X = new m60(ri1.d(), ri1.u, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
        this.Z = true;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.X.getFirmwareVersion();
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.jr1
    public void h() {
        String firmwareVersion = ((zk0) this).x.a().getFirmwareVersion();
        this.V = firmwareVersion;
        String str = this.a0;
        if (str == null || !mh7.b(firmwareVersion, str, true)) {
            if (yp0.f.a()) {
                ((zk0) this).w.v = true;
                a(new mf1(this));
            }
            if (yp0.f.b(((zk0) this).x.a())) {
                zk0.a(this, new vv0(((zk0) this).w, ((zk0) this).x, ((s81) this).S, false, 23131, 0.001f, ((zk0) this).z), new ap1(this), new zq1(this), new xs1(this), (gd7) null, (gd7) null, 48, (Object) null);
            } else {
                super.h();
            }
        } else {
            this.W = this.V;
            this.X = ((zk0) this).x.a();
            this.Z = false;
            a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        JSONObject i = super.i();
        r51 r51 = r51.i5;
        Object obj = this.a0;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        return yz0.a(i, r51, obj);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.jr1
    public JSONObject k() {
        return yz0.a(yz0.a(yz0.a(super.k(), r51.o4, Long.valueOf(Math.max(this.U - this.T, 0L))), r51.p4, this.V), r51.q4, this.W);
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public nw0 p() {
        return new fr0(((v61) this).D, ((zk0) this).w);
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public void q() {
        if (yp0.f.a()) {
            zk0.a(this, new hn0(((zk0) this).w), ih1.a, fj1.a, (kd7) null, new dl1(this), bn1.a, 8, (Object) null);
        } else {
            t();
        }
    }

    @DexIgnore
    public final m60 s() {
        return this.X;
    }

    @DexIgnore
    public final void t() {
        int i = this.Y;
        if (i < 2) {
            this.Y = i + 1;
            yo1 yo1 = yo1.AUTO_CONNECT;
            yp0.f.f();
            HashMap b = oa7.b(w87.a(yo1, false), w87.a(yo1.CONNECTION_TIME_OUT, 55000L));
            if (this.T == 0) {
                this.T = System.currentTimeMillis();
            }
            zk0.a(this, new s21(((zk0) this).w, ((zk0) this).x, b, ((zk0) this).z), new ea1(this), new yb1(this), (kd7) null, (gd7) null, sd1.a, 24, (Object) null);
            return;
        }
        this.Z = true;
        a(((zk0) this).v);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void a(dd1 dd1) {
        if (j81.a[dd1.ordinal()] == 1) {
            v81 v81 = ((zk0) this).b;
            if (v81 == null || v81.t) {
                zk0 zk0 = ((zk0) this).n;
                if (zk0 == null || zk0.t) {
                    a(is0.CONNECTION_DROPPED);
                }
            }
        }
    }
}
