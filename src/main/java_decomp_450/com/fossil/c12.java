package com.fossil;

import com.fossil.i12;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c12<R extends i12> {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Status status);
    }

    @DexIgnore
    public abstract R a(long j, TimeUnit timeUnit);

    @DexIgnore
    public abstract void a(a aVar);

    @DexIgnore
    public abstract void a(j12<? super R> j12);
}
