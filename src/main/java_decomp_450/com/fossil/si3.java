package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class si3 implements Runnable {
    @DexIgnore
    public /* final */ ti3 a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public si3(ti3 ti3, Bundle bundle) {
        this.a = ti3;
        this.b = bundle;
    }

    @DexIgnore
    public final void run() {
        ti3 ti3 = this.a;
        Bundle bundle = this.b;
        if (g23.a() && ti3.l().a(wb3.N0)) {
            if (bundle == null) {
                ti3.k().C.a(new Bundle());
                return;
            }
            Bundle a2 = ti3.k().C.a();
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj != null && !(obj instanceof String) && !(obj instanceof Long) && !(obj instanceof Double)) {
                    ti3.j();
                    if (jm3.a(obj)) {
                        ti3.j().a(27, (String) null, (String) null, 0);
                    }
                    ti3.e().y().a("Invalid default event parameter type. Name, value", str, obj);
                } else if (jm3.i(str)) {
                    ti3.e().y().a("Invalid default event parameter name. Name", str);
                } else if (obj == null) {
                    a2.remove(str);
                } else if (ti3.j().a("param", str, 100, obj)) {
                    ti3.j().a(a2, str, obj);
                }
            }
            ti3.j();
            if (jm3.a(a2, ti3.l().m())) {
                ti3.j().a(26, (String) null, (String) null, 0);
                ti3.e().y().a("Too many default event parameters set. Discarding beyond event parameter limit");
            }
            ti3.k().C.a(a2);
            ti3.q().a(a2);
        }
    }
}
