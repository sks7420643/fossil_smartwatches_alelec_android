package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mz1 extends if2 implements lz1 {
    @DexIgnore
    public mz1() {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
    }

    @DexIgnore
    @Override // com.fossil.if2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a();
        } else if (i != 2) {
            return false;
        } else {
            h();
        }
        return true;
    }
}
