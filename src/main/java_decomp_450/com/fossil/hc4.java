package com.fossil;

import com.fossil.ec4;
import com.fossil.gc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hc4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(gc4.a aVar);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract hc4 a();

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(String str);

        @DexIgnore
        public abstract a d(String str);
    }

    /*
    static {
        p().a();
    }
    */

    @DexIgnore
    public static a p() {
        ec4.b bVar = new ec4.b();
        bVar.b(0);
        bVar.a(gc4.a.ATTEMPT_MIGRATION);
        bVar.a(0);
        return bVar;
    }

    @DexIgnore
    public hc4 a(String str, String str2, long j, String str3, long j2) {
        a m = m();
        m.b(str);
        m.a(gc4.a.REGISTERED);
        m.a(str3);
        m.d(str2);
        m.a(j2);
        m.b(j);
        return m.a();
    }

    @DexIgnore
    public abstract String a();

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public hc4 b(String str) {
        a m = m();
        m.b(str);
        m.a(gc4.a.UNREGISTERED);
        return m.a();
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract gc4.a f();

    @DexIgnore
    public abstract long g();

    @DexIgnore
    public boolean h() {
        return f() == gc4.a.REGISTER_ERROR;
    }

    @DexIgnore
    public boolean i() {
        return f() == gc4.a.NOT_GENERATED || f() == gc4.a.ATTEMPT_MIGRATION;
    }

    @DexIgnore
    public boolean j() {
        return f() == gc4.a.REGISTERED;
    }

    @DexIgnore
    public boolean k() {
        return f() == gc4.a.UNREGISTERED;
    }

    @DexIgnore
    public boolean l() {
        return f() == gc4.a.ATTEMPT_MIGRATION;
    }

    @DexIgnore
    public abstract a m();

    @DexIgnore
    public hc4 n() {
        a m = m();
        m.a((String) null);
        return m.a();
    }

    @DexIgnore
    public hc4 o() {
        a m = m();
        m.a(gc4.a.NOT_GENERATED);
        return m.a();
    }

    @DexIgnore
    public hc4 a(String str) {
        a m = m();
        m.c(str);
        m.a(gc4.a.REGISTER_ERROR);
        return m.a();
    }

    @DexIgnore
    public hc4 a(String str, long j, long j2) {
        a m = m();
        m.a(str);
        m.a(j);
        m.b(j2);
        return m.a();
    }
}
