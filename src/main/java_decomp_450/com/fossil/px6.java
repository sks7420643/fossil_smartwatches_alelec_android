package com.fossil;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px6 {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final boolean a(Context context) {
            ee7.b(context, "context");
            return a(context, "android.permission.ACCESS_BACKGROUND_LOCATION");
        }

        @DexIgnore
        public final boolean b(Context context) {
            ee7.b(context, "context");
            return a(context, "android.permission.ACCESS_FINE_LOCATION");
        }

        @DexIgnore
        public final boolean c() {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            return defaultAdapter != null && defaultAdapter.isEnabled();
        }

        @DexIgnore
        public final boolean d() {
            LocationManager locationManager = (LocationManager) PortfolioApp.g0.c().getSystemService(PlaceFields.LOCATION);
            if (locationManager == null) {
                return false;
            }
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);
            if (!(xs7.a(bestProvider) || ee7.a("passive", bestProvider) || (mh7.b(LocationUtils.HUAWEI_MODEL, Build.MANUFACTURER, true) && mh7.b(LocationUtils.HUAWEI_LOCAL_PROVIDER, bestProvider, true)))) {
                return true;
            }
            try {
                if (Settings.Secure.getInt(PortfolioApp.g0.c().getContentResolver(), "location_mode") != 0) {
                    return true;
                }
                return false;
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        }

        @DexIgnore
        public final boolean e() {
            PortfolioApp c = PortfolioApp.g0.c();
            String string = Settings.Secure.getString(c.getContentResolver(), "enabled_notification_listeners");
            String str = c.getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
            FLogger.INSTANCE.getLocal().d("PermissionUtils", "isNotificationListenerEnabled - notificationServicePath=" + str + ", enabledNotificationListeners=" + string);
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            ee7.a((Object) string, "enabledNotificationListeners");
            return nh7.a(string, str, false, 2, null);
        }

        @DexIgnore
        public final boolean f(Context context) {
            return a(context, "android.permission.READ_PHONE_STATE");
        }

        @DexIgnore
        public final boolean g(Context context) {
            return a(context, "android.permission.READ_SMS");
        }

        @DexIgnore
        public final boolean h(Context context) {
            ee7.b(context, "context");
            return a(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final boolean a(Context context, String... strArr) {
            ee7.b(context, "context");
            ee7.b(strArr, "perms");
            return ku7.a(context, (String[]) Arrays.copyOf(strArr, strArr.length));
        }

        @DexIgnore
        public final HashMap<String, Boolean> b() {
            PortfolioApp c = PortfolioApp.g0.c();
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put(InAppPermission.ACCESS_BACKGROUND_LOCATION, Boolean.valueOf(a(c)));
            linkedHashMap.put(InAppPermission.ACCESS_FINE_LOCATION, Boolean.valueOf(b(c)));
            linkedHashMap.put(InAppPermission.LOCATION_SERVICE, Boolean.valueOf(d()));
            linkedHashMap.put(InAppPermission.BLUETOOTH, Boolean.valueOf(c()));
            linkedHashMap.put(InAppPermission.READ_CONTACTS, Boolean.valueOf(e(c)));
            linkedHashMap.put(InAppPermission.READ_PHONE_STATE, Boolean.valueOf(f(c)));
            linkedHashMap.put(InAppPermission.READ_SMS, Boolean.valueOf(g(c)));
            linkedHashMap.put(InAppPermission.NOTIFICATION_ACCESS, Boolean.valueOf(e()));
            linkedHashMap.put(InAppPermission.CAMERA, Boolean.valueOf(c(c)));
            linkedHashMap.put(InAppPermission.WRITE_EXTERNAL_STORAGE, Boolean.valueOf(h(c)));
            return linkedHashMap;
        }

        @DexIgnore
        public final String[] a() {
            HashMap<String, Boolean> b = b();
            Set<String> keySet = b.keySet();
            ee7.a((Object) keySet, "permissionList.keys");
            ArrayList arrayList = new ArrayList();
            for (T t : keySet) {
                T t2 = t;
                ee7.a((Object) t2, "it");
                if (((Boolean) oa7.b(b, t2)).booleanValue()) {
                    arrayList.add(t);
                }
            }
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                return (String[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final boolean c(Context context) {
            return a(context, "android.permission.CAMERA");
        }

        @DexIgnore
        public final boolean c(Activity activity, int i) {
            ee7.b(activity, Constants.ACTIVITY);
            if (h(activity)) {
                return true;
            }
            g6.a(activity, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, i);
            return false;
        }

        @DexIgnore
        public final boolean d(Context context) {
            ee7.b(context, "context");
            return a(context, "android.permission.READ_CALL_LOG");
        }

        @DexIgnore
        public final boolean e(Context context) {
            ee7.b(context, "context");
            return a(context, "android.permission.READ_CONTACTS");
        }

        @DexIgnore
        public final boolean a(Activity activity, int i) {
            ee7.b(activity, Constants.ACTIVITY);
            if (a(activity)) {
                return true;
            }
            g6.a(activity, new String[]{"android.permission.ACCESS_BACKGROUND_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final boolean a(Context context, String str) {
            if (Build.VERSION.SDK_INT >= 23 && v6.a(context, str) != 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, String... strArr) {
            ee7.b(fragment, "fragment");
            ee7.b(strArr, "perms");
            if (ku7.a(fragment.requireContext(), (String[]) Arrays.copyOf(strArr, strArr.length))) {
                a((Object) fragment, i, (String[]) Arrays.copyOf(strArr, strArr.length));
            } else {
                fragment.requestPermissions(strArr, i);
            }
        }

        @DexIgnore
        public final boolean b(Activity activity, int i) {
            ee7.b(activity, Constants.ACTIVITY);
            if (b(activity)) {
                return true;
            }
            g6.a(activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final void a(Object obj, int i, String... strArr) {
            int[] iArr = new int[strArr.length];
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = 0;
            }
            ku7.a(i, strArr, iArr, obj);
        }
    }
}
