package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class no3<TResult> {
    @DexIgnore
    public no3<TResult> a(ho3<TResult> ho3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public abstract no3<TResult> a(io3 io3);

    @DexIgnore
    public abstract no3<TResult> a(jo3<? super TResult> jo3);

    @DexIgnore
    public abstract no3<TResult> a(Executor executor, io3 io3);

    @DexIgnore
    public abstract no3<TResult> a(Executor executor, jo3<? super TResult> jo3);

    @DexIgnore
    public abstract Exception a();

    @DexIgnore
    public abstract <X extends Throwable> TResult a(Class<X> cls) throws Throwable;

    @DexIgnore
    public <TContinuationResult> no3<TContinuationResult> b(fo3<TResult, no3<TContinuationResult>> fo3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public abstract TResult b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract boolean d();

    @DexIgnore
    public abstract boolean e();

    @DexIgnore
    public no3<TResult> a(Executor executor, ho3<TResult> ho3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> no3<TContinuationResult> b(Executor executor, fo3<TResult, no3<TContinuationResult>> fo3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public no3<TResult> a(Executor executor, go3 go3) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> no3<TContinuationResult> a(fo3<TResult, TContinuationResult> fo3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> no3<TContinuationResult> a(Executor executor, fo3<TResult, TContinuationResult> fo3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> no3<TContinuationResult> a(mo3<TResult, TContinuationResult> mo3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> no3<TContinuationResult> a(Executor executor, mo3<TResult, TContinuationResult> mo3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
