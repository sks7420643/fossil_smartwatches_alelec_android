package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jd5<TranscodeType> extends hw<TranscodeType> implements Cloneable {
    @DexIgnore
    public jd5(aw awVar, iw iwVar, Class<TranscodeType> cls, Context context) {
        super(awVar, iwVar, cls, context);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> c(int i) {
        return (jd5) super.c(i);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> f() {
        return (jd5) super.f();
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> J() {
        return (jd5) super.J();
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> K() {
        return (jd5) super.K();
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> L() {
        return (jd5) super.L();
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> h() {
        return (jd5) super.h();
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> b(boolean z) {
        return (jd5) super.b(z);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> b(int i) {
        return (jd5) super.b(i);
    }

    @DexIgnore
    @Override // com.fossil.hw, com.fossil.hw, com.fossil.hw, com.fossil.k40, com.fossil.k40, java.lang.Object
    public jd5<TranscodeType> clone() {
        return (jd5) super.clone();
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> b(q40<TranscodeType> q40) {
        return (jd5) super.b((q40) q40);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(float f) {
        return (jd5) super.a(f);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(iy iyVar) {
        return (jd5) super.a(iyVar);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(ew ewVar) {
        return (jd5) super.a(ewVar);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(boolean z) {
        return (jd5) super.a(z);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(int i, int i2) {
        return (jd5) super.a(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(yw ywVar) {
        return (jd5) super.a(ywVar);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public <Y> jd5<TranscodeType> a(zw<Y> zwVar, Y y) {
        return (jd5) super.a((zw) zwVar, (Object) y);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(Class<?> cls) {
        return (jd5) super.a(cls);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(r10 r10) {
        return (jd5) super.a(r10);
    }

    @DexIgnore
    @Override // com.fossil.k40
    public jd5<TranscodeType> a(ex<Bitmap> exVar) {
        return (jd5) super.a(exVar);
    }

    @DexIgnore
    @Override // com.fossil.hw, com.fossil.hw, com.fossil.k40
    public jd5<TranscodeType> a(k40<?> k40) {
        return (jd5) super.a(k40);
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(q40<TranscodeType> q40) {
        super.a((q40) q40);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(hw<TranscodeType> hwVar) {
        super.a((hw) hwVar);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(Object obj) {
        super.a(obj);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(Bitmap bitmap) {
        return (jd5) super.a(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(String str) {
        super.a(str);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(Uri uri) {
        super.a(uri);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(File file) {
        super.a(file);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw
    public jd5<TranscodeType> a(Integer num) {
        return (jd5) super.a(num);
    }
}
