package com.fossil;

import android.util.Log;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u20 implements dx<t20> {
    @DexIgnore
    @Override // com.fossil.dx
    public uw a(ax axVar) {
        return uw.SOURCE;
    }

    @DexIgnore
    public boolean a(uy<t20> uyVar, File file, ax axVar) {
        try {
            l50.a(uyVar.get().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
