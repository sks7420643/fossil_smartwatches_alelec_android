package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f35 extends e35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131363342, 1);
        B.put(2131362636, 2);
        B.put(2131362599, 3);
        B.put(2131362597, 4);
        B.put(2131362751, 5);
        B.put(2131363112, 6);
        B.put(2131362935, 7);
        B.put(2131362400, 8);
    }
    */

    @DexIgnore
    public f35(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public f35(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[8], (ImageView) objArr[4], (ImageView) objArr[3], (RTLImageView) objArr[2], (View) objArr[5], (RecyclerView) objArr[7], (SwipeRefreshLayout) objArr[6], (FlexibleTextView) objArr[1]);
        this.z = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.y = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
