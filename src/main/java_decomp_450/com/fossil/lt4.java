package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt4 implements Factory<kt4> {
    @DexIgnore
    public /* final */ Provider<ro4> a;

    @DexIgnore
    public lt4(Provider<ro4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static lt4 a(Provider<ro4> provider) {
        return new lt4(provider);
    }

    @DexIgnore
    public static kt4 a(ro4 ro4) {
        return new kt4(ro4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public kt4 get() {
        return a(this.a.get());
    }
}
