package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr4 extends he {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<wt4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ko4<List<un4>>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<ko4<Boolean>, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<String, String>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Stack<String> f; // = new Stack<>();
    @DexIgnore
    public /* final */ xo4 g;
    @DexIgnore
    public /* final */ ro4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$addFriendRequestedToLocal$1", f = "BCFindFriendsViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(tr4 tr4, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tr4;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$friend, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (this.this$0.g.a(this.$friend.d()) == null) {
                    this.$friend.a(1);
                } else {
                    this.$friend.a(0);
                }
                this.this$0.g.a(this.$friend);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1", f = "BCFindFriendsViewModel.kt", l = {85}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1$1", f = "BCFindFriendsViewModel.kt", l = {91}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tr4$c$a$a")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1$1$1", f = "BCFindFriendsViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.tr4$c$a$a  reason: collision with other inner class name */
            public static final class C0187a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $friends;
                @DexIgnore
                public /* final */ /* synthetic */ List $players;
                @DexIgnore
                public /* final */ /* synthetic */ List $suggestedFriends;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0187a(a aVar, List list, List list2, List list3, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$players = list;
                    this.$friends = list2;
                    this.$suggestedFriends = list3;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0187a aVar = new C0187a(this.this$0, this.$players, this.$friends, this.$suggestedFriends, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0187a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.un4 */
                /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.un4 */
                /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.fossil.un4 */
                /* JADX WARN: Multi-variable type inference failed */
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        Iterator it = this.$players.iterator();
                        while (true) {
                            Object obj2 = null;
                            if (it.hasNext()) {
                                jn4 jn4 = (jn4) it.next();
                                if ((!ee7.a((Object) jn4.d(), (Object) PortfolioApp.g0.c().w())) && (!ee7.a(jn4.s(), pb7.a(true))) && (!ee7.a((Object) jn4.j(), (Object) "left_after_start"))) {
                                    Iterator it2 = this.$friends.iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            break;
                                        }
                                        Object next = it2.next();
                                        if (pb7.a(ee7.a((Object) jn4.d(), (Object) ((un4) next).d())).booleanValue()) {
                                            obj2 = next;
                                            break;
                                        }
                                    }
                                    if (((un4) obj2) == null) {
                                        this.$suggestedFriends.add(new un4(jn4.d(), jn4.i(), jn4.c(), jn4.e(), null, jn4.g(), true, 0, 1));
                                    }
                                }
                            } else {
                                this.this$0.this$0.this$0.c.a(new ko4(this.$suggestedFriends, null, 2, null));
                                return i97.a;
                            }
                        }
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                List<jn4> list;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    yn4 g = this.this$0.this$0.h.g(this.this$0.$historyId);
                    List<un4> c = this.this$0.this$0.g.c();
                    ArrayList arrayList = new ArrayList();
                    if (g == null || (list = g.k()) == null) {
                        list = this.this$0.this$0.h.c();
                    }
                    ti7 a2 = qj7.a();
                    C0187a aVar = new C0187a(this, list, c, arrayList, null);
                    this.L$0 = yi7;
                    this.L$1 = g;
                    this.L$2 = c;
                    this.L$3 = arrayList;
                    this.L$4 = list;
                    this.label = 1;
                    if (vh7.a(a2, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    List list2 = (List) this.L$4;
                    List list3 = (List) this.L$3;
                    List list4 = (List) this.L$2;
                    yn4 yn4 = (yn4) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(tr4 tr4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tr4;
            this.$historyId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$historyId, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.a.a(pb7.a(true));
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a.a(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$searchPeople$1", f = "BCFindFriendsViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $keyword;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$searchPeople$1$result$1", f = "BCFindFriendsViewModel.kt", l = {60}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<List<un4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<List<un4>>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    xo4 b = this.this$0.this$0.g;
                    String str = this.this$0.$keyword;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.a(str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(tr4 tr4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tr4;
            this.$keyword = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$keyword, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j = tr4.i;
                local.e(j, "searchPeople " + this.$keyword);
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (this.this$0.f.contains(this.$keyword)) {
                this.this$0.c.a(ko4);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$sendFriendRequest$1", f = "BCFindFriendsViewModel.kt", l = {121}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public /* final */ /* synthetic */ int $position;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$sendFriendRequest$1$result$1", f = "BCFindFriendsViewModel.kt", l = {122}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<Boolean>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<Boolean>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    xo4 b = this.this$0.this$0.g;
                    un4 un4 = this.this$0.$friend;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.c(un4, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(tr4 tr4, un4 un4, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tr4;
            this.$friend = un4;
            this.$position = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$friend, this.$position, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                FLogger.INSTANCE.getLocal().d(tr4.i, "sendFriendRequest");
                this.this$0.a.a(pb7.a(true));
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            this.this$0.a.a(pb7.a(false));
            if (ko4.c() != null && ((Boolean) ko4.c()).booleanValue()) {
                this.this$0.e.a(w87.a(PortfolioApp.g0.c().w(), this.$friend.d()));
                this.this$0.a(this.$friend);
            }
            this.this$0.d.a(w87.a(ko4, pb7.a(this.$position)));
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = tr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCFindFriendsViewModel::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public tr4(xo4 xo4, ro4 ro4) {
        ee7.b(xo4, "mSocialFriendRepository");
        ee7.b(ro4, "challengeRepository");
        this.g = xo4;
        this.h = ro4;
    }

    @DexIgnore
    public final void h() {
        this.b.a(wt4.SEARCH);
    }

    @DexIgnore
    public final void i() {
        this.b.a(wt4.TYPING);
    }

    @DexIgnore
    public final LiveData<r87<String, String>> b() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<ko4<List<un4>>> d() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<wt4> e() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<r87<ko4<Boolean>, Integer>> f() {
        return this.d;
    }

    @DexIgnore
    public final void g() {
        a();
        this.b.a(wt4.CANCEL);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "historyId");
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "keyword");
        a();
        this.f.push(str);
        ik7 unused = xh7.b(ie.a(this), null, null, new d(this, str, null), 3, null);
    }

    @DexIgnore
    public final void a() {
        if (!this.f.isEmpty()) {
            this.f.pop();
        }
    }

    @DexIgnore
    public final void a(un4 un4, int i2) {
        ee7.b(un4, "friend");
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, un4, i2, null), 3, null);
    }

    @DexIgnore
    public final void a(un4 un4) {
        ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new b(this, un4, null), 2, null);
    }
}
