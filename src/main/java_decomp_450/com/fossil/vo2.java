package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ o43 f;
    @DexIgnore
    public /* final */ /* synthetic */ sn2.b g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vo2(sn2.b bVar, Activity activity, o43 o43) {
        super(sn2.this);
        this.g = bVar;
        this.e = activity;
        this.f = o43;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        sn2.this.h.onActivitySaveInstanceState(cb2.a(this.e), this.f, ((sn2.a) this).b);
    }
}
