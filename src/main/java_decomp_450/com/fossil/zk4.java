package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk4 implements Factory<kh5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public zk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static zk4 a(wj4 wj4) {
        return new zk4(wj4);
    }

    @DexIgnore
    public static kh5 b(wj4 wj4) {
        kh5 p = wj4.p();
        c87.a(p, "Cannot return null from a non-@Nullable @Provides method");
        return p;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public kh5 get() {
        return b(this.a);
    }
}
