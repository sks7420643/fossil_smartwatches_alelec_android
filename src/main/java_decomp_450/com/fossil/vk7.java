package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk7 implements rj7, gi7 {
    @DexIgnore
    public static /* final */ vk7 a; // = new vk7();

    @DexIgnore
    @Override // com.fossil.gi7
    public boolean a(Throwable th) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.rj7
    public void dispose() {
    }

    @DexIgnore
    public String toString() {
        return "NonDisposableHandle";
    }
}
