package com.fossil;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l42 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ i12 a;
    @DexIgnore
    public /* final */ /* synthetic */ m42 b;

    @DexIgnore
    public l42(m42 m42, i12 i12) {
        this.b = m42;
        this.a = i12;
    }

    @DexIgnore
    public final void run() {
        try {
            BasePendingResult.p.set(true);
            this.b.g.sendMessage(this.b.g.obtainMessage(0, this.b.a.a(this.a)));
            BasePendingResult.p.set(false);
            m42.b(this.a);
            a12 a12 = (a12) this.b.f.get();
            if (a12 != null) {
                a12.a(this.b);
            }
        } catch (RuntimeException e) {
            this.b.g.sendMessage(this.b.g.obtainMessage(1, e));
            BasePendingResult.p.set(false);
            m42.b(this.a);
            a12 a122 = (a12) this.b.f.get();
            if (a122 != null) {
                a122.a(this.b);
            }
        } catch (Throwable th) {
            BasePendingResult.p.set(false);
            m42.b(this.a);
            a12 a123 = (a12) this.b.f.get();
            if (a123 != null) {
                a123.a(this.b);
            }
            throw th;
        }
    }
}
