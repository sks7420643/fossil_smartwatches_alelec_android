package com.fossil;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oe5 {
    @DexIgnore
    public static oe5 b;
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public oe5() {
        WindowManager windowManager = (WindowManager) PortfolioApp.c0.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            this.a = point.x;
            return;
        }
        this.a = 0;
    }

    @DexIgnore
    public static oe5 b() {
        if (b == null) {
            b = new oe5();
        }
        return b;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }
}
