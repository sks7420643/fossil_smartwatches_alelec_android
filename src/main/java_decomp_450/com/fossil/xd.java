package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xd<T> extends MutableLiveData<T> {
    @DexIgnore
    public s3<LiveData<?>, a<?>> k; // = new s3<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<V> implements zd<V> {
        @DexIgnore
        public /* final */ LiveData<V> a;
        @DexIgnore
        public /* final */ zd<? super V> b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public a(LiveData<V> liveData, zd<? super V> zdVar) {
            this.a = liveData;
            this.b = zdVar;
        }

        @DexIgnore
        public void a() {
            this.a.a(this);
        }

        @DexIgnore
        public void b() {
            this.a.b(this);
        }

        @DexIgnore
        @Override // com.fossil.zd
        public void onChanged(V v) {
            if (this.c != this.a.b()) {
                this.c = this.a.b();
                this.b.onChanged(v);
            }
        }
    }

    @DexIgnore
    public <S> void a(LiveData<S> liveData, zd<? super S> zdVar) {
        a<?> aVar = new a<>(liveData, zdVar);
        a<?> b = this.k.b(liveData, aVar);
        if (b != null && b.b != zdVar) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        } else if (b == null && c()) {
            aVar.a();
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void d() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().getValue().a();
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void e() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().getValue().b();
        }
    }

    @DexIgnore
    public <S> void a(LiveData<S> liveData) {
        a<?> remove = this.k.remove(liveData);
        if (remove != null) {
            remove.b();
        }
    }
}
