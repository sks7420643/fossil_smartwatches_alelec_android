package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bl6;
import com.fossil.cy6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk6 extends go5 implements gy6, cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public bl6 g;
    @DexIgnore
    public qw6<gz4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return yk6.p;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<bl6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ yk6 a;

        @DexIgnore
        public b(yk6 yk6) {
            this.a = yk6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bl6.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.o(a2.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.n(b.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yk6 a;

        @DexIgnore
        public c(yk6 yk6) {
            this.a = yk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, Action.Apps.IF);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yk6 a;

        @DexIgnore
        public d(yk6 yk6) {
            this.a = yk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.i(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = yk6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeActiveMinutesCh\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363307) {
            bl6 bl6 = this.g;
            if (bl6 != null) {
                bl6.a(cn6.q.a(), p);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void b(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        we7 we7 = we7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(i3 & 16777215)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        bl6 bl6 = this.g;
        if (bl6 != null) {
            bl6.a(i2, Color.parseColor(format));
            if (i2 == 502) {
                p = format;
                return;
            }
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void f1() {
        OverviewDayChart overviewDayChart;
        qw6<gz4> qw6 = this.h;
        if (qw6 != null) {
            gz4 a2 = qw6.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 125, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a3 = w97.a((Object[]) new ArrayList[]{arrayList});
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 200, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a4 = w97.a((Object[]) new ArrayList[]{arrayList2});
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 60, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a5 = w97.a((Object[]) new ArrayList[]{arrayList3});
            ArrayList arrayList4 = new ArrayList();
            arrayList4.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 200, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a6 = w97.a((Object[]) new ArrayList[]{arrayList4});
            ArrayList arrayList5 = new ArrayList();
            arrayList5.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 150, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a7 = w97.a((Object[]) new ArrayList[]{arrayList5});
            ArrayList arrayList6 = new ArrayList();
            arrayList6.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 80, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a8 = w97.a((Object[]) new ArrayList[]{arrayList6});
            ArrayList arrayList7 = new ArrayList();
            arrayList7.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 75, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a9 = w97.a((Object[]) new ArrayList[]{arrayList7});
            ArrayList arrayList8 = new ArrayList();
            arrayList8.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 20, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a10 = w97.a((Object[]) new ArrayList[]{arrayList8});
            ArrayList arrayList9 = new ArrayList();
            arrayList9.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 10, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a11 = w97.a((Object[]) new ArrayList[]{arrayList9});
            ArrayList arrayList10 = new ArrayList();
            arrayList10.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 80, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a12 = w97.a((Object[]) new ArrayList[]{arrayList10});
            ArrayList arrayList11 = new ArrayList();
            arrayList11.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 100, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a13 = w97.a((Object[]) new ArrayList[]{arrayList11});
            ArrayList arrayList12 = new ArrayList();
            arrayList12.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 55, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a14 = w97.a((Object[]) new ArrayList[]{arrayList12});
            ArrayList arrayList13 = new ArrayList();
            arrayList13.add(new BarChart.a(-1, a3, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a4, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a5, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a6, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a7, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a8, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a9, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a10, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a11, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a12, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a13, -1, false, 8, null));
            arrayList13.add(new BarChart.a(-1, a14, -1, false, 8, null));
            BarChart.c cVar = new BarChart.c(200, 50, arrayList13);
            if (a2 != null && (overviewDayChart = a2.r) != null) {
                overviewDayChart.b(cVar);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void j(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<gz4> qw6 = this.h;
        if (qw6 != null) {
            gz4 a2 = qw6.a();
            if (a2 != null) {
                a2.r.setGraphPreviewColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        qw6<gz4> qw6 = this.h;
        if (qw6 != null) {
            gz4 a2 = qw6.a();
            if (a2 != null) {
                a2.v.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        gz4 gz4 = (gz4) qb.a(LayoutInflater.from(getContext()), 2131558529, null, false, a1());
        PortfolioApp.g0.c().f().a(new al6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(bl6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            bl6 bl6 = (bl6) a2;
            this.g = bl6;
            if (bl6 != null) {
                bl6.b().a(getViewLifecycleOwner(), new b(this));
                bl6 bl62 = this.g;
                if (bl62 != null) {
                    bl62.c();
                    this.h = new qw6<>(this, gz4);
                    f1();
                    ee7.a((Object) gz4, "binding");
                    return gz4.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        p = null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        bl6 bl6 = this.g;
        if (bl6 != null) {
            bl6.c();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<gz4> qw6 = this.h;
        if (qw6 != null) {
            gz4 a2 = qw6.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new c(this));
                a2.s.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
