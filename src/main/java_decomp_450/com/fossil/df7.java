package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface df7<R, T> {
    @DexIgnore
    T a(R r, zf7<?> zf7);

    @DexIgnore
    void a(R r, zf7<?> zf7, T t);
}
