package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju0 extends zk0 {
    @DexIgnore
    public byte[] C;
    @DexIgnore
    public /* final */ ArrayList<ul0> D; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.AUTHENTICATION}));
    @DexIgnore
    public /* final */ f31 E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public ju0(ri1 ri1, en0 en0, f31 f31, byte[] bArr) {
        super(ri1, en0, wm0.P, null, false, 24);
        this.E = f31;
        this.F = bArr;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        byte[] bArr = this.C;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        byte[] bArr = this.F;
        if (bArr.length != 8) {
            a(is0.INVALID_PARAMETER);
        } else {
            zk0.a(this, new fp0(((zk0) this).w, this.E, bArr), new xo0(this), tq0.a, (kd7) null, new os0(this), (gd7) null, 40, (Object) null);
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(yz0.a(super.i(), r51.s2, yz0.a(this.E)), r51.m2, yz0.a(this.F, (String) null, 1));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.o2;
        byte[] bArr = this.C;
        String str = null;
        if (bArr != null) {
            str = yz0.a(bArr, (String) null, 1);
        }
        return yz0.a(k, r51, str);
    }
}
