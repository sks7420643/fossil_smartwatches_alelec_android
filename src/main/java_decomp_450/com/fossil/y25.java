package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y25 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleCheckBox r;
    @DexIgnore
    public /* final */ FlexibleCheckBox s;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout z;

    @DexIgnore
    public y25(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleCheckBox flexibleCheckBox, FlexibleCheckBox flexibleCheckBox2, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextInputLayout flexibleTextInputLayout, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleCheckBox;
        this.s = flexibleCheckBox2;
        this.t = flexibleTextInputEditText;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = flexibleTextView4;
        this.y = flexibleTextView5;
        this.z = flexibleTextInputLayout;
        this.A = constraintLayout;
        this.B = flexibleTextView6;
        this.C = flexibleTextView7;
    }
}
