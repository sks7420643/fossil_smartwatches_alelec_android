package com.fossil;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qi3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;
    @DexIgnore
    public long f;
    @DexIgnore
    public qn2 g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public Long i;

    @DexIgnore
    public qi3(Context context, qn2 qn2, Long l) {
        a72.a(context);
        Context applicationContext = context.getApplicationContext();
        a72.a(applicationContext);
        this.a = applicationContext;
        this.i = l;
        if (qn2 != null) {
            this.g = qn2;
            this.b = qn2.f;
            this.c = qn2.e;
            this.d = qn2.d;
            this.h = qn2.c;
            this.f = qn2.b;
            Bundle bundle = qn2.g;
            if (bundle != null) {
                this.e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
