package com.fossil;

import com.fossil.qu7;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu7 extends qu7.a {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements qu7<Object, Call<?>> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;
        @DexIgnore
        public /* final */ /* synthetic */ Executor b;

        @DexIgnore
        public a(uu7 uu7, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        @DexIgnore
        @Override // com.fossil.qu7
        public Type a() {
            return this.a;
        }

        @DexIgnore
        /* Return type fixed from 'retrofit2.Call<java.lang.Object>' to match base method */
        @Override // com.fossil.qu7
        public Call<?> a(Call<Object> call) {
            Executor executor = this.b;
            return executor == null ? call : new b(executor, call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Call<T> {
        @DexIgnore
        public /* final */ Executor a;
        @DexIgnore
        public /* final */ Call<T> b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ru7<T> {
            @DexIgnore
            public /* final */ /* synthetic */ ru7 a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uu7$b$a$a")
            /* renamed from: com.fossil.uu7$b$a$a  reason: collision with other inner class name */
            public class RunnableC0201a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ fv7 a;

                @DexIgnore
                public RunnableC0201a(fv7 fv7) {
                    this.a = fv7;
                }

                @DexIgnore
                public void run() {
                    if (b.this.b.e()) {
                        a aVar = a.this;
                        aVar.a.onFailure(b.this, new IOException("Canceled"));
                        return;
                    }
                    a aVar2 = a.this;
                    aVar2.a.onResponse(b.this, this.a);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uu7$b$a$b")
            /* renamed from: com.fossil.uu7$b$a$b  reason: collision with other inner class name */
            public class RunnableC0202b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable a;

                @DexIgnore
                public RunnableC0202b(Throwable th) {
                    this.a = th;
                }

                @DexIgnore
                public void run() {
                    a aVar = a.this;
                    aVar.a.onFailure(b.this, this.a);
                }
            }

            @DexIgnore
            public a(ru7 ru7) {
                this.a = ru7;
            }

            @DexIgnore
            @Override // com.fossil.ru7
            public void onFailure(Call<T> call, Throwable th) {
                b.this.a.execute(new RunnableC0202b(th));
            }

            @DexIgnore
            @Override // com.fossil.ru7
            public void onResponse(Call<T> call, fv7<T> fv7) {
                b.this.a.execute(new RunnableC0201a(fv7));
            }
        }

        @DexIgnore
        public b(Executor executor, Call<T> call) {
            this.a = executor;
            this.b = call;
        }

        @DexIgnore
        @Override // retrofit2.Call
        public void a(ru7<T> ru7) {
            jv7.a(ru7, "callback == null");
            this.b.a(new a(ru7));
        }

        @DexIgnore
        @Override // retrofit2.Call
        public lo7 c() {
            return this.b.c();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public void cancel() {
            this.b.cancel();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public boolean e() {
            return this.b.e();
        }

        @DexIgnore
        @Override // java.lang.Object, retrofit2.Call
        public Call<T> clone() {
            return new b(this.a, this.b.clone());
        }

        @DexIgnore
        @Override // retrofit2.Call
        public fv7<T> a() throws IOException {
            return this.b.a();
        }
    }

    @DexIgnore
    public uu7(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    @Override // com.fossil.qu7.a
    public qu7<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        Executor executor = null;
        if (qu7.a.a(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b2 = jv7.b(0, (ParameterizedType) type);
            if (!jv7.a(annotationArr, (Class<? extends Annotation>) hv7.class)) {
                executor = this.a;
            }
            return new a(this, b2, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
