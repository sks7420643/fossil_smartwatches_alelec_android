package com.fossil;

import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fd1 extends pk1 {
    @DexIgnore
    public byte[] l; // = new byte[0];
    @DexIgnore
    public /* final */ boolean m;

    @DexIgnore
    public fd1(qk1 qk1, boolean z, cx0 cx0) {
        super(aq0.g, qk1, cx0);
        this.m = z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:58:0x014f  */
    @Override // com.fossil.eo0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.fossil.ri1 r12) {
        /*
            r11 = this;
            com.fossil.qk1 r0 = r11.k
            boolean r1 = r11.m
            boolean r0 = r12.a(r0, r1)
            r1 = 5
            r2 = 0
            if (r0 == 0) goto L_0x0160
            com.fossil.qk1 r0 = r11.k
            com.fossil.f60 r3 = com.fossil.f60.k
            com.fossil.f60$c r3 = r3.d()
            com.fossil.f60$c r4 = com.fossil.f60.c.ENABLED
            r5 = 0
            if (r3 != r4) goto L_0x0046
            android.bluetooth.BluetoothGatt r3 = r12.b
            if (r3 != 0) goto L_0x001e
            goto L_0x0046
        L_0x001e:
            java.util.HashMap<com.fossil.qk1, com.fossil.no1> r3 = r12.i
            java.lang.Object r0 = r3.get(r0)
            com.fossil.no1 r0 = (com.fossil.no1) r0
            if (r0 == 0) goto L_0x0035
            android.bluetooth.BluetoothGattCharacteristic r0 = r0.a
            if (r0 == 0) goto L_0x0035
            int r0 = r0.getProperties()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x0036
        L_0x0035:
            r0 = r2
        L_0x0036:
            if (r0 != 0) goto L_0x003b
            com.fossil.ms1[] r0 = new com.fossil.ms1[r5]
            goto L_0x0048
        L_0x003b:
            com.fossil.nq1 r3 = com.fossil.ms1.e
            int r0 = r0.intValue()
            com.fossil.ms1[] r0 = r3.a(r0)
            goto L_0x0048
        L_0x0046:
            com.fossil.ms1[] r0 = new com.fossil.ms1[r5]
        L_0x0048:
            boolean r3 = r11.m
            r4 = 1
            r3 = r3 ^ r4
            if (r4 != r3) goto L_0x0055
            com.fossil.pi0 r0 = com.fossil.fo0.d
            byte[] r0 = r0.a()
            goto L_0x0075
        L_0x0055:
            com.fossil.ms1 r3 = com.fossil.ms1.b
            boolean r3 = com.fossil.t97.a(r0, r3)
            if (r4 != r3) goto L_0x0064
            com.fossil.pi0 r0 = com.fossil.fo0.d
            byte[] r0 = r0.c()
            goto L_0x0075
        L_0x0064:
            com.fossil.ms1 r3 = com.fossil.ms1.c
            boolean r0 = com.fossil.t97.a(r0, r3)
            if (r4 != r0) goto L_0x0073
            com.fossil.pi0 r0 = com.fossil.fo0.d
            byte[] r0 = r0.b()
            goto L_0x0075
        L_0x0073:
            byte[] r0 = new byte[r5]
        L_0x0075:
            r11.l = r0
            int r0 = r0.length
            if (r0 != 0) goto L_0x007c
            r0 = 1
            goto L_0x007d
        L_0x007c:
            r0 = 0
        L_0x007d:
            if (r0 == 0) goto L_0x0096
            com.fossil.qk1 r0 = r11.k
            boolean r3 = r11.m
            r3 = r3 ^ r4
            r12.a(r0, r3)
            com.fossil.lk0 r12 = r11.d
            com.fossil.oi0 r0 = com.fossil.oi0.g
            com.fossil.lk0 r12 = com.fossil.lk0.a(r12, r2, r0, r2, r1)
            r11.d = r12
            r11.a()
            goto L_0x016d
        L_0x0096:
            com.fossil.qk1 r8 = r11.k
            com.fossil.jm0 r9 = com.fossil.jm0.CLIENT_CHARACTERISTIC_CONFIGURATION
            byte[] r0 = r11.l
            com.fossil.f60 r1 = com.fossil.f60.k
            com.fossil.f60$c r1 = r1.d()
            com.fossil.f60$c r3 = com.fossil.f60.c.ENABLED
            r6 = 2
            if (r1 == r3) goto L_0x00c0
            com.fossil.le0 r0 = com.fossil.le0.DEBUG
            com.fossil.x71 r7 = new com.fossil.x71
            com.fossil.z51 r0 = com.fossil.z51.BLUETOOTH_OFF
            r7.<init>(r0, r5, r6)
            byte[] r10 = new byte[r5]
            android.os.Handler r0 = r12.a
            com.fossil.ug1 r1 = new com.fossil.ug1
            r5 = r1
            r6 = r12
            r5.<init>(r6, r7, r8, r9, r10)
            r0.post(r1)
            goto L_0x015d
        L_0x00c0:
            com.fossil.x71 r1 = new com.fossil.x71
            com.fossil.z51 r3 = com.fossil.z51.SUCCESS
            r1.<init>(r3, r5, r6)
            android.bluetooth.BluetoothGatt r3 = r12.b
            if (r3 != 0) goto L_0x00d5
            com.fossil.x71 r0 = new com.fossil.x71
            com.fossil.z51 r1 = com.fossil.z51.GATT_NULL
            r0.<init>(r1, r5, r6)
        L_0x00d2:
            r7 = r0
            goto L_0x0149
        L_0x00d5:
            java.util.HashMap<com.fossil.qk1, com.fossil.no1> r3 = r12.i
            java.lang.Object r3 = r3.get(r8)
            com.fossil.no1 r3 = (com.fossil.no1) r3
            if (r3 == 0) goto L_0x00e2
            android.bluetooth.BluetoothGattCharacteristic r3 = r3.a
            goto L_0x00e3
        L_0x00e2:
            r3 = r2
        L_0x00e3:
            if (r3 != 0) goto L_0x00ed
            com.fossil.x71 r0 = new com.fossil.x71
            com.fossil.z51 r1 = com.fossil.z51.CHARACTERISTIC_NOT_FOUND
            r0.<init>(r1, r5, r6)
            goto L_0x00d2
        L_0x00ed:
            java.util.UUID r7 = r9.a()
            android.bluetooth.BluetoothGattDescriptor r3 = r3.getDescriptor(r7)
            if (r3 == 0) goto L_0x00fa
            r3.setValue(r0)
        L_0x00fa:
            if (r3 != 0) goto L_0x0104
            com.fossil.x71 r0 = new com.fossil.x71
            com.fossil.z51 r1 = com.fossil.z51.DESCRIPTOR_NOT_FOUND
            r0.<init>(r1, r5, r6)
            goto L_0x00d2
        L_0x0104:
            android.bluetooth.BluetoothGatt r0 = r12.b
            if (r0 == 0) goto L_0x0111
            boolean r0 = r0.writeDescriptor(r3)
            if (r4 == r0) goto L_0x010f
            goto L_0x0111
        L_0x010f:
            r7 = r1
            goto L_0x0149
        L_0x0111:
            com.fossil.le0 r0 = com.fossil.le0.DEBUG
            android.bluetooth.BluetoothGattCharacteristic r0 = r3.getCharacteristic()
            java.lang.String r1 = "descriptor.characteristic"
            com.fossil.ee7.a(r0, r1)
            android.bluetooth.BluetoothGattService r0 = r0.getService()
            java.lang.String r7 = "descriptor.characteristic.service"
            com.fossil.ee7.a(r0, r7)
            r0.getUuid()
            android.bluetooth.BluetoothGattCharacteristic r0 = r3.getCharacteristic()
            com.fossil.ee7.a(r0, r1)
            r0.getUuid()
            r3.getUuid()
            byte[] r0 = r3.getValue()
            java.lang.String r1 = "descriptor.value"
            com.fossil.ee7.a(r0, r1)
            com.fossil.yz0.a(r0, r2, r4)
            com.fossil.x71 r0 = new com.fossil.x71
            com.fossil.z51 r1 = com.fossil.z51.START_FAIL
            r0.<init>(r1, r5, r6)
            goto L_0x00d2
        L_0x0149:
            com.fossil.z51 r0 = r7.a
            com.fossil.z51 r1 = com.fossil.z51.SUCCESS
            if (r0 == r1) goto L_0x015d
            byte[] r10 = new byte[r5]
            android.os.Handler r0 = r12.a
            com.fossil.ug1 r1 = new com.fossil.ug1
            r5 = r1
            r6 = r12
            r5.<init>(r6, r7, r8, r9, r10)
            r0.post(r1)
        L_0x015d:
            r11.j = r4
            goto L_0x016d
        L_0x0160:
            com.fossil.lk0 r12 = r11.d
            com.fossil.oi0 r0 = com.fossil.oi0.d
            com.fossil.lk0 r12 = com.fossil.lk0.a(r12, r2, r0, r2, r1)
            r11.d = r12
            r11.a()
        L_0x016d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fd1.a(com.fossil.ri1):void");
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        if (s91 instanceof yy0) {
            yy0 yy0 = (yy0) s91;
            return yy0.b == ((pk1) this).k && yy0.c == jm0.CLIENT_CHARACTERISTIC_CONFIGURATION;
        }
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.b;
    }

    @DexIgnore
    @Override // com.fossil.pk1, com.fossil.eo0
    public void a(s91 s91) {
        lk0 lk0;
        oi0 oi0;
        ((pk1) this).j = false;
        x71 x71 = s91.a;
        if (x71.a == z51.SUCCESS) {
            if (Arrays.equals(this.l, ((yy0) s91).d)) {
                oi0 = oi0.a;
            } else {
                oi0 = oi0.d;
            }
            lk0 = lk0.a(((eo0) this).d, null, oi0, s91.a, 1);
        } else {
            lk0 a = lk0.d.a(x71);
            lk0 = lk0.a(((eo0) this).d, null, a.b, a.c, 1);
        }
        ((eo0) this).d = lk0;
    }

    @DexIgnore
    @Override // com.fossil.pk1, com.fossil.eo0
    public JSONObject a(boolean z) {
        return yz0.a(super.a(z), r51.E, Boolean.valueOf(this.m));
    }
}
