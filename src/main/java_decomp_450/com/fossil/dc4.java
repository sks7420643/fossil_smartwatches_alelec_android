package com.fossil;

import android.text.TextUtils;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dc4 {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.HOURS.toSeconds(1);
    @DexIgnore
    public static /* final */ Pattern b; // = Pattern.compile("\\AA[\\w-]{38}\\z");

    @DexIgnore
    public static boolean b(String str) {
        return str.contains(":");
    }

    @DexIgnore
    public boolean a(hc4 hc4) {
        if (!TextUtils.isEmpty(hc4.a()) && hc4.g() + hc4.b() >= a() + a) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public long a() {
        return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }

    @DexIgnore
    public static boolean a(String str) {
        return b.matcher(str).matches();
    }
}
