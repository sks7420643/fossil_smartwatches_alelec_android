package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f01 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ oy0 CREATOR; // = new oy0(null);
    @DexIgnore
    public /* final */ rg0 a;
    @DexIgnore
    public /* final */ r60 b;

    @DexIgnore
    public f01(rg0 rg0, r60 r60) {
        this.a = rg0;
        this.b = r60;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.t3, this.a.a()), r51.X3, this.b.a());
    }

    @DexIgnore
    public abstract List<pe1> b();

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            f01 f01 = (f01) obj;
            return !(ee7.a(this.a, f01.a) ^ true) && !(ee7.a(this.b, f01.b) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppResponse");
    }

    @DexIgnore
    public final byte[] getData() {
        byte[] a2 = s97.a(this.a.b(), 0, 8);
        byte[] bArr = new byte[0];
        for (T t : b()) {
            ByteBuffer order = ByteBuffer.allocate(t.b().length + 2).order(ByteOrder.LITTLE_ENDIAN);
            order.putShort((short) ((t.b().length << 7) | ((pe1) t).a.a));
            order.put(t.b());
            byte[] array = order.array();
            ee7.a((Object) array, "dataBuffer.array()");
            bArr = yz0.a(bArr, array);
        }
        ByteBuffer order2 = ByteBuffer.allocate(bArr.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order2, "ByteBuffer.allocate(3 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order2.put((byte) 255);
        order2.putShort((short) (((short) bArr.length) + 3));
        order2.put(bArr);
        byte[] array2 = order2.array();
        ee7.a((Object) array2, "byteBuffer.array()");
        byte[] a3 = yz0.a(yz0.a(yz0.a(new byte[0], new byte[]{this.b.getMajor(), this.b.getMinor(), ck1.c.a}), a2), array2);
        int a4 = (int) ik1.a.a(a3, ng1.CRC32);
        ByteBuffer order3 = ByteBuffer.allocate(a3.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order3, "ByteBuffer.allocate(resp\u2026(ByteOrder.LITTLE_ENDIAN)");
        order3.put(a3);
        order3.putInt(a4);
        byte[] array3 = order3.array();
        ee7.a((Object) array3, "byteBuffer.array()");
        return array3;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getName());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public f01(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.Class<com.fossil.rg0> r0 = com.fossil.rg0.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r4.readParcelable(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0025
            com.fossil.rg0 r0 = (com.fossil.rg0) r0
            java.lang.Class<com.fossil.r60> r2 = com.fossil.r60.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            android.os.Parcelable r4 = r4.readParcelable(r2)
            if (r4 == 0) goto L_0x0021
            com.fossil.r60 r4 = (com.fossil.r60) r4
            r3.<init>(r0, r4)
            return
        L_0x0021:
            com.fossil.ee7.a()
            throw r1
        L_0x0025:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.f01.<init>(android.os.Parcel):void");
    }
}
