package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d12 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends i12> extends BasePendingResult<R> {
        @DexIgnore
        public /* final */ R q;

        @DexIgnore
        public a(a12 a12, R r) {
            super(a12);
            this.q = r;
        }

        @DexIgnore
        @Override // com.google.android.gms.common.api.internal.BasePendingResult
        public final R a(Status status) {
            return this.q;
        }
    }

    @DexIgnore
    public static c12<Status> a(Status status, a12 a12) {
        a72.a(status, "Result must not be null");
        e22 e22 = new e22(a12);
        e22.a((i12) status);
        return e22;
    }

    @DexIgnore
    public static <R extends i12> c12<R> a(R r, a12 a12) {
        a72.a(r, "Result must not be null");
        a72.a(!r.a().y(), "Status code must not be SUCCESS");
        a aVar = new a(a12, r);
        aVar.a(r);
        return aVar;
    }
}
