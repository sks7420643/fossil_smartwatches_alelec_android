package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ju3 extends Transition {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ TextView a;

        @DexIgnore
        public a(ju3 ju3, TextView textView) {
            this.a = textView;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.setScaleX(floatValue);
            this.a.setScaleY(floatValue);
        }
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public void a(hk hkVar) {
        d(hkVar);
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public void c(hk hkVar) {
        d(hkVar);
    }

    @DexIgnore
    public final void d(hk hkVar) {
        View view = hkVar.b;
        if (view instanceof TextView) {
            hkVar.a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public Animator a(ViewGroup viewGroup, hk hkVar, hk hkVar2) {
        if (hkVar == null || hkVar2 == null || !(hkVar.b instanceof TextView)) {
            return null;
        }
        View view = hkVar2.b;
        if (!(view instanceof TextView)) {
            return null;
        }
        TextView textView = (TextView) view;
        Map<String, Object> map = hkVar.a;
        Map<String, Object> map2 = hkVar2.a;
        float f = 1.0f;
        float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
        if (map2.get("android:textscale:scale") != null) {
            f = ((Float) map2.get("android:textscale:scale")).floatValue();
        }
        if (floatValue == f) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(floatValue, f);
        ofFloat.addUpdateListener(new a(this, textView));
        return ofFloat;
    }
}
