package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum v11 {
    ROUTE_IMAGE("req_route"),
    DISTANCE("req_distance");
    
    @DexIgnore
    public static /* final */ b01 e; // = new b01(null);
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public v11(String str) {
        this.a = str;
    }
}
