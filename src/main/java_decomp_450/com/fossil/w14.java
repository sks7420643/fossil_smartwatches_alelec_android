package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class w14 implements Executor {
    @DexIgnore
    public static /* final */ Executor a; // = new w14();

    @DexIgnore
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
