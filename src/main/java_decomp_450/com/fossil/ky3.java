package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ky3<E> extends nz3<E> implements c04<E> {
    @DexIgnore
    public ky3(ny3<E> ny3, zx3<E> zx3) {
        super(ny3, zx3);
    }

    @DexIgnore
    @Override // com.fossil.c04
    public Comparator<? super E> comparator() {
        return delegateCollection().comparator();
    }

    @DexIgnore
    @Override // com.fossil.tx3, com.fossil.vx3, com.fossil.zx3
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    @Override // com.fossil.zx3
    public int indexOf(Object obj) {
        int indexOf = delegateCollection().indexOf(obj);
        if (indexOf < 0 || !get(indexOf).equals(obj)) {
            return -1;
        }
        return indexOf;
    }

    @DexIgnore
    @Override // com.fossil.zx3
    public int lastIndexOf(Object obj) {
        return indexOf(obj);
    }

    @DexIgnore
    @Override // com.fossil.zx3
    public zx3<E> subListUnchecked(int i, int i2) {
        return new tz3(super.subListUnchecked(i, i2), comparator()).asList();
    }

    @DexIgnore
    @Override // com.fossil.tx3, com.fossil.nz3
    public ny3<E> delegateCollection() {
        return (ny3) super.delegateCollection();
    }
}
