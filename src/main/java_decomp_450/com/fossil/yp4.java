package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.aq4;
import com.fossil.vx6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp4 extends go5 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ int q; // = 110;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public /* final */ String f; // = eh5.l.a().b("primaryText");
    @DexIgnore
    public qw6<y25> g;
    @DexIgnore
    public aq4 h;
    @DexIgnore
    public rj4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a() {
            return yp4.q;
        }

        @DexIgnore
        public final String b() {
            return yp4.p;
        }

        @DexIgnore
        public final yp4 c() {
            return new yp4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y25 a;
        @DexIgnore
        public /* final */ /* synthetic */ yp4 b;

        @DexIgnore
        public b(y25 y25, yp4 yp4) {
            this.a = y25;
            this.b = yp4;
        }

        @DexIgnore
        public final void onClick(View view) {
            aq4 b2 = yp4.b(this.b);
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.t;
            ee7.a((Object) flexibleTextInputEditText, "etSocialId");
            String valueOf = String.valueOf(flexibleTextInputEditText.getText());
            if (valueOf != null) {
                b2.a(nh7.d((CharSequence) valueOf).toString());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ y25 a;
        @DexIgnore
        public /* final */ /* synthetic */ yp4 b;

        @DexIgnore
        public c(y25 y25, yp4 yp4) {
            this.a = y25;
            this.b = yp4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.a.z;
            ee7.a((Object) flexibleTextInputLayout, "inputSocialId");
            flexibleTextInputLayout.setErrorEnabled(false);
            aq4 b2 = yp4.b(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                b2.b(nh7.d((CharSequence) valueOf).toString());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ yp4 a;

        @DexIgnore
        public d(yp4 yp4) {
            this.a = yp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ee7.a((Object) bool, "loading");
            if (bool.booleanValue()) {
                yp4 yp4 = this.a;
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886255);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026reating_Text__PleaseWait)");
                yp4.W(a2);
                return;
            }
            this.a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<aq4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ yp4 a;

        @DexIgnore
        public e(yp4 yp4) {
            this.a = yp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(aq4.c cVar) {
            y25 y25 = (y25) yp4.a(this.a).a();
            if (y25 != null) {
                if (cVar.b()) {
                    y25.B.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131231110), (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    y25.B.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131230964), (Drawable) null, (Drawable) null, (Drawable) null);
                }
                if (cVar.a()) {
                    y25.C.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131231110), (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    y25.C.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131230964), (Drawable) null, (Drawable) null, (Drawable) null);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ yp4 a;

        @DexIgnore
        public f(yp4 yp4) {
            this.a = yp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ee7.a((Object) bool, "it");
            if (bool.booleanValue()) {
                um4.a.a(PortfolioApp.g0.c().w(), PortfolioApp.g0.c());
                this.a.g1();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<aq4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ yp4 a;

        @DexIgnore
        public g(yp4 yp4) {
            this.a = yp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(aq4.b bVar) {
            if (bVar.a() != 409) {
                this.a.a(bVar.a(), bVar.b());
                return;
            }
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886258);
            y25 y25 = (y25) yp4.a(this.a).a();
            if (y25 != null) {
                FlexibleButton flexibleButton = y25.q;
                ee7.a((Object) flexibleButton, "btnCreateSocialProfile");
                flexibleButton.setEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout = y25.z;
                ee7.a((Object) flexibleTextInputLayout, "inputSocialId");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = y25.z;
                ee7.a((Object) flexibleTextInputLayout2, "inputSocialId");
                flexibleTextInputLayout2.setError(a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ yp4 a;

        @DexIgnore
        public h(yp4 yp4) {
            this.a = yp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            FlexibleButton flexibleButton;
            y25 y25 = (y25) yp4.a(this.a).a();
            if (y25 != null && (flexibleButton = y25.q) != null) {
                ee7.a((Object) bool, "it");
                flexibleButton.setEnabled(bool.booleanValue());
            }
        }
    }

    /*
    static {
        String simpleName = yp4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCCreateSocialProfileFra\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(yp4 yp4) {
        qw6<y25> qw6 = yp4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ aq4 b(yp4 yp4) {
        aq4 aq4 = yp4.h;
        if (aq4 != null) {
            return aq4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final String f1() {
        String a2 = vx6.a(vx6.c.TERMS, null);
        String a3 = vx6.a(vx6.c.PRIVACY, null);
        String str = ig5.a(PortfolioApp.g0.c(), 2131886923).toString();
        ee7.a((Object) a2, "termOfUseUrl");
        String a4 = mh7.a(str, "term_of_use_url", a2, false, 4, (Object) null);
        ee7.a((Object) a3, "privacyPolicyUrl");
        return mh7.a(a4, "privacy_policy", a3, false, 4, (Object) null);
    }

    @DexIgnore
    public final void g1() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            int i2 = q;
            parentFragment.onActivityResult(i2, i2, null);
        }
    }

    @DexIgnore
    public final void h1() {
        qw6<y25> qw6 = this.g;
        if (qw6 != null) {
            y25 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                ee7.a((Object) flexibleTextView, "ftvTermsService");
                flexibleTextView.setMovementMethod(new LinkMovementMethod());
                String str = this.f;
                if (str != null) {
                    a2.x.setLinkTextColor(Color.parseColor(str));
                }
                a2.q.setOnClickListener(new b(a2, this));
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886262);
                FlexibleTextView flexibleTextView2 = a2.w;
                ee7.a((Object) flexibleTextView2, "ftvDescriptionBold");
                xe5 xe5 = xe5.b;
                ee7.a((Object) a3, "strBold");
                flexibleTextView2.setText(xe5.a(xe5, a3, a3, 0, 4, null));
                FlexibleTextView flexibleTextView3 = a2.x;
                ee7.a((Object) flexibleTextView3, "ftvTermsService");
                flexibleTextView3.setText(r8.a(f1(), 0));
                a2.t.addTextChangedListener(new c(a2, this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        aq4 aq4 = this.h;
        if (aq4 != null) {
            aq4.c().a(getViewLifecycleOwner(), new d(this));
            aq4 aq42 = this.h;
            if (aq42 != null) {
                aq42.e().a(getViewLifecycleOwner(), new e(this));
                aq4 aq43 = this.h;
                if (aq43 != null) {
                    aq43.d().a(getViewLifecycleOwner(), new f(this));
                    aq4 aq44 = this.h;
                    if (aq44 != null) {
                        aq44.b().a(getViewLifecycleOwner(), new g(this));
                        aq4 aq45 = this.h;
                        if (aq45 != null) {
                            aq45.a().a(getViewLifecycleOwner(), new h(this));
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().i().a(this);
        rj4 rj4 = this.i;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(aq4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ileViewModel::class.java)");
            this.h = (aq4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        y25 y25 = (y25) qb.a(layoutInflater, 2131558577, viewGroup, false, a1());
        this.g = new qw6<>(this, y25);
        ee7.a((Object) y25, "binding");
        return y25.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        h1();
        i1();
    }

    @DexIgnore
    public final void a(int i2, String str) {
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.a(i2, str, childFragmentManager);
    }
}
