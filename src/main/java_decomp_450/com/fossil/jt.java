package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.os.Build;
import com.fossil.fo7;
import com.fossil.ht;
import com.fossil.it;
import com.fossil.jt;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jt<T extends jt<T>> {
    @DexIgnore
    public Object a;
    @DexIgnore
    public String b;
    @DexIgnore
    public List<String> c;
    @DexIgnore
    public it.a d;
    @DexIgnore
    public st e;
    @DexIgnore
    public qt f;
    @DexIgnore
    public pt g;
    @DexIgnore
    public fr h;
    @DexIgnore
    public ti7 i;
    @DexIgnore
    public List<? extends yt> j;
    @DexIgnore
    public Bitmap.Config k;
    @DexIgnore
    public ColorSpace l;
    @DexIgnore
    public fo7.a m;
    @DexIgnore
    public ht.a n;
    @DexIgnore
    public dt o;
    @DexIgnore
    public dt p;
    @DexIgnore
    public dt q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;

    @DexIgnore
    public /* synthetic */ jt(kq kqVar, zd7 zd7) {
        this(kqVar);
    }

    @DexIgnore
    public final void a(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public final boolean b() {
        return this.r;
    }

    @DexIgnore
    public final boolean c() {
        return this.s;
    }

    @DexIgnore
    public final Bitmap.Config d() {
        return this.k;
    }

    @DexIgnore
    public final ColorSpace e() {
        return this.l;
    }

    @DexIgnore
    public final Object f() {
        return this.a;
    }

    @DexIgnore
    public final fr g() {
        return this.h;
    }

    @DexIgnore
    public final dt h() {
        return this.p;
    }

    @DexIgnore
    public final ti7 i() {
        return this.i;
    }

    @DexIgnore
    public final fo7.a j() {
        return this.m;
    }

    @DexIgnore
    public final String k() {
        return this.b;
    }

    @DexIgnore
    public final it.a l() {
        return this.d;
    }

    @DexIgnore
    public final dt m() {
        return this.q;
    }

    @DexIgnore
    public final dt n() {
        return this.o;
    }

    @DexIgnore
    public final ht.a o() {
        return this.n;
    }

    @DexIgnore
    public final pt p() {
        return this.g;
    }

    @DexIgnore
    public final qt q() {
        return this.f;
    }

    @DexIgnore
    public final st r() {
        return this.e;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.yt>, java.util.List<com.fossil.yt> */
    public final List<yt> s() {
        return this.j;
    }

    @DexIgnore
    public jt(kq kqVar) {
        this.a = null;
        this.b = null;
        this.c = w97.a();
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = kqVar.g();
        this.h = null;
        this.i = kqVar.c();
        this.j = w97.a();
        this.k = ku.a.a();
        if (Build.VERSION.SDK_INT >= 26) {
            this.l = null;
        }
        this.m = null;
        this.n = null;
        dt dtVar = dt.ENABLED;
        this.o = dtVar;
        this.p = dtVar;
        this.q = dtVar;
        this.r = kqVar.a();
        this.s = kqVar.b();
    }

    @DexIgnore
    public final List<String> a() {
        return this.c;
    }

    @DexIgnore
    public final T a(it.a aVar) {
        this.d = aVar;
        return this;
    }

    @DexIgnore
    public final T a(yt... ytVarArr) {
        ee7.b(ytVarArr, "transformations");
        this.j = t97.h(ytVarArr);
        return this;
    }
}
