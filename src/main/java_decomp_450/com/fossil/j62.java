package com.fossil;

import android.accounts.Account;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j62 {
    @DexIgnore
    public /* final */ Account a;
    @DexIgnore
    public /* final */ Set<Scope> b;
    @DexIgnore
    public /* final */ Set<Scope> c;
    @DexIgnore
    public /* final */ Map<v02<?>, b> d;
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ fn3 h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public Integer j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Account a;
        @DexIgnore
        public o4<Scope> b;
        @DexIgnore
        public Map<v02<?>, b> c;
        @DexIgnore
        public int d; // = 0;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public fn3 h; // = fn3.j;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public final a a(Account account) {
            this.a = account;
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            this.f = str;
            return this;
        }

        @DexIgnore
        public final a a(Collection<Scope> collection) {
            if (this.b == null) {
                this.b = new o4<>();
            }
            this.b.addAll(collection);
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public final j62 a() {
            return new j62(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Set<Scope> a;

        @DexIgnore
        public b(Set<Scope> set) {
            a72.a(set);
            this.a = Collections.unmodifiableSet(set);
        }
    }

    @DexIgnore
    public j62(Account account, Set<Scope> set, Map<v02<?>, b> map, int i2, View view, String str, String str2, fn3 fn3, boolean z) {
        this.a = account;
        this.b = set == null ? Collections.emptySet() : Collections.unmodifiableSet(set);
        this.d = map == null ? Collections.emptyMap() : map;
        this.e = view;
        this.f = str;
        this.g = str2;
        this.h = fn3;
        this.i = z;
        HashSet hashSet = new HashSet(this.b);
        for (b bVar : this.d.values()) {
            hashSet.addAll(bVar.a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public final Account a() {
        return this.a;
    }

    @DexIgnore
    @Deprecated
    public final String b() {
        Account account = this.a;
        if (account != null) {
            return account.name;
        }
        return null;
    }

    @DexIgnore
    public final Account c() {
        Account account = this.a;
        if (account != null) {
            return account;
        }
        return new Account("<<default account>>", "com.google");
    }

    @DexIgnore
    public final Set<Scope> d() {
        return this.c;
    }

    @DexIgnore
    public final Integer e() {
        return this.j;
    }

    @DexIgnore
    public final Map<v02<?>, b> f() {
        return this.d;
    }

    @DexIgnore
    public final String g() {
        return this.g;
    }

    @DexIgnore
    public final String h() {
        return this.f;
    }

    @DexIgnore
    public final Set<Scope> i() {
        return this.b;
    }

    @DexIgnore
    public final fn3 j() {
        return this.h;
    }

    @DexIgnore
    public final boolean k() {
        return this.i;
    }

    @DexIgnore
    public final void a(Integer num) {
        this.j = num;
    }

    @DexIgnore
    public final Set<Scope> a(v02<?> v02) {
        b bVar = this.d.get(v02);
        if (bVar == null || bVar.a.isEmpty()) {
            return this.b;
        }
        HashSet hashSet = new HashSet(this.b);
        hashSet.addAll(bVar.a);
        return hashSet;
    }
}
