package com.fossil;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an2 extends tm2 implements ym2 {
    @DexIgnore
    public an2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    @DexIgnore
    @Override // com.fossil.ym2
    public final ab2 d() throws RemoteException {
        Parcel a = a(4, zza());
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.ym2
    public final ab2 zza(String str) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        Parcel a = a(2, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.ym2
    public final ab2 d(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        Parcel a = a(5, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.ym2
    public final ab2 zza(Bitmap bitmap) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, bitmap);
        Parcel a = a(6, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
