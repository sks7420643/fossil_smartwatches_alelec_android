package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys2 extends ln2 implements xq2 {
    @DexIgnore
    public ys2(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @DexIgnore
    @Override // com.fossil.xq2
    public final Bundle a(Bundle bundle) throws RemoteException {
        Parcel E = E();
        jo2.a(E, bundle);
        Parcel a = a(1, E);
        Bundle bundle2 = (Bundle) jo2.a(a, Bundle.CREATOR);
        a.recycle();
        return bundle2;
    }
}
