package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ln3 extends IInterface {
    @DexIgnore
    void a(rn3 rn3, jn3 jn3) throws RemoteException;

    @DexIgnore
    void a(s62 s62, int i, boolean z) throws RemoteException;

    @DexIgnore
    void b(int i) throws RemoteException;
}
