package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vc4 {
    @DexIgnore
    public static Executor a(String str) {
        return new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ba2(str));
    }

    @DexIgnore
    public static ExecutorService b() {
        return Executors.newSingleThreadExecutor(new ba2("Firebase-Messaging-Network-Io"));
    }

    @DexIgnore
    public static ScheduledExecutorService c() {
        return new ScheduledThreadPoolExecutor(1, new ba2("Firebase-Messaging-Topics-Io"));
    }

    @DexIgnore
    public static Executor d() {
        return a("Firebase-Messaging-Trigger-Topics-Io");
    }

    @DexIgnore
    public static ExecutorService a() {
        return tg2.a().a(new ba2("Firebase-Messaging-Intent-Handle"), yg2.a);
    }
}
