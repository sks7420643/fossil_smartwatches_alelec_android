package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r35 extends q35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        B.put(2131362517, 2);
        B.put(2131362281, 3);
        B.put(2131362653, 4);
        B.put(2131362028, 5);
        B.put(2131362336, 6);
        B.put(2131363102, 7);
        B.put(2131362995, 8);
    }
    */

    @DexIgnore
    public r35(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public r35(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[5], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (RTLImageView) objArr[4], (RTLImageView) objArr[1], (ConstraintLayout) objArr[0], (RecyclerView) objArr[8], (FlexibleSwitchCompat) objArr[7]);
        this.z = -1;
        ((q35) this).w.setTag(null);
        a(view);
        f();
    }
}
