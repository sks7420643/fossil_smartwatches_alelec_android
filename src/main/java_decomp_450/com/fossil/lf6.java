package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lf6 implements Factory<kf6> {
    @DexIgnore
    public static kf6 a(gf6 gf6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, pj4 pj4, PortfolioApp portfolioApp) {
        return new kf6(gf6, summariesRepository, activitiesRepository, userRepository, workoutSessionRepository, fileRepository, pj4, portfolioApp);
    }
}
