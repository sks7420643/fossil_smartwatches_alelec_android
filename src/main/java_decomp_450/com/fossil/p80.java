package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ g90 b;
    @DexIgnore
    public /* final */ b90 c;
    @DexIgnore
    public /* final */ e90 d;
    @DexIgnore
    public /* final */ h90 e;
    @DexIgnore
    public /* final */ d90 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<p80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final p80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                int i = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                g90 a = g90.c.a((i >> 0) & 1);
                b90 a2 = b90.c.a((i >> 1) & 1);
                e90 a3 = e90.c.a((i >> 2) & 1);
                h90 a4 = h90.c.a((i >> 3) & 1);
                d90 a5 = d90.c.a((i >> 4) & 1);
                if (a != null && a2 != null && a3 != null && a4 != null && a5 != null) {
                    return new p80(a, a2, a3, a4, a5);
                }
                throw new IllegalArgumentException("Invalid data properties");
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public p80 createFromParcel(Parcel parcel) {
            return new p80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public p80[] newArray(int i) {
            return new p80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public p80 m48createFromParcel(Parcel parcel) {
            return new p80(parcel, null);
        }
    }

    @DexIgnore
    public p80(g90 g90, b90 b90, e90 e90, h90 h90, d90 d90) {
        super(o80.DISPLAY_UNIT);
        this.b = g90;
        this.c = b90;
        this.d = e90;
        this.e = h90;
        this.f = d90;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((this.f.a() << 4) | (this.b.a() << 0) | 0 | (this.c.a() << 1) | (this.d.a() << 2) | (this.e.a() << 3)).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(p80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            p80 p80 = (p80) obj;
            return this.b == p80.b && this.c == p80.c && this.d == p80.d && this.e == p80.e && this.f == p80.f;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DisplayUnitConfig");
    }

    @DexIgnore
    public final b90 getCaloriesUnit() {
        return this.c;
    }

    @DexIgnore
    public final d90 getDateFormat() {
        return this.f;
    }

    @DexIgnore
    public final e90 getDistanceUnit() {
        return this.d;
    }

    @DexIgnore
    public final g90 getTemperatureUnit() {
        return this.b;
    }

    @DexIgnore
    public final h90 getTimeFormat() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        return this.f.hashCode() + ((hashCode3 + ((hashCode2 + ((hashCode + (this.b.hashCode() * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(jSONObject, r51.R1, yz0.a(this.b)), r51.S1, yz0.a(this.c)), r51.T1, yz0.a(this.d)), r51.U1, yz0.a(this.e)), r51.V1, yz0.a(this.f));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ p80(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            this.b = g90.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                ee7.a((Object) readString2, "parcel.readString()!!");
                this.c = b90.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    ee7.a((Object) readString3, "parcel.readString()!!");
                    this.d = e90.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        ee7.a((Object) readString4, "parcel.readString()!!");
                        this.e = h90.valueOf(readString4);
                        String readString5 = parcel.readString();
                        if (readString5 != null) {
                            ee7.a((Object) readString5, "parcel.readString()!!");
                            this.f = d90.valueOf(readString5);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }
}
