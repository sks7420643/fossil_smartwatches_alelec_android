package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bw2;
import com.fossil.rp2;
import com.fossil.zp2;
import com.sina.weibo.sdk.api.ImageObject;
import java.util.Collections;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp2 extends bw2<vp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ vp2 zzaw;
    @DexIgnore
    public static volatile wx2<vp2> zzax;
    @DexIgnore
    public int zzaa;
    @DexIgnore
    public String zzab; // = "";
    @DexIgnore
    public String zzac; // = "";
    @DexIgnore
    public boolean zzad;
    @DexIgnore
    public jw2<pp2> zzae; // = bw2.o();
    @DexIgnore
    public String zzaf; // = "";
    @DexIgnore
    public int zzag;
    @DexIgnore
    public int zzah;
    @DexIgnore
    public int zzai;
    @DexIgnore
    public String zzaj; // = "";
    @DexIgnore
    public long zzak;
    @DexIgnore
    public long zzal;
    @DexIgnore
    public String zzam; // = "";
    @DexIgnore
    public String zzan; // = "";
    @DexIgnore
    public int zzao;
    @DexIgnore
    public String zzap; // = "";
    @DexIgnore
    public wp2 zzaq;
    @DexIgnore
    public hw2 zzar; // = bw2.m();
    @DexIgnore
    public long zzas;
    @DexIgnore
    public long zzat;
    @DexIgnore
    public String zzau; // = "";
    @DexIgnore
    public String zzav; // = "";
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public int zze;
    @DexIgnore
    public jw2<rp2> zzf; // = bw2.o();
    @DexIgnore
    public jw2<zp2> zzg; // = bw2.o();
    @DexIgnore
    public long zzh;
    @DexIgnore
    public long zzi;
    @DexIgnore
    public long zzj;
    @DexIgnore
    public long zzk;
    @DexIgnore
    public long zzl;
    @DexIgnore
    public String zzm; // = "";
    @DexIgnore
    public String zzn; // = "";
    @DexIgnore
    public String zzo; // = "";
    @DexIgnore
    public String zzp; // = "";
    @DexIgnore
    public int zzq;
    @DexIgnore
    public String zzr; // = "";
    @DexIgnore
    public String zzs; // = "";
    @DexIgnore
    public String zzt; // = "";
    @DexIgnore
    public long zzu;
    @DexIgnore
    public long zzv;
    @DexIgnore
    public String zzw; // = "";
    @DexIgnore
    public boolean zzx;
    @DexIgnore
    public String zzy; // = "";
    @DexIgnore
    public long zzz;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<vp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(vp2.zzaw);
        }

        @DexIgnore
        public final a a(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).d(1);
            return this;
        }

        @DexIgnore
        public final rp2 b(int i) {
            return ((vp2) ((bw2.a) this).b).b(i);
        }

        @DexIgnore
        public final a c(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).e(i);
            return this;
        }

        @DexIgnore
        public final zp2 d(int i) {
            return ((vp2) ((bw2.a) this).b).c(i);
        }

        @DexIgnore
        public final a e(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).f(i);
            return this;
        }

        @DexIgnore
        public final a f(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).g(i);
            return this;
        }

        @DexIgnore
        public final a g(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).g(str);
            return this;
        }

        @DexIgnore
        public final a h(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).h(str);
            return this;
        }

        @DexIgnore
        public final a i(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).i(str);
            return this;
        }

        @DexIgnore
        public final a j(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).j(str);
            return this;
        }

        @DexIgnore
        public final a k(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).k(str);
            return this;
        }

        @DexIgnore
        public final a l(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).l(str);
            return this;
        }

        @DexIgnore
        public final a m(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).m(str);
            return this;
        }

        @DexIgnore
        public final a n(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).n(null);
            return this;
        }

        @DexIgnore
        public final int o() {
            return ((vp2) ((bw2.a) this).b).e0();
        }

        @DexIgnore
        public final a p() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).W();
            return this;
        }

        @DexIgnore
        public final List<zp2> q() {
            return Collections.unmodifiableList(((vp2) ((bw2.a) this).b).f0());
        }

        @DexIgnore
        public final long r() {
            return ((vp2) ((bw2.a) this).b).m0();
        }

        @DexIgnore
        public final a s() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).Y();
            return this;
        }

        @DexIgnore
        public final a t() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).Z();
            return this;
        }

        @DexIgnore
        public final String u() {
            return ((vp2) ((bw2.a) this).b).w0();
        }

        @DexIgnore
        public final a v() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a0();
            return this;
        }

        @DexIgnore
        public final String w() {
            return ((vp2) ((bw2.a) this).b).B();
        }

        @DexIgnore
        public final a x() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).b0();
            return this;
        }

        @DexIgnore
        public final a y() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).c0();
            return this;
        }

        @DexIgnore
        public final String z() {
            return ((vp2) ((bw2.a) this).b).U();
        }

        @DexIgnore
        public final List<rp2> zza() {
            return Collections.unmodifiableList(((vp2) ((bw2.a) this).b).d0());
        }

        @DexIgnore
        public final int zze() {
            return ((vp2) ((bw2.a) this).b).g0();
        }

        @DexIgnore
        public final long zzf() {
            return ((vp2) ((bw2.a) this).b).k0();
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a b(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).b(j);
            return this;
        }

        @DexIgnore
        public final a d(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).d(j);
            return this;
        }

        @DexIgnore
        public final a o(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).o(str);
            return this;
        }

        @DexIgnore
        public final a a(int i, rp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(i, (rp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a c(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).c(j);
            return this;
        }

        @DexIgnore
        public final a e(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).e(j);
            return this;
        }

        @DexIgnore
        public final a f(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).f(str);
            return this;
        }

        @DexIgnore
        public final a g(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).g(j);
            return this;
        }

        @DexIgnore
        public final a h(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).h(j);
            return this;
        }

        @DexIgnore
        public final a i(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).i(j);
            return this;
        }

        @DexIgnore
        public final a j(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).j(j);
            return this;
        }

        @DexIgnore
        public final a k(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).k(j);
            return this;
        }

        @DexIgnore
        public final a l(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).l(j);
            return this;
        }

        @DexIgnore
        public final a p(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).p(str);
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).b(str);
            return this;
        }

        @DexIgnore
        public final a d(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).d(str);
            return this;
        }

        @DexIgnore
        public final a c(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).c(str);
            return this;
        }

        @DexIgnore
        public final a e(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).e(str);
            return this;
        }

        @DexIgnore
        public final a f(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).f(j);
            return this;
        }

        @DexIgnore
        public final a g(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).h(i);
            return this;
        }

        @DexIgnore
        public final a h(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).i(i);
            return this;
        }

        @DexIgnore
        public final a i(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).j(i);
            return this;
        }

        @DexIgnore
        public final a b(boolean z) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).b(z);
            return this;
        }

        @DexIgnore
        public final a a(rp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a((rp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a c(Iterable<? extends Integer> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).c(iterable);
            return this;
        }

        @DexIgnore
        public final a b(Iterable<? extends pp2> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).b(iterable);
            return this;
        }

        @DexIgnore
        public final a a(Iterable<? extends rp2> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(iterable);
            return this;
        }

        @DexIgnore
        public final a a(int i, zp2 zp2) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(i, zp2);
            return this;
        }

        @DexIgnore
        public final a a(zp2 zp2) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(zp2);
            return this;
        }

        @DexIgnore
        public final a a(zp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a((zp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a a(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(j);
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public final a a(boolean z) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((vp2) ((bw2.a) this).b).a(z);
            return this;
        }
    }

    /*
    static {
        vp2 vp2 = new vp2();
        zzaw = vp2;
        bw2.a(vp2.class, vp2);
    }
    */

    @DexIgnore
    public static a z0() {
        return (a) zzaw.e();
    }

    @DexIgnore
    public final String A() {
        return this.zzab;
    }

    @DexIgnore
    public final String B() {
        return this.zzac;
    }

    @DexIgnore
    public final boolean C() {
        return (this.zzc & 8388608) != 0;
    }

    @DexIgnore
    public final boolean D() {
        return this.zzad;
    }

    @DexIgnore
    public final List<pp2> E() {
        return this.zzae;
    }

    @DexIgnore
    public final String F() {
        return this.zzaf;
    }

    @DexIgnore
    public final boolean G() {
        return (this.zzc & 33554432) != 0;
    }

    @DexIgnore
    public final int H() {
        return this.zzag;
    }

    @DexIgnore
    public final String I() {
        return this.zzaj;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 536870912) != 0;
    }

    @DexIgnore
    public final long K() {
        return this.zzak;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 1073741824) != 0;
    }

    @DexIgnore
    public final long M() {
        return this.zzal;
    }

    @DexIgnore
    public final String N() {
        return this.zzam;
    }

    @DexIgnore
    public final boolean O() {
        return (this.zzd & 2) != 0;
    }

    @DexIgnore
    public final int P() {
        return this.zze;
    }

    @DexIgnore
    public final int Q() {
        return this.zzao;
    }

    @DexIgnore
    public final String R() {
        return this.zzap;
    }

    @DexIgnore
    public final boolean S() {
        return (this.zzd & 16) != 0;
    }

    @DexIgnore
    public final long T() {
        return this.zzas;
    }

    @DexIgnore
    public final String U() {
        return this.zzau;
    }

    @DexIgnore
    public final void V() {
        jw2<rp2> jw2 = this.zzf;
        if (!jw2.zza()) {
            this.zzf = bw2.a(jw2);
        }
    }

    @DexIgnore
    public final void W() {
        this.zzf = bw2.o();
    }

    @DexIgnore
    public final void X() {
        jw2<zp2> jw2 = this.zzg;
        if (!jw2.zza()) {
            this.zzg = bw2.a(jw2);
        }
    }

    @DexIgnore
    public final void Y() {
        this.zzc &= -17;
        this.zzk = 0;
    }

    @DexIgnore
    public final void Z() {
        this.zzc &= -33;
        this.zzl = 0;
    }

    @DexIgnore
    public final void a(int i, rp2 rp2) {
        rp2.getClass();
        V();
        this.zzf.set(i, rp2);
    }

    @DexIgnore
    public final void a0() {
        this.zzc &= -2097153;
        this.zzab = zzaw.zzab;
    }

    @DexIgnore
    public final rp2 b(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final void b0() {
        this.zzae = bw2.o();
    }

    @DexIgnore
    public final zp2 c(int i) {
        return this.zzg.get(i);
    }

    @DexIgnore
    public final void c0() {
        this.zzc &= Integer.MAX_VALUE;
        this.zzam = zzaw.zzam;
    }

    @DexIgnore
    public final void d(int i) {
        this.zzc |= 1;
        this.zze = i;
    }

    @DexIgnore
    public final List<rp2> d0() {
        return this.zzf;
    }

    @DexIgnore
    public final void e(int i) {
        V();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final int e0() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void f(int i) {
        X();
        this.zzg.remove(i);
    }

    @DexIgnore
    public final List<zp2> f0() {
        return this.zzg;
    }

    @DexIgnore
    public final int g() {
        return this.zzq;
    }

    @DexIgnore
    public final int g0() {
        return this.zzg.size();
    }

    @DexIgnore
    public final void h(String str) {
        str.getClass();
        this.zzc |= 65536;
        this.zzw = str;
    }

    @DexIgnore
    public final boolean h0() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final void i(String str) {
        str.getClass();
        this.zzc |= 262144;
        this.zzy = str;
    }

    @DexIgnore
    public final long i0() {
        return this.zzh;
    }

    @DexIgnore
    public final void j(String str) {
        str.getClass();
        this.zzc |= ImageObject.DATA_SIZE;
        this.zzab = str;
    }

    @DexIgnore
    public final boolean j0() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final void k(String str) {
        str.getClass();
        this.zzc |= 4194304;
        this.zzac = str;
    }

    @DexIgnore
    public final long k0() {
        return this.zzi;
    }

    @DexIgnore
    public final void l(String str) {
        str.getClass();
        this.zzc |= 16777216;
        this.zzaf = str;
    }

    @DexIgnore
    public final boolean l0() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final void m(String str) {
        str.getClass();
        this.zzc |= SQLiteDatabase.CREATE_IF_NECESSARY;
        this.zzaj = str;
    }

    @DexIgnore
    public final long m0() {
        return this.zzj;
    }

    @DexIgnore
    public final void n(String str) {
        str.getClass();
        this.zzc |= RecyclerView.UNDEFINED_DURATION;
        this.zzam = str;
    }

    @DexIgnore
    public final boolean n0() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final void o(String str) {
        str.getClass();
        this.zzd |= 4;
        this.zzap = str;
    }

    @DexIgnore
    public final long o0() {
        return this.zzk;
    }

    @DexIgnore
    public final long p() {
        return this.zzu;
    }

    @DexIgnore
    public final boolean p0() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 32768) != 0;
    }

    @DexIgnore
    public final long q0() {
        return this.zzl;
    }

    @DexIgnore
    public final long r() {
        return this.zzv;
    }

    @DexIgnore
    public final String r0() {
        return this.zzm;
    }

    @DexIgnore
    public final String s() {
        return this.zzw;
    }

    @DexIgnore
    public final String s0() {
        return this.zzn;
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 131072) != 0;
    }

    @DexIgnore
    public final String t0() {
        return this.zzo;
    }

    @DexIgnore
    public final boolean u() {
        return this.zzx;
    }

    @DexIgnore
    public final String u0() {
        return this.zzp;
    }

    @DexIgnore
    public final String v() {
        return this.zzy;
    }

    @DexIgnore
    public final String v0() {
        return this.zzr;
    }

    @DexIgnore
    public final boolean w() {
        return (this.zzc & 524288) != 0;
    }

    @DexIgnore
    public final String w0() {
        return this.zzs;
    }

    @DexIgnore
    public final long x() {
        return this.zzz;
    }

    @DexIgnore
    public final String x0() {
        return this.zzt;
    }

    @DexIgnore
    public final boolean y() {
        return (this.zzc & 1048576) != 0;
    }

    @DexIgnore
    public final boolean y0() {
        return (this.zzc & 16384) != 0;
    }

    @DexIgnore
    public final int z() {
        return this.zzaa;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final void b(long j) {
        this.zzc |= 4;
        this.zzi = j;
    }

    @DexIgnore
    public final void c(long j) {
        this.zzc |= 8;
        this.zzj = j;
    }

    @DexIgnore
    public final void g(int i) {
        this.zzc |= 1024;
        this.zzq = i;
    }

    @DexIgnore
    public final void p(String str) {
        str.getClass();
        this.zzd |= 64;
        this.zzau = str;
    }

    @DexIgnore
    public final void d(long j) {
        this.zzc |= 16;
        this.zzk = j;
    }

    @DexIgnore
    public final void e(long j) {
        this.zzc |= 32;
        this.zzl = j;
    }

    @DexIgnore
    public final void f(String str) {
        str.getClass();
        this.zzc |= 4096;
        this.zzs = str;
    }

    @DexIgnore
    public final void a(rp2 rp2) {
        rp2.getClass();
        V();
        this.zzf.add(rp2);
    }

    @DexIgnore
    public final void b(String str) {
        str.getClass();
        this.zzc |= 128;
        this.zzn = str;
    }

    @DexIgnore
    public final void c(String str) {
        str.getClass();
        this.zzc |= 256;
        this.zzo = str;
    }

    @DexIgnore
    public final void g(String str) {
        str.getClass();
        this.zzc |= 8192;
        this.zzt = str;
    }

    @DexIgnore
    public final void h(long j) {
        this.zzc |= 524288;
        this.zzz = j;
    }

    @DexIgnore
    public final void i(int i) {
        this.zzc |= 33554432;
        this.zzag = i;
    }

    @DexIgnore
    public final void j(long j) {
        this.zzc |= 1073741824;
        this.zzal = j;
    }

    @DexIgnore
    public final void k(long j) {
        this.zzd |= 16;
        this.zzas = j;
    }

    @DexIgnore
    public final void l(long j) {
        this.zzd |= 32;
        this.zzat = j;
    }

    @DexIgnore
    public final void d(String str) {
        str.getClass();
        this.zzc |= 512;
        this.zzp = str;
    }

    @DexIgnore
    public final void e(String str) {
        str.getClass();
        this.zzc |= 2048;
        this.zzr = str;
    }

    @DexIgnore
    public final void f(long j) {
        this.zzc |= 16384;
        this.zzu = j;
    }

    @DexIgnore
    public final void h(int i) {
        this.zzc |= 1048576;
        this.zzaa = i;
    }

    @DexIgnore
    public final void i(long j) {
        this.zzc |= 536870912;
        this.zzak = j;
    }

    @DexIgnore
    public final void j(int i) {
        this.zzd |= 2;
        this.zzao = i;
    }

    @DexIgnore
    public final void a(Iterable<? extends rp2> iterable) {
        V();
        ju2.a(iterable, this.zzf);
    }

    @DexIgnore
    public final boolean b() {
        return (this.zzc & 1024) != 0;
    }

    @DexIgnore
    public final void c(Iterable<? extends Integer> iterable) {
        hw2 hw2 = this.zzar;
        if (!hw2.zza()) {
            int size = hw2.size();
            this.zzar = hw2.zzb(size == 0 ? 10 : size << 1);
        }
        ju2.a(iterable, this.zzar);
    }

    @DexIgnore
    public final void g(long j) {
        this.zzc |= 32768;
        this.zzv = j;
    }

    @DexIgnore
    public final void b(boolean z) {
        this.zzc |= 8388608;
        this.zzad = z;
    }

    @DexIgnore
    public final void a(int i, zp2 zp2) {
        zp2.getClass();
        X();
        this.zzg.set(i, zp2);
    }

    @DexIgnore
    public final void b(Iterable<? extends pp2> iterable) {
        jw2<pp2> jw2 = this.zzae;
        if (!jw2.zza()) {
            this.zzae = bw2.a(jw2);
        }
        ju2.a(iterable, this.zzae);
    }

    @DexIgnore
    public final void a(zp2 zp2) {
        zp2.getClass();
        X();
        this.zzg.add(zp2);
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 2;
        this.zzh = j;
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 64;
        this.zzm = str;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.zzc |= 131072;
        this.zzx = z;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new vp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzaw, "\u0001,\u0000\u0002\u00014,\u0000\u0004\u0000\u0001\u1004\u0000\u0002\u001b\u0003\u001b\u0004\u1002\u0001\u0005\u1002\u0002\u0006\u1002\u0003\u0007\u1002\u0005\b\u1008\u0006\t\u1008\u0007\n\u1008\b\u000b\u1008\t\f\u1004\n\r\u1008\u000b\u000e\u1008\f\u0010\u1008\r\u0011\u1002\u000e\u0012\u1002\u000f\u0013\u1008\u0010\u0014\u1007\u0011\u0015\u1008\u0012\u0016\u1002\u0013\u0017\u1004\u0014\u0018\u1008\u0015\u0019\u1008\u0016\u001a\u1002\u0004\u001c\u1007\u0017\u001d\u001b\u001e\u1008\u0018\u001f\u1004\u0019 \u1004\u001a!\u1004\u001b\"\u1008\u001c#\u1002\u001d$\u1002\u001e%\u1008\u001f&\u1008 '\u1004!)\u1008\",\u1009#-\u001d.\u1002$/\u1002%2\u1008&4\u1008'", new Object[]{"zzc", "zzd", "zze", "zzf", rp2.class, "zzg", zp2.class, "zzh", "zzi", "zzj", "zzl", "zzm", "zzn", "zzo", "zzp", "zzq", "zzr", "zzs", "zzt", "zzu", "zzv", "zzw", "zzx", "zzy", "zzz", "zzaa", "zzab", "zzac", "zzk", "zzad", "zzae", pp2.class, "zzaf", "zzag", "zzah", "zzai", "zzaj", "zzak", "zzal", "zzam", "zzan", "zzao", "zzap", "zzaq", "zzar", "zzas", "zzat", "zzau", "zzav"});
            case 4:
                return zzaw;
            case 5:
                wx2<vp2> wx2 = zzax;
                if (wx2 == null) {
                    synchronized (vp2.class) {
                        wx2 = zzax;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzaw);
                            zzax = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
