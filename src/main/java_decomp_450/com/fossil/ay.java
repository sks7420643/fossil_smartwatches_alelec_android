package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7530898992688511851L;

    @DexIgnore
    public ay(Throwable th) {
        super("Unexpected exception thrown by non-Glide code", th);
    }
}
