package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq2 implements jq2 {
    @DexIgnore
    public static oq2 c;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ContentObserver b;

    @DexIgnore
    public oq2(Context context) {
        this.a = context;
        this.b = new qq2(this, null);
        context.getContentResolver().registerContentObserver(cq2.a, true, this.b);
    }

    @DexIgnore
    public static oq2 a(Context context) {
        oq2 oq2;
        synchronized (oq2.class) {
            if (c == null) {
                c = w6.a(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new oq2(context) : new oq2();
            }
            oq2 = c;
        }
        return oq2;
    }

    @DexIgnore
    /* renamed from: b */
    public final String zza(String str) {
        if (this.a == null) {
            return null;
        }
        try {
            return (String) mq2.a(new nq2(this, str));
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    @DexIgnore
    public oq2() {
        this.a = null;
        this.b = null;
    }

    @DexIgnore
    public static synchronized void a() {
        synchronized (oq2.class) {
            if (!(c == null || c.a == null || c.b == null)) {
                c.a.getContentResolver().unregisterContentObserver(c.b);
            }
            c = null;
        }
    }

    @DexIgnore
    public final /* synthetic */ String a(String str) {
        return cq2.a(this.a.getContentResolver(), str, (String) null);
    }
}
