package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vg1 {
    @DexIgnore
    public /* synthetic */ vg1(zd7 zd7) {
    }

    @DexIgnore
    public final qk1 a(byte b) {
        qk1 qk1;
        qk1[] values = qk1.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                qk1 = null;
                break;
            }
            qk1 = values[i];
            if (qk1.b == b) {
                break;
            }
            i++;
        }
        return qk1 != null ? qk1 : qk1.UNKNOWN;
    }
}
