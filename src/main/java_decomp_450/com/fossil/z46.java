package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z46 extends fl4<c, d, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public DianaPreset e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository h;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository i;
    @DexIgnore
    public /* final */ WatchFaceRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ DianaPreset a;

        @DexIgnore
        public c(DianaPreset dianaPreset) {
            ee7.b(dianaPreset, "mPreset");
            this.a = dianaPreset;
        }

        @DexIgnore
        public final DianaPreset a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int $errorCode$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ DianaPreset $it;
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $permissionErrorCodes$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(DianaPreset dianaPreset, fb7 fb7, e eVar, int i, ArrayList arrayList) {
                super(2, fb7);
                this.$it = dianaPreset;
                this.this$0 = eVar;
                this.$errorCode$inlined = i;
                this.$permissionErrorCodes$inlined = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7, this.this$0, this.$errorCode$inlined, this.$permissionErrorCodes$inlined);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    z46 z46 = z46.this;
                    DianaPreset dianaPreset = this.$it;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (z46.a(dianaPreset, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                z46.this.a(new b(this.$errorCode$inlined, this.$permissionErrorCodes$inlined));
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (z46.this.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetDianaPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_PRESET_APPS_DATA) {
                    z46.this.a(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "onReceive - success");
                        z46.this.a(new d());
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "onReceive - failed isSettingChangedOnly");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    DianaPreset e = z46.this.e();
                    if (e == null || xh7.b(z46.this.b(), null, null, new a(e, null, this, intExtra2, integerArrayListExtra), 3, null) == null) {
                        z46.this.a(new b(intExtra2, integerArrayListExtra));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase", f = "SetDianaPresetToWatchUseCase.kt", l = {86}, m = "run")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ z46 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(z46 z46, fb7 fb7) {
            super(fb7);
            this.this$0 = z46;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase", f = "SetDianaPresetToWatchUseCase.kt", l = {96}, m = "setPresetToDb")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ z46 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(z46 z46, fb7 fb7) {
            super(fb7);
            this.this$0 = z46;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((DianaPreset) null, this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public z46(DianaPresetRepository dianaPresetRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository) {
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(complicationLastSettingRepository, "mLastSettingRepository");
        ee7.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        ee7.b(watchFaceRepository, "watchFaceRepository");
        this.g = dianaPresetRepository;
        this.h = complicationLastSettingRepository;
        this.i = watchAppLastSettingRepository;
        this.j = watchFaceRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SetDianaPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final DianaPreset e() {
        return this.e;
    }

    @DexIgnore
    public final void f() {
        nj5.d.a(this.f, CommunicateMode.SET_PRESET_APPS_DATA);
    }

    @DexIgnore
    public final void g() {
        nj5.d.b(this.f, CommunicateMode.SET_PRESET_APPS_DATA);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00e2, code lost:
        if (com.fossil.pb7.a(com.portfolio.platform.PortfolioApp.g0.c().a(r2, r1, r9, r3)) == null) goto L_0x00e6;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.z46.c r9, com.fossil.fb7<java.lang.Object> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.z46.f
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.z46$f r0 = (com.fossil.z46.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.z46$f r0 = new com.fossil.z46$f
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0052
            if (r2 != r3) goto L_0x004a
            java.lang.Object r9 = r0.L$7
            com.misfit.frameworks.buttonservice.model.background.BackgroundConfig r9 = (com.misfit.frameworks.buttonservice.model.background.BackgroundConfig) r9
            java.lang.Object r1 = r0.L$6
            com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings r1 = (com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings) r1
            java.lang.Object r2 = r0.L$5
            com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings r2 = (com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings) r2
            java.lang.Object r3 = r0.L$4
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r4 = r0.L$3
            com.portfolio.platform.data.model.diana.preset.DianaPreset r4 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r4
            java.lang.Object r4 = r0.L$2
            com.fossil.z46$c r4 = (com.fossil.z46.c) r4
            java.lang.Object r4 = r0.L$1
            com.fossil.z46$c r4 = (com.fossil.z46.c) r4
            java.lang.Object r0 = r0.L$0
            com.fossil.z46 r0 = (com.fossil.z46) r0
            com.fossil.t87.a(r10)
            goto L_0x00d4
        L_0x004a:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0052:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = "SetDianaPresetToWatchUseCase"
            java.lang.String r4 = "executeUseCase"
            r10.d(r2, r4)
            if (r9 == 0) goto L_0x00e5
            r8.d = r3
            com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = r9.a()
            com.portfolio.platform.data.source.DianaPresetRepository r2 = r8.g
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            java.lang.String r4 = r4.c()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r2 = r2.getActivePresetBySerial(r4)
            r8.e = r2
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r2 = r2.c()
            java.util.ArrayList r4 = r10.getWatchapps()
            com.google.gson.Gson r5 = new com.google.gson.Gson
            r5.<init>()
            com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings r4 = com.fossil.xc5.b(r4, r5)
            java.util.ArrayList r5 = r10.getComplications()
            com.google.gson.Gson r6 = new com.google.gson.Gson
            r6.<init>()
            com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings r5 = com.fossil.xc5.a(r5, r6)
            com.portfolio.platform.data.source.WatchFaceRepository r6 = r8.j
            java.lang.String r7 = r10.getWatchFaceId()
            com.portfolio.platform.data.model.diana.preset.WatchFace r6 = r6.getWatchFaceWithId(r7)
            if (r6 == 0) goto L_0x00b5
            java.util.ArrayList r7 = r10.getComplications()
            com.misfit.frameworks.buttonservice.model.background.BackgroundConfig r6 = com.fossil.yc5.a(r6, r7)
            goto L_0x00b6
        L_0x00b5:
            r6 = 0
        L_0x00b6:
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r9
            r0.L$3 = r10
            r0.L$4 = r2
            r0.L$5 = r4
            r0.L$6 = r5
            r0.L$7 = r6
            r0.label = r3
            java.lang.Object r9 = r8.a(r10, r0)
            if (r9 != r1) goto L_0x00cf
            return r1
        L_0x00cf:
            r0 = r8
            r3 = r2
            r2 = r4
            r1 = r5
            r9 = r6
        L_0x00d4:
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r10 = r10.c()
            long r9 = r10.a(r2, r1, r9, r3)
            java.lang.Long r9 = com.fossil.pb7.a(r9)
            if (r9 == 0) goto L_0x00e6
            goto L_0x00f4
        L_0x00e5:
            r0 = r8
        L_0x00e6:
            r9 = -1
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            com.fossil.z46$b r1 = new com.fossil.z46$b
            r1.<init>(r9, r10)
            r0.a(r1)
        L_0x00f4:
            java.lang.Object r9 = new java.lang.Object
            r9.<init>()
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z46.a(com.fossil.z46$c, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d5 A[EDGE_INSN: B:37:0x00d5->B:25:0x00d5 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.portfolio.platform.data.model.diana.preset.DianaPreset r10, com.fossil.fb7<? super com.fossil.i97> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.fossil.z46.g
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.z46$g r0 = (com.fossil.z46.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.z46$g r0 = new com.fossil.z46$g
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "SetDianaPresetToWatchUseCase"
            r4 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r4) goto L_0x0033
            java.lang.Object r10 = r0.L$1
            com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10
            java.lang.Object r0 = r0.L$0
            com.fossil.z46 r0 = (com.fossil.z46) r0
            com.fossil.t87.a(r11)
            goto L_0x006b
        L_0x0033:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003b:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "setPresetToDb "
            r2.append(r5)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            r11.d(r3, r2)
            r10.setActive(r4)
            com.portfolio.platform.data.source.DianaPresetRepository r11 = r9.g
            r0.L$0 = r9
            r0.L$1 = r10
            r0.label = r4
            java.lang.Object r11 = r11.upsertPreset(r10, r0)
            if (r11 != r1) goto L_0x006a
            return r1
        L_0x006a:
            r0 = r9
        L_0x006b:
            java.util.Calendar r11 = java.util.Calendar.getInstance()
            java.lang.String r1 = "Calendar.getInstance()"
            com.fossil.ee7.a(r11, r1)
            java.util.Date r11 = r11.getTime()
            java.lang.String r11 = com.fossil.zd5.y(r11)
            java.util.ArrayList r1 = r10.getComplications()
            java.util.Iterator r1 = r1.iterator()
        L_0x0084:
            boolean r2 = r1.hasNext()
            java.lang.String r4 = ""
            java.lang.String r5 = "updatedAt"
            java.lang.String r6 = "setPresetToWatch success save last user setting "
            if (r2 == 0) goto L_0x00d5
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r2
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r6)
            java.lang.String r6 = r2.getSettings()
            r8.append(r6)
            java.lang.String r6 = r8.toString()
            r7.d(r3, r6)
            java.lang.String r6 = r2.getSettings()
            boolean r6 = com.fossil.sc5.a(r6)
            if (r6 != 0) goto L_0x0084
            com.portfolio.platform.data.source.ComplicationLastSettingRepository r6 = r0.h
            com.portfolio.platform.data.model.diana.ComplicationLastSetting r7 = new com.portfolio.platform.data.model.diana.ComplicationLastSetting
            java.lang.String r8 = r2.getId()
            com.fossil.ee7.a(r11, r5)
            java.lang.String r2 = r2.getSettings()
            if (r2 == 0) goto L_0x00ce
            r4 = r2
        L_0x00ce:
            r7.<init>(r8, r11, r4)
            r6.upsertComplicationLastSetting(r7)
            goto L_0x0084
        L_0x00d5:
            java.util.ArrayList r10 = r10.getWatchapps()
            java.util.Iterator r10 = r10.iterator()
        L_0x00dd:
            boolean r1 = r10.hasNext()
            if (r1 == 0) goto L_0x0129
            java.lang.Object r1 = r10.next()
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r1 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r6)
            java.lang.String r8 = r1.getSettings()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r2.d(r3, r7)
            java.lang.String r2 = r1.getSettings()
            boolean r2 = com.fossil.sc5.a(r2)
            if (r2 != 0) goto L_0x00dd
            com.portfolio.platform.data.source.WatchAppLastSettingRepository r2 = r0.i
            com.portfolio.platform.data.model.diana.WatchAppLastSetting r7 = new com.portfolio.platform.data.model.diana.WatchAppLastSetting
            java.lang.String r8 = r1.getId()
            com.fossil.ee7.a(r11, r5)
            java.lang.String r1 = r1.getSettings()
            if (r1 == 0) goto L_0x0121
            goto L_0x0122
        L_0x0121:
            r1 = r4
        L_0x0122:
            r7.<init>(r8, r11, r1)
            r2.upsertWatchAppLastSetting(r7)
            goto L_0x00dd
        L_0x0129:
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z46.a(com.portfolio.platform.data.model.diana.preset.DianaPreset, com.fossil.fb7):java.lang.Object");
    }
}
