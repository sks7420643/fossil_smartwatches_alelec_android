package com.fossil;

import android.content.Intent;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ e d; // = new e();
    @DexIgnore
    public List<WorkoutSetting> e;
    @DexIgnore
    public /* final */ WorkoutSettingRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ List<WorkoutSetting> a;

        @DexIgnore
        public b(List<WorkoutSetting> list) {
            ee7.b(list, "workoutSettings");
            this.a = list;
        }

        @DexIgnore
        public final List<WorkoutSetting> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "errorCodes");
            this.a = i2;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$SetWorkoutSettingReceiver$receive$1", f = "SetWorkoutSettingUseCase.kt", l = {35, 37}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sn6$e$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$SetWorkoutSettingReceiver$receive$1$2", f = "SetWorkoutSettingUseCase.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.sn6$e$a$a  reason: collision with other inner class name */
            public static final class C0173a extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0173a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0173a aVar = new C0173a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
                    return ((C0173a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return sn6.this.a(new d());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    List<WorkoutSetting> b = sn6.this.e;
                    if (b != null) {
                        WorkoutSettingRepository c = sn6.this.f;
                        this.L$0 = yi7;
                        this.L$1 = b;
                        this.label = 1;
                        if (c.upsertWorkoutSettingList(b, this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) this.L$1;
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                tk7 c2 = qj7.c();
                C0173a aVar = new C0173a(this, null);
                this.L$0 = yi7;
                this.label = 2;
                if (vh7.a(c2, aVar, this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "onReceive()");
            if (communicateMode != CommunicateMode.SET_WORKOUT_DETECTION) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "onReceive() - success");
                ik7 unused = xh7.b(sn6.this.b(), null, null, new a(this, null), 3, null);
                return;
            }
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetWorkoutSettingUseCase", "onReceive() - failed, errorCode = " + intExtra);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra);
            }
            sn6.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public sn6(WorkoutSettingRepository workoutSettingRepository) {
        ee7.b(workoutSettingRepository, "mWorkoutSettingRepository");
        this.f = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SetWorkoutSettingUseCase";
    }

    @DexIgnore
    public final void d() {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "registerBroadcastReceiver()");
        nj5.d.a(this.d, CommunicateMode.SET_WORKOUT_DETECTION);
        nj5.d.a(CommunicateMode.SET_WORKOUT_DETECTION);
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "unregisterBroadcastReceiver()");
        nj5.d.b(this.d, CommunicateMode.SET_WORKOUT_DETECTION);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "executeUseCase()");
        if (bVar != null) {
            this.e = bVar.a();
            pb7.a(PortfolioApp.g0.c().a(PortfolioApp.g0.c().c(), ay6.a(bVar.a())));
        }
        return new Object();
    }
}
