package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ic1 extends uh1 {
    @DexIgnore
    public /* final */ qk1 G;
    @DexIgnore
    public /* final */ boolean H;
    @DexIgnore
    public long I;
    @DexIgnore
    public /* final */ ArrayList<u81> J;
    @DexIgnore
    public /* final */ byte[] K;
    @DexIgnore
    public /* final */ qk1 L;
    @DexIgnore
    public /* final */ int M;

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.I = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public qk1 l() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public qk1 o() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean p() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public mw0 a(byte b) {
        return new oa1();
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public void e(rk1 rk1) {
        super.e(rk1);
        this.J.add(new u81(rk1.a, System.currentTimeMillis(), rk1.b));
        if (this.J.size() >= this.M) {
            ((uh1) this).E = true;
        }
    }
}
