package com.fossil;

import java.util.Map;

public final class qg4 implements sg4 {

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|(3:25|26|28)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0084 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0090 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.fossil.mg4[] r0 = com.fossil.mg4.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.qg4.a.a = r0
                com.fossil.mg4 r1 = com.fossil.mg4.EAN_8     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.mg4 r1 = com.fossil.mg4.UPC_E     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.mg4 r1 = com.fossil.mg4.EAN_13     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.mg4 r1 = com.fossil.mg4.UPC_A     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x003e }
                com.fossil.mg4 r1 = com.fossil.mg4.QR_CODE     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.fossil.mg4 r1 = com.fossil.mg4.CODE_39     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.fossil.mg4 r1 = com.fossil.mg4.CODE_93     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.fossil.mg4 r1 = com.fossil.mg4.CODE_128     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x006c }
                com.fossil.mg4 r1 = com.fossil.mg4.ITF     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.fossil.mg4 r1 = com.fossil.mg4.PDF_417     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.fossil.mg4 r1 = com.fossil.mg4.CODABAR     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x0090 }
                com.fossil.mg4 r1 = com.fossil.mg4.DATA_MATRIX     // Catch:{ NoSuchFieldError -> 0x0090 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0090 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0090 }
            L_0x0090:
                int[] r0 = com.fossil.qg4.a.a     // Catch:{ NoSuchFieldError -> 0x009c }
                com.fossil.mg4 r1 = com.fossil.mg4.AZTEC     // Catch:{ NoSuchFieldError -> 0x009c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009c }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009c }
            L_0x009c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qg4.a.<clinit>():void");
        }
        */
    }

    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        sg4 sg4;
        switch (a.a[mg4.ordinal()]) {
            case 1:
                sg4 = new hi4();
                break;
            case 2:
                sg4 = new qi4();
                break;
            case 3:
                sg4 = new gi4();
                break;
            case 4:
                sg4 = new mi4();
                break;
            case 5:
                sg4 = new zi4();
                break;
            case 6:
                sg4 = new ci4();
                break;
            case 7:
                sg4 = new ei4();
                break;
            case 8:
                sg4 = new ai4();
                break;
            case 9:
                sg4 = new ji4();
                break;
            case 10:
                sg4 = new ri4();
                break;
            case 11:
                sg4 = new yh4();
                break;
            case 12:
                sg4 = new ih4();
                break;
            case 13:
                sg4 = new ug4();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format " + mg4);
        }
        return sg4.a(str, mg4, i, i2, map);
    }
}
