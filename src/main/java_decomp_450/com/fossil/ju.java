package com.fossil;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju {
    @DexIgnore
    public static final void a(String str, Throwable th) {
        ee7.b(str, "tag");
        ee7.b(th, "throwable");
        if (cu.c.a() && cu.c.b() <= 6) {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            Log.println(6, str, stringWriter.toString());
        }
    }
}
