package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht3 implements Comparable<ht3>, Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ht3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ Calendar a;
    @DexIgnore
    public /* final */ String b; // = nt3.e().format(this.a.getTime());
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d; // = this.a.get(1);
    @DexIgnore
    public /* final */ int e; // = this.a.getMaximum(7);
    @DexIgnore
    public /* final */ int f; // = this.a.getActualMaximum(5);
    @DexIgnore
    public /* final */ long g; // = this.a.getTimeInMillis();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ht3> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ht3 createFromParcel(Parcel parcel) {
            return ht3.a(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ht3[] newArray(int i) {
            return new ht3[i];
        }
    }

    @DexIgnore
    public ht3(Calendar calendar) {
        calendar.set(5, 1);
        Calendar a2 = nt3.a(calendar);
        this.a = a2;
        this.c = a2.get(2);
    }

    @DexIgnore
    public static ht3 a(long j) {
        Calendar d2 = nt3.d();
        d2.setTimeInMillis(j);
        return new ht3(d2);
    }

    @DexIgnore
    public static ht3 d() {
        return new ht3(nt3.b());
    }

    @DexIgnore
    public int b(ht3 ht3) {
        if (this.a instanceof GregorianCalendar) {
            return ((ht3.d - this.d) * 12) + (ht3.c - this.c);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    @DexIgnore
    public long c() {
        return this.a.getTimeInMillis();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ht3)) {
            return false;
        }
        ht3 ht3 = (ht3) obj;
        if (this.c == ht3.c && this.d == ht3.d) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.c), Integer.valueOf(this.d)});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.d);
        parcel.writeInt(this.c);
    }

    @DexIgnore
    public static ht3 a(int i, int i2) {
        Calendar d2 = nt3.d();
        d2.set(1, i);
        d2.set(2, i2);
        return new ht3(d2);
    }

    @DexIgnore
    public ht3 b(int i) {
        Calendar a2 = nt3.a(this.a);
        a2.add(2, i);
        return new ht3(a2);
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public int a() {
        int firstDayOfWeek = this.a.get(7) - this.a.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.e : firstDayOfWeek;
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(ht3 ht3) {
        return this.a.compareTo(ht3.a);
    }

    @DexIgnore
    public long a(int i) {
        Calendar a2 = nt3.a(this.a);
        a2.set(5, i);
        return a2.getTimeInMillis();
    }
}
