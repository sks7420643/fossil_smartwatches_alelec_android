package com.fossil;

import android.view.ViewGroup;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fk {
    @DexIgnore
    public abstract long a(ViewGroup viewGroup, Transition transition, hk hkVar, hk hkVar2);

    @DexIgnore
    public abstract void a(hk hkVar);

    @DexIgnore
    public abstract String[] a();
}
