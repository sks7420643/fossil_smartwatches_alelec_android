package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e47 {
    @DexIgnore
    public static String l;
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public l57 d; // = null;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;
    @DexIgnore
    public String h; // = null;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public Context j;
    @DexIgnore
    public a47 k; // = null;

    @DexIgnore
    public e47(Context context, int i2, a47 a47) {
        this.j = context;
        this.b = System.currentTimeMillis() / 1000;
        this.c = i2;
        this.g = w37.d(context);
        this.h = v57.m(context);
        this.a = w37.b(context);
        if (a47 != null) {
            this.k = a47;
            if (v57.c(a47.a())) {
                this.a = a47.a();
            }
            if (v57.c(a47.b())) {
                this.g = a47.b();
            }
            if (v57.c(a47.c())) {
                this.h = a47.c();
            }
            this.i = a47.d();
        }
        this.f = w37.c(context);
        this.d = x47.b(context).a(context);
        f47 a2 = a();
        f47 f47 = f47.i;
        this.e = a2 != f47 ? v57.v(context).intValue() : -f47.a();
        if (!j27.b(l)) {
            String e2 = w37.e(context);
            l = e2;
            if (!v57.c(e2)) {
                l = "0";
            }
        }
    }

    @DexIgnore
    public abstract f47 a();

    @DexIgnore
    public abstract boolean a(JSONObject jSONObject);

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public boolean b(JSONObject jSONObject) {
        try {
            a67.a(jSONObject, "ky", this.a);
            jSONObject.put("et", a().a());
            if (this.d != null) {
                jSONObject.put("ui", this.d.b());
                a67.a(jSONObject, "mc", this.d.c());
                int d2 = this.d.d();
                jSONObject.put("ut", d2);
                if (d2 == 0 && v57.z(this.j) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            a67.a(jSONObject, "cui", this.f);
            if (a() != f47.b) {
                a67.a(jSONObject, "av", this.h);
                a67.a(jSONObject, "ch", this.g);
            }
            if (this.i) {
                jSONObject.put("impt", 1);
            }
            a67.a(jSONObject, "mid", l);
            jSONObject.put("idx", this.e);
            jSONObject.put("si", this.c);
            jSONObject.put("ts", this.b);
            jSONObject.put("dts", v57.a(this.j, false));
            return a(jSONObject);
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public a47 c() {
        return this.k;
    }

    @DexIgnore
    public Context d() {
        return this.j;
    }

    @DexIgnore
    public boolean e() {
        return this.i;
    }

    @DexIgnore
    public String f() {
        try {
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            return jSONObject.toString();
        } catch (Throwable unused) {
            return "";
        }
    }
}
