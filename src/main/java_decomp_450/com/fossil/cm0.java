package com.fossil;

import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.fossil.blesdk.utils.TextEncryption$loadSymmetricSecretKey$1", f = "TextEncryption.kt", l = {}, m = "invokeSuspend")
public final class cm0 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public int b;

    @DexIgnore
    public cm0(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        cm0 cm0 = new cm0(fb7);
        cm0.a = (yi7) obj;
        return cm0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((cm0) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.b == 0) {
            t87.a(obj);
            yn0 yn0 = yn0.h;
            synchronized (yn0.g) {
                if (Build.VERSION.SDK_INT >= 23) {
                    yn0 yn02 = yn0.h;
                    if (yn0.f == null) {
                        SecretKey b2 = mt0.b.b("c");
                        SharedPreferences a2 = yz0.a(j91.f);
                        if (a2 != null) {
                            byte[] bArr = null;
                            String string = a2.getString("d", null);
                            byte[] decode = string != null ? Base64.decode(string, 0) : null;
                            String string2 = a2.getString("e", null);
                            if (string2 != null) {
                                bArr = Base64.decode(string2, 0);
                            }
                            if (decode == null || bArr == null) {
                                SecureRandom secureRandom = new SecureRandom();
                                KeyGenerator instance = KeyGenerator.getInstance("AES");
                                instance.init(256, secureRandom);
                                SecretKey generateKey = instance.generateKey();
                                ee7.a((Object) generateKey, "keyGenerator.generateKey()");
                                byte[] encoded = generateKey.getEncoded();
                                Cipher instance2 = Cipher.getInstance(fo1.GCM_NO_PADDING.a);
                                instance2.init(1, b2);
                                decode = instance2.doFinal(encoded);
                                ee7.a((Object) instance2, "cipher");
                                bArr = instance2.getIV();
                                a2.edit().putString("d", Base64.encodeToString(decode, 0)).putString("e", Base64.encodeToString(bArr, 0)).apply();
                            }
                            Cipher instance3 = Cipher.getInstance(fo1.GCM_NO_PADDING.a);
                            instance3.init(2, b2, new GCMParameterSpec(128, bArr));
                            yn0 yn03 = yn0.h;
                            yn0.f = instance3.doFinal(decode);
                        }
                    }
                }
                i97 i97 = i97.a;
            }
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
