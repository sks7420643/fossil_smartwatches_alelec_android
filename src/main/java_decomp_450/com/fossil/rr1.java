package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rr1 extends uh1 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ qk1 I;
    @DexIgnore
    public /* final */ qk1 J;
    @DexIgnore
    public /* final */ ln0 K;
    @DexIgnore
    public /* final */ short L;

    @DexIgnore
    public rr1(ln0 ln0, short s, qa1 qa1, ri1 ri1, int i) {
        super(qa1, ri1, i);
        this.K = ln0;
        this.L = s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.a).putShort(this.L).array();
        ee7.a((Object) array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.a()).putShort(this.L).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        qk1 qk1 = qk1.FTC;
        this.I = qk1;
        this.J = qk1;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final long a(rk1 rk1) {
        if (rk1.a == qk1.FTC) {
            byte[] bArr = rk1.b;
            if (bArr.length >= 9) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                short s = order.getShort(1);
                byte b2 = order.get(3);
                byte b3 = order.get(4);
                long b4 = yz0.b(order.getInt(5));
                if (b == ln0.j.a() && this.L == s && b2 == dr0.c.b && b3 == this.K.a && b4 > 0) {
                    return b4;
                }
                return 0;
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.y0, yz0.a(this.L));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 l() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final byte[] n() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 o() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final byte[] q() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final mw0 a(byte b) {
        return dr0.h.a(b);
    }
}
