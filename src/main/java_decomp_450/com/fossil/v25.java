package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v25 extends u25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362998, 1);
        F.put(2131363339, 2);
        F.put(2131363116, 3);
        F.put(2131362114, 4);
        F.put(2131363342, 5);
        F.put(2131363254, 6);
        F.put(2131362887, 7);
        F.put(2131362081, 8);
        F.put(2131362368, 9);
        F.put(2131362635, 10);
        F.put(2131362472, 11);
        F.put(2131362378, 12);
    }
    */

    @DexIgnore
    public v25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 13, E, F));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public v25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[8], (ConstraintLayout) objArr[4], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[12], (FlexibleButton) objArr[11], (RTLImageView) objArr[10], (ProgressBar) objArr[7], (RelativeLayout) objArr[0], (ViewPager2) objArr[1], (TabLayout) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5]);
        this.D = -1;
        ((u25) this).x.setTag(null);
        a(view);
        f();
    }
}
