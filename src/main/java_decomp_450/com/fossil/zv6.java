package com.fossil;

import com.fossil.fl4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ DianaPresetRepository d;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public zv6(DianaPresetRepository dianaPresetRepository, CustomizeRealDataRepository customizeRealDataRepository) {
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(customizeRealDataRepository, "mRealDataRepository");
        this.d = dianaPresetRepository;
        this.e = customizeRealDataRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "DSTChangeUseCase";
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        DianaPreset activePresetBySerial;
        String str;
        String c2 = PortfolioApp.g0.c().c();
        if (be5.o.f(c2) && (activePresetBySerial = this.d.getActivePresetBySerial(c2)) != null) {
            ComplicationAppMappingSettings a2 = xc5.a(activePresetBySerial.getComplications(), new Gson());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DSTChangeUseCase", "DST change, reset complications " + a2);
            PortfolioApp.g0.c().a(a2, c2);
            for (T t : activePresetBySerial.getComplications()) {
                if (ee7.a((Object) t.getId(), (Object) "second-timezone")) {
                    try {
                        SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().a(t.getSettings(), SecondTimezoneSetting.class);
                        if (secondTimezoneSetting != null) {
                            double timezoneRawOffsetById = (((double) ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())) / 60.0d) - ((double) (((float) ye5.a()) / ((float) 3600)));
                            if (timezoneRawOffsetById > ((double) 0)) {
                                StringBuilder sb = new StringBuilder();
                                sb.append('+');
                                sb.append(timezoneRawOffsetById);
                                sb.append('h');
                                str = sb.toString();
                            } else {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(timezoneRawOffsetById);
                                sb2.append('h');
                                str = sb2.toString();
                            }
                            this.e.upsertCustomizeRealData(new CustomizeRealData("second-timezone", str));
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("DSTChangeUseCase", "exception " + e2);
                    }
                }
            }
        }
        return new d();
    }
}
