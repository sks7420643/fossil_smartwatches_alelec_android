package com.fossil;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk5<T> {
    @DexIgnore
    public /* final */ Set<T> a; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ Object b; // = new Object();

    @DexIgnore
    public final boolean a(T t) {
        boolean add;
        synchronized (this.b) {
            add = !this.a.contains(t) ? this.a.add(t) : false;
        }
        return add;
    }

    @DexIgnore
    public final boolean b(T t) {
        boolean remove;
        synchronized (this.b) {
            remove = this.a.remove(t);
        }
        return remove;
    }

    @DexIgnore
    public final T a(gd7<? super T, Boolean> gd7) {
        T t;
        ee7.b(gd7, "predicate");
        synchronized (this.b) {
            Iterator<T> it = this.a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (gd7.invoke(t).booleanValue()) {
                    break;
                }
            }
        }
        return t;
    }
}
