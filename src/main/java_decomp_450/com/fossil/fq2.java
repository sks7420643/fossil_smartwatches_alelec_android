package com.fossil;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.StrictMode;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq2 implements jq2 {
    @DexIgnore
    public static /* final */ Map<Uri, fq2> g; // = new n4();
    @DexIgnore
    public static /* final */ String[] h; // = {"key", "value"};
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentObserver c; // = new hq2(this, null);
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public volatile Map<String, String> e;
    @DexIgnore
    public /* final */ List<kq2> f; // = new ArrayList();

    @DexIgnore
    public fq2(ContentResolver contentResolver, Uri uri) {
        this.a = contentResolver;
        this.b = uri;
        contentResolver.registerContentObserver(uri, false, this.c);
    }

    @DexIgnore
    public static fq2 a(ContentResolver contentResolver, Uri uri) {
        fq2 fq2;
        synchronized (fq2.class) {
            fq2 = g.get(uri);
            if (fq2 == null) {
                try {
                    fq2 fq22 = new fq2(contentResolver, uri);
                    try {
                        g.put(uri, fq22);
                    } catch (SecurityException unused) {
                    }
                    fq2 = fq22;
                } catch (SecurityException unused2) {
                }
            }
        }
        return fq2;
    }

    @DexIgnore
    public static synchronized void e() {
        synchronized (fq2.class) {
            for (fq2 fq2 : g.values()) {
                fq2.a.unregisterContentObserver(fq2.c);
            }
            g.clear();
        }
    }

    @DexIgnore
    public final void b() {
        synchronized (this.d) {
            this.e = null;
            tq2.c();
        }
        synchronized (this) {
            for (kq2 kq2 : this.f) {
                kq2.zza();
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Map c() {
        Map map;
        Cursor query = this.a.query(this.b, h, null, null, null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            if (count <= 256) {
                map = new n4(count);
            } else {
                map = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                map.put(query.getString(0), query.getString(1));
            }
            query.close();
            return map;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final Map<String, String> d() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            Map<String, String> map = (Map) mq2.a(new iq2(this));
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return map;
        } catch (SQLiteException | IllegalStateException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return null;
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.jq2
    public final /* synthetic */ Object zza(String str) {
        return a().get(str);
    }

    @DexIgnore
    public final Map<String, String> a() {
        Map<String, String> map = this.e;
        if (map == null) {
            synchronized (this.d) {
                map = this.e;
                if (map == null) {
                    map = d();
                    this.e = map;
                }
            }
        }
        if (map != null) {
            return map;
        }
        return Collections.emptyMap();
    }
}
