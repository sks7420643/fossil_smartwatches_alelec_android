package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.nq4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lq4 extends go5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<q75> f;
    @DexIgnore
    public nq4 g;
    @DexIgnore
    public rj4 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lq4.j;
        }

        @DexIgnore
        public final lq4 b() {
            return new lq4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lq4 a;

        @DexIgnore
        public b(lq4 lq4) {
            this.a = lq4;
        }

        @DexIgnore
        public final void onClick(View view) {
            lq4.b(this.a).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ lq4 a;

        @DexIgnore
        public c(lq4 lq4) {
            this.a = lq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            q75 q75 = (q75) lq4.a(this.a).a();
            if (q75 != null) {
                ee7.a((Object) bool, "it");
                if (bool.booleanValue()) {
                    ConstraintLayout constraintLayout = q75.r;
                    ee7.a((Object) constraintLayout, "clError");
                    constraintLayout.setVisibility(8);
                    ProgressBar progressBar = q75.u;
                    ee7.a((Object) progressBar, "prg");
                    progressBar.setVisibility(0);
                    return;
                }
                ProgressBar progressBar2 = q75.u;
                ee7.a((Object) progressBar2, "prg");
                progressBar2.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<nq4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ lq4 a;

        @DexIgnore
        public d(lq4 lq4) {
            this.a = lq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(nq4.b bVar) {
            q75 q75;
            if (bVar.a()) {
                q75 q752 = (q75) lq4.a(this.a).a();
                if (q752 != null) {
                    ProgressBar progressBar = q752.u;
                    ee7.a((Object) progressBar, "prg");
                    progressBar.setVisibility(8);
                    this.a.f1();
                }
            } else if (bVar.b() && (q75 = (q75) lq4.a(this.a).a()) != null) {
                ProgressBar progressBar2 = q75.u;
                ee7.a((Object) progressBar2, "prg");
                progressBar2.setVisibility(8);
                this.a.g1();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ lq4 a;

        @DexIgnore
        public e(lq4 lq4) {
            this.a = lq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            q75 q75 = (q75) lq4.a(this.a).a();
            if (q75 != null) {
                FlexibleTextView flexibleTextView = q75.t;
                ee7.a((Object) flexibleTextView, "ftvError");
                flexibleTextView.setText(xe5.b.a(num, ""));
                ConstraintLayout constraintLayout = q75.r;
                ee7.a((Object) constraintLayout, "clError");
                constraintLayout.setVisibility(0);
            }
        }
    }

    /*
    static {
        String simpleName = lq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCMainFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(lq4 lq4) {
        qw6<q75> qw6 = lq4.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ nq4 b(lq4 lq4) {
        nq4 nq4 = lq4.g;
        if (nq4 != null) {
            return nq4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return j;
    }

    @DexIgnore
    public final void f1() {
        nc b2 = getChildFragmentManager().b();
        qw6<q75> qw6 = this.f;
        if (qw6 != null) {
            q75 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "binding.get()!!");
                View d2 = a2.d();
                ee7.a((Object) d2, "binding.get()!!.root");
                b2.a(d2.getId(), yp4.r.c(), yp4.r.b());
                b2.a();
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        nc b2 = getChildFragmentManager().b();
        qw6<q75> qw6 = this.f;
        if (qw6 != null) {
            q75 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "binding.get()!!");
                View d2 = a2.d();
                ee7.a((Object) d2, "binding.get()!!.root");
                b2.b(d2.getId(), xr4.q.b(), xr4.q.a());
                b2.a();
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        qw6<q75> qw6 = this.f;
        if (qw6 != null) {
            q75 a2 = qw6.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        nq4 nq4 = this.g;
        if (nq4 != null) {
            nq4.b().a(getViewLifecycleOwner(), new c(this));
            nq4 nq42 = this.g;
            if (nq42 != null) {
                nq42.d().a(getViewLifecycleOwner(), new d(this));
                nq4 nq43 = this.g;
                if (nq43 != null) {
                    nq43.c().a(getViewLifecycleOwner(), new e(this));
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == yp4.r.a()) {
            g1();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().e().a(this);
        FragmentActivity requireActivity = requireActivity();
        rj4 rj4 = this.h;
        if (rj4 != null) {
            he a2 = je.a(requireActivity, rj4).a(nq4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(re\u2026ainViewModel::class.java)");
            this.g = (nq4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        q75 q75 = (q75) qb.a(layoutInflater, 2131558645, viewGroup, false, a1());
        this.f = new qw6<>(this, q75);
        ee7.a((Object) q75, "thisBinding");
        return q75.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        nq4 nq4 = this.g;
        if (nq4 != null) {
            nq4.a();
            h1();
            i1();
            return;
        }
        ee7.d("viewModel");
        throw null;
    }
}
