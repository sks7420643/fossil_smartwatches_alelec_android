package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long a;
    @DexIgnore
    public /* final */ /* synthetic */ ti3 b;

    @DexIgnore
    public xi3(ti3 ti3, long j) {
        this.b = ti3;
        this.a = j;
    }

    @DexIgnore
    public final void run() {
        this.b.k().q.a(this.a);
        this.b.e().A().a("Session timeout duration set", Long.valueOf(this.a));
    }
}
