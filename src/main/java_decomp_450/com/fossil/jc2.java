package com.fossil;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc2 {
    @DexIgnore
    public static /* final */ DataType a; // = new DataType("com.google.blood_pressure", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", kc2.a, kc2.e, kc2.i, kc2.j);
    @DexIgnore
    public static /* final */ DataType b; // = new DataType("com.google.blood_glucose", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", kc2.k, kc2.l, ic2.L, kc2.m, kc2.n);
    @DexIgnore
    public static /* final */ DataType c; // = new DataType("com.google.oxygen_saturation", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", kc2.o, kc2.s, kc2.w, kc2.x, kc2.y);
    @DexIgnore
    public static /* final */ DataType d; // = new DataType("com.google.body.temperature", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", kc2.z, kc2.A);
    @DexIgnore
    public static /* final */ DataType e; // = new DataType("com.google.body.temperature.basal", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", kc2.z, kc2.A);
    @DexIgnore
    public static /* final */ DataType f; // = new DataType("com.google.cervical_mucus", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", kc2.B, kc2.C);
    @DexIgnore
    public static /* final */ DataType g; // = new DataType("com.google.cervical_position", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", kc2.D, kc2.E, kc2.F);
    @DexIgnore
    public static /* final */ DataType h; // = new DataType("com.google.menstruation", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", kc2.G);
    @DexIgnore
    public static /* final */ DataType i; // = new DataType("com.google.ovulation_test", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", kc2.H);
    @DexIgnore
    public static /* final */ DataType j; // = new DataType("com.google.vaginal_spotting", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ic2.g0);
    @DexIgnore
    public static /* final */ DataType k; // = new DataType("com.google.blood_pressure.summary", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", kc2.b, kc2.d, kc2.c, kc2.f, kc2.h, kc2.g, kc2.i, kc2.j);
    @DexIgnore
    public static /* final */ DataType l; // = new DataType("com.google.blood_glucose.summary", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", ic2.Z, ic2.a0, ic2.b0, kc2.l, ic2.L, kc2.m, kc2.n);
    @DexIgnore
    public static /* final */ DataType m; // = new DataType("com.google.oxygen_saturation.summary", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", kc2.p, kc2.r, kc2.q, kc2.t, kc2.v, kc2.u, kc2.w, kc2.x, kc2.y);
    @DexIgnore
    public static /* final */ DataType n; // = new DataType("com.google.body.temperature.summary", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", ic2.Z, ic2.a0, ic2.b0, kc2.A);
    @DexIgnore
    public static /* final */ DataType o; // = new DataType("com.google.body.temperature.basal.summary", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ic2.Z, ic2.a0, ic2.b0, kc2.A);
}
