package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public byte d;
    @DexIgnore
    public byte e;
    @DexIgnore
    public byte f;
    @DexIgnore
    public byte g;
    @DexIgnore
    public /* final */ byte h;
    @DexIgnore
    public byte i;
    @DexIgnore
    public /* final */ pg0 j; // = l51.b.a(this.c);
    @DexIgnore
    public /* final */ qg0 k; // = l51.b.b(this.c);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public rg0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                return new rg0(createByteArray);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public rg0[] newArray(int i) {
            return new rg0[i];
        }
    }

    @DexIgnore
    public rg0(byte[] bArr) {
        this.a = bArr;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.b = order.get(0);
        this.c = order.getShort(1);
        this.d = order.get(3);
        this.e = order.get(4);
        this.f = order.get(5);
        this.g = order.get(6);
        this.h = order.get(7);
        this.i = order.get(8);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D3, this.j.name()), r51.H3, this.k), r51.r3, Byte.valueOf(this.b)), r51.G3, Byte.valueOf(this.d)), r51.M3, Byte.valueOf(this.e)), r51.N3, Byte.valueOf(this.f)), r51.M2, Byte.valueOf(this.g)), r51.p, Byte.valueOf(this.h)), r51.t3, Byte.valueOf(this.i));
    }

    @DexIgnore
    public final byte[] b() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(rg0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.a, ((rg0) obj).a);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppEvent");
    }

    @DexIgnore
    public final pg0 getMicroAppId() {
        return this.j;
    }

    @DexIgnore
    public final byte getRequestId() {
        return this.h;
    }

    @DexIgnore
    public final qg0 getVariant() {
        return this.k;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.a);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
    }
}
