package com.fossil;

import android.os.CountDownTimer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs6 {
    @DexIgnore
    public CountDownTimer a; // = new a(this, 30000, 1000);
    @DexIgnore
    public xq6 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ bs6 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bs6 bs6, long j, long j2) {
            super(j, j2);
            this.a = bs6;
        }

        @DexIgnore
        public void onFinish() {
        }

        @DexIgnore
        public void onTick(long j) {
            xq6 a2 = this.a.b;
            if (a2 != null) {
                a2.n((int) (j / ((long) 1000)));
            }
        }
    }

    @DexIgnore
    public bs6(xq6 xq6) {
        this.b = xq6;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.a.cancel();
        if (z) {
            this.a.start();
        }
    }

    @DexIgnore
    public final void a() {
        this.a.cancel();
        this.b = null;
    }
}
