package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.cy6;
import com.fossil.vx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cj6 extends go5 implements bj6, View.OnClickListener, cy6.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public qw6<e25> f;
    @DexIgnore
    public aj6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return cj6.i;
        }

        @DexIgnore
        public final cj6 b() {
            return new cj6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public b(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public c(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            if (tm4.a.a().f()) {
                str = vx6.a(vx6.c.FAQ, null);
                ee7.a((Object) str, "URLHelper.buildStaticPag\u2026per.StaticPage.FAQ, null)");
            } else {
                str = this.a.Y("https://support.fossil.com/hc/%s/categories/360000064626-Smartwatch-FAQ");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cj6.j.a();
            local.d(a2, "FAQ URL = " + str);
            this.a.Z(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public d(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            if (tm4.a.a().f()) {
                str = vx6.a(vx6.c.REPAIR_CENTER, null);
                ee7.a((Object) str, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
            } else {
                we7 we7 = we7.a;
                Locale a2 = ig5.a();
                ee7.a((Object) a2, "LanguageHelper.getLocale()");
                str = String.format("https://c.fossil.com/web/service_centers", Arrays.copyOf(new Object[]{a2.getLanguage()}, 1));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            }
            FLogger.INSTANCE.getLocal().d(cj6.j.a(), "Repair Center URL = https://c.fossil.com/web/service_centers");
            this.a.Z(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public e(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (tm4.a.a().l()) {
                String a2 = vx6.a(vx6.c.REPAIR_CENTER, null);
                cj6 cj6 = this.a;
                ee7.a((Object) a2, "url");
                cj6.Z(a2);
                return;
            }
            this.a.f1().i();
            this.a.f1().a("Contact Us - From app [Fossil] - [Android]");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public f(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String Y = this.a.Y("https://support.fossil.com/hc/%s?wearablesChat=true");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cj6.j.a();
            local.d(a2, "Chat URL = " + Y);
            this.a.Z(Y);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public g(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            if (tm4.a.a().f()) {
                str = vx6.a(vx6.c.CALL, null);
                ee7.a((Object) str, "URLHelper.buildStaticPag\u2026er.StaticPage.CALL, null)");
            } else {
                we7 we7 = we7.a;
                Locale a2 = ig5.a();
                ee7.a((Object) a2, "LanguageHelper.getLocale()");
                str = String.format("https://c.fossil.com/web/call", Arrays.copyOf(new Object[]{a2.getLanguage()}, 1));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = cj6.j.a();
            local.d(a3, "Call Us URL = " + str);
            this.a.Z(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cj6 a;

        @DexIgnore
        public h(cj6 cj6, String str) {
            this.a = cj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                DeleteAccountActivity.a aVar = DeleteAccountActivity.z;
                FragmentActivity requireActivity = this.a.requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    /*
    static {
        String simpleName = cj6.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "HelpFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final String Y(String str) {
        ee7.b(str, "featureLink");
        Locale a2 = ig5.a();
        ee7.a((Object) a2, "LanguageHelper.getLocale()");
        String language = a2.getLanguage();
        Locale a3 = ig5.a();
        ee7.a((Object) a3, "LanguageHelper.getLocale()");
        String country = a3.getCountry();
        if (ee7.a((Object) language, (Object) "zh")) {
            if (ee7.a((Object) country, (Object) "tw")) {
                we7 we7 = we7.a;
                Locale locale = Locale.US;
                ee7.a((Object) locale, "Locale.US");
                language = String.format(locale, "%s-%s", Arrays.copyOf(new Object[]{language, "tw"}, 2));
                ee7.a((Object) language, "java.lang.String.format(locale, format, *args)");
            } else {
                we7 we72 = we7.a;
                Locale locale2 = Locale.US;
                ee7.a((Object) locale2, "Locale.US");
                language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, "cn"}, 2));
                ee7.a((Object) language, "java.lang.String.format(locale, format, *args)");
            }
        }
        we7 we73 = we7.a;
        String format = String.format(str, Arrays.copyOf(new Object[]{language}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final void Z(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), i);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final aj6 f1() {
        aj6 aj6 = this.g;
        if (aj6 != null) {
            return aj6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            aj6 aj6 = this.g;
            if (aj6 != null) {
                aj6.h();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ee7.b(view, "v");
        if (view.getId() == 2131361851) {
            v();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        e25 e25 = (e25) qb.a(LayoutInflater.from(getContext()), R.layout.fragment_help, null, false, a1());
        this.f = new qw6<>(this, e25);
        ee7.a((Object) e25, "binding");
        return e25.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        aj6 aj6 = this.g;
        if (aj6 != null) {
            aj6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        aj6 aj6 = this.g;
        if (aj6 != null) {
            aj6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        String g2 = PortfolioApp.g0.c().g();
        qw6<e25> qw6 = this.f;
        if (qw6 != null) {
            e25 a2 = qw6.a();
            if (a2 != null) {
                String b2 = eh5.l.a().b("nonBrandSeparatorLine");
                if (!TextUtils.isEmpty(b2)) {
                    int parseColor = Color.parseColor(b2);
                    a2.J.setBackgroundColor(parseColor);
                    a2.K.setBackgroundColor(parseColor);
                }
                FlexibleTextView flexibleTextView = a2.B;
                ee7.a((Object) flexibleTextView, "binding.tvAppVersion");
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131887051);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026w_Text__AppVersionNumber)");
                String format = String.format(a3, Arrays.copyOf(new Object[]{g2}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.setOnClickListener(new b(this, g2));
                a2.r.setOnClickListener(new c(this, g2));
                a2.s.setOnClickListener(new d(this, g2));
                a2.M.setOnClickListener(new e(this, g2));
                a2.N.setOnClickListener(new f(this, g2));
                a2.L.setOnClickListener(new g(this, g2));
                a2.E.setOnClickListener(new h(this, g2));
                if (!tm4.a.a().c()) {
                    FlexibleTextView flexibleTextView2 = a2.H;
                    ee7.a((Object) flexibleTextView2, "binding.tvLiveChat");
                    flexibleTextView2.setVisibility(8);
                    CustomizeWidget customizeWidget = a2.N;
                    ee7.a((Object) customizeWidget, "binding.wcLiveChat");
                    customizeWidget.setVisibility(8);
                }
                if (!tm4.a.a().a()) {
                    FlexibleTextView flexibleTextView3 = a2.D;
                    ee7.a((Object) flexibleTextView3, "binding.tvContacts");
                    flexibleTextView3.setVisibility(4);
                    ConstraintLayout constraintLayout = a2.u;
                    ee7.a((Object) constraintLayout, "binding.clContacts");
                    constraintLayout.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void a(aj6 aj6) {
        ee7.b(aj6, "presenter");
        jw3.a(aj6);
        ee7.a((Object) aj6, "Preconditions.checkNotNull(presenter)");
        this.g = aj6;
    }

    @DexIgnore
    @Override // com.fossil.bj6
    public void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        ee7.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = jn6.w.a();
        local.d(a2, "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof cl5)) {
            activity = null;
        }
        cl5 cl5 = (cl5) activity;
        if (cl5 != null) {
            cl5.a(str, i2, intent);
        }
    }
}
