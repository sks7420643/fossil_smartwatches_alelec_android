package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r87<A, B> implements Serializable {
    @DexIgnore
    public /* final */ A first;
    @DexIgnore
    public /* final */ B second;

    @DexIgnore
    public r87(A a, B b) {
        this.first = a;
        this.second = b;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.r87 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ r87 copy$default(r87 r87, Object obj, Object obj2, int i, Object obj3) {
        if ((i & 1) != 0) {
            obj = r87.first;
        }
        if ((i & 2) != 0) {
            obj2 = r87.second;
        }
        return r87.copy(obj, obj2);
    }

    @DexIgnore
    public final A component1() {
        return this.first;
    }

    @DexIgnore
    public final B component2() {
        return this.second;
    }

    @DexIgnore
    public final r87<A, B> copy(A a, B b) {
        return new r87<>(a, b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof r87)) {
            return false;
        }
        r87 r87 = (r87) obj;
        return ee7.a(this.first, r87.first) && ee7.a(this.second, r87.second);
    }

    @DexIgnore
    public final A getFirst() {
        return this.first;
    }

    @DexIgnore
    public final B getSecond() {
        return this.second;
    }

    @DexIgnore
    public int hashCode() {
        A a = this.first;
        int i = 0;
        int hashCode = (a != null ? a.hashCode() : 0) * 31;
        B b = this.second;
        if (b != null) {
            i = b.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return '(' + ((Object) this.first) + ", " + ((Object) this.second) + ')';
    }
}
