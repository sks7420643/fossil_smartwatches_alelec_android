package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw1 implements Factory<String> {
    @DexIgnore
    public static /* final */ vw1 a; // = new vw1();

    @DexIgnore
    public static vw1 a() {
        return a;
    }

    @DexIgnore
    public static String b() {
        String a2 = uw1.a();
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public String get() {
        return b();
    }
}
