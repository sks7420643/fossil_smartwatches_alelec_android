package com.fossil;

import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ou7 extends lu7<Fragment> {
    @DexIgnore
    public ou7(Fragment fragment) {
        super(fragment);
    }

    @DexIgnore
    @Override // com.fossil.nu7
    public boolean b(String str) {
        return ((Fragment) a()).shouldShowRequestPermissionRationale(str);
    }
}
