package com.fossil;

import com.fossil.fl4;
import com.fossil.s87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl4.e<P, E> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 a;
        @DexIgnore
        public /* final */ /* synthetic */ fl4 b;

        @DexIgnore
        public a(ai7 ai7, fl4 fl4, fl4.b bVar) {
            this.a = ai7;
            this.b = fl4;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(P p) {
            ee7.b(p, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "success continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                ai7 ai7 = this.a;
                s87.a aVar = s87.Companion;
                ai7.resumeWith(s87.m60constructorimpl(p));
            }
        }

        @DexIgnore
        public void a(E e) {
            ee7.b(e, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "fail continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                ai7 ai7 = this.a;
                s87.a aVar = s87.Companion;
                ai7.resumeWith(s87.m60constructorimpl(e));
            }
        }
    }

    @DexIgnore
    public static final <R extends fl4.b, P extends fl4.d, E extends fl4.a> Object a(fl4<? super R, P, E> fl4, R r, fb7<? super fl4.c> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        fl4.a(r, new a(bi7, fl4, r));
        Object g = bi7.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }
}
