package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo5 extends go5 implements hp6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public qw6<g15> f;
    @DexIgnore
    public gp6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lo5.i;
        }

        @DexIgnore
        public final lo5 b() {
            return new lo5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lo5 a;

        @DexIgnore
        public b(lo5 lo5) {
            this.a = lo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g15 a;
        @DexIgnore
        public /* final */ /* synthetic */ lo5 b;

        @DexIgnore
        public c(g15 g15, lo5 lo5) {
            this.a = g15;
            this.b = lo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            gp6 a2 = lo5.a(this.b);
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.t;
            ee7.a((Object) flexibleTextInputEditText, "binding.etEmail");
            a2.b(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g15 a;
        @DexIgnore
        public /* final */ /* synthetic */ lo5 b;

        @DexIgnore
        public d(g15 g15, lo5 lo5) {
            this.a = g15;
            this.b = lo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            gp6 a2 = lo5.a(this.b);
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.t;
            ee7.a((Object) flexibleTextInputEditText, "binding.etEmail");
            a2.b(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lo5 a;

        @DexIgnore
        public e(lo5 lo5) {
            this.a = lo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lo5 a;

        @DexIgnore
        public f(lo5 lo5) {
            this.a = lo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpActivity.a aVar = SignUpActivity.D;
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lo5 a;

        @DexIgnore
        public g(lo5 lo5) {
            this.a = lo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ lo5 a;

        @DexIgnore
        public h(lo5 lo5) {
            this.a = lo5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String str;
            gp6 a2 = lo5.a(this.a);
            if (charSequence == null || (str = charSequence.toString()) == null) {
                str = "";
            }
            a2.a(str);
        }
    }

    /*
    static {
        String simpleName = lo5.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "ForgotPasswordFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gp6 a(lo5 lo5) {
        gp6 gp6 = lo5.g;
        if (gp6 != null) {
            return gp6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void I() {
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                ee7.a((Object) flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.v;
                ee7.a((Object) flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.v;
                ee7.a((Object) flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(true);
                a2.v.a("flexible_button_primary");
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void K(String str) {
        ee7.b(str, "message");
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = a2.B;
                ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.B;
                ee7.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void M(boolean z) {
        FlexibleButton flexibleButton;
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null && (flexibleButton = a2.z) != null) {
                if (z) {
                    ee7.a((Object) flexibleButton, "it");
                    flexibleButton.setVisibility(0);
                    return;
                }
                ee7.a((Object) flexibleButton, "it");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void T(String str) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        ee7.b(str, Constants.EMAIL);
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null && (flexibleTextInputEditText = a2.t) != null) {
                flexibleTextInputEditText.setText(str);
                flexibleTextInputEditText.requestFocus();
                le5 le5 = le5.a;
                ee7.a((Object) flexibleTextInputEditText, "it");
                Context context = flexibleTextInputEditText.getContext();
                ee7.a((Object) context, "it.context");
                le5.b(flexibleTextInputEditText, context);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return i;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void f() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void f0() {
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                ee7.a((Object) flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.v;
                ee7.a((Object) flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.v;
                ee7.a((Object) flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(false);
                a2.v.a("flexible_button_disabled");
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void g() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<g15> qw6 = new qw6<>(this, (g15) qb.a(layoutInflater, 2131558555, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        gp6 gp6 = this.g;
        if (gp6 != null) {
            gp6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null) {
                a2.u.setOnClickListener(new b(this));
                a2.v.setOnClickListener(new c(a2, this));
                a2.x.setOnClickListener(new d(a2, this));
                a2.C.setOnClickListener(new e(this));
                a2.z.setOnClickListener(new f(this));
                a2.D.setOnClickListener(new g(this));
                a2.t.addTextChangedListener(new h(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void s0() {
        qw6<g15> qw6 = this.f;
        if (qw6 != null) {
            g15 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                ee7.a((Object) constraintLayout, "it.clResetPwSuccess");
                if (constraintLayout.getVisibility() == 0) {
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    bx6.O(childFragmentManager);
                    return;
                }
                ConstraintLayout constraintLayout2 = a2.q;
                ee7.a((Object) constraintLayout2, "it.clResetPw");
                constraintLayout2.setVisibility(8);
                ConstraintLayout constraintLayout3 = a2.r;
                ee7.a((Object) constraintLayout3, "it.clResetPwSuccess");
                constraintLayout3.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(gp6 gp6) {
        ee7.b(gp6, "presenter");
        this.g = gp6;
    }

    @DexIgnore
    @Override // com.fossil.hp6
    public void a(int i2, String str) {
        ee7.b(str, "errorMessage");
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.a(i2, str, childFragmentManager);
    }
}
