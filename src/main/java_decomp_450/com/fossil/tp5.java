package com.fossil;

import com.fossil.ng;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp5 extends ng.d<DailyHeartRateSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        ee7.b(dailyHeartRateSummary, "oldItem");
        ee7.b(dailyHeartRateSummary2, "newItem");
        return ee7.a(dailyHeartRateSummary, dailyHeartRateSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        ee7.b(dailyHeartRateSummary, "oldItem");
        ee7.b(dailyHeartRateSummary2, "newItem");
        return zd5.d(dailyHeartRateSummary.getDate(), dailyHeartRateSummary2.getDate());
    }
}
