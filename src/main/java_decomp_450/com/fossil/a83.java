package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a83 extends wm2 implements z73 {
    @DexIgnore
    public a83() {
        super("com.google.android.gms.maps.internal.IOnStreetViewPanoramaReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        a73 a73;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            a73 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaDelegate");
            if (queryLocalInterface instanceof a73) {
                a73 = (a73) queryLocalInterface;
            } else {
                a73 = new e83(readStrongBinder);
            }
        }
        a(a73);
        parcel2.writeNoException();
        return true;
    }
}
