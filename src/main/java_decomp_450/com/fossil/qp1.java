package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qp1 {
    HEARTBEAT_INTERVAL(new byte[]{(byte) 240});
    
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public qp1(byte[] bArr) {
        this.a = bArr;
    }
}
