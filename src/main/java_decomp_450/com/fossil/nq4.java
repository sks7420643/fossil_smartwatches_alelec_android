package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq4 extends he {
    @DexIgnore
    public MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<b> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Integer> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ fp4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public b(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && this.b == bVar.b;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            boolean z = this.a;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = i2 * 31;
            boolean z2 = this.b;
            if (z2 == 0) {
                i = z2;
            }
            return i4 + i;
        }

        @DexIgnore
        public String toString() {
            return "Tab(shouldGoToCreatedPage=" + this.a + ", shouldGoToTabs=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel", f = "BCMainViewModel.kt", l = {54}, m = "checkOnServer")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(nq4 nq4, fb7 fb7) {
            super(fb7);
            this.this$0 = nq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkOnServer$result$1", f = "BCMainViewModel.kt", l = {54}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super ko4<fo4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(nq4 nq4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = nq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<fo4>> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                fp4 a2 = this.this$0.d;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.a(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkSocialProfile$1", f = "BCMainViewModel.kt", l = {36, 41}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nq4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkSocialProfile$1$socialProfile$1", f = "BCMainViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super fo4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super fo4> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.d.b();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(nq4 nq4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = nq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                fo4 fo4 = (fo4) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            fo4 fo42 = (fo4) obj;
            if (fo42 != null) {
                nq4.a(this.this$0, true, false, 2, null);
            } else {
                nq4 nq4 = this.this$0;
                this.L$0 = yi7;
                this.L$1 = fo42;
                this.label = 2;
                if (nq4.a(this) == a2) {
                    return a2;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$retry$1", f = "BCMainViewModel.kt", l = {49}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(nq4 nq4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = nq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.a.a(pb7.a(true));
                nq4 nq4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (nq4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        ee7.a((Object) nq4.class.getName(), "BCMainViewModel::class.java.name");
    }
    */

    @DexIgnore
    public nq4(fp4 fp4) {
        ee7.b(fp4, "socialProfileRepository");
        this.d = fp4;
    }

    @DexIgnore
    public final LiveData<Integer> c() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<b> d() {
        return this.b;
    }

    @DexIgnore
    public final void e() {
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final void a() {
        this.a.a((Boolean) true);
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.nq4.c
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.nq4$c r0 = (com.fossil.nq4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.nq4$c r0 = new com.fossil.nq4$c
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.nq4 r0 = (com.fossil.nq4) r0
            com.fossil.t87.a(r7)
            goto L_0x004e
        L_0x002e:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0036:
            com.fossil.t87.a(r7)
            com.fossil.ti7 r7 = com.fossil.qj7.b()
            com.fossil.nq4$d r2 = new com.fossil.nq4$d
            r2.<init>(r6, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
            if (r7 != r1) goto L_0x004d
            return r1
        L_0x004d:
            r0 = r6
        L_0x004e:
            com.fossil.ko4 r7 = (com.fossil.ko4) r7
            java.lang.Object r1 = r7.c()
            r2 = 0
            if (r1 == 0) goto L_0x005c
            r7 = 2
            a(r0, r4, r2, r7, r3)
            goto L_0x008f
        L_0x005c:
            r1 = 404(0x194, float:5.66E-43)
            com.portfolio.platform.data.model.ServerError r5 = r7.a()
            if (r5 == 0) goto L_0x0069
            java.lang.Integer r5 = r5.getCode()
            goto L_0x006a
        L_0x0069:
            r5 = r3
        L_0x006a:
            if (r5 != 0) goto L_0x006d
            goto L_0x0077
        L_0x006d:
            int r5 = r5.intValue()
            if (r1 != r5) goto L_0x0077
            a(r0, r2, r4, r4, r3)
            goto L_0x008f
        L_0x0077:
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r0.a
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r1.a(r2)
            androidx.lifecycle.MutableLiveData<java.lang.Integer> r0 = r0.c
            com.portfolio.platform.data.model.ServerError r7 = r7.a()
            if (r7 == 0) goto L_0x008c
            java.lang.Integer r3 = r7.getCode()
        L_0x008c:
            r0.a(r3)
        L_0x008f:
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nq4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ void a(nq4 nq4, boolean z, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        if ((i & 2) != 0) {
            z2 = false;
        }
        nq4.a(z, z2);
    }

    @DexIgnore
    public final void a(boolean z, boolean z2) {
        this.b.a(new b(z2, z));
    }
}
