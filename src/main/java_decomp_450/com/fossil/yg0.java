package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yg0 extends k60 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public gk1 a;
    @DexIgnore
    public do1[] b;
    @DexIgnore
    public do1[] c;
    @DexIgnore
    public do1[] d;
    @DexIgnore
    public do1[] e;
    @DexIgnore
    public do1[] f;
    @DexIgnore
    public do1[] g;
    @DexIgnore
    public long h;
    @DexIgnore
    public /* final */ r60 i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ byte[] k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final do1[] a(byte[] bArr) {
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < bArr.length) {
                short b = yz0.b(bArr[i]);
                int i2 = i + b;
                int i3 = i2 + 1;
                int i4 = i3 + 2;
                ByteBuffer order = ByteBuffer.wrap(s97.a(bArr, i3, i4)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order, "ByteBuffer.wrap(\n       \u2026(ByteOrder.LITTLE_ENDIAN)");
                int b2 = yz0.b(order.getShort());
                arrayList.add(new do1(new String(s97.a(bArr, i + 1, i2), b21.x.c()), s97.a(bArr, i4, i4 + b2)));
                i += b + 1 + 2 + b2;
            }
            Object[] array = arrayList.toArray(new do1[0]);
            if (array != null) {
                return (do1[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public yg0(byte[] bArr) {
        do1 do1;
        this.k = bArr;
        try {
            gk1 a2 = gk1.CREATOR.a(s97.a(bArr, 12, 24));
            if (a2 != null) {
                ByteBuffer order = ByteBuffer.wrap(s97.a(this.k, 24, 28)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order, "ByteBuffer.wrap(rawData.\u2026(ByteOrder.LITTLE_ENDIAN)");
                int i2 = order.getInt();
                ByteBuffer order2 = ByteBuffer.wrap(s97.a(this.k, 28, 32)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order2, "ByteBuffer.wrap(rawData.\u2026(ByteOrder.LITTLE_ENDIAN)");
                int i3 = order2.getInt();
                ByteBuffer order3 = ByteBuffer.wrap(s97.a(this.k, 32, 36)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order3, "ByteBuffer.wrap(rawData.\u2026(ByteOrder.LITTLE_ENDIAN)");
                int i4 = order3.getInt();
                ByteBuffer order4 = ByteBuffer.wrap(s97.a(this.k, 36, 40)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order4, "ByteBuffer.wrap(rawData.\u2026(ByteOrder.LITTLE_ENDIAN)");
                int i5 = order4.getInt();
                ByteBuffer order5 = ByteBuffer.wrap(s97.a(this.k, 40, 44)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order5, "ByteBuffer.wrap(rawData.\u2026(ByteOrder.LITTLE_ENDIAN)");
                int i6 = order5.getInt();
                ByteBuffer order6 = ByteBuffer.wrap(s97.a(this.k, 44, 48)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order6, "ByteBuffer.wrap(rawData.\u2026(ByteOrder.LITTLE_ENDIAN)");
                int i7 = order6.getInt();
                this.a = a2;
                this.b = l.a(s97.a(this.k, i2, i3));
                this.c = l.a(s97.a(this.k, i3, i4));
                this.d = l.a(s97.a(this.k, i4, i5));
                this.e = l.a(s97.a(this.k, i5, i6));
                this.f = l.a(s97.a(this.k, i6, i7));
                this.g = l.a(s97.a(this.k, i7, this.k.length - 4));
                this.h = yz0.b(ByteBuffer.wrap(s97.a(this.k, this.k.length - 4, this.k.length)).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                gk1 gk1 = this.a;
                String str = null;
                if (gk1 != null) {
                    this.i = gk1.b;
                    do1[] do1Arr = this.f;
                    if (do1Arr != null) {
                        int length = do1Arr.length;
                        int i8 = 0;
                        while (true) {
                            if (i8 >= length) {
                                do1 = null;
                                break;
                            }
                            do1 = do1Arr[i8];
                            if (ee7.a((Object) do1.a, (Object) "!display_name")) {
                                break;
                            }
                            i8++;
                        }
                        if (do1 != null) {
                            if (!(do1.b.length == 0)) {
                                byte[] bArr2 = do1.b;
                                str = new String(s97.a(bArr2, 0, bArr2.length - 1), b21.x.c());
                            }
                        }
                        this.j = str;
                        return;
                    }
                    ee7.d("info");
                    throw null;
                }
                ee7.d("packageInfo");
                throw null;
            }
            throw new IllegalArgumentException("Invalid Data.");
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        r51 r51 = r51.Q4;
        gk1 gk1 = this.a;
        if (gk1 != null) {
            JSONObject a2 = yz0.a(jSONObject, r51, gk1.a());
            r51 r512 = r51.R4;
            do1[] do1Arr = this.b;
            if (do1Arr != null) {
                JSONObject a3 = yz0.a(a2, r512, yz0.a(do1Arr));
                r51 r513 = r51.S4;
                do1[] do1Arr2 = this.c;
                if (do1Arr2 != null) {
                    JSONObject a4 = yz0.a(a3, r513, yz0.a(do1Arr2));
                    r51 r514 = r51.T4;
                    do1[] do1Arr3 = this.d;
                    if (do1Arr3 != null) {
                        JSONObject a5 = yz0.a(a4, r514, yz0.a(do1Arr3));
                        r51 r515 = r51.U4;
                        do1[] do1Arr4 = this.e;
                        if (do1Arr4 != null) {
                            JSONObject a6 = yz0.a(a5, r515, yz0.a(do1Arr4));
                            r51 r516 = r51.V4;
                            do1[] do1Arr5 = this.b;
                            if (do1Arr5 != null) {
                                JSONObject a7 = yz0.a(a6, r516, yz0.a(do1Arr5));
                                r51 r517 = r51.W4;
                                do1[] do1Arr6 = this.b;
                                if (do1Arr6 != null) {
                                    return yz0.a(a7, r517, yz0.a(do1Arr6));
                                }
                                ee7.d("nodes");
                                throw null;
                            }
                            ee7.d("nodes");
                            throw null;
                        }
                        ee7.d("locales");
                        throw null;
                    }
                    ee7.d("layouts");
                    throw null;
                }
                ee7.d("images");
                throw null;
            }
            ee7.d("nodes");
            throw null;
        }
        ee7.d("packageInfo");
        throw null;
    }

    @DexIgnore
    public final do1[] b() {
        do1[] do1Arr = this.c;
        if (do1Arr != null) {
            return do1Arr;
        }
        ee7.d("images");
        throw null;
    }

    @DexIgnore
    public final do1[] c() {
        do1[] do1Arr = this.b;
        if (do1Arr != null) {
            return do1Arr;
        }
        ee7.d("nodes");
        throw null;
    }

    @DexIgnore
    public final long d() {
        return this.h;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final gk1 e() {
        gk1 gk1 = this.a;
        if (gk1 != null) {
            return gk1;
        }
        ee7.d("packageInfo");
        throw null;
    }

    @DexIgnore
    public final byte[] f() {
        return this.k;
    }

    @DexIgnore
    public final String getDisplayName() {
        return this.j;
    }

    @DexIgnore
    public final r60 getVersion() {
        return this.i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        gk1 gk1 = this.a;
        if (gk1 != null) {
            parcel.writeParcelable(gk1, i2);
            do1[] do1Arr = this.b;
            if (do1Arr != null) {
                parcel.writeTypedArray(do1Arr, i2);
                do1[] do1Arr2 = this.c;
                if (do1Arr2 != null) {
                    parcel.writeTypedArray(do1Arr2, i2);
                    do1[] do1Arr3 = this.d;
                    if (do1Arr3 != null) {
                        parcel.writeTypedArray(do1Arr3, i2);
                        do1[] do1Arr4 = this.e;
                        if (do1Arr4 != null) {
                            parcel.writeTypedArray(do1Arr4, i2);
                            do1[] do1Arr5 = this.f;
                            if (do1Arr5 != null) {
                                parcel.writeTypedArray(do1Arr5, i2);
                                do1[] do1Arr6 = this.g;
                                if (do1Arr6 != null) {
                                    parcel.writeTypedArray(do1Arr6, i2);
                                } else {
                                    ee7.d("config");
                                    throw null;
                                }
                            } else {
                                ee7.d("info");
                                throw null;
                            }
                        } else {
                            ee7.d("locales");
                            throw null;
                        }
                    } else {
                        ee7.d("layouts");
                        throw null;
                    }
                } else {
                    ee7.d("images");
                    throw null;
                }
            } else {
                ee7.d("nodes");
                throw null;
            }
        } else {
            ee7.d("packageInfo");
            throw null;
        }
    }
}
