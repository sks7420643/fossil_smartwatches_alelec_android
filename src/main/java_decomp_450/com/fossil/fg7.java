package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fg7<T> implements hg7<T> {
    @DexIgnore
    public /* final */ hg7<T> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ gd7<T, Boolean> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, ye7 {
        @DexIgnore
        public /* final */ Iterator<T> a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public T c;
        @DexIgnore
        public /* final */ /* synthetic */ fg7 d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(fg7 fg7) {
            this.d = fg7;
            this.a = fg7.a.iterator();
        }

        @DexIgnore
        public final void a() {
            while (this.a.hasNext()) {
                T next = this.a.next();
                if (((Boolean) this.d.c.invoke(next)).booleanValue() == this.d.b) {
                    this.c = next;
                    this.b = 1;
                    return;
                }
            }
            this.b = 0;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.b == -1) {
                a();
            }
            return this.b == 1;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (this.b == -1) {
                a();
            }
            if (this.b != 0) {
                T t = this.c;
                this.c = null;
                this.b = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.hg7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.gd7<? super T, java.lang.Boolean> */
    /* JADX WARN: Multi-variable type inference failed */
    public fg7(hg7<? extends T> hg7, boolean z, gd7<? super T, Boolean> gd7) {
        ee7.b(hg7, "sequence");
        ee7.b(gd7, "predicate");
        this.a = hg7;
        this.b = z;
        this.c = gd7;
    }

    @DexIgnore
    @Override // com.fossil.hg7
    public Iterator<T> iterator() {
        return new a(this);
    }
}
