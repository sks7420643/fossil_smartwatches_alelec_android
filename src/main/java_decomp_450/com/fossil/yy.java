package com.fossil;

import android.util.Log;
import com.fossil.ey;
import com.fossil.ix;
import com.fossil.m00;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yy implements ey, ey.a {
    @DexIgnore
    public /* final */ fy<?> a;
    @DexIgnore
    public /* final */ ey.a b;
    @DexIgnore
    public int c;
    @DexIgnore
    public by d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public volatile m00.a<?> f;
    @DexIgnore
    public cy g;

    @DexIgnore
    public yy(fy<?> fyVar, ey.a aVar) {
        this.a = fyVar;
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ey
    public boolean a() {
        Object obj = this.e;
        if (obj != null) {
            this.e = null;
            a(obj);
        }
        by byVar = this.d;
        if (byVar != null && byVar.a()) {
            return true;
        }
        this.d = null;
        this.f = null;
        boolean z = false;
        while (!z && c()) {
            List<m00.a<?>> g2 = this.a.g();
            int i = this.c;
            this.c = i + 1;
            this.f = g2.get(i);
            if (this.f != null && (this.a.e().a(this.f.c.b()) || this.a.c(this.f.c.getDataClass()))) {
                b(this.f);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public final void b(m00.a<?> aVar) {
        this.f.c.a(this.a.j(), new a(aVar));
    }

    @DexIgnore
    public final boolean c() {
        return this.c < this.a.g().size();
    }

    @DexIgnore
    @Override // com.fossil.ey
    public void cancel() {
        m00.a<?> aVar = this.f;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ix.a<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ m00.a a;

        @DexIgnore
        public a(m00.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.ix.a
        public void a(Object obj) {
            if (yy.this.a(this.a)) {
                yy.this.a(this.a, obj);
            }
        }

        @DexIgnore
        @Override // com.fossil.ix.a
        public void a(Exception exc) {
            if (yy.this.a(this.a)) {
                yy.this.a(this.a, exc);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ey.a
    public void b() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean a(m00.a<?> aVar) {
        m00.a<?> aVar2 = this.f;
        return aVar2 != null && aVar2 == aVar;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(Object obj) {
        long a2 = q50.a();
        try {
            vw<X> a3 = this.a.a(obj);
            dy dyVar = new dy(a3, obj, this.a.i());
            this.g = new cy(this.f.a, this.a.l());
            this.a.d().a(this.g, dyVar);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.g + ", data: " + obj + ", encoder: " + a3 + ", duration: " + q50.a(a2));
            }
            this.f.c.a();
            this.d = new by(Collections.singletonList(this.f.a), this.a, this);
        } catch (Throwable th) {
            this.f.c.a();
            throw th;
        }
    }

    @DexIgnore
    public void a(m00.a<?> aVar, Object obj) {
        iy e2 = this.a.e();
        if (obj == null || !e2.a(aVar.c.b())) {
            ey.a aVar2 = this.b;
            yw ywVar = aVar.a;
            ix<Data> ixVar = aVar.c;
            aVar2.a(ywVar, obj, ixVar, ixVar.b(), this.g);
            return;
        }
        this.e = obj;
        this.b.b();
    }

    @DexIgnore
    public void a(m00.a<?> aVar, Exception exc) {
        ey.a aVar2 = this.b;
        cy cyVar = this.g;
        ix<Data> ixVar = aVar.c;
        aVar2.a(cyVar, exc, ixVar, ixVar.b());
    }

    @DexIgnore
    @Override // com.fossil.ey.a
    public void a(yw ywVar, Object obj, ix<?> ixVar, sw swVar, yw ywVar2) {
        this.b.a(ywVar, obj, ixVar, this.f.c.b(), ywVar);
    }

    @DexIgnore
    @Override // com.fossil.ey.a
    public void a(yw ywVar, Exception exc, ix<?> ixVar, sw swVar) {
        this.b.a(ywVar, exc, ixVar, this.f.c.b());
    }
}
