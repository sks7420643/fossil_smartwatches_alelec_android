package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur0 extends fe7 implements vc7<i97> {
    @DexIgnore
    public /* final */ /* synthetic */ kv0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ur0(kv0 kv0) {
        super(0);
        this.a = kv0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.vc7
    public i97 invoke() {
        kv0 kv0 = this.a;
        eo0 poll = kv0.a.poll();
        if (poll != null) {
            kv0.b(poll);
        } else {
            kv0.b = null;
        }
        return i97.a;
    }
}
