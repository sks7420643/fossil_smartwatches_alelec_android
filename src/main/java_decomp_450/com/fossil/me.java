package com.fossil;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class me<D> extends oe<D> {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ String TAG; // = "AsyncTaskLoader";
    @DexIgnore
    public volatile me<D>.a mCancellingTask;
    @DexIgnore
    public /* final */ Executor mExecutor;
    @DexIgnore
    public Handler mHandler;
    @DexIgnore
    public long mLastLoadCompleteTime;
    @DexIgnore
    public volatile me<D>.a mTask;
    @DexIgnore
    public long mUpdateThrottle;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends pe<Void, Void, D> implements Runnable {
        @DexIgnore
        public /* final */ CountDownLatch j; // = new CountDownLatch(1);
        @DexIgnore
        public boolean p;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.pe
        public void b(D d) {
            try {
                me.this.dispatchOnCancelled(this, d);
            } finally {
                this.j.countDown();
            }
        }

        @DexIgnore
        @Override // com.fossil.pe
        public void c(D d) {
            try {
                me.this.dispatchOnLoadComplete(this, d);
            } finally {
                this.j.countDown();
            }
        }

        @DexIgnore
        public void e() {
            try {
                this.j.await();
            } catch (InterruptedException unused) {
            }
        }

        @DexIgnore
        public void run() {
            this.p = false;
            me.this.executePendingTask();
        }

        @DexIgnore
        public D a(Void... voidArr) {
            try {
                return (D) me.this.onLoadInBackground();
            } catch (k8 e) {
                if (a()) {
                    return null;
                }
                throw e;
            }
        }
    }

    @DexIgnore
    public me(Context context) {
        this(context, pe.h);
    }

    @DexIgnore
    public void cancelLoadInBackground() {
    }

    @DexIgnore
    public void dispatchOnCancelled(me<D>.a aVar, D d) {
        onCanceled(d);
        if (this.mCancellingTask == aVar) {
            rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            deliverCancellation();
            executePendingTask();
        }
    }

    @DexIgnore
    public void dispatchOnLoadComplete(me<D>.a aVar, D d) {
        if (this.mTask != aVar) {
            dispatchOnCancelled(aVar, d);
        } else if (isAbandoned()) {
            onCanceled(d);
        } else {
            commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            deliverResult(d);
        }
    }

    @DexIgnore
    @Override // com.fossil.oe
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        if (this.mTask != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.p);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.p);
        }
        if (this.mUpdateThrottle != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            f9.a(this.mUpdateThrottle, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            f9.a(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }

    @DexIgnore
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.p) {
                this.mTask.p = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if (this.mUpdateThrottle <= 0 || SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.a(this.mExecutor, null);
                return;
            }
            this.mTask.p = true;
            this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
    }

    @DexIgnore
    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }

    @DexIgnore
    public abstract D loadInBackground();

    @DexIgnore
    @Override // com.fossil.oe
    public boolean onCancelLoad() {
        if (this.mTask == null) {
            return false;
        }
        if (!((oe) this).mStarted) {
            ((oe) this).mContentChanged = true;
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.p) {
                this.mTask.p = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            this.mTask = null;
            return false;
        } else if (this.mTask.p) {
            this.mTask.p = false;
            this.mHandler.removeCallbacks(this.mTask);
            this.mTask = null;
            return false;
        } else {
            boolean a2 = this.mTask.a(false);
            if (a2) {
                this.mCancellingTask = this.mTask;
                cancelLoadInBackground();
            }
            this.mTask = null;
            return a2;
        }
    }

    @DexIgnore
    public void onCanceled(D d) {
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onForceLoad() {
        super.onForceLoad();
        cancelLoad();
        this.mTask = new a();
        executePendingTask();
    }

    @DexIgnore
    public D onLoadInBackground() {
        return loadInBackground();
    }

    @DexIgnore
    public void setUpdateThrottle(long j) {
        this.mUpdateThrottle = j;
        if (j != 0) {
            this.mHandler = new Handler();
        }
    }

    @DexIgnore
    public void waitForLoader() {
        me<D>.a aVar = this.mTask;
        if (aVar != null) {
            aVar.e();
        }
    }

    @DexIgnore
    public me(Context context, Executor executor) {
        super(context);
        this.mLastLoadCompleteTime = -10000;
        this.mExecutor = executor;
    }
}
