package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ a47 c;

    @DexIgnore
    public o47(Context context, String str, a47 a47) {
        this.a = context;
        this.b = str;
        this.c = a47;
    }

    @DexIgnore
    public final void run() {
        Long l;
        try {
            z37.f(this.a);
            synchronized (z37.k) {
                l = (Long) z37.k.remove(this.b);
            }
            if (l != null) {
                Long valueOf = Long.valueOf((System.currentTimeMillis() - l.longValue()) / 1000);
                if (valueOf.longValue() <= 0) {
                    valueOf = 1L;
                }
                String j = z37.j;
                if (j != null && j.equals(this.b)) {
                    j = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                i47 i47 = new i47(this.a, j, this.b, z37.a(this.a, false, this.c), valueOf, this.c);
                if (!this.b.equals(z37.i)) {
                    z37.m.h("Invalid invocation since previous onResume on diff page.");
                }
                new t47(i47).a();
                String unused = z37.j = this.b;
                return;
            }
            k57 f = z37.m;
            f.c("Starttime for PageID:" + this.b + " not found, lost onResume()?");
        } catch (Throwable th) {
            z37.m.a(th);
            z37.a(this.a, th);
        }
    }
}
