package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d30 implements f30<Drawable, byte[]> {
    @DexIgnore
    public /* final */ dz a;
    @DexIgnore
    public /* final */ f30<Bitmap, byte[]> b;
    @DexIgnore
    public /* final */ f30<t20, byte[]> c;

    @DexIgnore
    public d30(dz dzVar, f30<Bitmap, byte[]> f30, f30<t20, byte[]> f302) {
        this.a = dzVar;
        this.b = f30;
        this.c = f302;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.uy<android.graphics.drawable.Drawable> */
    /* JADX WARN: Multi-variable type inference failed */
    public static uy<t20> a(uy<Drawable> uyVar) {
        return uyVar;
    }

    @DexIgnore
    @Override // com.fossil.f30
    public uy<byte[]> a(uy<Drawable> uyVar, ax axVar) {
        Drawable drawable = uyVar.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(k10.a(((BitmapDrawable) drawable).getBitmap(), this.a), axVar);
        }
        if (!(drawable instanceof t20)) {
            return null;
        }
        f30<t20, byte[]> f30 = this.c;
        a(uyVar);
        return f30.a(uyVar, axVar);
    }
}
