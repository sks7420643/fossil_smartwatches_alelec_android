package com.fossil;

import com.fossil.ax7;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx7 {
    @DexIgnore
    public static /* final */ dx7 a; // = new dx7();

    @DexIgnore
    public final Map<String, Object> a(List<zw7> list) {
        ee7.b(list, "list");
        ArrayList arrayList = new ArrayList();
        for (zw7 zw7 : list) {
            long j = (long) 1000;
            arrayList.add(oa7.c(w87.a("id", zw7.e()), w87.a("duration", Long.valueOf(zw7.c() / j)), w87.a("type", Integer.valueOf(zw7.j())), w87.a("createDt", Long.valueOf(zw7.a() / j)), w87.a("width", Integer.valueOf(zw7.k())), w87.a("height", Integer.valueOf(zw7.d())), w87.a("modifiedDt", Long.valueOf(zw7.h())), w87.a(Constants.LAT, zw7.f()), w87.a("lng", zw7.g()), w87.a("title", zw7.b())));
        }
        return na7.a(w87.a("data", arrayList));
    }

    @DexIgnore
    public final Map<String, Object> b(List<bx7> list) {
        ee7.b(list, "list");
        ArrayList arrayList = new ArrayList();
        for (bx7 bx7 : list) {
            Map c = oa7.c(w87.a("id", bx7.a()), w87.a("name", bx7.c()), w87.a("length", Integer.valueOf(bx7.b())), w87.a("isAll", Boolean.valueOf(bx7.d())));
            if (bx7.b() > 0) {
                arrayList.add(c);
            }
        }
        return na7.a(w87.a("data", arrayList));
    }

    @DexIgnore
    public final Map<String, Object> a(zw7 zw7) {
        ee7.b(zw7, "entity");
        return na7.a(w87.a("data", oa7.c(w87.a("id", zw7.e()), w87.a("duration", Long.valueOf(zw7.c())), w87.a("type", Integer.valueOf(zw7.j())), w87.a("createDt", Long.valueOf(zw7.a() / ((long) 1000))), w87.a("width", Integer.valueOf(zw7.k())), w87.a("height", Integer.valueOf(zw7.d())), w87.a("modifiedDt", Long.valueOf(zw7.h())), w87.a(Constants.LAT, zw7.f()), w87.a("lng", zw7.g()), w87.a("title", zw7.b()))));
    }

    @DexIgnore
    public final ax7 a(Map<?, ?> map) {
        ee7.b(map, Constants.MAP);
        ax7 ax7 = new ax7();
        Object obj = map.get("title");
        if (obj != null) {
            ax7.a(((Boolean) obj).booleanValue());
            ax7.c cVar = new ax7.c();
            ax7.a(cVar);
            Object obj2 = map.get("size");
            if (obj2 != null) {
                Map map2 = (Map) obj2;
                Object obj3 = map2.get("minWidth");
                if (obj3 != null) {
                    cVar.d(((Integer) obj3).intValue());
                    Object obj4 = map2.get("maxWidth");
                    if (obj4 != null) {
                        cVar.b(((Integer) obj4).intValue());
                        Object obj5 = map2.get("minHeight");
                        if (obj5 != null) {
                            cVar.c(((Integer) obj5).intValue());
                            Object obj6 = map2.get("maxHeight");
                            if (obj6 != null) {
                                cVar.a(((Integer) obj6).intValue());
                                ax7.b bVar = new ax7.b();
                                ax7.a(bVar);
                                Object obj7 = map.get("duration");
                                if (obj7 != null) {
                                    Map map3 = (Map) obj7;
                                    Object obj8 = map3.get("min");
                                    if (obj8 != null) {
                                        bVar.b((long) ((Integer) obj8).intValue());
                                        Object obj9 = map3.get("max");
                                        if (obj9 != null) {
                                            bVar.a((long) ((Integer) obj9).intValue());
                                            return ax7;
                                        }
                                        throw new x87("null cannot be cast to non-null type kotlin.Int");
                                    }
                                    throw new x87("null cannot be cast to non-null type kotlin.Int");
                                }
                                throw new x87("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                            }
                            throw new x87("null cannot be cast to non-null type kotlin.Int");
                        }
                        throw new x87("null cannot be cast to non-null type kotlin.Int");
                    }
                    throw new x87("null cannot be cast to non-null type kotlin.Int");
                }
                throw new x87("null cannot be cast to non-null type kotlin.Int");
            }
            throw new x87("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Boolean");
    }
}
