package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp2 extends bw2<lp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ lp2 zzh;
    @DexIgnore
    public static volatile wx2<lp2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public int zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<lp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(lp2.zzh);
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((lp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public final boolean o() {
            return ((lp2) ((bw2.a) this).b).p();
        }

        @DexIgnore
        public final boolean p() {
            return ((lp2) ((bw2.a) this).b).q();
        }

        @DexIgnore
        public final boolean q() {
            return ((lp2) ((bw2.a) this).b).r();
        }

        @DexIgnore
        public final String zza() {
            return ((lp2) ((bw2.a) this).b).zza();
        }

        @DexIgnore
        public final int zze() {
            return ((lp2) ((bw2.a) this).b).s();
        }

        @DexIgnore
        public /* synthetic */ a(jp2 jp2) {
            this();
        }
    }

    /*
    static {
        lp2 lp2 = new lp2();
        zzh = lp2;
        bw2.a(lp2.class, lp2);
    }
    */

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    @DexIgnore
    public final boolean p() {
        return this.zze;
    }

    @DexIgnore
    public final boolean q() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int s() {
        return this.zzg;
    }

    @DexIgnore
    public final String zza() {
        return this.zzd;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (jp2.a[i - 1]) {
            case 1:
                return new lp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1007\u0001\u0003\u1007\u0002\u0004\u1004\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                wx2<lp2> wx2 = zzi;
                if (wx2 == null) {
                    synchronized (lp2.class) {
                        wx2 = zzi;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzh);
                            zzi = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
