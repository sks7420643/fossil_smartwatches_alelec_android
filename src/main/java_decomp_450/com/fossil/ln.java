package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ln implements vm {
    @DexIgnore
    public static /* final */ String p; // = im.a("SystemAlarmDispatcher");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ vp b;
    @DexIgnore
    public /* final */ rp c;
    @DexIgnore
    public /* final */ xm d;
    @DexIgnore
    public /* final */ dn e;
    @DexIgnore
    public /* final */ in f;
    @DexIgnore
    public /* final */ Handler g;
    @DexIgnore
    public /* final */ List<Intent> h;
    @DexIgnore
    public Intent i;
    @DexIgnore
    public c j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            d dVar;
            ln lnVar;
            synchronized (ln.this.h) {
                ln.this.i = ln.this.h.get(0);
            }
            Intent intent = ln.this.i;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = ln.this.i.getIntExtra("KEY_START_ID", 0);
                im.a().a(ln.p, String.format("Processing command %s, %s", ln.this.i, Integer.valueOf(intExtra)), new Throwable[0]);
                PowerManager.WakeLock a2 = op.a(ln.this.a, String.format("%s (%s)", action, Integer.valueOf(intExtra)));
                try {
                    im.a().a(ln.p, String.format("Acquiring operation wake lock (%s) %s", action, a2), new Throwable[0]);
                    a2.acquire();
                    ln.this.f.e(ln.this.i, intExtra, ln.this);
                    im.a().a(ln.p, String.format("Releasing operation wake lock (%s) %s", action, a2), new Throwable[0]);
                    a2.release();
                    lnVar = ln.this;
                    dVar = new d(lnVar);
                } catch (Throwable th) {
                    im.a().a(ln.p, String.format("Releasing operation wake lock (%s) %s", action, a2), new Throwable[0]);
                    a2.release();
                    ln lnVar2 = ln.this;
                    lnVar2.a(new d(lnVar2));
                    throw th;
                }
                lnVar.a(dVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ ln a;
        @DexIgnore
        public /* final */ Intent b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(ln lnVar, Intent intent, int i) {
            this.a = lnVar;
            this.b = intent;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            this.a.a(this.b, this.c);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ ln a;

        @DexIgnore
        public d(ln lnVar) {
            this.a = lnVar;
        }

        @DexIgnore
        public void run() {
            this.a.b();
        }
    }

    @DexIgnore
    public ln(Context context) {
        this(context, null, null);
    }

    @DexIgnore
    @Override // com.fossil.vm
    public void a(String str, boolean z) {
        a(new b(this, in.a(this.a, str, z), 0));
    }

    @DexIgnore
    public void b() {
        im.a().a(p, "Checking if commands are complete.", new Throwable[0]);
        a();
        synchronized (this.h) {
            if (this.i != null) {
                im.a().a(p, String.format("Removing command %s", this.i), new Throwable[0]);
                if (this.h.remove(0).equals(this.i)) {
                    this.i = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            lp b2 = this.b.b();
            if (!this.f.a() && this.h.isEmpty() && !b2.a()) {
                im.a().a(p, "No more commands & intents.", new Throwable[0]);
                if (this.j != null) {
                    this.j.a();
                }
            } else if (!this.h.isEmpty()) {
                h();
            }
        }
    }

    @DexIgnore
    public xm c() {
        return this.d;
    }

    @DexIgnore
    public vp d() {
        return this.b;
    }

    @DexIgnore
    public dn e() {
        return this.e;
    }

    @DexIgnore
    public rp f() {
        return this.c;
    }

    @DexIgnore
    public void g() {
        im.a().a(p, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.d.b(this);
        this.c.a();
        this.j = null;
    }

    @DexIgnore
    public final void h() {
        a();
        PowerManager.WakeLock a2 = op.a(this.a, "ProcessCommand");
        try {
            a2.acquire();
            this.e.g().a(new a());
        } finally {
            a2.release();
        }
    }

    @DexIgnore
    public ln(Context context, xm xmVar, dn dnVar) {
        this.a = context.getApplicationContext();
        this.f = new in(this.a);
        this.c = new rp();
        dnVar = dnVar == null ? dn.a(context) : dnVar;
        this.e = dnVar;
        this.d = xmVar == null ? dnVar.d() : xmVar;
        this.b = this.e.g();
        this.d.a(this);
        this.h = new ArrayList();
        this.i = null;
        this.g = new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public boolean a(Intent intent, int i2) {
        boolean z = false;
        im.a().a(p, String.format("Adding command %s (%s)", intent, Integer.valueOf(i2)), new Throwable[0]);
        a();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            im.a().e(p, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && a("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i2);
            synchronized (this.h) {
                if (!this.h.isEmpty()) {
                    z = true;
                }
                this.h.add(intent);
                if (!z) {
                    h();
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void a(c cVar) {
        if (this.j != null) {
            im.a().b(p, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.j = cVar;
        }
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.g.post(runnable);
    }

    @DexIgnore
    public final boolean a(String str) {
        a();
        synchronized (this.h) {
            for (Intent intent : this.h) {
                if (str.equals(intent.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public final void a() {
        if (this.g.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }
}
