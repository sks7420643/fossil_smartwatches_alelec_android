package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ no3 a;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 b;

    @DexIgnore
    public gp3(fp3 fp3, no3 no3) {
        this.b = fp3;
        this.a = no3;
    }

    @DexIgnore
    public final void run() {
        try {
            no3 then = this.b.b.then(this.a.b());
            if (then == null) {
                this.b.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            then.a(po3.b, (jo3) this.b);
            then.a(po3.b, (io3) this.b);
            then.a(po3.b, (go3) this.b);
        } catch (lo3 e) {
            if (e.getCause() instanceof Exception) {
                this.b.onFailure((Exception) e.getCause());
            } else {
                this.b.onFailure(e);
            }
        } catch (CancellationException unused) {
            this.b.onCanceled();
        } catch (Exception e2) {
            this.b.onFailure(e2);
        }
    }
}
