package com.fossil;

import com.fossil.vx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iy3<E> extends vx3<E> implements Set<E> {
    @DexIgnore
    public static /* final */ int MAX_TABLE_SIZE; // = 1073741824;
    @DexIgnore
    @LazyInit
    public transient zx3<E> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<E> extends vx3.a<E> {
        @DexIgnore
        public a() {
            this(4);
        }

        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.vx3.a, com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(E e) {
            super.a((Object) e);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vx3.a, com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(E... eArr) {
            super.a((Object[]) eArr);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(Iterator<? extends E> it) {
            super.a((Iterator) it);
            return this;
        }

        @DexIgnore
        public iy3<E> a() {
            iy3<E> access$000 = iy3.a(((vx3.a) this).b, ((vx3.a) this).a);
            ((vx3.a) this).b = access$000.size();
            return access$000;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> extends iy3<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends tx3<E> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // java.util.List
            public E get(int i) {
                return (E) b.this.get(i);
            }

            @DexIgnore
            @Override // com.fossil.tx3
            public b<E> delegateCollection() {
                return b.this;
            }
        }

        @DexIgnore
        @Override // com.fossil.iy3
        public zx3<E> createAsList() {
            return new a();
        }

        @DexIgnore
        public abstract E get(int i);

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
        public j04<E> iterator() {
            return asList().iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public c(Object[] objArr) {
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            return iy3.copyOf(this.elements);
        }
    }

    @DexIgnore
    public static <E> iy3<E> a(int i, Object... objArr) {
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return of(objArr[0]);
        }
        int chooseTableSize = chooseTableSize(i);
        Object[] objArr2 = new Object[chooseTableSize];
        int i2 = chooseTableSize - 1;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            Object obj = objArr[i5];
            iz3.a(obj, i5);
            int hashCode = obj.hashCode();
            int a2 = sx3.a(hashCode);
            while (true) {
                int i6 = a2 & i2;
                Object obj2 = objArr2[i6];
                if (obj2 == null) {
                    objArr[i3] = obj;
                    objArr2[i6] = obj;
                    i4 += hashCode;
                    i3++;
                    break;
                } else if (obj2.equals(obj)) {
                    break;
                } else {
                    a2++;
                }
            }
        }
        Arrays.fill(objArr, i3, i, (Object) null);
        if (i3 == 1) {
            return new b04(objArr[0], i4);
        }
        if (chooseTableSize != chooseTableSize(i3)) {
            return a(i3, objArr);
        }
        if (i3 < objArr.length) {
            objArr = iz3.a(objArr, i3);
        }
        return new sz3(objArr, i4, objArr2, i2);
    }

    @DexIgnore
    public static <E> a<E> builder() {
        return new a<>();
    }

    @DexIgnore
    public static int chooseTableSize(int i) {
        boolean z = true;
        if (i < 751619276) {
            int highestOneBit = Integer.highestOneBit(i - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) i)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (i >= 1073741824) {
            z = false;
        }
        jw3.a(z, "collection too large");
        return 1073741824;
    }

    @DexIgnore
    public static <E> iy3<E> copyOf(Collection<? extends E> collection) {
        if ((collection instanceof iy3) && !(collection instanceof ny3)) {
            iy3<E> iy3 = (iy3) collection;
            if (!iy3.isPartialView()) {
                return iy3;
            }
        } else if (collection instanceof EnumSet) {
            return a((EnumSet) collection);
        }
        Object[] array = collection.toArray();
        return a(array.length, array);
    }

    @DexIgnore
    public static <E> iy3<E> of() {
        return sz3.EMPTY;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public zx3<E> asList() {
        zx3<E> zx3 = this.a;
        if (zx3 != null) {
            return zx3;
        }
        zx3<E> createAsList = createAsList();
        this.a = createAsList;
        return createAsList;
    }

    @DexIgnore
    public zx3<E> createAsList() {
        return new nz3(this, toArray());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof iy3) || !isHashCodeFast() || !((iy3) obj).isHashCodeFast() || hashCode() == obj.hashCode()) {
            return yz3.a(this, obj);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return yz3.a(this);
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, java.util.Collection, java.util.Set, java.lang.Iterable
    public abstract j04<E> iterator();

    @DexIgnore
    @Override // com.fossil.vx3
    public Object writeReplace() {
        return new c(toArray());
    }

    @DexIgnore
    public static <E> iy3<E> of(E e) {
        return new b04(e);
    }

    @DexIgnore
    public static <E> iy3<E> of(E e, E e2) {
        return a(2, e, e2);
    }

    @DexIgnore
    public static <E> iy3<E> of(E e, E e2, E e3) {
        return a(3, e, e2, e3);
    }

    @DexIgnore
    public static <E> iy3<E> of(E e, E e2, E e3, E e4) {
        return a(4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E> iy3<E> of(E e, E e2, E e3, E e4, E e5) {
        return a(5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    @SafeVarargs
    public static <E> iy3<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        int length = eArr.length + 6;
        Object[] objArr = new Object[length];
        objArr[0] = e;
        objArr[1] = e2;
        objArr[2] = e3;
        objArr[3] = e4;
        objArr[4] = e5;
        objArr[5] = e6;
        System.arraycopy(eArr, 0, objArr, 6, eArr.length);
        return a(length, objArr);
    }

    @DexIgnore
    public static <E> iy3<E> copyOf(Iterable<? extends E> iterable) {
        return iterable instanceof Collection ? copyOf((Collection) iterable) : copyOf(iterable.iterator());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.iy3$a */
    /* JADX WARN: Multi-variable type inference failed */
    public static <E> iy3<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        a aVar = new a();
        aVar.a(next);
        aVar.a((Iterator) it);
        return aVar.a();
    }

    @DexIgnore
    public static <E> iy3<E> copyOf(E[] eArr) {
        int length = eArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return a(eArr.length, (Object[]) eArr.clone());
        }
        return of(eArr[0]);
    }

    @DexIgnore
    public static iy3 a(EnumSet enumSet) {
        return yx3.asImmutable(EnumSet.copyOf(enumSet));
    }
}
