package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq0 extends zk1 {
    @DexIgnore
    public static /* final */ zo0 W; // = new zo0(null);
    @DexIgnore
    public a61 U;
    @DexIgnore
    public /* final */ boolean V;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r12v3, resolved type: com.fossil.zo0 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ vq0(com.fossil.ri1 r10, com.fossil.en0 r11, java.util.HashMap r12, int r13) {
        /*
            r9 = this;
            r13 = r13 & 4
            if (r13 == 0) goto L_0x0009
            java.util.HashMap r12 = new java.util.HashMap
            r12.<init>()
        L_0x0009:
            r5 = r12
            com.fossil.wm0 r3 = com.fossil.wm0.N
            com.fossil.gq0 r12 = com.fossil.gq0.b
            java.lang.String r13 = r10.u
            com.fossil.pb1 r0 = com.fossil.pb1.ACTIVITY_FILE
            short r4 = r12.a(r13, r0)
            com.fossil.zo0 r12 = com.fossil.vq0.W
            r12.a(r5)
            r6 = 0
            r7 = 0
            r8 = 96
            r0 = r9
            r1 = r10
            r2 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            com.fossil.a61 r10 = com.fossil.a61.LOW
            r9.U = r10
            r10 = 1
            r9.V = r10
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vq0.<init>(com.fossil.ri1, com.fossil.en0, java.util.HashMap, int):void");
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.V;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public a61 e() {
        return this.U;
    }
}
