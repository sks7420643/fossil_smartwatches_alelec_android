package com.fossil;

import com.fossil.bw2;
import com.fossil.vp2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up2 extends bw2<up2, a> implements lx2 {
    @DexIgnore
    public static /* final */ up2 zzd;
    @DexIgnore
    public static volatile wx2<up2> zze;
    @DexIgnore
    public jw2<vp2> zzc; // = bw2.o();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<up2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(up2.zzd);
        }

        @DexIgnore
        public final vp2 a(int i) {
            return ((up2) ((bw2.a) this).b).b(0);
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(vp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((up2) ((bw2.a) this).b).a((vp2) ((bw2) aVar.g()));
            return this;
        }
    }

    /*
    static {
        up2 up2 = new up2();
        zzd = up2;
        bw2.a(up2.class, up2);
    }
    */

    @DexIgnore
    public static a p() {
        return (a) zzd.e();
    }

    @DexIgnore
    public final void a(vp2 vp2) {
        vp2.getClass();
        jw2<vp2> jw2 = this.zzc;
        if (!jw2.zza()) {
            this.zzc = bw2.a(jw2);
        }
        this.zzc.add(vp2);
    }

    @DexIgnore
    public final vp2 b(int i) {
        return this.zzc.get(0);
    }

    @DexIgnore
    public final List<vp2> zza() {
        return this.zzc;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new up2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", vp2.class});
            case 4:
                return zzd;
            case 5:
                wx2<up2> wx2 = zze;
                if (wx2 == null) {
                    synchronized (up2.class) {
                        wx2 = zze;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzd);
                            zze = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
