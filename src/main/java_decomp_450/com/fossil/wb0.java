package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb0 extends ac0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ rg0 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public wb0 createFromParcel(Parcel parcel) {
            return new wb0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public wb0[] newArray(int i) {
            return new wb0[i];
        }
    }

    @DexIgnore
    public wb0(byte b, rg0 rg0) {
        super(cb0.COMMUTE_TIME_TRAVEL_MICRO_APP, b, rg0);
        this.e = rg0;
    }

    @DexIgnore
    @Override // com.fossil.ac0, com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(wb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.e, ((wb0) obj).e) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest");
    }

    @DexIgnore
    public final rg0 getCommuteTimeTravelMicroAppEvent() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ac0, com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        return this.e.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public /* synthetic */ wb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(rg0.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (rg0) readParcelable;
        } else {
            ee7.a();
            throw null;
        }
    }
}
