package com.fossil;

import android.graphics.Point;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m63 {
    @DexIgnore
    public static v63 a;

    @DexIgnore
    public static void a(v63 v63) {
        a72.a(v63);
        a = v63;
    }

    @DexIgnore
    public static l63 b() {
        try {
            return new l63(c().z());
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static v63 c() {
        v63 v63 = a;
        a72.a(v63, "CameraUpdateFactory is not initialized");
        return v63;
    }

    @DexIgnore
    public static l63 a() {
        try {
            return new l63(c().n());
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 b(float f) {
        try {
            return new l63(c().e(f));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(float f, float f2) {
        try {
            return new l63(c().a(f, f2));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(float f) {
        try {
            return new l63(c().a(f));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(float f, Point point) {
        try {
            return new l63(c().a(f, point.x, point.y));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(CameraPosition cameraPosition) {
        try {
            return new l63(c().a(cameraPosition));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(LatLng latLng) {
        try {
            return new l63(c().a(latLng));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(LatLng latLng, float f) {
        try {
            return new l63(c().a(latLng, f));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static l63 a(LatLngBounds latLngBounds, int i) {
        try {
            return new l63(c().a(latLngBounds, i));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
