package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum l21 {
    UNSUPPORTED_VERSION(1),
    INVALID_FILE_DATA(2);

    @DexIgnore
    public l21(int i) {
    }
}
