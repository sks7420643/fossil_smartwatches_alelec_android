package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm4 implements Factory<lm4> {
    @DexIgnore
    public /* final */ Provider<jm4> a;
    @DexIgnore
    public /* final */ Provider<km4> b;

    @DexIgnore
    public mm4(Provider<jm4> provider, Provider<km4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static mm4 a(Provider<jm4> provider, Provider<km4> provider2) {
        return new mm4(provider, provider2);
    }

    @DexIgnore
    public static lm4 a(jm4 jm4, km4 km4) {
        return new lm4(jm4, km4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public lm4 get() {
        return a(this.a.get(), this.b.get());
    }
}
