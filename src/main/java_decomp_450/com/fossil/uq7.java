package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uq7 {
    @DexIgnore
    public static final String a(byte[] bArr) {
        ee7.b(bArr, "$this$toUtf8String");
        return new String(bArr, sg7.a);
    }

    @DexIgnore
    public static final byte[] a(String str) {
        ee7.b(str, "$this$asUtf8ToByteArray");
        byte[] bytes = str.getBytes(sg7.a);
        ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }
}
