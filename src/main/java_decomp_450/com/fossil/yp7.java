package com.fossil;

import com.fossil.zp7;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp7 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService D; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), ro7.a("OkHttp Http2Connection", true));
    @DexIgnore
    public /* final */ bq7 A;
    @DexIgnore
    public /* final */ l B;
    @DexIgnore
    public /* final */ Set<Integer> C; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ j b;
    @DexIgnore
    public /* final */ Map<Integer, aq7> c; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ ScheduledExecutorService h;
    @DexIgnore
    public /* final */ ExecutorService i;
    @DexIgnore
    public /* final */ dq7 j;
    @DexIgnore
    public long p; // = 0;
    @DexIgnore
    public long q; // = 0;
    @DexIgnore
    public long r; // = 0;
    @DexIgnore
    public long s; // = 0;
    @DexIgnore
    public long t; // = 0;
    @DexIgnore
    public long u; // = 0;
    @DexIgnore
    public long v; // = 0;
    @DexIgnore
    public long w;
    @DexIgnore
    public eq7 x; // = new eq7();
    @DexIgnore
    public /* final */ eq7 y; // = new eq7();
    @DexIgnore
    public /* final */ Socket z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qo7 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ tp7 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, tp7 tp7) {
            super(str, objArr);
            this.b = i;
            this.c = tp7;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            try {
                yp7.this.b(this.b, this.c);
            } catch (IOException unused) {
                yp7.this.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends qo7 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.b = i;
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            try {
                yp7.this.A.a(this.b, this.c);
            } catch (IOException unused) {
                yp7.this.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends qo7 {
        @DexIgnore
        public c(String str, Object... objArr) {
            super(str, objArr);
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            yp7.this.a(false, 2, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends qo7 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.b = i;
            this.c = list;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            if (yp7.this.j.a(this.b, this.c)) {
                try {
                    yp7.this.A.a(this.b, tp7.CANCEL);
                    synchronized (yp7.this) {
                        yp7.this.C.remove(Integer.valueOf(this.b));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends qo7 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i, List list, boolean z) {
            super(str, objArr);
            this.b = i;
            this.c = list;
            this.d = z;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            boolean a = yp7.this.j.a(this.b, this.c, this.d);
            if (a) {
                try {
                    yp7.this.A.a(this.b, tp7.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.d) {
                synchronized (yp7.this) {
                    yp7.this.C.remove(Integer.valueOf(this.b));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends qo7 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ yq7 c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i, yq7 yq7, int i2, boolean z) {
            super(str, objArr);
            this.b = i;
            this.c = yq7;
            this.d = i2;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            try {
                boolean a = yp7.this.j.a(this.b, this.c, this.d, this.e);
                if (a) {
                    yp7.this.A.a(this.b, tp7.CANCEL);
                }
                if (a || this.e) {
                    synchronized (yp7.this) {
                        yp7.this.C.remove(Integer.valueOf(this.b));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends qo7 {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ tp7 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str, Object[] objArr, int i, tp7 tp7) {
            super(str, objArr);
            this.b = i;
            this.c = tp7;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            yp7.this.j.a(this.b, this.c);
            synchronized (yp7.this) {
                yp7.this.C.remove(Integer.valueOf(this.b));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i extends qo7 {
        @DexIgnore
        public i() {
            super("OkHttp %s ping", yp7.this.d);
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            boolean z;
            synchronized (yp7.this) {
                if (yp7.this.q < yp7.this.p) {
                    z = true;
                } else {
                    yp7.e(yp7.this);
                    z = false;
                }
            }
            if (z) {
                yp7.this.a();
            } else {
                yp7.this.a(false, 1, 0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class j {
        @DexIgnore
        public static /* final */ j a; // = new a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends j {
            @DexIgnore
            @Override // com.fossil.yp7.j
            public void a(aq7 aq7) throws IOException {
                aq7.a(tp7.REFUSED_STREAM);
            }
        }

        @DexIgnore
        public abstract void a(aq7 aq7) throws IOException;

        @DexIgnore
        public void a(yp7 yp7) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends qo7 {
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public k(boolean z, int i, int i2) {
            super("OkHttp %s ping %08x%08x", yp7.this.d, Integer.valueOf(i), Integer.valueOf(i2));
            this.b = z;
            this.c = i;
            this.d = i2;
        }

        @DexIgnore
        @Override // com.fossil.qo7
        public void b() {
            yp7.this.a(this.b, this.c, this.d);
        }
    }

    @DexIgnore
    public yp7(h hVar) {
        this.j = hVar.f;
        boolean z2 = hVar.g;
        this.a = z2;
        this.b = hVar.e;
        int i2 = z2 ? 1 : 2;
        this.f = i2;
        if (hVar.g) {
            this.f = i2 + 2;
        }
        if (hVar.g) {
            this.x.a(7, 16777216);
        }
        this.d = hVar.b;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, ro7.a(ro7.a("OkHttp %s Writer", this.d), false));
        this.h = scheduledThreadPoolExecutor;
        if (hVar.h != 0) {
            i iVar = new i();
            int i3 = hVar.h;
            scheduledThreadPoolExecutor.scheduleAtFixedRate(iVar, (long) i3, (long) i3, TimeUnit.MILLISECONDS);
        }
        this.i = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), ro7.a(ro7.a("OkHttp %s Push Observer", this.d), true));
        this.y.a(7, 65535);
        this.y.a(5, 16384);
        this.w = (long) this.y.c();
        this.z = hVar.a;
        this.A = new bq7(hVar.d, this.a);
        this.B = new l(new zp7(hVar.c, this.a));
    }

    @DexIgnore
    public static /* synthetic */ long c(yp7 yp7) {
        long j2 = yp7.q;
        yp7.q = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long e(yp7 yp7) {
        long j2 = yp7.p;
        yp7.p = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long h(yp7 yp7) {
        long j2 = yp7.s;
        yp7.s = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long i(yp7 yp7) {
        long j2 = yp7.t;
        yp7.t = 1 + j2;
        return j2;
    }

    @DexIgnore
    public boolean b(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        a(tp7.NO_ERROR, tp7.CANCEL);
    }

    @DexIgnore
    public void flush() throws IOException {
        this.A.flush();
    }

    @DexIgnore
    public synchronized int b() {
        return this.y.b(Integer.MAX_VALUE);
    }

    @DexIgnore
    public synchronized aq7 c(int i2) {
        aq7 remove;
        remove = this.c.remove(Integer.valueOf(i2));
        notifyAll();
        return remove;
    }

    @DexIgnore
    public void e() throws IOException {
        a(true);
    }

    @DexIgnore
    public synchronized aq7 a(int i2) {
        return this.c.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public void b(int i2, tp7 tp7) throws IOException {
        this.A.a(i2, tp7);
    }

    @DexIgnore
    public synchronized boolean e(long j2) {
        if (this.g) {
            return false;
        }
        if (this.s >= this.r || j2 < this.u) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public synchronized void g(long j2) {
        long j3 = this.v + j2;
        this.v = j3;
        if (j3 >= ((long) (this.x.c() / 2))) {
            a(0, this.v);
            this.v = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public Socket a;
        @DexIgnore
        public String b;
        @DexIgnore
        public ar7 c;
        @DexIgnore
        public zq7 d;
        @DexIgnore
        public j e; // = j.a;
        @DexIgnore
        public dq7 f; // = dq7.a;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public int h;

        @DexIgnore
        public h(boolean z) {
            this.g = z;
        }

        @DexIgnore
        public h a(Socket socket, String str, ar7 ar7, zq7 zq7) {
            this.a = socket;
            this.b = str;
            this.c = ar7;
            this.d = zq7;
            return this;
        }

        @DexIgnore
        public h a(j jVar) {
            this.e = jVar;
            return this;
        }

        @DexIgnore
        public h a(int i) {
            this.h = i;
            return this;
        }

        @DexIgnore
        public yp7 a() {
            return new yp7(this);
        }
    }

    @DexIgnore
    public aq7 a(List<up7> list, boolean z2) throws IOException {
        return a(0, list, z2);
    }

    @DexIgnore
    public void b(int i2, List<up7> list, boolean z2) {
        try {
            a(new e("OkHttp %s Push Headers[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, list, z2));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.aq7 a(int r11, java.util.List<com.fossil.up7> r12, boolean r13) throws java.io.IOException {
        /*
            r10 = this;
            r6 = r13 ^ 1
            r4 = 0
            com.fossil.bq7 r7 = r10.A
            monitor-enter(r7)
            monitor-enter(r10)     // Catch:{ all -> 0x0078 }
            int r0 = r10.f     // Catch:{ all -> 0x0075 }
            r1 = 1073741823(0x3fffffff, float:1.9999999)
            if (r0 <= r1) goto L_0x0013
            com.fossil.tp7 r0 = com.fossil.tp7.REFUSED_STREAM     // Catch:{ all -> 0x0075 }
            r10.a(r0)     // Catch:{ all -> 0x0075 }
        L_0x0013:
            boolean r0 = r10.g     // Catch:{ all -> 0x0075 }
            if (r0 != 0) goto L_0x006f
            int r8 = r10.f     // Catch:{ all -> 0x0075 }
            int r0 = r10.f     // Catch:{ all -> 0x0075 }
            int r0 = r0 + 2
            r10.f = r0     // Catch:{ all -> 0x0075 }
            com.fossil.aq7 r9 = new com.fossil.aq7     // Catch:{ all -> 0x0075 }
            r5 = 0
            r0 = r9
            r1 = r8
            r2 = r10
            r3 = r6
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0075 }
            if (r13 == 0) goto L_0x003c
            long r0 = r10.w     // Catch:{ all -> 0x0075 }
            r2 = 0
            int r13 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r13 == 0) goto L_0x003c
            long r0 = r9.b     // Catch:{ all -> 0x0075 }
            int r13 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r13 != 0) goto L_0x003a
            goto L_0x003c
        L_0x003a:
            r13 = 0
            goto L_0x003d
        L_0x003c:
            r13 = 1
        L_0x003d:
            boolean r0 = r9.g()     // Catch:{ all -> 0x0075 }
            if (r0 == 0) goto L_0x004c
            java.util.Map<java.lang.Integer, com.fossil.aq7> r0 = r10.c     // Catch:{ all -> 0x0075 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0075 }
            r0.put(r1, r9)     // Catch:{ all -> 0x0075 }
        L_0x004c:
            monitor-exit(r10)     // Catch:{ all -> 0x0075 }
            if (r11 != 0) goto L_0x0055
            com.fossil.bq7 r0 = r10.A
            r0.a(r6, r8, r11, r12)
            goto L_0x005e
        L_0x0055:
            boolean r0 = r10.a
            if (r0 != 0) goto L_0x0067
            com.fossil.bq7 r0 = r10.A
            r0.a(r11, r8, r12)
        L_0x005e:
            monitor-exit(r7)
            if (r13 == 0) goto L_0x0066
            com.fossil.bq7 r11 = r10.A
            r11.flush()
        L_0x0066:
            return r9
        L_0x0067:
            java.lang.IllegalArgumentException r11 = new java.lang.IllegalArgumentException
            java.lang.String r12 = "client streams shouldn't have associated stream IDs"
            r11.<init>(r12)
            throw r11
        L_0x006f:
            com.fossil.sp7 r11 = new com.fossil.sp7
            r11.<init>()
            throw r11
        L_0x0075:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        L_0x0078:
            r11 = move-exception
            monitor-exit(r7)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yp7.a(int, java.util.List, boolean):com.fossil.aq7");
    }

    @DexIgnore
    public void c(int i2, tp7 tp7) {
        try {
            this.h.execute(new a("OkHttp %s stream %d", new Object[]{this.d, Integer.valueOf(i2)}, i2, tp7));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public void c() {
        synchronized (this) {
            if (this.s >= this.r) {
                this.r++;
                this.u = System.nanoTime() + 1000000000;
                try {
                    this.h.execute(new c("OkHttp %s ping", this.d));
                } catch (RejectedExecutionException unused) {
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends qo7 implements zp7.b {
        @DexIgnore
        public /* final */ zp7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends qo7 {
            @DexIgnore
            public /* final */ /* synthetic */ aq7 b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, aq7 aq7) {
                super(str, objArr);
                this.b = aq7;
            }

            @DexIgnore
            @Override // com.fossil.qo7
            public void b() {
                try {
                    yp7.this.b.a(this.b);
                } catch (IOException e) {
                    mq7 d = mq7.d();
                    d.a(4, "Http2Connection.Listener failure for " + yp7.this.d, e);
                    try {
                        this.b.a(tp7.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends qo7 {
            @DexIgnore
            public /* final */ /* synthetic */ boolean b;
            @DexIgnore
            public /* final */ /* synthetic */ eq7 c;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(String str, Object[] objArr, boolean z, eq7 eq7) {
                super(str, objArr);
                this.b = z;
                this.c = eq7;
            }

            @DexIgnore
            @Override // com.fossil.qo7
            public void b() {
                l.this.b(this.b, this.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends qo7 {
            @DexIgnore
            public c(String str, Object... objArr) {
                super(str, objArr);
            }

            @DexIgnore
            @Override // com.fossil.qo7
            public void b() {
                yp7 yp7 = yp7.this;
                yp7.b.a(yp7);
            }
        }

        @DexIgnore
        public l(zp7 zp7) {
            super("OkHttp %s", yp7.this.d);
            this.b = zp7;
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(boolean z, int i, ar7 ar7, int i2) throws IOException {
            if (yp7.this.b(i)) {
                yp7.this.a(i, ar7, i2, z);
                return;
            }
            aq7 a2 = yp7.this.a(i);
            if (a2 == null) {
                yp7.this.c(i, tp7.PROTOCOL_ERROR);
                long j = (long) i2;
                yp7.this.g(j);
                ar7.skip(j);
                return;
            }
            a2.a(ar7, i2);
            if (z) {
                a2.i();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1 = com.fossil.tp7.PROTOCOL_ERROR;
            r0 = com.fossil.tp7.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0020, code lost:
            r2 = r4.c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x002b, code lost:
            r2 = th;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
        @Override // com.fossil.qo7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void b() {
            /*
                r4 = this;
                com.fossil.tp7 r0 = com.fossil.tp7.INTERNAL_ERROR
                com.fossil.zp7 r1 = r4.b     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                r1.a(r4)     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
            L_0x0007:
                com.fossil.zp7 r1 = r4.b     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                r2 = 0
                boolean r1 = r1.a(r2, r4)     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                if (r1 == 0) goto L_0x0011
                goto L_0x0007
            L_0x0011:
                com.fossil.tp7 r1 = com.fossil.tp7.NO_ERROR     // Catch:{ IOException -> 0x001b, all -> 0x0018 }
                com.fossil.tp7 r0 = com.fossil.tp7.CANCEL     // Catch:{ IOException -> 0x001c }
                com.fossil.yp7 r2 = com.fossil.yp7.this     // Catch:{ IOException -> 0x0025 }
                goto L_0x0022
            L_0x0018:
                r2 = move-exception
                r1 = r0
                goto L_0x002c
            L_0x001b:
                r1 = r0
            L_0x001c:
                com.fossil.tp7 r1 = com.fossil.tp7.PROTOCOL_ERROR     // Catch:{ all -> 0x002b }
                com.fossil.tp7 r0 = com.fossil.tp7.PROTOCOL_ERROR     // Catch:{ all -> 0x002b }
                com.fossil.yp7 r2 = com.fossil.yp7.this
            L_0x0022:
                r2.a(r1, r0)
            L_0x0025:
                com.fossil.zp7 r0 = r4.b
                com.fossil.ro7.a(r0)
                return
            L_0x002b:
                r2 = move-exception
            L_0x002c:
                com.fossil.yp7 r3 = com.fossil.yp7.this     // Catch:{ IOException -> 0x0031 }
                r3.a(r1, r0)     // Catch:{ IOException -> 0x0031 }
            L_0x0031:
                com.fossil.zp7 r0 = r4.b
                com.fossil.ro7.a(r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.yp7.l.b():void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0076, code lost:
            r0.a(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0079, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x007b, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
            return;
         */
        @DexIgnore
        @Override // com.fossil.zp7.b
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(boolean r10, int r11, int r12, java.util.List<com.fossil.up7> r13) {
            /*
                r9 = this;
                com.fossil.yp7 r12 = com.fossil.yp7.this
                boolean r12 = r12.b(r11)
                if (r12 == 0) goto L_0x000e
                com.fossil.yp7 r12 = com.fossil.yp7.this
                r12.b(r11, r13, r10)
                return
            L_0x000e:
                com.fossil.yp7 r12 = com.fossil.yp7.this
                monitor-enter(r12)
                com.fossil.yp7 r0 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                com.fossil.aq7 r0 = r0.a(r11)     // Catch:{ all -> 0x007f }
                if (r0 != 0) goto L_0x0075
                com.fossil.yp7 r0 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                boolean r0 = r0.g     // Catch:{ all -> 0x007f }
                if (r0 == 0) goto L_0x0023
                monitor-exit(r12)     // Catch:{ all -> 0x007f }
                return
            L_0x0023:
                com.fossil.yp7 r0 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                int r0 = r0.e     // Catch:{ all -> 0x007f }
                if (r11 > r0) goto L_0x002b
                monitor-exit(r12)     // Catch:{ all -> 0x007f }
                return
            L_0x002b:
                int r0 = r11 % 2
                com.fossil.yp7 r1 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                int r1 = r1.f     // Catch:{ all -> 0x007f }
                r2 = 2
                int r1 = r1 % r2
                if (r0 != r1) goto L_0x0037
                monitor-exit(r12)     // Catch:{ all -> 0x007f }
                return
            L_0x0037:
                com.fossil.fo7 r8 = com.fossil.ro7.b(r13)     // Catch:{ all -> 0x007f }
                com.fossil.aq7 r13 = new com.fossil.aq7     // Catch:{ all -> 0x007f }
                com.fossil.yp7 r5 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                r6 = 0
                r3 = r13
                r4 = r11
                r7 = r10
                r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ all -> 0x007f }
                com.fossil.yp7 r10 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                r10.e = r11     // Catch:{ all -> 0x007f }
                com.fossil.yp7 r10 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                java.util.Map<java.lang.Integer, com.fossil.aq7> r10 = r10.c     // Catch:{ all -> 0x007f }
                java.lang.Integer r0 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x007f }
                r10.put(r0, r13)     // Catch:{ all -> 0x007f }
                java.util.concurrent.ExecutorService r10 = com.fossil.yp7.D     // Catch:{ all -> 0x007f }
                com.fossil.yp7$l$a r0 = new com.fossil.yp7$l$a     // Catch:{ all -> 0x007f }
                java.lang.String r1 = "OkHttp %s stream %d"
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007f }
                r3 = 0
                com.fossil.yp7 r4 = com.fossil.yp7.this     // Catch:{ all -> 0x007f }
                java.lang.String r4 = r4.d     // Catch:{ all -> 0x007f }
                r2[r3] = r4     // Catch:{ all -> 0x007f }
                r3 = 1
                java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x007f }
                r2[r3] = r11     // Catch:{ all -> 0x007f }
                r0.<init>(r1, r2, r13)     // Catch:{ all -> 0x007f }
                r10.execute(r0)     // Catch:{ all -> 0x007f }
                monitor-exit(r12)     // Catch:{ all -> 0x007f }
                return
            L_0x0075:
                monitor-exit(r12)     // Catch:{ all -> 0x007f }
                r0.a(r13)
                if (r10 == 0) goto L_0x007e
                r0.i()
            L_0x007e:
                return
            L_0x007f:
                r10 = move-exception
                monitor-exit(r12)
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.yp7.l.a(boolean, int, int, java.util.List):void");
        }

        @DexIgnore
        public void b(boolean z, eq7 eq7) {
            aq7[] aq7Arr;
            long j;
            synchronized (yp7.this.A) {
                synchronized (yp7.this) {
                    int c2 = yp7.this.y.c();
                    if (z) {
                        yp7.this.y.a();
                    }
                    yp7.this.y.a(eq7);
                    int c3 = yp7.this.y.c();
                    aq7Arr = null;
                    if (c3 == -1 || c3 == c2) {
                        j = 0;
                    } else {
                        j = (long) (c3 - c2);
                        if (!yp7.this.c.isEmpty()) {
                            aq7Arr = (aq7[]) yp7.this.c.values().toArray(new aq7[yp7.this.c.size()]);
                        }
                    }
                }
                try {
                    yp7.this.A.a(yp7.this.y);
                } catch (IOException unused) {
                    yp7.this.a();
                }
            }
            if (aq7Arr != null) {
                for (aq7 aq7 : aq7Arr) {
                    synchronized (aq7) {
                        aq7.a(j);
                    }
                }
            }
            yp7.D.execute(new c("OkHttp %s settings", yp7.this.d));
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(int i, tp7 tp7) {
            if (yp7.this.b(i)) {
                yp7.this.a(i, tp7);
                return;
            }
            aq7 c2 = yp7.this.c(i);
            if (c2 != null) {
                c2.d(tp7);
            }
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(boolean z, eq7 eq7) {
            try {
                yp7.this.h.execute(new b("OkHttp %s ACK Settings", new Object[]{yp7.this.d}, z, eq7));
            } catch (RejectedExecutionException unused) {
            }
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(boolean z, int i, int i2) {
            if (z) {
                synchronized (yp7.this) {
                    if (i == 1) {
                        try {
                            yp7.c(yp7.this);
                        } catch (Throwable th) {
                            throw th;
                        }
                    } else if (i == 2) {
                        yp7.h(yp7.this);
                    } else if (i == 3) {
                        yp7.i(yp7.this);
                        yp7.this.notifyAll();
                    }
                }
                return;
            }
            try {
                yp7.this.h.execute(new k(true, i, i2));
            } catch (RejectedExecutionException unused) {
            }
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(int i, tp7 tp7, br7 br7) {
            aq7[] aq7Arr;
            br7.size();
            synchronized (yp7.this) {
                aq7Arr = (aq7[]) yp7.this.c.values().toArray(new aq7[yp7.this.c.size()]);
                boolean unused = yp7.this.g = true;
            }
            for (aq7 aq7 : aq7Arr) {
                if (aq7.c() > i && aq7.f()) {
                    aq7.d(tp7.REFUSED_STREAM);
                    yp7.this.c(aq7.c());
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(int i, long j) {
            if (i == 0) {
                synchronized (yp7.this) {
                    yp7.this.w += j;
                    yp7.this.notifyAll();
                }
                return;
            }
            aq7 a2 = yp7.this.a(i);
            if (a2 != null) {
                synchronized (a2) {
                    a2.a(j);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.zp7.b
        public void a(int i, int i2, List<up7> list) {
            yp7.this.a(i2, list);
        }
    }

    @DexIgnore
    public void a(int i2, boolean z2, yq7 yq7, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.A.a(z2, i2, yq7, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.w <= 0) {
                    try {
                        if (this.c.containsKey(Integer.valueOf(i2))) {
                            wait();
                        } else {
                            throw new IOException("stream closed");
                        }
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.w), this.A.b());
                j3 = (long) min;
                this.w -= j3;
            }
            j2 -= j3;
            this.A.a(z2 && j2 == 0, i2, yq7, min);
        }
    }

    @DexIgnore
    public void a(int i2, long j2) {
        try {
            this.h.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.d, Integer.valueOf(i2)}, i2, j2));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public void a(boolean z2, int i2, int i3) {
        try {
            this.A.a(z2, i2, i3);
        } catch (IOException unused) {
            a();
        }
    }

    @DexIgnore
    public void a(tp7 tp7) throws IOException {
        synchronized (this.A) {
            synchronized (this) {
                if (!this.g) {
                    this.g = true;
                    this.A.a(this.e, tp7, ro7.a);
                }
            }
        }
    }

    @DexIgnore
    public void a(tp7 tp7, tp7 tp72) throws IOException {
        aq7[] aq7Arr = null;
        try {
            a(tp7);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.c.isEmpty()) {
                aq7Arr = (aq7[]) this.c.values().toArray(new aq7[this.c.size()]);
                this.c.clear();
            }
        }
        if (aq7Arr != null) {
            for (aq7 aq7 : aq7Arr) {
                try {
                    aq7.a(tp72);
                } catch (IOException e3) {
                    if (e != null) {
                        e = e3;
                    }
                }
            }
        }
        try {
            this.A.close();
        } catch (IOException e4) {
            if (e == null) {
                e = e4;
            }
        }
        try {
            this.z.close();
        } catch (IOException e5) {
            e = e5;
        }
        this.h.shutdown();
        this.i.shutdown();
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public final void a() {
        try {
            a(tp7.PROTOCOL_ERROR, tp7.PROTOCOL_ERROR);
        } catch (IOException unused) {
        }
    }

    @DexIgnore
    public void a(boolean z2) throws IOException {
        if (z2) {
            this.A.a();
            this.A.b(this.x);
            int c2 = this.x.c();
            if (c2 != 65535) {
                this.A.a(0, (long) (c2 - 65535));
            }
        }
        new Thread(this.B).start();
    }

    @DexIgnore
    public void a(int i2, List<up7> list) {
        synchronized (this) {
            if (this.C.contains(Integer.valueOf(i2))) {
                c(i2, tp7.PROTOCOL_ERROR);
                return;
            }
            this.C.add(Integer.valueOf(i2));
            try {
                a(new d("OkHttp %s Push Request[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, list));
            } catch (RejectedExecutionException unused) {
            }
        }
    }

    @DexIgnore
    public void a(int i2, ar7 ar7, int i3, boolean z2) throws IOException {
        yq7 yq7 = new yq7();
        long j2 = (long) i3;
        ar7.h(j2);
        ar7.b(yq7, j2);
        if (yq7.x() == j2) {
            a(new f("OkHttp %s Push Data[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, yq7, i3, z2));
            return;
        }
        throw new IOException(yq7.x() + " != " + i3);
    }

    @DexIgnore
    public void a(int i2, tp7 tp7) {
        a(new g("OkHttp %s Push Reset[%s]", new Object[]{this.d, Integer.valueOf(i2)}, i2, tp7));
    }

    @DexIgnore
    public final synchronized void a(qo7 qo7) {
        if (!this.g) {
            this.i.execute(qo7);
        }
    }
}
