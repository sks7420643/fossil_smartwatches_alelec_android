package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm7 {
    @DexIgnore
    public static final int a() {
        return nm7.a();
    }

    @DexIgnore
    public static final int a(String str, int i, int i2, int i3) {
        return om7.a(str, i, i2, i3);
    }

    @DexIgnore
    public static final long a(String str, long j, long j2, long j3) {
        return om7.a(str, j, j2, j3);
    }

    @DexIgnore
    public static final String a(String str) {
        return nm7.a(str);
    }

    @DexIgnore
    public static final boolean a(String str, boolean z) {
        return om7.a(str, z);
    }
}
