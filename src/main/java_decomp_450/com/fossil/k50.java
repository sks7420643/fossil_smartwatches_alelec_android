package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k50 implements yw {
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public k50(Object obj) {
        u50.a(obj);
        this.b = obj;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(yw.a));
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (obj instanceof k50) {
            return this.b.equals(((k50) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
