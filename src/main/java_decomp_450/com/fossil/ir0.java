package com.fossil;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir0 {
    @DexIgnore
    public static /* final */ fo1 a; // = fo1.CTR_NO_PADDING;
    @DexIgnore
    public static /* final */ ir0 b; // = new ir0();

    @DexIgnore
    public final byte[] a(String str, qk1 qk1, gm1 gm1, byte[] bArr) {
        qn0 a2 = mp0.b.a(str);
        byte[] a3 = a2.a();
        byte[] a4 = a2.a(qk1).a();
        if (a3 == null) {
            return bArr;
        }
        boolean z = false;
        if (true != (!(a3.length == 0))) {
            return bArr;
        }
        if (a4.length == 0) {
            z = true;
        }
        if (!(!z)) {
            return bArr;
        }
        fo1 fo1 = a;
        SecretKeySpec secretKeySpec = new SecretKeySpec(a3, "AES");
        Cipher instance = Cipher.getInstance(fo1.a);
        instance.init(gm1.a, secretKeySpec, new IvParameterSpec(a4));
        byte[] doFinal = instance.doFinal(bArr);
        ee7.a((Object) doFinal, "cipher.doFinal(data)");
        int ceil = (int) ((float) Math.ceil((double) (((float) bArr.length) / ((float) 16))));
        dt0 dt0 = a2.d.get(qk1);
        if (dt0 != null) {
            dt0.e += ceil;
        }
        return doFinal;
    }

    @DexIgnore
    public final byte[] b(String str, qk1 qk1, byte[] bArr) {
        return a(str, qk1, gm1.ENCRYPT, bArr);
    }

    @DexIgnore
    public final byte[] a(String str, qk1 qk1, byte[] bArr) {
        return a(str, qk1, gm1.DECRYPT, bArr);
    }
}
