package com.fossil;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc2 implements ty1 {
    @DexIgnore
    public /* final */ Set<Scope> a;

    @DexIgnore
    public bc2(a aVar) {
        this.a = xd2.a(aVar.a);
    }

    @DexIgnore
    public static a b() {
        return new a();
    }

    @DexIgnore
    @Override // com.fossil.ty1
    public final List<Scope> a() {
        return new ArrayList(this.a);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bc2)) {
            return false;
        }
        return this.a.equals(((bc2) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(this.a);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Set<Scope> a;

        @DexIgnore
        public a() {
            this.a = new HashSet();
        }

        @DexIgnore
        public final a a(DataType dataType, int i) {
            a72.a(i == 0 || i == 1, "valid access types are FitnessOptions.ACCESS_READ or FitnessOptions.ACCESS_WRITE");
            if (i == 0 && dataType.v() != null) {
                this.a.add(new Scope(dataType.v()));
            } else if (i == 1 && dataType.w() != null) {
                this.a.add(new Scope(dataType.w()));
            }
            return this;
        }

        @DexIgnore
        public final bc2 a() {
            return new bc2(this);
        }
    }
}
