package com.fossil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.Utility;
import com.fossil.fl4;
import com.fossil.mn5;
import com.fossil.on5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oi6 extends he {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public MFUser a;
    @DexIgnore
    public MFUser b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public MutableLiveData<b> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ on5 i;
    @DexIgnore
    public /* final */ mn5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return oi6.k;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public MFUser a;
        @DexIgnore
        public Uri b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public r87<Integer, String> d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public MFUser f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public Bundle h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;

        @DexIgnore
        public b(MFUser mFUser, Uri uri, Boolean bool, r87<Integer, String> r87, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2) {
            this.a = mFUser;
            this.b = uri;
            this.c = bool;
            this.d = r87;
            this.e = z;
            this.f = mFUser2;
            this.g = z2;
            this.h = bundle;
            this.i = str;
            this.j = str2;
        }

        @DexIgnore
        public final String a() {
            return this.i;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final Bundle c() {
            return this.h;
        }

        @DexIgnore
        public final Uri d() {
            return this.b;
        }

        @DexIgnore
        public final boolean e() {
            return this.g;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && ee7.a(this.b, bVar.b) && ee7.a(this.c, bVar.c) && ee7.a(this.d, bVar.d) && this.e == bVar.e && ee7.a(this.f, bVar.f) && this.g == bVar.g && ee7.a(this.h, bVar.h) && ee7.a(this.i, bVar.i) && ee7.a(this.j, bVar.j);
        }

        @DexIgnore
        public final boolean f() {
            return this.e;
        }

        @DexIgnore
        public final r87<Integer, String> g() {
            return this.d;
        }

        @DexIgnore
        public final MFUser h() {
            return this.f;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r2v10, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            MFUser mFUser = this.a;
            int i2 = 0;
            int hashCode = (mFUser != null ? mFUser.hashCode() : 0) * 31;
            Uri uri = this.b;
            int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
            Boolean bool = this.c;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            r87<Integer, String> r87 = this.d;
            int hashCode4 = (hashCode3 + (r87 != null ? r87.hashCode() : 0)) * 31;
            boolean z = this.e;
            int i3 = 1;
            if (z) {
                z = true;
            }
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            int i6 = (hashCode4 + i4) * 31;
            MFUser mFUser2 = this.f;
            int hashCode5 = (i6 + (mFUser2 != null ? mFUser2.hashCode() : 0)) * 31;
            boolean z2 = this.g;
            if (z2 == 0) {
                i3 = z2;
            }
            int i7 = (hashCode5 + i3) * 31;
            Bundle bundle = this.h;
            int hashCode6 = (i7 + (bundle != null ? bundle.hashCode() : 0)) * 31;
            String str = this.i;
            int hashCode7 = (hashCode6 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.j;
            if (str2 != null) {
                i2 = str2.hashCode();
            }
            return hashCode7 + i2;
        }

        @DexIgnore
        public final MFUser i() {
            return this.a;
        }

        @DexIgnore
        public final Boolean j() {
            return this.c;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(user=" + this.a + ", imageUri=" + this.b + ", isUserChanged=" + this.c + ", showServerError=" + this.d + ", showProcessImageError=" + this.e + ", showSuccess=" + this.f + ", showLoading=" + this.g + ", dobBundle=" + this.h + ", birthday=" + this.i + ", birthdayErrorMsg=" + this.j + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<mn5.a, fl4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ oi6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(oi6 oi6) {
            this.a = oi6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mn5.a aVar) {
            ee7.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(oi6.l.a(), ".Inside mGetUser onSuccess");
            oi6.a(this.a, null, null, null, null, false, null, false, null, null, null, 959, null);
            if (aVar.a() != null) {
                this.a.b = aVar.a();
                this.a.a = MFUser.copy$default(aVar.a(), null, null, null, null, null, false, null, false, false, null, null, 0, null, null, null, null, false, null, false, false, false, null, 0, 0, null, 0, 67108863, null);
                oi6 oi6 = this.a;
                oi6.a(oi6, oi6.b, null, null, null, false, null, false, null, null, null, 1022, null);
            } else {
                FLogger.INSTANCE.getLocal().d(oi6.l.a(), "loadUserFirstTime user is null");
            }
            this.a.f = true;
        }

        @DexIgnore
        public void a(fl4.a aVar) {
            ee7.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(oi6.l.a(), ".Inside mGetUser onError");
            oi6.a(this.a, null, null, null, null, false, null, false, null, null, null, 959, null);
            this.a.f = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1", f = "ProfileEditViewModel.kt", l = {196}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $data;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oi6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1$bitmap$1", f = "ProfileEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Bitmap>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Uri $imageUri;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Uri uri, fb7 fb7) {
                super(2, fb7);
                this.$imageUri = uri;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$imageUri, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Bitmap> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return hd5.a(PortfolioApp.g0.c()).b().a(this.$imageUri).a(((r40) ((r40) new r40().a(iy.a)).a(true)).a((ex<Bitmap>) new vd5())).c(200, 200).get();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(oi6 oi6, Intent intent, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oi6;
            this.$data = intent;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$data, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Uri a3 = je5.a(this.$data, PortfolioApp.g0.c());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a4 = oi6.l.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .onActivityResult imageUri=");
                if (a3 != null) {
                    sb.append(a3);
                    local.d(a4, sb.toString());
                    if (!PortfolioApp.g0.c().a(this.$data, a3)) {
                        return i97.a;
                    }
                    oi6.a(this.this$0, null, a3, null, null, false, null, false, null, null, null, 1021, null);
                    ti7 b = qj7.b();
                    a aVar = new a(a3, null);
                    this.L$0 = yi7;
                    this.L$1 = a3;
                    this.label = 1;
                    obj2 = vh7.a(b, aVar, this);
                    if (obj2 == a2) {
                        return a2;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (i == 1) {
                Uri uri = (Uri) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                try {
                    t87.a(obj);
                    obj2 = obj;
                } catch (Exception e) {
                    e.printStackTrace();
                    oi6.a(this.this$0, null, null, null, null, true, null, false, null, null, null, 1007, null);
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Bitmap bitmap = (Bitmap) obj2;
            MFUser a5 = this.this$0.b;
            if (a5 != null) {
                if (bitmap != null) {
                    String a6 = sw6.a(bitmap);
                    ee7.a((Object) a6, "BitmapUtils.encodeToBase64(bitmap!!)");
                    a5.setProfilePicture(a6);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.this$0.c = true;
            oi6.a(this.this$0, null, null, pb7.a(this.this$0.c()), null, false, null, false, null, null, null, 1019, null);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ oi6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(oi6 oi6) {
            this.a = oi6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(oi6.l.a(), ".Inside updateUser onSuccess");
            oi6.a(this.a, null, null, null, null, false, dVar.a(), false, null, null, null, 927, null);
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = oi6.l.a();
            local.d(a2, ".Inside updateUser onError, errorCode=" + cVar.a());
            oi6.a(this.a, null, null, null, new r87(Integer.valueOf(cVar.a()), ""), false, null, false, null, null, null, 951, null);
        }
    }

    /*
    static {
        String simpleName = oi6.class.getSimpleName();
        ee7.a((Object) simpleName, "ProfileEditViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public oi6(on5 on5, mn5 mn5) {
        ee7.b(on5, "mUpdateUser");
        ee7.b(mn5, "mGetUser");
        this.i = on5;
        this.j = mn5;
    }

    @DexIgnore
    public final boolean c() {
        String str;
        String str2;
        MFUser mFUser = this.a;
        if (mFUser == null || this.b == null) {
            return false;
        }
        if (mFUser != null) {
            String firstName = mFUser.getFirstName();
            if (firstName == null) {
                str = null;
            } else if (firstName != null) {
                str = nh7.d((CharSequence) firstName).toString();
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
            }
            MFUser mFUser2 = this.b;
            if (mFUser2 != null) {
                if (!(!ee7.a((Object) str, (Object) mFUser2.getFirstName()))) {
                    MFUser mFUser3 = this.a;
                    if (mFUser3 != null) {
                        String lastName = mFUser3.getLastName();
                        if (lastName == null) {
                            str2 = null;
                        } else if (lastName != null) {
                            str2 = nh7.d((CharSequence) lastName).toString();
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
                        }
                        MFUser mFUser4 = this.b;
                        if (mFUser4 == null) {
                            ee7.a();
                            throw null;
                        } else if (!(!ee7.a((Object) str2, (Object) mFUser4.getLastName())) && !b()) {
                            MFUser mFUser5 = this.a;
                            if (mFUser5 != null) {
                                String gender = mFUser5.getGender();
                                MFUser mFUser6 = this.b;
                                if (mFUser6 == null) {
                                    ee7.a();
                                    throw null;
                                } else if (!(!ee7.a((Object) gender, (Object) mFUser6.getGender())) && !this.c) {
                                    MFUser mFUser7 = this.a;
                                    if (mFUser7 != null) {
                                        String birthday = mFUser7.getBirthday();
                                        MFUser mFUser8 = this.b;
                                        if (mFUser8 == null) {
                                            ee7.a();
                                            throw null;
                                        } else if (!(!ee7.a((Object) birthday, (Object) mFUser8.getBirthday()))) {
                                            return false;
                                        }
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (this.g) {
                    return true;
                }
                return false;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void d() {
        if (!this.f) {
            a(this, null, null, null, null, false, null, true, null, null, null, 959, null);
            this.j.a((fl4.b) null, new c(this));
        }
    }

    @DexIgnore
    public final void e() {
        List list;
        String birthday;
        Bundle bundle = new Bundle();
        String str = this.d;
        if (str == null || (list = nh7.a((CharSequence) str, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, (Object) null)) == null) {
            MFUser mFUser = this.b;
            list = (mFUser == null || (birthday = mFUser.getBirthday()) == null) ? null : nh7.a((CharSequence) birthday, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, (Object) null);
        }
        FLogger.INSTANCE.getLocal().d(k, String.valueOf(list));
        if (list != null && list.size() == 3) {
            bundle.putInt("DAY", Integer.parseInt((String) list.get(2)));
            bundle.putInt("MONTH", Integer.parseInt((String) list.get(1)));
            bundle.putInt("YEAR", Integer.parseInt((String) list.get(0)));
        }
        a(this, null, null, null, null, false, null, false, bundle, null, null, 895, null);
    }

    @DexIgnore
    public final void f() {
        if (this.b != null) {
            a(this, null, null, null, null, false, null, true, null, null, null, 959, null);
            MFUser mFUser = this.b;
            if (mFUser != null) {
                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                    MFUser mFUser2 = this.b;
                    if (mFUser2 != null) {
                        String profilePicture = mFUser2.getProfilePicture();
                        Boolean valueOf = profilePicture != null ? Boolean.valueOf(nh7.a((CharSequence) profilePicture, (CharSequence) Utility.URL_SCHEME, false, 2, (Object) null)) : null;
                        if (valueOf == null) {
                            ee7.a();
                            throw null;
                        } else if (valueOf.booleanValue()) {
                            MFUser mFUser3 = this.b;
                            if (mFUser3 != null) {
                                mFUser3.setProfilePicture("");
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                MFUser mFUser4 = this.b;
                if (mFUser4 != null) {
                    if (mFUser4.getUseDefaultBiometric() && this.e) {
                        MFUser mFUser5 = this.b;
                        if (mFUser5 != null) {
                            mFUser5.setUseDefaultBiometric(false);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    on5 on5 = this.i;
                    MFUser mFUser6 = this.b;
                    if (mFUser6 != null) {
                        on5.a(new on5.b(mFUser6), new e(this));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        if (r0 != r4.getHeightInCentimeters()) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
        if (r5 != (r0 + r4.intValue())) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a6, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e8, code lost:
        if (com.fossil.af7.a(r4 * r5) != com.fossil.af7.a((((float) r7.getWeightInGrams()) / 1000.0f) * r5)) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x011a, code lost:
        if (com.fossil.af7.a(r4 * r5) != com.fossil.af7.a(com.fossil.xd5.f((float) r6.getWeightInGrams()) * r5)) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x011e, code lost:
        r2 = false;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean b() {
        /*
            r8 = this;
            com.portfolio.platform.data.model.MFUser r0 = r8.a
            r1 = 0
            if (r0 == 0) goto L_0x013d
            com.portfolio.platform.data.model.MFUser r2 = r8.b
            if (r2 != 0) goto L_0x000b
            goto L_0x013d
        L_0x000b:
            r2 = 0
            if (r0 == 0) goto L_0x0139
            com.portfolio.platform.data.model.MFUser$UnitGroup r0 = r0.getUnitGroup()
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = r0.getHeight()
            goto L_0x001a
        L_0x0019:
            r0 = r2
        L_0x001a:
            com.fossil.ob5 r3 = com.fossil.ob5.METRIC
            java.lang.String r3 = r3.getValue()
            boolean r0 = com.fossil.ee7.a(r0, r3)
            r3 = 1
            if (r0 == 0) goto L_0x0042
            com.portfolio.platform.data.model.MFUser r0 = r8.a
            if (r0 == 0) goto L_0x003e
            int r0 = r0.getHeightInCentimeters()
            com.portfolio.platform.data.model.MFUser r4 = r8.b
            if (r4 == 0) goto L_0x003a
            int r4 = r4.getHeightInCentimeters()
            if (r0 == r4) goto L_0x00a6
            goto L_0x00a4
        L_0x003a:
            com.fossil.ee7.a()
            throw r2
        L_0x003e:
            com.fossil.ee7.a()
            throw r2
        L_0x0042:
            com.portfolio.platform.data.model.MFUser r0 = r8.a
            if (r0 == 0) goto L_0x0135
            int r0 = r0.getHeightInCentimeters()
            float r0 = (float) r0
            com.fossil.r87 r0 = com.fossil.xd5.b(r0)
            com.portfolio.platform.data.model.MFUser r4 = r8.b
            if (r4 == 0) goto L_0x0131
            int r4 = r4.getHeightInCentimeters()
            float r4 = (float) r4
            com.fossil.r87 r4 = com.fossil.xd5.b(r4)
            java.lang.Object r5 = r0.getFirst()
            java.lang.String r6 = "defaultHeightInFeetAndInches.first"
            com.fossil.ee7.a(r5, r6)
            java.lang.Number r5 = (java.lang.Number) r5
            int r5 = r5.intValue()
            int r5 = com.fossil.xd5.a(r5)
            java.lang.Object r0 = r0.getSecond()
            java.lang.String r6 = "defaultHeightInFeetAndInches.second"
            com.fossil.ee7.a(r0, r6)
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            int r5 = r5 + r0
            java.lang.Object r0 = r4.getFirst()
            java.lang.String r6 = "currentHeightInFeetAndInches.first"
            com.fossil.ee7.a(r0, r6)
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            int r0 = com.fossil.xd5.a(r0)
            java.lang.Object r4 = r4.getSecond()
            java.lang.String r6 = "currentHeightInFeetAndInches.second"
            com.fossil.ee7.a(r4, r6)
            java.lang.Number r4 = (java.lang.Number) r4
            int r4 = r4.intValue()
            int r0 = r0 + r4
            if (r5 == r0) goto L_0x00a6
        L_0x00a4:
            r0 = 1
            goto L_0x00a7
        L_0x00a6:
            r0 = 0
        L_0x00a7:
            com.portfolio.platform.data.model.MFUser r4 = r8.a
            if (r4 == 0) goto L_0x012d
            com.portfolio.platform.data.model.MFUser$UnitGroup r4 = r4.getUnitGroup()
            if (r4 == 0) goto L_0x00b6
            java.lang.String r4 = r4.getWeight()
            goto L_0x00b7
        L_0x00b6:
            r4 = r2
        L_0x00b7:
            com.fossil.ob5 r5 = com.fossil.ob5.METRIC
            java.lang.String r5 = r5.getValue()
            boolean r4 = com.fossil.ee7.a(r4, r5)
            r5 = 10
            if (r4 == 0) goto L_0x00f3
            com.portfolio.platform.data.model.MFUser r4 = r8.a
            if (r4 == 0) goto L_0x00ef
            int r4 = r4.getWeightInGrams()
            float r4 = (float) r4
            r6 = 1148846080(0x447a0000, float:1000.0)
            float r4 = r4 / r6
            com.portfolio.platform.data.model.MFUser r7 = r8.b
            if (r7 == 0) goto L_0x00eb
            int r2 = r7.getWeightInGrams()
            float r2 = (float) r2
            float r2 = r2 / r6
            float r5 = (float) r5
            float r4 = r4 * r5
            int r4 = com.fossil.af7.a(r4)
            float r2 = r2 * r5
            int r2 = com.fossil.af7.a(r2)
            if (r4 == r2) goto L_0x011e
            goto L_0x011c
        L_0x00eb:
            com.fossil.ee7.a()
            throw r2
        L_0x00ef:
            com.fossil.ee7.a()
            throw r2
        L_0x00f3:
            com.portfolio.platform.data.model.MFUser r4 = r8.a
            if (r4 == 0) goto L_0x0129
            int r4 = r4.getWeightInGrams()
            float r4 = (float) r4
            float r4 = com.fossil.xd5.f(r4)
            com.portfolio.platform.data.model.MFUser r6 = r8.b
            if (r6 == 0) goto L_0x0125
            int r2 = r6.getWeightInGrams()
            float r2 = (float) r2
            float r2 = com.fossil.xd5.f(r2)
            float r5 = (float) r5
            float r4 = r4 * r5
            int r4 = com.fossil.af7.a(r4)
            float r2 = r2 * r5
            int r2 = com.fossil.af7.a(r2)
            if (r4 == r2) goto L_0x011e
        L_0x011c:
            r2 = 1
            goto L_0x011f
        L_0x011e:
            r2 = 0
        L_0x011f:
            if (r0 != 0) goto L_0x0123
            if (r2 == 0) goto L_0x0124
        L_0x0123:
            r1 = 1
        L_0x0124:
            return r1
        L_0x0125:
            com.fossil.ee7.a()
            throw r2
        L_0x0129:
            com.fossil.ee7.a()
            throw r2
        L_0x012d:
            com.fossil.ee7.a()
            throw r2
        L_0x0131:
            com.fossil.ee7.a()
            throw r2
        L_0x0135:
            com.fossil.ee7.a()
            throw r2
        L_0x0139:
            com.fossil.ee7.a()
            throw r2
        L_0x013d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oi6.b():boolean");
    }

    @DexIgnore
    public final MutableLiveData<b> a() {
        return this.h;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setFirstName(str);
        }
        a(this, null, null, Boolean.valueOf(c()), null, false, null, false, null, null, null, 1019, null);
    }

    @DexIgnore
    public final void a(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
        a(this, null, null, Boolean.valueOf(c()), null, false, null, false, null, null, null, 1019, null);
    }

    @DexIgnore
    public final void a(fb5 fb5) {
        ee7.b(fb5, "gender");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setGender(fb5.toString());
        }
        a(this, null, null, Boolean.valueOf(c()), null, false, null, false, null, null, null, 1019, null);
    }

    @DexIgnore
    public final void a(Intent intent) {
        ik7 unused = xh7.b(ie.a(this), null, null, new d(this, intent, null), 3, null);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.oi6 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void a(oi6 oi6, MFUser mFUser, Uri uri, Boolean bool, r87 r87, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            mFUser = null;
        }
        if ((i2 & 2) != 0) {
            uri = null;
        }
        if ((i2 & 4) != 0) {
            bool = null;
        }
        if ((i2 & 8) != 0) {
            r87 = null;
        }
        if ((i2 & 16) != 0) {
            z = false;
        }
        if ((i2 & 32) != 0) {
            mFUser2 = null;
        }
        if ((i2 & 64) != 0) {
            z2 = false;
        }
        if ((i2 & 128) != 0) {
            bundle = null;
        }
        if ((i2 & 256) != 0) {
            str = null;
        }
        if ((i2 & 512) != 0) {
            str2 = null;
        }
        oi6.a(mFUser, uri, bool, r87, z, mFUser2, z2, bundle, str, str2);
    }

    @DexIgnore
    public final void a(MFUser mFUser, Uri uri, Boolean bool, r87<Integer, String> r87, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2) {
        this.h.a(new b(mFUser, uri, bool, r87, z, mFUser2, z2, bundle, str, str2));
    }

    @DexIgnore
    public final boolean a(Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(instance), LocalDate.now());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("age=");
        ee7.a((Object) yearsBetween, "age");
        sb.append(yearsBetween.getYears());
        local.d(str, sb.toString());
        return yearsBetween.getYears() >= 16;
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setLastName(str);
        }
        a(this, null, null, Boolean.valueOf(c()), null, false, null, false, null, null, null, 1019, null);
    }

    @DexIgnore
    public final void b(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
        a(this, null, null, Boolean.valueOf(c()), null, false, null, false, null, null, null, 1019, null);
    }

    @DexIgnore
    public final void b(Date date) {
        ee7.b(date, "birthDay");
        FLogger.INSTANCE.getLocal().d(k, "onBirthDayChanged");
        this.d = zd5.g(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "mBirthday=" + this.d);
        MFUser mFUser = this.b;
        if (mFUser != null) {
            String str2 = this.d;
            if (str2 != null) {
                mFUser.setBirthday(str2);
            } else {
                ee7.a();
                throw null;
            }
        }
        if (!a(date)) {
            this.g = false;
            a(this, null, null, false, null, false, null, false, null, zd5.f(date), ig5.a(PortfolioApp.g0.c(), 2131886911), 251, null);
            return;
        }
        this.g = true;
        a(this, null, null, Boolean.valueOf(c()), null, false, null, false, null, zd5.f(date), null, 763, null);
    }
}
