package com.fossil;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw7 implements MethodChannel.MethodCallHandler {
    @DexIgnore
    public static /* final */ ThreadPoolExecutor e; // = new ThreadPoolExecutor(11, 1000, 200, TimeUnit.MINUTES, new ArrayBlockingQueue(11));
    @DexIgnore
    public static boolean f; // = true;
    @DexIgnore
    public static /* final */ c g; // = new c(null);
    @DexIgnore
    public /* final */ hx7 a; // = new hx7();
    @DexIgnore
    public /* final */ uw7 b; // = new uw7(this.d, new Handler());
    @DexIgnore
    public /* final */ tw7 c;
    @DexIgnore
    public /* final */ PluginRegistry.Registrar d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements PluginRegistry.RequestPermissionsResultListener {
        @DexIgnore
        public /* final */ /* synthetic */ vw7 a;

        @DexIgnore
        public a(vw7 vw7) {
            this.a = vw7;
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener
        public final boolean onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
            this.a.a.a(i, strArr, iArr);
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gx7 {
        @DexIgnore
        @Override // com.fossil.gx7
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.gx7
        public void a(List<String> list, List<String> list2) {
            ee7.b(list, "deniedPermissions");
            ee7.b(list2, "grantedPermissions");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final void a(vc7<i97> vc7) {
            ee7.b(vc7, "runnable");
            vw7.e.execute(new ww7(vc7));
        }

        @DexIgnore
        public /* synthetic */ c(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final boolean a() {
            return vw7.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("ids");
            if (argument != null) {
                ee7.a(argument, "call.argument<List<String>>(\"ids\")!!");
                this.$resultHandler.a(this.this$0.c.a((List) argument));
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            try {
                Object argument = this.$call.argument("image");
                if (argument != null) {
                    ee7.a(argument, "call.argument<ByteArray>(\"image\")!!");
                    byte[] bArr = (byte[]) argument;
                    String str = (String) this.$call.argument("title");
                    String str2 = "";
                    if (str == null) {
                        str = str2;
                    }
                    ee7.a((Object) str, "call.argument<String>(\"title\") ?: \"\"");
                    String str3 = (String) this.$call.argument(Constants.DESC);
                    if (str3 != null) {
                        str2 = str3;
                    }
                    ee7.a((Object) str2, "call.argument<String>(\"desc\") ?: \"\"");
                    zw7 a = this.this$0.c.a(bArr, str, str2);
                    if (a == null) {
                        this.$resultHandler.a(null);
                        return;
                    }
                    this.$resultHandler.a(dx7.a.a(a));
                    return;
                }
                ee7.a();
                throw null;
            } catch (Exception e) {
                mx7.a("save image error", e);
                this.$resultHandler.a(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            try {
                Object argument = this.$call.argument("path");
                if (argument != null) {
                    ee7.a(argument, "call.argument<String>(\"path\")!!");
                    String str = (String) argument;
                    Object argument2 = this.$call.argument("title");
                    if (argument2 != null) {
                        ee7.a(argument2, "call.argument<String>(\"title\")!!");
                        String str2 = (String) argument2;
                        String str3 = (String) this.$call.argument(Constants.DESC);
                        if (str3 == null) {
                            str3 = "";
                        }
                        ee7.a((Object) str3, "call.argument<String>(\"desc\") ?: \"\"");
                        zw7 a = this.this$0.c.a(str, str2, str3);
                        if (a == null) {
                            this.$resultHandler.a(null);
                            return;
                        }
                        this.$resultHandler.a(dx7.a.a(a));
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            } catch (Exception e) {
                mx7.a("save video error", e);
                this.$resultHandler.a(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("type");
            if (argument != null) {
                ee7.a(argument, "call.argument<Int>(\"type\")!!");
                int intValue = ((Number) argument).intValue();
                long b = this.this$0.b(this.$call);
                Object argument2 = this.$call.argument("hasAll");
                if (argument2 != null) {
                    ee7.a(argument2, "call.argument<Boolean>(\"hasAll\")!!");
                    this.$resultHandler.a(dx7.a.b(this.this$0.c.a(intValue, b, ((Boolean) argument2).booleanValue(), this.this$0.a(this.$call))));
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                ee7.a(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                Object argument2 = this.$call.argument(PlaceFields.PAGE);
                if (argument2 != null) {
                    ee7.a(argument2, "call.argument<Int>(\"page\")!!");
                    int intValue = ((Number) argument2).intValue();
                    Object argument3 = this.$call.argument("pageCount");
                    if (argument3 != null) {
                        ee7.a(argument3, "call.argument<Int>(\"pageCount\")!!");
                        int intValue2 = ((Number) argument3).intValue();
                        Object argument4 = this.$call.argument("type");
                        if (argument4 != null) {
                            ee7.a(argument4, "call.argument<Int>(\"type\")!!");
                            this.$resultHandler.a(dx7.a.a(this.this$0.c.a(str, intValue, intValue2, ((Number) argument4).intValue(), this.this$0.b(this.$call), this.this$0.a(this.$call))));
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            this.$resultHandler.a(dx7.a.a(this.this$0.c.b(this.this$0.b(this.$call, "galleryId"), this.this$0.a(this.$call, "type"), this.this$0.a(this.$call, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE), this.this$0.a(this.$call, "end"), this.this$0.b(this.$call), this.this$0.a(this.$call))));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                ee7.a(argument, "call.argument<String>(\"id\")!!");
                this.this$0.c.a((String) argument, this.$resultHandler);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $haveLocationPermission;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(vw7 vw7, MethodCall methodCall, boolean z, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$haveLocationPermission = z;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            boolean z;
            Object argument = this.$call.argument("id");
            if (argument != null) {
                ee7.a(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                if (!this.$haveLocationPermission) {
                    z = false;
                } else {
                    Object argument2 = this.$call.argument("isOrigin");
                    if (argument2 != null) {
                        ee7.a(argument2, "call.argument<Boolean>(\"isOrigin\")!!");
                        z = ((Boolean) argument2).booleanValue();
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                this.this$0.c.a(str, z, this.$resultHandler);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $haveLocationPermission;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(vw7 vw7, MethodCall methodCall, boolean z, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$haveLocationPermission = z;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                ee7.a(argument, "call.argument<String>(\"id\")!!");
                this.this$0.c.a((String) argument, vw7.g.a(), this.$haveLocationPermission, this.$resultHandler);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                ee7.a(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                Object argument2 = this.$call.argument("type");
                if (argument2 != null) {
                    ee7.a(argument2, "call.argument<Int>(\"type\")!!");
                    bx7 a = this.this$0.c.a(str, ((Number) argument2).intValue(), this.this$0.b(this.$call), this.this$0.a(this.$call));
                    if (a != null) {
                        this.$resultHandler.a(dx7.a.b(v97.a(a)));
                        return;
                    }
                    this.$resultHandler.a(null);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
            this.$resultHandler = nx7;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                ee7.a(argument, "call.argument<String>(\"id\")!!");
                this.$resultHandler.a(this.this$0.c.a((String) argument));
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ vw7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(vw7 vw7, MethodCall methodCall) {
            super(0);
            this.this$0 = vw7;
            this.$call = methodCall;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            if (ee7.a((Object) ((Boolean) this.$call.argument("notify")), (Object) true)) {
                this.this$0.b.b();
            } else {
                this.this$0.b.c();
            }
        }
    }

    @DexIgnore
    public vw7(PluginRegistry.Registrar registrar) {
        ee7.b(registrar, "registrar");
        this.d = registrar;
        this.d.addRequestPermissionsResultListener(new a(this));
        this.a.a(new b());
        Context context = this.d.context();
        ee7.a((Object) context, "registrar.context()");
        Context applicationContext = context.getApplicationContext();
        ee7.a((Object) applicationContext, "registrar.context().applicationContext");
        this.c = new tw7(applicationContext);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0068, code lost:
        if (r7.equals("getOriginBytes") != false) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ba, code lost:
        if (android.os.Build.VERSION.SDK_INT >= 29) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c8, code lost:
        if (r7.equals("getLatLngAndroidQ") != false) goto L_0x00de;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e1  */
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMethodCall(io.flutter.plugin.common.MethodCall r6, io.flutter.plugin.common.MethodChannel.Result r7) {
        /*
            r5 = this;
            java.lang.String r0 = "call"
            com.fossil.ee7.b(r6, r0)
            java.lang.String r0 = "result"
            com.fossil.ee7.b(r7, r0)
            com.fossil.nx7 r0 = new com.fossil.nx7
            r0.<init>(r7)
            java.lang.String r7 = r6.method
            r1 = 29
            r2 = 0
            r3 = 1
            if (r7 != 0) goto L_0x0019
            goto L_0x00dd
        L_0x0019:
            int r4 = r7.hashCode()
            switch(r4) {
                case -1914421335: goto L_0x00cb;
                case -1283288098: goto L_0x00c2;
                case -886445535: goto L_0x009b;
                case -582375106: goto L_0x0086;
                case 107332: goto L_0x006c;
                case 1063055279: goto L_0x0062;
                case 1252395988: goto L_0x0053;
                case 1469201411: goto L_0x0037;
                case 1789114534: goto L_0x0022;
                default: goto L_0x0020;
            }
        L_0x0020:
            goto L_0x00dd
        L_0x0022:
            java.lang.String r4 = "openSetting"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            com.fossil.hx7 r7 = r5.a
            io.flutter.plugin.common.PluginRegistry$Registrar r4 = r5.d
            android.app.Activity r4 = r4.activity()
            r7.a(r4)
            goto L_0x00dc
        L_0x0037:
            java.lang.String r4 = "cacheOriginBytes"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            java.lang.Object r7 = r6.arguments()
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            com.fossil.ee7.a(r7, r4)
            boolean r4 = r7.booleanValue()
            com.fossil.vw7.f = r4
            r0.a(r7)
            goto L_0x00dc
        L_0x0053:
            java.lang.String r4 = "releaseMemCache"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            com.fossil.tw7 r7 = r5.c
            r7.a()
            goto L_0x00dc
        L_0x0062:
            java.lang.String r4 = "getOriginBytes"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            goto L_0x00de
        L_0x006c:
            java.lang.String r4 = "log"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            java.lang.Object r7 = r6.arguments()
            java.lang.String r4 = "call.arguments()"
            com.fossil.ee7.a(r7, r4)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            com.fossil.mx7.a = r7
            goto L_0x00dc
        L_0x0086:
            java.lang.String r4 = "forceOldApi"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            com.fossil.tw7 r7 = r5.c
            r7.a(r3)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)
            r0.a(r7)
            goto L_0x00dc
        L_0x009b:
            java.lang.String r4 = "getFullFile"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            java.lang.String r7 = "isOrigin"
            java.lang.Object r7 = r6.argument(r7)
            if (r7 == 0) goto L_0x00bd
            java.lang.String r4 = "call.argument<Boolean>(\"isOrigin\")!!"
            com.fossil.ee7.a(r7, r4)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x00dd
            int r7 = android.os.Build.VERSION.SDK_INT
            if (r7 < r1) goto L_0x00dd
            goto L_0x00de
        L_0x00bd:
            com.fossil.ee7.a()
            r6 = 0
            throw r6
        L_0x00c2:
            java.lang.String r4 = "getLatLngAndroidQ"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            goto L_0x00de
        L_0x00cb:
            java.lang.String r4 = "systemVersion"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00dd
            int r7 = android.os.Build.VERSION.SDK_INT
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r0.a(r7)
        L_0x00dc:
            r2 = 1
        L_0x00dd:
            r3 = 0
        L_0x00de:
            if (r2 == 0) goto L_0x00e1
            return
        L_0x00e1:
            com.fossil.hx7 r7 = r5.a
            io.flutter.plugin.common.PluginRegistry$Registrar r2 = r5.d
            android.app.Activity r2 = r2.activity()
            r7.a(r2)
            com.fossil.vw7$p r2 = new com.fossil.vw7$p
            r2.<init>(r5, r6, r0)
            r7.a(r2)
            java.lang.String r6 = "android.permission.READ_EXTERNAL_STORAGE"
            java.lang.String r0 = "android.permission.WRITE_EXTERNAL_STORAGE"
            java.lang.String[] r6 = new java.lang.String[]{r6, r0}
            java.util.ArrayList r6 = com.fossil.w97.a(r6)
            if (r3 == 0) goto L_0x010b
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r1) goto L_0x010b
            java.lang.String r0 = "android.permission.ACCESS_MEDIA_LOCATION"
            r6.add(r0)
        L_0x010b:
            io.flutter.plugin.common.PluginRegistry$Registrar r0 = r5.d
            android.app.Activity r0 = r0.activity()
            r1 = 3001(0xbb9, float:4.205E-42)
            r7.a(r0, r1, r6)
            return
            switch-data {-1914421335->0x00cb, -1283288098->0x00c2, -886445535->0x009b, -582375106->0x0086, 107332->0x006c, 1063055279->0x0062, 1252395988->0x0053, 1469201411->0x0037, 1789114534->0x0022, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vw7.onMethodCall(io.flutter.plugin.common.MethodCall, io.flutter.plugin.common.MethodChannel$Result):void");
    }

    @DexIgnore
    public final long b(MethodCall methodCall) {
        ee7.b(methodCall, "$this$getTimeStamp");
        Object argument = methodCall.argument("timestamp");
        if (argument != null) {
            return ((Number) argument).longValue();
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final String b(MethodCall methodCall, String str) {
        ee7.b(methodCall, "$this$getString");
        ee7.b(str, "key");
        Object argument = methodCall.argument(str);
        if (argument != null) {
            return (String) argument;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(nx7 nx7) {
        nx7.a("Request for permission failed.", "User denied permission.", null);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements gx7 {
        @DexIgnore
        public /* final */ /* synthetic */ vw7 a;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 c;

        @DexIgnore
        public p(vw7 vw7, MethodCall methodCall, nx7 nx7) {
            this.a = vw7;
            this.b = methodCall;
            this.c = nx7;
        }

        @DexIgnore
        @Override // com.fossil.gx7
        public void a(List<String> list, List<String> list2) {
            ee7.b(list, "deniedPermissions");
            ee7.b(list2, "grantedPermissions");
            mx7.a("onDenied call.method = " + this.b.method);
            if (ee7.a((Object) this.b.method, (Object) "requestPermission")) {
                this.c.a(0);
            } else if (list2.containsAll(w97.a((Object[]) new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}))) {
                this.a.a(this.b, this.c, false);
            } else {
                this.a.a(this.c);
            }
        }

        @DexIgnore
        @Override // com.fossil.gx7
        public void a() {
            this.a.a(this.b, this.c, true);
        }
    }

    @DexIgnore
    public final void a(MethodCall methodCall, nx7 nx7, boolean z) {
        mx7.a("onGranted call.method = " + methodCall.method);
        String str = methodCall.method;
        if (str != null) {
            switch (str.hashCode()) {
                case -1283288098:
                    if (str.equals("getLatLngAndroidQ")) {
                        g.a(new n(this, methodCall, nx7));
                        return;
                    }
                    break;
                case -1039689911:
                    if (str.equals("notify")) {
                        g.a(new o(this, methodCall));
                        return;
                    }
                    break;
                case -886445535:
                    if (str.equals("getFullFile")) {
                        g.a(new k(this, methodCall, z, nx7));
                        return;
                    }
                    break;
                case -151967598:
                    if (str.equals("fetchPathProperties")) {
                        g.a(new m(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 163601886:
                    if (str.equals("saveImage")) {
                        g.a(new e(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 175491326:
                    if (str.equals("saveVideo")) {
                        g.a(new f(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 594039295:
                    if (str.equals("getAssetListWithRange")) {
                        g.a(new i(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 746581438:
                    if (str.equals("requestPermission")) {
                        nx7.a(1);
                        return;
                    }
                    break;
                case 857200492:
                    if (str.equals("assetExists")) {
                        g.a(new j(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 1063055279:
                    if (str.equals("getOriginBytes")) {
                        g.a(new l(this, methodCall, z, nx7));
                        return;
                    }
                    break;
                case 1150344167:
                    if (str.equals("deleteWithIds")) {
                        g.a(new d(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 1505159642:
                    if (str.equals("getGalleryList")) {
                        if (Build.VERSION.SDK_INT >= 29) {
                            this.b.a(true);
                        }
                        g.a(new g(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 1642188493:
                    if (str.equals("getAssetWithGalleryId")) {
                        g.a(new h(this, methodCall, nx7));
                        return;
                    }
                    break;
                case 1966168096:
                    if (str.equals("getThumb")) {
                        Object argument = methodCall.argument("id");
                        if (argument != null) {
                            ee7.a(argument, "call.argument<String>(\"id\")!!");
                            String str2 = (String) argument;
                            Object argument2 = methodCall.argument("width");
                            if (argument2 != null) {
                                ee7.a(argument2, "call.argument<Int>(\"width\")!!");
                                int intValue = ((Number) argument2).intValue();
                                Object argument3 = methodCall.argument("height");
                                if (argument3 != null) {
                                    ee7.a(argument3, "call.argument<Int>(\"height\")!!");
                                    int intValue2 = ((Number) argument3).intValue();
                                    Object argument4 = methodCall.argument("format");
                                    if (argument4 != null) {
                                        ee7.a(argument4, "call.argument<Int>(\"format\")!!");
                                        this.c.a(str2, intValue, intValue2, ((Number) argument4).intValue(), nx7);
                                        return;
                                    }
                                    ee7.a();
                                    throw null;
                                }
                                ee7.a();
                                throw null;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    break;
            }
        }
        nx7.b();
    }

    @DexIgnore
    public final int a(MethodCall methodCall, String str) {
        ee7.b(methodCall, "$this$getInt");
        ee7.b(str, "key");
        Object argument = methodCall.argument(str);
        if (argument != null) {
            return ((Number) argument).intValue();
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ax7 a(MethodCall methodCall) {
        ee7.b(methodCall, "$this$getOption");
        Object argument = methodCall.argument("option");
        if (argument != null) {
            ee7.a(argument, "argument<Map<*, *>>(\"option\")!!");
            return dx7.a.a((Map) argument);
        }
        ee7.a();
        throw null;
    }
}
