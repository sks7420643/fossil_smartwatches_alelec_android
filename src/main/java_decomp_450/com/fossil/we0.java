package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ne0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<we0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public we0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(rb0.class.getClassLoader());
            if (readParcelable != null) {
                return new we0((rb0) readParcelable, (df0) parcel.readParcelable(df0.class.getClassLoader()), (ne0) parcel.readParcelable(ne0.class.getClassLoader()));
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public we0[] newArray(int i) {
            return new we0[i];
        }
    }

    @DexIgnore
    public we0(rb0 rb0, ne0 ne0) {
        super(rb0, null);
        this.c = ne0;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.c != null) {
                jSONObject.put("buddyChallengeApp._.config.sync", this.c.b());
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        JSONObject jSONObject;
        JSONObject b = super.b();
        ne0 ne0 = this.c;
        if (ne0 == null || (jSONObject = ne0.a()) == null) {
            jSONObject = new JSONObject();
        }
        return yz0.a(b, jSONObject);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(we0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.c, ((we0) obj).c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppGetChallengeInfoData");
    }

    @DexIgnore
    public final ne0 getBuddyChallenge() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        ne0 ne0 = this.c;
        return hashCode + (ne0 != null ? ne0.hashCode() : 0);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public we0(rb0 rb0, df0 df0, ne0 ne0) {
        super(rb0, df0);
        this.c = ne0;
    }
}
