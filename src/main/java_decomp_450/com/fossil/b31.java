package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.BinaryFile;
import com.fossil.fitness.FitnessAlgorithm;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GpsLogFileManager;
import com.fossil.fitness.Result;
import com.fossil.fitness.StatusCode;
import com.fossil.fitness.SyncMode;
import com.fossil.fitness.UserProfile;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b31 extends zk1 {
    @DexIgnore
    public /* final */ boolean U; // = true;
    @DexIgnore
    public FitnessData[] V;
    @DexIgnore
    public StatusCode W;
    @DexIgnore
    public /* final */ HashMap<String, Long> X; // = new HashMap<>();
    @DexIgnore
    public long Y; // = -1;
    @DexIgnore
    public /* final */ c80 Z;

    @DexIgnore
    public b31(ri1 ri1, en0 en0, c80 c80, HashMap<xf0, Object> hashMap, String str) {
        super(ri1, en0, wm0.m, gq0.b.a(ri1.u, pb1.ACTIVITY_FILE), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
        this.Z = c80;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.U;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public Object d() {
        FitnessData[] fitnessDataArr = this.V;
        return fitnessDataArr != null ? fitnessDataArr : new FitnessData[0];
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public void h() {
        if (yp0.f.b(((zk0) this).x.a())) {
            zk0.a(this, new zm1(((zk0) this).w, ((zk0) this).x, this.Z, false, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 32), new fw0(this), new xx0(this), new pz0(this), (gd7) null, (gd7) null, 48, (Object) null);
        } else {
            super.h();
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1, com.fossil.v61
    public JSONObject i() {
        JSONObject put = super.i().put(o80.BIOMETRIC_PROFILE.b(), this.Z.d());
        ee7.a((Object) put, "super.optionDescription(\u2026ofile.valueDescription())");
        return put;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002f, code lost:
        if (r2 != null) goto L_0x0034;
     */
    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject k() {
        /*
            r4 = this;
            org.json.JSONObject r0 = super.k()
            com.fossil.r51 r1 = com.fossil.r51.U
            com.fossil.fitness.FitnessData[] r2 = r4.V
            if (r2 == 0) goto L_0x000f
            org.json.JSONArray r2 = com.fossil.yz0.a(r2)
            goto L_0x0010
        L_0x000f:
            r2 = 0
        L_0x0010:
            org.json.JSONObject r0 = com.fossil.yz0.a(r0, r1, r2)
            com.fossil.r51 r1 = com.fossil.r51.l2
            com.fossil.fitness.StatusCode r2 = r4.W
            if (r2 == 0) goto L_0x0032
            java.lang.String r2 = r2.name()
            if (r2 == 0) goto L_0x0032
            com.fossil.b21 r3 = com.fossil.b21.x
            java.util.Locale r3 = r3.e()
            java.lang.String r2 = r2.toLowerCase(r3)
            java.lang.String r3 = "(this as java.lang.String).toLowerCase(locale)"
            com.fossil.ee7.a(r2, r3)
            if (r2 == 0) goto L_0x0032
            goto L_0x0034
        L_0x0032:
            java.lang.Object r2 = org.json.JSONObject.NULL
        L_0x0034:
            org.json.JSONObject r0 = com.fossil.yz0.a(r0, r1, r2)
            com.fossil.r51 r1 = com.fossil.r51.O5
            java.util.HashMap<java.lang.String, java.lang.Long> r2 = r4.X
            int r2 = r2.size()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            org.json.JSONObject r0 = com.fossil.yz0.a(r0, r1, r2)
            com.fossil.r51 r1 = com.fossil.r51.P5
            long r2 = r4.Y
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            org.json.JSONObject r0 = com.fossil.yz0.a(r0, r1, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.b31.k():org.json.JSONObject");
    }

    @DexIgnore
    @Override // com.fossil.zk1
    public void a(ArrayList<bi1> arrayList) {
        ik7 unused = xh7.b(bk7.a, null, null, new qs0(this, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void b(ArrayList<bi1> arrayList) {
        ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
        for (T t : arrayList) {
            arrayList2.add(new BinaryFile(((bi1) t).e, (int) (((bi1) t).h / ((long) 1000))));
        }
        ArrayList<BinaryFile> arrayList3 = new ArrayList<>(arrayList2);
        FitnessAlgorithm create = FitnessAlgorithm.create(new UserProfile((short) this.Z.getAge(), this.Z.getGender().b(), ((float) this.Z.getHeightInCentimeter()) / 100.0f, (float) this.Z.getWeightInKilogram()));
        ee7.a((Object) create, "FitnessAlgorithm.create(userProfile)");
        ArrayList<String> list = GpsLogFileManager.defaultManager().list();
        ee7.a((Object) list, "GpsLogFileManager.defaultManager().list()");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String str = list.get(i);
            File file = new File(str);
            HashMap<String, Long> hashMap = this.X;
            ee7.a((Object) str, "filePath");
            hashMap.put(str, Long.valueOf(file.length()));
            ((zk0) this).o.post(new j11(this, file, i, list));
        }
        long currentTimeMillis = System.currentTimeMillis();
        le0 le0 = le0.DEBUG;
        arrayList3.size();
        SyncMode.FULL_SYNC.name();
        Result parseWithBinaries = create.parseWithBinaries(arrayList3, SyncMode.FULL_SYNC);
        ee7.a((Object) parseWithBinaries, "fitnessAlgorithm.parseWi\u2026List, SyncMode.FULL_SYNC)");
        this.Y = System.currentTimeMillis() - currentTimeMillis;
        this.W = parseWithBinaries.getStatus();
        if (parseWithBinaries.getStatus() == StatusCode.SUCCESS) {
            ArrayList<FitnessData> fitnessData = parseWithBinaries.getFitnessData();
            ee7.a((Object) fitnessData, "parsedResult.fitnessData");
            Object[] array = fitnessData.toArray(new FitnessData[0]);
            if (array != null) {
                this.V = (FitnessData[]) array;
                le0 le02 = le0.DEBUG;
                FitnessData[] fitnessDataArr = this.V;
                if (fitnessDataArr != null) {
                    yz0.a(fitnessDataArr).toString(2);
                    return;
                }
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        le0 le03 = le0.DEBUG;
        parseWithBinaries.getStatus();
    }
}
