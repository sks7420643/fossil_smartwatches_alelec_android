package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r23 implements tr2<q23> {
    @DexIgnore
    public static r23 b; // = new r23();
    @DexIgnore
    public /* final */ tr2<q23> a;

    @DexIgnore
    public r23(tr2<q23> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((q23) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((q23) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((q23) b.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ q23 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public r23() {
        this(sr2.a(new t23()));
    }
}
