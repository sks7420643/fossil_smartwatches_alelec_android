package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tb extends nb {
    @DexIgnore
    public static /* final */ SparseIntArray a; // = new SparseIntArray(0);

    @DexIgnore
    @Override // com.fossil.nb
    public ViewDataBinding a(pb pbVar, View view, int i) {
        if (a.get(i) <= 0 || view.getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    @Override // com.fossil.nb
    public ViewDataBinding a(pb pbVar, View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    @Override // com.fossil.nb
    public List<nb> a() {
        return new ArrayList(0);
    }
}
