package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja3 implements Parcelable.Creator<v93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ v93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        LatLngBounds latLngBounds = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                latLng = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
            } else if (a2 == 3) {
                latLng2 = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
            } else if (a2 == 4) {
                latLng3 = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
            } else if (a2 == 5) {
                latLng4 = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
            } else if (a2 != 6) {
                j72.v(parcel, a);
            } else {
                latLngBounds = (LatLngBounds) j72.a(parcel, a, LatLngBounds.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new v93(latLng, latLng2, latLng3, latLng4, latLngBounds);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ v93[] newArray(int i) {
        return new v93[i];
    }
}
