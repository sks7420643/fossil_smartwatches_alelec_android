package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo4 {
    @DexIgnore
    @te4("step")
    public Integer a;

    @DexIgnore
    public lo4() {
        this(null, 1, null);
    }

    @DexIgnore
    public lo4(Integer num) {
        this.a = num;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof lo4) && ee7.a(this.a, ((lo4) obj).a);
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.a;
        if (num != null) {
            return num.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "StepResult(step=" + this.a + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ lo4(Integer num, int i, zd7 zd7) {
        this((i & 1) != 0 ? null : num);
    }
}
