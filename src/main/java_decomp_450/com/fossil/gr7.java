package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr7 implements sr7 {
    @DexIgnore
    public int a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ ar7 c;
    @DexIgnore
    public /* final */ Inflater d;

    @DexIgnore
    public gr7(ar7 ar7, Inflater inflater) {
        ee7.b(ar7, "source");
        ee7.b(inflater, "inflater");
        this.c = ar7;
        this.d = inflater;
    }

    @DexIgnore
    public final boolean a() throws IOException {
        if (!this.d.needsInput()) {
            return false;
        }
        b();
        if (!(this.d.getRemaining() == 0)) {
            throw new IllegalStateException("?".toString());
        } else if (this.c.h()) {
            return true;
        } else {
            nr7 nr7 = this.c.getBuffer().a;
            if (nr7 != null) {
                int i = nr7.c;
                int i2 = nr7.b;
                int i3 = i - i2;
                this.a = i3;
                this.d.setInput(nr7.a, i2, i3);
                return false;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public long b(yq7 yq7, long j) throws IOException {
        nr7 b2;
        ee7.b(yq7, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!(!this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (i == 0) {
            return 0;
        } else {
            while (true) {
                boolean a2 = a();
                try {
                    b2 = yq7.b(1);
                    int inflate = this.d.inflate(b2.a, b2.c, (int) Math.min(j, (long) (8192 - b2.c)));
                    if (inflate > 0) {
                        b2.c += inflate;
                        long j2 = (long) inflate;
                        yq7.j(yq7.x() + j2);
                        return j2;
                    } else if (this.d.finished()) {
                        break;
                    } else if (this.d.needsDictionary()) {
                        break;
                    } else if (a2) {
                        throw new EOFException("source exhausted prematurely");
                    }
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            }
            b();
            if (b2.b != b2.c) {
                return -1;
            }
            yq7.a = b2.b();
            or7.c.a(b2);
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.b) {
            this.d.end();
            this.b = true;
            this.c.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public tr7 d() {
        return this.c.d();
    }

    @DexIgnore
    public final void b() {
        int i = this.a;
        if (i != 0) {
            int remaining = i - this.d.getRemaining();
            this.a -= remaining;
            this.c.skip((long) remaining);
        }
    }
}
