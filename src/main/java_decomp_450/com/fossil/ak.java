package com.fossil;

import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ak {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public Runnable b;

    @DexIgnore
    public void a() {
        Runnable runnable;
        if (a(this.a) == this && (runnable = this.b) != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, ak akVar) {
        viewGroup.setTag(yj.transition_current_scene, akVar);
    }

    @DexIgnore
    public static ak a(ViewGroup viewGroup) {
        return (ak) viewGroup.getTag(yj.transition_current_scene);
    }
}
