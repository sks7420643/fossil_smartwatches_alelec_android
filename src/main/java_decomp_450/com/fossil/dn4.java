package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public dn4(String str, Object obj, boolean z) {
        ee7.b(str, "name");
        ee7.b(obj, "data");
        this.a = str;
        this.b = obj;
        this.c = z;
    }

    @DexIgnore
    public final Object a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dn4)) {
            return false;
        }
        dn4 dn4 = (dn4) obj;
        return ee7.a(this.a, dn4.a) && ee7.a(this.b, dn4.b) && this.c == dn4.c;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Object obj = this.b;
        if (obj != null) {
            i = obj.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public String toString() {
        return "BottomDialogModel(name=" + this.a + ", data=" + this.b + ", isChecked=" + this.c + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ dn4(String str, Object obj, boolean z, int i, zd7 zd7) {
        this(str, obj, (i & 4) != 0 ? false : z);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.c = z;
    }
}
