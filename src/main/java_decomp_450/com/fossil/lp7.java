package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp7 {
    @DexIgnore
    public static boolean a(String str) {
        return str.equals("POST") || str.equals("PATCH") || str.equals("PUT") || str.equals("DELETE") || str.equals("MOVE");
    }

    @DexIgnore
    public static boolean b(String str) {
        return !str.equals("GET") && !str.equals("HEAD");
    }

    @DexIgnore
    public static boolean c(String str) {
        return !str.equals("PROPFIND");
    }

    @DexIgnore
    public static boolean d(String str) {
        return str.equals("PROPFIND");
    }

    @DexIgnore
    public static boolean e(String str) {
        return str.equals("POST") || str.equals("PUT") || str.equals("PATCH") || str.equals("PROPPATCH") || str.equals("REPORT");
    }
}
