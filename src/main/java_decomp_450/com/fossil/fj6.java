package com.fossil;

import com.fossil.fl4;
import com.fossil.fn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj6 extends aj6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ bj6 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ fn5 g;
    @DexIgnore
    public /* final */ qd5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendFeedbackSuccessfullyEvent$1", f = "HelpPresenter.kt", l = {78}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fj6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendFeedbackSuccessfullyEvent$1$skuModel$1", f = "HelpPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super SKUModel> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fj6 fj6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fj6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                String str = "";
                if (sku == null) {
                    sku = str;
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName != null) {
                    str = deviceName;
                }
                hashMap.put("Device_Name", str);
                this.this$0.h.a("feedback_submit", hashMap);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendOpenFeedbackEvent$1", f = "HelpPresenter.kt", l = {92}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fj6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "HelpPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super SKUModel> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fj6 fj6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fj6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                String str = "";
                if (sku == null) {
                    sku = str;
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName != null) {
                    str = deviceName;
                }
                hashMap.put("Device_Name", str);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.h.a("feedback_open", hashMap);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<fn5.d, fn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ fj6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ fn5.d $responseValue;

            @DexIgnore
            public a(fn5.d dVar) {
                this.$responseValue = dVar;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String k = fj6.i;
                local.d(k, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                FLogger.INSTANCE.getLocal().d(fj6.i, "getRequestSubject");
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                FLogger.INSTANCE.getLocal().d(fj6.i, "getTags");
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(fj6 fj6) {
            this.a = fj6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fn5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(fj6.i, "sendFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
            this.a.e.a((ZendeskFeedbackConfiguration) new a(dVar));
        }

        @DexIgnore
        public void a(fn5.b bVar) {
            ee7.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(fj6.i, "sendFeedback onError");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = fj6.class.getSimpleName();
        ee7.a((Object) simpleName, "HelpPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public fj6(bj6 bj6, DeviceRepository deviceRepository, fn5 fn5, qd5 qd5) {
        ee7.b(bj6, "mView");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(fn5, "mGetZendeskInformation");
        ee7.b(qd5, "mAnalyticsHelper");
        this.e = bj6;
        this.f = deviceRepository;
        this.g = fn5;
        this.h = qd5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.aj6
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.aj6
    public void i() {
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public void j() {
        this.e.a(this);
    }

    @DexIgnore
    @Override // com.fossil.aj6
    public void a(String str) {
        ee7.b(str, "subject");
        this.g.a(new fn5.c(str), new d(this));
    }
}
