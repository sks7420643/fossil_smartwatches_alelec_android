package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ry4 extends qy4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131361979, 1);
        z.put(2131362234, 2);
        z.put(2131361972, 3);
        z.put(2131361961, 4);
        z.put(2131363001, 5);
        z.put(2131363306, 6);
    }
    */

    @DexIgnore
    public ry4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 7, y, z));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public ry4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[4], (ImageView) objArr[3], (ConstraintLayout) objArr[1], (FlexibleEditText) objArr[2], (ConstraintLayout) objArr[0], (RecyclerViewEmptySupport) objArr[5], (FlexibleTextView) objArr[6]);
        this.x = -1;
        ((qy4) this).u.setTag(null);
        a(view);
        f();
    }
}
