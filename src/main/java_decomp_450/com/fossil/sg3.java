package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sg3 implements Runnable {
    @DexIgnore
    public /* final */ pg3 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ Map<String, List<String>> f;

    @DexIgnore
    public sg3(String str, pg3 pg3, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        a72.a(pg3);
        this.a = pg3;
        this.b = i;
        this.c = th;
        this.d = bArr;
        this.e = str;
        this.f = map;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.e, this.b, this.c, this.d, this.f);
    }
}
