package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface o40 {

    @DexIgnore
    public enum a {
        RUNNING(false),
        PAUSED(false),
        CLEARED(false),
        SUCCESS(true),
        FAILED(true);
        
        @DexIgnore
        public /* final */ boolean isComplete;

        @DexIgnore
        public a(boolean z) {
            this.isComplete = z;
        }

        @DexIgnore
        public boolean isComplete() {
            return this.isComplete;
        }
    }

    @DexIgnore
    void a(n40 n40);

    @DexIgnore
    boolean a();

    @DexIgnore
    o40 b();

    @DexIgnore
    boolean c(n40 n40);

    @DexIgnore
    boolean d(n40 n40);

    @DexIgnore
    void e(n40 n40);

    @DexIgnore
    boolean f(n40 n40);
}
