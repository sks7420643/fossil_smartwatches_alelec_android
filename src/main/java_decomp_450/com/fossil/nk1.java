package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nk1 implements Parcelable.Creator<lm1> {
    @DexIgnore
    public /* synthetic */ nk1(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public lm1 createFromParcel(Parcel parcel) {
        return new lm1(parcel, (zd7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public lm1[] newArray(int i) {
        return new lm1[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public lm1 m42createFromParcel(Parcel parcel) {
        return new lm1(parcel, (zd7) null);
    }
}
