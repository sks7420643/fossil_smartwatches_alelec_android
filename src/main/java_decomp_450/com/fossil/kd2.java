package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kd2> CREATOR; // = new jd2();
    @DexIgnore
    public /* final */ zi2 a;

    @DexIgnore
    public kd2(IBinder iBinder) {
        this.a = bj2.a(iBinder);
    }

    @DexIgnore
    public final String toString() {
        return String.format("DisableFitRequest", new Object[0]);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a.asBinder(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public kd2(zi2 zi2) {
        this.a = zi2;
    }
}
