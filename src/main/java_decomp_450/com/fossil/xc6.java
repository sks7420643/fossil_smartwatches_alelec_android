package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.view.chart.WeekHeartRateChart;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc6 extends go5 implements wc6 {
    @DexIgnore
    public qw6<c25> f;
    @DexIgnore
    public vc6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HeartRateOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        MFLogger.d("HeartRateOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewWeekFragment", "onCreateView");
        qw6<c25> qw6 = new qw6<>(this, (c25) qb.a(layoutInflater, 2131558566, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            c25 a2 = qw6.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        MFLogger.d("HeartRateOverviewWeekFragment", "onResume");
        vc6 vc6 = this.g;
        if (vc6 != null) {
            vc6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        MFLogger.d("HeartRateOverviewWeekFragment", "onStop");
        vc6 vc6 = this.g;
        if (vc6 != null) {
            vc6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(vc6 vc6) {
        ee7.b(vc6, "presenter");
        this.g = vc6;
    }

    @DexIgnore
    @Override // com.fossil.wc6
    public void a(List<Integer> list, List<String> list2) {
        WeekHeartRateChart weekHeartRateChart;
        ee7.b(list, "data");
        ee7.b(list2, "listWeekDays");
        qw6<c25> qw6 = this.f;
        if (qw6 != null) {
            c25 a2 = qw6.a();
            if (a2 != null && (weekHeartRateChart = a2.q) != null) {
                weekHeartRateChart.setListWeekDays(list2);
                weekHeartRateChart.a(list);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
