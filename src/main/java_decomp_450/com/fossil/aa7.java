package com.fossil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aa7 extends z97 {
    @DexIgnore
    public static final <T> void a(List<T> list, Comparator<? super T> comparator) {
        ee7.b(list, "$this$sortWith");
        ee7.b(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> void c(List<T> list) {
        ee7.b(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }
}
