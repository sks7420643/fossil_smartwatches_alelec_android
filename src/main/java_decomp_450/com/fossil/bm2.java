package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bm2> CREATOR; // = new cm2();
    @DexIgnore
    public static /* final */ List<i62> h; // = Collections.emptyList();
    @DexIgnore
    public LocationRequest a;
    @DexIgnore
    public List<i62> b;
    @DexIgnore
    public String c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public String g;

    @DexIgnore
    public bm2(LocationRequest locationRequest, List<i62> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.a = locationRequest;
        this.b = list;
        this.c = str;
        this.d = z;
        this.e = z2;
        this.f = z3;
        this.g = str2;
    }

    @DexIgnore
    @Deprecated
    public static bm2 a(LocationRequest locationRequest) {
        return new bm2(locationRequest, h, null, false, false, false, null);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof bm2)) {
            return false;
        }
        bm2 bm2 = (bm2) obj;
        return y62.a(this.a, bm2.a) && y62.a(this.b, bm2.b) && y62.a(this.c, bm2.c) && this.d == bm2.d && this.e == bm2.e && this.f == bm2.f && y62.a(this.g, bm2.g);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        if (this.c != null) {
            sb.append(" tag=");
            sb.append(this.c);
        }
        if (this.g != null) {
            sb.append(" moduleId=");
            sb.append(this.g);
        }
        sb.append(" hideAppOps=");
        sb.append(this.d);
        sb.append(" clients=");
        sb.append(this.b);
        sb.append(" forceCoarseLocation=");
        sb.append(this.e);
        if (this.f) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) this.a, i, false);
        k72.c(parcel, 5, this.b, false);
        k72.a(parcel, 6, this.c, false);
        k72.a(parcel, 7, this.d);
        k72.a(parcel, 8, this.e);
        k72.a(parcel, 9, this.f);
        k72.a(parcel, 10, this.g, false);
        k72.a(parcel, a2);
    }
}
