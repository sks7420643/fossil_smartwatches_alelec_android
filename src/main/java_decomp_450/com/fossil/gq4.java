package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq4 extends he {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public Timer f;
    @DexIgnore
    public /* final */ ro4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {94}, m = "initPlayers")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gq4 gq4, fb7 fb7) {
            super(fb7);
            this.this$0 = gq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$initPlayers$historyChallenge$1", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super yn4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gq4 gq4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$challengeId, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super yn4> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return this.this$0.g.g(this.$challengeId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayers$1", f = "BCLeaderBoardViewModel.kt", l = {51, 56, 58}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $status;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gq4 gq4, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$challengeId = str;
            this.$status = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$challengeId, this.$status, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0092 A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 3
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x0032
                if (r1 == r4) goto L_0x002a
                if (r1 == r3) goto L_0x0022
                if (r1 != r2) goto L_0x001a
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x0093
            L_0x001a:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x0022:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0076
            L_0x002a:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0054
            L_0x0032:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r7 = r6.p$
                com.fossil.gq4 r1 = r6.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.c
                java.lang.Boolean r5 = com.fossil.pb7.a(r4)
                r1.a(r5)
                com.fossil.gq4 r1 = r6.this$0
                java.lang.String r5 = r6.$challengeId
                r6.L$0 = r7
                r6.label = r4
                java.lang.Object r1 = r1.a(r5, r6)
                if (r1 != r0) goto L_0x0053
                return r0
            L_0x0053:
                r1 = r7
            L_0x0054:
                java.lang.String r7 = r6.$status
                java.lang.String r4 = "completed"
                boolean r7 = com.fossil.ee7.a(r7, r4)
                if (r7 == 0) goto L_0x0067
                com.fossil.gq4 r7 = r6.this$0
                androidx.lifecycle.MutableLiveData r7 = r7.e
                r7.a(r4)
            L_0x0067:
                com.fossil.gq4 r7 = r6.this$0
                java.lang.String r4 = r6.$challengeId
                r6.L$0 = r1
                r6.label = r3
                java.lang.Object r7 = r7.b(r4, r6)
                if (r7 != r0) goto L_0x0076
                return r0
            L_0x0076:
                com.fossil.gq4 r7 = r6.this$0
                androidx.lifecycle.MutableLiveData r7 = r7.c
                r3 = 0
                java.lang.Boolean r3 = com.fossil.pb7.a(r3)
                r7.a(r3)
                com.fossil.gq4 r7 = r6.this$0
                java.lang.String r3 = r6.$challengeId
                r6.L$0 = r1
                r6.label = r2
                java.lang.Object r7 = r7.c(r3, r6)
                if (r7 != r0) goto L_0x0093
                return r0
            L_0x0093:
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gq4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {103, 116, 122}, m = "loadPlayersFromServer")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(gq4 gq4, fb7 fb7) {
            super(fb7);
            this.this$0 = gq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayersFromServer$2", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $players;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(gq4 gq4, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$players = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$players, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.g.a(this.$players);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayersFromServer$result$1", f = "BCLeaderBoardViewModel.kt", l = {104}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jn4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(gq4 gq4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$challengeId, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jn4>>> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                this.L$0 = this.p$;
                this.label = 1;
                obj = ro4.a(this.this$0.g, this.$challengeId, new String[]{"running", "completed", "left_after_start"}, 0, this, 4, null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$refresh$1", f = "BCLeaderBoardViewModel.kt", l = {65}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(gq4 gq4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$challengeId, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                gq4 gq4 = this.this$0;
                String str = this.$challengeId;
                this.L$0 = yi7;
                this.label = 1;
                if (gq4.b(str, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.c.a(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$updateHistoryChallenge$2", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ List $players;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(gq4 gq4, String str, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$challengeId = str;
            this.$players = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$challengeId, this.$players, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                yn4 g = this.this$0.g.g(this.$challengeId);
                if (g != null) {
                    List list = this.$players;
                    if (!(list == null || list.isEmpty())) {
                        this.this$0.g.a(nt4.a(g, this.$players));
                    }
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ gq4 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, i iVar) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    FLogger.INSTANCE.getLocal().e(this.this$0.a.a, "waitingChallengeEnd - end challenge");
                    Object a = this.this$0.a.b.a();
                    if (!(a instanceof List)) {
                        a = null;
                    }
                    List<jn4> list = (List) a;
                    if (list != null) {
                        this.this$0.a.g.a(list);
                    }
                    this.this$0.a.e.a((Object) "completed");
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public i(gq4 gq4) {
            this.a = gq4;
        }

        @DexIgnore
        public void run() {
            ik7 unused = xh7.b(ie.a(this.a), qj7.b(), null, new a(null, this), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {72}, m = "waitingChallengeEnd")
    public static final class j extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(gq4 gq4, fb7 fb7) {
            super(fb7);
            this.this$0 = gq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$waitingChallengeEnd$challenge$1", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class k extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(gq4 gq4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gq4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            k kVar = new k(this.this$0, this.$challengeId, fb7);
            kVar.p$ = (yi7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
            return ((k) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return this.this$0.g.a(this.$challengeId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public gq4(ro4 ro4) {
        ee7.b(ro4, "challengeRepository");
        this.g = ro4;
        String simpleName = gq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCLeaderBoardViewModel::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<String> a() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> b() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<List<Object>> d() {
        return this.b;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (str != null) {
            ik7 unused = xh7.b(ie.a(this), null, null, new c(this, str, str2, null), 3, null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0157 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(java.lang.String r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.fossil.gq4.d
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.gq4$d r0 = (com.fossil.gq4.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gq4$d r0 = new com.fossil.gq4$d
            r0.<init>(r13, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "completed"
            r4 = 3
            r5 = 2
            r6 = 0
            r7 = 1
            if (r2 == 0) goto L_0x0074
            if (r2 == r7) goto L_0x0068
            if (r2 == r5) goto L_0x004f
            if (r2 != r4) goto L_0x0047
            java.lang.Object r14 = r0.L$4
            com.fossil.jn4 r14 = (com.fossil.jn4) r14
            java.lang.Object r14 = r0.L$3
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r14 = r0.L$2
            com.fossil.ko4 r14 = (com.fossil.ko4) r14
            java.lang.Object r14 = r0.L$1
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r14 = r0.L$0
            com.fossil.gq4 r14 = (com.fossil.gq4) r14
            com.fossil.t87.a(r15)
            goto L_0x0184
        L_0x0047:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x004f:
            java.lang.Object r14 = r0.L$4
            com.fossil.jn4 r14 = (com.fossil.jn4) r14
            java.lang.Object r2 = r0.L$3
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r5 = r0.L$2
            com.fossil.ko4 r5 = (com.fossil.ko4) r5
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r7 = r0.L$0
            com.fossil.gq4 r7 = (com.fossil.gq4) r7
            com.fossil.t87.a(r15)
            goto L_0x0134
        L_0x0068:
            java.lang.Object r14 = r0.L$1
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r2 = r0.L$0
            com.fossil.gq4 r2 = (com.fossil.gq4) r2
            com.fossil.t87.a(r15)
            goto L_0x008e
        L_0x0074:
            com.fossil.t87.a(r15)
            com.fossil.ti7 r15 = com.fossil.qj7.b()
            com.fossil.gq4$f r2 = new com.fossil.gq4$f
            r2.<init>(r13, r14, r6)
            r0.L$0 = r13
            r0.L$1 = r14
            r0.label = r7
            java.lang.Object r15 = com.fossil.vh7.a(r15, r2, r0)
            if (r15 != r1) goto L_0x008d
            return r1
        L_0x008d:
            r2 = r13
        L_0x008e:
            com.fossil.ko4 r15 = (com.fossil.ko4) r15
            java.lang.Object r8 = r15.c()
            java.util.List r8 = (java.util.List) r8
            if (r8 == 0) goto L_0x0158
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r7 = r2.b
            r7.a(r8)
            java.util.Iterator r7 = r8.iterator()
        L_0x00a1:
            boolean r9 = r7.hasNext()
            if (r9 == 0) goto L_0x00cb
            java.lang.Object r9 = r7.next()
            r10 = r9
            com.fossil.jn4 r10 = (com.fossil.jn4) r10
            java.lang.String r10 = r10.d()
            com.portfolio.platform.PortfolioApp$a r11 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r11 = r11.c()
            java.lang.String r11 = r11.w()
            boolean r10 = com.fossil.ee7.a(r10, r11)
            java.lang.Boolean r10 = com.fossil.pb7.a(r10)
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L_0x00a1
            goto L_0x00cc
        L_0x00cb:
            r9 = r6
        L_0x00cc:
            r7 = r9
            com.fossil.jn4 r7 = (com.fossil.jn4) r7
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = r2.a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "loadPlayersFromServer - current: "
            r11.append(r12)
            r11.append(r7)
            java.lang.String r11 = r11.toString()
            r9.e(r10, r11)
            if (r7 == 0) goto L_0x0145
            java.lang.String r9 = r7.j()
            boolean r9 = com.fossil.ee7.a(r9, r3)
            if (r9 == 0) goto L_0x0145
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = r2.a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "loadPlayersFromServer - end challenge - current: "
            r11.append(r12)
            r11.append(r7)
            java.lang.String r11 = r11.toString()
            r9.e(r10, r11)
            com.fossil.ti7 r9 = com.fossil.qj7.b()
            com.fossil.gq4$e r10 = new com.fossil.gq4$e
            r10.<init>(r2, r8, r6)
            r0.L$0 = r2
            r0.L$1 = r14
            r0.L$2 = r15
            r0.L$3 = r8
            r0.L$4 = r7
            r0.label = r5
            java.lang.Object r5 = com.fossil.vh7.a(r9, r10, r0)
            if (r5 != r1) goto L_0x012f
            return r1
        L_0x012f:
            r6 = r14
            r5 = r15
            r14 = r7
            r7 = r2
            r2 = r8
        L_0x0134:
            java.util.Timer r15 = r7.f
            if (r15 == 0) goto L_0x013b
            r15.cancel()
        L_0x013b:
            androidx.lifecycle.MutableLiveData<java.lang.String> r15 = r7.e
            r15.a(r3)
            r8 = r2
            r15 = r5
            r2 = r7
            r7 = r14
            r14 = r6
        L_0x0145:
            r0.L$0 = r2
            r0.L$1 = r14
            r0.L$2 = r15
            r0.L$3 = r8
            r0.L$4 = r7
            r0.label = r4
            java.lang.Object r14 = r2.a(r14, r8, r0)
            if (r14 != r1) goto L_0x0184
            return r1
        L_0x0158:
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r14 = r2.b
            java.lang.Object r14 = r14.a()
            if (r14 != 0) goto L_0x0172
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r14 = r2.d
            java.lang.Boolean r0 = com.fossil.pb7.a(r7)
            com.portfolio.platform.data.model.ServerError r15 = r15.a()
            com.fossil.r87 r15 = com.fossil.w87.a(r0, r15)
            r14.a(r15)
            goto L_0x0184
        L_0x0172:
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r14 = r2.d
            r0 = 0
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            com.portfolio.platform.data.model.ServerError r15 = r15.a()
            com.fossil.r87 r15 = com.fossil.w87.a(r0, r15)
            r14.a(r15)
        L_0x0184:
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gq4.b(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object c(java.lang.String r6, com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.gq4.j
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.gq4$j r0 = (com.fossil.gq4.j) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gq4$j r0 = new com.fossil.gq4$j
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.gq4 r6 = (com.fossil.gq4) r6
            com.fossil.t87.a(r7)
            goto L_0x0054
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.fossil.ti7 r7 = com.fossil.qj7.b()
            com.fossil.gq4$k r2 = new com.fossil.gq4$k
            r4 = 0
            r2.<init>(r5, r6, r4)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
            if (r7 != r1) goto L_0x0053
            return r1
        L_0x0053:
            r6 = r5
        L_0x0054:
            com.fossil.mn4 r7 = (com.fossil.mn4) r7
            if (r7 == 0) goto L_0x008e
            java.util.Date r7 = r7.e()
            r0 = 0
            if (r7 == 0) goto L_0x006f
            long r2 = r7.getTime()
            java.lang.Long r7 = com.fossil.pb7.a(r2)
            if (r7 == 0) goto L_0x006f
            long r2 = r7.longValue()
            goto L_0x0077
        L_0x006f:
            com.fossil.vt4 r7 = com.fossil.vt4.a
            long r2 = r7.b()
            long r2 = r0 - r2
        L_0x0077:
            int r7 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r7 >= 0) goto L_0x007c
            goto L_0x007d
        L_0x007c:
            r0 = r2
        L_0x007d:
            java.util.Timer r7 = new java.util.Timer
            r7.<init>()
            r6.f = r7
            if (r7 == 0) goto L_0x008e
            com.fossil.gq4$i r2 = new com.fossil.gq4$i
            r2.<init>(r6)
            r7.schedule(r2, r0)
        L_0x008e:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gq4.c(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "challengeId");
        ik7 unused = xh7.b(ie.a(this), null, null, new g(this, str, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r6, com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.gq4.a
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.gq4$a r0 = (com.fossil.gq4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gq4$a r0 = new com.fossil.gq4$a
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.gq4 r6 = (com.fossil.gq4) r6
            com.fossil.t87.a(r7)
            goto L_0x0054
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.fossil.ti7 r7 = com.fossil.qj7.b()
            com.fossil.gq4$b r2 = new com.fossil.gq4$b
            r4 = 0
            r2.<init>(r5, r6, r4)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
            if (r7 != r1) goto L_0x0053
            return r1
        L_0x0053:
            r6 = r5
        L_0x0054:
            com.fossil.yn4 r7 = (com.fossil.yn4) r7
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r6.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "initPlayers - historyChallenge: "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            if (r7 == 0) goto L_0x007d
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r6 = r6.b
            java.util.List r7 = r7.k()
            r6.a(r7)
        L_0x007d:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gq4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, List<jn4> list, fb7<? super i97> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.e(str2, "updateHistoryChallenge - challengeId: " + str);
        Object a2 = vh7.a(qj7.b(), new h(this, str, list, null), fb7);
        if (a2 == nb7.a()) {
            return a2;
        }
        return i97.a;
    }
}
