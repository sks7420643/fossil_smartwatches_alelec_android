package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p73 extends wm2 implements o73 {
    @DexIgnore
    public p73() {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        w63 w63;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            w63 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IGoogleMapDelegate");
            if (queryLocalInterface instanceof w63) {
                w63 = (w63) queryLocalInterface;
            } else {
                w63 = new n83(readStrongBinder);
            }
        }
        a(w63);
        parcel2.writeNoException();
        return true;
    }
}
