package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zr<T, V> {
    @DexIgnore
    V a(T t, rt rtVar);

    @DexIgnore
    boolean a(T t);
}
