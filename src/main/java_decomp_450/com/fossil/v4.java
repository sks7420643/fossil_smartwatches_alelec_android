package com.fossil;

import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c5;
import com.fossil.y4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v4 implements y4.a {
    @DexIgnore
    public c5 a; // = null;
    @DexIgnore
    public float b; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ u4 d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public v4(w4 w4Var) {
        this.d = new u4(this, w4Var);
    }

    @DexIgnore
    public v4 a(c5 c5Var, c5 c5Var2, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(c5Var, -1.0f);
            this.d.a(c5Var2, 1.0f);
        } else {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public boolean b() {
        c5 c5Var = this.a;
        return c5Var != null && (c5Var.g == c5.a.UNRESTRICTED || this.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public v4 c(c5 c5Var, int i) {
        if (i < 0) {
            this.b = (float) (i * -1);
            this.d.a(c5Var, 1.0f);
        } else {
            this.b = (float) i;
            this.d.a(c5Var, -1.0f);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.y4.a
    public void clear() {
        this.d.a();
        this.a = null;
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void d() {
        this.a = null;
        this.d.a();
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String e() {
        /*
            r10 = this;
            com.fossil.c5 r0 = r10.a
            java.lang.String r1 = ""
            if (r0 != 0) goto L_0x0018
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            java.lang.String r1 = "0"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x0029
        L_0x0018:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            com.fossil.c5 r1 = r10.a
            r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0029:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = " = "
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            float r1 = r10.b
            r2 = 0
            r3 = 1
            r4 = 0
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x0056
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            float r0 = r10.b
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r1 = 1
            goto L_0x0057
        L_0x0056:
            r1 = 0
        L_0x0057:
            com.fossil.u4 r5 = r10.d
            int r5 = r5.a
        L_0x005b:
            if (r2 >= r5) goto L_0x00ea
            com.fossil.u4 r6 = r10.d
            com.fossil.c5 r6 = r6.a(r2)
            if (r6 != 0) goto L_0x0067
            goto L_0x00e6
        L_0x0067:
            com.fossil.u4 r7 = r10.d
            float r7 = r7.b(r2)
            int r8 = (r7 > r4 ? 1 : (r7 == r4 ? 0 : -1))
            if (r8 != 0) goto L_0x0073
            goto L_0x00e6
        L_0x0073:
            java.lang.String r6 = r6.toString()
            r9 = -1082130432(0xffffffffbf800000, float:-1.0)
            if (r1 != 0) goto L_0x0091
            int r1 = (r7 > r4 ? 1 : (r7 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x00b8
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = "- "
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x00b6
        L_0x0091:
            if (r8 <= 0) goto L_0x00a5
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = " + "
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x00b8
        L_0x00a5:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = " - "
            r1.append(r0)
            java.lang.String r0 = r1.toString()
        L_0x00b6:
            float r7 = r7 * r9
        L_0x00b8:
            r1 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x00ce
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            goto L_0x00e5
        L_0x00ce:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r7)
            java.lang.String r0 = " "
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
        L_0x00e5:
            r1 = 1
        L_0x00e6:
            int r2 = r2 + 1
            goto L_0x005b
        L_0x00ea:
            if (r1 != 0) goto L_0x00fd
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = "0.0"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
        L_0x00fd:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.v4.e():java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.y4.a
    public c5 getKey() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return e();
    }

    @DexIgnore
    public boolean b(c5 c5Var) {
        return this.d.a(c5Var);
    }

    @DexIgnore
    public v4 b(c5 c5Var, int i) {
        this.a = c5Var;
        float f = (float) i;
        c5Var.e = f;
        this.b = f;
        this.e = true;
        return this;
    }

    @DexIgnore
    public c5 c(c5 c5Var) {
        return this.d.a((boolean[]) null, c5Var);
    }

    @DexIgnore
    public void d(c5 c5Var) {
        c5 c5Var2 = this.a;
        if (c5Var2 != null) {
            this.d.a(c5Var2, -1.0f);
            this.a = null;
        }
        float a2 = this.d.a(c5Var, true) * -1.0f;
        this.a = c5Var;
        if (a2 != 1.0f) {
            this.b /= a2;
            this.d.a(a2);
        }
    }

    @DexIgnore
    public v4 a(c5 c5Var, int i) {
        this.d.a(c5Var, (float) i);
        return this;
    }

    @DexIgnore
    public boolean c() {
        return this.a == null && this.b == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.d.a == 0;
    }

    @DexIgnore
    public v4 a(c5 c5Var, c5 c5Var2, c5 c5Var3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(c5Var, -1.0f);
            this.d.a(c5Var2, 1.0f);
            this.d.a(c5Var3, 1.0f);
        } else {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
            this.d.a(c5Var3, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public v4 b(c5 c5Var, c5 c5Var2, c5 c5Var3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(c5Var, -1.0f);
            this.d.a(c5Var2, 1.0f);
            this.d.a(c5Var3, -1.0f);
        } else {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
            this.d.a(c5Var3, 1.0f);
        }
        return this;
    }

    @DexIgnore
    public v4 a(float f, float f2, float f3, c5 c5Var, c5 c5Var2, c5 c5Var3, c5 c5Var4) {
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f == f3) {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
            this.d.a(c5Var4, 1.0f);
            this.d.a(c5Var3, -1.0f);
        } else if (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
        } else if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(c5Var3, 1.0f);
            this.d.a(c5Var4, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
            this.d.a(c5Var4, f4);
            this.d.a(c5Var3, -f4);
        }
        return this;
    }

    @DexIgnore
    public v4 b(c5 c5Var, c5 c5Var2, c5 c5Var3, c5 c5Var4, float f) {
        this.d.a(c5Var3, 0.5f);
        this.d.a(c5Var4, 0.5f);
        this.d.a(c5Var, -0.5f);
        this.d.a(c5Var2, -0.5f);
        this.b = -f;
        return this;
    }

    @DexIgnore
    public v4 a(c5 c5Var, c5 c5Var2, int i, float f, c5 c5Var3, c5 c5Var4, int i2) {
        if (c5Var2 == c5Var3) {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var4, 1.0f);
            this.d.a(c5Var2, -2.0f);
            return this;
        }
        if (f == 0.5f) {
            this.d.a(c5Var, 1.0f);
            this.d.a(c5Var2, -1.0f);
            this.d.a(c5Var3, -1.0f);
            this.d.a(c5Var4, 1.0f);
            if (i > 0 || i2 > 0) {
                this.b = (float) ((-i) + i2);
            }
        } else if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(c5Var, -1.0f);
            this.d.a(c5Var2, 1.0f);
            this.b = (float) i;
        } else if (f >= 1.0f) {
            this.d.a(c5Var3, -1.0f);
            this.d.a(c5Var4, 1.0f);
            this.b = (float) i2;
        } else {
            float f2 = 1.0f - f;
            this.d.a(c5Var, f2 * 1.0f);
            this.d.a(c5Var2, f2 * -1.0f);
            this.d.a(c5Var3, -1.0f * f);
            this.d.a(c5Var4, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.b = (((float) (-i)) * f2) + (((float) i2) * f);
            }
        }
        return this;
    }

    @DexIgnore
    public v4 a(y4 y4Var, int i) {
        this.d.a(y4Var.a(i, "ep"), 1.0f);
        this.d.a(y4Var.a(i, UserDataStore.EMAIL), -1.0f);
        return this;
    }

    @DexIgnore
    public v4 a(c5 c5Var, c5 c5Var2, c5 c5Var3, float f) {
        this.d.a(c5Var, -1.0f);
        this.d.a(c5Var2, 1.0f - f);
        this.d.a(c5Var3, f);
        return this;
    }

    @DexIgnore
    public v4 a(c5 c5Var, c5 c5Var2, c5 c5Var3, c5 c5Var4, float f) {
        this.d.a(c5Var, -1.0f);
        this.d.a(c5Var2, 1.0f);
        this.d.a(c5Var3, f);
        this.d.a(c5Var4, -f);
        return this;
    }

    @DexIgnore
    public void a() {
        float f = this.b;
        if (f < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b = f * -1.0f;
            this.d.b();
        }
    }

    @DexIgnore
    public boolean a(y4 y4Var) {
        boolean z;
        c5 a2 = this.d.a(y4Var);
        if (a2 == null) {
            z = true;
        } else {
            d(a2);
            z = false;
        }
        if (this.d.a == 0) {
            this.e = true;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.y4.a
    public c5 a(y4 y4Var, boolean[] zArr) {
        return this.d.a(zArr, (c5) null);
    }

    @DexIgnore
    @Override // com.fossil.y4.a
    public void a(y4.a aVar) {
        if (aVar instanceof v4) {
            v4 v4Var = (v4) aVar;
            this.a = null;
            this.d.a();
            int i = 0;
            while (true) {
                u4 u4Var = v4Var.d;
                if (i < u4Var.a) {
                    this.d.a(u4Var.a(i), v4Var.d.b(i), true);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.y4.a
    public void a(c5 c5Var) {
        int i = c5Var.d;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.d.a(c5Var, f);
    }
}
