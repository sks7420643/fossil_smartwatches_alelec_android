package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class ae2 extends xc {
    @DexIgnore
    public static ze2 c;
    @DexIgnore
    public static ze2 d;

    @DexIgnore
    public static int d(Context context, Intent intent) {
        ComponentName componentName;
        ServiceInfo serviceInfo;
        String str;
        if (Log.isLoggable("GcmReceiver", 3)) {
            Log.d("GcmReceiver", "Starting service");
        }
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService == null || (serviceInfo = resolveService.serviceInfo) == null) {
            Log.e("GcmReceiver", "Failed to resolve target intent service, skipping classname enforcement");
        } else if (!context.getPackageName().equals(serviceInfo.packageName) || (str = serviceInfo.name) == null) {
            String str2 = serviceInfo.packageName;
            String str3 = serviceInfo.name;
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 94 + String.valueOf(str3).length());
            sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
            sb.append(str2);
            sb.append("/");
            sb.append(str3);
            Log.e("GcmReceiver", sb.toString());
        } else {
            if (str.startsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                String valueOf = String.valueOf(context.getPackageName());
                String valueOf2 = String.valueOf(str);
                str = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
            }
            if (Log.isLoggable("GcmReceiver", 3)) {
                String valueOf3 = String.valueOf(str);
                Log.d("GcmReceiver", valueOf3.length() != 0 ? "Restricting intent to a specific service: ".concat(valueOf3) : new String("Restricting intent to a specific service: "));
            }
            intent.setClassName(context.getPackageName(), str);
        }
        try {
            if (context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0) {
                componentName = xc.b(context, intent);
            } else {
                componentName = context.startService(intent);
                Log.d("GcmReceiver", "Missing wake lock permission, service start may be delayed");
            }
            if (componentName != null) {
                return -1;
            }
            Log.e("GcmReceiver", "Error while delivering the message: ServiceIntent not found.");
            return 404;
        } catch (SecurityException e) {
            Log.e("GcmReceiver", "Error while delivering the message to the serviceIntent", e);
            return 401;
        } catch (IllegalStateException e2) {
            String valueOf4 = String.valueOf(e2);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf4).length() + 45);
            sb2.append("Failed to start service while in background: ");
            sb2.append(valueOf4);
            Log.e("GcmReceiver", sb2.toString());
            return Action.ActivityTracker.TAG_ACTIVITY;
        }
    }

    @DexIgnore
    public final synchronized ze2 a(Context context, String str) {
        if ("com.google.android.c2dm.intent.RECEIVE".equals(str)) {
            if (d == null) {
                d = new ze2(context, str);
            }
            return d;
        }
        if (c == null) {
            c = new ze2(context, str);
        }
        return c;
    }

    @DexIgnore
    public final int c(Context context, Intent intent) {
        if (Log.isLoggable("GcmReceiver", 3)) {
            Log.d("GcmReceiver", "Binding to service");
        }
        if (isOrderedBroadcast()) {
            setResultCode(-1);
        }
        a(context, intent.getAction()).a(intent, goAsync());
        return -1;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        int i;
        int i2;
        if (Log.isLoggable("GcmReceiver", 3)) {
            Log.d("GcmReceiver", "received new intent");
        }
        intent.setComponent(null);
        intent.setPackage(context.getPackageName());
        if (Build.VERSION.SDK_INT <= 18) {
            intent.removeCategory(context.getPackageName());
        }
        if ("google.com/iid".equals(intent.getStringExtra("from"))) {
            intent.setAction("com.google.android.gms.iid.InstanceID");
        }
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        boolean z = false;
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        if (isOrderedBroadcast()) {
            setResultCode(500);
        }
        boolean z2 = v92.j() && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & SQLiteDatabase.CREATE_IF_NECESSARY) != 0) {
            z = true;
        }
        if (!z2 || z) {
            if ("com.google.android.c2dm.intent.RECEIVE".equals(intent.getAction())) {
                i2 = d(context, intent);
            } else {
                i2 = d(context, intent);
            }
            if (!v92.j() || i2 != 402) {
                i = i2;
            } else {
                c(context, intent);
                i = MFNetworkReturnCode.WRONG_PASSWORD;
            }
        } else {
            i = c(context, intent);
        }
        if (isOrderedBroadcast()) {
            setResultCode(i);
        }
    }
}
