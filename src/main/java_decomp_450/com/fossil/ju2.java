package com.fossil;

import com.fossil.iu2;
import com.fossil.ju2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ju2<MessageType extends ju2<MessageType, BuilderType>, BuilderType extends iu2<MessageType, BuilderType>> implements jx2 {
    @DexIgnore
    public int zza; // = 0;

    @DexIgnore
    public final byte[] a() {
        try {
            byte[] bArr = new byte[k()];
            iv2 a = iv2.a(bArr);
            a(a);
            a.b();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "byte array".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public int c() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.jx2
    public final tu2 h() {
        try {
            cv2 zzc = tu2.zzc(k());
            a(zzc.b());
            return zzc.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public void a(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <T> void a(Iterable<T> iterable, List<? super T> list) {
        ew2.a(iterable);
        if (iterable instanceof tw2) {
            List<?> zzd = ((tw2) iterable).zzd();
            tw2 tw2 = (tw2) list;
            int size = list.size();
            for (Object obj : zzd) {
                if (obj == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(tw2.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = tw2.size() - 1; size2 >= size; size2--) {
                        tw2.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (obj instanceof tu2) {
                    tw2.a((tu2) obj);
                } else {
                    tw2.add((String) obj);
                }
            }
        } else if (iterable instanceof vx2) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size3 = list.size();
            for (T t : iterable) {
                if (t == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size4 = list.size() - 1; size4 >= size3; size4--) {
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(t);
            }
        }
    }
}
