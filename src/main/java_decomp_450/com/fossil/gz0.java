package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz0 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ r21 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gz0(r21 r21) {
        super(1);
        this.a = r21;
    }

    @DexIgnore
    public final void a(v81 v81) {
        sh1 sh1 = (sh1) v81;
        List h = t97.h(sh1.A);
        List h2 = t97.h(sh1.B);
        ea7.a(h, null, null, null, 0, null, ox0.a, 31, null);
        ea7.a(h2, null, null, null, 0, null, wv0.a, 31, null);
        s21 s21 = this.a.a;
        ri1 ri1 = ((zk0) s21).w;
        le0 le0 = le0.DEBUG;
        String str = ((zk0) s21).a;
        s21.H.clear();
        CopyOnWriteArrayList<qk1> copyOnWriteArrayList = s21.H;
        ArrayList arrayList = new ArrayList();
        for (Object obj : h2) {
            if (s21.Q.contains((qk1) obj)) {
                arrayList.add(obj);
            }
        }
        copyOnWriteArrayList.addAll(arrayList);
        this.a.a.p();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(v81 v81) {
        a(v81);
        return i97.a;
    }
}
