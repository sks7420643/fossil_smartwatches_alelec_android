package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lg1 extends k60 implements Parcelable {
    @DexIgnore
    public cs1 a;
    @DexIgnore
    public String b;
    @DexIgnore
    public byte c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore
    public lg1(byte[] bArr) {
        this.e = bArr;
        cs1 a2 = cs1.e.a(bArr[0]);
        if (a2 != null) {
            this.a = a2;
            int b2 = yz0.b(this.e[1]) + 2;
            this.b = new String(s97.a(this.e, 2, b2 - 1), b21.x.c());
            byte[] bArr2 = this.e;
            this.c = bArr2[b2];
            int i = b2 + 1;
            int i2 = i + 4;
            ByteBuffer order = ByteBuffer.wrap(s97.a(bArr2, i, i2)).order(ByteOrder.LITTLE_ENDIAN);
            ee7.a((Object) order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
            this.d = yz0.b(order.getInt());
            byte[] bArr3 = this.e;
            new r60(bArr3[i2], bArr3[i2 + 1]);
            return;
        }
        throw new IllegalArgumentException("Invalid data.");
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        r51 r51 = r51.d;
        cs1 cs1 = this.a;
        if (cs1 != null) {
            JSONObject a2 = yz0.a(jSONObject, r51, yz0.a(cs1));
            r51 r512 = r51.X4;
            String str = this.b;
            if (str != null) {
                return yz0.a(yz0.a(yz0.a(a2, r512, str), r51.Y4, Byte.valueOf(this.c)), r51.Z4, yz0.a((int) this.d));
            }
            ee7.d("bundleId");
            throw null;
        }
        ee7.d("type");
        throw null;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.e);
        }
    }
}
