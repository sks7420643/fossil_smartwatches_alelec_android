package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ x47 d;

    @DexIgnore
    public z47(x47 x47, List list, boolean z, boolean z2) {
        this.d = x47;
        this.a = list;
        this.b = z;
        this.c = z2;
    }

    @DexIgnore
    public void run() {
        this.d.a(this.a, this.b);
        if (this.c) {
            this.a.clear();
        }
    }
}
