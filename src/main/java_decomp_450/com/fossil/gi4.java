package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gi4 extends oi4 {
    @DexIgnore
    @Override // com.fossil.sg4, com.fossil.li4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (mg4 == mg4.EAN_13) {
            return super.a(str, mg4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_13, but got " + mg4);
    }

    @DexIgnore
    @Override // com.fossil.li4
    public boolean[] a(String str) {
        if (str.length() == 13) {
            try {
                if (ni4.a(str)) {
                    int i = fi4.f[Integer.parseInt(str.substring(0, 1))];
                    boolean[] zArr = new boolean[95];
                    int a = li4.a(zArr, 0, ni4.a, true) + 0;
                    int i2 = 1;
                    while (i2 <= 6) {
                        int i3 = i2 + 1;
                        int parseInt = Integer.parseInt(str.substring(i2, i3));
                        if (((i >> (6 - i2)) & 1) == 1) {
                            parseInt += 10;
                        }
                        a += li4.a(zArr, a, ni4.e[parseInt], false);
                        i2 = i3;
                    }
                    int a2 = a + li4.a(zArr, a, ni4.b, false);
                    int i4 = 7;
                    while (i4 <= 12) {
                        int i5 = i4 + 1;
                        a2 += li4.a(zArr, a2, ni4.d[Integer.parseInt(str.substring(i4, i5))], true);
                        i4 = i5;
                    }
                    li4.a(zArr, a2, ni4.a, true);
                    return zArr;
                }
                throw new IllegalArgumentException("Contents do not pass checksum");
            } catch (pg4 unused) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 13 digits long, but got " + str.length());
        }
    }
}
