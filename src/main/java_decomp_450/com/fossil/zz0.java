package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz0 {
    @DexIgnore
    public static /* final */ iy0 a; // = new iy0();
    @DexIgnore
    public static BluetoothProfile b;
    @DexIgnore
    public static /* final */ BroadcastReceiver c; // = new qw0();
    @DexIgnore
    public static /* final */ zz0 d; // = new zz0();

    @DexIgnore
    public final String a(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "DISCONNECTING" : "CONNECTED" : "CONNECTING" : "DISCONNECTED";
    }

    @DexIgnore
    public final void a(BluetoothDevice bluetoothDevice, int i, int i2) {
        t11 t11 = t11.a;
        bluetoothDevice.getAddress();
        a(i);
        a(i2);
        wl0 wl0 = wl0.h;
        String a2 = yz0.a(yu0.HID_STATE_CHANGED);
        ci1 ci1 = ci1.h;
        String address = bluetoothDevice.getAddress();
        String str = "";
        String str2 = address != null ? address : str;
        JSONObject jSONObject = new JSONObject();
        r51 r51 = r51.i0;
        String address2 = bluetoothDevice.getAddress();
        if (address2 != null) {
            str = address2;
        }
        wl0.a(new wr1(a2, ci1, str2, "", "", true, null, null, null, yz0.a(yz0.a(yz0.a(jSONObject, r51, str), r51.A0, a(i)), r51.z0, a(i2)), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", i);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", i2);
        Context a3 = u31.g.a();
        if (a3 != null) {
            qe.a(a3).a(intent);
        }
    }

    @DexIgnore
    public final int b(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile == null) {
            return xr0.f.a;
        }
        try {
            Method method = bluetoothProfile.getClass().getMethod("disconnect", BluetoothDevice.class);
            if (method == null) {
                t11 t11 = t11.a;
                return xr0.i.a;
            }
            Object invoke = method.invoke(bluetoothProfile, bluetoothDevice);
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                t11 t112 = t11.a;
                if (booleanValue) {
                    return xr0.b.a;
                }
                return xr0.i.a;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e) {
            t11 t113 = t11.a;
            StringBuilder b2 = yh0.b("disconnectDevice got exception: ");
            String localizedMessage = e.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = e.toString();
            }
            b2.append(localizedMessage);
            b2.toString();
            wl0.h.a(e);
            return xr0.g.a;
        } catch (Exception e2) {
            t11 t114 = t11.a;
            StringBuilder b3 = yh0.b("disconnectDevice got exception: ");
            String localizedMessage2 = e2.getLocalizedMessage();
            if (localizedMessage2 == null) {
                localizedMessage2 = e2.toString();
            }
            b3.append(localizedMessage2);
            b3.toString();
            wl0.h.a(e2);
            return xr0.i.a;
        }
    }

    @DexIgnore
    public final int c(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile != null) {
            return bluetoothProfile.getConnectionState(bluetoothDevice);
        }
        return 0;
    }

    @DexIgnore
    public final int a(BluetoothProfile bluetoothProfile, BluetoothDevice bluetoothDevice) throws Exception {
        Method method = bluetoothProfile.getClass().getMethod("getPriority", BluetoothDevice.class);
        if (method == null) {
            t11 t11 = t11.a;
            return -1;
        }
        Object invoke = method.invoke(bluetoothProfile, bluetoothDevice);
        if (invoke != null) {
            int intValue = ((Integer) invoke).intValue();
            t11 t112 = t11.a;
            bluetoothDevice.getAddress();
            return intValue;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Int");
    }

    @DexIgnore
    public final void a(Context context) {
        BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(4);
        BluetoothAdapter.getDefaultAdapter().getProfileProxy(context, a, 4);
        t11 t11 = t11.a;
        context.registerReceiver(c, new IntentFilter("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"));
    }

    @DexIgnore
    public final int a(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile == null) {
            return xr0.f.a;
        }
        try {
            int a2 = d.a(bluetoothProfile, bluetoothDevice);
            t11 t11 = t11.a;
            bluetoothDevice.getAddress();
            if (a2 == 0) {
                return xr0.h.a;
            }
            Method method = bluetoothProfile.getClass().getMethod("connect", BluetoothDevice.class);
            if (method == null) {
                t11 t112 = t11.a;
                return xr0.g.a;
            }
            Object invoke = method.invoke(bluetoothProfile, bluetoothDevice);
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                t11 t113 = t11.a;
                if (booleanValue) {
                    return xr0.b.a;
                }
                return xr0.i.a;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e) {
            t11 t114 = t11.a;
            StringBuilder b2 = yh0.b("connectDevice got exception: ");
            String localizedMessage = e.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = e.toString();
            }
            b2.append(localizedMessage);
            b2.toString();
            wl0.h.a(e);
            return xr0.g.a;
        } catch (Exception e2) {
            t11 t115 = t11.a;
            StringBuilder b3 = yh0.b("connectDevice got exception: ");
            String localizedMessage2 = e2.getLocalizedMessage();
            if (localizedMessage2 == null) {
                localizedMessage2 = e2.toString();
            }
            b3.append(localizedMessage2);
            b3.toString();
            e2.printStackTrace();
            wl0.h.a(e2);
            return xr0.i.a;
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> a() {
        List<BluetoothDevice> connectedDevices;
        BluetoothProfile bluetoothProfile = b;
        return (bluetoothProfile == null || (connectedDevices = bluetoothProfile.getConnectedDevices()) == null) ? new ArrayList() : connectedDevices;
    }
}
