package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv6 implements Factory<iv6> {
    @DexIgnore
    public static iv6 a(qe qeVar, DeviceRepository deviceRepository, ch5 ch5, fv6 fv6, tm5 tm5, wm5 wm5, rm5 rm5, xm5 xm5, PortfolioApp portfolioApp) {
        return new iv6(qeVar, deviceRepository, ch5, fv6, tm5, wm5, rm5, xm5, portfolioApp);
    }
}
