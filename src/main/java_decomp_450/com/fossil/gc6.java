package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.cc5;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gc6 extends go5 implements fc6 {
    @DexIgnore
    public qw6<y15> f;
    @DexIgnore
    public ec6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.A;
            Date date = new Date();
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HeartRateOverviewDayFragment";
    }

    @DexIgnore
    public final void f1() {
        y15 a2;
        TodayHeartRateChart todayHeartRateChart;
        qw6<y15> qw6 = this.f;
        if (qw6 == null) {
            ee7.d("mBinding");
            throw null;
        } else if (qw6 != null && (a2 = qw6.a()) != null && (todayHeartRateChart = a2.w) != null) {
            todayHeartRateChart.a("maxHeartRate", "lowestHeartRate");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewDayFragment", "onCreateView");
        y15 y15 = (y15) qb.a(layoutInflater, 2131558564, viewGroup, false, a1());
        y15.u.setOnClickListener(b.a);
        this.f = new qw6<>(this, y15);
        f1();
        qw6<y15> qw6 = this.f;
        if (qw6 != null) {
            y15 a2 = qw6.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ec6 ec6 = this.g;
        if (ec6 != null) {
            ec6.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f1();
        ec6 ec6 = this.g;
        if (ec6 != null) {
            ec6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ec6 ec6) {
        ee7.b(ec6, "presenter");
        this.g = ec6;
    }

    @DexIgnore
    @Override // com.fossil.fc6
    public void a(int i, List<fz6> list, List<v87<Integer, r87<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        ee7.b(list, "listTodayHeartRateModel");
        ee7.b(list2, "listTimeZoneChange");
        qw6<y15> qw6 = this.f;
        if (qw6 != null) {
            y15 a2 = qw6.a();
            if (a2 != null && (todayHeartRateChart = a2.w) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.a(list);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fc6
    public void a(boolean z, List<WorkoutSession> list) {
        View d;
        View d2;
        ee7.b(list, "workoutSessions");
        qw6<y15> qw6 = this.f;
        if (qw6 != null) {
            y15 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.v;
                ee7.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.r, list.get(0));
                if (size == 1) {
                    s95 s95 = a2.s;
                    if (s95 != null && (d2 = s95.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                s95 s952 = a2.s;
                if (!(s952 == null || (d = s952.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.s, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.v;
            ee7.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(s95 s95, WorkoutSession workoutSession) {
        if (s95 != null) {
            View d = s95.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            cc5.a aVar = cc5.Companion;
            r87<Integer, Integer> a2 = aVar.a(aVar.a(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String a3 = ig5.a(context, a2.getSecond().intValue());
            s95.w.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = s95.v;
            ee7.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = s95.t;
            ee7.a((Object) flexibleTextView2, "it.ftvRestingValue");
            WorkoutHeartRate heartRate = workoutSession.getHeartRate();
            int i = 0;
            flexibleTextView2.setText(String.valueOf(heartRate != null ? af7.a(heartRate.getAverage()) : 0));
            FlexibleTextView flexibleTextView3 = s95.r;
            ee7.a((Object) flexibleTextView3, "it.ftvMaxValue");
            WorkoutHeartRate heartRate2 = workoutSession.getHeartRate();
            if (heartRate2 != null) {
                i = heartRate2.getMax();
            }
            flexibleTextView3.setText(String.valueOf(i));
            FlexibleTextView flexibleTextView4 = s95.u;
            ee7.a((Object) flexibleTextView4, "it.ftvWorkoutTime");
            flexibleTextView4.setText(zd5.a(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            String b2 = eh5.l.a().b("dianaHeartRateTab");
            if (b2 != null) {
                s95.w.setColorFilter(Color.parseColor(b2));
            }
            String b3 = eh5.l.a().b("nonBrandLineColor");
            if (b3 != null) {
                s95.x.setBackgroundColor(Color.parseColor(b3));
            }
        }
    }
}
