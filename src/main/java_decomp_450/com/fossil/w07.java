package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w07 extends m07<ImageView> {
    @DexIgnore
    public q07 m;

    @DexIgnore
    public w07(Picasso picasso, ImageView imageView, g17 g17, int i, int i2, int i3, Drawable drawable, String str, Object obj, q07 q07, boolean z) {
        super(picasso, imageView, g17, i, i2, i3, drawable, str, obj, z);
        this.m = q07;
    }

    @DexIgnore
    @Override // com.fossil.m07
    public void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            T t = ((m07) this).c.get();
            if (t != null) {
                Picasso picasso = ((m07) this).a;
                e17.a(t, picasso.e, bitmap, loadedFrom, ((m07) this).d, picasso.m);
                q07 q07 = this.m;
                if (q07 != null) {
                    q07.onSuccess();
                    return;
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    @DexIgnore
    @Override // com.fossil.m07
    public void b() {
        T t = ((m07) this).c.get();
        if (t != null) {
            int i = ((m07) this).g;
            if (i != 0) {
                t.setImageResource(i);
            } else {
                Drawable drawable = ((m07) this).h;
                if (drawable != null) {
                    t.setImageDrawable(drawable);
                }
            }
            q07 q07 = this.m;
            if (q07 != null) {
                q07.onError();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m07
    public void a() {
        super.a();
        if (this.m != null) {
            this.m = null;
        }
    }
}
