package com.fossil;

import com.fossil.eu1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ou1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(bt1 bt1);

        @DexIgnore
        public abstract a a(ct1<?> ct1);

        @DexIgnore
        public abstract a a(et1<?, byte[]> et1);

        @DexIgnore
        public abstract a a(pu1 pu1);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract ou1 a();
    }

    @DexIgnore
    public static a g() {
        return new eu1.b();
    }

    @DexIgnore
    public abstract bt1 a();

    @DexIgnore
    public abstract ct1<?> b();

    @DexIgnore
    public byte[] c() {
        return d().apply(b().b());
    }

    @DexIgnore
    public abstract et1<?, byte[]> d();

    @DexIgnore
    public abstract pu1 e();

    @DexIgnore
    public abstract String f();
}
