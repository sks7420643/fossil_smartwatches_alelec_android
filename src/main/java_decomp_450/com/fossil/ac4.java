package com.fossil;

import com.fossil.pb4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ac4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract ac4 a();

        @DexIgnore
        public abstract a b(long j);
    }

    @DexIgnore
    public static a d() {
        return new pb4.b();
    }

    @DexIgnore
    public abstract String a();

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract long c();
}
