package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g70 extends dg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public me0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<g70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public g70 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                    Parcelable readParcelable = parcel.readParcelable(me0.class.getClassLoader());
                    if (readParcelable != null) {
                        return new g70(readString, createByteArray, (me0) readParcelable);
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public g70[] newArray(int i) {
            return new g70[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ g70(String str, byte[] bArr, me0 me0, int i, zd7 zd7) {
        this(str, bArr, (i & 4) != 0 ? new me0(0, 62, 0) : me0);
    }

    @DexIgnore
    public final void a(me0 me0) {
        this.d = me0;
    }

    @DexIgnore
    public final JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("image_name", d()).put("pos", this.d.a());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final me0 getPositionConfig() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.dg0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public g70(String str, byte[] bArr, me0 me0) {
        super(str, bArr);
        this.d = me0;
    }

    @DexIgnore
    @Override // com.fossil.dg0, com.fossil.k60
    public JSONObject a() {
        JSONObject put = super.a().put("pos", this.d.a());
        ee7.a((Object) put, "super.toJSONObject()\n   \u2026ionConfig.toJSONObject())");
        return put;
    }
}
