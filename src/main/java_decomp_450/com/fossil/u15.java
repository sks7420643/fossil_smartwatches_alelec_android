package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ Guideline E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ ImageView G;
    @DexIgnore
    public /* final */ ImageView H;
    @DexIgnore
    public /* final */ View I;
    @DexIgnore
    public /* final */ LinearLayout J;
    @DexIgnore
    public /* final */ ConstraintLayout K;
    @DexIgnore
    public /* final */ RecyclerView L;
    @DexIgnore
    public /* final */ View M;
    @DexIgnore
    public /* final */ FlexibleTextView N;
    @DexIgnore
    public /* final */ View O;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ TodayHeartRateChart v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public u15(Object obj, View view, int i, AppBarLayout appBarLayout, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, TodayHeartRateChart todayHeartRateChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, Guideline guideline, RTLImageView rTLImageView, ImageView imageView, ImageView imageView2, View view2, LinearLayout linearLayout, ConstraintLayout constraintLayout5, RecyclerView recyclerView, View view3, FlexibleTextView flexibleTextView9, View view4) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = constraintLayout4;
        this.v = todayHeartRateChart;
        this.w = flexibleTextView;
        this.x = flexibleTextView2;
        this.y = flexibleTextView3;
        this.z = flexibleTextView4;
        this.A = flexibleTextView5;
        this.B = flexibleTextView6;
        this.C = flexibleTextView7;
        this.D = flexibleTextView8;
        this.E = guideline;
        this.F = rTLImageView;
        this.G = imageView;
        this.H = imageView2;
        this.I = view2;
        this.J = linearLayout;
        this.K = constraintLayout5;
        this.L = recyclerView;
        this.M = view3;
        this.N = flexibleTextView9;
        this.O = view4;
    }
}
