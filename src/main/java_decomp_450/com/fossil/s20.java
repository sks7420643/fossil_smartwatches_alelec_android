package com.fossil;

import android.graphics.Bitmap;
import com.fossil.nw;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s20 implements nw.a {
    @DexIgnore
    public /* final */ dz a;
    @DexIgnore
    public /* final */ az b;

    @DexIgnore
    public s20(dz dzVar, az azVar) {
        this.a = dzVar;
        this.b = azVar;
    }

    @DexIgnore
    @Override // com.fossil.nw.a
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.a.b(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.nw.a
    public byte[] b(int i) {
        az azVar = this.b;
        if (azVar == null) {
            return new byte[i];
        }
        return (byte[]) azVar.b(i, byte[].class);
    }

    @DexIgnore
    @Override // com.fossil.nw.a
    public void a(Bitmap bitmap) {
        this.a.a(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.nw.a
    public void a(byte[] bArr) {
        az azVar = this.b;
        if (azVar != null) {
            azVar.a(bArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.nw.a
    public int[] a(int i) {
        az azVar = this.b;
        if (azVar == null) {
            return new int[i];
        }
        return (int[]) azVar.b(i, int[].class);
    }

    @DexIgnore
    @Override // com.fossil.nw.a
    public void a(int[] iArr) {
        az azVar = this.b;
        if (azVar != null) {
            azVar.a(iArr);
        }
    }
}
