package com.fossil;

import com.facebook.AccessToken;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w74 implements v74 {
    @DexIgnore
    @Override // com.fossil.v74
    public c84 a(d44 d44, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new c84(a(d44, (long) optInt2, jSONObject), a(jSONObject.getJSONObject("fabric"), jSONObject.getJSONObject("app")), a(), a(jSONObject.getJSONObject("features")), optInt, optInt2);
    }

    @DexIgnore
    public static y74 a(JSONObject jSONObject, JSONObject jSONObject2) throws JSONException {
        String str;
        String string = jSONObject2.getString("status");
        boolean equals = "new".equals(string);
        String string2 = jSONObject.getString("bundle_id");
        String string3 = jSONObject.getString("org_id");
        if (equals) {
            str = "https://update.crashlytics.com/spi/v1/platforms/android/apps";
        } else {
            str = String.format(Locale.US, "https://update.crashlytics.com/spi/v1/platforms/android/apps/%s", string2);
        }
        return new y74(string, str, String.format(Locale.US, "https://reports.crashlytics.com/spi/v1/platforms/android/apps/%s/reports", string2), String.format(Locale.US, "https://reports.crashlytics.com/sdk-api/v1/platforms/android/apps/%s/minidumps", string2), string2, string3, jSONObject2.optBoolean("update_required", false), jSONObject2.optInt("report_upload_variant", 0), jSONObject2.optInt("native_report_upload_variant", 0));
    }

    @DexIgnore
    public static z74 a(JSONObject jSONObject) {
        return new z74(jSONObject.optBoolean("collect_reports", true));
    }

    @DexIgnore
    public static a84 a() {
        return new a84(8, 4);
    }

    @DexIgnore
    public static long a(d44 d44, long j, JSONObject jSONObject) {
        if (jSONObject.has(AccessToken.EXPIRES_AT_KEY)) {
            return jSONObject.optLong(AccessToken.EXPIRES_AT_KEY);
        }
        return d44.a() + (j * 1000);
    }
}
