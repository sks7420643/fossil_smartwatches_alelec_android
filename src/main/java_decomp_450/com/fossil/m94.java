package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m94 {
    @DexIgnore
    public static m94 b;
    @DexIgnore
    public /* final */ SharedPreferences a;

    @DexIgnore
    public m94(Context context) {
        this.a = context.getSharedPreferences("FirebaseAppHeartBeat", 0);
    }

    @DexIgnore
    public static synchronized m94 a(Context context) {
        m94 m94;
        synchronized (m94.class) {
            if (b == null) {
                b = new m94(context);
            }
            m94 = b;
        }
        return m94;
    }

    @DexIgnore
    public synchronized boolean a(String str, long j) {
        if (!this.a.contains(str)) {
            this.a.edit().putLong(str, j).apply();
            return true;
        } else if (j - this.a.getLong(str, -1) < LogBuilder.MAX_INTERVAL) {
            return false;
        } else {
            this.a.edit().putLong(str, j).apply();
            return true;
        }
    }

    @DexIgnore
    public synchronized boolean a(long j) {
        return a("fire-global", j);
    }
}
