package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b83 extends tm2 implements z63 {
    @DexIgnore
    public b83(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IProjectionDelegate");
    }

    @DexIgnore
    @Override // com.fossil.z63
    public final ab2 b(LatLng latLng) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLng);
        Parcel a = a(2, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.z63
    public final LatLng e(ab2 ab2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        Parcel a = a(1, zza);
        LatLng latLng = (LatLng) xm2.a(a, LatLng.CREATOR);
        a.recycle();
        return latLng;
    }

    @DexIgnore
    @Override // com.fossil.z63
    public final v93 q() throws RemoteException {
        Parcel a = a(3, zza());
        v93 v93 = (v93) xm2.a(a, v93.CREATOR);
        a.recycle();
        return v93;
    }
}
