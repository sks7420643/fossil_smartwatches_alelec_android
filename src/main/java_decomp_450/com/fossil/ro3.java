package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro3 extends do3 {
    @DexIgnore
    public /* final */ lp3<Void> a; // = new lp3<>();

    @DexIgnore
    @Override // com.fossil.do3
    public final boolean a() {
        return this.a.d();
    }

    @DexIgnore
    public final void b() {
        this.a.b((Void) null);
    }

    @DexIgnore
    @Override // com.fossil.do3
    public final do3 a(ko3 ko3) {
        this.a.a(new so3(this, ko3));
        return this;
    }
}
