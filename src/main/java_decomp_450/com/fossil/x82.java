package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x82 extends eg2 implements w82 {
    @DexIgnore
    public x82(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @DexIgnore
    @Override // com.fossil.w82
    public final boolean a(ta2 ta2, ab2 ab2) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ta2);
        fg2.a(zza, ab2);
        Parcel a = a(5, zza);
        boolean a2 = fg2.a(a);
        a.recycle();
        return a2;
    }
}
