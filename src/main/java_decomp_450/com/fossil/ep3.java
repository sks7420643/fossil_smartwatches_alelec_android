package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep3<TResult> implements hp3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public jo3<? super TResult> c;

    @DexIgnore
    public ep3(Executor executor, jo3<? super TResult> jo3) {
        this.a = executor;
        this.c = jo3;
    }

    @DexIgnore
    @Override // com.fossil.hp3
    public final void a(no3<TResult> no3) {
        if (no3.e()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new dp3(this, no3));
                }
            }
        }
    }
}
