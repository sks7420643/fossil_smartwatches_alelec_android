package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ia5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public ia5(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, ImageView imageView, View view2, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleButton2;
        this.s = imageView;
        this.t = view2;
        this.u = constraintLayout;
        this.v = flexibleTextView;
    }

    @DexIgnore
    public static ia5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static ia5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (ia5) ViewDataBinding.a(layoutInflater, 2131558711, viewGroup, z, obj);
    }
}
