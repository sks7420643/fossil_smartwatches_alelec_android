package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl5 implements MembersInjector<cl5> {
    @DexIgnore
    public static void a(cl5 cl5, UserRepository userRepository) {
        cl5.h = userRepository;
    }

    @DexIgnore
    public static void a(cl5 cl5, ch5 ch5) {
        cl5.i = ch5;
    }

    @DexIgnore
    public static void a(cl5 cl5, DeviceRepository deviceRepository) {
        cl5.j = deviceRepository;
    }

    @DexIgnore
    public static void a(cl5 cl5, ll4 ll4) {
        cl5.p = ll4;
    }

    @DexIgnore
    public static void a(cl5 cl5, xm5 xm5) {
        cl5.q = xm5;
    }
}
