package com.fossil;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class og7 extends ng7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterable<T>, ye7 {
        @DexIgnore
        public /* final */ /* synthetic */ hg7 a;

        @DexIgnore
        public a(hg7 hg7) {
            this.a = hg7;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<T, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'boolean' to match base method */
        @Override // com.fossil.gd7
        public final Boolean invoke(T t) {
            return t == null;
        }
    }

    @DexIgnore
    public static final <T> hg7<T> a(hg7<? extends T> hg7, gd7<? super T, Boolean> gd7) {
        ee7.b(hg7, "$this$filter");
        ee7.b(gd7, "predicate");
        return new fg7(hg7, true, gd7);
    }

    @DexIgnore
    public static final <T> hg7<T> b(hg7<? extends T> hg7, gd7<? super T, Boolean> gd7) {
        ee7.b(hg7, "$this$filterNot");
        ee7.b(gd7, "predicate");
        return new fg7(hg7, false, gd7);
    }

    @DexIgnore
    public static final <T> hg7<T> c(hg7<? extends T> hg7) {
        ee7.b(hg7, "$this$filterNotNull");
        hg7<T> b2 = b(hg7, b.INSTANCE);
        if (b2 != null) {
            return b2;
        }
        throw new x87("null cannot be cast to non-null type kotlin.sequences.Sequence<T>");
    }

    @DexIgnore
    public static final <T, R> hg7<R> d(hg7<? extends T> hg7, gd7<? super T, ? extends R> gd7) {
        ee7.b(hg7, "$this$mapNotNull");
        ee7.b(gd7, "transform");
        return c(new pg7(hg7, gd7));
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T e(com.fossil.hg7<? extends T> r3) {
        /*
            java.lang.String r0 = "$this$min"
            com.fossil.ee7.b(r3, r0)
            java.util.Iterator r3 = r3.iterator()
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0011
            r3 = 0
            return r3
        L_0x0011:
            java.lang.Object r0 = r3.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
        L_0x0017:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r3.next()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r2 = r0.compareTo(r1)
            if (r2 <= 0) goto L_0x0017
            r0 = r1
            goto L_0x0017
        L_0x002b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.og7.e(com.fossil.hg7):java.lang.Comparable");
    }

    @DexIgnore
    public static final <T> List<T> f(hg7<? extends T> hg7) {
        ee7.b(hg7, "$this$toList");
        return w97.b(g(hg7));
    }

    @DexIgnore
    public static final <T> List<T> g(hg7<? extends T> hg7) {
        ee7.b(hg7, "$this$toMutableList");
        ArrayList arrayList = new ArrayList();
        a(hg7, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C a(hg7<? extends T> hg7, C c) {
        ee7.b(hg7, "$this$toCollection");
        ee7.b(c, ShareConstants.DESTINATION);
        Iterator<? extends T> it = hg7.iterator();
        while (it.hasNext()) {
            c.add(it.next());
        }
        return c;
    }

    @DexIgnore
    public static final <T> Iterable<T> b(hg7<? extends T> hg7) {
        ee7.b(hg7, "$this$asIterable");
        return new a(hg7);
    }

    @DexIgnore
    public static final <T, R> hg7<R> c(hg7<? extends T> hg7, gd7<? super T, ? extends R> gd7) {
        ee7.b(hg7, "$this$map");
        ee7.b(gd7, "transform");
        return new pg7(hg7, gd7);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T d(com.fossil.hg7<? extends T> r3) {
        /*
            java.lang.String r0 = "$this$max"
            com.fossil.ee7.b(r3, r0)
            java.util.Iterator r3 = r3.iterator()
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0011
            r3 = 0
            return r3
        L_0x0011:
            java.lang.Object r0 = r3.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
        L_0x0017:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r3.next()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r2 = r0.compareTo(r1)
            if (r2 >= 0) goto L_0x0017
            r0 = r1
            goto L_0x0017
        L_0x002b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.og7.d(com.fossil.hg7):java.lang.Comparable");
    }

    @DexIgnore
    public static final <T, A extends Appendable> A a(hg7<? extends T> hg7, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7<? super T, ? extends CharSequence> gd7) {
        ee7.b(hg7, "$this$joinTo");
        ee7.b(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        ee7.b(charSequence, "separator");
        ee7.b(charSequence2, "prefix");
        ee7.b(charSequence3, "postfix");
        ee7.b(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object obj : hg7) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            eh7.a(a2, obj, gd7);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static /* synthetic */ String a(hg7 hg7, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7 gd7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        if ((i2 & 32) != 0) {
            gd7 = null;
        }
        return a(hg7, charSequence, charSequence6, charSequence5, i3, charSequence4, gd7);
    }

    @DexIgnore
    public static final <T> String a(hg7<? extends T> hg7, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7<? super T, ? extends CharSequence> gd7) {
        ee7.b(hg7, "$this$joinToString");
        ee7.b(charSequence, "separator");
        ee7.b(charSequence2, "prefix");
        ee7.b(charSequence3, "postfix");
        ee7.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(hg7, sb, charSequence, charSequence2, charSequence3, i, charSequence4, gd7);
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
