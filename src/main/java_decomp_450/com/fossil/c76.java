package com.fossil;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c76 extends io5<b76> {
    @DexIgnore
    void C(boolean z);

    @DexIgnore
    void D();

    @DexIgnore
    void J();

    @DexIgnore
    void P();

    @DexIgnore
    void Q0();

    @DexIgnore
    void W0();

    @DexIgnore
    void Y0();

    @DexIgnore
    void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z);

    @DexIgnore
    void a(boolean z, boolean z2, boolean z3);

    @DexIgnore
    void b(String str, String str2);

    @DexIgnore
    void b(Date date);

    @DexIgnore
    void c(boolean z);

    @DexIgnore
    void f(int i);

    @DexIgnore
    void q(boolean z);
}
