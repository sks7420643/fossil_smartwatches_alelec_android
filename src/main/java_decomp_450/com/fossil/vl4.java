package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl4 extends RecyclerView.g<a> {
    @DexIgnore
    public ArrayList<String> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            ee7.b(view, "itemView");
            View findViewById = view.findViewById(2131362378);
            ee7.a((Object) findViewById, "itemView.findViewById(R.id.ftv_description)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362661);
            ee7.a((Object) findViewById2, "itemView.findViewById(R.id.iv_device)");
            this.b = (ImageView) findViewById2;
        }

        @DexIgnore
        public final void a(String str) {
            if (str != null) {
                this.a.setText(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    public final void a(List<String> list) {
        ee7.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558701, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new a(inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "viewHolder");
        aVar.a(this.a.get(i));
    }
}
