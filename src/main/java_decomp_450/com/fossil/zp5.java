package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp5 extends bq5<c> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> i; // = new ArrayList<>();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public b q;
    @DexIgnore
    public AlphabetIndexer r;
    @DexIgnore
    public /* final */ List<bt5> s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ w85 h;
        @DexIgnore
        public /* final */ /* synthetic */ zp5 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.b();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.b();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zp5 zp5, w85 w85) {
            super(w85.d());
            ee7.b(w85, "binding");
            this.i = zp5;
            this.h = w85;
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = eh5.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                this.h.s.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.h.x.setBackgroundColor(Color.parseColor(b3));
            }
            this.h.r.setOnClickListener(new a(this));
            this.h.q.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void b() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.s.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((bt5) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 != -1) {
                    this.i.s.remove(i2);
                } else {
                    this.i.s.add(a());
                }
                b c2 = this.i.q;
                if (c2 != null) {
                    c2.a();
                }
                this.i.notifyItemChanged(adapterPosition);
            }
        }

        @DexIgnore
        public final void a(Cursor cursor, int i2) {
            Object obj;
            boolean z;
            ee7.b(cursor, "cursor");
            cursor.moveToPosition(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d2 = zp5.t;
            local.d(d2, ".Inside renderData, cursor move position=" + i2);
            this.a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            boolean z2 = true;
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            if (this.i.p < i2) {
                this.i.p = i2;
            }
            String b2 = yx6.b(this.c);
            if (this.i.i.contains(Integer.valueOf(i2)) || (this.i.p < i2 && this.i.j == this.e && this.i.h.contains(b2))) {
                if (!this.i.i.contains(Integer.valueOf(i2))) {
                    this.i.i.add(Integer.valueOf(i2));
                }
                a(8);
            } else {
                if (i2 > this.i.p) {
                    this.i.p = i2;
                }
                if (this.i.j != this.e) {
                    this.i.h.clear();
                    this.i.j = this.e;
                }
                this.i.h.add(b2);
                a(0);
                if (cursor.moveToPrevious() && cursor.getInt(cursor.getColumnIndex("contact_id")) == this.e) {
                    FlexibleCheckBox flexibleCheckBox = this.h.q;
                    ee7.a((Object) flexibleCheckBox, "binding.accbSelect");
                    flexibleCheckBox.setVisibility(4);
                    FlexibleTextView flexibleTextView = this.h.w;
                    ee7.a((Object) flexibleTextView, "binding.pickContactTitle");
                    flexibleTextView.setVisibility(8);
                }
                cursor.moveToNext();
            }
            FlexibleTextView flexibleTextView2 = this.h.w;
            ee7.a((Object) flexibleTextView2, "binding.pickContactTitle");
            flexibleTextView2.setText(this.a);
            if (this.f == 1) {
                FlexibleTextView flexibleTextView3 = this.h.v;
                ee7.a((Object) flexibleTextView3, "binding.pickContactPhone");
                flexibleTextView3.setText(this.c);
                FlexibleTextView flexibleTextView4 = this.h.v;
                ee7.a((Object) flexibleTextView4, "binding.pickContactPhone");
                flexibleTextView4.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView5 = this.h.v;
                ee7.a((Object) flexibleTextView5, "binding.pickContactPhone");
                flexibleTextView5.setVisibility(8);
            }
            Iterator it = this.i.s.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                Contact contact = ((bt5) obj).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            bt5 bt5 = (bt5) obj;
            FlexibleCheckBox flexibleCheckBox2 = this.h.q;
            ee7.a((Object) flexibleCheckBox2, "binding.accbSelect");
            if (bt5 == null) {
                z2 = false;
            }
            flexibleCheckBox2.setChecked(z2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String d3 = zp5.t;
            local2.d(d3, "Inside renderData, contactId = " + this.e + ", displayName = " + this.a + ", hasPhoneNumber = " + this.f + ',' + " phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                FlexibleTextView flexibleTextView6 = this.h.t;
                ee7.a((Object) flexibleTextView6, "binding.ftvAlphabet");
                flexibleTextView6.setVisibility(0);
                FlexibleTextView flexibleTextView7 = this.h.t;
                ee7.a((Object) flexibleTextView7, "binding.ftvAlphabet");
                flexibleTextView7.setText(Character.toString(xe5.b.a(this.d)));
                return;
            }
            FlexibleTextView flexibleTextView8 = this.h.t;
            ee7.a((Object) flexibleTextView8, "binding.ftvAlphabet");
            flexibleTextView8.setVisibility(8);
        }

        @DexIgnore
        public final void a(int i2) {
            FlexibleCheckBox flexibleCheckBox = this.h.q;
            ee7.a((Object) flexibleCheckBox, "binding.accbSelect");
            flexibleCheckBox.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            ee7.a((Object) constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView = this.h.t;
            ee7.a((Object) flexibleTextView, "binding.ftvAlphabet");
            flexibleTextView.setVisibility(i2);
            LinearLayout linearLayout = this.h.u;
            ee7.a((Object) linearLayout, "binding.llTextContainer");
            linearLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView2 = this.h.v;
            ee7.a((Object) flexibleTextView2, "binding.pickContactPhone");
            flexibleTextView2.setVisibility(i2);
            FlexibleTextView flexibleTextView3 = this.h.w;
            ee7.a((Object) flexibleTextView3, "binding.pickContactTitle");
            flexibleTextView3.setVisibility(i2);
            View view = this.h.x;
            ee7.a((Object) view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.s;
            ee7.a((Object) constraintLayout2, "binding.clRoot");
            constraintLayout2.setVisibility(i2);
        }

        @DexIgnore
        public final bt5 a() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.a);
            contact.setPhotoThumbUri(this.b);
            bt5 bt5 = new bt5(contact, null, 2, null);
            if (this.f == 1) {
                bt5.setHasPhoneNumber(true);
                bt5.setPhoneNumber(this.c);
            } else {
                bt5.setHasPhoneNumber(false);
            }
            Contact contact2 = bt5.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
                Contact contact3 = bt5.getContact();
                if (contact3 != null) {
                    contact3.setUseCall(true);
                    bt5.setFavorites(this.g);
                    bt5.setAdded(true);
                    return bt5;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = zp5.class.getSimpleName();
        ee7.a((Object) simpleName, "CursorContactSearchAdapter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zp5(Cursor cursor, List<bt5> list) {
        super(cursor);
        ee7.b(list, "mContactWrapperList");
        this.s = list;
    }

    @DexIgnore
    public int getPositionForSection(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.r;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i2);
            }
            ee7.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.r;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i2);
            }
            ee7.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.r;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.bq5
    public Cursor c(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        AlphabetIndexer alphabetIndexer = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRTSUVWXYZ");
        this.r = alphabetIndexer;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.h.clear();
            this.i.clear();
            this.j = -1;
            this.p = -1;
            return super.c(cursor);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i2) {
        ee7.b(viewGroup, "parent");
        w85 a2 = w85.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemContactBinding.infla\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public void a(c cVar, Cursor cursor, int i2) {
        ee7.b(cVar, "holder");
        if (cursor != null) {
            cVar.a(cursor, i2);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "itemClickListener");
        this.q = bVar;
    }
}
