package com.fossil;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public xp2 c;
    @DexIgnore
    public BitSet d;
    @DexIgnore
    public BitSet e;
    @DexIgnore
    public Map<Integer, Long> f;
    @DexIgnore
    public Map<Integer, List<Long>> g;
    @DexIgnore
    public /* final */ /* synthetic */ om3 h;

    @DexIgnore
    public qm3(om3 om3, String str) {
        this.h = om3;
        this.a = str;
        this.b = true;
        this.d = new BitSet();
        this.e = new BitSet();
        this.f = new n4();
        this.g = new n4();
    }

    @DexIgnore
    public final void a(vm3 vm3) {
        int a2 = vm3.a();
        Boolean bool = vm3.c;
        if (bool != null) {
            this.e.set(a2, bool.booleanValue());
        }
        Boolean bool2 = vm3.d;
        if (bool2 != null) {
            this.d.set(a2, bool2.booleanValue());
        }
        if (vm3.e != null) {
            Long l = this.f.get(Integer.valueOf(a2));
            long longValue = vm3.e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.f.put(Integer.valueOf(a2), Long.valueOf(longValue));
            }
        }
        if (vm3.f != null) {
            List<Long> list = this.g.get(Integer.valueOf(a2));
            if (list == null) {
                list = new ArrayList<>();
                this.g.put(Integer.valueOf(a2), list);
            }
            if (vm3.b()) {
                list.clear();
            }
            if (b13.a() && this.h.l().d(this.a, wb3.g0) && vm3.c()) {
                list.clear();
            }
            if (!b13.a() || !this.h.l().d(this.a, wb3.g0)) {
                list.add(Long.valueOf(vm3.f.longValue() / 1000));
                return;
            }
            long longValue2 = vm3.f.longValue() / 1000;
            if (!list.contains(Long.valueOf(longValue2))) {
                list.add(Long.valueOf(longValue2));
            }
        }
    }

    @DexIgnore
    public qm3(om3 om3, String str, xp2 xp2, BitSet bitSet, BitSet bitSet2, Map<Integer, Long> map, Map<Integer, Long> map2) {
        this.h = om3;
        this.a = str;
        this.d = bitSet;
        this.e = bitSet2;
        this.f = map;
        this.g = new n4();
        if (map2 != null) {
            for (Integer num : map2.keySet()) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(map2.get(num));
                this.g.put(num, arrayList);
            }
        }
        this.b = false;
        this.c = xp2;
    }

    @DexIgnore
    public /* synthetic */ qm3(om3 om3, String str, xp2 xp2, BitSet bitSet, BitSet bitSet2, Map map, Map map2, rm3 rm3) {
        this(om3, str, xp2, bitSet, bitSet2, map, map2);
    }

    @DexIgnore
    public /* synthetic */ qm3(om3 om3, String str, rm3 rm3) {
        this(om3, str);
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:23:0x00ce */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0, types: [com.fossil.pp2$a, com.fossil.bw2$a] */
    /* JADX WARN: Type inference failed for: r8v3, types: [com.fossil.xp2$a] */
    /* JADX WARN: Type inference failed for: r1v7, types: [java.lang.Iterable] */
    /* JADX WARN: Type inference failed for: r1v8, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r1v9, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.pp2 a(int r8) {
        /*
            r7 = this;
            com.fossil.pp2$a r0 = com.fossil.pp2.v()
            r0.a(r8)
            boolean r8 = r7.b
            r0.a(r8)
            com.fossil.xp2 r8 = r7.c
            if (r8 == 0) goto L_0x0013
            r0.a(r8)
        L_0x0013:
            com.fossil.xp2$a r8 = com.fossil.xp2.A()
            java.util.BitSet r1 = r7.d
            java.util.List r1 = com.fossil.fm3.a(r1)
            r8.b(r1)
            java.util.BitSet r1 = r7.e
            java.util.List r1 = com.fossil.fm3.a(r1)
            r8.a(r1)
            java.util.Map<java.lang.Integer, java.lang.Long> r1 = r7.f
            if (r1 != 0) goto L_0x002f
            r1 = 0
            goto L_0x007a
        L_0x002f:
            java.util.ArrayList r1 = new java.util.ArrayList
            java.util.Map<java.lang.Integer, java.lang.Long> r2 = r7.f
            int r2 = r2.size()
            r1.<init>(r2)
            java.util.Map<java.lang.Integer, java.lang.Long> r2 = r7.f
            java.util.Set r2 = r2.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0044:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x007a
            java.lang.Object r3 = r2.next()
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r3 = r3.intValue()
            com.fossil.qp2$a r4 = com.fossil.qp2.s()
            r4.a(r3)
            java.util.Map<java.lang.Integer, java.lang.Long> r5 = r7.f
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            java.lang.Object r3 = r5.get(r3)
            java.lang.Long r3 = (java.lang.Long) r3
            long r5 = r3.longValue()
            r4.a(r5)
            com.fossil.jx2 r3 = r4.g()
            com.fossil.bw2 r3 = (com.fossil.bw2) r3
            com.fossil.qp2 r3 = (com.fossil.qp2) r3
            r1.add(r3)
            goto L_0x0044
        L_0x007a:
            r8.c(r1)
            java.util.Map<java.lang.Integer, java.util.List<java.lang.Long>> r1 = r7.g
            if (r1 != 0) goto L_0x0086
            java.util.List r1 = java.util.Collections.emptyList()
            goto L_0x00ce
        L_0x0086:
            java.util.ArrayList r1 = new java.util.ArrayList
            java.util.Map<java.lang.Integer, java.util.List<java.lang.Long>> r2 = r7.g
            int r2 = r2.size()
            r1.<init>(r2)
            java.util.Map<java.lang.Integer, java.util.List<java.lang.Long>> r2 = r7.g
            java.util.Set r2 = r2.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x009b:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x00ce
            java.lang.Object r3 = r2.next()
            java.lang.Integer r3 = (java.lang.Integer) r3
            com.fossil.yp2$a r4 = com.fossil.yp2.s()
            int r5 = r3.intValue()
            r4.a(r5)
            java.util.Map<java.lang.Integer, java.util.List<java.lang.Long>> r5 = r7.g
            java.lang.Object r3 = r5.get(r3)
            java.util.List r3 = (java.util.List) r3
            if (r3 == 0) goto L_0x00c2
            java.util.Collections.sort(r3)
            r4.a(r3)
        L_0x00c2:
            com.fossil.jx2 r3 = r4.g()
            com.fossil.bw2 r3 = (com.fossil.bw2) r3
            com.fossil.yp2 r3 = (com.fossil.yp2) r3
            r1.add(r3)
            goto L_0x009b
        L_0x00ce:
            r8.d(r1)
            r0.a(r8)
            com.fossil.jx2 r8 = r0.g()
            com.fossil.bw2 r8 = (com.fossil.bw2) r8
            com.fossil.pp2 r8 = (com.fossil.pp2) r8
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qm3.a(int):com.fossil.pp2");
    }
}
