package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht0 implements Parcelable.Creator<cv0> {
    @DexIgnore
    public /* synthetic */ ht0(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public cv0 createFromParcel(Parcel parcel) {
        return new cv0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public cv0[] newArray(int i) {
        return new cv0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public cv0 m25createFromParcel(Parcel parcel) {
        return new cv0(parcel, null);
    }
}
