package com.fossil;

import com.fossil.ql4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt5 extends ql4<b, c, ql4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.b {
        @DexIgnore
        public /* final */ List<at5> a;

        @DexIgnore
        public b(List<at5> list) {
            ee7.b(list, "appWrapperList");
            this.a = list;
        }

        @DexIgnore
        public final List<at5> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a(null);
        String simpleName = mt5.class.getSimpleName();
        ee7.a((Object) simpleName, "SaveAppsNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public mt5(NotificationsRepository notificationsRepository) {
        ee7.b(notificationsRepository, "notificationsRepository");
        jw3.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        ee7.a((Object) notificationsRepository, "checkNotNull(notificatio\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<at5> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (at5 at5 : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(at5.getCurrentHandGroup());
                InstalledApp installedApp = at5.getInstalledApp();
                String str = null;
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved: App name = ");
                InstalledApp installedApp2 = at5.getInstalledApp();
                if (installedApp2 != null) {
                    str = installedApp2.getIdentifier();
                }
                sb.append(str);
                sb.append(", hour = ");
                sb.append(at5.getCurrentHandGroup());
                local.d(str2, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        ee7.b(bVar, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (at5 at5 : bVar.a()) {
            InstalledApp installedApp = at5.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                ee7.a();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList.add(at5);
            } else {
                arrayList2.add(at5);
            }
        }
        a(arrayList2);
        b(arrayList);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAppsNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<at5> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (at5 at5 : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = at5.getInstalledApp();
                Integer num = null;
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(at5.getCurrentHandGroup());
                    InstalledApp installedApp2 = at5.getInstalledApp();
                    String identifier = installedApp2 != null ? installedApp2.getIdentifier() : null;
                    if (identifier != null) {
                        appFilter.setType(identifier);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Removed: App name = ");
                        InstalledApp installedApp3 = at5.getInstalledApp();
                        sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                        sb.append(", row id = ");
                        InstalledApp installedApp4 = at5.getInstalledApp();
                        if (installedApp4 != null) {
                            num = Integer.valueOf(installedApp4.getDbRowId());
                        }
                        sb.append(num);
                        sb.append(", hour = ");
                        sb.append(at5.getCurrentHandGroup());
                        local.d(str, sb.toString());
                        arrayList.add(appFilter);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }
}
