package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl6 implements Factory<bl6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public cl6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static cl6 a(Provider<ThemeRepository> provider) {
        return new cl6(provider);
    }

    @DexIgnore
    public static bl6 a(ThemeRepository themeRepository) {
        return new bl6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public bl6 get() {
        return a(this.a.get());
    }
}
