package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ew3 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ew3 {
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ew3 ew3, String str) {
            super(ew3, null);
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.ew3
        public CharSequence a(Object obj) {
            return obj == null ? this.b : ew3.this.a(obj);
        }

        @DexIgnore
        @Override // com.fossil.ew3
        public ew3 a(String str) {
            throw new UnsupportedOperationException("already specified useForNull");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ ew3 a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public /* synthetic */ b(ew3 ew3, String str, a aVar) {
            this(ew3, str);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder a(StringBuilder sb, Map<?, ?> map) {
            a(sb, map.entrySet());
            return sb;
        }

        @DexIgnore
        public b(ew3 ew3, String str) {
            this.a = ew3;
            jw3.a(str);
            this.b = str;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public <A extends Appendable> A a(A a2, Iterator<? extends Map.Entry<?, ?>> it) throws IOException {
            jw3.a(a2);
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                a2.append(this.a.a(entry.getKey()));
                a2.append(this.b);
                a2.append(this.a.a(entry.getValue()));
                while (it.hasNext()) {
                    a2.append(this.a.a);
                    Map.Entry entry2 = (Map.Entry) it.next();
                    a2.append(this.a.a(entry2.getKey()));
                    a2.append(this.b);
                    a2.append(this.a.a(entry2.getValue()));
                }
            }
            return a2;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder a(StringBuilder sb, Iterable<? extends Map.Entry<?, ?>> iterable) {
            a(sb, iterable.iterator());
            return sb;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder a(StringBuilder sb, Iterator<? extends Map.Entry<?, ?>> it) {
            try {
                a((Appendable) sb, it);
                return sb;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }

    @DexIgnore
    public /* synthetic */ ew3(ew3 ew3, a aVar) {
        this(ew3);
    }

    @DexIgnore
    public static ew3 c(String str) {
        return new ew3(str);
    }

    @DexIgnore
    public b b(String str) {
        return new b(this, str, null);
    }

    @DexIgnore
    public ew3(String str) {
        jw3.a(str);
        this.a = str;
    }

    @DexIgnore
    public static ew3 a(char c) {
        return new ew3(String.valueOf(c));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <A extends Appendable> A a(A a2, Iterator<?> it) throws IOException {
        jw3.a(a2);
        if (it.hasNext()) {
            a2.append(a(it.next()));
            while (it.hasNext()) {
                a2.append(this.a);
                a2.append(a(it.next()));
            }
        }
        return a2;
    }

    @DexIgnore
    public ew3(ew3 ew3) {
        this.a = ew3.a;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final StringBuilder a(StringBuilder sb, Iterable<?> iterable) {
        a(sb, iterable.iterator());
        return sb;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final StringBuilder a(StringBuilder sb, Iterator<?> it) {
        try {
            a((Appendable) sb, it);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final String a(Iterable<?> iterable) {
        return a(iterable.iterator());
    }

    @DexIgnore
    public final String a(Iterator<?> it) {
        StringBuilder sb = new StringBuilder();
        a(sb, it);
        return sb.toString();
    }

    @DexIgnore
    public final String a(Object[] objArr) {
        return a((Iterable<?>) Arrays.asList(objArr));
    }

    @DexIgnore
    public ew3 a(String str) {
        jw3.a(str);
        return new a(this, str);
    }

    @DexIgnore
    public CharSequence a(Object obj) {
        jw3.a(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }
}
