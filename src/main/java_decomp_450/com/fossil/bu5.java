package com.fossil;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bu5 extends dl4<au5> {
    @DexIgnore
    void R(boolean z);

    @DexIgnore
    void a(Cursor cursor);

    @DexIgnore
    void a(List<bt5> list, FilterQueryProvider filterQueryProvider);

    @DexIgnore
    void close();

    @DexIgnore
    void s();
}
