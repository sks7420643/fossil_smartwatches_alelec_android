package com.fossil;

import android.os.Bundle;
import com.fossil.a12;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e32 implements a12.b {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ e22 b;
    @DexIgnore
    public /* final */ /* synthetic */ c32 c;

    @DexIgnore
    public e32(c32 c32, AtomicReference atomicReference, e22 e22) {
        this.c = c32;
        this.a = atomicReference;
        this.b = e22;
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void a(int i) {
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void b(Bundle bundle) {
        this.c.a((a12) this.a.get(), this.b, true);
    }
}
