package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp0 extends zk0 {
    @DexIgnore
    public int C;
    @DexIgnore
    public h41 D;
    @DexIgnore
    public /* final */ ArrayList<ul0> E; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.DEVICE_CONFIG}));
    @DexIgnore
    public /* final */ x91[] F;

    @DexIgnore
    public bp0(ri1 ri1, en0 en0, String str, x91[] x91Arr) {
        super(ri1, en0, wm0.S, str, false, 16);
        this.F = x91Arr;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        h41 h41 = this.D;
        return h41 != null ? h41 : new h41(0, 0, 0);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        m();
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.l4;
        h41 h41 = this.D;
        return yz0.a(k, r51, h41 != null ? h41.a() : null);
    }

    @DexIgnore
    public final void m() {
        int i = this.C;
        x91[] x91Arr = this.F;
        if (i < x91Arr.length) {
            zk0.a(this, new xh1(x91Arr[i], ((zk0) this).w), new jl0(this), new fn0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        } else {
            a(eu0.a(((zk0) this).v, null, is0.EXCHANGED_VALUE_NOT_SATISFIED, null, 5));
        }
    }
}
