package com.fossil;

import com.facebook.internal.ServerProtocol;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yr7 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public List args; // = new LinkedList();
    @DexIgnore
    public List options; // = new ArrayList();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.fs7 a(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.String r4 = com.fossil.os7.b(r4)
            java.util.List r0 = r3.options
            java.util.Iterator r0 = r0.iterator()
        L_0x000a:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x002c
            java.lang.Object r1 = r0.next()
            com.fossil.fs7 r1 = (com.fossil.fs7) r1
            java.lang.String r2 = r1.getOpt()
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0021
            return r1
        L_0x0021:
            java.lang.String r2 = r1.getLongOpt()
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x000a
            return r1
        L_0x002c:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yr7.a(java.lang.String):com.fossil.fs7");
    }

    @DexIgnore
    public void addArg(String str) {
        this.args.add(str);
    }

    @DexIgnore
    public void addOption(fs7 fs7) {
        this.options.add(fs7);
    }

    @DexIgnore
    public List getArgList() {
        return this.args;
    }

    @DexIgnore
    public String[] getArgs() {
        String[] strArr = new String[this.args.size()];
        this.args.toArray(strArr);
        return strArr;
    }

    @DexIgnore
    public Object getOptionObject(String str) {
        try {
            return getParsedOptionValue(str);
        } catch (js7 e) {
            PrintStream printStream = System.err;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Exception found converting ");
            stringBuffer.append(str);
            stringBuffer.append(" to desired type: ");
            stringBuffer.append(e.getMessage());
            printStream.println(stringBuffer.toString());
            return null;
        }
    }

    @DexIgnore
    public Properties getOptionProperties(String str) {
        Properties properties = new Properties();
        for (fs7 fs7 : this.options) {
            if (str.equals(fs7.getOpt()) || str.equals(fs7.getLongOpt())) {
                List valuesList = fs7.getValuesList();
                if (valuesList.size() >= 2) {
                    properties.put(valuesList.get(0), valuesList.get(1));
                } else if (valuesList.size() == 1) {
                    properties.put(valuesList.get(0), ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                }
            }
        }
        return properties;
    }

    @DexIgnore
    public String getOptionValue(String str) {
        String[] optionValues = getOptionValues(str);
        if (optionValues == null) {
            return null;
        }
        return optionValues[0];
    }

    @DexIgnore
    public String[] getOptionValues(String str) {
        ArrayList arrayList = new ArrayList();
        for (fs7 fs7 : this.options) {
            if (str.equals(fs7.getOpt()) || str.equals(fs7.getLongOpt())) {
                arrayList.addAll(fs7.getValuesList());
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    @DexIgnore
    public fs7[] getOptions() {
        List list = this.options;
        return (fs7[]) list.toArray(new fs7[list.size()]);
    }

    @DexIgnore
    public Object getParsedOptionValue(String str) throws js7 {
        String optionValue = getOptionValue(str);
        fs7 a = a(str);
        if (a == null) {
            return null;
        }
        Object type = a.getType();
        if (optionValue == null) {
            return null;
        }
        return ms7.a(optionValue, type);
    }

    @DexIgnore
    public boolean hasOption(String str) {
        return this.options.contains(a(str));
    }

    @DexIgnore
    public Iterator iterator() {
        return this.options.iterator();
    }

    @DexIgnore
    public boolean hasOption(char c) {
        return hasOption(String.valueOf(c));
    }

    @DexIgnore
    public Object getOptionObject(char c) {
        return getOptionObject(String.valueOf(c));
    }

    @DexIgnore
    public String getOptionValue(char c) {
        return getOptionValue(String.valueOf(c));
    }

    @DexIgnore
    public String getOptionValue(String str, String str2) {
        String optionValue = getOptionValue(str);
        return optionValue != null ? optionValue : str2;
    }

    @DexIgnore
    public String getOptionValue(char c, String str) {
        return getOptionValue(String.valueOf(c), str);
    }

    @DexIgnore
    public String[] getOptionValues(char c) {
        return getOptionValues(String.valueOf(c));
    }
}
