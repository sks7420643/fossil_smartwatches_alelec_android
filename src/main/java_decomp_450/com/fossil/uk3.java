package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 c;
    @DexIgnore
    public /* final */ /* synthetic */ r43 d;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 e;

    @DexIgnore
    public uk3(ek3 ek3, String str, String str2, nm3 nm3, r43 r43) {
        this.e = ek3;
        this.a = str;
        this.b = str2;
        this.c = nm3;
        this.d = r43;
    }

    @DexIgnore
    public final void run() {
        ArrayList<Bundle> arrayList = new ArrayList<>();
        try {
            bg3 d2 = this.e.d;
            if (d2 == null) {
                this.e.e().t().a("Failed to get conditional properties; not connected to service", this.a, this.b);
                return;
            }
            ArrayList<Bundle> b2 = jm3.b(d2.a(this.a, this.b, this.c));
            this.e.J();
            this.e.j().a(this.d, b2);
        } catch (RemoteException e2) {
            this.e.e().t().a("Failed to get conditional properties; remote exception", this.a, this.b, e2);
        } finally {
            this.e.j().a(this.d, arrayList);
        }
    }
}
