package com.fossil;

import com.fossil.c80;
import com.fossil.r80;
import com.fossil.t80;
import com.fossil.w80;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a90 {
    @DexIgnore
    public /* final */ ArrayList<n80> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements gd7<n80, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ n80 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(n80 n80) {
            super(1);
            this.a = n80;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public Boolean invoke(n80 n80) {
            return Boolean.valueOf(n80.getKey() == this.a.getKey());
        }
    }

    @DexIgnore
    public final void a(n80 n80) {
        ba7.a((List) this.a, (gd7) new a(n80));
        this.a.add(n80);
    }

    @DexIgnore
    public final a90 b(long j) throws IllegalArgumentException {
        a(new h80(j));
        return this;
    }

    @DexIgnore
    public final a90 c(long j) throws IllegalArgumentException {
        a(new i80(j));
        return this;
    }

    @DexIgnore
    public final a90 d(long j) throws IllegalArgumentException {
        a(new k80(j));
        return this;
    }

    @DexIgnore
    public final a90 e(long j) throws IllegalArgumentException {
        a(new l80(j));
        return this;
    }

    @DexIgnore
    public final a90 b(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a(new j80(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final n80[] a() {
        Object[] array = this.a.toArray(new n80[0]);
        if (array != null) {
            return (n80[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final a90 b(int i) throws IllegalArgumentException {
        a(new m80(i));
        return this;
    }

    @DexIgnore
    public final a90 a(z80[] z80Arr) throws IllegalArgumentException {
        a(new y80(z80Arr));
        return this;
    }

    @DexIgnore
    public final a90 a(byte b, c80.a aVar, short s, short s2, c80.b bVar) throws IllegalArgumentException {
        a(new c80(b, aVar, s, s2, bVar));
        return this;
    }

    @DexIgnore
    public final a90 a(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a(new e80(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final a90 a(int i) throws IllegalArgumentException {
        a(new f80(i));
        return this;
    }

    @DexIgnore
    public final a90 a(long j) throws IllegalArgumentException {
        a(new g80(j));
        return this;
    }

    @DexIgnore
    public final a90 a(g90 g90, b90 b90, e90 e90, h90 h90, d90 d90) {
        a(new p80(g90, b90, e90, h90, d90));
        return this;
    }

    @DexIgnore
    public final a90 a(r80.a aVar) {
        a(new r80(aVar));
        return this;
    }

    @DexIgnore
    public final a90 a(byte b, byte b2, byte b3, byte b4, short s, t80.a aVar) throws IllegalArgumentException {
        a(new t80(b, b2, b3, b4, s, aVar));
        return this;
    }

    @DexIgnore
    public final a90 a(short s) throws IllegalArgumentException {
        a(new u80(s));
        return this;
    }

    @DexIgnore
    public final a90 a(long j, short s, short s2) throws IllegalArgumentException {
        a(new v80(j, s, s2));
        return this;
    }

    @DexIgnore
    public final a90 a(w80.a aVar) {
        a(new w80(aVar));
        return this;
    }
}
