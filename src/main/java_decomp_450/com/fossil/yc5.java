package com.fossil;

import android.graphics.Color;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yc5 {
    @DexIgnore
    public static final List<WatchFaceWrapper> a(List<WatchFace> list, List<DianaPresetComplicationSetting> list2) {
        ee7.b(list, "$this$toListOfWatchFaceWrappers");
        ArrayList arrayList = new ArrayList();
        for (WatchFace watchFace : list) {
            arrayList.add(b(watchFace, list2));
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0163  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper b(com.portfolio.platform.data.model.diana.preset.WatchFace r17, java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting> r18) {
        /*
            java.lang.String r0 = "$this$toWatchFaceWrapper"
            r1 = r17
            com.fossil.ee7.b(r1, r0)
            java.lang.String r2 = r17.getId()
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            r3 = 2131231251(0x7f080213, float:1.8078578E38)
            android.graphics.drawable.Drawable r0 = com.fossil.v6.c(r0, r3)
            r4 = 0
            if (r0 == 0) goto L_0x003b
            r0.getIntrinsicHeight()
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            android.graphics.drawable.Drawable r0 = com.fossil.v6.c(r0, r3)
            if (r0 == 0) goto L_0x0033
            int r0 = r0.getMinimumWidth()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x0034
        L_0x0033:
            r0 = r4
        L_0x0034:
            if (r0 == 0) goto L_0x003b
            int r0 = r0.intValue()
            goto L_0x004c
        L_0x003b:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            android.content.res.Resources r0 = r0.getResources()
            r3 = 2131165418(0x7f0700ea, float:1.7945053E38)
            int r0 = r0.getDimensionPixelSize(r3)
        L_0x004c:
            r8 = r0
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            android.content.res.Resources r0 = r0.getResources()
            r3 = 2131165379(0x7f0700c3, float:1.7944973E38)
            int r0 = r0.getDimensionPixelSize(r3)
            com.fossil.ee5 r3 = com.fossil.ee5.a
            java.lang.String r5 = r17.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r6 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            android.graphics.drawable.Drawable r3 = r3.b(r5, r0, r0, r6)
            com.fossil.ee5 r5 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.Background r0 = r17.getBackground()
            com.portfolio.platform.data.model.diana.preset.Data r0 = r0.getData()
            java.lang.String r6 = r0.getPreviewUrl()
            int r9 = r17.getWatchFaceType()
            com.misfit.frameworks.buttonservice.model.FileType r10 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r7 = r8
            android.graphics.drawable.Drawable r0 = r5.a(r6, r7, r8, r9, r10)
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            android.content.res.Resources r5 = r5.getResources()
            r6 = 2131165420(0x7f0700ec, float:1.7945057E38)
            int r5 = r5.getDimensionPixelSize(r6)
            java.util.ArrayList r6 = r17.getRingStyleItems()
            if (r6 == 0) goto L_0x0163
            java.util.Iterator r6 = r6.iterator()
            r7 = r4
            r8 = r7
            r9 = r8
            r10 = r9
            r11 = r10
            r12 = r11
            r13 = r12
        L_0x00a5:
            boolean r14 = r6.hasNext()
            if (r14 == 0) goto L_0x015a
            java.lang.Object r14 = r6.next()
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r14 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r14
            java.lang.String r15 = r14.component1()
            com.portfolio.platform.data.model.diana.preset.RingStyle r14 = r14.component2()
            int r16 = r15.hashCode()
            switch(r16) {
                case -1383228885: goto L_0x0132;
                case 115029: goto L_0x010d;
                case 3317767: goto L_0x00e8;
                case 108511772: goto L_0x00c2;
                default: goto L_0x00c0;
            }
        L_0x00c0:
            goto L_0x0156
        L_0x00c2:
            java.lang.String r1 = "right"
            boolean r1 = r15.equals(r1)
            if (r1 == 0) goto L_0x0156
            com.fossil.ee5 r1 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.Data r7 = r14.getData()
            java.lang.String r7 = r7.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r15 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            android.graphics.drawable.Drawable r1 = r1.b(r7, r5, r5, r15)
            com.portfolio.platform.data.model.diana.preset.MetaData r7 = r14.getMetadata()
            if (r7 == 0) goto L_0x00e5
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r7 = a(r7)
            r11 = r7
        L_0x00e5:
            r7 = r1
            goto L_0x0156
        L_0x00e8:
            java.lang.String r1 = "left"
            boolean r1 = r15.equals(r1)
            if (r1 == 0) goto L_0x0156
            com.fossil.ee5 r1 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.Data r9 = r14.getData()
            java.lang.String r9 = r9.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r15 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            android.graphics.drawable.Drawable r1 = r1.b(r9, r5, r5, r15)
            com.portfolio.platform.data.model.diana.preset.MetaData r9 = r14.getMetadata()
            if (r9 == 0) goto L_0x010b
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r9 = a(r9)
            r13 = r9
        L_0x010b:
            r9 = r1
            goto L_0x0156
        L_0x010d:
            java.lang.String r1 = "top"
            boolean r1 = r15.equals(r1)
            if (r1 == 0) goto L_0x0156
            com.fossil.ee5 r1 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.Data r4 = r14.getData()
            java.lang.String r4 = r4.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r15 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            android.graphics.drawable.Drawable r1 = r1.b(r4, r5, r5, r15)
            com.portfolio.platform.data.model.diana.preset.MetaData r4 = r14.getMetadata()
            if (r4 == 0) goto L_0x0130
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r4 = a(r4)
            r10 = r4
        L_0x0130:
            r4 = r1
            goto L_0x0156
        L_0x0132:
            java.lang.String r1 = "bottom"
            boolean r1 = r15.equals(r1)
            if (r1 == 0) goto L_0x0156
            com.fossil.ee5 r1 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.Data r8 = r14.getData()
            java.lang.String r8 = r8.getPreviewUrl()
            com.misfit.frameworks.buttonservice.model.FileType r15 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            android.graphics.drawable.Drawable r1 = r1.b(r8, r5, r5, r15)
            com.portfolio.platform.data.model.diana.preset.MetaData r8 = r14.getMetadata()
            if (r8 == 0) goto L_0x0155
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r8 = a(r8)
            r12 = r8
        L_0x0155:
            r8 = r1
        L_0x0156:
            r1 = r17
            goto L_0x00a5
        L_0x015a:
            r5 = r4
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            goto L_0x016b
        L_0x0163:
            r5 = r4
            r6 = r5
            r7 = r6
            r8 = r7
            r9 = r8
            r10 = r9
            r11 = r10
            r12 = r11
        L_0x016b:
            java.lang.String r13 = r17.getName()
            com.misfit.frameworks.buttonservice.model.background.BackgroundConfig r14 = a(r17, r18)
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$Companion r1 = com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper.Companion
            com.fossil.qb5$a r4 = com.fossil.qb5.Companion
            int r15 = r17.getWatchFaceType()
            com.fossil.qb5 r15 = r4.a(r15)
            r4 = r0
            com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r0 = r1.createBackgroundWrapper(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return r0
            switch-data {-1383228885->0x0132, 115029->0x010d, 3317767->0x00e8, 108511772->0x00c2, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yc5.b(com.portfolio.platform.data.model.diana.preset.WatchFace, java.util.List):com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0032, code lost:
        if (r0 != null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0041, code lost:
        if (r0 != null) goto L_0x0045;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.misfit.frameworks.buttonservice.model.background.BackgroundConfig a(com.portfolio.platform.data.model.diana.preset.WatchFace r13, java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting> r14) {
        /*
            java.lang.String r0 = "$this$toBackgroundConfig"
            com.fossil.ee7.b(r13, r0)
            int r0 = r13.getWatchFaceType()
            com.fossil.qb5 r1 = com.fossil.qb5.BACKGROUND
            int r1 = r1.getValue()
            java.lang.String r2 = ""
            if (r0 != r1) goto L_0x0035
            com.portfolio.platform.data.model.diana.preset.Background r0 = r13.getBackground()
            com.portfolio.platform.data.model.diana.preset.Data r0 = r0.getData()
            java.lang.String r0 = r0.getUrl()
            if (r0 == 0) goto L_0x0044
            com.fossil.ee5 r1 = com.fossil.ee5.a
            java.lang.String r0 = com.fossil.mg5.a(r0)
            java.lang.String r3 = "StringHelper.fileNameFromUrl(it)"
            com.fossil.ee7.a(r0, r3)
            com.misfit.frameworks.buttonservice.model.FileType r3 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.lang.String r0 = r1.a(r0, r3)
            if (r0 == 0) goto L_0x0044
            goto L_0x0045
        L_0x0035:
            com.portfolio.platform.data.model.diana.preset.Background r0 = r13.getBackground()
            com.portfolio.platform.data.model.diana.preset.Data r0 = r0.getData()
            java.lang.String r0 = r0.getUrl()
            if (r0 == 0) goto L_0x0044
            goto L_0x0045
        L_0x0044:
            r0 = r2
        L_0x0045:
            if (r14 == 0) goto L_0x01ba
            java.util.Iterator r14 = r14.iterator()
            r1 = r2
            r3 = r1
            r4 = r3
        L_0x004e:
            boolean r5 = r14.hasNext()
            if (r5 == 0) goto L_0x01bd
            java.lang.Object r5 = r14.next()
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r5 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r5
            java.lang.String r6 = r5.component1()
            java.lang.String r5 = r5.component2()
            int r7 = r6.hashCode()
            java.lang.String r8 = "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)"
            java.lang.String r9 = "empty"
            r10 = 0
            switch(r7) {
                case -1383228885: goto L_0x0167;
                case 115029: goto L_0x0114;
                case 3317767: goto L_0x00c1;
                case 108511772: goto L_0x006f;
                default: goto L_0x006e;
            }
        L_0x006e:
            goto L_0x004e
        L_0x006f:
            java.lang.String r7 = "right"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x004e
            boolean r5 = com.fossil.ee7.a(r5, r9)
            r5 = r5 ^ 1
            if (r5 == 0) goto L_0x004e
            java.util.ArrayList r5 = r13.getRingStyleItems()
            if (r5 == 0) goto L_0x00a3
            java.util.Iterator r5 = r5.iterator()
        L_0x0089:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00a1
            java.lang.Object r6 = r5.next()
            r9 = r6
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r9 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r9
            java.lang.String r9 = r9.getPosition()
            boolean r9 = com.fossil.ee7.a(r9, r7)
            if (r9 == 0) goto L_0x0089
            r10 = r6
        L_0x00a1:
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r10 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r10
        L_0x00a3:
            if (r10 == 0) goto L_0x004e
            com.fossil.ee5 r1 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.RingStyle r5 = r10.getRingStyle()
            com.portfolio.platform.data.model.diana.preset.Data r5 = r5.getData()
            java.lang.String r5 = r5.getUrl()
            java.lang.String r5 = com.fossil.mg5.a(r5)
            com.fossil.ee7.a(r5, r8)
            com.misfit.frameworks.buttonservice.model.FileType r6 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.lang.String r1 = r1.a(r5, r6)
            goto L_0x004e
        L_0x00c1:
            java.lang.String r7 = "left"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x004e
            boolean r5 = com.fossil.ee7.a(r5, r9)
            r5 = r5 ^ 1
            if (r5 == 0) goto L_0x004e
            java.util.ArrayList r5 = r13.getRingStyleItems()
            if (r5 == 0) goto L_0x00f5
            java.util.Iterator r5 = r5.iterator()
        L_0x00db:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00f3
            java.lang.Object r6 = r5.next()
            r9 = r6
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r9 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r9
            java.lang.String r9 = r9.getPosition()
            boolean r9 = com.fossil.ee7.a(r9, r7)
            if (r9 == 0) goto L_0x00db
            r10 = r6
        L_0x00f3:
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r10 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r10
        L_0x00f5:
            if (r10 == 0) goto L_0x004e
            com.fossil.ee5 r4 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.RingStyle r5 = r10.getRingStyle()
            com.portfolio.platform.data.model.diana.preset.Data r5 = r5.getData()
            java.lang.String r5 = r5.getUrl()
            java.lang.String r5 = com.fossil.mg5.a(r5)
            com.fossil.ee7.a(r5, r8)
            com.misfit.frameworks.buttonservice.model.FileType r6 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.lang.String r4 = r4.a(r5, r6)
            goto L_0x004e
        L_0x0114:
            java.lang.String r7 = "top"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x004e
            boolean r5 = com.fossil.ee7.a(r5, r9)
            r5 = r5 ^ 1
            if (r5 == 0) goto L_0x004e
            java.util.ArrayList r5 = r13.getRingStyleItems()
            if (r5 == 0) goto L_0x0148
            java.util.Iterator r5 = r5.iterator()
        L_0x012e:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0146
            java.lang.Object r6 = r5.next()
            r9 = r6
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r9 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r9
            java.lang.String r9 = r9.getPosition()
            boolean r9 = com.fossil.ee7.a(r9, r7)
            if (r9 == 0) goto L_0x012e
            r10 = r6
        L_0x0146:
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r10 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r10
        L_0x0148:
            if (r10 == 0) goto L_0x004e
            com.fossil.ee5 r2 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.RingStyle r5 = r10.getRingStyle()
            com.portfolio.platform.data.model.diana.preset.Data r5 = r5.getData()
            java.lang.String r5 = r5.getUrl()
            java.lang.String r5 = com.fossil.mg5.a(r5)
            com.fossil.ee7.a(r5, r8)
            com.misfit.frameworks.buttonservice.model.FileType r6 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.lang.String r2 = r2.a(r5, r6)
            goto L_0x004e
        L_0x0167:
            java.lang.String r7 = "bottom"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x004e
            boolean r5 = com.fossil.ee7.a(r5, r9)
            r5 = r5 ^ 1
            if (r5 == 0) goto L_0x004e
            java.util.ArrayList r5 = r13.getRingStyleItems()
            if (r5 == 0) goto L_0x019b
            java.util.Iterator r5 = r5.iterator()
        L_0x0181:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0199
            java.lang.Object r6 = r5.next()
            r9 = r6
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r9 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r9
            java.lang.String r9 = r9.getPosition()
            boolean r9 = com.fossil.ee7.a(r9, r7)
            if (r9 == 0) goto L_0x0181
            r10 = r6
        L_0x0199:
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r10 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r10
        L_0x019b:
            if (r10 == 0) goto L_0x004e
            com.fossil.ee5 r3 = com.fossil.ee5.a
            com.portfolio.platform.data.model.diana.preset.RingStyle r5 = r10.getRingStyle()
            com.portfolio.platform.data.model.diana.preset.Data r5 = r5.getData()
            java.lang.String r5 = r5.getUrl()
            java.lang.String r5 = com.fossil.mg5.a(r5)
            com.fossil.ee7.a(r5, r8)
            com.misfit.frameworks.buttonservice.model.FileType r6 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.lang.String r3 = r3.a(r5, r6)
            goto L_0x004e
        L_0x01ba:
            r1 = r2
            r3 = r1
            r4 = r3
        L_0x01bd:
            com.misfit.frameworks.buttonservice.model.background.BackgroundConfig r13 = new com.misfit.frameworks.buttonservice.model.background.BackgroundConfig
            long r6 = java.lang.System.currentTimeMillis()
            com.misfit.frameworks.buttonservice.model.background.BackgroundImgData r8 = new com.misfit.frameworks.buttonservice.model.background.BackgroundImgData
            java.lang.String r14 = "main_bg"
            r8.<init>(r14, r0)
            com.misfit.frameworks.buttonservice.model.background.BackgroundImgData r9 = new com.misfit.frameworks.buttonservice.model.background.BackgroundImgData
            java.lang.String r14 = "top_bg"
            r9.<init>(r14, r2)
            com.misfit.frameworks.buttonservice.model.background.BackgroundImgData r10 = new com.misfit.frameworks.buttonservice.model.background.BackgroundImgData
            java.lang.String r14 = "right_bg"
            r10.<init>(r14, r1)
            com.misfit.frameworks.buttonservice.model.background.BackgroundImgData r11 = new com.misfit.frameworks.buttonservice.model.background.BackgroundImgData
            java.lang.String r14 = "bottom_bg"
            r11.<init>(r14, r3)
            com.misfit.frameworks.buttonservice.model.background.BackgroundImgData r12 = new com.misfit.frameworks.buttonservice.model.background.BackgroundImgData
            java.lang.String r14 = "left_bg"
            r12.<init>(r14, r4)
            r5 = r13
            r5.<init>(r6, r8, r9, r10, r11, r12)
            return r13
            switch-data {-1383228885->0x0167, 115029->0x0114, 3317767->0x00c1, 108511772->0x006f, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yc5.a(com.portfolio.platform.data.model.diana.preset.WatchFace, java.util.List):com.misfit.frameworks.buttonservice.model.background.BackgroundConfig");
    }

    @DexIgnore
    public static final WatchFaceWrapper.MetaData a(MetaData metaData) {
        Integer num;
        Integer num2;
        Integer num3;
        ee7.b(metaData, "$this$toColorMetaData");
        Integer num4 = null;
        try {
            num = Integer.valueOf(Color.parseColor(metaData.getSelectedForegroundColor()));
        } catch (Exception e) {
            e.printStackTrace();
            num = null;
        }
        try {
            num2 = Integer.valueOf(Color.parseColor(metaData.getSelectedBackgroundColor()));
        } catch (Exception e2) {
            e2.printStackTrace();
            num2 = null;
        }
        try {
            num3 = Integer.valueOf(Color.parseColor(metaData.getUnselectedForegroundColor()));
        } catch (Exception e3) {
            e3.printStackTrace();
            num3 = null;
        }
        try {
            num4 = Integer.valueOf(Color.parseColor(metaData.getUnselectedBackgroundColor()));
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        return new WatchFaceWrapper.MetaData(num, num2, num3, num4);
    }
}
