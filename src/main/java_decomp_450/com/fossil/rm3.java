package com.fossil;

public final /* synthetic */ class rm3 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    /* JADX WARNING: Can't wrap try/catch for region: R(21:0|(2:1|2)|3|(2:5|6)|7|9|10|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0044 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x004e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0058 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0062 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006d */
    /*
    static {
        /*
            com.fossil.cp2$b[] r0 = com.fossil.cp2.b.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.fossil.rm3.b = r0
            r1 = 1
            com.fossil.cp2$b r2 = com.fossil.cp2.b.LESS_THAN     // Catch:{ NoSuchFieldError -> 0x0012 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
        L_0x0012:
            r0 = 2
            int[] r2 = com.fossil.rm3.b     // Catch:{ NoSuchFieldError -> 0x001d }
            com.fossil.cp2$b r3 = com.fossil.cp2.b.GREATER_THAN     // Catch:{ NoSuchFieldError -> 0x001d }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
            r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
        L_0x001d:
            r2 = 3
            int[] r3 = com.fossil.rm3.b     // Catch:{ NoSuchFieldError -> 0x0028 }
            com.fossil.cp2$b r4 = com.fossil.cp2.b.EQUAL     // Catch:{ NoSuchFieldError -> 0x0028 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
        L_0x0028:
            r3 = 4
            int[] r4 = com.fossil.rm3.b     // Catch:{ NoSuchFieldError -> 0x0033 }
            com.fossil.cp2$b r5 = com.fossil.cp2.b.BETWEEN     // Catch:{ NoSuchFieldError -> 0x0033 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
            r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0033 }
        L_0x0033:
            com.fossil.ep2$a[] r4 = com.fossil.ep2.a.values()
            int r4 = r4.length
            int[] r4 = new int[r4]
            com.fossil.rm3.a = r4
            com.fossil.ep2$a r5 = com.fossil.ep2.a.REGEXP     // Catch:{ NoSuchFieldError -> 0x0044 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0044 }
            r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x0044 }
        L_0x0044:
            int[] r1 = com.fossil.rm3.a     // Catch:{ NoSuchFieldError -> 0x004e }
            com.fossil.ep2$a r4 = com.fossil.ep2.a.BEGINS_WITH     // Catch:{ NoSuchFieldError -> 0x004e }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x004e }
            r1[r4] = r0     // Catch:{ NoSuchFieldError -> 0x004e }
        L_0x004e:
            int[] r0 = com.fossil.rm3.a     // Catch:{ NoSuchFieldError -> 0x0058 }
            com.fossil.ep2$a r1 = com.fossil.ep2.a.ENDS_WITH     // Catch:{ NoSuchFieldError -> 0x0058 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0058 }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0058 }
        L_0x0058:
            int[] r0 = com.fossil.rm3.a     // Catch:{ NoSuchFieldError -> 0x0062 }
            com.fossil.ep2$a r1 = com.fossil.ep2.a.PARTIAL     // Catch:{ NoSuchFieldError -> 0x0062 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
            r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0062 }
        L_0x0062:
            int[] r0 = com.fossil.rm3.a     // Catch:{ NoSuchFieldError -> 0x006d }
            com.fossil.ep2$a r1 = com.fossil.ep2.a.EXACT     // Catch:{ NoSuchFieldError -> 0x006d }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006d }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006d }
        L_0x006d:
            int[] r0 = com.fossil.rm3.a     // Catch:{ NoSuchFieldError -> 0x0078 }
            com.fossil.ep2$a r1 = com.fossil.ep2.a.IN_LIST     // Catch:{ NoSuchFieldError -> 0x0078 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
            r2 = 6
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
        L_0x0078:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rm3.<clinit>():void");
    }
    */
}
