package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz2 {
    @DexIgnore
    public static /* final */ fz2 a;

    /*
    static {
        fz2 fz2;
        if (!(bz2.a() && bz2.b()) || mu2.a()) {
            fz2 = new ez2();
        } else {
            fz2 = new gz2();
        }
        a = fz2;
    }
    */

    @DexIgnore
    public static boolean a(byte[] bArr) {
        return a.b(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static int b(int i) {
        if (i > -12) {
            return -1;
        }
        return i;
    }

    @DexIgnore
    public static int b(int i, int i2) {
        if (i > -12 || i2 > -65) {
            return -1;
        }
        return i ^ (i2 << 8);
    }

    @DexIgnore
    public static int b(int i, int i2, int i3) {
        if (i > -12 || i2 > -65 || i3 > -65) {
            return -1;
        }
        return (i ^ (i2 << 8)) ^ (i3 << 16);
    }

    @DexIgnore
    public static String b(byte[] bArr, int i, int i2) throws iw2 {
        return a.a(bArr, i, i2);
    }

    @DexIgnore
    public static int d(byte[] bArr, int i, int i2) {
        byte b = bArr[i - 1];
        int i3 = i2 - i;
        if (i3 == 0) {
            return b(b);
        }
        if (i3 == 1) {
            return b(b, bArr[i]);
        }
        if (i3 == 2) {
            return b(b, bArr[i], bArr[i + 1]);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static boolean a(byte[] bArr, int i, int i2) {
        return a.b(bArr, i, i2);
    }

    @DexIgnore
    public static int a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < '\u0080') {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < '\u0800') {
                i3 += ('\u007f' - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < '\u0800') {
                        i += ('\u007f' - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if ('\ud800' <= charAt2 && charAt2 <= '\udfff') {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                throw new hz2(i2, length2);
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(((long) i3) + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public static int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        return a.a(charSequence, bArr, i, i2);
    }
}
