package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl7 {
    /*
    static {
        Object obj;
        try {
            s87.a aVar = s87.Companion;
            obj = s87.m60constructorimpl(new pl7(a(Looper.getMainLooper(), true), "Main"));
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            obj = s87.m60constructorimpl(t87.a(th));
        }
        if (s87.m65isFailureimpl(obj)) {
            obj = null;
        }
        ql7 ql7 = (ql7) obj;
    }
    */

    @DexIgnore
    public static final Handler a(Looper looper, boolean z) {
        int i;
        if (!z || (i = Build.VERSION.SDK_INT) < 16) {
            return new Handler(looper);
        }
        if (i >= 28) {
            Object invoke = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, looper);
            if (invoke != null) {
                return (Handler) invoke;
            }
            throw new x87("null cannot be cast to non-null type android.os.Handler");
        }
        try {
            return (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, true);
        } catch (NoSuchMethodException unused) {
            return new Handler(looper);
        }
    }
}
