package com.fossil;

import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao6 implements Factory<zn6> {
    @DexIgnore
    public /* final */ Provider<sn6> a;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> b;

    @DexIgnore
    public ao6(Provider<sn6> provider, Provider<WorkoutSettingRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ao6 a(Provider<sn6> provider, Provider<WorkoutSettingRepository> provider2) {
        return new ao6(provider, provider2);
    }

    @DexIgnore
    public static zn6 a(sn6 sn6, WorkoutSettingRepository workoutSettingRepository) {
        return new zn6(sn6, workoutSettingRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public zn6 get() {
        return a(this.a.get(), this.b.get());
    }
}
