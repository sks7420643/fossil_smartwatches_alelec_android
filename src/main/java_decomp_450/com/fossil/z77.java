package com.fossil;

import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z77<T> implements Provider<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Provider<T> a;
    @DexIgnore
    public volatile Object b; // = c;

    @DexIgnore
    public z77(Provider<T> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Object a(Object obj, Object obj2) {
        if (!(obj != c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public T get() {
        T t;
        T t2 = (T) this.b;
        if (t2 == c) {
            synchronized (this) {
                T t3 = this.b;
                if (t3 == c) {
                    t3 = this.a.get();
                    a(this.b, t3);
                    this.b = t3;
                    this.a = null;
                }
            }
        }
        return t;
    }

    @DexIgnore
    public static <P extends Provider<T>, T> Provider<T> a(P p) {
        c87.a(p);
        if (p instanceof z77) {
            return p;
        }
        return new z77(p);
    }
}
