package com.fossil;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t07 implements ViewTreeObserver.OnPreDrawListener {
    @DexIgnore
    public /* final */ h17 a;
    @DexIgnore
    public /* final */ WeakReference<ImageView> b;
    @DexIgnore
    public q07 c;

    @DexIgnore
    public t07(h17 h17, ImageView imageView, q07 q07) {
        this.a = h17;
        this.b = new WeakReference<>(imageView);
        this.c = q07;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @DexIgnore
    public void a() {
        this.c = null;
        ImageView imageView = this.b.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        ImageView imageView = this.b.get();
        if (imageView == null) {
            return true;
        }
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        int width = imageView.getWidth();
        int height = imageView.getHeight();
        if (width > 0 && height > 0) {
            viewTreeObserver.removeOnPreDrawListener(this);
            h17 h17 = this.a;
            h17.c();
            h17.a(width, height);
            h17.a(imageView, this.c);
        }
        return true;
    }
}
