package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.AnimationImageView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl4 extends RecyclerView.g<b> {
    @DexIgnore
    public static /* final */ String c; // = c;
    @DexIgnore
    public ArrayList<Explore> a; // = new ArrayList<>();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ AnimationImageView c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(View view) {
            super(view);
            ee7.b(view, "itemView");
            View findViewById = view.findViewById(2131362517);
            ee7.a((Object) findViewById, "itemView.findViewById(R.id.ftv_title)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362377);
            ee7.a((Object) findViewById2, "itemView.findViewById(R.id.ftv_desc)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362661);
            ee7.a((Object) findViewById3, "itemView.findViewById(R.id.iv_device)");
            this.c = (AnimationImageView) findViewById3;
        }

        @DexIgnore
        public final AnimationImageView a() {
            return this.c;
        }

        @DexIgnore
        public final void a(Explore explore) {
            if (explore != null) {
                this.a.setText(explore.getTitle());
                this.b.setText(explore.getDescription());
                Explore.ExploreType exploreType = explore.getExploreType();
                if (exploreType != null) {
                    int i = xl4.a[exploreType.ordinal()];
                    if (i == 1) {
                        AnimationImageView animationImageView = this.c;
                        String string = PortfolioApp.g0.c().getString(2131887231);
                        ee7.a((Object) string, "PortfolioApp.instance.ge\u2026ng.animation_wrist_flick)");
                        animationImageView.a(string, 1, 80, 30, MFNetworkReturnCode.BAD_REQUEST, 2000);
                    } else if (i == 2) {
                        AnimationImageView animationImageView2 = this.c;
                        String string2 = PortfolioApp.g0.c().getString(2131887229);
                        ee7.a((Object) string2, "PortfolioApp.instance.ge\u2026ing.animation_double_tap)");
                        animationImageView2.a(string2, 1, 50, 30, 600, 2400);
                    } else if (i == 3) {
                        AnimationImageView animationImageView3 = this.c;
                        String string3 = PortfolioApp.g0.c().getString(2131887230);
                        ee7.a((Object) string3, "PortfolioApp.instance.ge\u2026animation_press_and_hold)");
                        animationImageView3.a(string3, 1, 50, 30, 1000, 3000);
                    }
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final void a(List<? extends Explore> list) {
        ee7.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: b */
    public void onViewDetachedFromWindow(b bVar) {
        ee7.b(bVar, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewDetachedFromWindow");
        bVar.a().d();
        super.onViewDetachedFromWindow(bVar);
    }

    @DexIgnore
    public final void c() {
        AnimationImageView a2;
        FLogger.INSTANCE.getLocal().d(c, "onStopAnimation");
        b bVar = this.b;
        if (bVar != null && (a2 = bVar.a()) != null) {
            a2.d();
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558719, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new b(inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "welcomeViewHolder");
        bVar.a(this.a.get(i));
    }

    @DexIgnore
    /* renamed from: a */
    public void onViewAttachedToWindow(b bVar) {
        ee7.b(bVar, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewAttachedToWindow");
        this.b = bVar;
        bVar.a().c();
        super.onViewAttachedToWindow(bVar);
    }
}
