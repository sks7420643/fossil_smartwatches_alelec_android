package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.fossil.blesdk.device.logic.phase.SyncPhase$onAllFileRead$1", f = "SyncPhase.kt", l = {}, m = "invokeSuspend")
public final class qs0 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ /* synthetic */ b31 c;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qs0(b31 b31, ArrayList arrayList, fb7 fb7) {
        super(2, fb7);
        this.c = b31;
        this.d = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        qs0 qs0 = new qs0(this.c, this.d, fb7);
        qs0.a = (yi7) obj;
        return qs0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        qs0 qs0 = new qs0(this.c, this.d, fb7);
        qs0.a = yi7;
        return qs0.invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.b == 0) {
            t87.a(obj);
            this.c.b(this.d);
            b31 b31 = this.c;
            b31.a(eu0.a(((zk0) b31).v, null, is0.SUCCESS, null, 5));
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
