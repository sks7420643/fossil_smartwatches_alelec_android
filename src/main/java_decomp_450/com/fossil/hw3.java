package com.fossil;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hw3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Iterable<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hw3$a$a")
        /* renamed from: com.fossil.hw3$a$a  reason: collision with other inner class name */
        public class C0079a extends yv3<T> {
            @DexIgnore
            public /* final */ Iterator<? extends hw3<? extends T>> c;

            @DexIgnore
            public C0079a() {
                Iterator<T> it = a.this.a.iterator();
                jw3.a(it);
                this.c = it;
            }

            @DexIgnore
            @Override // com.fossil.yv3
            public T a() {
                while (this.c.hasNext()) {
                    hw3 hw3 = (hw3) this.c.next();
                    if (hw3.isPresent()) {
                        return (T) hw3.get();
                    }
                }
                return (T) b();
            }
        }

        @DexIgnore
        public a(Iterable iterable) {
            this.a = iterable;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return new C0079a();
        }
    }

    @DexIgnore
    public static <T> hw3<T> absent() {
        return xv3.withType();
    }

    @DexIgnore
    public static <T> hw3<T> fromNullable(T t) {
        return t == null ? absent() : new mw3(t);
    }

    @DexIgnore
    public static <T> hw3<T> of(T t) {
        jw3.a(t);
        return new mw3(t);
    }

    @DexIgnore
    public static <T> Iterable<T> presentInstances(Iterable<? extends hw3<? extends T>> iterable) {
        jw3.a(iterable);
        return new a(iterable);
    }

    @DexIgnore
    public abstract Set<T> asSet();

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public abstract T get();

    @DexIgnore
    public abstract int hashCode();

    @DexIgnore
    public abstract boolean isPresent();

    @DexIgnore
    public abstract hw3<T> or(hw3<? extends T> hw3);

    @DexIgnore
    public abstract T or(nw3<? extends T> nw3);

    @DexIgnore
    public abstract T or(T t);

    @DexIgnore
    public abstract T orNull();

    @DexIgnore
    public abstract String toString();

    @DexIgnore
    public abstract <V> hw3<V> transform(cw3<? super T, V> cw3);
}
