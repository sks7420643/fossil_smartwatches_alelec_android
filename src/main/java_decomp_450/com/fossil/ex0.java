package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex0 extends eo0 {
    @DexIgnore
    public /* final */ nm1 j; // = nm1.HIGH;
    @DexIgnore
    public p91 k; // = p91.DISCONNECTED;

    @DexIgnore
    public ex0(cx0 cx0) {
        super(aq0.l, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.a();
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public nm1 b() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.k;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(s91 s91) {
        lk0 lk0;
        c(s91);
        x71 x71 = s91.a;
        if (x71.a != z51.SUCCESS) {
            lk0 a = lk0.d.a(x71);
            lk0 = lk0.a(((eo0) this).d, null, a.b, a.c, 1);
        } else if (this.k == p91.CONNECTED) {
            lk0 = lk0.a(((eo0) this).d, null, oi0.a, null, 5);
        } else {
            lk0 = lk0.a(((eo0) this).d, null, oi0.d, null, 5);
        }
        ((eo0) this).d = lk0;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        p91 p91;
        return (s91 instanceof tt0) && ((p91 = ((tt0) s91).b) == p91.CONNECTED || p91 == p91.DISCONNECTED);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.k = ((tt0) s91).b;
    }
}
