package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m44 {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ File a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends JSONObject {
        @DexIgnore
        public /* final */ /* synthetic */ u44 a;

        @DexIgnore
        public a(u44 u44) throws JSONException {
            this.a = u44;
            put(ButtonService.USER_ID, this.a.b());
        }
    }

    @DexIgnore
    public m44(File file) {
        this.a = file;
    }

    @DexIgnore
    public static u44 d(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        u44 u44 = new u44();
        u44.a(a(jSONObject, ButtonService.USER_ID));
        return u44;
    }

    @DexIgnore
    public void a(String str, u44 u44) {
        File b2 = b(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a2 = a(u44);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(b2), b));
            try {
                bufferedWriter2.write(a2);
                bufferedWriter2.flush();
                t34.a((Closeable) bufferedWriter2, "Failed to close user metadata file.");
            } catch (Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    z24.a().b("Error serializing user metadata.", e);
                    t34.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th) {
                    th = th;
                    t34.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                t34.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            z24.a().b("Error serializing user metadata.", e);
            t34.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
        }
    }

    @DexIgnore
    public File b(String str) {
        File file = this.a;
        return new File(file, str + "user" + ".meta");
    }

    @DexIgnore
    public u44 c(String str) {
        File b2 = b(str);
        if (!b2.exists()) {
            return new u44();
        }
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(b2);
            try {
                u44 d = d(t34.b(fileInputStream2));
                t34.a((Closeable) fileInputStream2, "Failed to close user metadata file.");
                return d;
            } catch (Exception e) {
                e = e;
                fileInputStream = fileInputStream2;
                try {
                    z24.a().b("Error deserializing user metadata.", e);
                    t34.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return new u44();
                } catch (Throwable th) {
                    th = th;
                    t34.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                t34.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            z24.a().b("Error deserializing user metadata.", e);
            t34.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return new u44();
        }
    }

    @DexIgnore
    public void a(String str, Map<String, String> map) {
        File a2 = a(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a3 = a(map);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(a2), b));
            try {
                bufferedWriter2.write(a3);
                bufferedWriter2.flush();
                t34.a((Closeable) bufferedWriter2, "Failed to close key/value metadata file.");
            } catch (Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    z24.a().b("Error serializing key/value metadata.", e);
                    t34.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th) {
                    th = th;
                    t34.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                t34.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            z24.a().b("Error serializing key/value metadata.", e);
            t34.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
        }
    }

    @DexIgnore
    public File a(String str) {
        File file = this.a;
        return new File(file, str + "keys" + ".meta");
    }

    @DexIgnore
    public static String a(u44 u44) throws JSONException {
        return new a(u44).toString();
    }

    @DexIgnore
    public static String a(Map<String, String> map) throws JSONException {
        return new JSONObject(map).toString();
    }

    @DexIgnore
    public static String a(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, null);
        }
        return null;
    }
}
