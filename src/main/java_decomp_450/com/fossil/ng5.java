package com.fossil;

import android.content.Context;
import com.google.gson.Gson;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.AccessGroup;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.nio.charset.StandardCharsets;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ng5 {
    @DexIgnore
    public static ng5 b;
    @DexIgnore
    public Gson a; // = new Gson();

    @DexIgnore
    public static ng5 a() {
        if (b == null) {
            synchronized (ng5.class) {
                if (b == null) {
                    b = new ng5();
                }
            }
        }
        return b;
    }

    @DexIgnore
    public final Access a(Context context, int i) throws Exception {
        int i2;
        Random random = new Random();
        SoLibraryLoader a2 = SoLibraryLoader.a();
        int nextInt = random.nextInt(1000);
        while (true) {
            i2 = nextInt + 1;
            if (SoLibraryLoader.a().a(i2)) {
                break;
            }
            nextInt = random.nextInt(1000);
        }
        String str = new String(a2.getData(i2, i), StandardCharsets.UTF_8);
        AccessGroup accessGroup = (AccessGroup) this.a.a(a2.a(a2.a(str, i2), a2.a(new String(a2.find(context, i2), StandardCharsets.UTF_8), i2).trim()), AccessGroup.class);
        if (accessGroup == null) {
            return null;
        }
        if (i != 0) {
            if (i == 1) {
                return accessGroup.getOreo();
            }
            if (i != 4) {
                return accessGroup.getLollipop();
            }
        }
        return accessGroup.getPie();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0011, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r2 = a(r2, 4);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.portfolio.platform.data.Access a(android.content.Context r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            r0 = 0
            com.portfolio.platform.data.Access r2 = r1.a(r2, r0)     // Catch:{ Exception -> 0x0009, all -> 0x0007 }
            goto L_0x0012
        L_0x0007:
            r2 = move-exception
            goto L_0x000f
        L_0x0009:
            r0 = 4
            com.portfolio.platform.data.Access r2 = r1.a(r2, r0)     // Catch:{ Exception -> 0x0011, all -> 0x0007 }
            goto L_0x0012
        L_0x000f:
            monitor-exit(r1)
            throw r2
        L_0x0011:
            r2 = 0
        L_0x0012:
            monitor-exit(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ng5.a(android.content.Context):com.portfolio.platform.data.Access");
    }
}
