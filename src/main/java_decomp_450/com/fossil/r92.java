package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r92 {
    @DexIgnore
    public static Boolean a;
    @DexIgnore
    public static Boolean b;
    @DexIgnore
    public static Boolean c;

    @DexIgnore
    @TargetApi(20)
    public static boolean a(Context context) {
        return a(context.getPackageManager());
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean b(Context context) {
        if (!a(context)) {
            return false;
        }
        if (v92.i()) {
            return c(context) && !v92.j();
        }
        return true;
    }

    @DexIgnore
    @TargetApi(21)
    public static boolean c(Context context) {
        if (b == null) {
            b = Boolean.valueOf(v92.h() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }

    @DexIgnore
    public static boolean d(Context context) {
        if (c == null) {
            c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return c.booleanValue();
    }

    @DexIgnore
    @TargetApi(20)
    public static boolean a(PackageManager packageManager) {
        if (a == null) {
            a = Boolean.valueOf(v92.g() && packageManager.hasSystemFeature("android.hardware.type.watch"));
        }
        return a.booleanValue();
    }

    @DexIgnore
    public static boolean a() {
        return "user".equals(Build.TYPE);
    }
}
