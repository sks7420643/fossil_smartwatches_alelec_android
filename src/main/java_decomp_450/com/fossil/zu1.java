package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu1 extends ev1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ by1 b;
    @DexIgnore
    public /* final */ by1 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public zu1(Context context, by1 by1, by1 by12, String str) {
        if (context != null) {
            this.a = context;
            if (by1 != null) {
                this.b = by1;
                if (by12 != null) {
                    this.c = by12;
                    if (str != null) {
                        this.d = str;
                        return;
                    }
                    throw new NullPointerException("Null backendName");
                }
                throw new NullPointerException("Null monotonicClock");
            }
            throw new NullPointerException("Null wallClock");
        }
        throw new NullPointerException("Null applicationContext");
    }

    @DexIgnore
    @Override // com.fossil.ev1
    public Context a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ev1
    public String b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ev1
    public by1 c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ev1
    public by1 d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ev1)) {
            return false;
        }
        ev1 ev1 = (ev1) obj;
        if (!this.a.equals(ev1.a()) || !this.b.equals(ev1.d()) || !this.c.equals(ev1.c()) || !this.d.equals(ev1.b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CreationContext{applicationContext=" + this.a + ", wallClock=" + this.b + ", monotonicClock=" + this.c + ", backendName=" + this.d + "}";
    }
}
