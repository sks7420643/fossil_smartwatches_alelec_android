package com.fossil;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.fl4;
import com.fossil.pq5;
import com.fossil.qq5;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mq5 extends fq5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public Alarm e;
    @DexIgnore
    public SparseIntArray f; // = new SparseIntArray();
    @DexIgnore
    public MFUser g;
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public /* final */ gq5 j;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ ArrayList<Alarm> l;
    @DexIgnore
    public /* final */ Alarm m;
    @DexIgnore
    public /* final */ qq5 n;
    @DexIgnore
    public /* final */ pd5 o;
    @DexIgnore
    public /* final */ pq5 p;
    @DexIgnore
    public /* final */ UserRepository q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mq5.r;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<pq5.e, pq5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ mq5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(mq5 mq5) {
            this.a = mq5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pq5.e eVar) {
            ee7.b(eVar, "responseValue");
            pd5 c = this.a.o;
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            c.d(applicationContext);
            this.a.j.a();
            this.a.j.A();
        }

        @DexIgnore
        public void a(pq5.c cVar) {
            ee7.b(cVar, "errorValue");
            pd5 c = this.a.o;
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            c.d(applicationContext);
            this.a.j.a();
            int b = cVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = mq5.s.a();
            local.d(a2, "deleteAlarm() - deleteAlarm - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.a.j.c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.a.j.J0();
                    return;
                }
            }
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(cVar.a());
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            gq5 k = this.a.j;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                k.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<qq5.d, qq5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ mq5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(mq5 mq5) {
            this.a = mq5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qq5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(mq5.s.a(), "saveAlarm SetAlarms onSuccess");
            this.a.j.a();
            Alarm b = this.a.m;
            if (b == null) {
                b = dVar.a();
            }
            this.a.j.a(b, true);
            this.a.m();
        }

        @DexIgnore
        public void a(qq5.b bVar) {
            ee7.b(bVar, "errorValue");
            this.a.h = bVar.a().getUri();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = mq5.s.a();
            local.d(a2, "saveAlarm SetAlarms onError - uri: " + this.a.h);
            this.a.j.a();
            int c = bVar.c();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = mq5.s.a();
            local2.d(a3, "saveAlarm - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.j.c();
                    return;
                } else if (!(c == 1112 || c == 1113)) {
                    if (this.a.m == null) {
                        this.a.j.a(bVar.a(), false);
                        return;
                    } else {
                        this.a.j.J0();
                        return;
                    }
                }
            }
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.b());
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            gq5 k = this.a.j;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                k.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1", f = "AlarmPresenter.kt", l = {56}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mq5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1$1", f = "AlarmPresenter.kt", l = {56}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository j = this.this$0.this$0.q;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = j.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(mq5 mq5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mq5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            mq5 mq5;
            Object a2 = nb7.a();
            int i = this.label;
            boolean z = true;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = mq5.s.a();
                local.d(a3, "start alarm " + this.this$0.m);
                PortfolioApp.g0.b(yi7);
                this.this$0.n.f();
                this.this$0.p.f();
                nj5.d.a(CommunicateMode.SET_LIST_ALARM);
                mq5 mq52 = this.this$0;
                ti7 a4 = mq52.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = mq52;
                this.label = 1;
                obj2 = vh7.a(a4, aVar, this);
                if (obj2 == a2) {
                    return a2;
                }
                mq5 = mq52;
            } else if (i == 1) {
                mq5 = (mq5) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                obj2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            mq5.g = (MFUser) obj2;
            MFUser i2 = this.this$0.g;
            if (!(i2 == null || i2.getUserId() == null)) {
                gq5 k = this.this$0.j;
                if (this.this$0.m == null) {
                    z = false;
                }
                k.O(z);
                if (this.this$0.m != null) {
                    if (this.this$0.e == null) {
                        mq5 mq53 = this.this$0;
                        mq53.e = Alarm.copy$default(mq53.m, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null);
                    }
                    this.this$0.j.l0();
                } else if (this.this$0.e == null) {
                    this.this$0.e = new Alarm(null, "", "", "", 0, 0, new int[0], true, false, "", "", 0, 2048, null);
                    this.this$0.n();
                }
                this.this$0.p();
                if (this.this$0.f.size() == 0) {
                    mq5 mq54 = this.this$0;
                    Alarm g = mq54.e;
                    mq54.f = mq54.a(g != null ? g.getDays() : null);
                }
                gq5 k2 = this.this$0.j;
                Alarm g2 = this.this$0.e;
                String title = g2 != null ? g2.getTitle() : null;
                if (title != null) {
                    k2.a(title);
                    gq5 k3 = this.this$0.j;
                    Alarm g3 = this.this$0.e;
                    String message = g3 != null ? g3.getMessage() : null;
                    if (message != null) {
                        k3.s(message);
                        gq5 k4 = this.this$0.j;
                        Alarm g4 = this.this$0.e;
                        Integer a5 = g4 != null ? pb7.a(g4.getTotalMinutes()) : null;
                        if (a5 != null) {
                            k4.a(a5.intValue());
                            gq5 k5 = this.this$0.j;
                            Alarm g5 = this.this$0.e;
                            Boolean a6 = g5 != null ? pb7.a(g5.isRepeated()) : null;
                            if (a6 != null) {
                                k5.t(a6.booleanValue());
                                this.this$0.j.a(this.this$0.f);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = mq5.class.getSimpleName();
        ee7.a((Object) simpleName, "AlarmPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public mq5(gq5 gq5, String str, ArrayList<Alarm> arrayList, Alarm alarm, qq5 qq5, pd5 pd5, pq5 pq5, UserRepository userRepository) {
        ee7.b(gq5, "mView");
        ee7.b(str, "mDeviceId");
        ee7.b(arrayList, "mAlarms");
        ee7.b(qq5, "mSetAlarms");
        ee7.b(pd5, "mAlarmHelper");
        ee7.b(pq5, "mDeleteAlarm");
        ee7.b(userRepository, "mUserRepository");
        this.j = gq5;
        this.k = str;
        this.l = arrayList;
        this.m = alarm;
        this.n = qq5;
        this.o = pd5;
        this.p = pq5;
        this.q = userRepository;
    }

    @DexIgnore
    public final void n() {
        int i2 = Calendar.getInstance().get(10);
        int i3 = Calendar.getInstance().get(12);
        boolean z = true;
        if (Calendar.getInstance().get(9) != 1) {
            z = false;
        }
        b(String.valueOf(i2), String.valueOf(i3), z);
    }

    @DexIgnore
    public void o() {
        this.j.a(this);
    }

    @DexIgnore
    public final void p() {
        if (l() || k()) {
            this.i = true;
            this.j.e(true);
            return;
        }
        this.i = false;
        this.j.e(false);
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void b(String str) {
        ee7.b(str, "title");
        a(this, str, null, null, null, null, null, null, null, null, 510, null);
        p();
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        this.n.g();
        this.p.g();
        PortfolioApp.g0.c(this);
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void h() {
        this.j.b();
        pq5 pq5 = this.p;
        String str = this.k;
        ArrayList<Alarm> arrayList = this.l;
        Alarm alarm = this.m;
        if (alarm != null) {
            pq5.a(new pq5.d(str, arrayList, alarm), new b(this));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void i() {
        if (this.i) {
            this.j.A0();
        } else {
            this.j.A();
        }
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void j() {
        xg5 xg5 = xg5.b;
        gq5 gq5 = this.j;
        if (gq5 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmFragment");
        } else if (xg5.a(xg5, ((hq5) gq5).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null)) {
            this.j.b();
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            String y = zd5.y(instance.getTime());
            if (this.m != null) {
                a(this, null, null, null, null, null, null, null, y, null, 383, null);
                Iterator<Alarm> it = this.l.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Alarm next = it.next();
                    if (ee7.a((Object) next.getUri(), (Object) this.m.getUri())) {
                        ArrayList<Alarm> arrayList = this.l;
                        int indexOf = arrayList.indexOf(next);
                        Alarm alarm = this.e;
                        if (alarm != null) {
                            arrayList.set(indexOf, alarm);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                }
            } else {
                String str = this.h;
                if (str == null) {
                    StringBuilder sb = new StringBuilder();
                    MFUser mFUser = this.g;
                    sb.append(mFUser != null ? mFUser.getUserId() : null);
                    sb.append(':');
                    Calendar instance2 = Calendar.getInstance();
                    ee7.a((Object) instance2, "Calendar.getInstance()");
                    sb.append(instance2.getTimeInMillis());
                    str = sb.toString();
                }
                a(this, null, null, null, null, null, null, y, y, str, 63, null);
                ArrayList<Alarm> arrayList2 = this.l;
                Alarm alarm2 = this.e;
                if (alarm2 != null) {
                    arrayList2.add(alarm2);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("saveAlarm - uri: ");
            Alarm alarm3 = this.e;
            sb2.append(alarm3 != null ? alarm3.getUri() : null);
            local.d(str2, sb2.toString());
            FLogger.INSTANCE.getLocal().d(r, "saveAlarm SetAlarms");
            qq5 qq5 = this.n;
            String str3 = this.k;
            ArrayList<Alarm> arrayList3 = this.l;
            Alarm alarm4 = this.e;
            if (alarm4 != null) {
                qq5.a(new qq5.c(str3, arrayList3, alarm4), new c(this));
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final boolean k() {
        return !ee7.a(this.m, this.e);
    }

    @DexIgnore
    public final boolean l() {
        return this.m == null;
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(r, "onSetAlarmsSuccess()");
        this.o.d(PortfolioApp.g0.c());
        PortfolioApp.g0.c().j(this.k);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x007f, code lost:
        if (r3 == 12) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0082, code lost:
        if (r3 == 12) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0087, code lost:
        if (r3 == 12) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008c, code lost:
        if (r3 == 12) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x008e, code lost:
        r2 = 12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0091, code lost:
        r2 = r3 + 12;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0085  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(java.lang.String r16, java.lang.String r17, boolean r18) {
        /*
            r15 = this;
            r1 = r18
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.mq5.r
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "updateTime: hourValue = "
            r3.append(r4)
            r4 = r16
            r3.append(r4)
            java.lang.String r5 = ", minuteValue = "
            r3.append(r5)
            r5 = r17
            r3.append(r5)
            java.lang.String r6 = ", isPM = "
            r3.append(r6)
            r3.append(r1)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            r2 = 0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r3 = "Integer.valueOf(hourValue)"
            com.fossil.ee7.a(r0, r3)     // Catch:{ Exception -> 0x0050 }
            int r3 = r0.intValue()     // Catch:{ Exception -> 0x0050 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x004e }
            java.lang.String r4 = "Integer.valueOf(minuteValue)"
            com.fossil.ee7.a(r0, r4)     // Catch:{ Exception -> 0x004e }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004e }
            goto L_0x006f
        L_0x004e:
            r0 = move-exception
            goto L_0x0052
        L_0x0050:
            r0 = move-exception
            r3 = 0
        L_0x0052:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.mq5.r
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Exception when parse time e="
            r6.append(r7)
            r6.append(r0)
            java.lang.String r0 = r6.toString()
            r4.e(r5, r0)
            r0 = 0
        L_0x006f:
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            boolean r4 = android.text.format.DateFormat.is24HourFormat(r4)
            r5 = 12
            if (r4 == 0) goto L_0x0085
            if (r1 == 0) goto L_0x0082
            if (r3 != r5) goto L_0x0091
            goto L_0x008e
        L_0x0082:
            if (r3 != r5) goto L_0x008a
            goto L_0x0093
        L_0x0085:
            if (r1 != 0) goto L_0x008c
            if (r3 != r5) goto L_0x008a
            goto L_0x0093
        L_0x008a:
            r2 = r3
            goto L_0x0093
        L_0x008c:
            if (r3 != r5) goto L_0x0091
        L_0x008e:
            r2 = 12
            goto L_0x0093
        L_0x0091:
            int r2 = r3 + 12
        L_0x0093:
            r4 = 0
            r5 = 0
            int r2 = r2 * 60
            int r2 = r2 + r0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 507(0x1fb, float:7.1E-43)
            r14 = 0
            r3 = r15
            a(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mq5.b(java.lang.String, java.lang.String, boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "updateDaysRepeat: isStateEnabled = " + z + ", day = " + i2);
        if (z) {
            this.f.put(i2, i2);
        } else {
            this.f.delete(i2);
        }
        a(this, null, null, null, a(this.f), null, null, null, null, null, 503, null);
        if (this.f.size() == 0) {
            a(this, null, null, null, null, null, false, null, null, null, 479, null);
            this.j.t(false);
        }
        p();
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void a(boolean z) {
        a(this, null, null, null, null, null, Boolean.valueOf(z), null, null, null, 479, null);
        if (!z) {
            a(this, null, null, null, new int[0], null, null, null, null, null, 503, null);
        } else if (this.f.size() != 0) {
            a(this, null, null, null, a(this.f), null, null, null, null, null, 503, null);
        } else {
            SparseIntArray a2 = a(new int[]{2, 3, 4, 5, 6, 7, 1});
            this.f = a2;
            a(this, null, null, null, a(a2), null, null, null, null, null, 503, null);
            this.j.a(this.f);
        }
        if (this.m != null) {
            p();
        }
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void a(String str, String str2, boolean z) {
        ee7.b(str, "hourValue");
        ee7.b(str2, "minuteValue");
        b(str, str2, z);
        if (this.m != null) {
            p();
        }
    }

    @DexIgnore
    @Override // com.fossil.fq5
    public void a(String str) {
        ee7.b(str, "message");
        a(this, null, str, null, null, null, null, null, null, null, 509, null);
        p();
    }

    @DexIgnore
    public final SparseIntArray a(int[] iArr) {
        int i2 = 0;
        int length = iArr != null ? iArr.length : 0;
        if (length <= 0) {
            return new SparseIntArray();
        }
        SparseIntArray sparseIntArray = new SparseIntArray();
        while (i2 < length) {
            if (iArr != null) {
                int i3 = iArr[i2];
                sparseIntArray.put(i3, i3);
                i2++;
            } else {
                ee7.a();
                throw null;
            }
        }
        return sparseIntArray;
    }

    @DexIgnore
    public final int[] a(SparseIntArray sparseIntArray) {
        int size = sparseIntArray != null ? sparseIntArray.size() : 0;
        if (size <= 0) {
            return null;
        }
        int i2 = 0;
        while (i2 < size) {
            if (sparseIntArray != null) {
                if (sparseIntArray.keyAt(i2) != sparseIntArray.valueAt(i2)) {
                    sparseIntArray.removeAt(i2);
                    size--;
                    i2--;
                }
                i2++;
            } else {
                ee7.a();
                throw null;
            }
        }
        if (sparseIntArray != null) {
            int size2 = sparseIntArray.size();
            if (size2 <= 0) {
                return null;
            }
            int[] iArr = new int[size2];
            for (int i3 = 0; i3 < size2; i3++) {
                iArr[i3] = sparseIntArray.valueAt(i3);
            }
            return iArr;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ void a(mq5 mq5, String str, String str2, Integer num, int[] iArr, Boolean bool, Boolean bool2, String str3, String str4, String str5, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = null;
        }
        if ((i2 & 2) != 0) {
            str2 = null;
        }
        if ((i2 & 4) != 0) {
            num = null;
        }
        if ((i2 & 8) != 0) {
            iArr = null;
        }
        if ((i2 & 16) != 0) {
            bool = null;
        }
        if ((i2 & 32) != 0) {
            bool2 = null;
        }
        if ((i2 & 64) != 0) {
            str3 = null;
        }
        if ((i2 & 128) != 0) {
            str4 = null;
        }
        if ((i2 & 256) != 0) {
            str5 = null;
        }
        mq5.a(str, str2, num, iArr, bool, bool2, str3, str4, str5);
    }

    @DexIgnore
    public final void a(String str, String str2, Integer num, int[] iArr, Boolean bool, Boolean bool2, String str3, String str4, String str5) {
        Alarm alarm;
        Alarm alarm2;
        Alarm alarm3;
        Alarm alarm4;
        Alarm alarm5;
        Alarm alarm6;
        if (!(str == null || (alarm6 = this.e) == null)) {
            alarm6.setTitle(str);
        }
        if (!(str2 == null || (alarm5 = this.e) == null)) {
            alarm5.setMessage(str2);
        }
        if (num != null) {
            int intValue = num.intValue();
            Alarm alarm7 = this.e;
            if (alarm7 != null) {
                alarm7.setTotalMinutes(intValue);
            }
        }
        if (!(iArr == null || (alarm4 = this.e) == null)) {
            alarm4.setDays(iArr);
        }
        if (bool != null) {
            boolean booleanValue = bool.booleanValue();
            Alarm alarm8 = this.e;
            if (alarm8 != null) {
                alarm8.setActive(booleanValue);
            }
        }
        if (bool2 != null) {
            boolean booleanValue2 = bool2.booleanValue();
            Alarm alarm9 = this.e;
            if (alarm9 != null) {
                alarm9.setRepeated(booleanValue2);
            }
        }
        if (!(str3 == null || (alarm3 = this.e) == null)) {
            alarm3.setCreatedAt(str3);
        }
        if (!(str4 == null || (alarm2 = this.e) == null)) {
            alarm2.setUpdatedAt(str4);
        }
        if (str5 != null && (alarm = this.e) != null) {
            alarm.setUri(str5);
        }
    }
}
