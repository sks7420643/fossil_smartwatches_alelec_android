package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um7 {
    @DexIgnore
    public static final <T, R> Object a(jm7<? super T> jm7, R r, kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7) {
        Object obj;
        jm7.n();
        if (kd7 != null) {
            try {
                xe7.a(kd7, 2);
                obj = kd7.invoke(r, jm7);
            } catch (Throwable th) {
                obj = new li7(th, false, 2, null);
            }
            if (obj == nb7.a()) {
                return nb7.a();
            }
            Object h = jm7.h(obj);
            if (h == qk7.b) {
                return nb7.a();
            }
            if (!(h instanceof li7)) {
                return qk7.b(h);
            }
            Throwable th2 = ((li7) h).a;
            fb7<T> fb7 = jm7.d;
            if (!dj7.d()) {
                throw th2;
            } else if (!(fb7 instanceof sb7)) {
                throw th2;
            } else {
                throw km7.b(th2, (sb7) fb7);
            }
        } else {
            throw new x87("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        }
    }

    @DexIgnore
    public static final <T, R> Object b(jm7<? super T> jm7, R r, kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7) {
        Object obj;
        jm7.n();
        boolean z = false;
        if (kd7 != null) {
            try {
                xe7.a(kd7, 2);
                obj = kd7.invoke(r, jm7);
            } catch (Throwable th) {
                obj = new li7(th, false, 2, null);
            }
            if (obj == nb7.a()) {
                return nb7.a();
            }
            Object h = jm7.h(obj);
            if (h == qk7.b) {
                return nb7.a();
            }
            if (!(h instanceof li7)) {
                return qk7.b(h);
            }
            li7 li7 = (li7) h;
            Throwable th2 = li7.a;
            if (!(th2 instanceof il7) || ((il7) th2).coroutine != jm7) {
                z = true;
            }
            if (z) {
                Throwable th3 = li7.a;
                fb7<T> fb7 = jm7.d;
                if (!dj7.d()) {
                    throw th3;
                } else if (!(fb7 instanceof sb7)) {
                    throw th3;
                } else {
                    throw km7.b(th3, (sb7) fb7);
                }
            } else if (!(obj instanceof li7)) {
                return obj;
            } else {
                Throwable th4 = ((li7) obj).a;
                fb7<T> fb72 = jm7.d;
                if (!dj7.d()) {
                    throw th4;
                } else if (!(fb72 instanceof sb7)) {
                    throw th4;
                } else {
                    throw km7.b(th4, (sb7) fb72);
                }
            }
        } else {
            throw new x87("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> void a(gd7<? super fb7<? super T>, ? extends Object> gd7, fb7<? super T> fb7) {
        vb7.a(fb7);
        try {
            ib7 context = fb7.getContext();
            Object b = pm7.b(context, null);
            if (gd7 != null) {
                try {
                    xe7.a(gd7, 1);
                    Object invoke = gd7.invoke(fb7);
                    pm7.a(context, b);
                    if (invoke != nb7.a()) {
                        s87.a aVar = s87.Companion;
                        fb7.resumeWith(s87.m60constructorimpl(invoke));
                    }
                } catch (Throwable th) {
                    pm7.a(context, b);
                    throw th;
                }
            } else {
                throw new x87("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            }
        } catch (Throwable th2) {
            s87.a aVar2 = s87.Companion;
            fb7.resumeWith(s87.m60constructorimpl(t87.a(th2)));
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <R, T> void a(kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7, R r, fb7<? super T> fb7) {
        vb7.a(fb7);
        try {
            ib7 context = fb7.getContext();
            Object b = pm7.b(context, null);
            if (kd7 != null) {
                try {
                    xe7.a(kd7, 2);
                    Object invoke = kd7.invoke(r, fb7);
                    pm7.a(context, b);
                    if (invoke != nb7.a()) {
                        s87.a aVar = s87.Companion;
                        fb7.resumeWith(s87.m60constructorimpl(invoke));
                    }
                } catch (Throwable th) {
                    pm7.a(context, b);
                    throw th;
                }
            } else {
                throw new x87("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            }
        } catch (Throwable th2) {
            s87.a aVar2 = s87.Companion;
            fb7.resumeWith(s87.m60constructorimpl(t87.a(th2)));
        }
    }
}
