package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r76 implements Factory<q76> {
    @DexIgnore
    public static q76 a(o76 o76, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        return new q76(o76, summariesRepository, activitiesRepository, workoutSessionRepository, portfolioApp);
    }
}
