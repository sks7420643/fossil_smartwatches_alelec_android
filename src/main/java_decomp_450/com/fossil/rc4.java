package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rc4 implements Runnable {
    @DexIgnore
    public /* final */ uc4 a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ oo3 c;

    @DexIgnore
    public rc4(uc4 uc4, Intent intent, oo3 oo3) {
        this.a = uc4;
        this.b = intent;
        this.c = oo3;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
