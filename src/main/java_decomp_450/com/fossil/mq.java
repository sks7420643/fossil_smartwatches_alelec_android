package com.fossil;

import android.content.Context;
import com.fossil.jq;
import com.fossil.qn7;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mq {
    @DexIgnore
    public qn7.a a;
    @DexIgnore
    public jq b;
    @DexIgnore
    public double c;
    @DexIgnore
    public double d; // = ku.a.b();
    @DexIgnore
    public kq e; // = new kq(null, null, null, false, false, null, null, null, 255, null);
    @DexIgnore
    public /* final */ Context f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements vc7<OkHttpClient> {
        @DexIgnore
        public /* final */ /* synthetic */ mq this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(mq mqVar) {
            super(0);
            this.this$0 = mqVar;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final OkHttpClient invoke() {
            OkHttpClient.b bVar = new OkHttpClient.b();
            bVar.a(du.a(this.this$0.f));
            OkHttpClient a = bVar.a();
            ee7.a((Object) a, "OkHttpClient.Builder()\n \u2026xt))\n            .build()");
            return a;
        }
    }

    @DexIgnore
    public mq(Context context) {
        ee7.b(context, "context");
        this.f = context;
        this.c = ku.a.a(context);
    }

    @DexIgnore
    public final qn7.a b() {
        return iu.a(new a(this));
    }

    @DexIgnore
    public final lq a() {
        long a2 = ku.a.a(this.f, this.c);
        long j = (long) (this.d * ((double) a2));
        rq a3 = rq.a.a(j);
        ds dsVar = new ds(a3);
        ns a4 = ns.a.a(dsVar, (int) (a2 - j));
        Context context = this.f;
        kq kqVar = this.e;
        qn7.a aVar = this.a;
        if (aVar == null) {
            aVar = b();
        }
        jq jqVar = this.b;
        if (jqVar == null) {
            jq.b bVar = jq.e;
            jqVar = new jq.a().a();
        }
        return new nq(context, kqVar, a3, dsVar, a4, aVar, jqVar);
    }
}
