package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow2<K> implements Iterator<Map.Entry<K, Object>> {
    @DexIgnore
    public Iterator<Map.Entry<K, Object>> a;

    @DexIgnore
    public ow2(Iterator<Map.Entry<K, Object>> it) {
        this.a = it;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        Map.Entry<K, Object> next = this.a.next();
        return next.getValue() instanceof nw2 ? new pw2(next) : next;
    }

    @DexIgnore
    public final void remove() {
        this.a.remove();
    }
}
