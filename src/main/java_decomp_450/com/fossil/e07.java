package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e07 {
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<g07>> a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, h07> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ l07 d;
    @DexIgnore
    public /* final */ i07 e;
    @DexIgnore
    public /* final */ ThreadLocal<ConcurrentLinkedQueue<c>> f;
    @DexIgnore
    public /* final */ ThreadLocal<Boolean> g;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<Class<?>>> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ThreadLocal<ConcurrentLinkedQueue<c>> {
        @DexIgnore
        public a(e07 e07) {
        }

        @DexIgnore
        @Override // java.lang.ThreadLocal
        public ConcurrentLinkedQueue<c> initialValue() {
            return new ConcurrentLinkedQueue<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ThreadLocal<Boolean> {
        @DexIgnore
        public b(e07 e07) {
        }

        @DexIgnore
        @Override // java.lang.ThreadLocal
        public Boolean initialValue() {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ g07 b;

        @DexIgnore
        public c(Object obj, g07 g07) {
            this.a = obj;
            this.b = g07;
        }
    }

    @DexIgnore
    public e07(l07 l07) {
        this(l07, Endpoints.DEFAULT_NAME);
    }

    @DexIgnore
    public final void a(g07 g07, h07 h07) {
        try {
            Object c2 = h07.c();
            if (c2 != null) {
                a(c2, g07);
            }
        } catch (InvocationTargetException e2) {
            a("Producer " + h07 + " threw an exception.", e2);
            throw null;
        }
    }

    @DexIgnore
    public void b(Object obj) {
        Set<g07> putIfAbsent;
        if (obj != null) {
            this.d.a(this);
            Map<Class<?>, h07> b2 = this.e.b(obj);
            for (Class<?> cls : b2.keySet()) {
                h07 h07 = b2.get(cls);
                h07 putIfAbsent2 = this.b.putIfAbsent(cls, h07);
                if (putIfAbsent2 == null) {
                    Set<g07> set = this.a.get(cls);
                    if (set != null && !set.isEmpty()) {
                        for (g07 g07 : set) {
                            a(g07, h07);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Producer method for type " + cls + " found on type " + h07.a.getClass() + ", but already registered by type " + putIfAbsent2.a.getClass() + CodelessMatcher.CURRENT_CLASS_NAME);
                }
            }
            Map<Class<?>, Set<g07>> a2 = this.e.a(obj);
            for (Class<?> cls2 : a2.keySet()) {
                Set<g07> set2 = this.a.get(cls2);
                if (set2 == null && (putIfAbsent = this.a.putIfAbsent(cls2, (set2 = new CopyOnWriteArraySet<>()))) != null) {
                    set2 = putIfAbsent;
                }
                if (!set2.addAll(a2.get(cls2))) {
                    throw new IllegalArgumentException("Object already registered.");
                }
            }
            for (Map.Entry<Class<?>, Set<g07>> entry : a2.entrySet()) {
                h07 h072 = this.b.get(entry.getKey());
                if (h072 != null && h072.b()) {
                    for (g07 g072 : entry.getValue()) {
                        if (!h072.b()) {
                            break;
                        } else if (g072.b()) {
                            a(g072, h072);
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("Object to register must not be null.");
    }

    @DexIgnore
    public void c(Object obj) {
        if (obj != null) {
            this.d.a(this);
            for (Map.Entry<Class<?>, h07> entry : this.e.b(obj).entrySet()) {
                Class<?> key = entry.getKey();
                h07 d2 = d(key);
                h07 value = entry.getValue();
                if (value == null || !value.equals(d2)) {
                    throw new IllegalArgumentException("Missing event producer for an annotated method. Is " + obj.getClass() + " registered?");
                }
                this.b.remove(key).a();
            }
            for (Map.Entry<Class<?>, Set<g07>> entry2 : this.e.a(obj).entrySet()) {
                Set<g07> c2 = c(entry2.getKey());
                Set<g07> value2 = entry2.getValue();
                if (c2 == null || !c2.containsAll(value2)) {
                    throw new IllegalArgumentException("Missing event handler for an annotated method. Is " + obj.getClass() + " registered?");
                }
                for (g07 g07 : c2) {
                    if (value2.contains(g07)) {
                        g07.a();
                    }
                }
                c2.removeAll(value2);
            }
            return;
        }
        throw new NullPointerException("Object to unregister must not be null.");
    }

    @DexIgnore
    public h07 d(Class<?> cls) {
        return this.b.get(cls);
    }

    @DexIgnore
    public String toString() {
        return "[Bus \"" + this.c + "\"]";
    }

    @DexIgnore
    public e07(l07 l07, String str) {
        this(l07, str, i07.a);
    }

    @DexIgnore
    public e07(l07 l07, String str, i07 i07) {
        this.a = new ConcurrentHashMap();
        this.b = new ConcurrentHashMap();
        this.f = new a(this);
        this.g = new b(this);
        this.h = new ConcurrentHashMap();
        this.d = l07;
        this.c = str;
        this.e = i07;
    }

    @DexIgnore
    public void a(Object obj) {
        if (obj != null) {
            this.d.a(this);
            boolean z = false;
            for (Class<?> cls : a(obj.getClass())) {
                Set<g07> c2 = c(cls);
                if (c2 != null && !c2.isEmpty()) {
                    z = true;
                    for (g07 g07 : c2) {
                        b(obj, g07);
                    }
                }
            }
            if (!z && !(obj instanceof f07)) {
                a(new f07(this, obj));
            }
            a();
            return;
        }
        throw new NullPointerException("Event to post must not be null.");
    }

    @DexIgnore
    public void a() {
        if (!this.g.get().booleanValue()) {
            this.g.set(true);
            while (true) {
                try {
                    c poll = this.f.get().poll();
                    if (poll != null) {
                        if (poll.b.b()) {
                            a(poll.a, poll.b);
                        }
                    } else {
                        return;
                    }
                } finally {
                    this.g.set(false);
                }
            }
        }
    }

    @DexIgnore
    public void a(Object obj, g07 g07) {
        try {
            g07.a(obj);
        } catch (InvocationTargetException e2) {
            a("Could not dispatch event: " + obj.getClass() + " to handler " + g07, e2);
            throw null;
        }
    }

    @DexIgnore
    public Set<g07> c(Class<?> cls) {
        return this.a.get(cls);
    }

    @DexIgnore
    public Set<Class<?>> a(Class<?> cls) {
        Set<Class<?>> set = this.h.get(cls);
        if (set != null) {
            return set;
        }
        Set<Class<?>> b2 = b(cls);
        Set<Class<?>> putIfAbsent = this.h.putIfAbsent(cls, b2);
        return putIfAbsent == null ? b2 : putIfAbsent;
    }

    @DexIgnore
    public static void a(String str, InvocationTargetException invocationTargetException) {
        Throwable cause = invocationTargetException.getCause();
        if (cause != null) {
            throw new RuntimeException(str + ": " + cause.getMessage(), cause);
        }
        throw new RuntimeException(str + ": " + invocationTargetException.getMessage(), invocationTargetException);
    }

    @DexIgnore
    public void b(Object obj, g07 g07) {
        this.f.get().offer(new c(obj, g07));
    }

    @DexIgnore
    public final Set<Class<?>> b(Class<?> cls) {
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        linkedList.add(cls);
        while (!linkedList.isEmpty()) {
            Class cls2 = (Class) linkedList.remove(0);
            hashSet.add(cls2);
            Class superclass = cls2.getSuperclass();
            if (superclass != null) {
                linkedList.add(superclass);
            }
        }
        return hashSet;
    }
}
