package com.fossil;

import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai4 extends li4 {

    @DexIgnore
    public enum a {
        UNCODABLE,
        ONE_DIGIT,
        TWO_DIGITS,
        FNC_1
    }

    @DexIgnore
    @Override // com.fossil.sg4, com.fossil.li4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (mg4 == mg4.CODE_128) {
            return super.a(str, mg4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_128, but got " + mg4);
    }

    @DexIgnore
    @Override // com.fossil.li4
    public boolean[] a(String str) {
        int i;
        int length = str.length();
        if (length <= 0 || length > 80) {
            throw new IllegalArgumentException("Contents length should be between 1 and 80 characters, but got " + length);
        }
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            if (charAt < ' ' || charAt > '~') {
                switch (charAt) {
                    default:
                        throw new IllegalArgumentException("Bad character in input: " + charAt);
                    case '\u00f1':
                    case '\u00f2':
                    case '\u00f3':
                    case '\u00f4':
                        break;
                }
            }
        }
        ArrayList<int[]> arrayList = new ArrayList();
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 1;
        while (i4 < length) {
            int a2 = a(str, i4, i6);
            int i8 = 100;
            if (a2 == i6) {
                switch (str.charAt(i4)) {
                    case '\u00f1':
                        i8 = 102;
                        break;
                    case '\u00f2':
                        i8 = 97;
                        break;
                    case '\u00f3':
                        i8 = 96;
                        break;
                    case '\u00f4':
                        break;
                    default:
                        if (i6 != 100) {
                            i8 = Integer.parseInt(str.substring(i4, i4 + 2));
                            i4++;
                            break;
                        } else {
                            i8 = str.charAt(i4) - ' ';
                            break;
                        }
                }
                i4++;
            } else {
                i = i6 == 0 ? a2 == 100 ? 104 : 105 : a2;
                i6 = a2;
            }
            arrayList.add(zh4.a[i]);
            i5 += i * i7;
            if (i4 != 0) {
                i7++;
            }
        }
        arrayList.add(zh4.a[i5 % 103]);
        arrayList.add(zh4.a[106]);
        int i9 = 0;
        for (int[] iArr : arrayList) {
            for (int i10 : iArr) {
                i9 += i10;
            }
        }
        boolean[] zArr = new boolean[i9];
        for (int[] iArr2 : arrayList) {
            i2 += li4.a(zArr, i2, iArr2, true);
        }
        return zArr;
    }

    @DexIgnore
    public static a a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        if (i >= length) {
            return a.UNCODABLE;
        }
        char charAt = charSequence.charAt(i);
        if (charAt == '\u00f1') {
            return a.FNC_1;
        }
        if (charAt < '0' || charAt > '9') {
            return a.UNCODABLE;
        }
        int i2 = i + 1;
        if (i2 >= length) {
            return a.ONE_DIGIT;
        }
        char charAt2 = charSequence.charAt(i2);
        if (charAt2 < '0' || charAt2 > '9') {
            return a.ONE_DIGIT;
        }
        return a.TWO_DIGITS;
    }

    @DexIgnore
    public static int a(CharSequence charSequence, int i, int i2) {
        a a2;
        a a3;
        a a4 = a(charSequence, i);
        if (!(a4 == a.UNCODABLE || a4 == a.ONE_DIGIT)) {
            if (i2 == 99) {
                return i2;
            }
            if (i2 != 100) {
                if (a4 == a.FNC_1) {
                    a4 = a(charSequence, i + 1);
                }
                if (a4 == a.TWO_DIGITS) {
                    return 99;
                }
            } else if (a4 == a.FNC_1 || (a2 = a(charSequence, i + 2)) == a.UNCODABLE || a2 == a.ONE_DIGIT) {
                return i2;
            } else {
                if (a2 == a.FNC_1) {
                    return a(charSequence, i + 3) == a.TWO_DIGITS ? 99 : 100;
                }
                int i3 = i + 4;
                while (true) {
                    a3 = a(charSequence, i3);
                    if (a3 != a.TWO_DIGITS) {
                        break;
                    }
                    i3 += 2;
                }
                return a3 == a.ONE_DIGIT ? 100 : 99;
            }
        }
        return 100;
    }
}
