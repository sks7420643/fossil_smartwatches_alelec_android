package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r70 extends s70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<r70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public r70 createFromParcel(Parcel parcel) {
            return new r70(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public r70[] newArray(int i) {
            return new r70[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public r70 m55createFromParcel(Parcel parcel) {
            return new r70(parcel, (zd7) null);
        }
    }

    @DexIgnore
    public r70() {
        super(u70.CHANCE_OF_RAIN, null, null, null, 14);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ r70(ue0 ue0, ve0 ve0, int i, zd7 zd7) {
        this(ue0, (i & 2) != 0 ? new ve0(ve0.CREATOR.a()) : ve0);
    }

    @DexIgnore
    public r70(ue0 ue0, ve0 ve0) {
        super(u70.CHANCE_OF_RAIN, null, ue0, ve0, 2);
    }

    @DexIgnore
    public /* synthetic */ r70(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
