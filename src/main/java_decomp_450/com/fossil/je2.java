package com.fossil;

import android.annotation.TargetApi;
import android.os.Trace;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je2 implements Closeable {
    @DexIgnore
    public static /* final */ re2<Boolean> b; // = qe2.a().a("nts.enable_tracing", true);
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    @TargetApi(18)
    public je2(String str) {
        boolean z = v92.e() && b.get().booleanValue();
        this.a = z;
        if (z) {
            Trace.beginSection(str.length() > 127 ? str.substring(0, 127) : str);
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    @TargetApi(18)
    public final void close() {
        if (this.a) {
            Trace.endSection();
        }
    }
}
