package com.fossil;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bk4 implements Factory<ApplicationEventListener> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> f;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> g;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> h;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> i;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> j;
    @DexIgnore
    public /* final */ Provider<UserRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<ad5> m;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> o;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> p;
    @DexIgnore
    public /* final */ Provider<fp4> q;
    @DexIgnore
    public /* final */ Provider<xo4> r;
    @DexIgnore
    public /* final */ Provider<ro4> s;
    @DexIgnore
    public /* final */ Provider<FileRepository> t;
    @DexIgnore
    public /* final */ Provider<to4> u;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> v;
    @DexIgnore
    public /* final */ Provider<lm4> w;
    @DexIgnore
    public /* final */ Provider<uj5> x;
    @DexIgnore
    public /* final */ Provider<ThemeRepository> y;

    @DexIgnore
    public bk4(wj4 wj4, Provider<PortfolioApp> provider, Provider<ch5> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<ad5> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<RingStyleRepository> provider15, Provider<fp4> provider16, Provider<xo4> provider17, Provider<ro4> provider18, Provider<FileRepository> provider19, Provider<to4> provider20, Provider<WorkoutSettingRepository> provider21, Provider<lm4> provider22, Provider<uj5> provider23, Provider<ThemeRepository> provider24) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
        this.p = provider15;
        this.q = provider16;
        this.r = provider17;
        this.s = provider18;
        this.t = provider19;
        this.u = provider20;
        this.v = provider21;
        this.w = provider22;
        this.x = provider23;
        this.y = provider24;
    }

    @DexIgnore
    public static bk4 a(wj4 wj4, Provider<PortfolioApp> provider, Provider<ch5> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<ad5> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<RingStyleRepository> provider15, Provider<fp4> provider16, Provider<xo4> provider17, Provider<ro4> provider18, Provider<FileRepository> provider19, Provider<to4> provider20, Provider<WorkoutSettingRepository> provider21, Provider<lm4> provider22, Provider<uj5> provider23, Provider<ThemeRepository> provider24) {
        return new bk4(wj4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20, provider21, provider22, provider23, provider24);
    }

    @DexIgnore
    public static ApplicationEventListener a(wj4 wj4, PortfolioApp portfolioApp, ch5 ch5, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, ad5 ad5, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, RingStyleRepository ringStyleRepository, fp4 fp4, xo4 xo4, ro4 ro4, FileRepository fileRepository, to4 to4, WorkoutSettingRepository workoutSettingRepository, lm4 lm4, uj5 uj5, ThemeRepository themeRepository) {
        ApplicationEventListener a2 = wj4.a(portfolioApp, ch5, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, deviceRepository, userRepository, alarmsRepository, ad5, watchFaceRepository, watchLocalizationRepository, ringStyleRepository, fp4, xo4, ro4, fileRepository, to4, workoutSettingRepository, lm4, uj5, themeRepository);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ApplicationEventListener get() {
        return a(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get(), this.s.get(), this.t.get(), this.u.get(), this.v.get(), this.w.get(), this.x.get(), this.y.get());
    }
}
