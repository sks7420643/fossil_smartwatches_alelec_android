package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv5 extends go5 implements qv5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public pv5 f;
    @DexIgnore
    public qw6<i45> g;
    @DexIgnore
    public jv5 h;
    @DexIgnore
    public aw5 i;
    @DexIgnore
    public kv5 j;
    @DexIgnore
    public bw5 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rv5.r;
        }

        @DexIgnore
        public final rv5 b() {
            return new rv5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public b(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rv5.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public c(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rv5.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public d(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rv5.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public e(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rv5.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public f(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rv5.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public g(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            yx6.a(view);
            jv5 a2 = this.a.h;
            if (a2 != null) {
                a2.n(0);
            }
            jv5 a3 = this.a.h;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, jv5.t.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public h(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            yx6.a(view);
            jv5 a2 = this.a.h;
            if (a2 != null) {
                a2.n(1);
            }
            jv5 a3 = this.a.h;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, jv5.t.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public i(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            yx6.a(view);
            aw5 c = this.a.i;
            if (c != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                c.show(childFragmentManager, aw5.t.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rv5 a;

        @DexIgnore
        public j(rv5 rv5) {
            this.a = rv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rv5.b(this.a).l();
        }
    }

    /*
    static {
        String simpleName = rv5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationWatchReminde\u2026nt::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pv5 b(rv5 rv5) {
        pv5 pv5 = rv5.f;
        if (pv5 != null) {
            return pv5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void H(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.U) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void I(boolean z) {
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.S;
                if (flexibleSwitchCompat != null) {
                    flexibleSwitchCompat.setChecked(z);
                }
                ConstraintLayout constraintLayout = a2.r;
                if (constraintLayout != null) {
                    constraintLayout.setAlpha(z ? 1.0f : 0.5f);
                }
                ConstraintLayout constraintLayout2 = a2.r;
                ee7.a((Object) constraintLayout2, "clInactivityNudgeContainer");
                int childCount = constraintLayout2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = a2.r.getChildAt(i2);
                    ee7.a((Object) childAt, "child");
                    childAt.setEnabled(z);
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void Q(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, LogBuilder.KEY_TIME);
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.H) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void d(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        ee7.b(spannableString, LogBuilder.KEY_TIME);
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.A) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void e(boolean z) {
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
                if (z) {
                    FlexibleButton flexibleButton2 = a2.v;
                    if (flexibleButton2 != null) {
                        flexibleButton2.a("flexible_button_primary");
                        return;
                    }
                    return;
                }
                FlexibleButton flexibleButton3 = a2.v;
                if (flexibleButton3 != null) {
                    flexibleButton3.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        pv5 pv5 = this.f;
        if (pv5 != null) {
            pv5.l();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void l(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.R) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        i45 i45 = (i45) qb.a(layoutInflater, 2131558596, viewGroup, false, a1());
        jv5 jv5 = (jv5) getChildFragmentManager().b(jv5.t.a());
        this.h = jv5;
        if (jv5 == null) {
            this.h = jv5.t.b();
        }
        aw5 aw5 = (aw5) getChildFragmentManager().b(aw5.t.a());
        this.i = aw5;
        if (aw5 == null) {
            this.i = aw5.t.b();
        }
        String b2 = eh5.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            i45.V.setBackgroundColor(parseColor);
            i45.W.setBackgroundColor(parseColor);
            i45.X.setBackgroundColor(parseColor);
        }
        FlexibleTextView flexibleTextView = i45.K;
        ee7.a((Object) flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setSelected(true);
        i45.L.setOnClickListener(new b(this));
        i45.S.setOnClickListener(new c(this));
        i45.T.setOnClickListener(new d(this));
        i45.U.setOnClickListener(new e(this));
        i45.R.setOnClickListener(new f(this));
        i45.N.setOnClickListener(new g(this));
        i45.M.setOnClickListener(new h(this));
        i45.O.setOnClickListener(new i(this));
        i45.v.setOnClickListener(new j(this));
        this.g = new qw6<>(this, i45);
        tj4 f2 = PortfolioApp.g0.c().f();
        jv5 jv52 = this.h;
        if (jv52 != null) {
            aw5 aw52 = this.i;
            if (aw52 != null) {
                f2.a(new fw5(jv52, aw52)).a(this);
                V("reminder_view");
                ee7.a((Object) i45, "binding");
                return i45.d();
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimeContract.View");
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimeContract.View");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        pv5 pv5 = this.f;
        if (pv5 != null) {
            pv5.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
            }
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pv5 pv5 = this.f;
        if (pv5 != null) {
            pv5.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.t;
                ee7.a((Object) constraintLayout, "clRemindersContainer");
                constraintLayout.setVisibility(8);
                FlexibleButton flexibleButton = a2.v;
                ee7.a((Object) flexibleButton, "fbSave");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void p(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.T) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv5
    public void c(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        ee7.b(spannableString, LogBuilder.KEY_TIME);
        qw6<i45> qw6 = this.g;
        if (qw6 != null) {
            i45 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.C) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(pv5 pv5) {
        ee7.b(pv5, "presenter");
        this.f = pv5;
    }
}
