package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class aa5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;

    @DexIgnore
    public aa5(Object obj, View view, int i, ConstraintLayout constraintLayout, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = imageView;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = flexibleTextView3;
        this.v = flexibleTextView4;
        this.w = flexibleTextView5;
    }

    @DexIgnore
    public static aa5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static aa5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (aa5) ViewDataBinding.a(layoutInflater, 2131558703, viewGroup, z, obj);
    }
}
