package com.fossil;

import android.os.Process;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lh3 extends Thread {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ BlockingQueue<mh3<?>> b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ /* synthetic */ hh3 d;

    @DexIgnore
    public lh3(hh3 hh3, String str, BlockingQueue<mh3<?>> blockingQueue) {
        this.d = hh3;
        a72.a((Object) str);
        a72.a(blockingQueue);
        this.a = new Object();
        this.b = blockingQueue;
        setName(str);
    }

    @DexIgnore
    public final void a() {
        synchronized (this.a) {
            this.a.notifyAll();
        }
    }

    @DexIgnore
    public final void b() {
        synchronized (this.d.i) {
            if (!this.c) {
                this.d.j.release();
                this.d.i.notifyAll();
                if (this == this.d.c) {
                    lh3 unused = this.d.c = null;
                } else if (this == this.d.d) {
                    lh3 unused2 = this.d.d = null;
                } else {
                    this.d.e().t().a("Current scheduler thread is neither worker nor network");
                }
                this.c = true;
            }
        }
    }

    @DexIgnore
    public final void run() {
        boolean z = false;
        while (!z) {
            try {
                this.d.j.acquire();
                z = true;
            } catch (InterruptedException e) {
                a(e);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                mh3<?> poll = this.b.poll();
                if (poll != null) {
                    Process.setThreadPriority(poll.b ? threadPriority : 10);
                    poll.run();
                } else {
                    synchronized (this.a) {
                        if (this.b.peek() == null && !this.d.k) {
                            try {
                                this.a.wait(30000);
                            } catch (InterruptedException e2) {
                                a(e2);
                            }
                        }
                    }
                    synchronized (this.d.i) {
                        if (this.b.peek() == null) {
                            if (this.d.l().a(wb3.y0)) {
                            }
                            b();
                            return;
                        }
                    }
                }
            }
        } finally {
            b();
        }
    }

    @DexIgnore
    public final void a(InterruptedException interruptedException) {
        this.d.e().w().a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
