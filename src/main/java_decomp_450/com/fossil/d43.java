package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d43 implements tr2<g43> {
    @DexIgnore
    public static d43 b; // = new d43();
    @DexIgnore
    public /* final */ tr2<g43> a;

    @DexIgnore
    public d43(tr2<g43> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((g43) b.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ g43 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public d43() {
        this(sr2.a(new f43()));
    }
}
