package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl3 {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ mb3 c; // = new sl3(this, ((ii3) this.d).a);
    @DexIgnore
    public /* final */ /* synthetic */ il3 d;

    @DexIgnore
    public pl3(il3 il3) {
        this.d = il3;
        long c2 = il3.zzm().c();
        this.a = c2;
        this.b = c2;
    }

    @DexIgnore
    public final void a(long j) {
        this.d.g();
        this.c.c();
        this.a = j;
        this.b = j;
    }

    @DexIgnore
    public final void b(long j) {
        this.c.c();
    }

    @DexIgnore
    public final void c() {
        this.d.g();
        a(false, false, this.d.zzm().c());
        this.d.n().a(this.d.zzm().c());
    }

    @DexIgnore
    public final long b() {
        long c2 = this.d.zzm().c();
        long j = c2 - this.b;
        this.b = c2;
        return j;
    }

    @DexIgnore
    public final long c(long j) {
        long j2 = j - this.b;
        this.b = j;
        return j2;
    }

    @DexIgnore
    public final void a() {
        this.c.c();
        this.a = 0;
        this.b = 0;
    }

    @DexIgnore
    public final boolean a(boolean z, boolean z2, long j) {
        this.d.g();
        this.d.w();
        if (!i13.a() || !this.d.l().a(wb3.A0)) {
            j = this.d.zzm().c();
        }
        if (!t13.a() || !this.d.l().a(wb3.w0) || ((ii3) this.d).a.g()) {
            this.d.k().u.a(this.d.zzm().b());
        }
        long j2 = j - this.a;
        if (z || j2 >= 1000) {
            if (this.d.l().a(wb3.U) && !z2) {
                j2 = (!u13.a() || !this.d.l().a(wb3.W) || !i13.a() || !this.d.l().a(wb3.A0)) ? b() : c(j);
            }
            this.d.e().B().a("Recording user engagement, ms", Long.valueOf(j2));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j2);
            zj3.a(this.d.r().a(!this.d.l().r().booleanValue()), bundle, true);
            if (this.d.l().a(wb3.U) && !this.d.l().a(wb3.V) && z2) {
                bundle.putLong("_fr", 1);
            }
            if (!this.d.l().a(wb3.V) || !z2) {
                this.d.o().a("auto", "_e", bundle);
            }
            this.a = j;
            this.c.c();
            this.c.a(3600000);
            return true;
        }
        this.d.e().B().a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
        return false;
    }
}
