package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cc5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un6 extends RecyclerView.g<c> {
    @DexIgnore
    public ArrayList<WorkoutSetting> a; // = new ArrayList<>();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(WorkoutSetting workoutSetting, boolean z);

        @DexIgnore
        void b(WorkoutSetting workoutSetting, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ sa5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(un6 un6, sa5 sa5) {
            super(sa5.d());
            ee7.b(sa5, "binding");
            this.a = sa5;
        }

        @DexIgnore
        public final sa5 a() {
            return this.a;
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.a.t;
                ee7.a((Object) flexibleTextView, "binding.ftvUserConfirmation");
                flexibleTextView.setVisibility(0);
                FlexibleCheckBox flexibleCheckBox = this.a.q;
                ee7.a((Object) flexibleCheckBox, "binding.cbConfirmation");
                flexibleCheckBox.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = this.a.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvUserConfirmation");
            flexibleTextView2.setVisibility(8);
            FlexibleCheckBox flexibleCheckBox2 = this.a.q;
            ee7.a((Object) flexibleCheckBox2, "binding.cbConfirmation");
            flexibleCheckBox2.setVisibility(8);
        }

        @DexIgnore
        public final void a(WorkoutSetting workoutSetting) {
            ee7.b(workoutSetting, "workout");
            View d = this.a.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            cc5.a aVar = cc5.Companion;
            r87<Integer, Integer> a2 = aVar.a(aVar.a(workoutSetting.getType(), workoutSetting.getMode()));
            String a3 = ig5.a(context, a2.getSecond().intValue());
            String a4 = ig5.a(context, 2131886589);
            sa5 sa5 = this.a;
            sa5.x.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = sa5.s;
            ee7.a((Object) flexibleTextView, "it.ftvName");
            flexibleTextView.setText(a3);
            FlexibleSwitchCompat flexibleSwitchCompat = sa5.y;
            ee7.a((Object) flexibleSwitchCompat, "it.swEnabled");
            flexibleSwitchCompat.setChecked(workoutSetting.getEnable());
            FlexibleCheckBox flexibleCheckBox = sa5.q;
            ee7.a((Object) flexibleCheckBox, "it.cbConfirmation");
            flexibleCheckBox.setChecked(workoutSetting.getAskMeFirst());
            we7 we7 = we7.a;
            String format = String.format("%d " + a4, Arrays.copyOf(new Object[]{Integer.valueOf(workoutSetting.getStartLatency())}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView2 = sa5.r;
            ee7.a((Object) flexibleTextView2, "it.ftvLatency");
            flexibleTextView2.setText(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ un6 a;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSetting b;
        @DexIgnore
        public /* final */ /* synthetic */ c c;

        @DexIgnore
        public d(un6 un6, WorkoutSetting workoutSetting, c cVar) {
            this.a = un6;
            this.b = workoutSetting;
            this.c = cVar;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            this.b.setEnable(z);
            this.b.setAskMeFirst(z);
            FlexibleCheckBox flexibleCheckBox = this.c.a().q;
            ee7.a((Object) flexibleCheckBox, "holder.binding.cbConfirmation");
            flexibleCheckBox.setChecked(z);
            this.c.a(this.b.getEnable());
            b a2 = this.a.b;
            if (a2 != null) {
                a2.b(this.b, z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ un6 a;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSetting b;
        @DexIgnore
        public /* final */ /* synthetic */ c c;

        @DexIgnore
        public e(un6 un6, WorkoutSetting workoutSetting, c cVar) {
            this.a = un6;
            this.b = workoutSetting;
            this.c = cVar;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            this.b.setAskMeFirst(z);
            FlexibleCheckBox flexibleCheckBox = this.c.a().q;
            ee7.a((Object) flexibleCheckBox, "holder.binding.cbConfirmation");
            flexibleCheckBox.setChecked(this.b.getAskMeFirst());
            b a2 = this.a.b;
            if (a2 != null) {
                a2.a(this.b, z);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final List<WorkoutSetting> c() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void a(List<WorkoutSetting> list) {
        ee7.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingAdapter", "setData, " + list.size());
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        sa5 a2 = sa5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemWorkoutSettingBindin\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        WorkoutSetting workoutSetting = this.a.get(i);
        ee7.a((Object) workoutSetting, "mData[position]");
        WorkoutSetting workoutSetting2 = workoutSetting;
        cVar.a().y.setOnCheckedChangeListener(null);
        cVar.a().q.setOnCheckedChangeListener(null);
        cVar.a(workoutSetting2);
        cVar.a(workoutSetting2.getEnable());
        cVar.a().y.setOnCheckedChangeListener(new d(this, workoutSetting2, cVar));
        cVar.a().q.setOnCheckedChangeListener(new e(this, workoutSetting2, cVar));
    }
}
