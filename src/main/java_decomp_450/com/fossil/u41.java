package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u41 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ rf1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u41(rf1 rf1) {
        super(1);
        this.a = rf1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(zk0 zk0) {
        T t;
        for (T t2 : ((ts1) zk0).m76d()) {
            if (t2 instanceof xn0) {
                Iterator<T> it = this.a.E.iterator();
                while (true) {
                    t = null;
                    if (!it.hasNext()) {
                        break;
                    }
                    T next = it.next();
                    String bundleId = next.getBundleId();
                    String str = ((lg1) t2).b;
                    if (str != null) {
                        if (ee7.a((Object) bundleId, (Object) str)) {
                            t = next;
                            break;
                        }
                    } else {
                        ee7.d("bundleId");
                        throw null;
                    }
                }
                T t3 = t;
                if (t3 == null) {
                    this.a.D.add(t2);
                } else if (t3.d() == ((lg1) t2).d) {
                    this.a.E.remove(t3);
                    this.a.C.add(t3);
                }
            }
        }
        rf1 rf1 = this.a;
        rf1.G = 0.9f / ((float) rf1.E.size());
        rf1.c(this.a);
        return i97.a;
    }
}
