package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t70 extends xg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ HashSet<s70> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<t70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public t70 createFromParcel(Parcel parcel) {
            return new t70(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public t70[] newArray(int i) {
            return new t70[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public t70 m72createFromParcel(Parcel parcel) {
            return new t70(parcel, null);
        }
    }

    @DexIgnore
    public t70(s70[] s70Arr) {
        HashSet<s70> hashSet = new HashSet<>();
        this.a = hashSet;
        ba7.a(hashSet, s70Arr);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return b();
    }

    @DexIgnore
    @Override // com.fossil.xg0
    public JSONObject b() {
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = this.a.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().a());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.comps", jSONArray);
        ee7.a((Object) put, "JSONObject().put(UIScrip\u2026ationAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final HashSet<s70> d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(t70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.a, ((t70) obj).a) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.ComplicationConfig");
    }

    @DexIgnore
    public final s70 getBottomFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            if (t2.getPositionConfig().getAngle() == 180 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final s70 getLeftFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            if (t2.getPositionConfig().getAngle() == 270 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final s70 getRightFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            if (t2.getPositionConfig().getAngle() == 90 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final s70 getTopFace() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 62) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.xg0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(ea7.n(this.a));
        }
    }

    @DexIgnore
    public t70(s70 s70, s70 s702, s70 s703, s70 s704) {
        this.a = new HashSet<>();
        s70.a(new ue0(0, 62));
        s702.a(new ue0(90, 62));
        s703.a(new ue0(180, 62));
        s704.a(new ue0(270, 62));
        this.a.add(s70);
        this.a.add(s702);
        this.a.add(s703);
        this.a.add(s704);
    }

    @DexIgnore
    public /* synthetic */ t70(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.a = new HashSet<>();
        ArrayList createTypedArrayList = parcel.createTypedArrayList(s70.CREATOR);
        if (createTypedArrayList != null) {
            this.a.addAll(createTypedArrayList);
        }
    }
}
