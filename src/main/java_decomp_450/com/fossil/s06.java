package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.hp5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s06 extends go5 implements y16 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<u55> f;
    @DexIgnore
    public x16 g;
    @DexIgnore
    public hp5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final s06 a() {
            return new s06();
        }

        @DexIgnore
        public final String b() {
            return s06.j;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements hp5.b {
        @DexIgnore
        public /* final */ /* synthetic */ s06 a;

        @DexIgnore
        public b(s06 s06) {
            this.a = s06;
        }

        @DexIgnore
        @Override // com.fossil.hp5.b
        public void a(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = s06.p.b();
            local.d(b, "onItemClick position=" + i);
            hp5 a2 = this.a.h;
            if (a2 != null) {
                hp5.c cVar = a2.c().get(i);
                if (cVar.b() == hp5.c.a.TYPE_VALUE) {
                    SecondTimezoneSetting c = cVar.c();
                    if (c != null) {
                        c.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(c.getTimeZoneId()));
                        Intent intent = new Intent();
                        intent.putExtra("SECOND_TIMEZONE", c);
                        FragmentActivity activity = this.a.getActivity();
                        if (activity != null) {
                            activity.setResult(-1, intent);
                        }
                        FragmentActivity activity2 = this.a.getActivity();
                        if (activity2 != null) {
                            activity2.finish();
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewAlphabetIndex.b {
        @DexIgnore
        public /* final */ /* synthetic */ s06 a;
        @DexIgnore
        public /* final */ /* synthetic */ u55 b;

        @DexIgnore
        public c(s06 s06, u55 u55) {
            this.a = s06;
            this.b = u55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex.b
        public void a(View view, int i, String str) {
            ee7.b(view, "view");
            ee7.b(str, "character");
            hp5 a2 = this.a.h;
            Integer valueOf = a2 != null ? Integer.valueOf(a2.b(str)) : null;
            if (valueOf != null && valueOf.intValue() != -1) {
                RecyclerView recyclerView = this.b.z;
                ee7.a((Object) recyclerView, "binding.timezoneRecyclerView");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    ((LinearLayoutManager) layoutManager).f(valueOf.intValue(), 0);
                    return;
                }
                throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ s06 a;
        @DexIgnore
        public /* final */ /* synthetic */ u55 b;

        @DexIgnore
        public d(s06 s06, u55 u55) {
            this.a = s06;
            this.b = u55;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FLogger.INSTANCE.getLocal().d(s06.p.b(), "afterTextChanged s=" + ((Object) editable));
            hp5 a2 = this.a.h;
            if (a2 != null) {
                String valueOf = String.valueOf(editable);
                int length = valueOf.length() - 1;
                int i = 0;
                boolean z = false;
                while (i <= length) {
                    boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                a2.a(valueOf.subSequence(i, length + 1).toString());
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = s06.p.b();
            local.d(b2, "beforeTextChanged s=" + charSequence + " start=" + i + " count=" + i2 + " after=" + i3);
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = s06.p.b();
            local.d(b2, "onTextChanged s=" + charSequence + " start=" + i + " before=" + i2 + " count=" + i3);
            ImageView imageView = this.b.r;
            ee7.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(!TextUtils.isEmpty(charSequence) ? 0 : 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s06 a;

        @DexIgnore
        public e(s06 s06) {
            this.a = s06;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u55 a;

        @DexIgnore
        public f(u55 u55) {
            this.a = u55;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.y.setText("");
        }
    }

    /*
    static {
        String simpleName = s06.class.getSimpleName();
        ee7.a((Object) simpleName, "SearchSecondTimezoneFrag\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.y16
    public void L(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "cityName");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local.d(str2, "cityName=" + str);
        qw6<u55> qw6 = this.f;
        if (qw6 != null) {
            u55 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.t) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.y16
    public void m(List<SecondTimezoneSetting> list) {
        ee7.b(list, "secondTimezones");
        hp5 hp5 = this.h;
        if (hp5 != null) {
            hp5.a(list);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        u55 u55 = (u55) qb.a(layoutInflater, 2131558619, viewGroup, false, a1());
        hp5 hp5 = new hp5();
        hp5.a(new b(this));
        this.h = hp5;
        RecyclerView recyclerView = u55.z;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        RecyclerViewAlphabetIndex recyclerViewAlphabetIndex = u55.x;
        recyclerViewAlphabetIndex.a();
        recyclerViewAlphabetIndex.setOnSectionIndexClickListener(new c(this, u55));
        u55.y.addTextChangedListener(new d(this, u55));
        u55.q.setOnClickListener(new e(this));
        u55.r.setOnClickListener(new f(u55));
        this.f = new qw6<>(this, u55);
        ee7.a((Object) u55, "binding");
        return u55.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        x16 x16 = this.g;
        if (x16 != null) {
            x16.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        x16 x16 = this.g;
        if (x16 != null) {
            x16.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(x16 x16) {
        ee7.b(x16, "presenter");
        this.g = x16;
    }
}
