package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class of0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ya0[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<of0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public of0 createFromParcel(Parcel parcel) {
            return new of0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public of0[] newArray(int i) {
            return new of0[i];
        }
    }

    @DexIgnore
    public of0(gc0 gc0, df0 df0, ya0[] ya0Arr) throws IllegalArgumentException {
        super(gc0, df0);
        this.c = ya0Arr;
        boolean z = true;
        if (ya0Arr.length < 1 && getDeviceMessage() == null) {
            z = false;
        }
        if (!z) {
            throw new IllegalArgumentException("weatherInfoArray must have at least 1 elements.".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (!(this.c.length == 0)) {
                JSONArray jSONArray = new JSONArray();
                int min = Math.min(this.c.length, 3);
                for (int i = 0; i < min; i++) {
                    jSONArray.put(this.c[i].b());
                }
                jSONObject.put("weatherApp._.config.locations", jSONArray);
            } else {
                JSONObject jSONObject2 = new JSONObject();
                df0 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
                jSONObject.put("weatherApp._.config.locations", jSONObject2);
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        ee7.a((Object) jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(super.b(), r51.d2, yz0.a(this.c));
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(of0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !Arrays.equals(this.c, ((of0) obj).c);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherWatchAppData");
    }

    @DexIgnore
    public final ya0[] getWeatherInfoArray() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        return (super.hashCode() * 31) + Arrays.hashCode(this.c);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }

    @DexIgnore
    public of0(gc0 gc0, ya0[] ya0Arr) throws IllegalArgumentException {
        this(gc0, null, ya0Arr);
    }

    @DexIgnore
    public of0(gc0 gc0, df0 df0) throws IllegalArgumentException {
        this(gc0, df0, new ya0[0]);
    }

    @DexIgnore
    public of0(df0 df0) throws IllegalArgumentException {
        this(null, df0, new ya0[0]);
    }

    @DexIgnore
    public of0(ya0[] ya0Arr) throws IllegalArgumentException {
        this(null, null, ya0Arr);
    }

    @DexIgnore
    public /* synthetic */ of0(Parcel parcel, zd7 zd7) {
        super((yb0) parcel.readParcelable(gc0.class.getClassLoader()), (df0) parcel.readParcelable(df0.class.getClassLoader()));
        Object[] createTypedArray = parcel.createTypedArray(ya0.CREATOR);
        if (createTypedArray != null) {
            this.c = (ya0[]) createTypedArray;
        } else {
            ee7.a();
            throw null;
        }
    }
}
