package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f23 implements tr2<e23> {
    @DexIgnore
    public static f23 b; // = new f23();
    @DexIgnore
    public /* final */ tr2<e23> a;

    @DexIgnore
    public f23(tr2<e23> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((e23) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((e23) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ e23 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public f23() {
        this(sr2.a(new h23()));
    }
}
