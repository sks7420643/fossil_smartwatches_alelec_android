package com.fossil;

import android.content.Context;
import com.facebook.internal.ServerProtocol;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h67 {
    @DexIgnore
    public static k57 e; // = v57.b();
    @DexIgnore
    public static h67 f; // = null;
    @DexIgnore
    public static Context g; // = null;
    @DexIgnore
    public DefaultHttpClient a; // = null;
    @DexIgnore
    public p57 b; // = null;
    @DexIgnore
    public StringBuilder c; // = new StringBuilder(4096);
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public h67(Context context) {
        try {
            g = context.getApplicationContext();
            this.d = System.currentTimeMillis() / 1000;
            this.b = new p57();
            if (w37.q()) {
                try {
                    Logger.getLogger("org.apache.http.wire").setLevel(Level.FINER);
                    Logger.getLogger("org.apache.http.headers").setLevel(Level.FINER);
                    System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
                    System.setProperty("org.apache.commons.logging.simplelog.showdatetime", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                    System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
                    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "debug");
                    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug");
                } catch (Throwable unused) {
                }
            }
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            this.a = defaultHttpClient;
            defaultHttpClient.setKeepAliveStrategy(new i67(this));
        } catch (Throwable th) {
            e.a(th);
        }
    }

    @DexIgnore
    public static Context a() {
        return g;
    }

    @DexIgnore
    public static void a(Context context) {
        g = context.getApplicationContext();
    }

    @DexIgnore
    public static h67 b(Context context) {
        if (f == null) {
            synchronized (h67.class) {
                if (f == null) {
                    f = new h67(context);
                }
            }
        }
        return f;
    }

    @DexIgnore
    public void a(e47 e47, g67 g67) {
        b(Arrays.asList(e47.f()), g67);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:72:0x02b2, code lost:
        if (r18 != null) goto L_0x028e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.List<?> r17, com.fossil.g67 r18) {
        /*
            r16 = this;
            r1 = r16
            r0 = r17
            java.lang.String r2 = "gzip"
            java.lang.String r3 = "rc4"
            java.lang.String r4 = "["
            java.lang.String r5 = "UTF-8"
            java.lang.String r6 = "Content-Encoding"
            if (r0 == 0) goto L_0x02f2
            boolean r7 = r17.isEmpty()
            if (r7 == 0) goto L_0x0018
            goto L_0x02f2
        L_0x0018:
            int r7 = r17.size()
            r8 = 0
            r0.get(r8)
            r9 = 0
            java.lang.StringBuilder r10 = r1.c     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r11 = r1.c     // Catch:{ all -> 0x02c1 }
            int r11 = r11.length()     // Catch:{ all -> 0x02c1 }
            r10.delete(r8, r11)     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r10 = r1.c     // Catch:{ all -> 0x02c1 }
            r10.append(r4)     // Catch:{ all -> 0x02c1 }
            r10 = 0
        L_0x0032:
            if (r10 >= r7) goto L_0x004f
            java.lang.StringBuilder r11 = r1.c     // Catch:{ all -> 0x02c1 }
            java.lang.Object r12 = r0.get(r10)     // Catch:{ all -> 0x02c1 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x02c1 }
            r11.append(r12)     // Catch:{ all -> 0x02c1 }
            int r11 = r7 + -1
            if (r10 == r11) goto L_0x004c
            java.lang.StringBuilder r11 = r1.c     // Catch:{ all -> 0x02c1 }
            java.lang.String r12 = ","
            r11.append(r12)     // Catch:{ all -> 0x02c1 }
        L_0x004c:
            int r10 = r10 + 1
            goto L_0x0032
        L_0x004f:
            java.lang.StringBuilder r0 = r1.c     // Catch:{ all -> 0x02c1 }
            java.lang.String r7 = "]"
            r0.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r0 = r1.c     // Catch:{ all -> 0x02c1 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02c1 }
            int r7 = r0.length()     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r10.<init>()     // Catch:{ all -> 0x02c1 }
            java.lang.String r11 = com.fossil.w37.n()     // Catch:{ all -> 0x02c1 }
            r10.append(r11)     // Catch:{ all -> 0x02c1 }
            java.lang.String r11 = "/?index="
            r10.append(r11)     // Catch:{ all -> 0x02c1 }
            long r11 = r1.d     // Catch:{ all -> 0x02c1 }
            r10.append(r11)     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x02c1 }
            long r11 = r1.d     // Catch:{ all -> 0x02c1 }
            r13 = 1
            long r11 = r11 + r13
            r1.d = r11     // Catch:{ all -> 0x02c1 }
            boolean r11 = com.fossil.w37.q()     // Catch:{ all -> 0x02c1 }
            if (r11 == 0) goto L_0x00a8
            com.fossil.k57 r11 = com.fossil.h67.e     // Catch:{ all -> 0x02c1 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x02c1 }
            r12.<init>(r4)     // Catch:{ all -> 0x02c1 }
            r12.append(r10)     // Catch:{ all -> 0x02c1 }
            java.lang.String r4 = "]Send request("
            r12.append(r4)     // Catch:{ all -> 0x02c1 }
            r12.append(r7)     // Catch:{ all -> 0x02c1 }
            java.lang.String r4 = "bytes), content:"
            r12.append(r4)     // Catch:{ all -> 0x02c1 }
            r12.append(r0)     // Catch:{ all -> 0x02c1 }
            java.lang.String r4 = r12.toString()     // Catch:{ all -> 0x02c1 }
            r11.e(r4)     // Catch:{ all -> 0x02c1 }
        L_0x00a8:
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ all -> 0x02c1 }
            r4.<init>(r10)     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "Accept-Encoding"
            r4.addHeader(r10, r2)     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "Connection"
            java.lang.String r11 = "Keep-Alive"
            r4.setHeader(r10, r11)     // Catch:{ all -> 0x02c1 }
            java.lang.String r10 = "Cache-Control"
            r4.removeHeaders(r10)     // Catch:{ all -> 0x02c1 }
            android.content.Context r10 = com.fossil.h67.g     // Catch:{ all -> 0x02c1 }
            com.fossil.k47 r10 = com.fossil.k47.a(r10)     // Catch:{ all -> 0x02c1 }
            org.apache.http.HttpHost r10 = r10.a()     // Catch:{ all -> 0x02c1 }
            r4.addHeader(r6, r3)     // Catch:{ all -> 0x02c1 }
            java.lang.String r11 = "http.route.default-proxy"
            java.lang.String r12 = "X-Content-Encoding"
            if (r10 != 0) goto L_0x00db
            org.apache.http.impl.client.DefaultHttpClient r13 = r1.a
            org.apache.http.params.HttpParams r13 = r13.getParams()
            r13.removeParameter(r11)
            goto L_0x0119
        L_0x00db:
            boolean r13 = com.fossil.w37.q()
            if (r13 == 0) goto L_0x00f8
            com.fossil.k57 r13 = com.fossil.h67.e
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "proxy:"
            r14.<init>(r15)
            java.lang.String r15 = r10.toHostString()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r13.a(r14)
        L_0x00f8:
            r4.addHeader(r12, r3)
            org.apache.http.impl.client.DefaultHttpClient r13 = r1.a
            org.apache.http.params.HttpParams r13 = r13.getParams()
            r13.setParameter(r11, r10)
            java.lang.String r11 = "X-Online-Host"
            java.lang.String r13 = com.fossil.w37.B
            r4.addHeader(r11, r13)
            java.lang.String r11 = "Accept"
        */
        //  java.lang.String r13 = "*/*"
        /*
            r4.addHeader(r11, r13)
            java.lang.String r11 = "Content-Type"
            java.lang.String r13 = "json"
            r4.addHeader(r11, r13)
        L_0x0119:
            java.io.ByteArrayOutputStream r11 = new java.io.ByteArrayOutputStream
            r11.<init>(r7)
            byte[] r0 = r0.getBytes(r5)
            int r13 = r0.length
            int r14 = com.fossil.w37.L
            if (r7 <= r14) goto L_0x0129
            r7 = 1
            goto L_0x012a
        L_0x0129:
            r7 = 0
        L_0x012a:
            if (r7 == 0) goto L_0x018e
            r4.removeHeaders(r6)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            java.lang.String r14 = ",gzip"
            r7.append(r14)
            java.lang.String r7 = r7.toString()
            r4.addHeader(r6, r7)
            if (r10 == 0) goto L_0x014b
            r4.removeHeaders(r12)
            r4.addHeader(r12, r7)
        L_0x014b:
            r7 = 4
            byte[] r10 = new byte[r7]
            r11.write(r10)
            java.util.zip.GZIPOutputStream r10 = new java.util.zip.GZIPOutputStream
            r10.<init>(r11)
            r10.write(r0)
            r10.close()
            byte[] r0 = r11.toByteArray()
            java.nio.ByteBuffer r7 = java.nio.ByteBuffer.wrap(r0, r8, r7)
            r7.putInt(r13)
            boolean r7 = com.fossil.w37.q()
            if (r7 == 0) goto L_0x018e
            com.fossil.k57 r7 = com.fossil.h67.e
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r10 = "before Gzip:"
            r8.<init>(r10)
            r8.append(r13)
            java.lang.String r10 = " bytes, after Gzip:"
            r8.append(r10)
            int r10 = r0.length
            r8.append(r10)
            java.lang.String r10 = " bytes"
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r7.a(r8)
        L_0x018e:
            byte[] r0 = com.fossil.q57.a(r0)
            org.apache.http.entity.ByteArrayEntity r7 = new org.apache.http.entity.ByteArrayEntity
            r7.<init>(r0)
            r4.setEntity(r7)
            org.apache.http.impl.client.DefaultHttpClient r0 = r1.a
            org.apache.http.HttpResponse r0 = r0.execute(r4)
            org.apache.http.HttpEntity r4 = r0.getEntity()
            org.apache.http.StatusLine r7 = r0.getStatusLine()
            int r7 = r7.getStatusCode()
            long r12 = r4.getContentLength()
            boolean r8 = com.fossil.w37.q()
            if (r8 == 0) goto L_0x01d1
            com.fossil.k57 r8 = com.fossil.h67.e
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r14 = "http recv response status code:"
            r10.<init>(r14)
            r10.append(r7)
            java.lang.String r14 = ", content length:"
            r10.append(r14)
            r10.append(r12)
            java.lang.String r10 = r10.toString()
            r8.e(r10)
        L_0x01d1:
            r14 = 0
            int r8 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r8 > 0) goto L_0x01e7
            com.fossil.k57 r0 = com.fossil.h67.e
            java.lang.String r2 = "Server response no data."
            r0.c(r2)
            if (r18 == 0) goto L_0x01e3
            r18.b()
        L_0x01e3:
            org.apache.http.util.EntityUtils.toString(r4)
            return
        L_0x01e7:
            if (r8 <= 0) goto L_0x02b9
            java.io.InputStream r8 = r4.getContent()
            java.io.DataInputStream r10 = new java.io.DataInputStream
            r10.<init>(r8)
            long r12 = r4.getContentLength()
            int r4 = (int) r12
            byte[] r4 = new byte[r4]
            r10.readFully(r4)
            r8.close()
            r10.close()
            org.apache.http.Header r0 = r0.getFirstHeader(r6)
            if (r0 == 0) goto L_0x024f
            java.lang.String r6 = r0.getValue()
            java.lang.String r10 = "gzip,rc4"
            boolean r6 = r6.equalsIgnoreCase(r10)
            if (r6 == 0) goto L_0x021d
            byte[] r0 = com.fossil.v57.a(r4)
            byte[] r4 = com.fossil.q57.b(r0)
            goto L_0x024f
        L_0x021d:
            java.lang.String r6 = r0.getValue()
            java.lang.String r10 = "rc4,gzip"
            boolean r6 = r6.equalsIgnoreCase(r10)
            if (r6 == 0) goto L_0x0232
            byte[] r0 = com.fossil.q57.b(r4)
            byte[] r4 = com.fossil.v57.a(r0)
            goto L_0x024f
        L_0x0232:
            java.lang.String r6 = r0.getValue()
            boolean r2 = r6.equalsIgnoreCase(r2)
            if (r2 == 0) goto L_0x0241
            byte[] r4 = com.fossil.v57.a(r4)
            goto L_0x024f
        L_0x0241:
            java.lang.String r0 = r0.getValue()
            boolean r0 = r0.equalsIgnoreCase(r3)
            if (r0 == 0) goto L_0x024f
            byte[] r4 = com.fossil.q57.b(r4)
        L_0x024f:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r4, r5)
            boolean r2 = com.fossil.w37.q()
            if (r2 == 0) goto L_0x026d
            com.fossil.k57 r2 = com.fossil.h67.e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r6 = "http get response data:"
            r3.<init>(r6)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r2.e(r3)
        L_0x026d:
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>(r0)
            r0 = 200(0xc8, float:2.8E-43)
            if (r7 != r0) goto L_0x0292
            r1.a(r2)
            if (r18 == 0) goto L_0x02b5
            java.lang.String r0 = "ret"
            int r0 = r2.optInt(r0)
            if (r0 != 0) goto L_0x0287
            r18.a()
            goto L_0x02b5
        L_0x0287:
            com.fossil.k57 r0 = com.fossil.h67.e
            java.lang.String r2 = "response error data."
            r0.d(r2)
        L_0x028e:
            r18.b()
            goto L_0x02b5
        L_0x0292:
            com.fossil.k57 r0 = com.fossil.h67.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Server response error code:"
            r2.<init>(r3)
            r2.append(r7)
            java.lang.String r3 = ", error:"
            r2.append(r3)
            java.lang.String r3 = new java.lang.String
            r3.<init>(r4, r5)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r2)
            if (r18 == 0) goto L_0x02b5
            goto L_0x028e
        L_0x02b5:
            r8.close()
            goto L_0x02bc
        L_0x02b9:
            org.apache.http.util.EntityUtils.toString(r4)
        L_0x02bc:
            r11.close()
            r2 = r9
            goto L_0x02c3
        L_0x02c1:
            r0 = move-exception
            r2 = r0
        L_0x02c3:
            if (r2 == 0) goto L_0x02f2
            com.fossil.k57 r0 = com.fossil.h67.e
            r0.b(r2)
            if (r18 == 0) goto L_0x02d7
            r18.b()     // Catch:{ all -> 0x02d0 }
            goto L_0x02d7
        L_0x02d0:
            r0 = move-exception
            r3 = r0
            com.fossil.k57 r0 = com.fossil.h67.e
            r0.a(r3)
        L_0x02d7:
            boolean r0 = r2 instanceof java.lang.OutOfMemoryError
            if (r0 == 0) goto L_0x02e9
            java.lang.System.gc()
            r1.c = r9
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r2 = 2048(0x800, float:2.87E-42)
            r0.<init>(r2)
            r1.c = r0
        L_0x02e9:
            android.content.Context r0 = com.fossil.h67.g
            com.fossil.k47 r0 = com.fossil.k47.a(r0)
            r0.d()
        L_0x02f2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.h67.a(java.util.List, com.fossil.g67):void");
    }

    @DexIgnore
    public final void a(JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("mid");
            if (j27.b(optString)) {
                if (w37.q()) {
                    k57 k57 = e;
                    k57.e("update mid:" + optString);
                }
                i27.a(g).a(optString);
            }
            if (!jSONObject.isNull("cfg")) {
                w37.a(g, jSONObject.getJSONObject("cfg"));
            }
            if (!jSONObject.isNull("ncts")) {
                int i = jSONObject.getInt("ncts");
                int currentTimeMillis = (int) (((long) i) - (System.currentTimeMillis() / 1000));
                if (w37.q()) {
                    k57 k572 = e;
                    k572.e("server time:" + i + ", diff time:" + currentTimeMillis);
                }
                v57.C(g);
                v57.a(g, currentTimeMillis);
            }
        } catch (Throwable th) {
            e.g(th);
        }
    }

    @DexIgnore
    public void b(List<?> list, g67 g67) {
        p57 p57 = this.b;
        if (p57 != null) {
            p57.a(new j67(this, list, g67));
        }
    }
}
