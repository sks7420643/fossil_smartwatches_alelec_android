package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class by4 extends ay4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131361979, 1);
        D.put(2131362234, 2);
        D.put(2131361972, 3);
        D.put(2131363003, 4);
        D.put(2131362466, 5);
        D.put(2131362923, 6);
        D.put(2131362495, 7);
        D.put(2131363342, 8);
        D.put(2131362636, 9);
        D.put(2131362751, 10);
    }
    */

    @DexIgnore
    public by4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 11, C, D));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public by4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[3], (ConstraintLayout) objArr[1], (FlexibleEditText) objArr[2], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[7], (RTLImageView) objArr[9], (View) objArr[10], (ProgressBar) objArr[6], (ConstraintLayout) objArr[0], (RecyclerView) objArr[4], (FlexibleTextView) objArr[8]);
        this.B = -1;
        ((ay4) this).y.setTag(null);
        a(view);
        f();
    }
}
