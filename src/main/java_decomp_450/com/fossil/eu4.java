package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.qg;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eu4 extends qg.i {
    @DexIgnore
    public /* final */ int f; // = 20;
    @DexIgnore
    public List<c> g; // = new ArrayList();
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public float i; // = 0.5f;
    @DexIgnore
    public Map<Integer, List<c>> j; // = new LinkedHashMap();
    @DexIgnore
    public GestureDetector k; // = new GestureDetector(this.o.getContext(), this.m);
    @DexIgnore
    public f l; // = new f();
    @DexIgnore
    public /* final */ d m; // = new d(this);
    @DexIgnore
    public /* final */ View.OnTouchListener n; // = new e(this);
    @DexIgnore
    public /* final */ RecyclerView o;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i, b bVar);
    }

    @DexIgnore
    public enum b {
        UNFRIEND,
        BLOCK,
        UNBLOCK,
        CANCEL
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public /* final */ /* synthetic */ eu4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(eu4 eu4) {
            this.a = eu4;
        }

        @DexIgnore
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            ee7.b(motionEvent, Constants.EVENT);
            if (this.a.h == -1) {
                return true;
            }
            for (c cVar : this.a.g) {
                int a2 = cVar.a(motionEvent.getX(), motionEvent.getY());
                if (a2 == this.a.h) {
                    this.a.h = -1;
                    continue;
                }
                if (a2 != -1) {
                    RecyclerView.g adapter = this.a.o.getAdapter();
                    if (adapter != null) {
                        adapter.notifyItemChanged(a2);
                    }
                    this.a.l.clear();
                    return true;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ eu4 a;

        @DexIgnore
        public e(eu4 eu4) {
            this.a = eu4;
        }

        @DexIgnore
        public final boolean onTouch(View view, MotionEvent motionEvent) {
            if (this.a.h < 0) {
                return false;
            }
            ee7.a((Object) motionEvent, Constants.EVENT);
            Point point = new Point((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
            RecyclerView.ViewHolder findViewHolderForAdapterPosition = this.a.o.findViewHolderForAdapterPosition(this.a.h);
            if (findViewHolderForAdapterPosition != null) {
                ee7.a((Object) findViewHolderForAdapterPosition, "recyclerView.findViewHol\u2026urn@OnTouchListener false");
                View view2 = findViewHolderForAdapterPosition.itemView;
                ee7.a((Object) view2, "swipedViewHolder.itemView");
                Rect rect = new Rect();
                view2.getGlobalVisibleRect(rect);
                if (motionEvent.getAction() == 0 || motionEvent.getAction() == 1 || motionEvent.getAction() == 2) {
                    int i = rect.top;
                    int i2 = point.y;
                    if (i >= i2 || rect.bottom <= i2) {
                        this.a.l.add(this.a.h);
                        this.a.h = -1;
                        this.a.e();
                    } else {
                        eu4.a(this.a).onTouchEvent(motionEvent);
                    }
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends LinkedList<Integer> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.util.Queue, java.util.LinkedList, java.util.Deque
        public /* bridge */ /* synthetic */ boolean add(Integer num) {
            return add(num.intValue());
        }

        @DexIgnore
        public /* bridge */ boolean contains(Integer num) {
            return super.contains((Object) num);
        }

        @DexIgnore
        public /* bridge */ int getSize() {
            return super.size();
        }

        @DexIgnore
        public /* bridge */ int indexOf(Integer num) {
            return super.indexOf((Object) num);
        }

        @DexIgnore
        public /* bridge */ int lastIndexOf(Integer num) {
            return super.lastIndexOf((Object) num);
        }

        @DexIgnore
        @Override // java.util.List, java.util.AbstractList, java.util.LinkedList, java.util.AbstractSequentialList
        public final /* bridge */ Integer remove(int i) {
            return removeAt(i);
        }

        @DexIgnore
        public /* bridge */ Integer removeAt(int i) {
            return (Integer) super.remove(i);
        }

        @DexIgnore
        public final /* bridge */ int size() {
            return getSize();
        }

        @DexIgnore
        public boolean add(int i) {
            if (contains((Object) Integer.valueOf(i))) {
                return false;
            }
            return super.add(Integer.valueOf(i));
        }

        @DexIgnore
        public final /* bridge */ boolean contains(Object obj) {
            if (obj instanceof Integer) {
                return contains((Integer) obj);
            }
            return false;
        }

        @DexIgnore
        public final /* bridge */ int indexOf(Object obj) {
            if (obj instanceof Integer) {
                return indexOf((Integer) obj);
            }
            return -1;
        }

        @DexIgnore
        public final /* bridge */ int lastIndexOf(Object obj) {
            if (obj instanceof Integer) {
                return lastIndexOf((Integer) obj);
            }
            return -1;
        }

        @DexIgnore
        public /* bridge */ boolean remove(Integer num) {
            return super.remove((Object) num);
        }

        @DexIgnore
        @Override // java.util.List, java.util.LinkedList
        public final /* bridge */ boolean remove(Object obj) {
            if (obj instanceof Integer) {
                return remove((Integer) obj);
            }
            return false;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eu4(RecyclerView recyclerView) {
        super(0, 4);
        ee7.b(recyclerView, "recyclerView");
        this.o = recyclerView;
        this.o.setOnTouchListener(this.n);
        d();
    }

    @DexIgnore
    public static final /* synthetic */ GestureDetector a(eu4 eu4) {
        GestureDetector gestureDetector = eu4.k;
        if (gestureDetector != null) {
            return gestureDetector;
        }
        ee7.d("gestureDetector");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public float a(float f2) {
        return f2 * 0.1f;
    }

    @DexIgnore
    public abstract void a(RecyclerView.ViewHolder viewHolder, List<c> list);

    @DexIgnore
    @Override // com.fossil.qg.f
    public float b(float f2) {
        return f2 * 5.0f;
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public boolean b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        ee7.b(recyclerView, "recyclerView");
        ee7.b(viewHolder, "viewHolder");
        ee7.b(viewHolder2, "target");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public void b(RecyclerView.ViewHolder viewHolder, int i2) {
        List<c> list;
        ee7.b(viewHolder, "viewHolder");
        int adapterPosition = viewHolder.getAdapterPosition();
        int i3 = this.h;
        if (i3 != adapterPosition) {
            this.l.add(i3);
        }
        this.h = adapterPosition;
        if (this.j.containsKey(Integer.valueOf(adapterPosition))) {
            List<c> list2 = this.j.get(Integer.valueOf(this.h));
            if (list2 == null || (list = ea7.d((Collection) list2)) == null) {
                list = new ArrayList<>();
            }
            this.g = list;
        }
        this.j.clear();
        View view = viewHolder.itemView;
        ee7.a((Object) view, "viewHolder.itemView");
        this.i = ((float) this.g.size()) * 0.5f * ((float) (view.getWidth() + this.f));
        e();
    }

    @DexIgnore
    public final void d() {
        new qg(this).a(this.o);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0011  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void e() {
        /*
            r3 = this;
            monitor-enter(r3)
        L_0x0001:
            com.fossil.eu4$f r0 = r3.l     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x000e
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x000c
            goto L_0x000e
        L_0x000c:
            r0 = 0
            goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            if (r0 != 0) goto L_0x003b
            com.fossil.eu4$f r0 = r3.l     // Catch:{ all -> 0x003d }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x003d }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0001
            int r1 = r0.intValue()     // Catch:{ all -> 0x003d }
            r2 = -1
            int r1 = com.fossil.ee7.a(r1, r2)     // Catch:{ all -> 0x003d }
            if (r1 <= 0) goto L_0x0001
            androidx.recyclerview.widget.RecyclerView r1 = r3.o     // Catch:{ all -> 0x003d }
            androidx.recyclerview.widget.RecyclerView$g r1 = r1.getAdapter()     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x0036
            int r0 = r0.intValue()     // Catch:{ all -> 0x003d }
            r1.notifyItemChanged(r0)     // Catch:{ all -> 0x003d }
            goto L_0x0001
        L_0x0036:
            com.fossil.ee7.a()     // Catch:{ all -> 0x003d }
            r0 = 0
            throw r0
        L_0x003b:
            monitor-exit(r3)
            return
        L_0x003d:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eu4.e():void");
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f2, float f3, int i2, boolean z) {
        float f4;
        ee7.b(canvas, "c");
        ee7.b(recyclerView, "recyclerView");
        ee7.b(viewHolder, "viewHolder");
        int adapterPosition = viewHolder.getAdapterPosition();
        View view = viewHolder.itemView;
        ee7.a((Object) view, "viewHolder.itemView");
        if (adapterPosition < 0) {
            this.h = adapterPosition;
            return;
        }
        if (i2 == 1) {
            if (f2 < ((float) 0)) {
                ArrayList arrayList = new ArrayList();
                a(viewHolder, arrayList);
                this.j.put(Integer.valueOf(adapterPosition), arrayList);
                float size = ((((float) arrayList.size()) * f2) * ((float) (view.getHeight() + this.f))) / ((float) view.getWidth());
                a(canvas, view, arrayList, adapterPosition, size);
                f4 = size;
                super.a(canvas, recyclerView, viewHolder, f4, f3, i2, z);
            }
            this.h = -1;
        }
        f4 = f2;
        super.a(canvas, recyclerView, viewHolder, f4, f3, i2, z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public int a;
        @DexIgnore
        public RectF b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ Integer d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ b f;
        @DexIgnore
        public /* final */ a g;

        @DexIgnore
        public c(String str, Integer num, int i, b bVar, a aVar) {
            ee7.b(str, "text");
            ee7.b(bVar, "type");
            ee7.b(aVar, "listener");
            this.c = str;
            this.d = num;
            this.e = i;
            this.f = bVar;
            this.g = aVar;
        }

        @DexIgnore
        public final int a(float f2, float f3) {
            RectF rectF = this.b;
            if (rectF == null) {
                return -1;
            }
            if (rectF == null) {
                ee7.a();
                throw null;
            } else if (!rectF.contains(f2, f3)) {
                return -1;
            } else {
                this.g.a(this.a, this.f);
                return this.a;
            }
        }

        @DexIgnore
        public final void a(Canvas canvas, RectF rectF, int i) {
            ee7.b(canvas, "canvas");
            ee7.b(rectF, "rect");
            Paint paint = new Paint(1);
            paint.setColor(this.e);
            canvas.drawRect(rectF, paint);
            if (this.d == null) {
                paint.setColor(-1);
                paint.setTextSize(28.0f);
                Rect rect = new Rect();
                paint.setTextAlign(Paint.Align.LEFT);
                String str = this.c;
                paint.getTextBounds(str, 0, str.length(), rect);
                canvas.drawText(this.c, rectF.left + (((rectF.width() / 2.0f) - (((float) rect.width()) / 2.0f)) - ((float) rect.left)), rectF.top + (((rectF.height() / 2.0f) + (((float) rect.height()) / 2.0f)) - ((float) rect.bottom)), paint);
            } else {
                Bitmap decodeResource = BitmapFactory.decodeResource(PortfolioApp.g0.c().getResources(), this.d.intValue());
                ee7.a((Object) decodeResource, "bitmap");
                canvas.drawBitmap(decodeResource, rectF.left + ((rectF.width() / 2.0f) - (((float) decodeResource.getWidth()) / 2.0f)), rectF.top + ((rectF.height() / 2.0f) - (((float) decodeResource.getHeight()) / 2.0f)), paint);
            }
            this.b = rectF;
            this.a = i;
        }
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public float b(RecyclerView.ViewHolder viewHolder) {
        ee7.b(viewHolder, "viewHolder");
        return this.i;
    }

    @DexIgnore
    public final void a(Canvas canvas, View view, List<c> list, int i2, float f2) {
        float right = (float) view.getRight();
        float size = (((float) -1) * f2) / ((float) list.size());
        for (c cVar : list) {
            float f3 = right - size;
            cVar.a(canvas, new RectF(f3, (float) view.getTop(), right, (float) view.getBottom()), i2);
            right = f3;
        }
    }
}
