package com.fossil;

import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "kotlin.collections.SlidingWindowKt$windowedIterator$1", f = "SlidingWindow.kt", l = {34, 40, 49, 55, 58}, m = "invokeSuspend")
    public static final class a extends yb7 implements kd7<jg7<? super List<? extends T>>, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator $iterator;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $partialWindows;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $reuseBuffer;
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public /* final */ /* synthetic */ int $step;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public jg7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, Iterator it, boolean z, boolean z2, fb7 fb7) {
            super(2, fb7);
            this.$size = i;
            this.$step = i2;
            this.$iterator = it;
            this.$reuseBuffer = z;
            this.$partialWindows = z2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.$size, this.$step, this.$iterator, this.$reuseBuffer, this.$partialWindows, fb7);
            aVar.p$ = (jg7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(Object obj, fb7<? super i97> fb7) {
            return ((a) create(obj, fb7)).invokeSuspend(i97.a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:46:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x0150  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x015c  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x014c A[SYNTHETIC] */
        @Override // com.fossil.ob7
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 5
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                if (r1 == 0) goto L_0x006e
                if (r1 == r6) goto L_0x0059
                if (r1 == r5) goto L_0x004c
                if (r1 == r4) goto L_0x0036
                if (r1 == r3) goto L_0x0024
                if (r1 != r2) goto L_0x001c
                java.lang.Object r0 = r14.L$1
                com.fossil.qa7 r0 = (com.fossil.qa7) r0
                goto L_0x0050
            L_0x001c:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0024:
                java.lang.Object r1 = r14.L$1
                com.fossil.qa7 r1 = (com.fossil.qa7) r1
                int r4 = r14.I$1
                int r5 = r14.I$0
                java.lang.Object r7 = r14.L$0
                com.fossil.jg7 r7 = (com.fossil.jg7) r7
                com.fossil.t87.a(r15)
                r15 = r14
                goto L_0x0178
            L_0x0036:
                java.lang.Object r1 = r14.L$3
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r5 = r14.L$1
                com.fossil.qa7 r5 = (com.fossil.qa7) r5
                int r7 = r14.I$1
                int r8 = r14.I$0
                java.lang.Object r9 = r14.L$0
                com.fossil.jg7 r9 = (com.fossil.jg7) r9
                com.fossil.t87.a(r15)
                r15 = r14
                goto L_0x0146
            L_0x004c:
                java.lang.Object r0 = r14.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
            L_0x0050:
                java.lang.Object r0 = r14.L$0
                com.fossil.jg7 r0 = (com.fossil.jg7) r0
                com.fossil.t87.a(r15)
                goto L_0x0196
            L_0x0059:
                java.lang.Object r1 = r14.L$3
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r2 = r14.L$1
                java.util.ArrayList r2 = (java.util.ArrayList) r2
                int r3 = r14.I$1
                int r4 = r14.I$0
                java.lang.Object r7 = r14.L$0
                com.fossil.jg7 r7 = (com.fossil.jg7) r7
                com.fossil.t87.a(r15)
                r8 = r14
                goto L_0x00c1
            L_0x006e:
                com.fossil.t87.a(r15)
                com.fossil.jg7 r15 = r14.p$
                int r1 = r14.$size
                r7 = 1024(0x400, float:1.435E-42)
                int r1 = com.fossil.qf7.b(r1, r7)
                int r7 = r14.$step
                int r8 = r14.$size
                int r7 = r7 - r8
                if (r7 < 0) goto L_0x00fa
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>(r1)
                r3 = 0
                java.util.Iterator r4 = r14.$iterator
                r8 = r14
                r13 = r4
                r4 = r1
                r1 = r13
            L_0x008e:
                boolean r9 = r1.hasNext()
                if (r9 == 0) goto L_0x00d4
                java.lang.Object r9 = r1.next()
                if (r3 <= 0) goto L_0x009d
                int r3 = r3 + -1
                goto L_0x008e
            L_0x009d:
                r2.add(r9)
                int r10 = r2.size()
                int r11 = r8.$size
                if (r10 != r11) goto L_0x008e
                r8.L$0 = r15
                r8.I$0 = r4
                r8.I$1 = r7
                r8.L$1 = r2
                r8.I$2 = r3
                r8.L$2 = r9
                r8.L$3 = r1
                r8.label = r6
                java.lang.Object r3 = r15.a(r2, r8)
                if (r3 != r0) goto L_0x00bf
                return r0
            L_0x00bf:
                r3 = r7
                r7 = r15
            L_0x00c1:
                boolean r15 = r8.$reuseBuffer
                if (r15 == 0) goto L_0x00c9
                r2.clear()
                goto L_0x00d1
            L_0x00c9:
                java.util.ArrayList r15 = new java.util.ArrayList
                int r2 = r8.$size
                r15.<init>(r2)
                r2 = r15
            L_0x00d1:
                r15 = r7
                r7 = r3
                goto L_0x008e
            L_0x00d4:
                boolean r1 = r2.isEmpty()
                r1 = r1 ^ r6
                if (r1 == 0) goto L_0x0196
                boolean r1 = r8.$partialWindows
                if (r1 != 0) goto L_0x00e7
                int r1 = r2.size()
                int r6 = r8.$size
                if (r1 != r6) goto L_0x0196
            L_0x00e7:
                r8.L$0 = r15
                r8.I$0 = r4
                r8.I$1 = r7
                r8.L$1 = r2
                r8.I$2 = r3
                r8.label = r5
                java.lang.Object r15 = r15.a(r2, r8)
                if (r15 != r0) goto L_0x0196
                return r0
            L_0x00fa:
                com.fossil.qa7 r5 = new com.fossil.qa7
                r5.<init>(r1)
                java.util.Iterator r8 = r14.$iterator
                r9 = r15
                r15 = r14
                r13 = r8
                r8 = r1
                r1 = r13
            L_0x0106:
                boolean r10 = r1.hasNext()
                if (r10 == 0) goto L_0x014c
                java.lang.Object r10 = r1.next()
                r5.add(r10)
                boolean r11 = r5.b()
                if (r11 == 0) goto L_0x0106
                int r11 = r5.size()
                int r12 = r15.$size
                if (r11 >= r12) goto L_0x0126
                com.fossil.qa7 r5 = r5.a(r12)
                goto L_0x0106
            L_0x0126:
                boolean r11 = r15.$reuseBuffer
                if (r11 == 0) goto L_0x012c
                r11 = r5
                goto L_0x0131
            L_0x012c:
                java.util.ArrayList r11 = new java.util.ArrayList
                r11.<init>(r5)
            L_0x0131:
                r15.L$0 = r9
                r15.I$0 = r8
                r15.I$1 = r7
                r15.L$1 = r5
                r15.L$2 = r10
                r15.L$3 = r1
                r15.label = r4
                java.lang.Object r10 = r9.a(r11, r15)
                if (r10 != r0) goto L_0x0146
                return r0
            L_0x0146:
                int r10 = r15.$step
                r5.b(r10)
                goto L_0x0106
            L_0x014c:
                boolean r1 = r15.$partialWindows
                if (r1 == 0) goto L_0x0196
                r1 = r5
                r4 = r7
                r5 = r8
                r7 = r9
            L_0x0154:
                int r8 = r1.size()
                int r9 = r15.$step
                if (r8 <= r9) goto L_0x017e
                boolean r8 = r15.$reuseBuffer
                if (r8 == 0) goto L_0x0162
                r8 = r1
                goto L_0x0167
            L_0x0162:
                java.util.ArrayList r8 = new java.util.ArrayList
                r8.<init>(r1)
            L_0x0167:
                r15.L$0 = r7
                r15.I$0 = r5
                r15.I$1 = r4
                r15.L$1 = r1
                r15.label = r3
                java.lang.Object r8 = r7.a(r8, r15)
                if (r8 != r0) goto L_0x0178
                return r0
            L_0x0178:
                int r8 = r15.$step
                r1.b(r8)
                goto L_0x0154
            L_0x017e:
                boolean r3 = r1.isEmpty()
                r3 = r3 ^ r6
                if (r3 == 0) goto L_0x0196
                r15.L$0 = r7
                r15.I$0 = r5
                r15.I$1 = r4
                r15.L$1 = r1
                r15.label = r2
                java.lang.Object r15 = r7.a(r1, r15)
                if (r15 != r0) goto L_0x0196
                return r0
            L_0x0196:
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ta7.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public static final void a(int i, int i2) {
        String str;
        if (!(i > 0 && i2 > 0)) {
            if (i != i2) {
                str = "Both size " + i + " and step " + i2 + " must be greater than zero.";
            } else {
                str = "size " + i + " must be greater than zero.";
            }
            throw new IllegalArgumentException(str.toString());
        }
    }

    @DexIgnore
    public static final <T> Iterator<List<T>> a(Iterator<? extends T> it, int i, int i2, boolean z, boolean z2) {
        ee7.b(it, "iterator");
        if (!it.hasNext()) {
            return fa7.a;
        }
        return kg7.a(new a(i, i2, it, z2, z, null));
    }
}
