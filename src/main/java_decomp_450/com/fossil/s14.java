package com.fossil;

import android.os.Bundle;
import com.fossil.eb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s14 implements eb3.a {
    @DexIgnore
    public /* final */ /* synthetic */ t14 a;

    @DexIgnore
    public s14(t14 t14) {
        this.a = t14;
    }

    @DexIgnore
    @Override // com.fossil.ri3
    public final void a(String str, String str2, Bundle bundle, long j) {
        if (this.a.a.contains(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("events", q14.c(str2));
            this.a.b.a(2, bundle2);
        }
    }
}
