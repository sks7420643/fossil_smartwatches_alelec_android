package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fl extends kl implements el {
    @DexIgnore
    public b b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public ArgbEvaluator d;
    @DexIgnore
    public /* final */ Drawable.Callback e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Drawable.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
            fl.this.invalidateSelf();
        }

        @DexIgnore
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            fl.this.scheduleSelf(runnable, j);
        }

        @DexIgnore
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            fl.this.unscheduleSelf(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Drawable.ConstantState {
        @DexIgnore
        public int a;
        @DexIgnore
        public ll b;
        @DexIgnore
        public AnimatorSet c;
        @DexIgnore
        public ArrayList<Animator> d;
        @DexIgnore
        public n4<Animator, String> e;

        @DexIgnore
        public b(Context context, b bVar, Drawable.Callback callback, Resources resources) {
            if (bVar != null) {
                this.a = bVar.a;
                ll llVar = bVar.b;
                if (llVar != null) {
                    Drawable.ConstantState constantState = llVar.getConstantState();
                    if (resources != null) {
                        this.b = (ll) constantState.newDrawable(resources);
                    } else {
                        this.b = (ll) constantState.newDrawable();
                    }
                    ll llVar2 = this.b;
                    llVar2.mutate();
                    ll llVar3 = llVar2;
                    this.b = llVar3;
                    llVar3.setCallback(callback);
                    this.b.setBounds(bVar.b.getBounds());
                    this.b.a(false);
                }
                ArrayList<Animator> arrayList = bVar.d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.d = new ArrayList<>(size);
                    this.e = new n4<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = bVar.d.get(i);
                        Animator clone = animator.clone();
                        String str = bVar.e.get(animator);
                        clone.setTarget(this.b.a(str));
                        this.d.add(clone);
                        this.e.put(clone, str);
                    }
                    a();
                }
            }
        }

        @DexIgnore
        public void a() {
            if (this.c == null) {
                this.c = new AnimatorSet();
            }
            this.c.playTogether(this.d);
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a;
        }

        @DexIgnore
        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    @DexIgnore
    public fl() {
        this(null, null, null);
    }

    @DexIgnore
    public static fl a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        fl flVar = new fl(context);
        flVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return flVar;
    }

    @DexIgnore
    @Override // com.fossil.kl
    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            p7.a(drawable, theme);
        }
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return p7.a(drawable);
        }
        return false;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.b.b.draw(canvas);
        if (this.b.c.isStarted()) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return p7.c(drawable);
        }
        return this.b.b.getAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.b.a;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return p7.d(drawable);
        }
        return this.b.b.getColorFilter();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        if (((kl) this).a == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new c(((kl) this).a.getConstantState());
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.b.b.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.b.b.getIntrinsicWidth();
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.b.b.getOpacity();
    }

    @DexIgnore
    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            p7.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray a2 = d7.a(resources, theme, attributeSet, dl.e);
                    int resourceId = a2.getResourceId(0, 0);
                    if (resourceId != 0) {
                        ll a3 = ll.a(resources, resourceId, theme);
                        a3.a(false);
                        a3.setCallback(this.e);
                        ll llVar = this.b.b;
                        if (llVar != null) {
                            llVar.setCallback(null);
                        }
                        this.b.b = a3;
                    }
                    a2.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, dl.f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.c;
                        if (context != null) {
                            a(string, hl.a(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.b.a();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return p7.f(drawable);
        }
        return this.b.b.isAutoMirrored();
    }

    @DexIgnore
    public boolean isRunning() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return ((AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.b.c.isRunning();
    }

    @DexIgnore
    public boolean isStateful() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.b.b.isStateful();
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.b.b.setBounds(rect);
        }
    }

    @DexIgnore
    @Override // com.fossil.kl
    public boolean onLevelChange(int i) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.setLevel(i);
        }
        return this.b.b.setLevel(i);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.b.b.setState(iArr);
    }

    @DexIgnore
    public void setAlpha(int i) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            drawable.setAlpha(i);
        } else {
            this.b.b.setAlpha(i);
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            p7.a(drawable, z);
        } else {
            this.b.b.setAutoMirrored(z);
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.b.b.setColorFilter(colorFilter);
        }
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTint(int i) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            p7.b(drawable, i);
        } else {
            this.b.b.setTint(i);
        }
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            p7.a(drawable, colorStateList);
        } else {
            this.b.b.setTintList(colorStateList);
        }
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            p7.a(drawable, mode);
        } else {
            this.b.b.setTintMode(mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.b.b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (!this.b.c.isStarted()) {
            this.b.c.start();
            invalidateSelf();
        }
    }

    @DexIgnore
    public void stop() {
        Drawable drawable = ((kl) this).a;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.b.c.end();
        }
    }

    @DexIgnore
    public fl(Context context) {
        this(context, null, null);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ Drawable.ConstantState a;

        @DexIgnore
        public c(Drawable.ConstantState constantState) {
            this.a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            fl flVar = new fl();
            Drawable newDrawable = this.a.newDrawable();
            ((kl) flVar).a = newDrawable;
            newDrawable.setCallback(flVar.e);
            return flVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            fl flVar = new fl();
            Drawable newDrawable = this.a.newDrawable(resources);
            ((kl) flVar).a = newDrawable;
            newDrawable.setCallback(flVar.e);
            return flVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            fl flVar = new fl();
            Drawable newDrawable = this.a.newDrawable(resources, theme);
            ((kl) flVar).a = newDrawable;
            newDrawable.setCallback(flVar.e);
            return flVar;
        }
    }

    @DexIgnore
    public fl(Context context, b bVar, Resources resources) {
        this.d = null;
        this.e = new a();
        this.c = context;
        if (bVar != null) {
            this.b = bVar;
        } else {
            this.b = new b(context, bVar, this.e, resources);
        }
    }

    @DexIgnore
    public final void a(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i = 0; i < childAnimations.size(); i++) {
                a(childAnimations.get(i));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.d == null) {
                    this.d = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.d);
            }
        }
    }

    @DexIgnore
    public final void a(String str, Animator animator) {
        animator.setTarget(this.b.b.a(str));
        if (Build.VERSION.SDK_INT < 21) {
            a(animator);
        }
        b bVar = this.b;
        if (bVar.d == null) {
            bVar.d = new ArrayList<>();
            this.b.e = new n4<>();
        }
        this.b.d.add(animator);
        this.b.e.put(animator, str);
    }

    @DexIgnore
    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, null);
    }
}
