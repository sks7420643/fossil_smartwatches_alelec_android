package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gf4 {
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        throw new com.fossil.ge4(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        throw new com.fossil.oe4(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002a, code lost:
        return com.fossil.he4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0030, code lost:
        throw new com.fossil.oe4(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        throw new com.fossil.oe4(r2);
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0016 A[ExcHandler: IOException (r2v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001d A[ExcHandler: qf4 (r2v4 'e' com.fossil.qf4 A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x000f A[ExcHandler: NumberFormatException (r2v6 'e' java.lang.NumberFormatException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.gson.JsonElement a(com.google.gson.stream.JsonReader r2) throws com.fossil.je4 {
        /*
            r2.peek()     // Catch:{ EOFException -> 0x0024, qf4 -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            r0 = 0
            com.google.gson.TypeAdapter<com.google.gson.JsonElement> r1 = com.google.gson.internal.bind.TypeAdapters.X     // Catch:{ EOFException -> 0x000d, qf4 -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            java.lang.Object r2 = r1.read(r2)     // Catch:{ EOFException -> 0x000d, qf4 -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            com.google.gson.JsonElement r2 = (com.google.gson.JsonElement) r2     // Catch:{ EOFException -> 0x000d, qf4 -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            return r2
        L_0x000d:
            r2 = move-exception
            goto L_0x0026
        L_0x000f:
            r2 = move-exception
            com.fossil.oe4 r0 = new com.fossil.oe4
            r0.<init>(r2)
            throw r0
        L_0x0016:
            r2 = move-exception
            com.fossil.ge4 r0 = new com.fossil.ge4
            r0.<init>(r2)
            throw r0
        L_0x001d:
            r2 = move-exception
            com.fossil.oe4 r0 = new com.fossil.oe4
            r0.<init>(r2)
            throw r0
        L_0x0024:
            r2 = move-exception
            r0 = 1
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            com.fossil.he4 r2 = com.fossil.he4.a
            return r2
        L_0x002b:
            com.fossil.oe4 r0 = new com.fossil.oe4
            r0.<init>(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gf4.a(com.google.gson.stream.JsonReader):com.google.gson.JsonElement");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Writer {
        @DexIgnore
        public /* final */ Appendable a;
        @DexIgnore
        public /* final */ C0068a b; // = new C0068a();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gf4$a$a")
        /* renamed from: com.fossil.gf4$a$a  reason: collision with other inner class name */
        public static class C0068a implements CharSequence {
            @DexIgnore
            public char[] a;

            @DexIgnore
            public char charAt(int i) {
                return this.a[i];
            }

            @DexIgnore
            public int length() {
                return this.a.length;
            }

            @DexIgnore
            public CharSequence subSequence(int i, int i2) {
                return new String(this.a, i, i2 - i);
            }
        }

        @DexIgnore
        public a(Appendable appendable) {
            this.a = appendable;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() {
        }

        @DexIgnore
        @Override // java.io.Writer, java.io.Flushable
        public void flush() {
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) throws IOException {
            C0068a aVar = this.b;
            aVar.a = cArr;
            this.a.append(aVar, i, i2 + i);
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(int i) throws IOException {
            this.a.append((char) i);
        }
    }

    @DexIgnore
    public static void a(JsonElement jsonElement, JsonWriter jsonWriter) throws IOException {
        TypeAdapters.X.write(jsonWriter, jsonElement);
    }

    @DexIgnore
    public static Writer a(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new a(appendable);
    }
}
