package com.fossil;

import java.io.Serializable;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ts7 extends Writer implements Serializable {
    @DexIgnore
    public /* final */ StringBuilder builder;

    @DexIgnore
    public ts7() {
        this.builder = new StringBuilder();
    }

    @DexIgnore
    @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
    public void close() {
    }

    @DexIgnore
    @Override // java.io.Writer, java.io.Flushable
    public void flush() {
    }

    @DexIgnore
    public StringBuilder getBuilder() {
        return this.builder;
    }

    @DexIgnore
    public String toString() {
        return this.builder.toString();
    }

    @DexIgnore
    @Override // java.io.Writer
    public void write(String str) {
        if (str != null) {
            this.builder.append(str);
        }
    }

    @DexIgnore
    @Override // java.io.Writer
    public void write(char[] cArr, int i, int i2) {
        if (cArr != null) {
            this.builder.append(cArr, i, i2);
        }
    }

    @DexIgnore
    public ts7(int i) {
        this.builder = new StringBuilder(i);
    }

    @DexIgnore
    @Override // java.lang.Appendable, java.io.Writer, java.io.Writer
    public Writer append(char c) {
        this.builder.append(c);
        return this;
    }

    @DexIgnore
    public ts7(StringBuilder sb) {
        this.builder = sb == null ? new StringBuilder() : sb;
    }

    @DexIgnore
    @Override // java.lang.Appendable, java.io.Writer, java.io.Writer
    public Writer append(CharSequence charSequence) {
        this.builder.append(charSequence);
        return this;
    }

    @DexIgnore
    @Override // java.lang.Appendable, java.io.Writer, java.io.Writer
    public Writer append(CharSequence charSequence, int i, int i2) {
        this.builder.append(charSequence, i, i2);
        return this;
    }
}
