package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zk0 {
    @DexIgnore
    public static /* final */ vm0 B; // = new vm0(null);
    @DexIgnore
    public boolean A;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public v81 b;
    @DexIgnore
    public /* final */ jb1 c;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<zk0, i97>> d;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<zk0, i97>> e;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<zk0, i97>> f;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<zk0, i97>> g;
    @DexIgnore
    public CopyOnWriteArrayList<kd7<zk0, Float, i97>> h;
    @DexIgnore
    public /* final */ ArrayList<ul0> i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public a61 k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public zk0 n;
    @DexIgnore
    public /* final */ Handler o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ db1 q;
    @DexIgnore
    public /* final */ jh1 r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public eu0 v;
    @DexIgnore
    public /* final */ ri1 w;
    @DexIgnore
    public /* final */ en0 x;
    @DexIgnore
    public /* final */ wm0 y;
    @DexIgnore
    public String z;

    @DexIgnore
    public zk0(ri1 ri1, en0 en0, wm0 wm0, String str, boolean z2) {
        this.w = ri1;
        this.x = en0;
        this.y = wm0;
        this.z = str;
        this.A = z2;
        this.a = yz0.a(wm0);
        this.c = new zb1(this);
        this.d = new CopyOnWriteArrayList<>();
        this.e = new CopyOnWriteArrayList<>();
        this.f = new CopyOnWriteArrayList<>();
        this.g = new CopyOnWriteArrayList<>();
        this.h = new CopyOnWriteArrayList<>();
        this.i = w97.a((Object[]) new ul0[]{ul0.DEVICE_INFORMATION});
        yk0 yk0 = yk0.a;
        this.k = a61.NORMAL;
        this.l = System.currentTimeMillis();
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.o = new Handler(myLooper);
            this.p = 30000;
            this.q = new db1(new gj1(this));
            this.r = new jh1(this);
            if (this.A) {
                wl0.h.a(new wr1(yz0.a(this.y), ci1.a, this.w.u, yz0.a(this.y), this.z, true, null, null, null, null, 960));
            }
            this.v = new eu0(this.y, is0.NOT_START, null, 4);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public v81 a(qa1 qa1) {
        return null;
    }

    @DexIgnore
    public final void a(db1 db1) {
        if (!db1.a) {
            db1.a = true;
            this.o.removeCallbacksAndMessages(db1);
        }
    }

    @DexIgnore
    public boolean a(v81 v81) {
        return false;
    }

    @DexIgnore
    public boolean b() {
        return this.j;
    }

    @DexIgnore
    public final boolean c() {
        return b() && this.x.a().e().compareTo(b21.x.k()) >= 0;
    }

    @DexIgnore
    public Object d() {
        return i97.a;
    }

    @DexIgnore
    public a61 e() {
        return this.k;
    }

    @DexIgnore
    public ArrayList<ul0> f() {
        return this.i;
    }

    @DexIgnore
    public final long g() {
        return this.l;
    }

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public JSONObject i() {
        return new JSONObject();
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public JSONObject k() {
        return new JSONObject();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        r0 = com.fossil.le0.DEBUG;
        f().toString();
        r0 = com.fossil.le0.DEBUG;
        r0 = r5.p;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        if (r0 <= 0) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        r5.o.postDelayed(r5.q, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        r0 = r5.x.b();
        r1 = r5.r;
        r0.a.b(r5);
        r0.c.put(r5, r1);
        r0.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void l() {
        /*
            r5 = this;
            com.fossil.u31 r0 = com.fossil.u31.g
            java.lang.Object r0 = r0.f()
            r1 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r1)
            boolean r0 = com.fossil.ee7.a(r0, r2)
            if (r0 == 0) goto L_0x0056
            boolean r0 = r5.s
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            monitor-enter(r0)
            boolean r2 = r5.s     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x001e
            monitor-exit(r0)
            return
        L_0x001e:
            r5.s = r1
            com.fossil.i97 r1 = com.fossil.i97.a
            monitor-exit(r0)
            com.fossil.le0 r0 = com.fossil.le0.DEBUG
            java.util.ArrayList r0 = r5.f()
            r0.toString()
            com.fossil.le0 r0 = com.fossil.le0.DEBUG
            long r0 = r5.p
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x003d
            android.os.Handler r2 = r5.o
            com.fossil.db1 r3 = r5.q
            r2.postDelayed(r3, r0)
        L_0x003d:
            com.fossil.en0 r0 = r5.x
            com.fossil.u01 r0 = r0.b()
            com.fossil.jh1 r1 = r5.r
            com.fossil.xc1<com.fossil.zk0> r2 = r0.a
            r2.b(r5)
            java.util.Hashtable<com.fossil.zk0, com.fossil.jh1> r2 = r0.c
            r2.put(r5, r1)
            r0.d()
            goto L_0x0063
        L_0x0053:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0056:
            com.fossil.eu0 r0 = r5.v
            com.fossil.is0 r1 = com.fossil.is0.SUCCESS
            r2 = 5
            r3 = 0
            com.fossil.eu0 r0 = com.fossil.eu0.a(r0, r3, r1, r3, r2)
            r5.a(r0)
        L_0x0063:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zk0.l():void");
    }

    @DexIgnore
    public String toString() {
        return yz0.a(this.y) + '(' + this.z + ')';
    }

    @DexIgnore
    public final boolean b(zk0 zk0) {
        if (ee7.a(this.n, zk0)) {
            return true;
        }
        zk0 zk02 = this.n;
        return zk02 != null && zk02.b(zk0);
    }

    @DexIgnore
    public final zk0 b(gd7<? super zk0, i97> gd7) {
        if (!this.t) {
            this.f.add(gd7);
        } else if (this.v.b != is0.SUCCESS) {
            gd7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final zk0 c(gd7<? super zk0, i97> gd7) {
        if (!this.t) {
            this.e.add(gd7);
        } else if (this.v.b == is0.SUCCESS) {
            gd7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public void a(a61 a61) {
        this.k = a61;
    }

    @DexIgnore
    public boolean a(zk0 zk0) {
        return b(zk0) || this.y != zk0.y;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.fossil.gd7<com.fossil.v81, com.fossil.i97>> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v15, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.fossil.kd7<com.fossil.v81, java.lang.Float, com.fossil.i97>> */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void a(zk0 zk0, v81 v81, gd7 gd7, gd7 gd72, kd7 kd7, gd7 gd73, gd7 gd74, int i2, Object obj) {
        boolean z2;
        if (obj == null) {
            if ((i2 & 8) != 0) {
                kd7 = qx0.a;
            }
            if ((i2 & 16) != 0) {
                gd73 = iz0.a;
            }
            if ((i2 & 32) != 0) {
                gd74 = b11.a;
            }
            if (zk0.t || ((z2 = zk0.u) && (!z2 || !zk0.a(v81)))) {
                zk0.a(zk0.v);
                return;
            }
            v81.w = v81.w && zk0.A;
            String str = zk0.z;
            v81.d = str;
            wr1 wr1 = v81.f;
            if (wr1 != null) {
                wr1.h = str;
            }
            String a2 = yz0.a(zk0.y);
            v81.c = a2;
            wr1 wr12 = v81.f;
            if (wr12 != null) {
                wr12.g = a2;
            }
            zk0.b = v81;
            if (v81 != null) {
                v81.b(gd7);
                v81.a(new t21(zk0, gd74, gd72));
                if (!v81.t) {
                    v81.o.add(kd7);
                }
                if (!v81.t) {
                    v81.n.add(gd73);
                } else {
                    gd73.invoke(v81);
                }
                if (!v81.t) {
                    le0 le0 = le0.DEBUG;
                    v81.g().toString(2);
                    long currentTimeMillis = System.currentTimeMillis();
                    wr1 wr13 = v81.f;
                    if (wr13 != null) {
                        wr13.a = currentTimeMillis;
                    }
                    wr1 wr14 = v81.f;
                    if (wr14 != null) {
                        wr14.m = yz0.a(wr14.m, yz0.a(yz0.a(yz0.a(v81.g(), r51.A1, Double.valueOf(yz0.a(v81.e))), r51.r1, Double.valueOf(yz0.a(currentTimeMillis))), r51.t1, Long.valueOf(v81.e())));
                    }
                    v81.a(v81.p);
                    ri1 ri1 = v81.y;
                    b51 b51 = v81.q;
                    if (!ri1.f.contains(b51)) {
                        ri1.f.add(b51);
                    }
                    ri1 ri12 = v81.y;
                    y61 y61 = v81.r;
                    if (!ri12.g.contains(y61)) {
                        ri12.g.add(y61);
                    }
                    v81.b();
                    return;
                }
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeRequest");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zk0(ri1 ri1, en0 en0, wm0 wm0, String str, boolean z2, int i2) {
        this(ri1, en0, wm0, (i2 & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, (i2 & 16) != 0 ? true : z2);
    }

    @DexIgnore
    public final void a(sz0 sz0) {
        this.v = eu0.a(this.v, null, is0.G.a(sz0), sz0, 1);
        a();
    }

    @DexIgnore
    public final void a(eu0 eu0) {
        this.v = eu0.a(eu0, this.y, null, null, 6);
        a();
    }

    @DexIgnore
    public final void a() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            this.w.h.remove(this.c);
            u01 b2 = this.x.b();
            b2.e.b(this);
            b2.a.remove(this);
            b2.b.remove(this);
            b2.c.remove(this);
            JSONObject a2 = yz0.a(yz0.a(yz0.a(new JSONObject(), r51.j, B.a(this.v)), r51.M0, yz0.a(this.v.b)), k());
            if (this.v.c.c != ay0.a) {
                yz0.a(a2, r51.N0, yz0.a(yz0.a(new JSONObject(), r51.p, this.v.c.a), r51.e2, this.v.c.b));
            }
            wr1 wr1 = new wr1(yz0.a(this.y), ci1.c, this.w.u, yz0.a(this.y), this.z, this.v.b == is0.SUCCESS, null, null, null, a2, 448);
            if (this.A) {
                wl0.h.a(wr1);
            }
            if (this instanceof b31) {
                xn1.h.a(wr1);
            } else if ((this instanceof wo1) || (this instanceof xm1)) {
                yl1.h.a(wr1);
            }
            eu0 eu0 = this.v;
            if (eu0.b == is0.SUCCESS) {
                le0 le0 = le0.DEBUG;
                k60.a(eu0, 0, 1, null);
                k().toString(2);
                Iterator<T> it = this.e.iterator();
                while (it.hasNext()) {
                    it.next().invoke(this);
                }
            } else {
                le0 le02 = le0.ERROR;
                k60.a(eu0, 0, 1, null);
                Iterator<T> it2 = this.f.iterator();
                while (it2.hasNext()) {
                    it2.next().invoke(this);
                }
            }
            Iterator<T> it3 = this.g.iterator();
            while (it3.hasNext()) {
                it3.next().invoke(this);
            }
        }
    }

    @DexIgnore
    public final void a(float f2) {
        Iterator<T> it = this.h.iterator();
        while (it.hasNext()) {
            it.next().invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void a(dd1 dd1) {
        if (yv0.a[dd1.ordinal()] == 1) {
            a(is0.CONNECTION_DROPPED);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(zk0 zk0, qa1 qa1, qa1 qa12, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                qa12 = qa1;
            }
            zk0.a(qa1, qa12);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryExecuteRequest");
    }

    @DexIgnore
    public final void a(qa1 qa1, qa1 qa12) {
        a(qa1, new el1(this, qa12));
    }

    @DexIgnore
    public final void a(qa1 qa1, vc7<i97> vc7) {
        v81 a2 = a(qa1);
        if (a2 == null) {
            a(eu0.a(this.v, null, is0.FLOW_BROKEN, null, 5));
        } else if (this.m < a2.z) {
            a(this, a2, cn1.a, new ys1(this, vc7), (kd7) null, new eh0(this), bj0.a, 8, (Object) null);
        } else {
            a(this.v);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.zk0 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.fossil.kd7<com.fossil.zk0, java.lang.Float, com.fossil.i97>> */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void a(zk0 zk0, zk0 zk02, gd7 gd7, gd7 gd72, kd7 kd7, gd7 gd73, gd7 gd74, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 8) != 0) {
                kd7 = p41.a;
            }
            if ((i2 & 16) != 0) {
                gd73 = m61.a;
            }
            if ((i2 & 32) != 0) {
                gd74 = k81.a;
            }
            if (!zk0.t) {
                zk0.n = zk02;
                if (zk02 != 0) {
                    zk02.c(gd7);
                    zk02.b(new fa1(zk0, gd74, gd72));
                    if (!zk02.t) {
                        zk02.h.add(kd7);
                    }
                    zk02.a(gd73);
                    zk02.l();
                    return;
                }
                return;
            }
            zk0.a(zk0.v);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeSubPhase");
    }

    @DexIgnore
    public void a(is0 is0) {
        if (!this.t && !this.u) {
            le0 le0 = le0.DEBUG;
            yz0.a(is0);
            this.o.removeCallbacksAndMessages(null);
            this.u = true;
            this.v = eu0.a(this.v, null, is0, null, 5);
            v81 v81 = this.b;
            if (v81 == null || v81.t) {
                zk0 zk0 = this.n;
                if (zk0 == null || zk0.t) {
                    a(this.v);
                } else if (zk0 != null) {
                    zk0.a(is0);
                }
            } else if (v81 != null) {
                v81.a(ay0.s);
            }
        }
    }

    @DexIgnore
    public final zk0 a(gd7<? super zk0, i97> gd7) {
        if (!this.t) {
            this.g.add(gd7);
        } else {
            gd7.invoke(this);
        }
        return this;
    }
}
