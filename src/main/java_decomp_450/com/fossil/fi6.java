package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.vx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fi6 extends go5 implements ei6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public qw6<s55> f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return fi6.h;
        }

        @DexIgnore
        public final fi6 b() {
            return new fi6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fi6 a;

        @DexIgnore
        public b(fi6 fi6) {
            this.a = fi6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = vx6.a(vx6.c.FEATURES, vx6.b.SHOP_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = fi6.i.a();
            local.d(a3, "Purchase Battery URL = " + a2);
            fi6 fi6 = this.a;
            ee7.a((Object) a2, "url");
            fi6.Y(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fi6 a;

        @DexIgnore
        public c(fi6 fi6) {
            this.a = fi6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = vx6.a(vx6.c.FEATURES, vx6.b.LOW_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = fi6.i.a();
            local.d(a3, "Need Help URL = " + a2);
            fi6 fi6 = this.a;
            ee7.a((Object) a2, "url");
            fi6.Y(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fi6 a;

        @DexIgnore
        public d(fi6 fi6) {
            this.a = fi6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    /*
    static {
        String simpleName = fi6.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "ReplaceBatteryFragment::class.java.simpleName!!");
            h = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final void Y(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), h);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        s55 s55 = (s55) qb.a(LayoutInflater.from(getContext()), 2131558617, null, false, a1());
        this.f = new qw6<>(this, s55);
        ee7.a((Object) s55, "binding");
        return s55.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<s55> qw6 = this.f;
        if (qw6 != null) {
            s55 a2 = qw6.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(di6 di6) {
        ee7.b(di6, "presenter");
        jw3.a(di6);
        ee7.a((Object) di6, "Preconditions.checkNotNull(presenter)");
    }
}
