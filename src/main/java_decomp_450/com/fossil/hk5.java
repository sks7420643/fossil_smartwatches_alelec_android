package com.fossil;

import android.view.KeyEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hk5 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public hk5(String str, String str2) {
        ee7.b(str, "appName");
        ee7.b(str2, "packageName");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public abstract boolean a(KeyEvent keyEvent);

    @DexIgnore
    public abstract ik5 b();

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof hk5)) {
            return false;
        }
        return ee7.a((Object) this.b, (Object) ((hk5) obj).b);
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }
}
