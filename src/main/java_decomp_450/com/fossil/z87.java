package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z87 implements Collection<y87>, ye7 {
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends va7 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ byte[] b;

        @DexIgnore
        public a(byte[] bArr) {
            ee7.b(bArr, "array");
            this.b = bArr;
        }

        @DexIgnore
        @Override // com.fossil.va7
        public byte a() {
            int i = this.a;
            byte[] bArr = this.b;
            if (i < bArr.length) {
                this.a = i + 1;
                byte b2 = bArr[i];
                y87.c(b2);
                return b2;
            }
            throw new NoSuchElementException(String.valueOf(this.a));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(byte[] bArr, Object obj) {
        return (obj instanceof z87) && ee7.a(bArr, ((z87) obj).b());
    }

    @DexIgnore
    public static int b(byte[] bArr) {
        if (bArr != null) {
            return Arrays.hashCode(bArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(byte[] bArr) {
        return bArr.length == 0;
    }

    @DexIgnore
    public static va7 d(byte[] bArr) {
        return new a(bArr);
    }

    @DexIgnore
    public static String e(byte[] bArr) {
        return "UByteArray(storage=" + Arrays.toString(bArr) + ")";
    }

    @DexIgnore
    public boolean a(byte b) {
        return a(this.a, b);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(y87 y87) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends y87> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ byte[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof y87) {
            return a(((y87) obj).a());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<y87>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.va7' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<y87> iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return yd7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public static boolean a(byte[] bArr, byte b) {
        return t97.b(bArr, b);
    }

    @DexIgnore
    public static boolean a(byte[] bArr, Collection<y87> collection) {
        boolean z;
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof y87) || !t97.b(bArr, t.a())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }
}
