package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.a12;
import com.fossil.h62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gn3 extends n62<ln3> implements xn3 {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ j62 F;
    @DexIgnore
    public /* final */ Bundle G;
    @DexIgnore
    public Integer H;

    @DexIgnore
    public gn3(Context context, Looper looper, boolean z, j62 j62, Bundle bundle, a12.b bVar, a12.c cVar) {
        super(context, looper, 44, j62, bVar, cVar);
        this.E = true;
        this.F = j62;
        this.G = bundle;
        this.H = j62.e();
    }

    @DexIgnore
    @Override // com.fossil.xn3
    public final void a(s62 s62, boolean z) {
        try {
            ((ln3) A()).a(s62, this.H.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    @DexIgnore
    @Override // com.fossil.xn3
    public final void b() {
        a(new h62.d());
    }

    @DexIgnore
    @Override // com.fossil.xn3
    public final void h() {
        try {
            ((ln3) A()).b(this.H.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    @DexIgnore
    @Override // com.fossil.h62
    public String i() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public int k() {
        return q02.a;
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public boolean n() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public String p() {
        return "com.google.android.gms.signin.service.START";
    }

    @DexIgnore
    @Override // com.fossil.h62
    public Bundle x() {
        if (!w().getPackageName().equals(this.F.h())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.h());
        }
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.xn3
    public final void a(jn3 jn3) {
        a72.a(jn3, "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.F.c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = xy1.a(w()).b();
            }
            ((ln3) A()).a(new rn3(new b72(c, this.H.intValue(), googleSignInAccount)), jn3);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                jn3.a(new tn3(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @DexIgnore
    public gn3(Context context, Looper looper, boolean z, j62 j62, fn3 fn3, a12.b bVar, a12.c cVar) {
        this(context, looper, true, j62, a(j62), bVar, cVar);
    }

    @DexIgnore
    public static Bundle a(j62 j62) {
        fn3 j = j62.j();
        Integer e = j62.e();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", j62.a());
        if (e != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", e.intValue());
        }
        if (j != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", j.j());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", j.i());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", j.g());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", j.h());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", j.d());
            bundle.putString("com.google.android.gms.signin.internal.logSessionId", j.e());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", j.k());
            if (j.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", j.a().longValue());
            }
            if (j.f() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", j.f().longValue());
            }
        }
        return bundle;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof ln3) {
            return (ln3) queryLocalInterface;
        }
        return new on3(iBinder);
    }
}
