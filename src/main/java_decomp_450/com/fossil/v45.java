package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v45 extends u45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        E.put(2131362927, 2);
        E.put(2131362517, 3);
        E.put(2131362662, 4);
        E.put(2131362377, 5);
        E.put(2131362928, 6);
        E.put(2131361922, 7);
        E.put(2131362956, 8);
        E.put(2131362527, 9);
        E.put(2131362266, 10);
        E.put(2131362255, 11);
    }
    */

    @DexIgnore
    public v45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public v45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (Barrier) objArr[7], (FlexibleButton) objArr[11], (FlexibleButton) objArr[10], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[9], (ImageView) objArr[1], (ImageView) objArr[4], (View) objArr[6], (DashBar) objArr[2], (RelativeLayout) objArr[8], (ConstraintLayout) objArr[0]);
        this.C = -1;
        ((u45) this).B.setTag(null);
        a(view);
        f();
    }
}
