package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk7 {
    @DexIgnore
    public static final ki7 a(ik7 ik7) {
        return nk7.a(ik7);
    }

    @DexIgnore
    public static final void b(ik7 ik7) {
        nk7.b(ik7);
    }

    @DexIgnore
    public static final void a(ib7 ib7, CancellationException cancellationException) {
        nk7.a(ib7, cancellationException);
    }

    @DexIgnore
    public static final rj7 a(ik7 ik7, rj7 rj7) {
        return nk7.a(ik7, rj7);
    }

    @DexIgnore
    public static final void a(ib7 ib7) {
        nk7.a(ib7);
    }
}
