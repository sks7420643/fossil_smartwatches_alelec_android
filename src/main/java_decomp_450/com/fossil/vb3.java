package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb3 implements Parcelable.Creator<tb3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tb3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 2) {
                j72.v(parcel, a);
            } else {
                bundle = j72.a(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new tb3(bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tb3[] newArray(int i) {
        return new tb3[i];
    }
}
