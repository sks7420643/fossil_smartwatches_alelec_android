package com.fossil;

import com.fossil.zp3;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ DataHolder a;
    @DexIgnore
    public /* final */ /* synthetic */ zp3.d b;

    @DexIgnore
    public zq3(zp3.d dVar, DataHolder dataHolder) {
        this.b = dVar;
        this.a = dataHolder;
    }

    @DexIgnore
    public final void run() {
        up3 up3 = new up3(this.a);
        try {
            zp3.this.a(up3);
        } finally {
            up3.release();
        }
    }
}
