package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ge4 extends je4 {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;

    @DexIgnore
    public ge4(String str) {
        super(str);
    }

    @DexIgnore
    public ge4(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public ge4(Throwable th) {
        super(th);
    }
}
