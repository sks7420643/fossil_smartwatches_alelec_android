package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.px6;
import com.fossil.rj5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.debug.DebugActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sj5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public WeakReference<Context> a;
    @DexIgnore
    public rj5 b;
    @DexIgnore
    public ms3 c;
    @DexIgnore
    public ms3 d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public int g; // = -1;
    @DexIgnore
    public /* final */ UserRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements rj5.a {
        @DexIgnore
        public /* final */ /* synthetic */ sj5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 1;
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        this.a.a.d();
                        ms3 a2 = this.a.a.c;
                        if (a2 != null) {
                            a2.dismiss();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj5$b$b")
        /* renamed from: com.fossil.sj5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0167b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj5$b$b$a")
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1", f = "ShakeFeedbackService.kt", l = {126, 127}, m = "invokeSuspend")
            /* renamed from: com.fossil.sj5$b$b$a */
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ View$OnClickListenerC0167b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj5$b$b$a$a")
                @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.sj5$b$b$a$a  reason: collision with other inner class name */
                public static final class C0168a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0168a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0168a aVar = new C0168a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0168a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            ms3 a = this.this$0.this$0.a.a.c;
                            if (a != null) {
                                a.dismiss();
                                return i97.a;
                            }
                            ee7.a();
                            throw null;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(View$OnClickListenerC0167b bVar, Bitmap bitmap, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$bitmap, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        sj5 sj5 = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (sj5.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                        return i97.a;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    tk7 c = qj7.c();
                    C0168a aVar = new C0168a(this, null);
                    this.L$0 = yi7;
                    this.label = 2;
                    if (vh7.a(c, aVar, this) == a) {
                        return a;
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public View$OnClickListenerC0167b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                ee7.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type android.app.Activity");
                        }
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1", f = "ShakeFeedbackService.kt", l = {142, 143}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj5$b$c$a$a")
                @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.sj5$b$c$a$a  reason: collision with other inner class name */
                public static final class C0169a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0169a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0169a aVar = new C0169a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0169a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            ms3 a = this.this$0.this$0.a.a.c;
                            if (a != null) {
                                a.dismiss();
                                return i97.a;
                            }
                            ee7.a();
                            throw null;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, Bitmap bitmap, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = cVar;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$bitmap, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        sj5 sj5 = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (sj5.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                        return i97.a;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    tk7 c = qj7.c();
                    C0169a aVar = new C0169a(this, null);
                    this.L$0 = yi7;
                    this.label = 2;
                    if (vh7.a(c, aVar, this) == a) {
                        return a;
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 0;
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                ee7.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type android.app.Activity");
                        }
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4$1", f = "ShakeFeedbackService.kt", l = {160}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ d this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(d dVar, Bitmap bitmap, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = dVar;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$bitmap, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        sj5 sj5 = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (sj5.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public d(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 1;
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                ee7.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                if (rootView.getDrawingCache() != null) {
                                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                    ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                    rootView.setDrawingCacheEnabled(false);
                                    ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                                }
                                ms3 a2 = this.a.a.c;
                                if (a2 != null) {
                                    a2.dismiss();
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                throw new x87("null cannot be cast to non-null type android.app.Activity");
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class e implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5$1", f = "ShakeFeedbackService.kt", l = {180}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ e this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(e eVar, Bitmap bitmap, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = eVar;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$bitmap, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        sj5 sj5 = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (sj5.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public e(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 3;
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                ee7.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                if (rootView.getDrawingCache() != null) {
                                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                    ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                    rootView.setDrawingCacheEnabled(false);
                                    ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                                }
                                ms3 a2 = this.a.a.c;
                                if (a2 != null) {
                                    a2.dismiss();
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                throw new x87("null cannot be cast to non-null type android.app.Activity");
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class f implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6$1", f = "ShakeFeedbackService.kt", l = {200}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ f this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(f fVar, Bitmap bitmap, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = fVar;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$bitmap, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        sj5 sj5 = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (sj5.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public f(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 2;
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                ee7.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                if (rootView.getDrawingCache() != null) {
                                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                    ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                    rootView.setDrawingCacheEnabled(false);
                                    ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                                }
                                ms3 a2 = this.a.a.c;
                                if (a2 != null) {
                                    a2.dismiss();
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                throw new x87("null cannot be cast to non-null type android.app.Activity");
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class g implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$7$1", f = "ShakeFeedbackService.kt", l = {213}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ g this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(g gVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = gVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        sj5 sj5 = this.this$0.a.a;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (sj5.a(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public g(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 1;
                px6.a aVar = px6.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    } else if (aVar.c((Activity) obj, 123)) {
                        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, null), 3, null);
                        ms3 a2 = this.a.a.c;
                        if (a2 != null) {
                            a2.dismiss();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class h implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public h(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                DebugActivity.a aVar = DebugActivity.M;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj != null) {
                        ee7.a(obj, "contextWeakReference!!.get()!!");
                        aVar.a((Context) obj);
                        ms3 a2 = this.a.a.c;
                        if (a2 != null) {
                            a2.dismiss();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public b(sj5 sj5) {
            this.a = sj5;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0149  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x014d  */
        @Override // com.fossil.rj5.a
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
                r5 = this;
                com.fossil.sj5 r0 = r5.a
                java.lang.ref.WeakReference r0 = r0.a
                r1 = 0
                if (r0 == 0) goto L_0x017a
                java.lang.Object r0 = r0.get()
                android.content.Context r0 = (android.content.Context) r0
                boolean r0 = com.fossil.de5.a(r0)
                if (r0 == 0) goto L_0x0179
                com.fossil.sj5 r0 = r5.a
                boolean r0 = r0.c()
                if (r0 != 0) goto L_0x0179
                com.fossil.sj5 r0 = r5.a
                com.fossil.ms3 r0 = r0.c
                if (r0 == 0) goto L_0x0035
                com.fossil.sj5 r0 = r5.a
                com.fossil.ms3 r0 = r0.c
                if (r0 == 0) goto L_0x0031
                r0.dismiss()
                goto L_0x0035
            L_0x0031:
                com.fossil.ee7.a()
                throw r1
            L_0x0035:
                com.fossil.sj5 r0 = r5.a
                java.lang.ref.WeakReference r0 = r0.a
                if (r0 == 0) goto L_0x0175
                java.lang.Object r0 = r0.get()
                if (r0 == 0) goto L_0x0171
                android.content.Context r0 = (android.content.Context) r0
                java.lang.String r2 = "layout_inflater"
                java.lang.Object r0 = r0.getSystemService(r2)
                if (r0 == 0) goto L_0x0169
                android.view.LayoutInflater r0 = (android.view.LayoutInflater) r0
                r2 = 2131558461(0x7f0d003d, float:1.8742238E38)
                android.view.View r0 = r0.inflate(r2, r1)
                com.fossil.sj5 r2 = r5.a
                com.fossil.ms3 r3 = new com.fossil.ms3
                com.fossil.sj5 r4 = r5.a
                java.lang.ref.WeakReference r4 = r4.a
                if (r4 == 0) goto L_0x0165
                java.lang.Object r4 = r4.get()
                if (r4 == 0) goto L_0x0161
                android.content.Context r4 = (android.content.Context) r4
                r3.<init>(r4)
                r2.c = r3
                com.fossil.sj5 r2 = r5.a
                com.fossil.ms3 r2 = r2.c
                if (r2 == 0) goto L_0x015d
                r2.setContentView(r0)
                r2 = 2131363351(0x7f0a0617, float:1.8346508E38)
                android.view.View r2 = r0.findViewById(r2)
                if (r2 == 0) goto L_0x0155
                android.widget.TextView r2 = (android.widget.TextView) r2
                java.lang.String r3 = "4.5.0"
                r2.setText(r3)
                r2 = 2131363054(0x7f0a04ee, float:1.8345906E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$a r3 = new com.fossil.sj5$b$a
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131363053(0x7f0a04ed, float:1.8345904E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$b r3 = new com.fossil.sj5$b$b
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131363050(0x7f0a04ea, float:1.8345898E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$c r3 = new com.fossil.sj5$b$c
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131363052(0x7f0a04ec, float:1.8345902E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$d r3 = new com.fossil.sj5$b$d
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131363049(0x7f0a04e9, float:1.8345896E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$e r3 = new com.fossil.sj5$b$e
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131363055(0x7f0a04ef, float:1.8345908E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$f r3 = new com.fossil.sj5$b$f
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131363051(0x7f0a04eb, float:1.83459E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5$b$g r3 = new com.fossil.sj5$b$g
                r3.<init>(r5)
                r2.setOnClickListener(r3)
                r2 = 2131362783(0x7f0a03df, float:1.8345356E38)
                android.view.View r2 = r0.findViewById(r2)
                com.fossil.sj5 r3 = r5.a
                java.lang.ref.WeakReference r3 = r3.a
                if (r3 == 0) goto L_0x0151
                java.lang.Object r3 = r3.get()
                boolean r3 = r3 instanceof com.portfolio.platform.ui.debug.DebugActivity
                java.lang.String r4 = "llOpenDebug"
                if (r3 != 0) goto L_0x012a
                com.fossil.sj5 r3 = r5.a
                java.lang.ref.WeakReference r3 = r3.a
                if (r3 == 0) goto L_0x0126
                java.lang.Object r3 = r3.get()
                boolean r3 = r3 instanceof com.portfolio.platform.ui.debug.LogcatActivity
                if (r3 == 0) goto L_0x011e
                goto L_0x012a
            L_0x011e:
                com.fossil.ee7.a(r2, r4)
                r3 = 0
                r2.setVisibility(r3)
                goto L_0x0132
            L_0x0126:
                com.fossil.ee7.a()
                throw r1
            L_0x012a:
                com.fossil.ee7.a(r2, r4)
                r3 = 8
                r2.setVisibility(r3)
            L_0x0132:
                r2 = 2131362871(0x7f0a0437, float:1.8345535E38)
                android.view.View r0 = r0.findViewById(r2)
                com.fossil.sj5$b$h r2 = new com.fossil.sj5$b$h
                r2.<init>(r5)
                r0.setOnClickListener(r2)
                com.fossil.sj5 r0 = r5.a
                com.fossil.ms3 r0 = r0.c
                if (r0 == 0) goto L_0x014d
                r0.show()
                goto L_0x0179
            L_0x014d:
                com.fossil.ee7.a()
                throw r1
            L_0x0151:
                com.fossil.ee7.a()
                throw r1
            L_0x0155:
                com.fossil.x87 r0 = new com.fossil.x87
                java.lang.String r1 = "null cannot be cast to non-null type android.widget.TextView"
                r0.<init>(r1)
                throw r0
            L_0x015d:
                com.fossil.ee7.a()
                throw r1
            L_0x0161:
                com.fossil.ee7.a()
                throw r1
            L_0x0165:
                com.fossil.ee7.a()
                throw r1
            L_0x0169:
                com.fossil.x87 r0 = new com.fossil.x87
                java.lang.String r1 = "null cannot be cast to non-null type android.view.LayoutInflater"
                r0.<init>(r1)
                throw r0
            L_0x0171:
                com.fossil.ee7.a()
                throw r1
            L_0x0175:
                com.fossil.ee7.a()
                throw r1
            L_0x0179:
                return
            L_0x017a:
                com.fossil.ee7.a()
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sj5.b.a():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {481}, m = "sendFeedbackEmail")
    public static final class c extends rb7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sj5 sj5, fb7 fb7) {
            super(fb7);
            this.this$0 = sj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;

        @DexIgnore
        public d(Context context) {
            this.a = context;
        }

        @DexIgnore
        public final void run() {
            Toast.makeText(this.a, "Can't zip file!", 1).show();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {FacebookRequestErrorClassification.EC_TOO_MANY_USER_ACTION_CALLS}, m = "sendHardwareLog")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(sj5 sj5, fb7 fb7) {
            super(fb7);
            this.this$0 = sj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$sendLog$2", f = "ShakeFeedbackService.kt", l = {83}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(sj5 sj5, Bitmap bitmap, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sj5;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$bitmap, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                sj5 sj5 = this.this$0;
                Bitmap bitmap = this.$bitmap;
                this.L$0 = yi7;
                this.label = 1;
                if (sj5.a(bitmap, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sj5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$1$1", f = "ShakeFeedbackService.kt", l = {267, 268}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj5$g$a$a")
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$1$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.sj5$g$a$a  reason: collision with other inner class name */
            public static final class C0170a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0170a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0170a aVar = new C0170a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0170a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        ms3 c = this.this$0.this$0.a.d;
                        if (c != null) {
                            c.dismiss();
                            return i97.a;
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, Bitmap bitmap, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$bitmap, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    sj5 sj5 = this.this$0.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (sj5.a(bitmap, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                tk7 c = qj7.c();
                C0170a aVar = new C0170a(this, null);
                this.L$0 = yi7;
                this.label = 2;
                if (vh7.a(c, aVar, this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public g(sj5 sj5) {
            this.a = sj5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.g = 0;
            px6.a aVar = px6.a;
            WeakReference b = this.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new x87("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            ee7.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                            return;
                        }
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    }
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sj5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1", f = "ShakeFeedbackService.kt", l = {284, 285}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sj5$h$a$a")
            @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.sj5$h$a$a  reason: collision with other inner class name */
            public static final class C0171a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0171a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0171a aVar = new C0171a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0171a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        ms3 c = this.this$0.this$0.a.d;
                        if (c != null) {
                            c.dismiss();
                            return i97.a;
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Bitmap bitmap, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$bitmap, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    sj5 sj5 = this.this$0.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (sj5.a(bitmap, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                tk7 c = qj7.c();
                C0171a aVar = new C0171a(this, null);
                this.L$0 = yi7;
                this.label = 2;
                if (vh7.a(c, aVar, this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public h(sj5 sj5) {
            this.a = sj5;
        }

        @DexIgnore
        public final void onClick(View view) {
            px6.a aVar = px6.a;
            WeakReference b = this.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new x87("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            ee7.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            ee7.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            ee7.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, createBitmap, null), 3, null);
                            return;
                        }
                        throw new x87("null cannot be cast to non-null type android.app.Activity");
                    }
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {590, Action.Bolt.TURN_OFF}, m = "takeScreenShot")
    public static final class i extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(sj5 sj5, fb7 fb7) {
            super(fb7);
            this.this$0 = sj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Bitmap) null, this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = sj5.class.getSimpleName();
        ee7.a((Object) simpleName, "ShakeFeedbackService::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public sj5(UserRepository userRepository) {
        String str;
        ee7.b(userRepository, "mUserRepository");
        this.h = userRepository;
        if (b()) {
            str = Environment.getExternalStorageDirectory().toString();
        } else {
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            str = applicationContext.getFilesDir().toString();
        }
        this.e = str;
        this.e = ee7.a(str, (Object) "/com.fossil.wearables.fossil/");
    }

    @DexIgnore
    public final boolean c() {
        ms3 ms3;
        ms3 ms32 = this.c;
        if (ms32 != null) {
            if (ms32 == null) {
                ee7.a();
                throw null;
            } else if (ms32.isShowing() && (ms3 = this.d) != null) {
                if (ms3 == null) {
                    ee7.a();
                    throw null;
                } else if (ms3.isShowing()) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference == null) {
            ee7.a();
            throw null;
        } else if (de5.a(weakReference.get()) && !c()) {
            ms3 ms3 = this.d;
            if (ms3 != null) {
                if (ms3 != null) {
                    ms3.dismiss();
                } else {
                    ee7.a();
                    throw null;
                }
            }
            WeakReference<Context> weakReference2 = this.a;
            if (weakReference2 != null) {
                Context context = weakReference2.get();
                if (context != null) {
                    Object systemService = context.getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558462, (ViewGroup) null);
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Context context2 = weakReference3.get();
                            if (context2 != null) {
                                ms3 ms32 = new ms3(context2);
                                this.d = ms32;
                                ms32.setContentView(inflate);
                                View findViewById = inflate.findViewById(2131363351);
                                if (findViewById != null) {
                                    ((TextView) findViewById).setText("4.5.0");
                                    inflate.findViewById(2131361942).setOnClickListener(new g(this));
                                    inflate.findViewById(2131361944).setOnClickListener(new h(this));
                                    ms3 ms33 = this.d;
                                    if (ms33 != null) {
                                        ms33.show();
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                } else {
                                    throw new x87("null cannot be cast to non-null type android.widget.TextView");
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final synchronized void e() {
        rj5 rj5 = this.b;
        if (rj5 != null) {
            rj5.a();
        }
        if (this.c != null) {
            ms3 ms3 = this.c;
            if (ms3 != null) {
                ms3.dismiss();
                this.c = null;
            } else {
                ee7.a();
                throw null;
            }
        }
        if (this.d != null) {
            ms3 ms32 = this.d;
            if (ms32 != null) {
                ms32.dismiss();
                this.d = null;
            } else {
                ee7.a();
                throw null;
            }
        }
        this.a = null;
    }

    @DexIgnore
    public final boolean b() {
        return ee7.a((Object) "mounted", (Object) Environment.getExternalStorageState());
    }

    @DexIgnore
    public final Object a(Context context, fb7<? super i97> fb7) {
        if (context != null) {
            Window window = ((Activity) context).getWindow();
            ee7.a((Object) window, "(context as Activity).window");
            View decorView = window.getDecorView();
            ee7.a((Object) decorView, "(context as Activity).window.decorView");
            View rootView = decorView.getRootView();
            ee7.a((Object) rootView, "v1");
            rootView.setDrawingCacheEnabled(true);
            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
            ee7.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
            Object a2 = vh7.a(qj7.b(), new f(this, createBitmap, null), fb7);
            if (a2 == nb7.a()) {
                return a2;
            }
            return i97.a;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    public final synchronized void a(Context context) {
        ee7.b(context, Constants.ACTIVITY);
        this.a = new WeakReference<>(context);
        Object systemService = context.getSystemService("sensor");
        if (systemService != null) {
            SensorManager sensorManager = (SensorManager) systemService;
            rj5 rj5 = new rj5(new b(this));
            this.b = rj5;
            if (rj5 != null) {
                rj5.a(sensorManager);
            }
        } else {
            throw new x87("null cannot be cast to non-null type android.hardware.SensorManager");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r14 = this;
            boolean r0 = r15 instanceof com.fossil.sj5.e
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.sj5$e r0 = (com.fossil.sj5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.sj5$e r0 = new com.fossil.sj5$e
            r0.<init>(r14, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x005a
            if (r2 != r3) goto L_0x0052
            java.lang.Object r1 = r0.L$8
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r1 = r0.L$7
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r1 = r0.L$6
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r0.L$5
            java.io.File r1 = (java.io.File) r1
            java.lang.Object r1 = r0.L$4
            java.io.File r1 = (java.io.File) r1
            java.lang.Object r1 = r0.L$3
            java.io.FileOutputStream r1 = (java.io.FileOutputStream) r1
            java.lang.Object r1 = r0.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r0.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.sj5 r0 = (com.fossil.sj5) r0
            com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x004f }
            goto L_0x0146
        L_0x004f:
            r15 = move-exception
            goto L_0x0170
        L_0x0052:
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r15.<init>(r0)
            throw r15
        L_0x005a:
            com.fossil.t87.a(r15)
            java.lang.ref.WeakReference<android.content.Context> r15 = r14.a
            if (r15 == 0) goto L_0x0196
            java.lang.Object r15 = r15.get()
            android.content.Context r15 = (android.content.Context) r15
            com.misfit.frameworks.buttonservice.db.HwLogProvider r15 = com.misfit.frameworks.buttonservice.db.HwLogProvider.getInstance(r15)
            java.lang.String r2 = "HwLogProvider.getInstanc\u2026extWeakReference!!.get())"
            com.fossil.ee7.a(r15, r2)
            java.util.List r15 = r15.getAllHardwareLogs()
            com.google.gson.Gson r2 = new com.google.gson.Gson
            r2.<init>()
            java.lang.String r2 = r2.a(r15)
            java.io.File r5 = new java.io.File
            java.lang.String r6 = r14.e
            if (r6 == 0) goto L_0x0192
            r5.<init>(r6)
            r5.mkdir()
            java.lang.String r6 = r14.e
            com.fossil.we7 r7 = com.fossil.we7.a
            java.lang.String r7 = "hwlog_%s.txt"
            java.lang.Object[] r8 = new java.lang.Object[r3]
            r9 = 0
            long r10 = java.lang.System.currentTimeMillis()
            java.lang.Long r10 = com.fossil.pb7.a(r10)
            r8[r9] = r10
            java.lang.Object[] r8 = java.util.Arrays.copyOf(r8, r3)
            java.lang.String r7 = java.lang.String.format(r7, r8)
            java.lang.String r8 = "java.lang.String.format(format, *args)"
            com.fossil.ee7.a(r7, r8)
            java.io.File r8 = new java.io.File
            r8.<init>(r6, r7)
            boolean r6 = r8.exists()
            if (r6 == 0) goto L_0x00b7
            r8.delete()
        L_0x00b7:
            r8.createNewFile()
            java.io.FileOutputStream r6 = new java.io.FileOutputStream
            r6.<init>(r8)
            java.lang.String r7 = "jsonLog"
            com.fossil.ee7.a(r2, r7)
            java.nio.charset.Charset r7 = com.fossil.sg7.a
            if (r2 == 0) goto L_0x0168
            byte[] r7 = r2.getBytes(r7)
            java.lang.String r9 = "(this as java.lang.String).getBytes(charset)"
            com.fossil.ee7.a(r7, r9)
            r6.write(r7)
            r6.flush()
            r6.close()
            com.fossil.yd5 r7 = com.fossil.yd5.a
            java.lang.ref.WeakReference<android.content.Context> r9 = r14.a
            if (r9 == 0) goto L_0x0164
            java.lang.Object r9 = r9.get()
            if (r9 == 0) goto L_0x0160
            java.lang.String r10 = "contextWeakReference!!.get()!!"
            com.fossil.ee7.a(r9, r10)
            android.content.Context r9 = (android.content.Context) r9
            java.lang.String r10 = r14.e
            if (r10 == 0) goto L_0x00f2
            goto L_0x00f4
        L_0x00f2:
            java.lang.String r10 = ""
        L_0x00f4:
            java.lang.String r7 = r7.a(r9, r10)
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            if (r7 == 0) goto L_0x015c
            r9.add(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.fossil.sj5.i
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "HwLogFile="
            r12.append(r13)
            java.lang.String r13 = r8.getAbsolutePath()
            r12.append(r13)
            java.lang.String r12 = r12.toString()
            r10.d(r11, r12)
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            r10.add(r8)
            r0.L$0 = r14
            r0.L$1 = r15
            r0.L$2 = r2
            r0.L$3 = r6
            r0.L$4 = r5
            r0.L$5 = r8
            r0.L$6 = r7
            r0.L$7 = r9
            r0.L$8 = r10
            r0.label = r3
            java.lang.Object r15 = r14.a(r4, r9, r10, r0)
            if (r15 != r1) goto L_0x0145
            return r1
        L_0x0145:
            r0 = r14
        L_0x0146:
            java.lang.ref.WeakReference<android.content.Context> r15 = r0.a
            if (r15 == 0) goto L_0x0158
            java.lang.Object r15 = r15.get()
            android.content.Context r15 = (android.content.Context) r15
            com.misfit.frameworks.buttonservice.db.HwLogProvider r15 = com.misfit.frameworks.buttonservice.db.HwLogProvider.getInstance(r15)
            r15.setHardwareLogRead()
            goto L_0x018f
        L_0x0158:
            com.fossil.ee7.a()
            throw r4
        L_0x015c:
            com.fossil.ee7.a()
            throw r4
        L_0x0160:
            com.fossil.ee7.a()
            throw r4
        L_0x0164:
            com.fossil.ee7.a()
            throw r4
        L_0x0168:
            com.fossil.x87 r15 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type java.lang.String"
            r15.<init>(r0)
            throw r15
        L_0x0170:
            r15.printStackTrace()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sj5.i
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ".sendHardwareLog - ex="
            r2.append(r3)
            r2.append(r15)
            java.lang.String r15 = r2.toString()
            r0.e(r1, r15)
        L_0x018f:
            com.fossil.i97 r15 = com.fossil.i97.a
            return r15
        L_0x0192:
            com.fossil.ee7.a()
            throw r4
        L_0x0196:
            com.fossil.ee7.a()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sj5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0447 A[Catch:{ Exception -> 0x04bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x044a A[Catch:{ Exception -> 0x04bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x045e A[Catch:{ Exception -> 0x04bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x04b8  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x04e0  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r26, java.util.List<java.lang.String> r27, java.util.List<? extends java.io.File> r28, com.fossil.fb7<? super com.fossil.i97> r29) {
        /*
            r25 = this;
            r1 = r25
            r0 = r26
            r2 = r29
            boolean r3 = r2 instanceof com.fossil.sj5.c
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.sj5$c r3 = (com.fossil.sj5.c) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.sj5$c r3 = new com.fossil.sj5$c
            r3.<init>(r1, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 1
            if (r5 == 0) goto L_0x0075
            if (r5 != r6) goto L_0x006d
            java.lang.Object r0 = r3.L$13
            java.lang.StringBuilder r0 = (java.lang.StringBuilder) r0
            java.lang.Object r4 = r3.L$12
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$11
            android.content.Intent r5 = (android.content.Intent) r5
            java.lang.Object r6 = r3.L$10
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r3.L$9
            java.io.File r6 = (java.io.File) r6
            java.lang.Object r6 = r3.L$8
            java.io.File r6 = (java.io.File) r6
            java.lang.Object r6 = r3.L$7
            android.net.Uri r6 = (android.net.Uri) r6
            long r8 = r3.J$0
            java.lang.Object r6 = r3.L$6
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            java.lang.Object r6 = r3.L$5
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            java.lang.Object r6 = r3.L$4
            android.content.Intent r6 = (android.content.Intent) r6
            java.lang.Object r8 = r3.L$3
            java.util.List r8 = (java.util.List) r8
            java.lang.Object r8 = r3.L$2
            java.util.List r8 = (java.util.List) r8
            java.lang.Object r8 = r3.L$1
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r3 = r3.L$0
            com.fossil.sj5 r3 = (com.fossil.sj5) r3
            com.fossil.t87.a(r2)     // Catch:{ Exception -> 0x006a }
            goto L_0x04a6
        L_0x006a:
            r0 = move-exception
            goto L_0x04c0
        L_0x006d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0075:
            com.fossil.t87.a(r2)
            android.content.Intent r5 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.SEND_MULTIPLE"
            r5.<init>(r2)
            java.lang.String r2 = "vnd.android.cursor.dir/email"
            r5.setType(r2)
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            boolean r2 = r2.e()
            r8 = 2
            r9 = 3
            r10 = -1
            java.lang.String r11 = "android.intent.extra.EMAIL"
            if (r2 == 0) goto L_0x010f
            int r2 = r1.g
            if (r2 == r10) goto L_0x00af
            java.lang.String r12 = "fossiluat@fossil.com"
            java.lang.String r13 = "bugs@fossil.com"
            java.lang.String r14 = "sw-qa@fossil.com"
            java.lang.String r15 = "minh@fossil.com"
            java.lang.String r16 = "sw-q-android@fossil.com"
            java.lang.String r17 = "sw-diana@fossil.com"
            java.lang.String r18 = "diana-bugs-sm@fossil.com"
            java.lang.String r19 = "diana-bugs-sm@fossil.com"
            java.lang.String r20 = "sw-sa@fossil.com"
            java.lang.String[] r2 = new java.lang.String[]{r12, r13, r14, r15, r16, r17, r18, r19, r20}
            r5.putExtra(r11, r2)
            goto L_0x0118
        L_0x00af:
            int r2 = r1.f
            if (r2 != r9) goto L_0x00bf
            java.lang.String r2 = "bugs@fossil.com"
            java.lang.String r12 = "buddy-challenge-sm@fossil.com"
            java.lang.String[] r2 = new java.lang.String[]{r2, r12}
            r5.putExtra(r11, r2)
            goto L_0x0118
        L_0x00bf:
            if (r2 != 0) goto L_0x00db
            java.lang.String r12 = "fossiluat@fossil.com"
            java.lang.String r13 = "bugs@fossil.com"
            java.lang.String r14 = "sw-qa@fossil.com"
            java.lang.String r15 = "minh@fossil.com"
            java.lang.String r16 = "sw-q-android@fossil.com"
            java.lang.String r17 = "sw-diana@fossil.com"
            java.lang.String r18 = "diana-bugs-sm@fossil.com"
            java.lang.String r19 = "diana-bugs-sm@fossil.com"
            java.lang.String r20 = "sw-sa@fossil.com"
            java.lang.String[] r2 = new java.lang.String[]{r12, r13, r14, r15, r16, r17, r18, r19, r20}
            r5.putExtra(r11, r2)
            goto L_0x0118
        L_0x00db:
            if (r2 != r8) goto L_0x00f9
            java.lang.String r12 = "bugs@fossil.com"
            java.lang.String r13 = "sw-qa@fossil.com"
            java.lang.String r14 = "minh@fossil.com"
            java.lang.String r15 = "dungdna@fossil.com"
            java.lang.String r16 = "sw-q-android@fossil.com"
            java.lang.String r17 = "sw-diana@fossil.com"
            java.lang.String r18 = "diana-bugs-sm@fossil.com"
            java.lang.String r19 = "diana-bugs-sm@fossil.com"
            java.lang.String r20 = "sw-sa@fossil.com"
            java.lang.String r21 = "fossiluat@fossil.com"
            java.lang.String[] r2 = new java.lang.String[]{r12, r13, r14, r15, r16, r17, r18, r19, r20, r21}
            r5.putExtra(r11, r2)
            goto L_0x0118
        L_0x00f9:
            java.lang.String r12 = "bugs@fossil.com"
            java.lang.String r13 = "sw-qa@fossil.com"
            java.lang.String r14 = "dungdna@fossil.com"
            java.lang.String r15 = "sw-portfolio-android@fossil.com"
            java.lang.String r16 = "sw-q-android@fossil.com"
            java.lang.String r17 = "diana-bugs-sm@fossil.com"
            java.lang.String r18 = "sw-sa@fossil.com"
            java.lang.String[] r2 = new java.lang.String[]{r12, r13, r14, r15, r16, r17, r18}
            r5.putExtra(r11, r2)
            goto L_0x0118
        L_0x010f:
            java.lang.String r2 = "help@misfit.com"
            java.lang.String[] r2 = new java.lang.String[]{r2}
            r5.putExtra(r11, r2)
        L_0x0118:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r12 = 0
            boolean r14 = r27.isEmpty()
            r14 = r14 ^ r6
            if (r14 == 0) goto L_0x018b
            com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
            boolean r14 = r14.e()
            if (r14 == 0) goto L_0x018b
            java.util.Iterator r14 = r27.iterator()
            r15 = 0
        L_0x0138:
            boolean r16 = r14.hasNext()
            if (r16 == 0) goto L_0x018c
            java.lang.Object r16 = r14.next()
            r8 = r16
            java.lang.String r8 = (java.lang.String) r8
            com.misfit.frameworks.buttonservice.log.FLogger r16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r16.getLocal()
            java.lang.String r6 = com.fossil.sj5.i
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r7 = ".sendFeedbackEmail - databaseFile="
            r10.append(r7)
            r10.append(r8)
            java.lang.String r7 = r10.toString()
            r9.e(r6, r7)
            if (r8 == 0) goto L_0x0186
            android.net.Uri r15 = r1.a(r8)
            if (r15 == 0) goto L_0x0186
            r2.add(r15)
            r11.add(r8)
            java.io.File r6 = new java.io.File
            java.lang.String r7 = r15.getPath()
            if (r7 == 0) goto L_0x0181
            r6.<init>(r7)
            long r6 = r6.length()
            long r12 = r12 + r6
            goto L_0x0186
        L_0x0181:
            com.fossil.ee7.a()
            r2 = 0
            throw r2
        L_0x0186:
            r6 = 1
            r8 = 2
            r9 = 3
            r10 = -1
            goto L_0x0138
        L_0x018b:
            r15 = 0
        L_0x018c:
            java.io.File r6 = new java.io.File
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            android.content.Context r8 = r8.getApplicationContext()
            java.lang.String r9 = "PortfolioApp.instance.applicationContext"
            com.fossil.ee7.a(r8, r9)
            java.io.File r8 = r8.getFilesDir()
            java.lang.String r10 = "PortfolioApp.instance.applicationContext.filesDir"
            com.fossil.ee7.a(r8, r10)
            java.lang.String r8 = r8.getAbsolutePath()
            r7.append(r8)
            java.lang.String r8 = java.io.File.separator
            r7.append(r8)
            com.fossil.zs1 r8 = com.fossil.zs1.b
            java.lang.String r8 = r8.d()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
            java.lang.String r7 = "NewSDKLog"
            java.io.File r7 = r1.a(r7, r6)
            if (r7 == 0) goto L_0x01ee
            java.lang.String r8 = r7.getAbsolutePath()
            java.lang.String r14 = "newSdkZipFile.absolutePath"
            com.fossil.ee7.a(r8, r14)
            android.net.Uri r8 = r1.a(r8)
            if (r8 == 0) goto L_0x01ee
            r2.add(r8)
            java.lang.String r8 = r7.getAbsolutePath()
            r11.add(r8)
            long r20 = r7.length()
            long r12 = r12 + r20
        L_0x01ee:
            com.fossil.zs1 r8 = com.fossil.zs1.b
            java.lang.String r8 = r8.c()
            boolean r14 = android.text.TextUtils.isEmpty(r8)
            if (r14 != 0) goto L_0x023c
            java.io.File r14 = new java.io.File
            r20 = r15
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            com.portfolio.platform.PortfolioApp$a r21 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r21 = r21.c()
            r22 = r4
            android.content.Context r4 = r21.getApplicationContext()
            com.fossil.ee7.a(r4, r9)
            java.io.File r4 = r4.getFilesDir()
            com.fossil.ee7.a(r4, r10)
            java.lang.String r4 = r4.getAbsolutePath()
            r15.append(r4)
            java.lang.String r4 = java.io.File.separator
            r15.append(r4)
            r15.append(r8)
            java.lang.String r4 = r15.toString()
            r14.<init>(r4)
            java.lang.String r4 = r14.getAbsolutePath()
            r11.add(r4)
            long r9 = r14.length()
            long r12 = r12 + r9
            goto L_0x0240
        L_0x023c:
            r22 = r4
            r20 = r15
        L_0x0240:
            boolean r4 = android.text.TextUtils.isEmpty(r26)
            if (r4 != 0) goto L_0x026f
            if (r0 == 0) goto L_0x026a
            android.net.Uri r15 = r25.a(r26)
            if (r15 == 0) goto L_0x0271
            r2.add(r15)
            r11.add(r0)
            java.io.File r4 = new java.io.File
            java.lang.String r9 = r15.getPath()
            if (r9 == 0) goto L_0x0265
            r4.<init>(r9)
            long r9 = r4.length()
            long r12 = r12 + r9
            goto L_0x0271
        L_0x0265:
            com.fossil.ee7.a()
            r4 = 0
            throw r4
        L_0x026a:
            r4 = 0
            com.fossil.ee7.a()
            throw r4
        L_0x026f:
            r15 = r20
        L_0x0271:
            java.util.Iterator r4 = r28.iterator()
        L_0x0275:
            boolean r9 = r4.hasNext()
            if (r9 == 0) goto L_0x02d8
            java.lang.Object r9 = r4.next()
            java.io.File r9 = (java.io.File) r9
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r14 = com.fossil.sj5.i
            r20 = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r21 = r8
            java.lang.String r8 = "sendFeedbackEmail - logFile="
            r4.append(r8)
            java.lang.String r8 = r9.getName()
            r4.append(r8)
            java.lang.String r8 = ", length="
            r4.append(r8)
            r23 = r7
            long r7 = r9.length()
            r4.append(r7)
            java.lang.String r4 = r4.toString()
            r10.d(r14, r4)
            java.lang.String r4 = r9.getAbsolutePath()
            java.lang.String r7 = "log.absolutePath"
            com.fossil.ee7.a(r4, r7)
            android.net.Uri r4 = r1.a(r4)
            if (r4 == 0) goto L_0x02d1
            long r7 = r9.length()
            long r12 = r12 + r7
            r2.add(r4)
            java.lang.String r4 = r9.getAbsolutePath()
            r11.add(r4)
        L_0x02d1:
            r4 = r20
            r8 = r21
            r7 = r23
            goto L_0x0275
        L_0x02d8:
            r23 = r7
            r21 = r8
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r7 = com.fossil.sj5.i
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = ".sendFeedbackEmail - dataFeedbackSize="
            r8.append(r9)
            r8.append(r12)
            java.lang.String r8 = r8.toString()
            r4.d(r7, r8)
            r7 = 1048576(0x100000, double:5.180654E-318)
            int r4 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r4 <= 0) goto L_0x034c
            java.io.File r4 = r1.a(r11)
            r2.clear()
            if (r4 != 0) goto L_0x033a
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.sj5.i
            java.lang.String r3 = "Can't zip file"
            r0.d(r2, r3)
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.a
            if (r0 == 0) goto L_0x0321
            java.lang.Object r0 = r0.get()
            r7 = r0
            android.content.Context r7 = (android.content.Context) r7
            goto L_0x0322
        L_0x0321:
            r7 = 0
        L_0x0322:
            if (r7 == 0) goto L_0x0332
            r0 = r7
            android.app.Activity r0 = (android.app.Activity) r0
            com.fossil.sj5$d r2 = new com.fossil.sj5$d
            r2.<init>(r7)
            r0.runOnUiThread(r2)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x0332:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r2 = "null cannot be cast to non-null type android.app.Activity"
            r0.<init>(r2)
            throw r0
        L_0x033a:
            java.lang.String r4 = r4.getAbsolutePath()
            java.lang.String r7 = "file.absolutePath"
            com.fossil.ee7.a(r4, r7)
            android.net.Uri r4 = r1.a(r4)
            if (r4 == 0) goto L_0x034c
            r2.add(r4)
        L_0x034c:
            java.lang.String r4 = "android.intent.extra.STREAM"
            r5.putParcelableArrayListExtra(r4, r2)
            int r4 = r1.g
            java.lang.String r7 = "["
            java.lang.String r8 = " "
            java.lang.String r9 = "android.intent.extra.SUBJECT"
            r10 = -1
            if (r4 == r10) goto L_0x036e
            if (r4 == 0) goto L_0x0368
            r10 = 1
            if (r4 == r10) goto L_0x0362
            goto L_0x0378
        L_0x0362:
            java.lang.String r4 = "HID CONNECTION"
            r5.putExtra(r9, r4)
            goto L_0x0378
        L_0x0368:
            java.lang.String r4 = "GATT CONNECTION"
            r5.putExtra(r9, r4)
            goto L_0x0378
        L_0x036e:
            int r4 = r1.f
            r10 = 3
            if (r4 != r10) goto L_0x037c
            java.lang.String r4 = "[BC]-[Android - Feedback]"
            r5.putExtra(r9, r4)
        L_0x0378:
            r24 = r15
            goto L_0x0425
        L_0x037c:
            java.lang.String r10 = "Staging] - "
            java.lang.String r14 = "] - "
            if (r4 != 0) goto L_0x03ba
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r17 = r10
            java.lang.String r10 = "[BETA - "
            r4.append(r10)
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r10 = r10.c()
            java.lang.String r10 = r10.h()
            r4.append(r10)
            r4.append(r8)
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
            boolean r10 = r10.e()
            if (r10 == 0) goto L_0x03a9
            r10 = r17
            goto L_0x03aa
        L_0x03a9:
            r10 = r14
        L_0x03aa:
            r4.append(r10)
            java.lang.String r10 = "[Android - Feedback]"
            r4.append(r10)
            java.lang.String r4 = r4.toString()
            r5.putExtra(r9, r4)
            goto L_0x0378
        L_0x03ba:
            r17 = r10
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r7)
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r10 = r10.c()
            java.lang.String r10 = r10.h()
            r4.append(r10)
            r4.append(r8)
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
            boolean r10 = r10.e()
            if (r10 == 0) goto L_0x03df
            r10 = r17
            goto L_0x03e0
        L_0x03df:
            r10 = r14
        L_0x03e0:
            r4.append(r10)
            java.lang.String r10 = "[Android Feedback] -"
            r4.append(r10)
            com.fossil.we7 r10 = com.fossil.we7.a
            r14 = 2
            java.lang.Object[] r10 = new java.lang.Object[r14]
            long r17 = java.lang.System.currentTimeMillis()
            r14 = 1000(0x3e8, float:1.401E-42)
            r24 = r15
            long r14 = (long) r14
            long r17 = r17 / r14
            java.lang.Long r14 = com.fossil.pb7.a(r17)
            r15 = 0
            r10[r15] = r14
            java.util.Date r14 = new java.util.Date
            r14.<init>()
            java.lang.String r14 = com.fossil.zd5.e(r14)
            r15 = 1
            r10[r15] = r14
            r14 = 2
            java.lang.Object[] r10 = java.util.Arrays.copyOf(r10, r14)
            java.lang.String r14 = " on %s (%s)"
            java.lang.String r10 = java.lang.String.format(r14, r10)
            java.lang.String r14 = "java.lang.String.format(format, *args)"
            com.fossil.ee7.a(r10, r14)
            r4.append(r10)
            java.lang.String r4 = r4.toString()
            r5.putExtra(r9, r4)
        L_0x0425:
            java.lang.String r4 = "android.intent.extra.TEXT"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04bd }
            r9.<init>()     // Catch:{ Exception -> 0x04bd }
            r9.append(r7)     // Catch:{ Exception -> 0x04bd }
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x04bd }
            com.portfolio.platform.PortfolioApp r7 = r7.c()     // Catch:{ Exception -> 0x04bd }
            java.lang.String r7 = r7.h()     // Catch:{ Exception -> 0x04bd }
            r9.append(r7)     // Catch:{ Exception -> 0x04bd }
            r9.append(r8)     // Catch:{ Exception -> 0x04bd }
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x04bd }
            boolean r7 = r7.e()     // Catch:{ Exception -> 0x04bd }
            if (r7 == 0) goto L_0x044a
            java.lang.String r7 = "Staging"
            goto L_0x044c
        L_0x044a:
            java.lang.String r7 = ""
        L_0x044c:
            r9.append(r7)     // Catch:{ Exception -> 0x04bd }
            java.lang.String r7 = "]\n"
            r9.append(r7)     // Catch:{ Exception -> 0x04bd }
            com.fossil.rd5$a r7 = com.fossil.rd5.g     // Catch:{ Exception -> 0x04bd }
            com.fossil.rd5 r7 = r7.b()     // Catch:{ Exception -> 0x04bd }
            java.lang.ref.WeakReference<android.content.Context> r8 = r1.a     // Catch:{ Exception -> 0x04bd }
            if (r8 == 0) goto L_0x04b8
            java.lang.Object r8 = r8.get()     // Catch:{ Exception -> 0x04bd }
            if (r8 == 0) goto L_0x04b3
            java.lang.String r10 = "contextWeakReference!!.get()!!"
            com.fossil.ee7.a(r8, r10)     // Catch:{ Exception -> 0x04bd }
            android.content.Context r8 = (android.content.Context) r8     // Catch:{ Exception -> 0x04bd }
            int r10 = r1.f     // Catch:{ Exception -> 0x04bd }
            int r14 = r1.g     // Catch:{ Exception -> 0x04bd }
            r3.L$0 = r1     // Catch:{ Exception -> 0x04bd }
            r3.L$1 = r0     // Catch:{ Exception -> 0x04bd }
            r0 = r27
            r3.L$2 = r0     // Catch:{ Exception -> 0x04bd }
            r0 = r28
            r3.L$3 = r0     // Catch:{ Exception -> 0x04bd }
            r3.L$4 = r5     // Catch:{ Exception -> 0x04bd }
            r3.L$5 = r2     // Catch:{ Exception -> 0x04bd }
            r3.L$6 = r11     // Catch:{ Exception -> 0x04bd }
            r3.J$0 = r12     // Catch:{ Exception -> 0x04bd }
            r15 = r24
            r3.L$7 = r15     // Catch:{ Exception -> 0x04bd }
            r3.L$8 = r6     // Catch:{ Exception -> 0x04bd }
            r0 = r23
            r3.L$9 = r0     // Catch:{ Exception -> 0x04bd }
            r0 = r21
            r3.L$10 = r0     // Catch:{ Exception -> 0x04bd }
            r3.L$11 = r5     // Catch:{ Exception -> 0x04bd }
            r3.L$12 = r4     // Catch:{ Exception -> 0x04bd }
            r3.L$13 = r9     // Catch:{ Exception -> 0x04bd }
            r0 = 1
            r3.label = r0     // Catch:{ Exception -> 0x04bd }
            java.lang.Object r2 = r7.a(r8, r10, r14, r3)     // Catch:{ Exception -> 0x04bd }
            r0 = r22
            if (r2 != r0) goto L_0x04a3
            return r0
        L_0x04a3:
            r3 = r1
            r6 = r5
            r0 = r9
        L_0x04a6:
            java.lang.String r2 = (java.lang.String) r2
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.putExtra(r4, r0)
            goto L_0x04dc
        L_0x04b3:
            com.fossil.ee7.a()
            r2 = 0
            throw r2
        L_0x04b8:
            r2 = 0
            com.fossil.ee7.a()
            throw r2
        L_0x04bd:
            r0 = move-exception
            r3 = r1
            r6 = r5
        L_0x04c0:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.fossil.sj5.i
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = ".sendFeedbackEmail - ex="
            r5.append(r7)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            r2.d(r4, r0)
        L_0x04dc:
            java.lang.ref.WeakReference<android.content.Context> r0 = r3.a
            if (r0 == 0) goto L_0x0528
            if (r0 == 0) goto L_0x0523
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x0528
            java.lang.ref.WeakReference<android.content.Context> r0 = r3.a
            if (r0 == 0) goto L_0x051e
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x0519
            android.content.Context r0 = (android.content.Context) r0
            java.lang.ref.WeakReference<android.content.Context> r2 = r3.a
            if (r2 == 0) goto L_0x0514
            java.lang.Object r2 = r2.get()
            if (r2 == 0) goto L_0x050f
            android.content.Context r2 = (android.content.Context) r2
            r3 = 2131887481(0x7f120579, float:1.940957E38)
            java.lang.String r2 = r2.getString(r3)
            android.content.Intent r2 = android.content.Intent.createChooser(r6, r2)
            r0.startActivity(r2)
            goto L_0x0528
        L_0x050f:
            com.fossil.ee7.a()
            r2 = 0
            throw r2
        L_0x0514:
            r2 = 0
            com.fossil.ee7.a()
            throw r2
        L_0x0519:
            r2 = 0
            com.fossil.ee7.a()
            throw r2
        L_0x051e:
            r2 = 0
            com.fossil.ee7.a()
            throw r2
        L_0x0523:
            r2 = 0
            com.fossil.ee7.a()
            throw r2
        L_0x0528:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sj5.a(java.lang.String, java.util.List, java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final File a(String str, File file) {
        File[] listFiles;
        File file2 = null;
        if (file == null || (listFiles = file.listFiles()) == null) {
            return null;
        }
        if (!(!(listFiles.length == 0))) {
            return null;
        }
        FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - files.length=" + listFiles.length);
        try {
            File file3 = new File(Environment.getExternalStorageDirectory(), str + ".zip");
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file3));
                for (File file4 : listFiles) {
                    byte[] bArr = new byte[1024];
                    FileInputStream fileInputStream = new FileInputStream(file4);
                    ee7.a((Object) file4, "file");
                    zipOutputStream.putNextEntry(new ZipEntry(file4.getName()));
                    qe7 qe7 = new qe7();
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        qe7.element = read;
                        if (!(read > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, qe7.element);
                    }
                    fileInputStream.close();
                }
                zipOutputStream.close();
                return file3;
            } catch (IOException e2) {
                e = e2;
                file2 = file3;
                FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e);
                return file2;
            }
        } catch (IOException e3) {
            e = e3;
            FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e);
            return file2;
        }
    }

    @DexIgnore
    public final List<File> a() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(FLogger.INSTANCE.getLocal().exportAppLogs());
        arrayList2.addAll(FLogger.INSTANCE.getRemote().exportAppLogs());
        arrayList2.addAll(MicroAppEventLogger.exportLogFiles());
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            StringBuilder sb = new StringBuilder();
            sb.append("Exporting ");
            ee7.a((Object) file, "file");
            sb.append(file.getName());
            sb.append(", size=");
            sb.append(file.length());
            local.d(str, sb.toString());
            try {
                File file2 = new File(this.e, file.getName());
                if (file2.exists()) {
                    file2.delete();
                } else {
                    file2.createNewFile();
                }
                FileChannel channel = new FileInputStream(file).getChannel();
                ee7.a((Object) channel, "FileInputStream(file).channel");
                FileChannel channel2 = new FileOutputStream(file2).getChannel();
                ee7.a((Object) channel2, "FileOutputStream(exportFile).channel");
                channel2.transferFrom(channel, 0, channel.size());
                channel.close();
                channel2.close();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = i;
                local2.d(str2, "Done exporting " + file2.getName() + ", size=" + file2.length());
                arrayList.add(file2);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local3.e(str3, "Error while exporting log files - e=" + e2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0144 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(android.graphics.Bitmap r19, com.fossil.fb7<? super com.fossil.i97> r20) {
        /*
            r18 = this;
            r1 = r18
            r2 = r19
            r0 = r20
            boolean r3 = r0 instanceof com.fossil.sj5.i
            if (r3 == 0) goto L_0x0019
            r3 = r0
            com.fossil.sj5$i r3 = (com.fossil.sj5.i) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.sj5$i r3 = new com.fossil.sj5$i
            r3.<init>(r1, r0)
        L_0x001e:
            java.lang.Object r0 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            java.lang.String r6 = "screenshot.jpg"
            r7 = 2
            r8 = 1
            if (r5 == 0) goto L_0x008c
            if (r5 == r8) goto L_0x0069
            if (r5 != r7) goto L_0x0061
            java.lang.Object r2 = r3.L$10
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r3.L$9
            java.util.ArrayList r2 = (java.util.ArrayList) r2
            java.lang.Object r2 = r3.L$8
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r3.L$7
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r3.L$6
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r3.L$5
            java.io.File r2 = (java.io.File) r2
            java.lang.Object r2 = r3.L$4
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r3.L$3
            java.io.File r2 = (java.io.File) r2
            java.lang.Object r2 = r3.L$2
            java.io.OutputStream r2 = (java.io.OutputStream) r2
            java.lang.Object r2 = r3.L$1
            android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
            java.lang.Object r2 = r3.L$0
            com.fossil.sj5 r2 = (com.fossil.sj5) r2
            com.fossil.t87.a(r0)
            goto L_0x01d8
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0069:
            java.lang.Object r2 = r3.L$5
            java.io.File r2 = (java.io.File) r2
            java.lang.Object r5 = r3.L$4
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r8 = r3.L$3
            java.io.File r8 = (java.io.File) r8
            java.lang.Object r10 = r3.L$2
            java.io.OutputStream r10 = (java.io.OutputStream) r10
            java.lang.Object r11 = r3.L$1
            android.graphics.Bitmap r11 = (android.graphics.Bitmap) r11
            java.lang.Object r12 = r3.L$0
            com.fossil.sj5 r12 = (com.fossil.sj5) r12
            com.fossil.t87.a(r0)
            r17 = r12
            r12 = r2
            r2 = r11
            r11 = r17
            goto L_0x0148
        L_0x008c:
            com.fossil.t87.a(r0)
            java.io.File r5 = new java.io.File
            java.lang.String r0 = r1.e
            if (r0 == 0) goto L_0x01ea
            r5.<init>(r0)
            r5.mkdir()     // Catch:{ IOException -> 0x00c3 }
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x00c3 }
            java.lang.String r10 = r1.e     // Catch:{ IOException -> 0x00c3 }
            r0.<init>(r10, r6)     // Catch:{ IOException -> 0x00c3 }
            boolean r10 = r0.exists()     // Catch:{ IOException -> 0x00c3 }
            if (r10 == 0) goto L_0x00ab
            r0.delete()     // Catch:{ IOException -> 0x00c3 }
        L_0x00ab:
            r0.createNewFile()     // Catch:{ IOException -> 0x00c3 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00c3 }
            r10.<init>(r0)     // Catch:{ IOException -> 0x00c3 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ IOException -> 0x00c1 }
            r11 = 90
            r2.compress(r0, r11, r10)     // Catch:{ IOException -> 0x00c1 }
            r10.flush()     // Catch:{ IOException -> 0x00c1 }
            r10.close()     // Catch:{ IOException -> 0x00c1 }
            goto L_0x00e4
        L_0x00c1:
            r0 = move-exception
            goto L_0x00c5
        L_0x00c3:
            r0 = move-exception
            r10 = 0
        L_0x00c5:
            r0.printStackTrace()
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.fossil.sj5.i
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = ".ScreenShootTask - doInBackground ex="
            r13.append(r14)
            r13.append(r0)
            java.lang.String r0 = r13.toString()
            r11.e(r12, r0)
        L_0x00e4:
            com.fossil.we7 r0 = com.fossil.we7.a
            java.lang.Object[] r0 = new java.lang.Object[r8]
            r11 = 0
            long r12 = java.lang.System.currentTimeMillis()
            r14 = 1000(0x3e8, float:1.401E-42)
            long r14 = (long) r14
            long r12 = r12 / r14
            java.lang.Long r12 = com.fossil.pb7.a(r12)
            r0[r11] = r12
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r8)
            java.lang.String r11 = "logCat_%s.txt"
            java.lang.String r11 = java.lang.String.format(r11, r0)
            java.lang.String r0 = "java.lang.String.format(format, *args)"
            com.fossil.ee7.a(r11, r0)
            java.io.File r12 = new java.io.File
            java.lang.String r0 = r1.e
            r12.<init>(r0, r11)
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x012a }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x012a }
            r13.<init>()     // Catch:{ IOException -> 0x012a }
            java.lang.String r14 = "logcat -v time -d -f "
            r13.append(r14)     // Catch:{ IOException -> 0x012a }
            java.lang.String r14 = r12.getAbsolutePath()     // Catch:{ IOException -> 0x012a }
            r13.append(r14)     // Catch:{ IOException -> 0x012a }
            java.lang.String r13 = r13.toString()     // Catch:{ IOException -> 0x012a }
            r0.exec(r13)     // Catch:{ IOException -> 0x012a }
            goto L_0x012e
        L_0x012a:
            r0 = move-exception
            r0.printStackTrace()
        L_0x012e:
            com.portfolio.platform.data.source.UserRepository r0 = r1.h
            r3.L$0 = r1
            r3.L$1 = r2
            r3.L$2 = r10
            r3.L$3 = r5
            r3.L$4 = r11
            r3.L$5 = r12
            r3.label = r8
            java.lang.Object r0 = r0.getCurrentUser(r3)
            if (r0 != r4) goto L_0x0145
            return r4
        L_0x0145:
            r8 = r5
            r5 = r11
            r11 = r1
        L_0x0148:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            java.lang.String r13 = ""
            if (r0 == 0) goto L_0x0155
            java.lang.String r0 = r0.getUserId()
            if (r0 == 0) goto L_0x0155
            goto L_0x0156
        L_0x0155:
            r0 = r13
        L_0x0156:
            com.fossil.yd5 r14 = com.fossil.yd5.a
            java.lang.ref.WeakReference<android.content.Context> r15 = r11.a
            if (r15 == 0) goto L_0x01e5
            java.lang.Object r15 = r15.get()
            if (r15 == 0) goto L_0x01e0
            java.lang.String r9 = "contextWeakReference!!.get()!!"
            com.fossil.ee7.a(r15, r9)
            android.content.Context r15 = (android.content.Context) r15
            java.lang.String r9 = r11.e
            if (r9 == 0) goto L_0x016e
            r13 = r9
        L_0x016e:
            java.util.List r9 = r14.a(r15, r0, r13)
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = r11.e
            if (r14 == 0) goto L_0x01db
            r13.append(r14)
            r13.append(r6)
            java.lang.String r6 = r13.toString()
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            r13.add(r12)
            java.util.List r14 = r11.a()
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r7 = com.fossil.sj5.i
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r16 = r4
            java.lang.String r4 = "Number of app logs="
            r1.append(r4)
            int r4 = r14.size()
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r15.d(r7, r1)
            r13.addAll(r14)
            r3.L$0 = r11
            r3.L$1 = r2
            r3.L$2 = r10
            r3.L$3 = r8
            r3.L$4 = r5
            r3.L$5 = r12
            r3.L$6 = r0
            r3.L$7 = r9
            r3.L$8 = r6
            r3.L$9 = r13
            r3.L$10 = r14
            r1 = 2
            r3.label = r1
            java.lang.Object r0 = r11.a(r6, r9, r13, r3)
            r1 = r16
            if (r0 != r1) goto L_0x01d8
            return r1
        L_0x01d8:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x01db:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x01e0:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        L_0x01e5:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        L_0x01ea:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sj5.a(android.graphics.Bitmap, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final File a(ArrayList<String> arrayList) {
        File file;
        FileInputStream fileInputStream;
        FLogger.INSTANCE.getLocal().d(i, "zipDataFile, total: " + arrayList.size() + " files");
        ZipOutputStream zipOutputStream = null;
        FileInputStream fileInputStream2 = null;
        zipOutputStream = null;
        try {
            file = new File(Environment.getExternalStorageDirectory(), "DebugDataLog.zip");
            try {
                ZipOutputStream zipOutputStream2 = new ZipOutputStream(new FileOutputStream(file));
                try {
                    Iterator<String> it = arrayList.iterator();
                    while (it.hasNext()) {
                        byte[] bArr = new byte[1024];
                        File file2 = new File(it.next());
                        if (file2.exists()) {
                            try {
                                fileInputStream = new FileInputStream(file2);
                                try {
                                    zipOutputStream2.putNextEntry(new ZipEntry(file2.getName()));
                                    FLogger.INSTANCE.getLocal().d(i, "zipDataFile - Processing file=" + file2.getName() + ", length=" + file2.length());
                                    qe7 qe7 = new qe7();
                                    while (true) {
                                        int read = fileInputStream.read(bArr);
                                        qe7.element = read;
                                        if (!(read > 0)) {
                                            break;
                                        }
                                        zipOutputStream2.write(bArr, 0, qe7.element);
                                    }
                                    zipOutputStream2.closeEntry();
                                } catch (Exception e2) {
                                    e = e2;
                                    try {
                                        FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ZipException=" + e);
                                        zipOutputStream2.closeEntry();
                                        rs7.a((InputStream) fileInputStream);
                                    } catch (Throwable th) {
                                        th = th;
                                        fileInputStream2 = fileInputStream;
                                        zipOutputStream2.closeEntry();
                                        rs7.a((InputStream) fileInputStream2);
                                        throw th;
                                    }
                                }
                            } catch (Exception e3) {
                                e = e3;
                                fileInputStream = null;
                                FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ZipException=" + e);
                                zipOutputStream2.closeEntry();
                                rs7.a((InputStream) fileInputStream);
                            } catch (Throwable th2) {
                                th = th2;
                                zipOutputStream2.closeEntry();
                                rs7.a((InputStream) fileInputStream2);
                                throw th;
                            }
                            rs7.a((InputStream) fileInputStream);
                        }
                    }
                    rs7.a((OutputStream) zipOutputStream2);
                } catch (Exception e4) {
                    e = e4;
                    zipOutputStream = zipOutputStream2;
                    try {
                        FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ex=" + e);
                        rs7.a((OutputStream) zipOutputStream);
                        return file;
                    } catch (Throwable th3) {
                        th = th3;
                        rs7.a((OutputStream) zipOutputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    zipOutputStream = zipOutputStream2;
                    rs7.a((OutputStream) zipOutputStream);
                    throw th;
                }
            } catch (Exception e5) {
                e = e5;
                FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ex=" + e);
                rs7.a((OutputStream) zipOutputStream);
                return file;
            }
        } catch (Exception e6) {
            e = e6;
            file = null;
            FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ex=" + e);
            rs7.a((OutputStream) zipOutputStream);
            return file;
        }
        return file;
    }

    @DexIgnore
    public final Uri a(String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.parse("file://" + str);
        } else if (this.a == null || TextUtils.isEmpty(str)) {
            return null;
        } else {
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Context context = weakReference.get();
                if (context != null) {
                    return FileProvider.getUriForFile(context, "com.fossil.wearables.fossil.provider", new File(str));
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }
}
