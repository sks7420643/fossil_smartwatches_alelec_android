package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux0 extends zk0 {
    @DexIgnore
    public /* final */ y90[] C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ux0(ri1 ri1, en0 en0, y90[] y90Arr, String str, int i) {
        super(ri1, en0, wm0.H, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = y90Arr;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        boolean z = false;
        for (y90 y90 : this.C) {
            sg0 iconConfig = y90.getIconConfig();
            if (iconConfig != null) {
                for (gg0 gg0 : iconConfig.b()) {
                    copyOnWriteArrayList.addIfAbsent(gg0);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new gg0[0]);
        if (array != null) {
            gg0[] gg0Arr = (gg0[]) array;
            if (gg0Arr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    zr0 zr0 = zr0.d;
                    r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.NOTIFICATION.a));
                    if (r60 == null) {
                        r60 = b21.x.d();
                    }
                    zk0.a(this, new s81(((zk0) this).w, ((zk0) this).x, wm0.V, true, 1793, zr0.a(gg0Arr, (short) 1793, r60), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 64), new ms0(this), new iu0(this), new cw0(this), (gd7) null, (gd7) null, 48, (Object) null);
                } catch (f41 e) {
                    wl0.h.a(e);
                    a(eu0.a(((zk0) this).v, null, is0.INCOMPATIBLE_FIRMWARE, null, 5));
                }
            } else {
                m();
            }
        } else {
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.T, yz0.a(this.C));
    }

    @DexIgnore
    public final void m() {
        short b = gq0.b.b(((zk0) this).w.u, pb1.NOTIFICATION_FILTER);
        try {
            nm0 nm0 = nm0.d;
            r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.NOTIFICATION_FILTER.a));
            if (r60 == null) {
                r60 = b21.x.d();
            }
            zk0.a(this, new s81(((zk0) this).w, ((zk0) this).x, wm0.W, true, b, nm0.a(b, r60, this.C), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 64), new vo0(this), new rq0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        } catch (f41 e) {
            wl0.h.a(e);
            a(eu0.a(((zk0) this).v, null, is0.UNSUPPORTED_FORMAT, null, 5));
        }
    }
}
