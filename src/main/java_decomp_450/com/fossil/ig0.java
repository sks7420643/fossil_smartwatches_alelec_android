package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ r60 b;
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ig0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ig0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                return new ig0(createByteArray);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ig0[] newArray(int i) {
            return new ig0[i];
        }
    }

    @DexIgnore
    public ig0(byte[] bArr) throws IllegalArgumentException {
        this.a = bArr;
        if (bArr.length >= 16) {
            this.c = ByteBuffer.wrap(s97.a(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
            this.b = new r60(bArr[2], bArr[3]);
            return;
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("data.size("), this.a.length, ") is not equal or larger ", "than 16"));
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.y0, yz0.a(this.c)), r51.h2, this.b.toString()), r51.d3, Long.valueOf(ik1.a.a(this.a, ng1.CRC32C))), r51.H, Integer.valueOf(this.a.length));
    }

    @DexIgnore
    public final short b() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.a;
    }

    @DexIgnore
    public final r60 getWatchParameterVersion() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
    }
}
