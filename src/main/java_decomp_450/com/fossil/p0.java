package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import com.fossil.p1;
import com.fossil.v1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p0 extends ActionBar {
    @DexIgnore
    public p2 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Window.Callback c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ArrayList<ActionBar.a> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Runnable g; // = new a();
    @DexIgnore
    public /* final */ Toolbar.e h; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            p0.this.n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Toolbar.e {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.Toolbar.e
        public boolean onMenuItemClick(MenuItem menuItem) {
            return p0.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements p1.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.p1.a
        public void a(p1 p1Var) {
            p0 p0Var = p0.this;
            if (p0Var.c == null) {
                return;
            }
            if (p0Var.a.a()) {
                p0.this.c.onPanelClosed(108, p1Var);
            } else if (p0.this.c.onPreparePanel(0, null, p1Var)) {
                p0.this.c.onMenuOpened(108, p1Var);
            }
        }

        @DexIgnore
        @Override // com.fossil.p1.a
        public boolean a(p1 p1Var, MenuItem menuItem) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends i1 {
        @DexIgnore
        public e(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        @Override // com.fossil.i1
        public View onCreatePanelView(int i) {
            if (i == 0) {
                return new View(p0.this.a.getContext());
            }
            return super.onCreatePanelView(i);
        }

        @DexIgnore
        @Override // com.fossil.i1
        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                p0 p0Var = p0.this;
                if (!p0Var.b) {
                    p0Var.a.b();
                    p0.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    @DexIgnore
    public p0(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.a = new h3(toolbar, false);
        e eVar = new e(callback);
        this.c = eVar;
        this.a.setWindowCallback(eVar);
        toolbar.setOnMenuItemClickListener(this.h);
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void a(float f2) {
        da.a(this.a.k(), f2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void b(CharSequence charSequence) {
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void c(boolean z) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void d(boolean z) {
        a(z ? 4 : 0, 4);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void e(boolean z) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean e() {
        return this.a.e();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean f() {
        if (!this.a.h()) {
            return false;
        }
        this.a.collapseActionView();
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public int g() {
        return this.a.l();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public Context h() {
        return this.a.getContext();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean i() {
        this.a.k().removeCallbacks(this.g);
        da.a(this.a.k(), this.g);
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void j() {
        this.a.k().removeCallbacks(this.g);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean k() {
        return this.a.f();
    }

    @DexIgnore
    public final Menu l() {
        if (!this.d) {
            this.a.a(new c(), new d());
            this.d = true;
        }
        return this.a.i();
    }

    @DexIgnore
    public Window.Callback m() {
        return this.c;
    }

    @DexIgnore
    public void n() {
        Menu l = l();
        p1 p1Var = l instanceof p1 ? (p1) l : null;
        if (p1Var != null) {
            p1Var.s();
        }
        try {
            l.clear();
            if (!this.c.onCreatePanelMenu(0, l) || !this.c.onPreparePanel(0, null, l)) {
                l.clear();
            }
        } finally {
            if (p1Var != null) {
                p1Var.r();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements v1.a {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public boolean a(p1 p1Var) {
            Window.Callback callback = p0.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, p1Var);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public void a(p1 p1Var, boolean z) {
            if (!this.a) {
                this.a = true;
                p0.this.a.g();
                Window.Callback callback = p0.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, p1Var);
                }
                this.a = false;
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void a(Configuration configuration) {
        super.a(configuration);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void b(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.get(i).a(z);
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void a(CharSequence charSequence) {
        this.a.setTitle(charSequence);
    }

    @DexIgnore
    public void a(int i, int i2) {
        this.a.a((i & i2) | ((~i2) & this.a.l()));
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            k();
        }
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean a(int i, KeyEvent keyEvent) {
        Menu l = l();
        if (l == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        l.setQwertyMode(z);
        return l.performShortcut(i, keyEvent, 0);
    }
}
