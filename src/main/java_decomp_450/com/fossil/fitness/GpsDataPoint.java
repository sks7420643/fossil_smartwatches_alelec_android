package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GpsDataPoint implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<GpsDataPoint> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ double mAltitude;
    @DexIgnore
    public /* final */ double mHeading;
    @DexIgnore
    public /* final */ float mHorizontalAccuracy;
    @DexIgnore
    public /* final */ double mLatitude;
    @DexIgnore
    public /* final */ double mLongitude;
    @DexIgnore
    public /* final */ double mSpeed;
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ float mVerticalAccuracy;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<GpsDataPoint> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public GpsDataPoint createFromParcel(Parcel parcel) {
            return new GpsDataPoint(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public GpsDataPoint[] newArray(int i) {
            return new GpsDataPoint[i];
        }
    }

    @DexIgnore
    public GpsDataPoint(int i, double d, double d2, float f, float f2, double d3, double d4, double d5) {
        this.mTimestamp = i;
        this.mLongitude = d;
        this.mLatitude = d2;
        this.mHorizontalAccuracy = f;
        this.mVerticalAccuracy = f2;
        this.mAltitude = d3;
        this.mSpeed = d4;
        this.mHeading = d5;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof GpsDataPoint)) {
            return false;
        }
        GpsDataPoint gpsDataPoint = (GpsDataPoint) obj;
        if (this.mTimestamp == gpsDataPoint.mTimestamp && this.mLongitude == gpsDataPoint.mLongitude && this.mLatitude == gpsDataPoint.mLatitude && this.mHorizontalAccuracy == gpsDataPoint.mHorizontalAccuracy && this.mVerticalAccuracy == gpsDataPoint.mVerticalAccuracy && this.mAltitude == gpsDataPoint.mAltitude && this.mSpeed == gpsDataPoint.mSpeed && this.mHeading == gpsDataPoint.mHeading) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public double getAltitude() {
        return this.mAltitude;
    }

    @DexIgnore
    public double getHeading() {
        return this.mHeading;
    }

    @DexIgnore
    public float getHorizontalAccuracy() {
        return this.mHorizontalAccuracy;
    }

    @DexIgnore
    public double getLatitude() {
        return this.mLatitude;
    }

    @DexIgnore
    public double getLongitude() {
        return this.mLongitude;
    }

    @DexIgnore
    public double getSpeed() {
        return this.mSpeed;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public float getVerticalAccuracy() {
        return this.mVerticalAccuracy;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((((((((((527 + this.mTimestamp) * 31) + ((int) (Double.doubleToLongBits(this.mLongitude) ^ (Double.doubleToLongBits(this.mLongitude) >>> 32)))) * 31) + ((int) (Double.doubleToLongBits(this.mLatitude) ^ (Double.doubleToLongBits(this.mLatitude) >>> 32)))) * 31) + Float.floatToIntBits(this.mHorizontalAccuracy)) * 31) + Float.floatToIntBits(this.mVerticalAccuracy)) * 31) + ((int) (Double.doubleToLongBits(this.mAltitude) ^ (Double.doubleToLongBits(this.mAltitude) >>> 32)))) * 31) + ((int) (Double.doubleToLongBits(this.mSpeed) ^ (Double.doubleToLongBits(this.mSpeed) >>> 32)))) * 31) + ((int) (Double.doubleToLongBits(this.mHeading) ^ (Double.doubleToLongBits(this.mHeading) >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "GpsDataPoint{mTimestamp=" + this.mTimestamp + ",mLongitude=" + this.mLongitude + ",mLatitude=" + this.mLatitude + ",mHorizontalAccuracy=" + this.mHorizontalAccuracy + ",mVerticalAccuracy=" + this.mVerticalAccuracy + ",mAltitude=" + this.mAltitude + ",mSpeed=" + this.mSpeed + ",mHeading=" + this.mHeading + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mTimestamp);
        parcel.writeDouble(this.mLongitude);
        parcel.writeDouble(this.mLatitude);
        parcel.writeFloat(this.mHorizontalAccuracy);
        parcel.writeFloat(this.mVerticalAccuracy);
        parcel.writeDouble(this.mAltitude);
        parcel.writeDouble(this.mSpeed);
        parcel.writeDouble(this.mHeading);
    }

    @DexIgnore
    public GpsDataPoint(Parcel parcel) {
        this.mTimestamp = parcel.readInt();
        this.mLongitude = parcel.readDouble();
        this.mLatitude = parcel.readDouble();
        this.mHorizontalAccuracy = parcel.readFloat();
        this.mVerticalAccuracy = parcel.readFloat();
        this.mAltitude = parcel.readDouble();
        this.mSpeed = parcel.readDouble();
        this.mHeading = parcel.readDouble();
    }
}
