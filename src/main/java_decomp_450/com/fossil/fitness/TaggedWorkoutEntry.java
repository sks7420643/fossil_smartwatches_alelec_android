package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class TaggedWorkoutEntry implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<TaggedWorkoutEntry> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Byte mAceLatencyInMinute;
    @DexIgnore
    public /* final */ byte mCaloriesBeforeTagged;
    @DexIgnore
    public /* final */ Integer mDistanceBeforeTagged;
    @DexIgnore
    public /* final */ short mHeartrate;
    @DexIgnore
    public /* final */ long mId;
    @DexIgnore
    public /* final */ WorkoutMode mMode;
    @DexIgnore
    public /* final */ byte mSecondsInMinute;
    @DexIgnore
    public /* final */ WorkoutState mState;
    @DexIgnore
    public /* final */ int mStateChangeIndexInSeconds;
    @DexIgnore
    public /* final */ short mStepsBeforeTagged;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<TaggedWorkoutEntry> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TaggedWorkoutEntry createFromParcel(Parcel parcel) {
            return new TaggedWorkoutEntry(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TaggedWorkoutEntry[] newArray(int i) {
            return new TaggedWorkoutEntry[i];
        }
    }

    @DexIgnore
    public TaggedWorkoutEntry(long j, byte b, short s, byte b2, short s2, WorkoutState workoutState, WorkoutType workoutType, int i, Byte b3, Integer num, WorkoutMode workoutMode) {
        this.mId = j;
        this.mSecondsInMinute = b;
        this.mStepsBeforeTagged = s;
        this.mCaloriesBeforeTagged = b2;
        this.mHeartrate = s2;
        this.mState = workoutState;
        this.mType = workoutType;
        this.mStateChangeIndexInSeconds = i;
        this.mAceLatencyInMinute = b3;
        this.mDistanceBeforeTagged = num;
        this.mMode = workoutMode;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        Byte b;
        if (!(obj instanceof TaggedWorkoutEntry)) {
            return false;
        }
        TaggedWorkoutEntry taggedWorkoutEntry = (TaggedWorkoutEntry) obj;
        if (this.mId != taggedWorkoutEntry.mId || this.mSecondsInMinute != taggedWorkoutEntry.mSecondsInMinute || this.mStepsBeforeTagged != taggedWorkoutEntry.mStepsBeforeTagged || this.mCaloriesBeforeTagged != taggedWorkoutEntry.mCaloriesBeforeTagged || this.mHeartrate != taggedWorkoutEntry.mHeartrate || this.mState != taggedWorkoutEntry.mState || this.mType != taggedWorkoutEntry.mType || this.mStateChangeIndexInSeconds != taggedWorkoutEntry.mStateChangeIndexInSeconds) {
            return false;
        }
        if ((this.mAceLatencyInMinute != null || taggedWorkoutEntry.mAceLatencyInMinute != null) && ((b = this.mAceLatencyInMinute) == null || !b.equals(taggedWorkoutEntry.mAceLatencyInMinute))) {
            return false;
        }
        if (((this.mDistanceBeforeTagged != null || taggedWorkoutEntry.mDistanceBeforeTagged != null) && ((num = this.mDistanceBeforeTagged) == null || !num.equals(taggedWorkoutEntry.mDistanceBeforeTagged))) || this.mMode != taggedWorkoutEntry.mMode) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public Byte getAceLatencyInMinute() {
        return this.mAceLatencyInMinute;
    }

    @DexIgnore
    public byte getCaloriesBeforeTagged() {
        return this.mCaloriesBeforeTagged;
    }

    @DexIgnore
    public Integer getDistanceBeforeTagged() {
        return this.mDistanceBeforeTagged;
    }

    @DexIgnore
    public short getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public long getId() {
        return this.mId;
    }

    @DexIgnore
    public WorkoutMode getMode() {
        return this.mMode;
    }

    @DexIgnore
    public byte getSecondsInMinute() {
        return this.mSecondsInMinute;
    }

    @DexIgnore
    public WorkoutState getState() {
        return this.mState;
    }

    @DexIgnore
    public int getStateChangeIndexInSeconds() {
        return this.mStateChangeIndexInSeconds;
    }

    @DexIgnore
    public short getStepsBeforeTagged() {
        return this.mStepsBeforeTagged;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.mId;
        int hashCode = (((((((((((((((527 + ((int) (j ^ (j >>> 32)))) * 31) + this.mSecondsInMinute) * 31) + this.mStepsBeforeTagged) * 31) + this.mCaloriesBeforeTagged) * 31) + this.mHeartrate) * 31) + this.mState.hashCode()) * 31) + this.mType.hashCode()) * 31) + this.mStateChangeIndexInSeconds) * 31;
        Byte b = this.mAceLatencyInMinute;
        int i = 0;
        int hashCode2 = (hashCode + (b == null ? 0 : b.hashCode())) * 31;
        Integer num = this.mDistanceBeforeTagged;
        if (num != null) {
            i = num.hashCode();
        }
        return ((hashCode2 + i) * 31) + this.mMode.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "TaggedWorkoutEntry{mId=" + this.mId + ",mSecondsInMinute=" + ((int) this.mSecondsInMinute) + ",mStepsBeforeTagged=" + ((int) this.mStepsBeforeTagged) + ",mCaloriesBeforeTagged=" + ((int) this.mCaloriesBeforeTagged) + ",mHeartrate=" + ((int) this.mHeartrate) + ",mState=" + this.mState + ",mType=" + this.mType + ",mStateChangeIndexInSeconds=" + this.mStateChangeIndexInSeconds + ",mAceLatencyInMinute=" + this.mAceLatencyInMinute + ",mDistanceBeforeTagged=" + this.mDistanceBeforeTagged + ",mMode=" + this.mMode + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mId);
        parcel.writeByte(this.mSecondsInMinute);
        parcel.writeInt(this.mStepsBeforeTagged);
        parcel.writeByte(this.mCaloriesBeforeTagged);
        parcel.writeInt(this.mHeartrate);
        parcel.writeInt(this.mState.ordinal());
        parcel.writeInt(this.mType.ordinal());
        parcel.writeInt(this.mStateChangeIndexInSeconds);
        if (this.mAceLatencyInMinute != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByte(this.mAceLatencyInMinute.byteValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mDistanceBeforeTagged != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mDistanceBeforeTagged.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeInt(this.mMode.ordinal());
    }

    @DexIgnore
    public TaggedWorkoutEntry(Parcel parcel) {
        this.mId = parcel.readLong();
        this.mSecondsInMinute = parcel.readByte();
        this.mStepsBeforeTagged = (short) parcel.readInt();
        this.mCaloriesBeforeTagged = parcel.readByte();
        this.mHeartrate = (short) parcel.readInt();
        this.mState = WorkoutState.values()[parcel.readInt()];
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mStateChangeIndexInSeconds = parcel.readInt();
        if (parcel.readByte() == 0) {
            this.mAceLatencyInMinute = null;
        } else {
            this.mAceLatencyInMinute = Byte.valueOf(parcel.readByte());
        }
        if (parcel.readByte() == 0) {
            this.mDistanceBeforeTagged = null;
        } else {
            this.mDistanceBeforeTagged = Integer.valueOf(parcel.readInt());
        }
        this.mMode = WorkoutMode.values()[parcel.readInt()];
    }
}
