package com.fossil.fitness;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FitnessAlgorithm {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends FitnessAlgorithm {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        @Override // com.fossil.fitness.FitnessAlgorithm
        public static native FitnessAlgorithm create(UserProfile userProfile);

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native Result native_parseWithActivitySamples(long j, ArrayList<ActivitySample> arrayList, SyncMode syncMode);

        @DexIgnore
        private native Result native_parseWithBinaries(long j, ArrayList<BinaryFile> arrayList, SyncMode syncMode);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        @Override // com.fossil.fitness.FitnessAlgorithm
        public Result parseWithActivitySamples(ArrayList<ActivitySample> arrayList, SyncMode syncMode) {
            return native_parseWithActivitySamples(this.nativeRef, arrayList, syncMode);
        }

        @DexIgnore
        @Override // com.fossil.fitness.FitnessAlgorithm
        public Result parseWithBinaries(ArrayList<BinaryFile> arrayList, SyncMode syncMode) {
            return native_parseWithBinaries(this.nativeRef, arrayList, syncMode);
        }
    }

    @DexIgnore
    public static FitnessAlgorithm create(UserProfile userProfile) {
        return CppProxy.create(userProfile);
    }

    @DexIgnore
    public abstract Result parseWithActivitySamples(ArrayList<ActivitySample> arrayList, SyncMode syncMode);

    @DexIgnore
    public abstract Result parseWithBinaries(ArrayList<BinaryFile> arrayList, SyncMode syncMode);
}
