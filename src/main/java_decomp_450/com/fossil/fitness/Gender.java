package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Gender {
    UNSPECIFIED(0),
    MALE(1),
    FEMALE(2);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public Gender(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
