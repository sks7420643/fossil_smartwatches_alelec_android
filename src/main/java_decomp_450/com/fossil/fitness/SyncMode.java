package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum SyncMode {
    AUTO_SYNC(0),
    FULL_SYNC(1);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public SyncMode(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
