package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum WorkoutType {
    UNKNOWN(0),
    RUNNING(1),
    CYCLING(2),
    TREADMILL(3),
    ELLIPTICAL(4),
    WEIGHTS(5),
    WORKOUT(6),
    YOGA(7),
    WALKING(8),
    ROWING(9),
    SWIMMING(10),
    AEROBIC(11),
    HIKING(12),
    SPINNING(13);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public WorkoutType(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
