package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LocationDistance implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<LocationDistance> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ double mDistanceInCentimeters;
    @DexIgnore
    public /* final */ int mDuration;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<LocationDistance> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LocationDistance createFromParcel(Parcel parcel) {
            return new LocationDistance(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LocationDistance[] newArray(int i) {
            return new LocationDistance[i];
        }
    }

    @DexIgnore
    public LocationDistance(double d, int i) {
        this.mDistanceInCentimeters = d;
        this.mDuration = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof LocationDistance)) {
            return false;
        }
        LocationDistance locationDistance = (LocationDistance) obj;
        if (this.mDistanceInCentimeters == locationDistance.mDistanceInCentimeters && this.mDuration == locationDistance.mDuration) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public double getDistanceInCentimeters() {
        return this.mDistanceInCentimeters;
    }

    @DexIgnore
    public int getDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + ((int) (Double.doubleToLongBits(this.mDistanceInCentimeters) ^ (Double.doubleToLongBits(this.mDistanceInCentimeters) >>> 32)))) * 31) + this.mDuration;
    }

    @DexIgnore
    public String toString() {
        return "LocationDistance{mDistanceInCentimeters=" + this.mDistanceInCentimeters + ",mDuration=" + this.mDuration + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.mDistanceInCentimeters);
        parcel.writeInt(this.mDuration);
    }

    @DexIgnore
    public LocationDistance(Parcel parcel) {
        this.mDistanceInCentimeters = parcel.readDouble();
        this.mDuration = parcel.readInt();
    }
}
