package com.fossil.fitness;

import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class GpsDataProvider {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends GpsDataProvider {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        @Override // com.fossil.fitness.GpsDataProvider
        public static native GpsDataProvider defaultProvider();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native void native_onLocationChanged(long j, GpsDataPoint gpsDataPoint);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        @Override // com.fossil.fitness.GpsDataProvider
        public void onLocationChanged(GpsDataPoint gpsDataPoint) {
            native_onLocationChanged(this.nativeRef, gpsDataPoint);
        }
    }

    @DexIgnore
    public static GpsDataProvider defaultProvider() {
        return CppProxy.defaultProvider();
    }

    @DexIgnore
    public abstract void onLocationChanged(GpsDataPoint gpsDataPoint);
}
