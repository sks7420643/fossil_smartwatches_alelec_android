package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HighResolutionHeartRate implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<HighResolutionHeartRate> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ byte mSecondsInMinute;
    @DexIgnore
    public /* final */ short mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<HighResolutionHeartRate> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HighResolutionHeartRate createFromParcel(Parcel parcel) {
            return new HighResolutionHeartRate(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HighResolutionHeartRate[] newArray(int i) {
            return new HighResolutionHeartRate[i];
        }
    }

    @DexIgnore
    public HighResolutionHeartRate(byte b, short s) {
        this.mSecondsInMinute = b;
        this.mValue = s;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof HighResolutionHeartRate)) {
            return false;
        }
        HighResolutionHeartRate highResolutionHeartRate = (HighResolutionHeartRate) obj;
        if (this.mSecondsInMinute == highResolutionHeartRate.mSecondsInMinute && this.mValue == highResolutionHeartRate.mValue) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public byte getSecondsInMinute() {
        return this.mSecondsInMinute;
    }

    @DexIgnore
    public short getValue() {
        return this.mValue;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.mSecondsInMinute) * 31) + this.mValue;
    }

    @DexIgnore
    public String toString() {
        return "HighResolutionHeartRate{mSecondsInMinute=" + ((int) this.mSecondsInMinute) + ",mValue=" + ((int) this.mValue) + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.mSecondsInMinute);
        parcel.writeInt(this.mValue);
    }

    @DexIgnore
    public HighResolutionHeartRate(Parcel parcel) {
        this.mSecondsInMinute = parcel.readByte();
        this.mValue = (short) parcel.readInt();
    }
}
