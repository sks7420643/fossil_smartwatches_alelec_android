package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Handler;
import android.os.PowerManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.workout.ResumeWorkoutInfoData;
import com.misfit.frameworks.buttonservice.model.watchapp.response.workout.StartWorkoutInfoData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk5 {
    @DexIgnore
    public Location a;
    @DexIgnore
    public /* final */ PowerManager b;
    @DexIgnore
    public b c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public Handler e;
    @DexIgnore
    public Runnable f;
    @DexIgnore
    public /* final */ kn7 g;
    @DexIgnore
    public /* final */ yi7 h;
    @DexIgnore
    public /* final */ c i;
    @DexIgnore
    public /* final */ PortfolioApp j;
    @DexIgnore
    public /* final */ LocationSource k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements LocationSource.LocationListener {
        @DexIgnore
        public /* final */ /* synthetic */ wk5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(wk5 wk5) {
            this.a = wk5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.LocationSource.LocationListener
        public void onLocationResult(Location location) {
            ee7.b(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutTetherGpsManager", "onLocationChanged location=" + location);
            if (this.a.a == null) {
                long currentTimeMillis = System.currentTimeMillis() - location.getTime();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("WorkoutTetherGpsManager", "delta of first location from android " + currentTimeMillis);
                if (currentTimeMillis > ((long) 2000)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("WorkoutTetherGpsManager", "Ignore this outdated location " + location);
                    return;
                }
                this.a.a = location;
            }
            this.a.j.a(this.a.j.c(), location);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ wk5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(wk5 wk5) {
            this.a = wk5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (ee7.a((Object) (intent != null ? intent.getAction() : null), (Object) "android.os.action.POWER_SAVE_MODE_CHANGED")) {
                FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "mPowerSavingModeChangedReceiver");
                this.a.d();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ wk5 a;

        @DexIgnore
        public d(wk5 wk5) {
            this.a = wk5;
        }

        @DexIgnore
        public final void run() {
            this.a.i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.workout.WorkoutTetherGpsManager$startLocationObserver$1", f = "WorkoutTetherGpsManager.kt", l = {261}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(wk5 wk5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = wk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            kn7 kn7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                kn7 e = this.this$0.g;
                this.L$0 = yi7;
                this.L$1 = e;
                this.label = 1;
                if (e.a(null, this) == a) {
                    return a;
                }
                kn7 = e;
            } else if (i == 1) {
                kn7 = (kn7) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                boolean isObservingLocation = this.this$0.k.isObservingLocation(this.this$0.c, false);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherGpsManager", "startLocationObserver isObserving " + isObservingLocation);
                this.this$0.e.removeCallbacksAndMessages(null);
                if (!isObservingLocation) {
                    this.this$0.a = (Location) null;
                    FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "startLocationObserver start observing location");
                    this.this$0.k.observerLocation(this.this$0.j, this.this$0.c, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                i97 i97 = i97.a;
                kn7.a(null);
                return i97.a;
            } catch (Throwable th) {
                kn7.a(null);
                throw th;
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public wk5(PortfolioApp portfolioApp, LocationSource locationSource) {
        ee7.b(portfolioApp, "mPortfolioApp");
        ee7.b(locationSource, "mLocationSource");
        this.j = portfolioApp;
        this.k = locationSource;
        Object systemService = PortfolioApp.g0.c().getSystemService("power");
        if (systemService != null) {
            this.b = (PowerManager) systemService;
            this.c = new b(this);
            this.e = new Handler();
            this.f = new d(this);
            this.g = mn7.a(false, 1, null);
            this.h = zi7.a(qj7.c().plus(dl7.a(null, 1, null)));
            this.i = new c(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.os.PowerManager");
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "stopLocationObserver");
        this.a = null;
        qh5.c.a(PortfolioApp.g0.c());
        this.k.unObserverLocation(this.c, false);
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "stopWorkoutTracking");
        this.d = false;
        h();
        j();
    }

    @DexIgnore
    public final void j() {
        try {
            PortfolioApp.g0.c().unregisterReceiver(this.i);
        } catch (Exception unused) {
        }
    }

    @DexIgnore
    public final r87<String, String> b() {
        String str;
        String str2;
        boolean a2 = xg5.b.a(PortfolioApp.g0.c(), xg5.c.LOCATION_FINE);
        boolean a3 = xg5.b.a(PortfolioApp.g0.c(), xg5.c.LOCATION_BACKGROUND);
        boolean a4 = xg5.b.a(PortfolioApp.g0.c(), xg5.c.LOCATION_SERVICE);
        boolean isPowerSaveMode = this.b.isPowerSaveMode();
        PortfolioApp c2 = PortfolioApp.g0.c();
        we7 we7 = we7.a;
        String string = c2.getResources().getString(2131887191);
        ee7.a((Object) string, "context.resources.getStr\u2026AllowBrandAppToVisualize)");
        Object[] objArr = new Object[1];
        String h2 = c2.h();
        if (h2 != null) {
            String upperCase = h2.toUpperCase();
            ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            objArr[0] = upperCase;
            String format = String.format(string, Arrays.copyOf(objArr, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "postWorkoutNotification isFinePermissionGranted " + a2 + " isBackgroundLocationGranted " + a3 + " isLocationTurnOn " + a4 + " isPowerSaving " + isPowerSaveMode);
            if (!a2 || !a3) {
                str2 = c2.getResources().getString(2131887187);
                ee7.a((Object) str2, "context.resources.getStr\u2026LocationPermissionAlways)");
                str = str2 + ' ' + format;
            } else if (a2 && a3 && !a4) {
                str2 = c2.getResources().getString(2131887193);
                ee7.a((Object) str2, "context.resources.getStr\u2026__TurnOnLocationServices)");
                str = str2 + "  " + format;
            } else if (!a2 || !a3 || !a4 || !isPowerSaveMode) {
                str2 = c2.getResources().getString(2131887189);
                ee7.a((Object) str2, "context.resources.getStr\u2026._____GpsWorkoutTracking)");
                str = c2.getResources().getString(2131886674);
                ee7.a((Object) str, "context.resources.getStr\u2026rkoutSessionLocationData)");
            } else {
                str2 = c2.getResources().getString(2131887192);
                ee7.a((Object) str2, "context.resources.getStr\u2026__TurnOffPowerSavingMode)");
                str = str2 + ' ' + format;
            }
            return new r87<>(str2, str);
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean c() {
        return this.d;
    }

    @DexIgnore
    public final void d() {
        r87<String, String> b2 = b();
        qh5.c.a(this.j, b2.getFirst(), b2.getSecond());
    }

    @DexIgnore
    public final void e() {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            PortfolioApp.g0.c().registerReceiver(this.i, intentFilter);
        } catch (Exception unused) {
        }
    }

    @DexIgnore
    public final void f() {
        boolean isObservingLocation = this.k.isObservingLocation(this.c, false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "scheduleStopGpsListener isObserving " + isObservingLocation);
        this.e.removeCallbacksAndMessages(null);
        if (isObservingLocation) {
            this.e.postDelayed(this.f, 3600000);
        }
    }

    @DexIgnore
    public final ik7 g() {
        return xh7.b(this.h, null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final String a(boolean z, boolean z2) {
        boolean a2 = xg5.b.a(PortfolioApp.g0.c(), xg5.c.LOCATION_BACKGROUND);
        boolean isPowerSaveMode = this.b.isPowerSaveMode();
        PortfolioApp c2 = PortfolioApp.g0.c();
        String str = "";
        if (!z || !a2) {
            str = str + ' ' + c2.getResources().getString(2131887187);
        }
        if (!z2) {
            if (str.length() > 0) {
                str = str + ',';
            }
            str = str + ' ' + c2.getResources().getString(2131887193);
        }
        if (isPowerSaveMode) {
            if (str.length() > 0) {
                str = str + ',';
            }
            str = str + ' ' + c2.getResources().getString(2131887192);
        }
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "watchContent " + str);
        if (!(str.length() > 0)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(' ');
        we7 we7 = we7.a;
        String string = c2.getResources().getString(2131887191);
        ee7.a((Object) string, "context.resources.getStr\u2026AllowBrandAppToVisualize)");
        Object[] objArr = new Object[1];
        String h2 = c2.h();
        if (h2 != null) {
            String upperCase = h2.toUpperCase();
            ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            objArr[0] = upperCase;
            String format = String.format(string, Arrays.copyOf(objArr, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            sb.append(format);
            return sb.toString();
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final void a(String str, ec0 ec0) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "startWorkoutTracking serial " + str + " request " + ec0);
        if (ec0 == null || ec0.isRequiredGPS()) {
            j();
            e();
            boolean a2 = xg5.b.a(PortfolioApp.g0.c(), xg5.c.LOCATION_FINE);
            boolean a3 = xg5.b.a(PortfolioApp.g0.c(), xg5.c.LOCATION_SERVICE);
            boolean z = true;
            this.d = true;
            if (a2 && a3) {
                g();
            }
            d();
            String a4 = a(a2, a3);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WorkoutTetherGpsManager", "startWorkoutTracking watchErrorContent " + a4);
            if (a4.length() <= 0) {
                z = false;
            }
            if (z) {
                PortfolioApp c2 = PortfolioApp.g0.c();
                if (ec0 != null) {
                    c2.a(new StartWorkoutInfoData(ec0, a4, uf0.ERROR), str);
                    return;
                }
                return;
            }
            PortfolioApp c3 = PortfolioApp.g0.c();
            if (ec0 != null) {
                c3.a(new StartWorkoutInfoData(ec0, "", uf0.SUCCESS), str);
                return;
            }
            return;
        }
        PortfolioApp.g0.c().a(new StartWorkoutInfoData(ec0, "", uf0.SUCCESS), str);
        h();
    }

    @DexIgnore
    public final void a(bc0 bc0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "resumeWorkoutTracking " + bc0);
        if (bc0 != null) {
            if (bc0.isRequiredGPS()) {
                FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "resumeWorkoutTracking gpsRequired");
                a();
            }
            PortfolioApp.g0.c().a(new ResumeWorkoutInfoData(bc0, "", uf0.SUCCESS), PortfolioApp.g0.c().c());
        }
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "autoResumeWorkoutTracking ");
        this.d = true;
        g();
        d();
        e();
    }
}
