package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z80 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutType a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ x80 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<z80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final z80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length >= 6) {
                boolean z = false;
                WorkoutType c = yz0.c(bArr[0]);
                if (c == null) {
                    c = WorkoutType.UNKNOWN;
                }
                boolean z2 = ((bArr[1] >> 0) & 1) == 1;
                if (((bArr[1] >> 1) & 1) == 1) {
                    z = true;
                }
                return new z80(c, z2, z, new x80(yz0.b(bArr[2]), yz0.b(bArr[3]), yz0.b(bArr[4]), yz0.b(bArr[5])));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 6"));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public z80 createFromParcel(Parcel parcel) {
            return new z80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public z80[] newArray(int i) {
            return new z80[i];
        }
    }

    @DexIgnore
    public z80(WorkoutType workoutType, boolean z, boolean z2, x80 x80) {
        this.a = workoutType;
        this.b = z;
        this.c = z2;
        this.d = x80;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("workout_type", this.a.name());
            jSONObject.put("enable_detection", this.b);
            jSONObject.put("enable_prompt", this.c);
            jSONObject.put("activity_detection_latency", this.d);
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final byte[] b() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put((byte) this.a.getValue()).put((byte) (((this.b ? 1 : 0) << 0) | 0 | ((this.c ? 1 : 0) << 1))).put((byte) this.d.getStartLatencyInMinute()).put((byte) this.d.getPauseLatencyInMinute()).put((byte) this.d.getResumeLatencyInMinute()).put((byte) this.d.getStopLatencyInMinute()).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(z80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            z80 z80 = (z80) obj;
            return this.a == z80.a && this.b == z80.b && this.c == z80.c && !(ee7.a(this.d, z80.d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.AutoWorkoutDetectionItemConfig");
    }

    @DexIgnore
    public final x80 getActivityDetectionLatency() {
        return this.d;
    }

    @DexIgnore
    public final boolean getEnableAutoDetection() {
        return this.b;
    }

    @DexIgnore
    public final boolean getEnablePrompt() {
        return this.c;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Boolean.valueOf(this.b).hashCode();
        int hashCode2 = Boolean.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte((byte) this.a.getValue());
        }
        if (parcel != null) {
            parcel.writeInt(this.b ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public /* synthetic */ z80(Parcel parcel, zd7 zd7) {
        WorkoutType c2 = yz0.c(parcel.readByte());
        if (c2 != null) {
            this.a = c2;
            boolean z = false;
            this.b = parcel.readInt() == 1;
            this.c = parcel.readInt() == 1 ? true : z;
            Parcelable readParcelable = parcel.readParcelable(x80.class.getClassLoader());
            if (readParcelable != null) {
                this.d = (x80) readParcelable;
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }
}
