package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.xx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class vx1 implements xx1.a {
    @DexIgnore
    public static /* final */ vx1 a; // = new vx1();

    @DexIgnore
    public static xx1.a a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.xx1.a
    public void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE events ADD COLUMN payload_encoding TEXT");
    }
}
