package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bh4 {
    @DexIgnore
    public static /* final */ bh4 b; // = new zg4(null, 0, 0);
    @DexIgnore
    public /* final */ bh4 a;

    @DexIgnore
    public bh4(bh4 bh4) {
        this.a = bh4;
    }

    @DexIgnore
    public final bh4 a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(ch4 ch4, byte[] bArr);

    @DexIgnore
    public final bh4 b(int i, int i2) {
        return new wg4(this, i, i2);
    }

    @DexIgnore
    public final bh4 a(int i, int i2) {
        return new zg4(this, i, i2);
    }
}
