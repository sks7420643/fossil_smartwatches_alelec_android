package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vv1 {
    @DexIgnore
    public static pw1 a(Context context, sw1 sw1, dw1 dw1, by1 by1) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new bw1(context, sw1, dw1);
        }
        return new xv1(context, sw1, by1, dw1);
    }
}
