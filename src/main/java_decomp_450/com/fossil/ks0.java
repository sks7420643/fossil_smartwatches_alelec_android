package com.fossil;

import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks0 {
    @DexIgnore
    public /* synthetic */ ks0(zd7 zd7) {
    }

    @DexIgnore
    public final l60.d a(p91 p91) {
        int i = pq0.a[p91.ordinal()];
        if (i == 1) {
            return l60.d.DISCONNECTED;
        }
        if (i == 2) {
            return l60.d.CONNECTING;
        }
        if (i == 3) {
            return l60.d.CONNECTED;
        }
        if (i == 4) {
            return l60.d.DISCONNECTING;
        }
        throw new p87();
    }
}
