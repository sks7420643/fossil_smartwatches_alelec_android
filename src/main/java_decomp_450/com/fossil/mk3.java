package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wj3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 b;

    @DexIgnore
    public mk3(ek3 ek3, wj3 wj3) {
        this.b = ek3;
        this.a = wj3;
    }

    @DexIgnore
    public final void run() {
        bg3 d = this.b.d;
        if (d == null) {
            this.b.e().t().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.a == null) {
                d.a(0, (String) null, (String) null, this.b.f().getPackageName());
            } else {
                d.a(this.a.c, this.a.a, this.a.b, this.b.f().getPackageName());
            }
            this.b.J();
        } catch (RemoteException e) {
            this.b.e().t().a("Failed to send current screen to the service", e);
        }
    }
}
