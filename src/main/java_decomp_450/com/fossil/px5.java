package com.fossil;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px5 extends jx5 implements LoaderManager.a<SparseArray<List<? extends BaseFeatureModel>>> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ kx5 e;
    @DexIgnore
    public /* final */ NotificationsLoader f;
    @DexIgnore
    public /* final */ LoaderManager g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1", f = "NotificationDialLandingPresenter.kt", l = {33}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ px5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1$1", f = "NotificationDialLandingPresenter.kt", l = {34}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(px5 px5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = px5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = px5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationDialLandingP\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public px5(kx5 kx5, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        ee7.b(kx5, "mView");
        ee7.b(notificationsLoader, "mNotificationLoader");
        ee7.b(loaderManager, "mLoaderManager");
        this.e = kx5;
        this.f = notificationsLoader;
        this.g = loaderManager;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.oe<android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>>] */
    @Override // androidx.loader.app.LoaderManager.a
    public void a(oe<SparseArray<List<? extends BaseFeatureModel>>> oeVar) {
        ee7.b(oeVar, "loader");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(h, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!PortfolioApp.g0.c().v().U()) {
            ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
        }
        this.g.a(3, null, this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.g.a(3);
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.oe, java.lang.Object] */
    @Override // androidx.loader.app.LoaderManager.a
    public /* bridge */ /* synthetic */ void a(oe<SparseArray<List<? extends BaseFeatureModel>>> oeVar, SparseArray<List<? extends BaseFeatureModel>> sparseArray) {
        a((oe<SparseArray<List<BaseFeatureModel>>>) oeVar, (SparseArray<List<BaseFeatureModel>>) sparseArray);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.oe<android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>>' to match base method */
    @Override // androidx.loader.app.LoaderManager.a
    public oe<SparseArray<List<? extends BaseFeatureModel>>> a(int i, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "onCreateLoader id=" + i);
        return this.f;
    }

    @DexIgnore
    public void a(oe<SparseArray<List<BaseFeatureModel>>> oeVar, SparseArray<List<BaseFeatureModel>> sparseArray) {
        ee7.b(oeVar, "loader");
        if (sparseArray != null) {
            this.e.a(sparseArray);
        } else {
            FLogger.INSTANCE.getLocal().d(h, "onLoadFinished, data=null");
        }
    }
}
