package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ul4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts5 extends ho5 implements ss5, ro5, View.OnClickListener {
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<i25> g;
    @DexIgnore
    public rs5 h;
    @DexIgnore
    public ul4 i;
    @DexIgnore
    public ps5 j;
    @DexIgnore
    public kw5 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ts5 a() {
            return new ts5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ul4.b {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public b(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        @Override // com.fossil.ul4.b
        public void a(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            ts5.b(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        @Override // com.fossil.ul4.b
        public void b(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            ts5.b(this.a).a(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public c(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
                ee7.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public d(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersActivity.z.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public e(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            yx6.a(view);
            NotificationAppsActivity.z.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public f(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationCallsAndMessagesActivity.z.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public g(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ts5.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public h(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ps5 a2 = this.a.j;
            if (a2 != null) {
                a2.n(0);
            }
            ps5 a3 = this.a.j;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, ps5.t.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ts5 a;

        @DexIgnore
        public i(ts5 ts5) {
            this.a = ts5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ps5 a2 = this.a.j;
            if (a2 != null) {
                a2.n(1);
            }
            ps5 a3 = this.a.j;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, ps5.t.a());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ rs5 b(ts5 ts5) {
        rs5 rs5 = ts5.h;
        if (rs5 != null) {
            return rs5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void d(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            qw6<i25> qw6 = this.g;
            if (qw6 != null) {
                i25 a2 = qw6.a();
                if (a2 != null && (rTLImageView2 = a2.G) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<i25> qw62 = this.g;
        if (qw62 != null) {
            i25 a3 = qw62.a();
            if (a3 != null && (rTLImageView = a3.G) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HomeAlertsFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void f(List<Alarm> list) {
        ee7.b(list, "alarms");
        ul4 ul4 = this.i;
        if (ul4 != null) {
            ul4.a(list);
        } else {
            ee7.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void l(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "notificationAppOverView");
        qw6<i25> qw6 = this.g;
        if (qw6 != null) {
            i25 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.v) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
        if (valueOf != null && valueOf.intValue() == 2131361960) {
            rs5 rs5 = this.h;
            if (rs5 != null) {
                rs5.a(null);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else if (valueOf != null && valueOf.intValue() == 2131362736) {
            rs5 rs52 = this.h;
            if (rs52 != null) {
                rs52.h();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        i25 i25 = (i25) qb.a(layoutInflater, 2131558570, viewGroup, false, a1());
        ps5 ps5 = (ps5) getChildFragmentManager().b(ps5.t.a());
        this.j = ps5;
        if (ps5 == null) {
            this.j = ps5.t.b();
        }
        String b2 = eh5.l.a().b("onPrimaryButton");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230983);
            if (drawable != null) {
                drawable.setTint(parseColor);
                i25.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        i25.G.setOnClickListener(this);
        FlexibleButton flexibleButton = i25.q;
        ee7.a((Object) flexibleButton, "binding.btnAdd");
        bf5.a(flexibleButton, this);
        i25.M.setOnClickListener(new d(this));
        i25.I.setOnClickListener(new e(this));
        i25.J.setOnClickListener(new f(this));
        i25.P.setOnClickListener(new g(this));
        i25.L.setOnClickListener(new h(this));
        i25.K.setOnClickListener(new i(this));
        ua5 ua5 = i25.F;
        if (ua5 != null) {
            ConstraintLayout constraintLayout = ua5.q;
            ee7.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            String b3 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b3)) {
                ua5.q.setBackgroundColor(Color.parseColor(b3));
            }
            ua5.t.setImageResource(2131230818);
            FlexibleTextView flexibleTextView = ua5.r;
            ee7.a((Object) flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(ig5.a(getContext(), 2131886996));
            ua5.s.setOnClickListener(new c(this));
        }
        ul4 ul4 = new ul4();
        ul4.a(new b(this));
        this.i = ul4;
        RecyclerView recyclerView = i25.O;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        ul4 ul42 = this.i;
        if (ul42 != null) {
            recyclerView.setAdapter(ul42);
            this.g = new qw6<>(this, i25);
            tj4 f2 = PortfolioApp.g0.c().f();
            ps5 ps52 = this.j;
            if (ps52 != null) {
                f2.a(new vs5(ps52)).a(this);
                qw6<i25> qw6 = this.g;
                if (qw6 != null) {
                    i25 a2 = qw6.a();
                    if (a2 != null) {
                        ee7.a((Object) a2, "mBinding.get()!!");
                        return a2.d();
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimeContract.View");
        }
        ee7.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        rs5 rs5 = this.h;
        if (rs5 != null) {
            if (rs5 != null) {
                rs5.g();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.a("");
        }
        super.onPause();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        rs5 rs5 = this.h;
        if (rs5 != null) {
            if (rs5 != null) {
                rs5.f();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        V("alert_view");
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void r() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.I(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void t() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "notifyListAlarm()");
        ul4 ul4 = this.i;
        if (ul4 != null) {
            ul4.notifyDataSetChanged();
        } else {
            ee7.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void z() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.S(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void b(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        ee7.b(spannableString, LogBuilder.KEY_TIME);
        qw6<i25> qw6 = this.g;
        if (qw6 != null) {
            i25 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.D) != null) {
                flexibleTextView.setText(spannableString.toString());
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z) {
        if (z) {
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        jf5 c12 = c1();
        if (c12 != null) {
            c12.a("");
        }
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void z(boolean z) {
        Context context;
        int i2;
        qw6<i25> qw6 = this.g;
        if (qw6 != null) {
            i25 a2 = qw6.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.P;
                ee7.a((Object) switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                ConstraintLayout constraintLayout = a2.s;
                ee7.a((Object) constraintLayout, "it.clScheduledTimeContainer");
                constraintLayout.setVisibility(z ? 0 : 8);
                FlexibleTextView flexibleTextView = a2.z;
                if (z) {
                    context = requireContext();
                    i2 = 2131099972;
                } else {
                    context = requireContext();
                    i2 = 2131100359;
                }
                flexibleTextView.setTextColor(v6.a(context, i2));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(rs5 rs5) {
        ee7.b(rs5, "presenter");
        this.h = rs5;
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        ee7.b(str, "deviceId");
        ee7.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.z;
        Context requireContext = requireContext();
        ee7.a((Object) requireContext, "requireContext()");
        aVar.a(requireContext, str, arrayList, alarm);
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void a(boolean z) {
        ConstraintLayout constraintLayout;
        qw6<i25> qw6 = this.g;
        if (qw6 != null) {
            i25 a2 = qw6.a();
            if (a2 != null && (constraintLayout = a2.r) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ss5
    public void a(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        ee7.b(spannableString, LogBuilder.KEY_TIME);
        qw6<i25> qw6 = this.g;
        if (qw6 != null) {
            i25 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.B) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
