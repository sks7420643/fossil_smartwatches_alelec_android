package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rb0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public rb0 createFromParcel(Parcel parcel) {
            return new rb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public rb0[] newArray(int i) {
            return new rb0[i];
        }
    }

    @DexIgnore
    public rb0(byte b, int i, String str) {
        super(cb0.BUDDY_CHALLENGE_GET_INFO, b, i);
        this.d = str;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        return yz0.a(super.a(), r51.D4, this.d);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(rb0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.d, ((rb0) obj).d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.BuddyChallengeGetInfoRequest");
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ rb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
        } else {
            ee7.a();
            throw null;
        }
    }
}
