package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j43 implements tr2<m43> {
    @DexIgnore
    public static j43 b; // = new j43();
    @DexIgnore
    public /* final */ tr2<m43> a;

    @DexIgnore
    public j43(tr2<m43> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((m43) b.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ m43 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public j43() {
        this(sr2.a(new l43()));
    }
}
