package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub6 extends rb6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public /* final */ LiveData<qx6<List<GoalTrackingSummary>>> g;
    @DexIgnore
    public BarChart.c h;
    @DexIgnore
    public /* final */ sb6 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ ch5 k;
    @DexIgnore
    public /* final */ GoalTrackingRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter", f = "GoalTrackingOverviewWeekPresenter.kt", l = {148}, m = "calculateStartAndEndDate")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ub6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ub6 ub6, fb7 fb7) {
            super(fb7);
            this.this$0 = ub6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Date) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ub6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$mGoalTrackingSummaries$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {35, 39, 37}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<GoalTrackingSummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ub6$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$mGoalTrackingSummaries$1$1$startAndEnd$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {35}, m = "invokeSuspend")
            /* renamed from: com.fossil.ub6$c$a$a  reason: collision with other inner class name */
            public static final class C0192a extends zb7 implements kd7<yi7, fb7<? super r87<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0192a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0192a aVar = new C0192a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super r87<? extends Date, ? extends Date>> fb7) {
                    return ((C0192a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        a aVar = this.this$0;
                        ub6 ub6 = aVar.this$0.a;
                        Date date = aVar.$it;
                        ee7.a((Object) date, "it");
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = ub6.a(date, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<GoalTrackingSummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00c8 A[RETURN] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                    r9 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r9.label
                    r2 = 3
                    r3 = 2
                    r4 = 1
                    if (r1 == 0) goto L_0x003f
                    if (r1 == r4) goto L_0x0037
                    if (r1 == r3) goto L_0x0026
                    if (r1 != r2) goto L_0x001e
                    java.lang.Object r0 = r9.L$1
                    com.fossil.r87 r0 = (com.fossil.r87) r0
                    java.lang.Object r0 = r9.L$0
                    com.fossil.vd r0 = (com.fossil.vd) r0
                    com.fossil.t87.a(r10)
                    goto L_0x00c9
                L_0x001e:
                    java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r10.<init>(r0)
                    throw r10
                L_0x0026:
                    java.lang.Object r1 = r9.L$2
                    com.fossil.vd r1 = (com.fossil.vd) r1
                    java.lang.Object r3 = r9.L$1
                    com.fossil.r87 r3 = (com.fossil.r87) r3
                    java.lang.Object r4 = r9.L$0
                    com.fossil.vd r4 = (com.fossil.vd) r4
                    com.fossil.t87.a(r10)
                    goto L_0x00ba
                L_0x0037:
                    java.lang.Object r1 = r9.L$0
                    com.fossil.vd r1 = (com.fossil.vd) r1
                    com.fossil.t87.a(r10)
                    goto L_0x0060
                L_0x003f:
                    com.fossil.t87.a(r10)
                    com.fossil.vd r10 = r9.p$
                    com.fossil.ub6$c r1 = r9.this$0
                    com.fossil.ub6 r1 = r1.a
                    com.fossil.ti7 r1 = r1.b()
                    com.fossil.ub6$c$a$a r5 = new com.fossil.ub6$c$a$a
                    r6 = 0
                    r5.<init>(r9, r6)
                    r9.L$0 = r10
                    r9.label = r4
                    java.lang.Object r1 = com.fossil.vh7.a(r1, r5, r9)
                    if (r1 != r0) goto L_0x005d
                    return r0
                L_0x005d:
                    r8 = r1
                    r1 = r10
                    r10 = r8
                L_0x0060:
                    com.fossil.r87 r10 = (com.fossil.r87) r10
                    com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "mGoalTrackingSummaries onDateChanged - startDate="
                    r5.append(r6)
                    java.lang.Object r6 = r10.getFirst()
                    java.util.Date r6 = (java.util.Date) r6
                    r5.append(r6)
                    java.lang.String r6 = ", endDate="
                    r5.append(r6)
                    java.lang.Object r6 = r10.getSecond()
                    java.util.Date r6 = (java.util.Date) r6
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    java.lang.String r6 = "GoalTrackingOverviewWeekPresenter"
                    r4.d(r6, r5)
                    com.fossil.ub6$c r4 = r9.this$0
                    com.fossil.ub6 r4 = r4.a
                    com.portfolio.platform.data.source.GoalTrackingRepository r4 = r4.l
                    java.lang.Object r5 = r10.getFirst()
                    java.util.Date r5 = (java.util.Date) r5
                    java.lang.Object r6 = r10.getSecond()
                    java.util.Date r6 = (java.util.Date) r6
                    r7 = 0
                    r9.L$0 = r1
                    r9.L$1 = r10
                    r9.L$2 = r1
                    r9.label = r3
                    java.lang.Object r3 = r4.getSummaries(r5, r6, r7, r9)
                    if (r3 != r0) goto L_0x00b6
                    return r0
                L_0x00b6:
                    r4 = r1
                    r8 = r3
                    r3 = r10
                    r10 = r8
                L_0x00ba:
                    androidx.lifecycle.LiveData r10 = (androidx.lifecycle.LiveData) r10
                    r9.L$0 = r4
                    r9.L$1 = r3
                    r9.label = r2
                    java.lang.Object r10 = r1.a(r10, r9)
                    if (r10 != r0) goto L_0x00c9
                    return r0
                L_0x00c9:
                    com.fossil.i97 r10 = com.fossil.i97.a
                    return r10
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(ub6 ub6) {
            this.a = ub6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<GoalTrackingSummary>>> apply(Date date) {
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<qx6<? extends List<GoalTrackingSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ub6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {65}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ub6$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ub6$d$a$a  reason: collision with other inner class name */
            public static final class C0193a extends zb7 implements kd7<yi7, fb7<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0193a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0193a aVar = new C0193a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super BarChart.c> fb7) {
                    return ((C0193a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        ub6 ub6 = this.this$0.this$0.a;
                        Date c = ub6.e;
                        if (c != null) {
                            return ub6.a(c, this.this$0.$data);
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                ub6 ub6;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ub6 ub62 = this.this$0.a;
                    ti7 a2 = ub62.b();
                    C0193a aVar = new C0193a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = ub62;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                    ub6 = ub62;
                } else if (i == 1) {
                    ub6 = (ub6) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ub6.h = (BarChart.c) obj;
                sb6 f = this.this$0.a.i;
                BarChart.c b = this.this$0.a.h;
                if (b == null) {
                    b = new BarChart.c(0, 0, null, 7, null);
                }
                f.a((do5) b);
                return i97.a;
            }
        }

        @DexIgnore
        public d(ub6 ub6) {
            this.a = ub6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<GoalTrackingSummary>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingSummaries -- GoalTrackingSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, list, null), 3, null);
                this.a.i.b(!this.a.k.M());
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ub6(sb6 sb6, UserRepository userRepository, ch5 ch5, GoalTrackingRepository goalTrackingRepository) {
        ee7.b(sb6, "mView");
        ee7.b(userRepository, "userRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.i = sb6;
        this.j = userRepository;
        this.k = ch5;
        this.l = goalTrackingRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<qx6<List<GoalTrackingSummary>>> b2 = ge.b(mutableLiveData, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = b2;
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        Date date = this.e;
        if (date == null || !zd5.w(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.a(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewWeekPresenter", "start with date " + this.e);
        LiveData<qx6<List<GoalTrackingSummary>>> liveData = this.g;
        sb6 sb6 = this.i;
        if (sb6 != null) {
            liveData.a((tb6) sb6, new d(this));
            this.i.b(!this.k.M());
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "stop");
        try {
            LiveData<qx6<List<GoalTrackingSummary>>> liveData = this.g;
            sb6 sb6 = this.i;
            if (sb6 != null) {
                liveData.a((tb6) sb6);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<GoalTrackingSummary> list) {
        int i2;
        T t;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
        Calendar instance = Calendar.getInstance(Locale.US);
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            ee7.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (zd5.d(t.getDate(), instance.getTime())) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    i5 = t2.getGoalTarget();
                    int totalTracked = t2.getTotalTracked();
                    i4 = Math.max(Math.max(i5, totalTracked), i4);
                    cVar.a().add(new BarChart.a(i5, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, totalTracked, null, 23, null)})}), time2, i3 == 6));
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, 0, null, 23, null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, 0, null, 23, null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 <= 0) {
            i4 = i5;
        }
        cVar.b(i4);
        return cVar;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.util.Date r5, com.fossil.fb7<? super com.fossil.r87<? extends java.util.Date, ? extends java.util.Date>> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.fossil.ub6.b
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.ub6$b r0 = (com.fossil.ub6.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ub6$b r0 = new com.fossil.ub6$b
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r5 = r0.L$2
            com.fossil.se7 r5 = (com.fossil.se7) r5
            java.lang.Object r1 = r0.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.ub6 r0 = (com.fossil.ub6) r0
            com.fossil.t87.a(r6)
            goto L_0x0060
        L_0x0035:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003d:
            com.fossil.t87.a(r6)
            com.fossil.se7 r6 = new com.fossil.se7
            r6.<init>()
            r2 = 6
            java.util.Date r2 = com.fossil.zd5.b(r5, r2)
            r6.element = r2
            com.portfolio.platform.data.source.UserRepository r2 = r4.j
            r0.L$0 = r4
            r0.L$1 = r5
            r0.L$2 = r6
            r0.label = r3
            java.lang.Object r0 = r2.getCurrentUser(r0)
            if (r0 != r1) goto L_0x005d
            return r1
        L_0x005d:
            r1 = r5
            r5 = r6
            r6 = r0
        L_0x0060:
            com.portfolio.platform.data.model.MFUser r6 = (com.portfolio.platform.data.model.MFUser) r6
            if (r6 == 0) goto L_0x0080
            java.lang.String r6 = r6.getCreatedAt()
            if (r6 == 0) goto L_0x007b
            java.util.Date r6 = com.fossil.zd5.d(r6)
            T r0 = r5.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.fossil.zd5.b(r0, r6)
            if (r0 != 0) goto L_0x0080
            r5.element = r6
            goto L_0x0080
        L_0x007b:
            com.fossil.ee7.a()
            r5 = 0
            throw r5
        L_0x0080:
            com.fossil.r87 r6 = new com.fossil.r87
            T r5 = r5.element
            java.util.Date r5 = (java.util.Date) r5
            r6.<init>(r5, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub6.a(java.util.Date, com.fossil.fb7):java.lang.Object");
    }
}
