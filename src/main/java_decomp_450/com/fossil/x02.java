package com.fossil;

import android.text.TextUtils;
import com.fossil.v02;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x02 extends Exception {
    @DexIgnore
    public /* final */ n4<p12<?>, i02> zaba;

    @DexIgnore
    public x02(n4<p12<?>, i02> n4Var) {
        this.zaba = n4Var;
    }

    @DexIgnore
    public i02 getConnectionResult(z02<? extends v02.d> z02) {
        p12<? extends v02.d> a = z02.a();
        boolean z = this.zaba.get(a) != null;
        String a2 = a.a();
        StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 58);
        sb.append("The given API (");
        sb.append(a2);
        sb.append(") was not part of the availability request.");
        a72.a(z, sb.toString());
        return this.zaba.get(a);
    }

    @DexIgnore
    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (p12<?> p12 : this.zaba.keySet()) {
            i02 i02 = this.zaba.get(p12);
            if (i02.x()) {
                z = false;
            }
            String a = p12.a();
            String valueOf = String.valueOf(i02);
            StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 2 + String.valueOf(valueOf).length());
            sb.append(a);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    @DexIgnore
    public final n4<p12<?>, i02> zaj() {
        return this.zaba;
    }

    @DexIgnore
    public i02 getConnectionResult(b12<? extends v02.d> b12) {
        p12<? extends v02.d> a = b12.a();
        boolean z = this.zaba.get(a) != null;
        String a2 = a.a();
        StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 58);
        sb.append("The given API (");
        sb.append(a2);
        sb.append(") was not part of the availability request.");
        a72.a(z, sb.toString());
        return this.zaba.get(a);
    }
}
