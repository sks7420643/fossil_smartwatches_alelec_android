package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ ProgressBar u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public e95(Object obj, View view, int i, ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, View view2, ProgressBar progressBar, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = imageView;
        this.s = imageView2;
        this.t = view2;
        this.u = progressBar;
        this.v = flexibleTextView;
    }

    @DexIgnore
    public static e95 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static e95 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (e95) ViewDataBinding.a(layoutInflater, 2131558684, viewGroup, z, obj);
    }
}
