package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs2<E> extends os2<E> {
    @DexIgnore
    public /* final */ transient os2<E> c;

    @DexIgnore
    public qs2(os2<E> os2) {
        this.c = os2;
    }

    @DexIgnore
    @Override // com.fossil.os2, com.fossil.ps2
    public final boolean contains(@NullableDecl Object obj) {
        return this.c.contains(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        or2.a(i, size());
        return this.c.get(zza(i));
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final int indexOf(@NullableDecl Object obj) {
        int lastIndexOf = this.c.lastIndexOf(obj);
        if (lastIndexOf >= 0) {
            return zza(lastIndexOf);
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final int lastIndexOf(@NullableDecl Object obj) {
        int indexOf = this.c.indexOf(obj);
        if (indexOf >= 0) {
            return zza(indexOf);
        }
        return -1;
    }

    @DexIgnore
    public final int size() {
        return this.c.size();
    }

    @DexIgnore
    public final int zza(int i) {
        return (size() - 1) - i;
    }

    @DexIgnore
    @Override // com.fossil.os2
    public final os2<E> zzd() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return this.c.zzh();
    }

    @DexIgnore
    @Override // com.fossil.os2
    /* renamed from: zza */
    public final os2<E> subList(int i, int i2) {
        or2.a(i, i2, size());
        return ((os2) this.c.subList(size() - i2, size() - i)).zzd();
    }
}
