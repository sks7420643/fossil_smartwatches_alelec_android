package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<u93> CREATOR; // = new ia3();
    @DexIgnore
    public static /* final */ u93 b; // = new u93(0);
    @DexIgnore
    public /* final */ int a;

    /*
    static {
        new u93(1);
    }
    */

    @DexIgnore
    public u93(int i) {
        this.a = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof u93) && this.a == ((u93) obj).a;
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(Integer.valueOf(this.a));
    }

    @DexIgnore
    public final String toString() {
        String str;
        int i = this.a;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", Integer.valueOf(i));
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", str);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a);
        k72.a(parcel, a2);
    }
}
