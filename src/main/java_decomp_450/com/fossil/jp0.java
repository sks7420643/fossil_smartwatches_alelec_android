package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp0 extends tj0 {
    @DexIgnore
    public ArrayList<bi1> X;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jp0(short s, ri1 ri1, int i, int i2) {
        super(ln0.c, s, qa1.u, ri1, (i2 & 4) != 0 ? 3 : i);
        this.X = new ArrayList<>();
    }

    @DexIgnore
    public final void c(byte[] bArr) {
        int i = 0;
        while (true) {
            int i2 = i + 10;
            if (i2 <= bArr.length) {
                ByteBuffer order = ByteBuffer.wrap(s97.a(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN);
                this.X.add(new bi1(((v81) this).y.u, order.getShort(0), yz0.b(order.getInt(2)), yz0.b(order.getInt(6))));
                i = i2;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        JSONObject h = super.h();
        r51 r51 = r51.u1;
        Object[] array = this.X.toArray(new bi1[0]);
        if (array != null) {
            return yz0.a(h, r51, yz0.a((k60[]) array));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.tj0
    public void u() {
        c(((tj0) this).N);
    }
}
