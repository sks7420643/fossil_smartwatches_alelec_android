package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum jm0 {
    CLIENT_CHARACTERISTIC_CONFIGURATION,
    UNKNOWN;

    @DexIgnore
    public final UUID a() {
        if (mk0.a[ordinal()] != 1) {
            return null;
        }
        return b21.x.a();
    }
}
