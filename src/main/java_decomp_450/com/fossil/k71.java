package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k71 implements Parcelable.Creator<g91> {
    @DexIgnore
    public /* synthetic */ k71(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public g91 createFromParcel(Parcel parcel) {
        return new g91(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public g91[] newArray(int i) {
        return new g91[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public g91 m32createFromParcel(Parcel parcel) {
        return new g91(parcel, null);
    }
}
