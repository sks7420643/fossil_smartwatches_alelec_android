package com.fossil;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import com.fossil.i17;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;

public class r07 extends i17 {
    public static final UriMatcher b;
    public final Context a;

    @TargetApi(14)
    public static class a {
        public static InputStream a(ContentResolver contentResolver, Uri uri) {
            return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
        }
    }

    /*
    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        b = uriMatcher;
        uriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        b.addURI("com.android.contacts", "contacts/lookup/*", 1);
        b.addURI("com.android.contacts", "contacts/#/photo", 2);
        b.addURI("com.android.contacts", "contacts/#", 3);
        b.addURI("com.android.contacts", "display_photo/#", 4);
    }
    */

    public r07(Context context) {
        this.a = context;
    }

    @Override // com.fossil.i17
    public boolean a(g17 g17) {
        Uri uri = g17.d;
        return "content".equals(uri.getScheme()) && ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) && b.match(g17.d) != -1;
    }

    public final InputStream c(g17 g17) throws IOException {
        ContentResolver contentResolver = this.a.getContentResolver();
        Uri uri = g17.d;
        int match = b.match(uri);
        if (match != 1) {
            if (match != 2) {
                if (match != 3) {
                    if (match != 4) {
                        throw new IllegalStateException("Invalid uri: " + uri);
                    }
                }
            }
            return contentResolver.openInputStream(uri);
        }
        uri = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (uri == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 14) {
            return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri);
        }
        return a.a(contentResolver, uri);
    }

    @Override // com.fossil.i17
    public i17.a a(g17 g17, int i) throws IOException {
        InputStream c = c(g17);
        if (c != null) {
            return new i17.a(c, Picasso.LoadedFrom.DISK);
        }
        return null;
    }
}
