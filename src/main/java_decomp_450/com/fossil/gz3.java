package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz3<T> extends jz3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ jz3<? super T> ordering;

    @DexIgnore
    public gz3(jz3<? super T> jz3) {
        this.ordering = jz3;
    }

    @DexIgnore
    @Override // com.fossil.jz3, java.util.Comparator
    public int compare(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return this.ordering.compare(t, t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof gz3) {
            return this.ordering.equals(((gz3) obj).ordering);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.ordering.hashCode() ^ 957692532;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends T> jz3<S> nullsFirst() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends T> jz3<S> nullsLast() {
        return this.ordering.nullsLast();
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends T> jz3<S> reverse() {
        return this.ordering.reverse().nullsLast();
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".nullsFirst()";
    }
}
