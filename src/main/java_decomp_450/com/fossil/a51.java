package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a51 extends qj1 {
    @DexIgnore
    public long A; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public m01 B; // = m01.BOND_NONE;

    @DexIgnore
    public a51(ri1 ri1) {
        super(qa1.Y, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.m1, yz0.a(((v81) this).y.getBondState()));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.B0, yz0.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new xy0(((v81) this).y.w);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        this.B = ((xy0) eo0).j;
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(new JSONObject(), r51.B0, yz0.a(this.B)), 7));
    }
}
