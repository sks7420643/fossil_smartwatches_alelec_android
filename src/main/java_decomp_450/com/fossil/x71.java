package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x71 extends k60 {
    @DexIgnore
    public static /* final */ i21 c; // = new i21(null);
    @DexIgnore
    public /* final */ z51 a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public x71(z51 z51, int i) {
        this.a = z51;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.M0, yz0.a(this.a));
            if (this.b != 0) {
                yz0.a(jSONObject, r51.i4, Integer.valueOf(this.b));
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ x71(z51 z51, int i, int i2) {
        i = (i2 & 2) != 0 ? 0 : i;
        this.a = z51;
        this.b = i;
    }
}
