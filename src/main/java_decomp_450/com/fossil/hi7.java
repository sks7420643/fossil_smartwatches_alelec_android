package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hi7 extends kk7<pk7> implements gi7 {
    @DexIgnore
    public /* final */ ii7 e;

    @DexIgnore
    public hi7(pk7 pk7, ii7 ii7) {
        super(pk7);
        this.e = ii7;
    }

    @DexIgnore
    @Override // com.fossil.gi7
    public boolean a(Throwable th) {
        return ((pk7) ((ok7) this).d).d(th);
    }

    @DexIgnore
    @Override // com.fossil.pi7
    public void b(Throwable th) {
        this.e.a((xk7) ((ok7) this).d);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        b(th);
        return i97.a;
    }

    @DexIgnore
    @Override // com.fossil.bm7
    public String toString() {
        return "ChildHandle[" + this.e + ']';
    }
}
