package com.fossil;

import android.content.Context;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ld4 implements Callable {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public /* final */ FirebaseInstanceId c;
    @DexIgnore
    public /* final */ ta4 d;
    @DexIgnore
    public /* final */ ga4 e;

    @DexIgnore
    public ld4(Context context, ScheduledExecutorService scheduledExecutorService, FirebaseInstanceId firebaseInstanceId, ta4 ta4, ga4 ga4) {
        this.a = context;
        this.b = scheduledExecutorService;
        this.c = firebaseInstanceId;
        this.d = ta4;
        this.e = ga4;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return md4.a(this.a, this.b, this.c, this.d, this.e);
    }
}
