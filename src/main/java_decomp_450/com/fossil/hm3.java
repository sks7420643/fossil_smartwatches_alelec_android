package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm3 implements Parcelable.Creator<em3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ em3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    str = j72.e(parcel, a);
                    break;
                case 3:
                    j = j72.s(parcel, a);
                    break;
                case 4:
                    l = j72.t(parcel, a);
                    break;
                case 5:
                    f = j72.o(parcel, a);
                    break;
                case 6:
                    str2 = j72.e(parcel, a);
                    break;
                case 7:
                    str3 = j72.e(parcel, a);
                    break;
                case 8:
                    d = j72.m(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new em3(i, str, j, l, f, str2, str3, d);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ em3[] newArray(int i) {
        return new em3[i];
    }
}
