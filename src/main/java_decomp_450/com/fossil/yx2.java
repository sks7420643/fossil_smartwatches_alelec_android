package com.fossil;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx2 {
    @DexIgnore
    public static /* final */ yx2 c; // = new yx2();
    @DexIgnore
    public /* final */ by2 a; // = new ax2();
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, cy2<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static yx2 a() {
        return c;
    }

    @DexIgnore
    public final <T> cy2<T> a(Class<T> cls) {
        ew2.a((Object) cls, "messageType");
        cy2<T> cy2 = (cy2<T>) this.b.get(cls);
        if (cy2 != null) {
            return cy2;
        }
        cy2<T> zza = this.a.zza(cls);
        ew2.a((Object) cls, "messageType");
        ew2.a((Object) zza, "schema");
        cy2<T> cy22 = (cy2<T>) this.b.putIfAbsent(cls, zza);
        return cy22 != null ? cy22 : zza;
    }

    @DexIgnore
    public final <T> cy2<T> a(T t) {
        return a((Class) t.getClass());
    }
}
