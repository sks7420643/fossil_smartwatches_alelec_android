package com.fossil;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iu3 {
    @DexIgnore
    public /* final */ TextPaint a; // = new TextPaint(1);
    @DexIgnore
    public /* final */ su3 b; // = new a();
    @DexIgnore
    public float c;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public WeakReference<b> e; // = new WeakReference<>(null);
    @DexIgnore
    public qu3 f;

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        int[] getState();

        @DexIgnore
        boolean onStateChange(int[] iArr);
    }

    @DexIgnore
    public iu3(b bVar) {
        a(bVar);
    }

    @DexIgnore
    public TextPaint b() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends su3 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.su3
        public void a(Typeface typeface, boolean z) {
            if (!z) {
                boolean unused = iu3.this.d = true;
                b bVar = (b) iu3.this.e.get();
                if (bVar != null) {
                    bVar.a();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.su3
        public void a(int i) {
            boolean unused = iu3.this.d = true;
            b bVar = (b) iu3.this.e.get();
            if (bVar != null) {
                bVar.a();
            }
        }
    }

    @DexIgnore
    public void a(b bVar) {
        this.e = new WeakReference<>(bVar);
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public float a(String str) {
        if (!this.d) {
            return this.c;
        }
        float a2 = a((CharSequence) str);
        this.c = a2;
        this.d = false;
        return a2;
    }

    @DexIgnore
    public final float a(CharSequence charSequence) {
        return charSequence == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.a.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public qu3 a() {
        return this.f;
    }

    @DexIgnore
    public void a(qu3 qu3, Context context) {
        if (this.f != qu3) {
            this.f = qu3;
            if (qu3 != null) {
                qu3.c(context, this.a, this.b);
                b bVar = this.e.get();
                if (bVar != null) {
                    this.a.drawableState = bVar.getState();
                }
                qu3.b(context, this.a, this.b);
                this.d = true;
            }
            b bVar2 = this.e.get();
            if (bVar2 != null) {
                bVar2.a();
                bVar2.onStateChange(bVar2.getState());
            }
        }
    }

    @DexIgnore
    public void a(Context context) {
        this.f.b(context, this.a, this.b);
    }
}
