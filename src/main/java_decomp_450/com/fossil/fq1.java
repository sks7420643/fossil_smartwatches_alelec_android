package com.fossil;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq1 {
    @DexIgnore
    public static /* final */ fq1 a; // = new fq1();

    @DexIgnore
    public final byte[] a(gm1 gm1, fo1 fo1, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        Cipher instance = Cipher.getInstance(fo1.a);
        instance.init(gm1.a, secretKeySpec, new IvParameterSpec(bArr2));
        byte[] doFinal = instance.doFinal(bArr3);
        ee7.a((Object) doFinal, "cipher.doFinal(data)");
        return doFinal;
    }

    @DexIgnore
    public final byte[] a(fo1 fo1, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        return a(gm1.DECRYPT, fo1, bArr, bArr2, bArr3);
    }
}
