package com.fossil;

import com.fossil.el7;
import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi7 extends cb7 implements el7<String> {
    @DexIgnore
    public static /* final */ a b; // = new a(null);
    @DexIgnore
    public /* final */ long a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ib7.c<wi7> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public wi7(long j) {
        super(b);
        this.a = j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof wi7) && this.a == ((wi7) obj).a;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.cb7
    public <R> R fold(R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
        return (R) el7.a.a(this, r, kd7);
    }

    @DexIgnore
    public final long g() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.ib7.b, com.fossil.cb7
    public <E extends ib7.b> E get(ib7.c<E> cVar) {
        return (E) el7.a.a(this, cVar);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.cb7
    public ib7 minusKey(ib7.c<?> cVar) {
        return el7.a.b(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.cb7
    public ib7 plus(ib7 ib7) {
        return el7.a.a(this, ib7);
    }

    @DexIgnore
    public String toString() {
        return "CoroutineId(" + this.a + ')';
    }

    @DexIgnore
    @Override // com.fossil.el7
    public String a(ib7 ib7) {
        String str;
        xi7 xi7 = (xi7) ib7.get(xi7.b);
        if (xi7 == null || (str = xi7.g()) == null) {
            str = "coroutine";
        }
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        int b2 = nh7.b((CharSequence) name, " @", 0, false, 6, (Object) null);
        if (b2 < 0) {
            b2 = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + b2 + 10);
        if (name != null) {
            String substring = name.substring(0, b2);
            ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(" @");
            sb.append(str);
            sb.append('#');
            sb.append(this.a);
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
            currentThread.setName(sb2);
            return name;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public void a(ib7 ib7, String str) {
        Thread.currentThread().setName(str);
    }
}
