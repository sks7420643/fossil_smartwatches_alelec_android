package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface os {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static void a(os osVar, Bitmap bitmap) {
            Object tag = osVar.b().getView().getTag(qq.coil_bitmap);
            if (!(tag instanceof Bitmap)) {
                tag = null;
            }
            Bitmap bitmap2 = (Bitmap) tag;
            if (bitmap2 != null) {
                osVar.a().a(bitmap2);
            }
            osVar.b().getView().setTag(qq.coil_bitmap, bitmap);
        }

        @DexIgnore
        public static void b(os osVar, Bitmap bitmap) {
            if (bitmap != null) {
                osVar.a().b(bitmap);
            }
        }
    }

    @DexIgnore
    ds a();

    @DexIgnore
    void a(Bitmap bitmap);

    @DexIgnore
    ut<?> b();

    @DexIgnore
    void b(Bitmap bitmap);
}
