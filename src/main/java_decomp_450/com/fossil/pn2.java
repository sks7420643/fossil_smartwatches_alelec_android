package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn2 extends ln2 implements nn2 {
    @DexIgnore
    public pn2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    @DexIgnore
    @Override // com.fossil.nn2
    public final void a(String str, String str2, Bundle bundle, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, bundle);
        E.writeLong(j);
        b(1, E);
    }

    @DexIgnore
    @Override // com.fossil.nn2
    public final int zza() throws RemoteException {
        Parcel a = a(2, E());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
