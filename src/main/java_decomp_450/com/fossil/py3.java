package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends lx3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;
        @DexIgnore
        public /* final */ /* synthetic */ kw3 c;

        @DexIgnore
        public a(Iterable iterable, kw3 kw3) {
            this.b = iterable;
            this.c = kw3;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return qy3.c(this.b.iterator(), this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends lx3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;
        @DexIgnore
        public /* final */ /* synthetic */ cw3 c;

        @DexIgnore
        public b(Iterable iterable, cw3 cw3) {
            this.b = iterable;
            this.c = cw3;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return qy3.a(this.b.iterator(), this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends lx3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public c(List list, int i) {
            this.b = list;
            this.c = i;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            int min = Math.min(this.b.size(), this.c);
            List list = this.b;
            return list.subList(min, list.size()).iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends lx3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<T> {
            @DexIgnore
            public boolean a; // = true;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator b;

            @DexIgnore
            public a(d dVar, Iterator it) {
                this.b = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public T next() {
                T t = (T) this.b.next();
                this.a = false;
                return t;
            }

            @DexIgnore
            public void remove() {
                bx3.a(!this.a);
                this.b.remove();
            }
        }

        @DexIgnore
        public d(Iterable iterable, int i) {
            this.b = iterable;
            this.c = i;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            Iterator it = this.b.iterator();
            qy3.a(it, this.c);
            return new a(this, it);
        }
    }

    @DexIgnore
    public static <T> T[] a(Iterable<? extends T> iterable, T[] tArr) {
        return (T[]) a(iterable).toArray(tArr);
    }

    @DexIgnore
    public static <T> T b(Iterable<T> iterable) {
        return (T) qy3.b(iterable.iterator());
    }

    @DexIgnore
    public static Object[] c(Iterable<?> iterable) {
        return a(iterable).toArray();
    }

    @DexIgnore
    public static String d(Iterable<?> iterable) {
        return qy3.d(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> b(Iterable<T> iterable, kw3<? super T> kw3) {
        jw3.a(iterable);
        jw3.a(kw3);
        return new a(iterable, kw3);
    }

    @DexIgnore
    public static <E> Collection<E> a(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : uy3.a(iterable.iterator());
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> boolean a(Collection<T> collection, Iterable<? extends T> iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll(cx3.a(iterable));
        }
        jw3.a(iterable);
        return qy3.a(collection, iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Iterable<T> iterable, kw3<? super T> kw3) {
        return qy3.a(iterable.iterator(), kw3);
    }

    @DexIgnore
    public static <F, T> Iterable<T> a(Iterable<F> iterable, cw3<? super F, ? extends T> cw3) {
        jw3.a(iterable);
        jw3.a(cw3);
        return new b(iterable, cw3);
    }

    @DexIgnore
    public static <T> T a(Iterable<? extends T> iterable, T t) {
        return (T) qy3.b((Iterator) iterable.iterator(), (Object) t);
    }

    @DexIgnore
    public static <T> Iterable<T> a(Iterable<T> iterable, int i) {
        jw3.a(iterable);
        jw3.a(i >= 0, "number to skip cannot be negative");
        if (iterable instanceof List) {
            return new c((List) iterable, i);
        }
        return new d(iterable, i);
    }
}
