package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ci3 implements Callable<byte[]> {
    @DexIgnore
    public /* final */ /* synthetic */ ub3 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ ph3 c;

    @DexIgnore
    public ci3(ph3 ph3, ub3 ub3, String str) {
        this.c = ph3;
        this.a = ub3;
        this.b = str;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ byte[] call() throws Exception {
        this.c.a.s();
        this.c.a.m().a(this.a, this.b);
        throw null;
    }
}
