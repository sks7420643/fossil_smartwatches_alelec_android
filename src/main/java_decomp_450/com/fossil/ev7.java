package com.fossil;

import com.fossil.bv7;
import com.fossil.fo7;
import com.fossil.io7;
import com.fossil.lo7;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit.RestMethodInfo;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev7 {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ go7 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ fo7 e;
    @DexIgnore
    public /* final */ ho7 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ bv7<?>[] j;
    @DexIgnore
    public /* final */ boolean k;

    @DexIgnore
    public ev7(a aVar) {
        this.a = aVar.b;
        this.b = aVar.a.c;
        this.c = aVar.n;
        this.d = aVar.r;
        this.e = aVar.s;
        this.f = aVar.t;
        this.g = aVar.o;
        this.h = aVar.p;
        this.i = aVar.q;
        this.j = aVar.v;
        this.k = aVar.w;
    }

    @DexIgnore
    public static ev7 a(Retrofit retrofit3, Method method) {
        return new a(retrofit3, method).a();
    }

    @DexIgnore
    public lo7 a(Object[] objArr) throws IOException {
        bv7<?>[] bv7Arr = this.j;
        int length = objArr.length;
        if (length == bv7Arr.length) {
            dv7 dv7 = new dv7(this.c, this.b, this.d, this.e, this.f, this.g, this.h, this.i);
            if (this.k) {
                length--;
            }
            ArrayList arrayList = new ArrayList(length);
            for (int i2 = 0; i2 < length; i2++) {
                arrayList.add(objArr[i2]);
                bv7Arr[i2].a(dv7, objArr[i2]);
            }
            lo7.a a2 = dv7.a();
            a2.a(xu7.class, new xu7(this.a, arrayList));
            return a2.a();
        }
        throw new IllegalArgumentException("Argument count (" + length + ") doesn't match expected count (" + bv7Arr.length + ")");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Pattern x; // = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
        @DexIgnore
        public static /* final */ Pattern y; // = Pattern.compile(RestMethodInfo.PARAM);
        @DexIgnore
        public /* final */ Retrofit a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Annotation[] c;
        @DexIgnore
        public /* final */ Annotation[][] d;
        @DexIgnore
        public /* final */ Type[] e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q;
        @DexIgnore
        public String r;
        @DexIgnore
        public fo7 s;
        @DexIgnore
        public ho7 t;
        @DexIgnore
        public Set<String> u;
        @DexIgnore
        public bv7<?>[] v;
        @DexIgnore
        public boolean w;

        @DexIgnore
        public a(Retrofit retrofit3, Method method) {
            this.a = retrofit3;
            this.b = method;
            this.c = method.getAnnotations();
            this.e = method.getGenericParameterTypes();
            this.d = method.getParameterAnnotations();
        }

        @DexIgnore
        public ev7 a() {
            for (Annotation annotation : this.c) {
                a(annotation);
            }
            if (this.n != null) {
                if (!this.o) {
                    if (this.q) {
                        throw jv7.a(this.b, "Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    } else if (this.p) {
                        throw jv7.a(this.b, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    }
                }
                int length = this.d.length;
                this.v = new bv7[length];
                int i2 = length - 1;
                int i3 = 0;
                while (true) {
                    boolean z = true;
                    if (i3 >= length) {
                        break;
                    }
                    bv7<?>[] bv7Arr = this.v;
                    Type type = this.e[i3];
                    Annotation[] annotationArr = this.d[i3];
                    if (i3 != i2) {
                        z = false;
                    }
                    bv7Arr[i3] = a(i3, type, annotationArr, z);
                    i3++;
                }
                if (this.r == null && !this.m) {
                    throw jv7.a(this.b, "Missing either @%s URL or @Url parameter.", this.n);
                } else if (!this.p && !this.q && !this.o && this.h) {
                    throw jv7.a(this.b, "Non-body HTTP method cannot contain @Body.", new Object[0]);
                } else if (this.p && !this.f) {
                    throw jv7.a(this.b, "Form-encoded method must contain at least one @Field.", new Object[0]);
                } else if (!this.q || this.g) {
                    return new ev7(this);
                } else {
                    throw jv7.a(this.b, "Multipart method must contain at least one @Part.", new Object[0]);
                }
            } else {
                throw jv7.a(this.b, "HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
            }
        }

        @DexIgnore
        public final void a(Annotation annotation) {
            if (annotation instanceof yv7) {
                a("DELETE", ((yv7) annotation).value(), false);
            } else if (annotation instanceof bw7) {
                a("GET", ((bw7) annotation).value(), false);
            } else if (annotation instanceof cw7) {
                a("HEAD", ((cw7) annotation).value(), false);
            } else if (annotation instanceof hw7) {
                a("PATCH", ((hw7) annotation).value(), true);
            } else if (annotation instanceof iw7) {
                a("POST", ((iw7) annotation).value(), true);
            } else if (annotation instanceof jw7) {
                a("PUT", ((jw7) annotation).value(), true);
            } else if (annotation instanceof gw7) {
                a("OPTIONS", ((gw7) annotation).value(), false);
            } else if (annotation instanceof dw7) {
                dw7 dw7 = (dw7) annotation;
                a(dw7.method(), dw7.path(), dw7.hasBody());
            } else if (annotation instanceof fw7) {
                String[] value = ((fw7) annotation).value();
                if (value.length != 0) {
                    this.s = a(value);
                    return;
                }
                throw jv7.a(this.b, "@Headers annotation is empty.", new Object[0]);
            }
        }

        @DexIgnore
        public final void a(String str, String str2, boolean z) {
            String str3 = this.n;
            if (str3 == null) {
                this.n = str;
                this.o = z;
                if (!str2.isEmpty()) {
                    int indexOf = str2.indexOf(63);
                    if (indexOf != -1 && indexOf < str2.length() - 1) {
                        String substring = str2.substring(indexOf + 1);
                        if (x.matcher(substring).find()) {
                            throw jv7.a(this.b, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                        }
                    }
                    this.r = str2;
                    this.u = a(str2);
                    return;
                }
                return;
            }
            throw jv7.a(this.b, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
        }

        @DexIgnore
        public final fo7 a(String[] strArr) {
            fo7.a aVar = new fo7.a();
            for (String str : strArr) {
                int indexOf = str.indexOf(58);
                if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                    throw jv7.a(this.b, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                }
                String substring = str.substring(0, indexOf);
                String trim = str.substring(indexOf + 1).trim();
                if ("Content-Type".equalsIgnoreCase(substring)) {
                    try {
                        this.t = ho7.a(trim);
                    } catch (IllegalArgumentException e2) {
                        throw jv7.a(this.b, e2, "Malformed content type: %s", trim);
                    }
                } else {
                    aVar.a(substring, trim);
                }
            }
            return aVar.a();
        }

        @DexIgnore
        public final bv7<?> a(int i2, Type type, Annotation[] annotationArr, boolean z) {
            bv7<?> bv7;
            if (annotationArr != null) {
                bv7 = null;
                for (Annotation annotation : annotationArr) {
                    bv7<?> a2 = a(i2, type, annotationArr, annotation);
                    if (a2 != null) {
                        if (bv7 == null) {
                            bv7 = a2;
                        } else {
                            throw jv7.a(this.b, i2, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                        }
                    }
                }
            } else {
                bv7 = null;
            }
            if (bv7 != null) {
                return bv7;
            }
            if (z) {
                try {
                    if (jv7.b(type) == fb7.class) {
                        this.w = true;
                        return null;
                    }
                } catch (NoClassDefFoundError unused) {
                }
            }
            throw jv7.a(this.b, i2, "No Retrofit annotation found.", new Object[0]);
        }

        @DexIgnore
        public final bv7<?> a(int i2, Type type, Annotation[] annotationArr, Annotation annotation) {
            if (annotation instanceof rw7) {
                a(i2, type);
                if (this.m) {
                    throw jv7.a(this.b, i2, "Multiple @Url method annotations found.", new Object[0]);
                } else if (this.i) {
                    throw jv7.a(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.j) {
                    throw jv7.a(this.b, i2, "A @Url parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw jv7.a(this.b, i2, "A @Url parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw jv7.a(this.b, i2, "A @Url parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.r == null) {
                    this.m = true;
                    if (type == go7.class || type == String.class || type == URI.class || ((type instanceof Class) && "android.net.Uri".equals(((Class) type).getName()))) {
                        return new bv7.n(this.b, i2);
                    }
                    throw jv7.a(this.b, i2, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                } else {
                    throw jv7.a(this.b, i2, "@Url cannot be used with @%s URL", this.n);
                }
            } else if (annotation instanceof mw7) {
                a(i2, type);
                if (this.j) {
                    throw jv7.a(this.b, i2, "A @Path parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw jv7.a(this.b, i2, "A @Path parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw jv7.a(this.b, i2, "A @Path parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.m) {
                    throw jv7.a(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.r != null) {
                    this.i = true;
                    mw7 mw7 = (mw7) annotation;
                    String value = mw7.value();
                    a(i2, value);
                    return new bv7.i(this.b, i2, value, this.a.c(type, annotationArr), mw7.encoded());
                } else {
                    throw jv7.a(this.b, i2, "@Path can only be used with relative url on @%s", this.n);
                }
            } else if (annotation instanceof nw7) {
                a(i2, type);
                nw7 nw7 = (nw7) annotation;
                String value2 = nw7.value();
                boolean encoded = nw7.encoded();
                Class<?> b2 = jv7.b(type);
                this.j = true;
                if (Iterable.class.isAssignableFrom(b2)) {
                    if (type instanceof ParameterizedType) {
                        return new bv7.j(value2, this.a.c(jv7.b(0, (ParameterizedType) type), annotationArr), encoded).b();
                    }
                    Method method = this.b;
                    throw jv7.a(method, i2, b2.getSimpleName() + " must include generic type (e.g., " + b2.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b2.isArray()) {
                    return new bv7.j(value2, this.a.c(type, annotationArr), encoded);
                } else {
                    return new bv7.j(value2, this.a.c(a(b2.getComponentType()), annotationArr), encoded).a();
                }
            } else if (annotation instanceof pw7) {
                a(i2, type);
                boolean encoded2 = ((pw7) annotation).encoded();
                Class<?> b3 = jv7.b(type);
                this.k = true;
                if (Iterable.class.isAssignableFrom(b3)) {
                    if (type instanceof ParameterizedType) {
                        return new bv7.l(this.a.c(jv7.b(0, (ParameterizedType) type), annotationArr), encoded2).b();
                    }
                    Method method2 = this.b;
                    throw jv7.a(method2, i2, b3.getSimpleName() + " must include generic type (e.g., " + b3.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b3.isArray()) {
                    return new bv7.l(this.a.c(type, annotationArr), encoded2);
                } else {
                    return new bv7.l(this.a.c(a(b3.getComponentType()), annotationArr), encoded2).a();
                }
            } else if (annotation instanceof ow7) {
                a(i2, type);
                Class<?> b4 = jv7.b(type);
                this.l = true;
                if (Map.class.isAssignableFrom(b4)) {
                    Type b5 = jv7.b(type, b4, Map.class);
                    if (b5 instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) b5;
                        Type b6 = jv7.b(0, parameterizedType);
                        if (String.class == b6) {
                            return new bv7.k(this.b, i2, this.a.c(jv7.b(1, parameterizedType), annotationArr), ((ow7) annotation).encoded());
                        }
                        Method method3 = this.b;
                        throw jv7.a(method3, i2, "@QueryMap keys must be of type String: " + b6, new Object[0]);
                    }
                    throw jv7.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                throw jv7.a(this.b, i2, "@QueryMap parameter type must be Map.", new Object[0]);
            } else if (annotation instanceof ew7) {
                a(i2, type);
                String value3 = ((ew7) annotation).value();
                Class<?> b7 = jv7.b(type);
                if (Iterable.class.isAssignableFrom(b7)) {
                    if (type instanceof ParameterizedType) {
                        return new bv7.f(value3, this.a.c(jv7.b(0, (ParameterizedType) type), annotationArr)).b();
                    }
                    Method method4 = this.b;
                    throw jv7.a(method4, i2, b7.getSimpleName() + " must include generic type (e.g., " + b7.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b7.isArray()) {
                    return new bv7.f(value3, this.a.c(type, annotationArr));
                } else {
                    return new bv7.f(value3, this.a.c(a(b7.getComponentType()), annotationArr)).a();
                }
            } else if (annotation instanceof zv7) {
                a(i2, type);
                if (this.p) {
                    zv7 zv7 = (zv7) annotation;
                    String value4 = zv7.value();
                    boolean encoded3 = zv7.encoded();
                    this.f = true;
                    Class<?> b8 = jv7.b(type);
                    if (Iterable.class.isAssignableFrom(b8)) {
                        if (type instanceof ParameterizedType) {
                            return new bv7.d(value4, this.a.c(jv7.b(0, (ParameterizedType) type), annotationArr), encoded3).b();
                        }
                        Method method5 = this.b;
                        throw jv7.a(method5, i2, b8.getSimpleName() + " must include generic type (e.g., " + b8.getSimpleName() + "<String>)", new Object[0]);
                    } else if (!b8.isArray()) {
                        return new bv7.d(value4, this.a.c(type, annotationArr), encoded3);
                    } else {
                        return new bv7.d(value4, this.a.c(a(b8.getComponentType()), annotationArr), encoded3).a();
                    }
                } else {
                    throw jv7.a(this.b, i2, "@Field parameters can only be used with form encoding.", new Object[0]);
                }
            } else if (annotation instanceof aw7) {
                a(i2, type);
                if (this.p) {
                    Class<?> b9 = jv7.b(type);
                    if (Map.class.isAssignableFrom(b9)) {
                        Type b10 = jv7.b(type, b9, Map.class);
                        if (b10 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType2 = (ParameterizedType) b10;
                            Type b11 = jv7.b(0, parameterizedType2);
                            if (String.class == b11) {
                                tu7 c2 = this.a.c(jv7.b(1, parameterizedType2), annotationArr);
                                this.f = true;
                                return new bv7.e(this.b, i2, c2, ((aw7) annotation).encoded());
                            }
                            Method method6 = this.b;
                            throw jv7.a(method6, i2, "@FieldMap keys must be of type String: " + b11, new Object[0]);
                        }
                        throw jv7.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw jv7.a(this.b, i2, "@FieldMap parameter type must be Map.", new Object[0]);
                }
                throw jv7.a(this.b, i2, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
            } else if (annotation instanceof kw7) {
                a(i2, type);
                if (this.q) {
                    kw7 kw7 = (kw7) annotation;
                    this.g = true;
                    String value5 = kw7.value();
                    Class<?> b12 = jv7.b(type);
                    if (!value5.isEmpty()) {
                        fo7 a2 = fo7.a("Content-Disposition", "form-data; name=\"" + value5 + "\"", "Content-Transfer-Encoding", kw7.encoding());
                        if (Iterable.class.isAssignableFrom(b12)) {
                            if (type instanceof ParameterizedType) {
                                Type b13 = jv7.b(0, (ParameterizedType) type);
                                if (!io7.b.class.isAssignableFrom(jv7.b(b13))) {
                                    return new bv7.g(this.b, i2, a2, this.a.a(b13, annotationArr, this.c)).b();
                                }
                                throw jv7.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                            }
                            Method method7 = this.b;
                            throw jv7.a(method7, i2, b12.getSimpleName() + " must include generic type (e.g., " + b12.getSimpleName() + "<String>)", new Object[0]);
                        } else if (b12.isArray()) {
                            Class<?> a3 = a(b12.getComponentType());
                            if (!io7.b.class.isAssignableFrom(a3)) {
                                return new bv7.g(this.b, i2, a2, this.a.a(a3, annotationArr, this.c)).a();
                            }
                            throw jv7.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        } else if (!io7.b.class.isAssignableFrom(b12)) {
                            return new bv7.g(this.b, i2, a2, this.a.a(type, annotationArr, this.c));
                        } else {
                            throw jv7.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                    } else if (Iterable.class.isAssignableFrom(b12)) {
                        if (!(type instanceof ParameterizedType)) {
                            Method method8 = this.b;
                            throw jv7.a(method8, i2, b12.getSimpleName() + " must include generic type (e.g., " + b12.getSimpleName() + "<String>)", new Object[0]);
                        } else if (io7.b.class.isAssignableFrom(jv7.b(jv7.b(0, (ParameterizedType) type)))) {
                            return bv7.m.a.b();
                        } else {
                            throw jv7.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                        }
                    } else if (b12.isArray()) {
                        if (io7.b.class.isAssignableFrom(b12.getComponentType())) {
                            return bv7.m.a.a();
                        }
                        throw jv7.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    } else if (io7.b.class.isAssignableFrom(b12)) {
                        return bv7.m.a;
                    } else {
                        throw jv7.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    }
                } else {
                    throw jv7.a(this.b, i2, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                }
            } else if (annotation instanceof lw7) {
                a(i2, type);
                if (this.q) {
                    this.g = true;
                    Class<?> b14 = jv7.b(type);
                    if (Map.class.isAssignableFrom(b14)) {
                        Type b15 = jv7.b(type, b14, Map.class);
                        if (b15 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType3 = (ParameterizedType) b15;
                            Type b16 = jv7.b(0, parameterizedType3);
                            if (String.class == b16) {
                                Type b17 = jv7.b(1, parameterizedType3);
                                if (!io7.b.class.isAssignableFrom(jv7.b(b17))) {
                                    return new bv7.h(this.b, i2, this.a.a(b17, annotationArr, this.c), ((lw7) annotation).encoding());
                                }
                                throw jv7.a(this.b, i2, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                            }
                            Method method9 = this.b;
                            throw jv7.a(method9, i2, "@PartMap keys must be of type String: " + b16, new Object[0]);
                        }
                        throw jv7.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw jv7.a(this.b, i2, "@PartMap parameter type must be Map.", new Object[0]);
                }
                throw jv7.a(this.b, i2, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
            } else if (!(annotation instanceof xv7)) {
                return null;
            } else {
                a(i2, type);
                if (this.p || this.q) {
                    throw jv7.a(this.b, i2, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                } else if (!this.h) {
                    try {
                        tu7 a4 = this.a.a(type, annotationArr, this.c);
                        this.h = true;
                        return new bv7.c(this.b, i2, a4);
                    } catch (RuntimeException e2) {
                        throw jv7.a(this.b, e2, i2, "Unable to create @Body converter for %s", type);
                    }
                } else {
                    throw jv7.a(this.b, i2, "Multiple @Body method annotations found.", new Object[0]);
                }
            }
        }

        @DexIgnore
        public final void a(int i2, Type type) {
            if (jv7.c(type)) {
                throw jv7.a(this.b, i2, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }

        @DexIgnore
        public final void a(int i2, String str) {
            if (!y.matcher(str).matches()) {
                throw jv7.a(this.b, i2, "@Path parameter name must match %s. Found: %s", x.pattern(), str);
            } else if (!this.u.contains(str)) {
                throw jv7.a(this.b, i2, "URL \"%s\" does not contain \"{%s}\".", this.r, str);
            }
        }

        @DexIgnore
        public static Set<String> a(String str) {
            Matcher matcher = x.matcher(str);
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            while (matcher.find()) {
                linkedHashSet.add(matcher.group(1));
            }
            return linkedHashSet;
        }

        @DexIgnore
        public static Class<?> a(Class<?> cls) {
            if (Boolean.TYPE == cls) {
                return Boolean.class;
            }
            if (Byte.TYPE == cls) {
                return Byte.class;
            }
            if (Character.TYPE == cls) {
                return Character.class;
            }
            if (Double.TYPE == cls) {
                return Double.class;
            }
            if (Float.TYPE == cls) {
                return Float.class;
            }
            if (Integer.TYPE == cls) {
                return Integer.class;
            }
            if (Long.TYPE == cls) {
                return Long.class;
            }
            return Short.TYPE == cls ? Short.class : cls;
        }
    }
}
