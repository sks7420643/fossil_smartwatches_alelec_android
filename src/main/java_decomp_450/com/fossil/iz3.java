package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz3 {
    @DexIgnore
    public static /* final */ Object[] a; // = new Object[0];

    @DexIgnore
    public static <T> T[] a(T[] tArr, int i) {
        T[] tArr2 = (T[]) c(tArr, i);
        System.arraycopy(tArr, 0, tArr2, 0, Math.min(tArr.length, i));
        return tArr2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object[] b(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            a(objArr[i2], i2);
        }
        return objArr;
    }

    @DexIgnore
    public static <T> T[] c(T[] tArr, int i) {
        return (T[]) lz3.a(tArr, i);
    }

    @DexIgnore
    public static <T> T[] a(Collection<?> collection, T[] tArr) {
        T[] tArr2;
        int size = collection.size();
        if (tArr.length < size) {
            tArr = (T[]) c(tArr, size);
        }
        a((Iterable<?>) collection, (Object[]) tArr2);
        if (tArr2.length > size) {
            tArr2[size] = null;
        }
        return tArr2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object[] a(Iterable<?> iterable, Object[] objArr) {
        Iterator<?> it = iterable.iterator();
        int i = 0;
        while (it.hasNext()) {
            objArr[i] = it.next();
            i++;
        }
        return objArr;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object[] a(Object... objArr) {
        b(objArr, objArr.length);
        return objArr;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static Object a(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException("at index " + i);
    }
}
