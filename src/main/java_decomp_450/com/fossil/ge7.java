package com.fossil;

import com.fossil.ag7;
import com.fossil.xf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ge7 extends je7 implements xf7 {
    @DexIgnore
    public ge7() {
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public sf7 computeReflected() {
        te7.a(this);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ag7
    public Object getDelegate() {
        return ((xf7) getReflected()).getDelegate();
    }

    @DexIgnore
    @Override // com.fossil.vc7
    public Object invoke() {
        return get();
    }

    @DexIgnore
    public ge7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.ag7
    public ag7.a getGetter() {
        return ((xf7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.xf7
    public xf7.a getSetter() {
        return ((xf7) getReflected()).getSetter();
    }
}
