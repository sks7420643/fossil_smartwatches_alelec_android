package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fz2 {
    @DexIgnore
    public abstract int a(int i, byte[] bArr, int i2, int i3);

    @DexIgnore
    public abstract int a(CharSequence charSequence, byte[] bArr, int i, int i2);

    @DexIgnore
    public abstract String a(byte[] bArr, int i, int i2) throws iw2;

    @DexIgnore
    public final boolean b(byte[] bArr, int i, int i2) {
        return a(0, bArr, i, i2) == 0;
    }
}
