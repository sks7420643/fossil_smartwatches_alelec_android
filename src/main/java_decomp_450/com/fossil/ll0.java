package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ll0 extends qj1 {
    @DexIgnore
    public String A; // = "";

    @DexIgnore
    public ll0(ri1 ri1) {
        super(qa1.c0, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        this.A = new String(((y51) eo0).l, sg7.a);
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(new JSONObject(), r51.d4, this.A), 7));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.d4, this.A);
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new y51(qk1.SOFTWARE_REVISION, ((v81) this).y.w);
    }
}
