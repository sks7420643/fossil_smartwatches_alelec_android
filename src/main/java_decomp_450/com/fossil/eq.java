package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eq<TResult> {
    @DexIgnore
    public static /* final */ Executor i; // = aq.b();
    @DexIgnore
    public static volatile g j;
    @DexIgnore
    public static eq<?> k; // = new eq<>((Object) null);
    @DexIgnore
    public static eq<Boolean> l; // = new eq<>((Boolean) true);
    @DexIgnore
    public static eq<Boolean> m; // = new eq<>((Boolean) false);
    @DexIgnore
    public static eq<?> n; // = new eq<>(true);
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public TResult d;
    @DexIgnore
    public Exception e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public gq g;
    @DexIgnore
    public List<cq<TResult, Void>> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements cq<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ fq a;
        @DexIgnore
        public /* final */ /* synthetic */ cq b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ bq d;

        @DexIgnore
        public a(eq eqVar, fq fqVar, cq cqVar, Executor executor, bq bqVar) {
            this.a = fqVar;
            this.b = cqVar;
            this.c = executor;
            this.d = bqVar;
        }

        @DexIgnore
        @Override // com.fossil.cq
        public Void then(eq<TResult> eqVar) {
            eq.d(this.a, this.b, eqVar, this.c, this.d);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements cq<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ fq a;
        @DexIgnore
        public /* final */ /* synthetic */ cq b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ bq d;

        @DexIgnore
        public b(eq eqVar, fq fqVar, cq cqVar, Executor executor, bq bqVar) {
            this.a = fqVar;
            this.b = cqVar;
            this.c = executor;
            this.d = bqVar;
        }

        @DexIgnore
        @Override // com.fossil.cq
        public Void then(eq<TResult> eqVar) {
            eq.c(this.a, this.b, eqVar, this.c, this.d);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements cq<TResult, eq<TContinuationResult>> {
        @DexIgnore
        public /* final */ /* synthetic */ bq a;
        @DexIgnore
        public /* final */ /* synthetic */ cq b;

        @DexIgnore
        public c(eq eqVar, bq bqVar, cq cqVar) {
            this.a = bqVar;
            this.b = cqVar;
        }

        @DexIgnore
        @Override // com.fossil.cq
        public eq<TContinuationResult> then(eq<TResult> eqVar) {
            bq bqVar = this.a;
            if (bqVar != null) {
                bqVar.a();
                throw null;
            } else if (eqVar.e()) {
                return eq.b(eqVar.a());
            } else {
                if (eqVar.c()) {
                    return eq.h();
                }
                return eqVar.a((cq) this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ bq a;
        @DexIgnore
        public /* final */ /* synthetic */ fq b;
        @DexIgnore
        public /* final */ /* synthetic */ cq c;
        @DexIgnore
        public /* final */ /* synthetic */ eq d;

        @DexIgnore
        public d(bq bqVar, fq fqVar, cq cqVar, eq eqVar) {
            this.a = bqVar;
            this.b = fqVar;
            this.c = cqVar;
            this.d = eqVar;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: com.fossil.fq */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            bq bqVar = this.a;
            if (bqVar == null) {
                try {
                    this.b.a(this.c.then(this.d));
                } catch (CancellationException unused) {
                    this.b.b();
                } catch (Exception e) {
                    this.b.a(e);
                }
            } else {
                bqVar.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ bq a;
        @DexIgnore
        public /* final */ /* synthetic */ fq b;
        @DexIgnore
        public /* final */ /* synthetic */ cq c;
        @DexIgnore
        public /* final */ /* synthetic */ eq d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements cq<TContinuationResult, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.cq
            public Void then(eq<TContinuationResult> eqVar) {
                bq bqVar = e.this.a;
                if (bqVar == null) {
                    if (eqVar.c()) {
                        e.this.b.b();
                    } else if (eqVar.e()) {
                        e.this.b.a(eqVar.a());
                    } else {
                        e.this.b.a((Object) eqVar.b());
                    }
                    return null;
                }
                bqVar.a();
                throw null;
            }
        }

        @DexIgnore
        public e(bq bqVar, fq fqVar, cq cqVar, eq eqVar) {
            this.a = bqVar;
            this.b = fqVar;
            this.c = cqVar;
            this.d = eqVar;
        }

        @DexIgnore
        public void run() {
            bq bqVar = this.a;
            if (bqVar == null) {
                try {
                    eq eqVar = (eq) this.c.then(this.d);
                    if (eqVar == null) {
                        this.b.a((Object) null);
                    } else {
                        eqVar.a((cq) new a());
                    }
                } catch (CancellationException unused) {
                    this.b.b();
                } catch (Exception e) {
                    this.b.a(e);
                }
            } else {
                bqVar.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends fq<TResult> {
        @DexIgnore
        public f(eq eqVar) {
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(eq<?> eqVar, hq hqVar);
    }

    /*
    static {
        aq.a();
        xp.b();
    }
    */

    @DexIgnore
    public eq() {
    }

    @DexIgnore
    public static <TResult> eq<TResult> h() {
        return (eq<TResult>) n;
    }

    @DexIgnore
    public static <TResult> eq<TResult>.f i() {
        eq eqVar = new eq();
        eqVar.getClass();
        return new f(eqVar);
    }

    @DexIgnore
    public static g j() {
        return j;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.b;
        }
        return z;
    }

    @DexIgnore
    public boolean e() {
        boolean z;
        synchronized (this.a) {
            z = a() != null;
        }
        return z;
    }

    @DexIgnore
    public final void f() {
        synchronized (this.a) {
            for (cq<TResult, Void> cqVar : this.h) {
                try {
                    cqVar.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    @DexIgnore
    public boolean g() {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public Exception a() {
        Exception exc;
        synchronized (this.a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    @DexIgnore
    public TResult b() {
        TResult tresult;
        synchronized (this.a) {
            tresult = this.d;
        }
        return tresult;
    }

    @DexIgnore
    public eq(TResult tresult) {
        a((Object) tresult);
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void d(fq<TContinuationResult> fqVar, cq<TResult, TContinuationResult> cqVar, eq<TResult> eqVar, Executor executor, bq bqVar) {
        try {
            executor.execute(new d(bqVar, fqVar, cqVar, eqVar));
        } catch (Exception e2) {
            fqVar.a(new dq(e2));
        }
    }

    @DexIgnore
    public <TContinuationResult> eq<TContinuationResult> c(cq<TResult, TContinuationResult> cqVar, Executor executor, bq bqVar) {
        return a(new c(this, bqVar, cqVar), executor);
    }

    @DexIgnore
    public static <TResult> eq<TResult> b(TResult tresult) {
        if (tresult == null) {
            return (eq<TResult>) k;
        }
        if (tresult instanceof Boolean) {
            return tresult.booleanValue() ? (eq<TResult>) l : (eq<TResult>) m;
        }
        fq fqVar = new fq();
        fqVar.a((Object) tresult);
        return fqVar.a();
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void c(fq<TContinuationResult> fqVar, cq<TResult, eq<TContinuationResult>> cqVar, eq<TResult> eqVar, Executor executor, bq bqVar) {
        try {
            executor.execute(new e(bqVar, fqVar, cqVar, eqVar));
        } catch (Exception e2) {
            fqVar.a(new dq(e2));
        }
    }

    @DexIgnore
    public eq(boolean z) {
        if (z) {
            g();
        } else {
            a((Object) null);
        }
    }

    @DexIgnore
    public <TContinuationResult> eq<TContinuationResult> a(cq<TResult, TContinuationResult> cqVar, Executor executor, bq bqVar) {
        boolean d2;
        fq fqVar = new fq();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new a(this, fqVar, cqVar, executor, bqVar));
            }
        }
        if (d2) {
            d(fqVar, cqVar, this, executor, bqVar);
        }
        return fqVar.a();
    }

    @DexIgnore
    public static <TResult> eq<TResult> b(Exception exc) {
        fq fqVar = new fq();
        fqVar.a(exc);
        return fqVar.a();
    }

    @DexIgnore
    public <TContinuationResult> eq<TContinuationResult> b(cq<TResult, eq<TContinuationResult>> cqVar, Executor executor, bq bqVar) {
        boolean d2;
        fq fqVar = new fq();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new b(this, fqVar, cqVar, executor, bqVar));
            }
        }
        if (d2) {
            c(fqVar, cqVar, this, executor, bqVar);
        }
        return fqVar.a();
    }

    @DexIgnore
    public <TContinuationResult> eq<TContinuationResult> a(cq<TResult, TContinuationResult> cqVar) {
        return a(cqVar, i, null);
    }

    @DexIgnore
    public <TContinuationResult> eq<TContinuationResult> a(cq<TResult, eq<TContinuationResult>> cqVar, Executor executor) {
        return b(cqVar, executor, null);
    }

    @DexIgnore
    public boolean a(TResult tresult) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public <TContinuationResult> eq<TContinuationResult> b(cq<TResult, TContinuationResult> cqVar) {
        return c(cqVar, i, null);
    }

    @DexIgnore
    public boolean a(Exception exc) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.a.notifyAll();
            f();
            if (!this.f && j() != null) {
                this.g = new gq(this);
            }
            return true;
        }
    }
}
