package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wj3 a;
    @DexIgnore
    public /* final */ /* synthetic */ wj3 b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ zj3 e;

    @DexIgnore
    public bk3(zj3 zj3, wj3 wj3, wj3 wj32, long j, boolean z) {
        this.e = zj3;
        this.a = wj3;
        this.b = wj32;
        this.c = j;
        this.d = z;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.a, this.b, this.c, this.d, (Bundle) null);
    }
}
