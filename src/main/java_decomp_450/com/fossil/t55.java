package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t55 extends s55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ ScrollView y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362074, 1);
        B.put(2131361851, 2);
        B.put(2131362517, 3);
        B.put(2131362379, 4);
        B.put(2131362380, 5);
        B.put(2131361948, 6);
        B.put(2131362381, 7);
        B.put(2131361943, 8);
    }
    */

    @DexIgnore
    public t55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public t55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[2], (FlexibleButton) objArr[8], (FlexibleButton) objArr[6], (ConstraintLayout) objArr[1], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[3]);
        this.z = -1;
        ScrollView scrollView = (ScrollView) objArr[0];
        this.y = scrollView;
        scrollView.setTag(null);
        a(view);
        f();
    }
}
