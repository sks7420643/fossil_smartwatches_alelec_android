package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g23 implements tr2<j23> {
    @DexIgnore
    public static g23 b; // = new g23();
    @DexIgnore
    public /* final */ tr2<j23> a;

    @DexIgnore
    public g23(tr2<j23> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((j23) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((j23) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((j23) b.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((j23) b.zza()).zzd();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ j23 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public g23() {
        this(sr2.a(new i23()));
    }
}
