package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz implements zy<int[]> {
    @DexIgnore
    @Override // com.fossil.zy
    public int a() {
        return 4;
    }

    @DexIgnore
    @Override // com.fossil.zy
    public String getTag() {
        return "IntegerArrayPool";
    }

    @DexIgnore
    public int a(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    @Override // com.fossil.zy
    public int[] newArray(int i) {
        return new int[i];
    }
}
