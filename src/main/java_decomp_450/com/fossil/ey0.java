package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ey0 extends uh1 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ qk1 I;
    @DexIgnore
    public /* final */ qk1 J;
    @DexIgnore
    public /* final */ byte[] K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ey0(uh0 uh0, qa1 qa1, ri1 ri1, int i, int i2) {
        super(qa1, ri1, (i2 & 8) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(uh0.c.length + 2).put(uh0.a.a()).put(uh0.b.a()).put(uh0.c).array();
        ee7.a((Object) array, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(uh0.f.length + 2).put(uh0.d.a()).put(uh0.e.a()).put(uh0.f).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.H = array2;
        qk1 qk1 = qk1.DC;
        this.I = qk1;
        this.J = qk1;
        this.K = new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final long a(rk1 rk1) {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final mw0 a(byte b) {
        return pl0.f.a(b);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 l() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final byte[] n() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 o() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final byte[] q() {
        return this.H;
    }
}
