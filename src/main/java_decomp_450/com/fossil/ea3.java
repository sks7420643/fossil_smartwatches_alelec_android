package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ea3 implements Parcelable.Creator<n93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ n93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        int i2 = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    arrayList2 = j72.c(parcel, a, LatLng.CREATOR);
                    break;
                case 3:
                    j72.a(parcel, a, arrayList, ea3.class.getClassLoader());
                    break;
                case 4:
                    f = j72.n(parcel, a);
                    break;
                case 5:
                    i = j72.q(parcel, a);
                    break;
                case 6:
                    i2 = j72.q(parcel, a);
                    break;
                case 7:
                    f2 = j72.n(parcel, a);
                    break;
                case 8:
                    z = j72.i(parcel, a);
                    break;
                case 9:
                    z2 = j72.i(parcel, a);
                    break;
                case 10:
                    z3 = j72.i(parcel, a);
                    break;
                case 11:
                    i3 = j72.q(parcel, a);
                    break;
                case 12:
                    arrayList3 = j72.c(parcel, a, l93.CREATOR);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new n93(arrayList2, arrayList, f, i, i2, f2, z, z2, z3, i3, arrayList3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ n93[] newArray(int i) {
        return new n93[i];
    }
}
