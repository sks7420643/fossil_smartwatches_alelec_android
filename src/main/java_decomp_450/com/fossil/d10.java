package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class d10<T> implements cx<ImageDecoder.Source, T> {
    @DexIgnore
    public /* final */ x10 a; // = x10.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ImageDecoder.OnHeaderDecodedListener {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ tw d;
        @DexIgnore
        public /* final */ /* synthetic */ r10 e;
        @DexIgnore
        public /* final */ /* synthetic */ bx f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d10$a$a")
        /* renamed from: com.fossil.d10$a$a  reason: collision with other inner class name */
        public class C0033a implements ImageDecoder.OnPartialImageListener {
            @DexIgnore
            public C0033a(a aVar) {
            }

            @DexIgnore
            public boolean onPartialImage(ImageDecoder.DecodeException decodeException) {
                return false;
            }
        }

        @DexIgnore
        public a(int i, int i2, boolean z, tw twVar, r10 r10, bx bxVar) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.d = twVar;
            this.e = r10;
            this.f = bxVar;
        }

        @DexIgnore
        @SuppressLint({"Override"})
        public void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
            boolean z = false;
            if (d10.this.a.a(this.a, this.b, this.c, false)) {
                imageDecoder.setAllocator(3);
            } else {
                imageDecoder.setAllocator(1);
            }
            if (this.d == tw.PREFER_RGB_565) {
                imageDecoder.setMemorySizePolicy(0);
            }
            imageDecoder.setOnPartialImageListener(new C0033a(this));
            Size size = imageInfo.getSize();
            int i = this.a;
            if (i == Integer.MIN_VALUE) {
                i = size.getWidth();
            }
            int i2 = this.b;
            if (i2 == Integer.MIN_VALUE) {
                i2 = size.getHeight();
            }
            float b2 = this.e.b(size.getWidth(), size.getHeight(), i, i2);
            int round = Math.round(((float) size.getWidth()) * b2);
            int round2 = Math.round(((float) size.getHeight()) * b2);
            if (Log.isLoggable("ImageDecoder", 2)) {
                Log.v("ImageDecoder", "Resizing from [" + size.getWidth() + "x" + size.getHeight() + "] to [" + round + "x" + round2 + "] scaleFactor: " + b2);
            }
            imageDecoder.setTargetSize(round, round2);
            int i3 = Build.VERSION.SDK_INT;
            if (i3 >= 28) {
                if (this.f == bx.DISPLAY_P3 && imageInfo.getColorSpace() != null && imageInfo.getColorSpace().isWideGamut()) {
                    z = true;
                }
                imageDecoder.setTargetColorSpace(ColorSpace.get(z ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB));
            } else if (i3 >= 26) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
            }
        }
    }

    @DexIgnore
    public abstract uy<T> a(ImageDecoder.Source source, int i, int i2, ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws IOException;

    @DexIgnore
    public final boolean a(ImageDecoder.Source source, ax axVar) {
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r13v0, resolved type: com.fossil.ax */
    /* JADX WARN: Multi-variable type inference failed */
    public final uy<T> a(ImageDecoder.Source source, int i, int i2, ax axVar) throws IOException {
        return a(source, i, i2, new a(i, i2, axVar.a(s10.i) != null && ((Boolean) axVar.a(s10.i)).booleanValue(), (tw) axVar.a(s10.f), (r10) axVar.a(r10.f), (bx) axVar.a(s10.g)));
    }
}
