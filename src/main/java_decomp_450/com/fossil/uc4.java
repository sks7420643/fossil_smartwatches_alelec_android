package com.fossil;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.fossil.jb4;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnwrappedWakefulBroadcastReceiver"})
public abstract class uc4 extends Service {
    @DexIgnore
    public /* final */ ExecutorService a; // = vc4.a();
    @DexIgnore
    public Binder b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements jb4.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.jb4.a
        public no3<Void> a(Intent intent) {
            return uc4.this.e(intent);
        }
    }

    @DexIgnore
    public final void a(Intent intent) {
        if (intent != null) {
            hb4.a(intent);
        }
        synchronized (this.c) {
            int i = this.e - 1;
            this.e = i;
            if (i == 0) {
                a(this.d);
            }
        }
    }

    @DexIgnore
    public Intent b(Intent intent) {
        return intent;
    }

    @DexIgnore
    public abstract void c(Intent intent);

    @DexIgnore
    public boolean d(Intent intent) {
        return false;
    }

    @DexIgnore
    public final no3<Void> e(Intent intent) {
        if (d(intent)) {
            return qo3.a((Object) null);
        }
        oo3 oo3 = new oo3();
        this.a.execute(new rc4(this, intent, oo3));
        return oo3.a();
    }

    @DexIgnore
    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.b == null) {
            this.b = new jb4(new a());
        }
        return this.b;
    }

    @DexIgnore
    public void onDestroy() {
        this.a.shutdown();
        super.onDestroy();
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.c) {
            this.d = i2;
            this.e++;
        }
        Intent b2 = b(intent);
        if (b2 == null) {
            a(intent);
            return 2;
        }
        no3<Void> e2 = e(b2);
        if (e2.d()) {
            a(intent);
            return 2;
        }
        e2.a(sc4.a, new tc4(this, intent));
        return 3;
    }

    @DexIgnore
    public boolean a(int i) {
        return stopSelfResult(i);
    }

    @DexIgnore
    public final /* synthetic */ void a(Intent intent, no3 no3) {
        a(intent);
    }

    @DexIgnore
    public final /* synthetic */ void a(Intent intent, oo3 oo3) {
        try {
            c(intent);
        } finally {
            oo3.a((Object) null);
        }
    }
}
