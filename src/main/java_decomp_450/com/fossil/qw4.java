package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qw4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ w75 s;
    @DexIgnore
    public /* final */ w75 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public qw4(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, w75 w75, w75 w752, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = w75;
        a((ViewDataBinding) w75);
        this.t = w752;
        a((ViewDataBinding) w752);
        this.u = linearLayout;
    }
}
