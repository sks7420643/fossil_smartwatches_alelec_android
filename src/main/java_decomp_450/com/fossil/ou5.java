package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ou5 extends dy6 implements nu5 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public /* final */ pb j; // = new pm4(this);
    @DexIgnore
    public qw6<g45> p;
    @DexIgnore
    public mu5 q;
    @DexIgnore
    public rj4 r;
    @DexIgnore
    public ju5 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ou5.u;
        }

        @DexIgnore
        public final ou5 b() {
            return new ou5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ou5 a;

        @DexIgnore
        public b(ou5 ou5) {
            this.a = ou5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ou5.a(this.a).a(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ou5 a;

        @DexIgnore
        public c(ou5 ou5) {
            this.a = ou5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ou5.a(this.a).a(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ou5 a;

        @DexIgnore
        public d(ou5 ou5) {
            this.a = ou5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ou5.a(this.a).a(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ou5 a;

        @DexIgnore
        public e(ou5 ou5) {
            this.a = ou5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.dismissAllowingStateLoss();
        }
    }

    /*
    static {
        String simpleName = ou5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationSettingsType\u2026nt::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ mu5 a(ou5 ou5) {
        mu5 mu5 = ou5.q;
        if (mu5 != null) {
            return mu5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void b1() {
        qw6<g45> qw6 = this.p;
        if (qw6 != null) {
            g45 a2 = qw6.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.w;
                ee7.a((Object) rTLImageView, "it.ivEveryoneCheck");
                rTLImageView.setVisibility(4);
                RTLImageView rTLImageView2 = a2.x;
                ee7.a((Object) rTLImageView2, "it.ivFavoriteContactsCheck");
                rTLImageView2.setVisibility(4);
                RTLImageView rTLImageView3 = a2.y;
                ee7.a((Object) rTLImageView3, "it.ivNoOneCheck");
                rTLImageView3.setVisibility(4);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nu5
    public void i(int i) {
        b1();
        qw6<g45> qw6 = this.p;
        if (qw6 != null) {
            g45 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (i == 0) {
                RTLImageView rTLImageView = a2.w;
                ee7.a((Object) rTLImageView, "it.ivEveryoneCheck");
                rTLImageView.setVisibility(0);
            } else if (i == 1) {
                RTLImageView rTLImageView2 = a2.x;
                ee7.a((Object) rTLImageView2, "it.ivFavoriteContactsCheck");
                rTLImageView2.setVisibility(0);
            } else if (i == 2) {
                RTLImageView rTLImageView3 = a2.y;
                ee7.a((Object) rTLImageView3, "it.ivNoOneCheck");
                rTLImageView3.setVisibility(0);
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.g0.c().f().a(new qu5(this)).a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
            rj4 rj4 = this.r;
            if (rj4 != null) {
                he a2 = je.a(notificationCallsAndMessagesActivity, rj4).a(ju5.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                ju5 ju5 = (ju5) a2;
                this.s = ju5;
                mu5 mu5 = this.q;
                if (mu5 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (ju5 != null) {
                    mu5.a(ju5);
                } else {
                    ee7.d("mNotificationSettingViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        g45 g45 = (g45) qb.a(layoutInflater, 2131558595, viewGroup, false, this.j);
        String b2 = eh5.l.a().b("nonBrandSurface");
        String b3 = eh5.l.a().b("nonBrandDisableCalendarDay");
        if (!TextUtils.isEmpty(b2)) {
            g45.q.setBackgroundColor(Color.parseColor(b2));
        }
        if (!TextUtils.isEmpty(b3)) {
            int parseColor = Color.parseColor(b3);
            g45.z.setBackgroundColor(parseColor);
            g45.A.setBackgroundColor(parseColor);
            g45.B.setBackgroundColor(parseColor);
        }
        g45.r.setOnClickListener(new b(this));
        g45.s.setOnClickListener(new c(this));
        g45.t.setOnClickListener(new d(this));
        g45.v.setOnClickListener(new e(this));
        this.p = new qw6<>(this, g45);
        ee7.a((Object) g45, "binding");
        return g45.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        mu5 mu5 = this.q;
        if (mu5 != null) {
            mu5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        mu5 mu5 = this.q;
        if (mu5 != null) {
            mu5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(mu5 mu5) {
        ee7.b(mu5, "presenter");
        this.q = mu5;
    }

    @DexIgnore
    @Override // com.fossil.nu5
    public void a(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "title");
        qw6<g45> qw6 = this.p;
        if (qw6 != null) {
            g45 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.u) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
