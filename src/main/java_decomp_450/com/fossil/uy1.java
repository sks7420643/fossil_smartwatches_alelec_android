package com.fossil;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uy1 implements i12 {
    @DexIgnore
    public Status a;
    @DexIgnore
    public GoogleSignInAccount b;

    @DexIgnore
    public uy1(GoogleSignInAccount googleSignInAccount, Status status) {
        this.b = googleSignInAccount;
        this.a = status;
    }

    @DexIgnore
    @Override // com.fossil.i12
    public Status a() {
        return this.a;
    }

    @DexIgnore
    public GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.a.y();
    }
}
