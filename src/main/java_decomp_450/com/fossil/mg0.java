package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ lg0 b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ short f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ short h;
    @DexIgnore
    public /* final */ byte i;
    @DexIgnore
    public /* final */ pg0 j;
    @DexIgnore
    public /* final */ qg0 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<mg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public mg0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                Parcelable readParcelable = parcel.readParcelable(lg0.class.getClassLoader());
                if (readParcelable != null) {
                    return new mg0(createByteArray, (lg0) readParcelable);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public mg0[] newArray(int i) {
            return new mg0[i];
        }
    }

    @DexIgnore
    public mg0(byte[] bArr, lg0 lg0) {
        this.a = bArr;
        this.b = lg0;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        boolean z = false;
        this.c = order.get(0);
        this.d = order.get(1);
        this.e = order.get(2);
        this.f = order.getShort(3);
        this.g = order.getShort(7) > 0 ? true : z;
        this.h = yz0.b(order.get(9));
        this.i = order.get(10);
        this.j = l51.b.a(this.f);
        this.k = l51.b.b(this.f);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D3, this.j.name()), r51.r3, Byte.valueOf(this.e)), r51.E3, Byte.valueOf(this.d)), r51.F3, Byte.valueOf(this.c)), r51.G3, Byte.valueOf(this.i)), r51.H3, this.k), r51.I3, Short.valueOf(this.h)), r51.J3, Boolean.valueOf(this.g)), r51.K3, Long.valueOf(ik1.a.a(this.a, ng1.CRC32)));
    }

    @DexIgnore
    public final short b() {
        return this.f;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(mg0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            mg0 mg0 = (mg0) obj;
            return Arrays.equals(this.a, mg0.a) && !(ee7.a(this.b, mg0.b) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration");
    }

    @DexIgnore
    public final lg0 getCustomization() {
        return this.b;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.a;
    }

    @DexIgnore
    public final boolean getHasCustomization() {
        return this.g;
    }

    @DexIgnore
    public final byte getMajorVersion() {
        return this.c;
    }

    @DexIgnore
    public final pg0 getMicroAppId() {
        return this.j;
    }

    @DexIgnore
    public final qg0 getMicroAppVariantId() {
        return this.k;
    }

    @DexIgnore
    public final byte getMicroAppVersion() {
        return this.e;
    }

    @DexIgnore
    public final byte getMinorVersion() {
        return this.d;
    }

    @DexIgnore
    public final byte getVariationNumber() {
        return this.i;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.a) * 31;
        lg0 lg0 = this.b;
        return hashCode + (lg0 != null ? lg0.hashCode() : 0);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i2);
        }
    }
}
