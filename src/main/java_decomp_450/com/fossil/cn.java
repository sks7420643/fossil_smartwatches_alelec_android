package com.fossil;

import android.content.Context;
import android.os.Build;
import java.io.File;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cn {
    @DexIgnore
    public static /* final */ String a; // = im.a("WrkDbPathHelper");
    @DexIgnore
    public static /* final */ String[] b; // = {"-journal", "-shm", "-wal"};

    @DexIgnore
    public static File a(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return b(context);
        }
        return a(context, "androidx.work.workdb");
    }

    @DexIgnore
    public static String a() {
        return "androidx.work.workdb";
    }

    @DexIgnore
    public static File b(Context context) {
        return context.getDatabasePath("androidx.work.workdb");
    }

    @DexIgnore
    public static void c(Context context) {
        String str;
        File b2 = b(context);
        if (Build.VERSION.SDK_INT >= 23 && b2.exists()) {
            im.a().a(a, "Migrating WorkDatabase to the no-backup directory", new Throwable[0]);
            Map<File, File> d = d(context);
            for (File file : d.keySet()) {
                File file2 = d.get(file);
                if (file.exists() && file2 != null) {
                    if (file2.exists()) {
                        im.a().e(a, String.format("Over-writing contents of %s", file2), new Throwable[0]);
                    }
                    if (file.renameTo(file2)) {
                        str = String.format("Migrated %s to %s", file, file2);
                    } else {
                        str = String.format("Renaming %s to %s failed", file, file2);
                    }
                    im.a().a(a, str, new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public static Map<File, File> d(Context context) {
        HashMap hashMap = new HashMap();
        if (Build.VERSION.SDK_INT >= 23) {
            File b2 = b(context);
            File a2 = a(context);
            hashMap.put(b2, a2);
            String[] strArr = b;
            for (String str : strArr) {
                hashMap.put(new File(b2.getPath() + str), new File(a2.getPath() + str));
            }
        }
        return hashMap;
    }

    @DexIgnore
    public static File a(Context context, String str) {
        return new File(context.getNoBackupFilesDir(), str);
    }
}
