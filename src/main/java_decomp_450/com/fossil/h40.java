package com.fossil;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h40 {
    @DexIgnore
    public /* final */ AtomicReference<t50> a; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ n4<t50, List<Class<?>>> b; // = new n4<>();

    @DexIgnore
    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        List<Class<?>> list;
        t50 andSet = this.a.getAndSet(null);
        if (andSet == null) {
            andSet = new t50(cls, cls2, cls3);
        } else {
            andSet.a(cls, cls2, cls3);
        }
        synchronized (this.b) {
            list = this.b.get(andSet);
        }
        this.a.set(andSet);
        return list;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new t50(cls, cls2, cls3), list);
        }
    }
}
