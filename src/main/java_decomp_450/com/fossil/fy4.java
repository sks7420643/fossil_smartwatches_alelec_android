package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fy4 extends ey4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I;
    @DexIgnore
    public long G;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        I = sparseIntArray;
        sparseIntArray.put(2131363342, 1);
        I.put(2131362636, 2);
        I.put(2131361979, 3);
        I.put(2131362234, 4);
        I.put(2131361972, 5);
        I.put(2131362437, 6);
        I.put(2131361996, 7);
        I.put(2131363388, 8);
        I.put(2131363112, 9);
        I.put(2131362987, 10);
        I.put(2131362400, 11);
        I.put(2131361973, 12);
        I.put(2131361974, 13);
        I.put(2131361966, 14);
        I.put(2131363263, 15);
    }
    */

    @DexIgnore
    public fy4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 16, H, I));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public fy4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[14], (ImageView) objArr[5], (FlexibleButton) objArr[12], (FlexibleButton) objArr[13], (ConstraintLayout) objArr[3], (FlexibleCheckBox) objArr[7], (FlexibleEditText) objArr[4], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[6], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (RecyclerView) objArr[10], (SwipeRefreshLayout) objArr[9], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[1], (View) objArr[8]);
        this.G = -1;
        ((ey4) this).A.setTag(null);
        a(view);
        f();
    }
}
