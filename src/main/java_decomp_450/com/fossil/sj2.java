package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sj2 extends IInterface {
    @DexIgnore
    void a(pj2 pj2, ii2 ii2) throws RemoteException;

    @DexIgnore
    void a(qj2 qj2, zi2 zi2) throws RemoteException;

    @DexIgnore
    void a(sd2 sd2, zi2 zi2) throws RemoteException;
}
