package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yl3 extends vl3 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public yl3(xl3 xl3) {
        super(xl3);
        ((vl3) this).b.a(this);
    }

    @DexIgnore
    public final boolean p() {
        return this.c;
    }

    @DexIgnore
    public final void q() {
        if (!p()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void r() {
        if (!this.c) {
            s();
            ((vl3) this).b.t();
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean s();
}
