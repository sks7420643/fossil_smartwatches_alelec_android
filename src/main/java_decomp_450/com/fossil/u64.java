package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.v54;
import com.misfit.frameworks.common.constants.Constants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u64 {
    @DexIgnore
    public static /* final */ Charset g; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ int h; // = 15;
    @DexIgnore
    public static /* final */ e64 i; // = new e64();
    @DexIgnore
    public static /* final */ Comparator<? super File> j; // = s64.a();
    @DexIgnore
    public static /* final */ FilenameFilter k; // = t64.a();
    @DexIgnore
    public /* final */ AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ t74 f;

    @DexIgnore
    public u64(File file, t74 t74) {
        File file2 = new File(file, "report-persistence");
        this.b = new File(file2, Constants.SESSIONS);
        this.c = new File(file2, "priority-reports");
        this.d = new File(file2, "reports");
        this.e = new File(file2, "native-reports");
        this.f = t74;
    }

    @DexIgnore
    public static boolean e(String str) {
        return str.startsWith(Constants.EVENT) && str.endsWith(LocaleConverter.LOCALE_DELIMITER);
    }

    @DexIgnore
    public void a(v54 v54) {
        v54.d h2 = v54.h();
        if (h2 == null) {
            z24.a().a("Could not get session for report");
            return;
        }
        String g2 = h2.g();
        try {
            File c2 = c(g2);
            c(c2);
            d(new File(c2, "report"), i.a(v54));
        } catch (IOException e2) {
            z24 a2 = z24.a();
            a2.a("Could not persist report for session " + g2, e2);
        }
    }

    @DexIgnore
    public List<b44> d() {
        List<File> c2 = c();
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(c2.size());
        for (File file : c()) {
            try {
                arrayList.add(b44.a(i.b(d(file)), file.getName()));
            } catch (IOException e2) {
                z24 a2 = z24.a();
                a2.a("Could not load report file " + file + "; deleting", e2);
                file.delete();
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static void e(File file) {
        if (file != null) {
            if (file.isDirectory()) {
                for (File file2 : file.listFiles()) {
                    e(file2);
                }
            }
            file.delete();
        }
    }

    @DexIgnore
    public void b() {
        for (File file : c()) {
            file.delete();
        }
    }

    @DexIgnore
    public final List<File> c() {
        return b(a(a(this.c), a(this.e)), a(this.d));
    }

    @DexIgnore
    public void b(String str) {
        FilenameFilter a2 = o64.a(str);
        for (File file : a(a(this.c, a2), a(this.e, a2), a(this.d, a2))) {
            file.delete();
        }
    }

    @DexIgnore
    public void a(v54.d.AbstractC0206d dVar, String str, boolean z) {
        int i2 = this.f.b().b().a;
        File c2 = c(str);
        try {
            d(new File(c2, a(this.a.getAndIncrement(), z)), i.a(dVar));
        } catch (IOException e2) {
            z24 a2 = z24.a();
            a2.a("Could not persist event for session " + str, e2);
        }
        a(c2, i2);
    }

    @DexIgnore
    public final File c(String str) {
        return new File(this.b, str);
    }

    @DexIgnore
    public static int c(File file, File file2) {
        return d(file.getName()).compareTo(d(file2.getName()));
    }

    @DexIgnore
    public static String d(String str) {
        return str.substring(0, h);
    }

    @DexIgnore
    public static void d(File file, String str) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), g);
        try {
            outputStreamWriter.write(str);
            outputStreamWriter.close();
            return;
        } catch (Throwable unused) {
        }
        throw th;
    }

    @DexIgnore
    public static List<File> b(List<File>... listArr) {
        for (List<File> list : listArr) {
            Collections.sort(list, j);
        }
        return a(listArr);
    }

    @DexIgnore
    public static File c(File file) throws IOException {
        if (b(file)) {
            return file;
        }
        throw new IOException("Could not create directory " + file);
    }

    @DexIgnore
    public static String d(File file) throws IOException {
        byte[] bArr = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        FileInputStream fileInputStream = new FileInputStream(file);
        while (true) {
            try {
                int read = fileInputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    String str = new String(byteArrayOutputStream.toByteArray(), g);
                    fileInputStream.close();
                    return str;
                }
            } catch (Throwable unused) {
            }
        }
        throw th;
    }

    @DexIgnore
    public static boolean b(File file, String str) {
        return str.startsWith(Constants.EVENT) && !str.endsWith(LocaleConverter.LOCALE_DELIMITER);
    }

    @DexIgnore
    public static boolean b(File file) {
        return file.exists() || file.mkdirs();
    }

    @DexIgnore
    public void a(String str, String str2) {
        try {
            d(new File(c(str2), "user"), str);
        } catch (IOException e2) {
            z24 a2 = z24.a();
            a2.a("Could not persist user ID for session " + str2, e2);
        }
    }

    @DexIgnore
    public void a(String str, long j2) {
        for (File file : a(str)) {
            a(file, j2);
            e(file);
        }
        a();
    }

    @DexIgnore
    public void a(String str, v54.c cVar) {
        a(new File(c(str), "report"), this.e, cVar, str);
    }

    @DexIgnore
    public final List<File> a(String str) {
        List<File> a2 = a(this.b, p64.a(str));
        Collections.sort(a2, j);
        if (a2.size() <= 8) {
            return a2;
        }
        for (File file : a2.subList(8, a2.size())) {
            e(file);
        }
        return a2.subList(0, 8);
    }

    @DexIgnore
    public static /* synthetic */ boolean a(String str, File file) {
        return file.isDirectory() && !file.getName().equals(str);
    }

    @DexIgnore
    public final void a() {
        int i2 = this.f.b().b().b;
        List<File> c2 = c();
        int size = c2.size();
        if (size > i2) {
            for (File file : c2.subList(i2, size)) {
                file.delete();
            }
        }
    }

    @DexIgnore
    public final void a(File file, long j2) {
        boolean z;
        List<File> a2 = a(file, k);
        if (!a2.isEmpty()) {
            Collections.sort(a2);
            ArrayList arrayList = new ArrayList();
            Iterator<File> it = a2.iterator();
            loop0:
            while (true) {
                z = false;
                while (true) {
                    if (!it.hasNext()) {
                        break loop0;
                    }
                    File next = it.next();
                    try {
                        arrayList.add(i.a(d(next)));
                        if (z || e(next.getName())) {
                            z = true;
                        }
                    } catch (IOException e2) {
                        z24 a3 = z24.a();
                        a3.a("Could not add event to report for " + next, e2);
                    }
                }
            }
            String str = null;
            File file2 = new File(file, "user");
            if (file2.isFile()) {
                try {
                    str = d(file2);
                } catch (IOException e3) {
                    z24 a4 = z24.a();
                    a4.a("Could not read user ID file in " + file.getName(), e3);
                }
            }
            a(new File(file, "report"), z ? this.c : this.d, arrayList, j2, z, str);
        }
    }

    @DexIgnore
    public static void a(File file, File file2, v54.c cVar, String str) {
        try {
            v54 a2 = i.b(d(file)).a(cVar);
            c(file2);
            d(new File(file2, str), i.a(a2));
        } catch (IOException e2) {
            z24 a3 = z24.a();
            a3.a("Could not synthesize final native report file for " + file, e2);
        }
    }

    @DexIgnore
    public static void a(File file, File file2, List<v54.d.AbstractC0206d> list, long j2, boolean z, String str) {
        try {
            v54 a2 = i.b(d(file)).a(j2, z, str).a(w54.a(list));
            v54.d h2 = a2.h();
            if (h2 != null) {
                c(file2);
                d(new File(file2, h2.g()), i.a(a2));
            }
        } catch (IOException e2) {
            z24 a3 = z24.a();
            a3.a("Could not synthesize final report file for " + file, e2);
        }
    }

    @DexIgnore
    public static List<File> a(List<File>... listArr) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        for (List<File> list : listArr) {
            i2 += list.size();
        }
        arrayList.ensureCapacity(i2);
        for (List<File> list2 : listArr) {
            arrayList.addAll(list2);
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(int i2, boolean z) {
        String format = String.format(Locale.US, "%010d", Integer.valueOf(i2));
        String str = z ? LocaleConverter.LOCALE_DELIMITER : "";
        return Constants.EVENT + format + str;
    }

    @DexIgnore
    public static int a(File file, int i2) {
        List<File> a2 = a(file, q64.a());
        Collections.sort(a2, r64.a());
        return a(a2, i2);
    }

    @DexIgnore
    public static List<File> a(File file) {
        return a(file, (FileFilter) null);
    }

    @DexIgnore
    public static List<File> a(File file, FilenameFilter filenameFilter) {
        if (!file.isDirectory()) {
            return Collections.emptyList();
        }
        File[] listFiles = filenameFilter == null ? file.listFiles() : file.listFiles(filenameFilter);
        return listFiles != null ? Arrays.asList(listFiles) : Collections.emptyList();
    }

    @DexIgnore
    public static List<File> a(File file, FileFilter fileFilter) {
        if (!file.isDirectory()) {
            return Collections.emptyList();
        }
        File[] listFiles = fileFilter == null ? file.listFiles() : file.listFiles(fileFilter);
        return listFiles != null ? Arrays.asList(listFiles) : Collections.emptyList();
    }

    @DexIgnore
    public static int a(List<File> list, int i2) {
        int size = list.size();
        for (File file : list) {
            if (size <= i2) {
                return size;
            }
            e(file);
            size--;
        }
        return size;
    }
}
