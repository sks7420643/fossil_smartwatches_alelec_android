package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d55 extends c55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131363342, 1);
        D.put(2131363220, 2);
        D.put(2131363196, 3);
        D.put(2131362047, 4);
        D.put(2131362640, 5);
        D.put(2131362021, 6);
        D.put(2131363463, 7);
        D.put(2131363455, 8);
        D.put(2131363462, 9);
        D.put(2131363459, 10);
    }
    */

    @DexIgnore
    public d55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 11, C, D));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public d55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (View) objArr[6], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[0], (ImageView) objArr[5], (TextView) objArr[3], (TextView) objArr[2], (TextView) objArr[1], (CustomizeWidget) objArr[8], (CustomizeWidget) objArr[10], (CustomizeWidget) objArr[9], (CustomizeWidget) objArr[7]);
        this.B = -1;
        ((c55) this).s.setTag(null);
        a(view);
        f();
    }
}
