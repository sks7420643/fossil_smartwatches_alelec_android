package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk6 extends he {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public ArrayList<Theme> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Style> b; // = new ArrayList<>();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public MutableLiveData<b> e; // = new MutableLiveData<>();
    @DexIgnore
    public b f; // = new b(null, null, null, null, false, 31, null);
    @DexIgnore
    public /* final */ ThemeRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public ArrayList<Theme> a;
        @DexIgnore
        public List<Style> b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b() {
            this(null, null, null, null, false, 31, null);
        }

        @DexIgnore
        public b(ArrayList<Theme> arrayList, List<Style> list, String str, String str2, boolean z) {
            this.a = arrayList;
            this.b = list;
            this.c = str;
            this.d = str2;
            this.e = z;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.e;
        }

        @DexIgnore
        public final ArrayList<Theme> c() {
            return this.a;
        }

        @DexIgnore
        public final List<Style> d() {
            return this.b;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(ArrayList arrayList, List list, String str, String str2, boolean z, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : arrayList, (i & 2) != 0 ? null : list, (i & 4) != 0 ? null : str, (i & 8) == 0 ? str2 : null, (i & 16) != 0 ? false : z);
        }

        @DexIgnore
        public final void a(boolean z) {
            this.e = z;
        }

        @DexIgnore
        public final void a(ArrayList<Theme> arrayList, String str, String str2, List<Style> list) {
            this.a = arrayList;
            this.d = str2;
            this.c = str;
            this.e = false;
            this.b = list;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1", f = "ThemesViewModel.kt", l = {66}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pk6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1$1", f = "ThemesViewModel.kt", l = {68, 69}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String e = pk6.h;
                    local.d(e, "apply theme " + this.this$0.this$0.d);
                    ThemeRepository e2 = this.this$0.this$0.g;
                    String g = this.this$0.this$0.d;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (e2.setCurrentThemeId(g, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    this.this$0.this$0.f.a(true);
                    this.this$0.this$0.b();
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                eh5 a2 = eh5.l.a();
                this.L$0 = yi7;
                this.label = 2;
                if (a2.a(this) == a) {
                    return a;
                }
                this.this$0.this$0.f.a(true);
                this.this$0.this$0.b();
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pk6 pk6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pk6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$deleteTheme$1", f = "ThemesViewModel.kt", l = {79, 80, 82, 83, 85}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pk6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(pk6 pk6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pk6;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$id, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00ad A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00ae  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00b4  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x00d5  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00fd A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0105  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x016c  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 5
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                if (r1 == 0) goto L_0x0065
                if (r1 == r6) goto L_0x005d
                if (r1 == r5) goto L_0x0051
                if (r1 == r4) goto L_0x0041
                if (r1 == r3) goto L_0x0030
                if (r1 != r2) goto L_0x0028
                java.lang.Object r0 = r9.L$2
                com.fossil.pk6 r0 = (com.fossil.pk6) r0
                java.lang.Object r1 = r9.L$1
                com.portfolio.platform.data.model.Theme r1 = (com.portfolio.platform.data.model.Theme) r1
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                goto L_0x0100
            L_0x0028:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x0030:
                java.lang.Object r1 = r9.L$2
                com.fossil.pk6 r1 = (com.fossil.pk6) r1
                java.lang.Object r3 = r9.L$1
                com.portfolio.platform.data.model.Theme r3 = (com.portfolio.platform.data.model.Theme) r3
                java.lang.Object r4 = r9.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r10)
                goto L_0x00d0
            L_0x0041:
                java.lang.Object r1 = r9.L$2
                com.fossil.pk6 r1 = (com.fossil.pk6) r1
                java.lang.Object r4 = r9.L$1
                com.portfolio.platform.data.model.Theme r4 = (com.portfolio.platform.data.model.Theme) r4
                java.lang.Object r5 = r9.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r10)
                goto L_0x00b2
            L_0x0051:
                java.lang.Object r1 = r9.L$1
                com.portfolio.platform.data.model.Theme r1 = (com.portfolio.platform.data.model.Theme) r1
                java.lang.Object r5 = r9.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r10)
                goto L_0x0099
            L_0x005d:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                goto L_0x0080
            L_0x0065:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r10 = r9.p$
                com.fossil.pk6 r1 = r9.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r1.g
                java.lang.String r7 = r9.$id
                r9.L$0 = r10
                r9.label = r6
                java.lang.Object r1 = r1.getThemeById(r7, r9)
                if (r1 != r0) goto L_0x007d
                return r0
            L_0x007d:
                r8 = r1
                r1 = r10
                r10 = r8
            L_0x0080:
                com.portfolio.platform.data.model.Theme r10 = (com.portfolio.platform.data.model.Theme) r10
                com.fossil.pk6 r6 = r9.this$0
                com.portfolio.platform.data.source.ThemeRepository r6 = r6.g
                if (r10 == 0) goto L_0x0174
                r9.L$0 = r1
                r9.L$1 = r10
                r9.label = r5
                java.lang.Object r5 = r6.deleteTheme(r10, r9)
                if (r5 != r0) goto L_0x0097
                return r0
            L_0x0097:
                r5 = r1
                r1 = r10
            L_0x0099:
                com.fossil.pk6 r10 = r9.this$0
                com.portfolio.platform.data.source.ThemeRepository r6 = r10.g
                r9.L$0 = r5
                r9.L$1 = r1
                r9.L$2 = r10
                r9.label = r4
                java.lang.Object r4 = r6.getListTheme(r9)
                if (r4 != r0) goto L_0x00ae
                return r0
            L_0x00ae:
                r8 = r1
                r1 = r10
                r10 = r4
                r4 = r8
            L_0x00b2:
                if (r10 == 0) goto L_0x016c
                java.util.ArrayList r10 = (java.util.ArrayList) r10
                r1.a = r10
                com.fossil.pk6 r1 = r9.this$0
                com.portfolio.platform.data.source.ThemeRepository r10 = r1.g
                r9.L$0 = r5
                r9.L$1 = r4
                r9.L$2 = r1
                r9.label = r3
                java.lang.Object r10 = r10.getCurrentThemeId(r9)
                if (r10 != r0) goto L_0x00ce
                return r0
            L_0x00ce:
                r3 = r4
                r4 = r5
            L_0x00d0:
                java.lang.String r10 = (java.lang.String) r10
                if (r10 == 0) goto L_0x00d5
                goto L_0x00d7
            L_0x00d5:
                java.lang.String r10 = "default"
            L_0x00d7:
                r1.c = r10
                com.fossil.pk6 r10 = r9.this$0
                java.lang.String r1 = r10.c
                r10.d = r1
                com.fossil.pk6 r10 = r9.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r10.g
                com.fossil.pk6 r5 = r9.this$0
                java.lang.String r5 = r5.c
                r9.L$0 = r4
                r9.L$1 = r3
                r9.L$2 = r10
                r9.label = r2
                java.lang.Object r1 = r1.getListStyleById(r5, r9)
                if (r1 != r0) goto L_0x00fe
                return r0
            L_0x00fe:
                r0 = r10
                r10 = r1
            L_0x0100:
                java.util.ArrayList r10 = (java.util.ArrayList) r10
                if (r10 == 0) goto L_0x0105
                goto L_0x010a
            L_0x0105:
                java.util.ArrayList r10 = new java.util.ArrayList
                r10.<init>()
            L_0x010a:
                r0.b = r10
                com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                java.lang.String r0 = com.fossil.pk6.h
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "mCurrentThemeId="
                r1.append(r2)
                com.fossil.pk6 r2 = r9.this$0
                java.lang.String r2 = r2.c
                r1.append(r2)
                java.lang.String r2 = " mThemes.size="
                r1.append(r2)
                com.fossil.pk6 r2 = r9.this$0
                java.util.ArrayList r2 = r2.a
                int r2 = r2.size()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r10.d(r0, r1)
                com.fossil.pk6 r10 = r9.this$0
                com.fossil.pk6$b r10 = r10.f
                com.fossil.pk6 r0 = r9.this$0
                java.util.ArrayList r0 = r0.a
                com.fossil.pk6 r1 = r9.this$0
                java.lang.String r1 = r1.d
                com.fossil.pk6 r2 = r9.this$0
                java.lang.String r2 = r2.c
                com.fossil.pk6 r3 = r9.this$0
                java.util.ArrayList r3 = r3.b
                r10.a(r0, r2, r1, r3)
                com.fossil.pk6 r10 = r9.this$0
                r10.b()
                com.fossil.i97 r10 = com.fossil.i97.a
                return r10
            L_0x016c:
                com.fossil.x87 r10 = new com.fossil.x87
            */
            //  java.lang.String r0 = "null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.data.model.Theme> /* = java.util.ArrayList<com.portfolio.platform.data.model.Theme> */"
            /*
                r10.<init>(r0)
                throw r10
            L_0x0174:
                com.fossil.ee7.a()
                r10 = 0
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1", f = "ThemesViewModel.kt", l = {50}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $themeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pk6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1$1", f = "ThemesViewModel.kt", l = {51}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ArrayList<Style>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ArrayList<Style>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ThemeRepository e = this.this$0.this$0.g;
                    String str = this.this$0.$themeId;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = e.getListStyleById(str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ArrayList arrayList = (ArrayList) obj;
                return arrayList != null ? arrayList : new ArrayList();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(pk6 pk6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pk6;
            this.$themeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$themeId, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            pk6 pk6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.d = this.$themeId;
                pk6 pk62 = this.this$0;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = pk62;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                pk6 = pk62;
            } else if (i == 1) {
                pk6 = (pk6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            pk6.b = (ArrayList) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = pk6.h;
            local.d(e, "userSelectedId " + this.this$0.d + " currentThemeId " + this.this$0.c);
            this.this$0.f.a(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.b();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1", f = "ThemesViewModel.kt", l = {27}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pk6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1$1", f = "ThemesViewModel.kt", l = {28, 29, 31}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x007d  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00a9 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00aa  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00b1  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r7.label
                    r2 = 3
                    r3 = 2
                    r4 = 1
                    if (r1 == 0) goto L_0x003e
                    if (r1 == r4) goto L_0x0032
                    if (r1 == r3) goto L_0x0026
                    if (r1 != r2) goto L_0x001e
                    java.lang.Object r0 = r7.L$1
                    com.fossil.pk6 r0 = (com.fossil.pk6) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r8)
                    goto L_0x00ac
                L_0x001e:
                    java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r8.<init>(r0)
                    throw r8
                L_0x0026:
                    java.lang.Object r1 = r7.L$1
                    com.fossil.pk6 r1 = (com.fossil.pk6) r1
                    java.lang.Object r3 = r7.L$0
                    com.fossil.yi7 r3 = (com.fossil.yi7) r3
                    com.fossil.t87.a(r8)
                    goto L_0x0078
                L_0x0032:
                    java.lang.Object r1 = r7.L$1
                    com.fossil.pk6 r1 = (com.fossil.pk6) r1
                    java.lang.Object r4 = r7.L$0
                    com.fossil.yi7 r4 = (com.fossil.yi7) r4
                    com.fossil.t87.a(r8)
                    goto L_0x005b
                L_0x003e:
                    com.fossil.t87.a(r8)
                    com.fossil.yi7 r8 = r7.p$
                    com.fossil.pk6$f r1 = r7.this$0
                    com.fossil.pk6 r1 = r1.this$0
                    com.portfolio.platform.data.source.ThemeRepository r5 = r1.g
                    r7.L$0 = r8
                    r7.L$1 = r1
                    r7.label = r4
                    java.lang.Object r4 = r5.getListTheme(r7)
                    if (r4 != r0) goto L_0x0058
                    return r0
                L_0x0058:
                    r6 = r4
                    r4 = r8
                    r8 = r6
                L_0x005b:
                    if (r8 == 0) goto L_0x00bc
                    java.util.ArrayList r8 = (java.util.ArrayList) r8
                    r1.a = r8
                    com.fossil.pk6$f r8 = r7.this$0
                    com.fossil.pk6 r1 = r8.this$0
                    com.portfolio.platform.data.source.ThemeRepository r8 = r1.g
                    r7.L$0 = r4
                    r7.L$1 = r1
                    r7.label = r3
                    java.lang.Object r8 = r8.getCurrentThemeId(r7)
                    if (r8 != r0) goto L_0x0077
                    return r0
                L_0x0077:
                    r3 = r4
                L_0x0078:
                    java.lang.String r8 = (java.lang.String) r8
                    if (r8 == 0) goto L_0x007d
                    goto L_0x007f
                L_0x007d:
                    java.lang.String r8 = "default"
                L_0x007f:
                    r1.c = r8
                    com.fossil.pk6$f r8 = r7.this$0
                    com.fossil.pk6 r8 = r8.this$0
                    java.lang.String r1 = r8.c
                    r8.d = r1
                    com.fossil.pk6$f r8 = r7.this$0
                    com.fossil.pk6 r8 = r8.this$0
                    com.portfolio.platform.data.source.ThemeRepository r1 = r8.g
                    com.fossil.pk6$f r4 = r7.this$0
                    com.fossil.pk6 r4 = r4.this$0
                    java.lang.String r4 = r4.c
                    r7.L$0 = r3
                    r7.L$1 = r8
                    r7.label = r2
                    java.lang.Object r1 = r1.getListStyleById(r4, r7)
                    if (r1 != r0) goto L_0x00aa
                    return r0
                L_0x00aa:
                    r0 = r8
                    r8 = r1
                L_0x00ac:
                    java.util.ArrayList r8 = (java.util.ArrayList) r8
                    if (r8 == 0) goto L_0x00b1
                    goto L_0x00b6
                L_0x00b1:
                    java.util.ArrayList r8 = new java.util.ArrayList
                    r8.<init>()
                L_0x00b6:
                    r0.b = r8
                    com.fossil.i97 r8 = com.fossil.i97.a
                    return r8
                L_0x00bc:
                    com.fossil.x87 r8 = new com.fossil.x87
                */
                //  java.lang.String r0 = "null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.data.model.Theme> /* = java.util.ArrayList<com.portfolio.platform.data.model.Theme> */"
                /*
                    r8.<init>(r0)
                    throw r8
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk6.f.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(pk6 pk6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pk6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = pk6.h;
            local.d(e, "mCurrentThemeId=" + this.this$0.c);
            this.this$0.f.a(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.b();
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = pk6.class.getSimpleName();
        ee7.a((Object) simpleName, "ThemesViewModel::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public pk6(ThemeRepository themeRepository) {
        ee7.b(themeRepository, "mThemesRepository");
        this.g = themeRepository;
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.e;
    }

    @DexIgnore
    public final void d() {
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final void a() {
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void b() {
        this.e.a(this.f);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "deleteTheme id=" + str);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new d(this, str, null), 3, null);
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "themeId");
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, str, null), 3, null);
    }
}
