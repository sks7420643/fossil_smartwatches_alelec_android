package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a16 extends dl4<z06> {
    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void e(List<String> list);

    @DexIgnore
    void p(String str);

    @DexIgnore
    void setTitle(String str);

    @DexIgnore
    void y(String str);
}
