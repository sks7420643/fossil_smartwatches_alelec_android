package com.fossil;

import android.content.Intent;
import android.os.Binder;
import android.os.Process;
import android.util.Log;
import com.fossil.mb4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jb4 extends Binder {
    @DexIgnore
    public /* final */ a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        no3<Void> a(Intent intent);
    }

    @DexIgnore
    public jb4(a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public void a(mb4.a aVar) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "service received new intent via bind strategy");
            }
            this.a.a(aVar.a).a(u94.a(), new ib4(aVar));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
