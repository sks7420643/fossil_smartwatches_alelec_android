package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class aq0 extends Enum<aq0> {
    @DexIgnore
    public static /* final */ aq0 a;
    @DexIgnore
    public static /* final */ aq0 b;
    @DexIgnore
    public static /* final */ aq0 c;
    @DexIgnore
    public static /* final */ aq0 d;
    @DexIgnore
    public static /* final */ aq0 e;
    @DexIgnore
    public static /* final */ aq0 f;
    @DexIgnore
    public static /* final */ aq0 g;
    @DexIgnore
    public static /* final */ aq0 h;
    @DexIgnore
    public static /* final */ aq0 i;
    @DexIgnore
    public static /* final */ aq0 j;
    @DexIgnore
    public static /* final */ aq0 k;
    @DexIgnore
    public static /* final */ aq0 l;
    @DexIgnore
    public static /* final */ aq0 m;
    @DexIgnore
    public static /* final */ aq0 n;
    @DexIgnore
    public static /* final */ /* synthetic */ aq0[] o;

    /*
    static {
        aq0 aq0 = new aq0("CLOSE", 0);
        a = aq0;
        aq0 aq02 = new aq0("CONNECT", 1);
        b = aq02;
        aq0 aq03 = new aq0("DISCONNECT", 2);
        c = aq03;
        aq0 aq04 = new aq0("DISCOVER_SERVICE", 3);
        d = aq04;
        aq0 aq05 = new aq0("READ_CHARACTERISTIC", 4);
        e = aq05;
        aq0 aq06 = new aq0("REQUEST_MTU", 6);
        f = aq06;
        aq0 aq07 = new aq0("SUBSCRIBE_CHARACTERISTIC", 7);
        g = aq07;
        aq0 aq08 = new aq0("WRITE_CHARACTERISTIC", 8);
        h = aq08;
        aq0 aq09 = new aq0("READ_RSSI", 10);
        i = aq09;
        aq0 aq010 = new aq0("CREATE_BOND", 11);
        j = aq010;
        aq0 aq011 = new aq0("REMOVE_BOND", 12);
        k = aq011;
        aq0 aq012 = new aq0("CONNECT_HID", 13);
        l = aq012;
        aq0 aq013 = new aq0("DISCONNECT_HID", 14);
        m = aq013;
        aq0 aq014 = new aq0("UNKNOWN", 15);
        n = aq014;
        o = new aq0[]{aq0, aq02, aq03, aq04, aq05, new aq0("READ_DESCRIPTOR", 5), aq06, aq07, aq08, new aq0("WRITE_DESCRIPTOR", 9), aq09, aq010, aq011, aq012, aq013, aq014};
    }
    */

    @DexIgnore
    public aq0(String str, int i2) {
    }

    @DexIgnore
    public static aq0 valueOf(String str) {
        return (aq0) Enum.valueOf(aq0.class, str);
    }

    @DexIgnore
    public static aq0[] values() {
        return (aq0[]) o.clone();
    }
}
