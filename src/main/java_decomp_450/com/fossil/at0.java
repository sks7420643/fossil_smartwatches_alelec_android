package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at0 extends rr1 {
    @DexIgnore
    public qk1 M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ at0(long j, long j2, long j3, short s, ri1 ri1, int i, int i2) {
        super(ln0.d, s, qa1.l, ri1, (i2 & 32) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.M = qk1.UNKNOWN;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        qk1 qk1;
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        if (true ^ (bArr.length == 0)) {
            qk1 = qk1.p.a(bArr[0]);
        } else {
            qk1 = qk1.FTD;
        }
        this.M = qk1;
        return yz0.a(jSONObject, r51.E0, qk1.a);
    }

    @DexIgnore
    @Override // com.fossil.v81, com.fossil.rr1
    public JSONObject g() {
        return yz0.a(yz0.a(yz0.a(super.g(), r51.a1, Long.valueOf(this.N)), r51.b1, Long.valueOf(this.O)), r51.c1, Long.valueOf(this.P));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.E0, this.M.a);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        ee7.a((Object) array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }
}
