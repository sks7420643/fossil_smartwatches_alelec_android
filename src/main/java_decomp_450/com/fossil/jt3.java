package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dt3;
import com.google.android.material.datepicker.MaterialCalendarGridView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jt3 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ ws3 a;
    @DexIgnore
    public /* final */ zs3<?> b;
    @DexIgnore
    public /* final */ dt3.l c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MaterialCalendarGridView a;

        @DexIgnore
        public a(MaterialCalendarGridView materialCalendarGridView) {
            this.a = materialCalendarGridView;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (this.a.getAdapter().e(i)) {
                jt3.this.c.a(this.a.getAdapter().getItem(i).longValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ MaterialCalendarGridView b;

        @DexIgnore
        public b(LinearLayout linearLayout, boolean z) {
            super(linearLayout);
            TextView textView = (TextView) linearLayout.findViewById(nr3.month_title);
            this.a = textView;
            da.a((View) textView, true);
            this.b = (MaterialCalendarGridView) linearLayout.findViewById(nr3.month_grid);
            if (!z) {
                this.a.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public jt3(Context context, zs3<?> zs3, ws3 ws3, dt3.l lVar) {
        ht3 e = ws3.e();
        ht3 b2 = ws3.b();
        ht3 d2 = ws3.d();
        if (e.compareTo(d2) > 0) {
            throw new IllegalArgumentException("firstPage cannot be after currentPage");
        } else if (d2.compareTo(b2) <= 0) {
            this.d = (it3.e * dt3.a(context)) + (et3.f(context) ? dt3.a(context) : 0);
            this.a = ws3;
            this.b = zs3;
            this.c = lVar;
            setHasStableIds(true);
        } else {
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
    }

    @DexIgnore
    public CharSequence b(int i) {
        return a(i).b();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.c();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return this.a.e().b(i).c();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ht3 b2 = this.a.e().b(i);
        bVar.a.setText(b2.b());
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) bVar.b.findViewById(nr3.month_grid);
        if (materialCalendarGridView.getAdapter() == null || !b2.equals(materialCalendarGridView.getAdapter().a)) {
            it3 it3 = new it3(b2, this.b, this.a);
            materialCalendarGridView.setNumColumns(b2.e);
            materialCalendarGridView.setAdapter((ListAdapter) it3);
        } else {
            materialCalendarGridView.getAdapter().notifyDataSetChanged();
        }
        materialCalendarGridView.setOnItemClickListener(new a(materialCalendarGridView));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(pr3.mtrl_calendar_month_labeled, viewGroup, false);
        if (!et3.f(viewGroup.getContext())) {
            return new b(linearLayout, false);
        }
        linearLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, this.d));
        return new b(linearLayout, true);
    }

    @DexIgnore
    public ht3 a(int i) {
        return this.a.e().b(i);
    }

    @DexIgnore
    public int a(ht3 ht3) {
        return this.a.e().b(ht3);
    }
}
