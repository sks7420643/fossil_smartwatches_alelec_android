package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca6 implements MembersInjector<CaloriesOverviewFragment> {
    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, z96 z96) {
        caloriesOverviewFragment.g = z96;
    }

    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, qa6 qa6) {
        caloriesOverviewFragment.h = qa6;
    }

    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, ka6 ka6) {
        caloriesOverviewFragment.i = ka6;
    }
}
