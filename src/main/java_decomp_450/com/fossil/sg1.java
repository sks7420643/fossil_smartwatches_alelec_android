package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sg1 implements Parcelable.Creator<pi1> {
    @DexIgnore
    public /* synthetic */ sg1(zd7 zd7) {
    }

    @DexIgnore
    public final pi1 a(byte b, byte[] bArr) {
        return new pi1(b, new rg0(bArr));
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public pi1 createFromParcel(Parcel parcel) {
        return new pi1(parcel, (zd7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public pi1[] newArray(int i) {
        return new pi1[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public pi1 m71createFromParcel(Parcel parcel) {
        return new pi1(parcel, (zd7) null);
    }
}
