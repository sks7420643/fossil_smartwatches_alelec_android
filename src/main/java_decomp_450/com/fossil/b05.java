package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b05 extends a05 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131363221, 1);
        x.put(2131363309, 2);
        x.put(2131362980, 3);
        x.put(2131362997, 4);
    }
    */

    @DexIgnore
    public b05(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 5, w, x));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public b05(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[0], (RecyclerView) objArr[3], (RecyclerView) objArr[4], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[2]);
        this.v = -1;
        ((a05) this).q.setTag(null);
        a(view);
        f();
    }
}
