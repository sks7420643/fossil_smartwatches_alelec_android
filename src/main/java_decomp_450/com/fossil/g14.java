package com.fossil;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g14 {
    @DexIgnore
    public /* final */ Set<Type> a; // = yz3.a();

    @DexIgnore
    public void a(Class<?> cls) {
    }

    @DexIgnore
    public void a(GenericArrayType genericArrayType) {
    }

    @DexIgnore
    public void a(ParameterizedType parameterizedType) {
    }

    @DexIgnore
    public abstract void a(TypeVariable<?> typeVariable);

    @DexIgnore
    public abstract void a(WildcardType wildcardType);

    @DexIgnore
    public final void a(Type... typeArr) {
        for (Type type : typeArr) {
            if (type != null && this.a.add(type)) {
                try {
                    if (type instanceof TypeVariable) {
                        a((TypeVariable) type);
                    } else if (type instanceof WildcardType) {
                        a((WildcardType) type);
                    } else if (type instanceof ParameterizedType) {
                        a((ParameterizedType) type);
                    } else if (type instanceof Class) {
                        a((Class) type);
                    } else if (type instanceof GenericArrayType) {
                        a((GenericArrayType) type);
                    } else {
                        throw new AssertionError("Unknown type: " + type);
                    }
                } catch (Throwable th) {
                    this.a.remove(type);
                    throw th;
                }
            }
        }
    }
}
