package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum z90 {
    SILENT((byte) 1),
    IMPORTANT((byte) 2),
    PRE_EXISTING((byte) 4),
    ALLOW_USER_POSITIVE_ACTION((byte) 8),
    ALLOW_USER_NEGATIVE_ACTION((byte) 16),
    ALLOW_USER_REPLY_ACTION((byte) 32);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final z90[] a(String[] strArr) {
            z90 z90;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                z90[] values = z90.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z90 = null;
                        break;
                    }
                    z90 = values[i];
                    if (ee7.a(yz0.a(z90), str) || ee7.a(z90.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (z90 != null) {
                    arrayList.add(z90);
                }
            }
            Object[] array = arrayList.toArray(new z90[0]);
            if (array != null) {
                return (z90[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public z90(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
