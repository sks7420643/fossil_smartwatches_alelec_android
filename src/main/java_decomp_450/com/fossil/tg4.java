package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg4 extends Exception {
    @DexIgnore
    public tg4() {
    }

    @DexIgnore
    public tg4(String str) {
        super(str);
    }

    @DexIgnore
    public tg4(Throwable th) {
        super(th);
    }
}
