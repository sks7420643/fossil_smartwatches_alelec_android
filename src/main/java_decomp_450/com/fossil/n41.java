package com.fossil;

import com.fossil.fitness.WorkoutSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n41 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ s21 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n41(s21 s21) {
        super(1);
        this.a = s21;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        ab0 r = ((ua1) v81).r();
        WorkoutSessionManager workoutSessionManager = ((zk0) this.a).x.a.s;
        if (workoutSessionManager == null || workoutSessionManager.getCurrentSessionId() != r.getSessionId()) {
            en0 en0 = ((zk0) this.a).x;
            en0.a.a(WorkoutSessionManager.initialize(en0.a().getSerialNumber(), r.getSessionId(), r.getWorkoutState(), r.getDurationInSecond()));
        }
        return i97.a;
    }
}
