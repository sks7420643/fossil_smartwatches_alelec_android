package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa2 implements Parcelable.Creator<ta2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ta2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        boolean z = false;
        String str = null;
        IBinder iBinder = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                str = j72.e(parcel, a);
            } else if (a2 == 2) {
                iBinder = j72.p(parcel, a);
            } else if (a2 == 3) {
                z = j72.i(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                z2 = j72.i(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new ta2(str, iBinder, z, z2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ta2[] newArray(int i) {
        return new ta2[i];
    }
}
