package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao5 extends fl4<a, c, b> {
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ ch5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl4.b {
        @DexIgnore
        public /* final */ SignUpEmailAuth a;

        @DexIgnore
        public a(SignUpEmailAuth signUpEmailAuth) {
            ee7.b(signUpEmailAuth, "emailAuth");
            this.a = signUpEmailAuth;
        }

        @DexIgnore
        public final SignUpEmailAuth a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            ee7.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase", f = "SignUpEmailUseCase.kt", l = {33, 36}, m = "run")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ao5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ao5 ao5, fb7 fb7) {
            super(fb7);
            this.this$0 = ao5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((a) null, (fb7<Object>) this);
        }
    }

    @DexIgnore
    public ao5(UserRepository userRepository, ch5 ch5) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(a aVar, fb7 fb7) {
        return a(aVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SignUpEmailUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.ao5.a r11, com.fossil.fb7<java.lang.Object> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.ao5.d
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.ao5$d r0 = (com.fossil.ao5.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ao5$d r0 = new com.fossil.ao5$d
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            r5 = 600(0x258, float:8.41E-43)
            java.lang.String r6 = "SignUpEmailUseCase"
            java.lang.String r7 = ""
            if (r2 == 0) goto L_0x005b
            if (r2 == r4) goto L_0x0047
            if (r2 != r3) goto L_0x003f
            java.lang.Object r11 = r0.L$2
            com.portfolio.platform.data.Access r11 = (com.portfolio.platform.data.Access) r11
            java.lang.Object r11 = r0.L$1
            com.fossil.ao5$a r11 = (com.fossil.ao5.a) r11
            java.lang.Object r11 = r0.L$0
            com.fossil.ao5 r11 = (com.fossil.ao5) r11
            com.fossil.t87.a(r12)
            goto L_0x00ba
        L_0x003f:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0047:
            java.lang.Object r11 = r0.L$2
            com.portfolio.platform.data.Access r11 = (com.portfolio.platform.data.Access) r11
            java.lang.Object r2 = r0.L$1
            com.fossil.ao5$a r2 = (com.fossil.ao5.a) r2
            java.lang.Object r4 = r0.L$0
            com.fossil.ao5 r4 = (com.fossil.ao5) r4
            com.fossil.t87.a(r12)
            r9 = r12
            r12 = r11
            r11 = r4
            r4 = r9
            goto L_0x009c
        L_0x005b:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = "running UseCase"
            r12.d(r6, r2)
            com.fossil.ng5 r12 = com.fossil.ng5.a()
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            com.portfolio.platform.data.Access r12 = r12.a(r2)
            if (r12 != 0) goto L_0x007f
            com.fossil.tn5$b r11 = new com.fossil.tn5$b
            r11.<init>(r5, r7)
            return r11
        L_0x007f:
            if (r11 != 0) goto L_0x0088
            com.fossil.ao5$b r11 = new com.fossil.ao5$b
            r11.<init>(r5, r7)
            goto L_0x012b
        L_0x0088:
            com.portfolio.platform.data.source.UserRepository r2 = r10.d
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r4
            java.lang.Object r2 = r2.getCurrentUser(r0)
            if (r2 != r1) goto L_0x0099
            return r1
        L_0x0099:
            r4 = r2
            r2 = r11
            r11 = r10
        L_0x009c:
            if (r4 == 0) goto L_0x00a5
            com.fossil.ao5$c r11 = new com.fossil.ao5$c
            r11.<init>()
            goto L_0x012b
        L_0x00a5:
            com.portfolio.platform.data.source.UserRepository r4 = r11.d
            com.portfolio.platform.data.SignUpEmailAuth r8 = r2.a()
            r0.L$0 = r11
            r0.L$1 = r2
            r0.L$2 = r12
            r0.label = r3
            java.lang.Object r12 = r4.signUpEmail(r8, r0)
            if (r12 != r1) goto L_0x00ba
            return r1
        L_0x00ba:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r0 = r12 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x00e5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "signUpEmail success"
            r0.d(r6, r2)
            com.fossil.ch5 r11 = r11.e
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r12 = r12.a()
            com.portfolio.platform.data.model.MFUser$Auth r12 = (com.portfolio.platform.data.model.MFUser.Auth) r12
            if (r12 == 0) goto L_0x00dc
            java.lang.String r1 = r12.getAccessToken()
        L_0x00dc:
            r11.y(r1)
            com.fossil.ao5$c r11 = new com.fossil.ao5$c
            r11.<init>()
            goto L_0x012b
        L_0x00e5:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x0126
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "signUpEmail failed code="
            r0.append(r2)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            if (r2 == 0) goto L_0x0105
            java.lang.String r1 = r2.getMessage()
        L_0x0105:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r11.d(r6, r0)
            com.fossil.ao5$b r11 = new com.fossil.ao5$b
            int r0 = r12.a()
            com.portfolio.platform.data.model.ServerError r12 = r12.c()
            if (r12 == 0) goto L_0x0122
            java.lang.String r12 = r12.getMessage()
            if (r12 == 0) goto L_0x0122
            r7 = r12
        L_0x0122:
            r11.<init>(r0, r7)
            goto L_0x012b
        L_0x0126:
            com.fossil.ao5$b r11 = new com.fossil.ao5$b
            r11.<init>(r5, r7)
        L_0x012b:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ao5.a(com.fossil.ao5$a, com.fossil.fb7):java.lang.Object");
    }
}
