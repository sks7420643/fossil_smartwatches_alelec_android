package com.fossil;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import androidx.core.app.SharedElementCallback;
import com.portfolio.platform.PortfolioApp;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk5 extends SharedElementCallback {
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public /* final */ Intent d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore
    public yk5(Intent intent, PortfolioApp portfolioApp) {
        ee7.b(intent, "intent");
        ee7.b(portfolioApp, "mApp");
        this.d = intent;
        this.e = portfolioApp;
        Resources resources = portfolioApp.getResources();
        this.b = (float) resources.getDimensionPixelSize(2131165726);
        this.c = (float) resources.getDimensionPixelSize(2131165722);
    }

    @DexIgnore
    @Override // androidx.core.app.SharedElementCallback
    public void a(List<String> list, List<? extends View> list2, List<? extends View> list3) {
        Integer num;
        if (list2 != null) {
            Iterator<? extends View> it = list2.iterator();
            while (true) {
                num = null;
                if (!it.hasNext()) {
                    break;
                }
                ((View) it.next()).setTag(2131363127, null);
            }
            if (list != null) {
                num = Integer.valueOf(list.indexOf(this.e.getString(2131887559)));
            }
            if (num != null) {
                Object obj = list2.get(num.intValue());
                if (obj != null) {
                    TextView textView = (TextView) obj;
                    int measuredWidth = textView.getMeasuredWidth();
                    int measuredHeight = textView.getMeasuredHeight();
                    textView.setTextSize(0, this.c);
                    textView.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                    int measuredWidth2 = (textView.getMeasuredWidth() - measuredWidth) / 2;
                    int measuredHeight2 = (textView.getMeasuredHeight() - measuredHeight) / 2;
                    textView.layout(textView.getLeft() - measuredWidth2, textView.getTop() - measuredHeight2, textView.getRight() + measuredWidth2, textView.getBottom() + measuredHeight2);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.TextView");
            }
        }
    }

    @DexIgnore
    @Override // androidx.core.app.SharedElementCallback
    public void b(List<String> list, List<? extends View> list2, List<? extends View> list3) {
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                String str = list.get(i);
                if (this.d.hasExtra(str) && list2 != null) {
                    ((View) list2.get(i)).setTag(2131363127, this.d.getBundleExtra(str));
                }
            }
            int indexOf = list.indexOf(this.e.getString(2131887559));
            if (list2 != null) {
                Object obj = list2.get(indexOf);
                if (obj != null) {
                    ((TextView) obj).setTextSize(0, this.b);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.TextView");
            }
        }
    }
}
