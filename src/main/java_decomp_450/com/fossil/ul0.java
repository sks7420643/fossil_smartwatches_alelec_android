package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ul0 {
    DEVICE_INFORMATION,
    DEVICE_CONFIG,
    FILE_CONFIG,
    TRANSFER_DATA,
    AUTHENTICATION,
    ASYNC,
    UNKNOWN
}
