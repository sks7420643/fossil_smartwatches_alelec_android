package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.fossil.aw;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cw extends ContextWrapper {
    @DexIgnore
    public static /* final */ jw<?, ?> k; // = new zv();
    @DexIgnore
    public /* final */ az a;
    @DexIgnore
    public /* final */ gw b;
    @DexIgnore
    public /* final */ a50 c;
    @DexIgnore
    public /* final */ aw.a d;
    @DexIgnore
    public /* final */ List<q40<Object>> e;
    @DexIgnore
    public /* final */ Map<Class<?>, jw<?, ?>> f;
    @DexIgnore
    public /* final */ jy g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public r40 j;

    @DexIgnore
    public cw(Context context, az azVar, gw gwVar, a50 a50, aw.a aVar, Map<Class<?>, jw<?, ?>> map, List<q40<Object>> list, jy jyVar, boolean z, int i2) {
        super(context.getApplicationContext());
        this.a = azVar;
        this.b = gwVar;
        this.c = a50;
        this.d = aVar;
        this.e = list;
        this.f = map;
        this.g = jyVar;
        this.h = z;
        this.i = i2;
    }

    @DexIgnore
    public <T> jw<?, T> a(Class<T> cls) {
        jw<?, T> jwVar;
        jw<?, T> jwVar2 = (jw<?, T>) this.f.get(cls);
        if (jwVar2 == null) {
            for (Map.Entry<Class<?>, jw<?, ?>> entry : this.f.entrySet()) {
                if (entry.getKey().isAssignableFrom(cls)) {
                    jwVar2 = (jw<?, T>) entry.getValue();
                }
            }
        }
        return jwVar == null ? (jw<?, T>) k : jwVar;
    }

    @DexIgnore
    public List<q40<Object>> b() {
        return this.e;
    }

    @DexIgnore
    public synchronized r40 c() {
        if (this.j == null) {
            this.j = (r40) this.d.build().I();
        }
        return this.j;
    }

    @DexIgnore
    public jy d() {
        return this.g;
    }

    @DexIgnore
    public int e() {
        return this.i;
    }

    @DexIgnore
    public gw f() {
        return this.b;
    }

    @DexIgnore
    public boolean g() {
        return this.h;
    }

    @DexIgnore
    public <X> d50<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    @DexIgnore
    public az a() {
        return this.a;
    }
}
