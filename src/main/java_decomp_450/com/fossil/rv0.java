package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ xt0 CREATOR; // = new xt0(null);
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    public rv0(boolean z) {
        this.a = z;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return b();
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("is_front_light_enabled", this.a);
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(rv0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((rv0) obj).a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.frontlight.FrontLightConfig");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.a).hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a ? 1 : 0);
        }
    }
}
