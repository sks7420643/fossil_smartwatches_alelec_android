package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t23 implements q23 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;
    @DexIgnore
    public static /* final */ tq2<Boolean> c;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        dr2.a("measurement.id.lifecycle.app_in_background_parameter", 0L);
        a = dr2.a("measurement.lifecycle.app_backgrounded_engagement", false);
        b = dr2.a("measurement.lifecycle.app_backgrounded_tracking", true);
        c = dr2.a("measurement.lifecycle.app_in_background_parameter", false);
        dr2.a("measurement.id.lifecycle.app_backgrounded_tracking", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.q23
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.q23
    public final boolean zzb() {
        return b.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.q23
    public final boolean zzc() {
        return c.b().booleanValue();
    }
}
