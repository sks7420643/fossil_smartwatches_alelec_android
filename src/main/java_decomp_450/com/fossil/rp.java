package com.fossil;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rp {
    @DexIgnore
    public static /* final */ String f; // = im.a("WorkTimer");
    @DexIgnore
    public /* final */ ThreadFactory a; // = new a(this);
    @DexIgnore
    public /* final */ ScheduledExecutorService b; // = Executors.newSingleThreadScheduledExecutor(this.a);
    @DexIgnore
    public /* final */ Map<String, c> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<String, b> d; // = new HashMap();
    @DexIgnore
    public /* final */ Object e; // = new Object();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {
        @DexIgnore
        public int a; // = 0;

        @DexIgnore
        public a(rp rpVar) {
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
            newThread.setName("WorkManager-WorkTimer-thread-" + this.a);
            this.a = this.a + 1;
            return newThread;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ rp a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(rp rpVar, String str) {
            this.a = rpVar;
            this.b = str;
        }

        @DexIgnore
        public void run() {
            synchronized (this.a.e) {
                if (this.a.c.remove(this.b) != null) {
                    b remove = this.a.d.remove(this.b);
                    if (remove != null) {
                        remove.a(this.b);
                    }
                } else {
                    im.a().a("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.b), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, long j, b bVar) {
        synchronized (this.e) {
            im.a().a(f, String.format("Starting timer for %s", str), new Throwable[0]);
            a(str);
            c cVar = new c(this, str);
            this.c.put(str, cVar);
            this.d.put(str, bVar);
            this.b.schedule(cVar, j, TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void a(String str) {
        synchronized (this.e) {
            if (this.c.remove(str) != null) {
                im.a().a(f, String.format("Stopping timer for %s", str), new Throwable[0]);
                this.d.remove(str);
            }
        }
    }

    @DexIgnore
    public void a() {
        if (!this.b.isShutdown()) {
            this.b.shutdownNow();
        }
    }
}
