package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm2 extends tm2 implements kn2 {
    @DexIgnore
    public vm2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IPolylineDelegate");
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final int a() throws RemoteException {
        Parcel a = a(16, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final boolean b(kn2 kn2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, kn2);
        Parcel a = a(15, zza);
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final String getId() throws RemoteException {
        Parcel a = a(2, zza());
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void remove() throws RemoteException {
        b(1, zza());
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setColor(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        b(7, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setEndCap(b93 b93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, b93);
        b(21, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setGeodesic(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(13, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setJointType(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        b(23, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setPattern(List<l93> list) throws RemoteException {
        Parcel zza = zza();
        zza.writeTypedList(list);
        b(25, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setPoints(List<LatLng> list) throws RemoteException {
        Parcel zza = zza();
        zza.writeTypedList(list);
        b(3, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setStartCap(b93 b93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, b93);
        b(19, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(11, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setWidth(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(5, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void setZIndex(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(9, zza);
    }

    @DexIgnore
    @Override // com.fossil.kn2
    public final void a(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(17, zza);
    }
}
