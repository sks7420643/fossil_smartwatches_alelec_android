package com.fossil;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q24 implements i94, h94 {
    @DexIgnore
    public /* final */ Map<Class<?>, ConcurrentHashMap<g94<Object>, Executor>> a; // = new HashMap();
    @DexIgnore
    public Queue<f94<?>> b; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public q24(Executor executor) {
        this.c = executor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r0.hasNext() == false) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1 = r0.next();
        r1.getValue().execute(com.fossil.p24.a(r1, r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = a(r4).iterator();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.fossil.f94<?> r4) {
        /*
            r3 = this;
            com.fossil.t24.a(r4)
            monitor-enter(r3)
            java.util.Queue<com.fossil.f94<?>> r0 = r3.b     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x000f
            java.util.Queue<com.fossil.f94<?>> r0 = r3.b     // Catch:{ all -> 0x0033 }
            r0.add(r4)     // Catch:{ all -> 0x0033 }
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            return
        L_0x000f:
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            java.util.Set r0 = r3.a(r4)
            java.util.Iterator r0 = r0.iterator()
        L_0x0018:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0032
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getValue()
            java.util.concurrent.Executor r2 = (java.util.concurrent.Executor) r2
            java.lang.Runnable r1 = com.fossil.p24.a(r1, r4)
            r2.execute(r1)
            goto L_0x0018
        L_0x0032:
            return
        L_0x0033:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q24.b(com.fossil.f94):void");
    }

    @DexIgnore
    public final synchronized Set<Map.Entry<g94<Object>, Executor>> a(f94<?> f94) {
        ConcurrentHashMap<g94<Object>, Executor> concurrentHashMap;
        concurrentHashMap = this.a.get(f94.b());
        return concurrentHashMap == null ? Collections.emptySet() : concurrentHashMap.entrySet();
    }

    @DexIgnore
    @Override // com.fossil.i94
    public synchronized <T> void a(Class<T> cls, Executor executor, g94<? super T> g94) {
        t24.a(cls);
        t24.a(g94);
        t24.a(executor);
        if (!this.a.containsKey(cls)) {
            this.a.put(cls, new ConcurrentHashMap<>());
        }
        this.a.get(cls).put(g94, executor);
    }

    @DexIgnore
    @Override // com.fossil.i94
    public <T> void a(Class<T> cls, g94<? super T> g94) {
        a(cls, this.c, g94);
    }

    @DexIgnore
    public void a() {
        Queue<f94<?>> queue;
        synchronized (this) {
            queue = null;
            if (this.b != null) {
                Queue<f94<?>> queue2 = this.b;
                this.b = null;
                queue = queue2;
            }
        }
        if (queue != null) {
            for (f94<?> f94 : queue) {
                b(f94);
            }
        }
    }
}
