package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class cs1 extends Enum<cs1> {
    @DexIgnore
    public static /* final */ cs1 b;
    @DexIgnore
    public static /* final */ cs1 c;
    @DexIgnore
    public static /* final */ /* synthetic */ cs1[] d;
    @DexIgnore
    public static /* final */ dq1 e; // = new dq1(null);
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        cs1 cs1 = new cs1("THEME", 1, (byte) 1);
        b = cs1;
        cs1 cs12 = new cs1("WATCH_APP", 2, (byte) 2);
        c = cs12;
        d = new cs1[]{new cs1("SYSTEM", 0, (byte) 0), cs1, cs12};
    }
    */

    @DexIgnore
    public cs1(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static cs1 valueOf(String str) {
        return (cs1) Enum.valueOf(cs1.class, str);
    }

    @DexIgnore
    public static cs1[] values() {
        return (cs1[]) d.clone();
    }
}
