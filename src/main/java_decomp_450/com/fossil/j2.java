package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j2 {
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public e3 b;
    @DexIgnore
    public e3 c;
    @DexIgnore
    public e3 d;

    @DexIgnore
    public j2(ImageView imageView) {
        this.a = imageView;
    }

    @DexIgnore
    public void a(AttributeSet attributeSet, int i) {
        int g;
        g3 a2 = g3.a(this.a.getContext(), attributeSet, h0.AppCompatImageView, i, 0);
        ImageView imageView = this.a;
        da.a(imageView, imageView.getContext(), h0.AppCompatImageView, attributeSet, a2.a(), i, 0);
        try {
            Drawable drawable = this.a.getDrawable();
            if (!(drawable != null || (g = a2.g(h0.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = t0.c(this.a.getContext(), g)) == null)) {
                this.a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                q2.b(drawable);
            }
            if (a2.g(h0.AppCompatImageView_tint)) {
                wa.a(this.a, a2.a(h0.AppCompatImageView_tint));
            }
            if (a2.g(h0.AppCompatImageView_tintMode)) {
                wa.a(this.a, q2.a(a2.d(h0.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            a2.b();
        }
    }

    @DexIgnore
    public ColorStateList b() {
        e3 e3Var = this.c;
        if (e3Var != null) {
            return e3Var.a;
        }
        return null;
    }

    @DexIgnore
    public PorterDuff.Mode c() {
        e3 e3Var = this.c;
        if (e3Var != null) {
            return e3Var.b;
        }
        return null;
    }

    @DexIgnore
    public boolean d() {
        return Build.VERSION.SDK_INT < 21 || !(this.a.getBackground() instanceof RippleDrawable);
    }

    @DexIgnore
    public final boolean e() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.b != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(int i) {
        if (i != 0) {
            Drawable c2 = t0.c(this.a.getContext(), i);
            if (c2 != null) {
                q2.b(c2);
            }
            this.a.setImageDrawable(c2);
        } else {
            this.a.setImageDrawable(null);
        }
        a();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new e3();
        }
        e3 e3Var = this.c;
        e3Var.a = colorStateList;
        e3Var.d = true;
        a();
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new e3();
        }
        e3 e3Var = this.c;
        e3Var.b = mode;
        e3Var.c = true;
        a();
    }

    @DexIgnore
    public void a() {
        Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            q2.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!e() || !a(drawable)) {
            e3 e3Var = this.c;
            if (e3Var != null) {
                h2.a(drawable, e3Var, this.a.getDrawableState());
                return;
            }
            e3 e3Var2 = this.b;
            if (e3Var2 != null) {
                h2.a(drawable, e3Var2, this.a.getDrawableState());
            }
        }
    }

    @DexIgnore
    public final boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new e3();
        }
        e3 e3Var = this.d;
        e3Var.a();
        ColorStateList a2 = wa.a(this.a);
        if (a2 != null) {
            e3Var.d = true;
            e3Var.a = a2;
        }
        PorterDuff.Mode b2 = wa.b(this.a);
        if (b2 != null) {
            e3Var.c = true;
            e3Var.b = b2;
        }
        if (!e3Var.d && !e3Var.c) {
            return false;
        }
        h2.a(drawable, e3Var, this.a.getDrawableState());
        return true;
    }
}
