package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.fossil.m00;
import java.io.File;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t00<Data> implements m00<String, Data> {
    @DexIgnore
    public /* final */ m00<Uri, Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements n00<String, AssetFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.n00
        public m00<String, AssetFileDescriptor> a(q00 q00) {
            return new t00(q00.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements n00<String, ParcelFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.n00
        public m00<String, ParcelFileDescriptor> a(q00 q00) {
            return new t00(q00.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements n00<String, InputStream> {
        @DexIgnore
        @Override // com.fossil.n00
        public m00<String, InputStream> a(q00 q00) {
            return new t00(q00.a(Uri.class, InputStream.class));
        }
    }

    @DexIgnore
    public t00(m00<Uri, Data> m00) {
        this.a = m00;
    }

    @DexIgnore
    public static Uri b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return c(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? c(str) : parse;
    }

    @DexIgnore
    public static Uri c(String str) {
        return Uri.fromFile(new File(str));
    }

    @DexIgnore
    public boolean a(String str) {
        return true;
    }

    @DexIgnore
    public m00.a<Data> a(String str, int i, int i2, ax axVar) {
        Uri b2 = b(str);
        if (b2 == null || !this.a.a(b2)) {
            return null;
        }
        return this.a.a(b2, i, i2, axVar);
    }
}
