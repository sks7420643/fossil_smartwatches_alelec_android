package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tr7 {
    @DexIgnore
    public static /* final */ tr7 d; // = new a();
    @DexIgnore
    public boolean a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends tr7 {
        @DexIgnore
        @Override // com.fossil.tr7
        public tr7 a(long j) {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tr7
        public tr7 a(long j, TimeUnit timeUnit) {
            ee7.b(timeUnit, Constants.PROFILE_KEY_UNIT);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tr7
        public void e() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    public tr7 a(long j, TimeUnit timeUnit) {
        ee7.b(timeUnit, Constants.PROFILE_KEY_UNIT);
        if (j >= 0) {
            this.c = timeUnit.toNanos(j);
            return this;
        }
        throw new IllegalArgumentException(("timeout < 0: " + j).toString());
    }

    @DexIgnore
    public tr7 b() {
        this.c = 0;
        return this;
    }

    @DexIgnore
    public long c() {
        if (this.a) {
            return this.b;
        }
        throw new IllegalStateException("No deadline".toString());
    }

    @DexIgnore
    public boolean d() {
        return this.a;
    }

    @DexIgnore
    public void e() throws IOException {
        if (Thread.interrupted()) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException("interrupted");
        } else if (this.a && this.b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    @DexIgnore
    public long f() {
        return this.c;
    }

    @DexIgnore
    public tr7 a(long j) {
        this.a = true;
        this.b = j;
        return this;
    }

    @DexIgnore
    public tr7 a() {
        this.a = false;
        return this;
    }
}
