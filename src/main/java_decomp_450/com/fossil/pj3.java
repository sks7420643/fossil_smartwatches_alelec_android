package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@TargetApi(14)
public final class pj3 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public /* final */ /* synthetic */ ti3 a;

    @DexIgnore
    public pj3(ti3 ti3) {
        this.a = ti3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x013e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x013f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(boolean r18, android.net.Uri r19, java.lang.String r20, java.lang.String r21) {
        /*
            r17 = this;
            r1 = r17
            r0 = r20
            r2 = r21
            com.fossil.ti3 r3 = r1.a
            r3.g()
            com.fossil.ti3 r3 = r1.a     // Catch:{ Exception -> 0x01d6 }
            com.fossil.ym3 r3 = r3.l()     // Catch:{ Exception -> 0x01d6 }
            com.fossil.yf3<java.lang.Boolean> r4 = com.fossil.wb3.i0     // Catch:{ Exception -> 0x01d6 }
            boolean r3 = r3.a(r4)     // Catch:{ Exception -> 0x01d6 }
            java.lang.String r4 = "Activity created with data 'referrer' without required params"
            java.lang.String r5 = "utm_medium"
            java.lang.String r6 = "_cis"
            java.lang.String r7 = "utm_source"
            java.lang.String r8 = "utm_campaign"
            java.lang.String r10 = "gclid"
            if (r3 != 0) goto L_0x0044
            com.fossil.ti3 r3 = r1.a
            com.fossil.ym3 r3 = r3.l()
            com.fossil.yf3<java.lang.Boolean> r11 = com.fossil.wb3.k0
            boolean r3 = r3.a(r11)
            if (r3 != 0) goto L_0x0044
            com.fossil.ti3 r3 = r1.a
            com.fossil.ym3 r3 = r3.l()
            com.fossil.yf3<java.lang.Boolean> r11 = com.fossil.wb3.j0
            boolean r3 = r3.a(r11)
            if (r3 == 0) goto L_0x0042
            goto L_0x0044
        L_0x0042:
            r3 = 0
            goto L_0x009b
        L_0x0044:
            com.fossil.ti3 r3 = r1.a
            com.fossil.jm3 r3 = r3.j()
            boolean r11 = android.text.TextUtils.isEmpty(r21)
            if (r11 == 0) goto L_0x0051
            goto L_0x0042
        L_0x0051:
            boolean r11 = r2.contains(r10)
            if (r11 != 0) goto L_0x0075
            boolean r11 = r2.contains(r8)
            if (r11 != 0) goto L_0x0075
            boolean r11 = r2.contains(r7)
            if (r11 != 0) goto L_0x0075
            boolean r11 = r2.contains(r5)
            if (r11 != 0) goto L_0x0075
            com.fossil.jg3 r3 = r3.e()
            com.fossil.mg3 r3 = r3.A()
            r3.a(r4)
            goto L_0x0042
        L_0x0075:
            java.lang.String r11 = "https://google.com/search?"
            java.lang.String r12 = java.lang.String.valueOf(r21)
            int r13 = r12.length()
            if (r13 == 0) goto L_0x0086
            java.lang.String r11 = r11.concat(r12)
            goto L_0x008c
        L_0x0086:
            java.lang.String r12 = new java.lang.String
            r12.<init>(r11)
            r11 = r12
        L_0x008c:
            android.net.Uri r11 = android.net.Uri.parse(r11)
            android.os.Bundle r3 = r3.a(r11)
            if (r3 == 0) goto L_0x009b
            java.lang.String r11 = "referrer"
            r3.putString(r6, r11)
        L_0x009b:
            r11 = 0
            java.lang.String r12 = "_cmp"
            r13 = 1
            if (r18 == 0) goto L_0x00fe
            com.fossil.ti3 r14 = r1.a
            com.fossil.jm3 r14 = r14.j()
            r15 = r19
            android.os.Bundle r14 = r14.a(r15)
            if (r14 == 0) goto L_0x00ff
            java.lang.String r15 = "intent"
            r14.putString(r6, r15)
            com.fossil.ti3 r6 = r1.a
            com.fossil.ym3 r6 = r6.l()
            com.fossil.yf3<java.lang.Boolean> r15 = com.fossil.wb3.i0
            boolean r6 = r6.a(r15)
            if (r6 == 0) goto L_0x00e3
            boolean r6 = r14.containsKey(r10)
            if (r6 != 0) goto L_0x00e3
            if (r3 == 0) goto L_0x00e3
            boolean r6 = r3.containsKey(r10)
            if (r6 == 0) goto L_0x00e3
            java.lang.String r6 = "_cer"
            java.lang.String r15 = "gclid=%s"
            java.lang.Object[] r9 = new java.lang.Object[r13]
            java.lang.String r16 = r3.getString(r10)
            r9[r11] = r16
            java.lang.String r9 = java.lang.String.format(r15, r9)
            r14.putString(r6, r9)
        L_0x00e3:
            com.fossil.ti3 r6 = r1.a
            r6.a(r0, r12, r14)
            com.fossil.ti3 r6 = r1.a
            com.fossil.ym3 r6 = r6.l()
            com.fossil.yf3<java.lang.Boolean> r9 = com.fossil.wb3.R0
            boolean r6 = r6.a(r9)
            if (r6 == 0) goto L_0x00ff
            com.fossil.ti3 r6 = r1.a
            com.fossil.pm3 r6 = r6.h
            r6.a(r0, r14)
            goto L_0x00ff
        L_0x00fe:
            r14 = 0
        L_0x00ff:
            com.fossil.ti3 r6 = r1.a
            com.fossil.ym3 r6 = r6.l()
            com.fossil.yf3<java.lang.Boolean> r9 = com.fossil.wb3.k0
            boolean r6 = r6.a(r9)
            java.lang.String r9 = "auto"
            if (r6 == 0) goto L_0x0138
            com.fossil.ti3 r6 = r1.a
            com.fossil.ym3 r6 = r6.l()
            com.fossil.yf3<java.lang.Boolean> r15 = com.fossil.wb3.j0
            boolean r6 = r6.a(r15)
            if (r6 != 0) goto L_0x0138
            if (r3 == 0) goto L_0x0138
            boolean r6 = r3.containsKey(r10)
            if (r6 == 0) goto L_0x0138
            if (r14 == 0) goto L_0x012d
            boolean r6 = r14.containsKey(r10)
            if (r6 != 0) goto L_0x0138
        L_0x012d:
            com.fossil.ti3 r6 = r1.a
            java.lang.String r14 = "_lgclid"
            java.lang.String r15 = r3.getString(r10)
            r6.a(r9, r14, r15, r13)
        L_0x0138:
            boolean r6 = android.text.TextUtils.isEmpty(r21)
            if (r6 == 0) goto L_0x013f
            return
        L_0x013f:
            com.fossil.ti3 r6 = r1.a
            com.fossil.jg3 r6 = r6.e()
            com.fossil.mg3 r6 = r6.A()
            java.lang.String r14 = "Activity created with referrer"
            r6.a(r14, r2)
            com.fossil.ti3 r6 = r1.a
            com.fossil.ym3 r6 = r6.l()
            com.fossil.yf3<java.lang.Boolean> r14 = com.fossil.wb3.j0
            boolean r6 = r6.a(r14)
            java.lang.String r14 = "_ldl"
            if (r6 == 0) goto L_0x0191
            if (r3 == 0) goto L_0x017b
            com.fossil.ti3 r2 = r1.a
            r2.a(r0, r12, r3)
            com.fossil.ti3 r2 = r1.a
            com.fossil.ym3 r2 = r2.l()
            com.fossil.yf3<java.lang.Boolean> r4 = com.fossil.wb3.R0
            boolean r2 = r2.a(r4)
            if (r2 == 0) goto L_0x018a
            com.fossil.ti3 r2 = r1.a
            com.fossil.pm3 r2 = r2.h
            r2.a(r0, r3)
            goto L_0x018a
        L_0x017b:
            com.fossil.ti3 r0 = r1.a
            com.fossil.jg3 r0 = r0.e()
            com.fossil.mg3 r0 = r0.A()
            java.lang.String r3 = "Referrer does not contain valid parameters"
            r0.a(r3, r2)
        L_0x018a:
            com.fossil.ti3 r0 = r1.a
            r2 = 0
            r0.a(r9, r14, r2, r13)
            return
        L_0x0191:
            boolean r0 = r2.contains(r10)
            if (r0 == 0) goto L_0x01ba
            boolean r0 = r2.contains(r8)
            if (r0 != 0) goto L_0x01b9
            boolean r0 = r2.contains(r7)
            if (r0 != 0) goto L_0x01b9
            boolean r0 = r2.contains(r5)
            if (r0 != 0) goto L_0x01b9
            java.lang.String r0 = "utm_term"
            boolean r0 = r2.contains(r0)
            if (r0 != 0) goto L_0x01b9
            java.lang.String r0 = "utm_content"
            boolean r0 = r2.contains(r0)
            if (r0 == 0) goto L_0x01ba
        L_0x01b9:
            r11 = 1
        L_0x01ba:
            if (r11 != 0) goto L_0x01ca
            com.fossil.ti3 r0 = r1.a
            com.fossil.jg3 r0 = r0.e()
            com.fossil.mg3 r0 = r0.A()
            r0.a(r4)
            return
        L_0x01ca:
            boolean r0 = android.text.TextUtils.isEmpty(r21)
            if (r0 != 0) goto L_0x01d5
            com.fossil.ti3 r0 = r1.a
            r0.a(r9, r14, r2, r13)
        L_0x01d5:
            return
        L_0x01d6:
            r0 = move-exception
            com.fossil.ti3 r2 = r1.a
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Throwable caught in handleReferrerForOnActivityCreated"
            r2.a(r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pj3.a(boolean, android.net.Uri, java.lang.String, java.lang.String):void");
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        try {
            this.a.e().B().a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data == null || !data.isHierarchical()) {
                    this.a.r().a(activity, bundle);
                    return;
                }
                this.a.j();
                this.a.c().a(new oj3(this, bundle == null, data, jm3.a(intent) ? "gs" : "auto", data.getQueryParameter("referrer")));
                this.a.r().a(activity, bundle);
            }
        } catch (Exception e) {
            this.a.e().t().a("Throwable caught in onActivityCreated", e);
        } finally {
            this.a.r().a(activity, bundle);
        }
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
        this.a.r().c(activity);
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
        this.a.r().b(activity);
        il3 t = this.a.t();
        t.c().a(new kl3(t, t.zzm().c()));
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        il3 t = this.a.t();
        t.c().a(new ll3(t, t.zzm().c()));
        this.a.r().a(activity);
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.a.r().b(activity, bundle);
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }

    @DexIgnore
    public /* synthetic */ pj3(ti3 ti3, ui3 ui3) {
        this(ti3);
    }
}
