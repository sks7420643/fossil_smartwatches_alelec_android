package com.fossil;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.cy6;
import com.fossil.os5;
import com.fossil.ov3;
import com.fossil.yl4;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr5 extends ho5 implements c56, View.OnClickListener, yl4.d, cy6.g, ro5 {
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public b56 g;
    @DexIgnore
    public ConstraintLayout h;
    @DexIgnore
    public ViewPager2 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public yl4 p;
    @DexIgnore
    public qw6<u25> q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final yr5 a() {
            return new yr5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ yr5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(yr5 yr5) {
            this.a = yr5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g b;
            Drawable b2;
            TabLayout tabLayout2;
            TabLayout.g b3;
            Drawable b4;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "onPageScrolled position " + i + " mCurrentPosition " + this.a.f1());
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.a.s)) {
                int parseColor = Color.parseColor(this.a.s);
                u25 u25 = (u25) yr5.b(this.a).a();
                if (!(u25 == null || (tabLayout2 = u25.z) == null || (b3 = tabLayout2.b(i)) == null || (b4 = b3.b()) == null)) {
                    b4.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.r) && this.a.f1() != i) {
                int parseColor2 = Color.parseColor(this.a.r);
                u25 u252 = (u25) yr5.b(this.a).a();
                if (!(u252 == null || (tabLayout = u252.z) == null || (b = tabLayout.b(this.a.f1())) == null || (b2 = b.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.n(i);
            this.a.g1().a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements os5.b {
        @DexIgnore
        public /* final */ /* synthetic */ yr5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(yr5 yr5, String str) {
            this.a = yr5;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.os5.b
        public void a(String str) {
            ee7.b(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.g1().a(str, this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.os5.b
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ yr5 a;

        @DexIgnore
        public d(yr5 yr5) {
            this.a = yr5;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            int itemCount = this.a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.a.r)) {
                int parseColor = Color.parseColor(this.a.r);
                if (i == 0) {
                    gVar.b(2131230941);
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor);
                    }
                } else if (i == itemCount - 1) {
                    gVar.b(2131230817);
                    Drawable b2 = gVar.b();
                    if (b2 != null) {
                        b2.setTint(parseColor);
                    }
                } else {
                    gVar.b(2131230963);
                    Drawable b3 = gVar.b();
                    if (b3 != null) {
                        b3.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.a.s) && i == this.a.f1()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeHybridCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.a.f1());
                    Drawable b4 = gVar.b();
                    if (b4 != null) {
                        b4.setTint(Color.parseColor(this.a.s));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ qw6 b(yr5 yr5) {
        qw6<u25> qw6 = yr5.q;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void M(String str) {
        ee7.b(str, "microAppId");
        if (isActive()) {
            xg5.a(xg5.b, requireContext(), ve5.a.b(str), false, false, false, (Integer) null, 60, (Object) null);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void d(int i2) {
        yl4 yl4 = this.p;
        if (yl4 == null) {
            return;
        }
        if (yl4 == null) {
            ee7.d("mHybridPresetDetailAdapter");
            throw null;
        } else if (yl4.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.j);
            } else {
                ee7.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HomeHybridCustomizeFragment";
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void e(int i2) {
        this.u = true;
        yl4 yl4 = this.p;
        if (yl4 == null) {
            ee7.d("mHybridPresetDetailAdapter");
            throw null;
        } else if (yl4.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                ee7.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final int f1() {
        return this.j;
    }

    @DexIgnore
    public final b56 g1() {
        b56 b56 = this.g;
        if (b56 != null) {
            return b56;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c56
    public int getItemCount() {
        ViewPager2 viewPager2 = this.i;
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        ee7.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        qw6<u25> qw6 = this.q;
        if (qw6 != null) {
            u25 a2 = qw6.a();
            TabLayout tabLayout = a2 != null ? a2.z : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    new ov3(tabLayout, viewPager2, new d(this)).a();
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void k() {
        String string = getString(2131886761);
        ee7.a((Object) string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        W(string);
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            ee7.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.g0.c().c(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void m() {
        a();
    }

    @DexIgnore
    public final void n(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        ee7.b(view, "v");
        if (view.getId() == 2131362472 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
            ee7.a((Object) activity, "it");
            PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        u25 u25 = (u25) qb.a(layoutInflater, 2131558575, viewGroup, false, a1());
        ee7.a((Object) u25, "binding");
        a(u25);
        qw6<u25> qw6 = new qw6<>(this, u25);
        this.q = qw6;
        if (qw6 != null) {
            u25 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onPause");
        b56 b56 = this.g;
        if (b56 == null) {
            return;
        }
        if (b56 != null) {
            b56.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onResume");
        b56 b56 = this.g;
        if (b56 == null) {
            return;
        }
        if (b56 != null) {
            b56.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        h1();
        if (this.g != null) {
            V("customize_view");
        }
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z) {
        if (z) {
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        jf5 c12 = c1();
        if (c12 != null) {
            c12.a("");
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void s(boolean z) {
        if (z) {
            qw6<u25> qw6 = this.q;
            if (qw6 != null) {
                u25 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B;
                    ee7.a((Object) flexibleTextView, "tvTapIconToCustomize");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<u25> qw62 = this.q;
        if (qw62 != null) {
            u25 a3 = qw62.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView2 = a3.B;
                ee7.a((Object) flexibleTextView2, "tvTapIconToCustomize");
                flexibleTextView2.setVisibility(4);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void w() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showCreateNewSuccessfully");
        yl4 yl4 = this.p;
        if (yl4 != null) {
            int itemCount = yl4.getItemCount();
            int i2 = this.j;
            if (itemCount > i2) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            }
        } else {
            ee7.d("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yl4.d
    public void x() {
        b56 b56 = this.g;
        if (b56 != null) {
            b56.i();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yl4.d
    public void y() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onAddPresetClick");
        b56 b56 = this.g;
        if (b56 != null) {
            b56.h();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showDeleteSuccessfully - position=" + i2 + " mCurrentPosition=" + this.j);
        yl4 yl4 = this.p;
        if (yl4 == null) {
            ee7.d("mHybridPresetDetailAdapter");
            throw null;
        } else if (yl4.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.j);
            } else {
                ee7.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void n(List<ez5> list) {
        ee7.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showPresets - data=" + list.size());
        yl4 yl4 = this.p;
        if (yl4 != null) {
            yl4.a(new ArrayList<>(list));
            if (!this.u && this.j == 0) {
                this.u = false;
            }
            if (!this.u) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(this.j);
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            }
        } else {
            ee7.d("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(u25 u25) {
        this.r = eh5.l.a().b("nonBrandSwitchDisabledGray");
        this.s = eh5.l.a().b("primaryColor");
        this.t = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        this.p = new yl4(new ArrayList(), this);
        ConstraintLayout constraintLayout = u25.q;
        ee7.a((Object) constraintLayout, "binding.clNoDevice");
        this.h = constraintLayout;
        u25.u.setOnClickListener(this);
        u25.v.setImageResource(2131230943);
        ViewPager2 viewPager2 = u25.y;
        ee7.a((Object) viewPager2, "binding.rvPreset");
        this.i = viewPager2;
        if (viewPager2 != null) {
            if (viewPager2.getChildAt(0) != null) {
                ViewPager2 viewPager22 = this.i;
                if (viewPager22 != null) {
                    View childAt = viewPager22.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setOverScrollMode(2);
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            }
            ViewPager2 viewPager23 = this.i;
            if (viewPager23 != null) {
                yl4 yl4 = this.p;
                if (yl4 != null) {
                    viewPager23.setAdapter(yl4);
                    if (!TextUtils.isEmpty(this.t)) {
                        TabLayout tabLayout = u25.z;
                        ee7.a((Object) tabLayout, "binding.tab");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.t)));
                    }
                    ViewPager2 viewPager24 = this.i;
                    if (viewPager24 != null) {
                        viewPager24.a(new b(this));
                        ViewPager2 viewPager25 = this.i;
                        if (viewPager25 != null) {
                            viewPager25.setCurrentItem(this.j);
                        } else {
                            ee7.d("rvCustomize");
                            throw null;
                        }
                    } else {
                        ee7.d("rvCustomize");
                        throw null;
                    }
                } else {
                    ee7.d("mHybridPresetDetailAdapter");
                    throw null;
                }
            } else {
                ee7.d("rvCustomize");
                throw null;
            }
        } else {
            ee7.d("rvCustomize");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yl4.d
    public void b(boolean z, String str, String str2, String str3) {
        ee7.b(str, "currentPresetName");
        ee7.b(str2, "nextPresetName");
        ee7.b(str3, "nextPresetId");
        if (isActive()) {
            String string = requireActivity().getString(2131886541);
            ee7.a((Object) string, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
            if (z) {
                String string2 = requireActivity().getString(2131886542);
                ee7.a((Object) string2, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
                we7 we7 = we7.a;
                string = String.format(string2, Arrays.copyOf(new Object[]{mh7.d(str2)}, 1));
                ee7.a((Object) string, "java.lang.String.format(format, *args)");
            }
            Bundle bundle = new Bundle();
            bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "nextPresetId " + str3);
            cy6.f fVar = new cy6.f(2131558484);
            we7 we72 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886543);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{str}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            fVar.a(2131363342, format);
            fVar.a(2131363255, string);
            fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886540));
            fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886539));
            fVar.a(2131363307);
            fVar.a(2131363229);
            fVar.a(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.yl4.d
    public void a(ez5 ez5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2, String str, int i2) {
        ee7.b(list, "views");
        ee7.b(list2, "customizeWidgetViews");
        ee7.b(str, "microAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(ez5 != null ? ez5.a() : null);
        sb.append(" microAppPos=");
        sb.append(str);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeHybridCustomizeFragment", sb.toString());
        if (ez5 != null) {
            HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.B;
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            aVar.a(requireActivity, ez5.c(), new ArrayList<>(list), list2, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.yl4.d
    public void a(String str, String str2) {
        ee7.b(str, "presetName");
        ee7.b(str2, "presetId");
        if (getChildFragmentManager().b("RenamePresetDialogFragment") == null) {
            os5 a2 = os5.g.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        String str2;
        ee7.b(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363307) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                ee7.a((Object) str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizeFragment", "onDialogFragmentResult - nextActivePreset " + str2);
            } else {
                str2 = "";
            }
            b56 b56 = this.g;
            if (b56 != null) {
                b56.a(str2);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.c56
    public void a(boolean z) {
        if (z) {
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                ConstraintLayout constraintLayout = this.h;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                } else {
                    ee7.d("clNoDevice");
                    throw null;
                }
            }
            ConstraintLayout constraintLayout2 = this.h;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(0);
            } else {
                ee7.d("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout3 = this.h;
            if (constraintLayout3 != null) {
                constraintLayout3.setVisibility(8);
            } else {
                ee7.d("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(b56 b56) {
        ee7.b(b56, "presenter");
        this.g = b56;
    }
}
