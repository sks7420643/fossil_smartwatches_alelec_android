package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev0 implements Parcelable.Creator<ww0> {
    @DexIgnore
    public /* synthetic */ ev0(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ww0 createFromParcel(Parcel parcel) {
        return new ww0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ww0[] newArray(int i) {
        return new ww0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public ww0 m16createFromParcel(Parcel parcel) {
        return new ww0(parcel, null);
    }
}
