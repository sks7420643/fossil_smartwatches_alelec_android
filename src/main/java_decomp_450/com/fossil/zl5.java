package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl5 implements Factory<yl5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ zl5 a; // = new zl5();
    }

    @DexIgnore
    public static zl5 a() {
        return a.a;
    }

    @DexIgnore
    public static yl5 b() {
        return new yl5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public yl5 get() {
        return b();
    }
}
