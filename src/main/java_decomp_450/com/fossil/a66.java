package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface a66 extends dl4<z56> {
    @DexIgnore
    void I(String str);

    @DexIgnore
    void a(String str, String str2, String str3);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, String str3);

    @DexIgnore
    void b(MicroApp microApp);

    @DexIgnore
    void c(String str);

    @DexIgnore
    void d(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void w(String str);

    @DexIgnore
    void w(List<MicroApp> list);
}
