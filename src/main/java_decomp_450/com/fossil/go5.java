package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.if5;
import com.fossil.ku7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class go5 extends Fragment implements ku7.a, View.OnKeyListener, if5.b {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public qd5 a;
    @DexIgnore
    public /* final */ pb b; // = new pm4(this);
    @DexIgnore
    public jf5 c;
    @DexIgnore
    public HashMap d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = go5.class.getSimpleName();
        ee7.a((Object) simpleName, "BaseFragment::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.if5.b
    public void E0() {
        FLogger.INSTANCE.getLocal().d(e, "Tracer ended");
    }

    @DexIgnore
    public final void V(String str) {
        ee7.b(str, "view");
        jf5 b2 = qd5.f.b();
        b2.b(str);
        this.c = b2;
        if (b2 != null) {
            b2.a(this);
        }
    }

    @DexIgnore
    public final void W(String str) {
        ee7.b(str, "title");
        FLogger.INSTANCE.getLocal().d(e, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).a(str);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void X(String str) {
        ee7.b(str, "content");
        LayoutInflater layoutInflater = getLayoutInflater();
        ee7.a((Object) layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558826, activity != null ? (ViewGroup) activity.findViewById(2131362169) : null);
        View findViewById = inflate.findViewById(2131362360);
        ee7.a((Object) findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public void Z0() {
        HashMap hashMap = this.d;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(e, "hideProgressDialog");
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).g();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    @Override // com.fossil.if5.b
    public void a0() {
        FLogger.INSTANCE.getLocal().d(e, "Tracer started");
        PortfolioApp.g0.c().a(this.c);
    }

    @DexIgnore
    public final pb a1() {
        return this.b;
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(e, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).o();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final qd5 b1() {
        qd5 qd5 = this.a;
        if (qd5 != null) {
            return qd5;
        }
        ee7.d("mAnalyticHelper");
        throw null;
    }

    @DexIgnore
    public final jf5 c1() {
        return this.c;
    }

    @DexIgnore
    public String d1() {
        return e;
    }

    @DexIgnore
    public boolean e1() {
        if (!isActive() || getParentFragment() == null || !(getParentFragment() instanceof go5)) {
            return false;
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            return ((go5) parentFragment).e1();
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = qd5.f.c();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        jf5 jf5 = this.c;
        if (jf5 != null) {
            jf5.c();
        }
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        ee7.b(view, "view");
        ee7.b(keyEvent, "keyEvent");
        if (keyEvent.getAction() != 1 || i != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(e, "onKey KEYCODE_BACK");
        return e1();
    }

    @DexIgnore
    @Override // com.fossil.g6.b, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        ee7.b(strArr, "permissions");
        ee7.b(iArr, "grantResults");
        super.onRequestPermissionsResult(i, strArr, iArr);
        ku7.a(i, strArr, iArr, this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131362962);
        if (viewGroup != null) {
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                viewGroup.setBackgroundColor(Color.parseColor(b2));
            }
        }
        view.setOnKeyListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i) {
        ee7.b(fragment, "fragment");
        ee7.b(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        nc b2 = childFragmentManager.b();
        ee7.a((Object) b2, "fragmentManager.beginTransaction()");
        b2.a((String) null);
        b2.a(i, fragment, str);
        b2.b();
    }

    @DexIgnore
    public final void b(Fragment fragment, String str, int i) {
        ee7.b(fragment, "fragment");
        ee7.b(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        nc b2 = childFragmentManager.b();
        ee7.a((Object) b2, "fragmentManager.beginTransaction()");
        Fragment b3 = childFragmentManager.b(i);
        if (b3 != null) {
            b2.d(b3);
        }
        b2.a((String) null);
        b2.b(i, fragment, str);
        b2.b();
    }

    @DexIgnore
    @Override // com.fossil.ku7.a
    public void a(int i, List<String> list) {
        ee7.b(list, "perms");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "onPermissionsDenied: perm = " + it.next());
        }
    }

    @DexIgnore
    public final void a(Intent intent, String str) {
        ee7.b(intent, "browserIntent");
        ee7.b(str, "tag");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            FLogger.INSTANCE.getLocal().e(str, "Exception when start url with no browser app");
        }
    }

    @DexIgnore
    @Override // com.fossil.ku7.a
    public void b(int i, List<String> list) {
        ee7.b(list, "perms");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "onPermissionsGranted: perm = " + it.next());
        }
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        ee7.b(str, Constants.EVENT);
        ee7.b(map, "values");
        qd5 qd5 = this.a;
        if (qd5 != null) {
            qd5.a(str, map);
        } else {
            ee7.d("mAnalyticHelper");
            throw null;
        }
    }
}
