package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bz6 {
    Left,
    Right,
    Top,
    Bottom;
    
    @DexIgnore
    public static /* final */ List<bz6> FREEDOM; // = Arrays.asList(values());
    @DexIgnore
    public static /* final */ List<bz6> FREEDOM_NO_BOTTOM; // = Arrays.asList(Top, Left, Right);
    @DexIgnore
    public static /* final */ List<bz6> HORIZONTAL; // = Arrays.asList(Left, Right);
    @DexIgnore
    public static /* final */ List<bz6> VERTICAL; // = Arrays.asList(Top, Bottom);

    @DexIgnore
    public static List<bz6> from(int i) {
        if (i == 0) {
            return FREEDOM;
        }
        if (i == 1) {
            return FREEDOM_NO_BOTTOM;
        }
        if (i == 2) {
            return HORIZONTAL;
        }
        if (i != 3) {
            return FREEDOM;
        }
        return VERTICAL;
    }
}
