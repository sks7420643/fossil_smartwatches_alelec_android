package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cg4 extends jg4 {
    @DexIgnore
    public static volatile cg4[] f;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public cg4() {
        a();
    }

    @DexIgnore
    public static cg4[] b() {
        if (f == null) {
            synchronized (hg4.a) {
                if (f == null) {
                    f = new cg4[0];
                }
            }
        }
        return f;
    }

    @DexIgnore
    public cg4 a() {
        this.a = "";
        this.b = "";
        this.c = lg4.a;
        this.d = "";
        this.e = "";
        return this;
    }

    @DexIgnore
    @Override // com.fossil.jg4
    public cg4 a(gg4 gg4) throws IOException {
        while (true) {
            int j = gg4.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                this.a = gg4.i();
            } else if (j == 18) {
                this.b = gg4.i();
            } else if (j == 26) {
                int a2 = lg4.a(gg4, 26);
                String[] strArr = this.c;
                int length = strArr == null ? 0 : strArr.length;
                int i = a2 + length;
                String[] strArr2 = new String[i];
                if (length != 0) {
                    System.arraycopy(this.c, 0, strArr2, 0, length);
                }
                while (length < i - 1) {
                    strArr2[length] = gg4.i();
                    gg4.j();
                    length++;
                }
                strArr2[length] = gg4.i();
                this.c = strArr2;
            } else if (j == 34) {
                this.d = gg4.i();
            } else if (j == 42) {
                this.e = gg4.i();
            } else if (j == 48) {
                gg4.c();
            } else if (!lg4.b(gg4, j)) {
                return this;
            }
        }
    }
}
