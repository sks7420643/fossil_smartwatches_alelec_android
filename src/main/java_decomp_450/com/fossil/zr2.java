package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zr2<K, V> implements Map.Entry<K, V> {
    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            if (!mr2.a(getKey(), entry.getKey()) || !mr2.a(getValue(), entry.getValue())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract K getKey();

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract V getValue();

    @DexIgnore
    public int hashCode() {
        int i;
        K key = getKey();
        V value = getValue();
        int i2 = 0;
        if (key == null) {
            i = 0;
        } else {
            i = key.hashCode();
        }
        if (value != null) {
            i2 = value.hashCode();
        }
        return i ^ i2;
    }

    @DexIgnore
    public String toString() {
        String valueOf = String.valueOf(getKey());
        String valueOf2 = String.valueOf(getValue());
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf2);
        return sb.toString();
    }
}
