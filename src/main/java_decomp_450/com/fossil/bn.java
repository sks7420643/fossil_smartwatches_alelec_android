package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bn {
    @DexIgnore
    public static li a; // = new a(1, 2);
    @DexIgnore
    public static li b; // = new b(3, 4);
    @DexIgnore
    public static li c; // = new c(4, 5);
    @DexIgnore
    public static li d; // = new d(6, 7);
    @DexIgnore
    public static li e; // = new e(7, 8);
    @DexIgnore
    public static li f; // = new f(8, 9);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends li {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            wiVar.execSQL("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            wiVar.execSQL("DROP TABLE IF EXISTS alarmInfo");
            wiVar.execSQL("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends li {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            if (Build.VERSION.SDK_INT >= 23) {
                wiVar.execSQL("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends li {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            wiVar.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends li {
        @DexIgnore
        public d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends li {
        @DexIgnore
        public e(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `workspec` (`period_start_time`)");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends li {
        @DexIgnore
        public f(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("ALTER TABLE workspec ADD COLUMN `run_in_foreground` INTEGER NOT NULL DEFAULT 0");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends li {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public g(Context context, int i, int i2) {
            super(i, i2);
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            if (((li) this).endVersion >= 10) {
                wiVar.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", 1});
                return;
            }
            this.a.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends li {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public h(Context context) {
            super(9, 10);
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
            kp.a(this.a, wiVar);
            ip.a(this.a, wiVar);
        }
    }
}
