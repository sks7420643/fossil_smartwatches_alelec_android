package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ub4 implements Runnable {
    @DexIgnore
    public /* final */ vb4 a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public ub4(vb4 vb4, boolean z) {
        this.a = vb4;
        this.b = z;
    }

    @DexIgnore
    public static Runnable a(vb4 vb4, boolean z) {
        return new ub4(vb4, z);
    }

    @DexIgnore
    public void run() {
        this.a.c(this.b);
    }
}
