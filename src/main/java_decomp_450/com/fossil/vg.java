package com.fossil;

import android.graphics.PointF;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vg extends eh {
    @DexIgnore
    public zg d;
    @DexIgnore
    public zg e;

    @DexIgnore
    @Override // com.fossil.eh
    public int[] a(RecyclerView.m mVar, View view) {
        int[] iArr = new int[2];
        if (mVar.a()) {
            iArr[0] = a(mVar, view, d(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.b()) {
            iArr[1] = a(mVar, view, e(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    public final View b(RecyclerView.m mVar, zg zgVar) {
        int e2 = mVar.e();
        View view = null;
        if (e2 == 0) {
            return null;
        }
        int f = zgVar.f() + (zgVar.g() / 2);
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < e2; i2++) {
            View d2 = mVar.d(i2);
            int abs = Math.abs((zgVar.d(d2) + (zgVar.b(d2) / 2)) - f);
            if (abs < i) {
                view = d2;
                i = abs;
            }
        }
        return view;
    }

    @DexIgnore
    @Override // com.fossil.eh
    public View c(RecyclerView.m mVar) {
        if (mVar.b()) {
            return b(mVar, e(mVar));
        }
        if (mVar.a()) {
            return b(mVar, d(mVar));
        }
        return null;
    }

    @DexIgnore
    public final zg d(RecyclerView.m mVar) {
        zg zgVar = this.e;
        if (zgVar == null || zgVar.a != mVar) {
            this.e = zg.a(mVar);
        }
        return this.e;
    }

    @DexIgnore
    public final zg e(RecyclerView.m mVar) {
        zg zgVar = this.d;
        if (zgVar == null || zgVar.a != mVar) {
            this.d = zg.b(mVar);
        }
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.eh
    public int a(RecyclerView.m mVar, int i, int i2) {
        int j;
        View c;
        int l;
        int i3;
        PointF a;
        int i4;
        int i5;
        if (!(mVar instanceof RecyclerView.v.b) || (j = mVar.j()) == 0 || (c = c(mVar)) == null || (l = mVar.l(c)) == -1 || (a = ((RecyclerView.v.b) mVar).a(j - 1)) == null) {
            return -1;
        }
        int i6 = 0;
        if (mVar.a()) {
            i4 = a(mVar, d(mVar), i, 0);
            if (a.x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i4 = -i4;
            }
        } else {
            i4 = 0;
        }
        if (mVar.b()) {
            i5 = a(mVar, e(mVar), 0, i2);
            if (a.y < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i5 = -i5;
            }
        } else {
            i5 = 0;
        }
        if (mVar.b()) {
            i4 = i5;
        }
        if (i4 == 0) {
            return -1;
        }
        int i7 = l + i4;
        if (i7 >= 0) {
            i6 = i7;
        }
        return i6 >= j ? i3 : i6;
    }

    @DexIgnore
    public final int a(RecyclerView.m mVar, View view, zg zgVar) {
        return (zgVar.d(view) + (zgVar.b(view) / 2)) - (zgVar.f() + (zgVar.g() / 2));
    }

    @DexIgnore
    public final int a(RecyclerView.m mVar, zg zgVar, int i, int i2) {
        int[] b = b(i, i2);
        float a = a(mVar, zgVar);
        if (a <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return 0;
        }
        return Math.round(((float) (Math.abs(b[0]) > Math.abs(b[1]) ? b[0] : b[1])) / a);
    }

    @DexIgnore
    public final float a(RecyclerView.m mVar, zg zgVar) {
        int max;
        int e2 = mVar.e();
        if (e2 == 0) {
            return 1.0f;
        }
        View view = null;
        View view2 = null;
        int i = Integer.MAX_VALUE;
        int i2 = RecyclerView.UNDEFINED_DURATION;
        for (int i3 = 0; i3 < e2; i3++) {
            View d2 = mVar.d(i3);
            int l = mVar.l(d2);
            if (l != -1) {
                if (l < i) {
                    view = d2;
                    i = l;
                }
                if (l > i2) {
                    view2 = d2;
                    i2 = l;
                }
            }
        }
        if (view == null || view2 == null || (max = Math.max(zgVar.a(view), zgVar.a(view2)) - Math.min(zgVar.d(view), zgVar.d(view2))) == 0) {
            return 1.0f;
        }
        return (((float) max) * 1.0f) / ((float) ((i2 - i) + 1));
    }
}
