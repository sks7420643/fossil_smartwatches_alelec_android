package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il6 implements Factory<hl6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public il6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static il6 a(Provider<ThemeRepository> provider) {
        return new il6(provider);
    }

    @DexIgnore
    public static hl6 a(ThemeRepository themeRepository) {
        return new hl6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public hl6 get() {
        return a(this.a.get());
    }
}
