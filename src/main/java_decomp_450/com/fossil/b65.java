package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b65 extends a65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i T; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131362110, 1);
        U.put(2131362636, 2);
        U.put(2131363342, 3);
        U.put(2131362083, 4);
        U.put(2131362637, 5);
        U.put(2131362374, 6);
        U.put(2131362373, 7);
        U.put(2131362751, 8);
        U.put(2131362371, 9);
        U.put(2131362369, 10);
        U.put(2131362372, 11);
        U.put(2131362370, 12);
        U.put(2131362705, 13);
        U.put(2131362102, 14);
        U.put(2131362085, 15);
        U.put(2131362886, 16);
        U.put(2131362476, 17);
        U.put(2131362417, 18);
        U.put(2131362057, 19);
        U.put(2131363004, 20);
        U.put(2131362153, 21);
        U.put(2131362024, 22);
        U.put(2131363386, 23);
        U.put(2131362694, 24);
        U.put(2131362425, 25);
        U.put(2131362989, 26);
        U.put(2131362424, 27);
    }
    */

    @DexIgnore
    public b65(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 28, T, U));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.S = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.S != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.S = 1;
        }
        g();
    }

    @DexIgnore
    public b65(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[22], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[15], (LinearLayout) objArr[14], (ConstraintLayout) objArr[1], (TabLayout) objArr[21], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[17], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (ImageView) objArr[24], (RTLImageView) objArr[13], (View) objArr[8], (FlexibleProgressBar) objArr[16], (ConstraintLayout) objArr[0], (ViewPager2) objArr[26], (ViewPager2) objArr[20], (FlexibleTextView) objArr[3], (View) objArr[23]);
        this.S = -1;
        ((a65) this).N.setTag(null);
        a(view);
        f();
    }
}
