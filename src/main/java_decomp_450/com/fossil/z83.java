package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z83 {
    @DexIgnore
    public static ym2 a;

    @DexIgnore
    public static void a(ym2 ym2) {
        if (a == null) {
            a72.a(ym2);
            a = ym2;
        }
    }

    @DexIgnore
    public static ym2 b() {
        ym2 ym2 = a;
        a72.a(ym2, "IBitmapDescriptorFactory is not initialized");
        return ym2;
    }

    @DexIgnore
    public static y83 a(String str) {
        try {
            return new y83(b().zza(str));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static y83 a() {
        try {
            return new y83(b().d());
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static y83 a(float f) {
        try {
            return new y83(b().d(f));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public static y83 a(Bitmap bitmap) {
        try {
            return new y83(b().zza(bitmap));
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
