package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.firebase.iid.FirebaseInstanceId;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class md4 {
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.HOURS.toSeconds(8);
    @DexIgnore
    public /* final */ FirebaseInstanceId a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ta4 c;
    @DexIgnore
    public /* final */ ga4 d;
    @DexIgnore
    public /* final */ Map<String, ArrayDeque<oo3<Void>>> e; // = new n4();
    @DexIgnore
    public /* final */ ScheduledExecutorService f;
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public /* final */ kd4 h;

    @DexIgnore
    public md4(FirebaseInstanceId firebaseInstanceId, ta4 ta4, kd4 kd4, ga4 ga4, Context context, ScheduledExecutorService scheduledExecutorService) {
        this.a = firebaseInstanceId;
        this.c = ta4;
        this.h = kd4;
        this.d = ga4;
        this.b = context;
        this.f = scheduledExecutorService;
    }

    @DexIgnore
    public static no3<md4> a(l14 l14, FirebaseInstanceId firebaseInstanceId, ta4 ta4, vd4 vd4, l94 l94, wb4 wb4, Context context, ScheduledExecutorService scheduledExecutorService) {
        return a(firebaseInstanceId, ta4, new ga4(l14, ta4, vd4, l94, wb4), context, scheduledExecutorService);
    }

    @DexIgnore
    public static boolean f() {
        if (!Log.isLoggable("FirebaseMessaging", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3);
        }
        return true;
    }

    @DexIgnore
    public boolean b(jd4 jd4) throws IOException {
        try {
            String a2 = jd4.a();
            char c2 = '\uffff';
            int hashCode = a2.hashCode();
            if (hashCode != 83) {
                if (hashCode == 85) {
                    if (a2.equals("U")) {
                        c2 = 1;
                    }
                }
            } else if (a2.equals(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX)) {
                c2 = 0;
            }
            if (c2 == 0) {
                a(jd4.b());
                if (f()) {
                    String b2 = jd4.b();
                    StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 31);
                    sb.append("Subscribe to topic: ");
                    sb.append(b2);
                    sb.append(" succeeded.");
                    Log.d("FirebaseMessaging", sb.toString());
                }
            } else if (c2 == 1) {
                b(jd4.b());
                if (f()) {
                    String b3 = jd4.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b3).length() + 35);
                    sb2.append("Unsubscribe from topic: ");
                    sb2.append(b3);
                    sb2.append(" succeeded.");
                    Log.d("FirebaseMessaging", sb2.toString());
                }
            } else if (f()) {
                String valueOf = String.valueOf(jd4);
                StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf).length() + 24);
                sb3.append("Unknown topic operation");
                sb3.append(valueOf);
                sb3.append(CodelessMatcher.CURRENT_CLASS_NAME);
                Log.d("FirebaseMessaging", sb3.toString());
            }
            return true;
        } catch (IOException e2) {
            if ("SERVICE_NOT_AVAILABLE".equals(e2.getMessage()) || "INTERNAL_SERVER_ERROR".equals(e2.getMessage())) {
                String message = e2.getMessage();
                StringBuilder sb4 = new StringBuilder(String.valueOf(message).length() + 53);
                sb4.append("Topic operation failed: ");
                sb4.append(message);
                sb4.append(". Will retry Topic operation.");
                Log.e("FirebaseMessaging", sb4.toString());
                return false;
            } else if (e2.getMessage() == null) {
                Log.e("FirebaseMessaging", "Topic operation failed without exception message. Will retry Topic operation.");
                return false;
            } else {
                throw e2;
            }
        }
    }

    @DexIgnore
    public final void c() {
        if (!b()) {
            a(0);
        }
    }

    @DexIgnore
    public void d() {
        if (a()) {
            c();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        if (b(r0) != false) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        return false;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() throws java.io.IOException {
        /*
            r2 = this;
        L_0x0000:
            monitor-enter(r2)
            com.fossil.kd4 r0 = r2.h     // Catch:{ all -> 0x002b }
            com.fossil.jd4 r0 = r0.a()     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0019
            boolean r0 = f()     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = "FirebaseMessaging"
            java.lang.String r1 = "topic sync succeeded"
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x002b }
        L_0x0016:
            r0 = 1
            monitor-exit(r2)     // Catch:{ all -> 0x002b }
            return r0
        L_0x0019:
            monitor-exit(r2)     // Catch:{ all -> 0x002b }
            boolean r1 = r2.b(r0)
            if (r1 != 0) goto L_0x0022
            r0 = 0
            return r0
        L_0x0022:
            com.fossil.kd4 r1 = r2.h
            r1.a(r0)
            r2.a(r0)
            goto L_0x0000
        L_0x002b:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.md4.e():boolean");
    }

    @DexIgnore
    public static no3<md4> a(FirebaseInstanceId firebaseInstanceId, ta4 ta4, ga4 ga4, Context context, ScheduledExecutorService scheduledExecutorService) {
        return qo3.a(scheduledExecutorService, new ld4(context, scheduledExecutorService, firebaseInstanceId, ta4, ga4));
    }

    @DexIgnore
    public boolean a() {
        return this.h.a() != null;
    }

    @DexIgnore
    public void a(long j) {
        a(new nd4(this, this.b, this.c, Math.min(Math.max(30L, j << 1), i)), j);
        a(true);
    }

    @DexIgnore
    public void a(Runnable runnable, long j) {
        this.f.schedule(runnable, j, TimeUnit.SECONDS);
    }

    @DexIgnore
    public final void a(jd4 jd4) {
        synchronized (this.e) {
            String c2 = jd4.c();
            if (this.e.containsKey(c2)) {
                ArrayDeque<oo3<Void>> arrayDeque = this.e.get(c2);
                oo3<Void> poll = arrayDeque.poll();
                if (poll != null) {
                    poll.a((Void) null);
                }
                if (arrayDeque.isEmpty()) {
                    this.e.remove(c2);
                }
            }
        }
    }

    @DexIgnore
    public final void b(String str) throws IOException {
        ka4 ka4 = (ka4) a(this.a.g());
        a(this.d.c(ka4.getId(), ka4.a(), str));
    }

    @DexIgnore
    public final void a(String str) throws IOException {
        ka4 ka4 = (ka4) a(this.a.g());
        a(this.d.b(ka4.getId(), ka4.a(), str));
    }

    @DexIgnore
    public synchronized boolean b() {
        return this.g;
    }

    @DexIgnore
    public static <T> T a(no3<T> no3) throws IOException {
        try {
            return (T) qo3.a(no3, 30, TimeUnit.SECONDS);
        } catch (ExecutionException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof IOException) {
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e2);
            }
        } catch (InterruptedException | TimeoutException e3) {
            throw new IOException("SERVICE_NOT_AVAILABLE", e3);
        }
    }

    @DexIgnore
    public synchronized void a(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public static final /* synthetic */ md4 a(Context context, ScheduledExecutorService scheduledExecutorService, FirebaseInstanceId firebaseInstanceId, ta4 ta4, ga4 ga4) throws Exception {
        return new md4(firebaseInstanceId, ta4, kd4.a(context, scheduledExecutorService), ga4, context, scheduledExecutorService);
    }
}
