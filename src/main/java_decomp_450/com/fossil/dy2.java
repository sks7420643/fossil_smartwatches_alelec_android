package com.fossil;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dy2<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public List<my2> b;
    @DexIgnore
    public Map<K, V> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public volatile oy2 e;
    @DexIgnore
    public Map<K, V> f;
    @DexIgnore
    public volatile iy2 g;

    @DexIgnore
    public dy2(int i) {
        this.a = i;
        this.b = Collections.emptyList();
        this.c = Collections.emptyMap();
        this.f = Collections.emptyMap();
    }

    @DexIgnore
    public static <FieldDescriptorType extends sv2<FieldDescriptorType>> dy2<FieldDescriptorType, Object> c(int i) {
        return new gy2(i);
    }

    @DexIgnore
    public void a() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.d) {
            if (this.c.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.c);
            }
            this.c = map;
            if (this.f.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.f);
            }
            this.f = map2;
            this.d = true;
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.d;
    }

    @DexIgnore
    public void clear() {
        f();
        if (!this.b.isEmpty()) {
            this.b.clear();
        }
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.dy2<K extends java.lang.Comparable<K>, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.Comparable */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.c.containsKey(comparable);
    }

    @DexIgnore
    public final Iterable<Map.Entry<K, V>> d() {
        if (this.c.isEmpty()) {
            return hy2.a();
        }
        return this.c.entrySet();
    }

    @DexIgnore
    public final Set<Map.Entry<K, V>> e() {
        if (this.g == null) {
            this.g = new iy2(this, null);
        }
        return this.g;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.e == null) {
            this.e = new oy2(this, null);
        }
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dy2)) {
            return super.equals(obj);
        }
        dy2 dy2 = (dy2) obj;
        int size = size();
        if (size != dy2.size()) {
            return false;
        }
        int c2 = c();
        if (c2 != dy2.c()) {
            return entrySet().equals(dy2.entrySet());
        }
        for (int i = 0; i < c2; i++) {
            if (!a(i).equals(dy2.a(i))) {
                return false;
            }
        }
        if (c2 != size) {
            return this.c.equals(dy2.c);
        }
        return true;
    }

    @DexIgnore
    public final void f() {
        if (this.d) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final SortedMap<K, V> g() {
        f();
        if (this.c.isEmpty() && !(this.c instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.c = treeMap;
            this.f = treeMap.descendingMap();
        }
        return (SortedMap) this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.dy2<K extends java.lang.Comparable<K>, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.Comparable */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        return a2 >= 0 ? (V) this.b.get(a2).getValue() : this.c.get(comparable);
    }

    @DexIgnore
    public int hashCode() {
        int c2 = c();
        int i = 0;
        for (int i2 = 0; i2 < c2; i2++) {
            i += this.b.get(i2).hashCode();
        }
        return this.c.size() > 0 ? i + this.c.hashCode() : i;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.dy2<K extends java.lang.Comparable<K>, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.Comparable */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        f();
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return (V) b(a2);
        }
        if (this.c.isEmpty()) {
            return null;
        }
        return this.c.remove(comparable);
    }

    @DexIgnore
    public int size() {
        return this.b.size() + this.c.size();
    }

    @DexIgnore
    public final V b(int i) {
        f();
        V v = (V) this.b.remove(i).getValue();
        if (!this.c.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = g().entrySet().iterator();
            this.b.add(new my2(this, it.next()));
            it.remove();
        }
        return v;
    }

    @DexIgnore
    public final int c() {
        return this.b.size();
    }

    @DexIgnore
    public /* synthetic */ dy2(int i, gy2 gy2) {
        this(i);
    }

    @DexIgnore
    public final Map.Entry<K, V> a(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    /* renamed from: a */
    public final V put(K k, V v) {
        f();
        int a2 = a(k);
        if (a2 >= 0) {
            return (V) this.b.get(a2).setValue(v);
        }
        f();
        if (this.b.isEmpty() && !(this.b instanceof ArrayList)) {
            this.b = new ArrayList(this.a);
        }
        int i = -(a2 + 1);
        if (i >= this.a) {
            return g().put(k, v);
        }
        int size = this.b.size();
        int i2 = this.a;
        if (size == i2) {
            my2 remove = this.b.remove(i2 - 1);
            g().put((K) ((Comparable) remove.getKey()), (V) remove.getValue());
        }
        this.b.add(i, new my2(this, k, v));
        return null;
    }

    @DexIgnore
    public final int a(K k) {
        int size = this.b.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.b.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo((Comparable) this.b.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }
}
