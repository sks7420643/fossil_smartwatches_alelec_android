package com.fossil;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bh extends g9 {
    @DexIgnore
    public /* final */ RecyclerView d;
    @DexIgnore
    public /* final */ a e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends g9 {
        @DexIgnore
        public /* final */ bh d;
        @DexIgnore
        public Map<View, g9> e; // = new WeakHashMap();

        @DexIgnore
        public a(bh bhVar) {
            this.d = bhVar;
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void a(View view, oa oaVar) {
            if (this.d.c() || this.d.d.getLayoutManager() == null) {
                super.a(view, oaVar);
                return;
            }
            this.d.d.getLayoutManager().a(view, oaVar);
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                g9Var.a(view, oaVar);
            } else {
                super.a(view, oaVar);
            }
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void b(View view, AccessibilityEvent accessibilityEvent) {
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                g9Var.b(view, accessibilityEvent);
            } else {
                super.b(view, accessibilityEvent);
            }
        }

        @DexIgnore
        public g9 c(View view) {
            return this.e.remove(view);
        }

        @DexIgnore
        public void d(View view) {
            g9 b = da.b(view);
            if (b != null && b != this) {
                this.e.put(view, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void c(View view, AccessibilityEvent accessibilityEvent) {
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                g9Var.c(view, accessibilityEvent);
            } else {
                super.c(view, accessibilityEvent);
            }
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void d(View view, AccessibilityEvent accessibilityEvent) {
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                g9Var.d(view, accessibilityEvent);
            } else {
                super.d(view, accessibilityEvent);
            }
        }

        @DexIgnore
        @Override // com.fossil.g9
        public boolean a(View view, int i, Bundle bundle) {
            if (this.d.c() || this.d.d.getLayoutManager() == null) {
                return super.a(view, i, bundle);
            }
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                if (g9Var.a(view, i, bundle)) {
                    return true;
                }
            } else if (super.a(view, i, bundle)) {
                return true;
            }
            return this.d.d.getLayoutManager().a(view, i, bundle);
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void a(View view, int i) {
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                g9Var.a(view, i);
            } else {
                super.a(view, i);
            }
        }

        @DexIgnore
        @Override // com.fossil.g9
        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                return g9Var.a(view, accessibilityEvent);
            }
            return super.a(view, accessibilityEvent);
        }

        @DexIgnore
        @Override // com.fossil.g9
        public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            g9 g9Var = this.e.get(viewGroup);
            if (g9Var != null) {
                return g9Var.a(viewGroup, view, accessibilityEvent);
            }
            return super.a(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        @Override // com.fossil.g9
        public pa a(View view) {
            g9 g9Var = this.e.get(view);
            if (g9Var != null) {
                return g9Var.a(view);
            }
            return super.a(view);
        }
    }

    @DexIgnore
    public bh(RecyclerView recyclerView) {
        this.d = recyclerView;
        g9 b = b();
        if (b == null || !(b instanceof a)) {
            this.e = new a(this);
        } else {
            this.e = (a) b;
        }
    }

    @DexIgnore
    @Override // com.fossil.g9
    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        if (c() || this.d.getLayoutManager() == null) {
            return false;
        }
        return this.d.getLayoutManager().a(i, bundle);
    }

    @DexIgnore
    @Override // com.fossil.g9
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        if ((view instanceof RecyclerView) && !c()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    @DexIgnore
    public boolean c() {
        return this.d.hasPendingAdapterUpdates();
    }

    @DexIgnore
    @Override // com.fossil.g9
    public void a(View view, oa oaVar) {
        super.a(view, oaVar);
        if (!c() && this.d.getLayoutManager() != null) {
            this.d.getLayoutManager().a(oaVar);
        }
    }

    @DexIgnore
    public g9 b() {
        return this.e;
    }
}
