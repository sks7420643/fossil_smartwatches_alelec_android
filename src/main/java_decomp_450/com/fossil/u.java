package com.fossil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.t;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public class u implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<u> CREATOR; // = new a();
    @DexIgnore
    public /* final */ boolean a; // = false;
    @DexIgnore
    public /* final */ Handler b; // = null;
    @DexIgnore
    public t c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Parcelable.Creator<u> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public u createFromParcel(Parcel parcel) {
            return new u(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public u[] newArray(int i) {
            return new u[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends t.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.t
        public void b(int i, Bundle bundle) {
            u uVar = u.this;
            Handler handler = uVar.b;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                uVar.a(i, bundle);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Bundle b;

        @DexIgnore
        public c(int i, Bundle bundle) {
            this.a = i;
            this.b = bundle;
        }

        @DexIgnore
        public void run() {
            u.this.a(this.a, this.b);
        }
    }

    @DexIgnore
    public u(Parcel parcel) {
        this.c = t.a.a(parcel.readStrongBinder());
    }

    @DexIgnore
    public void a(int i, Bundle bundle) {
    }

    @DexIgnore
    public void b(int i, Bundle bundle) {
        if (this.a) {
            Handler handler = this.b;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                a(i, bundle);
            }
        } else {
            t tVar = this.c;
            if (tVar != null) {
                try {
                    tVar.b(i, bundle);
                } catch (RemoteException unused) {
                }
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.c == null) {
                this.c = new b();
            }
            parcel.writeStrongBinder(this.c.asBinder());
        }
    }
}
