package com.fossil;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x52 implements Parcelable.Creator<Status> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Status createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        PendingIntent pendingIntent = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i2 = j72.q(parcel, a);
            } else if (a2 == 2) {
                str = j72.e(parcel, a);
            } else if (a2 == 3) {
                pendingIntent = (PendingIntent) j72.a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 1000) {
                j72.v(parcel, a);
            } else {
                i = j72.q(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new Status(i, i2, str, pendingIntent);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Status[] newArray(int i) {
        return new Status[i];
    }
}
