package com.fossil;

import androidx.work.ListenableWorker;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sm {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public zo b;
    @DexIgnore
    public Set<String> c;

    @DexIgnore
    public sm(UUID uuid, zo zoVar, Set<String> set) {
        this.a = uuid;
        this.b = zoVar;
        this.c = set;
    }

    @DexIgnore
    public UUID a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.a.toString();
    }

    @DexIgnore
    public Set<String> c() {
        return this.c;
    }

    @DexIgnore
    public zo d() {
        return this.b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<B extends a<?, ?>, W extends sm> {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public UUID b; // = UUID.randomUUID();
        @DexIgnore
        public zo c;
        @DexIgnore
        public Set<String> d; // = new HashSet();

        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            this.c = new zo(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.sm$a<B extends com.fossil.sm$a<?, ?>, W extends com.fossil.sm> */
        /* JADX WARN: Multi-variable type inference failed */
        public final B a(am amVar) {
            this.c.j = amVar;
            c();
            return this;
        }

        @DexIgnore
        public abstract W b();

        @DexIgnore
        public abstract B c();

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.sm$a<B extends com.fossil.sm$a<?, ?>, W extends com.fossil.sm> */
        /* JADX WARN: Multi-variable type inference failed */
        public final B a(String str) {
            this.d.add(str);
            c();
            return this;
        }

        @DexIgnore
        public final W a() {
            W b2 = b();
            this.b = UUID.randomUUID();
            zo zoVar = new zo(this.c);
            this.c = zoVar;
            zoVar.a = this.b.toString();
            return b2;
        }
    }
}
