package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum k90 {
    PLAY((byte) 0),
    PAUSE((byte) 1),
    TOGGLE_PLAY_PAUSE((byte) 2),
    NEXT((byte) 3),
    PREVIOUS((byte) 4),
    VOLUME_UP((byte) 5),
    VOLUME_DOWN((byte) 6);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final k90 a(byte b) {
            k90[] values = k90.values();
            for (k90 k90 : values) {
                if (k90.a() == b) {
                    return k90;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public k90(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
