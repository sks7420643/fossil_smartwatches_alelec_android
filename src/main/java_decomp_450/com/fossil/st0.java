package com.fossil;

import android.bluetooth.BluetoothGatt;
import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st0 extends eo0 {
    @DexIgnore
    public /* final */ nm1 j; // = nm1.DAEMON;
    @DexIgnore
    public /* final */ qy0<s91> k;

    @DexIgnore
    public st0(cx0 cx0) {
        super(aq0.a, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        BluetoothGatt bluetoothGatt = ri1.b;
        if (bluetoothGatt != null) {
            if (ri1.v) {
                ri1.a(bluetoothGatt);
            }
            le0 le0 = le0.DEBUG;
            bluetoothGatt.close();
        }
        ri1.b = null;
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            new Handler(myLooper).postDelayed(new wr0(this), 1000);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public nm1 b() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return this.k;
    }
}
