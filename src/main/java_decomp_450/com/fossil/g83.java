package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g83 extends tm2 implements c73 {
    @DexIgnore
    public g83(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean B() throws RemoteException {
        Parcel a = a(12, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean D() throws RemoteException {
        Parcel a = a(9, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean i() throws RemoteException {
        Parcel a = a(15, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean k() throws RemoteException {
        Parcel a = a(14, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean l() throws RemoteException {
        Parcel a = a(13, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean o() throws RemoteException {
        Parcel a = a(10, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean p() throws RemoteException {
        Parcel a = a(11, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setCompassEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(2, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setMapToolbarEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(18, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setMyLocationButtonEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(3, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setRotateGesturesEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(7, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setScrollGesturesEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(4, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setTiltGesturesEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(6, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setZoomControlsEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(1, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final void setZoomGesturesEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(5, zza);
    }

    @DexIgnore
    @Override // com.fossil.c73
    public final boolean t() throws RemoteException {
        Parcel a = a(19, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }
}
