package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i10 implements dx<Bitmap> {
    @DexIgnore
    public static /* final */ zw<Integer> b; // = zw.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", (Object) 90);
    @DexIgnore
    public static /* final */ zw<Bitmap.CompressFormat> c; // = zw.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
    @DexIgnore
    public /* final */ az a;

    @DexIgnore
    public i10(az azVar) {
        this.a = azVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0066, code lost:
        if (r6 == null) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0061 A[Catch:{ all -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bc A[SYNTHETIC, Splitter:B:34:0x00bc] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.fossil.uy<android.graphics.Bitmap> r9, java.io.File r10, com.fossil.ax r11) {
        /*
            r8 = this;
            java.lang.String r0 = "BitmapEncoder"
            java.lang.Object r9 = r9.get()
            android.graphics.Bitmap r9 = (android.graphics.Bitmap) r9
            android.graphics.Bitmap$CompressFormat r1 = r8.a(r9, r11)
            int r2 = r9.getWidth()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r3 = r9.getHeight()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            java.lang.String r4 = "encode: [%dx%d] %s"
            com.fossil.x50.a(r4, r2, r3, r1)
            long r2 = com.fossil.q50.a()     // Catch:{ all -> 0x00c0 }
            com.fossil.zw<java.lang.Integer> r4 = com.fossil.i10.b     // Catch:{ all -> 0x00c0 }
            java.lang.Object r4 = r11.a(r4)     // Catch:{ all -> 0x00c0 }
            java.lang.Integer r4 = (java.lang.Integer) r4     // Catch:{ all -> 0x00c0 }
            int r4 = r4.intValue()     // Catch:{ all -> 0x00c0 }
            r5 = 0
            r6 = 0
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0059 }
            r7.<init>(r10)     // Catch:{ IOException -> 0x0059 }
            com.fossil.az r10 = r8.a     // Catch:{ IOException -> 0x0054, all -> 0x0051 }
            if (r10 == 0) goto L_0x0045
            com.fossil.hx r10 = new com.fossil.hx     // Catch:{ IOException -> 0x0054, all -> 0x0051 }
            com.fossil.az r6 = r8.a     // Catch:{ IOException -> 0x0054, all -> 0x0051 }
            r10.<init>(r7, r6)     // Catch:{ IOException -> 0x0054, all -> 0x0051 }
            r6 = r10
            goto L_0x0046
        L_0x0045:
            r6 = r7
        L_0x0046:
            r9.compress(r1, r4, r6)
            r6.close()
            r5 = 1
        L_0x004d:
            r6.close()     // Catch:{ IOException -> 0x0069 }
            goto L_0x0069
        L_0x0051:
            r9 = move-exception
            r6 = r7
            goto L_0x00ba
        L_0x0054:
            r10 = move-exception
            r6 = r7
            goto L_0x005a
        L_0x0057:
            r9 = move-exception
            goto L_0x00ba
        L_0x0059:
            r10 = move-exception
        L_0x005a:
            r4 = 3
            boolean r4 = android.util.Log.isLoggable(r0, r4)     // Catch:{ all -> 0x0057 }
            if (r4 == 0) goto L_0x0066
            java.lang.String r4 = "Failed to encode Bitmap"
            android.util.Log.d(r0, r4, r10)     // Catch:{ all -> 0x0057 }
        L_0x0066:
            if (r6 == 0) goto L_0x0069
            goto L_0x004d
        L_0x0069:
            r10 = 2
            boolean r10 = android.util.Log.isLoggable(r0, r10)
            if (r10 == 0) goto L_0x00b6
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r4 = "Compressed with type: "
            r10.append(r4)
            r10.append(r1)
            java.lang.String r1 = " of size "
            r10.append(r1)
            int r1 = com.fossil.v50.a(r9)
            r10.append(r1)
            java.lang.String r1 = " in "
            r10.append(r1)
            double r1 = com.fossil.q50.a(r2)
            r10.append(r1)
            java.lang.String r1 = ", options format: "
            r10.append(r1)
            com.fossil.zw<android.graphics.Bitmap$CompressFormat> r1 = com.fossil.i10.c
            java.lang.Object r11 = r11.a(r1)
            r10.append(r11)
            java.lang.String r11 = ", hasAlpha: "
            r10.append(r11)
            boolean r9 = r9.hasAlpha()
            r10.append(r9)
            java.lang.String r9 = r10.toString()
            android.util.Log.v(r0, r9)
        L_0x00b6:
            com.fossil.x50.a()
            return r5
        L_0x00ba:
            if (r6 == 0) goto L_0x00bf
            r6.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00bf:
            throw r9
        L_0x00c0:
            r9 = move-exception
            com.fossil.x50.a()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i10.a(com.fossil.uy, java.io.File, com.fossil.ax):boolean");
    }

    @DexIgnore
    public final Bitmap.CompressFormat a(Bitmap bitmap, ax axVar) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) axVar.a(c);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @DexIgnore
    @Override // com.fossil.dx
    public uw a(ax axVar) {
        return uw.TRANSFORMED;
    }
}
