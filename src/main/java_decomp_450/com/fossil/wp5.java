package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp5 extends RecyclerView.g<c> implements Filterable {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public List<at5> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public List<at5> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return wp5.d;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(at5 at5, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ k85 a;
        @DexIgnore
        public /* final */ /* synthetic */ wp5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.a.b.c;
                    if (b == null) {
                        ee7.a();
                        throw null;
                    } else if (((at5) b.get(adapterPosition)).getUri() != null) {
                        List b2 = this.a.b.c;
                        if (b2 != null) {
                            InstalledApp installedApp = ((at5) b2.get(adapterPosition)).getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected != null) {
                                boolean booleanValue = isSelected.booleanValue();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a2 = wp5.e.a();
                                local.d(a2, "isSelected = " + booleanValue);
                                b c = this.a.b.b;
                                if (c != null) {
                                    List b3 = this.a.b.c;
                                    if (b3 != null) {
                                        c.a((at5) b3.get(adapterPosition), !booleanValue);
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(wp5 wp5, k85 k85) {
            super(k85.d());
            ee7.b(k85, "binding");
            this.b = wp5;
            this.a = k85;
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = eh5.l.a().b("nonBrandSeparatorLine");
            if (b2 != null) {
                this.a.q.setBackgroundColor(Color.parseColor(b2));
            }
            if (b3 != null) {
                this.a.t.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.u.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(at5 at5) {
            ee7.b(at5, "appWrapper");
            ImageView imageView = this.a.s;
            ee7.a((Object) imageView, "binding.ivAppIcon");
            kd5 a2 = hd5.a(imageView.getContext());
            InstalledApp installedApp = at5.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new ed5(installedApp)).b(at5.getIconResourceId()).h().a(this.a.s);
                FlexibleTextView flexibleTextView = this.a.r;
                ee7.a((Object) flexibleTextView, "binding.ftvAppName");
                InstalledApp installedApp2 = at5.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                if (at5.getUri() != null) {
                    FlexibleTextView flexibleTextView2 = this.a.r;
                    ee7.a((Object) flexibleTextView2, "binding.ftvAppName");
                    flexibleTextView2.setTextColor(flexibleTextView2.getTextColors().withAlpha(255));
                    FlexibleSwitchCompat flexibleSwitchCompat = this.a.u;
                    ee7.a((Object) flexibleSwitchCompat, "binding.swEnabled");
                    flexibleSwitchCompat.setEnabled(true);
                    FlexibleSwitchCompat flexibleSwitchCompat2 = this.a.u;
                    ee7.a((Object) flexibleSwitchCompat2, "binding.swEnabled");
                    Drawable background = flexibleSwitchCompat2.getBackground();
                    ee7.a((Object) background, "binding.swEnabled.background");
                    background.setAlpha(255);
                } else {
                    FlexibleTextView flexibleTextView3 = this.a.r;
                    ee7.a((Object) flexibleTextView3, "binding.ftvAppName");
                    flexibleTextView3.setTextColor(flexibleTextView3.getTextColors().withAlpha(100));
                    FlexibleSwitchCompat flexibleSwitchCompat3 = this.a.u;
                    ee7.a((Object) flexibleSwitchCompat3, "binding.swEnabled");
                    flexibleSwitchCompat3.setEnabled(false);
                    FlexibleSwitchCompat flexibleSwitchCompat4 = this.a.u;
                    ee7.a((Object) flexibleSwitchCompat4, "binding.swEnabled");
                    Drawable background2 = flexibleSwitchCompat4.getBackground();
                    ee7.a((Object) background2, "binding.swEnabled.background");
                    background2.setAlpha(100);
                }
                FlexibleSwitchCompat flexibleSwitchCompat5 = this.a.u;
                ee7.a((Object) flexibleSwitchCompat5, "binding.swEnabled");
                InstalledApp installedApp3 = at5.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected != null) {
                    flexibleSwitchCompat5.setChecked(isSelected.booleanValue());
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ wp5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(wp5 wp5) {
            this.a = wp5;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            ee7.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.a;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (at5 at5 : this.a.a) {
                        InstalledApp installedApp = at5.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            ee7.a((Object) str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && nh7.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                            arrayList.add(at5);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            ee7.b(charSequence, "charSequence");
            ee7.b(filterResults, "results");
            this.a.c = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    /*
    static {
        String simpleName = wp5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationAppsAdapter::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public wp5() {
        setHasStableIds(true);
    }

    @DexIgnore
    public Filter getFilter() {
        return new d(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<at5> list = this.c;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        k85 a2 = k85.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemAppNotificationBindi\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        List<at5> list = this.c;
        if (list != null) {
            cVar.a(list.get(i));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<at5> list) {
        ee7.b(list, "listAppWrapper");
        this.a.clear();
        this.a.addAll(list);
        this.c = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
