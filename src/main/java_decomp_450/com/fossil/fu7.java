package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fu7 implements st7 {
    @DexIgnore
    public boolean a; // = false;
    @DexIgnore
    public /* final */ Map<String, eu7> b; // = new HashMap();
    @DexIgnore
    public /* final */ LinkedBlockingQueue<zt7> c; // = new LinkedBlockingQueue<>();

    @DexIgnore
    @Override // com.fossil.st7
    public synchronized tt7 a(String str) {
        eu7 eu7;
        eu7 = this.b.get(str);
        if (eu7 == null) {
            eu7 = new eu7(str, this.c, this.a);
            this.b.put(str, eu7);
        }
        return eu7;
    }

    @DexIgnore
    public LinkedBlockingQueue<zt7> b() {
        return this.c;
    }

    @DexIgnore
    public List<eu7> c() {
        return new ArrayList(this.b.values());
    }

    @DexIgnore
    public void d() {
        this.a = true;
    }

    @DexIgnore
    public void a() {
        this.b.clear();
        this.c.clear();
    }
}
