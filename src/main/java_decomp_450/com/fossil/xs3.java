package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs3 {
    @DexIgnore
    public /* final */ Rect a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ hv3 f;

    @DexIgnore
    public xs3(ColorStateList colorStateList, ColorStateList colorStateList2, ColorStateList colorStateList3, int i, hv3 hv3, Rect rect) {
        e9.a(rect.left);
        e9.a(rect.top);
        e9.a(rect.right);
        e9.a(rect.bottom);
        this.a = rect;
        this.b = colorStateList2;
        this.c = colorStateList;
        this.d = colorStateList3;
        this.e = i;
        this.f = hv3;
    }

    @DexIgnore
    public static xs3 a(Context context, int i) {
        e9.a(i != 0, "Cannot create a CalendarItemStyle with a styleResId of 0");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, tr3.MaterialCalendarItem);
        Rect rect = new Rect(obtainStyledAttributes.getDimensionPixelOffset(tr3.MaterialCalendarItem_android_insetLeft, 0), obtainStyledAttributes.getDimensionPixelOffset(tr3.MaterialCalendarItem_android_insetTop, 0), obtainStyledAttributes.getDimensionPixelOffset(tr3.MaterialCalendarItem_android_insetRight, 0), obtainStyledAttributes.getDimensionPixelOffset(tr3.MaterialCalendarItem_android_insetBottom, 0));
        ColorStateList a2 = pu3.a(context, obtainStyledAttributes, tr3.MaterialCalendarItem_itemFillColor);
        ColorStateList a3 = pu3.a(context, obtainStyledAttributes, tr3.MaterialCalendarItem_itemTextColor);
        ColorStateList a4 = pu3.a(context, obtainStyledAttributes, tr3.MaterialCalendarItem_itemStrokeColor);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(tr3.MaterialCalendarItem_itemStrokeWidth, 0);
        hv3 a5 = hv3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendarItem_itemShapeAppearance, 0), obtainStyledAttributes.getResourceId(tr3.MaterialCalendarItem_itemShapeAppearanceOverlay, 0)).a();
        obtainStyledAttributes.recycle();
        return new xs3(a2, a3, a4, dimensionPixelSize, a5, rect);
    }

    @DexIgnore
    public int b() {
        return this.a.top;
    }

    @DexIgnore
    public void a(TextView textView) {
        dv3 dv3 = new dv3();
        dv3 dv32 = new dv3();
        dv3.setShapeAppearanceModel(this.f);
        dv32.setShapeAppearanceModel(this.f);
        dv3.a(this.c);
        dv3.a((float) this.e, this.d);
        textView.setTextColor(this.b);
        Drawable rippleDrawable = Build.VERSION.SDK_INT >= 21 ? new RippleDrawable(this.b.withAlpha(30), dv3, dv32) : dv3;
        Rect rect = this.a;
        da.a(textView, new InsetDrawable(rippleDrawable, rect.left, rect.top, rect.right, rect.bottom));
    }

    @DexIgnore
    public int a() {
        return this.a.bottom;
    }
}
