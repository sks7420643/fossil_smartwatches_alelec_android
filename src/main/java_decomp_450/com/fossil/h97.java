package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h97 extends RuntimeException {
    @DexIgnore
    public h97() {
    }

    @DexIgnore
    public h97(String str) {
        super(str);
    }

    @DexIgnore
    public h97(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public h97(Throwable th) {
        super(th);
    }
}
