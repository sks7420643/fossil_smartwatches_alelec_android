package com.fossil;

import android.content.Context;
import com.fossil.vn;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sn implements vn.a {
    @DexIgnore
    public static /* final */ String d; // = im.a("WorkConstraintsTracker");
    @DexIgnore
    public /* final */ rn a;
    @DexIgnore
    public /* final */ vn<?>[] b;
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public sn(Context context, vp vpVar, rn rnVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = rnVar;
        this.b = new vn[]{new tn(applicationContext, vpVar), new un(applicationContext, vpVar), new ao(applicationContext, vpVar), new wn(applicationContext, vpVar), new zn(applicationContext, vpVar), new yn(applicationContext, vpVar), new xn(applicationContext, vpVar)};
    }

    @DexIgnore
    public void a(Iterable<zo> iterable) {
        synchronized (this.c) {
            for (vn<?> vnVar : this.b) {
                vnVar.a((vn.a) null);
            }
            for (vn<?> vnVar2 : this.b) {
                vnVar2.a(iterable);
            }
            for (vn<?> vnVar3 : this.b) {
                vnVar3.a((vn.a) this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.vn.a
    public void b(List<String> list) {
        synchronized (this.c) {
            if (this.a != null) {
                this.a.a(list);
            }
        }
    }

    @DexIgnore
    public void a() {
        synchronized (this.c) {
            for (vn<?> vnVar : this.b) {
                vnVar.a();
            }
        }
    }

    @DexIgnore
    public boolean a(String str) {
        synchronized (this.c) {
            vn<?>[] vnVarArr = this.b;
            for (vn<?> vnVar : vnVarArr) {
                if (vnVar.a(str)) {
                    im.a().a(d, String.format("Work %s constrained by %s", str, vnVar.getClass().getSimpleName()), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    @Override // com.fossil.vn.a
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String str : list) {
                if (a(str)) {
                    im.a().a(d, String.format("Constraints met for %s", str), new Throwable[0]);
                    arrayList.add(str);
                }
            }
            if (this.a != null) {
                this.a.b(arrayList);
            }
        }
    }
}
