package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uf1 extends fe7 implements kd7<v81, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ jr1 a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public uf1(jr1 jr1, long j) {
        super(2);
        this.a = jr1;
        this.b = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(v81 v81, Float f) {
        jr1 jr1 = this.a;
        long floatValue = jr1.I + ((long) (f.floatValue() * ((float) this.b)));
        jr1.F = floatValue;
        float o = (((float) floatValue) * 1.0f) / ((float) jr1.o());
        if (Math.abs(o - this.a.G) > this.a.R || o == 1.0f) {
            jr1 jr12 = this.a;
            jr12.G = o;
            jr12.a(o);
        }
        return i97.a;
    }
}
