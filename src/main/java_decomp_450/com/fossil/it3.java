package com.fossil;

import android.content.Context;
import android.widget.BaseAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class it3 extends BaseAdapter {
    @DexIgnore
    public static /* final */ int e; // = nt3.d().getMaximum(4);
    @DexIgnore
    public /* final */ ht3 a;
    @DexIgnore
    public /* final */ zs3<?> b;
    @DexIgnore
    public ys3 c;
    @DexIgnore
    public /* final */ ws3 d;

    @DexIgnore
    public it3(ht3 ht3, zs3<?> zs3, ws3 ws3) {
        this.a = ht3;
        this.b = zs3;
        this.d = ws3;
    }

    @DexIgnore
    public final void a(Context context) {
        if (this.c == null) {
            this.c = new ys3(context);
        }
    }

    @DexIgnore
    public int b() {
        return (this.a.a() + this.a.f) - 1;
    }

    @DexIgnore
    public boolean c(int i) {
        return (i + 1) % this.a.e == 0;
    }

    @DexIgnore
    public int d(int i) {
        return (i - this.a.a()) + 1;
    }

    @DexIgnore
    public boolean e(int i) {
        return i >= a() && i <= b();
    }

    @DexIgnore
    public int getCount() {
        return this.a.f + a();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) (i / this.a.e);
    }

    @DexIgnore
    public boolean hasStableIds() {
        return true;
    }

    @DexIgnore
    public boolean b(int i) {
        return i % this.a.e == 0;
    }

    @DexIgnore
    public Long getItem(int i) {
        if (i < this.a.a() || i > b()) {
            return null;
        }
        return Long.valueOf(this.a.a(d(i)));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.widget.TextView getView(int r6, android.view.View r7, android.view.ViewGroup r8) {
        /*
            r5 = this;
            android.content.Context r0 = r8.getContext()
            r5.a(r0)
            r0 = r7
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1 = 0
            if (r7 != 0) goto L_0x001e
            android.content.Context r7 = r8.getContext()
            android.view.LayoutInflater r7 = android.view.LayoutInflater.from(r7)
            int r0 = com.fossil.pr3.mtrl_calendar_day
            android.view.View r7 = r7.inflate(r0, r8, r1)
            r0 = r7
            android.widget.TextView r0 = (android.widget.TextView) r0
        L_0x001e:
            int r7 = r5.a()
            int r7 = r6 - r7
            r8 = 1
            if (r7 < 0) goto L_0x0061
            com.fossil.ht3 r2 = r5.a
            int r3 = r2.f
            if (r7 < r3) goto L_0x002e
            goto L_0x0061
        L_0x002e:
            int r7 = r7 + r8
            r0.setTag(r2)
            java.lang.String r2 = java.lang.String.valueOf(r7)
            r0.setText(r2)
            com.fossil.ht3 r2 = r5.a
            long r2 = r2.a(r7)
            com.fossil.ht3 r7 = r5.a
            int r7 = r7.d
            com.fossil.ht3 r4 = com.fossil.ht3.d()
            int r4 = r4.d
            if (r7 != r4) goto L_0x0053
            java.lang.String r7 = com.fossil.at3.a(r2)
            r0.setContentDescription(r7)
            goto L_0x005a
        L_0x0053:
            java.lang.String r7 = com.fossil.at3.b(r2)
            r0.setContentDescription(r7)
        L_0x005a:
            r0.setVisibility(r1)
            r0.setEnabled(r8)
            goto L_0x0069
        L_0x0061:
            r7 = 8
            r0.setVisibility(r7)
            r0.setEnabled(r1)
        L_0x0069:
            java.lang.Long r6 = r5.getItem(r6)
            if (r6 != 0) goto L_0x0070
            return r0
        L_0x0070:
            com.fossil.ws3 r7 = r5.d
            com.fossil.ws3$c r7 = r7.a()
            long r2 = r6.longValue()
            boolean r7 = r7.e(r2)
            if (r7 == 0) goto L_0x00d5
            r0.setEnabled(r8)
            com.fossil.zs3<?> r7 = r5.b
            java.util.Collection r7 = r7.q()
            java.util.Iterator r7 = r7.iterator()
        L_0x008d:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L_0x00b5
            java.lang.Object r8 = r7.next()
            java.lang.Long r8 = (java.lang.Long) r8
            long r1 = r8.longValue()
            long r3 = r6.longValue()
            long r3 = com.fossil.nt3.a(r3)
            long r1 = com.fossil.nt3.a(r1)
            int r8 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r8 != 0) goto L_0x008d
            com.fossil.ys3 r6 = r5.c
            com.fossil.xs3 r6 = r6.b
            r6.a(r0)
            return r0
        L_0x00b5:
            java.util.Calendar r7 = com.fossil.nt3.b()
            long r7 = r7.getTimeInMillis()
            long r1 = r6.longValue()
            int r6 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r6 != 0) goto L_0x00cd
            com.fossil.ys3 r6 = r5.c
            com.fossil.xs3 r6 = r6.c
            r6.a(r0)
            return r0
        L_0x00cd:
            com.fossil.ys3 r6 = r5.c
            com.fossil.xs3 r6 = r6.a
            r6.a(r0)
            return r0
        L_0x00d5:
            r0.setEnabled(r1)
            com.fossil.ys3 r6 = r5.c
            com.fossil.xs3 r6 = r6.g
            r6.a(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.it3.getView(int, android.view.View, android.view.ViewGroup):android.widget.TextView");
    }

    @DexIgnore
    public int a() {
        return this.a.a();
    }

    @DexIgnore
    public int a(int i) {
        return a() + (i - 1);
    }
}
