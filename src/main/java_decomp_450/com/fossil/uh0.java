package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class uh0 extends Enum<uh0> {
    @DexIgnore
    public static /* final */ uh0 g;
    @DexIgnore
    public static /* final */ uh0 h;
    @DexIgnore
    public static /* final */ uh0 i;
    @DexIgnore
    public static /* final */ uh0 j;
    @DexIgnore
    public static /* final */ uh0 k;
    @DexIgnore
    public static /* final */ uh0 l;
    @DexIgnore
    public static /* final */ uh0 m;
    @DexIgnore
    public static /* final */ uh0 n;
    @DexIgnore
    public static /* final */ uh0 o;
    @DexIgnore
    public static /* final */ uh0 p;
    @DexIgnore
    public static /* final */ uh0 q;
    @DexIgnore
    public static /* final */ uh0 r;
    @DexIgnore
    public static /* final */ uh0 s;
    @DexIgnore
    public static /* final */ uh0 t;
    @DexIgnore
    public static /* final */ /* synthetic */ uh0[] u;
    @DexIgnore
    public /* final */ zf1 a;
    @DexIgnore
    public /* final */ vh1 b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ zf1 d;
    @DexIgnore
    public /* final */ vh1 e;
    @DexIgnore
    public /* final */ byte[] f;

    /*
    static {
        uh0 uh0 = new uh0("GET_CONNECTION_PARAMETERS", 0, zf1.GET, vh1.CURRENT_CONNECTION_PARAMETERS, null, null, null, null, 60);
        g = uh0;
        uh0 uh02 = new uh0("REQUEST_CONNECTION_PRIORITY", 1, zf1.SET, vh1.CONNECTION_PARAMETERS_REQUEST, null, null, vh1.CURRENT_CONNECTION_PARAMETERS, null, 44);
        h = uh02;
        uh0 uh03 = new uh0("PLAY_ANIMATION", 2, zf1.SET, vh1.DIAGNOSTIC_FUNCTIONS, tj1.PAIR_ANIMATION.a, null, null, null, 56);
        i = uh03;
        zf1 zf1 = zf1.GET;
        vh1 vh1 = vh1.DIAGNOSTIC_FUNCTIONS;
        byte[] bArr = tj1.OPTIMAL_PAYLOAD.a;
        uh0 uh04 = new uh0("GET_OPTIMAL_PAYLOAD", 3, zf1, vh1, bArr, null, null, bArr, 24);
        j = uh04;
        zf1 zf12 = zf1.GET;
        vh1 vh12 = vh1.DIAGNOSTIC_FUNCTIONS;
        byte[] bArr2 = tj1.BLE_TROUBLESHOOT.a;
        uh0 uh05 = new uh0("REQUEST_HANDS", 5, zf1.SET, vh1.CALIBRATION_SETTING, rl1.REQUEST_HANDS.a, null, null, null, 56);
        k = uh05;
        uh0 uh06 = new uh0("RELEASE_HANDS", 6, zf1.SET, vh1.CALIBRATION_SETTING, rl1.RELEASE_HANDS.a, null, null, null, 56);
        l = uh06;
        uh0 uh07 = new uh0("MOVE_HANDS", 7, zf1.SET, vh1.CALIBRATION_SETTING, rl1.MOVE_HANDS.a, null, null, null, 56);
        m = uh07;
        uh0 uh08 = new uh0("SET_CALIBRATION_POSITION", 8, zf1.SET, vh1.HARDWARE_TEST, rl1.SET_CALIBRATION_POSITION.a, null, null, null, 56);
        n = uh08;
        zf1 zf13 = zf1.GET;
        vh1 vh13 = vh1.WORKOUT_SESSION_PRIMARY_ID;
        byte[] bArr3 = pr1.WORKOUT_SESSION_CONTROL.a;
        uh0 uh09 = new uh0("GET_CURRENT_WORKOUT_SESSION", 9, zf13, vh13, bArr3, null, null, bArr3, 24);
        o = uh09;
        uh0 uh010 = new uh0("STOP_CURRENT_WORKOUT_SESSION", 10, zf1.SET, vh1.WORKOUT_SESSION_PRIMARY_ID, pr1.WORKOUT_SESSION_CONTROL.a, null, null, null, 56);
        p = uh010;
        uh0 uh011 = new uh0("SET_HEARTBEAT_INTERVAL", 13, zf1.SET, vh1.STREAMING_CONFIG, qp1.HEARTBEAT_INTERVAL.a, null, null, null, 56);
        q = uh011;
        uh0 uh012 = new uh0("CLEAN_UP_DEVICE", 15, zf1.SET, vh1.DIAGNOSTIC_FUNCTIONS, fe1.CLEAN_UP_DEVICE.a, null, null, null, 56);
        r = uh012;
        uh0 uh013 = new uh0("LEGACY_OTA_ENTER", 16, zf1.SET, vh1.DIAGNOSTIC_FUNCTIONS, qn1.LEGACY_OTA_ENTER.a, null, null, qn1.LEGACY_OTA_ENTER_RESPONSE.a, 24);
        s = uh013;
        uh0 uh014 = new uh0("LEGACY_OTA_RESET", 17, zf1.SET, vh1.DIAGNOSTIC_FUNCTIONS, qn1.LEGACY_OTA_RESET.a, null, null, null, 56);
        t = uh014;
        u = new uh0[]{uh0, uh02, uh03, uh04, new uh0("BLE_TROUBLESHOOT", 4, zf12, vh12, bArr2, null, null, bArr2, 24), uh05, uh06, uh07, uh08, uh09, uh010, new uh0("GET_HEARTBEAT_STATISTIC", 11, zf1.GET, vh1.DIAGNOSTIC_FUNCTIONS, tj1.HEARTBEAT_STATISTIC.a, zf1.RESPONSE, vh1.DIAGNOSTIC_FUNCTIONS, tj1.HEARTBEAT_STATISTIC.a), new uh0("GET_HEARTBEAT_INTERVAL", 12, zf1.GET, vh1.STREAMING_CONFIG, qp1.HEARTBEAT_INTERVAL.a, null, null, null, 56), uh011, new uh0("REQUEST_DISCOVER_SERVICE", 14, zf1.SET, vh1.ADVANCE_BLE_REQUEST, fe1.REQUEST_DISCOVER_SERVICE.a, null, null, null, 56), uh012, uh013, uh014};
    }
    */

    @DexIgnore
    public uh0(String str, int i2, zf1 zf1, vh1 vh1, byte[] bArr, zf1 zf12, vh1 vh12, byte[] bArr2) {
        this.a = zf1;
        this.b = vh1;
        this.c = bArr;
        this.d = zf12;
        this.e = vh12;
        this.f = bArr2;
    }

    @DexIgnore
    public static uh0 valueOf(String str) {
        return (uh0) Enum.valueOf(uh0.class, str);
    }

    @DexIgnore
    public static uh0[] values() {
        return (uh0[]) u.clone();
    }

    @DexIgnore
    public /* synthetic */ uh0(String str, int i2, zf1 zf1, vh1 vh1, byte[] bArr, zf1 zf12, vh1 vh12, byte[] bArr2, int i3) {
        bArr = (i3 & 4) != 0 ? new byte[0] : bArr;
        zf12 = (i3 & 8) != 0 ? zf1.RESPONSE : zf12;
        vh12 = (i3 & 16) != 0 ? vh1 : vh12;
        bArr2 = (i3 & 32) != 0 ? new byte[0] : bArr2;
        this.a = zf1;
        this.b = vh1;
        this.c = bArr;
        this.d = zf12;
        this.e = vh12;
        this.f = bArr2;
    }
}
