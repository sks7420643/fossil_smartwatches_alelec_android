package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zs {
    @DexIgnore
    public static final a a = a.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ a a; // = new a();

        @DexIgnore
        public final zs a(Context context, b bVar) {
            ee7.b(context, "context");
            ee7.b(bVar, "listener");
            ConnectivityManager connectivityManager = (ConnectivityManager) v6.a(context, ConnectivityManager.class);
            if (connectivityManager != null) {
                if (v6.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                    try {
                        if (Build.VERSION.SDK_INT >= 21) {
                            return new bt(connectivityManager, bVar);
                        }
                        return new at(context, connectivityManager, bVar);
                    } catch (Exception e) {
                        ju.a("NetworkObserverStrategy", new RuntimeException("Failed to register network observer.", e));
                        return ws.b;
                    }
                }
            }
            if (cu.c.a() && cu.c.b() <= 5) {
                Log.println(5, "NetworkObserverStrategy", "Unable to register network observer.");
            }
            return ws.b;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    void start();

    @DexIgnore
    void stop();
}
