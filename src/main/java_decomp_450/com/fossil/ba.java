package com.fossil;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba {
    @DexIgnore
    public Object a;

    @DexIgnore
    public ba(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public static ba a(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new ba(PointerIcon.getSystemIcon(context, i));
        }
        return new ba(null);
    }
}
