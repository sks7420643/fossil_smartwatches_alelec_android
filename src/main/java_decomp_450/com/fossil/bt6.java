package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt6 {
    @DexIgnore
    public /* final */ cl5 a;
    @DexIgnore
    public /* final */ zs6 b;

    @DexIgnore
    public bt6(cl5 cl5, zs6 zs6) {
        ee7.b(cl5, "mContext");
        ee7.b(zs6, "mView");
        this.a = cl5;
        this.b = zs6;
    }

    @DexIgnore
    public final cl5 a() {
        return this.a;
    }

    @DexIgnore
    public final zs6 b() {
        return this.b;
    }
}
