package com.fossil;

import com.fossil.io7;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bv7<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends bv7<Iterable<T>> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public /* bridge */ /* synthetic */ void a(dv7 dv7, Object obj) throws IOException {
            a(dv7, (Iterable) ((Iterable) obj));
        }

        @DexIgnore
        public void a(dv7 dv7, Iterable<T> iterable) throws IOException {
            if (iterable != null) {
                for (T t : iterable) {
                    bv7.this.a(dv7, t);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends bv7<Object> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.bv7 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.bv7
        public void a(dv7 dv7, Object obj) throws IOException {
            if (obj != null) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    bv7.this.a(dv7, Array.get(obj, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> extends bv7<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ tu7<T, RequestBody> c;

        @DexIgnore
        public c(Method method, int i, tu7<T, RequestBody> tu7) {
            this.a = method;
            this.b = i;
            this.c = tu7;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) {
            if (t != null) {
                try {
                    dv7.a(this.c.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw jv7.a(method, e, i, "Unable to convert " + ((Object) t) + " to RequestBody", new Object[0]);
                }
            } else {
                throw jv7.a(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> extends bv7<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ tu7<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public d(String str, tu7<T, String> tu7, boolean z) {
            jv7.a(str, "name == null");
            this.a = str;
            this.b = tu7;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                dv7.a(this.a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> extends bv7<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ tu7<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public e(Method method, int i, tu7<T, String> tu7, boolean z) {
            this.a = method;
            this.b = i;
            this.c = tu7;
            this.d = z;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public /* bridge */ /* synthetic */ void a(dv7 dv7, Object obj) throws IOException {
            a(dv7, (Map) ((Map) obj));
        }

        @DexIgnore
        public void a(dv7 dv7, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                dv7.a(key, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw jv7.a(method, i, "Field map value '" + ((Object) value) + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw jv7.a(method2, i2, "Field map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw jv7.a(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw jv7.a(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> extends bv7<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ tu7<T, String> b;

        @DexIgnore
        public f(String str, tu7<T, String> tu7) {
            jv7.a(str, "name == null");
            this.a = str;
            this.b = tu7;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                dv7.a(this.a, a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> extends bv7<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ fo7 c;
        @DexIgnore
        public /* final */ tu7<T, RequestBody> d;

        @DexIgnore
        public g(Method method, int i, fo7 fo7, tu7<T, RequestBody> tu7) {
            this.a = method;
            this.b = i;
            this.c = fo7;
            this.d = tu7;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) {
            if (t != null) {
                try {
                    dv7.a(this.c, this.d.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw jv7.a(method, i, "Unable to convert " + ((Object) t) + " to RequestBody", e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends bv7<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ tu7<T, RequestBody> c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public h(Method method, int i, tu7<T, RequestBody> tu7, String str) {
            this.a = method;
            this.b = i;
            this.c = tu7;
            this.d = str;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public /* bridge */ /* synthetic */ void a(dv7 dv7, Object obj) throws IOException {
            a(dv7, (Map) ((Map) obj));
        }

        @DexIgnore
        public void a(dv7 dv7, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            dv7.a(fo7.a("Content-Disposition", "form-data; name=\"" + key + "\"", "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw jv7.a(method, i, "Part map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw jv7.a(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw jv7.a(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> extends bv7<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ tu7<T, String> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public i(Method method, int i, String str, tu7<T, String> tu7, boolean z) {
            this.a = method;
            this.b = i;
            jv7.a(str, "name == null");
            this.c = str;
            this.d = tu7;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) throws IOException {
            if (t != null) {
                dv7.b(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.a;
            int i = this.b;
            throw jv7.a(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> extends bv7<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ tu7<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public j(String str, tu7<T, String> tu7, boolean z) {
            jv7.a(str, "name == null");
            this.a = str;
            this.b = tu7;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                dv7.c(this.a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> extends bv7<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ tu7<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public k(Method method, int i, tu7<T, String> tu7, boolean z) {
            this.a = method;
            this.b = i;
            this.c = tu7;
            this.d = z;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public /* bridge */ /* synthetic */ void a(dv7 dv7, Object obj) throws IOException {
            a(dv7, (Map) ((Map) obj));
        }

        @DexIgnore
        public void a(dv7 dv7, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                dv7.c(key, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw jv7.a(method, i, "Query map value '" + ((Object) value) + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw jv7.a(method2, i2, "Query map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw jv7.a(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw jv7.a(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> extends bv7<T> {
        @DexIgnore
        public /* final */ tu7<T, String> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public l(tu7<T, String> tu7, boolean z) {
            this.a = tu7;
            this.b = z;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, T t) throws IOException {
            if (t != null) {
                dv7.c(this.a.a(t), null, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends bv7<io7.b> {
        @DexIgnore
        public static /* final */ m a; // = new m();

        @DexIgnore
        public void a(dv7 dv7, io7.b bVar) {
            if (bVar != null) {
                dv7.a(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends bv7<Object> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public n(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.bv7
        public void a(dv7 dv7, Object obj) {
            if (obj != null) {
                dv7.a(obj);
                return;
            }
            throw jv7.a(this.a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    @DexIgnore
    public final bv7<Object> a() {
        return new b();
    }

    @DexIgnore
    public abstract void a(dv7 dv7, T t) throws IOException;

    @DexIgnore
    public final bv7<Iterable<T>> b() {
        return new a();
    }
}
