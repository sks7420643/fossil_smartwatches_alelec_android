package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q31 implements Parcelable.Creator<n51> {
    @DexIgnore
    public /* synthetic */ q31(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public n51 createFromParcel(Parcel parcel) {
        return new n51(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public n51[] newArray(int i) {
        return new n51[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public n51 m51createFromParcel(Parcel parcel) {
        return new n51(parcel, null);
    }
}
