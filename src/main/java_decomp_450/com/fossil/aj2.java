package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj2 extends gi2 implements zi2 {
    @DexIgnore
    public aj2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    @Override // com.fossil.zi2
    public final void c(Status status) throws RemoteException {
        Parcel zza = zza();
        ej2.a(zza, status);
        b(1, zza);
    }
}
