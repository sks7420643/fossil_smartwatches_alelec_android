package com.fossil;

import android.os.Build;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rh extends qh {
    @DexIgnore
    public hh e;

    @DexIgnore
    public rh(long j, RenderScript renderScript) {
        super(j, renderScript);
    }

    @DexIgnore
    public static rh a(RenderScript renderScript, jh jhVar) {
        if (jhVar.a(jh.h(renderScript)) || jhVar.a(jh.g(renderScript))) {
            boolean z = renderScript.d() && Build.VERSION.SDK_INT < 19;
            rh rhVar = new rh(renderScript.a(5, jhVar.a(renderScript), z), renderScript);
            rhVar.a(z);
            rhVar.a(5.0f);
            return rhVar;
        }
        throw new mh("Unsupported element type.");
    }

    @DexIgnore
    public void b(hh hhVar) {
        if (hhVar.e().i() != 0) {
            a(0, null, hhVar, null);
            return;
        }
        throw new mh("Output is a 1D Allocation");
    }

    @DexIgnore
    public void c(hh hhVar) {
        if (hhVar.e().i() != 0) {
            this.e = hhVar;
            a(1, hhVar);
            return;
        }
        throw new mh("Input set to a 1D Allocation");
    }

    @DexIgnore
    public void a(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 25.0f) {
            throw new mh("Radius out of range (0 < r <= 25).");
        }
        a(0, f);
    }
}
