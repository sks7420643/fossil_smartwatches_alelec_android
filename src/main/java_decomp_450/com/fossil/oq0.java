package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq0 extends s81 {
    @DexIgnore
    public /* final */ eg0 T;

    @DexIgnore
    public oq0(ri1 ri1, en0 en0, eg0 eg0) {
        super(ri1, en0, wm0.J0, true, 1797, eg0.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = eg0;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.T.b();
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.S5, this.T.a());
    }
}
