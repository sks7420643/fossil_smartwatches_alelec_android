package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.fossil.fl4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.security.KeyPair;
import java.security.KeyStore;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aw6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ ch5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ kb5 b;

        @DexIgnore
        public b(String str, kb5 kb5) {
            ee7.b(str, "aliasName");
            ee7.b(kb5, "scope");
            this.a = str;
            this.b = kb5;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final kb5 b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            ee7.b(str, "errorMessage");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            ee7.b(str, "decryptedValue");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public aw6(ch5 ch5) {
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "DecryptValueKeyStoreUseCase";
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                ch5 ch5 = this.d;
                if (bVar != null) {
                    String a2 = ch5.a(bVar.a(), bVar.b());
                    String b2 = this.d.b(bVar.a(), bVar.b());
                    KeyStore.SecretKeyEntry e = ix6.b.e(bVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(2, e.getSecretKey(), new GCMParameterSpec(128, Base64.decode(a2, 0)));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DecryptValueKeyStoreUseCase", "Start decrypt with iv " + a2 + " encryptedValue " + b2);
                    byte[] doFinal = instance.doFinal(Base64.decode(b2, 0));
                    ee7.a((Object) doFinal, "decryptedByteArray");
                    String str = new String(doFinal, sg7.a);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str);
                    a(new d(str));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ch5 ch52 = this.d;
                if (bVar != null) {
                    String b3 = ch52.b(bVar.a(), bVar.b());
                    KeyPair c2 = ix6.b.c(bVar.a());
                    if (c2 == null) {
                        c2 = ix6.b.a(bVar.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(2, c2.getPrivate());
                    byte[] doFinal2 = instance2.doFinal(Base64.decode(b3, 0));
                    ee7.a((Object) doFinal2, "decryptedByte");
                    String str2 = new String(doFinal2, sg7.a);
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str2);
                    a(new d(str2));
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("DecryptValueKeyStoreUseCase", "Exception when decrypt value " + e2);
            a(new c(600, ""));
        }
        return new Object();
    }
}
