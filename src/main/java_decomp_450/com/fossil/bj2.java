package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bj2 extends kh2 implements zi2 {
    @DexIgnore
    public bj2() {
        super("com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    public static zi2 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IStatusCallback");
        if (queryLocalInterface instanceof zi2) {
            return (zi2) queryLocalInterface;
        }
        return new aj2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        c((Status) ej2.a(parcel, Status.CREATOR));
        return true;
    }
}
