package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n66 implements Factory<m66> {
    @DexIgnore
    public static m66 a(i66 i66, MicroAppRepository microAppRepository, ch5 ch5, PortfolioApp portfolioApp) {
        return new m66(i66, microAppRepository, ch5, portfolioApp);
    }
}
