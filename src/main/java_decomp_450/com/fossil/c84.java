package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c84 implements b84 {
    @DexIgnore
    public /* final */ y74 a;
    @DexIgnore
    public /* final */ a84 b;
    @DexIgnore
    public /* final */ z74 c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public c84(long j, y74 y74, a84 a84, z74 z74, int i, int i2) {
        this.d = j;
        this.a = y74;
        this.b = a84;
        this.c = z74;
    }

    @DexIgnore
    public boolean a(long j) {
        return this.d < j;
    }

    @DexIgnore
    @Override // com.fossil.b84
    public a84 b() {
        return this.b;
    }

    @DexIgnore
    public y74 c() {
        return this.a;
    }

    @DexIgnore
    public long d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.b84
    public z74 a() {
        return this.c;
    }
}
