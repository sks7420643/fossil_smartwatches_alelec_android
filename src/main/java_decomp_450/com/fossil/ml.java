package com.fossil;

import android.os.Parcelable;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ml {
    @DexIgnore
    public /* final */ n4<String, Method> a;
    @DexIgnore
    public /* final */ n4<String, Method> b;
    @DexIgnore
    public /* final */ n4<String, Class> c;

    @DexIgnore
    public ml(n4<String, Method> n4Var, n4<String, Method> n4Var2, n4<String, Class> n4Var3) {
        this.a = n4Var;
        this.b = n4Var2;
        this.c = n4Var3;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(Parcelable parcelable);

    @DexIgnore
    public abstract void a(CharSequence charSequence);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public void a(boolean z, boolean z2) {
    }

    @DexIgnore
    public abstract void a(byte[] bArr);

    @DexIgnore
    public abstract boolean a(int i);

    @DexIgnore
    public boolean a(boolean z, int i) {
        if (!a(i)) {
            return z;
        }
        return d();
    }

    @DexIgnore
    public abstract ml b();

    @DexIgnore
    public abstract void b(int i);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public void b(boolean z, int i) {
        b(i);
        a(z);
    }

    @DexIgnore
    public abstract void c(int i);

    @DexIgnore
    public boolean c() {
        return false;
    }

    @DexIgnore
    public abstract boolean d();

    @DexIgnore
    public abstract byte[] e();

    @DexIgnore
    public abstract CharSequence f();

    @DexIgnore
    public abstract int g();

    @DexIgnore
    public abstract <T extends Parcelable> T h();

    @DexIgnore
    public abstract String i();

    @DexIgnore
    public <T extends ol> T j() {
        String i = i();
        if (i == null) {
            return null;
        }
        return (T) a(i, b());
    }

    @DexIgnore
    public int a(int i, int i2) {
        if (!a(i2)) {
            return i;
        }
        return g();
    }

    @DexIgnore
    public void b(byte[] bArr, int i) {
        b(i);
        a(bArr);
    }

    @DexIgnore
    public String a(String str, int i) {
        if (!a(i)) {
            return str;
        }
        return i();
    }

    @DexIgnore
    public void b(CharSequence charSequence, int i) {
        b(i);
        a(charSequence);
    }

    @DexIgnore
    public byte[] a(byte[] bArr, int i) {
        if (!a(i)) {
            return bArr;
        }
        return e();
    }

    @DexIgnore
    public void b(int i, int i2) {
        b(i2);
        c(i);
    }

    @DexIgnore
    public <T extends Parcelable> T a(T t, int i) {
        return !a(i) ? t : (T) h();
    }

    @DexIgnore
    public void b(String str, int i) {
        b(i);
        b(str);
    }

    @DexIgnore
    public CharSequence a(CharSequence charSequence, int i) {
        if (!a(i)) {
            return charSequence;
        }
        return f();
    }

    @DexIgnore
    public void b(Parcelable parcelable, int i) {
        b(i);
        a(parcelable);
    }

    @DexIgnore
    public void a(ol olVar) {
        if (olVar == null) {
            b((String) null);
            return;
        }
        b(olVar);
        ml b2 = b();
        a(olVar, b2);
        b2.a();
    }

    @DexIgnore
    public void b(ol olVar, int i) {
        b(i);
        a(olVar);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.ml */
    /* JADX WARN: Multi-variable type inference failed */
    public final void b(ol olVar) {
        try {
            b(a((Class<? extends ol>) olVar.getClass()).getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(olVar.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    @DexIgnore
    public <T extends ol> T a(T t, int i) {
        return !a(i) ? t : (T) j();
    }

    @DexIgnore
    public final Method b(Class cls) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Method method = this.b.get(cls.getName());
        if (method != null) {
            return method;
        }
        Class a2 = a(cls);
        System.currentTimeMillis();
        Method declaredMethod = a2.getDeclaredMethod("write", cls, ml.class);
        this.b.put(cls.getName(), declaredMethod);
        return declaredMethod;
    }

    @DexIgnore
    public <T extends ol> T a(String str, ml mlVar) {
        try {
            return (T) ((ol) a(str).invoke(null, mlVar));
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public <T extends ol> void a(T t, ml mlVar) {
        try {
            b(t.getClass()).invoke(null, t, mlVar);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public final Method a(String str) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Method method = this.a.get(str);
        if (method != null) {
            return method;
        }
        System.currentTimeMillis();
        Method declaredMethod = Class.forName(str, true, ml.class.getClassLoader()).getDeclaredMethod(HardwareLog.COLUMN_READ, ml.class);
        this.a.put(str, declaredMethod);
        return declaredMethod;
    }

    @DexIgnore
    public final Class a(Class<? extends ol> cls) throws ClassNotFoundException {
        Class cls2 = this.c.get(cls.getName());
        if (cls2 != null) {
            return cls2;
        }
        Class<?> cls3 = Class.forName(String.format("%s.%sParcelizer", cls.getPackage().getName(), cls.getSimpleName()), false, cls.getClassLoader());
        this.c.put(cls.getName(), cls3);
        return cls3;
    }
}
