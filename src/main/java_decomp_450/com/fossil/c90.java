package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum c90 {
    DC(qk1.DC),
    FTC(qk1.FTC),
    FTD(qk1.FTD),
    AUT(qk1.AUTHENTICATION),
    ASYNC(qk1.ASYNC),
    FTD_1(qk1.FTD_1),
    HEART_RATE(qk1.HEART_RATE);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ qk1 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }
    }

    @DexIgnore
    public c90(qk1 qk1) {
        this.a = qk1;
    }

    @DexIgnore
    public final qk1 a() {
        return this.a;
    }
}
