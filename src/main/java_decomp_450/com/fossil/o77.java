package com.fossil;

import android.app.Application;
import com.google.errorprone.annotations.ForOverride;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o77 extends Application implements u77 {
    @DexIgnore
    public volatile t77<Object> a;

    @DexIgnore
    @Override // com.fossil.u77
    public m77<Object> a() {
        c();
        return this.a;
    }

    @DexIgnore
    @ForOverride
    public abstract m77<? extends o77> b();

    @DexIgnore
    public final void c() {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    b().a(this);
                    if (this.a == null) {
                        throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
                    }
                }
            }
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        c();
    }
}
