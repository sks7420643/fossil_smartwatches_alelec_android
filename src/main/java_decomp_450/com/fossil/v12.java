package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public final class v12 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static v12 e;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Status b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public v12(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(s02.common_google_play_services_unknown_issue));
        boolean z = false;
        if (identifier != 0) {
            this.c = !(resources.getInteger(identifier) != 0 ? true : z);
        } else {
            this.c = false;
        }
        String a2 = z82.a(context);
        a2 = a2 == null ? new g72(context).a("google_app_id") : a2;
        if (TextUtils.isEmpty(a2)) {
            this.b = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.a = null;
            return;
        }
        this.a = a2;
        this.b = Status.e;
    }

    @DexIgnore
    public static Status a(Context context) {
        Status status;
        a72.a(context, "Context must not be null.");
        synchronized (d) {
            if (e == null) {
                e = new v12(context);
            }
            status = e.b;
        }
        return status;
    }

    @DexIgnore
    public static boolean b() {
        return a("isMeasurementExplicitlyDisabled").c;
    }

    @DexIgnore
    public static String a() {
        return a("getGoogleAppId").a;
    }

    @DexIgnore
    public static v12 a(String str) {
        v12 v12;
        synchronized (d) {
            if (e != null) {
                v12 = e;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(CodelessMatcher.CURRENT_CLASS_NAME);
                throw new IllegalStateException(sb.toString());
            }
        }
        return v12;
    }
}
