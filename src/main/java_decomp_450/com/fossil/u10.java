package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u10 implements ex<Drawable> {
    @DexIgnore
    public /* final */ ex<Bitmap> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public u10(ex<Bitmap> exVar, boolean z) {
        this.b = exVar;
        this.c = z;
    }

    @DexIgnore
    public ex<BitmapDrawable> a() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ex
    public uy<Drawable> a(Context context, uy<Drawable> uyVar, int i, int i2) {
        dz c2 = aw.a(context).c();
        Drawable drawable = uyVar.get();
        uy<Bitmap> a = t10.a(c2, drawable, i, i2);
        if (a != null) {
            uy<Bitmap> a2 = this.b.a(context, a, i, i2);
            if (!a2.equals(a)) {
                return a(context, a2);
            }
            a2.b();
            return uyVar;
        } else if (!this.c) {
            return uyVar;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (obj instanceof u10) {
            return this.b.equals(((u10) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final uy<Drawable> a(Context context, uy<Bitmap> uyVar) {
        return a20.a(context.getResources(), uyVar);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
