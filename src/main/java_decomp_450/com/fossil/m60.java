package com.fossil;

import android.bluetooth.BluetoothAdapter;
import com.fossil.r60;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m60 extends k60 {
    @DexIgnore
    public static /* final */ c t; // = new c(null);
    @DexIgnore
    public n60 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ r60 i;
    @DexIgnore
    public /* final */ r60 j;
    @DexIgnore
    public /* final */ r60 k;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, r60> l;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, r60> m;
    @DexIgnore
    public /* final */ a n;
    @DexIgnore
    public /* final */ o80[] o;
    @DexIgnore
    public /* final */ r60 p;
    @DexIgnore
    public /* final */ String q;
    @DexIgnore
    public /* final */ r60 r;
    @DexIgnore
    public /* final */ String s;

    @DexIgnore
    public enum a {
        NO_REQUIRE((byte) 0),
        REQUIRE((byte) 1);
        
        @DexIgnore
        public static /* final */ C0121a c; // = new C0121a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m60$a$a")
        /* renamed from: com.fossil.m60$a$a  reason: collision with other inner class name */
        public static final class C0121a {
            @DexIgnore
            public /* synthetic */ C0121a(zd7 zd7) {
            }

            @DexIgnore
            public final a a(byte b) {
                a[] values = a.values();
                for (a aVar : values) {
                    if (aVar.a() == b) {
                        return aVar;
                    }
                }
                return null;
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore
    public enum b {
        SERIAL_NUMBER(1),
        HARDWARE_REVISION(2),
        FIRMWARE_VERSION(3),
        MODEL_NUMBER(4),
        HEART_RATE_SERIAL_NUMBER(5),
        BOOTLOADER_VERSION(6),
        WATCH_APP_VERSION(7),
        FONT_VERSION(8),
        LUTS_VERSION(9),
        SUPPORTED_FILES_VERSION(10),
        BOND_REQUIREMENT(11),
        SUPPORTED_DEVICE_CONFIGS(12),
        DEVICE_SECURITY_VERSION(14),
        SOCKET_INFO(15),
        LOCALE(16),
        MICRO_APP_SYSTEM_VERSION(17),
        LOCALE_VERSION(18),
        CURRENT_FILES_VERSION(19),
        UNKNOWN((short) 65535);
        
        @DexIgnore
        public static /* final */ a c; // = new a(null);
        @DexIgnore
        public /* final */ short a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
            }

            @DexIgnore
            public final b a(short s) {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == s) {
                        break;
                    }
                    i++;
                }
                return bVar != null ? bVar : b.UNKNOWN;
            }
        }

        @DexIgnore
        public b(short s) {
            this.a = s;
        }

        @DexIgnore
        public final short a() {
            return this.a;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ m60(java.lang.String r23, java.lang.String r24, java.lang.String r25, java.lang.String r26, java.lang.String r27, java.lang.String r28, java.lang.String r29, com.fossil.r60 r30, com.fossil.r60 r31, com.fossil.r60 r32, java.util.LinkedHashMap r33, java.util.LinkedHashMap r34, com.fossil.m60.a r35, com.fossil.o80[] r36, com.fossil.r60 r37, java.lang.String r38, com.fossil.r60 r39, java.lang.String r40, int r41) {
        /*
            r22 = this;
            r0 = r41
            r1 = r0 & 8
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x000a
            r7 = r2
            goto L_0x000c
        L_0x000a:
            r7 = r26
        L_0x000c:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0012
            r8 = r2
            goto L_0x0014
        L_0x0012:
            r8 = r27
        L_0x0014:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x001a
            r9 = r2
            goto L_0x001c
        L_0x001a:
            r9 = r28
        L_0x001c:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0022
            r10 = r2
            goto L_0x0024
        L_0x0022:
            r10 = r29
        L_0x0024:
            r1 = r0 & 128(0x80, float:1.794E-43)
            r3 = 0
            if (r1 == 0) goto L_0x0030
            com.fossil.r60 r1 = new com.fossil.r60
            r1.<init>(r3, r3)
            r11 = r1
            goto L_0x0032
        L_0x0030:
            r11 = r30
        L_0x0032:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x003d
            com.fossil.r60 r1 = new com.fossil.r60
            r1.<init>(r3, r3)
            r12 = r1
            goto L_0x003f
        L_0x003d:
            r12 = r31
        L_0x003f:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x004a
            com.fossil.r60 r1 = new com.fossil.r60
            r1.<init>(r3, r3)
            r13 = r1
            goto L_0x004c
        L_0x004a:
            r13 = r32
        L_0x004c:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x0057
            java.util.LinkedHashMap r1 = new java.util.LinkedHashMap
            r1.<init>()
            r14 = r1
            goto L_0x0059
        L_0x0057:
            r14 = r33
        L_0x0059:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0064
            java.util.LinkedHashMap r1 = new java.util.LinkedHashMap
            r1.<init>()
            r15 = r1
            goto L_0x0066
        L_0x0064:
            r15 = r34
        L_0x0066:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x006f
            com.fossil.m60$a r1 = com.fossil.m60.a.NO_REQUIRE
            r16 = r1
            goto L_0x0071
        L_0x006f:
            r16 = r35
        L_0x0071:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x007a
            com.fossil.o80[] r1 = new com.fossil.o80[r3]
            r17 = r1
            goto L_0x007c
        L_0x007a:
            r17 = r36
        L_0x007c:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x0088
            com.fossil.r60 r1 = new com.fossil.r60
            r1.<init>(r3, r3)
            r18 = r1
            goto L_0x008a
        L_0x0088:
            r18 = r37
        L_0x008a:
            r1 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0095
            java.lang.String r1 = "en_US"
            r19 = r1
            goto L_0x0097
        L_0x0095:
            r19 = r38
        L_0x0097:
            r1 = 65536(0x10000, float:9.18355E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00a4
            com.fossil.r60 r1 = new com.fossil.r60
            r1.<init>(r3, r3)
            r20 = r1
            goto L_0x00a6
        L_0x00a4:
            r20 = r39
        L_0x00a6:
            r1 = 131072(0x20000, float:1.83671E-40)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x00ae
            r21 = r2
            goto L_0x00b0
        L_0x00ae:
            r21 = r40
        L_0x00b0:
            r3 = r22
            r4 = r23
            r5 = r24
            r6 = r25
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.m60.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.fossil.r60, com.fossil.r60, com.fossil.r60, java.util.LinkedHashMap, java.util.LinkedHashMap, com.fossil.m60$a, com.fossil.o80[], com.fossil.r60, java.lang.String, com.fossil.r60, java.lang.String, int):void");
    }

    @DexIgnore
    public static /* synthetic */ m60 a(m60 m60, String str, String str2, String str3, String str4, String str5, String str6, String str7, r60 r60, r60 r602, r60 r603, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, a aVar, o80[] o80Arr, r60 r604, String str8, r60 r605, String str9, int i2) {
        return m60.a((i2 & 1) != 0 ? m60.b : str, (i2 & 2) != 0 ? m60.c : str2, (i2 & 4) != 0 ? m60.d : str3, (i2 & 8) != 0 ? m60.e : str4, (i2 & 16) != 0 ? m60.f : str5, (i2 & 32) != 0 ? m60.g : str6, (i2 & 64) != 0 ? m60.h : str7, (i2 & 128) != 0 ? m60.i : r60, (i2 & 256) != 0 ? m60.j : r602, (i2 & 512) != 0 ? m60.k : r603, (i2 & 1024) != 0 ? m60.l : linkedHashMap, (i2 & 2048) != 0 ? m60.m : linkedHashMap2, (i2 & 4096) != 0 ? m60.n : aVar, (i2 & 8192) != 0 ? m60.o : o80Arr, (i2 & 16384) != 0 ? m60.p : r604, (i2 & 32768) != 0 ? m60.q : str8, (i2 & 65536) != 0 ? m60.r : r605, (i2 & 131072) != 0 ? m60.s : str9);
    }

    @DexIgnore
    public final m60 a(String str, String str2, String str3, String str4, String str5, String str6, String str7, r60 r60, r60 r602, r60 r603, LinkedHashMap<Short, r60> linkedHashMap, LinkedHashMap<Short, r60> linkedHashMap2, a aVar, o80[] o80Arr, r60 r604, String str8, r60 r605, String str9) {
        return new m60(str, str2, str3, str4, str5, str6, str7, r60, r602, r603, linkedHashMap, linkedHashMap2, aVar, o80Arr, r604, str8, r605, str9);
    }

    @DexIgnore
    public final void a(n60 n60) {
        this.a = n60;
    }

    @DexIgnore
    public final a b() {
        return this.n;
    }

    @DexIgnore
    public final r60 c() {
        return this.i;
    }

    @DexIgnore
    public final LinkedHashMap<Short, r60> d() {
        return this.m;
    }

    @DexIgnore
    public final r60 e() {
        return this.p;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(m60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            m60 m60 = (m60) obj;
            return !(ee7.a(this.b, m60.b) ^ true) && !(ee7.a(this.c, m60.c) ^ true) && !(ee7.a(this.d, m60.d) ^ true) && !(ee7.a(this.e, m60.e) ^ true) && !(ee7.a(this.f, m60.f) ^ true) && !(ee7.a(this.g, m60.g) ^ true) && !(ee7.a(this.h, m60.h) ^ true) && !(ee7.a(this.i, m60.i) ^ true) && !(ee7.a(this.j, m60.j) ^ true) && !(ee7.a(this.k, m60.k) ^ true) && !(ee7.a(this.l, m60.l) ^ true) && !(ee7.a(this.m, m60.m) ^ true) && this.n == m60.n && Arrays.equals(this.o, m60.o) && !(ee7.a(this.p, m60.p) ^ true) && !(ee7.a(this.q, m60.q) ^ true) && !(ee7.a(this.r, m60.r) ^ true) && !(ee7.a(this.s, m60.s) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.DeviceInformation");
    }

    @DexIgnore
    public final r60 f() {
        return this.k;
    }

    @DexIgnore
    public final String g() {
        return this.h;
    }

    @DexIgnore
    public final n60 getDeviceType() {
        return this.a;
    }

    @DexIgnore
    public final s60 getELabelVersions() {
        r60 r60 = this.m.get((short) 1797);
        if (r60 == null) {
            r60 = b21.x.d();
        }
        r60 r602 = this.l.get((short) 1797);
        if (r602 == null) {
            r602 = b21.x.d();
        }
        return new s60(r60, r602);
    }

    @DexIgnore
    public final String getFastPairIdInHexString() {
        return this.s;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.f;
    }

    @DexIgnore
    public final String getHardwareRevision() {
        return this.e;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.q;
    }

    @DexIgnore
    public final s60 getLocaleVersions() {
        r60 r60 = this.m.get((short) 1794);
        if (r60 == null) {
            r60 = b21.x.d();
        }
        r60 r602 = this.l.get(Short.valueOf(pb1.ASSET.a));
        if (r602 == null) {
            r602 = b21.x.d();
        }
        return new s60(r60, r602);
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.c;
    }

    @DexIgnore
    public final r60 getMicroAppVersion() {
        return this.r;
    }

    @DexIgnore
    public final String getModelNumber() {
        return this.g;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.d;
    }

    @DexIgnore
    public final r60 getUiPackageOSVersion() {
        r60 r60 = this.l.get(Short.valueOf(pb1.UI_PACKAGE_FILE.a));
        return r60 != null ? r60 : new r60((byte) 0, (byte) 0);
    }

    @DexIgnore
    public final s60 getWatchParameterVersions() {
        r60 r60 = this.m.get(Short.valueOf(pb1.WATCH_PARAMETERS_FILE.a));
        if (r60 == null) {
            r60 = b21.x.d();
        }
        r60 r602 = this.l.get(Short.valueOf(pb1.WATCH_PARAMETERS_FILE.a));
        if (r602 == null) {
            r602 = b21.x.d();
        }
        return new s60(r60, r602);
    }

    @DexIgnore
    public final o80[] h() {
        return this.o;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = this.l.hashCode();
        int hashCode11 = this.m.hashCode();
        int hashCode12 = this.n.hashCode();
        int hashCode13 = this.p.hashCode();
        int hashCode14 = this.q.hashCode();
        int hashCode15 = this.r.hashCode();
        return this.s.hashCode() + ((hashCode15 + ((hashCode14 + ((hashCode13 + ((((hashCode12 + ((hashCode11 + ((hashCode10 + ((hashCode9 + ((hashCode8 + ((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (this.b.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.o)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public final LinkedHashMap<Short, r60> i() {
        return this.l;
    }

    @DexIgnore
    public final r60 j() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder b2 = yh0.b("DeviceInformation(name=");
        b2.append(this.b);
        b2.append(", macAddress=");
        b2.append(this.c);
        b2.append(", serialNumber=");
        b2.append(this.d);
        b2.append(", hardwareRevision=");
        b2.append(this.e);
        b2.append(", firmwareVersion=");
        b2.append(this.f);
        b2.append(", modelNumber=");
        b2.append(this.g);
        b2.append(", heartRateSerialNumber=");
        b2.append(this.h);
        b2.append(", bootloaderVersion=");
        b2.append(this.i);
        b2.append(", watchAppVersion=");
        b2.append(this.j);
        b2.append(", fontVersion=");
        b2.append(this.k);
        b2.append(", supportedFilesVersion=");
        b2.append(this.l);
        b2.append(", currentFilesVersion=");
        b2.append(this.m);
        b2.append(", bondRequired=");
        b2.append(this.n);
        b2.append(", supportedDeviceConfigKeys=");
        b2.append(Arrays.toString(this.o));
        b2.append(", deviceSecurityVersion=");
        b2.append(this.p);
        b2.append(", localeString=");
        b2.append(this.q);
        b2.append(", microAppVersion=");
        b2.append(this.r);
        b2.append(", fastPairIdInHexString=");
        b2.append(this.s);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.G, this.b), r51.J2, yz0.a(this.a)), r51.i0, this.c), r51.j0, this.d), r51.k0, this.e), r51.l0, this.f), r51.m0, this.g), r51.n0, this.h), r51.o0, this.i.getShortDescription()), r51.p0, this.j.getShortDescription()), r51.q0, this.k.getShortDescription()), r51.G2, a(this.l)), r51.A4, a(this.m));
        r51 r51 = r51.q3;
        int i2 = jq1.a[this.n.ordinal()];
        boolean z = true;
        if (i2 == 1) {
            z = false;
        } else if (i2 != 2) {
            throw new p87();
        }
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(a2, r51, Boolean.valueOf(z)), r51.r0, yz0.a(this.o)), r51.t2, this.p.getShortDescription()), r51.k3, this.q), r51.r3, this.r.getShortDescription()), r51.s3, this.s);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* synthetic */ c(zd7 zd7) {
        }

        @DexIgnore
        public final boolean a(String str) {
            if (str.length() != 10) {
                return false;
            }
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isLetterOrDigit(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public final m60 a(byte[] bArr) throws IllegalArgumentException {
            m60 m60;
            a a;
            m60 m602 = new m60("", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            m60 m603 = m602;
            int i = 0;
            while (i <= bArr.length - 3) {
                short s = order.getShort(i);
                byte b = order.get(i + 2);
                b a2 = b.c.a(s);
                int i2 = i + 3;
                int i3 = i2 + b;
                if (bArr.length < i3) {
                    return m603;
                }
                byte[] a3 = s97.a(bArr, i2, i3);
                switch (jo1.a[a2.ordinal()]) {
                    case 1:
                        Charset forName = Charset.forName("US-ASCII");
                        ee7.a((Object) forName, "Charset.forName(\"US-ASCII\")");
                        String str = new String(a3, forName);
                        if (a(str)) {
                            m603 = m60.a(m603, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262139);
                            continue;
                            i += b + 3;
                        } else {
                            throw new IllegalArgumentException("Invalid Serial Number: " + str);
                        }
                    case 2:
                        Charset forName2 = Charset.forName("US-ASCII");
                        ee7.a((Object) forName2, "Charset.forName(\"US-ASCII\")");
                        m60 = m60.a(m603, null, null, null, new String(a3, forName2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262135);
                        m603 = m60;
                        break;
                    case 3:
                        Charset forName3 = Charset.forName("US-ASCII");
                        ee7.a((Object) forName3, "Charset.forName(\"US-ASCII\")");
                        m60 = m60.a(m603, null, null, null, null, new String(a3, forName3), null, null, null, null, null, null, null, null, null, null, null, null, null, 262127);
                        m603 = m60;
                        break;
                    case 4:
                        Charset forName4 = Charset.forName("US-ASCII");
                        ee7.a((Object) forName4, "Charset.forName(\"US-ASCII\")");
                        m60 = m60.a(m603, null, null, null, null, null, new String(a3, forName4), null, null, null, null, null, null, null, null, null, null, null, null, 262111);
                        m603 = m60;
                        break;
                    case 5:
                        Charset forName5 = Charset.forName("US-ASCII");
                        ee7.a((Object) forName5, "Charset.forName(\"US-ASCII\")");
                        m60 = m60.a(m603, null, null, null, null, null, null, new String(a3, forName5), null, null, null, null, null, null, null, null, null, null, null, 262079);
                        m603 = m60;
                        break;
                    case 6:
                        r60 a4 = r60.CREATOR.a(a3);
                        if (a4 != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, a4, null, null, null, null, null, null, null, null, null, null, 262015);
                            m603 = m60;
                            break;
                        }
                        break;
                    case 7:
                        r60 a5 = r60.CREATOR.a(a3);
                        if (a5 != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, null, a5, null, null, null, null, null, null, null, null, null, 261887);
                            m603 = m60;
                            break;
                        }
                        break;
                    case 8:
                        r60 a6 = r60.CREATOR.a(a3);
                        if (a6 != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, null, null, a6, null, null, null, null, null, null, null, null, 261631);
                            m603 = m60;
                            break;
                        }
                        break;
                    case 9:
                        r60 a7 = r60.CREATOR.a(a3);
                        if (a7 != null) {
                            m603.d().put(Short.valueOf(pb1.LUTS_FILE.a), a7);
                            break;
                        }
                        break;
                    case 10:
                        jf7 a8 = qf7.a(t97.a(a3), 3);
                        int first = a8.getFirst();
                        int last = a8.getLast();
                        int a9 = a8.a();
                        if (a9 < 0 ? first >= last : first <= last) {
                            while (true) {
                                pb1 a10 = pb1.z.a(a3[first]);
                                if (a10 != null) {
                                    m603.i().put(Short.valueOf(a10.a), new r60(a3[first + 1], a3[first + 2]));
                                    i97 i97 = i97.a;
                                }
                                if (first == last) {
                                    break;
                                } else {
                                    first += a9;
                                }
                            }
                        }
                    case 11:
                        jf7 a11 = qf7.a(t97.a(a3), 4);
                        int first2 = a11.getFirst();
                        int last2 = a11.getLast();
                        int a12 = a11.a();
                        if (a12 < 0 ? first2 >= last2 : first2 <= last2) {
                            while (true) {
                                ByteBuffer order2 = ByteBuffer.wrap(a3).order(ByteOrder.LITTLE_ENDIAN);
                                m603.d().put(Short.valueOf(order2.getShort(first2)), new r60(order2.get(first2 + 2), order2.get(first2 + 3)));
                                if (first2 == last2) {
                                    break;
                                } else {
                                    first2 += a12;
                                }
                            }
                        }
                    case 12:
                        if ((!(a3.length == 0)) && (a = a.c.a(a3[0])) != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, null, null, null, null, null, a, null, null, null, null, null, 258047);
                            i97 i972 = i97.a;
                            m603 = m60;
                            break;
                        }
                    case 13:
                        ByteBuffer order3 = ByteBuffer.wrap(a3).order(ByteOrder.LITTLE_ENDIAN);
                        ArrayList arrayList = new ArrayList();
                        jf7 a13 = qf7.a(t97.a(a3), 2);
                        int first3 = a13.getFirst();
                        int last3 = a13.getLast();
                        int a14 = a13.a();
                        if (a14 < 0 ? first3 >= last3 : first3 <= last3) {
                            while (true) {
                                o80 a15 = o80.d.a(order3.getShort(first3));
                                if (a15 != null) {
                                    arrayList.add(a15);
                                }
                                if (first3 != last3) {
                                    first3 += a14;
                                }
                            }
                        }
                        Object[] array = arrayList.toArray(new o80[0]);
                        if (array != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, null, null, null, null, null, null, (o80[]) array, null, null, null, null, 253951);
                            m603 = m60;
                            break;
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    case 14:
                        r60 a16 = r60.CREATOR.a(a3);
                        if (a16 != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, null, null, null, null, null, null, null, a16, null, null, null, 245759);
                            m603 = m60;
                            break;
                        }
                        break;
                    case 16:
                        Charset forName6 = Charset.forName("US-ASCII");
                        ee7.a((Object) forName6, "Charset.forName(\"US-ASCII\")");
                        m60 = m60.a(m603, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, new String(a3, forName6), null, null, 229375);
                        m603 = m60;
                        break;
                    case 17:
                        r60 a17 = r60.CREATOR.a(a3);
                        if (a17 != null) {
                            m603.d().put((short) 1794, a17);
                            break;
                        }
                        break;
                    case 18:
                        r60 a18 = r60.CREATOR.a(a3);
                        if (a18 != null) {
                            m60 = m60.a(m603, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, a18, null, 196607);
                            m603 = m60;
                            break;
                        }
                        break;
                }
                i += b + 3;
            }
            return m603;
        }

        @DexIgnore
        public final m60 a(JSONObject jSONObject) {
            String optString = jSONObject.optString(yz0.a(r51.G), "");
            String optString2 = jSONObject.optString(yz0.a(r51.i0), "");
            ee7.a((Object) optString, "name");
            if ((optString.length() == 0) || !BluetoothAdapter.checkBluetoothAddress(optString2)) {
                return null;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            JSONArray optJSONArray = jSONObject.optJSONArray(yz0.a(r51.G2));
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            Short valueOf = Short.valueOf(Short.parseShort(optJSONObject.get(yz0.a(r51.y0)).toString()));
                            r60.a aVar = r60.CREATOR;
                            String optString3 = optJSONObject.optString(yz0.a(r51.I2));
                            ee7.a((Object) optString3, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap.put(valueOf, aVar.a(optString3));
                        }
                    } catch (JSONException e) {
                        wl0.h.a(e);
                    }
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            JSONArray optJSONArray2 = jSONObject.optJSONArray(yz0.a(r51.A4));
            if (optJSONArray2 != null) {
                int length2 = optJSONArray2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    try {
                        JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject2 != null) {
                            Short valueOf2 = Short.valueOf(Short.parseShort(optJSONObject2.get(yz0.a(r51.y0)).toString()));
                            r60.a aVar2 = r60.CREATOR;
                            String optString4 = optJSONObject2.optString(yz0.a(r51.I2));
                            ee7.a((Object) optString4, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap2.put(valueOf2, aVar2.a(optString4));
                        }
                    } catch (JSONException e2) {
                        wl0.h.a(e2);
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray3 = jSONObject.optJSONArray(yz0.a(r51.r0));
            if (optJSONArray3 != null) {
                int length3 = optJSONArray3.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    try {
                        o80 a = o80.d.a(optJSONArray3.get(i3).toString());
                        if (a != null) {
                            arrayList.add(a);
                        }
                    } catch (JSONException e3) {
                        wl0.h.a(e3);
                    }
                }
            }
            ee7.a((Object) optString2, "macAddress");
            String optString5 = jSONObject.optString(yz0.a(r51.j0), "");
            ee7.a((Object) optString5, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString6 = jSONObject.optString(yz0.a(r51.k0), "");
            ee7.a((Object) optString6, "jsonObject.optString(JSO\u2026VISION.lowerCaseName, \"\")");
            String optString7 = jSONObject.optString(yz0.a(r51.l0), "");
            ee7.a((Object) optString7, "jsonObject.optString(JSO\u2026ERSION.lowerCaseName, \"\")");
            String optString8 = jSONObject.optString(yz0.a(r51.m0), "");
            ee7.a((Object) optString8, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString9 = jSONObject.optString(yz0.a(r51.n0), "");
            ee7.a((Object) optString9, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            r60.a aVar3 = r60.CREATOR;
            String optString10 = jSONObject.optString(yz0.a(r51.o0), "");
            ee7.a((Object) optString10, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            r60 a2 = aVar3.a(optString10);
            r60.a aVar4 = r60.CREATOR;
            String optString11 = jSONObject.optString(yz0.a(r51.p0), "");
            ee7.a((Object) optString11, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            r60 a3 = aVar4.a(optString11);
            r60.a aVar5 = r60.CREATOR;
            String optString12 = jSONObject.optString(yz0.a(r51.q0), "");
            ee7.a((Object) optString12, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            r60 a4 = aVar5.a(optString12);
            a a5 = a.c.a((byte) jSONObject.optInt(yz0.a(r51.q3), 0));
            if (a5 == null) {
                a5 = a.NO_REQUIRE;
            }
            Object[] array = arrayList.toArray(new o80[0]);
            if (array != null) {
                o80[] o80Arr = (o80[]) array;
                r60.a aVar6 = r60.CREATOR;
                String optString13 = jSONObject.optString(yz0.a(r51.t2), "");
                ee7.a((Object) optString13, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                r60 a6 = aVar6.a(optString13);
                String optString14 = jSONObject.optString(yz0.a(r51.k3), "en_US");
                ee7.a((Object) optString14, "jsonObject.optString(JSO\u2026nt.DEFAULT_LOCALE_STRING)");
                r60.a aVar7 = r60.CREATOR;
                String optString15 = jSONObject.optString(yz0.a(r51.r3), "");
                ee7.a((Object) optString15, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                r60 a7 = aVar7.a(optString15);
                String optString16 = jSONObject.optString(yz0.a(r51.s3), "");
                ee7.a((Object) optString16, "jsonObject.optString(JSO\u2026STRING.lowerCaseName, \"\")");
                return new m60(optString, optString2, optString5, optString6, optString7, optString8, optString9, a2, a3, a4, linkedHashMap, linkedHashMap2, a5, o80Arr, a6, optString14, a7, optString16);
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final m60 a(m60 m60, m60 m602) {
            String str;
            String str2;
            String str3;
            String str4;
            String str5;
            String str6;
            String str7;
            r60 r60;
            r60 r602;
            r60 r603;
            LinkedHashMap<Short, r60> linkedHashMap;
            LinkedHashMap<Short, r60> linkedHashMap2;
            o80[] o80Arr;
            r60 r604;
            String str8;
            r60 r605;
            String str9;
            if (m602.getName().length() > 0) {
                str = m602.getName();
            } else {
                str = m60.getName();
            }
            if (m602.getMacAddress().length() > 0) {
                str2 = m602.getMacAddress();
            } else {
                str2 = m60.getMacAddress();
            }
            if (a(m602.getSerialNumber())) {
                str3 = m602.getSerialNumber();
            } else {
                str3 = m60.getSerialNumber();
            }
            if (m602.getHardwareRevision().length() > 0) {
                str4 = m602.getHardwareRevision();
            } else {
                str4 = m60.getHardwareRevision();
            }
            if (m602.getFirmwareVersion().length() > 0) {
                str5 = m602.getFirmwareVersion();
            } else {
                str5 = m60.getFirmwareVersion();
            }
            if (m602.getModelNumber().length() > 0) {
                str6 = m602.getModelNumber();
            } else {
                str6 = m60.getModelNumber();
            }
            if (m602.g().length() > 0) {
                str7 = m602.g();
            } else {
                str7 = m60.g();
            }
            if (!ee7.a(m602.c(), new r60((byte) 0, (byte) 0))) {
                r60 = m602.c();
            } else {
                r60 = m60.c();
            }
            if (!ee7.a(m602.j(), new r60((byte) 0, (byte) 0))) {
                r602 = m602.j();
            } else {
                r602 = m60.j();
            }
            if (!ee7.a(m602.f(), new r60((byte) 0, (byte) 0))) {
                r603 = m602.f();
            } else {
                r603 = m60.f();
            }
            if (!m602.i().isEmpty()) {
                linkedHashMap = m602.i();
            } else {
                linkedHashMap = m60.i();
            }
            if (!m602.d().isEmpty()) {
                linkedHashMap2 = m602.d();
            } else {
                linkedHashMap2 = m60.d();
            }
            a b = m602.b();
            if (!(m602.h().length == 0)) {
                o80Arr = m602.h();
            } else {
                o80Arr = m60.h();
            }
            if (!ee7.a(m602.e(), new r60((byte) 0, (byte) 0))) {
                r604 = m602.e();
            } else {
                r604 = m60.e();
            }
            if (!ee7.a((Object) m602.getLocaleString(), (Object) "en_US")) {
                str8 = m602.getLocaleString();
            } else {
                str8 = m60.getLocaleString();
            }
            if (!ee7.a(m602.getMicroAppVersion(), new r60((byte) 0, (byte) 0))) {
                r605 = m602.getMicroAppVersion();
            } else {
                r605 = m60.getMicroAppVersion();
            }
            if (!ee7.a((Object) m602.getFastPairIdInHexString(), (Object) "")) {
                str9 = m602.getFastPairIdInHexString();
            } else {
                str9 = m60.getFastPairIdInHexString();
            }
            return new m60(str, str2, str3, str4, str5, str6, str7, r60, r602, r603, linkedHashMap, linkedHashMap2, b, o80Arr, r604, str8, r605, str9);
        }
    }

    @DexIgnore
    public m60(String str, String str2, String str3, String str4, String str5, String str6, String str7, r60 r60, r60 r602, r60 r603, LinkedHashMap<Short, r60> linkedHashMap, LinkedHashMap<Short, r60> linkedHashMap2, a aVar, o80[] o80Arr, r60 r604, String str8, r60 r605, String str9) {
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = r60;
        this.j = r602;
        this.k = r603;
        this.l = linkedHashMap;
        this.m = linkedHashMap2;
        this.n = aVar;
        this.o = o80Arr;
        this.p = r604;
        this.q = str8;
        this.r = r605;
        this.s = str9;
        this.a = n60.d.a(str6, str3);
    }

    @DexIgnore
    public final JSONArray a(HashMap<Short, r60> hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry<Short, r60> entry : hashMap.entrySet()) {
            jSONArray.put(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.y0, yz0.a(entry.getKey().shortValue())), r51.e4, a81.d.a(entry.getKey().shortValue())), r51.I2, entry.getValue().toString()));
        }
        return jSONArray;
    }
}
