package com.fossil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir7 {
    @DexIgnore
    public static final ar7 a(sr7 sr7) {
        ee7.b(sr7, "$this$buffer");
        return new mr7(sr7);
    }

    @DexIgnore
    public static final qr7 b(File file) throws FileNotFoundException {
        return a(file, false, 1, null);
    }

    @DexIgnore
    public static final sr7 b(Socket socket) throws IOException {
        ee7.b(socket, "$this$source");
        rr7 rr7 = new rr7(socket);
        InputStream inputStream = socket.getInputStream();
        ee7.a((Object) inputStream, "getInputStream()");
        return rr7.a(new hr7(inputStream, rr7));
    }

    @DexIgnore
    public static final sr7 c(File file) throws FileNotFoundException {
        ee7.b(file, "$this$source");
        return a(new FileInputStream(file));
    }

    @DexIgnore
    public static final zq7 a(qr7 qr7) {
        ee7.b(qr7, "$this$buffer");
        return new lr7(qr7);
    }

    @DexIgnore
    public static final qr7 a(OutputStream outputStream) {
        ee7.b(outputStream, "$this$sink");
        return new jr7(outputStream, new tr7());
    }

    @DexIgnore
    public static final sr7 a(InputStream inputStream) {
        ee7.b(inputStream, "$this$source");
        return new hr7(inputStream, new tr7());
    }

    @DexIgnore
    public static final qr7 a() {
        return new xq7();
    }

    @DexIgnore
    public static final qr7 a(Socket socket) throws IOException {
        ee7.b(socket, "$this$sink");
        rr7 rr7 = new rr7(socket);
        OutputStream outputStream = socket.getOutputStream();
        ee7.a((Object) outputStream, "getOutputStream()");
        return rr7.a(new jr7(outputStream, rr7));
    }

    @DexIgnore
    public static final qr7 a(File file, boolean z) throws FileNotFoundException {
        ee7.b(file, "$this$sink");
        return a(new FileOutputStream(file, z));
    }

    @DexIgnore
    public static /* synthetic */ qr7 a(File file, boolean z, int i, Object obj) throws FileNotFoundException {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(file, z);
    }

    @DexIgnore
    public static final qr7 a(File file) throws FileNotFoundException {
        ee7.b(file, "$this$appendingSink");
        return a(new FileOutputStream(file, true));
    }

    @DexIgnore
    public static final boolean a(AssertionError assertionError) {
        ee7.b(assertionError, "$this$isAndroidGetsocknameError");
        if (assertionError.getCause() == null) {
            return false;
        }
        String message = assertionError.getMessage();
        return message != null ? nh7.a(message, "getsockname failed", false, 2, null) : false;
    }
}
