package com.fossil;

import android.content.Context;
import android.os.Binder;
import com.fossil.a12;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rz1 extends mz1 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public rz1(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void E() {
        if (!q02.b(this.a, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.lz1
    public final void a() {
        E();
        xy1 a2 = xy1.a(this.a);
        GoogleSignInAccount b = a2.b();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.u;
        if (b != null) {
            googleSignInOptions = a2.c();
        }
        a12.a aVar = new a12.a(this.a);
        aVar.a(my1.e, googleSignInOptions);
        a12 a3 = aVar.a();
        try {
            if (a3.a().x()) {
                if (b != null) {
                    my1.f.a(a3);
                } else {
                    a3.b();
                }
            }
        } finally {
            a3.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.lz1
    public final void h() {
        E();
        kz1.a(this.a).a();
    }
}
