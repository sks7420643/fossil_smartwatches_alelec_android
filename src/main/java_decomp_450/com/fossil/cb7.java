package com.fossil;

import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cb7 implements ib7.b {
    @DexIgnore
    public /* final */ ib7.c<?> key;

    @DexIgnore
    public cb7(ib7.c<?> cVar) {
        ee7.b(cVar, "key");
        this.key = cVar;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public <R> R fold(R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
        ee7.b(kd7, "operation");
        return (R) ib7.b.a.a(this, r, kd7);
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.ib7.b
    public <E extends ib7.b> E get(ib7.c<E> cVar) {
        ee7.b(cVar, "key");
        return (E) ib7.b.a.a(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.ib7.b
    public ib7.c<?> getKey() {
        return this.key;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 minusKey(ib7.c<?> cVar) {
        ee7.b(cVar, "key");
        return ib7.b.a.b(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 plus(ib7 ib7) {
        ee7.b(ib7, "context");
        return ib7.b.a.a(this, ib7);
    }
}
