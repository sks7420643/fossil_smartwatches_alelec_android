package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mq6 implements MembersInjector<kq6> {
    @DexIgnore
    public static void a(kq6 kq6, ao5 ao5) {
        kq6.e = ao5;
    }

    @DexIgnore
    public static void a(kq6 kq6, bo5 bo5) {
        kq6.f = bo5;
    }

    @DexIgnore
    public static void a(kq6 kq6, mn5 mn5) {
        kq6.g = mn5;
    }

    @DexIgnore
    public static void a(kq6 kq6, qd5 qd5) {
        kq6.h = qd5;
    }

    @DexIgnore
    public static void a(kq6 kq6) {
        kq6.A();
    }
}
