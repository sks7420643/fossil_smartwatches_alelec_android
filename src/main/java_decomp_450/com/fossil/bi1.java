package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bi1 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ le1 CREATOR; // = new le1(null);
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte[] e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ boolean i;

    @DexIgnore
    public bi1(String str, byte b2, byte b3, byte[] bArr, long j, long j2, long j3, boolean z) {
        this.b = str;
        this.c = b2;
        this.d = b3;
        this.e = bArr;
        this.f = j;
        this.g = j2;
        this.h = j3;
        this.i = z;
    }

    @DexIgnore
    public static /* synthetic */ bi1 a(bi1 bi1, String str, byte b2, byte b3, byte[] bArr, long j, long j2, long j3, boolean z, int i2) {
        return bi1.a((i2 & 1) != 0 ? bi1.b : str, (i2 & 2) != 0 ? bi1.c : b2, (i2 & 4) != 0 ? bi1.d : b3, (i2 & 8) != 0 ? bi1.e : bArr, (i2 & 16) != 0 ? bi1.f : j, (i2 & 32) != 0 ? bi1.g : j2, (i2 & 64) != 0 ? bi1.h : j3, (i2 & 128) != 0 ? bi1.i : z);
    }

    @DexIgnore
    public final bi1 a(String str, byte b2, byte b3, byte[] bArr, long j, long j2, long j3, boolean z) {
        return new bi1(str, b2, b3, bArr, j, j2, j3, z);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return a(true);
    }

    @DexIgnore
    public final long b() {
        return this.h;
    }

    @DexIgnore
    public final short c() {
        return ByteBuffer.allocate(2).put(this.c).put(this.d).getShort(0);
    }

    @DexIgnore
    public final boolean d() {
        return this.i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(bi1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            bi1 bi1 = (bi1) obj;
            return !(ee7.a(this.b, bi1.b) ^ true) && this.c == bi1.c && this.d == bi1.d && Arrays.equals(this.e, bi1.e) && this.f == bi1.f && this.g == bi1.g && this.h == bi1.h && this.i == bi1.i;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.database.entity.DeviceFile");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.e);
        int hashCode2 = Long.valueOf(this.f).hashCode();
        int hashCode3 = Long.valueOf(this.g).hashCode();
        int hashCode4 = Long.valueOf(this.h).hashCode();
        return Boolean.valueOf(this.i).hashCode() + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (((((this.b.hashCode() * 31) + this.c) * 31) + this.d) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder b2 = yh0.b("DeviceFile(deviceMacAddress=");
        b2.append(this.b);
        b2.append(", fileType=");
        b2.append((int) this.c);
        b2.append(", fileIndex=");
        b2.append((int) this.d);
        b2.append(", rawData=");
        b2.append(Arrays.toString(this.e));
        b2.append(", fileLength=");
        b2.append(this.f);
        b2.append(", fileCrc=");
        b2.append(this.g);
        b2.append(", createdTimeStamp=");
        b2.append(this.h);
        b2.append(", isCompleted=");
        b2.append(this.i);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.e);
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i ? 1 : 0);
        }
    }

    @DexIgnore
    public bi1(String str, short s, long j, long j2) {
        this(str, CREATOR.b(s), CREATOR.a(s), new byte[0], j, j2, 0, false);
    }

    @DexIgnore
    public final JSONObject a(boolean z) {
        JSONObject a2 = yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.i0, this.b), r51.y0, yz0.a(c())), r51.H, Long.valueOf(this.f)), r51.I, Long.valueOf(this.g)), r51.R0, Integer.valueOf(this.e.length)), r51.A1, Double.valueOf(yz0.a(this.h))), r51.L2, Boolean.valueOf(this.i)), r51.e4, a81.d.a(c()));
        if (z) {
            yz0.a(a2, r51.Q0, yz0.a(this.e, (String) null, 1));
        }
        pb1 a3 = pb1.z.a(this.c);
        if (a3 != null) {
            int i2 = fg1.a[a3.ordinal()];
            if (i2 == 1) {
                yz0.a(a2, r51.f4, (Object) -1);
                byte[] bArr = this.e;
                if (bArr.length >= 18) {
                    ByteBuffer order = ByteBuffer.wrap(s97.a(bArr, 16, 18)).order(ByteOrder.LITTLE_ENDIAN);
                    ee7.a((Object) order, "byteBuffer");
                    yz0.a(a2, r51.f4, Integer.valueOf(yz0.b(order.getShort())));
                }
            } else if (i2 == 2) {
                yz0.a(a2, r51.h2, "unknown");
                yz0.a(a2, r51.f4, (Object) -1);
                int i3 = 0;
                while (true) {
                    int i4 = i3 + 16;
                    byte[] bArr2 = this.e;
                    if (i4 > bArr2.length) {
                        break;
                    }
                    byte[] a4 = s97.a(bArr2, i3, i4);
                    byte b2 = a4[0];
                    if (b2 != -59) {
                        if (b2 == 0) {
                            r51 r51 = r51.h2;
                            StringBuilder sb = new StringBuilder();
                            sb.append((int) yz0.b(a4[1]));
                            sb.append('.');
                            sb.append((int) yz0.b(a4[2]));
                            yz0.a(a2, r51, sb.toString());
                        }
                    } else if (a4[1] == 9) {
                        ByteBuffer order2 = ByteBuffer.wrap(s97.a(s97.a(a4, 2, 5), (byte) 0)).order(ByteOrder.LITTLE_ENDIAN);
                        r51 r512 = r51.f4;
                        ee7.a((Object) order2, "byteBuffer");
                        yz0.a(a2, r512, Integer.valueOf(order2.getInt()));
                    }
                    i3 = i4;
                }
            }
        }
        return a2;
    }
}
