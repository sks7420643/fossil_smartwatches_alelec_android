package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.f;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x3 extends Service {
    @DexIgnore
    public /* final */ Map<IBinder, IBinder.DeathRecipient> a; // = new n4();
    @DexIgnore
    public f.a b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends f.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x3$a$a")
        /* renamed from: com.fossil.x3$a$a  reason: collision with other inner class name */
        public class C0239a implements IBinder.DeathRecipient {
            @DexIgnore
            public /* final */ /* synthetic */ z3 a;

            @DexIgnore
            public C0239a(z3 z3Var) {
                this.a = z3Var;
            }

            @DexIgnore
            public void binderDied() {
                x3.this.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.f
        public boolean a(long j) {
            return x3.this.a(j);
        }

        @DexIgnore
        @Override // com.fossil.f
        public Bundle b(String str, Bundle bundle) {
            return x3.this.a(str, bundle);
        }

        @DexIgnore
        @Override // com.fossil.f
        public boolean a(e eVar) {
            z3 z3Var = new z3(eVar);
            try {
                C0239a aVar = new C0239a(z3Var);
                synchronized (x3.this.a) {
                    eVar.asBinder().linkToDeath(aVar, 0);
                    x3.this.a.put(eVar.asBinder(), aVar);
                }
                return x3.this.b(z3Var);
            } catch (RemoteException unused) {
                return false;
            }
        }

        @DexIgnore
        @Override // com.fossil.f
        public boolean b(e eVar, Bundle bundle) {
            return x3.this.a(new z3(eVar), bundle);
        }

        @DexIgnore
        @Override // com.fossil.f
        public int b(e eVar, String str, Bundle bundle) {
            return x3.this.a(new z3(eVar), str, bundle);
        }

        @DexIgnore
        @Override // com.fossil.f
        public boolean a(e eVar, Uri uri, Bundle bundle, List<Bundle> list) {
            return x3.this.a(new z3(eVar), uri, bundle, list);
        }

        @DexIgnore
        @Override // com.fossil.f
        public boolean a(e eVar, Uri uri) {
            return x3.this.a(new z3(eVar), uri);
        }

        @DexIgnore
        @Override // com.fossil.f
        public boolean a(e eVar, int i, Uri uri, Bundle bundle) {
            return x3.this.a(new z3(eVar), i, uri, bundle);
        }
    }

    @DexIgnore
    public abstract int a(z3 z3Var, String str, Bundle bundle);

    @DexIgnore
    public abstract Bundle a(String str, Bundle bundle);

    @DexIgnore
    public abstract boolean a(long j);

    @DexIgnore
    public boolean a(z3 z3Var) {
        try {
            synchronized (this.a) {
                IBinder a2 = z3Var.a();
                a2.unlinkToDeath(this.a.get(a2), 0);
                this.a.remove(a2);
            }
            return true;
        } catch (NoSuchElementException unused) {
            return false;
        }
    }

    @DexIgnore
    public abstract boolean a(z3 z3Var, int i, Uri uri, Bundle bundle);

    @DexIgnore
    public abstract boolean a(z3 z3Var, Uri uri);

    @DexIgnore
    public abstract boolean a(z3 z3Var, Uri uri, Bundle bundle, List<Bundle> list);

    @DexIgnore
    public abstract boolean a(z3 z3Var, Bundle bundle);

    @DexIgnore
    public abstract boolean b(z3 z3Var);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.b;
    }
}
