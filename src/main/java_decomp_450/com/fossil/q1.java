package com.fossil;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.fossil.i0;
import com.fossil.v1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q1 implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, v1.a {
    @DexIgnore
    public p1 a;
    @DexIgnore
    public i0 b;
    @DexIgnore
    public n1 c;
    @DexIgnore
    public v1.a d;

    @DexIgnore
    public q1(p1 p1Var) {
        this.a = p1Var;
    }

    @DexIgnore
    public void a(IBinder iBinder) {
        p1 p1Var = this.a;
        i0.a aVar = new i0.a(p1Var.e());
        n1 n1Var = new n1(aVar.b(), e0.abc_list_menu_item_layout);
        this.c = n1Var;
        n1Var.a(this);
        this.a.a(this.c);
        aVar.a(this.c.a(), this);
        View i = p1Var.i();
        if (i != null) {
            aVar.a(i);
        } else {
            aVar.a(p1Var.g());
            aVar.b(p1Var.h());
        }
        aVar.a(this);
        i0 a2 = aVar.a();
        this.b = a2;
        a2.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.b.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.b.show();
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a((r1) this.c.a().getItem(i), 0);
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        this.c.a(this.a, true);
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.b.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.b.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.a.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.a.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    public void a() {
        i0 i0Var = this.b;
        if (i0Var != null) {
            i0Var.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.v1.a
    public void a(p1 p1Var, boolean z) {
        if (z || p1Var == this.a) {
            a();
        }
        v1.a aVar = this.d;
        if (aVar != null) {
            aVar.a(p1Var, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.v1.a
    public boolean a(p1 p1Var) {
        v1.a aVar = this.d;
        if (aVar != null) {
            return aVar.a(p1Var);
        }
        return false;
    }
}
