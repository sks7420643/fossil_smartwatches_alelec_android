package com.fossil;

import coil.util.CoilContentProvider;
import com.fossil.lq;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq {
    @DexIgnore
    public static lq a;
    @DexIgnore
    public static vc7<? extends lq> b;
    @DexIgnore
    public static /* final */ iq c; // = new iq();

    @DexIgnore
    public static final void a(lq lqVar) {
        ee7.b(lqVar, "loader");
        lq lqVar2 = a;
        if (lqVar2 != null) {
            lqVar2.shutdown();
        }
        a = lqVar;
        b = null;
    }

    @DexIgnore
    public static final lq b() {
        lq lqVar = a;
        return lqVar != null ? lqVar : c.a();
    }

    @DexIgnore
    public final synchronized lq a() {
        lq lqVar;
        lqVar = a;
        if (lqVar == null) {
            vc7<? extends lq> vc7 = b;
            if (vc7 == null || (lqVar = (lq) vc7.invoke()) == null) {
                lq.a aVar = lq.l;
                lqVar = new mq(CoilContentProvider.b.a()).a();
            }
            b = null;
            a(lqVar);
        }
        return lqVar;
    }
}
