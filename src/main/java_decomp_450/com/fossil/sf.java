package com.fossil;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sf<T> extends AbstractList<T> {
    @DexIgnore
    public static /* final */ List j; // = new ArrayList();
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ ArrayList<List<T>> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(int i);

        @DexIgnore
        void a(int i, int i2);

        @DexIgnore
        void a(int i, int i2, int i3);

        @DexIgnore
        void b();

        @DexIgnore
        void b(int i);

        @DexIgnore
        void b(int i, int i2);

        @DexIgnore
        void b(int i, int i2, int i3);

        @DexIgnore
        void c(int i, int i2);
    }

    @DexIgnore
    public sf() {
        this.a = 0;
        this.b = new ArrayList<>();
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 1;
        this.h = 0;
        this.i = 0;
    }

    @DexIgnore
    public final void a(int i2, List<T> list, int i3, int i4) {
        this.a = i2;
        this.b.clear();
        this.b.add(list);
        this.c = i3;
        this.d = i4;
        int size = list.size();
        this.e = size;
        this.f = size;
        this.g = list.size();
        this.h = 0;
        this.i = 0;
    }

    @DexIgnore
    public int b() {
        int i2 = this.c;
        for (int size = this.b.size() - 1; size >= 0; size--) {
            List<T> list = this.b.get(size);
            if (list != null && list != j) {
                break;
            }
            i2 += this.g;
        }
        return i2;
    }

    @DexIgnore
    public boolean c(int i2, int i3) {
        return a(i2, i3, this.b.size() - 1);
    }

    @DexIgnore
    public boolean d(int i2, int i3) {
        return a(i2, i3, 0);
    }

    @DexIgnore
    public int e() {
        return this.a;
    }

    @DexIgnore
    public int f() {
        return this.a + this.d + (this.f / 2);
    }

    @DexIgnore
    public int g() {
        return this.i;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public T get(int i2) {
        int i3;
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        int i4 = i2 - this.a;
        if (i4 >= 0 && i4 < this.f) {
            if (m()) {
                int i5 = this.g;
                i3 = i4 / i5;
                i4 %= i5;
            } else {
                int size = this.b.size();
                i3 = 0;
                while (i3 < size) {
                    int size2 = this.b.get(i3).size();
                    if (size2 > i4) {
                        break;
                    }
                    i4 -= size2;
                    i3++;
                }
            }
            List<T> list = this.b.get(i3);
            if (!(list == null || list.size() == 0)) {
                return list.get(i4);
            }
        }
        return null;
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public int i() {
        return this.b.size();
    }

    @DexIgnore
    public int j() {
        return this.d;
    }

    @DexIgnore
    public int k() {
        return this.f;
    }

    @DexIgnore
    public int l() {
        return this.c;
    }

    @DexIgnore
    public boolean m() {
        return this.g > 0;
    }

    @DexIgnore
    public sf<T> n() {
        return new sf<>(this);
    }

    @DexIgnore
    public int size() {
        return this.a + this.f + this.c;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("leading " + this.a + ", storage " + this.f + ", trailing " + l());
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            sb.append(" ");
            sb.append(this.b.get(i2));
        }
        return sb.toString();
    }

    @DexIgnore
    public T c() {
        return this.b.get(0).get(0);
    }

    @DexIgnore
    public T d() {
        ArrayList<List<T>> arrayList = this.b;
        List<T> list = arrayList.get(arrayList.size() - 1);
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public boolean b(int i2, int i3, int i4) {
        if (this.e + i4 <= i2 || this.b.size() <= 1 || this.e < i3) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean b(boolean z, int i2, int i3, a aVar) {
        int i4 = 0;
        while (d(i2, i3)) {
            List<T> remove = this.b.remove(0);
            int size = remove == null ? this.g : remove.size();
            i4 += size;
            this.f -= size;
            this.e -= remove == null ? 0 : remove.size();
        }
        if (i4 > 0) {
            if (z) {
                int i5 = this.a;
                this.a = i5 + i4;
                aVar.a(i5, i4);
            } else {
                this.d += i4;
                aVar.b(this.a, i4);
            }
        }
        if (i4 > 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public sf(sf<T> sfVar) {
        this.a = sfVar.a;
        this.b = new ArrayList<>(sfVar.b);
        this.c = sfVar.c;
        this.d = sfVar.d;
        this.e = sfVar.e;
        this.f = sfVar.f;
        this.g = sfVar.g;
        this.h = sfVar.h;
        this.i = sfVar.i;
    }

    @DexIgnore
    public void a(int i2, List<T> list, int i3, int i4, a aVar) {
        a(i2, list, i3, i4);
        aVar.a(size());
    }

    @DexIgnore
    public int a() {
        int i2 = this.a;
        int size = this.b.size();
        for (int i3 = 0; i3 < size; i3++) {
            List<T> list = this.b.get(i3);
            if (list != null && list != j) {
                break;
            }
            i2 += this.g;
        }
        return i2;
    }

    @DexIgnore
    public final boolean a(int i2, int i3, int i4) {
        List<T> list = this.b.get(i4);
        return list == null || (this.e > i2 && this.b.size() > 2 && list != j && this.e - list.size() >= i3);
    }

    @DexIgnore
    public void b(List<T> list, a aVar) {
        int size = list.size();
        if (size == 0) {
            aVar.b();
            return;
        }
        int i2 = this.g;
        if (i2 > 0 && size != i2) {
            if (this.b.size() != 1 || size <= this.g) {
                this.g = -1;
            } else {
                this.g = size;
            }
        }
        this.b.add(0, list);
        this.e += size;
        this.f += size;
        int min = Math.min(this.a, size);
        int i3 = size - min;
        if (min != 0) {
            this.a -= min;
        }
        this.d -= i3;
        this.h += size;
        aVar.a(this.a, min, i3);
    }

    @DexIgnore
    public boolean a(boolean z, int i2, int i3, a aVar) {
        int i4 = 0;
        while (c(i2, i3)) {
            ArrayList<List<T>> arrayList = this.b;
            List<T> remove = arrayList.remove(arrayList.size() - 1);
            int size = remove == null ? this.g : remove.size();
            i4 += size;
            this.f -= size;
            this.e -= remove == null ? 0 : remove.size();
        }
        if (i4 > 0) {
            int i5 = this.a + this.f;
            if (z) {
                this.c += i4;
                aVar.a(i5, i4);
            } else {
                aVar.b(i5, i4);
            }
        }
        if (i4 > 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(List<T> list, a aVar) {
        int size = list.size();
        if (size == 0) {
            aVar.a();
            return;
        }
        if (this.g > 0) {
            ArrayList<List<T>> arrayList = this.b;
            int size2 = arrayList.get(arrayList.size() - 1).size();
            int i2 = this.g;
            if (size2 != i2 || size > i2) {
                this.g = -1;
            }
        }
        this.b.add(list);
        this.e += size;
        this.f += size;
        int min = Math.min(this.c, size);
        int i3 = size - min;
        if (min != 0) {
            this.c -= min;
        }
        this.i += size;
        aVar.b((this.a + this.f) - size, min, i3);
    }

    @DexIgnore
    public void b(int i2, List<T> list, int i3, int i4, int i5, a aVar) {
        boolean z = i4 != Integer.MAX_VALUE;
        boolean z2 = i3 > f();
        if (!z || !b(i4, i5, list.size()) || !a(i2, z2)) {
            a(i2, list, aVar);
        } else {
            this.b.set((i2 - this.a) / this.g, null);
            this.f -= list.size();
            if (z2) {
                this.b.remove(0);
                this.a += list.size();
            } else {
                ArrayList<List<T>> arrayList = this.b;
                arrayList.remove(arrayList.size() - 1);
                this.c += list.size();
            }
        }
        if (!z) {
            return;
        }
        if (z2) {
            b(true, i4, i5, aVar);
        } else {
            a(true, i4, i5, aVar);
        }
    }

    @DexIgnore
    public boolean a(int i2, boolean z) {
        if (this.g < 1 || this.b.size() < 2) {
            throw new IllegalStateException("Trimming attempt before sufficient load");
        }
        int i3 = this.a;
        if (i2 < i3) {
            return z;
        }
        if (i2 >= this.f + i3) {
            return !z;
        }
        int i4 = (i2 - i3) / this.g;
        if (z) {
            for (int i5 = 0; i5 < i4; i5++) {
                if (this.b.get(i5) != null) {
                    return false;
                }
            }
        } else {
            for (int size = this.b.size() - 1; size > i4; size--) {
                if (this.b.get(size) != null) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public boolean b(int i2, int i3) {
        List<T> list;
        int i4 = this.a / i2;
        if (i3 < i4 || i3 >= this.b.size() + i4 || (list = this.b.get(i3 - i4)) == null || list == j) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void a(int i2, List<T> list, int i3, int i4, int i5, a aVar) {
        int size = (list.size() + (i5 - 1)) / i5;
        int i6 = 0;
        while (i6 < size) {
            int i7 = i6 * i5;
            int i8 = i6 + 1;
            List<T> subList = list.subList(i7, Math.min(list.size(), i8 * i5));
            if (i6 == 0) {
                a(i2, subList, (list.size() + i3) - subList.size(), i4);
            } else {
                a(i7 + i2, subList, (a) null);
            }
            i6 = i8;
        }
        aVar.a(size());
    }

    @DexIgnore
    public void a(int i2, List<T> list, a aVar) {
        int size = list.size();
        if (size != this.g) {
            int size2 = size();
            int i3 = this.g;
            boolean z = false;
            boolean z2 = i2 == size2 - (size2 % i3) && size < i3;
            if (this.c == 0 && this.b.size() == 1 && size > this.g) {
                z = true;
            }
            if (!z && !z2) {
                throw new IllegalArgumentException("page introduces incorrect tiling");
            } else if (z) {
                this.g = size;
            }
        }
        int i4 = i2 / this.g;
        a(i4, i4);
        int i5 = i4 - (this.a / this.g);
        List<T> list2 = this.b.get(i5);
        if (list2 == null || list2 == j) {
            this.b.set(i5, list);
            this.e += size;
            if (aVar != null) {
                aVar.c(i2, size);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Invalid position " + i2 + ": data already loaded");
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int i4;
        int i5 = this.a / this.g;
        if (i2 < i5) {
            int i6 = 0;
            while (true) {
                i4 = i5 - i2;
                if (i6 >= i4) {
                    break;
                }
                this.b.add(0, null);
                i6++;
            }
            int i7 = i4 * this.g;
            this.f += i7;
            this.a -= i7;
        } else {
            i2 = i5;
        }
        if (i3 >= this.b.size() + i2) {
            int min = Math.min(this.c, ((i3 + 1) - (this.b.size() + i2)) * this.g);
            for (int size = this.b.size(); size <= i3 - i2; size++) {
                ArrayList<List<T>> arrayList = this.b;
                arrayList.add(arrayList.size(), null);
            }
            this.f += min;
            this.c -= min;
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, a aVar) {
        int i5 = this.g;
        if (i4 != i5) {
            if (i4 < i5) {
                throw new IllegalArgumentException("Page size cannot be reduced");
            } else if (this.b.size() == 1 && this.c == 0) {
                this.g = i4;
            } else {
                throw new IllegalArgumentException("Page size can change only if last page is only one present");
            }
        }
        int size = size();
        int i6 = this.g;
        int i7 = ((size + i6) - 1) / i6;
        int max = Math.max((i2 - i3) / i6, 0);
        int min = Math.min((i2 + i3) / this.g, i7 - 1);
        a(max, min);
        int i8 = this.a / this.g;
        while (max <= min) {
            int i9 = max - i8;
            if (this.b.get(i9) == null) {
                this.b.set(i9, j);
                aVar.b(max);
            }
            max++;
        }
    }
}
