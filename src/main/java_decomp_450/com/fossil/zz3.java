package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz3<K, V> extends ux3<K, V> {
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient ux3<V, K> inverse;
    @DexIgnore
    public /* final */ transient K singleKey;
    @DexIgnore
    public /* final */ transient V singleValue;

    @DexIgnore
    public zz3(K k, V v) {
        bx3.a(k, v);
        this.singleKey = k;
        this.singleValue = v;
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean containsKey(Object obj) {
        return this.singleKey.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean containsValue(Object obj) {
        return this.singleValue.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public iy3<Map.Entry<K, V>> createEntrySet() {
        return iy3.of(yy3.a((Object) this.singleKey, (Object) this.singleValue));
    }

    @DexIgnore
    @Override // com.fossil.by3
    public iy3<K> createKeySet() {
        return iy3.of(this.singleKey);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.by3
    public V get(Object obj) {
        if (this.singleKey.equals(obj)) {
            return this.singleValue;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.ux3, com.fossil.ux3
    public ux3<V, K> inverse() {
        ux3<V, K> ux3 = this.inverse;
        if (ux3 != null) {
            return ux3;
        }
        zz3 zz3 = new zz3(this.singleValue, this.singleKey, this);
        this.inverse = zz3;
        return zz3;
    }

    @DexIgnore
    public zz3(K k, V v, ux3<V, K> ux3) {
        this.singleKey = k;
        this.singleValue = v;
        this.inverse = ux3;
    }
}
