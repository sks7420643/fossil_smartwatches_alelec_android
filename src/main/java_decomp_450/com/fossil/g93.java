package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g93 extends l93 {
    @DexIgnore
    public g93() {
        super(1, null);
    }

    @DexIgnore
    @Override // com.fossil.l93
    public final String toString() {
        return "[Dot]";
    }
}
