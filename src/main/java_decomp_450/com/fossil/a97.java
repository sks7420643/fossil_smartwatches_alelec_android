package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a97 implements Comparable<a97> {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public /* synthetic */ a97(int i) {
        this.a = i;
    }

    @DexIgnore
    public static boolean a(int i, Object obj) {
        return (obj instanceof a97) && i == ((a97) obj).a();
    }

    @DexIgnore
    public static final /* synthetic */ a97 b(int i) {
        return new a97(i);
    }

    @DexIgnore
    public static int c(int i) {
        return i;
    }

    @DexIgnore
    public static int d(int i) {
        return i;
    }

    @DexIgnore
    public static String e(int i) {
        return String.valueOf(((long) i) & 4294967295L);
    }

    @DexIgnore
    public final /* synthetic */ int a() {
        return this.a;
    }

    @DexIgnore
    public final int a(int i) {
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(a97 a97) {
        a(a97.a());
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a;
        d(i);
        return i;
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }
}
