package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c66 extends z56 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public j56 e;
    @DexIgnore
    public ArrayList<Category> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<MicroApp>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<String, Boolean, Parcelable>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ zd<String> l;
    @DexIgnore
    public /* final */ LiveData<List<MicroApp>> m;
    @DexIgnore
    public /* final */ zd<List<MicroApp>> n;
    @DexIgnore
    public /* final */ zd<v87<String, Boolean, Parcelable>> o;
    @DexIgnore
    public /* final */ a66 p;
    @DexIgnore
    public /* final */ CategoryRepository q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1", f = "MicroAppPresenter.kt", l = {229}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Parcelable>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Parcelable> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return c66.f(this.this$0.this$0).c(this.this$0.$id);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(c66 c66, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = c66;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$id, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            boolean z2;
            Object a2 = nb7.a();
            int i = this.label;
            Parcelable parcelable = null;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                z = pe5.c.d(this.$id);
                if (z) {
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = null;
                    this.Z$0 = z;
                    this.label = 1;
                    obj = vh7.a(a3, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                    z2 = z;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String k = c66.r;
                local.d(k, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.j.a(new v87(this.$id, pb7.a(z), parcelable));
                return i97.a;
            } else if (i == 1) {
                z2 = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) obj;
            z = z2;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String k2 = c66.r;
            local2.d(k2, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.j.a(new v87(this.$id, pb7.a(z), parcelable));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ c66 a;

        @DexIgnore
        public c(c66 c66) {
            this.a = c66;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = c66.r;
            local.d(k, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.p.d(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ c66 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1", f = "MicroAppPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ c66 $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.c66$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.c66$d$a$a  reason: collision with other inner class name */
            public static final class C0022a extends zb7 implements kd7<yi7, fb7<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0022a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0022a aVar = new C0022a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<? extends Category>> fb7) {
                    return ((C0022a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return this.this$0.$this_run.q.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c66 c66, fb7 fb7) {
                super(2, fb7);
                this.$this_run = c66;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$this_run, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 b = qj7.b();
                    C0022a aVar = new C0022a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (!list.isEmpty()) {
                    this.$this_run.h.a((Object) ((Category) list.get(0)).getId());
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(c66 c66) {
            this.a = c66;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(MicroApp microApp) {
            ik7 unused = this.a.b(microApp.getId());
            if (microApp != null) {
                String str = (String) this.a.h.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String k = c66.r;
                local.d(k, "transform from selected microApp to category currentCategory=" + str + " compsCategories=" + microApp.getCategories());
                ArrayList<String> categories = microApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.h.a((Object) categories.get(0));
                } else {
                    this.a.h.a((Object) str);
                }
            } else {
                c66 c66 = this.a;
                ik7 unused2 = xh7.b(c66.e(), null, null, new a(c66, null), 3, null);
            }
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends MicroApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ c66 a;

        @DexIgnore
        public e(c66 c66) {
            this.a = c66;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<MicroApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = c66.r;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged microApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(k, sb.toString());
            if (list != null) {
                this.a.p.w(list);
                MicroApp a2 = c66.f(this.a).d().a();
                if (a2 != null) {
                    this.a.p.b(a2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ c66 a;

        @DexIgnore
        public f(c66 c66) {
            this.a = c66;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<MicroApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d(c66.r, "transform from category to list microApp with category=" + str);
            j56 f = c66.f(this.a);
            ee7.a((Object) str, "category");
            List<MicroApp> a2 = f.a(str);
            ArrayList arrayList = new ArrayList();
            HybridPreset a3 = c66.f(this.a).a().a();
            MicroApp a4 = c66.f(this.a).d().a();
            String id = a4 != null ? a4.getId() : null;
            if (a3 != null) {
                for (MicroApp microApp : a2) {
                    Iterator<T> it = a3.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        T t2 = t;
                        boolean z = true;
                        if (!ee7.a((Object) t2.getAppId(), (Object) microApp.getId()) || !(!ee7.a((Object) t2.getAppId(), (Object) id))) {
                            z = false;
                            continue;
                        }
                        if (z) {
                            break;
                        }
                    }
                    if (t == null || ee7.a((Object) microApp.getId(), (Object) "empty")) {
                        arrayList.add(microApp);
                    }
                }
            }
            this.a.i.a(arrayList);
            return this.a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<v87<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ c66 a;

        @DexIgnore
        public g(c66 c66) {
            this.a = c66;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<String, Boolean, ? extends Parcelable> v87) {
            String str;
            if (v87 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String k = c66.r;
                local.d(k, "onLiveDataChanged setting of " + v87.getFirst() + " isSettingRequired " + v87.getSecond().booleanValue() + " setting " + ((Parcelable) v87.getThird()));
                boolean booleanValue = v87.getSecond().booleanValue();
                MicroApp a2 = c66.f(this.a).d().a();
                if (ee7.a((Object) (a2 != null ? a2.getId() : null), (Object) v87.getFirst())) {
                    String str2 = "";
                    if (booleanValue) {
                        Parcelable parcelable = (Parcelable) v87.getThird();
                        if (parcelable == null) {
                            str2 = pe5.c.a(v87.getFirst());
                            str = str2;
                        } else {
                            try {
                                String first = v87.getFirst();
                                if (ee7.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                                    str = ((CommuteTimeSetting) parcelable).getAddress();
                                } else if (ee7.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                    str = ((SecondTimezoneSetting) parcelable).getTimeZoneName();
                                } else {
                                    if (ee7.a((Object) first, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                                        str = ((Ringtone) parcelable).getRingtoneName();
                                    }
                                    str = str2;
                                }
                            } catch (Exception e) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String k2 = c66.r;
                                local2.d(k2, "exception when parse micro app setting " + e);
                            }
                        }
                        this.a.p.a(true, v87.getFirst(), str2, str);
                        return;
                    }
                    this.a.p.a(false, v87.getFirst(), str2, null);
                    if (a2 != null) {
                        a66 j = this.a.p;
                        String a3 = ig5.a(PortfolioApp.g0.c(), a2.getDescriptionKey(), a2.getDescription());
                        ee7.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                        j.w(a3);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1", f = "MicroAppPresenter.kt", l = {187}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c66 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends Category>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.q.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(c66 c66, fb7 fb7) {
            super(2, fb7);
            this.this$0 = c66;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) obj) {
                if (!c66.f(this.this$0).a(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.f.clear();
            this.this$0.f.addAll(arrayList);
            this.this$0.p.a((List<Category>) this.this$0.f);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<MicroApp> {
        @DexIgnore
        public /* final */ /* synthetic */ c66 a;

        @DexIgnore
        public i(c66 c66) {
            this.a = c66;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(MicroApp microApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = c66.r;
            local.d(k, "onLiveDataChanged selectedMicroApp value=" + microApp);
            this.a.g.a(microApp);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = c66.class.getSimpleName();
        ee7.a((Object) simpleName, "MicroAppPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public c66(a66 a66, CategoryRepository categoryRepository) {
        ee7.b(a66, "mView");
        ee7.b(categoryRepository, "mCategoryRepository");
        this.p = a66;
        this.q = categoryRepository;
        LiveData<String> b2 = ge.b(this.g, new d(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.k = b2;
        this.l = new c(this);
        LiveData<List<MicroApp>> b3 = ge.b(this.k, new f(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.m = b3;
        this.n = new e(this);
        this.o = new g(this);
    }

    @DexIgnore
    public static final /* synthetic */ j56 f(c66 c66) {
        j56 j56 = c66.e;
        if (j56 != null) {
            return j56;
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final ik7 b(String str) {
        return xh7.b(e(), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(r, "onStart");
        this.k.a(this.l);
        this.m.a(this.n);
        this.j.a(this.o);
        if (this.f.isEmpty()) {
            ik7 unused = xh7.b(e(), null, null, new h(this, null), 3, null);
        } else {
            this.p.a((List<Category>) this.f);
        }
        j56 j56 = this.e;
        if (j56 != null) {
            LiveData<MicroApp> d2 = j56.d();
            a66 a66 = this.p;
            if (a66 != null) {
                d2.a((l56) a66, new i(this));
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        j56 j56 = this.e;
        if (j56 != null) {
            LiveData<MicroApp> d2 = j56.d();
            a66 a66 = this.p;
            if (a66 != null) {
                d2.a((l56) a66);
                this.i.b(this.n);
                this.h.b(this.l);
                this.j.b(this.o);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.z56
    public void h() {
        T t;
        String str;
        T t2;
        String str2;
        String appId;
        j56 j56 = this.e;
        T t3 = null;
        if (j56 != null) {
            HybridPreset a2 = j56.a().a();
            String str3 = "empty";
            if (a2 != null) {
                Iterator<T> it = a2.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                T t4 = t;
                if (t4 == null || (str = t4.getAppId()) == null) {
                    str = str3;
                }
                Iterator<T> it2 = a2.getButtons().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    if (ee7.a((Object) t2.getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                T t5 = t2;
                if (t5 == null || (str2 = t5.getAppId()) == null) {
                    str2 = str3;
                }
                Iterator<T> it3 = a2.getButtons().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (ee7.a((Object) next.getPosition(), (Object) "bottom")) {
                        t3 = next;
                        break;
                    }
                }
                T t6 = t3;
                if (!(t6 == null || (appId = t6.getAppId()) == null)) {
                    str3 = appId;
                }
                this.p.a(str, str2, str3);
                return;
            }
            this.p.a(str3, str3, str3);
            return;
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    @Override // com.fossil.z56
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r6 = this;
            com.fossil.j56 r0 = r6.e
            java.lang.String r1 = "mHybridCustomizeViewModel"
            r2 = 0
            if (r0 == 0) goto L_0x0095
            androidx.lifecycle.LiveData r0 = r0.d()
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.room.microapp.MicroApp r0 = (com.portfolio.platform.data.model.room.microapp.MicroApp) r0
            if (r0 == 0) goto L_0x0094
            com.fossil.j56 r3 = r6.e
            if (r3 == 0) goto L_0x0090
            androidx.lifecycle.MutableLiveData r1 = r3.a()
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r1
            if (r1 == 0) goto L_0x0054
            java.util.ArrayList r1 = r1.getButtons()
            if (r1 == 0) goto L_0x0054
            java.util.Iterator r1 = r1.iterator()
        L_0x002d:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x0049
            java.lang.Object r3 = r1.next()
            r4 = r3
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r4 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r4
            java.lang.String r5 = r0.getId()
            java.lang.String r4 = r4.getAppId()
            boolean r4 = com.fossil.ee7.a(r5, r4)
            if (r4 == 0) goto L_0x002d
            r2 = r3
        L_0x0049:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r2 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r2
            if (r2 == 0) goto L_0x0054
            java.lang.String r1 = r2.getSettings()
            if (r1 == 0) goto L_0x0054
            goto L_0x0056
        L_0x0054:
            java.lang.String r1 = ""
        L_0x0056:
            java.lang.String r0 = r0.getId()
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.fossil.ee7.a(r0, r2)
            if (r2 == 0) goto L_0x006c
            com.fossil.a66 r0 = r6.p
            r0.c(r1)
            goto L_0x0094
        L_0x006c:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.fossil.ee7.a(r0, r2)
            if (r2 == 0) goto L_0x007e
            com.fossil.a66 r0 = r6.p
            r0.e(r1)
            goto L_0x0094
        L_0x007e:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE
            java.lang.String r2 = r2.getValue()
            boolean r0 = com.fossil.ee7.a(r0, r2)
            if (r0 == 0) goto L_0x0094
            com.fossil.a66 r0 = r6.p
            r0.I(r1)
            goto L_0x0094
        L_0x0090:
            com.fossil.ee7.d(r1)
            throw r2
        L_0x0094:
            return
        L_0x0095:
            com.fossil.ee7.d(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.c66.i():void");
    }

    @DexIgnore
    public void j() {
        this.p.a(this);
    }

    @DexIgnore
    @Override // com.fossil.z56
    public void a(j56 j56) {
        ee7.b(j56, "viewModel");
        this.e = j56;
    }

    @DexIgnore
    @Override // com.fossil.z56
    public void a(Category category) {
        ee7.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "category change " + category);
        this.h.a(category.getId());
    }

    @DexIgnore
    @Override // com.fossil.z56
    public void a(String str) {
        T t;
        ee7.b(str, "newMicroAppId");
        j56 j56 = this.e;
        if (j56 != null) {
            MicroApp b2 = j56.b(str);
            FLogger.INSTANCE.getLocal().d(r, "onUserChooseMicroApp " + b2);
            if (b2 != null) {
                j56 j562 = this.e;
                if (j562 != null) {
                    HybridPreset a2 = j562.a().a();
                    if (a2 != null) {
                        HybridPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        j56 j563 = this.e;
                        if (j563 != null) {
                            String a3 = j563.e().a();
                            if (a3 != null) {
                                ee7.a((Object) a3, "mHybridCustomizeViewMode\u2026ctedMicroAppPos().value!!");
                                String str2 = a3;
                                j56 j564 = this.e;
                                if (j564 == null) {
                                    ee7.d("mHybridCustomizeViewModel");
                                    throw null;
                                } else if (!j564.d(str)) {
                                    Iterator<HybridPresetAppSetting> it = clone.getButtons().iterator();
                                    while (it.hasNext()) {
                                        HybridPresetAppSetting next = it.next();
                                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                                            arrayList.add(new HybridPresetAppSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getButtons().clear();
                                    clone.getButtons().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(r, "Update current preset=" + clone);
                                    j56 j565 = this.e;
                                    if (j565 != null) {
                                        j565.a(clone);
                                    } else {
                                        ee7.d("mHybridCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = a2.getButtons().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t = null;
                                            break;
                                        }
                                        t = it2.next();
                                        if (ee7.a((Object) t.getAppId(), (Object) str)) {
                                            break;
                                        }
                                    }
                                    T t2 = t;
                                    if (t2 != null) {
                                        j56 j566 = this.e;
                                        if (j566 != null) {
                                            j566.e(t2.getPosition());
                                        } else {
                                            ee7.d("mHybridCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.d("mHybridCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    ee7.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.z56
    public void a(String str, String str2) {
        T t;
        ee7.b(str, "microAppId");
        ee7.b(str2, MicroAppSetting.SETTING);
        j56 j56 = this.e;
        if (j56 != null) {
            HybridPreset a2 = j56.a().a();
            if (a2 != null) {
                HybridPreset clone = a2.clone();
                Iterator<T> it = clone.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getAppId(), (Object) str)) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    t2.setSettings(str2);
                }
                FLogger.INSTANCE.getLocal().d(r, "update new setting " + str2 + " of " + str);
                j56 j562 = this.e;
                if (j562 != null) {
                    j562.a(clone);
                } else {
                    ee7.d("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
