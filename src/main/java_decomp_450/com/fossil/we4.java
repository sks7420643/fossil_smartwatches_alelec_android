package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we4 {
    @DexIgnore
    public static <T> T a(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    public static void a(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }
}
