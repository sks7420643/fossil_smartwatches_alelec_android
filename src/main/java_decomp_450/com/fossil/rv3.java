package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rv3 extends dv3 {
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ RectF C;
    @DexIgnore
    public int D;

    @DexIgnore
    public rv3() {
        this(null);
    }

    @DexIgnore
    public boolean D() {
        return !this.C.isEmpty();
    }

    @DexIgnore
    public void E() {
        a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void F() {
        this.B.setStyle(Paint.Style.FILL_AND_STROKE);
        this.B.setColor(-1);
        this.B.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    @DexIgnore
    public void a(float f, float f2, float f3, float f4) {
        RectF rectF = this.C;
        if (f != rectF.left || f2 != rectF.top || f3 != rectF.right || f4 != rectF.bottom) {
            this.C.set(f, f2, f3, f4);
            invalidateSelf();
        }
    }

    @DexIgnore
    @Override // com.fossil.dv3
    public void draw(Canvas canvas) {
        f(canvas);
        super.draw(canvas);
        canvas.drawRect(this.C, this.B);
        e(canvas);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        if (!a(getCallback())) {
            canvas.restoreToCount(this.D);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        Drawable.Callback callback = getCallback();
        if (a(callback)) {
            View view = (View) callback;
            if (view.getLayerType() != 2) {
                view.setLayerType(2, null);
                return;
            }
            return;
        }
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.D = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), null);
        } else {
            this.D = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), null, 31);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rv3(hv3 hv3) {
        super(hv3 == null ? new hv3() : hv3);
        this.B = new Paint(1);
        F();
        this.C = new RectF();
    }

    @DexIgnore
    public void a(RectF rectF) {
        a(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    @DexIgnore
    public final boolean a(Drawable.Callback callback) {
        return callback instanceof View;
    }
}
