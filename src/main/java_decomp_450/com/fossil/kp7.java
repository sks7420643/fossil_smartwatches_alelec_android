package com.fossil;

import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.fo7;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp7 {
    /*
    static {
        br7.encodeUtf8("\"\\");
        br7.encodeUtf8("\t ,=");
    }
    */

    @DexIgnore
    public static long a(Response response) {
        return a(response.k());
    }

    @DexIgnore
    public static boolean b(fo7 fo7) {
        return c(fo7).contains(vt7.ANY_MARKER);
    }

    @DexIgnore
    public static boolean c(Response response) {
        return b(response.k());
    }

    @DexIgnore
    public static Set<String> d(Response response) {
        return c(response.k());
    }

    @DexIgnore
    public static fo7 e(Response response) {
        return a(response.o().x().c(), response.k());
    }

    @DexIgnore
    public static long a(fo7 fo7) {
        return a(fo7.a(HttpHeaders.CONTENT_LENGTH));
    }

    @DexIgnore
    public static boolean b(Response response) {
        if (response.x().e().equals("HEAD")) {
            return false;
        }
        int e = response.e();
        if (((e >= 100 && e < 200) || e == 204 || e == 304) && a(response) == -1 && !"chunked".equalsIgnoreCase(response.b("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static Set<String> c(fo7 fo7) {
        Set<String> emptySet = Collections.emptySet();
        int b = fo7.b();
        for (int i = 0; i < b; i++) {
            if ("Vary".equalsIgnoreCase(fo7.a(i))) {
                String b2 = fo7.b(i);
                if (emptySet.isEmpty()) {
                    emptySet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String str : b2.split(",")) {
                    emptySet.add(str.trim());
                }
            }
        }
        return emptySet;
    }

    @DexIgnore
    public static long a(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static boolean a(Response response, fo7 fo7, lo7 lo7) {
        for (String str : d(response)) {
            if (!ro7.a(fo7.b(str), lo7.b(str))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static fo7 a(fo7 fo7, fo7 fo72) {
        Set<String> c = c(fo72);
        if (c.isEmpty()) {
            return new fo7.a().a();
        }
        fo7.a aVar = new fo7.a();
        int b = fo7.b();
        for (int i = 0; i < b; i++) {
            String a = fo7.a(i);
            if (c.contains(a)) {
                aVar.a(a, fo7.b(i));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static int b(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == '\t')) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static void a(yn7 yn7, go7 go7, fo7 fo7) {
        if (yn7 != yn7.a) {
            List<xn7> a = xn7.a(go7, fo7);
            if (!a.isEmpty()) {
                yn7.a(go7, a);
            }
        }
    }

    @DexIgnore
    public static int a(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static int a(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
