package com.fossil;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.SecondTimezoneRaw;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rd5 {
    @DexIgnore
    public static rd5 e;
    @DexIgnore
    public static /* final */ ArrayList<Ringtone> f; // = new ArrayList<>();
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public ch5 a;
    @DexIgnore
    public DeviceRepository b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public aw6 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rd5$a$a")
        /* renamed from: com.fossil.rd5$a$a  reason: collision with other inner class name */
        public static final class C0162a extends TypeToken<List<? extends SecondTimezoneRaw>> {
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(rd5 rd5) {
            rd5.e = rd5;
        }

        @DexIgnore
        public final synchronized rd5 b() {
            rd5 e;
            if (rd5.g.e() == null) {
                rd5.g.a(new rd5(null));
            }
            e = rd5.g.e();
            if (e == null) {
                ee7.a();
                throw null;
            }
            return e;
        }

        @DexIgnore
        public final ArrayList<Ringtone> c() {
            return rd5.f;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0058, code lost:
            if (r0 != null) goto L_0x005a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x005a, code lost:
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x007b, code lost:
            if (0 == 0) goto L_0x007e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0082, code lost:
            return c();
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.util.List<com.portfolio.platform.data.model.Ringtone> d() {
            /*
                r8 = this;
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = "AppHelper"
                java.lang.String r2 = "getListRingtoneFromRingtonePhone"
                r0.d(r1, r2)
                java.util.ArrayList r0 = r8.c()
                boolean r0 = r0.isEmpty()
                r2 = 1
                r0 = r0 ^ r2
                if (r0 == 0) goto L_0x001e
                java.util.ArrayList r0 = r8.c()
                return r0
            L_0x001e:
                r0 = 0
                android.media.RingtoneManager r3 = new android.media.RingtoneManager     // Catch:{ Exception -> 0x0060 }
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0060 }
                com.portfolio.platform.PortfolioApp r4 = r4.c()     // Catch:{ Exception -> 0x0060 }
                android.content.Context r4 = r4.getApplicationContext()     // Catch:{ Exception -> 0x0060 }
                r3.<init>(r4)     // Catch:{ Exception -> 0x0060 }
                r3.setType(r2)     // Catch:{ Exception -> 0x0060 }
                android.database.Cursor r0 = r3.getCursor()     // Catch:{ Exception -> 0x0060 }
                if (r0 == 0) goto L_0x0058
            L_0x0037:
                boolean r3 = r0.moveToNext()     // Catch:{ Exception -> 0x0060 }
                if (r3 == 0) goto L_0x0058
                java.lang.String r3 = r0.getString(r2)     // Catch:{ Exception -> 0x0060 }
                r4 = 0
                java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x0060 }
                java.util.ArrayList r5 = r8.c()     // Catch:{ Exception -> 0x0060 }
                com.portfolio.platform.data.model.Ringtone r6 = new com.portfolio.platform.data.model.Ringtone     // Catch:{ Exception -> 0x0060 }
                java.lang.String r7 = "title"
                com.fossil.ee7.a(r3, r7)     // Catch:{ Exception -> 0x0060 }
                r6.<init>(r3, r4)     // Catch:{ Exception -> 0x0060 }
                r5.add(r6)     // Catch:{ Exception -> 0x0060 }
                goto L_0x0037
            L_0x0058:
                if (r0 == 0) goto L_0x007e
            L_0x005a:
                r0.close()
                goto L_0x007e
            L_0x005e:
                r1 = move-exception
                goto L_0x0083
            L_0x0060:
                r2 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x005e }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ all -> 0x005e }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x005e }
                r4.<init>()     // Catch:{ all -> 0x005e }
                java.lang.String r5 = "getListRingtoneFromRingtonePhone - Exception="
                r4.append(r5)     // Catch:{ all -> 0x005e }
                r4.append(r2)     // Catch:{ all -> 0x005e }
                java.lang.String r2 = r4.toString()     // Catch:{ all -> 0x005e }
                r3.e(r1, r2)     // Catch:{ all -> 0x005e }
                if (r0 == 0) goto L_0x007e
                goto L_0x005a
            L_0x007e:
                java.util.ArrayList r0 = r8.c()
                return r0
            L_0x0083:
                if (r0 == 0) goto L_0x0088
                r0.close()
            L_0x0088:
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.rd5.a.d():java.util.List");
        }

        @DexIgnore
        public final rd5 e() {
            return rd5.e;
        }

        @DexIgnore
        public final ArrayList<b> f() {
            return a();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:30:0x009b, code lost:
            if (r3 != null) goto L_0x0078;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0098  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00a2  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00a7  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.util.ArrayList<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> g() {
            /*
                r10 = this;
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
                r1 = 0
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
                com.portfolio.platform.PortfolioApp r2 = r2.c()     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
                android.content.res.Resources r2 = r2.getResources()     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
                r3 = 2131820551(0x7f110007, float:1.927382E38)
                java.io.InputStream r2 = r2.openRawResource(r3)     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
                java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0082, all -> 0x007e }
                java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0082, all -> 0x007e }
                r4.<init>(r2)     // Catch:{ Exception -> 0x0082, all -> 0x007e }
                r3.<init>(r4)     // Catch:{ Exception -> 0x0082, all -> 0x007e }
                com.google.gson.stream.JsonReader r1 = new com.google.gson.stream.JsonReader     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r1.<init>(r3)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                com.google.gson.Gson r4 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r4.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                com.fossil.rd5$a$a r5 = new com.fossil.rd5$a$a     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r5.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.lang.reflect.Type r5 = r5.getType()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.lang.Object r1 = r4.a(r1, r5)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.util.List r1 = (java.util.List) r1     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.lang.String r4 = "rawSecondTimezoneList"
                com.fossil.ee7.a(r1, r4)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r5 = 10
                int r5 = com.fossil.x97.a(r1, r5)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r4.<init>(r5)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.util.Iterator r1 = r1.iterator()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            L_0x004e:
                boolean r5 = r1.hasNext()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                if (r5 == 0) goto L_0x0070
                java.lang.Object r5 = r1.next()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                com.portfolio.platform.data.model.SecondTimezoneRaw r5 = (com.portfolio.platform.data.model.SecondTimezoneRaw) r5     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                com.portfolio.platform.data.model.setting.SecondTimezoneSetting r6 = new com.portfolio.platform.data.model.setting.SecondTimezoneSetting     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.lang.String r7 = r5.getCityName()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                java.lang.String r8 = r5.getTimeZoneId()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r9 = 0
                java.lang.String r5 = r5.getCityCode()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r6.<init>(r7, r8, r9, r5)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                r4.add(r6)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                goto L_0x004e
            L_0x0070:
                r0.addAll(r4)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
                if (r2 == 0) goto L_0x0078
                r2.close()
            L_0x0078:
                r3.close()
                goto L_0x009e
            L_0x007c:
                r0 = move-exception
                goto L_0x0080
            L_0x007e:
                r0 = move-exception
                r3 = r1
            L_0x0080:
                r1 = r2
                goto L_0x00a0
            L_0x0082:
                r3 = r1
            L_0x0083:
                r1 = r2
                goto L_0x0089
            L_0x0085:
                r0 = move-exception
                r3 = r1
                goto L_0x00a0
            L_0x0088:
                r3 = r1
            L_0x0089:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x009f }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x009f }
                java.lang.String r4 = "AppHelper"
                java.lang.String r5 = "exception when close stream."
                r2.d(r4, r5)     // Catch:{ all -> 0x009f }
                if (r1 == 0) goto L_0x009b
                r1.close()
            L_0x009b:
                if (r3 == 0) goto L_0x009e
                goto L_0x0078
            L_0x009e:
                return r0
            L_0x009f:
                r0 = move-exception
            L_0x00a0:
                if (r1 == 0) goto L_0x00a5
                r1.close()
            L_0x00a5:
                if (r3 == 0) goto L_0x00aa
                r3.close()
            L_0x00aa:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.rd5.a.g():java.util.ArrayList");
        }

        @DexIgnore
        public final String h() {
            try {
                String sb = new StringBuilder("4.5.0").delete(nh7.a((CharSequence) "4.5.0", '-', 0, false, 6, (Object) null), 5).toString();
                ee7.a((Object) sb, "stringBuilder.delete(pos\u2026N_NAME.length).toString()");
                return sb;
            } catch (Exception unused) {
                return "4.5.0";
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final ArrayList<b> a() {
            PortfolioApp c = PortfolioApp.g0.c();
            ArrayList<b> arrayList = new ArrayList<>();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = c.getPackageManager().queryIntentActivities(intent, 0);
            PackageManager packageManager = c.getPackageManager();
            for (ResolveInfo resolveInfo : queryIntentActivities) {
                if (!mh7.b(resolveInfo.loadLabel(packageManager).toString(), "Contacts", true) && !mh7.b(resolveInfo.loadLabel(packageManager).toString(), "Phone", true)) {
                    b bVar = new b(resolveInfo.loadLabel(c.getPackageManager()).toString());
                    String str = resolveInfo.activityInfo.packageName;
                    ee7.a((Object) str, "item.activityInfo.packageName");
                    bVar.a(str);
                    try {
                        ApplicationInfo applicationInfo = packageManager.getApplicationInfo(bVar.b(), 0);
                        ee7.a((Object) applicationInfo, "packageManager.getApplic\u2026ionInfo(newInfo.pname, 0)");
                        if (applicationInfo.icon != 0) {
                            bVar.a(Uri.parse("android.resource://" + bVar.b() + "/" + applicationInfo.icon));
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    Iterator<b> it = arrayList.iterator();
                    boolean z = false;
                    while (it.hasNext()) {
                        if (ee7.a((Object) it.next().b(), (Object) bVar.b())) {
                            z = true;
                        }
                    }
                    if (!z) {
                        arrayList.add(bVar);
                    }
                }
            }
            return arrayList;
        }

        @DexIgnore
        public final void b(Context context, String str) {
            ee7.b(context, "context");
            ee7.b(str, "packageName");
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + str));
            try {
                intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("AppHelper", "openMarketApp - ex=" + e);
            }
        }

        @DexIgnore
        @SuppressLint({"HardwareIds"})
        public final String a(String str) {
            ee7.b(str, ButtonService.USER_ID);
            if (TextUtils.isEmpty(str)) {
                String str2 = Build.SERIAL;
                ee7.a((Object) str2, "Build.SERIAL");
                return str2;
            }
            return Build.SERIAL + ":" + str;
        }

        @DexIgnore
        public final boolean a(Context context, String str) {
            ee7.b(context, "context");
            ee7.b(str, "packageName");
            try {
                context.getPackageManager().getPackageInfo(str, 0);
                return true;
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a; // = "";
        @DexIgnore
        public Uri b;
        @DexIgnore
        public String c;

        @DexIgnore
        public b(String str) {
            ee7.b(str, "appname");
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final Uri c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && ee7.a(this.c, ((b) obj).c);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.c;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PInfo(appname=" + this.c + ")";
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void a(Uri uri) {
            this.b = uri;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.AppHelper$getAppLogInfo$2", f = "AppHelper.kt", l = {67}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super AppLogInfo>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ rd5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(rd5 rd5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = rd5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super AppLogInfo> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            String str;
            String str2;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                UserRepository c = this.this$0.c();
                this.L$0 = yi7;
                this.label = 1;
                obj = c.getCurrentUser(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser == null || (str = mFUser.getUserId()) == null) {
                str = "";
            }
            if (ee7.a((Object) "release", (Object) "release")) {
                str2 = "4.5.0";
            } else {
                str2 = rd5.g.h();
            }
            String str3 = ke5.c.a(PortfolioApp.g0.c()) + ':' + Build.MODEL;
            String str4 = Build.MODEL;
            String str5 = Build.VERSION.RELEASE;
            ee7.a((Object) str5, "Build.VERSION.RELEASE");
            String sDKVersion = ButtonService.Companion.getSDKVersion();
            ee7.a((Object) str4, "phoneModel");
            return new AppLogInfo(str, str2, "android", str5, str3, sDKVersion, str4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.AppHelper", f = "AppHelper.kt", l = {95, 99}, m = "getBuildInfo")
    public static final class d extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rd5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(rd5 rd5, fb7 fb7) {
            super(fb7);
            this.this$0 = rd5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, 0, 0, this);
        }
    }

    @DexIgnore
    public rd5() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final String b() {
        if (ee7.a((Object) "release", (Object) "release")) {
            return "4.5.0";
        }
        return g.h();
    }

    @DexIgnore
    public final UserRepository c() {
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final Object a(fb7<? super AppLogInfo> fb7) {
        return vh7.a(qj7.b(), new c(this, null), fb7);
    }

    @DexIgnore
    public /* synthetic */ rd5(zd7 zd7) {
        this();
    }

    @DexIgnore
    public final ActiveDeviceInfo a() {
        String str;
        String firmwareRevision;
        String c2 = PortfolioApp.g0.c().c();
        DeviceRepository deviceRepository = this.b;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(c2);
            String str2 = "";
            if (deviceBySerial == null || (str = deviceBySerial.getSku()) == null) {
                str = str2;
            }
            if (!(deviceBySerial == null || (firmwareRevision = deviceBySerial.getFirmwareRevision()) == null)) {
                str2 = firmwareRevision;
            }
            return new ActiveDeviceInfo(c2, str, str2);
        }
        ee7.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:107:0x07ff  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0222 A[LOOP:0: B:56:0x021c->B:58:0x0222, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x02b4  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0311  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0509  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0679  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(android.content.Context r29, int r30, int r31, com.fossil.fb7<? super java.lang.String> r32) {
        /*
            r28 = this;
            r0 = r28
            r1 = r32
            boolean r2 = r1 instanceof com.fossil.rd5.d
            if (r2 == 0) goto L_0x0018
            r2 = r1
            com.fossil.rd5$d r2 = (com.fossil.rd5.d) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r4 & r3
            if (r4 == 0) goto L_0x0018
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            int r3 = r3 - r1
            r2.label = r3
            goto L_0x001d
        L_0x0018:
            com.fossil.rd5$d r2 = new com.fossil.rd5$d
            r2.<init>(r0, r1)
        L_0x001d:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            java.lang.String r5 = "mDeviceRepository"
            java.lang.String r7 = ""
            r8 = 2
            r9 = 1
            if (r4 == 0) goto L_0x006a
            if (r4 == r9) goto L_0x005a
            if (r4 != r8) goto L_0x0052
            java.lang.Object r3 = r2.L$5
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r4 = r2.L$4
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r10 = r2.L$3
            com.portfolio.platform.data.model.Device r10 = (com.portfolio.platform.data.model.Device) r10
            java.lang.Object r11 = r2.L$2
            com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
            int r12 = r2.I$1
            int r13 = r2.I$0
            java.lang.Object r14 = r2.L$1
            android.content.Context r14 = (android.content.Context) r14
            java.lang.Object r2 = r2.L$0
            com.fossil.rd5 r2 = (com.fossil.rd5) r2
            com.fossil.t87.a(r1)
            goto L_0x00e3
        L_0x0052:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x005a:
            int r4 = r2.I$1
            int r10 = r2.I$0
            java.lang.Object r11 = r2.L$1
            android.content.Context r11 = (android.content.Context) r11
            java.lang.Object r12 = r2.L$0
            com.fossil.rd5 r12 = (com.fossil.rd5) r12
            com.fossil.t87.a(r1)
            goto L_0x008e
        L_0x006a:
            com.fossil.t87.a(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r0.c
            if (r1 == 0) goto L_0x0804
            r2.L$0 = r0
            r4 = r29
            r2.L$1 = r4
            r10 = r30
            r2.I$0 = r10
            r11 = r31
            r2.I$1 = r11
            r2.label = r9
            java.lang.Object r1 = r1.getCurrentUser(r2)
            if (r1 != r3) goto L_0x0088
            return r3
        L_0x0088:
            r12 = r0
            r26 = r11
            r11 = r4
            r4 = r26
        L_0x008e:
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            com.portfolio.platform.data.source.DeviceRepository r13 = r12.b
            if (r13 == 0) goto L_0x07ff
            com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r14 = r14.c()
            java.lang.String r14 = r14.c()
            com.portfolio.platform.data.model.Device r13 = r13.getDeviceBySerial(r14)
            com.portfolio.platform.data.source.DeviceRepository r14 = r12.b
            if (r14 == 0) goto L_0x07fa
            java.util.List r14 = r14.getAllDevice()
            com.fossil.aw6 r15 = r12.d
            if (r15 == 0) goto L_0x07f3
            com.fossil.aw6$b r6 = new com.fossil.aw6$b
            com.fossil.ya5 r9 = new com.fossil.ya5
            r9.<init>()
            java.lang.String r8 = "Passphrase"
            r6.<init>(r8, r9)
            r2.L$0 = r12
            r2.L$1 = r11
            r2.I$0 = r10
            r2.I$1 = r4
            r2.L$2 = r1
            r2.L$3 = r13
            r2.L$4 = r14
            r2.L$5 = r7
            r8 = 2
            r2.label = r8
            java.lang.Object r2 = com.fossil.gl4.a(r15, r6, r2)
            if (r2 != r3) goto L_0x00d4
            return r3
        L_0x00d4:
            r3 = r7
            r26 = r11
            r11 = r1
            r1 = r2
            r2 = r12
            r12 = r4
            r4 = r14
            r14 = r26
            r27 = r13
            r13 = r10
            r10 = r27
        L_0x00e3:
            com.fossil.fl4$c r1 = (com.fossil.fl4.c) r1
            boolean r6 = r1 instanceof com.fossil.aw6.d
            if (r6 == 0) goto L_0x00ef
            com.fossil.aw6$d r1 = (com.fossil.aw6.d) r1
            java.lang.String r3 = r1.a()
        L_0x00ef:
            java.util.TimeZone r1 = java.util.TimeZone.getDefault()
            java.lang.String r6 = "java.lang.String.format(format, *args)"
            java.lang.String r9 = " %s (%s)"
            java.lang.String r15 = "Model: "
            java.lang.String r8 = "\n"
            if (r10 == 0) goto L_0x01e4
            java.lang.String r16 = r10.getSku()
            java.lang.String r17 = r10.getFirmwareRevision()
            int r18 = r10.getBatteryLevel()
            com.portfolio.platform.data.source.DeviceRepository r0 = r2.b
            if (r0 == 0) goto L_0x01df
            java.lang.String r5 = r10.getDeviceId()
            com.portfolio.platform.data.model.Device r0 = r0.getDeviceBySerial(r5)
            if (r0 == 0) goto L_0x0132
            boolean r5 = android.text.TextUtils.isEmpty(r16)
            if (r5 == 0) goto L_0x0121
            java.lang.String r16 = r0.getSku()
        L_0x0121:
            boolean r5 = android.text.TextUtils.isEmpty(r17)
            if (r5 == 0) goto L_0x012b
            java.lang.String r17 = r0.getFirmwareRevision()
        L_0x012b:
            if (r18 <= 0) goto L_0x012e
            goto L_0x0132
        L_0x012e:
            int r18 = r0.getBatteryLevel()
        L_0x0132:
            r31 = r1
            r0 = r16
            r5 = r17
            r1 = r18
            com.fossil.ch5 r2 = r2.a
            if (r2 == 0) goto L_0x01d8
            r16 = r14
            java.lang.String r14 = r10.getDeviceId()
            r17 = r13
            long r13 = r2.e(r14)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r18 = r7
            java.lang.String r7 = "Serial: "
            r2.append(r7)
            java.lang.String r7 = r10.getDeviceId()
            r2.append(r7)
            r2.append(r8)
            r2.append(r15)
            r2.append(r0)
            r2.append(r8)
            java.lang.String r0 = "FW: "
            r2.append(r0)
            r2.append(r5)
            r2.append(r8)
            java.lang.String r0 = "SDKVersion: "
            r2.append(r0)
            java.lang.String r0 = "5.12.6-production-release"
            r2.append(r0)
            r2.append(r8)
            java.lang.String r0 = "Latest Firmware: "
            r2.append(r0)
            java.lang.String r0 = r10.getFirmwareRevision()
            r2.append(r0)
            r2.append(r8)
            java.lang.String r0 = "BatteryLevel: "
            r2.append(r0)
            r2.append(r1)
            r2.append(r8)
            java.lang.String r0 = "Last successful sync: "
            r2.append(r0)
            com.fossil.we7 r0 = com.fossil.we7.a
            r1 = 2
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r32 = r2
            r5 = 1000(0x3e8, float:1.401E-42)
            long r1 = (long) r5
            long r1 = r13 / r1
            java.lang.Long r1 = com.fossil.pb7.a(r1)
            r2 = 0
            r0[r2] = r1
            java.util.Date r1 = new java.util.Date
            r1.<init>(r13)
            java.lang.String r1 = com.fossil.zd5.e(r1)
            r2 = 1
            r0[r2] = r1
            r1 = 2
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r1)
            java.lang.String r0 = java.lang.String.format(r9, r0)
            com.fossil.ee7.a(r0, r6)
            r1 = r32
            r1.append(r0)
            r1.append(r8)
            java.lang.String r0 = r1.toString()
            goto L_0x01ee
        L_0x01d8:
            java.lang.String r0 = "mSharedPreferencesManager"
            com.fossil.ee7.d(r0)
            r0 = 0
            throw r0
        L_0x01df:
            r0 = 0
            com.fossil.ee7.d(r5)
            throw r0
        L_0x01e4:
            r31 = r1
            r18 = r7
            r17 = r13
            r16 = r14
            r0 = r18
        L_0x01ee:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = "Passphrase: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " \n"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = "List of paired device:"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            java.util.Iterator r1 = r4.iterator()
        L_0x021c:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0241
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.Device r2 = (com.portfolio.platform.data.model.Device) r2
            java.lang.String r2 = r2.component1()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            r0 = 32
            r3.append(r0)
            r3.append(r2)
            java.lang.String r0 = r3.toString()
            goto L_0x021c
        L_0x0241:
            if (r11 == 0) goto L_0x024c
            java.lang.String r1 = r11.getEmail()
            java.lang.String r2 = r11.getUserId()
            goto L_0x024f
        L_0x024c:
            r1 = r18
            r2 = r1
        L_0x024f:
            java.lang.String r3 = "Tags: "
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            java.lang.String r4 = r4.h()
            com.fossil.eb5 r5 = com.fossil.eb5.PORTFOLIO
            java.lang.String r5 = r5.getName()
            boolean r4 = com.fossil.ee7.a(r4, r5)
            if (r4 == 0) goto L_0x0278
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            java.lang.String r3 = "Android"
            r4.append(r3)
            java.lang.String r3 = r4.toString()
        L_0x0278:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Brand: "
            r4.append(r5)
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            java.lang.String r5 = r5.h()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.String r7 = "System version: "
            java.lang.String r10 = "Email: "
            java.lang.String r11 = "NULL"
            java.lang.String r13 = "26467-2020-08-13"
            java.lang.String r14 = "Build number: "
            java.lang.String r5 = "\n\n\n__________\nAppName: "
            r19 = r11
            java.lang.String r11 = "Calendar.getInstance()"
            r20 = r0
            java.lang.String r0 = "offset "
            r21 = r4
            java.lang.String r4 = ")"
            r22 = r15
            java.lang.String r15 = " ("
            r23 = r6
            r6 = -1
            if (r12 != r6) goto L_0x0311
            if (r17 != 0) goto L_0x02ef
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r3)
            java.lang.String r3 = "uat"
            r6.append(r3)
            java.lang.String r3 = r6.toString()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r12 = r18
            r6.append(r12)
            r32 = r3
            java.lang.String r3 = "\nPlease give a detailed Description of the issue you are experiencing.\n\n\nHow often is the issue happening?\n\n\nIf you are able, provide the steps you took that caused the bug. (example: opened app > synced > tapped new preset > exited app > watch did not adapt to new functions assigned)\n\nPlease attach any additional pictures, screenshots, or video that may help describe the issue.\n\n\n"
            r6.append(r3)
            java.lang.String r3 = r6.toString()
            r6 = r12
            r24 = r21
            r12 = r31
            r31 = r13
            r13 = r22
            r26 = r3
            r3 = r32
            r32 = r14
            r14 = r26
            goto L_0x0503
        L_0x02ef:
            r12 = r18
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r12)
            java.lang.String r12 = "\n1. What did you do?\n\n2. What did you actually get?\n\n3. What did you expect?\n\n\n"
            r6.append(r12)
            java.lang.String r6 = r6.toString()
            r12 = r31
            r31 = r13
            r32 = r14
            r24 = r21
            r13 = r22
            r14 = r6
            r6 = r18
            goto L_0x0503
        L_0x0311:
            r6 = r18
            if (r12 == 0) goto L_0x04e3
            r17 = r3
            r3 = 1
            if (r12 == r3) goto L_0x04c5
            r3 = 2
            if (r12 == r3) goto L_0x032c
            r12 = r31
            r31 = r13
            r32 = r14
            r3 = r17
            r24 = r21
            r13 = r22
            r14 = r6
            goto L_0x0503
        L_0x032c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r6)
            r3.append(r5)
            android.content.res.Resources r5 = r16.getResources()
            r6 = 2131887234(0x7f120482, float:1.940907E38)
            java.lang.String r5 = r5.getString(r6)
            r3.append(r5)
            r3.append(r8)
            java.lang.String r5 = "AppVersion: "
            r3.append(r5)
            java.lang.String r5 = "4.5.0"
            r3.append(r5)
            r3.append(r8)
            r3.append(r14)
            r3.append(r13)
            r3.append(r8)
            java.lang.String r5 = "UserID: "
            r3.append(r5)
            boolean r5 = android.text.TextUtils.isEmpty(r2)
            if (r5 == 0) goto L_0x036b
            r2 = r19
        L_0x036b:
            r3.append(r2)
            r3.append(r8)
            r3.append(r10)
            r3.append(r1)
            r3.append(r8)
            java.lang.String r1 = "Phone Info: "
            r3.append(r1)
            java.lang.String r1 = android.os.Build.DEVICE
            r3.append(r1)
            java.lang.String r1 = " - "
            r3.append(r1)
            java.lang.String r1 = android.os.Build.MODEL
            r3.append(r1)
            r3.append(r8)
            r3.append(r7)
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            r3.append(r1)
            r3.append(r8)
            java.lang.String r1 = "Local timezone: ("
            r3.append(r1)
            java.lang.String r1 = "timeZone"
            r12 = r31
            com.fossil.ee7.a(r12, r1)
            java.lang.String r1 = r12.getID()
            r3.append(r1)
            r3.append(r15)
            r1 = 0
            java.lang.String r2 = r12.getDisplayName(r1, r1)
            r3.append(r2)
            r3.append(r4)
            r3.append(r0)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r1, r11)
            long r1 = r1.getTimeInMillis()
            int r1 = r12.getOffset(r1)
            r3.append(r1)
            r3.append(r8)
            java.lang.String r1 = "System timezone: ("
            r3.append(r1)
            java.lang.String r1 = r12.getID()
            r3.append(r1)
            r3.append(r15)
            r1 = 0
            java.lang.String r2 = r12.getDisplayName(r1, r1)
            r3.append(r2)
            r3.append(r4)
            r3.append(r0)
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r0, r11)
            long r0 = r0.getTimeInMillis()
            int r0 = r12.getOffset(r0)
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "Date time: "
            r3.append(r0)
            com.fossil.we7 r0 = com.fossil.we7.a
            r1 = 2
            java.lang.Object[] r0 = new java.lang.Object[r1]
            long r4 = java.lang.System.currentTimeMillis()
            r2 = 1000(0x3e8, float:1.401E-42)
            long r6 = (long) r2
            long r4 = r4 / r6
            java.lang.Long r2 = com.fossil.pb7.a(r4)
            r4 = 0
            r0[r4] = r2
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            java.lang.String r2 = com.fossil.zd5.e(r2)
            r4 = 1
            r0[r4] = r2
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r1)
            java.lang.String r0 = java.lang.String.format(r9, r0)
            r1 = r23
            com.fossil.ee7.a(r0, r1)
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "SDK Version V1: "
            r3.append(r0)
            com.misfit.frameworks.buttonservice.ButtonService$Companion r0 = com.misfit.frameworks.buttonservice.ButtonService.Companion
            java.lang.String r0 = r0.getSdkVersionV2()
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "SDK Version V2 (Diana): "
            r3.append(r0)
            com.misfit.frameworks.buttonservice.ButtonService$Companion r0 = com.misfit.frameworks.buttonservice.ButtonService.Companion
            java.lang.String r0 = r0.getSdkVersionV2()
            r3.append(r0)
            r3.append(r8)
            r0 = r22
            r3.append(r0)
            java.lang.String r0 = android.os.Build.MODEL
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "App code: android_"
            r3.append(r0)
            com.fossil.ol4$a r0 = com.fossil.ol4.A
            java.lang.String r0 = r0.a()
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "Language code: "
            r3.append(r0)
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.String r1 = "Locale.getDefault()"
            com.fossil.ee7.a(r0, r1)
            java.lang.String r0 = r0.getLanguage()
            r3.append(r0)
            java.lang.String r0 = "_t"
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "Tags: "
            r3.append(r0)
            r0 = r17
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "Brand: "
            r3.append(r0)
            r0 = r21
            r3.append(r0)
            r3.append(r8)
            java.lang.String r0 = "\n__________\n"
            r3.append(r0)
            r0 = r20
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            return r0
        L_0x04c5:
            r12 = r31
            r31 = r13
            r32 = r14
            r24 = r21
            r13 = r22
            r3 = r23
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r6)
            java.lang.String r3 = "Hardware/FW\nDid the device have any form of feedback such as hands-movement, vibe, led-flashing when you press/tap on it?\n*Note: if there's none, pls replace the battery. If the issue persist, report it as FW/HW issue.\n\nWhat's the connection status in phone's Bluetooth settings?\nDisconnected \u2192 Was you able to find the device with another phone, using either nRF or ShineSample?\nNo \u2192 move away from everyone and try sync again.\nIt works \u2192 it's probably someone else was connected to your watch.\nNo \u2192 feedback as HW/FW issue.\nYes \u2192 go on.\nConnected \u2192 Go on.\nSW and OS\nPlease provide the followings info:\nWhen was the last successful sync (?mins/hours/days) and what was the battery level?\n\nHow many apps which use bluetooth in background installed on your phone?\n\n*Note:\nShineSample, nRF and TestShine are NOT counted as these apps don't automatically do any Bluetooth thing in background.\nMisfit, Whitelabel, Fossil Q and portfolio apps each have a background service, which continuously manage bluetooth connection.\nFor QA, it is recommended to have only one of these on your phone during the test.\nHow many bluetooth devices are currently connected to your phone? Please provide a screenshot of Settings > Bluetooth.\n*Note:\nThere're unconfirmed reports that things would go wrong if people have multiple Bluetooth-enabled device connected to the phone at the same time.\nWas ShineSample, nRF or TestShine on your phone able to connect?\n\nWas you able to connect with another device using ShineSample or nRF?\n\nLet's try the following tricks. If the connection recover in any of the following steps, you may stop there and report the result.\nDid killing the app then relaunch help?\n\nDid turn off/on Bluetooth help?\n\nDid turn on/off Airplane mode help?\n\nDid remove/forget device in Settings > Bluetooth then kill app and relaunch help?\n\nDid reboot phone help?\n\nDid clearBluetoothCache help?\n\nHardware\nTurn off Bluetooth on your phone \u2192 Try with another phone and see if it works.\n\n\n"
            r14.append(r3)
            java.lang.String r3 = r14.toString()
            goto L_0x0500
        L_0x04e3:
            r12 = r31
            r17 = r3
            r31 = r13
            r32 = r14
            r24 = r21
            r13 = r22
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r6)
            java.lang.String r14 = "Hardware/FW\nDid the device have any form of feedback such as hands-movement, vibe, led-flashing when you press/tap on it?\n*Note: if there's none, pls replace the battery. If the issue persist, report it as FW/HW issue.\nWhat's the connection status in phone's Bluetooth settings?\n\nDisconnected \u2192 Was you able to find the device with another phone, using either nRF or ShineSample?\n\nNo \u2192 move away from everyone and try sync again.\nIt works \u2192 it's probably someone else was connected to your watch.\nNo \u2192 feedback as HW/FW issue.\nYes \u2192 go on.\nConnected \u2192 Go on.\nSW and OS\nPlease provide the followings info:\nWhen was the last successful sync (? mins/hours/days ago) and what was the battery level?\n\nHow many apps which use bluetooth in background installed on your phone?\n\n*Note:\nShineSample, nRF and TestShine are NOT counted as these apps don't automatically do any Bluetooth thing in background.\nMisfit, Whitelabel, Fossil Q and portfolio apps each have a background service, which continuously manage bluetooth connection.\nFor QA, it is recommended to have only one of these on your phone during the test.\nHow many bluetooth devices are currently connected to your phone? Please provide a screenshot of Settings > Bluetooth.\n*Note:\nThere're unconfirmed reports that things would go wrong if people have multiple Bluetooth-enabled device connected to the phone at the same time.\nWas ShineSample, nRF or TestShine on your phone able to connect?\nWas you able to connect with another device using ShineSample or nRF?\nLet's try the following tricks. If the connection recover in any of the following steps, you may stop there and report the result.\nDid killing the app then relaunch help?\n\nDid turn off/on Bluetooth help?\n\nDid turn on/off Airplane mode help?\n\nDid remove/forget device in Settings > Bluetooth then kill app and relaunch help?\n\nDid reboot phone help?\n\nDid clearBluetoothCache help?\n\nHardware\nTurn off Bluetooth on your phone \u2192 Try with another phone and see if it works.\"\nHID connection issue: https://misfit.jira.com/wiki/display/SDS/Troubleshoot%3A+HID+Connection\n\n\n"
            r3.append(r14)
            java.lang.String r3 = r3.toString()
        L_0x0500:
            r14 = r3
            r3 = r17
        L_0x0503:
            boolean r17 = android.text.TextUtils.isEmpty(r1)
            if (r17 == 0) goto L_0x0679
            r18 = r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r14)
            r6.append(r5)
            android.content.res.Resources r5 = r16.getResources()
            r14 = 2131887234(0x7f120482, float:1.940907E38)
            java.lang.String r5 = r5.getString(r14)
            r6.append(r5)
            java.lang.String r5 = " "
            r6.append(r5)
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            boolean r5 = r5.e()
            if (r5 == 0) goto L_0x0534
            r5 = r18
            goto L_0x0536
        L_0x0534:
            java.lang.String r5 = "Production"
        L_0x0536:
            r6.append(r5)
            r6.append(r8)
            r6.append(r10)
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "UserID:"
            r6.append(r1)
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 == 0) goto L_0x0552
            r2 = r19
        L_0x0552:
            r6.append(r2)
            r6.append(r8)
            java.lang.String r1 = "Device name: "
            r6.append(r1)
            java.lang.String r1 = android.os.Build.DEVICE
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "System name: "
            r6.append(r1)
            java.lang.String r1 = "Android OS"
            r6.append(r1)
            r6.append(r8)
            r6.append(r7)
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            r6.append(r1)
            r6.append(r8)
            r6.append(r13)
            java.lang.String r1 = android.os.Build.MODEL
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "App code: android_"
            r6.append(r1)
            com.fossil.ol4$a r1 = com.fossil.ol4.A
            java.lang.String r1 = r1.a()
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "Language code: "
            r6.append(r1)
            java.util.Locale r1 = java.util.Locale.getDefault()
            java.lang.String r2 = "Locale.getDefault()"
            com.fossil.ee7.a(r1, r2)
            java.lang.String r1 = r1.getLanguage()
            r6.append(r1)
            java.lang.String r1 = "_t"
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "Server type: "
            r6.append(r1)
            java.lang.String r1 = "release"
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "Local timezone: "
            r6.append(r1)
            java.lang.String r1 = "Local Time Zone ("
            r6.append(r1)
            java.lang.String r1 = "timeZone"
            com.fossil.ee7.a(r12, r1)
            java.lang.String r1 = r12.getID()
            r6.append(r1)
            r6.append(r15)
            r1 = 0
            java.lang.String r2 = r12.getDisplayName(r1, r1)
            r6.append(r2)
            r6.append(r4)
            r6.append(r0)
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r0, r11)
            long r0 = r0.getTimeInMillis()
            int r0 = r12.getOffset(r0)
            r6.append(r0)
            r6.append(r8)
            java.lang.String r0 = "Date time: "
            r6.append(r0)
            com.fossil.we7 r0 = com.fossil.we7.a
            r1 = 2
            java.lang.Object[] r0 = new java.lang.Object[r1]
            long r4 = java.lang.System.currentTimeMillis()
            r2 = 1000(0x3e8, float:1.401E-42)
            long r10 = (long) r2
            long r4 = r4 / r10
            java.lang.Long r2 = com.fossil.pb7.a(r4)
            r4 = 0
            r0[r4] = r2
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            java.lang.String r2 = com.fossil.zd5.e(r2)
            r4 = 1
            r0[r4] = r2
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r1)
            java.lang.String r0 = java.lang.String.format(r9, r0)
            r1 = r23
            com.fossil.ee7.a(r0, r1)
            r6.append(r0)
            r6.append(r8)
            java.lang.String r0 = "App version:"
            r6.append(r0)
            com.fossil.rd5$a r0 = com.fossil.rd5.g
            java.lang.String r0 = r0.h()
            r6.append(r0)
            r6.append(r8)
            r0 = r32
            r6.append(r0)
            r0 = r31
            r6.append(r0)
            r6.append(r8)
            r0 = r20
            r6.append(r0)
            r6.append(r8)
            r6.append(r3)
            r6.append(r8)
            r0 = r24
            r6.append(r0)
            r6.append(r8)
            r6.append(r8)
            java.lang.String r0 = "Data Log:"
            r6.append(r0)
            java.lang.String r0 = r6.toString()
            goto L_0x07f2
        L_0x0679:
            r17 = r3
            r18 = r6
            r3 = r23
            r25 = r24
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r14)
            r6.append(r5)
            android.content.res.Resources r5 = r16.getResources()
            r14 = 2131887234(0x7f120482, float:1.940907E38)
            java.lang.String r5 = r5.getString(r14)
            r6.append(r5)
            java.lang.String r5 = " "
            r6.append(r5)
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            boolean r5 = r5.e()
            if (r5 == 0) goto L_0x06aa
            r5 = r18
            goto L_0x06ac
        L_0x06aa:
            java.lang.String r5 = "Production"
        L_0x06ac:
            r6.append(r5)
            r6.append(r8)
            r6.append(r10)
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "UserID:"
            r6.append(r1)
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 == 0) goto L_0x06c8
            r2 = r19
        L_0x06c8:
            r6.append(r2)
            r6.append(r8)
            java.lang.String r1 = "Device name: "
            r6.append(r1)
            java.lang.String r1 = android.os.Build.DEVICE
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "System name: "
            r6.append(r1)
            java.lang.String r1 = "Android OS"
            r6.append(r1)
            r6.append(r8)
            r6.append(r7)
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            r6.append(r1)
            r6.append(r8)
            r6.append(r13)
            java.lang.String r1 = android.os.Build.MODEL
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "App code: android_"
            r6.append(r1)
            com.fossil.ol4$a r1 = com.fossil.ol4.A
            java.lang.String r1 = r1.a()
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "Language code: "
            r6.append(r1)
            java.util.Locale r1 = java.util.Locale.getDefault()
            java.lang.String r2 = "Locale.getDefault()"
            com.fossil.ee7.a(r1, r2)
            java.lang.String r1 = r1.getLanguage()
            r6.append(r1)
            java.lang.String r1 = "_t"
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "Server type: "
            r6.append(r1)
            java.lang.String r1 = "release"
            r6.append(r1)
            r6.append(r8)
            java.lang.String r1 = "Local timezone: "
            r6.append(r1)
            java.lang.String r1 = "Local Time Zone ("
            r6.append(r1)
            java.lang.String r1 = "timeZone"
            com.fossil.ee7.a(r12, r1)
            java.lang.String r1 = r12.getID()
            r6.append(r1)
            r6.append(r15)
            r1 = 0
            java.lang.String r2 = r12.getDisplayName(r1, r1)
            r6.append(r2)
            r6.append(r4)
            r6.append(r0)
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r0, r11)
            long r0 = r0.getTimeInMillis()
            int r0 = r12.getOffset(r0)
            r6.append(r0)
            r6.append(r8)
            java.lang.String r0 = "Date time: "
            r6.append(r0)
            com.fossil.we7 r0 = com.fossil.we7.a
            r1 = 2
            java.lang.Object[] r0 = new java.lang.Object[r1]
            long r4 = java.lang.System.currentTimeMillis()
            r2 = 1000(0x3e8, float:1.401E-42)
            long r10 = (long) r2
            long r4 = r4 / r10
            java.lang.Long r2 = com.fossil.pb7.a(r4)
            r4 = 0
            r0[r4] = r2
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            java.lang.String r2 = com.fossil.zd5.e(r2)
            r4 = 1
            r0[r4] = r2
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r1)
            java.lang.String r0 = java.lang.String.format(r9, r0)
            com.fossil.ee7.a(r0, r3)
            r6.append(r0)
            r6.append(r8)
            java.lang.String r0 = "App version: "
            r6.append(r0)
            com.fossil.rd5$a r0 = com.fossil.rd5.g
            java.lang.String r0 = r0.h()
            r6.append(r0)
            r6.append(r8)
            r0 = r32
            r6.append(r0)
            r0 = r31
            r6.append(r0)
            r6.append(r8)
            java.lang.String r0 = "\n__________\n"
            r6.append(r0)
            r0 = r20
            r6.append(r0)
            r6.append(r8)
            r3 = r17
            r6.append(r3)
            r6.append(r8)
            r0 = r25
            r6.append(r0)
            r6.append(r8)
            r6.append(r8)
            java.lang.String r0 = "Data Log:"
            r6.append(r0)
            java.lang.String r0 = r6.toString()
        L_0x07f2:
            return r0
        L_0x07f3:
            java.lang.String r0 = "mDecryptValueKeystore"
            com.fossil.ee7.d(r0)
            r0 = 0
            throw r0
        L_0x07fa:
            r0 = 0
            com.fossil.ee7.d(r5)
            throw r0
        L_0x07ff:
            r0 = 0
            com.fossil.ee7.d(r5)
            throw r0
        L_0x0804:
            r0 = 0
            java.lang.String r1 = "mUserRepository"
            com.fossil.ee7.d(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rd5.a(android.content.Context, int, int, com.fossil.fb7):java.lang.Object");
    }
}
