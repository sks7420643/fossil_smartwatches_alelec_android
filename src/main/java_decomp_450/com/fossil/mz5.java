package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.q66;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz5 extends go5 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<c05> g;
    @DexIgnore
    public q66 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final mz5 a(String str) {
            ee7.b(str, "watchAppId");
            mz5 mz5 = new mz5();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCHAPP_ID", str);
            mz5.setArguments(bundle);
            return mz5;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 a;

        @DexIgnore
        public b(mz5 mz5) {
            this.a = mz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<q66.b> {
        @DexIgnore
        public /* final */ /* synthetic */ mz5 a;
        @DexIgnore
        public /* final */ /* synthetic */ c05 b;

        @DexIgnore
        public c(mz5 mz5, c05 c05) {
            this.a = mz5;
            this.b = c05;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(q66.b bVar) {
            Integer a2 = bVar.a();
            if (a2 != null) {
                ((hw) ((hw) aw.a(this.a).a(Integer.valueOf(a2.intValue())).a(iy.a)).a(true)).a(this.b.r);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        c05 c05 = (c05) qb.a(layoutInflater, 2131558540, viewGroup, false, a1());
        PortfolioApp.g0.c().f().o().a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(q66.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ialViewModel::class.java)");
            this.h = (q66) a2;
            c05.q.setOnClickListener(new b(this));
            Bundle arguments = getArguments();
            if (arguments != null) {
                q66 q66 = this.h;
                if (q66 != null) {
                    String string = arguments.getString("EXTRA_WATCHAPP_ID");
                    if (string != null) {
                        ee7.a((Object) string, "it.getString(EXTRA_WATCHAPP_ID)!!");
                        q66.a(string);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mViewModel");
                    throw null;
                }
            }
            q66 q662 = this.h;
            if (q662 != null) {
                q662.a().a(getViewLifecycleOwner(), new c(this, c05));
                qw6<c05> qw6 = new qw6<>(this, c05);
                this.g = qw6;
                if (qw6 != null) {
                    c05 a3 = qw6.a();
                    if (a3 != null) {
                        ee7.a((Object) a3, "mBinding.get()!!");
                        return a3.d();
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }
}
