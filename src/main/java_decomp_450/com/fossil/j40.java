package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j40 {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ dx<T> b;

        @DexIgnore
        public a(Class<T> cls, dx<T> dxVar) {
            this.a = cls;
            this.b = dxVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <Z> void a(Class<Z> cls, dx<Z> dxVar) {
        this.a.add(new a<>(cls, dxVar));
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r5v3. Raw type applied. Possible types: com.fossil.dx<T>, com.fossil.dx<Z> */
    public synchronized <Z> dx<Z> a(Class<Z> cls) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            a<?> aVar = this.a.get(i);
            if (aVar.a(cls)) {
                return (dx<T>) aVar.b;
            }
        }
        return null;
    }
}
