package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt6 implements Factory<wt6> {
    @DexIgnore
    public static wt6 a(tt6 tt6, UserRepository userRepository, ph5 ph5, ThemeRepository themeRepository) {
        return new wt6(tt6, userRepository, ph5, themeRepository);
    }
}
