package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz3 extends jz3<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ fz3 INSTANCE; // = new fz3();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public transient jz3<Comparable> a;
    @DexIgnore
    public transient jz3<Comparable> b;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends Comparable> jz3<S> nullsFirst() {
        jz3<S> jz3 = (jz3<S>) this.a;
        if (jz3 != null) {
            return jz3;
        }
        jz3<S> nullsFirst = super.nullsFirst();
        this.a = nullsFirst;
        return nullsFirst;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends Comparable> jz3<S> nullsLast() {
        jz3<S> jz3 = (jz3<S>) this.b;
        if (jz3 != null) {
            return jz3;
        }
        jz3<S> nullsLast = super.nullsLast();
        this.b = nullsLast;
        return nullsLast;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends Comparable> jz3<S> reverse() {
        return uz3.INSTANCE;
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural()";
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        jw3.a(comparable);
        jw3.a(comparable2);
        return comparable.compareTo(comparable2);
    }
}
