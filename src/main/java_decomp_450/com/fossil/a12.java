package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.fossil.j62;
import com.fossil.v02;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class a12 {
    @DexIgnore
    public static /* final */ Set<a12> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    @Deprecated
    public interface b extends t12 {
    }

    @DexIgnore
    @Deprecated
    public interface c extends a22 {
    }

    @DexIgnore
    public static Set<a12> i() {
        Set<a12> set;
        synchronized (a) {
            set = a;
        }
        return set;
    }

    @DexIgnore
    public abstract i02 a();

    @DexIgnore
    public <A extends v02.b, R extends i12, T extends r12<R, A>> T a(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void a(c cVar);

    @DexIgnore
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    public abstract c12<Status> b();

    @DexIgnore
    public <A extends v02.b, T extends r12<? extends i12, A>> T b(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void b(c cVar);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public Context e() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Looper f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public void h() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static final class a {
        @DexIgnore
        public Account a;
        @DexIgnore
        public /* final */ Set<Scope> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Scope> c; // = new HashSet();
        @DexIgnore
        public int d;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public /* final */ Map<v02<?>, j62.b> h; // = new n4();
        @DexIgnore
        public /* final */ Context i;
        @DexIgnore
        public /* final */ Map<v02<?>, v02.d> j; // = new n4();
        @DexIgnore
        public w12 k;
        @DexIgnore
        public int l; // = -1;
        @DexIgnore
        public c m;
        @DexIgnore
        public Looper n;
        @DexIgnore
        public l02 o; // = l02.a();
        @DexIgnore
        public v02.a<? extends xn3, fn3> p; // = un3.c;
        @DexIgnore
        public /* final */ ArrayList<b> q; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<c> r; // = new ArrayList<>();

        @DexIgnore
        public a(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @DexIgnore
        public final a a(Handler handler) {
            a72.a((Object) handler, (Object) "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }

        @DexIgnore
        public final j62 b() {
            fn3 fn3 = fn3.j;
            if (this.j.containsKey(un3.e)) {
                fn3 = (fn3) this.j.get(un3.e);
            }
            return new j62(this.a, this.b, this.h, this.d, this.e, this.f, this.g, fn3, false);
        }

        @DexIgnore
        public final a a(b bVar) {
            a72.a(bVar, "Listener must not be null");
            this.q.add(bVar);
            return this;
        }

        @DexIgnore
        public final a a(c cVar) {
            a72.a(cVar, "Listener must not be null");
            this.r.add(cVar);
            return this;
        }

        @DexIgnore
        public final a a(String[] strArr) {
            for (String str : strArr) {
                this.b.add(new Scope(str));
            }
            return this;
        }

        @DexIgnore
        public final a a(v02<? extends v02.d.e> v02) {
            a72.a(v02, "Api must not be null");
            this.j.put(v02, null);
            List<Scope> a2 = v02.c().a(null);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final a a(v02<? extends v02.d.e> v02, Scope... scopeArr) {
            a72.a(v02, "Api must not be null");
            this.j.put(v02, null);
            a(v02, null, scopeArr);
            return this;
        }

        @DexIgnore
        public final <O extends v02.d.c> a a(v02<O> v02, O o2) {
            a72.a(v02, "Api must not be null");
            a72.a(o2, "Null options are not permitted for this Api");
            this.j.put(v02, o2);
            List<Scope> a2 = v02.c().a(o2);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final a12 a() {
            a72.a(!this.j.isEmpty(), "must call addApi() to add at least one API");
            j62 b2 = b();
            v02<?> v02 = null;
            Map<v02<?>, j62.b> f2 = b2.f();
            n4 n4Var = new n4();
            n4 n4Var2 = new n4();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (v02<?> v022 : this.j.keySet()) {
                v02.d dVar = this.j.get(v022);
                boolean z2 = f2.get(v022) != null;
                n4Var.put(v022, Boolean.valueOf(z2));
                f52 f52 = new f52(v022, z2);
                arrayList.add(f52);
                v02.a<?, ?> d2 = v022.d();
                v02.f a2 = d2.a(this.i, this.n, b2, dVar, f52, f52);
                n4Var2.put(v022.a(), a2);
                if (d2.a() == 1) {
                    z = dVar != null;
                }
                if (a2.d()) {
                    if (v02 == null) {
                        v02 = v022;
                    } else {
                        String b3 = v022.b();
                        String b4 = v02.b();
                        StringBuilder sb = new StringBuilder(String.valueOf(b3).length() + 21 + String.valueOf(b4).length());
                        sb.append(b3);
                        sb.append(" cannot be used with ");
                        sb.append(b4);
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
            if (v02 != null) {
                if (!z) {
                    a72.b(this.a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", v02.b());
                    a72.b(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", v02.b());
                } else {
                    String b5 = v02.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b5).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b5);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            c32 c32 = new c32(this.i, new ReentrantLock(), this.n, b2, this.o, this.p, n4Var, this.q, this.r, n4Var2, this.l, c32.a((Iterable<v02.f>) n4Var2.values(), true), arrayList, false);
            synchronized (a12.a) {
                a12.a.add(c32);
            }
            if (this.l >= 0) {
                x42.b(this.k).a(this.l, c32, this.m);
            }
            return c32;
        }

        @DexIgnore
        public final <O extends v02.d> void a(v02<O> v02, O o2, Scope... scopeArr) {
            HashSet hashSet = new HashSet(v02.c().a(o2));
            for (Scope scope : scopeArr) {
                hashSet.add(scope);
            }
            this.h.put(v02, new j62.b(hashSet));
        }
    }

    @DexIgnore
    public boolean a(c22 c22) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public void a(m42 m42) {
        throw new UnsupportedOperationException();
    }
}
