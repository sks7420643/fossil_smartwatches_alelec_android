package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl4 extends ArrayAdapter<AutocompletePrediction> implements Filterable {
    @DexIgnore
    public static /* final */ String f; // = f;
    @DexIgnore
    public static /* final */ StyleSpan g; // = new StyleSpan(1);
    @DexIgnore
    public List<? extends AutocompletePrediction> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public AutocompleteSessionToken c;
    @DexIgnore
    public Date d;
    @DexIgnore
    public /* final */ PlacesClient e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void m(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ zl4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(zl4 zl4) {
            this.a = zl4;
        }

        @DexIgnore
        public CharSequence convertResultToString(Object obj) {
            SpannableString fullText;
            ee7.b(obj, "resultValue");
            AutocompletePrediction autocompletePrediction = (AutocompletePrediction) (!(obj instanceof AutocompletePrediction) ? null : obj);
            if (autocompletePrediction != null && (fullText = autocompletePrediction.getFullText(null)) != null) {
                return fullText;
            }
            CharSequence convertResultToString = super.convertResultToString(obj);
            ee7.a((Object) convertResultToString, "super.convertResultToString(resultValue)");
            return convertResultToString;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            List arrayList = new ArrayList();
            if (charSequence != null) {
                arrayList = this.a.a(charSequence);
            }
            filterResults.values = arrayList;
            if (arrayList != null) {
                filterResults.count = arrayList.size();
            } else {
                filterResults.count = 0;
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.count <= 0) {
                this.a.notifyDataSetInvalidated();
                if (this.a.b != null) {
                    b a2 = this.a.b;
                    if (a2 != null) {
                        a2.m(true);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            } else {
                zl4 zl4 = this.a;
                Object obj = filterResults.values;
                if (obj != null) {
                    zl4.a = (List) obj;
                    this.a.notifyDataSetChanged();
                    if (this.a.b != null) {
                        b a3 = this.a.b;
                        if (a3 != null) {
                            a3.m(false);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.collections.List<com.google.android.libraries.places.api.model.AutocompletePrediction>");
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zl4(Context context, PlacesClient placesClient) {
        super(context, 2131558489, 2131362924);
        ee7.b(context, "context");
        this.e = placesClient;
    }

    @DexIgnore
    public final AutocompleteSessionToken b() {
        return this.c;
    }

    @DexIgnore
    public int getCount() {
        return this.a.size();
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        View view2 = super.getView(i, view, viewGroup);
        ee7.a((Object) view2, "super.getView(position, convertView, parent)");
        AutocompletePrediction item = getItem(i);
        if (item != null) {
            FlexibleTextView flexibleTextView = (FlexibleTextView) view2.findViewById(2131362535);
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) view2.findViewById(2131362924);
            FlexibleTextView flexibleTextView3 = (FlexibleTextView) view2.findViewById(2131363044);
            ee7.a((Object) flexibleTextView, "fullTv");
            flexibleTextView.setText(item.getFullText(g));
            ee7.a((Object) flexibleTextView2, "textView1");
            flexibleTextView2.setText(item.getPrimaryText(g));
            ee7.a((Object) flexibleTextView3, "textView2");
            flexibleTextView3.setText(item.getSecondaryText(g));
        }
        return view2;
    }

    @DexIgnore
    @Override // android.widget.ArrayAdapter
    public AutocompletePrediction getItem(int i) {
        return (AutocompletePrediction) this.a.get(i);
    }

    @DexIgnore
    public final void a(b bVar) {
        this.b = bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        if ((r2 - r0.getTime()) >= ((long) 180000)) goto L_0x002c;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.libraries.places.api.model.AutocompletePrediction> a(java.lang.CharSequence r7) {
        /*
            r6 = this;
            com.google.android.libraries.places.api.net.PlacesClient r0 = r6.e
            r1 = 0
            if (r0 == 0) goto L_0x00bb
            com.google.android.libraries.places.api.model.AutocompleteSessionToken r0 = r6.c
            if (r0 == 0) goto L_0x002c
            java.util.Date r0 = r6.d
            if (r0 == 0) goto L_0x0039
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            long r2 = r0.getTime()
            java.util.Date r0 = r6.d
            if (r0 == 0) goto L_0x0028
            long r4 = r0.getTime()
            long r2 = r2 - r4
            r0 = 180000(0x2bf20, float:2.52234E-40)
            long r4 = (long) r0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0039
            goto L_0x002c
        L_0x0028:
            com.fossil.ee7.a()
            throw r1
        L_0x002c:
            com.google.android.libraries.places.api.model.AutocompleteSessionToken r0 = com.google.android.libraries.places.api.model.AutocompleteSessionToken.newInstance()
            r6.c = r0
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            r6.d = r0
        L_0x0039:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.zl4.f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Starting autocomplete query for: "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r0 = com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest.builder()
            java.lang.String r7 = r7.toString()
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r7 = r0.setQuery(r7)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r7 = r7.setCountry(r1)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r7 = r7.setLocationBias(r1)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r7 = r7.setLocationRestriction(r1)
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest$Builder r7 = r7.setTypeFilter(r1)
            java.lang.String r0 = "FindAutocompletePredicti\u2026     .setTypeFilter(null)"
            com.fossil.ee7.a(r7, r0)
            com.google.android.libraries.places.api.model.AutocompleteSessionToken r0 = r6.c
            r7.setSessionToken(r0)
            com.google.android.libraries.places.api.net.PlacesClient r0 = r6.e
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest r7 = r7.build()
            com.fossil.no3 r7 = r0.findAutocompletePredictions(r7)
            java.lang.String r0 = "mPlacesClient.findAutoco\u2026s(requestBuilder.build())"
            com.fossil.ee7.a(r7, r0)
            java.lang.Object r7 = com.fossil.qo3.a(r7)     // Catch:{ Exception -> 0x009a }
            com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse r7 = (com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse) r7     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = "response"
            com.fossil.ee7.a(r7, r0)     // Catch:{ Exception -> 0x009a }
            java.util.List r7 = r7.getAutocompletePredictions()     // Catch:{ Exception -> 0x009a }
            return r7
        L_0x009a:
            r7 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.zl4.f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Query failed. Received Exception="
            r3.append(r4)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            r7.printStackTrace()
            return r1
        L_0x00bb:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r0 = com.fossil.zl4.f
            java.lang.String r2 = "client is not connected for autocomplete query."
            r7.e(r0, r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zl4.a(java.lang.CharSequence):java.util.List");
    }

    @DexIgnore
    public final void a() {
        this.c = null;
    }
}
