package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z60 extends w60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ j90 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<z60> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final LinkedHashSet<j90> a(j90 j90) {
            LinkedHashSet<j90> linkedHashSet = new LinkedHashSet<>();
            if (j90 == null) {
                ba7.a(linkedHashSet, j90.values());
            } else {
                linkedHashSet.add(j90);
            }
            return linkedHashSet;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public z60 createFromParcel(Parcel parcel) {
            return (z60) w60.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public z60[] newArray(int i) {
            return new z60[i];
        }
    }

    @DexIgnore
    public z60(byte b, byte b2, j90 j90) throws IllegalArgumentException {
        super(b, b2, CREATOR.a(j90), false, false, true);
        this.h = j90;
    }

    @DexIgnore
    public final j90 getDayOfWeek() {
        return this.h;
    }
}
