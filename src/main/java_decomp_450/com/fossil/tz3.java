package com.fossil;

import com.fossil.e04;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz3<E> extends ny3<E> {
    @DexIgnore
    public static /* final */ tz3<Comparable> NATURAL_EMPTY_SET; // = new tz3<>(zx3.of(), jz3.natural());
    @DexIgnore
    public /* final */ transient zx3<E> b;

    @DexIgnore
    public tz3(zx3<E> zx3, Comparator<? super E> comparator) {
        super(comparator);
        this.b = zx3;
    }

    @DexIgnore
    public final int a(Object obj) throws ClassCastException {
        return Collections.binarySearch(this.b, obj, unsafeComparator());
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E ceiling(E e) {
        int tailIndex = tailIndex(e, true);
        if (tailIndex == size()) {
            return null;
        }
        return this.b.get(tailIndex);
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a(obj) >= 0;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, java.util.AbstractCollection
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof dz3) {
            collection = ((dz3) collection).elementSet();
        }
        if (!d04.a(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        kz3 c = qy3.c(iterator());
        Iterator<?> it = collection.iterator();
        Object next = it.next();
        while (c.hasNext()) {
            try {
                int unsafeCompare = unsafeCompare(c.peek(), next);
                if (unsafeCompare < 0) {
                    c.next();
                } else if (unsafeCompare == 0) {
                    if (!it.hasNext()) {
                        return true;
                    }
                    next = it.next();
                } else if (unsafeCompare > 0) {
                    break;
                }
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public int copyIntoArray(Object[] objArr, int i) {
        return this.b.copyIntoArray(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public zx3<E> createAsList() {
        return size() <= 1 ? this.b : new ky3(this, this.b);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> createDescendingSet() {
        jz3 reverse = jz3.from(((ny3) this).comparator).reverse();
        return isEmpty() ? ny3.emptySet(reverse) : new tz3(this.b.reverse(), reverse);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034 A[Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }] */
    @Override // com.fossil.iy3
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r0 = 1
            if (r6 != r5) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r6 instanceof java.util.Set
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            java.util.Set r6 = (java.util.Set) r6
            int r1 = r5.size()
            int r3 = r6.size()
            if (r1 == r3) goto L_0x0017
            return r2
        L_0x0017:
            boolean r1 = r5.isEmpty()
            if (r1 == 0) goto L_0x001e
            return r0
        L_0x001e:
            java.util.Comparator<? super E> r1 = r5.comparator
            boolean r1 = com.fossil.d04.a(r1, r6)
            if (r1 == 0) goto L_0x0047
            java.util.Iterator r6 = r6.iterator()
            com.fossil.j04 r1 = r5.iterator()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
        L_0x002e:
            boolean r3 = r1.hasNext()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            if (r3 == 0) goto L_0x0045
            java.lang.Object r3 = r1.next()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            java.lang.Object r4 = r6.next()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            if (r4 == 0) goto L_0x0044
            int r3 = r5.unsafeCompare(r3, r4)     // Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }
            if (r3 == 0) goto L_0x002e
        L_0x0044:
            return r2
        L_0x0045:
            return r0
        L_0x0046:
            return r2
        L_0x0047:
            boolean r6 = r5.containsAll(r6)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tz3.equals(java.lang.Object):boolean");
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.ny3
    public E first() {
        if (!isEmpty()) {
            return this.b.get(0);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E floor(E e) {
        int headIndex = headIndex(e, true) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.b.get(headIndex);
    }

    @DexIgnore
    public tz3<E> getSubSet(int i, int i2) {
        if (i == 0 && i2 == size()) {
            return this;
        }
        if (i < i2) {
            return new tz3<>(this.b.subList(i, i2), ((ny3) this).comparator);
        }
        return ny3.emptySet(((ny3) this).comparator);
    }

    @DexIgnore
    public int headIndex(E e, boolean z) {
        zx3<E> zx3 = this.b;
        jw3.a(e);
        return e04.a(zx3, e, comparator(), z ? e04.c.FIRST_AFTER : e04.c.FIRST_PRESENT, e04.b.NEXT_HIGHER);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> headSetImpl(E e, boolean z) {
        return getSubSet(0, headIndex(e, z));
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E higher(E e) {
        int tailIndex = tailIndex(e, false);
        if (tailIndex == size()) {
            return null;
        }
        return this.b.get(tailIndex);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        try {
            int a = e04.a(this.b, obj, unsafeComparator(), e04.c.ANY_PRESENT, e04.b.INVERTED_INSERTION_INDEX);
            if (a >= 0) {
                return a;
            }
            return -1;
        } catch (ClassCastException unused) {
            return -1;
        }
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return this.b.isPartialView();
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.ny3
    public E last() {
        if (!isEmpty()) {
            return this.b.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3
    public E lower(E e) {
        int headIndex = headIndex(e, false) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.b.get(headIndex);
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> subSetImpl(E e, boolean z, E e2, boolean z2) {
        return tailSetImpl(e, z).headSetImpl(e2, z2);
    }

    @DexIgnore
    public int tailIndex(E e, boolean z) {
        zx3<E> zx3 = this.b;
        jw3.a(e);
        return e04.a(zx3, e, comparator(), z ? e04.c.FIRST_PRESENT : e04.c.FIRST_AFTER, e04.b.NEXT_HIGHER);
    }

    @DexIgnore
    @Override // com.fossil.ny3
    public ny3<E> tailSetImpl(E e, boolean z) {
        return getSubSet(tailIndex(e, z), size());
    }

    @DexIgnore
    public Comparator<Object> unsafeComparator() {
        return ((ny3) this).comparator;
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.ny3, com.fossil.ny3
    public j04<E> descendingIterator() {
        return this.b.reverse().iterator();
    }

    @DexIgnore
    @Override // com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.util.NavigableSet, java.lang.Iterable, java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.ny3, com.fossil.ny3
    public j04<E> iterator() {
        return this.b.iterator();
    }
}
