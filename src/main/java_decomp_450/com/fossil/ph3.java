package com.fossil;

import android.os.Binder;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph3 extends ag3 {
    @DexIgnore
    public /* final */ xl3 a;
    @DexIgnore
    public Boolean b;
    @DexIgnore
    public String c;

    @DexIgnore
    public ph3(xl3 xl3) {
        this(xl3, null);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(ub3 ub3, nm3 nm3) {
        a72.a(ub3);
        b(nm3, false);
        a(new ai3(this, ub3, nm3));
    }

    @DexIgnore
    public final ub3 b(ub3 ub3, nm3 nm3) {
        tb3 tb3;
        boolean z = false;
        if (!(!"_cmp".equals(ub3.a) || (tb3 = ub3.b) == null || tb3.zza() == 0)) {
            String e = ub3.b.e("_cis");
            if (!TextUtils.isEmpty(e) && (("referrer broadcast".equals(e) || "referrer API".equals(e)) && this.a.h().e(nm3.a, wb3.S))) {
                z = true;
            }
        }
        if (!z) {
            return ub3;
        }
        this.a.e().z().a("Event has been filtered ", ub3.toString());
        return new ub3("_cmpx", ub3.b, ub3.c, ub3.d);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void c(nm3 nm3) {
        b(nm3, false);
        a(new di3(this, nm3));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void d(nm3 nm3) {
        b(nm3, false);
        a(new rh3(this, nm3));
    }

    @DexIgnore
    public ph3(xl3 xl3, String str) {
        a72.a(xl3);
        this.a = xl3;
        this.c = null;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(ub3 ub3, String str, String str2) {
        a72.a(ub3);
        a72.b(str);
        a(str, true);
        a(new zh3(this, ub3, str));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final byte[] a(ub3 ub3, String str) {
        a72.b(str);
        a72.a(ub3);
        a(str, true);
        this.a.e().A().a("Log and bundle. event", this.a.o().a(ub3.a));
        long a2 = this.a.zzm().a() / 1000000;
        try {
            byte[] bArr = (byte[]) this.a.c().b(new ci3(this, ub3, str)).get();
            if (bArr == null) {
                this.a.e().t().a("Log and bundle returned null. appId", jg3.a(str));
                bArr = new byte[0];
            }
            this.a.e().A().a("Log and bundle processed. event, size, time_ms", this.a.o().a(ub3.a), Integer.valueOf(bArr.length), Long.valueOf((this.a.zzm().a() / 1000000) - a2));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.a.e().t().a("Failed to log and bundle. appId, event, error", jg3.a(str), this.a.o().a(ub3.a), e);
            return null;
        }
    }

    @DexIgnore
    public final void b(nm3 nm3, boolean z) {
        a72.a(nm3);
        a(nm3.a, false);
        this.a.p().a(nm3.b, nm3.w, nm3.A);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void b(nm3 nm3) {
        a(nm3.a, false);
        a(new xh3(this, nm3));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(em3 em3, nm3 nm3) {
        a72.a(em3);
        b(nm3, false);
        a(new bi3(this, em3, nm3));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<em3> a(nm3 nm3, boolean z) {
        b(nm3, false);
        try {
            List<gm3> list = (List) this.a.c().a(new ei3(this, nm3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (gm3 gm3 : list) {
                if (z || !jm3.i(gm3.c)) {
                    arrayList.add(new em3(gm3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.e().t().a("Failed to get user properties. appId", jg3.a(nm3.a), e);
            return null;
        }
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.b == null) {
                        if (!"com.google.android.gms".equals(this.c) && !y92.a(this.a.f(), Binder.getCallingUid())) {
                            if (!r02.a(this.a.f()).a(Binder.getCallingUid())) {
                                z2 = false;
                                this.b = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.b = Boolean.valueOf(z2);
                    }
                    if (this.b.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e) {
                    this.a.e().t().a("Measurement Service called with invalid calling package. appId", jg3.a(str));
                    throw e;
                }
            }
            if (this.c == null && q02.a(this.a.f(), Binder.getCallingUid(), str)) {
                this.c = str;
            }
            if (!str.equals(this.c)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", str));
            }
            return;
        }
        this.a.e().t().a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(long j, String str, String str2, String str3) {
        a(new gi3(this, str2, str3, str, j));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(Bundle bundle, nm3 nm3) {
        if (g23.a() && this.a.h().a(wb3.O0)) {
            b(nm3, false);
            a(new sh3(this, nm3, bundle));
        }
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final String a(nm3 nm3) {
        b(nm3, false);
        return this.a.d(nm3);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(wm3 wm3, nm3 nm3) {
        a72.a(wm3);
        a72.a(wm3.c);
        b(nm3, false);
        wm3 wm32 = new wm3(wm3);
        wm32.a = nm3.a;
        a(new fi3(this, wm32, nm3));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(wm3 wm3) {
        a72.a(wm3);
        a72.a(wm3.c);
        a(wm3.a, true);
        a(new uh3(this, new wm3(wm3)));
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<em3> a(String str, String str2, boolean z, nm3 nm3) {
        b(nm3, false);
        try {
            List<gm3> list = (List) this.a.c().a(new th3(this, nm3, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (gm3 gm3 : list) {
                if (z || !jm3.i(gm3.c)) {
                    arrayList.add(new em3(gm3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.e().t().a("Failed to query user properties. appId", jg3.a(nm3.a), e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<em3> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<gm3> list = (List) this.a.c().a(new wh3(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (gm3 gm3 : list) {
                if (z || !jm3.i(gm3.c)) {
                    arrayList.add(new em3(gm3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.e().t().a("Failed to get user properties as. appId", jg3.a(str), e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<wm3> a(String str, String str2, nm3 nm3) {
        b(nm3, false);
        try {
            return (List) this.a.c().a(new vh3(this, nm3, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.a.e().t().a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<wm3> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) this.a.c().a(new yh3(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.a.e().t().a("Failed to get conditional user properties as", e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        a72.a(runnable);
        if (this.a.c().s()) {
            runnable.run();
        } else {
            this.a.c().a(runnable);
        }
    }

    @DexIgnore
    public final /* synthetic */ void a(nm3 nm3, Bundle bundle) {
        this.a.k().a(nm3.a, bundle);
    }
}
