package com.fossil;

import android.app.Dialog;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c52 implements Runnable {
    @DexIgnore
    public /* final */ z42 a;
    @DexIgnore
    public /* final */ /* synthetic */ a52 b;

    @DexIgnore
    public c52(a52 a52, z42 z42) {
        this.b = a52;
        this.a = z42;
    }

    @DexIgnore
    public final void run() {
        if (this.b.b) {
            i02 a2 = this.a.a();
            if (a2.w()) {
                a52 a52 = this.b;
                ((LifecycleCallback) a52).a.startActivityForResult(GoogleApiActivity.a(a52.a(), a2.v(), this.a.b(), false), 1);
            } else if (this.b.e.c(a2.e())) {
                a52 a522 = this.b;
                a522.e.a(a522.a(), ((LifecycleCallback) this.b).a, a2.e(), 2, this.b);
            } else if (a2.e() == 18) {
                Dialog a3 = l02.a(this.b.a(), this.b);
                a52 a523 = this.b;
                a523.e.a(a523.a().getApplicationContext(), new b52(this, a3));
            } else {
                this.b.a(a2, this.a.b());
            }
        }
    }
}
