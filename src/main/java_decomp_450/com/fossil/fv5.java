package com.fossil;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.GraphRequest;
import com.fossil.dm5;
import com.fossil.fl4;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv5 extends el4 {
    @DexIgnore
    public MutableLiveData<b> e; // = new MutableLiveData<>();
    @DexIgnore
    public b f; // = new b(null, null, null, null, null, null, null, null, 255, null);
    @DexIgnore
    public /* final */ QuickResponseRepository g;
    @DexIgnore
    public /* final */ dm5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Boolean b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public b() {
            this(null, null, null, null, null, null, null, null, 255, null);
        }

        @DexIgnore
        public b(List<QuickResponseMessage> list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6) {
            this.a = num;
            this.b = bool;
            this.c = bool2;
            this.d = bool3;
            this.e = bool4;
            this.f = bool5;
            this.g = bool6;
        }

        @DexIgnore
        public final Boolean a() {
            return this.g;
        }

        @DexIgnore
        public final Boolean b() {
            return this.f;
        }

        @DexIgnore
        public final Boolean c() {
            return this.d;
        }

        @DexIgnore
        public final Boolean d() {
            return this.b;
        }

        @DexIgnore
        public final Boolean e() {
            return this.c;
        }

        @DexIgnore
        public final Boolean f() {
            return this.e;
        }

        @DexIgnore
        public final Integer g() {
            return this.a;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(List list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : list, (i & 2) != 0 ? null : num, (i & 4) != 0 ? null : bool, (i & 8) != 0 ? null : bool2, (i & 16) != 0 ? false : bool3, (i & 32) != 0 ? false : bool4, (i & 64) != 0 ? null : bool5, (i & 128) == 0 ? bool6 : null);
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, List list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, int i, Object obj) {
            bVar.a((i & 1) != 0 ? new ArrayList() : list, (i & 2) != 0 ? 0 : num, (i & 4) != 0 ? false : bool, (i & 8) != 0 ? false : bool2, (i & 16) != 0 ? false : bool3, (i & 32) != 0 ? false : bool4, (i & 64) != 0 ? false : bool5, (i & 128) != 0 ? false : bool6);
        }

        @DexIgnore
        public final void a(List<QuickResponseMessage> list, Integer num, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6) {
            this.a = num;
            this.b = bool;
            this.c = bool2;
            this.d = bool3;
            this.e = bool4;
            this.f = bool5;
            this.g = bool6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$addResponse$1", f = "QuickResponseViewModel.kt", l = {56}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $text;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$addResponse$1$1", f = "QuickResponseViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.g.insertQR(new QuickResponseMessage(this.this$0.$text));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fv5 fv5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fv5;
            this.$text = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$text, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$backPressHandle$1", f = "QuickResponseViewModel.kt", l = {100}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $messages;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$backPressHandle$1$allQuickMessages$1", f = "QuickResponseViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends QuickResponseMessage>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends QuickResponseMessage>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.g.getAllQuickResponse();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(fv5 fv5, ArrayList arrayList, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fv5;
            this.$messages = arrayList;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$messages, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!ee7.a((List) obj, ea7.n(this.$messages))) {
                b.a(this.this$0.f, null, null, null, null, null, null, null, pb7.a(true), 127, null);
                this.this$0.e();
            } else {
                b.a(this.this$0.f, null, null, null, null, pb7.a(true), null, null, null, 239, null);
                this.this$0.e();
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$removeResponse$1", f = "QuickResponseViewModel.kt", l = {70}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseMessage $qr;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$removeResponse$1$1", f = "QuickResponseViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.g.removeQRById(this.this$0.$qr.getId());
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(fv5 fv5, QuickResponseMessage quickResponseMessage, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fv5;
            this.$qr = quickResponseMessage;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$qr, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$setQuickResponseMessages$1", f = "QuickResponseViewModel.kt", l = {80}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$setQuickResponseMessages$1$1", f = "QuickResponseViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends QuickResponseMessage>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends QuickResponseMessage>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.g.getAllQuickResponse();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(fv5 fv5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fv5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<dm5.d, dm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ fv5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$setReplyMessageToDevice$1$onSuccess$1", f = "QuickResponseViewModel.kt", l = {133}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fv5$g$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel$setReplyMessageToDevice$1$onSuccess$1$1", f = "QuickResponseViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.fv5$g$a$a  reason: collision with other inner class name */
            public static final class C0066a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0066a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0066a aVar = new C0066a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0066a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.a.g.insertQRs(this.this$0.this$0.b);
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 b = qj7.b();
                    C0066a aVar = new C0066a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(b, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                el4.a(this.this$0.a, false, true, 1, null);
                b.a(this.this$0.a.f, null, null, null, null, pb7.a(true), pb7.a(false), null, null, 207, null);
                this.this$0.a.e();
                return i97.a;
            }
        }

        @DexIgnore
        public g(fv5 fv5, ArrayList arrayList) {
            this.a = fv5;
            this.b = arrayList;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dm5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("QuickResponseViewModel", "Set reply message to device success");
            ik7 unused = xh7.b(ie.a(this.a), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void a(dm5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().e("QuickResponseViewModel", "Set reply message to device fail");
            el4.a(this.a, false, true, 1, null);
            if (!xg5.b.a(PortfolioApp.g0.c().getApplicationContext(), xg5.c.BLUETOOTH_CONNECTION)) {
                this.a.a(ib5.BLUETOOTH_OFF);
                return;
            }
            b.a(this.a.f, null, null, null, null, false, true, null, null, 207, null);
            this.a.e();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public fv5(QuickResponseRepository quickResponseRepository, dm5 dm5) {
        ee7.b(quickResponseRepository, "mQRRepository");
        ee7.b(dm5, "mReplyMessageUseCase");
        this.g = quickResponseRepository;
        this.h = dm5;
    }

    @DexIgnore
    public final void e() {
        this.e.a(this.f);
    }

    @DexIgnore
    public final LiveData<List<QuickResponseMessage>> f() {
        return this.g.getAllQuickResponseLiveData();
    }

    @DexIgnore
    public final MutableLiveData<b> g() {
        return this.e;
    }

    @DexIgnore
    public final void h() {
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final boolean b(ArrayList<QuickResponseMessage> arrayList) {
        int i = 0;
        for (T t : arrayList) {
            int i2 = i + 1;
            if (i < 0) {
                w97.c();
                throw null;
            } else if (mh7.a((CharSequence) t.getResponse())) {
                return true;
            } else {
                i = i2;
            }
        }
        return false;
    }

    @DexIgnore
    public final void a(String str, int i) {
        ee7.b(str, "text");
        int length = str.length();
        b.a(this.f, null, Integer.valueOf(length), Boolean.valueOf(length >= i), null, null, null, null, null, 240, null);
        e();
    }

    @DexIgnore
    public final void a(String str, int i, int i2) {
        ee7.b(str, "text");
        if (i >= i2) {
            b.a(this.f, null, null, null, true, null, null, null, null, 240, null);
            e();
        } else if (true ^ mh7.a((CharSequence) str)) {
            ik7 unused = xh7.b(ie.a(this), null, null, new c(this, str, null), 3, null);
        }
    }

    @DexIgnore
    public final void a(QuickResponseMessage quickResponseMessage) {
        ee7.b(quickResponseMessage, "qr");
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, quickResponseMessage, null), 3, null);
    }

    @DexIgnore
    public final void a(ArrayList<QuickResponseMessage> arrayList) {
        ee7.b(arrayList, GraphRequest.DEBUG_MESSAGES_KEY);
        ik7 unused = xh7.b(ie.a(this), null, null, new d(this, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void a(Context context, ArrayList<QuickResponseMessage> arrayList, boolean z) {
        ee7.b(context, "context");
        ee7.b(arrayList, GraphRequest.DEBUG_MESSAGES_KEY);
        if (b(arrayList)) {
            b.a(this.f, null, null, null, null, null, null, true, null, 191, null);
            e();
        } else if (!xg5.b.a(PortfolioApp.g0.c().getApplicationContext(), xg5.c.BLUETOOTH_CONNECTION)) {
            a(ib5.BLUETOOTH_OFF);
        } else if (!z || xg5.a(xg5.b, context, xg5.a.QUICK_RESPONSE, false, false, true, (Integer) 112, 12, (Object) null)) {
            el4.a(this, true, false, 2, null);
            this.h.a(new dm5.b(arrayList), new g(this, arrayList));
        }
    }
}
