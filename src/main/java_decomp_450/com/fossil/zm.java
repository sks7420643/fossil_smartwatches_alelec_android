package com.fossil;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zm {
    @DexIgnore
    public static /* final */ String a; // = im.a("Schedulers");

    @DexIgnore
    public static void a(zl zlVar, WorkDatabase workDatabase, List<ym> list) {
        if (list != null && list.size() != 0) {
            ap f = workDatabase.f();
            workDatabase.beginTransaction();
            try {
                List<zo> a2 = f.a(zlVar.e());
                List<zo> a3 = f.a();
                if (a2 != null && a2.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (zo zoVar : a2) {
                        f.a(zoVar.a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (a2 != null && a2.size() > 0) {
                    zo[] zoVarArr = (zo[]) a2.toArray(new zo[a2.size()]);
                    for (ym ymVar : list) {
                        if (ymVar.a()) {
                            ymVar.a(zoVarArr);
                        }
                    }
                }
                if (a3 != null && a3.size() > 0) {
                    zo[] zoVarArr2 = (zo[]) a3.toArray(new zo[a3.size()]);
                    for (ym ymVar2 : list) {
                        if (!ymVar2.a()) {
                            ymVar2.a(zoVarArr2);
                        }
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    public static ym a(Context context, dn dnVar) {
        if (Build.VERSION.SDK_INT >= 23) {
            on onVar = new on(context, dnVar);
            jp.a(context, SystemJobService.class, true);
            im.a().a(a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return onVar;
        }
        ym a2 = a(context);
        if (a2 != null) {
            return a2;
        }
        mn mnVar = new mn(context);
        jp.a(context, SystemAlarmService.class, true);
        im.a().a(a, "Created SystemAlarmScheduler", new Throwable[0]);
        return mnVar;
    }

    @DexIgnore
    public static ym a(Context context) {
        try {
            ym ymVar = (ym) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            im.a().a(a, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
            return ymVar;
        } catch (Throwable th) {
            im.a().a(a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
