package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a77 {
    @DexIgnore
    public static /* final */ int action0; // = 2131361856;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361857;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361858;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361859;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361860;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361861;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361862;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361863;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361865;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361868;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361869;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361870;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361871;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361872;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361875;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361884;
    @DexIgnore
    public static /* final */ int always; // = 2131361887;
    @DexIgnore
    public static /* final */ int beginning; // = 2131361908;
    @DexIgnore
    public static /* final */ int belvedere_dialog_listview; // = 2131361911;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_image; // = 2131361912;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_text; // = 2131361913;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361977;
    @DexIgnore
    public static /* final */ int cancel_action; // = 2131361981;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131362015;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362019;
    @DexIgnore
    public static /* final */ int collapseActionView; // = 2131362128;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362150;
    @DexIgnore
    public static /* final */ int custom; // = 2131362161;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362162;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362189;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362190;
    @DexIgnore
    public static /* final */ int disableHome; // = 2131362199;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362208;
    @DexIgnore
    public static /* final */ int end; // = 2131362215;
    @DexIgnore
    public static /* final */ int end_padder; // = 2131362216;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362239;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362240;
    @DexIgnore
    public static /* final */ int home; // = 2131362554;
    @DexIgnore
    public static /* final */ int homeAsUp; // = 2131362555;
    @DexIgnore
    public static /* final */ int icon; // = 2131362579;
    @DexIgnore
    public static /* final */ int ifRoom; // = 2131362586;
    @DexIgnore
    public static /* final */ int image; // = 2131362587;
    @DexIgnore
    public static /* final */ int info; // = 2131362603;
    @DexIgnore
    public static /* final */ int line1; // = 2131362752;
    @DexIgnore
    public static /* final */ int line3; // = 2131362754;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362761;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362762;
    @DexIgnore
    public static /* final */ int media_actions; // = 2131362804;
    @DexIgnore
    public static /* final */ int middle; // = 2131362807;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362837;
    @DexIgnore
    public static /* final */ int never; // = 2131362839;
    @DexIgnore
    public static /* final */ int none; // = 2131362843;
    @DexIgnore
    public static /* final */ int normal; // = 2131362844;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362880;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362930;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362931;
    @DexIgnore
    public static /* final */ int radio; // = 2131362934;
    @DexIgnore
    public static /* final */ int screen; // = 2131363025;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131363027;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131363028;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131363029;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131363033;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131363034;
    @DexIgnore
    public static /* final */ int search_button; // = 2131363035;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131363036;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131363037;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131363038;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131363039;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131363040;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131363041;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131363043;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131363047;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131363062;
    @DexIgnore
    public static /* final */ int showCustom; // = 2131363063;
    @DexIgnore
    public static /* final */ int showHome; // = 2131363064;
    @DexIgnore
    public static /* final */ int showTitle; // = 2131363065;
    @DexIgnore
    public static /* final */ int spacer; // = 2131363079;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131363081;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131363085;
    @DexIgnore
    public static /* final */ int src_in; // = 2131363086;
    @DexIgnore
    public static /* final */ int src_over; // = 2131363087;
    @DexIgnore
    public static /* final */ int status_bar_latest_event_content; // = 2131363092;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131363097;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131363117;
    @DexIgnore
    public static /* final */ int text; // = 2131363132;
    @DexIgnore
    public static /* final */ int text2; // = 2131363133;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131363135;
    @DexIgnore
    public static /* final */ int time; // = 2131363154;
    @DexIgnore
    public static /* final */ int title; // = 2131363156;
    @DexIgnore
    public static /* final */ int title_template; // = 2131363159;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131363166;
    @DexIgnore
    public static /* final */ int up; // = 2131363369;
    @DexIgnore
    public static /* final */ int useLogo; // = 2131363376;
    @DexIgnore
    public static /* final */ int withText; // = 2131363471;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363473;
}
