package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.h62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gg3 extends h62<bg3> {
    @DexIgnore
    public gg3(Context context, Looper looper, h62.a aVar, h62.b bVar) {
        super(context, looper, 93, aVar, bVar, null);
    }

    @DexIgnore
    /* Return type fixed from 'android.os.IInterface' to match base method */
    @Override // com.fossil.h62
    public final /* synthetic */ bg3 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof bg3) {
            return (bg3) queryLocalInterface;
        }
        return new dg3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final int k() {
        return q02.a;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.measurement.START";
    }
}
