package com.fossil;

import java.util.regex.Pattern;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw0 extends i60 {
    @DexIgnore
    public Pattern a;

    @DexIgnore
    public pw0(String str) throws IllegalArgumentException {
        Pattern compile = Pattern.compile(str);
        ee7.a((Object) compile, "Pattern.compile(serialNumberRegex)");
        this.a = compile;
    }

    @DexIgnore
    @Override // com.fossil.i60
    public boolean a(km1 km1) {
        return this.a.matcher(km1.t.getSerialNumber()).matches();
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.F1, "serial_number_pattern"), r51.I1, this.a.toString());
    }
}
