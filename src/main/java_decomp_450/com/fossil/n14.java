package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.y62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n14 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public n14(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        a72.b(!x92.a(str), "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    @DexIgnore
    public static n14 a(Context context) {
        g72 g72 = new g72(context);
        String a2 = g72.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new n14(a2, g72.a("google_api_key"), g72.a("firebase_database_url"), g72.a("ga_trackingId"), g72.a("gcm_defaultSenderId"), g72.a("google_storage_bucket"), g72.a("project_id"));
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof n14)) {
            return false;
        }
        n14 n14 = (n14) obj;
        if (!y62.a(this.b, n14.b) || !y62.a(this.a, n14.a) || !y62.a(this.c, n14.c) || !y62.a(this.d, n14.d) || !y62.a(this.e, n14.e) || !y62.a(this.f, n14.f) || !y62.a(this.g, n14.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a("applicationId", this.b);
        a2.a("apiKey", this.a);
        a2.a("databaseUrl", this.c);
        a2.a("gcmSenderId", this.e);
        a2.a("storageBucket", this.f);
        a2.a("projectId", this.g);
        return a2.toString();
    }

    @DexIgnore
    public String a() {
        return this.a;
    }
}
