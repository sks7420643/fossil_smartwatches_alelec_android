package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class si1 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[qk1.values().length];
        a = iArr;
        iArr[qk1.MODEL_NUMBER.ordinal()] = 1;
        a[qk1.SERIAL_NUMBER.ordinal()] = 2;
        a[qk1.FIRMWARE_VERSION.ordinal()] = 3;
        a[qk1.SOFTWARE_REVISION.ordinal()] = 4;
        a[qk1.DC.ordinal()] = 5;
        a[qk1.FTC.ordinal()] = 6;
        a[qk1.FTD.ordinal()] = 7;
        a[qk1.FTD_1.ordinal()] = 8;
        a[qk1.AUTHENTICATION.ordinal()] = 9;
        a[qk1.ASYNC.ordinal()] = 10;
        int[] iArr2 = new int[qk1.values().length];
        b = iArr2;
        iArr2[qk1.MODEL_NUMBER.ordinal()] = 1;
        b[qk1.SERIAL_NUMBER.ordinal()] = 2;
        b[qk1.FIRMWARE_VERSION.ordinal()] = 3;
        b[qk1.SOFTWARE_REVISION.ordinal()] = 4;
        b[qk1.DC.ordinal()] = 5;
        b[qk1.FTC.ordinal()] = 6;
        b[qk1.ASYNC.ordinal()] = 7;
        b[qk1.FTD.ordinal()] = 8;
        b[qk1.FTD_1.ordinal()] = 9;
        b[qk1.AUTHENTICATION.ordinal()] = 10;
    }
    */
}
