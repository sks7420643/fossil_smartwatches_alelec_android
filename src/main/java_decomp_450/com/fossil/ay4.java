package com.fossil;

import android.view.View;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ay4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleEditText s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ RTLImageView v;
    @DexIgnore
    public /* final */ View w;
    @DexIgnore
    public /* final */ ProgressBar x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ RecyclerView z;

    @DexIgnore
    public ay4(Object obj, View view, int i, RTLImageView rTLImageView, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView2, View view2, ProgressBar progressBar, ConstraintLayout constraintLayout2, RecyclerView recyclerView, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = constraintLayout;
        this.s = flexibleEditText;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = rTLImageView2;
        this.w = view2;
        this.x = progressBar;
        this.y = constraintLayout2;
        this.z = recyclerView;
        this.A = flexibleTextView3;
    }
}
