package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n80 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ o80 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<n80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public n80 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                o80 valueOf = o80.valueOf(readString);
                parcel.setDataPosition(0);
                switch (d41.a[valueOf.ordinal()]) {
                    case 1:
                        return c80.CREATOR.createFromParcel(parcel);
                    case 2:
                        return k80.CREATOR.createFromParcel(parcel);
                    case 3:
                        return l80.CREATOR.createFromParcel(parcel);
                    case 4:
                        return g80.CREATOR.createFromParcel(parcel);
                    case 5:
                        return h80.CREATOR.createFromParcel(parcel);
                    case 6:
                        return m80.CREATOR.createFromParcel(parcel);
                    case 7:
                        return f80.CREATOR.createFromParcel(parcel);
                    case 8:
                        return i80.CREATOR.createFromParcel(parcel);
                    case 9:
                        return t80.CREATOR.createFromParcel(parcel);
                    case 10:
                        return w80.CREATOR.createFromParcel(parcel);
                    case 11:
                        return q80.CREATOR.createFromParcel(parcel);
                    case 12:
                        return v80.CREATOR.createFromParcel(parcel);
                    case 13:
                        return b80.CREATOR.createFromParcel(parcel);
                    case 14:
                        return r80.CREATOR.createFromParcel(parcel);
                    case 15:
                        return j80.CREATOR.createFromParcel(parcel);
                    case 16:
                        return p80.CREATOR.createFromParcel(parcel);
                    case 17:
                        return u80.CREATOR.createFromParcel(parcel);
                    case 18:
                        return d80.CREATOR.createFromParcel(parcel);
                    case 19:
                        return s80.CREATOR.createFromParcel(parcel);
                    case 20:
                        return y80.CREATOR.createFromParcel(parcel);
                    case 21:
                        return e80.CREATOR.createFromParcel(parcel);
                    default:
                        throw new p87();
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public n80[] newArray(int i) {
            return new n80[i];
        }
    }

    @DexIgnore
    public n80(o80 o80) {
        this.a = o80;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.w0, this.a.b());
            yz0.a(jSONObject, r51.x0, d());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final byte[] b() {
        ByteBuffer allocate = ByteBuffer.allocate(c().length + 3);
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.a.a()).put((byte) c().length).array();
        ee7.a((Object) array, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        byte[] array2 = allocate.put(array).put(c()).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        return array2;
    }

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public abstract Object d();

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((n80) obj).a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DeviceConfigItem");
    }

    @DexIgnore
    public final o80 getKey() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public n80(android.os.Parcel r2) {
        /*
            r1 = this;
            java.lang.String r2 = r2.readString()
            if (r2 == 0) goto L_0x0013
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.ee7.a(r2, r0)
            com.fossil.o80 r2 = com.fossil.o80.valueOf(r2)
            r1.<init>(r2)
            return
        L_0x0013:
            com.fossil.ee7.a()
            r2 = 0
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n80.<init>(android.os.Parcel):void");
    }
}
