package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ce6 extends zd6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE g;
    @DexIgnore
    public /* final */ LiveData<qx6<List<MFSleepDay>>> h;
    @DexIgnore
    public BarChart.c i;
    @DexIgnore
    public /* final */ ae6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ SleepSummariesRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter", f = "SleepOverviewWeekPresenter.kt", l = {155}, m = "calculateStartAndEndDate")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ce6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ce6 ce6, fb7 fb7) {
            super(fb7);
            this.this$0 = ce6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Date) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ce6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$mSleepSummaries$1$1", f = "SleepOverviewWeekPresenter.kt", l = {44, 48, 48}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<MFSleepDay>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ce6$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$mSleepSummaries$1$1$startAndEnd$1", f = "SleepOverviewWeekPresenter.kt", l = {44}, m = "invokeSuspend")
            /* renamed from: com.fossil.ce6$c$a$a  reason: collision with other inner class name */
            public static final class C0026a extends zb7 implements kd7<yi7, fb7<? super r87<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0026a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0026a aVar = new C0026a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super r87<? extends Date, ? extends Date>> fb7) {
                    return ((C0026a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        a aVar = this.this$0;
                        ce6 ce6 = aVar.this$0.a;
                        Date date = aVar.$it;
                        ee7.a((Object) date, "it");
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = ce6.a(date, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<MFSleepDay>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00d6 A[RETURN] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                    r10 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r10.label
                    r2 = 3
                    r3 = 2
                    r4 = 1
                    if (r1 == 0) goto L_0x004f
                    if (r1 == r4) goto L_0x0047
                    if (r1 == r3) goto L_0x002e
                    if (r1 != r2) goto L_0x0026
                    java.lang.Object r0 = r10.L$3
                    java.util.Date r0 = (java.util.Date) r0
                    java.lang.Object r0 = r10.L$2
                    java.util.Date r0 = (java.util.Date) r0
                    java.lang.Object r0 = r10.L$1
                    com.fossil.r87 r0 = (com.fossil.r87) r0
                    java.lang.Object r0 = r10.L$0
                    com.fossil.vd r0 = (com.fossil.vd) r0
                    com.fossil.t87.a(r11)
                    goto L_0x00d7
                L_0x0026:
                    java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r11.<init>(r0)
                    throw r11
                L_0x002e:
                    java.lang.Object r1 = r10.L$4
                    com.fossil.vd r1 = (com.fossil.vd) r1
                    java.lang.Object r3 = r10.L$3
                    java.util.Date r3 = (java.util.Date) r3
                    java.lang.Object r4 = r10.L$2
                    java.util.Date r4 = (java.util.Date) r4
                    java.lang.Object r5 = r10.L$1
                    com.fossil.r87 r5 = (com.fossil.r87) r5
                    java.lang.Object r6 = r10.L$0
                    com.fossil.vd r6 = (com.fossil.vd) r6
                    com.fossil.t87.a(r11)
                    goto L_0x00c4
                L_0x0047:
                    java.lang.Object r1 = r10.L$0
                    com.fossil.vd r1 = (com.fossil.vd) r1
                    com.fossil.t87.a(r11)
                    goto L_0x0070
                L_0x004f:
                    com.fossil.t87.a(r11)
                    com.fossil.vd r11 = r10.p$
                    com.fossil.ce6$c r1 = r10.this$0
                    com.fossil.ce6 r1 = r1.a
                    com.fossil.ti7 r1 = r1.b()
                    com.fossil.ce6$c$a$a r5 = new com.fossil.ce6$c$a$a
                    r6 = 0
                    r5.<init>(r10, r6)
                    r10.L$0 = r11
                    r10.label = r4
                    java.lang.Object r1 = com.fossil.vh7.a(r1, r5, r10)
                    if (r1 != r0) goto L_0x006d
                    return r0
                L_0x006d:
                    r9 = r1
                    r1 = r11
                    r11 = r9
                L_0x0070:
                    r5 = r11
                    com.fossil.r87 r5 = (com.fossil.r87) r5
                    java.lang.Object r11 = r5.getFirst()
                    r4 = r11
                    java.util.Date r4 = (java.util.Date) r4
                    java.lang.Object r11 = r5.getSecond()
                    java.util.Date r11 = (java.util.Date) r11
                    com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    java.lang.String r8 = "mSleepSummaries onDateChanged  - startDate="
                    r7.append(r8)
                    r7.append(r4)
                    java.lang.String r8 = ", endDate="
                    r7.append(r8)
                    r7.append(r11)
                    java.lang.String r7 = r7.toString()
                    java.lang.String r8 = "SleepOverviewWeekPresenter"
                    r6.d(r8, r7)
                    com.fossil.ce6$c r6 = r10.this$0
                    com.fossil.ce6 r6 = r6.a
                    com.portfolio.platform.data.source.SleepSummariesRepository r6 = r6.l
                    r7 = 0
                    r10.L$0 = r1
                    r10.L$1 = r5
                    r10.L$2 = r4
                    r10.L$3 = r11
                    r10.L$4 = r1
                    r10.label = r3
                    java.lang.Object r3 = r6.getSleepSummaries(r4, r11, r7, r10)
                    if (r3 != r0) goto L_0x00c0
                    return r0
                L_0x00c0:
                    r6 = r1
                    r9 = r3
                    r3 = r11
                    r11 = r9
                L_0x00c4:
                    androidx.lifecycle.LiveData r11 = (androidx.lifecycle.LiveData) r11
                    r10.L$0 = r6
                    r10.L$1 = r5
                    r10.L$2 = r4
                    r10.L$3 = r3
                    r10.label = r2
                    java.lang.Object r11 = r1.a(r11, r10)
                    if (r11 != r0) goto L_0x00d7
                    return r0
                L_0x00d7:
                    com.fossil.i97 r11 = com.fossil.i97.a
                    return r11
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ce6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(ce6 ce6) {
            this.a = ce6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<MFSleepDay>>> apply(Date date) {
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<qx6<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ce6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1$1", f = "SleepOverviewWeekPresenter.kt", l = {71}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ce6$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1$1$1", f = "SleepOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ce6$d$a$a  reason: collision with other inner class name */
            public static final class C0027a extends zb7 implements kd7<yi7, fb7<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0027a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0027a aVar = new C0027a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super BarChart.c> fb7) {
                    return ((C0027a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        ce6 ce6 = this.this$0.this$0.a;
                        Date c = ce6.e;
                        List list = null;
                        if (c != null) {
                            qx6 qx6 = (qx6) this.this$0.this$0.a.h.a();
                            if (qx6 != null) {
                                list = (List) qx6.c();
                            }
                            return ce6.a(c, list);
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                ce6 ce6;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ce6 ce62 = this.this$0.a;
                    ti7 a2 = ce62.b();
                    C0027a aVar = new C0027a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = ce62;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                    ce6 = ce62;
                } else if (i == 1) {
                    ce6 = (ce6) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ce6.i = (BarChart.c) obj;
                this.this$0.a.j.a((do5) this.this$0.a.i);
                return i97.a;
            }
        }

        @DexIgnore
        public d(ce6 ce6) {
            this.a = ce6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<MFSleepDay>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("SleepOverviewWeekPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, null), 3, null);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ce6(ae6 ae6, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        ee7.b(ae6, "mView");
        ee7.b(userRepository, "userRepository");
        ee7.b(sleepSummariesRepository, "mSummariesRepository");
        ee7.b(portfolioApp, "mApp");
        this.j = ae6;
        this.k = userRepository;
        this.l = sleepSummariesRepository;
        this.g = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.c());
        LiveData<qx6<List<MFSleepDay>>> b2 = ge.b(this.f, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.h = b2;
        this.i = new BarChart.c(0, 0, null, 7, null);
    }

    @DexIgnore
    @Override // com.fossil.zd6
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.g;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void j() {
        this.j.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        i();
        Date date = this.e;
        if (date == null || !zd5.w(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.a(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewWeekPresenter", "start with date " + this.e);
        LiveData<qx6<List<MFSleepDay>>> liveData = this.h;
        ae6 ae6 = this.j;
        if (ae6 != null) {
            liveData.a((be6) ae6, new d(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "stop");
        try {
            LiveData<qx6<List<MFSleepDay>>> liveData = this.h;
            ae6 ae6 = this.j;
            if (ae6 != null) {
                liveData.a((be6) ae6);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<MFSleepDay> list) {
        int i2;
        T t;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
        Calendar instance = Calendar.getInstance(Locale.US);
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            ee7.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (zd5.d(t.getDate(), instance.getTime())) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    i5 = ge5.c.a((MFSleepDay) t2);
                    int sleepMinutes = t2.getSleepMinutes();
                    i4 = Math.max(Math.max(i5, sleepMinutes), i4);
                    cVar.a().add(new BarChart.a(i5, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, sleepMinutes, null, 23, null)})}), time2, i3 == 6));
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, 0, null, 23, null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, 0, null, 23, null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 <= 0) {
            i4 = i5 > 0 ? i5 : 480;
        }
        cVar.b(i4);
        return cVar;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.util.Date r5, com.fossil.fb7<? super com.fossil.r87<? extends java.util.Date, ? extends java.util.Date>> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.fossil.ce6.b
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.ce6$b r0 = (com.fossil.ce6.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ce6$b r0 = new com.fossil.ce6$b
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r5 = r0.L$2
            com.fossil.se7 r5 = (com.fossil.se7) r5
            java.lang.Object r1 = r0.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.ce6 r0 = (com.fossil.ce6) r0
            com.fossil.t87.a(r6)
            goto L_0x0060
        L_0x0035:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003d:
            com.fossil.t87.a(r6)
            com.fossil.se7 r6 = new com.fossil.se7
            r6.<init>()
            r2 = 6
            java.util.Date r2 = com.fossil.zd5.b(r5, r2)
            r6.element = r2
            com.portfolio.platform.data.source.UserRepository r2 = r4.k
            r0.L$0 = r4
            r0.L$1 = r5
            r0.L$2 = r6
            r0.label = r3
            java.lang.Object r0 = r2.getCurrentUser(r0)
            if (r0 != r1) goto L_0x005d
            return r1
        L_0x005d:
            r1 = r5
            r5 = r6
            r6 = r0
        L_0x0060:
            com.portfolio.platform.data.model.MFUser r6 = (com.portfolio.platform.data.model.MFUser) r6
            if (r6 == 0) goto L_0x0080
            java.lang.String r6 = r6.getCreatedAt()
            if (r6 == 0) goto L_0x007b
            java.util.Date r6 = com.fossil.zd5.d(r6)
            T r0 = r5.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.fossil.zd5.b(r0, r6)
            if (r0 != 0) goto L_0x0080
            r5.element = r6
            goto L_0x0080
        L_0x007b:
            com.fossil.ee7.a()
            r5 = 0
            throw r5
        L_0x0080:
            com.fossil.r87 r6 = new com.fossil.r87
            T r5 = r5.element
            java.util.Date r5 = (java.util.Date) r5
            r6.<init>(r5, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ce6.a(java.util.Date, com.fossil.fb7):java.lang.Object");
    }
}
