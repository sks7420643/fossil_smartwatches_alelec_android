package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 b;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 c;

    @DexIgnore
    public pk3(ek3 ek3, Bundle bundle, nm3 nm3) {
        this.c = ek3;
        this.a = bundle;
        this.b = nm3;
    }

    @DexIgnore
    public final void run() {
        bg3 d = this.c.d;
        if (d == null) {
            this.c.e().t().a("Failed to send default event parameters to service");
            return;
        }
        try {
            d.a(this.a, this.b);
        } catch (RemoteException e) {
            this.c.e().t().a("Failed to send default event parameters to service", e);
        }
    }
}
