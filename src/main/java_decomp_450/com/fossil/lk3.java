package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ r43 a;
    @DexIgnore
    public /* final */ /* synthetic */ ub3 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService d;

    @DexIgnore
    public lk3(AppMeasurementDynamiteService appMeasurementDynamiteService, r43 r43, ub3 ub3, String str) {
        this.d = appMeasurementDynamiteService;
        this.a = r43;
        this.b = ub3;
        this.c = str;
    }

    @DexIgnore
    public final void run() {
        this.d.a.E().a(this.a, this.b, this.c);
    }
}
