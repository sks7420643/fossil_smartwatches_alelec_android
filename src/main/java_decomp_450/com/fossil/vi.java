package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi implements zi {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Object[] b;

    @DexIgnore
    public vi(String str, Object[] objArr) {
        this.a = str;
        this.b = objArr;
    }

    @DexIgnore
    @Override // com.fossil.zi
    public void a(yi yiVar) {
        a(yiVar, this.b);
    }

    @DexIgnore
    @Override // com.fossil.zi
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.zi
    public int a() {
        Object[] objArr = this.b;
        if (objArr == null) {
            return 0;
        }
        return objArr.length;
    }

    @DexIgnore
    public static void a(yi yiVar, Object[] objArr) {
        if (objArr != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                Object obj = objArr[i];
                i++;
                a(yiVar, i, obj);
            }
        }
    }

    @DexIgnore
    public vi(String str) {
        this(str, null);
    }

    @DexIgnore
    public static void a(yi yiVar, int i, Object obj) {
        if (obj == null) {
            yiVar.bindNull(i);
        } else if (obj instanceof byte[]) {
            yiVar.bindBlob(i, (byte[]) obj);
        } else if (obj instanceof Float) {
            yiVar.bindDouble(i, (double) ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            yiVar.bindDouble(i, ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            yiVar.bindLong(i, ((Long) obj).longValue());
        } else if (obj instanceof Integer) {
            yiVar.bindLong(i, (long) ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            yiVar.bindLong(i, (long) ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            yiVar.bindLong(i, (long) ((Byte) obj).byteValue());
        } else if (obj instanceof String) {
            yiVar.bindString(i, (String) obj);
        } else if (obj instanceof Boolean) {
            yiVar.bindLong(i, ((Boolean) obj).booleanValue() ? 1 : 0);
        } else {
            throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte, string");
        }
    }
}
