package com.fossil;

import android.os.Bundle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f34 implements e34, d34 {
    @DexIgnore
    public /* final */ h34 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ TimeUnit c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public CountDownLatch e;

    @DexIgnore
    public f34(h34 h34, int i, TimeUnit timeUnit) {
        this.a = h34;
        this.b = i;
        this.c = timeUnit;
    }

    @DexIgnore
    @Override // com.fossil.e34
    public void a(String str, Bundle bundle) {
        CountDownLatch countDownLatch = this.e;
        if (countDownLatch != null && "_ae".equals(str)) {
            countDownLatch.countDown();
        }
    }

    @DexIgnore
    @Override // com.fossil.d34
    public void b(String str, Bundle bundle) {
        synchronized (this.d) {
            z24.a().a("Logging Crashlytics event to Firebase");
            this.e = new CountDownLatch(1);
            this.a.b(str, bundle);
            z24.a().a("Awaiting app exception callback from FA...");
            try {
                if (this.e.await((long) this.b, this.c)) {
                    z24.a().a("App exception callback received from FA listener.");
                } else {
                    z24.a().a("Timeout exceeded while awaiting app exception callback from FA listener.");
                }
            } catch (InterruptedException unused) {
                z24.a().a("Interrupted while awaiting app exception callback from FA listener.");
            }
            this.e = null;
        }
    }
}
