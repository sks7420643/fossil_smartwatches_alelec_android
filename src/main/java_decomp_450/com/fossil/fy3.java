package com.fossil;

import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy3<K, V> extends vx3<V> {
    @DexIgnore
    @Weak
    public /* final */ by3<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends j04<V> {
        @DexIgnore
        public /* final */ j04<Map.Entry<K, V>> a; // = fy3.this.map.entrySet().iterator();

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return this.a.next().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends tx3<V> {
        @DexIgnore
        public /* final */ /* synthetic */ zx3 val$entryList;

        @DexIgnore
        public b(zx3 zx3) {
            this.val$entryList = zx3;
        }

        @DexIgnore
        @Override // com.fossil.tx3
        public vx3<V> delegateCollection() {
            return fy3.this;
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            return (V) ((Map.Entry) this.val$entryList.get(i)).getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ by3<?, V> map;

        @DexIgnore
        public c(by3<?, V> by3) {
            this.map = by3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.values();
        }
    }

    @DexIgnore
    public fy3(by3<K, V> by3) {
        this.map = by3;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public zx3<V> asList() {
        return new b(this.map.entrySet().asList());
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        return obj != null && qy3.a(iterator(), obj);
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public Object writeReplace() {
        return new c(this.map);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, java.util.Collection, java.lang.Iterable
    public j04<V> iterator() {
        return new a();
    }
}
