package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ag3 extends kp2 implements bg3 {
    @DexIgnore
    public ag3() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    @Override // com.fossil.kp2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((ub3) jo2.a(parcel, ub3.CREATOR), (nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                a((em3) jo2.a(parcel, em3.CREATOR), (nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                c((nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                a((ub3) jo2.a(parcel, ub3.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                d((nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<em3> a = a((nm3) jo2.a(parcel, nm3.CREATOR), jo2.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a);
                return true;
            case 9:
                byte[] a2 = a((ub3) jo2.a(parcel, ub3.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a2);
                return true;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String a3 = a((nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(a3);
                return true;
            case 12:
                a((wm3) jo2.a(parcel, wm3.CREATOR), (nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                a((wm3) jo2.a(parcel, wm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<em3> a4 = a(parcel.readString(), parcel.readString(), jo2.a(parcel), (nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                return true;
            case 15:
                List<em3> a5 = a(parcel.readString(), parcel.readString(), parcel.readString(), jo2.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                return true;
            case 16:
                List<wm3> a6 = a(parcel.readString(), parcel.readString(), (nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                return true;
            case 17:
                List<wm3> a7 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a7);
                return true;
            case 18:
                b((nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 19:
                a((Bundle) jo2.a(parcel, Bundle.CREATOR), (nm3) jo2.a(parcel, nm3.CREATOR));
                parcel2.writeNoException();
                return true;
        }
    }
}
