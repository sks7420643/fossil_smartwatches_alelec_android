package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j75 extends i75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131362049, 1);
        D.put(2131362976, 2);
        D.put(2131363335, 3);
        D.put(2131363336, 4);
        D.put(2131363057, 5);
        D.put(2131363259, 6);
        D.put(2131363261, 7);
        D.put(2131362163, 8);
        D.put(2131362636, 9);
        D.put(2131362263, 10);
    }
    */

    @DexIgnore
    public j75(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 11, C, D));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public j75(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[8], (FlexibleButton) objArr[10], (RTLImageView) objArr[9], (ConstraintLayout) objArr[0], (RecyclerView) objArr[2], (View) objArr[5], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[4]);
        this.B = -1;
        ((i75) this).u.setTag(null);
        a(view);
        f();
    }
}
