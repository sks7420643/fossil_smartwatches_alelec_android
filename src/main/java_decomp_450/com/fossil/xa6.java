package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa6 implements Factory<wa6> {
    @DexIgnore
    public static wa6 a(ua6 ua6, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, pj4 pj4) {
        return new wa6(ua6, goalTrackingRepository, userRepository, pj4);
    }
}
