package com.fossil;

import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r81 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ km1 a;
    @DexIgnore
    public /* final */ /* synthetic */ l60.c b;
    @DexIgnore
    public /* final */ /* synthetic */ l60.c c;

    @DexIgnore
    public r81(km1 km1, l60.c cVar, l60.c cVar2) {
        this.a = km1;
        this.b = cVar;
        this.c = cVar2;
    }

    @DexIgnore
    public final void run() {
        km1 km1 = this.a;
        l60.b bVar = km1.v;
        if (bVar != null) {
            bVar.onDeviceStateChanged(km1, this.b, this.c);
        }
    }
}
