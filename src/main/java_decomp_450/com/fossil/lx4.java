package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lx4 extends kx4 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362247, 1);
        A.put(2131362517, 2);
        A.put(2131362038, 3);
        A.put(2131362855, 4);
        A.put(2131362851, 5);
        A.put(2131362858, 6);
        A.put(2131362263, 7);
    }
    */

    @DexIgnore
    public lx4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public lx4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[3], (FlexibleButton) objArr[1], (FlexibleButton) objArr[7], (FlexibleTextView) objArr[2], (NumberPicker) objArr[5], (NumberPicker) objArr[4], (NumberPicker) objArr[6]);
        this.y = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.x = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
