package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da6 {
    @DexIgnore
    public /* final */ x96 a;
    @DexIgnore
    public /* final */ oa6 b;
    @DexIgnore
    public /* final */ ia6 c;

    @DexIgnore
    public da6(x96 x96, oa6 oa6, ia6 ia6) {
        ee7.b(x96, "mCaloriesOverviewDayView");
        ee7.b(oa6, "mCaloriesOverviewWeekView");
        ee7.b(ia6, "mCaloriesOverviewMonthView");
        this.a = x96;
        this.b = oa6;
        this.c = ia6;
    }

    @DexIgnore
    public final x96 a() {
        return this.a;
    }

    @DexIgnore
    public final ia6 b() {
        return this.c;
    }

    @DexIgnore
    public final oa6 c() {
        return this.b;
    }
}
