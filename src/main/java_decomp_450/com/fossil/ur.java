package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.TypedValue;
import android.webkit.MimeTypeMap;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur implements pr<Uri> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ gr b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ur(Context context, gr grVar) {
        ee7.b(context, "context");
        ee7.b(grVar, "drawableDecoder");
        this.a = context;
        this.b = grVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7] */
    @Override // com.fossil.pr
    public /* bridge */ /* synthetic */ Object a(rq rqVar, Uri uri, rt rtVar, ir irVar, fb7 fb7) {
        return a(rqVar, uri, rtVar, irVar, (fb7<? super or>) fb7);
    }

    @DexIgnore
    public final Void c(Uri uri) {
        throw new IllegalStateException("Invalid android.resource URI: " + uri);
    }

    @DexIgnore
    public String b(Uri uri) {
        ee7.b(uri, "data");
        StringBuilder sb = new StringBuilder();
        sb.append(uri);
        sb.append('-');
        Resources resources = this.a.getResources();
        ee7.a((Object) resources, "context.resources");
        Configuration configuration = resources.getConfiguration();
        ee7.a((Object) configuration, "context.resources.configuration");
        sb.append(iu.a(configuration));
        return sb.toString();
    }

    @DexIgnore
    public boolean a(Uri uri) {
        ee7.b(uri, "data");
        return ee7.a((Object) uri.getScheme(), (Object) "android.resource");
    }

    @DexIgnore
    public Object a(rq rqVar, Uri uri, rt rtVar, ir irVar, fb7<? super or> fb7) {
        Integer b2;
        BitmapDrawable bitmapDrawable;
        String authority = uri.getAuthority();
        if (authority != null) {
            ee7.a((Object) authority, "it");
            if (!pb7.a(!mh7.a((CharSequence) authority)).booleanValue()) {
                authority = null;
            }
            if (authority != null) {
                ee7.a((Object) authority, "data.authority?.takeIf {\u2026InvalidUriException(data)");
                List<String> pathSegments = uri.getPathSegments();
                ee7.a((Object) pathSegments, "data.pathSegments");
                String str = (String) ea7.g((List) pathSegments);
                if (str == null || (b2 = lh7.b(str)) == null) {
                    c(uri);
                    throw null;
                }
                int intValue = b2.intValue();
                Resources resourcesForApplication = this.a.getPackageManager().getResourcesForApplication(authority);
                TypedValue typedValue = new TypedValue();
                resourcesForApplication.getValue(intValue, typedValue, true);
                CharSequence charSequence = typedValue.string;
                ee7.a((Object) charSequence, "path");
                String obj = charSequence.subSequence(nh7.b(charSequence, '/', 0, false, 6, (Object) null), charSequence.length()).toString();
                MimeTypeMap singleton = MimeTypeMap.getSingleton();
                ee7.a((Object) singleton, "MimeTypeMap.getSingleton()");
                String a2 = iu.a(singleton, obj);
                if (ee7.a((Object) a2, (Object) "text/xml")) {
                    if (ee7.a((Object) authority, (Object) this.a.getPackageName())) {
                        bitmapDrawable = fu.a(this.a, intValue);
                    } else {
                        Context context = this.a;
                        ee7.a((Object) resourcesForApplication, "resources");
                        bitmapDrawable = fu.a(context, resourcesForApplication, intValue);
                    }
                    boolean a3 = iu.a(bitmapDrawable);
                    if (a3) {
                        Bitmap a4 = this.b.a(bitmapDrawable, rtVar, irVar.d());
                        Resources resources = this.a.getResources();
                        ee7.a((Object) resources, "context.resources");
                        bitmapDrawable = new BitmapDrawable(resources, a4);
                    }
                    return new nr(bitmapDrawable, a3, br.MEMORY);
                }
                InputStream openRawResource = resourcesForApplication.openRawResource(intValue);
                ee7.a((Object) openRawResource, "resources.openRawResource(resId)");
                return new vr(ir7.a(ir7.a(openRawResource)), a2, br.MEMORY);
            }
        }
        c(uri);
        throw null;
    }
}
