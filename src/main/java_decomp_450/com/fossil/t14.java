package com.fossil;

import com.fossil.o14;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t14 {
    @DexIgnore
    public Set<String> a; // = new HashSet();
    @DexIgnore
    public o14.b b;
    @DexIgnore
    public eb3 c;
    @DexIgnore
    public s14 d;

    @DexIgnore
    public t14(eb3 eb3, o14.b bVar) {
        this.b = bVar;
        this.c = eb3;
        s14 s14 = new s14(this);
        this.d = s14;
        this.c.a(s14);
    }
}
