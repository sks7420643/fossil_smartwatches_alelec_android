package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e70 extends b70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<e70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public e70 createFromParcel(Parcel parcel) {
            b70 createFromParcel = b70.CREATOR.createFromParcel(parcel);
            if (createFromParcel != null) {
                return (e70) createFromParcel;
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedReminder");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public e70[] newArray(int i) {
            return new e70[i];
        }
    }

    @DexIgnore
    public e70(d70 d70, f70 f70, x60 x60) {
        super(d70, f70, x60);
    }

    @DexIgnore
    @Override // com.fossil.t60
    public d70 getFireTime() {
        v60[] c = c();
        int length = c.length;
        int i = 0;
        while (i < length) {
            v60 v60 = c[i];
            if (!(v60 instanceof d70)) {
                i++;
            } else if (v60 != null) {
                return (d70) v60;
            } else {
                throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedFireTime");
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
