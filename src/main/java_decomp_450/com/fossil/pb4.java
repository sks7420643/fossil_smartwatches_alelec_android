package com.fossil;

import com.fossil.ac4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb4 extends ac4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    @Override // com.fossil.ac4
    public String a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ac4
    public long b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ac4
    public long c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ac4)) {
            return false;
        }
        ac4 ac4 = (ac4) obj;
        if (this.a.equals(ac4.a()) && this.b == ac4.c() && this.c == ac4.b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        long j2 = this.c;
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "InstallationTokenResult{token=" + this.a + ", tokenExpirationTimestamp=" + this.b + ", tokenCreationTimestamp=" + this.c + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ac4.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Long c;

        @DexIgnore
        @Override // com.fossil.ac4.a
        public ac4.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null token");
        }

        @DexIgnore
        @Override // com.fossil.ac4.a
        public ac4.a b(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ac4.a
        public ac4.a a(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ac4.a
        public ac4 a() {
            String str = "";
            if (this.a == null) {
                str = str + " token";
            }
            if (this.b == null) {
                str = str + " tokenExpirationTimestamp";
            }
            if (this.c == null) {
                str = str + " tokenCreationTimestamp";
            }
            if (str.isEmpty()) {
                return new pb4(this.a, this.b.longValue(), this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public pb4(String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }
}
