package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt1 extends wt1 {
    @DexIgnore
    public /* final */ long a;

    @DexIgnore
    public qt1(long j) {
        this.a = j;
    }

    @DexIgnore
    @Override // com.fossil.wt1
    public long a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof wt1) || this.a != ((wt1) obj).a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return 1000003 ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "LogResponse{nextRequestWaitMillis=" + this.a + "}";
    }
}
