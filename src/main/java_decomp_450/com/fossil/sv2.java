package com.fossil;

import com.fossil.sv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sv2<T extends sv2<T>> extends Comparable<T> {
    @DexIgnore
    mx2 a(mx2 mx2, jx2 jx2);

    @DexIgnore
    sx2 a(sx2 sx2, sx2 sx22);

    @DexIgnore
    int zza();

    @DexIgnore
    iz2 zzb();

    @DexIgnore
    pz2 zzc();

    @DexIgnore
    boolean zzd();

    @DexIgnore
    boolean zze();
}
