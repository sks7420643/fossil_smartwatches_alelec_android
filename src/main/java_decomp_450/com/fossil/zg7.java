package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg7 implements yg7 {
    @DexIgnore
    public /* final */ Matcher a;
    @DexIgnore
    public /* final */ CharSequence b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends l97<vg7> implements xg7 {
        @DexIgnore
        public /* final */ /* synthetic */ zg7 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zg7$a$a")
        /* renamed from: com.fossil.zg7$a$a  reason: collision with other inner class name */
        public static final class C0260a extends fe7 implements gd7<Integer, vg7> {
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0260a(a aVar) {
                super(1);
                this.this$0 = aVar;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ vg7 invoke(Integer num) {
                return invoke(num.intValue());
            }

            @DexIgnore
            public final vg7 invoke(int i) {
                return this.this$0.get(i);
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(zg7 zg7) {
            this.a = zg7;
        }

        @DexIgnore
        public /* bridge */ boolean a(vg7 vg7) {
            return super.contains(vg7);
        }

        @DexIgnore
        @Override // com.fossil.l97
        public final /* bridge */ boolean contains(Object obj) {
            if (obj != null ? obj instanceof vg7 : true) {
                return a((vg7) obj);
            }
            return false;
        }

        @DexIgnore
        public vg7 get(int i) {
            lf7 a2 = ch7.b(this.a.b(), i);
            if (a2.c().intValue() < 0) {
                return null;
            }
            String group = this.a.b().group(i);
            ee7.a((Object) group, "matchResult.group(index)");
            return new vg7(group, a2);
        }

        @DexIgnore
        @Override // com.fossil.l97
        public boolean isEmpty() {
            return false;
        }

        @DexIgnore
        @Override // java.util.Collection, java.lang.Iterable
        public Iterator<vg7> iterator() {
            return og7.c(ea7.b(w97.a((Collection<?>) this)), new C0260a(this)).iterator();
        }

        @DexIgnore
        @Override // com.fossil.l97
        public int a() {
            return this.a.b().groupCount() + 1;
        }
    }

    @DexIgnore
    public zg7(Matcher matcher, CharSequence charSequence) {
        ee7.b(matcher, "matcher");
        ee7.b(charSequence, "input");
        this.a = matcher;
        this.b = charSequence;
        new a(this);
    }

    @DexIgnore
    public final MatchResult b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.yg7
    public String getValue() {
        String group = b().group();
        ee7.a((Object) group, "matchResult.group()");
        return group;
    }

    @DexIgnore
    @Override // com.fossil.yg7
    public yg7 next() {
        int end = b().end() + (b().end() == b().start() ? 1 : 0);
        if (end > this.b.length()) {
            return null;
        }
        Matcher matcher = this.a.pattern().matcher(this.b);
        ee7.a((Object) matcher, "matcher.pattern().matcher(input)");
        return ch7.b(matcher, end, this.b);
    }

    @DexIgnore
    @Override // com.fossil.yg7
    public lf7 a() {
        return ch7.b(b());
    }
}
