package com.fossil;

import android.text.TextUtils;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.gson.AlarmDeserializer;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.gson.DianaRecommendPresetDeserializer;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.gson.HybridRecommendPresetDeserializer;
import com.portfolio.platform.gson.NotificationDeserializer;
import com.portfolio.platform.gson.ThemeDeserializer;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.joda.time.DateTime;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj5 {
    @DexIgnore
    public static /* final */ Retrofit.b a;
    @DexIgnore
    public static Retrofit b;
    @DexIgnore
    public static Cache c;
    @DexIgnore
    public static /* final */ ly6 d;
    @DexIgnore
    public static /* final */ OkHttpClient.b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public static /* final */ kj5 g; // = new kj5();

    /*
    static {
        OkHttpClient.b bVar;
        Retrofit.b bVar2 = new Retrofit.b();
        be4 be4 = new be4();
        be4.b(new od5());
        be4.a(new od5());
        be4.a(Date.class, new GsonConvertDate());
        be4.a(DianaPreset.class, new DianaPresetDeserializer());
        be4.a(DianaRecommendPreset.class, new DianaRecommendPresetDeserializer());
        be4.a(HybridPreset.class, new HybridPresetDeserializer());
        be4.a(HybridRecommendPreset.class, new HybridRecommendPresetDeserializer());
        be4.a(DateTime.class, new GsonConvertDateTime());
        be4.a(Alarm.class, new AlarmDeserializer());
        be4.a(Theme.class, new ThemeDeserializer());
        be4.a(ao4.class, new NotificationDeserializer());
        bVar2.a(GsonConverterFactory.a(be4.a()));
        a = bVar2;
        ly6 ly6 = new ly6();
        ly6.a(ee7.a("release", "release") ? HttpLoggingInterceptor.a.BASIC : HttpLoggingInterceptor.a.BODY);
        d = ly6;
        if (ee7.a((Object) "release", (Object) "release")) {
            bVar = new OkHttpClient.b();
            bVar.a(d);
        } else {
            bVar = new OkHttpClient.b();
            bVar.a(d);
            bVar.b(new StethoInterceptor());
        }
        e = bVar;
    }
    */

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "baseUrl");
        if (!TextUtils.equals(f, str)) {
            f = str;
            Retrofit.b bVar = a;
            if (str != null) {
                bVar.a(str);
                b = a.a();
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(on7 on7) {
        ee7.b(on7, "authenticator");
        e.a(on7);
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final void a(File file) {
        ee7.b(file, "cacheDir");
        if (c == null) {
            File file2 = new File(file.getAbsolutePath(), "cacheResponse");
            if (!file2.exists()) {
                file2.mkdir();
            }
            c = new Cache(file2, 10485760);
        }
        e.a(c);
    }

    @DexIgnore
    public final void a() {
        Cache cache = c;
        if (cache != null) {
            cache.a();
        }
    }

    @DexIgnore
    public final void a(Interceptor interceptor) {
        e.c(10, TimeUnit.SECONDS);
        e.b(10, TimeUnit.SECONDS);
        e.d(10, TimeUnit.SECONDS);
        e.b().clear();
        e.a(d);
        if (interceptor != null && !e.b().contains(interceptor)) {
            e.a(interceptor);
        }
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final <S> S a(Class<S> cls) {
        ee7.b(cls, "serviceClass");
        Retrofit retrofit3 = b;
        if (retrofit3 != null) {
            return (S) retrofit3.a(cls);
        }
        ee7.a();
        throw null;
    }
}
