package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kc1 extends qj1 {
    @DexIgnore
    public int A; // = 23;
    @DexIgnore
    public /* final */ int B;

    @DexIgnore
    public kc1(int i, ri1 ri1) {
        super(qa1.f, ri1);
        this.B = i;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        this.A = ((lb1) eo0).j;
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(new JSONObject(), r51.i1, Integer.valueOf(this.A)), 7));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.h1, Integer.valueOf(this.B));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.i1, Integer.valueOf(this.A));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new lb1(this.B, ((v81) this).y.w);
    }
}
