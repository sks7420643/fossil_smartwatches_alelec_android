package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p03 implements tr2<o03> {
    @DexIgnore
    public static p03 b; // = new p03();
    @DexIgnore
    public /* final */ tr2<o03> a;

    @DexIgnore
    public p03(tr2<o03> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((o03) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((o03) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((o03) b.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((o03) b.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((o03) b.zza()).zze();
    }

    @DexIgnore
    public static boolean f() {
        return ((o03) b.zza()).zzf();
    }

    @DexIgnore
    public static boolean g() {
        return ((o03) b.zza()).zzg();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ o03 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public p03() {
        this(sr2.a(new r03()));
    }
}
