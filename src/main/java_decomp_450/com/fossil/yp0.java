package com.fossil;

import android.os.Build;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp0 {
    @DexIgnore
    public static /* final */ r60 a; // = new r60((byte) 4, (byte) 0);
    @DexIgnore
    public static /* final */ wm0[] b; // = {wm0.b, wm0.y0, wm0.d, wm0.g, wm0.k, wm0.K, wm0.m, wm0.e, wm0.m0, wm0.l0, wm0.q0, wm0.r0, wm0.s0, wm0.t0, wm0.u0, wm0.q, wm0.s, wm0.r, wm0.t};
    @DexIgnore
    public static /* final */ Type[] c; // = {qc0.class, tc0.class};
    @DexIgnore
    public static /* final */ x91[] d; // = {new x91(12, 12, 30, 600), new x91(18, 18, 19, 600), new x91(24, 24, 14, 600), new x91(48, 48, 6, 600), new x91(72, 72, 4, 600)};
    @DexIgnore
    public static /* final */ x91[] e; // = {new x91(12, 12, 45, 600), new x91(24, 24, 22, 600), new x91(36, 36, 15, 600), new x91(104, 112, 4, 600)};
    @DexIgnore
    public static /* final */ yp0 f; // = new yp0();

    @DexIgnore
    public final long a(boolean z) {
        return z ? 60000 : 30000;
    }

    @DexIgnore
    public final boolean a(m60 m60, zk0 zk0) {
        if (f.b(m60)) {
            return t97.a(b, zk0.y);
        }
        return true;
    }

    @DexIgnore
    public final boolean b(m60 m60) {
        if (m60.getDeviceType().b()) {
            r60 r60 = m60.i().get(Short.valueOf(pb1.OTA.a));
            if (r60 == null) {
                r60 = b21.x.f();
            }
            if (r60.compareTo(b21.x.w()) < 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Type[] c() {
        Object[] array = w97.a((Object[]) new Type[]{pd0.class, cd0.class, hd0.class, jd0.class, mc0.class, ld0.class, rd0.class, pc0.class, qc0.class, rc0.class, tc0.class, ud0.class, uc0.class, vc0.class, xc0.class, ad0.class, bd0.class, vd0.class, ed0.class, fd0.class, xd0.class, gd0.class, de0.class, ge0.class, zd0.class, zc0.class, sc0.class, kd0.class, nd0.class, od0.class, wd0.class, qd0.class, sd0.class, ee0.class, fe0.class, be0.class, yc0.class, ce0.class, dd0.class, kc0.class, id0.class, lc0.class, yd0.class, md0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final r60 d() {
        return a;
    }

    @DexIgnore
    public final Type[] e() {
        Object[] array = w97.a((Object[]) new Type[]{cd0.class, hd0.class, jd0.class, mc0.class, ld0.class, pc0.class, qc0.class, rc0.class, tc0.class, uc0.class, vc0.class, xc0.class, bd0.class, ui0.class, rk0.class, om0.class, wc0.class, nc0.class, ed0.class, fd0.class, gd0.class, od0.class, wd0.class, qd0.class, sd0.class, fe0.class, kd0.class, ce0.class, kc0.class, id0.class, lc0.class, md0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final boolean f() {
        return false;
    }

    @DexIgnore
    public final Type[] g() {
        Object[] array = w97.a((Object[]) new Type[]{cd0.class, hd0.class, jd0.class, mc0.class, ld0.class, pc0.class, qc0.class, rc0.class, tc0.class, uc0.class, vc0.class, xc0.class, bd0.class, ui0.class, rk0.class, om0.class, wc0.class, nc0.class, ed0.class, fd0.class, gd0.class, od0.class, wd0.class, qd0.class, sd0.class, fe0.class, kd0.class, ce0.class, kc0.class, id0.class, lc0.class, md0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] h() {
        Object[] array = w97.a((Object[]) new Type[]{cd0.class, hd0.class, jd0.class, mc0.class, ld0.class, pc0.class, qc0.class, rc0.class, tc0.class, vc0.class, xc0.class, bd0.class, ui0.class, rk0.class, om0.class, wc0.class, nc0.class, ed0.class, fd0.class, gd0.class, wd0.class, qd0.class, sd0.class, fe0.class, kd0.class, ce0.class, kc0.class, id0.class, lc0.class, md0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] i() {
        return c;
    }

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT < 28;
    }

    @DexIgnore
    public final x91[] a(m60 m60) {
        switch (co0.a[m60.getDeviceType().ordinal()]) {
            case 1:
            case 2:
                return d;
            case 3:
            case 4:
            case 5:
            case 6:
                if (b(m60)) {
                    return new x91[0];
                }
                return e;
            case 7:
            case 8:
            case 9:
                return new x91[0];
            default:
                throw new p87();
        }
    }

    @DexIgnore
    public final Type[] b() {
        Object[] array = w97.a((Object[]) new Type[]{pd0.class, cd0.class, hd0.class, jd0.class, mc0.class, ld0.class, rd0.class, pc0.class, qc0.class, rc0.class, tc0.class, ud0.class, uc0.class, vc0.class, xc0.class, ad0.class, bd0.class, vd0.class, ed0.class, fd0.class, xd0.class, gd0.class, de0.class, ge0.class, zd0.class, zc0.class, sc0.class, kd0.class, nd0.class, od0.class, wd0.class, qd0.class, sd0.class, ee0.class, fe0.class, be0.class, yc0.class, ce0.class, dd0.class, kc0.class, id0.class, lc0.class, yd0.class, md0.class, ae0.class, td0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
