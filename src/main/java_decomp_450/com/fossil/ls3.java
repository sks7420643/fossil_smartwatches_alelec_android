package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.v1;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ls3 implements v1 {
    @DexIgnore
    public p1 a;
    @DexIgnore
    public BottomNavigationMenuView b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0113a();
        @DexIgnore
        public int a;
        @DexIgnore
        public gu3 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ls3$a$a")
        /* renamed from: com.fossil.ls3$a$a  reason: collision with other inner class name */
        public static class C0113a implements Parcelable.Creator<a> {
            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.b, 0);
        }

        @DexIgnore
        public a(Parcel parcel) {
            this.a = parcel.readInt();
            this.b = (gu3) parcel.readParcelable(a.class.getClassLoader());
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(p1 p1Var, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(v1.a aVar) {
    }

    @DexIgnore
    public void a(BottomNavigationMenuView bottomNavigationMenuView) {
        this.b = bottomNavigationMenuView;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(a2 a2Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    public void b(boolean z) {
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public Parcelable c() {
        a aVar = new a();
        aVar.a = this.b.getSelectedItemId();
        aVar.b = is3.a(this.b.getBadgeDrawables());
        return aVar;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public int getId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Context context, p1 p1Var) {
        this.a = p1Var;
        this.b.a(p1Var);
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(boolean z) {
        if (!this.c) {
            if (z) {
                this.b.a();
            } else {
                this.b.d();
            }
        }
    }

    @DexIgnore
    public void a(int i) {
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Parcelable parcelable) {
        if (parcelable instanceof a) {
            a aVar = (a) parcelable;
            this.b.c(aVar.a);
            this.b.setBadgeDrawables(is3.a(this.b.getContext(), aVar.b));
        }
    }
}
