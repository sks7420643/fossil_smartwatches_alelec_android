package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ta5 extends sa5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131362697, 1);
        D.put(2131362458, 2);
        D.put(2131363104, 3);
        D.put(2131362444, 4);
        D.put(2131362524, 5);
        D.put(2131361989, 6);
        D.put(2131363387, 7);
        D.put(2131362545, 8);
        D.put(2131362546, 9);
        D.put(2131362547, 10);
    }
    */

    @DexIgnore
    public ta5(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 11, C, D));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public ta5(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleCheckBox) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5], (Guideline) objArr[8], (Guideline) objArr[9], (Guideline) objArr[10], (RTLImageView) objArr[1], (FlexibleSwitchCompat) objArr[3], (View) objArr[7]);
        this.B = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.A = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
