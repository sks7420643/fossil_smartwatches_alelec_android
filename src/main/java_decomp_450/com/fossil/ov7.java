package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov7 implements tu7<mo7, Byte> {
    @DexIgnore
    public static /* final */ ov7 a; // = new ov7();

    @DexIgnore
    public Byte a(mo7 mo7) throws IOException {
        return Byte.valueOf(mo7.string());
    }
}
