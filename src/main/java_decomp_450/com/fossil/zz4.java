package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zz4 extends yz4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public /* final */ NestedScrollView B;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362088, 1);
        E.put(2131363314, 2);
        E.put(2131363423, 3);
        E.put(2131362678, 4);
        E.put(2131362098, 5);
        E.put(2131363322, 6);
        E.put(2131363444, 7);
        E.put(2131362680, 8);
        E.put(2131363312, 9);
        E.put(2131363313, 10);
        E.put(2131362245, 11);
    }
    */

    @DexIgnore
    public zz4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public zz4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[5], (FlexibleButton) objArr[11], (ImageView) objArr[4], (ImageView) objArr[8], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[6], (View) objArr[3], (View) objArr[7]);
        this.C = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.B = nestedScrollView;
        nestedScrollView.setTag(null);
        a(view);
        f();
    }
}
