package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sf0 {
    LOW((byte) 1),
    NORMAL((byte) 2);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final sf0 a(byte b) {
            sf0[] values = sf0.values();
            for (sf0 sf0 : values) {
                if (sf0.a() == b) {
                    return sf0;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public sf0(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
