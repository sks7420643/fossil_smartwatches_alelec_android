package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ View x;

    @DexIgnore
    public y85(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view2) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = flexibleTextView;
        this.u = constraintLayout3;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = view2;
    }

    @DexIgnore
    public static y85 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static y85 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (y85) ViewDataBinding.a(layoutInflater, 2131558671, viewGroup, z, obj);
    }
}
