package com.fossil;

import android.content.Context;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o63 {
    @DexIgnore
    public static boolean a;

    @DexIgnore
    public static synchronized int a(Context context) {
        synchronized (o63.class) {
            a72.a(context, "Context is null");
            if (a) {
                return 0;
            }
            try {
                l83 a2 = i83.a(context);
                try {
                    m63.a(a2.zze());
                    z83.a(a2.zzf());
                    a = true;
                    return 0;
                } catch (RemoteException e) {
                    throw new r93(e);
                }
            } catch (n02 e2) {
                return e2.errorCode;
            }
        }
    }
}
