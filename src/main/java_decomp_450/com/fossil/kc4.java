package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import com.facebook.GraphRequest;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.fossil.l94;
import com.fossil.lc4;
import com.fossil.mc4;
import com.fossil.xb4;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kc4 {
    @DexIgnore
    public static /* final */ Pattern d; // = Pattern.compile("[0-9]+s");
    @DexIgnore
    public static /* final */ Charset e; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ vd4 b;
    @DexIgnore
    public /* final */ l94 c;

    @DexIgnore
    public kc4(Context context, vd4 vd4, l94 l94) {
        this.a = context;
        this.b = vd4;
        this.c = l94;
    }

    @DexIgnore
    public static JSONObject b() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sdkVersion", "a:16.3.2");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(Constants.INSTALLTION, jSONObject);
            return jSONObject2;
        } catch (JSONException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String d(java.net.HttpURLConnection r7) {
        /*
            java.io.InputStream r0 = r7.getErrorStream()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.io.BufferedReader r2 = new java.io.BufferedReader
            java.io.InputStreamReader r3 = new java.io.InputStreamReader
            java.nio.charset.Charset r4 = com.fossil.kc4.e
            r3.<init>(r0, r4)
            r2.<init>(r3)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r0.<init>()     // Catch:{ IOException -> 0x004f, all -> 0x004a }
        L_0x0019:
            java.lang.String r3 = r2.readLine()     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            if (r3 == 0) goto L_0x0028
            r0.append(r3)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r3 = 10
            r0.append(r3)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            goto L_0x0019
        L_0x0028:
            java.lang.String r3 = "Error when communicating with the Firebase Installations server API. HTTP response: [%d %s: %s]"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r5 = 0
            int r6 = r7.getResponseCode()     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r4[r5] = r6     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r5 = 1
            java.lang.String r7 = r7.getResponseMessage()     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r4[r5] = r7     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r7 = 2
            r4[r7] = r0     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            java.lang.String r7 = java.lang.String.format(r3, r4)     // Catch:{ IOException -> 0x004f, all -> 0x004a }
            r2.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0049:
            return r7
        L_0x004a:
            r7 = move-exception
            r2.close()     // Catch:{ IOException -> 0x004e }
        L_0x004e:
            throw r7
        L_0x004f:
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0052:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kc4.d(java.net.HttpURLConnection):java.lang.String");
    }

    @DexIgnore
    public lc4 a(String str, String str2, String str3, String str4, String str5) throws IOException {
        int i = 0;
        URL url = new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", String.format("projects/%s/installations", str3)));
        while (i <= 1) {
            HttpURLConnection a2 = a(url, str);
            try {
                a2.setRequestMethod("POST");
                a2.setDoOutput(true);
                if (str5 != null) {
                    a2.addRequestProperty("x-goog-fis-android-iid-migration-auth", str5);
                }
                a(a2, str2, str4);
                int responseCode = a2.getResponseCode();
                if (responseCode == 200) {
                    lc4 a3 = a(a2);
                    a2.disconnect();
                    return a3;
                }
                a(a2, str4, str, str3);
                if (responseCode == 429 || (responseCode >= 500 && responseCode < 600)) {
                    i++;
                    a2.disconnect();
                } else {
                    c();
                    lc4.a f = lc4.f();
                    f.a(lc4.b.BAD_CONFIG);
                    return f.a();
                }
            } finally {
                a2.disconnect();
            }
        }
        throw new IOException();
    }

    @DexIgnore
    public final void c(HttpURLConnection httpURLConnection) throws IOException {
        a(httpURLConnection, a(b()));
    }

    @DexIgnore
    public static void c() {
        Log.e("Firebase-Installations", "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase.");
    }

    @DexIgnore
    public mc4 b(String str, String str2, String str3, String str4) throws IOException {
        int i = 0;
        URL url = new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", String.format("projects/%s/installations/%s/authTokens:generate", str3, str2)));
        while (i <= 1) {
            HttpURLConnection a2 = a(url, str);
            try {
                a2.setRequestMethod("POST");
                a2.addRequestProperty("Authorization", "FIS_v2 " + str4);
                c(a2);
                int responseCode = a2.getResponseCode();
                if (responseCode == 200) {
                    mc4 b2 = b(a2);
                    a2.disconnect();
                    return b2;
                }
                a(a2, (String) null, str, str3);
                if (responseCode == 401 || responseCode == 404) {
                    mc4.a d2 = mc4.d();
                    d2.a(mc4.b.AUTH_ERROR);
                    mc4 a3 = d2.a();
                    a2.disconnect();
                    return a3;
                } else if (responseCode == 429 || (responseCode >= 500 && responseCode < 600)) {
                    i++;
                    a2.disconnect();
                } else {
                    c();
                    mc4.a d3 = mc4.d();
                    d3.a(mc4.b.BAD_CONFIG);
                    return d3.a();
                }
            } finally {
                a2.disconnect();
            }
        }
        throw new IOException();
    }

    @DexIgnore
    public final void a(HttpURLConnection httpURLConnection, String str, String str2) throws IOException {
        a(httpURLConnection, a(a(str, str2)));
    }

    @DexIgnore
    public static byte[] a(JSONObject jSONObject) throws IOException {
        return jSONObject.toString().getBytes("UTF-8");
    }

    @DexIgnore
    public static void a(URLConnection uRLConnection, byte[] bArr) throws IOException {
        OutputStream outputStream = uRLConnection.getOutputStream();
        if (outputStream != null) {
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
            try {
                gZIPOutputStream.write(bArr);
            } finally {
                try {
                    gZIPOutputStream.close();
                    outputStream.close();
                } catch (IOException unused) {
                }
            }
        } else {
            throw new IOException("Cannot send request to FIS servers. No OutputStream available.");
        }
    }

    @DexIgnore
    public final mc4 b(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, e));
        mc4.a d2 = mc4.d();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("token")) {
                d2.a(jsonReader.nextString());
            } else if (nextName.equals("expiresIn")) {
                d2.a(a(jsonReader.nextString()));
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        d2.a(mc4.b.OK);
        return d2.a();
    }

    @DexIgnore
    public static JSONObject a(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("fid", str);
            jSONObject.put("appId", str2);
            jSONObject.put("authVersion", "FIS_v2");
            jSONObject.put("sdkVersion", "a:16.3.2");
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3, String str4) throws m14, IOException {
        int i = 0;
        URL url = new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", String.format("projects/%s/installations/%s", str3, str2)));
        while (i <= 1) {
            HttpURLConnection a2 = a(url, str);
            try {
                a2.setRequestMethod("DELETE");
                a2.addRequestProperty("Authorization", "FIS_v2 " + str4);
                int responseCode = a2.getResponseCode();
                if (!(responseCode == 200 || responseCode == 401)) {
                    if (responseCode != 404) {
                        a(a2, (String) null, str, str3);
                        if (responseCode != 429) {
                            if (responseCode < 500 || responseCode >= 600) {
                                c();
                                throw new xb4("Bad config while trying to delete FID", xb4.a.BAD_CONFIG);
                            }
                        }
                        i++;
                    }
                }
                a2.disconnect();
                return;
            } finally {
                a2.disconnect();
            }
        }
        throw new IOException();
    }

    @DexIgnore
    public final HttpURLConnection a(URL url, String str) throws IOException {
        l94.a a2;
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.addRequestProperty("Content-Type", com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        httpURLConnection.addRequestProperty(com.zendesk.sdk.network.Constants.ACCEPT_HEADER, com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        httpURLConnection.addRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
        httpURLConnection.addRequestProperty(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, "no-cache");
        httpURLConnection.addRequestProperty("X-Android-Package", this.a.getPackageName());
        l94 l94 = this.c;
        if (!(l94 == null || this.b == null || (a2 = l94.a("fire-installations-id")) == l94.a.NONE)) {
            httpURLConnection.addRequestProperty("x-firebase-client", this.b.a());
            httpURLConnection.addRequestProperty("x-firebase-client-log-type", Integer.toString(a2.getCode()));
        }
        httpURLConnection.addRequestProperty("X-Android-Cert", a());
        httpURLConnection.addRequestProperty("x-goog-api-key", str);
        return httpURLConnection;
    }

    @DexIgnore
    public final lc4 a(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, e));
        mc4.a d2 = mc4.d();
        lc4.a f = lc4.f();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("name")) {
                f.c(jsonReader.nextString());
            } else if (nextName.equals("fid")) {
                f.a(jsonReader.nextString());
            } else if (nextName.equals(Constants.PROFILE_KEY_REFRESH_TOKEN)) {
                f.b(jsonReader.nextString());
            } else if (nextName.equals("authToken")) {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    String nextName2 = jsonReader.nextName();
                    if (nextName2.equals("token")) {
                        d2.a(jsonReader.nextString());
                    } else if (nextName2.equals("expiresIn")) {
                        d2.a(a(jsonReader.nextString()));
                    } else {
                        jsonReader.skipValue();
                    }
                }
                f.a(d2.a());
                jsonReader.endObject();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        f.a(lc4.b.OK);
        return f.a();
    }

    @DexIgnore
    public final String a() {
        try {
            byte[] a2 = j92.a(this.a, this.a.getPackageName());
            if (a2 != null) {
                return s92.a(a2, false);
            }
            Log.e("ContentValues", "Could not get fingerprint hash for package: " + this.a.getPackageName());
            return null;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("ContentValues", "No such package: " + this.a.getPackageName(), e2);
            return null;
        }
    }

    @DexIgnore
    public static long a(String str) {
        a72.a(d.matcher(str).matches(), "Invalid Expiration Timestamp.");
        if (str == null || str.length() == 0) {
            return 0;
        }
        return Long.parseLong(str.substring(0, str.length() - 1));
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, String str, String str2, String str3) {
        String d2 = d(httpURLConnection);
        if (!TextUtils.isEmpty(d2)) {
            Log.w("Firebase-Installations", d2);
            Log.w("Firebase-Installations", a(str, str2, str3));
        }
    }

    @DexIgnore
    public static String a(String str, String str2, String str3) {
        String str4;
        Object[] objArr = new Object[3];
        objArr[0] = str2;
        objArr[1] = str3;
        if (TextUtils.isEmpty(str)) {
            str4 = "";
        } else {
            str4 = ", " + str;
        }
        objArr[2] = str4;
        return String.format("Firebase options used while communicating with Firebase server APIs: %s, %s%s", objArr);
    }
}
