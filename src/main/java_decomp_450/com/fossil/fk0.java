package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import java.security.KeyPair;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.fossil.blesdk.utils.TextEncryption$loadAsymmetricSecretKey$1", f = "TextEncryption.kt", l = {}, m = "invokeSuspend")
public final class fk0 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public int b;

    @DexIgnore
    public fk0(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        fk0 fk0 = new fk0(fb7);
        fk0.a = (yi7) obj;
        return fk0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((fk0) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        SharedPreferences a2;
        Context a3;
        nb7.a();
        if (this.b == 0) {
            t87.a(obj);
            yn0 yn0 = yn0.h;
            synchronized (yn0.e) {
                yn0 yn02 = yn0.h;
                if (yn0.d == null) {
                    KeyPair a4 = mt0.b.a("a");
                    byte[] bArr = null;
                    if (a4 == null) {
                        a4 = (Build.VERSION.SDK_INT >= 23 || (a3 = u31.g.a()) == null) ? null : mt0.b.a(a3, "a");
                    }
                    if (!(a4 == null || (a2 = yz0.a(j91.f)) == null)) {
                        String string = a2.getString("b", null);
                        if (string != null) {
                            bArr = Base64.decode(string, 0);
                        }
                        if (bArr == null) {
                            SecureRandom secureRandom = new SecureRandom();
                            KeyGenerator instance = KeyGenerator.getInstance("AES");
                            instance.init(256, secureRandom);
                            SecretKey generateKey = instance.generateKey();
                            ee7.a((Object) generateKey, "keyGenerator.generateKey()");
                            byte[] encoded = generateKey.getEncoded();
                            Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                            instance2.init(1, a4.getPublic());
                            bArr = instance2.doFinal(encoded);
                            a2.edit().putString("b", Base64.encodeToString(bArr, 0)).apply();
                        }
                        Cipher instance3 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                        instance3.init(2, a4.getPrivate());
                        yn0 yn03 = yn0.h;
                        yn0.d = instance3.doFinal(bArr);
                    }
                }
                i97 i97 = i97.a;
            }
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
