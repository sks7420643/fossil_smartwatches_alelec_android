package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vv0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ym1(vv0 vv0) {
        super(1);
        this.a = vv0;
    }

    @DexIgnore
    public final void a(v81 v81) {
        xj1 xj1 = (xj1) v81;
        vv0 vv0 = this.a;
        byte[] bArr = vv0.N;
        long j = xj1.N;
        byte[] a2 = s97.a(bArr, (int) j, (int) Math.min(vv0.D, j + xj1.O));
        vv0 vv02 = this.a;
        long j2 = xj1.N;
        short s = vv02.P;
        ri1 ri1 = ((zk0) vv02).w;
        zk0.a(vv02, new up1(s, new qz0(a2, Math.min(ri1.j, ri1.p), qk1.FTD), ((zk0) vv02).w), new vk0(vv02, a2), new sm0(vv02), new xi0(vv02, a2, j2), (gd7) null, (gd7) null, 48, (Object) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(v81 v81) {
        a(v81);
        return i97.a;
    }
}
