package com.fossil;

import com.google.maps.model.TravelMode;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum CAR can be incorrect */
public enum bb5 {
    CAR(r2, r3);
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ TravelMode travelMode;
    @DexIgnore
    public /* final */ String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final TravelMode a(String str) {
            bb5 bb5;
            TravelMode travelMode;
            ee7.b(str, "type");
            bb5[] values = bb5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    bb5 = null;
                    break;
                }
                bb5 = values[i];
                if (ee7.a((Object) bb5.getType(), (Object) str)) {
                    break;
                }
                i++;
            }
            return (bb5 == null || (travelMode = bb5.getTravelMode()) == null) ? bb5.CAR.getTravelMode() : travelMode;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        TravelMode travelMode2 = TravelMode.DRIVING;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887278);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026nstance, R.string.by_car)");
    }
    */

    @DexIgnore
    public bb5(TravelMode travelMode2, String str) {
        this.travelMode = travelMode2;
        this.type = str;
    }

    @DexIgnore
    public static final TravelMode getTravelModeFromType(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public final TravelMode getTravelMode() {
        return this.travelMode;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }
}
