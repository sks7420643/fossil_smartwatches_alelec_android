package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e84 extends m34 {
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public e84(String str, String str2, m64 m64, k64 k64, String str3) {
        super(str, str2, m64, k64);
        this.f = str3;
    }

    @DexIgnore
    public boolean a(x74 x74, boolean z) {
        if (z) {
            l64 a = a();
            a(a, x74);
            b(a, x74);
            z24 a2 = z24.a();
            a2.a("Sending app info to " + b());
            try {
                n64 b = a.b();
                int b2 = b.b();
                String str = "POST".equalsIgnoreCase(a.d()) ? "Create" : "Update";
                z24 a3 = z24.a();
                a3.a(str + " app request ID: " + b.a("X-REQUEST-ID"));
                z24 a4 = z24.a();
                a4.a("Result was " + b2);
                return p44.a(b2) == 0;
            } catch (IOException e) {
                z24.a().b("HTTP request failed.", e);
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public final l64 b(l64 l64, x74 x74) {
        l64.b("org_id", x74.a);
        l64.b("app[identifier]", x74.c);
        l64.b("app[name]", x74.g);
        l64.b("app[display_version]", x74.d);
        l64.b("app[build_version]", x74.e);
        l64.b("app[source]", Integer.toString(x74.h));
        l64.b("app[minimum_sdk_version]", x74.i);
        l64.b("app[built_sdk_version]", x74.j);
        if (!t34.b(x74.f)) {
            l64.b("app[instance_identifier]", x74.f);
        }
        return l64;
    }

    @DexIgnore
    public final l64 a(l64 l64, x74 x74) {
        l64.a("X-CRASHLYTICS-ORG-ID", x74.a);
        l64.a("X-CRASHLYTICS-GOOGLE-APP-ID", x74.b);
        l64.a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        l64.a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
        return l64;
    }
}
