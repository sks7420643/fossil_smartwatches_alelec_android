package com.fossil;

import com.fossil.jj7;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wj7 extends xj7 implements jj7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e; // = AtomicReferenceFieldUpdater.newUpdater(wj7.class, Object.class, "_queue");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater f; // = AtomicReferenceFieldUpdater.newUpdater(wj7.class, Object.class, "_delayed");
    @DexIgnore
    public volatile Object _delayed; // = null;
    @DexIgnore
    public volatile int _isCompleted; // = 0;
    @DexIgnore
    public volatile Object _queue; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends c {
        @DexIgnore
        public /* final */ ai7<i97> d;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.ai7<? super com.fossil.i97> */
        /* JADX WARN: Multi-variable type inference failed */
        public a(long j, ai7<? super i97> ai7) {
            super(j);
            this.d = ai7;
        }

        @DexIgnore
        public void run() {
            this.d.a(wj7.this, i97.a);
        }

        @DexIgnore
        @Override // com.fossil.wj7.c
        public String toString() {
            return super.toString() + this.d.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c {
        @DexIgnore
        public /* final */ Runnable d;

        @DexIgnore
        public b(long j, Runnable runnable) {
            super(j);
            this.d = runnable;
        }

        @DexIgnore
        public void run() {
            this.d.run();
        }

        @DexIgnore
        @Override // com.fossil.wj7.c
        public String toString() {
            return super.toString() + this.d.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c implements Runnable, Comparable<c>, rj7, rm7 {
        @DexIgnore
        public Object a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public long c;

        @DexIgnore
        public c(long j) {
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.rm7
        public qm7<?> a() {
            Object obj = this.a;
            if (!(obj instanceof qm7)) {
                obj = null;
            }
            return (qm7) obj;
        }

        @DexIgnore
        @Override // com.fossil.rm7
        public int b() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.rj7
        public final synchronized void dispose() {
            Object obj = this.a;
            if (obj != zj7.a) {
                if (!(obj instanceof d)) {
                    obj = null;
                }
                d dVar = (d) obj;
                if (dVar != null) {
                    dVar.b(this);
                }
                this.a = zj7.a;
            }
        }

        @DexIgnore
        public String toString() {
            return "Delayed[nanos=" + this.c + ']';
        }

        @DexIgnore
        @Override // com.fossil.rm7
        public void a(qm7<?> qm7) {
            if (this.a != zj7.a) {
                this.a = qm7;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @DexIgnore
        @Override // com.fossil.rm7
        public void a(int i) {
            this.b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i = ((this.c - cVar.c) > 0 ? 1 : ((this.c - cVar.c) == 0 ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @DexIgnore
        public final boolean a(long j) {
            return j - this.c >= 0;
        }

        @DexIgnore
        public final synchronized int a(long j, d dVar, wj7 wj7) {
            if (this.a == zj7.a) {
                return 2;
            }
            synchronized (dVar) {
                c cVar = (c) dVar.a();
                if (wj7.y()) {
                    return 1;
                }
                if (cVar == null) {
                    dVar.b = j;
                } else {
                    long j2 = cVar.c;
                    if (j2 - j < 0) {
                        j = j2;
                    }
                    if (j - dVar.b > 0) {
                        dVar.b = j;
                    }
                }
                if (this.c - dVar.b < 0) {
                    this.c = dVar.b;
                }
                dVar.a(this);
                return 0;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qm7<c> {
        @DexIgnore
        public long b;

        @DexIgnore
        public d(long j) {
            this.b = j;
        }
    }

    @DexIgnore
    public final void A() {
        c cVar;
        gl7 a2 = hl7.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        while (true) {
            d dVar = (d) this._delayed;
            if (dVar != null && (cVar = (c) dVar.f()) != null) {
                a(a3, cVar);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void B() {
        this._queue = null;
        this._delayed = null;
    }

    @DexIgnore
    public final rj7 b(long j, Runnable runnable) {
        long a2 = zj7.a(j);
        if (a2 >= 4611686018427387903L) {
            return vk7.a;
        }
        gl7 a3 = hl7.a();
        long a4 = a3 != null ? a3.a() : System.nanoTime();
        b bVar = new b(a2 + a4, runnable);
        b(a4, (c) bVar);
        return bVar;
    }

    @DexIgnore
    public final int c(long j, c cVar) {
        if (y()) {
            return 1;
        }
        d dVar = (d) this._delayed;
        if (dVar == null) {
            f.compareAndSet(this, null, new d(j));
            Object obj = this._delayed;
            if (obj != null) {
                dVar = (d) obj;
            } else {
                ee7.a();
                throw null;
            }
        }
        return cVar.a(j, dVar, this);
    }

    @DexIgnore
    public final void d(boolean z) {
        this._isCompleted = z ? 1 : 0;
    }

    @DexIgnore
    @Override // com.fossil.vj7
    public long g() {
        c cVar;
        if (super.g() == 0) {
            return 0;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof dm7) {
                if (!((dm7) obj).c()) {
                    return 0;
                }
            } else if (obj == zj7.b) {
                return Long.MAX_VALUE;
            } else {
                return 0;
            }
        }
        d dVar = (d) this._delayed;
        if (dVar == null || (cVar = (c) dVar.d()) == null) {
            return Long.MAX_VALUE;
        }
        long j = cVar.c;
        gl7 a2 = hl7.a();
        return qf7.a(j - (a2 != null ? a2.a() : System.nanoTime()), 0L);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0055  */
    @Override // com.fossil.vj7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long n() {
        /*
            r7 = this;
            boolean r0 = r7.o()
            if (r0 == 0) goto L_0x000b
            long r0 = r7.g()
            return r0
        L_0x000b:
            java.lang.Object r0 = r7._delayed
            com.fossil.wj7$d r0 = (com.fossil.wj7.d) r0
            if (r0 == 0) goto L_0x004f
            boolean r1 = r0.c()
            if (r1 != 0) goto L_0x004f
            com.fossil.gl7 r1 = com.fossil.hl7.a()
            if (r1 == 0) goto L_0x0022
            long r1 = r1.a()
            goto L_0x0026
        L_0x0022:
            long r1 = java.lang.System.nanoTime()
        L_0x0026:
            monitor-enter(r0)
            com.fossil.rm7 r3 = r0.a()     // Catch:{ all -> 0x004c }
            r4 = 0
            if (r3 == 0) goto L_0x0046
            com.fossil.wj7$c r3 = (com.fossil.wj7.c) r3     // Catch:{ all -> 0x004c }
            boolean r5 = r3.a(r1)     // Catch:{ all -> 0x004c }
            r6 = 0
            if (r5 == 0) goto L_0x003c
            boolean r3 = r7.b(r3)     // Catch:{ all -> 0x004c }
            goto L_0x003d
        L_0x003c:
            r3 = 0
        L_0x003d:
            if (r3 == 0) goto L_0x0044
            com.fossil.rm7 r3 = r0.a(r6)     // Catch:{ all -> 0x004c }
            r4 = r3
        L_0x0044:
            monitor-exit(r0)
            goto L_0x0047
        L_0x0046:
            monitor-exit(r0)
        L_0x0047:
            com.fossil.wj7$c r4 = (com.fossil.wj7.c) r4
            if (r4 == 0) goto L_0x004f
            goto L_0x0026
        L_0x004c:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x004f:
            java.lang.Runnable r0 = r7.x()
            if (r0 == 0) goto L_0x0058
            r0.run()
        L_0x0058:
            long r0 = r7.g()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wj7.n():long");
    }

    @DexIgnore
    @Override // com.fossil.vj7
    public void shutdown() {
        fl7.b.c();
        d(true);
        w();
        do {
        } while (n() <= 0);
        A();
    }

    @DexIgnore
    public final void w() {
        if (!dj7.a() || y()) {
            while (true) {
                Object obj = this._queue;
                if (obj == null) {
                    if (e.compareAndSet(this, null, zj7.b)) {
                        return;
                    }
                } else if (obj instanceof dm7) {
                    ((dm7) obj).a();
                    return;
                } else if (obj != zj7.b) {
                    dm7 dm7 = new dm7(8, true);
                    if (obj != null) {
                        dm7.a((Runnable) obj);
                        if (e.compareAndSet(this, obj, dm7)) {
                            return;
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Runnable x() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof dm7) {
                if (obj != null) {
                    dm7 dm7 = (dm7) obj;
                    Object f2 = dm7.f();
                    if (f2 != dm7.g) {
                        return (Runnable) f2;
                    }
                    e.compareAndSet(this, obj, dm7.e());
                } else {
                    throw new x87("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == zj7.b) {
                return null;
            } else {
                if (e.compareAndSet(this, obj, null)) {
                    if (obj != null) {
                        return (Runnable) obj;
                    }
                    throw new x87("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final boolean y() {
        return this._isCompleted;
    }

    @DexIgnore
    public boolean z() {
        if (!l()) {
            return false;
        }
        d dVar = (d) this._delayed;
        if (dVar != null && !dVar.c()) {
            return false;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof dm7) {
                return ((dm7) obj).c();
            }
            if (obj == zj7.b) {
                return true;
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.jj7
    public rj7 a(long j, Runnable runnable) {
        return jj7.a.a(this, j, runnable);
    }

    @DexIgnore
    @Override // com.fossil.jj7
    public void a(long j, ai7<? super i97> ai7) {
        long a2 = zj7.a(j);
        if (a2 < 4611686018427387903L) {
            gl7 a3 = hl7.a();
            long a4 = a3 != null ? a3.a() : System.nanoTime();
            a aVar = new a(a2 + a4, ai7);
            di7.a(ai7, aVar);
            b(a4, (c) aVar);
        }
    }

    @DexIgnore
    public final void b(long j, c cVar) {
        int c2 = c(j, cVar);
        if (c2 != 0) {
            if (c2 == 1) {
                a(j, cVar);
            } else if (c2 != 2) {
                throw new IllegalStateException("unexpected result".toString());
            }
        } else if (a(cVar)) {
            v();
        }
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public final void a(ib7 ib7, Runnable runnable) {
        a(runnable);
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        if (b(runnable)) {
            v();
        } else {
            fj7.h.a(runnable);
        }
    }

    @DexIgnore
    public final boolean b(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (y()) {
                return false;
            }
            if (obj == null) {
                if (e.compareAndSet(this, null, runnable)) {
                    return true;
                }
            } else if (obj instanceof dm7) {
                if (obj != null) {
                    dm7 dm7 = (dm7) obj;
                    int a2 = dm7.a(runnable);
                    if (a2 == 0) {
                        return true;
                    }
                    if (a2 == 1) {
                        e.compareAndSet(this, obj, dm7.e());
                    } else if (a2 == 2) {
                        return false;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == zj7.b) {
                return false;
            } else {
                dm7 dm72 = new dm7(8, true);
                if (obj != null) {
                    dm72.a((Runnable) obj);
                    dm72.a(runnable);
                    if (e.compareAndSet(this, obj, dm72)) {
                        return true;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(c cVar) {
        d dVar = (d) this._delayed;
        return (dVar != null ? (c) dVar.d() : null) == cVar;
    }
}
