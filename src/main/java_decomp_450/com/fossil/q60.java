package com.fossil;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q60<V, E> {
    @DexIgnore
    public V a;
    @DexIgnore
    public E b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<V, i97>> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<gd7<E, i97>> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<gd7<i97, i97>> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<gd7<Float, i97>> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<gd7<E, i97>> h; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public final boolean a() {
        return c() || b();
    }

    @DexIgnore
    public final boolean b() {
        return this.b != null;
    }

    @DexIgnore
    public final boolean c() {
        return this.a != null;
    }

    @DexIgnore
    public final synchronized void d() {
        Iterator<T> it = this.f.iterator();
        while (it.hasNext()) {
            try {
                it.next().invoke(i97.a);
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h.clear();
    }

    @DexIgnore
    public q60<V, E> a(gd7<? super E, i97> gd7) {
        if (!a()) {
            this.h.add(gd7);
        }
        return this;
    }

    @DexIgnore
    public q60<V, E> b(gd7<? super E, i97> gd7) {
        if (!a()) {
            this.e.add(gd7);
        } else if (b()) {
            try {
                E e2 = this.b;
                if (e2 != null) {
                    gd7.invoke(e2);
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception e3) {
                wl0.h.a(e3);
            }
        }
        return this;
    }

    @DexIgnore
    public q60<V, E> c(gd7<? super V, i97> gd7) {
        if (!a()) {
            this.d.add(gd7);
        } else if (c()) {
            try {
                V v = this.a;
                if (v != null) {
                    gd7.invoke(v);
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
        return this;
    }

    @DexIgnore
    public final synchronized void a(float f2) {
        Iterator<T> it = this.g.iterator();
        while (it.hasNext()) {
            try {
                it.next().invoke(Float.valueOf(f2));
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final synchronized void b(E e2) {
        if (!a()) {
            this.b = e2;
            for (T t : this.e) {
                try {
                    E e3 = this.b;
                    if (e3 != null) {
                        t.invoke(e3);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } catch (Exception e4) {
                    wl0.h.a(e4);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void c(V v) {
        if (!a()) {
            this.a = v;
            for (T t : this.d) {
                try {
                    V v2 = this.a;
                    if (v2 != null) {
                        t.invoke(v2);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } catch (Exception e2) {
                    wl0.h.a(e2);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void a(E e2) {
        if (!a() && !this.c) {
            this.c = true;
            if (true ^ this.h.isEmpty()) {
                Iterator<T> it = this.h.iterator();
                while (it.hasNext()) {
                    try {
                        it.next().invoke(e2);
                    } catch (Exception e3) {
                        wl0.h.a(e3);
                    }
                }
                this.c = false;
            } else {
                b((Object) e2);
            }
        }
    }
}
