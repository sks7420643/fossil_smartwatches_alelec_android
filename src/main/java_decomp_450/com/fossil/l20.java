package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l20<T extends Drawable> implements uy<T>, qy {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public l20(T t) {
        u50.a(t);
        this.a = t;
    }

    @DexIgnore
    @Override // com.fossil.qy
    public void a() {
        T t = this.a;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof t20) {
            ((t20) t).e().prepareToDraw();
        }
    }

    @DexIgnore
    @Override // com.fossil.uy
    public final T get() {
        Drawable.ConstantState constantState = this.a.getConstantState();
        return constantState == null ? this.a : (T) constantState.newDrawable();
    }
}
