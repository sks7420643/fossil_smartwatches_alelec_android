package com.fossil;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ej3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ ti3 b;

    @DexIgnore
    public ej3(ti3 ti3, AtomicReference atomicReference) {
        this.b = ti3;
        this.a = atomicReference;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                this.a.set(this.b.l().j(this.b.p().A()));
                this.a.notify();
            } catch (Throwable th) {
                this.a.notify();
                throw th;
            }
        }
    }
}
