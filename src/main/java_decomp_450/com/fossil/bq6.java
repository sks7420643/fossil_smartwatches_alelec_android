package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.fl4;
import com.fossil.nj5;
import com.fossil.pm5;
import com.fossil.tl5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq6 extends xp6 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Device g;
    @DexIgnore
    public if5 h;
    @DexIgnore
    public /* final */ e i; // = new e(this);
    @DexIgnore
    public /* final */ d j; // = new d(this);
    @DexIgnore
    public /* final */ yp6 k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ ad5 m;
    @DexIgnore
    public /* final */ ch5 n;
    @DexIgnore
    public /* final */ pm5 o;
    @DexIgnore
    public /* final */ tl5 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bq6.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<tl5.d, tl5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ bq6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ Device c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1", f = "UpdateFirmwarePresenter.kt", l = {214, 217}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ tl5.d $responseValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bq6$b$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.bq6$b$a$a  reason: collision with other inner class name */
            public static final class C0018a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0018a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0018a aVar = new C0018a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                    return ((C0018a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return pb7.a(yw6.h.a().b(this.this$0.this$0.b));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bq6$b$a$b")
            @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1", f = "UpdateFirmwarePresenter.kt", l = {217}, m = "invokeSuspend")
            /* renamed from: com.fossil.bq6$b$a$b  reason: collision with other inner class name */
            public static final class C0019b extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0019b(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0019b bVar = new C0019b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                    return ((C0019b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        yw6 a2 = yw6.h.a();
                        b bVar = this.this$0.this$0;
                        String str = bVar.b;
                        Device device = bVar.c;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = a2.a(str, device, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, tl5.d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$responseValue = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$responseValue, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x00a4  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00b0  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                    r10 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r10.label
                    r2 = 0
                    java.lang.String r3 = "checkFirmware - downloadFw SUCCESS, latestFwVersion="
                    r4 = 2
                    r5 = 1
                    if (r1 == 0) goto L_0x0032
                    if (r1 == r5) goto L_0x0026
                    if (r1 != r4) goto L_0x001e
                    java.lang.Object r0 = r10.L$1
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r10.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r11)
                    goto L_0x009c
                L_0x001e:
                    java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r11.<init>(r0)
                    throw r11
                L_0x0026:
                    java.lang.Object r1 = r10.L$1
                    java.lang.String r1 = (java.lang.String) r1
                    java.lang.Object r5 = r10.L$0
                    com.fossil.yi7 r5 = (com.fossil.yi7) r5
                    com.fossil.t87.a(r11)
                    goto L_0x0078
                L_0x0032:
                    com.fossil.t87.a(r11)
                    com.fossil.yi7 r11 = r10.p$
                    com.fossil.tl5$d r1 = r10.$responseValue
                    java.lang.String r1 = r1.a()
                    com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                    com.fossil.bq6$a r7 = com.fossil.bq6.r
                    java.lang.String r7 = r7.a()
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder
                    r8.<init>()
                    r8.append(r3)
                    r8.append(r1)
                    java.lang.String r8 = r8.toString()
                    r6.d(r7, r8)
                    com.fossil.bq6$b r6 = r10.this$0
                    com.fossil.bq6 r6 = r6.a
                    com.fossil.ti7 r6 = r6.b()
                    com.fossil.bq6$b$a$a r7 = new com.fossil.bq6$b$a$a
                    r7.<init>(r10, r2)
                    r10.L$0 = r11
                    r10.L$1 = r1
                    r10.label = r5
                    java.lang.Object r5 = com.fossil.vh7.a(r6, r7, r10)
                    if (r5 != r0) goto L_0x0075
                    return r0
                L_0x0075:
                    r9 = r5
                    r5 = r11
                    r11 = r9
                L_0x0078:
                    java.lang.Boolean r11 = (java.lang.Boolean) r11
                    boolean r11 = r11.booleanValue()
                    if (r11 != 0) goto L_0x00ba
                    com.fossil.bq6$b r3 = r10.this$0
                    com.fossil.bq6 r3 = r3.a
                    com.fossil.ti7 r3 = r3.b()
                    com.fossil.bq6$b$a$b r6 = new com.fossil.bq6$b$a$b
                    r6.<init>(r10, r2)
                    r10.L$0 = r5
                    r10.L$1 = r1
                    r10.Z$0 = r11
                    r10.label = r4
                    java.lang.Object r11 = com.fossil.vh7.a(r3, r6, r10)
                    if (r11 != r0) goto L_0x009c
                    return r0
                L_0x009c:
                    java.lang.Boolean r11 = (java.lang.Boolean) r11
                    boolean r11 = r11.booleanValue()
                    if (r11 == 0) goto L_0x00b0
                    com.fossil.bq6$b r11 = r10.this$0
                    com.fossil.bq6 r11 = r11.a
                    com.fossil.yp6 r11 = r11.o()
                    r11.t0()
                    goto L_0x00dd
                L_0x00b0:
                    com.fossil.bq6$b r11 = r10.this$0
                    com.fossil.bq6 r0 = r11.a
                    com.portfolio.platform.data.model.Device r11 = r11.c
                    r0.c(r11)
                    goto L_0x00dd
                L_0x00ba:
                    com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
                    com.fossil.bq6$a r0 = com.fossil.bq6.r
                    java.lang.String r0 = r0.a()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    r2.append(r3)
                    r2.append(r1)
                    java.lang.String r1 = " but device is DianaEV1!!!"
                    r2.append(r1)
                    java.lang.String r1 = r2.toString()
                    r11.e(r0, r1)
                L_0x00dd:
                    com.fossil.i97 r11 = com.fossil.i97.a
                    return r11
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.bq6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(bq6 bq6, String str, Device device) {
            this.a = bq6;
            this.b = str;
            this.c = device;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tl5.d dVar) {
            ee7.b(dVar, "responseValue");
            ik7 unused = xh7.b(this.a.e(), null, null, new a(this, dVar, null), 3, null);
        }

        @DexIgnore
        public void a(tl5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().e(bq6.r.a(), "checkFirmware - downloadFw FAILED!!!");
            this.a.o().U();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Device> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.n().getDeviceBySerial(this.this$0.$deviceId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, String str2, fb7 fb7, bq6 bq6, Device device) {
            super(2, fb7);
            this.$deviceId = str;
            this.$lastFwTemp = str2;
            this.this$0 = bq6;
            this.$device$inlined = device;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.$deviceId, this.$lastFwTemp, fb7, this.this$0, this.$device$inlined);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) obj;
            FLogger.INSTANCE.getLocal().d(bq6.r.a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
            if (device != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = bq6.r.a();
                local.d(a3, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
                if (!mh7.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                    FLogger.INSTANCE.getLocal().d(bq6.r.a(), "Handle OTA complete on check last OTA success");
                    this.this$0.n.n(this.$deviceId);
                    this.this$0.o().t0();
                } else {
                    this.this$0.a(this.$device$inlined);
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ bq6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(bq6 bq6) {
            this.a = bq6;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bq6.r.a();
            local.d(a2, "otaCompleteReceiver - isSuccess=" + booleanExtra);
            this.a.o().c(booleanExtra);
            this.a.n.n(PortfolioApp.g0.c().c());
            if (booleanExtra) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.e, bq6.r.a(), "[Sync Start] AUTO SYNC after OTA");
                PortfolioApp.g0.c().a(this.a.m, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ bq6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(bq6 bq6) {
            this.a = bq6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bq6.r.a();
                local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && mh7.b(otaEvent.getSerial(), PortfolioApp.g0.c().c(), true)) {
                    this.a.o().c((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1", f = "UpdateFirmwarePresenter.kt", l = {106}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Device>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$activeSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$activeSerial, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Device> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.n().getDeviceBySerial(this.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(bq6 bq6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bq6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            String str;
            bq6 bq6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                str = PortfolioApp.g0.c().c();
                bq6 bq62 = this.this$0;
                ti7 b = bq62.b();
                a aVar = new a(this, str, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.L$2 = bq62;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                bq6 = bq62;
            } else if (i == 1) {
                bq6 = (bq6) this.L$2;
                str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            bq6.g = (Device) obj;
            boolean D = PortfolioApp.g0.c().D();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = bq6.r.a();
            local.d(a3, "start - activeSerial=" + str + ", isDeviceOtaing=" + D);
            if (!D) {
                bq6 bq63 = this.this$0;
                bq63.b(bq63.g);
            }
            this.this$0.m();
            if5 b2 = qd5.f.b("ota_session");
            this.this$0.h = b2;
            qd5.f.a("ota_session", b2);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1", f = "UpdateFirmwarePresenter.kt", l = {248}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends String, ? extends String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends String, ? extends String>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return yw6.h.a().a(this.this$0.$activeSerial, (Device) null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements fl4.e<pm5.d, pm5.c> {
            @DexIgnore
            public void a(pm5.c cVar) {
                ee7.b(cVar, "errorValue");
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(pm5.d dVar) {
                ee7.b(dVar, "responseValue");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(bq6 bq6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bq6;
            this.$activeSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$activeSerial, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            if5 c;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b2 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b2, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            r87 r87 = (r87) obj;
            String str = (String) r87.component1();
            String str2 = (String) r87.component2();
            if (!(str == null || str2 == null || (c = this.this$0.h) == null)) {
                c.a("old_firmware", str);
                if (c != null) {
                    c.a("new_firmware", str2);
                    if (c != null) {
                        c.d();
                    }
                }
            }
            this.this$0.o.a(new pm5.b(this.$activeSerial, false, 2, null), new b());
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = bq6.class.getSimpleName();
        ee7.a((Object) simpleName, "UpdateFirmwarePresenter::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public bq6(yp6 yp6, DeviceRepository deviceRepository, UserRepository userRepository, ad5 ad5, ch5 ch5, pm5 pm5, tl5 tl5) {
        ee7.b(yp6, "mView");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(pm5, "mUpdateFirmwareUseCase");
        ee7.b(tl5, "mDownloadFwByDeviceModel");
        this.k = yp6;
        this.l = deviceRepository;
        this.m = ad5;
        this.n = ch5;
        this.o = pm5;
        this.p = tl5;
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public boolean j() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void k() {
        if (be5.o.f(PortfolioApp.g0.c().c())) {
            this.k.u();
        } else {
            this.k.i();
        }
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void l() {
        FLogger.INSTANCE.getLocal().d(q, "tryOtaAgain");
        Device device = this.g;
        if (device != null) {
            c(device);
        }
        this.k.k0();
    }

    @DexIgnore
    public final void m() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (be5.o.a(deviceBySerial)) {
            explore.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886862));
            explore.setBackground(2131231334);
            explore2.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886863));
            explore2.setBackground(2131231332);
            explore3.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886860));
            explore3.setBackground(2131231335);
            explore4.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886861));
            explore4.setBackground(2131231331);
        } else {
            explore.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886869));
            explore.setBackground(2131231334);
            explore2.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886868));
            explore2.setBackground(2131231336);
            explore3.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886866));
            explore3.setBackground(2131231335);
            explore4.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886867));
            explore4.setBackground(2131231333);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.k.d(arrayList);
    }

    @DexIgnore
    public final DeviceRepository n() {
        return this.l;
    }

    @DexIgnore
    public final yp6 o() {
        return this.k;
    }

    @DexIgnore
    public void p() {
        this.k.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "start - mIsOnBoardingFlow=" + this.f);
        if (!this.f) {
            this.k.F();
        }
        nj5.d.a(this.j, CommunicateMode.OTA);
        PortfolioApp c2 = PortfolioApp.g0.c();
        e eVar = this.i;
        c2.registerReceiver(eVar, new IntentFilter(PortfolioApp.g0.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        this.k.e();
        nj5.d.a(CommunicateMode.OTA);
        ik7 unused = xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        try {
            PortfolioApp.g0.c().unregisterReceiver(this.i);
            nj5.d.b(this.j, CommunicateMode.OTA);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.e(str, "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void h() {
        nj5.d.a(CommunicateMode.OTA);
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void i() {
        FLogger.INSTANCE.getLocal().d(q, "checkFwAgain");
        b(this.g);
    }

    @DexIgnore
    public final void b(Device device) {
        if (device != null) {
            String deviceId = device.getDeviceId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.d(str, "checkLastOTASuccess - deviceId=" + deviceId + ", sku=" + device.getSku());
            if (TextUtils.isEmpty(deviceId)) {
                FLogger.INSTANCE.getLocal().e(q, "checkLastOTASuccess - DEVICE ID IS EMPTY!!!");
                return;
            }
            String g2 = this.n.g(deviceId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = q;
            local2.d(str2, "checkLastOTASuccess - lastTempFw=" + g2);
            if (TextUtils.isEmpty(g2)) {
                a(device);
            } else {
                ik7 unused = xh7.b(e(), null, null, new c(deviceId, g2, null, this, device), 3, null);
            }
        }
    }

    @DexIgnore
    public final void c(Device device) {
        ee7.b(device, "Device");
        String deviceId = device.getDeviceId();
        boolean D = PortfolioApp.g0.c().D();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "updateFirmware - activeSerial=" + deviceId + ", isDeviceOtaing=" + D);
        if (!D) {
            ik7 unused = xh7.b(e(), null, null, new g(this, deviceId, null), 3, null);
        }
    }

    @DexIgnore
    public final void a(Device device) {
        String deviceId = device.getDeviceId();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "checkFirmware - deviceId=" + deviceId + ", sku=" + device.getSku() + ", fw=" + device.getFirmwareRevision());
        if (PortfolioApp.g0.c().G() || !this.n.a0()) {
            tl5 tl5 = this.p;
            String sku = device.getSku();
            if (sku == null) {
                sku = "";
            }
            tl5.a(new tl5.b(sku), new b(this, deviceId, device));
            return;
        }
        this.k.t0();
    }

    @DexIgnore
    @Override // com.fossil.xp6
    public void a(boolean z, String str) {
        ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        this.f = z;
        this.e = str;
    }
}
