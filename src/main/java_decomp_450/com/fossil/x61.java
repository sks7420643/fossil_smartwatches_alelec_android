package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x61 implements Parcelable.Creator<u81> {
    @DexIgnore
    public /* synthetic */ x61(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public u81 createFromParcel(Parcel parcel) {
        qk1 qk1 = qk1.values()[parcel.readInt()];
        long readLong = parcel.readLong();
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
            return new u81(qk1, readLong, createByteArray);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public u81[] newArray(int i) {
        return new u81[i];
    }
}
