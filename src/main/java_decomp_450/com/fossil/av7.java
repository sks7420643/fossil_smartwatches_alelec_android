package com.fossil;

import com.fossil.tu7;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@IgnoreJRERequirement
public final class av7 extends tu7.a {
    @DexIgnore
    public static /* final */ tu7.a a; // = new av7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class a<T> implements tu7<mo7, Optional<T>> {
        @DexIgnore
        public /* final */ tu7<mo7, T> a;

        @DexIgnore
        public a(tu7<mo7, T> tu7) {
            this.a = tu7;
        }

        @DexIgnore
        public Optional<T> a(mo7 mo7) throws IOException {
            return Optional.ofNullable(this.a.a(mo7));
        }
    }

    @DexIgnore
    @Override // com.fossil.tu7.a
    public tu7<mo7, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (tu7.a.a(type) != Optional.class) {
            return null;
        }
        return new a(retrofit3.b(tu7.a.a(0, (ParameterizedType) type), annotationArr));
    }
}
