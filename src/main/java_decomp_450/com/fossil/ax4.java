package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ax4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ c85 s;
    @DexIgnore
    public /* final */ c85 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public ax4(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, c85 c85, c85 c852, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = c85;
        a((ViewDataBinding) c85);
        this.t = c852;
        a((ViewDataBinding) c852);
        this.u = linearLayout;
    }
}
