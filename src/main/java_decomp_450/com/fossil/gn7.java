package com.fossil;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn7 {
    @DexIgnore
    public static /* final */ long a; // = om7.a("kotlinx.coroutines.scheduler.resolution.ns", 100000L, 0L, 0L, 12, (Object) null);
    @DexIgnore
    public static /* final */ int b; // = om7.a("kotlinx.coroutines.scheduler.core.pool.size", qf7.a(mm7.a(), 2), 1, 0, 8, (Object) null);
    @DexIgnore
    public static /* final */ int c; // = om7.a("kotlinx.coroutines.scheduler.max.pool.size", qf7.a(mm7.a() * 128, b, 2097150), 0, 2097150, 4, (Object) null);
    @DexIgnore
    public static /* final */ long d; // = TimeUnit.SECONDS.toNanos(om7.a("kotlinx.coroutines.scheduler.keep.alive.sec", 60L, 0L, 0L, 12, (Object) null));
    @DexIgnore
    public static hn7 e; // = bn7.a;

    /*
    static {
        int unused = om7.a("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, (Object) null);
    }
    */
}
