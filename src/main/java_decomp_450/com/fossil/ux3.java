package com.fossil;

import com.fossil.by3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ux3<K, V> extends by3<K, V> implements zw3<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends by3.e {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public b(ux3<?, ?> ux3) {
            super(ux3);
        }

        @DexIgnore
        @Override // com.fossil.by3.e
        public Object readResolve() {
            return createMap(new a());
        }
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if (map instanceof ux3) {
            ux3<K, V> ux3 = (ux3) map;
            if (!ux3.isPartialView()) {
                return ux3;
            }
        }
        return copyOf((Iterable) map.entrySet());
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> of() {
        return oz3.EMPTY;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public V forcePut(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract ux3<V, K> inverse();

    @DexIgnore
    @Override // com.fossil.by3
    public Object writeReplace() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> of(K k, V v) {
        return new zz3(k, v);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> of(K k, V v, K k2, V v2) {
        return oz3.fromEntries(by3.entryOf(k, v), by3.entryOf(k2, v2));
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return oz3.fromEntries(by3.entryOf(k, v), by3.entryOf(k2, v2), by3.entryOf(k3, v3));
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.by3, com.fossil.by3
    public iy3<V> values() {
        return inverse().keySet();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends by3.b<K, V> {
        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public a<K, V> a(K k, V v) {
            super.a((Object) k, (Object) v);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public a<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            super.a((Map.Entry) entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public a<K, V> a(Map<? extends K, ? extends V> map) {
            super.a((Map) map);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public a<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a((Iterable) iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        public ux3<K, V> a() {
            int i = ((by3.b) this).c;
            if (i == 0) {
                return ux3.of();
            }
            boolean z = true;
            if (i == 1) {
                return ux3.of((Object) ((by3.b) this).b[0].getKey(), (Object) ((by3.b) this).b[0].getValue());
            }
            if (((by3.b) this).a != null) {
                if (((by3.b) this).d) {
                    ((by3.b) this).b = (cy3[]) iz3.a((Object[]) ((by3.b) this).b, i);
                }
                Arrays.sort(((by3.b) this).b, 0, ((by3.b) this).c, jz3.from(((by3.b) this).a).onResultOf(yy3.c()));
            }
            if (((by3.b) this).c != ((by3.b) this).b.length) {
                z = false;
            }
            ((by3.b) this).d = z;
            return oz3.fromEntryArray(((by3.b) this).c, ((by3.b) this).b);
        }
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) py3.a((Iterable) iterable, (Object[]) by3.EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return oz3.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return oz3.fromEntries(by3.entryOf(k, v), by3.entryOf(k2, v2), by3.entryOf(k3, v3), by3.entryOf(k4, v4));
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ux3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return oz3.fromEntries(by3.entryOf(k, v), by3.entryOf(k2, v2), by3.entryOf(k3, v3), by3.entryOf(k4, v4), by3.entryOf(k5, v5));
    }
}
