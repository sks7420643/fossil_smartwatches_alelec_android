package com.fossil;

import com.fossil.bw2;
import java.io.IOException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv2 implements oz2 {
    @DexIgnore
    public /* final */ iv2 a;

    @DexIgnore
    public kv2(iv2 iv2) {
        ew2.a((Object) iv2, "output");
        iv2 iv22 = iv2;
        this.a = iv22;
        iv22.a = this;
    }

    @DexIgnore
    public static kv2 a(iv2 iv2) {
        kv2 kv2 = iv2.a;
        if (kv2 != null) {
            return kv2;
        }
        return new kv2(iv2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void b(int i, Object obj, cy2 cy2) throws IOException {
        iv2 iv2 = this.a;
        iv2.a(i, 3);
        cy2.a((jx2) obj, iv2.a);
        iv2.a(i, 4);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final int zza() {
        return bw2.f.k;
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzb(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzc(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzd(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zze(int i, int i2) throws IOException {
        this.a.c(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzf(int i, int i2) throws IOException {
        this.a.d(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.b(list.get(i4).doubleValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.k(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.b(list.get(i4).booleanValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.g(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.j(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.h(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.h(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.d(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.f(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzc(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzd(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zze(int i, long j) throws IOException {
        this.a.b(i, j);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.b(list.get(i4).floatValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void a(int i, tu2 tu2) throws IOException {
        this.a.a(i, tu2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzb(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.d(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.e(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.g(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void a(int i, Object obj, cy2 cy2) throws IOException {
        this.a.a(i, (jx2) obj, cy2);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void a(int i, List<?> list, cy2 cy2) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, list.get(i2), cy2);
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void b(int i, List<?> list, cy2 cy2) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(i, list.get(i2), cy2);
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzb(int i) throws IOException {
        this.a.a(i, 4);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, float f) throws IOException {
        this.a.a(i, f);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.i(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final <K, V> void a(int i, ex2<K, V> ex2, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.a.a(i, 2);
            this.a.b(bx2.a(ex2, entry.getKey(), entry.getValue()));
            bx2.a(this.a, ex2, entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, double d) throws IOException {
        this.a.a(i, d);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, boolean z) throws IOException {
        this.a.a(i, z);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, String str) throws IOException {
        this.a.a(i, str);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i) throws IOException {
        this.a.a(i, 3);
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof tu2) {
            this.a.b(i, (tu2) obj);
        } else {
            this.a.a(i, (jx2) obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += iv2.f(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zzb(int i, List<tu2> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.a(i, list.get(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.oz2
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof tw2) {
            tw2 tw2 = (tw2) list;
            while (i2 < list.size()) {
                Object zzb = tw2.zzb(i2);
                if (zzb instanceof String) {
                    this.a.a(i, (String) zzb);
                } else {
                    this.a.a(i, (tu2) zzb);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2));
            i2++;
        }
    }
}
