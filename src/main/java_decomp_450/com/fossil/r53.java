package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r53 implements Parcelable.Creator<f53> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ f53 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        boolean z = false;
        ArrayList arrayList = null;
        p53 p53 = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                arrayList = j72.c(parcel, a, LocationRequest.CREATOR);
            } else if (a2 == 2) {
                z = j72.i(parcel, a);
            } else if (a2 == 3) {
                z2 = j72.i(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                p53 = (p53) j72.a(parcel, a, p53.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new f53(arrayList, z, z2, p53);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ f53[] newArray(int i) {
        return new f53[i];
    }
}
