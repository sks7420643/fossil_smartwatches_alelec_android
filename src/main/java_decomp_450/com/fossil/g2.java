package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.CompoundButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g2 {
    @DexIgnore
    public /* final */ CompoundButton a;
    @DexIgnore
    public ColorStateList b; // = null;
    @DexIgnore
    public PorterDuff.Mode c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public g2(CompoundButton compoundButton) {
        this.a = compoundButton;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.util.AttributeSet r11, int r12) {
        /*
            r10 = this;
            android.widget.CompoundButton r0 = r10.a
            android.content.Context r0 = r0.getContext()
            int[] r1 = com.fossil.h0.CompoundButton
            r2 = 0
            com.fossil.g3 r0 = com.fossil.g3.a(r0, r11, r1, r12, r2)
            android.widget.CompoundButton r3 = r10.a
            android.content.Context r4 = r3.getContext()
            int[] r5 = com.fossil.h0.CompoundButton
            android.content.res.TypedArray r7 = r0.a()
            r9 = 0
            r6 = r11
            r8 = r12
            com.fossil.da.a(r3, r4, r5, r6, r7, r8, r9)
            int r11 = com.fossil.h0.CompoundButton_buttonCompat     // Catch:{ all -> 0x0092 }
            boolean r11 = r0.g(r11)     // Catch:{ all -> 0x0092 }
            if (r11 == 0) goto L_0x0040
            int r11 = com.fossil.h0.CompoundButton_buttonCompat     // Catch:{ all -> 0x0092 }
            int r11 = r0.g(r11, r2)     // Catch:{ all -> 0x0092 }
            if (r11 == 0) goto L_0x0040
            android.widget.CompoundButton r12 = r10.a     // Catch:{ NotFoundException -> 0x0040 }
            android.widget.CompoundButton r1 = r10.a     // Catch:{ NotFoundException -> 0x0040 }
            android.content.Context r1 = r1.getContext()     // Catch:{ NotFoundException -> 0x0040 }
            android.graphics.drawable.Drawable r11 = com.fossil.t0.c(r1, r11)     // Catch:{ NotFoundException -> 0x0040 }
            r12.setButtonDrawable(r11)     // Catch:{ NotFoundException -> 0x0040 }
            r11 = 1
            goto L_0x0041
        L_0x0040:
            r11 = 0
        L_0x0041:
            if (r11 != 0) goto L_0x0062
            int r11 = com.fossil.h0.CompoundButton_android_button
            boolean r11 = r0.g(r11)
            if (r11 == 0) goto L_0x0062
            int r11 = com.fossil.h0.CompoundButton_android_button
            int r11 = r0.g(r11, r2)
            if (r11 == 0) goto L_0x0062
            android.widget.CompoundButton r12 = r10.a
            android.widget.CompoundButton r1 = r10.a
            android.content.Context r1 = r1.getContext()
            android.graphics.drawable.Drawable r11 = com.fossil.t0.c(r1, r11)
            r12.setButtonDrawable(r11)
        L_0x0062:
            int r11 = com.fossil.h0.CompoundButton_buttonTint
            boolean r11 = r0.g(r11)
            if (r11 == 0) goto L_0x0075
            android.widget.CompoundButton r11 = r10.a
            int r12 = com.fossil.h0.CompoundButton_buttonTint
            android.content.res.ColorStateList r12 = r0.a(r12)
            com.fossil.ua.a(r11, r12)
        L_0x0075:
            int r11 = com.fossil.h0.CompoundButton_buttonTintMode
            boolean r11 = r0.g(r11)
            if (r11 == 0) goto L_0x008e
            android.widget.CompoundButton r11 = r10.a
            int r12 = com.fossil.h0.CompoundButton_buttonTintMode
            r1 = -1
            int r12 = r0.d(r12, r1)
            r1 = 0
            android.graphics.PorterDuff$Mode r12 = com.fossil.q2.a(r12, r1)
            com.fossil.ua.a(r11, r12)
        L_0x008e:
            r0.b()
            return
        L_0x0092:
            r11 = move-exception
            r0.b()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.g2.a(android.util.AttributeSet, int):void");
    }

    @DexIgnore
    public ColorStateList b() {
        return this.b;
    }

    @DexIgnore
    public PorterDuff.Mode c() {
        return this.c;
    }

    @DexIgnore
    public void d() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        a();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.b = colorStateList;
        this.d = true;
        a();
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        this.c = mode;
        this.e = true;
        a();
    }

    @DexIgnore
    public void a() {
        Drawable a2 = ua.a(this.a);
        if (a2 == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = p7.i(a2).mutate();
            if (this.d) {
                p7.a(mutate, this.b);
            }
            if (this.e) {
                p7.a(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.a.getDrawableState());
            }
            this.a.setButtonDrawable(mutate);
        }
    }

    @DexIgnore
    public int a(int i) {
        Drawable a2;
        return (Build.VERSION.SDK_INT >= 17 || (a2 = ua.a(this.a)) == null) ? i : i + a2.getIntrinsicWidth();
    }
}
