package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import com.fossil.w2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h2 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode b; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static h2 c;
    @DexIgnore
    public w2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements w2.e {
        @DexIgnore
        public /* final */ int[] a; // = {c0.abc_textfield_search_default_mtrl_alpha, c0.abc_textfield_default_mtrl_alpha, c0.abc_ab_share_pack_mtrl_alpha};
        @DexIgnore
        public /* final */ int[] b; // = {c0.abc_ic_commit_search_api_mtrl_alpha, c0.abc_seekbar_tick_mark_material, c0.abc_ic_menu_share_mtrl_alpha, c0.abc_ic_menu_copy_mtrl_am_alpha, c0.abc_ic_menu_cut_mtrl_alpha, c0.abc_ic_menu_selectall_mtrl_alpha, c0.abc_ic_menu_paste_mtrl_am_alpha};
        @DexIgnore
        public /* final */ int[] c; // = {c0.abc_textfield_activated_mtrl_alpha, c0.abc_textfield_search_activated_mtrl_alpha, c0.abc_cab_background_top_mtrl_alpha, c0.abc_text_cursor_material, c0.abc_text_select_handle_left_mtrl_dark, c0.abc_text_select_handle_middle_mtrl_dark, c0.abc_text_select_handle_right_mtrl_dark, c0.abc_text_select_handle_left_mtrl_light, c0.abc_text_select_handle_middle_mtrl_light, c0.abc_text_select_handle_right_mtrl_light};
        @DexIgnore
        public /* final */ int[] d; // = {c0.abc_popup_background_mtrl_mult, c0.abc_cab_background_internal_bg, c0.abc_menu_hardkey_panel_mtrl_mult};
        @DexIgnore
        public /* final */ int[] e; // = {c0.abc_tab_indicator_material, c0.abc_textfield_search_material};
        @DexIgnore
        public /* final */ int[] f; // = {c0.abc_btn_check_material, c0.abc_btn_radio_material, c0.abc_btn_check_material_anim, c0.abc_btn_radio_material_anim};

        @DexIgnore
        public final ColorStateList a(Context context) {
            return b(context, 0);
        }

        @DexIgnore
        public final ColorStateList b(Context context) {
            return b(context, b3.b(context, y.colorAccent));
        }

        @DexIgnore
        public final ColorStateList c(Context context) {
            return b(context, b3.b(context, y.colorButtonNormal));
        }

        @DexIgnore
        public final ColorStateList d(Context context) {
            int[][] iArr = new int[3][];
            int[] iArr2 = new int[3];
            ColorStateList c2 = b3.c(context, y.colorSwitchThumbNormal);
            if (c2 == null || !c2.isStateful()) {
                iArr[0] = b3.b;
                iArr2[0] = b3.a(context, y.colorSwitchThumbNormal);
                iArr[1] = b3.e;
                iArr2[1] = b3.b(context, y.colorControlActivated);
                iArr[2] = b3.f;
                iArr2[2] = b3.b(context, y.colorSwitchThumbNormal);
            } else {
                iArr[0] = b3.b;
                iArr2[0] = c2.getColorForState(iArr[0], 0);
                iArr[1] = b3.e;
                iArr2[1] = b3.b(context, y.colorControlActivated);
                iArr[2] = b3.f;
                iArr2[2] = c2.getDefaultColor();
            }
            return new ColorStateList(iArr, iArr2);
        }

        @DexIgnore
        @Override // com.fossil.w2.e
        public Drawable a(w2 w2Var, Context context, int i) {
            if (i != c0.abc_cab_background_top_material) {
                return null;
            }
            return new LayerDrawable(new Drawable[]{w2Var.b(context, c0.abc_cab_background_internal_bg), w2Var.b(context, c0.abc_cab_background_top_mtrl_alpha)});
        }

        @DexIgnore
        public final ColorStateList b(Context context, int i) {
            int b2 = b3.b(context, y.colorControlHighlight);
            int a2 = b3.a(context, y.colorButtonNormal);
            return new ColorStateList(new int[][]{b3.b, b3.d, b3.c, b3.f}, new int[]{a2, e7.b(b2, i), e7.b(b2, i), i});
        }

        @DexIgnore
        public final void a(Drawable drawable, int i, PorterDuff.Mode mode) {
            if (q2.a(drawable)) {
                drawable = drawable.mutate();
            }
            if (mode == null) {
                mode = h2.b;
            }
            drawable.setColorFilter(h2.a(i, mode));
        }

        @DexIgnore
        public final boolean a(int[] iArr, int i) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.w2.e
        public ColorStateList a(Context context, int i) {
            if (i == c0.abc_edit_text_material) {
                return t0.b(context, a0.abc_tint_edittext);
            }
            if (i == c0.abc_switch_track_mtrl_alpha) {
                return t0.b(context, a0.abc_tint_switch_track);
            }
            if (i == c0.abc_switch_thumb_material) {
                return d(context);
            }
            if (i == c0.abc_btn_default_mtrl_shape) {
                return c(context);
            }
            if (i == c0.abc_btn_borderless_material) {
                return a(context);
            }
            if (i == c0.abc_btn_colored_material) {
                return b(context);
            }
            if (i == c0.abc_spinner_mtrl_am_alpha || i == c0.abc_spinner_textfield_background_material) {
                return t0.b(context, a0.abc_tint_spinner);
            }
            if (a(this.b, i)) {
                return b3.c(context, y.colorControlNormal);
            }
            if (a(this.e, i)) {
                return t0.b(context, a0.abc_tint_default);
            }
            if (a(this.f, i)) {
                return t0.b(context, a0.abc_tint_btn_checkable);
            }
            if (i == c0.abc_seekbar_thumb_material) {
                return t0.b(context, a0.abc_tint_seek_thumb);
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.w2.e
        public boolean b(Context context, int i, Drawable drawable) {
            if (i == c0.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                a(layerDrawable.findDrawableByLayerId(16908288), b3.b(context, y.colorControlNormal), h2.b);
                a(layerDrawable.findDrawableByLayerId(16908303), b3.b(context, y.colorControlNormal), h2.b);
                a(layerDrawable.findDrawableByLayerId(16908301), b3.b(context, y.colorControlActivated), h2.b);
                return true;
            } else if (i != c0.abc_ratingbar_material && i != c0.abc_ratingbar_indicator_material && i != c0.abc_ratingbar_small_material) {
                return false;
            } else {
                LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                a(layerDrawable2.findDrawableByLayerId(16908288), b3.a(context, y.colorControlNormal), h2.b);
                a(layerDrawable2.findDrawableByLayerId(16908303), b3.b(context, y.colorControlActivated), h2.b);
                a(layerDrawable2.findDrawableByLayerId(16908301), b3.b(context, y.colorControlActivated), h2.b);
                return true;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0061 A[RETURN] */
        @Override // com.fossil.w2.e
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.content.Context r7, int r8, android.graphics.drawable.Drawable r9) {
            /*
                r6 = this;
                android.graphics.PorterDuff$Mode r0 = com.fossil.h2.b
                int[] r1 = r6.a
                boolean r1 = r6.a(r1, r8)
                r2 = 16842801(0x1010031, float:2.3693695E-38)
                r3 = -1
                r4 = 0
                r5 = 1
                if (r1 == 0) goto L_0x0017
                int r2 = com.fossil.y.colorControlNormal
            L_0x0014:
                r8 = -1
            L_0x0015:
                r1 = 1
                goto L_0x0044
            L_0x0017:
                int[] r1 = r6.c
                boolean r1 = r6.a(r1, r8)
                if (r1 == 0) goto L_0x0022
                int r2 = com.fossil.y.colorControlActivated
                goto L_0x0014
            L_0x0022:
                int[] r1 = r6.d
                boolean r1 = r6.a(r1, r8)
                if (r1 == 0) goto L_0x002d
                android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
                goto L_0x0014
            L_0x002d:
                int r1 = com.fossil.c0.abc_list_divider_mtrl_alpha
                if (r8 != r1) goto L_0x003c
                r2 = 16842800(0x1010030, float:2.3693693E-38)
                r8 = 1109603123(0x42233333, float:40.8)
                int r8 = java.lang.Math.round(r8)
                goto L_0x0015
            L_0x003c:
                int r1 = com.fossil.c0.abc_dialog_material_background
                if (r8 != r1) goto L_0x0041
                goto L_0x0014
            L_0x0041:
                r8 = -1
                r1 = 0
                r2 = 0
            L_0x0044:
                if (r1 == 0) goto L_0x0061
                boolean r1 = com.fossil.q2.a(r9)
                if (r1 == 0) goto L_0x0050
                android.graphics.drawable.Drawable r9 = r9.mutate()
            L_0x0050:
                int r7 = com.fossil.b3.b(r7, r2)
                android.graphics.PorterDuffColorFilter r7 = com.fossil.h2.a(r7, r0)
                r9.setColorFilter(r7)
                if (r8 == r3) goto L_0x0060
                r9.setAlpha(r8)
            L_0x0060:
                return r5
            L_0x0061:
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.h2.a.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
        }

        @DexIgnore
        @Override // com.fossil.w2.e
        public PorterDuff.Mode a(int i) {
            if (i == c0.abc_switch_thumb_material) {
                return PorterDuff.Mode.MULTIPLY;
            }
            return null;
        }
    }

    @DexIgnore
    public static synchronized h2 b() {
        h2 h2Var;
        synchronized (h2.class) {
            if (c == null) {
                c();
            }
            h2Var = c;
        }
        return h2Var;
    }

    @DexIgnore
    public static synchronized void c() {
        synchronized (h2.class) {
            if (c == null) {
                h2 h2Var = new h2();
                c = h2Var;
                h2Var.a = w2.a();
                c.a.a(new a());
            }
        }
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i) {
        return this.a.b(context, i);
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i, boolean z) {
        return this.a.a(context, i, z);
    }

    @DexIgnore
    public synchronized void a(Context context) {
        this.a.b(context);
    }

    @DexIgnore
    public synchronized ColorStateList b(Context context, int i) {
        return this.a.c(context, i);
    }

    @DexIgnore
    public static void a(Drawable drawable, e3 e3Var, int[] iArr) {
        w2.a(drawable, e3Var, iArr);
    }

    @DexIgnore
    public static synchronized PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (h2.class) {
            a2 = w2.a(i, mode);
        }
        return a2;
    }
}
