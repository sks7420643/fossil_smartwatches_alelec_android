package com.fossil;

import com.fossil.ng;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sp5 extends ng.d<SleepSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        ee7.b(sleepSummary, "oldItem");
        ee7.b(sleepSummary2, "newItem");
        return ee7.a(sleepSummary, sleepSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        ee7.b(sleepSummary, "oldItem");
        ee7.b(sleepSummary2, "newItem");
        MFSleepDay sleepDay = sleepSummary.getSleepDay();
        Date date = null;
        Date date2 = sleepDay != null ? sleepDay.getDate() : null;
        MFSleepDay sleepDay2 = sleepSummary2.getSleepDay();
        if (sleepDay2 != null) {
            date = sleepDay2.getDate();
        }
        return zd5.d(date2, date);
    }
}
