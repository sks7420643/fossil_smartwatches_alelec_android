package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class n74 implements et1 {
    @DexIgnore
    public static /* final */ n74 a; // = new n74();

    @DexIgnore
    public static et1 a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.et1
    public Object apply(Object obj) {
        return o74.b.a((v54) obj).getBytes(Charset.forName("UTF-8"));
    }
}
