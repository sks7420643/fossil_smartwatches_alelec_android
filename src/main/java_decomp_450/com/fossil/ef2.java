package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ef2 extends IInterface {
    @DexIgnore
    boolean b(boolean z) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;
}
