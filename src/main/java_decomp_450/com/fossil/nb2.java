package com.fossil;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb2 implements DynamiteModule.b.AbstractC0264b {
    @DexIgnore
    @Override // com.google.android.gms.dynamite.DynamiteModule.b.AbstractC0264b
    public final int a(Context context, String str, boolean z) throws DynamiteModule.a {
        return DynamiteModule.a(context, str, z);
    }

    @DexIgnore
    @Override // com.google.android.gms.dynamite.DynamiteModule.b.AbstractC0264b
    public final int a(Context context, String str) {
        return DynamiteModule.a(context, str);
    }
}
