package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ja5 extends ia5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362751, 1);
        y.put(2131361965, 2);
        y.put(2131361958, 3);
        y.put(2131363270, 4);
        y.put(2131362635, 5);
    }
    */

    @DexIgnore
    public ja5(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 6, x, y));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public ja5(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[3], (FlexibleButton) objArr[2], (ImageView) objArr[5], (View) objArr[1], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[4]);
        this.w = -1;
        ((ia5) this).u.setTag(null);
        a(view);
        f();
    }
}
