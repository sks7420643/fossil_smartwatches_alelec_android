package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo5 extends RecyclerView.g<b> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<Complication> c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public Complication e;
        @DexIgnore
        public /* final */ /* synthetic */ yo5 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mData[adapterPosition]");
                    d.a((Complication) obj);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yo5$b$b")
        /* renamed from: com.fossil.yo5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0257b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public View$OnClickListenerC0257b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mData[adapterPosition]");
                    d.b((Complication) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                ee7.b(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yo5 yo5, View view) {
            super(view);
            ee7.b(view, "view");
            this.f = yo5;
            View findViewById = view.findViewById(2131363457);
            ee7.a((Object) findViewById, "view.findViewById(R.id.wc_complication)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363232);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_complication_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362698);
            this.d = view.findViewById(2131362736);
        }

        @DexIgnore
        public final void a(Complication complication) {
            String str;
            ee7.b(complication, "complication");
            this.a.setOnClickListener(new a(this));
            this.d.setOnClickListener(new View$OnClickListenerC0257b(this));
            this.e = complication;
            if (complication != null) {
                this.a.b(complication.getComplicationId());
                this.b.setText(ig5.a(PortfolioApp.g0.c(), complication.getNameKey(), complication.getName()));
            }
            this.a.setSelectedWc(ee7.a((Object) complication.getComplicationId(), (Object) this.f.b));
            if (ee7.a((Object) complication.getComplicationId(), (Object) this.f.b)) {
                View view = this.c;
                ee7.a((Object) view, "ivIndicator");
                view.setBackground(v6.c(PortfolioApp.g0.c(), 2131230953));
            } else {
                View view2 = this.c;
                ee7.a((Object) view2, "ivIndicator");
                view2.setBackground(v6.c(PortfolioApp.g0.c(), 2131230954));
            }
            View view3 = this.d;
            ee7.a((Object) view3, "ivWarning");
            view3.setVisibility(!wd5.b.d(complication.getComplicationId()) ? 0 : 8);
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            Complication complication2 = this.e;
            if (complication2 == null || (str = complication2.getComplicationId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            ee7.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "COMPLICATION", putExtra, null, new c(this), 4, null);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Complication complication);

        @DexIgnore
        void b(Complication complication);
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ yo5(ArrayList arrayList, c cVar, int i, zd7 zd7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("ComplicationsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final c d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        T t;
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getComplicationId(), (Object) "empty")) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            Collections.swap(this.c, this.c.indexOf(t2), 0);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "complicationId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558667, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026plication, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public yo5(ArrayList<Complication> arrayList, c cVar) {
        ee7.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<Complication> list) {
        ee7.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        e();
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        ee7.b(str, "complicationId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getComplicationId(), (Object) str)) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            Complication complication = this.c.get(i);
            ee7.a((Object) complication, "mData[position]");
            bVar.a(complication);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        ee7.b(cVar, "listener");
        this.d = cVar;
    }
}
