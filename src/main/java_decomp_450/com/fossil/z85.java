package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z85 extends y85 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362340, 1);
        A.put(2131362075, 2);
        A.put(2131361814, 3);
        A.put(2131362792, 4);
        A.put(2131362892, 5);
        A.put(2131362891, 6);
        A.put(2131363397, 7);
    }
    */

    @DexIgnore
    public z85(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public z85(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleCheckBox) objArr[3], (ConstraintLayout) objArr[2], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[1], (ConstraintLayout) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (View) objArr[7]);
        this.y = -1;
        ((y85) this).s.setTag(null);
        a(view);
        f();
    }
}
