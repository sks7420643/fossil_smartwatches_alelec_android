package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.fossil.blesdk.device.logic.phase.SyncPhase$onStart$1$1", f = "SyncPhase.kt", l = {}, m = "invokeSuspend")
public final class lu0 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ /* synthetic */ fw0 c;
    @DexIgnore
    public /* final */ /* synthetic */ zk0 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lu0(fw0 fw0, zk0 zk0, fb7 fb7) {
        super(2, fb7);
        this.c = fw0;
        this.d = zk0;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        lu0 lu0 = new lu0(this.c, this.d, fb7);
        lu0.a = (yi7) obj;
        return lu0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        lu0 lu0 = new lu0(this.c, this.d, fb7);
        lu0.a = yi7;
        return lu0.invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.b == 0) {
            t87.a(obj);
            ((zk1) this.c.a).H.clear();
            ArrayList<bi1> arrayList = ((zk1) this.c.a).H;
            zk0 zk0 = this.d;
            if (zk0 != null) {
                arrayList.addAll(((zm1) zk0).G);
                b31 b31 = this.c.a;
                b31.b(((zk1) b31).H);
                b31 b312 = this.c.a;
                b312.a(eu0.a(((zk0) b312).v, null, is0.SUCCESS, null, 5));
                return i97.a;
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.logic.phase.LegacySyncPhase");
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
