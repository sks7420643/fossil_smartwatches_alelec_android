package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk4 implements Factory<ch5> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<Context> b;

    @DexIgnore
    public tk4(wj4 wj4, Provider<Context> provider) {
        this.a = wj4;
        this.b = provider;
    }

    @DexIgnore
    public static tk4 a(wj4 wj4, Provider<Context> provider) {
        return new tk4(wj4, provider);
    }

    @DexIgnore
    public static ch5 a(wj4 wj4, Context context) {
        ch5 a2 = wj4.a(context);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ch5 get() {
        return a(this.a, this.b.get());
    }
}
