package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg0 {
    @DexIgnore
    public ArrayList<vg0> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements gd7<vg0, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ba0 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ba0 ba0) {
            super(1);
            this.a = ba0;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public Boolean invoke(vg0 vg0) {
            return Boolean.valueOf(this.a == vg0.getNotificationType());
        }
    }

    @DexIgnore
    public final vg0[] a() {
        Object[] array = this.a.toArray(new vg0[0]);
        if (array != null) {
            return (vg0[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final synchronized tg0 a(ba0 ba0, ug0 ug0) {
        ba7.a((List) this.a, (gd7) new a(ba0));
        if (ug0 != null) {
            if (!(ug0.getReplyMessages().length == 0)) {
                this.a.add(new vg0(ba0, ug0));
            }
        }
        return this;
    }
}
