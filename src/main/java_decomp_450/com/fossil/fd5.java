package com.fossil;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.fossil.ix;
import com.fossil.m00;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fd5 implements m00<gd5, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n00<gd5, InputStream> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.fd5' to match base method */
        @Override // com.fossil.n00
        public m00<gd5, InputStream> a(q00 q00) {
            ee7.b(q00, "multiFactory");
            return new fd5();
        }
    }

    @DexIgnore
    public boolean a(gd5 gd5) {
        ee7.b(gd5, "avatarModel");
        return true;
    }

    @DexIgnore
    public m00.a<InputStream> a(gd5 gd5, int i, int i2, ax axVar) {
        ee7.b(gd5, "avatarModel");
        ee7.b(axVar, "options");
        return new m00.a<>(gd5, new a(this, gd5));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements ix<InputStream> {
        @DexIgnore
        public ix<InputStream> a;
        @DexIgnore
        public volatile boolean b;
        @DexIgnore
        public /* final */ gd5 c;

        @DexIgnore
        public a(fd5 fd5, gd5 gd5) {
            ee7.b(gd5, "mAvatarModel");
            this.c = gd5;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super InputStream> aVar) {
            ee7.b(ewVar, "priority");
            ee7.b(aVar, Constants.CALLBACK);
            try {
                if (!TextUtils.isEmpty(this.c.c())) {
                    this.a = new ox(new f00(this.c.c()), 100);
                } else if (this.c.b() != null) {
                    this.a = new tx(PortfolioApp.g0.c().getContentResolver(), this.c.b());
                }
                ix<InputStream> ixVar = this.a;
                if (ixVar != null) {
                    ixVar.a(ewVar, aVar);
                }
            } catch (Exception unused) {
                ix<InputStream> ixVar2 = this.a;
                if (ixVar2 != null) {
                    ixVar2.a();
                }
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String a2 = this.c.a();
            if (a2 == null) {
                a2 = "";
            }
            Bitmap c2 = sw6.c(a2);
            if (c2 != null) {
                c2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            }
            aVar.a(this.b ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
            this.b = true;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
            ix<InputStream> ixVar = this.a;
            if (ixVar != null) {
                ixVar.a();
            }
        }
    }
}
