package com.fossil;

import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zs7 {
    @DexIgnore
    public static /* final */ Object a; // = "y";
    @DexIgnore
    public static /* final */ Object b; // = "M";
    @DexIgnore
    public static /* final */ Object c; // = "d";
    @DexIgnore
    public static /* final */ Object d; // = "H";
    @DexIgnore
    public static /* final */ Object e; // = "m";
    @DexIgnore
    public static /* final */ Object f; // = "s";
    @DexIgnore
    public static /* final */ Object g; // = DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX;

    @DexIgnore
    public static String a(long j) {
        return a(j, "HH:mm:ss.SSS");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public int b; // = 1;

        @DexIgnore
        public a(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static boolean a(a[] aVarArr, Object obj) {
            for (a aVar : aVarArr) {
                if (aVar.b() == obj) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public Object b() {
            return this.a;
        }

        @DexIgnore
        public void c() {
            this.b++;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a.getClass() != aVar.a.getClass() || this.b != aVar.b) {
                return false;
            }
            Object obj2 = this.a;
            if (obj2 instanceof StringBuilder) {
                return obj2.toString().equals(aVar.a.toString());
            }
            if (obj2 instanceof Number) {
                return obj2.equals(aVar.a);
            }
            if (obj2 == aVar.a) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return xs7.a(this.a.toString(), this.b);
        }

        @DexIgnore
        public int a() {
            return this.b;
        }
    }

    @DexIgnore
    public static String a(long j, String str) {
        return a(j, str, true);
    }

    @DexIgnore
    public static String a(long j, String str, boolean z) {
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        long j7;
        ys7.a(0, Long.MAX_VALUE, j, "durationMillis must not be negative");
        a[] a2 = a(str);
        if (a.a(a2, c)) {
            long j8 = j / LogBuilder.MAX_INTERVAL;
            j3 = j - (LogBuilder.MAX_INTERVAL * j8);
            j2 = j8;
        } else {
            j3 = j;
            j2 = 0;
        }
        if (a.a(a2, d)) {
            long j9 = j3 / 3600000;
            j3 -= 3600000 * j9;
            j4 = j9;
        } else {
            j4 = 0;
        }
        if (a.a(a2, e)) {
            long j10 = j3 / 60000;
            j3 -= 60000 * j10;
            j5 = j10;
        } else {
            j5 = 0;
        }
        if (a.a(a2, f)) {
            long j11 = j3 / 1000;
            j6 = j3 - (1000 * j11);
            j7 = j11;
        } else {
            j7 = 0;
            j6 = j3;
        }
        return a(a2, 0, 0, j2, j4, j5, j7, j6, z);
    }

    @DexIgnore
    public static String a(a[] aVarArr, long j, long j2, long j3, long j4, long j5, long j6, long j7, boolean z) {
        int i;
        int i2;
        a[] aVarArr2 = aVarArr;
        StringBuilder sb = new StringBuilder();
        int length = aVarArr2.length;
        int i3 = 0;
        boolean z2 = false;
        while (i3 < length) {
            a aVar = aVarArr2[i3];
            Object b2 = aVar.b();
            int a2 = aVar.a();
            if (b2 instanceof StringBuilder) {
                sb.append(b2.toString());
                i2 = length;
                i = i3;
            } else {
                if (b2.equals(a)) {
                    sb.append(a(j, z, a2));
                    i2 = length;
                    i = i3;
                } else {
                    if (b2.equals(b)) {
                        i = i3;
                        sb.append(a(j2, z, a2));
                    } else {
                        i = i3;
                        if (b2.equals(c)) {
                            sb.append(a(j3, z, a2));
                        } else if (b2.equals(d)) {
                            sb.append(a(j4, z, a2));
                            i2 = length;
                        } else if (b2.equals(e)) {
                            sb.append(a(j5, z, a2));
                            i2 = length;
                        } else {
                            if (b2.equals(f)) {
                                i2 = length;
                                sb.append(a(j6, z, a2));
                                z2 = true;
                            } else {
                                i2 = length;
                                if (b2.equals(g)) {
                                    if (z2) {
                                        int i4 = 3;
                                        if (z) {
                                            i4 = Math.max(3, a2);
                                        }
                                        sb.append(a(j7, true, i4));
                                    } else {
                                        sb.append(a(j7, z, a2));
                                    }
                                    z2 = false;
                                }
                            }
                            i3 = i + 1;
                            length = i2;
                            aVarArr2 = aVarArr;
                        }
                    }
                    i2 = length;
                }
                z2 = false;
            }
            i3 = i + 1;
            length = i2;
            aVarArr2 = aVarArr;
        }
        return sb.toString();
    }

    @DexIgnore
    public static String a(long j, boolean z, int i) {
        String l = Long.toString(j);
        return z ? xs7.a(l, i, '0') : l;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x009b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.zs7.a[] a(java.lang.String r9) {
        /*
            java.util.ArrayList r0 = new java.util.ArrayList
            int r1 = r9.length()
            r0.<init>(r1)
            r1 = 0
            r2 = 0
            r5 = r2
            r6 = r5
            r3 = 0
            r4 = 0
        L_0x000f:
            int r7 = r9.length()
            if (r3 >= r7) goto L_0x009f
            char r7 = r9.charAt(r3)
            r8 = 39
            if (r4 == 0) goto L_0x0024
            if (r7 == r8) goto L_0x0024
            r5.append(r7)
            goto L_0x009b
        L_0x0024:
            if (r7 == r8) goto L_0x006a
            r8 = 72
            if (r7 == r8) goto L_0x0067
            r8 = 77
            if (r7 == r8) goto L_0x0064
            r8 = 83
            if (r7 == r8) goto L_0x0061
            r8 = 100
            if (r7 == r8) goto L_0x005e
            r8 = 109(0x6d, float:1.53E-43)
            if (r7 == r8) goto L_0x005b
            r8 = 115(0x73, float:1.61E-43)
            if (r7 == r8) goto L_0x0058
            r8 = 121(0x79, float:1.7E-43)
            if (r7 == r8) goto L_0x0055
            if (r5 != 0) goto L_0x0051
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.fossil.zs7$a r8 = new com.fossil.zs7$a
            r8.<init>(r5)
            r0.add(r8)
        L_0x0051:
            r5.append(r7)
            goto L_0x007e
        L_0x0055:
            java.lang.Object r7 = com.fossil.zs7.a
            goto L_0x007f
        L_0x0058:
            java.lang.Object r7 = com.fossil.zs7.f
            goto L_0x007f
        L_0x005b:
            java.lang.Object r7 = com.fossil.zs7.e
            goto L_0x007f
        L_0x005e:
            java.lang.Object r7 = com.fossil.zs7.c
            goto L_0x007f
        L_0x0061:
            java.lang.Object r7 = com.fossil.zs7.g
            goto L_0x007f
        L_0x0064:
            java.lang.Object r7 = com.fossil.zs7.b
            goto L_0x007f
        L_0x0067:
            java.lang.Object r7 = com.fossil.zs7.d
            goto L_0x007f
        L_0x006a:
            if (r4 == 0) goto L_0x0070
            r5 = r2
            r7 = r5
            r4 = 0
            goto L_0x007f
        L_0x0070:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.fossil.zs7$a r4 = new com.fossil.zs7$a
            r4.<init>(r5)
            r0.add(r4)
            r4 = 1
        L_0x007e:
            r7 = r2
        L_0x007f:
            if (r7 == 0) goto L_0x009b
            if (r6 == 0) goto L_0x0091
            java.lang.Object r5 = r6.b()
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0091
            r6.c()
            goto L_0x009a
        L_0x0091:
            com.fossil.zs7$a r5 = new com.fossil.zs7$a
            r5.<init>(r7)
            r0.add(r5)
            r6 = r5
        L_0x009a:
            r5 = r2
        L_0x009b:
            int r3 = r3 + 1
            goto L_0x000f
        L_0x009f:
            if (r4 != 0) goto L_0x00ae
            int r9 = r0.size()
            com.fossil.zs7$a[] r9 = new com.fossil.zs7.a[r9]
            java.lang.Object[] r9 = r0.toArray(r9)
            com.fossil.zs7$a[] r9 = (com.fossil.zs7.a[]) r9
            return r9
        L_0x00ae:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unmatched quote in format: "
            r1.append(r2)
            r1.append(r9)
            java.lang.String r9 = r1.toString()
            r0.<init>(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zs7.a(java.lang.String):com.fossil.zs7$a[]");
    }
}
