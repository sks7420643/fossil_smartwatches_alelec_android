package com.fossil;

import android.content.Context;
import com.facebook.internal.Utility;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb3 {
    @DexIgnore
    public static yf3<Long> A; // = a("measurement.upload.initial_upload_delay_time", 15000L, 15000L, sc3.a);
    @DexIgnore
    public static yf3<Boolean> A0; // = a("measurement.engagement_time_main_thread", true, true, we3.a);
    @DexIgnore
    public static yf3<Long> B; // = a("measurement.upload.retry_time", 1800000L, 1800000L, rc3.a);
    @DexIgnore
    public static yf3<Boolean> B0; // = a("measurement.sdk.referrer.delayed_install_referrer_api", false, false, ve3.a);
    @DexIgnore
    public static yf3<Integer> C; // = a("measurement.upload.retry_count", 6, 6, uc3.a);
    @DexIgnore
    public static yf3<Boolean> C0; // = a("measurement.sdk.screen.disabling_automatic_reporting", false, false, ye3.a);
    @DexIgnore
    public static yf3<Long> D; // = a("measurement.upload.max_queue_time", 2419200000L, 2419200000L, tc3.a);
    @DexIgnore
    public static yf3<Boolean> D0; // = a("measurement.sdk.screen.manual_screen_view_logging", false, false, xe3.a);
    @DexIgnore
    public static yf3<Integer> E; // = a("measurement.lifetimevalue.max_currency_tracked", 4, 4, wc3.a);
    @DexIgnore
    public static yf3<Boolean> E0; // = a("measurement.gold.enhanced_ecommerce.format_logs", true, true, af3.a);
    @DexIgnore
    public static yf3<Integer> F; // = a("measurement.audience.filter_result_max_count", 200, 200, yc3.a);
    @DexIgnore
    public static yf3<Boolean> F0; // = a("measurement.gold.enhanced_ecommerce.nested_param_daily_event_count", true, true, ze3.a);
    @DexIgnore
    public static yf3<Integer> G; // = a("measurement.upload.max_public_user_properties", 25, 25, null);
    @DexIgnore
    public static yf3<Boolean> G0; // = a("measurement.gold.enhanced_ecommerce.upload_nested_complex_events", true, true, bf3.a);
    @DexIgnore
    public static yf3<Integer> H; // = a("measurement.upload.max_event_name_cardinality", 500, 500, null);
    @DexIgnore
    public static yf3<Boolean> H0; // = a("measurement.gold.enhanced_ecommerce.log_nested_complex_events", true, true, ef3.a);
    @DexIgnore
    public static yf3<Integer> I; // = a("measurement.upload.max_public_event_params", 25, 25, null);
    @DexIgnore
    public static yf3<Boolean> I0; // = a("measurement.gold.enhanced_ecommerce.updated_schema.client", true, true, df3.a);
    @DexIgnore
    public static yf3<Long> J; // = a("measurement.service_client.idle_disconnect_millis", 5000L, 5000L, xc3.a);
    @DexIgnore
    public static yf3<Boolean> J0; // = a("measurement.gold.enhanced_ecommerce.updated_schema.service", true, true, gf3.a);
    @DexIgnore
    public static yf3<Boolean> K; // = a("measurement.test.boolean_flag", false, false, ad3.a);
    @DexIgnore
    public static yf3<Boolean> K0; // = a("measurement.service.configurable_service_limits", true, true, if3.a);
    @DexIgnore
    public static yf3<String> L; // = a("measurement.test.string_flag", "---", "---", zc3.a);
    @DexIgnore
    public static yf3<Boolean> L0; // = a("measurement.client.configurable_service_limits", false, false, hf3.a);
    @DexIgnore
    public static yf3<Long> M; // = a("measurement.test.long_flag", -1L, -1L, cd3.a);
    @DexIgnore
    public static yf3<Boolean> M0; // = a("measurement.androidId.delete_feature", true, true, lf3.a);
    @DexIgnore
    public static yf3<Integer> N; // = a("measurement.test.int_flag", -2, -2, bd3.a);
    @DexIgnore
    public static yf3<Boolean> N0; // = a("measurement.client.global_params.dev", false, false, kf3.a);
    @DexIgnore
    public static yf3<Double> O;
    @DexIgnore
    public static yf3<Boolean> O0; // = a("measurement.service.global_params", false, false, nf3.a);
    @DexIgnore
    public static yf3<Integer> P; // = a("measurement.experiment.max_ids", 50, 50, dd3.a);
    @DexIgnore
    public static yf3<Boolean> P0; // = a("measurement.service.global_params_in_payload", true, true, pf3.a);
    @DexIgnore
    public static yf3<Integer> Q; // = a("measurement.max_bundles_per_iteration", 2, 2, gd3.a);
    @DexIgnore
    public static yf3<Boolean> Q0; // = a("measurement.client.string_reader", true, true, of3.a);
    @DexIgnore
    public static yf3<Boolean> R; // = a("measurement.validation.internal_limits_internal_event_params", false, false, fd3.a);
    @DexIgnore
    public static yf3<Boolean> R0; // = a("measurement.sdk.attribution.cache", true, true, rf3.a);
    @DexIgnore
    public static yf3<Boolean> S; // = a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", true, true, id3.a);
    @DexIgnore
    public static yf3<Long> S0; // = a("measurement.sdk.attribution.cache.ttl", 604800000L, 604800000L, qf3.a);
    @DexIgnore
    public static yf3<Boolean> T; // = a("measurement.collection.firebase_global_collection_flag_enabled", true, true, ld3.a);
    @DexIgnore
    public static yf3<Boolean> T0; // = a("measurement.service.database_return_empty_collection", true, true, tf3.a);
    @DexIgnore
    public static yf3<Boolean> U; // = a("measurement.collection.efficient_engagement_reporting_enabled_2", true, true, kd3.a);
    @DexIgnore
    public static yf3<Boolean> V; // = a("measurement.collection.redundant_engagement_removal_enabled", false, false, nd3.a);
    @DexIgnore
    public static yf3<Boolean> W; // = a("measurement.client.freeride_engagement_fix", true, true, md3.a);
    @DexIgnore
    public static yf3<Boolean> X; // = a("measurement.experiment.enable_experiment_reporting", true, true, pd3.a);
    @DexIgnore
    public static yf3<Boolean> Y; // = a("measurement.collection.log_event_and_bundle_v2", true, true, od3.a);
    @DexIgnore
    public static yf3<Boolean> Z; // = a("measurement.quality.checksum", false, false, null);
    @DexIgnore
    public static List<yf3<?>> a; // = Collections.synchronizedList(new ArrayList());
    @DexIgnore
    public static yf3<Boolean> a0; // = a("measurement.sdk.dynamite.allow_remote_dynamite2", false, false, rd3.a);
    @DexIgnore
    public static yf3<Long> b;
    @DexIgnore
    public static yf3<Boolean> b0; // = a("measurement.sdk.collection.validate_param_names_alphabetical", true, true, qd3.a);
    @DexIgnore
    public static yf3<Long> c;
    @DexIgnore
    public static yf3<Boolean> c0; // = a("measurement.collection.event_safelist", true, true, td3.a);
    @DexIgnore
    public static yf3<Long> d;
    @DexIgnore
    public static yf3<Boolean> d0; // = a("measurement.service.audience.fix_skip_audience_with_failed_filters", true, true, ud3.a);
    @DexIgnore
    public static yf3<String> e; // = a("measurement.config.url_scheme", Utility.URL_SCHEME, Utility.URL_SCHEME, vc3.a);
    @DexIgnore
    public static yf3<Boolean> e0; // = a("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false, false, xd3.a);
    @DexIgnore
    public static yf3<String> f; // = a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com", jd3.a);
    @DexIgnore
    public static yf3<Boolean> f0; // = a("measurement.audience.refresh_event_count_filters_timestamp", false, false, wd3.a);
    @DexIgnore
    public static yf3<Integer> g; // = a("measurement.upload.max_bundles", 100, 100, sd3.a);
    @DexIgnore
    public static yf3<Boolean> g0; // = a("measurement.audience.use_bundle_timestamp_for_event_count_filters", false, false, zd3.a);
    @DexIgnore
    public static yf3<Integer> h; // = a("measurement.upload.max_batch_size", 65536, 65536, fe3.a);
    @DexIgnore
    public static yf3<Boolean> h0; // = a("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true, true, yd3.a);
    @DexIgnore
    public static yf3<Integer> i; // = a("measurement.upload.max_bundle_size", 65536, 65536, pe3.a);
    @DexIgnore
    public static yf3<Boolean> i0; // = a("measurement.sdk.collection.last_deep_link_referrer2", true, true, be3.a);
    @DexIgnore
    public static yf3<Integer> j; // = a("measurement.upload.max_events_per_bundle", 1000, 1000, cf3.a);
    @DexIgnore
    public static yf3<Boolean> j0; // = a("measurement.sdk.collection.last_deep_link_referrer_campaign2", false, false, ae3.a);
    @DexIgnore
    public static yf3<Integer> k; // = a("measurement.upload.max_events_per_day", 100000, 100000, mf3.a);
    @DexIgnore
    public static yf3<Boolean> k0; // = a("measurement.sdk.collection.last_gclid_from_referrer2", false, false, de3.a);
    @DexIgnore
    public static yf3<Integer> l; // = a("measurement.upload.max_error_events_per_day", 1000, 1000, bc3.a);
    @DexIgnore
    public static yf3<Boolean> l0; // = a("measurement.sdk.collection.enable_extend_user_property_size", true, true, ce3.a);
    @DexIgnore
    public static yf3<Integer> m;
    @DexIgnore
    public static yf3<Boolean> m0; // = a("measurement.upload.file_lock_state_check", false, false, ee3.a);
    @DexIgnore
    public static yf3<Integer> n; // = a("measurement.upload.max_conversions_per_day", 10000, 10000, dc3.a);
    @DexIgnore
    public static yf3<Boolean> n0; // = a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true, true, he3.a);
    @DexIgnore
    public static yf3<Integer> o; // = a("measurement.upload.max_realtime_events_per_day", 10, 10, cc3.a);
    @DexIgnore
    public static yf3<Boolean> o0; // = a("measurement.ga.ga_app_id", false, false, ge3.a);
    @DexIgnore
    public static yf3<Integer> p; // = a("measurement.store.max_stored_events_per_app", 100000, 100000, fc3.a);
    @DexIgnore
    public static yf3<Boolean> p0; // = a("measurement.lifecycle.app_backgrounded_tracking", true, true, ke3.a);
    @DexIgnore
    public static yf3<String> q; // = a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a", ec3.a);
    @DexIgnore
    public static yf3<Boolean> q0; // = a("measurement.lifecycle.app_in_background_parameter", false, false, je3.a);
    @DexIgnore
    public static yf3<Long> r; // = a("measurement.upload.backoff_period", 43200000L, 43200000L, ic3.a);
    @DexIgnore
    public static yf3<Boolean> r0; // = a("measurement.integration.disable_firebase_instance_id", false, false, me3.a);
    @DexIgnore
    public static yf3<Long> s; // = a("measurement.upload.window_interval", 3600000L, 3600000L, hc3.a);
    @DexIgnore
    public static yf3<Boolean> s0; // = a("measurement.lifecycle.app_backgrounded_engagement", false, false, le3.a);
    @DexIgnore
    public static yf3<Long> t; // = a("measurement.upload.interval", 3600000L, 3600000L, kc3.a);
    @DexIgnore
    public static yf3<Boolean> t0; // = a("measurement.collection.service.update_with_analytics_fix", false, false, oe3.a);
    @DexIgnore
    public static yf3<Long> u;
    @DexIgnore
    public static yf3<Boolean> u0; // = a("measurement.service.use_appinfo_modified", false, false, ne3.a);
    @DexIgnore
    public static yf3<Long> v; // = a("measurement.upload.debug_upload_interval", 1000L, 1000L, lc3.a);
    @DexIgnore
    public static yf3<Boolean> v0; // = a("measurement.client.firebase_feature_rollout.v1.enable", true, true, qe3.a);
    @DexIgnore
    public static yf3<Long> w; // = a("measurement.upload.minimum_delay", 500L, 500L, oc3.a);
    @DexIgnore
    public static yf3<Boolean> w0; // = a("measurement.client.sessions.check_on_reset_and_enable2", true, true, se3.a);
    @DexIgnore
    public static yf3<Long> x; // = a("measurement.alarm_manager.minimum_interval", 60000L, 60000L, nc3.a);
    @DexIgnore
    public static yf3<Boolean> x0; // = a("measurement.config.string.always_update_disk_on_set", true, true, re3.a);
    @DexIgnore
    public static yf3<Long> y;
    @DexIgnore
    public static yf3<Boolean> y0; // = a("measurement.scheduler.task_thread.cleanup_on_exit", false, false, ue3.a);
    @DexIgnore
    public static yf3<Long> z; // = a("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L, pc3.a);
    @DexIgnore
    public static yf3<Boolean> z0; // = a("measurement.upload.file_truncate_fix", false, false, te3.a);

    /*
    static {
        Collections.synchronizedSet(new HashSet());
        Long valueOf = Long.valueOf((long) ButtonService.CONNECT_TIMEOUT);
        b = a("measurement.ad_id_cache_time", valueOf, valueOf, zb3.a);
        Long valueOf2 = Long.valueOf((long) LogBuilder.MAX_INTERVAL);
        c = a("measurement.monitoring.sample_period_millis", valueOf2, valueOf2, yb3.a);
        d = a("measurement.config.cache_time", valueOf2, 3600000L, mc3.a);
        Integer valueOf3 = Integer.valueOf((int) SQLiteDatabase.SQLITE_MAX_LIKE_PATTERN_LENGTH);
        m = a("measurement.upload.max_public_events_per_day", valueOf3, valueOf3, ac3.a);
        u = a("measurement.upload.realtime_upload_interval", valueOf, valueOf, jc3.a);
        y = a("measurement.upload.stale_data_deletion_interval", valueOf2, valueOf2, qc3.a);
        Double valueOf4 = Double.valueOf(-3.0d);
        O = a("measurement.test.double_flag", valueOf4, valueOf4, ed3.a);
        a("measurement.service.audience.invalidate_config_cache_after_app_unisntall", true, true, vd3.a);
        a("measurement.collection.synthetic_data_mitigation", false, false, ff3.a);
        a("measurement.service.ssaid_removal", true, true, sf3.a);
        a("measurement.client.consent_state_v1.dev", false, false, vf3.a);
        a("measurement.service.consent_state_v1", false, false, uf3.a);
    }
    */

    @DexIgnore
    public static Map<String, String> a(Context context) {
        fq2 a2 = fq2.a(context.getContentResolver(), uq2.a("com.google.android.gms.measurement"));
        return a2 == null ? Collections.emptyMap() : a2.a();
    }

    @DexIgnore
    public static <V> yf3<V> a(String str, V v2, V v3, wf3<V> wf3) {
        yf3<V> yf3 = new yf3<>(str, v2, v3, wf3);
        a.add(yf3);
        return yf3;
    }
}
