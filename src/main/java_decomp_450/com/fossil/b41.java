package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b41 extends eo0 {
    @DexIgnore
    public UUID[] j; // = new UUID[0];
    @DexIgnore
    public qk1[] k; // = new qk1[0];

    @DexIgnore
    public b41(cx0 cx0) {
        super(aq0.d, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.c();
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return s91 instanceof p01;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.g;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        p01 p01 = (p01) s91;
        this.j = p01.b;
        this.k = p01.c;
    }
}
