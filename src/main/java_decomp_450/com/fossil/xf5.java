package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xf5 {
    @DexIgnore
    public HashMap<Integer, Object> a;

    @DexIgnore
    public xf5() {
        this.a = null;
        this.a = new HashMap<>();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0105, code lost:
        if (r9 <= 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x010c, code lost:
        if (r9 < 192) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x010e, code lost:
        if (r9 > 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x011e, code lost:
        if (r9 <= 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0126, code lost:
        if (r9 < 192) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0128, code lost:
        if (r9 > 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x007a, code lost:
        if (r9 <= 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0082, code lost:
        if (r9 < 192) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0084, code lost:
        if (r9 > 255) goto L_0x0086;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r9, int r10) throws com.fossil.kf5 {
        /*
            r8 = this;
            r0 = 134(0x86, float:1.88E-43)
            r1 = 129(0x81, float:1.81E-43)
            r2 = 192(0xc0, float:2.69E-43)
            r3 = 224(0xe0, float:3.14E-43)
            java.lang.String r4 = "Invalid Octet value!"
            r5 = 128(0x80, float:1.794E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 153(0x99, float:2.14E-43)
            r6 = 255(0xff, float:3.57E-43)
            if (r10 == r0) goto L_0x0112
            r0 = 165(0xa5, float:2.31E-43)
            if (r10 == r0) goto L_0x00f9
            r0 = 167(0xa7, float:2.34E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 169(0xa9, float:2.37E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 171(0xab, float:2.4E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 177(0xb1, float:2.48E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 180(0xb4, float:2.52E-43)
            if (r10 == r0) goto L_0x00f0
            r0 = 191(0xbf, float:2.68E-43)
            if (r10 == r0) goto L_0x00e5
            r0 = 140(0x8c, float:1.96E-43)
            if (r10 == r0) goto L_0x00d8
            r0 = 141(0x8d, float:1.98E-43)
            if (r10 == r0) goto L_0x00cc
            r0 = 148(0x94, float:2.07E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 149(0x95, float:2.09E-43)
            r7 = 135(0x87, float:1.89E-43)
            if (r10 == r0) goto L_0x00c0
            r0 = 155(0x9b, float:2.17E-43)
            if (r10 == r0) goto L_0x00b4
            r0 = 156(0x9c, float:2.19E-43)
            if (r10 == r0) goto L_0x00a6
            r0 = 162(0xa2, float:2.27E-43)
            if (r10 == r0) goto L_0x012c
            r0 = 163(0xa3, float:2.28E-43)
            if (r10 == r0) goto L_0x0098
            switch(r10) {
                case 143: goto L_0x008a;
                case 144: goto L_0x012c;
                case 145: goto L_0x012c;
                case 146: goto L_0x006c;
                default: goto L_0x0055;
            }
        L_0x0055:
            switch(r10) {
                case 186: goto L_0x0060;
                case 187: goto L_0x012c;
                case 188: goto L_0x012c;
                default: goto L_0x0058;
            }
        L_0x0058:
            java.lang.RuntimeException r9 = new java.lang.RuntimeException
            java.lang.String r10 = "Invalid header field!"
            r9.<init>(r10)
            throw r9
        L_0x0060:
            if (r9 < r5) goto L_0x0066
            if (r9 > r7) goto L_0x0066
            goto L_0x0137
        L_0x0066:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x006c:
            r0 = 196(0xc4, float:2.75E-43)
            if (r9 <= r0) goto L_0x0076
            if (r9 >= r3) goto L_0x0076
        L_0x0072:
            r9 = 192(0xc0, float:2.69E-43)
            goto L_0x0137
        L_0x0076:
            r0 = 235(0xeb, float:3.3E-43)
            if (r9 <= r0) goto L_0x007c
            if (r9 <= r6) goto L_0x0086
        L_0x007c:
            if (r9 < r5) goto L_0x0086
            r0 = 136(0x88, float:1.9E-43)
            if (r9 <= r0) goto L_0x0084
            if (r9 < r2) goto L_0x0086
        L_0x0084:
            if (r9 <= r6) goto L_0x0137
        L_0x0086:
            r9 = 224(0xe0, float:3.14E-43)
            goto L_0x0137
        L_0x008a:
            if (r9 < r5) goto L_0x0092
            r0 = 130(0x82, float:1.82E-43)
            if (r9 > r0) goto L_0x0092
            goto L_0x0137
        L_0x0092:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x0098:
            if (r9 < r5) goto L_0x00a0
            r0 = 132(0x84, float:1.85E-43)
            if (r9 > r0) goto L_0x00a0
            goto L_0x0137
        L_0x00a0:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00a6:
            if (r9 < r5) goto L_0x00ae
            r0 = 131(0x83, float:1.84E-43)
            if (r9 > r0) goto L_0x00ae
            goto L_0x0137
        L_0x00ae:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00b4:
            if (r5 == r9) goto L_0x0137
            if (r1 != r9) goto L_0x00ba
            goto L_0x0137
        L_0x00ba:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00c0:
            if (r9 < r5) goto L_0x00c6
            if (r9 > r7) goto L_0x00c6
            goto L_0x0137
        L_0x00c6:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00cc:
            r0 = 16
            if (r9 < r0) goto L_0x00d4
            r0 = 19
            if (r9 <= r0) goto L_0x0137
        L_0x00d4:
            r9 = 18
            goto L_0x0137
        L_0x00d8:
            if (r9 < r5) goto L_0x00df
            r0 = 151(0x97, float:2.12E-43)
            if (r9 > r0) goto L_0x00df
            goto L_0x0137
        L_0x00df:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00e5:
            if (r5 == r9) goto L_0x0137
            if (r1 != r9) goto L_0x00ea
            goto L_0x0137
        L_0x00ea:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00f0:
            if (r5 != r9) goto L_0x00f3
            goto L_0x0137
        L_0x00f3:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x00f9:
            r0 = 193(0xc1, float:2.7E-43)
            if (r9 <= r0) goto L_0x0101
            if (r9 >= r3) goto L_0x0101
            goto L_0x0072
        L_0x0101:
            r0 = 228(0xe4, float:3.2E-43)
            if (r9 <= r0) goto L_0x0108
            if (r9 > r6) goto L_0x0108
            goto L_0x0120
        L_0x0108:
            if (r9 < r5) goto L_0x0086
            if (r9 <= r5) goto L_0x010e
            if (r9 < r2) goto L_0x0086
        L_0x010e:
            if (r9 <= r6) goto L_0x0137
            goto L_0x0086
        L_0x0112:
            r0 = 194(0xc2, float:2.72E-43)
            if (r9 <= r0) goto L_0x011a
            if (r9 >= r3) goto L_0x011a
            goto L_0x0072
        L_0x011a:
            r0 = 227(0xe3, float:3.18E-43)
            if (r9 <= r0) goto L_0x0122
            if (r9 > r6) goto L_0x0122
        L_0x0120:
            goto L_0x0086
        L_0x0122:
            if (r9 < r5) goto L_0x0086
            if (r9 <= r5) goto L_0x0128
            if (r9 < r2) goto L_0x0086
        L_0x0128:
            if (r9 <= r6) goto L_0x0137
            goto L_0x0086
        L_0x012c:
            if (r5 == r9) goto L_0x0137
            if (r1 != r9) goto L_0x0131
            goto L_0x0137
        L_0x0131:
            com.fossil.kf5 r9 = new com.fossil.kf5
            r9.<init>(r4)
            throw r9
        L_0x0137:
            java.util.HashMap<java.lang.Integer, java.lang.Object> r0 = r8.a
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r0.put(r10, r9)
            return
            switch-data {143->0x008a, 144->0x012c, 145->0x012c, 146->0x006c, }
            switch-data {186->0x0060, 187->0x012c, 188->0x012c, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xf5.a(int, int):void");
    }

    @DexIgnore
    public qf5[] b(int i) {
        ArrayList arrayList = (ArrayList) this.a.get(Integer.valueOf(i));
        if (arrayList == null) {
            return null;
        }
        return (qf5[]) arrayList.toArray(new qf5[arrayList.size()]);
    }

    @DexIgnore
    public long c(int i) {
        Long l = (Long) this.a.get(Integer.valueOf(i));
        if (l == null) {
            return -1;
        }
        return l.longValue();
    }

    @DexIgnore
    public int d(int i) {
        Integer num = (Integer) this.a.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    @DexIgnore
    public byte[] e(int i) {
        return (byte[]) this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void b(qf5 qf5, int i) {
        if (qf5 == null) {
            throw null;
        } else if (i == 137 || i == 147 || i == 150 || i == 154 || i == 160 || i == 164 || i == 166 || i == 181 || i == 182) {
            this.a.put(Integer.valueOf(i), qf5);
        } else {
            throw new RuntimeException("Invalid header field!");
        }
    }

    @DexIgnore
    public void a(byte[] bArr, int i) {
        if (bArr != null) {
            if (!(i == 131 || i == 132 || i == 138 || i == 139 || i == 152 || i == 158 || i == 189 || i == 190)) {
                switch (i) {
                    case 183:
                    case 184:
                    case 185:
                        break;
                    default:
                        throw new RuntimeException("Invalid header field!");
                }
            }
            this.a.put(Integer.valueOf(i), bArr);
            return;
        }
        throw null;
    }

    @DexIgnore
    public qf5 a(int i) {
        return (qf5) this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void a(qf5 qf5, int i) {
        if (qf5 == null) {
            throw null;
        } else if (i == 129 || i == 130 || i == 151) {
            ArrayList arrayList = (ArrayList) this.a.get(Integer.valueOf(i));
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(qf5);
            this.a.put(Integer.valueOf(i), arrayList);
        } else {
            throw new RuntimeException("Invalid header field!");
        }
    }

    @DexIgnore
    public void a(long j, int i) {
        if (i == 133 || i == 142 || i == 157 || i == 159 || i == 161 || i == 173 || i == 175 || i == 179 || i == 135 || i == 136) {
            this.a.put(Integer.valueOf(i), Long.valueOf(j));
            return;
        }
        throw new RuntimeException("Invalid header field!");
    }
}
