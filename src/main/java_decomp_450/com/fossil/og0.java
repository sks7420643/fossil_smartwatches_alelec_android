package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum og0 {
    ETA(true),
    TRAVEL_TIME(false),
    DEFAULT(false);
    
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    public og0(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public final boolean a() {
        return this.a;
    }
}
