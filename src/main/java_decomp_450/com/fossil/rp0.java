package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp0 extends pe1 {
    @DexIgnore
    public static /* final */ vn0 CREATOR; // = new vn0(null);
    @DexIgnore
    public /* final */ tn0 b;

    @DexIgnore
    public rp0(tn0 tn0) {
        super(zp1.VIBE);
        this.b = tn0;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.pe1
    public JSONObject a() {
        return yz0.a(super.a(), r51.A2, yz0.a(this.b));
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b.a);
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(rp0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((rp0) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.VibeInstr");
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }

    @DexIgnore
    public /* synthetic */ rp0(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            this.b = tn0.valueOf(readString);
            return;
        }
        ee7.a();
        throw null;
    }
}
