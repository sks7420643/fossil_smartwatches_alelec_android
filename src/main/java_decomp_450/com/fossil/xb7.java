package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xb7 extends ob7 {
    @DexIgnore
    public xb7(fb7<Object> fb7) {
        super(fb7);
        if (fb7 != null) {
            if (!(fb7.getContext() == jb7.INSTANCE)) {
                throw new IllegalArgumentException("Coroutines with restricted suspension must have EmptyCoroutineContext".toString());
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        return jb7.INSTANCE;
    }
}
