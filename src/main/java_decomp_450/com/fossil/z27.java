package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z27 extends v27 {
    @DexIgnore
    public z27(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.v27
    public int a() {
        return 15;
    }

    @DexIgnore
    @Override // com.fossil.v27
    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.getString("_wxapi_join_chatroom_ext_msg");
    }
}
