package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface w1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(r1 r1Var, int i);

        @DexIgnore
        boolean c();

        @DexIgnore
        r1 getItemData();
    }

    @DexIgnore
    void a(p1 p1Var);
}
