package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wo1 extends zk1 {
    @DexIgnore
    public /* final */ boolean U;

    @DexIgnore
    public wo1(ri1 ri1, en0 en0, wm0 wm0, HashMap<xf0, Object> hashMap, String str) {
        super(ri1, en0, wm0, gq0.b.a(ri1.u, pb1.HARDWARE_LOG), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
        this.U = true;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.U;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public Object d() {
        return r();
    }

    @DexIgnore
    public final byte[][] r() {
        ArrayList<bi1> arrayList = ((zk1) this).H;
        ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(((bi1) it.next()).e);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wo1(ri1 ri1, en0 en0, HashMap hashMap, String str, int i) {
        this(ri1, en0, wm0.n, (i & 4) != 0 ? new HashMap() : hashMap, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str);
    }
}
