package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fi3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wm3 a;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 b;
    @DexIgnore
    public /* final */ /* synthetic */ ph3 c;

    @DexIgnore
    public fi3(ph3 ph3, wm3 wm3, nm3 nm3) {
        this.c = ph3;
        this.a = wm3;
        this.b = nm3;
    }

    @DexIgnore
    public final void run() {
        this.c.a.s();
        if (this.a.c.zza() == null) {
            this.c.a.b(this.a, this.b);
        } else {
            this.c.a.a(this.a, this.b);
        }
    }
}
