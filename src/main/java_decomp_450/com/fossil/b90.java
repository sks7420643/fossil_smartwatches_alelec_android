package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum b90 {
    KCAL(0),
    CAL(1);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final b90 a(int i) {
            b90[] values = b90.values();
            for (b90 b90 : values) {
                if (b90.a() == i) {
                    return b90;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public b90(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
