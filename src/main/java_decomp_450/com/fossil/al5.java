package com.fossil;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al5 extends Transition {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ String[] b; // = {"transition:textResize"};
    @DexIgnore
    public static /* final */ a c; // = new a(Float.TYPE, "textSize");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Property<TextView, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(TextView textView) {
            ee7.b(textView, "textView");
            return Float.valueOf(textView.getTextSize());
        }

        @DexIgnore
        /* renamed from: a */
        public void set(TextView textView, Float f) {
            ee7.b(textView, "textView");
            if (f != null) {
                textView.setTextSize(0, f.floatValue());
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new b(null);
        String simpleName = al5.class.getSimpleName();
        ee7.a((Object) simpleName, "TextResizeTransition::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final void a(TransitionValues transitionValues) {
        View view = transitionValues.view;
        if (!(view instanceof TextView)) {
            return;
        }
        if (view != null) {
            TextView textView = (TextView) view;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "captureValues textView.textSize = " + textView.getTextSize());
            Map map = transitionValues.values;
            ee7.a((Object) map, "transitionValues.values");
            map.put("transition:textResize", Float.valueOf(textView.getTextSize()));
            return;
        }
        throw new x87("null cannot be cast to non-null type android.widget.TextView");
    }

    @DexIgnore
    public void captureEndValues(TransitionValues transitionValues) {
        ee7.b(transitionValues, "transitionValues");
        FLogger.INSTANCE.getLocal().d(a, "captureEndValues()");
        a(transitionValues);
    }

    @DexIgnore
    public void captureStartValues(TransitionValues transitionValues) {
        ee7.b(transitionValues, "transitionValues");
        FLogger.INSTANCE.getLocal().d(a, "captureStartValues()");
        a(transitionValues);
    }

    @DexIgnore
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        ee7.b(viewGroup, "sceneRoot");
        if (transitionValues == null || transitionValues2 == null || transitionValues.values.get("transition:textResize") == null || transitionValues2.values.get("transition:textResize") == null) {
            return null;
        }
        Object obj = transitionValues.values.get("transition:textResize");
        if (obj != null) {
            float floatValue = ((Float) obj).floatValue();
            Object obj2 = transitionValues2.values.get("transition:textResize");
            if (obj2 != null) {
                float floatValue2 = ((Float) obj2).floatValue();
                if (floatValue == floatValue2) {
                    return null;
                }
                View view = transitionValues2.view;
                if (view != null) {
                    TextView textView = (TextView) view;
                    textView.setTextSize(0, floatValue2);
                    return ObjectAnimator.ofFloat(textView, c, floatValue, floatValue2);
                }
                throw new x87("null cannot be cast to non-null type android.widget.TextView");
            }
            throw new x87("null cannot be cast to non-null type kotlin.Float");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Float");
    }

    @DexIgnore
    public String[] getTransitionProperties() {
        return b;
    }
}
