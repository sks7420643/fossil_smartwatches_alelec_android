package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ag0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jf0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(dc0.class.getClassLoader());
            if (readParcelable != null) {
                return new jf0((dc0) readParcelable, (df0) parcel.readParcelable(df0.class.getClassLoader()), ag0.values()[parcel.readInt()]);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jf0[] newArray(int i) {
            return new jf0[i];
        }
    }

    @DexIgnore
    public jf0(dc0 dc0, df0 df0, ag0 ag0) {
        super(dc0, df0);
        this.c = ag0;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Constants.RING_MY_PHONE, new JSONObject().put(Constants.RESULT, yz0.a(this.c)));
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(super.b(), r51.l1, yz0.a(this.c));
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(jf0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((jf0) obj).c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingPhoneData");
    }

    @DexIgnore
    public final ag0 getState() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
    }

    @DexIgnore
    public jf0(dc0 dc0, ag0 ag0) {
        super(dc0, null);
        this.c = ag0;
    }

    @DexIgnore
    public jf0(ag0 ag0) {
        this(null, null, ag0);
    }
}
