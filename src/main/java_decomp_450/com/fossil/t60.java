package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t60 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ qi0 a;
    @DexIgnore
    public /* final */ v60[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<t60> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final t60[] a(byte[] bArr) throws IllegalArgumentException {
            t60 t60;
            w60 w60;
            f70 f70;
            x60 x60;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < (bArr.length - 1) - 2) {
                qi0 a = qi0.e.a(bArr[i]);
                int i2 = i + 1;
                int i3 = i2 + 2;
                ByteBuffer order = ByteBuffer.wrap(s97.a(bArr, i2, i3)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
                int b = yz0.b(order.getShort());
                v60[] a2 = v60.CREATOR.a(s97.a(bArr, i3, i3 + b));
                int length = a2.length;
                int i4 = 0;
                while (true) {
                    t60 = null;
                    if (i4 >= length) {
                        w60 = null;
                        break;
                    }
                    w60 = a2[i4];
                    if (w60 instanceof w60) {
                        break;
                    }
                    i4++;
                }
                w60 w602 = w60;
                if (w602 != null) {
                    int length2 = a2.length;
                    int i5 = 0;
                    while (true) {
                        if (i5 >= length2) {
                            f70 = null;
                            break;
                        }
                        f70 = a2[i5];
                        if (f70 instanceof f70) {
                            break;
                        }
                        i5++;
                    }
                    f70 f702 = f70;
                    int length3 = a2.length;
                    int i6 = 0;
                    while (true) {
                        if (i6 >= length3) {
                            x60 = null;
                            break;
                        }
                        x60 = a2[i6];
                        if (x60 instanceof x60) {
                            break;
                        }
                        i6++;
                    }
                    x60 x602 = x60;
                    if (a != null) {
                        int i7 = oq1.a[a.ordinal()];
                        if (i7 != 1) {
                            if (i7 != 2) {
                                throw new p87();
                            } else if (w602 instanceof z60) {
                                t60 = new a70((z60) w602, f702, x602);
                            } else if (w602 instanceof d70) {
                                t60 = new e70((d70) w602, f702, x602);
                            }
                        } else if (w602 instanceof z60) {
                            t60 = new y60((z60) w602, f702, x602);
                        } else if (w602 instanceof d70) {
                            t60 = new c70((d70) w602, f702, x602);
                        }
                    }
                    if (t60 != null) {
                        arrayList.add(t60);
                    }
                    i += b + 3;
                } else {
                    throw new IllegalArgumentException("Fire Time is required.");
                }
            }
            Object[] array = arrayList.toArray(new t60[0]);
            if (array != null) {
                return (t60[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public t60[] newArray(int i) {
            return new t60[i];
        }

        @DexIgnore
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:47:0x0038 */
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:49:0x004c */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r9v2, types: [com.fossil.v60[]] */
        /* JADX WARN: Type inference failed for: r5v0 */
        /* JADX WARN: Type inference failed for: r4v6 */
        /* JADX WARN: Type inference failed for: r6v6 */
        /* JADX WARNING: Unknown variable types count: 2 */
        @Override // android.os.Parcelable.Creator
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.fossil.t60 createFromParcel(android.os.Parcel r9) {
            /*
                r8 = this;
                com.fossil.qi0[] r0 = com.fossil.qi0.values()
                int r1 = r9.readInt()
                r0 = r0[r1]
                com.fossil.v60$a r1 = com.fossil.v60.CREATOR
                java.lang.Object[] r9 = r9.createTypedArray(r1)
                r1 = 0
                if (r9 == 0) goto L_0x00a0
                java.lang.String r2 = "parcel.createTypedArray(AlarmSubEntry.CREATOR)!!"
                com.fossil.ee7.a(r9, r2)
                com.fossil.v60[] r9 = (com.fossil.v60[]) r9
                int r2 = r9.length
                r3 = 0
                r4 = 0
            L_0x001d:
                if (r4 >= r2) goto L_0x0098
                r5 = r9[r4]
                boolean r6 = r5 instanceof com.fossil.w60
                if (r6 == 0) goto L_0x0095
                if (r5 == 0) goto L_0x008d
                com.fossil.w60 r5 = (com.fossil.w60) r5
                int r2 = r9.length
                r4 = 0
            L_0x002b:
                if (r4 >= r2) goto L_0x0037
                r6 = r9[r4]
                boolean r7 = r6 instanceof com.fossil.f70
                if (r7 == 0) goto L_0x0034
                goto L_0x0038
            L_0x0034:
                int r4 = r4 + 1
                goto L_0x002b
            L_0x0037:
                r6 = r1
            L_0x0038:
                if (r6 == 0) goto L_0x003d
                com.fossil.f70 r6 = (com.fossil.f70) r6
                goto L_0x003e
            L_0x003d:
                r6 = r1
            L_0x003e:
                int r2 = r9.length
            L_0x003f:
                if (r3 >= r2) goto L_0x004b
                r4 = r9[r3]
                boolean r7 = r4 instanceof com.fossil.x60
                if (r7 == 0) goto L_0x0048
                goto L_0x004c
            L_0x0048:
                int r3 = r3 + 1
                goto L_0x003f
            L_0x004b:
                r4 = r1
            L_0x004c:
                if (r4 == 0) goto L_0x0051
                r1 = r4
                com.fossil.x60 r1 = (com.fossil.x60) r1
            L_0x0051:
                int[] r9 = com.fossil.oq1.b
                int r0 = r0.ordinal()
                r9 = r9[r0]
                r0 = 1
                if (r9 == r0) goto L_0x0079
                r0 = 2
                if (r9 != r0) goto L_0x0073
                boolean r9 = r5 instanceof com.fossil.z60
                if (r9 == 0) goto L_0x006b
                com.fossil.a70 r9 = new com.fossil.a70
                com.fossil.z60 r5 = (com.fossil.z60) r5
                r9.<init>(r5, r6, r1)
                goto L_0x008c
            L_0x006b:
                com.fossil.e70 r9 = new com.fossil.e70
                com.fossil.d70 r5 = (com.fossil.d70) r5
                r9.<init>(r5, r6, r1)
                goto L_0x008c
            L_0x0073:
                com.fossil.p87 r9 = new com.fossil.p87
                r9.<init>()
                throw r9
            L_0x0079:
                boolean r9 = r5 instanceof com.fossil.z60
                if (r9 == 0) goto L_0x0085
                com.fossil.y60 r9 = new com.fossil.y60
                com.fossil.z60 r5 = (com.fossil.z60) r5
                r9.<init>(r5, r6, r1)
                goto L_0x008c
            L_0x0085:
                com.fossil.c70 r9 = new com.fossil.c70
                com.fossil.d70 r5 = (com.fossil.d70) r5
                r9.<init>(r5, r6, r1)
            L_0x008c:
                return r9
            L_0x008d:
                com.fossil.x87 r9 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime"
                r9.<init>(r0)
                throw r9
            L_0x0095:
                int r4 = r4 + 1
                goto L_0x001d
            L_0x0098:
                java.util.NoSuchElementException r9 = new java.util.NoSuchElementException
                java.lang.String r0 = "Array contains no element matching the predicate."
                r9.<init>(r0)
                throw r9
            L_0x00a0:
                com.fossil.ee7.a()
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.t60.a.createFromParcel(android.os.Parcel):com.fossil.t60");
        }
    }

    @DexIgnore
    public t60(qi0 qi0, v60[] v60Arr) {
        this.a = qi0;
        this.b = v60Arr;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(new JSONObject(), r51.d, yz0.a(this.a));
        for (v60 v60 : this.b) {
            yz0.a(a2, v60.a(), false, 2);
        }
        return a2;
    }

    @DexIgnore
    public final byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (v60 v60 : this.b) {
            byteArrayOutputStream.write(v60.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "byteArrayOutputStreamWriter.toByteArray()");
        byte[] array = ByteBuffer.allocate(byteArray.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.a.a).putShort((short) byteArray.length).put(byteArray).array();
        ee7.a((Object) array, "ByteBuffer.allocate(ENTR\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final v60[] c() {
        return this.b;
    }

    @DexIgnore
    public final qi0 d() {
        return this.a;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            u60 u60 = (u60) obj;
            return this.a == u60.d() && r97.a(this.b, u60.c());
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.Alarm");
    }

    @DexIgnore
    public w60 getFireTime() {
        v60[] v60Arr = this.b;
        int length = v60Arr.length;
        int i = 0;
        while (i < length) {
            v60 v60 = v60Arr[i];
            if (!(v60 instanceof f70)) {
                i++;
            } else if (v60 != null) {
                return (w60) v60;
            } else {
                throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public final x60 getMessage() {
        x60 x60;
        v60[] v60Arr = this.b;
        int length = v60Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                x60 = null;
                break;
            }
            x60 = v60Arr[i];
            if (x60 instanceof x60) {
                break;
            }
            i++;
        }
        return x60;
    }

    @DexIgnore
    public final f70 getTitle() {
        f70 f70;
        v60[] v60Arr = this.b;
        int length = v60Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                f70 = null;
                break;
            }
            f70 = v60Arr[i];
            if (f70 instanceof f70) {
                break;
            }
            i++;
        }
        return f70;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + q97.a(this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }
}
