package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ o85 r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;

    @DexIgnore
    public q85(Object obj, View view, int i, ConstraintLayout constraintLayout, o85 o85, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = o85;
        a((ViewDataBinding) o85);
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
    }

    @DexIgnore
    public static q85 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static q85 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (q85) ViewDataBinding.a(layoutInflater, 2131558663, viewGroup, z, obj);
    }
}
