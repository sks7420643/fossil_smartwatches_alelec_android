package com.fossil;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt0 extends f01 {
    @DexIgnore
    public static /* final */ nr0 CREATOR; // = new nr0(null);
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;

    @DexIgnore
    public jt0(rg0 rg0, r60 r60, int i, int i2) {
        super(rg0, r60);
        this.c = false;
        this.d = 0;
        this.e = i;
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.f01
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.u3, Integer.valueOf(this.c ? 1 : 0)), r51.v3, Integer.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.f01
    public List<pe1> b() {
        short s;
        int i;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cm1());
        if (this.c) {
            arrayList.add(new lr0(new e91[]{new e91(di0.b, ei1.c, ak0.POSITION, xl0.b, 0), new e91(di0.c, ei1.c, ak0.POSITION, xl0.b, 0)}));
        }
        arrayList.add(new my0(0.5d));
        arrayList.add(new rp0(tn0.b));
        di0 di0 = di0.b;
        ei1 ei1 = ei1.b;
        ak0 c2 = c();
        xl0 xl0 = xl0.b;
        if (d()) {
            s = (short) (((this.f * 30) / 60) + (this.e * 30));
        } else {
            int i2 = this.d;
            s = (short) (i2 >= 60 ? (i2 / 60) * 30 : i2 * 6);
        }
        e91 e91 = new e91(di0, ei1, c2, xl0, s);
        di0 di02 = di0.c;
        ei1 ei12 = ei1.b;
        ak0 c3 = c();
        xl0 xl02 = xl0.b;
        if (d()) {
            i = this.f;
        } else {
            i = this.d;
            if (i >= 60) {
                i %= 60;
            }
        }
        arrayList.add(new lr0(new e91[]{e91, new e91(di02, ei12, c3, xl02, (short) (i * 6))}));
        arrayList.add(new my0(8.0d));
        arrayList.add(new cv0());
        return arrayList;
    }

    @DexIgnore
    public final ak0 c() {
        if (d()) {
            return ak0.POSITION;
        }
        if (this.d >= 60) {
            return ak0.POSITION;
        }
        return ak0.DISTANCE;
    }

    @DexIgnore
    public final boolean d() {
        return this.d == 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.f01
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(jt0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            jt0 jt0 = (jt0) obj;
            return this.c == jt0.c && this.d == jt0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse");
    }

    @DexIgnore
    @Override // com.fossil.f01
    public int hashCode() {
        return (Boolean.valueOf(this.c).hashCode() * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.f01
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public jt0(rg0 rg0, r60 r60, boolean z, int i) {
        super(rg0, r60);
        this.c = z;
        this.d = i;
    }

    @DexIgnore
    public /* synthetic */ jt0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.c = parcel.readInt() != 0;
        this.d = parcel.readInt();
    }
}
