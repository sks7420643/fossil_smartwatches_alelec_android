package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an5 extends fl4<b, fl4.d, fl4.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ FitnessDataRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            ee7.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries", f = "FetchDailyHeartRateSummaries.kt", l = {27, 55, 57}, m = "run")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ an5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(an5 an5, fb7 fb7) {
            super(fb7);
            this.this$0 = an5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<? super i97>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = an5.class.getSimpleName();
        ee7.a((Object) simpleName, "FetchDailyHeartRateSumma\u2026es::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public an5(HeartRateSummaryRepository heartRateSummaryRepository, UserRepository userRepository, FitnessDataRepository fitnessDataRepository) {
        ee7.b(heartRateSummaryRepository, "mHeartRateSummariesRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        this.d = heartRateSummaryRepository;
        this.e = userRepository;
        this.f = fitnessDataRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.an5.b r18, com.fossil.fb7<? super com.fossil.i97> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.fossil.an5.c
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.an5$c r3 = (com.fossil.an5.c) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.an5$c r3 = new com.fossil.an5$c
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 3
            r7 = 2
            r8 = 1
            if (r5 == 0) goto L_0x009a
            if (r5 == r8) goto L_0x0087
            if (r5 == r7) goto L_0x0062
            if (r5 != r6) goto L_0x005a
            java.lang.Object r1 = r3.L$8
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$7
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$6
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$5
            java.util.Calendar r1 = (java.util.Calendar) r1
            java.lang.Object r1 = r3.L$4
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$3
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r1 = r3.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$1
            com.fossil.an5$b r1 = (com.fossil.an5.b) r1
            java.lang.Object r1 = r3.L$0
            com.fossil.an5 r1 = (com.fossil.an5) r1
            com.fossil.t87.a(r2)
            goto L_0x01a8
        L_0x005a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0062:
            java.lang.Object r1 = r3.L$7
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r5 = r3.L$6
            java.util.Date r5 = (java.util.Date) r5
            java.lang.Object r7 = r3.L$5
            java.util.Calendar r7 = (java.util.Calendar) r7
            java.lang.Object r8 = r3.L$4
            java.util.Date r8 = (java.util.Date) r8
            java.lang.Object r9 = r3.L$3
            com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
            java.lang.Object r10 = r3.L$2
            java.util.Date r10 = (java.util.Date) r10
            java.lang.Object r11 = r3.L$1
            com.fossil.an5$b r11 = (com.fossil.an5.b) r11
            java.lang.Object r12 = r3.L$0
            com.fossil.an5 r12 = (com.fossil.an5) r12
            com.fossil.t87.a(r2)
            goto L_0x0182
        L_0x0087:
            java.lang.Object r1 = r3.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r5 = r3.L$1
            com.fossil.an5$b r5 = (com.fossil.an5.b) r5
            java.lang.Object r9 = r3.L$0
            com.fossil.an5 r9 = (com.fossil.an5) r9
            com.fossil.t87.a(r2)
            r10 = r1
            r1 = r5
            r12 = r9
            goto L_0x00da
        L_0x009a:
            com.fossil.t87.a(r2)
            if (r1 != 0) goto L_0x00a2
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x00a2:
            java.util.Date r2 = r18.a()
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r9 = com.fossil.an5.g
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "executeUseCase - date="
            r10.append(r11)
            java.lang.String r11 = com.fossil.qy6.a(r2)
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r5.d(r9, r10)
            com.portfolio.platform.data.source.UserRepository r5 = r0.e
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r2
            r3.label = r8
            java.lang.Object r5 = r5.getCurrentUser(r3)
            if (r5 != r4) goto L_0x00d7
            return r4
        L_0x00d7:
            r12 = r0
            r10 = r2
            r2 = r5
        L_0x00da:
            r9 = r2
            com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
            if (r9 == 0) goto L_0x01b0
            java.lang.String r2 = r9.getCreatedAt()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x00eb
            goto L_0x01b0
        L_0x00eb:
            java.lang.String r2 = r9.getCreatedAt()
            if (r2 == 0) goto L_0x01ab
            java.util.Date r2 = com.fossil.zd5.d(r2)
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            java.lang.String r11 = "calendar"
            com.fossil.ee7.a(r5, r11)
            r5.setTime(r10)
            r11 = 5
            r5.set(r11, r8)
            long r13 = r5.getTimeInMillis()
            java.lang.String r8 = "createdDate"
            com.fossil.ee7.a(r2, r8)
            long r6 = r2.getTime()
            boolean r6 = com.fossil.zd5.a(r13, r6)
            if (r6 == 0) goto L_0x011a
            r6 = r2
            goto L_0x0135
        L_0x011a:
            java.util.Calendar r6 = com.fossil.zd5.e(r5)
            java.lang.String r7 = "DateHelper.getStartOfMonth(calendar)"
            com.fossil.ee7.a(r6, r7)
            java.util.Date r6 = r6.getTime()
            java.lang.String r7 = "DateHelper.getStartOfMonth(calendar).time"
            com.fossil.ee7.a(r6, r7)
            boolean r7 = com.fossil.zd5.b(r2, r6)
            if (r7 == 0) goto L_0x0135
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0135:
            java.lang.Boolean r7 = com.fossil.zd5.v(r6)
            java.lang.String r11 = "DateHelper.isThisMonth(startDate)"
            com.fossil.ee7.a(r7, r11)
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x014a
            java.util.Date r7 = new java.util.Date
            r7.<init>()
            goto L_0x015c
        L_0x014a:
            java.util.Calendar r7 = com.fossil.zd5.m(r6)
            java.lang.String r11 = "DateHelper.getEndOfMonth(startDate)"
            com.fossil.ee7.a(r7, r11)
            java.util.Date r7 = r7.getTime()
            java.lang.String r11 = "DateHelper.getEndOfMonth(startDate).time"
            com.fossil.ee7.a(r7, r11)
        L_0x015c:
            com.portfolio.platform.data.source.FitnessDataRepository r11 = r12.f
            r3.L$0 = r12
            r3.L$1 = r1
            r3.L$2 = r10
            r3.L$3 = r9
            r3.L$4 = r2
            r3.L$5 = r5
            r3.L$6 = r7
            r3.L$7 = r6
            r8 = 2
            r3.label = r8
            java.lang.Object r8 = r11.getFitnessData(r6, r7, r3)
            if (r8 != r4) goto L_0x0178
            return r4
        L_0x0178:
            r11 = r1
            r1 = r6
            r15 = r8
            r8 = r2
            r2 = r15
            r16 = r7
            r7 = r5
            r5 = r16
        L_0x0182:
            java.util.List r2 = (java.util.List) r2
            boolean r6 = r2.isEmpty()
            if (r6 == 0) goto L_0x01a8
            com.portfolio.platform.data.source.HeartRateSummaryRepository r6 = r12.d
            r3.L$0 = r12
            r3.L$1 = r11
            r3.L$2 = r10
            r3.L$3 = r9
            r3.L$4 = r8
            r3.L$5 = r7
            r3.L$6 = r5
            r3.L$7 = r1
            r3.L$8 = r2
            r2 = 3
            r3.label = r2
            java.lang.Object r1 = r6.loadSummaries(r1, r5, r3)
            if (r1 != r4) goto L_0x01a8
            return r4
        L_0x01a8:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x01ab:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x01b0:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.an5.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "executeUseCase - FAILED!!! with user="
            r3.append(r4)
            r3.append(r9)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.an5.a(com.fossil.an5$b, com.fossil.fb7):java.lang.Object");
    }
}
