package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ve0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ f90 b; // = f90.DEFAULT;
    @DexIgnore
    public /* final */ f90 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ve0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final f90 a() {
            return ve0.b;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ve0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                return new ve0(f90.valueOf(readString));
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ve0[] newArray(int i) {
            return new ve0[i];
        }
    }

    @DexIgnore
    public ve0(f90 f90) {
        this.a = f90;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("font_color", yz0.a(this.a));
        ee7.a((Object) put, "JSONObject()\n           \u2026 fontColor.lowerCaseName)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ve0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((ve0) obj).a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig");
    }

    @DexIgnore
    public final f90 getFontColor() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
    }
}
