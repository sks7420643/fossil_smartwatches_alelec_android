package com.fossil;

import com.fossil.ql4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rl4 {
    @DexIgnore
    public /* final */ sl4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends ql4.c, E extends ql4.a> implements ql4.d<R, E> {
        @DexIgnore
        public /* final */ ql4.d<R, E> a;
        @DexIgnore
        public /* final */ rl4 b;

        @DexIgnore
        public a(ql4.d<R, E> dVar, rl4 rl4) {
            this.a = dVar;
            this.b = rl4;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(R r) {
            this.b.a(r, this.a);
        }

        @DexIgnore
        public void a(E e) {
            this.b.a(e, this.a);
        }
    }

    @DexIgnore
    public rl4(sl4 sl4) {
        this.a = sl4;
    }

    @DexIgnore
    public <Q extends ql4.b, R extends ql4.c, E extends ql4.a> void a(ql4<Q, R, E> ql4, Q q, ql4.d<R, E> dVar) {
        ql4.b(q);
        ql4.a(new a(dVar, this));
        cx6.c();
        this.a.execute(new oj4(ql4));
    }

    @DexIgnore
    public static /* synthetic */ void a(ql4 ql4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UseCaseHandler", "execute: getIdlingResource = " + cx6.b());
        ql4.b();
        if (!cx6.b().a()) {
            cx6.a();
        }
    }

    @DexIgnore
    public <R extends ql4.c, E extends ql4.a> void a(R r, ql4.d<R, E> dVar) {
        this.a.a(r, dVar);
    }

    @DexIgnore
    public <R extends ql4.c, E extends ql4.a> void a(E e, ql4.d<R, E> dVar) {
        this.a.a(e, dVar);
    }
}
