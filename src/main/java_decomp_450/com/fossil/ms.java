package com.fossil;

import android.util.Log;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms extends is {
    @DexIgnore
    public static /* final */ File b; // = new File("/proc/self/fd");
    @DexIgnore
    public static volatile int c;
    @DexIgnore
    public static volatile boolean d; // = true;
    @DexIgnore
    public static /* final */ ms e; // = new ms();

    @DexIgnore
    public ms() {
        super(null);
    }

    @DexIgnore
    @Override // com.fossil.is
    public boolean a(rt rtVar) {
        ee7.b(rtVar, "size");
        if (rtVar instanceof ot) {
            ot otVar = (ot) rtVar;
            if (otVar.d() < 100 || otVar.c() < 100) {
                return false;
            }
        }
        return a();
    }

    @DexIgnore
    public final synchronized boolean a() {
        int i = c;
        c = i + 1;
        if (i >= 50) {
            boolean z = false;
            c = 0;
            String[] list = b.list();
            if (list == null) {
                list = new String[0];
            }
            int length = list.length;
            if (length < 750) {
                z = true;
            }
            d = z;
            if (d && cu.c.a() && cu.c.b() <= 5) {
                Log.println(5, "LimitedFileDescriptorHardwareBitmapService", "Unable to allocate more hardware bitmaps. Number of used file descriptors: " + length);
            }
        }
        return d;
    }
}
