package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq implements uq {
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ yq<b, Bitmap> b; // = new yq<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Bitmap.Config c;

        @DexIgnore
        public b(int i, int i2, Bitmap.Config config) {
            ee7.b(config, "config");
            this.a = i;
            this.b = i2;
            this.c = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && this.b == bVar.b && ee7.a(this.c, bVar.c);
        }

        @DexIgnore
        public int hashCode() {
            int i = ((this.a * 31) + this.b) * 31;
            Bitmap.Config config = this.c;
            return i + (config != null ? config.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            a aVar = tq.c;
            int i = this.a;
            int i2 = this.b;
            Bitmap.Config config = this.c;
            return '[' + i + " x " + i2 + "], " + config;
        }
    }

    @DexIgnore
    @Override // com.fossil.uq
    public void a(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        yq<b, Bitmap> yqVar = this.b;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        ee7.a((Object) config, "bitmap.config");
        yqVar.a(new b(width, height, config), bitmap);
    }

    @DexIgnore
    @Override // com.fossil.uq
    public String b(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        ee7.a((Object) config, "bitmap.config");
        return '[' + width + " x " + height + "], " + config;
    }

    @DexIgnore
    @Override // com.fossil.uq
    public Bitmap removeLast() {
        return this.b.a();
    }

    @DexIgnore
    public String toString() {
        return "AttributeStrategy: groupedMap=" + this.b;
    }

    @DexIgnore
    @Override // com.fossil.uq
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        ee7.b(config, "config");
        return this.b.a(new b(i, i2, config));
    }

    @DexIgnore
    @Override // com.fossil.uq
    public String b(int i, int i2, Bitmap.Config config) {
        ee7.b(config, "config");
        return '[' + i + " x " + i2 + "], " + config;
    }
}
