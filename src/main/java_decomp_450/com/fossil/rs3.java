package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.iu3;
import java.lang.ref.WeakReference;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rs3 extends dv3 implements q7, Drawable.Callback, iu3.b {
    @DexIgnore
    public static /* final */ int[] K0; // = {16842910};
    @DexIgnore
    public static /* final */ ShapeDrawable L0; // = new ShapeDrawable(new OvalShape());
    @DexIgnore
    public ColorStateList A0;
    @DexIgnore
    public ColorStateList B;
    @DexIgnore
    public PorterDuff.Mode B0; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public ColorStateList C;
    @DexIgnore
    public int[] C0;
    @DexIgnore
    public float D;
    @DexIgnore
    public boolean D0;
    @DexIgnore
    public float E;
    @DexIgnore
    public ColorStateList E0;
    @DexIgnore
    public ColorStateList F;
    @DexIgnore
    public WeakReference<a> F0;
    @DexIgnore
    public float G;
    @DexIgnore
    public TextUtils.TruncateAt G0;
    @DexIgnore
    public ColorStateList H;
    @DexIgnore
    public boolean H0;
    @DexIgnore
    public CharSequence I;
    @DexIgnore
    public int I0;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public boolean J0;
    @DexIgnore
    public Drawable K;
    @DexIgnore
    public ColorStateList L;
    @DexIgnore
    public float M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public Drawable P;
    @DexIgnore
    public Drawable Q;
    @DexIgnore
    public ColorStateList R;
    @DexIgnore
    public float S;
    @DexIgnore
    public CharSequence T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public Drawable W;
    @DexIgnore
    public bs3 X;
    @DexIgnore
    public bs3 Y;
    @DexIgnore
    public float Z;
    @DexIgnore
    public float a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public /* final */ Context h0;
    @DexIgnore
    public /* final */ Paint i0; // = new Paint(1);
    @DexIgnore
    public /* final */ Paint j0;
    @DexIgnore
    public /* final */ Paint.FontMetrics k0; // = new Paint.FontMetrics();
    @DexIgnore
    public /* final */ RectF l0; // = new RectF();
    @DexIgnore
    public /* final */ PointF m0; // = new PointF();
    @DexIgnore
    public /* final */ Path n0; // = new Path();
    @DexIgnore
    public /* final */ iu3 o0;
    @DexIgnore
    public int p0;
    @DexIgnore
    public int q0;
    @DexIgnore
    public int r0;
    @DexIgnore
    public int s0;
    @DexIgnore
    public int t0;
    @DexIgnore
    public int u0;
    @DexIgnore
    public boolean v0;
    @DexIgnore
    public int w0;
    @DexIgnore
    public int x0; // = 255;
    @DexIgnore
    public ColorFilter y0;
    @DexIgnore
    public PorterDuffColorFilter z0;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public rs3(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Paint paint = null;
        this.F0 = new WeakReference<>(null);
        a(context);
        this.h0 = context;
        iu3 iu3 = new iu3(this);
        this.o0 = iu3;
        this.I = "";
        iu3.b().density = context.getResources().getDisplayMetrics().density;
        this.j0 = null;
        if (0 != 0) {
            paint.setStyle(Paint.Style.STROKE);
        }
        setState(K0);
        b(K0);
        this.H0 = true;
        if (uu3.a) {
            L0.setTint(-1);
        }
    }

    @DexIgnore
    public static rs3 a(Context context, AttributeSet attributeSet, int i, int i2) {
        rs3 rs3 = new rs3(context, attributeSet, i, i2);
        rs3.a(attributeSet, i, i2);
        return rs3;
    }

    @DexIgnore
    public void A(int i) {
        this.I0 = i;
    }

    @DexIgnore
    public void B(int i) {
        h(t0.b(this.h0, i));
    }

    @DexIgnore
    public void C(int i) {
        b(bs3.a(this.h0, i));
    }

    @DexIgnore
    public float D() {
        if (r0() || q0()) {
            return this.a0 + this.M + this.b0;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public float E() {
        return s0() ? this.e0 + this.S + this.f0 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float F() {
        this.o0.b().getFontMetrics(this.k0);
        Paint.FontMetrics fontMetrics = this.k0;
        return (fontMetrics.descent + fontMetrics.ascent) / 2.0f;
    }

    @DexIgnore
    public final boolean G() {
        return this.V && this.W != null && this.U;
    }

    @DexIgnore
    public Drawable H() {
        return this.W;
    }

    @DexIgnore
    public ColorStateList I() {
        return this.C;
    }

    @DexIgnore
    public float J() {
        return this.J0 ? q() : this.E;
    }

    @DexIgnore
    public float K() {
        return this.g0;
    }

    @DexIgnore
    public Drawable L() {
        Drawable drawable = this.K;
        if (drawable != null) {
            return p7.h(drawable);
        }
        return null;
    }

    @DexIgnore
    public float M() {
        return this.M;
    }

    @DexIgnore
    public ColorStateList N() {
        return this.L;
    }

    @DexIgnore
    public float O() {
        return this.D;
    }

    @DexIgnore
    public float P() {
        return this.Z;
    }

    @DexIgnore
    public ColorStateList Q() {
        return this.F;
    }

    @DexIgnore
    public float R() {
        return this.G;
    }

    @DexIgnore
    public Drawable S() {
        Drawable drawable = this.P;
        if (drawable != null) {
            return p7.h(drawable);
        }
        return null;
    }

    @DexIgnore
    public CharSequence T() {
        return this.T;
    }

    @DexIgnore
    public float U() {
        return this.f0;
    }

    @DexIgnore
    public float V() {
        return this.S;
    }

    @DexIgnore
    public float W() {
        return this.e0;
    }

    @DexIgnore
    public int[] X() {
        return this.C0;
    }

    @DexIgnore
    public ColorStateList Y() {
        return this.R;
    }

    @DexIgnore
    public TextUtils.TruncateAt Z() {
        return this.G0;
    }

    @DexIgnore
    public bs3 a0() {
        return this.Y;
    }

    @DexIgnore
    public final void b(Canvas canvas, Rect rect) {
        if (!this.J0) {
            this.i0.setColor(this.q0);
            this.i0.setStyle(Paint.Style.FILL);
            this.i0.setColorFilter(j0());
            this.l0.set(rect);
            canvas.drawRoundRect(this.l0, J(), J(), this.i0);
        }
    }

    @DexIgnore
    public float b0() {
        return this.b0;
    }

    @DexIgnore
    public final void c(Canvas canvas, Rect rect) {
        if (r0()) {
            a(rect, this.l0);
            RectF rectF = this.l0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.K.setBounds(0, 0, (int) this.l0.width(), (int) this.l0.height());
            this.K.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public float c0() {
        return this.a0;
    }

    @DexIgnore
    public final void d(Canvas canvas, Rect rect) {
        if (this.G > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && !this.J0) {
            this.i0.setColor(this.s0);
            this.i0.setStyle(Paint.Style.STROKE);
            if (!this.J0) {
                this.i0.setColorFilter(j0());
            }
            RectF rectF = this.l0;
            float f = this.G;
            rectF.set(((float) rect.left) + (f / 2.0f), ((float) rect.top) + (f / 2.0f), ((float) rect.right) - (f / 2.0f), ((float) rect.bottom) - (f / 2.0f));
            float f2 = this.E - (this.G / 2.0f);
            canvas.drawRoundRect(this.l0, f2, f2, this.i0);
        }
    }

    @DexIgnore
    public ColorStateList d0() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.dv3
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && getAlpha() != 0) {
            int i = 0;
            int i2 = this.x0;
            if (i2 < 255) {
                i = ps3.a(canvas, (float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom, i2);
            }
            e(canvas, bounds);
            b(canvas, bounds);
            if (this.J0) {
                super.draw(canvas);
            }
            d(canvas, bounds);
            g(canvas, bounds);
            c(canvas, bounds);
            a(canvas, bounds);
            if (this.H0) {
                i(canvas, bounds);
            }
            f(canvas, bounds);
            h(canvas, bounds);
            if (this.x0 < 255) {
                canvas.restoreToCount(i);
            }
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, Rect rect) {
        if (!this.J0) {
            this.i0.setColor(this.p0);
            this.i0.setStyle(Paint.Style.FILL);
            this.l0.set(rect);
            canvas.drawRoundRect(this.l0, J(), J(), this.i0);
        }
    }

    @DexIgnore
    public bs3 e0() {
        return this.X;
    }

    @DexIgnore
    public void f(boolean z) {
        if (this.D0 != z) {
            this.D0 = z;
            t0();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public CharSequence f0() {
        return this.I;
    }

    @DexIgnore
    public final void g(Canvas canvas, Rect rect) {
        this.i0.setColor(this.t0);
        this.i0.setStyle(Paint.Style.FILL);
        this.l0.set(rect);
        if (!this.J0) {
            canvas.drawRoundRect(this.l0, J(), J(), this.i0);
            return;
        }
        b(new RectF(rect), this.n0);
        super.a(canvas, this.i0, this.n0, e());
    }

    @DexIgnore
    public qu3 g0() {
        return this.o0.a();
    }

    @DexIgnore
    public int getAlpha() {
        return this.x0;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.y0;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return (int) this.D;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return Math.min(Math.round(this.Z + D() + this.c0 + this.o0.a(f0().toString()) + this.d0 + E() + this.g0), this.I0);
    }

    @DexIgnore
    @Override // com.fossil.dv3
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @Override // com.fossil.dv3
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.J0) {
            super.getOutline(outline);
            return;
        }
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.E);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), getIntrinsicHeight(), this.E);
        }
        outline.setAlpha(((float) getAlpha()) / 255.0f);
    }

    @DexIgnore
    public final void h(Canvas canvas, Rect rect) {
        Paint paint = this.j0;
        if (paint != null) {
            paint.setColor(e7.c(-16777216, 127));
            canvas.drawRect(rect, this.j0);
            if (r0() || q0()) {
                a(rect, this.l0);
                canvas.drawRect(this.l0, this.j0);
            }
            if (this.I != null) {
                canvas.drawLine((float) rect.left, rect.exactCenterY(), (float) rect.right, rect.exactCenterY(), this.j0);
            }
            if (s0()) {
                c(rect, this.l0);
                canvas.drawRect(this.l0, this.j0);
            }
            this.j0.setColor(e7.c(-65536, 127));
            b(rect, this.l0);
            canvas.drawRect(this.l0, this.j0);
            this.j0.setColor(e7.c(-16711936, 127));
            d(rect, this.l0);
            canvas.drawRect(this.l0, this.j0);
        }
    }

    @DexIgnore
    public float h0() {
        return this.d0;
    }

    @DexIgnore
    public final void i(Canvas canvas, Rect rect) {
        if (this.I != null) {
            Paint.Align a2 = a(rect, this.m0);
            e(rect, this.l0);
            if (this.o0.a() != null) {
                this.o0.b().drawableState = getState();
                this.o0.a(this.h0);
            }
            this.o0.b().setTextAlign(a2);
            int i = 0;
            boolean z = Math.round(this.o0.a(f0().toString())) > Math.round(this.l0.width());
            if (z) {
                i = canvas.save();
                canvas.clipRect(this.l0);
            }
            CharSequence charSequence = this.I;
            if (z && this.G0 != null) {
                charSequence = TextUtils.ellipsize(charSequence, this.o0.b(), this.l0.width(), this.G0);
            }
            int length = charSequence.length();
            PointF pointF = this.m0;
            canvas.drawText(charSequence, 0, length, pointF.x, pointF.y, this.o0.b());
            if (z) {
                canvas.restoreToCount(i);
            }
        }
    }

    @DexIgnore
    public float i0() {
        return this.c0;
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @DexIgnore
    @Override // com.fossil.dv3
    public boolean isStateful() {
        return i(this.B) || i(this.C) || i(this.F) || (this.D0 && i(this.E0)) || b(this.o0.a()) || G() || f(this.K) || f(this.W) || i(this.A0);
    }

    @DexIgnore
    public void j(float f) {
        if (this.Z != f) {
            this.Z = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public final ColorFilter j0() {
        ColorFilter colorFilter = this.y0;
        return colorFilter != null ? colorFilter : this.z0;
    }

    @DexIgnore
    public void k(float f) {
        if (this.G != f) {
            this.G = f;
            this.i0.setStrokeWidth(f);
            if (this.J0) {
                super.e(f);
            }
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean k0() {
        return this.D0;
    }

    @DexIgnore
    public void l(int i) {
        h(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public boolean l0() {
        return this.U;
    }

    @DexIgnore
    public void m(int i) {
        d(t0.b(this.h0, i));
    }

    @DexIgnore
    public boolean m0() {
        return f(this.P);
    }

    @DexIgnore
    public void n(int i) {
        c(this.h0.getResources().getBoolean(i));
    }

    @DexIgnore
    public boolean n0() {
        return this.O;
    }

    @DexIgnore
    public void o(int i) {
        i(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public void o0() {
        a aVar = this.F0.get();
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public boolean onLayoutDirectionChanged(int i) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i);
        if (r0()) {
            onLayoutDirectionChanged |= p7.a(this.K, i);
        }
        if (q0()) {
            onLayoutDirectionChanged |= p7.a(this.W, i);
        }
        if (s0()) {
            onLayoutDirectionChanged |= p7.a(this.P, i);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        boolean onLevelChange = super.onLevelChange(i);
        if (r0()) {
            onLevelChange |= this.K.setLevel(i);
        }
        if (q0()) {
            onLevelChange |= this.W.setLevel(i);
        }
        if (s0()) {
            onLevelChange |= this.P.setLevel(i);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    @DexIgnore
    @Override // com.fossil.dv3, com.fossil.iu3.b
    public boolean onStateChange(int[] iArr) {
        if (this.J0) {
            super.onStateChange(iArr);
        }
        return a(iArr, X());
    }

    @DexIgnore
    public void p(int i) {
        j(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public boolean p0() {
        return this.H0;
    }

    @DexIgnore
    public void q(int i) {
        e(t0.b(this.h0, i));
    }

    @DexIgnore
    public final boolean q0() {
        return this.V && this.W != null && this.v0;
    }

    @DexIgnore
    public void r(int i) {
        k(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public final boolean r0() {
        return this.J && this.K != null;
    }

    @DexIgnore
    public void s(int i) {
        l(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public final boolean s0() {
        return this.O && this.P != null;
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @DexIgnore
    @Override // com.fossil.dv3
    public void setAlpha(int i) {
        if (this.x0 != i) {
            this.x0 = i;
            invalidateSelf();
        }
    }

    @DexIgnore
    @Override // com.fossil.dv3
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.y0 != colorFilter) {
            this.y0 = colorFilter;
            invalidateSelf();
        }
    }

    @DexIgnore
    @Override // com.fossil.dv3, com.fossil.q7
    public void setTintList(ColorStateList colorStateList) {
        if (this.A0 != colorStateList) {
            this.A0 = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    @Override // com.fossil.dv3, com.fossil.q7
    public void setTintMode(PorterDuff.Mode mode) {
        if (this.B0 != mode) {
            this.B0 = mode;
            this.z0 = qt3.a(this, this.A0, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (r0()) {
            visible |= this.K.setVisible(z, z2);
        }
        if (q0()) {
            visible |= this.W.setVisible(z, z2);
        }
        if (s0()) {
            visible |= this.P.setVisible(z, z2);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    @DexIgnore
    public void t(int i) {
        d(t0.c(this.h0, i));
    }

    @DexIgnore
    public final void t0() {
        this.E0 = this.D0 ? uu3.b(this.H) : null;
    }

    @DexIgnore
    public void u(int i) {
        m(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    @TargetApi(21)
    public final void u0() {
        this.Q = new RippleDrawable(uu3.b(d0()), this.P, L0);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    @DexIgnore
    public void v(int i) {
        n(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public void w(int i) {
        g(t0.b(this.h0, i));
    }

    @DexIgnore
    public void x(int i) {
        a(bs3.a(this.h0, i));
    }

    @DexIgnore
    public void y(int i) {
        o(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public void z(int i) {
        p(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public void l(float f) {
        if (this.f0 != f) {
            this.f0 = f;
            invalidateSelf();
            if (s0()) {
                o0();
            }
        }
    }

    @DexIgnore
    public void m(float f) {
        if (this.S != f) {
            this.S = f;
            invalidateSelf();
            if (s0()) {
                o0();
            }
        }
    }

    @DexIgnore
    public void n(float f) {
        if (this.e0 != f) {
            this.e0 = f;
            invalidateSelf();
            if (s0()) {
                o0();
            }
        }
    }

    @DexIgnore
    public void o(float f) {
        if (this.b0 != f) {
            float D2 = D();
            this.b0 = f;
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void p(float f) {
        if (this.a0 != f) {
            float D2 = D();
            this.a0 = f;
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void q(float f) {
        if (this.d0 != f) {
            this.d0 = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void r(float f) {
        if (this.c0 != f) {
            this.c0 = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void D(int i) {
        a(new qu3(this.h0, i));
    }

    @DexIgnore
    public void E(int i) {
        q(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public void F(int i) {
        r(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet, int i, int i2) {
        TypedArray c = ku3.c(this.h0, attributeSet, tr3.Chip, i, i2, new int[0]);
        this.J0 = c.hasValue(tr3.Chip_shapeAppearance);
        f(pu3.a(this.h0, c, tr3.Chip_chipSurfaceColor));
        c(pu3.a(this.h0, c, tr3.Chip_chipBackgroundColor));
        i(c.getDimension(tr3.Chip_chipMinHeight, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        if (c.hasValue(tr3.Chip_chipCornerRadius)) {
            f(c.getDimension(tr3.Chip_chipCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        }
        e(pu3.a(this.h0, c, tr3.Chip_chipStrokeColor));
        k(c.getDimension(tr3.Chip_chipStrokeWidth, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        h(pu3.a(this.h0, c, tr3.Chip_rippleColor));
        b(c.getText(tr3.Chip_android_text));
        a(pu3.c(this.h0, c, tr3.Chip_android_textAppearance));
        int i3 = c.getInt(tr3.Chip_android_ellipsize, 0);
        if (i3 == 1) {
            a(TextUtils.TruncateAt.START);
        } else if (i3 == 2) {
            a(TextUtils.TruncateAt.MIDDLE);
        } else if (i3 == 3) {
            a(TextUtils.TruncateAt.END);
        }
        c(c.getBoolean(tr3.Chip_chipIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconVisible") != null)) {
            c(c.getBoolean(tr3.Chip_chipIconEnabled, false));
        }
        c(pu3.b(this.h0, c, tr3.Chip_chipIcon));
        if (c.hasValue(tr3.Chip_chipIconTint)) {
            d(pu3.a(this.h0, c, tr3.Chip_chipIconTint));
        }
        h(c.getDimension(tr3.Chip_chipIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        d(c.getBoolean(tr3.Chip_closeIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconVisible") != null)) {
            d(c.getBoolean(tr3.Chip_closeIconEnabled, false));
        }
        d(pu3.b(this.h0, c, tr3.Chip_closeIcon));
        g(pu3.a(this.h0, c, tr3.Chip_closeIconTint));
        m(c.getDimension(tr3.Chip_closeIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        a(c.getBoolean(tr3.Chip_android_checkable, false));
        b(c.getBoolean(tr3.Chip_checkedIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconVisible") != null)) {
            b(c.getBoolean(tr3.Chip_checkedIconEnabled, false));
        }
        b(pu3.b(this.h0, c, tr3.Chip_checkedIcon));
        b(bs3.a(this.h0, c, tr3.Chip_showMotionSpec));
        a(bs3.a(this.h0, c, tr3.Chip_hideMotionSpec));
        j(c.getDimension(tr3.Chip_chipStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        p(c.getDimension(tr3.Chip_iconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        o(c.getDimension(tr3.Chip_iconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        r(c.getDimension(tr3.Chip_textStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        q(c.getDimension(tr3.Chip_textEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        n(c.getDimension(tr3.Chip_closeIconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        l(c.getDimension(tr3.Chip_closeIconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        g(c.getDimension(tr3.Chip_chipEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        A(c.getDimensionPixelSize(tr3.Chip_android_maxWidth, Integer.MAX_VALUE));
        c.recycle();
    }

    @DexIgnore
    public final void f(Canvas canvas, Rect rect) {
        if (s0()) {
            c(rect, this.l0);
            RectF rectF = this.l0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.P.setBounds(0, 0, (int) this.l0.width(), (int) this.l0.height());
            if (uu3.a) {
                this.Q.setBounds(this.P.getBounds());
                this.Q.jumpToCurrentState();
                this.Q.draw(canvas);
            } else {
                this.P.draw(canvas);
            }
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public void j(int i) {
        g(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public final void e(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (this.I != null) {
            float D2 = this.Z + D() + this.c0;
            float E2 = this.g0 + E() + this.d0;
            if (p7.e(this) == 0) {
                rectF.left = ((float) rect.left) + D2;
                rectF.right = ((float) rect.right) - E2;
            } else {
                rectF.left = ((float) rect.left) + E2;
                rectF.right = ((float) rect.right) - D2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public final void b(Rect rect, RectF rectF) {
        rectF.set(rect);
        if (s0()) {
            float f = this.g0 + this.f0 + this.S + this.e0 + this.d0;
            if (p7.e(this) == 0) {
                rectF.right = ((float) rect.right) - f;
            } else {
                rectF.left = ((float) rect.left) + f;
            }
        }
    }

    @DexIgnore
    public void k(int i) {
        c(t0.c(this.h0, i));
    }

    @DexIgnore
    public void g(ColorStateList colorStateList) {
        if (this.R != colorStateList) {
            this.R = colorStateList;
            if (s0()) {
                p7.a(this.P, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void c(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (s0()) {
            float f = this.g0 + this.f0;
            if (p7.e(this) == 0) {
                float f2 = ((float) rect.right) - f;
                rectF.right = f2;
                rectF.left = f2 - this.S;
            } else {
                float f3 = ((float) rect.left) + f;
                rectF.left = f3;
                rectF.right = f3 + this.S;
            }
            float exactCenterY = rect.exactCenterY();
            float f4 = this.S;
            float f5 = exactCenterY - (f4 / 2.0f);
            rectF.top = f5;
            rectF.bottom = f5 + f4;
        }
    }

    @DexIgnore
    public final void d(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (s0()) {
            float f = this.g0 + this.f0 + this.S + this.e0 + this.d0;
            if (p7.e(this) == 0) {
                float f2 = (float) rect.right;
                rectF.right = f2;
                rectF.left = f2 - f;
            } else {
                int i = rect.left;
                rectF.left = (float) i;
                rectF.right = ((float) i) + f;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public boolean b(int[] iArr) {
        if (Arrays.equals(this.C0, iArr)) {
            return false;
        }
        this.C0 = iArr;
        if (s0()) {
            return a(getState(), iArr);
        }
        return false;
    }

    @DexIgnore
    public void g(int i) {
        b(this.h0.getResources().getBoolean(i));
    }

    @DexIgnore
    public void g(float f) {
        if (this.g0 != f) {
            this.g0 = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public static boolean b(qu3 qu3) {
        ColorStateList colorStateList;
        return (qu3 == null || (colorStateList = qu3.b) == null || !colorStateList.isStateful()) ? false : true;
    }

    @DexIgnore
    public static boolean f(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    @DexIgnore
    public final void e(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    @DexIgnore
    public void e(ColorStateList colorStateList) {
        if (this.F != colorStateList) {
            this.F = colorStateList;
            if (this.J0) {
                b(colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void f(ColorStateList colorStateList) {
        if (this.B != colorStateList) {
            this.B = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (!TextUtils.equals(this.I, charSequence)) {
            this.I = charSequence;
            this.o0.a(true);
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.C != colorStateList) {
            this.C = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void d(ColorStateList colorStateList) {
        this.N = true;
        if (this.L != colorStateList) {
            this.L = colorStateList;
            if (r0()) {
                p7.a(this.K, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public static boolean i(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    @DexIgnore
    public void h(int i) {
        c(t0.b(this.h0, i));
    }

    @DexIgnore
    @Deprecated
    public void f(float f) {
        if (this.E != f) {
            this.E = f;
            setShapeAppearanceModel(n().a(f));
        }
    }

    @DexIgnore
    public void h(ColorStateList colorStateList) {
        if (this.H != colorStateList) {
            this.H = colorStateList;
            t0();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void i(float f) {
        if (this.D != f) {
            this.D = f;
            invalidateSelf();
            o0();
        }
    }

    @DexIgnore
    public void c(boolean z) {
        if (this.J != z) {
            boolean r02 = r0();
            this.J = z;
            boolean r03 = r0();
            if (r02 != r03) {
                if (r03) {
                    a(this.K);
                } else {
                    e(this.K);
                }
                invalidateSelf();
                o0();
            }
        }
    }

    @DexIgnore
    public void e(int i) {
        a(this.h0.getResources().getBoolean(i));
    }

    @DexIgnore
    public void b(boolean z) {
        if (this.V != z) {
            boolean q02 = q0();
            this.V = z;
            boolean q03 = q0();
            if (q02 != q03) {
                if (q03) {
                    a(this.W);
                } else {
                    e(this.W);
                }
                invalidateSelf();
                o0();
            }
        }
    }

    @DexIgnore
    public void e(boolean z) {
        this.H0 = z;
    }

    @DexIgnore
    public void f(int i) {
        b(t0.c(this.h0, i));
    }

    @DexIgnore
    public void d(boolean z) {
        if (this.O != z) {
            boolean s02 = s0();
            this.O = z;
            boolean s03 = s0();
            if (s02 != s03) {
                if (s03) {
                    a(this.P);
                } else {
                    e(this.P);
                }
                invalidateSelf();
                o0();
            }
        }
    }

    @DexIgnore
    public void h(float f) {
        if (this.M != f) {
            float D2 = D();
            this.M = f;
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    @Deprecated
    public void i(int i) {
        f(this.h0.getResources().getDimension(i));
    }

    @DexIgnore
    public void c(Drawable drawable) {
        Drawable L2 = L();
        if (L2 != drawable) {
            float D2 = D();
            this.K = drawable != null ? p7.i(drawable).mutate() : null;
            float D3 = D();
            e(L2);
            if (r0()) {
                a(this.K);
            }
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void b(Drawable drawable) {
        if (this.W != drawable) {
            float D2 = D();
            this.W = drawable;
            float D3 = D();
            e(this.W);
            a(this.W);
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void d(Drawable drawable) {
        Drawable S2 = S();
        if (S2 != drawable) {
            float E2 = E();
            this.P = drawable != null ? p7.i(drawable).mutate() : null;
            if (uu3.a) {
                u0();
            }
            float E3 = E();
            e(S2);
            if (s0()) {
                a(this.P);
            }
            invalidateSelf();
            if (E2 != E3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void b(bs3 bs3) {
        this.X = bs3;
    }

    @DexIgnore
    public void a(a aVar) {
        this.F0 = new WeakReference<>(aVar);
    }

    @DexIgnore
    public void a(RectF rectF) {
        d(getBounds(), rectF);
    }

    @DexIgnore
    public final void a(Canvas canvas, Rect rect) {
        if (q0()) {
            a(rect, this.l0);
            RectF rectF = this.l0;
            float f = rectF.left;
            float f2 = rectF.top;
            canvas.translate(f, f2);
            this.W.setBounds(0, 0, (int) this.l0.width(), (int) this.l0.height());
            this.W.draw(canvas);
            canvas.translate(-f, -f2);
        }
    }

    @DexIgnore
    public final void a(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (r0() || q0()) {
            float f = this.Z + this.a0;
            if (p7.e(this) == 0) {
                float f2 = ((float) rect.left) + f;
                rectF.left = f2;
                rectF.right = f2 + this.M;
            } else {
                float f3 = ((float) rect.right) - f;
                rectF.right = f3;
                rectF.left = f3 - this.M;
            }
            float exactCenterY = rect.exactCenterY();
            float f4 = this.M;
            float f5 = exactCenterY - (f4 / 2.0f);
            rectF.top = f5;
            rectF.bottom = f5 + f4;
        }
    }

    @DexIgnore
    public Paint.Align a(Rect rect, PointF pointF) {
        pointF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        Paint.Align align = Paint.Align.LEFT;
        if (this.I != null) {
            float D2 = this.Z + D() + this.c0;
            if (p7.e(this) == 0) {
                pointF.x = ((float) rect.left) + D2;
                align = Paint.Align.LEFT;
            } else {
                pointF.x = ((float) rect.right) - D2;
                align = Paint.Align.RIGHT;
            }
            pointF.y = ((float) rect.centerY()) - F();
        }
        return align;
    }

    @DexIgnore
    @Override // com.fossil.iu3.b
    public void a() {
        o0();
        invalidateSelf();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x014a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(int[] r7, int[] r8) {
        /*
            r6 = this;
            boolean r0 = super.onStateChange(r7)
            android.content.res.ColorStateList r1 = r6.B
            r2 = 0
            if (r1 == 0) goto L_0x0010
            int r3 = r6.p0
            int r1 = r1.getColorForState(r7, r3)
            goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            int r3 = r6.p0
            r4 = 1
            if (r3 == r1) goto L_0x0019
            r6.p0 = r1
            r0 = 1
        L_0x0019:
            android.content.res.ColorStateList r3 = r6.C
            if (r3 == 0) goto L_0x0024
            int r5 = r6.q0
            int r3 = r3.getColorForState(r7, r5)
            goto L_0x0025
        L_0x0024:
            r3 = 0
        L_0x0025:
            int r5 = r6.q0
            if (r5 == r3) goto L_0x002c
            r6.q0 = r3
            r0 = 1
        L_0x002c:
            int r1 = com.fossil.vs3.a(r1, r3)
            int r3 = r6.r0
            if (r3 == r1) goto L_0x0036
            r3 = 1
            goto L_0x0037
        L_0x0036:
            r3 = 0
        L_0x0037:
            android.content.res.ColorStateList r5 = r6.h()
            if (r5 != 0) goto L_0x003f
            r5 = 1
            goto L_0x0040
        L_0x003f:
            r5 = 0
        L_0x0040:
            r3 = r3 | r5
            if (r3 == 0) goto L_0x004d
            r6.r0 = r1
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r1)
            r6.a(r0)
            r0 = 1
        L_0x004d:
            android.content.res.ColorStateList r1 = r6.F
            if (r1 == 0) goto L_0x0058
            int r3 = r6.s0
            int r1 = r1.getColorForState(r7, r3)
            goto L_0x0059
        L_0x0058:
            r1 = 0
        L_0x0059:
            int r3 = r6.s0
            if (r3 == r1) goto L_0x0060
            r6.s0 = r1
            r0 = 1
        L_0x0060:
            android.content.res.ColorStateList r1 = r6.E0
            if (r1 == 0) goto L_0x0073
            boolean r1 = com.fossil.uu3.a(r7)
            if (r1 == 0) goto L_0x0073
            android.content.res.ColorStateList r1 = r6.E0
            int r3 = r6.t0
            int r1 = r1.getColorForState(r7, r3)
            goto L_0x0074
        L_0x0073:
            r1 = 0
        L_0x0074:
            int r3 = r6.t0
            if (r3 == r1) goto L_0x007f
            r6.t0 = r1
            boolean r1 = r6.D0
            if (r1 == 0) goto L_0x007f
            r0 = 1
        L_0x007f:
            com.fossil.iu3 r1 = r6.o0
            com.fossil.qu3 r1 = r1.a()
            if (r1 == 0) goto L_0x00a0
            com.fossil.iu3 r1 = r6.o0
            com.fossil.qu3 r1 = r1.a()
            android.content.res.ColorStateList r1 = r1.b
            if (r1 == 0) goto L_0x00a0
            com.fossil.iu3 r1 = r6.o0
            com.fossil.qu3 r1 = r1.a()
            android.content.res.ColorStateList r1 = r1.b
            int r3 = r6.u0
            int r1 = r1.getColorForState(r7, r3)
            goto L_0x00a1
        L_0x00a0:
            r1 = 0
        L_0x00a1:
            int r3 = r6.u0
            if (r3 == r1) goto L_0x00a8
            r6.u0 = r1
            r0 = 1
        L_0x00a8:
            int[] r1 = r6.getState()
            r3 = 16842912(0x10100a0, float:2.3694006E-38)
            boolean r1 = a(r1, r3)
            if (r1 == 0) goto L_0x00bb
            boolean r1 = r6.U
            if (r1 == 0) goto L_0x00bb
            r1 = 1
            goto L_0x00bc
        L_0x00bb:
            r1 = 0
        L_0x00bc:
            boolean r3 = r6.v0
            if (r3 == r1) goto L_0x00d6
            android.graphics.drawable.Drawable r3 = r6.W
            if (r3 == 0) goto L_0x00d6
            float r0 = r6.D()
            r6.v0 = r1
            float r1 = r6.D()
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x00d5
            r0 = 1
            r1 = 1
            goto L_0x00d7
        L_0x00d5:
            r0 = 1
        L_0x00d6:
            r1 = 0
        L_0x00d7:
            android.content.res.ColorStateList r3 = r6.A0
            if (r3 == 0) goto L_0x00e2
            int r5 = r6.w0
            int r3 = r3.getColorForState(r7, r5)
            goto L_0x00e3
        L_0x00e2:
            r3 = 0
        L_0x00e3:
            int r5 = r6.w0
            if (r5 == r3) goto L_0x00f4
            r6.w0 = r3
            android.content.res.ColorStateList r0 = r6.A0
            android.graphics.PorterDuff$Mode r3 = r6.B0
            android.graphics.PorterDuffColorFilter r0 = com.fossil.qt3.a(r6, r0, r3)
            r6.z0 = r0
            goto L_0x00f5
        L_0x00f4:
            r4 = r0
        L_0x00f5:
            android.graphics.drawable.Drawable r0 = r6.K
            boolean r0 = f(r0)
            if (r0 == 0) goto L_0x0104
            android.graphics.drawable.Drawable r0 = r6.K
            boolean r0 = r0.setState(r7)
            r4 = r4 | r0
        L_0x0104:
            android.graphics.drawable.Drawable r0 = r6.W
            boolean r0 = f(r0)
            if (r0 == 0) goto L_0x0113
            android.graphics.drawable.Drawable r0 = r6.W
            boolean r0 = r0.setState(r7)
            r4 = r4 | r0
        L_0x0113:
            android.graphics.drawable.Drawable r0 = r6.P
            boolean r0 = f(r0)
            if (r0 == 0) goto L_0x0130
            int r0 = r7.length
            int r3 = r8.length
            int r0 = r0 + r3
            int[] r0 = new int[r0]
            int r3 = r7.length
            java.lang.System.arraycopy(r7, r2, r0, r2, r3)
            int r7 = r7.length
            int r3 = r8.length
            java.lang.System.arraycopy(r8, r2, r0, r7, r3)
            android.graphics.drawable.Drawable r7 = r6.P
            boolean r7 = r7.setState(r0)
            r4 = r4 | r7
        L_0x0130:
            boolean r7 = com.fossil.uu3.a
            if (r7 == 0) goto L_0x0143
            android.graphics.drawable.Drawable r7 = r6.Q
            boolean r7 = f(r7)
            if (r7 == 0) goto L_0x0143
            android.graphics.drawable.Drawable r7 = r6.Q
            boolean r7 = r7.setState(r8)
            r4 = r4 | r7
        L_0x0143:
            if (r4 == 0) goto L_0x0148
            r6.invalidateSelf()
        L_0x0148:
            if (r1 == 0) goto L_0x014d
            r6.o0()
        L_0x014d:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rs3.a(int[], int[]):boolean");
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            p7.a(drawable, p7.e(this));
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            if (drawable == this.P) {
                if (drawable.isStateful()) {
                    drawable.setState(X());
                }
                p7.a(drawable, this.R);
                return;
            }
            if (drawable.isStateful()) {
                drawable.setState(getState());
            }
            Drawable drawable2 = this.K;
            if (drawable == drawable2 && this.N) {
                p7.a(drawable2, this.L);
            }
        }
    }

    @DexIgnore
    public static boolean a(int[] iArr, int i) {
        if (iArr == null) {
            return false;
        }
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(qu3 qu3) {
        this.o0.a(qu3, this.h0);
    }

    @DexIgnore
    public void a(TextUtils.TruncateAt truncateAt) {
        this.G0 = truncateAt;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        if (this.T != charSequence) {
            this.T = q8.b().a(charSequence);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.U != z) {
            this.U = z;
            float D2 = D();
            if (!z && this.v0) {
                this.v0 = false;
            }
            float D3 = D();
            invalidateSelf();
            if (D2 != D3) {
                o0();
            }
        }
    }

    @DexIgnore
    public void a(bs3 bs3) {
        this.Y = bs3;
    }
}
