package com.fossil;

import com.fossil.ay1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gw1 implements ay1.a {
    @DexIgnore
    public /* final */ jw1 a;
    @DexIgnore
    public /* final */ dv1 b;
    @DexIgnore
    public /* final */ Iterable c;
    @DexIgnore
    public /* final */ pu1 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public gw1(jw1 jw1, dv1 dv1, Iterable iterable, pu1 pu1, int i) {
        this.a = jw1;
        this.b = dv1;
        this.c = iterable;
        this.d = pu1;
        this.e = i;
    }

    @DexIgnore
    public static ay1.a a(jw1 jw1, dv1 dv1, Iterable iterable, pu1 pu1, int i) {
        return new gw1(jw1, dv1, iterable, pu1, i);
    }

    @DexIgnore
    @Override // com.fossil.ay1.a
    public Object a() {
        return jw1.a(this.a, this.b, this.c, this.d, this.e);
    }
}
