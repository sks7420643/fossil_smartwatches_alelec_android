package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ms3 extends l0 {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> c;
    @DexIgnore
    public FrameLayout d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BottomSheetBehavior.e i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            ms3 ms3 = ms3.this;
            if (ms3.f && ms3.isShowing() && ms3.this.f()) {
                ms3.this.cancel();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnTouchListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends BottomSheetBehavior.e {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, float f) {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, int i) {
            if (i == 5) {
                ms3.this.cancel();
            }
        }
    }

    @DexIgnore
    public ms3(Context context) {
        this(context, 0);
    }

    @DexIgnore
    public final View a(int i2, View view, ViewGroup.LayoutParams layoutParams) {
        b();
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) this.d.findViewById(nr3.coordinator);
        if (i2 != 0 && view == null) {
            view = getLayoutInflater().inflate(i2, (ViewGroup) coordinatorLayout, false);
        }
        FrameLayout frameLayout = (FrameLayout) this.d.findViewById(nr3.design_bottom_sheet);
        if (layoutParams == null) {
            frameLayout.addView(view);
        } else {
            frameLayout.addView(view, layoutParams);
        }
        coordinatorLayout.findViewById(nr3.touch_outside).setOnClickListener(new a());
        da.a(frameLayout, new b());
        frameLayout.setOnTouchListener(new c());
        return this.d;
    }

    @DexIgnore
    public final FrameLayout b() {
        if (this.d == null) {
            FrameLayout frameLayout = (FrameLayout) View.inflate(getContext(), pr3.design_bottom_sheet_dialog, null);
            this.d = frameLayout;
            BottomSheetBehavior<FrameLayout> b2 = BottomSheetBehavior.b((FrameLayout) frameLayout.findViewById(nr3.design_bottom_sheet));
            this.c = b2;
            b2.a(this.i);
            this.c.b(this.f);
        }
        return this.d;
    }

    @DexIgnore
    public BottomSheetBehavior<FrameLayout> c() {
        if (this.c == null) {
            b();
        }
        return this.c;
    }

    @DexIgnore
    public void cancel() {
        BottomSheetBehavior<FrameLayout> c2 = c();
        if (!this.e || c2.e() == 5) {
            super.cancel();
        } else {
            c2.e(5);
        }
    }

    @DexIgnore
    public boolean d() {
        return this.e;
    }

    @DexIgnore
    public void e() {
        this.c.b(this.i);
    }

    @DexIgnore
    public boolean f() {
        if (!this.h) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new int[]{16843611});
            this.g = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
            this.h = true;
        }
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.l0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
                window.addFlags(RecyclerView.UNDEFINED_DURATION);
            }
            window.setLayout(-1, -1);
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.c;
        if (bottomSheetBehavior != null && bottomSheetBehavior.e() == 5) {
            this.c.e(4);
        }
    }

    @DexIgnore
    public void setCancelable(boolean z) {
        super.setCancelable(z);
        if (this.f != z) {
            this.f = z;
            BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.c;
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.b(z);
            }
        }
    }

    @DexIgnore
    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        if (z && !this.f) {
            this.f = true;
        }
        this.g = z;
        this.h = true;
    }

    @DexIgnore
    @Override // com.fossil.l0, android.app.Dialog
    public void setContentView(int i2) {
        super.setContentView(a(i2, null, null));
    }

    @DexIgnore
    public ms3(Context context, int i2) {
        super(context, a(context, i2));
        this.f = true;
        this.g = true;
        this.i = new d();
        a(1);
    }

    @DexIgnore
    @Override // com.fossil.l0, android.app.Dialog
    public void setContentView(View view) {
        super.setContentView(a(0, view, null));
    }

    @DexIgnore
    @Override // com.fossil.l0
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(a(0, view, layoutParams));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends g9 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void a(View view, oa oaVar) {
            super.a(view, oaVar);
            if (ms3.this.f) {
                oaVar.a(1048576);
                oaVar.g(true);
                return;
            }
            oaVar.g(false);
        }

        @DexIgnore
        @Override // com.fossil.g9
        public boolean a(View view, int i, Bundle bundle) {
            if (i == 1048576) {
                ms3 ms3 = ms3.this;
                if (ms3.f) {
                    ms3.cancel();
                    return true;
                }
            }
            return super.a(view, i, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.l0
    public static int a(Context context, int i2) {
        if (i2 != 0) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(jr3.bottomSheetDialogTheme, typedValue, true)) {
            return typedValue.resourceId;
        }
        return sr3.Theme_Design_Light_BottomSheetDialog;
    }
}
