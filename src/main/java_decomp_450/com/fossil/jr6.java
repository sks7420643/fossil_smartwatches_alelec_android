package com.fossil;

import com.fossil.xg5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr6 extends fr6 {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ gr6 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public jr6(gr6 gr6) {
        ee7.b(gr6, "mPairingInstructionsView");
        this.f = gr6;
    }

    @DexIgnore
    @Override // com.fossil.fr6
    public void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        this.f.e();
        this.f.i(!this.e);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.fr6
    public void h() {
        xg5 xg5 = xg5.b;
        gr6 gr6 = this.f;
        if (gr6 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment");
        } else if (xg5.a(xg5, ((ar6) gr6).getContext(), xg5.a.PAIR_DEVICE, false, false, false, (Integer) null, 60, (Object) null)) {
            this.f.N();
        }
    }

    @DexIgnore
    @Override // com.fossil.fr6
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }
}
