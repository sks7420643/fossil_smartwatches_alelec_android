package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleCheckBox;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp5 extends RecyclerView.g<a> {
    @DexIgnore
    public List<WeatherLocationWrapper> a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public FlexibleCheckBox b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ jp5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jp5$a$a")
        /* renamed from: com.fossil.jp5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0087a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0087a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2;
                if (this.a.d.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && !((WeatherLocationWrapper) this.a.d.a.get(this.a.getAdapterPosition())).isUseCurrentLocation() && (a2 = this.a.d.b) != null) {
                    a2.a(this.a.getAdapterPosition() - 1, !((WeatherLocationWrapper) this.a.d.a.get(this.a.getAdapterPosition())).isEnableLocation());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(jp5 jp5, View view) {
            super(view);
            ee7.b(view, "view");
            this.d = jp5;
            View findViewById = view.findViewById(2131362049);
            if (findViewById != null) {
                this.c = findViewById;
                View findViewById2 = view.findViewById(2131361998);
                if (findViewById2 != null) {
                    this.b = (FlexibleCheckBox) findViewById2;
                    View findViewById3 = view.findViewById(2131363340);
                    if (findViewById3 != null) {
                        this.a = (TextView) findViewById3;
                        View view2 = this.c;
                        if (view2 != null) {
                            view2.setOnClickListener(new View$OnClickListenerC0087a(this));
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public final void a(WeatherLocationWrapper weatherLocationWrapper) {
            FlexibleCheckBox flexibleCheckBox;
            ee7.b(weatherLocationWrapper, "locationWrapper");
            TextView textView = this.a;
            if (textView != null) {
                textView.setText(weatherLocationWrapper.getFullName());
            }
            FlexibleCheckBox flexibleCheckBox2 = this.b;
            if (flexibleCheckBox2 != null) {
                flexibleCheckBox2.setChecked(weatherLocationWrapper.isEnableLocation());
            }
            FlexibleCheckBox flexibleCheckBox3 = this.b;
            if (flexibleCheckBox3 != null) {
                flexibleCheckBox3.setEnabled(!weatherLocationWrapper.isUseCurrentLocation());
            }
            TextView textView2 = this.a;
            if (ee7.a((Object) (textView2 != null ? textView2.getText() : null), (Object) ig5.a(PortfolioApp.g0.c(), 2131886389)) && (flexibleCheckBox = this.b) != null) {
                flexibleCheckBox.setEnableColor("nonBrandDisableCalendarDay");
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            aVar.a(this.a.get(i));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558720, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<WeatherLocationWrapper> list) {
        ee7.b(list, "locationSearchList");
        this.a.clear();
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886389);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886389);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        this.a.add(0, new WeatherLocationWrapper("", 0.0d, 0.0d, a2, a3, true, true, 6, null));
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
