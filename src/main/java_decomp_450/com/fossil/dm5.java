package com.fossil;

import android.content.Intent;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm5 extends fl4<b, d, c> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ e e; // = new e();
    @DexIgnore
    public /* final */ List<QuickResponseMessage> f; // = new ArrayList();
    @DexIgnore
    public /* final */ QuickResponseRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ List<QuickResponseMessage> a;

        @DexIgnore
        public b(List<QuickResponseMessage> list) {
            ee7.b(list, "quickResponseMessageList");
            this.a = list;
        }

        @DexIgnore
        public final List<QuickResponseMessage> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "errorCodes");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase$SetReplyMessageReceiver$receive$1", f = "SetReplyMessageMappingUseCase.kt", l = {39}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dm5$e$a$a")
            @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase$SetReplyMessageReceiver$receive$1$1", f = "SetReplyMessageMappingUseCase.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.dm5$e$a$a  reason: collision with other inner class name */
            public static final class C0040a extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0040a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0040a aVar = new C0040a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
                    return ((C0040a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return dm5.this.a(new d());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    dm5.this.g.updateQR(dm5.this.f);
                    tk7 c = qj7.c();
                    C0040a aVar = new C0040a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(c, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetReplyMessageMappingUseCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_REPLY_MESSAGE_MAPPING && dm5.this.d) {
                boolean z = false;
                dm5.this.d = false;
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                dm5.this.e();
                if (z) {
                    ik7 unused = xh7.b(dm5.this.b(), null, null, new a(this, null), 3, null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetReplyMessageMappingUseCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                dm5.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public dm5(QuickResponseRepository quickResponseRepository) {
        ee7.b(quickResponseRepository, "mQuickResponseRepository");
        this.g = quickResponseRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SetReplyMessageMappingUseCase";
    }

    @DexIgnore
    public final void e() {
        nj5.d.b(this.e, CommunicateMode.SET_REPLY_MESSAGE_MAPPING);
    }

    @DexIgnore
    public final void d() {
        nj5.d.a(this.e, CommunicateMode.SET_REPLY_MESSAGE_MAPPING);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        this.d = true;
        d();
        if (bVar != null) {
            List<QuickResponseMessage> a2 = bVar.a();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList();
            for (T t : a2) {
                if (!pb7.a(t.getResponse().length() == 0).booleanValue()) {
                    arrayList.add(t);
                }
            }
            ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
            for (QuickResponseMessage quickResponseMessage : arrayList) {
                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
            }
            pb7.a(PortfolioApp.g0.c().a(new ReplyMessageMappingGroup(ea7.d((Collection) arrayList2), "icMessage.icon"), PortfolioApp.g0.c().c()));
        }
        return new Object();
    }
}
