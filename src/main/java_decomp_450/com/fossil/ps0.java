package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps0 extends fe7 implements gd7<eu0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ se7 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ps0(se7 se7) {
        super(1);
        this.a = se7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(eu0 eu0) {
        T t = this.a.element;
        if (t != null) {
            t.a(is0.INTERRUPTED);
        }
        return i97.a;
    }
}
