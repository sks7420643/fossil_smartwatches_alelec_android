package com.fossil;

import android.webkit.MimeTypeMap;
import com.fossil.pr;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr implements pr<File> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7] */
    @Override // com.fossil.pr
    public /* bridge */ /* synthetic */ Object a(rq rqVar, File file, rt rtVar, ir irVar, fb7 fb7) {
        return a(rqVar, file, rtVar, irVar, (fb7<? super or>) fb7);
    }

    @DexIgnore
    public boolean a(File file) {
        ee7.b(file, "data");
        return pr.a.a(this, file);
    }

    @DexIgnore
    public String b(File file) {
        ee7.b(file, "data");
        return file.getPath() + ':' + file.lastModified();
    }

    @DexIgnore
    public Object a(rq rqVar, File file, rt rtVar, ir irVar, fb7<? super or> fb7) {
        return new vr(ir7.a(ir7.c(file)), MimeTypeMap.getSingleton().getMimeTypeFromExtension(rc7.d(file)), br.DISK);
    }
}
