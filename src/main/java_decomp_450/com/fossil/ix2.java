package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix2 {
    @DexIgnore
    public static /* final */ gx2 a; // = c();
    @DexIgnore
    public static /* final */ gx2 b; // = new fx2();

    @DexIgnore
    public static gx2 a() {
        return a;
    }

    @DexIgnore
    public static gx2 b() {
        return b;
    }

    @DexIgnore
    public static gx2 c() {
        try {
            return (gx2) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
