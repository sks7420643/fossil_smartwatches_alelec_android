package com.fossil;

import android.os.RemoteException;
import com.fossil.v02;
import com.fossil.v02.b;
import com.fossil.y12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b22<A extends v02.b, L> {
    @DexIgnore
    public /* final */ y12<L> a;
    @DexIgnore
    public /* final */ k02[] b; // = null;
    @DexIgnore
    public /* final */ boolean c; // = false;

    @DexIgnore
    public b22(y12<L> y12) {
        this.a = y12;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }

    @DexIgnore
    public abstract void a(A a2, oo3<Void> oo3) throws RemoteException;

    @DexIgnore
    public y12.a<L> b() {
        return this.a.b();
    }

    @DexIgnore
    public k02[] c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }
}
