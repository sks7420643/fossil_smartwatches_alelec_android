package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e03 implements tr2<h03> {
    @DexIgnore
    public static e03 b; // = new e03();
    @DexIgnore
    public /* final */ tr2<h03> a;

    @DexIgnore
    public e03(tr2<h03> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((h03) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((h03) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((h03) b.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ h03 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public e03() {
        this(sr2.a(new g03()));
    }
}
