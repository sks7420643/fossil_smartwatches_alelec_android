package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c70 extends u60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<c70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public c70 createFromParcel(Parcel parcel) {
            u60 createFromParcel = u60.CREATOR.createFromParcel(parcel);
            if (createFromParcel != null) {
                return (c70) createFromParcel;
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedAlarm");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public c70[] newArray(int i) {
            return new c70[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ c70(d70 d70, f70 f70, x60 x60, int i, zd7 zd7) {
        this(d70, (i & 2) != 0 ? null : f70, (i & 4) != 0 ? null : x60);
    }

    @DexIgnore
    public c70(d70 d70, f70 f70, x60 x60) {
        super(d70, f70, x60);
    }

    @DexIgnore
    @Override // com.fossil.t60
    public d70 getFireTime() {
        v60[] c = c();
        int length = c.length;
        int i = 0;
        while (i < length) {
            v60 v60 = c[i];
            if (!(v60 instanceof d70)) {
                i++;
            } else if (v60 != null) {
                return (d70) v60;
            } else {
                throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedFireTime");
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
