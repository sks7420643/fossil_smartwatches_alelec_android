package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la6 implements Factory<ka6> {
    @DexIgnore
    public static ka6 a(ia6 ia6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new ka6(ia6, userRepository, summariesRepository, portfolioApp);
    }
}
