package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yh6 extends th6 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ uh6 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = yh6.class.getSimpleName();
        ee7.a((Object) simpleName, "AboutPresenter::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public yh6(uh6 uh6) {
        ee7.b(uh6, "mView");
        this.e = uh6;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(f, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(f, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }
}
