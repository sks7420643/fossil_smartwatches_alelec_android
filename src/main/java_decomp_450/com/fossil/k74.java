package com.fossil;

import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k74 extends m34 implements j74 {
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public k74(String str, String str2, m64 m64, String str3) {
        this(str, str2, m64, k64.POST, str3);
    }

    @DexIgnore
    @Override // com.fossil.j74
    public boolean a(e74 e74, boolean z) {
        if (z) {
            l64 a = a();
            a(a, e74);
            a(a, e74.c);
            z24 a2 = z24.a();
            a2.a("Sending report to: " + b());
            try {
                n64 b = a.b();
                int b2 = b.b();
                z24 a3 = z24.a();
                a3.a("Create report request ID: " + b.a("X-REQUEST-ID"));
                z24 a4 = z24.a();
                a4.a("Result was: " + b2);
                return p44.a(b2) == 0;
            } catch (IOException e) {
                z24.a().b("Create report HTTP request failed.", e);
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public k74(String str, String str2, m64 m64, k64 k64, String str3) {
        super(str, str2, m64, k64);
        this.f = str3;
    }

    @DexIgnore
    public final l64 a(l64 l64, e74 e74) {
        l64.a("X-CRASHLYTICS-GOOGLE-APP-ID", e74.b);
        l64.a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        l64.a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
        for (Map.Entry<String, String> entry : e74.c.a().entrySet()) {
            l64.a(entry);
        }
        return l64;
    }

    @DexIgnore
    public final l64 a(l64 l64, g74 g74) {
        l64.b("report[identifier]", g74.b());
        if (g74.d().length == 1) {
            z24.a().a("Adding single file " + g74.e() + " to report " + g74.b());
            l64.a("report[file]", g74.e(), "application/octet-stream", g74.c());
            return l64;
        }
        File[] d = g74.d();
        int i = 0;
        for (File file : d) {
            z24.a().a("Adding file " + file.getName() + " to report " + g74.b());
            StringBuilder sb = new StringBuilder();
            sb.append("report[file");
            sb.append(i);
            sb.append("]");
            l64.a(sb.toString(), file.getName(), "application/octet-stream", file);
            i++;
        }
        return l64;
    }
}
