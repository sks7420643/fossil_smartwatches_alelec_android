package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import java.util.concurrent.ExecutorService;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s94 implements n94 {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static mb4 d;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ExecutorService b;

    @DexIgnore
    public s94(Context context, ExecutorService executorService) {
        this.a = context;
        this.b = executorService;
    }

    @DexIgnore
    public static no3<Integer> b(Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Binding to service");
        }
        return a(context, "com.google.firebase.MESSAGING_EVENT").a(intent).a(u94.a(), q94.a);
    }

    @DexIgnore
    @Override // com.fossil.n94
    public no3<Integer> a(Intent intent) {
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        return a(this.a, intent);
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public no3<Integer> a(Context context, Intent intent) {
        boolean z = true;
        boolean z2 = v92.j() && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & SQLiteDatabase.CREATE_IF_NECESSARY) == 0) {
            z = false;
        }
        if (!z2 || z) {
            return qo3.a(this.b, new o94(context, intent)).b(this.b, new p94(context, intent));
        }
        return b(context, intent);
    }

    @DexIgnore
    public static mb4 a(Context context, String str) {
        mb4 mb4;
        synchronized (c) {
            if (d == null) {
                d = new mb4(context, str);
            }
            mb4 = d;
        }
        return mb4;
    }

    @DexIgnore
    public static final /* synthetic */ Integer a(no3 no3) throws Exception {
        return -1;
    }

    @DexIgnore
    public static final /* synthetic */ no3 a(Context context, Intent intent, no3 no3) throws Exception {
        return (!v92.j() || ((Integer) no3.b()).intValue() != 402) ? no3 : b(context, intent).a(u94.a(), r94.a);
    }
}
