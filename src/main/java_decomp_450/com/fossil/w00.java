package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.m00;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w00<Data> implements m00<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", Utility.URL_SCHEME)));
    @DexIgnore
    public /* final */ m00<f00, Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements n00<Uri, InputStream> {
        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, InputStream> a(q00 q00) {
            return new w00(q00.a(f00.class, InputStream.class));
        }
    }

    @DexIgnore
    public w00(m00<f00, Data> m00) {
        this.a = m00;
    }

    @DexIgnore
    public m00.a<Data> a(Uri uri, int i, int i2, ax axVar) {
        return this.a.a(new f00(uri.toString()), i, i2, axVar);
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
