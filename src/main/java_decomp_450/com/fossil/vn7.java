package com.fossil;

import com.fossil.fp7;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn7 {
    @DexIgnore
    public static /* final */ Executor g; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), ro7.a("OkHttp ConnectionPool", true));
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Runnable c;
    @DexIgnore
    public /* final */ Deque<bp7> d;
    @DexIgnore
    public /* final */ cp7 e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
            L_0x0000:
                com.fossil.vn7 r0 = com.fossil.vn7.this
                long r1 = java.lang.System.nanoTime()
                long r0 = r0.a(r1)
                r2 = -1
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 != 0) goto L_0x0011
                return
            L_0x0011:
                r2 = 0
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 <= 0) goto L_0x0000
                r2 = 1000000(0xf4240, double:4.940656E-318)
                long r4 = r0 / r2
                long r2 = r2 * r4
                long r0 = r0 - r2
                com.fossil.vn7 r2 = com.fossil.vn7.this
                monitor-enter(r2)
                com.fossil.vn7 r3 = com.fossil.vn7.this     // Catch:{ InterruptedException -> 0x002b }
                int r1 = (int) r0     // Catch:{ InterruptedException -> 0x002b }
                r3.wait(r4, r1)     // Catch:{ InterruptedException -> 0x002b }
                goto L_0x002b
            L_0x0029:
                r0 = move-exception
                goto L_0x002d
            L_0x002b:
                monitor-exit(r2)     // Catch:{ all -> 0x0029 }
                goto L_0x0000
            L_0x002d:
                monitor-exit(r2)     // Catch:{ all -> 0x0029 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vn7.a.run():void");
        }
    }

    @DexIgnore
    public vn7() {
        this(5, 5, TimeUnit.MINUTES);
    }

    @DexIgnore
    public bp7 a(nn7 nn7, fp7 fp7, no7 no7) {
        for (bp7 bp7 : this.d) {
            if (bp7.a(nn7, no7)) {
                fp7.a(bp7, true);
                return bp7;
            }
        }
        return null;
    }

    @DexIgnore
    public void b(bp7 bp7) {
        if (!this.f) {
            this.f = true;
            g.execute(this.c);
        }
        this.d.add(bp7);
    }

    @DexIgnore
    public vn7(int i, long j, TimeUnit timeUnit) {
        this.c = new a();
        this.d = new ArrayDeque();
        this.e = new cp7();
        this.a = i;
        this.b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    @DexIgnore
    public Socket a(nn7 nn7, fp7 fp7) {
        for (bp7 bp7 : this.d) {
            if (bp7.a(nn7, null) && bp7.e() && bp7 != fp7.c()) {
                return fp7.b(bp7);
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(bp7 bp7) {
        if (bp7.k || this.a == 0) {
            this.d.remove(bp7);
            return true;
        }
        notifyAll();
        return false;
    }

    @DexIgnore
    public long a(long j) {
        synchronized (this) {
            bp7 bp7 = null;
            long j2 = Long.MIN_VALUE;
            int i = 0;
            int i2 = 0;
            for (bp7 bp72 : this.d) {
                if (a(bp72, j) > 0) {
                    i2++;
                } else {
                    i++;
                    long j3 = j - bp72.o;
                    if (j3 > j2) {
                        bp7 = bp72;
                        j2 = j3;
                    }
                }
            }
            if (j2 < this.b) {
                if (i <= this.a) {
                    if (i > 0) {
                        return this.b - j2;
                    } else if (i2 > 0) {
                        return this.b;
                    } else {
                        this.f = false;
                        return -1;
                    }
                }
            }
            this.d.remove(bp7);
            ro7.a(bp7.g());
            return 0;
        }
    }

    @DexIgnore
    public final int a(bp7 bp7, long j) {
        List<Reference<fp7>> list = bp7.n;
        int i = 0;
        while (i < list.size()) {
            Reference<fp7> reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                mq7.d().a("A connection to " + bp7.f().a().k() + " was leaked. Did you forget to close a response body?", ((fp7.a) reference).a);
                list.remove(i);
                bp7.k = true;
                if (list.isEmpty()) {
                    bp7.o = j - this.b;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
