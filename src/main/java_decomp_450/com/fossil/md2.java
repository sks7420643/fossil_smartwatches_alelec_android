package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md2 implements Parcelable.Creator<id2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ id2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        lc2 lc2 = null;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                lc2 = (lc2) j72.a(parcel, a, lc2.CREATOR);
            } else if (a2 == 2) {
                arrayList = j72.c(parcel, a, DataSet.CREATOR);
            } else if (a2 == 3) {
                arrayList2 = j72.c(parcel, a, DataPoint.CREATOR);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                iBinder = j72.p(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new id2(lc2, arrayList, arrayList2, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ id2[] newArray(int i) {
        return new id2[i];
    }
}
