package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os4 extends he {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a(null);
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<hn4, String, Integer>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, mn4>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public Timer g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public LiveData<mn4> i;
    @DexIgnore
    public Timer j;
    @DexIgnore
    public /* final */ ro4 k;
    @DexIgnore
    public /* final */ ch5 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return os4.m;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ os4 a;

        @DexIgnore
        public b(os4 os4) {
            this.a = os4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<mn4> apply(mn4 mn4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = os4.n.a();
            local.e(a2, "switchMap - challenge: " + mn4);
            MutableLiveData<mn4> mutableLiveData = new MutableLiveData<>();
            mutableLiveData.a(mn4);
            if (mn4 == null) {
                Timer d = this.a.g;
                if (d != null) {
                    d.cancel();
                }
                if (!this.a.h) {
                    this.a.b.a((Object) true);
                    Timer d2 = this.a.g;
                    if (d2 != null) {
                        d2.cancel();
                    }
                    Timer c = this.a.j;
                    if (c != null) {
                        c.cancel();
                    }
                    this.a.l.a((Long) 0L);
                    this.a.f.a((Object) true);
                }
            } else {
                this.a.c(nt4.a(mn4));
                this.a.o();
                this.a.a(mn4);
                this.a.b(mn4);
            }
            if (this.a.h) {
                this.a.h = false;
                this.a.m();
            }
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel", f = "BCCurrentChallengeSubTabViewModel.kt", l = {277, 297}, m = "challengesFromServer")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ os4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(os4 os4, fb7 fb7) {
            super(fb7);
            this.this$0 = os4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$challengesFromServer$localChallenge$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ os4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(os4 os4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = os4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return this.this$0.k.a(new String[]{"running", "waiting"}, vt4.a.a());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$completedChallenge$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {196}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ mn4 $currentChallenge;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ os4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$completedChallenge$1$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {197, 200}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends yn4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends yn4>>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (kj7.a(1000, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.this$0.k.d(this.this$0.$currentChallenge.f());
                ro4 a2 = this.this$0.this$0.k;
                this.L$0 = yi7;
                this.label = 2;
                obj = ro4.a(a2, 5, 0, this, 2, (Object) null);
                return obj == a ? a : obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(os4 os4, mn4 mn4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = os4;
            this.$currentChallenge = mn4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$currentChallenge, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$initDisplayPlayer$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ int $target;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ os4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(os4 os4, String str, String str2, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = os4;
            this.$challengeId = str;
            this.$type = str2;
            this.$target = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$challengeId, this.$type, this.$target, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                hn4 e = this.this$0.k.e(this.$challengeId);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = os4.n.a();
                local.e(a, "initDisplayPlayer - bcDisplayPlayer: " + e);
                if (e != null) {
                    this.this$0.c.a(new v87(e, this.$type, pb7.a(this.$target)));
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$refresh$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {177, 181}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ os4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(os4 os4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = os4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0075  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r5.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x0026
                if (r1 == r3) goto L_0x001e
                if (r1 != r2) goto L_0x0016
                java.lang.Object r0 = r5.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r6)
                goto L_0x006b
            L_0x0016:
                java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r6.<init>(r0)
                throw r6
            L_0x001e:
                java.lang.Object r1 = r5.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r6)
                goto L_0x0038
            L_0x0026:
                com.fossil.t87.a(r6)
                com.fossil.yi7 r1 = r5.p$
                com.fossil.os4 r6 = r5.this$0
                r5.L$0 = r1
                r5.label = r3
                java.lang.Object r6 = r6.a(r5)
                if (r6 != r0) goto L_0x0038
                return r0
            L_0x0038:
                com.fossil.os4 r6 = r5.this$0
                androidx.lifecycle.MutableLiveData r6 = r6.a
                r4 = 0
                java.lang.Boolean r4 = com.fossil.pb7.a(r4)
                r6.a(r4)
                com.fossil.os4 r6 = r5.this$0
                com.fossil.ch5 r6 = r6.l
                java.lang.Boolean r6 = r6.P()
                java.lang.Boolean r3 = com.fossil.pb7.a(r3)
                boolean r6 = com.fossil.ee7.a(r6, r3)
                if (r6 == 0) goto L_0x0082
                com.fossil.os4 r6 = r5.this$0
                com.fossil.ro4 r6 = r6.k
                r5.L$0 = r1
                r5.label = r2
                java.lang.Object r6 = r6.b(r5)
                if (r6 != r0) goto L_0x006b
                return r0
            L_0x006b:
                com.fossil.ko4 r6 = (com.fossil.ko4) r6
                java.lang.Object r6 = r6.c()
                com.fossil.no4 r6 = (com.fossil.no4) r6
                if (r6 == 0) goto L_0x0082
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                java.lang.String r6 = r6.a()
                r0.n(r6)
            L_0x0082:
                com.fossil.i97 r6 = com.fossil.i97.a
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.os4.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ os4 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, h hVar) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ro4 a2 = this.this$0.a.k;
                    List d = w97.d(this.this$0.b);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = ro4.a(a2, d, 3, 3, false, (fb7) this, 8, (Object) null);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<hn4> list = (List) ((ko4) obj).c();
                if (list != null && (!list.isEmpty())) {
                    for (hn4 hn4 : list) {
                        if (pb7.a(ee7.a((Object) hn4.a(), (Object) this.this$0.b)).booleanValue()) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a3 = os4.n.a();
                            local.e(a3, " setupFetchDisplayPlayersTimer - displayPlayer: " + hn4);
                            MutableLiveData e = this.this$0.a.c;
                            h hVar = this.this$0;
                            e.a(new v87(hn4, hVar.c, pb7.a(hVar.d)));
                        }
                    }
                    throw new NoSuchElementException("Collection contains no element matching the predicate.");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public h(os4 os4, String str, String str2, int i) {
            this.a = os4;
            this.b = str;
            this.c = str2;
            this.d = i;
        }

        @DexIgnore
        public void run() {
            ik7 unused = xh7.b(ie.a(this.a), qj7.b(), null, new a(null, this), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ os4 a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ mn4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.os4$i$a$a")
            /* renamed from: com.fossil.os4$i$a$a  reason: collision with other inner class name */
            public static final class C0153a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0153a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0153a aVar = new C0153a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0153a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        FLogger.INSTANCE.getLocal().e(os4.n.a(), "starAndStopChallenge - startChallenge");
                        mn4 a = this.this$0.this$0.a.k.a(this.this$0.this$0.c.f());
                        if (a != null) {
                            a.a("running");
                            this.this$0.this$0.a.k.a(a);
                        }
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends yn4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends yn4>>> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        FLogger.INSTANCE.getLocal().e(os4.n.a(), "starAndStopChallenge - stopChallenge");
                        this.L$0 = yi7;
                        this.label = 1;
                        if (kj7.a(1000, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.this$0.a.k.d(this.this$0.this$0.c.f());
                    ro4 a2 = this.this$0.this$0.a.k;
                    this.L$0 = yi7;
                    this.label = 2;
                    obj = ro4.a(a2, 5, 0, this, 2, (Object) null);
                    return obj == a ? a : obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, i iVar) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    if (this.this$0.b) {
                        ti7 b2 = qj7.b();
                        C0153a aVar = new C0153a(this, null);
                        this.L$0 = yi7;
                        this.label = 1;
                        if (vh7.a(b2, aVar, this) == a) {
                            return a;
                        }
                    } else {
                        ti7 b3 = qj7.b();
                        b bVar = new b(this, null);
                        this.L$0 = yi7;
                        this.label = 2;
                        if (vh7.a(b3, bVar, this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1 || i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public i(os4 os4, boolean z, mn4 mn4) {
            this.a = os4;
            this.b = z;
            this.c = mn4;
        }

        @DexIgnore
        public void run() {
            ik7 unused = xh7.b(ie.a(this.a), null, null, new a(null, this), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$start$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {94}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ os4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$start$1$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {94}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = jVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    os4 os4 = this.this$0.this$0;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (os4.a(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(os4 os4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = os4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = os4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCCurrentChallengeSubTab\u2026el::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public os4(ro4 ro4, ch5 ch5) {
        ee7.b(ro4, "challengeRepository");
        ee7.b(ch5, "shared");
        this.k = ro4;
        this.l = ch5;
        LiveData<mn4> b2 = ge.b(au4.a(this.k.b(new String[]{"running", "waiting"}, vt4.a.a())), new b(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026    }\n\n        live\n    }");
        this.i = b2;
    }

    @DexIgnore
    public final void l() {
        mn4 a2 = this.i.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "resume - challenge: " + a2);
        if (a2 != null) {
            c(nt4.a(a2));
            a(a2);
            b(a2);
        }
    }

    @DexIgnore
    public final void m() {
        ik7 unused = xh7.b(ie.a(this), null, null, new j(this, null), 3, null);
    }

    @DexIgnore
    public final void n() {
        String p;
        mn4 a2 = this.i.a();
        if (a2 != null && (p = a2.p()) != null) {
            if (ee7.a((Object) "waiting", (Object) p)) {
                this.d.a(w87.a(false, a2));
            } else {
                this.d.a(w87.a(true, a2));
            }
        }
    }

    @DexIgnore
    public final void o() {
        this.a.a((Boolean) false);
        this.b.a((Boolean) false);
    }

    @DexIgnore
    public final LiveData<v87<hn4, String, Integer>> d() {
        return au4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Boolean> e() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> f() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> g() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Boolean> h() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, mn4>> i() {
        return this.d;
    }

    @DexIgnore
    public final void j() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "pause - playerTimer: " + this.g);
        Timer timer = this.g;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = this.j;
        if (timer2 != null) {
            timer2.cancel();
        }
    }

    @DexIgnore
    public final void k() {
        ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new g(this, null), 2, null);
    }

    @DexIgnore
    public final void b(mn4 mn4) {
        String f2 = mn4.f();
        String u = mn4.u();
        if (u == null) {
            u = "activity_reach_goal";
        }
        Integer t = mn4.t();
        int intValue = t != null ? t.intValue() : -1;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "scheduleFetchDisplayPlayers - challenge: " + mn4);
        if (ee7.a((Object) "running", (Object) mn4.p())) {
            Timer timer = this.g;
            if (timer != null) {
                timer.cancel();
            }
            Timer timer2 = new Timer();
            this.g = timer2;
            if (timer2 != null) {
                timer2.schedule(new h(this, f2, u, intValue), 0, 15000);
            }
        }
    }

    @DexIgnore
    public final LiveData<mn4> c() {
        return this.i;
    }

    @DexIgnore
    public final void a() {
        if (this.l.c().longValue() <= vt4.a.b() || !ee7.a((Object) this.l.P(), (Object) true)) {
            this.f.a((Boolean) true);
        } else {
            this.f.a((Boolean) false);
        }
    }

    @DexIgnore
    public final void c(mn4 mn4) {
        long j2;
        Date m2 = mn4.m();
        long j3 = 0;
        long time = m2 != null ? m2.getTime() : 0;
        Date e2 = mn4.e();
        long time2 = e2 != null ? e2.getTime() : 0;
        boolean z = false;
        if (!ee7.a((Object) "waiting", (Object) mn4.p())) {
            j2 = time2 - vt4.a.b();
        } else if (vt4.a.b() < time2) {
            j2 = time - vt4.a.b();
            z = true;
        } else {
            j2 = 0;
        }
        FLogger.INSTANCE.getLocal().e(m, "starAndStopChallenge - startTime: " + time + " - endTime: " + time2 + " - timeLeft: " + j2 + " - isStart: " + z);
        if (j2 >= 0) {
            j3 = j2;
        }
        Timer timer = this.j;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = new Timer();
        this.j = timer2;
        if (timer2 != null) {
            timer2.schedule(new i(this, z, mn4), j3);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        this.f.a(Boolean.valueOf(z));
    }

    @DexIgnore
    public final void a(mn4 mn4) {
        String f2 = mn4.f();
        String u = mn4.u();
        if (u == null) {
            u = "activity_reach_goal";
        }
        Integer t = mn4.t();
        int intValue = t != null ? t.intValue() : -1;
        String p = mn4.p();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "initDisplayPlayer - id: " + f2 + " - type: " + u + " - target: " + intValue + " -status: " + p);
        if (ee7.a((Object) "running", (Object) p)) {
            ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new f(this, f2, u, intValue, null), 2, null);
        }
    }

    @DexIgnore
    public final void b() {
        mn4 a2 = this.i.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "completedChallenge - current: " + a2);
        if (a2 != null) {
            ik7 unused = xh7.b(ie.a(this), null, null, new e(this, a2, null), 3, null);
            Timer timer = this.g;
            if (timer != null) {
                timer.cancel();
            }
            this.l.a((Long) 0L);
            this.f.a((Boolean) true);
            this.b.a((Boolean) true);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r13) {
        /*
            r12 = this;
            boolean r0 = r13 instanceof com.fossil.os4.c
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.os4$c r0 = (com.fossil.os4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.os4$c r0 = new com.fossil.os4$c
            r0.<init>(r12, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 0
            r5 = 1
            if (r2 == 0) goto L_0x004e
            if (r2 == r5) goto L_0x0046
            if (r2 != r3) goto L_0x003e
            java.lang.Object r1 = r0.L$3
            java.lang.Integer r1 = (java.lang.Integer) r1
            java.lang.Object r1 = r0.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.os4 r0 = (com.fossil.os4) r0
            com.fossil.t87.a(r13)
            goto L_0x0121
        L_0x003e:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r0)
            throw r13
        L_0x0046:
            java.lang.Object r2 = r0.L$0
            com.fossil.os4 r2 = (com.fossil.os4) r2
            com.fossil.t87.a(r13)
            goto L_0x0067
        L_0x004e:
            com.fossil.t87.a(r13)
            com.fossil.ro4 r13 = r12.k
            java.lang.String r2 = "running"
            java.lang.String r6 = "waiting"
            java.lang.String[] r2 = new java.lang.String[]{r2, r6}
            r0.L$0 = r12
            r0.label = r5
            java.lang.Object r13 = r13.a(r2, r5, r0)
            if (r13 != r1) goto L_0x0066
            return r1
        L_0x0066:
            r2 = r12
        L_0x0067:
            com.fossil.ko4 r13 = (com.fossil.ko4) r13
            java.lang.Object r6 = r13.c()
            java.util.List r6 = (java.util.List) r6
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r7 = r2.a
            java.lang.Boolean r8 = com.fossil.pb7.a(r4)
            r7.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.fossil.os4.m
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "challengesFromServer - wrapper: "
            r9.append(r10)
            r9.append(r13)
            java.lang.String r9 = r9.toString()
            r7.e(r8, r9)
            r7 = 0
            if (r6 == 0) goto L_0x00c6
            boolean r13 = r6.isEmpty()
            if (r13 == 0) goto L_0x00bb
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r13 = r2.b
            java.lang.Boolean r0 = com.fossil.pb7.a(r5)
            r13.a(r0)
            com.fossil.ch5 r13 = r2.l
            java.lang.Long r0 = com.fossil.pb7.a(r7)
            r13.a(r0)
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r13 = r2.f
            java.lang.Boolean r0 = com.fossil.pb7.a(r5)
            r13.a(r0)
            goto L_0x0164
        L_0x00bb:
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r13 = r2.b
            java.lang.Boolean r0 = com.fossil.pb7.a(r4)
            r13.a(r0)
            goto L_0x0164
        L_0x00c6:
            com.portfolio.platform.data.model.ServerError r9 = r13.a()
            if (r9 == 0) goto L_0x0164
            com.portfolio.platform.data.model.ServerError r9 = r13.a()
            java.lang.Integer r9 = r9.getCode()
            r10 = 404(0x194, float:5.66E-43)
            if (r9 != 0) goto L_0x00d9
            goto L_0x0103
        L_0x00d9:
            int r11 = r9.intValue()
            if (r11 != r10) goto L_0x0103
            androidx.lifecycle.LiveData<com.fossil.mn4> r10 = r2.i
            java.lang.Object r10 = r10.a()
            if (r10 == 0) goto L_0x0103
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r13 = r2.b
            java.lang.Boolean r0 = com.fossil.pb7.a(r5)
            r13.a(r0)
            com.fossil.ch5 r13 = r2.l
            java.lang.Long r0 = com.fossil.pb7.a(r7)
            r13.a(r0)
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r13 = r2.f
            java.lang.Boolean r0 = com.fossil.pb7.a(r5)
            r13.a(r0)
            goto L_0x0164
        L_0x0103:
            com.fossil.ti7 r7 = com.fossil.qj7.b()
            com.fossil.os4$d r8 = new com.fossil.os4$d
            r10 = 0
            r8.<init>(r2, r10)
            r0.L$0 = r2
            r0.L$1 = r13
            r0.L$2 = r6
            r0.L$3 = r9
            r0.label = r3
            java.lang.Object r0 = com.fossil.vh7.a(r7, r8, r0)
            if (r0 != r1) goto L_0x011e
            return r1
        L_0x011e:
            r1 = r13
            r13 = r0
            r0 = r2
        L_0x0121:
            com.fossil.mn4 r13 = (com.fossil.mn4) r13
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.os4.m
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "challengesFromServer - localChallenge: "
            r6.append(r7)
            r6.append(r13)
            java.lang.String r6 = r6.toString()
            r2.e(r3, r6)
            if (r13 != 0) goto L_0x0153
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r13 = r0.e
            java.lang.Boolean r0 = com.fossil.pb7.a(r5)
            com.portfolio.platform.data.model.ServerError r1 = r1.a()
            com.fossil.r87 r0 = com.fossil.w87.a(r0, r1)
            r13.a(r0)
            goto L_0x0164
        L_0x0153:
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r13 = r0.e
            java.lang.Boolean r0 = com.fossil.pb7.a(r4)
            com.portfolio.platform.data.model.ServerError r1 = r1.a()
            com.fossil.r87 r0 = com.fossil.w87.a(r0, r1)
            r13.a(r0)
        L_0x0164:
            com.fossil.i97 r13 = com.fossil.i97.a
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.os4.a(com.fossil.fb7):java.lang.Object");
    }
}
