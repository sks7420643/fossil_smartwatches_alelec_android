package com.fossil;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y17 extends u17 {
    @DexIgnore
    public /* final */ MethodCall a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements a27 {
        @DexIgnore
        public /* final */ MethodChannel.Result a;

        @DexIgnore
        public a(y17 y17, MethodChannel.Result result) {
            this.a = result;
        }

        @DexIgnore
        @Override // com.fossil.a27
        public void error(String str, String str2, Object obj) {
            this.a.error(str, str2, obj);
        }

        @DexIgnore
        @Override // com.fossil.a27
        public void success(Object obj) {
            this.a.success(obj);
        }
    }

    @DexIgnore
    public y17(MethodCall methodCall, MethodChannel.Result result) {
        this.a = methodCall;
        this.b = new a(this, result);
    }

    @DexIgnore
    @Override // com.fossil.z17
    public <T> T a(String str) {
        return (T) this.a.argument(str);
    }

    @DexIgnore
    @Override // com.fossil.u17
    public a27 g() {
        return this.b;
    }
}
