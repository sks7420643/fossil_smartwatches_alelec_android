package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p54 extends v54.d.AbstractC0206d.a.b.e {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> c;

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e
    public w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e
    public int b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e
    public String c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a.b.e)) {
            return false;
        }
        v54.d.AbstractC0206d.a.b.e eVar = (v54.d.AbstractC0206d.a.b.e) obj;
        if (!this.a.equals(eVar.c()) || this.b != eVar.b() || !this.c.equals(eVar.a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Thread{name=" + this.a + ", importance=" + this.b + ", frames=" + this.c + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.b.e.AbstractC0214a {
        @DexIgnore
        public String a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> c;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0214a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0214a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0214a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0214a a(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0214a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0214a a(w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> w54) {
            if (w54 != null) {
                this.c = w54;
                return this;
            }
            throw new NullPointerException("Null frames");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0214a
        public v54.d.AbstractC0206d.a.b.e a() {
            String str = "";
            if (this.a == null) {
                str = str + " name";
            }
            if (this.b == null) {
                str = str + " importance";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (str.isEmpty()) {
                return new p54(this.a, this.b.intValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public p54(String str, int i, w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> w54) {
        this.a = str;
        this.b = i;
        this.c = w54;
    }
}
