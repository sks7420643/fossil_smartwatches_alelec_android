package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u53 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<u53> CREATOR; // = new v53();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public u53(int i, int i2, long j, long j2) {
        this.a = i;
        this.b = i2;
        this.c = j;
        this.d = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && u53.class == obj.getClass()) {
            u53 u53 = (u53) obj;
            return this.a == u53.a && this.b == u53.b && this.c == u53.c && this.d == u53.d;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(Integer.valueOf(this.b), Integer.valueOf(this.a), Long.valueOf(this.d), Long.valueOf(this.c));
    }

    @DexIgnore
    public final String toString() {
        return "NetworkLocationStatus:" + " Wifi status: " + this.a + " Cell status: " + this.b + " elapsed time NS: " + this.d + " system time ms: " + this.c;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d);
        k72.a(parcel, a2);
    }
}
