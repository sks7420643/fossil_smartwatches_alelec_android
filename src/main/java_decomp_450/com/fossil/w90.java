package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w90 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ s90 b;
    @DexIgnore
    public /* final */ r90 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w90> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public w90 createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                s90 valueOf = s90.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    return new w90(readInt, valueOf, r90.valueOf(readString2));
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public w90[] newArray(int i) {
            return new w90[i];
        }
    }

    @DexIgnore
    public w90(int i, s90 s90, r90 r90) {
        this.a = i;
        this.b = s90;
        this.c = r90;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.b4, Integer.valueOf(this.a)), r51.t0, yz0.a(this.b)), r51.u0, yz0.a(this.c));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(w90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            w90 w90 = (w90) obj;
            return this.a == w90.a && this.b == w90.b && this.c == w90.c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationEvent");
    }

    @DexIgnore
    public final s90 getAction() {
        return this.b;
    }

    @DexIgnore
    public final r90 getActionStatus() {
        return this.c;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + (Integer.valueOf(this.a).hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
