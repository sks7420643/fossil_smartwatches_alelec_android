package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a20 implements uy<BitmapDrawable>, qy {
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ uy<Bitmap> b;

    @DexIgnore
    public a20(Resources resources, uy<Bitmap> uyVar) {
        u50.a(resources);
        this.a = resources;
        u50.a(uyVar);
        this.b = uyVar;
    }

    @DexIgnore
    public static uy<BitmapDrawable> a(Resources resources, uy<Bitmap> uyVar) {
        if (uyVar == null) {
            return null;
        }
        return new a20(resources, uyVar);
    }

    @DexIgnore
    @Override // com.fossil.uy
    public void b() {
        this.b.b();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return this.b.c();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<BitmapDrawable> d() {
        return BitmapDrawable.class;
    }

    @DexIgnore
    @Override // com.fossil.qy
    public void a() {
        uy<Bitmap> uyVar = this.b;
        if (uyVar instanceof qy) {
            ((qy) uyVar).a();
        }
    }

    @DexIgnore
    @Override // com.fossil.uy
    public BitmapDrawable get() {
        return new BitmapDrawable(this.a, this.b.get());
    }
}
