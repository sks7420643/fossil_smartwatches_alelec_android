package com.fossil;

import com.fossil.pf;
import com.fossil.qf;
import com.fossil.sf;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wf<T> extends qf<T> implements sf.a {
    @DexIgnore
    public /* final */ uf<T> t;
    @DexIgnore
    public pf.a<T> u; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pf.a<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.pf.a
        public void a(int i, pf<T> pfVar) {
            if (pfVar.a()) {
                wf.this.c();
            } else if (!wf.this.h()) {
                if (i == 0 || i == 3) {
                    List<T> list = pfVar.a;
                    if (((qf) wf.this).e.i() == 0) {
                        wf wfVar = wf.this;
                        ((qf) wfVar).e.a(pfVar.b, list, pfVar.c, pfVar.d, ((qf) wfVar).d.a, wfVar);
                    } else {
                        wf wfVar2 = wf.this;
                        ((qf) wfVar2).e.b(pfVar.d, list, ((qf) wfVar2).f, ((qf) wfVar2).d.d, ((qf) wfVar2).h, wfVar2);
                    }
                    wf wfVar3 = wf.this;
                    if (((qf) wfVar3).c != null) {
                        boolean z = true;
                        boolean z2 = ((qf) wfVar3).e.size() == 0;
                        boolean z3 = !z2 && pfVar.b == 0 && pfVar.d == 0;
                        int size = wf.this.size();
                        if (z2 || (!(i == 0 && pfVar.c == 0) && (i != 3 || pfVar.d + ((qf) wf.this).d.a < size))) {
                            z = false;
                        }
                        wf.this.a(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("unexpected resultType" + i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public b(int i) {
            this.a = i;
        }

        @DexIgnore
        public void run() {
            if (!wf.this.h()) {
                wf wfVar = wf.this;
                int i = ((qf) wfVar).d.a;
                if (wfVar.t.isInvalid()) {
                    wf.this.c();
                    return;
                }
                int i2 = this.a * i;
                int min = Math.min(i, ((qf) wf.this).e.size() - i2);
                wf wfVar2 = wf.this;
                wfVar2.t.dispatchLoadRange(3, i2, min, ((qf) wfVar2).a, wfVar2.u);
            }
        }
    }

    @DexIgnore
    public wf(uf<T> ufVar, Executor executor, Executor executor2, qf.c<T> cVar, qf.f fVar, int i) {
        super(new sf(), executor, executor2, cVar, fVar);
        this.t = ufVar;
        int i2 = ((qf) this).d.a;
        ((qf) this).f = i;
        if (ufVar.isInvalid()) {
            c();
            return;
        }
        int max = Math.max(((qf) this).d.e / i2, 2) * i2;
        this.t.dispatchLoadInitial(true, Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, ((qf) this).a, this.u);
    }

    @DexIgnore
    @Override // com.fossil.qf
    public void a(qf<T> qfVar, qf.e eVar) {
        sf<T> sfVar = qfVar.e;
        if (sfVar.isEmpty() || ((qf) this).e.size() != sfVar.size()) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i = ((qf) this).d.a;
        int e = ((qf) this).e.e() / i;
        int i2 = ((qf) this).e.i();
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i3 + e;
            int i5 = 0;
            while (i5 < ((qf) this).e.i()) {
                int i6 = i4 + i5;
                if (!((qf) this).e.b(i, i6) || sfVar.b(i, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                eVar.a(i4 * i, i * i5);
                i3 += i5 - 1;
            }
            i3++;
        }
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void c(int i, int i2) {
        d(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.qf
    public lf<?, T> d() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.qf
    public Object e() {
        return Integer.valueOf(((qf) this).f);
    }

    @DexIgnore
    @Override // com.fossil.qf
    public boolean g() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.qf
    public void d(int i) {
        sf<T> sfVar = ((qf) this).e;
        qf.f fVar = ((qf) this).d;
        sfVar.a(i, fVar.b, fVar.a, this);
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b(int i) {
        ((qf) this).b.execute(new b(i));
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a(int i) {
        e(0, i);
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a(int i, int i2) {
        d(i, i2);
    }
}
