package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qu7<R, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public static Type a(int i, ParameterizedType parameterizedType) {
            return jv7.b(i, parameterizedType);
        }

        @DexIgnore
        public abstract qu7<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3);

        @DexIgnore
        public static Class<?> a(Type type) {
            return jv7.b(type);
        }
    }

    @DexIgnore
    T a(Call<R> call);

    @DexIgnore
    Type a();
}
