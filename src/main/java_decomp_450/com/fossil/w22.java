package com.fossil;

import com.fossil.v02;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w22 extends a32 {
    @DexIgnore
    public /* final */ ArrayList<v02.f> b;
    @DexIgnore
    public /* final */ /* synthetic */ q22 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w22(q22 q22, ArrayList<v02.f> arrayList) {
        super(q22, null);
        this.c = q22;
        this.b = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.a32
    public final void a() {
        this.c.a.s.q = this.c.i();
        ArrayList<v02.f> arrayList = this.b;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            v02.f fVar = arrayList.get(i);
            i++;
            fVar.a(this.c.o, this.c.a.s.q);
        }
    }
}
