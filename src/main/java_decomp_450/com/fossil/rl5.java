package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.fossil.nw6;
import com.fossil.pm5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl5 extends fl4<km5, lm5, jm5> {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ DianaPresetRepository e;
    @DexIgnore
    public /* final */ ch5 f;
    @DexIgnore
    public /* final */ nw5 g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase j;
    @DexIgnore
    public /* final */ vu5 k;
    @DexIgnore
    public /* final */ nw6 l;
    @DexIgnore
    public /* final */ pm5 m;
    @DexIgnore
    public /* final */ qd5 n;
    @DexIgnore
    public /* final */ WatchFaceRepository o;
    @DexIgnore
    public /* final */ AlarmsRepository p;
    @DexIgnore
    public /* final */ WorkoutSettingRepository q;
    @DexIgnore
    public /* final */ DianaPresetRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rl5.s;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ rl5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(rl5 rl5) {
            this.a = rl5;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && mh7.b(stringExtra, this.a.h.c(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(rl5.t.a(), "sync success, remove device now");
                    nj5.d.b(this, CommunicateMode.SYNC);
                    this.a.a(new lm5());
                } else if (intExtra == 2 || intExtra == 4) {
                    nj5.d.b(this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = rl5.t.a();
                    local.d(a2, "sync fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1603) {
                            this.a.a(new jm5(nb5.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT, null));
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.a(new jm5(nb5.FAIL_DUE_TO_SYNC_FAIL, null));
                            return;
                        }
                    }
                    this.a.a(new jm5(nb5.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                } else if (intExtra == 5) {
                    FLogger.INSTANCE.getLocal().d(rl5.t.a(), "sync pending due to workout");
                    this.a.a(new jm5(nb5.FAIL_DUE_TO_PENDING_WORKOUT, null));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {280, 307}, m = "removeBuddyChallengeOnAllPresets")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(rl5 rl5, fb7 fb7) {
            super(fb7);
            this.this$0 = rl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$removeBuddyChallengeOnAllPresets$3", f = "DianaSyncUseCase.kt", l = {307}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $presets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ rl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(rl5 rl5, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = rl5;
            this.$presets = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$presets, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                DianaPresetRepository d = this.this$0.r;
                List<DianaPreset> list = this.$presets;
                this.L$0 = yi7;
                this.label = 1;
                if (d.upsertPresetList(list, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$removeBuddyChallengeOnAllPresets$presets$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super List<? extends DianaPreset>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ rl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(rl5 rl5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = rl5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$serial, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super List<? extends DianaPreset>> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return this.this$0.r.getAllPresets(this.$serial);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {107, 109, 110, 152, 162, 166}, m = "run")
    public static final class f extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(rl5 rl5, fb7 fb7) {
            super(fb7);
            this.this$0 = rl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((km5) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$run$isAA$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        public g(fb7 fb7) {
            super(2, fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return pb7.a(ng5.a().a(PortfolioApp.g0.c()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {242}, m = "setRuleNotificationFilterToDevice")
    public static final class h extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(rl5 rl5, fb7 fb7) {
            super(fb7);
            this.this$0 = rl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements fl4.e<nw6.d, nw6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ rl5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ UserProfile e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements fl4.e<pm5.d, pm5.c> {
            @DexIgnore
            public void a(pm5.c cVar) {
                ee7.b(cVar, "errorValue");
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(pm5.d dVar) {
                ee7.b(dVar, "responseValue");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    i iVar = this.this$0;
                    iVar.a.a(iVar.c, iVar.d, iVar.e, iVar.b);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public i(rl5 rl5, String str, int i, boolean z, UserProfile userProfile) {
            this.a = rl5;
            this.b = str;
            this.c = i;
            this.d = z;
            this.e = userProfile;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(nw6.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = rl5.t.a();
            local.d(a2, "secret key " + dVar.a() + ", start sync");
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b, rl5.t.a(), "[Sync] Verify secret key success, start sync");
            PortfolioApp.g0.c().e(dVar.a(), this.b);
            ik7 unused = xh7.b(this.a.b(), null, null, new b(this, null), 3, null);
        }

        @DexIgnore
        public void a(nw6.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = rl5.t.a();
            local.d(a2, "verify secret key fail, stop sync " + cVar.a());
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.b;
            String a3 = rl5.t.a();
            remote.i(component, session, str, a3, "[Sync] Verify secret key failed by " + cVar.a());
            if (cVar.a() == p60.REQUEST_UNSUPPORTED.getCode() && this.e.getOriginalSyncMode() == 12) {
                this.a.m.a(new pm5.b(this.b, true), new a());
            }
            this.a.a(this.e.getOriginalSyncMode(), this.b, 2);
        }
    }

    /*
    static {
        String simpleName = rl5.class.getSimpleName();
        ee7.a((Object) simpleName, "DianaSyncUseCase::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public rl5(DianaPresetRepository dianaPresetRepository, ch5 ch5, nw5 nw5, PortfolioApp portfolioApp, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, vu5 vu5, nw6 nw6, pm5 pm5, qd5 qd5, WatchFaceRepository watchFaceRepository, AlarmsRepository alarmsRepository, WorkoutSettingRepository workoutSettingRepository, DianaPresetRepository dianaPresetRepository2) {
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(ch5, "mSharedPrefs");
        ee7.b(nw5, "mGetApps");
        ee7.b(portfolioApp, "mApp");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        ee7.b(vu5, "mGetAllContactGroup");
        ee7.b(nw6, "mVerifySecretKeyUseCase");
        ee7.b(pm5, "mUpdateFirmwareUseCase");
        ee7.b(qd5, "mAnalyticsHelper");
        ee7.b(watchFaceRepository, "watchFaceRepository");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        ee7.b(workoutSettingRepository, "mWorkoutSettingRepository");
        ee7.b(dianaPresetRepository2, "presetRepository");
        this.e = dianaPresetRepository;
        this.f = ch5;
        this.g = nw5;
        this.h = portfolioApp;
        this.i = deviceRepository;
        this.j = notificationSettingsDatabase;
        this.k = vu5;
        this.l = nw6;
        this.m = pm5;
        this.n = qd5;
        this.o = watchFaceRepository;
        this.p = alarmsRepository;
        this.q = workoutSettingRepository;
        this.r = dianaPresetRepository2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(km5 km5, fb7 fb7) {
        return a(km5, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void b(int i2, boolean z, UserProfile userProfile, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "verifySecretKey " + str);
        a(userProfile.getOriginalSyncMode(), str, 0);
        this.l.a(new nw6.b(str), new i(this, str, i2, z, userProfile));
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return s;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0135 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0158 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x019c  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0343 A[SYNTHETIC, Splitter:B:75:0x0343] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x03a9 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x03aa  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x03b1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x03ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.km5 r18, com.fossil.fb7<java.lang.Object> r19) {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.fossil.rl5.f
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.rl5$f r3 = (com.fossil.rl5.f) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.rl5$f r3 = new com.fossil.rl5$f
            r3.<init>(r1, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            java.lang.String r7 = "Inside "
            r8 = 6
            r9 = 1
            r10 = 2
            r11 = 13
            r12 = 0
            switch(r5) {
                case 0: goto L_0x00d3;
                case 1: goto L_0x00c3;
                case 2: goto L_0x00b4;
                case 3: goto L_0x009f;
                case 4: goto L_0x0080;
                case 5: goto L_0x005a;
                case 6: goto L_0x0039;
                default: goto L_0x0031;
            }
        L_0x0031:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0039:
            java.lang.Object r0 = r3.L$3
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r3.Z$1
            boolean r0 = r3.Z$0
            java.lang.Object r0 = r3.L$2
            r4 = r0
            com.misfit.frameworks.buttonservice.model.UserProfile r4 = (com.misfit.frameworks.buttonservice.model.UserProfile) r4
            int r5 = r3.I$0
            java.lang.Object r0 = r3.L$1
            r6 = r0
            com.fossil.km5 r6 = (com.fossil.km5) r6
            java.lang.Object r0 = r3.L$0
            r3 = r0
            com.fossil.rl5 r3 = (com.fossil.rl5) r3
            com.fossil.t87.a(r2)     // Catch:{ Exception -> 0x0057 }
            goto L_0x03ad
        L_0x0057:
            r0 = move-exception
            goto L_0x046a
        L_0x005a:
            java.lang.Object r0 = r3.L$3
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r5 = r3.Z$1
            boolean r6 = r3.Z$0
            java.lang.Object r9 = r3.L$2
            com.misfit.frameworks.buttonservice.model.UserProfile r9 = (com.misfit.frameworks.buttonservice.model.UserProfile) r9
            int r10 = r3.I$0
            java.lang.Object r11 = r3.L$1
            com.fossil.km5 r11 = (com.fossil.km5) r11
            java.lang.Object r13 = r3.L$0
            com.fossil.rl5 r13 = (com.fossil.rl5) r13
            com.fossil.t87.a(r2)     // Catch:{ Exception -> 0x0079 }
            r2 = r6
            r14 = r11
            r6 = r5
            r5 = r10
            goto L_0x036b
        L_0x0079:
            r0 = move-exception
            r4 = r9
            r5 = r10
            r6 = r11
            r3 = r13
            goto L_0x046a
        L_0x0080:
            java.lang.Object r0 = r3.L$4
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$3
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r5 = r3.Z$1
            boolean r9 = r3.Z$0
            java.lang.Object r10 = r3.L$2
            com.misfit.frameworks.buttonservice.model.UserProfile r10 = (com.misfit.frameworks.buttonservice.model.UserProfile) r10
            int r13 = r3.I$0
            java.lang.Object r14 = r3.L$1
            com.fossil.km5 r14 = (com.fossil.km5) r14
            java.lang.Object r15 = r3.L$0
            com.fossil.rl5 r15 = (com.fossil.rl5) r15
            com.fossil.t87.a(r2)
            goto L_0x0320
        L_0x009f:
            java.lang.Object r0 = r3.L$2
            com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
            int r5 = r3.I$0
            java.lang.Object r13 = r3.L$1
            com.fossil.km5 r13 = (com.fossil.km5) r13
            java.lang.Object r14 = r3.L$0
            com.fossil.rl5 r14 = (com.fossil.rl5) r14
            com.fossil.t87.a(r2)
            r15 = r14
            r14 = r13
            goto L_0x015e
        L_0x00b4:
            int r0 = r3.I$0
            java.lang.Object r5 = r3.L$1
            com.fossil.km5 r5 = (com.fossil.km5) r5
            java.lang.Object r13 = r3.L$0
            com.fossil.rl5 r13 = (com.fossil.rl5) r13
            com.fossil.t87.a(r2)
            goto L_0x013c
        L_0x00c3:
            int r0 = r3.I$0
            java.lang.Object r5 = r3.L$1
            com.fossil.km5 r5 = (com.fossil.km5) r5
            java.lang.Object r13 = r3.L$0
            com.fossil.rl5 r13 = (com.fossil.rl5) r13
            com.fossil.t87.a(r2)
            r2 = r0
            r0 = r5
            goto L_0x0121
        L_0x00d3:
            com.fossil.t87.a(r2)
            if (r0 != 0) goto L_0x00e0
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_INVALID_REQUEST
            r0.<init>(r2, r12)
            return r0
        L_0x00e0:
            int r2 = r18.b()
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r13 = r17.c()
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "start on thread="
            r14.append(r15)
            java.lang.Thread r15 = java.lang.Thread.currentThread()
            java.lang.String r6 = "Thread.currentThread()"
            com.fossil.ee7.a(r15, r6)
            java.lang.String r6 = r15.getName()
            r14.append(r6)
            java.lang.String r6 = r14.toString()
            r5.d(r13, r6)
            com.portfolio.platform.data.source.WorkoutSettingRepository r5 = r1.q
            r3.L$0 = r1
            r3.L$1 = r0
            r3.I$0 = r2
            r3.label = r9
            java.lang.Object r5 = r5.downloadWorkoutSettings(r3)
            if (r5 != r4) goto L_0x0120
            return r4
        L_0x0120:
            r13 = r1
        L_0x0121:
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            r3.L$0 = r13
            r3.L$1 = r0
            r3.I$0 = r2
            r3.label = r10
            java.lang.Object r5 = r5.d(r3)
            if (r5 != r4) goto L_0x0136
            return r4
        L_0x0136:
            r16 = r5
            r5 = r0
            r0 = r2
            r2 = r16
        L_0x013c:
            com.misfit.frameworks.buttonservice.model.UserProfile r2 = (com.misfit.frameworks.buttonservice.model.UserProfile) r2
            com.fossil.ti7 r6 = com.fossil.qj7.b()
            com.fossil.rl5$g r14 = new com.fossil.rl5$g
            r14.<init>(r12)
            r3.L$0 = r13
            r3.L$1 = r5
            r3.I$0 = r0
            r3.L$2 = r2
            r15 = 3
            r3.label = r15
            java.lang.Object r6 = com.fossil.vh7.a(r6, r14, r3)
            if (r6 != r4) goto L_0x0159
            return r4
        L_0x0159:
            r14 = r5
            r15 = r13
            r5 = r0
            r0 = r2
            r2 = r6
        L_0x015e:
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            java.lang.String r6 = "Error inside "
            if (r2 != 0) goto L_0x019c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = r15.c()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r6)
            java.lang.String r4 = r15.c()
            r3.append(r4)
            java.lang.String r4 = ".startDeviceSync - FAIL_DUE_TO_UAA"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.e(r2, r3)
            java.lang.String r0 = r14.a()
            r15.a(r5, r0, r8)
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_UAA
            r0.<init>(r2, r12)
            return r0
        L_0x019c:
            if (r0 != 0) goto L_0x01d5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = r15.c()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r6)
            java.lang.String r6 = r15.c()
            r4.append(r6)
            java.lang.String r6 = ".startDeviceSync - user is null isAA "
            r4.append(r6)
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            r0.e(r3, r2)
            java.lang.String r0 = r14.a()
            r15.a(r5, r0, r10)
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_INVALID_REQUEST
            r0.<init>(r2, r12)
            return r0
        L_0x01d5:
            java.lang.String r13 = r14.a()
            boolean r13 = android.text.TextUtils.isEmpty(r13)
            if (r13 == 0) goto L_0x0213
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = r15.c()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r6)
            java.lang.String r4 = r15.c()
            r3.append(r4)
            java.lang.String r4 = ".startDeviceSync - serial is null"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.e(r2, r3)
            java.lang.String r0 = r14.a()
            r15.a(r5, r0, r10)
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_INVALID_REQUEST
            r0.<init>(r2, r12)
            return r0
        L_0x0213:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r10 = r15.c()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r7)
            java.lang.String r12 = r15.c()
            r13.append(r12)
            java.lang.String r12 = ".startDeviceSync - serial="
            r13.append(r12)
            java.lang.String r12 = r14.a()
            r13.append(r12)
            java.lang.String r12 = ","
            r13.append(r12)
            java.lang.String r12 = " weightInKg="
            r13.append(r12)
            com.misfit.frameworks.buttonservice.model.UserBiometricData r12 = r0.getUserBiometricData()
            float r12 = r12.getWeightInKilogram()
            r13.append(r12)
            java.lang.String r12 = ", heightInMeter="
            r13.append(r12)
            com.misfit.frameworks.buttonservice.model.UserBiometricData r12 = r0.getUserBiometricData()
            float r12 = r12.getHeightInMeter()
            r13.append(r12)
            java.lang.String r12 = ", goal="
            r13.append(r12)
            long r8 = r0.getGoalSteps()
            r13.append(r8)
            java.lang.String r8 = ", isNewDevice="
            r13.append(r8)
            boolean r8 = r14.c()
            r13.append(r8)
            java.lang.String r8 = ", SyncMode="
            r13.append(r8)
            r13.append(r5)
            java.lang.String r8 = r13.toString()
            r6.d(r10, r8)
            r0.setOriginalSyncMode(r5)
            com.fossil.ch5 r6 = r15.f
            java.lang.String r8 = r14.a()
            boolean r6 = r6.i(r8)
            if (r6 == 0) goto L_0x0298
            if (r5 != r11) goto L_0x0296
            goto L_0x0298
        L_0x0296:
            r13 = r5
            goto L_0x029a
        L_0x0298:
            r13 = 13
        L_0x029a:
            if (r13 == r11) goto L_0x02d6
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            java.lang.String r8 = r14.a()
            boolean r5 = r5.g(r8)
            if (r5 == 0) goto L_0x02d6
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = r15.c()
            java.lang.String r3 = "Device is syncing, wait for result..."
            r0.e(r2, r3)
            com.fossil.fl4$e r0 = r15.a()
            if (r0 == 0) goto L_0x02d0
            com.fossil.nj5 r0 = com.fossil.nj5.d
            com.fossil.rl5$b r2 = r15.d
            r3 = 1
            com.misfit.frameworks.buttonservice.communite.CommunicateMode[] r3 = new com.misfit.frameworks.buttonservice.communite.CommunicateMode[r3]
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r4 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
            r5 = 0
            r3[r5] = r4
            r0.a(r2, r3)
        L_0x02d0:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            return r0
        L_0x02d6:
            com.fossil.ch5 r5 = r15.f
            java.lang.Boolean r5 = r5.a()
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = com.fossil.rl5.s
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r12 = "isBCOn : "
            r10.append(r12)
            r10.append(r5)
            java.lang.String r10 = r10.toString()
            r8.e(r9, r10)
            boolean r8 = r5.booleanValue()
            if (r8 != 0) goto L_0x0324
            java.lang.String r8 = r14.a()
            r3.L$0 = r15
            r3.L$1 = r14
            r3.I$0 = r13
            r3.L$2 = r0
            r3.Z$0 = r2
            r3.Z$1 = r6
            r3.L$3 = r5
            r3.L$4 = r8
            r9 = 4
            r3.label = r9
            java.lang.Object r8 = r15.a(r8, r3)
            if (r8 != r4) goto L_0x031c
            return r4
        L_0x031c:
            r10 = r0
            r9 = r2
            r0 = r5
            r5 = r6
        L_0x0320:
            r6 = r5
            r2 = r9
        L_0x0322:
            r5 = r13
            goto L_0x0327
        L_0x0324:
            r10 = r0
            r0 = r5
            goto L_0x0322
        L_0x0327:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = com.fossil.rl5.s
            java.lang.String r12 = "start set localization"
            r8.e(r9, r12)
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            java.lang.String r9 = r14.a()
            r8.p(r9)
            if (r5 != r11) goto L_0x0491
            com.fossil.ch5 r8 = r15.f     // Catch:{ Exception -> 0x0466 }
            java.lang.String r9 = r14.a()     // Catch:{ Exception -> 0x0466 }
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0466 }
            r13 = 0
            r8.a(r9, r11, r13)     // Catch:{ Exception -> 0x0466 }
            r3.L$0 = r15     // Catch:{ Exception -> 0x0466 }
            r3.L$1 = r14     // Catch:{ Exception -> 0x0466 }
            r3.I$0 = r5     // Catch:{ Exception -> 0x0466 }
            r3.L$2 = r10     // Catch:{ Exception -> 0x0466 }
            r3.Z$0 = r2     // Catch:{ Exception -> 0x0466 }
            r3.Z$1 = r6     // Catch:{ Exception -> 0x0466 }
            r3.L$3 = r0     // Catch:{ Exception -> 0x0466 }
            r8 = 5
            r3.label = r8     // Catch:{ Exception -> 0x0466 }
            java.lang.Object r8 = r15.a(r3)     // Catch:{ Exception -> 0x0466 }
            if (r8 != r4) goto L_0x0369
            return r4
        L_0x0369:
            r9 = r10
            r13 = r15
        L_0x036b:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0461 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ Exception -> 0x0461 }
            java.lang.String r10 = r13.c()     // Catch:{ Exception -> 0x0461 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0461 }
            r11.<init>()     // Catch:{ Exception -> 0x0461 }
            r11.append(r7)     // Catch:{ Exception -> 0x0461 }
            java.lang.String r7 = r13.c()     // Catch:{ Exception -> 0x0461 }
            r11.append(r7)     // Catch:{ Exception -> 0x0461 }
            java.lang.String r7 = ".startDeviceSync - Start full-sync"
            r11.append(r7)     // Catch:{ Exception -> 0x0461 }
            java.lang.String r7 = r11.toString()     // Catch:{ Exception -> 0x0461 }
            r8.d(r10, r7)     // Catch:{ Exception -> 0x0461 }
            com.portfolio.platform.data.source.AlarmsRepository r7 = r13.p     // Catch:{ Exception -> 0x0461 }
            r3.L$0 = r13     // Catch:{ Exception -> 0x0461 }
            r3.L$1 = r14     // Catch:{ Exception -> 0x0461 }
            r3.I$0 = r5     // Catch:{ Exception -> 0x0461 }
            r3.L$2 = r9     // Catch:{ Exception -> 0x0461 }
            r3.Z$0 = r2     // Catch:{ Exception -> 0x0461 }
            r3.Z$1 = r6     // Catch:{ Exception -> 0x0461 }
            r3.L$3 = r0     // Catch:{ Exception -> 0x0461 }
            r0 = 6
            r3.label = r0     // Catch:{ Exception -> 0x0461 }
            java.lang.Object r2 = r7.getActiveAlarms(r3)     // Catch:{ Exception -> 0x0461 }
            if (r2 != r4) goto L_0x03aa
            return r4
        L_0x03aa:
            r4 = r9
            r3 = r13
            r6 = r14
        L_0x03ad:
            java.util.List r2 = (java.util.List) r2
            if (r2 != 0) goto L_0x03b6
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
        L_0x03b6:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.util.List r2 = com.fossil.rc5.a(r2)
            r0.a(r2)
            com.portfolio.platform.data.source.DianaPresetRepository r0 = r3.e
            java.lang.String r2 = r6.a()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = r0.getActivePresetBySerial(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r7 = r3.c()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "startDeviceSync activePreset="
            r8.append(r9)
            r8.append(r0)
            java.lang.String r8 = r8.toString()
            r2.d(r7, r8)
            if (r0 == 0) goto L_0x048e
            java.util.ArrayList r2 = r0.getComplications()
            com.google.gson.Gson r7 = new com.google.gson.Gson
            r7.<init>()
            com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings r2 = com.fossil.xc5.a(r2, r7)
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            java.lang.String r8 = r6.a()
            r7.a(r2, r8)
            java.util.ArrayList r2 = r0.getWatchapps()
            com.google.gson.Gson r7 = new com.google.gson.Gson
            r7.<init>()
            com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings r2 = com.fossil.xc5.b(r2, r7)
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            java.lang.String r8 = r6.a()
            r7.a(r2, r8)
            com.portfolio.platform.data.source.WatchFaceRepository r2 = r3.o
            java.lang.String r7 = r0.getWatchFaceId()
            com.portfolio.platform.data.model.diana.preset.WatchFace r2 = r2.getWatchFaceWithId(r7)
            if (r2 == 0) goto L_0x0436
            java.util.ArrayList r0 = r0.getComplications()
            com.misfit.frameworks.buttonservice.model.background.BackgroundConfig r12 = com.fossil.yc5.a(r2, r0)
            goto L_0x0437
        L_0x0436:
            r12 = 0
        L_0x0437:
            if (r12 == 0) goto L_0x0446
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r2 = r6.a()
            r0.a(r12, r2)
        L_0x0446:
            com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData r0 = new com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData
            r2 = 686(0x2ae, float:9.61E-43)
            r7 = 38
            r8 = 50
            r9 = 14
            r0.<init>(r2, r7, r8, r9)
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r7 = r6.a()
            r2.a(r0, r7)
            goto L_0x048e
        L_0x0461:
            r0 = move-exception
            r4 = r9
            r3 = r13
            r6 = r14
            goto L_0x046a
        L_0x0466:
            r0 = move-exception
            r4 = r10
            r6 = r14
            r3 = r15
        L_0x046a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r7 = r2.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r8 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r9 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            java.lang.String r10 = r6.a()
            java.lang.String r11 = com.fossil.rl5.s
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r12 = "[Sync] Exception when prepare settings "
            r2.append(r12)
            r2.append(r0)
            java.lang.String r12 = r2.toString()
            r7.i(r8, r9, r10, r11, r12)
        L_0x048e:
            r15 = r3
            r10 = r4
            r14 = r6
        L_0x0491:
            boolean r0 = r14.c()
            java.lang.String r2 = r14.a()
            r15.b(r5, r0, r10, r2)
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            return r0
            switch-data {0->0x00d3, 1->0x00c3, 2->0x00b4, 3->0x009f, 4->0x0080, 5->0x005a, 6->0x0039, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rl5.a(com.fossil.km5, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.fossil.rl5.h
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.rl5$h r0 = (com.fossil.rl5.h) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.rl5$h r0 = new com.fossil.rl5$h
            r0.<init>(r7, r8)
        L_0x0018:
            r6 = r0
            java.lang.Object r8 = r6.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r6.label
            r2 = 1
            if (r1 == 0) goto L_0x0036
            if (r1 != r2) goto L_0x002e
            java.lang.Object r0 = r6.L$0
            com.fossil.rl5 r0 = (com.fossil.rl5) r0
            com.fossil.t87.a(r8)
            goto L_0x004f
        L_0x002e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0036:
            com.fossil.t87.a(r8)
            com.fossil.nx6 r1 = com.fossil.nx6.b
            com.fossil.nw5 r8 = r7.g
            com.fossil.vu5 r3 = r7.k
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r4 = r7.j
            com.fossil.ch5 r5 = r7.f
            r6.L$0 = r7
            r6.label = r2
            r2 = r8
            java.lang.Object r8 = r1.a(r2, r3, r4, r5, r6)
            if (r8 != r0) goto L_0x004f
            return r0
        L_0x004f:
            java.util.List r8 = (java.util.List) r8
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r0 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            long r1 = java.lang.System.currentTimeMillis()
            r0.<init>(r8, r1)
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r1 = r1.c()
            r8.a(r0, r1)
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rl5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(int i2, String str, int i3) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String c2 = c();
        local.d(c2, "broadcastSyncStatus serial=" + str + ' ' + i3);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), i2);
        intent.putExtra("SERIAL", str);
        nj5 nj5 = nj5.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        nj5.a(communicateMode, new nj5.a(communicateMode, str, intent));
    }

    @DexIgnore
    public final void a(int i2, boolean z, UserProfile userProfile, String str) {
        ee7.b(userProfile, "userProfile");
        ee7.b(str, "serial");
        if (a() != null) {
            nj5.d.a(this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String c2 = c();
        local.d(c2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.b0()) {
            SKUModel skuModelBySerialPrefix = this.i.getSkuModelBySerialPrefix(be5.o.b(str));
            this.f.c((Boolean) true);
            this.n.a(i2, skuModelBySerialPrefix);
            if5 b2 = qd5.f.b("sync_session");
            qd5.f.a("sync_session", b2);
            b2.d();
        }
        if (!PortfolioApp.g0.c().a(str, userProfile)) {
            a(userProfile.getOriginalSyncMode(), str, 2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r17, com.fossil.fb7<? super com.fossil.i97> r18) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            boolean r3 = r2 instanceof com.fossil.rl5.c
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.rl5$c r3 = (com.fossil.rl5.c) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.rl5$c r3 = new com.fossil.rl5$c
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 0
            r7 = 2
            r8 = 1
            if (r5 == 0) goto L_0x0058
            if (r5 == r8) goto L_0x004c
            if (r5 != r7) goto L_0x0044
            java.lang.Object r1 = r3.L$3
            com.fossil.oe7 r1 = (com.fossil.oe7) r1
            java.lang.Object r1 = r3.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r3.L$0
            com.fossil.rl5 r1 = (com.fossil.rl5) r1
            com.fossil.t87.a(r2)
            goto L_0x0119
        L_0x0044:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004c:
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r5 = r3.L$0
            com.fossil.rl5 r5 = (com.fossil.rl5) r5
            com.fossil.t87.a(r2)
            goto L_0x0072
        L_0x0058:
            com.fossil.t87.a(r2)
            com.fossil.ti7 r2 = com.fossil.qj7.b()
            com.fossil.rl5$e r5 = new com.fossil.rl5$e
            r5.<init>(r0, r1, r6)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.label = r8
            java.lang.Object r2 = com.fossil.vh7.a(r2, r5, r3)
            if (r2 != r4) goto L_0x0071
            return r4
        L_0x0071:
            r5 = r0
        L_0x0072:
            java.util.List r2 = (java.util.List) r2
            com.fossil.oe7 r9 = new com.fossil.oe7
            r9.<init>()
            r10 = 0
            r9.element = r10
            java.util.Iterator r10 = r2.iterator()
        L_0x0080:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x00ed
            java.lang.Object r11 = r10.next()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r11 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r11
            java.util.ArrayList r12 = r11.getWatchapps()
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.util.Iterator r12 = r12.iterator()
        L_0x0099:
            boolean r14 = r12.hasNext()
            if (r14 == 0) goto L_0x00bd
            java.lang.Object r14 = r12.next()
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r14 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r14
            java.lang.String r15 = r14.getId()
            java.lang.String r6 = "buddy-challenge"
            boolean r6 = com.fossil.ee7.a(r15, r6)
            r6 = r6 ^ r8
            if (r6 == 0) goto L_0x00b6
            r13.add(r14)
            goto L_0x00bb
        L_0x00b6:
            r9.element = r8
            r11.setPinType(r7)
        L_0x00bb:
            r6 = 0
            goto L_0x0099
        L_0x00bd:
            r11.setWatchapps(r13)
            boolean r6 = r11.isActive()
            if (r6 == 0) goto L_0x00eb
            int r6 = r11.getPinType()
            if (r6 != r7) goto L_0x00eb
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r11 = com.fossil.rl5.s
            java.lang.String r12 = "removeBuddyChallengeOnAllPresets - start reset active watchApp"
            r6.e(r11, r12)
            com.google.gson.Gson r6 = new com.google.gson.Gson
            r6.<init>()
            com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings r6 = com.fossil.xc5.b(r13, r6)
            com.portfolio.platform.PortfolioApp$a r11 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r11 = r11.c()
            r11.a(r6, r1)
        L_0x00eb:
            r6 = 0
            goto L_0x0080
        L_0x00ed:
            boolean r6 = r9.element
            if (r6 == 0) goto L_0x0119
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r8 = com.fossil.rl5.s
            java.lang.String r10 = "removeBuddyChallengeOnAllPresets - reset watch apps in database"
            r6.e(r8, r10)
            com.fossil.ti7 r6 = com.fossil.qj7.b()
            com.fossil.rl5$d r8 = new com.fossil.rl5$d
            r10 = 0
            r8.<init>(r5, r2, r10)
            r3.L$0 = r5
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r9
            r3.label = r7
            java.lang.Object r1 = com.fossil.vh7.a(r6, r8, r3)
            if (r1 != r4) goto L_0x0119
            return r4
        L_0x0119:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rl5.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
