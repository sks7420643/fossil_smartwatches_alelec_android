package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm7 {
    @DexIgnore
    public Object[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ ib7 c;

    @DexIgnore
    public sm7(ib7 ib7, int i) {
        this.c = ib7;
        this.a = new Object[i];
    }

    @DexIgnore
    public final ib7 a() {
        return this.c;
    }

    @DexIgnore
    public final void b() {
        this.b = 0;
    }

    @DexIgnore
    public final Object c() {
        Object[] objArr = this.a;
        int i = this.b;
        this.b = i + 1;
        return objArr[i];
    }

    @DexIgnore
    public final void a(Object obj) {
        Object[] objArr = this.a;
        int i = this.b;
        this.b = i + 1;
        objArr[i] = obj;
    }
}
