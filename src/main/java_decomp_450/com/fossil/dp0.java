package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dp0 implements Runnable {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ vc7<i97> b;

    @DexIgnore
    public dp0(v81 v81, vc7<i97> vc7) {
        this.b = vc7;
    }

    @DexIgnore
    public void run() {
        if (!this.a) {
            this.b.invoke();
        }
    }
}
