package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aa1 extends fe7 implements kd7<v81, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk1 a;
    @DexIgnore
    public /* final */ /* synthetic */ bi1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa1(zk1 zk1, bi1 bi1) {
        super(2);
        this.a = zk1;
        this.b = bi1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(v81 v81, Float f) {
        float floatValue = f.floatValue();
        if (this.a.I > 0) {
            zk1 zk1 = this.a;
            floatValue = ((((float) (this.b.f - this.a.M)) * floatValue) + ((float) (zk1.M + zk1.J))) / ((float) this.a.I);
        }
        if (Math.abs(floatValue - this.a.K) > this.a.T || floatValue == 1.0f) {
            zk1 zk12 = this.a;
            zk12.K = floatValue;
            zk12.a(floatValue);
        }
        return i97.a;
    }
}
