package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn0 extends sh0 {
    @DexIgnore
    public /* final */ f31 K;
    @DexIgnore
    public /* final */ byte[] L;

    @DexIgnore
    public jn0(ri1 ri1, f31 f31, byte[] bArr) {
        super(ri1, x81.SEND_BOTH_SIDES_RANDOM_NUMBERS, qa1.H, 0, 8);
        this.K = f31;
        this.L = bArr;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.s2, yz0.a(this.K)), r51.o2, yz0.a(this.L, (String) null, 1));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(this.L.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.K.a).put(this.L).array();
        ee7.a((Object) array, "ByteBuffer.allocate(KEY_\u2026\n                .array()");
        return array;
    }
}
