package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so1 {
    @DexIgnore
    public static /* final */ HashMap<r87<String, ru0>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ so1 b; // = new so1();

    @DexIgnore
    public final xp0 a(String str, byte[] bArr) {
        xp0 we1;
        xp0 ik0;
        if (bArr.length < 3) {
            t11 t11 = t11.a;
            return null;
        }
        cy0 a2 = cy0.d.a(bArr[0]);
        ru0 a3 = ru0.o.a(bArr[1]);
        byte b2 = (byte) (bArr[2] & -1);
        if (a2 == null) {
            t11 t112 = t11.a;
        }
        if (a3 == null) {
            t11 t113 = t11.a;
            return null;
        }
        Byte b3 = a.get(new r87(str, a3));
        byte byteValue = b3 != null ? b3.byteValue() : -1;
        byte b4 = (byte) ((byteValue + 1) % (yz0.b((byte) -1) + 1));
        a.put(new r87<>(str, a3), Byte.valueOf(b2));
        if (byteValue == b2) {
            t11 t114 = t11.a;
        } else if (b2 != b4) {
            t11 t115 = t11.a;
        }
        byte[] a4 = s97.a(bArr, 3, bArr.length);
        switch (tm1.a[a3.ordinal()]) {
            case 1:
                try {
                    Charset forName = Charset.forName("UTF-8");
                    ee7.a((Object) forName, "Charset.forName(\"UTF-8\")");
                    JSONObject jSONObject = new JSONObject(new String(a4, forName)).getJSONObject("req");
                    int i = jSONObject.getInt("id");
                    ee7.a((Object) jSONObject, "requestJSON");
                    we1 = new we1(b2, i, jSONObject);
                    return we1;
                } catch (JSONException e) {
                    wl0.h.a(e);
                    return null;
                }
            case 2:
                return new ib1(b2);
            case 3:
                return null;
            case 4:
                try {
                    int i2 = ByteBuffer.wrap(a4).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                    s90 a5 = s90.c.a(a4[4]);
                    if (a5 == null) {
                        return null;
                    }
                    int i3 = tm1.b[a5.ordinal()];
                    if (i3 == 1) {
                        ik0 = new ik0(b2, i2, new p90());
                    } else if (i3 == 2) {
                        ik0 = new ik0(b2, i2, new u90());
                    } else if (i3 == 3) {
                        ik0 = new ik0(b2, i2, new t90());
                    } else if (i3 == 4) {
                        byte b5 = a4[5];
                        ByteBuffer order = ByteBuffer.wrap(a4, 6, 4).order(ByteOrder.LITTLE_ENDIAN);
                        ee7.a((Object) order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
                        return new ik0(b2, i2, new v90(b5, order.getInt()));
                    } else {
                        throw new p87();
                    }
                    return ik0;
                } catch (Exception e2) {
                    wl0.h.a(e2);
                    return null;
                }
            case 5:
                try {
                    k90 a6 = k90.c.a(a4[0]);
                    if (a6 != null) {
                        return new lm1(b2, a6);
                    }
                    return null;
                } catch (IllegalArgumentException e3) {
                    wl0.h.a(e3);
                    return null;
                }
            case 6:
                try {
                    ik0 = bx0.CREATOR.a(b2, a4);
                    return ik0;
                } catch (IllegalArgumentException e4) {
                    wl0.h.a(e4);
                    return null;
                }
            case 7:
                return new kq1(b2);
            case 8:
                try {
                    ik0 = pi1.CREATOR.a(b2, a4);
                    return ik0;
                } catch (IllegalArgumentException e5) {
                    wl0.h.a(e5);
                    return null;
                }
            case 9:
                return new gm0(b2);
            case 10:
                return new pt0(b2);
            case 11:
                try {
                    sf0 a7 = sf0.c.a(a4[0]);
                    if (a7 == null) {
                        return null;
                    }
                    we1 = new l01(b2, a7, a4[1]);
                    return we1;
                } catch (Exception e6) {
                    wl0.h.a(e6);
                    return null;
                }
            case 12:
                return a(b2, a4);
            default:
                throw new p87();
        }
    }

    @DexIgnore
    public final t71 a(byte b2, byte[] bArr) {
        int i;
        int i2;
        try {
            wf0 a2 = wf0.c.a(bArr[0]);
            vf0 a3 = vf0.c.a(bArr[1]);
            yf0 a4 = yf0.c.a(bArr[2]);
            byte[] a5 = s97.a(bArr, 3, bArr.length);
            if (a3 == vf0.XOR) {
                ByteBuffer wrap = ByteBuffer.wrap(s97.a(bArr, 6, 10));
                int b3 = yz0.b(wrap.getShort(0));
                i = yz0.b(wrap.getShort(2));
                i2 = b3;
            } else {
                i2 = 0;
                i = 0;
            }
            if (!(a2 == null || a3 == null || a4 == null)) {
                return new t71(b2, a2, a3, a4, a5, i2, i);
            }
        } catch (Exception e) {
            wl0.h.a(e);
        }
        return null;
    }

    @DexIgnore
    public final void a(String str) {
        synchronized (a) {
            Set<Map.Entry<r87<String, ru0>, Byte>> entrySet = a.entrySet();
            ee7.a((Object) entrySet, "eventSequenceManager.entries");
            for (T t : entrySet) {
                if (ee7.a((Object) str, (Object) ((String) ((r87) t.getKey()).getFirst()))) {
                    AbstractMap abstractMap = a;
                    Object key = t.getKey();
                    ee7.a(key, "entry.key");
                    abstractMap.put(key, (byte) -1);
                }
            }
            i97 i97 = i97.a;
        }
    }
}
