package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y0 {
    @DexIgnore
    public static /* final */ int abc_vector_test; // = 2131230811;
    @DexIgnore
    public static /* final */ int notification_action_background; // = 2131231252;
    @DexIgnore
    public static /* final */ int notification_bg; // = 2131231253;
    @DexIgnore
    public static /* final */ int notification_bg_low; // = 2131231254;
    @DexIgnore
    public static /* final */ int notification_bg_low_normal; // = 2131231255;
    @DexIgnore
    public static /* final */ int notification_bg_low_pressed; // = 2131231256;
    @DexIgnore
    public static /* final */ int notification_bg_normal; // = 2131231257;
    @DexIgnore
    public static /* final */ int notification_bg_normal_pressed; // = 2131231258;
    @DexIgnore
    public static /* final */ int notification_icon_background; // = 2131231260;
    @DexIgnore
    public static /* final */ int notification_template_icon_bg; // = 2131231265;
    @DexIgnore
    public static /* final */ int notification_template_icon_low_bg; // = 2131231266;
    @DexIgnore
    public static /* final */ int notification_tile_bg; // = 2131231267;
    @DexIgnore
    public static /* final */ int notify_panel_notification_icon_bg; // = 2131231268;
}
