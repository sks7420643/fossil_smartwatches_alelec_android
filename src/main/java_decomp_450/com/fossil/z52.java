package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z52<T> extends e12, Closeable, Iterable<T> {
    @DexIgnore
    T get(int i);

    @DexIgnore
    int getCount();
}
