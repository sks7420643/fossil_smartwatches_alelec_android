package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fs0 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vv0 a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fs0(vv0 vv0, long j) {
        super(1);
        this.a = vv0;
        this.b = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        if (this.b == ((zh0) v81).L) {
            vv0 vv0 = this.a;
            vv0.I = vv0.H;
            vv0 vv02 = this.a;
            long j = vv02.G;
            long j2 = vv02.I;
            if (j != j2) {
                zk0.a(vv02, new j31(Math.max(0L, vv02.G - ((long) 4)), vv02.P, ((zk0) vv02).w, 0, 8), new i61(vv02), new g81(vv02), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
            } else if (j2 == vv02.D) {
                vv02.n();
            } else {
                vv02.m();
            }
        } else {
            vv0 vv03 = this.a;
            vv03.H = Math.max(0L, vv03.H - 6144);
            vv0 vv04 = this.a;
            if (vv04.H == 0) {
                vv04.m();
            } else {
                vv04.o();
            }
        }
        return i97.a;
    }
}
