package com.fossil;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import com.fossil.qm;
import com.misfit.frameworks.common.constants.Constants;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gn implements ym, rn, vm {
    @DexIgnore
    public static /* final */ String i; // = im.a("GreedyScheduler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ dn b;
    @DexIgnore
    public /* final */ sn c;
    @DexIgnore
    public /* final */ Set<zo> d; // = new HashSet();
    @DexIgnore
    public fn e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public Boolean h;

    @DexIgnore
    public gn(Context context, zl zlVar, vp vpVar, dn dnVar) {
        this.a = context;
        this.b = dnVar;
        this.c = new sn(context, vpVar, this);
        this.e = new fn(this, zlVar.h());
        this.g = new Object();
    }

    @DexIgnore
    @Override // com.fossil.ym
    public void a(zo... zoVarArr) {
        if (this.h == null) {
            this.h = Boolean.valueOf(TextUtils.equals(this.a.getPackageName(), b()));
        }
        if (!this.h.booleanValue()) {
            im.a().c(i, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        c();
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (zo zoVar : zoVarArr) {
            long a2 = zoVar.a();
            long currentTimeMillis = System.currentTimeMillis();
            if (zoVar.b == qm.a.ENQUEUED) {
                if (currentTimeMillis < a2) {
                    fn fnVar = this.e;
                    if (fnVar != null) {
                        fnVar.a(zoVar);
                    }
                } else if (!zoVar.b()) {
                    im.a().a(i, String.format("Starting work for %s", zoVar.a), new Throwable[0]);
                    this.b.a(zoVar.a);
                } else if (Build.VERSION.SDK_INT >= 23 && zoVar.j.h()) {
                    im.a().a(i, String.format("Ignoring WorkSpec %s, Requires device idle.", zoVar), new Throwable[0]);
                } else if (Build.VERSION.SDK_INT < 24 || !zoVar.j.e()) {
                    hashSet.add(zoVar);
                    hashSet2.add(zoVar.a);
                } else {
                    im.a().a(i, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", zoVar), new Throwable[0]);
                }
            }
        }
        synchronized (this.g) {
            if (!hashSet.isEmpty()) {
                im.a().a(i, String.format("Starting tracking for [%s]", TextUtils.join(",", hashSet2)), new Throwable[0]);
                this.d.addAll(hashSet);
                this.c.a(this.d);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ym
    public boolean a() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.rn
    public void b(List<String> list) {
        for (String str : list) {
            im.a().a(i, String.format("Constraints met: Scheduling work ID %s", str), new Throwable[0]);
            this.b.a(str);
        }
    }

    @DexIgnore
    public final void c() {
        if (!this.f) {
            this.b.d().a(this);
            this.f = true;
        }
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this.g) {
            Iterator<zo> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                zo next = it.next();
                if (next.a.equals(str)) {
                    im.a().a(i, String.format("Stopping tracking for %s", str), new Throwable[0]);
                    this.d.remove(next);
                    this.c.a(this.d);
                    break;
                }
            }
        }
    }

    @DexIgnore
    public final String b() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        Object obj;
        if (Build.VERSION.SDK_INT >= 28) {
            return Application.getProcessName();
        }
        try {
            Class<?> cls = Class.forName("android.app.ActivityThread", false, gn.class.getClassLoader());
            if (Build.VERSION.SDK_INT >= 18) {
                Method declaredMethod = cls.getDeclaredMethod("currentProcessName", new Class[0]);
                declaredMethod.setAccessible(true);
                obj = declaredMethod.invoke(null, new Object[0]);
            } else {
                Method declaredMethod2 = cls.getDeclaredMethod("currentActivityThread", new Class[0]);
                declaredMethod2.setAccessible(true);
                Method declaredMethod3 = cls.getDeclaredMethod("getProcessName", new Class[0]);
                declaredMethod3.setAccessible(true);
                obj = declaredMethod3.invoke(declaredMethod2.invoke(null, new Object[0]), new Object[0]);
            }
            if (obj instanceof String) {
                return (String) obj;
            }
        } catch (Throwable th) {
            im.a().a(i, "Unable to check ActivityThread for processName", th);
        }
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) this.a.getSystemService(Constants.ACTIVITY);
        if (activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.isEmpty()) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if (runningAppProcessInfo.pid == myPid) {
                return runningAppProcessInfo.processName;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ym
    public void a(String str) {
        if (this.h == null) {
            this.h = Boolean.valueOf(TextUtils.equals(this.a.getPackageName(), b()));
        }
        if (!this.h.booleanValue()) {
            im.a().c(i, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        c();
        im.a().a(i, String.format("Cancelling work ID %s", str), new Throwable[0]);
        fn fnVar = this.e;
        if (fnVar != null) {
            fnVar.a(str);
        }
        this.b.c(str);
    }

    @DexIgnore
    @Override // com.fossil.rn
    public void a(List<String> list) {
        for (String str : list) {
            im.a().a(i, String.format("Constraints not met: Cancelling work ID %s", str), new Throwable[0]);
            this.b.c(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.vm
    public void a(String str, boolean z) {
        b(str);
    }
}
