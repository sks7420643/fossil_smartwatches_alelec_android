package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.j62;
import com.fossil.v02;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m52 implements b42 {
    @DexIgnore
    public /* final */ Map<v02.c<?>, k52<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<v02.c<?>, k52<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<v02<?>, Boolean> c;
    @DexIgnore
    public /* final */ u12 d;
    @DexIgnore
    public /* final */ c32 e;
    @DexIgnore
    public /* final */ Lock f;
    @DexIgnore
    public /* final */ Looper g;
    @DexIgnore
    public /* final */ m02 h;
    @DexIgnore
    public /* final */ Condition i;
    @DexIgnore
    public /* final */ j62 j;
    @DexIgnore
    public /* final */ boolean p;
    @DexIgnore
    public /* final */ boolean q;
    @DexIgnore
    public /* final */ Queue<r12<?, ?>> r; // = new LinkedList();
    @DexIgnore
    public boolean s;
    @DexIgnore
    public Map<p12<?>, i02> t;
    @DexIgnore
    public Map<p12<?>, i02> u;
    @DexIgnore
    public n52 v;
    @DexIgnore
    public i02 w;

    @DexIgnore
    public m52(Context context, Lock lock, Looper looper, m02 m02, Map<v02.c<?>, v02.f> map, j62 j62, Map<v02<?>, Boolean> map2, v02.a<? extends xn3, fn3> aVar, ArrayList<f52> arrayList, c32 c32, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.f = lock;
        this.g = looper;
        this.i = lock.newCondition();
        this.h = m02;
        this.e = c32;
        this.c = map2;
        this.j = j62;
        this.p = z;
        HashMap hashMap = new HashMap();
        for (v02<?> v02 : map2.keySet()) {
            hashMap.put(v02.a(), v02);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            f52 f52 = arrayList.get(i2);
            i2++;
            f52 f522 = f52;
            hashMap2.put(f522.a, f522);
        }
        boolean z5 = false;
        boolean z6 = true;
        boolean z7 = false;
        for (Map.Entry<v02.c<?>, v02.f> entry : map.entrySet()) {
            v02 v022 = (v02) hashMap.get(entry.getKey());
            v02.f value = entry.getValue();
            if (value.j()) {
                z3 = z6;
                if (!this.c.get(v022).booleanValue()) {
                    z4 = true;
                    z2 = true;
                } else {
                    z2 = z7;
                    z4 = true;
                }
            } else {
                z4 = z5;
                z2 = z7;
                z3 = false;
            }
            k52<?> k52 = new k52<>(context, v022, looper, value, (f52) hashMap2.get(v022), j62, aVar);
            this.a.put(entry.getKey(), k52);
            if (value.n()) {
                this.b.put(entry.getKey(), k52);
            }
            z5 = z4;
            z6 = z3;
            z7 = z2;
        }
        this.q = z5 && !z6 && !z7;
        this.d = u12.e();
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(T t2) {
        v02.c<A> i2 = t2.i();
        if (this.p && c(t2)) {
            return t2;
        }
        this.e.y.a(t2);
        this.a.get(i2).b(t2);
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t2) {
        if (this.p && c(t2)) {
            return t2;
        }
        if (!c()) {
            this.r.add(t2);
            return t2;
        }
        this.e.y.a(t2);
        this.a.get(t2.i()).a((r12) t2);
        return t2;
    }

    @DexIgnore
    public final <T extends r12<? extends i12, ? extends v02.b>> boolean c(T t2) {
        v02.c<?> i2 = t2.i();
        i02 a2 = a(i2);
        if (a2 == null || a2.e() != 4) {
            return false;
        }
        t2.c(new Status(4, null, this.d.a(this.a.get(i2).a(), System.identityHashCode(this.e))));
        return true;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void d() {
        this.f.lock();
        try {
            this.d.a();
            if (this.v != null) {
                this.v.a();
                this.v = null;
            }
            if (this.u == null) {
                this.u = new n4(this.b.size());
            }
            i02 i02 = new i02(4);
            for (k52<?> k52 : this.b.values()) {
                this.u.put(k52.a(), i02);
            }
            if (this.t != null) {
                this.t.putAll(this.u);
            }
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void e() {
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final i02 f() {
        b();
        while (g()) {
            try {
                this.i.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new i02(15, null);
            }
        }
        if (c()) {
            return i02.e;
        }
        i02 i02 = this.w;
        if (i02 != null) {
            return i02;
        }
        return new i02(13, null);
    }

    @DexIgnore
    public final boolean g() {
        this.f.lock();
        try {
            return this.t == null && this.s;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f A[Catch:{ all -> 0x0044 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean h() {
        /*
            r3 = this;
            java.util.concurrent.locks.Lock r0 = r3.f
            r0.lock()
            boolean r0 = r3.s     // Catch:{ all -> 0x0044 }
            r1 = 0
            if (r0 == 0) goto L_0x003e
            boolean r0 = r3.p     // Catch:{ all -> 0x0044 }
            if (r0 != 0) goto L_0x000f
            goto L_0x003e
        L_0x000f:
            java.util.Map<com.fossil.v02$c<?>, com.fossil.k52<?>> r0 = r3.b     // Catch:{ all -> 0x0044 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x0044 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0044 }
        L_0x0019:
            boolean r2 = r0.hasNext()     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x0037
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x0044 }
            com.fossil.v02$c r2 = (com.fossil.v02.c) r2     // Catch:{ all -> 0x0044 }
            com.fossil.i02 r2 = r3.a(r2)     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x0031
            boolean r2 = r2.x()     // Catch:{ all -> 0x0044 }
            if (r2 != 0) goto L_0x0019
        L_0x0031:
            java.util.concurrent.locks.Lock r0 = r3.f
            r0.unlock()
            return r1
        L_0x0037:
            java.util.concurrent.locks.Lock r0 = r3.f
            r0.unlock()
            r0 = 1
            return r0
        L_0x003e:
            java.util.concurrent.locks.Lock r0 = r3.f
            r0.unlock()
            return r1
        L_0x0044:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r3.f
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.m52.h():boolean");
    }

    @DexIgnore
    public final void i() {
        if (this.j == null) {
            this.e.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(this.j.i());
        Map<v02<?>, j62.b> f2 = this.j.f();
        for (v02<?> v02 : f2.keySet()) {
            i02 a2 = a(v02);
            if (a2 != null && a2.x()) {
                hashSet.addAll(f2.get(v02).a);
            }
        }
        this.e.q = hashSet;
    }

    @DexIgnore
    public final void j() {
        while (!this.r.isEmpty()) {
            a(this.r.remove());
        }
        this.e.a((Bundle) null);
    }

    @DexIgnore
    public final i02 k() {
        int i2 = 0;
        i02 i02 = null;
        i02 i022 = null;
        int i3 = 0;
        for (k52<?> k52 : this.a.values()) {
            v02<?> d2 = k52.d();
            i02 i023 = this.t.get(k52.a());
            if (!i023.x() && (!this.c.get(d2).booleanValue() || i023.w() || this.h.c(i023.e()))) {
                if (i023.e() != 4 || !this.p) {
                    int a2 = d2.c().a();
                    if (i02 == null || i2 > a2) {
                        i02 = i023;
                        i2 = a2;
                    }
                } else {
                    int a3 = d2.c().a();
                    if (i022 == null || i3 > a3) {
                        i022 = i023;
                        i3 = a3;
                    }
                }
            }
        }
        return (i02 == null || i022 == null || i2 <= i3) ? i02 : i022;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void a() {
        this.f.lock();
        try {
            this.s = false;
            this.t = null;
            this.u = null;
            if (this.v != null) {
                this.v.a();
                this.v = null;
            }
            this.w = null;
            while (!this.r.isEmpty()) {
                r12<?, ?> remove = this.r.remove();
                remove.a((r42) null);
                remove.a();
            }
            this.i.signalAll();
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void b() {
        this.f.lock();
        try {
            if (!this.s) {
                this.s = true;
                this.t = null;
                this.u = null;
                this.v = null;
                this.w = null;
                this.d.c();
                this.d.a(this.a.values()).a(new aa2(this.g), new o52(this));
                this.f.unlock();
            }
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final boolean c() {
        this.f.lock();
        try {
            return this.t != null && this.w == null;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final i02 a(v02<?> v02) {
        return a(v02.a());
    }

    @DexIgnore
    public final i02 a(v02.c<?> cVar) {
        this.f.lock();
        try {
            k52<?> k52 = this.a.get(cVar);
            if (this.t != null && k52 != null) {
                return this.t.get(k52.a());
            }
            this.f.unlock();
            return null;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.b42
    public final boolean a(c22 c22) {
        this.f.lock();
        try {
            if (!this.s || h()) {
                this.f.unlock();
                return false;
            }
            this.d.c();
            this.v = new n52(this, c22);
            this.d.a(this.b.values()).a(new aa2(this.g), this.v);
            this.f.unlock();
            return true;
        } catch (Throwable th) {
            this.f.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(k52<?> k52, i02 i02) {
        return !i02.x() && !i02.w() && this.c.get(k52.d()).booleanValue() && k52.i().j() && this.h.c(i02.e());
    }
}
