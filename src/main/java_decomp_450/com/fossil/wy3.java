package com.fossil;

import com.fossil.wy3.i;
import com.fossil.wy3.n;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wy3<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    @DexIgnore
    public static /* final */ long CLEANUP_EXECUTOR_DELAY_SECS; // = 60;
    @DexIgnore
    public static /* final */ int CONTAINS_VALUE_RETRIES; // = 3;
    @DexIgnore
    public static /* final */ int DRAIN_MAX; // = 16;
    @DexIgnore
    public static /* final */ int DRAIN_THRESHOLD; // = 63;
    @DexIgnore
    public static /* final */ int MAXIMUM_CAPACITY; // = 1073741824;
    @DexIgnore
    public static /* final */ int MAX_SEGMENTS; // = 65536;
    @DexIgnore
    public static /* final */ c0<Object, Object, e> UNSET_WEAK_VALUE_REFERENCE; // = new a();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 5;
    @DexIgnore
    public /* final */ int concurrencyLevel;
    @DexIgnore
    public /* final */ transient j<K, V, E, S> entryHelper;
    @DexIgnore
    public transient Set<Map.Entry<K, V>> entrySet;
    @DexIgnore
    public /* final */ aw3<Object> keyEquivalence;
    @DexIgnore
    public transient Set<K> keySet;
    @DexIgnore
    public /* final */ transient int segmentMask;
    @DexIgnore
    public /* final */ transient int segmentShift;
    @DexIgnore
    public /* final */ transient n<K, V, E, S>[] segments;
    @DexIgnore
    public transient Collection<V> values;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c0<Object, Object, e> {
        @DexIgnore
        public c0<Object, Object, e> a(ReferenceQueue<Object> referenceQueue, e eVar) {
            return this;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.wy3$c0' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.ref.ReferenceQueue, com.fossil.wy3$i] */
        @Override // com.fossil.wy3.c0
        public /* bridge */ /* synthetic */ c0<Object, Object, e> a(ReferenceQueue<Object> referenceQueue, e eVar) {
            a(referenceQueue, eVar);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.wy3.c0
        public e a() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.wy3.c0
        public void clear() {
        }

        @DexIgnore
        @Override // com.fossil.wy3.c0
        public Object get() {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0<K, V> extends n<K, V, z<K, V>, a0<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public a0(wy3<K, V, z<K, V>, a0<K, V>> wy3, int i, int i2) {
            super(wy3, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public c0<K, V, z<K, V>> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            return castForTesting((i) iVar).b();
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<K>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.wy3.n
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<K>) this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public c0<K, V, z<K, V>> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            return new d0(this.queueForValues, v, castForTesting((i) iVar));
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public a0<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            z<K, V> castForTesting = castForTesting((i) iVar);
            c0 a = castForTesting.c;
            c0 unused = castForTesting.c = c0Var;
            a.clear();
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public z<K, V> castForTesting(i<K, V, ?> iVar) {
            return (z) iVar;
        }
    }

    @DexIgnore
    public interface b0<K, V, E extends i<K, V, E>> extends i<K, V, E> {
        @DexIgnore
        c0<K, V, E> b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V, E extends i<K, V, E>> implements i<K, V, E> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E c;

        @DexIgnore
        public c(K k, int i, E e) {
            this.a = k;
            this.b = i;
            this.c = e;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public E a() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public int c() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public K getKey() {
            return this.a;
        }
    }

    @DexIgnore
    public interface c0<K, V, E extends i<K, V, E>> {
        @DexIgnore
        c0<K, V, E> a(ReferenceQueue<V> referenceQueue, E e);

        @DexIgnore
        E a();

        @DexIgnore
        void clear();

        @DexIgnore
        V get();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K, V, E extends i<K, V, E>> extends WeakReference<K> implements i<K, V, E> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ E b;

        @DexIgnore
        public d(ReferenceQueue<K> referenceQueue, K k, int i, E e) {
            super(k, referenceQueue);
            this.a = i;
            this.b = e;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public E a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public int c() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public K getKey() {
            return (K) get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d0<K, V, E extends i<K, V, E>> extends WeakReference<V> implements c0<K, V, E> {
        @DexIgnore
        public /* final */ E a;

        @DexIgnore
        public d0(ReferenceQueue<V> referenceQueue, V v, E e) {
            super(v, referenceQueue);
            this.a = e;
        }

        @DexIgnore
        @Override // com.fossil.wy3.c0
        public E a() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.wy3.c0
        public c0<K, V, E> a(ReferenceQueue<V> referenceQueue, E e) {
            return new d0(referenceQueue, get(), e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements i<Object, Object, e> {
        @DexIgnore
        public e() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public int c() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public Object getKey() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public Object getValue() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public e a() {
            throw new AssertionError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 extends uw3<K, V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public V b;

        @DexIgnore
        public e0(K k, V v) {
            this.a = k;
            this.b = v;
        }

        @DexIgnore
        @Override // com.fossil.uw3
        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (!this.a.equals(entry.getKey()) || !this.b.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.uw3
        public K getKey() {
            return this.a;
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.uw3
        public V getValue() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.uw3
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.uw3
        public V setValue(V v) {
            V v2 = (V) wy3.this.put(this.a, v);
            this.b = v;
            return v2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends wy3<K, V, E, S>.h {
        @DexIgnore
        public f(wy3 wy3) {
            super();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Map.Entry<K, V> next() {
            return b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends m<Map.Entry<K, V>> {
        @DexIgnore
        public g() {
            super(null);
        }

        @DexIgnore
        public void clear() {
            wy3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            Map.Entry entry;
            Object key;
            Object obj2;
            if ((obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && (obj2 = wy3.this.get(key)) != null && wy3.this.valueEquivalence().equivalent(entry.getValue(), obj2)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public boolean isEmpty() {
            return wy3.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new f(wy3.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            Map.Entry entry;
            Object key;
            if ((obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && wy3.this.remove(key, entry.getValue())) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int size() {
            return wy3.this.size();
        }
    }

    @DexIgnore
    public interface i<K, V, E extends i<K, V, E>> {
        @DexIgnore
        E a();

        @DexIgnore
        int c();

        @DexIgnore
        K getKey();

        @DexIgnore
        V getValue();
    }

    @DexIgnore
    public interface j<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> {
        @DexIgnore
        E a(S s, E e, E e2);

        @DexIgnore
        E a(S s, K k, int i, E e);

        @DexIgnore
        S a(wy3<K, V, E, S> wy3, int i, int i2);

        @DexIgnore
        p a();

        @DexIgnore
        void a(S s, E e, V v);

        @DexIgnore
        p b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends wy3<K, V, E, S>.h {
        @DexIgnore
        public k(wy3 wy3) {
            super();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public K next() {
            return (K) b().getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends m<K> {
        @DexIgnore
        public l() {
            super(null);
        }

        @DexIgnore
        public void clear() {
            wy3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return wy3.this.containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return wy3.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new k(wy3.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return wy3.this.remove(obj) != null;
        }

        @DexIgnore
        public int size() {
            return wy3.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class m<E> extends AbstractSet<E> {
        @DexIgnore
        public m() {
        }

        @DexIgnore
        public Object[] toArray() {
            return wy3.a(this).toArray();
        }

        @DexIgnore
        public /* synthetic */ m(a aVar) {
            this();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public <E> E[] toArray(E[] eArr) {
            return (E[]) wy3.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<K, V> extends b<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;

        @DexIgnore
        public o(p pVar, p pVar2, aw3<Object> aw3, aw3<Object> aw32, int i, ConcurrentMap<K, V> concurrentMap) {
            super(pVar, pVar2, aw3, aw32, i, concurrentMap);
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            ((b) this).delegate = readMapMaker(objectInputStream).f();
            readEntries(objectInputStream);
        }

        @DexIgnore
        private Object readResolve() {
            return ((b) this).delegate;
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            writeMapTo(objectOutputStream);
        }
    }

    @DexIgnore
    public enum p {
        STRONG {
            @DexIgnore
            @Override // com.fossil.wy3.p
            public aw3<Object> defaultEquivalence() {
                return aw3.equals();
            }
        },
        WEAK {
            @DexIgnore
            @Override // com.fossil.wy3.p
            public aw3<Object> defaultEquivalence() {
                return aw3.identity();
            }
        };

        @DexIgnore
        public abstract aw3<Object> defaultEquivalence();

        @DexIgnore
        public /* synthetic */ p(a aVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<K, V> extends c<K, V, q<K, V>> implements u<K, V, q<K, V>> {
        @DexIgnore
        public volatile V d; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, q<K, V>, r<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return (a<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, i iVar, i iVar2) {
                return a((r) ((r) nVar), (q) ((q) iVar), (q) ((q) iVar2));
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p b() {
                return p.STRONG;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, Object obj, int i, i iVar) {
                return a((r) ((r) nVar), obj, i, (q) ((q) iVar));
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ void a(n nVar, i iVar, Object obj) {
                a((r) ((r) nVar), (q) ((q) iVar), obj);
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p a() {
                return p.STRONG;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public r<K, V> a(wy3<K, V, q<K, V>, r<K, V>> wy3, int i, int i2) {
                return new r<>(wy3, i, i2);
            }

            @DexIgnore
            public q<K, V> a(r<K, V> rVar, q<K, V> qVar, q<K, V> qVar2) {
                return qVar.a((q) qVar2);
            }

            @DexIgnore
            public void a(r<K, V> rVar, q<K, V> qVar, V v) {
                qVar.a((Object) v);
            }

            @DexIgnore
            public q<K, V> a(r<K, V> rVar, K k, int i, q<K, V> qVar) {
                return new q<>(k, i, qVar);
            }
        }

        @DexIgnore
        public q(K k, int i, q<K, V> qVar) {
            super(k, i, qVar);
        }

        @DexIgnore
        public void a(V v) {
            this.d = v;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public V getValue() {
            return this.d;
        }

        @DexIgnore
        public q<K, V> a(q<K, V> qVar) {
            q<K, V> qVar2 = new q<>(((c) this).a, ((c) this).b, qVar);
            qVar2.d = this.d;
            return qVar2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<K, V> extends n<K, V, q<K, V>, r<K, V>> {
        @DexIgnore
        public r(wy3<K, V, q<K, V>, r<K, V>> wy3, int i, int i2) {
            super(wy3, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public r<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public q<K, V> castForTesting(i<K, V, ?> iVar) {
            return (q) iVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t<K, V> extends n<K, V, s<K, V>, t<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public t(wy3<K, V, s<K, V>, t<K, V>> wy3, int i, int i2) {
            super(wy3, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public c0<K, V, s<K, V>> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            return castForTesting((i) iVar).b();
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<V>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.wy3.n
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<V>) this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public void maybeDrainReferenceQueues() {
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public c0<K, V, s<K, V>> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            return new d0(this.queueForValues, v, castForTesting((i) iVar));
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public t<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            s<K, V> castForTesting = castForTesting((i) iVar);
            c0 a = castForTesting.d;
            c0 unused = castForTesting.d = c0Var;
            a.clear();
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public s<K, V> castForTesting(i<K, V, ?> iVar) {
            return (s) iVar;
        }
    }

    @DexIgnore
    public interface u<K, V, E extends i<K, V, E>> extends i<K, V, E> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v extends wy3<K, V, E, S>.h {
        @DexIgnore
        public v(wy3 wy3) {
            super();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return (V) b().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w extends AbstractCollection<V> {
        @DexIgnore
        public w() {
        }

        @DexIgnore
        public void clear() {
            wy3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return wy3.this.containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return wy3.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return new v(wy3.this);
        }

        @DexIgnore
        public int size() {
            return wy3.this.size();
        }

        @DexIgnore
        public Object[] toArray() {
            return wy3.a(this).toArray();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public <E> E[] toArray(E[] eArr) {
            return (E[]) wy3.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x<K, V> extends d<K, V, x<K, V>> implements u<K, V, x<K, V>> {
        @DexIgnore
        public volatile V c; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, x<K, V>, y<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return (a<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, i iVar, i iVar2) {
                return a((y) ((y) nVar), (x) ((x) iVar), (x) ((x) iVar2));
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p b() {
                return p.STRONG;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, Object obj, int i, i iVar) {
                return a((y) ((y) nVar), obj, i, (x) ((x) iVar));
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ void a(n nVar, i iVar, Object obj) {
                a((y) ((y) nVar), (x) ((x) iVar), obj);
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p a() {
                return p.WEAK;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public y<K, V> a(wy3<K, V, x<K, V>, y<K, V>> wy3, int i, int i2) {
                return new y<>(wy3, i, i2);
            }

            @DexIgnore
            public x<K, V> a(y<K, V> yVar, x<K, V> xVar, x<K, V> xVar2) {
                if (xVar.getKey() == null) {
                    return null;
                }
                return xVar.a(yVar.queueForKeys, xVar2);
            }

            @DexIgnore
            public void a(y<K, V> yVar, x<K, V> xVar, V v) {
                xVar.a(v);
            }

            @DexIgnore
            public x<K, V> a(y<K, V> yVar, K k, int i, x<K, V> xVar) {
                return new x<>(yVar.queueForKeys, k, i, xVar);
            }
        }

        @DexIgnore
        public x(ReferenceQueue<K> referenceQueue, K k, int i, x<K, V> xVar) {
            super(referenceQueue, k, i, xVar);
        }

        @DexIgnore
        public void a(V v) {
            this.c = v;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public V getValue() {
            return this.c;
        }

        @DexIgnore
        public x<K, V> a(ReferenceQueue<K> referenceQueue, x<K, V> xVar) {
            x<K, V> xVar2 = new x<>(referenceQueue, getKey(), ((d) this).a, xVar);
            xVar2.a(this.c);
            return xVar2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class y<K, V> extends n<K, V, x<K, V>, y<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();

        @DexIgnore
        public y(wy3<K, V, x<K, V>, y<K, V>> wy3, int i, int i2) {
            super(wy3, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<K>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.wy3.n
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<K>) this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public y<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.wy3.n
        public x<K, V> castForTesting(i<K, V, ?> iVar) {
            return (x) iVar;
        }
    }

    @DexIgnore
    public wy3(vy3 vy3, j<K, V, E, S> jVar) {
        this.concurrencyLevel = Math.min(vy3.a(), 65536);
        this.keyEquivalence = vy3.c();
        this.entryHelper = jVar;
        int min = Math.min(vy3.b(), 1073741824);
        int i2 = 0;
        int i3 = 1;
        int i4 = 1;
        int i5 = 0;
        while (i4 < this.concurrencyLevel) {
            i5++;
            i4 <<= 1;
        }
        this.segmentShift = 32 - i5;
        this.segmentMask = i4 - 1;
        this.segments = newSegmentArray(i4);
        int i6 = min / i4;
        while (i3 < (i4 * i6 < min ? i6 + 1 : i6)) {
            i3 <<= 1;
        }
        while (true) {
            n<K, V, E, S>[] nVarArr = this.segments;
            if (i2 < nVarArr.length) {
                nVarArr[i2] = createSegment(i3, -1);
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public static <E> ArrayList<E> a(Collection<E> collection) {
        ArrayList<E> arrayList = new ArrayList<>(collection.size());
        qy3.a(arrayList, collection.iterator());
        return arrayList;
    }

    @DexIgnore
    public static <K, V> wy3<K, V, ? extends i<K, V, ?>, ?> create(vy3 vy3) {
        if (vy3.d() == p.STRONG && vy3.e() == p.STRONG) {
            return new wy3<>(vy3, q.a.c());
        }
        if (vy3.d() == p.STRONG && vy3.e() == p.WEAK) {
            return new wy3<>(vy3, s.a.c());
        }
        if (vy3.d() == p.WEAK && vy3.e() == p.STRONG) {
            return new wy3<>(vy3, x.a.c());
        }
        if (vy3.d() == p.WEAK && vy3.e() == p.WEAK) {
            return new wy3<>(vy3, z.a.c());
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static int rehash(int i2) {
        int i3 = i2 + ((i2 << 15) ^ -12931);
        int i4 = i3 ^ (i3 >>> 10);
        int i5 = i4 + (i4 << 3);
        int i6 = i5 ^ (i5 >>> 6);
        int i7 = i6 + (i6 << 2) + (i6 << 14);
        return i7 ^ (i7 >>> 16);
    }

    @DexIgnore
    public static <K, V, E extends i<K, V, E>> c0<K, V, E> unsetWeakValueReference() {
        return (c0<K, V, E>) UNSET_WEAK_VALUE_REFERENCE;
    }

    @DexIgnore
    public void clear() {
        for (n<K, V, E, S> nVar : this.segments) {
            nVar.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        if (obj == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).containsKey(obj, hash);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>>[] */
    /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: com.fossil.wy3$t */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean containsValue(Object obj) {
        if (obj == null) {
            return false;
        }
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = -1;
        int i2 = 0;
        while (i2 < 3) {
            long j3 = 0;
            for (t tVar : nVarArr) {
                int i3 = ((n) tVar).count;
                AtomicReferenceArray<E> atomicReferenceArray = ((n) tVar).table;
                for (int i4 = 0; i4 < atomicReferenceArray.length(); i4++) {
                    for (i iVar = atomicReferenceArray.get(i4); iVar != null; iVar = iVar.a()) {
                        Object liveValue = tVar.getLiveValue(iVar);
                        if (liveValue != null && valueEquivalence().equivalent(obj, liveValue)) {
                            return true;
                        }
                    }
                }
                j3 += (long) ((n) tVar).modCount;
            }
            if (j3 == j2) {
                return false;
            }
            i2++;
            j2 = j3;
        }
        return false;
    }

    @DexIgnore
    public E copyEntry(E e2, E e3) {
        return segmentFor(e2.c()).copyEntry(e2, e3);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r2v1. Raw type applied. Possible types: S extends com.fossil.wy3$n<K, V, E, S>, com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
    public n<K, V, E, S> createSegment(int i2, int i3) {
        return (S) this.entryHelper.a(this, i2, i3);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.entrySet;
        if (set != null) {
            return set;
        }
        g gVar = new g();
        this.entrySet = gVar;
        return gVar;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).get(obj, hash);
    }

    @DexIgnore
    public E getEntry(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).getEntry(obj, hash);
    }

    @DexIgnore
    public V getLiveValue(E e2) {
        V v2;
        if (e2.getKey() == null || (v2 = (V) e2.getValue()) == null) {
            return null;
        }
        return v2;
    }

    @DexIgnore
    public int hash(Object obj) {
        return rehash(this.keyEquivalence.hash(obj));
    }

    @DexIgnore
    public boolean isEmpty() {
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = 0;
        for (int i2 = 0; i2 < nVarArr.length; i2++) {
            if (nVarArr[i2].count != 0) {
                return false;
            }
            j2 += (long) nVarArr[i2].modCount;
        }
        if (j2 == 0) {
            return true;
        }
        for (int i3 = 0; i3 < nVarArr.length; i3++) {
            if (nVarArr[i3].count != 0) {
                return false;
            }
            j2 -= (long) nVarArr[i3].modCount;
        }
        if (j2 != 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean isLiveForTesting(i<K, V, ?> iVar) {
        return segmentFor(iVar.c()).getLiveValueForTesting(iVar) != null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        Set<K> set = this.keySet;
        if (set != null) {
            return set;
        }
        l lVar = new l();
        this.keySet = lVar;
        return lVar;
    }

    @DexIgnore
    public p keyStrength() {
        return this.entryHelper.a();
    }

    @DexIgnore
    public final n<K, V, E, S>[] newSegmentArray(int i2) {
        return new n[i2];
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V put(K k2, V v2) {
        jw3.a(k2);
        jw3.a(v2);
        int hash = hash(k2);
        return segmentFor(hash).put(k2, hash, v2, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.wy3<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V putIfAbsent(K k2, V v2) {
        jw3.a(k2);
        jw3.a(v2);
        int hash = hash(k2);
        return segmentFor(hash).put(k2, hash, v2, true);
    }

    @DexIgnore
    public void reclaimKey(E e2) {
        int c2 = e2.c();
        segmentFor(c2).reclaimKey(e2, c2);
    }

    @DexIgnore
    public void reclaimValue(c0<K, V, E> c0Var) {
        E a2 = c0Var.a();
        int c2 = a2.c();
        segmentFor(c2).reclaimValue((K) a2.getKey(), c2, c0Var);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V remove(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public boolean replace(K k2, V v2, V v3) {
        jw3.a(k2);
        jw3.a(v3);
        if (v2 == null) {
            return false;
        }
        int hash = hash(k2);
        return segmentFor(hash).replace(k2, hash, v2, v3);
    }

    @DexIgnore
    public n<K, V, E, S> segmentFor(int i2) {
        return this.segments[(i2 >>> this.segmentShift) & this.segmentMask];
    }

    @DexIgnore
    public int size() {
        n<K, V, E, S>[] nVarArr;
        long j2 = 0;
        for (n<K, V, E, S> nVar : this.segments) {
            j2 += (long) nVar.count;
        }
        return x04.a(j2);
    }

    @DexIgnore
    public aw3<Object> valueEquivalence() {
        return this.entryHelper.b().defaultEquivalence();
    }

    @DexIgnore
    public p valueStrength() {
        return this.entryHelper.b();
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Collection<V> values() {
        Collection<V> collection = this.values;
        if (collection != null) {
            return collection;
        }
        w wVar = new w();
        this.values = wVar;
        return wVar;
    }

    @DexIgnore
    public Object writeReplace() {
        return new o(this.entryHelper.a(), this.entryHelper.b(), this.keyEquivalence, this.entryHelper.b().defaultEquivalence(), this.concurrencyLevel, this);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<K, V> extends nx3<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;
        @DexIgnore
        public /* final */ int concurrencyLevel;
        @DexIgnore
        public transient ConcurrentMap<K, V> delegate;
        @DexIgnore
        public /* final */ aw3<Object> keyEquivalence;
        @DexIgnore
        public /* final */ p keyStrength;
        @DexIgnore
        public /* final */ aw3<Object> valueEquivalence;
        @DexIgnore
        public /* final */ p valueStrength;

        @DexIgnore
        public b(p pVar, p pVar2, aw3<Object> aw3, aw3<Object> aw32, int i, ConcurrentMap<K, V> concurrentMap) {
            this.keyStrength = pVar;
            this.valueStrength = pVar2;
            this.keyEquivalence = aw3;
            this.valueEquivalence = aw32;
            this.concurrencyLevel = i;
            this.delegate = concurrentMap;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.concurrent.ConcurrentMap<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        public void readEntries(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.delegate.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public vy3 readMapMaker(ObjectInputStream objectInputStream) throws IOException {
            int readInt = objectInputStream.readInt();
            vy3 vy3 = new vy3();
            vy3.b(readInt);
            vy3.a(this.keyStrength);
            vy3.b(this.valueStrength);
            vy3.a(this.keyEquivalence);
            vy3.a(this.concurrencyLevel);
            return vy3;
        }

        @DexIgnore
        public void writeMapTo(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeInt(this.delegate.size());
            for (Map.Entry<K, V> entry : this.delegate.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }

        @DexIgnore
        @Override // com.fossil.nx3, com.fossil.nx3, com.fossil.nx3, com.fossil.ox3, com.fossil.ox3, com.fossil.px3
        public ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s<K, V> extends c<K, V, s<K, V>> implements b0<K, V, s<K, V>> {
        @DexIgnore
        public volatile c0<K, V, s<K, V>> d; // = wy3.unsetWeakValueReference();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, s<K, V>, t<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return (a<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, i iVar, i iVar2) {
                return a((t) ((t) nVar), (s) ((s) iVar), (s) ((s) iVar2));
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p b() {
                return p.WEAK;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, Object obj, int i, i iVar) {
                return a((t) ((t) nVar), obj, i, (s) ((s) iVar));
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ void a(n nVar, i iVar, Object obj) {
                a((t) ((t) nVar), (s) ((s) iVar), obj);
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p a() {
                return p.STRONG;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public t<K, V> a(wy3<K, V, s<K, V>, t<K, V>> wy3, int i, int i2) {
                return new t<>(wy3, i, i2);
            }

            @DexIgnore
            public s<K, V> a(t<K, V> tVar, s<K, V> sVar, s<K, V> sVar2) {
                if (n.isCollected(sVar)) {
                    return null;
                }
                return sVar.a(tVar.queueForValues, sVar2);
            }

            @DexIgnore
            public void a(t<K, V> tVar, s<K, V> sVar, V v) {
                sVar.a(v, tVar.queueForValues);
            }

            @DexIgnore
            public s<K, V> a(t<K, V> tVar, K k, int i, s<K, V> sVar) {
                return new s<>(k, i, sVar);
            }
        }

        @DexIgnore
        public s(K k, int i, s<K, V> sVar) {
            super(k, i, sVar);
        }

        @DexIgnore
        @Override // com.fossil.wy3.b0
        public c0<K, V, s<K, V>> b() {
            return this.d;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public V getValue() {
            return this.d.get();
        }

        @DexIgnore
        public void a(V v, ReferenceQueue<V> referenceQueue) {
            c0<K, V, s<K, V>> c0Var = this.d;
            this.d = new d0(referenceQueue, v, this);
            c0Var.clear();
        }

        @DexIgnore
        public s<K, V> a(ReferenceQueue<V> referenceQueue, s<K, V> sVar) {
            s<K, V> sVar2 = new s<>(((c) this).a, ((c) this).b, sVar);
            sVar2.d = this.d.a(referenceQueue, sVar2);
            return sVar2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z<K, V> extends d<K, V, z<K, V>> implements b0<K, V, z<K, V>> {
        @DexIgnore
        public volatile c0<K, V, z<K, V>> c; // = wy3.unsetWeakValueReference();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, z<K, V>, a0<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return (a<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, i iVar, i iVar2) {
                return a((a0) ((a0) nVar), (z) ((z) iVar), (z) ((z) iVar2));
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p b() {
                return p.WEAK;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ i a(n nVar, Object obj, int i, i iVar) {
                return a((a0) ((a0) nVar), obj, i, (z) ((z) iVar));
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.wy3.j
            public /* bridge */ /* synthetic */ void a(n nVar, i iVar, Object obj) {
                a((a0) ((a0) nVar), (z) ((z) iVar), obj);
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public p a() {
                return p.WEAK;
            }

            @DexIgnore
            @Override // com.fossil.wy3.j
            public a0<K, V> a(wy3<K, V, z<K, V>, a0<K, V>> wy3, int i, int i2) {
                return new a0<>(wy3, i, i2);
            }

            @DexIgnore
            public z<K, V> a(a0<K, V> a0Var, z<K, V> zVar, z<K, V> zVar2) {
                if (zVar.getKey() != null && !n.isCollected(zVar)) {
                    return zVar.a(a0Var.queueForKeys, a0Var.queueForValues, zVar2);
                }
                return null;
            }

            @DexIgnore
            public void a(a0<K, V> a0Var, z<K, V> zVar, V v) {
                zVar.a(v, a0Var.queueForValues);
            }

            @DexIgnore
            public z<K, V> a(a0<K, V> a0Var, K k, int i, z<K, V> zVar) {
                return new z<>(a0Var.queueForKeys, k, i, zVar);
            }
        }

        @DexIgnore
        public z(ReferenceQueue<K> referenceQueue, K k, int i, z<K, V> zVar) {
            super(referenceQueue, k, i, zVar);
        }

        @DexIgnore
        @Override // com.fossil.wy3.b0
        public c0<K, V, z<K, V>> b() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.wy3.i
        public V getValue() {
            return this.c.get();
        }

        @DexIgnore
        public z<K, V> a(ReferenceQueue<K> referenceQueue, ReferenceQueue<V> referenceQueue2, z<K, V> zVar) {
            z<K, V> zVar2 = new z<>(referenceQueue, getKey(), ((d) this).a, zVar);
            zVar2.c = this.c.a(referenceQueue2, zVar2);
            return zVar2;
        }

        @DexIgnore
        public void a(V v, ReferenceQueue<V> referenceQueue) {
            c0<K, V, z<K, V>> c0Var = this.c;
            this.c = new d0(referenceQueue, v, this);
            c0Var.clear();
        }
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash, obj2);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V replace(K k2, V v2) {
        jw3.a(k2);
        jw3.a(v2);
        int hash = hash(k2);
        return segmentFor(hash).replace(k2, hash, v2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<T> implements Iterator<T> {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public n<K, V, E, S> c;
        @DexIgnore
        public AtomicReferenceArray<E> d;
        @DexIgnore
        public E e;
        @DexIgnore
        public wy3<K, V, E, S>.e0 f;
        @DexIgnore
        public wy3<K, V, E, S>.e0 g;

        @DexIgnore
        public h() {
            this.a = wy3.this.segments.length - 1;
            a();
        }

        @DexIgnore
        public final void a() {
            this.f = null;
            if (!c() && !d()) {
                while (true) {
                    int i = this.a;
                    if (i >= 0) {
                        n<K, V, E, S>[] nVarArr = wy3.this.segments;
                        this.a = i - 1;
                        n<K, V, E, S> nVar = nVarArr[i];
                        this.c = nVar;
                        if (nVar.count != 0) {
                            AtomicReferenceArray<E> atomicReferenceArray = this.c.table;
                            this.d = atomicReferenceArray;
                            this.b = atomicReferenceArray.length() - 1;
                            if (d()) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public wy3<K, V, E, S>.e0 b() {
            wy3<K, V, E, S>.e0 e0Var = this.f;
            if (e0Var != null) {
                this.g = e0Var;
                a();
                return this.g;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean c() {
            E e2 = this.e;
            if (e2 == null) {
                return false;
            }
            while (true) {
                this.e = (E) e2.a();
                E e3 = this.e;
                if (e3 == null) {
                    return false;
                }
                if (a(e3)) {
                    return true;
                }
                e2 = this.e;
            }
        }

        @DexIgnore
        public boolean d() {
            while (true) {
                int i = this.b;
                if (i < 0) {
                    return false;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.d;
                this.b = i - 1;
                E e2 = atomicReferenceArray.get(i);
                this.e = e2;
                if (e2 != null && (a(e2) || c())) {
                    return true;
                }
            }
        }

        @DexIgnore
        public boolean hasNext() {
            return this.f != null;
        }

        @DexIgnore
        public void remove() {
            bx3.a(this.g != null);
            wy3.this.remove(this.g.getKey());
            this.g = null;
        }

        @DexIgnore
        public boolean a(E e2) {
            boolean z;
            try {
                Object key = e2.getKey();
                Object liveValue = wy3.this.getLiveValue(e2);
                if (liveValue != null) {
                    this.f = new e0(key, liveValue);
                    z = true;
                } else {
                    z = false;
                }
                return z;
            } finally {
                this.c.postReadCleanup();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class n<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends ReentrantLock {
        @DexIgnore
        public volatile int count;
        @DexIgnore
        @Weak
        public /* final */ wy3<K, V, E, S> map;
        @DexIgnore
        public /* final */ int maxSegmentSize;
        @DexIgnore
        public int modCount;
        @DexIgnore
        public /* final */ AtomicInteger readCount; // = new AtomicInteger();
        @DexIgnore
        public volatile AtomicReferenceArray<E> table;
        @DexIgnore
        public int threshold;

        @DexIgnore
        public n(wy3<K, V, E, S> wy3, int i, int i2) {
            this.map = wy3;
            this.maxSegmentSize = i2;
            initTable(newEntryArray(i));
        }

        @DexIgnore
        public static <K, V, E extends i<K, V, E>> boolean isCollected(E e) {
            return e.getValue() == null;
        }

        @DexIgnore
        public abstract E castForTesting(i<K, V, ?> iVar);

        @DexIgnore
        public void clear() {
            if (this.count != 0) {
                lock();
                try {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    for (int i = 0; i < atomicReferenceArray.length(); i++) {
                        atomicReferenceArray.set(i, null);
                    }
                    maybeClearReferenceQueues();
                    this.readCount.set(0);
                    this.modCount++;
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public <T> void clearReferenceQueue(ReferenceQueue<T> referenceQueue) {
            do {
            } while (referenceQueue.poll() != null);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v5, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean clearValueForTesting(K k, int i, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else if (((b0) iVar2).b() == c0Var) {
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        return true;
                    } else {
                        unlock();
                        return false;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj, int i) {
            try {
                boolean z = false;
                if (this.count != 0) {
                    E liveEntry = getLiveEntry(obj, i);
                    if (!(liveEntry == null || liveEntry.getValue() == null)) {
                        z = true;
                    }
                    return z;
                }
                postReadCleanup();
                return false;
            } finally {
                postReadCleanup();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean containsValue(Object obj) {
            try {
                if (this.count != 0) {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    int length = atomicReferenceArray.length();
                    for (int i = 0; i < length; i++) {
                        for (E e = atomicReferenceArray.get(i); e != null; e = e.a()) {
                            Object liveValue = getLiveValue(e);
                            if (liveValue != null) {
                                if (this.map.valueEquivalence().equivalent(obj, liveValue)) {
                                    postReadCleanup();
                                    return true;
                                }
                            }
                        }
                    }
                }
                postReadCleanup();
                return false;
            } catch (Throwable th) {
                postReadCleanup();
                throw th;
            }
        }

        @DexIgnore
        public E copyEntry(E e, E e2) {
            return this.map.entryHelper.a((n) self(), (i) e, (i) e2);
        }

        @DexIgnore
        public E copyForTesting(i<K, V, ?> iVar, i<K, V, ?> iVar2) {
            return this.map.entryHelper.a((n) self(), (i) castForTesting(iVar), (i) castForTesting(iVar2));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.wy3<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public void drainKeyReferenceQueue(ReferenceQueue<K> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends K> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimKey((i) poll);
                    i++;
                } else {
                    return;
                }
            } while (i != 16);
        }

        @DexIgnore
        public void drainValueReferenceQueue(ReferenceQueue<V> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends V> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimValue((c0) poll);
                    i++;
                } else {
                    return;
                }
            } while (i != 16);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: java.util.concurrent.atomic.AtomicReferenceArray<E extends com.fossil.wy3$i<K, V, E>> */
        /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r6v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r8v4, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r8v5, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r6v3, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v3, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public void expand() {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = atomicReferenceArray.length();
            if (length < 1073741824) {
                int i = this.count;
                AtomicReferenceArray<E> atomicReferenceArray2 = (AtomicReferenceArray<E>) newEntryArray(length << 1);
                this.threshold = (atomicReferenceArray2.length() * 3) / 4;
                int length2 = atomicReferenceArray2.length() - 1;
                for (int i2 = 0; i2 < length; i2++) {
                    E e = atomicReferenceArray.get(i2);
                    if (e != null) {
                        i a = e.a();
                        int c = e.c() & length2;
                        if (a == null) {
                            atomicReferenceArray2.set(c, e);
                        } else {
                            i iVar = e;
                            while (a != null) {
                                int c2 = a.c() & length2;
                                if (c2 != c) {
                                    iVar = a;
                                    c = c2;
                                }
                                a = a.a();
                            }
                            atomicReferenceArray2.set(c, iVar);
                            while (e != iVar) {
                                int c3 = e.c() & length2;
                                i copyEntry = copyEntry(e, (i) atomicReferenceArray2.get(c3));
                                if (copyEntry != null) {
                                    atomicReferenceArray2.set(c3, copyEntry);
                                } else {
                                    i--;
                                }
                                e = e.a();
                            }
                        }
                    }
                }
                this.table = atomicReferenceArray2;
                this.count = i;
            }
        }

        @DexIgnore
        public V get(Object obj, int i) {
            try {
                E liveEntry = getLiveEntry(obj, i);
                if (liveEntry == null) {
                    return null;
                }
                V v = (V) liveEntry.getValue();
                if (v == null) {
                    tryDrainReferenceQueues();
                }
                postReadCleanup();
                return v;
            } finally {
                postReadCleanup();
            }
        }

        @DexIgnore
        public E getEntry(Object obj, int i) {
            E e;
            if (this.count == 0) {
                return null;
            }
            E first = getFirst(i);
            while (e != null) {
                if (e.c() == i) {
                    Object key = e.getKey();
                    if (key == null) {
                        tryDrainReferenceQueues();
                    } else if (this.map.keyEquivalence.equivalent(obj, key)) {
                        return e;
                    }
                }
                first = (E) e.a();
            }
            return null;
        }

        @DexIgnore
        public E getFirst(int i) {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            return atomicReferenceArray.get(i & (atomicReferenceArray.length() - 1));
        }

        @DexIgnore
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public E getLiveEntry(Object obj, int i) {
            return getEntry(obj, i);
        }

        @DexIgnore
        public V getLiveValue(E e) {
            if (e.getKey() == null) {
                tryDrainReferenceQueues();
                return null;
            }
            V v = (V) e.getValue();
            if (v != null) {
                return v;
            }
            tryDrainReferenceQueues();
            return null;
        }

        @DexIgnore
        public V getLiveValueForTesting(i<K, V, ?> iVar) {
            return getLiveValue(castForTesting(iVar));
        }

        @DexIgnore
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public c0<K, V, E> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            throw new AssertionError();
        }

        @DexIgnore
        public void initTable(AtomicReferenceArray<E> atomicReferenceArray) {
            int length = (atomicReferenceArray.length() * 3) / 4;
            this.threshold = length;
            if (length == this.maxSegmentSize) {
                this.threshold = length + 1;
            }
            this.table = atomicReferenceArray;
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
        }

        @DexIgnore
        public AtomicReferenceArray<E> newEntryArray(int i) {
            return new AtomicReferenceArray<>(i);
        }

        @DexIgnore
        public E newEntryForTesting(K k, int i, i<K, V, ?> iVar) {
            return this.map.entryHelper.a(self(), k, i, castForTesting(iVar));
        }

        @DexIgnore
        public c0<K, V, E> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            throw new AssertionError();
        }

        @DexIgnore
        public void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                runCleanup();
            }
        }

        @DexIgnore
        public void preWriteCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r12v4, resolved type: com.fossil.wy3$j<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$n */
        /* JADX DEBUG: Multi-variable search result rejected for r9v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public V put(K k, int i, V v, boolean z) {
            lock();
            try {
                preWriteCleanup();
                int i2 = this.count + 1;
                if (i2 > this.threshold) {
                    expand();
                    i2 = this.count + 1;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        V v2 = (V) iVar2.getValue();
                        if (v2 == null) {
                            this.modCount++;
                            setValue(iVar2, v);
                            this.count = this.count;
                            return null;
                        } else if (z) {
                            unlock();
                            return v2;
                        } else {
                            this.modCount++;
                            setValue(iVar2, v);
                            unlock();
                            return v2;
                        }
                    }
                }
                this.modCount++;
                E a = this.map.entryHelper.a(self(), k, i, iVar);
                setValue(a, v);
                atomicReferenceArray.set(length, a);
                this.count = i2;
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r5v5, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean reclaimKey(E e, int i) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = i & (atomicReferenceArray.length() - 1);
                i iVar = (i) atomicReferenceArray.get(length);
                for (i iVar2 = iVar; iVar2 != null; iVar2 = iVar2.a()) {
                    if (iVar2 == e) {
                        this.modCount++;
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        this.count--;
                        return true;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v7, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean reclaimValue(K k, int i, c0<K, V, E> c0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else if (((b0) iVar2).b() == c0Var) {
                        this.modCount++;
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        this.count--;
                        return true;
                    } else {
                        unlock();
                        return false;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v3, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public V remove(Object obj, int i) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(obj, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        V v = (V) iVar2.getValue();
                        if (v == null) {
                            if (!isCollected(iVar2)) {
                                unlock();
                                return null;
                            }
                        }
                        this.modCount++;
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        this.count--;
                        return v;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r2v3, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r6v4, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean removeEntryForTesting(E e) {
            int c = e.c();
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = c & (atomicReferenceArray.length() - 1);
            i iVar = (i) atomicReferenceArray.get(length);
            for (i iVar2 = iVar; iVar2 != null; iVar2 = iVar2.a()) {
                if (iVar2 == e) {
                    this.modCount++;
                    atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                    this.count--;
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public E removeFromChain(E e, E e2) {
            E e3;
            int i = this.count;
            E e4 = (E) e2.a();
            while (e != e2) {
                E copyEntry = copyEntry(e, e3);
                if (copyEntry != null) {
                    e3 = copyEntry;
                } else {
                    i--;
                }
                e = (E) e.a();
            }
            this.count = i;
            return e3;
        }

        @DexIgnore
        public E removeFromChainForTesting(i<K, V, ?> iVar, i<K, V, ?> iVar2) {
            return removeFromChain(castForTesting(iVar), castForTesting(iVar2));
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public boolean removeTableEntryForTesting(i<K, V, ?> iVar) {
            return removeEntryForTesting(castForTesting(iVar));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v9, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean replace(K k, int i, V v, V v2) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        Object value = iVar2.getValue();
                        if (value == null) {
                            if (isCollected(iVar2)) {
                                this.modCount++;
                                atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                                this.count--;
                            }
                            return false;
                        } else if (this.map.valueEquivalence().equivalent(v, value)) {
                            this.modCount++;
                            setValue(iVar2, v2);
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public void runCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        public void runLockedCleanup() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                    this.readCount.set(0);
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public abstract S self();

        @DexIgnore
        public void setTableEntryForTesting(int i, i<K, V, ?> iVar) {
            this.table.set(i, castForTesting(iVar));
        }

        @DexIgnore
        public void setValue(E e, V v) {
            this.map.entryHelper.a((n) self(), (i) e, (Object) v);
        }

        @DexIgnore
        public void setValueForTesting(i<K, V, ?> iVar, V v) {
            this.map.entryHelper.a((n) self(), (i) castForTesting(iVar), (Object) v);
        }

        @DexIgnore
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            throw new AssertionError();
        }

        @DexIgnore
        public void tryDrainReferenceQueues() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r9v6, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean remove(Object obj, int i, Object obj2) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (true) {
                    boolean z = false;
                    if (iVar2 != null) {
                        Object key = iVar2.getKey();
                        if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(obj, key)) {
                            iVar2 = iVar2.a();
                        } else {
                            if (this.map.valueEquivalence().equivalent(obj2, iVar2.getValue())) {
                                z = true;
                            } else if (!isCollected(iVar2)) {
                                unlock();
                                return false;
                            }
                            this.modCount++;
                            atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                            this.count--;
                            return z;
                        }
                    } else {
                        unlock();
                        return false;
                    }
                }
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.wy3$n<K, V, E extends com.fossil.wy3$i<K, V, E>, S extends com.fossil.wy3$n<K, V, E, S>> */
        /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: com.fossil.wy3$i */
        /* JADX DEBUG: Multi-variable search result rejected for r8v6, resolved type: com.fossil.wy3$i */
        /* JADX WARN: Multi-variable type inference failed */
        public V replace(K k, int i, V v) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.c() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        V v2 = (V) iVar2.getValue();
                        if (v2 == null) {
                            if (isCollected(iVar2)) {
                                this.modCount++;
                                atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                                this.count--;
                            }
                            return null;
                        }
                        this.modCount++;
                        setValue(iVar2, v);
                        unlock();
                        return v2;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }
    }
}
