package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum s90 {
    ACCEPT_PHONE_CALL((byte) 0),
    REJECT_PHONE_CALL((byte) 1),
    DISMISS_NOTIFICATION((byte) 2),
    REPLY_MESSAGE((byte) 3);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final s90 a(byte b) {
            s90[] values = s90.values();
            for (s90 s90 : values) {
                if (s90.a() == b) {
                    return s90;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public s90(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
