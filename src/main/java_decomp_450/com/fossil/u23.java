package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u23 implements v23 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.sdk.screen.manual_screen_view_logging", true);
        b = dr2.a("measurement.sdk.screen.disabling_automatic_reporting", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.v23
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v23
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.v23
    public final boolean zzc() {
        return b.b().booleanValue();
    }
}
