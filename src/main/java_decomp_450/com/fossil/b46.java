package com.fossil;

import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b46 extends he {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public PlacesClient a;
    @DexIgnore
    public AddressWrapper b;
    @DexIgnore
    public AddressWrapper c;
    @DexIgnore
    public List<String> d;
    @DexIgnore
    public MutableLiveData<b> e; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public AddressWrapper.AddressType a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public PlacesClient g;
        @DexIgnore
        public Boolean h;
        @DexIgnore
        public Boolean i;

        @DexIgnore
        public b(AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
            this.a = addressType;
            this.b = str;
            this.c = str2;
            this.d = bool;
            this.e = bool2;
            this.f = bool3;
            this.g = placesClient;
            this.h = bool4;
            this.i = bool5;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final Boolean b() {
            return this.d;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public final PlacesClient d() {
            return this.g;
        }

        @DexIgnore
        public final AddressWrapper.AddressType e() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && ee7.a(this.b, bVar.b) && ee7.a(this.c, bVar.c) && ee7.a(this.d, bVar.d) && ee7.a(this.e, bVar.e) && ee7.a(this.f, bVar.f) && ee7.a(this.g, bVar.g) && ee7.a(this.h, bVar.h) && ee7.a(this.i, bVar.i);
        }

        @DexIgnore
        public final Boolean f() {
            return this.h;
        }

        @DexIgnore
        public final Boolean g() {
            return this.i;
        }

        @DexIgnore
        public final Boolean h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            AddressWrapper.AddressType addressType = this.a;
            int i2 = 0;
            int hashCode = (addressType != null ? addressType.hashCode() : 0) * 31;
            String str = this.b;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.c;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            Boolean bool = this.d;
            int hashCode4 = (hashCode3 + (bool != null ? bool.hashCode() : 0)) * 31;
            Boolean bool2 = this.e;
            int hashCode5 = (hashCode4 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.f;
            int hashCode6 = (hashCode5 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
            PlacesClient placesClient = this.g;
            int hashCode7 = (hashCode6 + (placesClient != null ? placesClient.hashCode() : 0)) * 31;
            Boolean bool4 = this.h;
            int hashCode8 = (hashCode7 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
            Boolean bool5 = this.i;
            if (bool5 != null) {
                i2 = bool5.hashCode();
            }
            return hashCode8 + i2;
        }

        @DexIgnore
        public final Boolean i() {
            return this.f;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(type=" + this.a + ", name=" + this.b + ", address=" + this.c + ", avoidTolls=" + this.d + ", isNameEditable=" + this.e + ", isValid=" + this.f + ", placesClient=" + this.g + ", isLoading=" + this.h + ", isLocationUnavailable=" + this.i + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<TResult> implements jo3<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ b46 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(b46 b46, String str) {
            this.a = b46;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            AddressWrapper a2 = this.a.b;
            if (a2 != null) {
                ee7.a((Object) fetchPlaceResponse, "response");
                Place place = fetchPlaceResponse.getPlace();
                ee7.a((Object) place, "response.place");
                LatLng latLng = place.getLatLng();
                if (latLng != null) {
                    a2.setLat(latLng.a);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            AddressWrapper a3 = this.a.b;
            if (a3 != null) {
                ee7.a((Object) fetchPlaceResponse, "response");
                Place place2 = fetchPlaceResponse.getPlace();
                ee7.a((Object) place2, "response.place");
                LatLng latLng2 = place2.getLatLng();
                if (latLng2 != null) {
                    a3.setLng(latLng2.b);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            AddressWrapper a4 = this.a.b;
            if (a4 != null) {
                a4.setAddress(this.b);
            }
            b46.a(this.a, null, null, null, null, null, Boolean.valueOf(this.a.d()), null, false, false, 95, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements io3 {
        @DexIgnore
        public /* final */ /* synthetic */ b46 a;

        @DexIgnore
        public d(b46 b46) {
            this.a = b46;
        }

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            ee7.b(exc, "exception");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String g = b46.f;
            local.e(g, "FetchPlaceRequest - exception=" + exc);
            b46.a(this.a, null, null, null, null, null, Boolean.valueOf(this.a.d()), null, false, true, 95, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1", f = "CommuteTimeSettingsDetailViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ b46 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(b46 b46, fb7 fb7) {
            super(2, fb7);
            this.this$0 = b46;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.a = Places.createClient(PortfolioApp.g0.c());
                AddressWrapper a = this.this$0.b;
                Boolean bool = null;
                boolean z = (a != null ? a.getType() : null) == AddressWrapper.AddressType.OTHER;
                b46 b46 = this.this$0;
                AddressWrapper a2 = b46.b;
                AddressWrapper.AddressType type = a2 != null ? a2.getType() : null;
                AddressWrapper a3 = this.this$0.b;
                String name = a3 != null ? a3.getName() : null;
                Boolean a4 = pb7.a(z);
                AddressWrapper a5 = this.this$0.b;
                String address = a5 != null ? a5.getAddress() : null;
                AddressWrapper a6 = this.this$0.b;
                if (a6 != null) {
                    bool = pb7.a(a6.getAvoidTolls());
                }
                b46.a(b46, type, name, address, bool, a4, null, this.this$0.a, null, null, 416, null);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = b46.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public b46(ch5 ch5, UserRepository userRepository) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(userRepository, "mUserRepository");
    }

    @DexIgnore
    public final boolean c() {
        String str;
        String name;
        List<String> list = this.d;
        if (list != null) {
            for (String str2 : list) {
                AddressWrapper addressWrapper = this.b;
                if (addressWrapper == null || (name = addressWrapper.getName()) == null) {
                    str = null;
                } else if (name != null) {
                    str = name.toUpperCase();
                    ee7.a((Object) str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
                if (str2 != null) {
                    String upperCase = str2.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    if (ee7.a((Object) str, (Object) upperCase)) {
                        return true;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            return false;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final boolean d() {
        String str;
        String str2;
        String name;
        String name2;
        AddressWrapper addressWrapper = this.b;
        String str3 = null;
        if (TextUtils.isEmpty(addressWrapper != null ? addressWrapper.getAddress() : null)) {
            return false;
        }
        AddressWrapper addressWrapper2 = this.b;
        if (TextUtils.isEmpty(addressWrapper2 != null ? addressWrapper2.getName() : null)) {
            return false;
        }
        AddressWrapper addressWrapper3 = this.b;
        Boolean valueOf = addressWrapper3 != null ? Boolean.valueOf(addressWrapper3.getAvoidTolls()) : null;
        AddressWrapper addressWrapper4 = this.c;
        if (!(!ee7.a(valueOf, addressWrapper4 != null ? Boolean.valueOf(addressWrapper4.getAvoidTolls()) : null))) {
            AddressWrapper addressWrapper5 = this.b;
            AddressWrapper.AddressType type = addressWrapper5 != null ? addressWrapper5.getType() : null;
            AddressWrapper addressWrapper6 = this.c;
            if (type == (addressWrapper6 != null ? addressWrapper6.getType() : null)) {
                AddressWrapper addressWrapper7 = this.b;
                if (addressWrapper7 == null || (name2 = addressWrapper7.getName()) == null) {
                    str = null;
                } else if (name2 != null) {
                    str = name2.toUpperCase();
                    ee7.a((Object) str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
                AddressWrapper addressWrapper8 = this.c;
                if (addressWrapper8 == null || (name = addressWrapper8.getName()) == null) {
                    str2 = null;
                } else if (name != null) {
                    str2 = name.toUpperCase();
                    ee7.a((Object) str2, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
                if (!(!ee7.a((Object) str, (Object) str2))) {
                    AddressWrapper addressWrapper9 = this.b;
                    String address = addressWrapper9 != null ? addressWrapper9.getAddress() : null;
                    AddressWrapper addressWrapper10 = this.c;
                    if (addressWrapper10 != null) {
                        str3 = addressWrapper10.getAddress();
                    }
                    if (!ee7.a((Object) address, (Object) str3)) {
                        return true;
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final void e() {
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void f() {
        this.a = null;
    }

    @DexIgnore
    public final MutableLiveData<b> b() {
        return this.e;
    }

    @DexIgnore
    public final AddressWrapper a() {
        return this.b;
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
        this.c = addressWrapper;
        this.b = addressWrapper != null ? addressWrapper.clone() : null;
        this.d = arrayList;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "name");
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setName(str);
        }
        a(this, null, null, null, null, null, Boolean.valueOf(d()), null, null, null, 479, null);
    }

    @DexIgnore
    public static /* synthetic */ void a(b46 b46, String str, String str2, AutocompleteSessionToken autocompleteSessionToken, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            autocompleteSessionToken = null;
        }
        b46.a(str, str2, autocompleteSessionToken);
    }

    @DexIgnore
    public final void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        ee7.b(str, "address");
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && autocompleteSessionToken != null) {
            if (str2 != null) {
                a(str2, autocompleteSessionToken, str);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(String str, Location location) {
        ee7.b(str, "address");
        ee7.b(location, PlaceFields.LOCATION);
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAddress(str);
        }
        AddressWrapper addressWrapper2 = this.b;
        if (addressWrapper2 != null) {
            addressWrapper2.setLat(location.getLatitude());
        }
        AddressWrapper addressWrapper3 = this.b;
        if (addressWrapper3 != null) {
            addressWrapper3.setLng(location.getLongitude());
        }
        a(this, null, null, null, null, null, Boolean.valueOf(d()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void a(boolean z) {
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAvoidTolls(z);
        }
        a(this, null, null, null, null, null, Boolean.valueOf(d()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void a(String str, AutocompleteSessionToken autocompleteSessionToken, String str2) {
        if (this.a != null && this.b != null) {
            a(this, null, null, null, null, null, null, null, true, null, 383, null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(Place.Field.ADDRESS);
            arrayList.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str, arrayList);
            ee7.a((Object) builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.a;
            if (placesClient != null) {
                no3<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                ee7.a((Object) fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.a(new c(this, str2));
                fetchPlace.a(new d(this));
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(b46 b46, AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5, int i, Object obj) {
        if ((i & 1) != 0) {
            addressType = null;
        }
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = null;
        }
        if ((i & 8) != 0) {
            bool = null;
        }
        if ((i & 16) != 0) {
            bool2 = null;
        }
        if ((i & 32) != 0) {
            bool3 = null;
        }
        if ((i & 64) != 0) {
            placesClient = null;
        }
        if ((i & 128) != 0) {
            bool4 = null;
        }
        if ((i & 256) != 0) {
            bool5 = null;
        }
        b46.a(addressType, str, str2, bool, bool2, bool3, placesClient, bool4, bool5);
    }

    @DexIgnore
    public final void a(AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
        this.e.a(new b(addressType, str, str2, bool, bool2, bool3, placesClient, bool4, bool5));
    }
}
