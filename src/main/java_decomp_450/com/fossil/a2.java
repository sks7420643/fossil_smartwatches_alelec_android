package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.fossil.p1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a2 extends p1 implements SubMenu {
    @DexIgnore
    public p1 B;
    @DexIgnore
    public r1 C;

    @DexIgnore
    public a2(Context context, p1 p1Var, r1 r1Var) {
        super(context);
        this.B = p1Var;
        this.C = r1Var;
    }

    @DexIgnore
    @Override // com.fossil.p1
    public void a(p1.a aVar) {
        this.B.a(aVar);
    }

    @DexIgnore
    @Override // com.fossil.p1
    public boolean b(r1 r1Var) {
        return this.B.b(r1Var);
    }

    @DexIgnore
    @Override // com.fossil.p1
    public String d() {
        r1 r1Var = this.C;
        int itemId = r1Var != null ? r1Var.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.d() + ":" + itemId;
    }

    @DexIgnore
    public MenuItem getItem() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.p1
    public p1 m() {
        return this.B.m();
    }

    @DexIgnore
    @Override // com.fossil.p1
    public boolean o() {
        return this.B.o();
    }

    @DexIgnore
    @Override // com.fossil.p1
    public boolean p() {
        return this.B.p();
    }

    @DexIgnore
    @Override // com.fossil.p1
    public boolean q() {
        return this.B.q();
    }

    @DexIgnore
    @Override // com.fossil.p1
    public void setGroupDividerEnabled(boolean z) {
        this.B.setGroupDividerEnabled(z);
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setIcon(Drawable drawable) {
        this.C.setIcon(drawable);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.p1
    public void setQwertyMode(boolean z) {
        this.B.setQwertyMode(z);
    }

    @DexIgnore
    public Menu t() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.p1
    public boolean a(p1 p1Var, MenuItem menuItem) {
        return super.a(p1Var, menuItem) || this.B.a(p1Var, menuItem);
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(int i) {
        super.d(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(int i) {
        super.e(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setIcon(int i) {
        this.C.setIcon(i);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.p1
    public boolean a(r1 r1Var) {
        return this.B.a(r1Var);
    }
}
