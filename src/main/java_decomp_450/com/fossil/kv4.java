package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<String>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<String>> {
    }

    @DexIgnore
    public final ArrayList<String> a(String str) {
        ee7.b(str, "value");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object a2 = new Gson().a(str, new b().getType());
        ee7.a(a2, "Gson().fromJson(value, type)");
        return (ArrayList) a2;
    }

    @DexIgnore
    public final String a(ArrayList<String> arrayList) {
        if (o92.a((Collection<?>) arrayList)) {
            return "";
        }
        String a2 = new Gson().a(arrayList, new a().getType());
        ee7.a((Object) a2, "Gson().toJson(list, type)");
        return a2;
    }
}
