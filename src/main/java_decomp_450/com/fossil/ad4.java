package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ad4 implements Callable {
    @DexIgnore
    public /* final */ bd4 a;

    @DexIgnore
    public ad4(bd4 bd4) {
        this.a = bd4;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.a.a();
    }
}
