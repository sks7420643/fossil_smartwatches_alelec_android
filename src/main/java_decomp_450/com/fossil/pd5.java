package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pd5 {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ ch5 a;
    @DexIgnore
    public /* final */ UserRepository b;
    @DexIgnore
    public /* final */ AlarmsRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(String str) {
            ee7.b(str, "day");
            String lowerCase = str.toLowerCase();
            ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            switch (lowerCase.hashCode()) {
                case 101661:
                    if (lowerCase.equals("fri")) {
                        return 6;
                    }
                    break;
                case 108300:
                    if (lowerCase.equals("mon")) {
                        return 2;
                    }
                    break;
                case 113638:
                    if (lowerCase.equals("sat")) {
                        return 7;
                    }
                    break;
                case 114252:
                    if (lowerCase.equals("sun")) {
                        return 1;
                    }
                    break;
                case 114817:
                    if (lowerCase.equals("thu")) {
                        return 5;
                    }
                    break;
                case 115204:
                    if (lowerCase.equals("tue")) {
                        return 3;
                    }
                    break;
                case 117590:
                    if (lowerCase.equals("wed")) {
                        return 4;
                    }
                    break;
            }
            return -1;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final long a(long j) {
            int i;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j);
            Calendar instance = Calendar.getInstance();
            if (instance.get(9) == 1) {
                int i2 = instance.get(10);
                i = i2 == 12 ? 12 : i2 + 12;
            } else {
                i = instance.get(10);
            }
            long j2 = ((long) ((((i * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000;
            long j3 = j2 <= j ? j - j2 : LogBuilder.MAX_INTERVAL - (j2 - j);
            long currentTimeMillis = System.currentTimeMillis() + j;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j3);
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("AlarmHelper", "getEndTimeFromAlarmMinute - currentSecond=" + instance.get(13));
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AlarmHelper", "getEndTimeFromAlarmMinute - alarmEnd=" + new Date(currentTimeMillis));
            return currentTimeMillis;
        }

        @DexIgnore
        public final boolean a(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            if (alarm.isRepeated() || !alarm.isActive()) {
                return false;
            }
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(zd5.d(alarm.getUpdatedAt()));
            int i = instance.get(9);
            int i2 = instance.get(10);
            if (i == 1 && i2 < 12) {
                i2 += 12;
            }
            Calendar instance2 = Calendar.getInstance();
            ee7.a((Object) instance2, "instanceCalendar");
            long timeInMillis = instance2.getTimeInMillis();
            zd5.d(instance2);
            ee7.a((Object) instance2, "DateHelper.getStartOfDay(instanceCalendar)");
            long timeInMillis2 = timeInMillis - instance2.getTimeInMillis();
            long millisecond = alarm.getMillisecond();
            if ((((long) ((((i2 * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000) + 1 <= millisecond && timeInMillis2 > millisecond) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1", f = "AlarmHelper.kt", l = {209, 216}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $currentMinute;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pd5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(pd5 pd5, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pd5;
            this.$currentMinute = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$currentMinute, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0099 A[EDGE_INSN: B:27:0x0099->B:24:0x0099 ?: BREAK  , SYNTHETIC] */
        @Override // com.fossil.ob7
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x0033
                if (r1 == r3) goto L_0x002b
                if (r1 != r2) goto L_0x0023
                java.lang.Object r1 = r9.L$3
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r4 = r9.L$2
                com.portfolio.platform.data.source.local.alarm.Alarm r4 = (com.portfolio.platform.data.source.local.alarm.Alarm) r4
                java.lang.Object r5 = r9.L$1
                java.util.List r5 = (java.util.List) r5
                java.lang.Object r6 = r9.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r10)
                r10 = r9
                goto L_0x008a
            L_0x0023:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x002b:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                goto L_0x0049
            L_0x0033:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r1 = r9.p$
                com.fossil.pd5 r10 = r9.this$0
                com.portfolio.platform.data.source.AlarmsRepository r10 = r10.b()
                r9.L$0 = r1
                r9.label = r3
                java.lang.Object r10 = r10.getActiveAlarms(r9)
                if (r10 != r0) goto L_0x0049
                return r0
            L_0x0049:
                java.util.List r10 = (java.util.List) r10
                if (r10 == 0) goto L_0x0099
                java.util.Iterator r4 = r10.iterator()
                r5 = r10
                r6 = r1
                r1 = r4
                r10 = r9
            L_0x0055:
                boolean r4 = r1.hasNext()
                if (r4 == 0) goto L_0x0099
                java.lang.Object r4 = r1.next()
                com.portfolio.platform.data.source.local.alarm.Alarm r4 = (com.portfolio.platform.data.source.local.alarm.Alarm) r4
                int r7 = r4.getTotalMinutes()
                int r8 = r10.$currentMinute
                if (r7 != r8) goto L_0x0055
                boolean r7 = r4.isRepeated()
                if (r7 != 0) goto L_0x0055
                r7 = 0
                r4.setActive(r7)
                com.fossil.pd5 r7 = r10.this$0
                com.portfolio.platform.data.source.AlarmsRepository r7 = r7.b()
                r10.L$0 = r6
                r10.L$1 = r5
                r10.L$2 = r4
                r10.L$3 = r1
                r10.label = r2
                java.lang.Object r7 = r7.updateAlarm(r4, r10)
                if (r7 != r0) goto L_0x008a
                return r0
            L_0x008a:
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.fossil.ec5 r8 = new com.fossil.ec5
                java.lang.String r4 = r4.getUri()
                r8.<init>(r3, r4)
                r7.a(r8)
                goto L_0x0055
            L_0x0099:
                com.fossil.i97 r10 = com.fossil.i97.a
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pd5.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1", f = "AlarmHelper.kt", l = {126, 184}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pd5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pd5 pd5, Context context, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pd5;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$context, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            long j;
            Object obj2;
            MFUser mFUser;
            yi7 yi7;
            long j2;
            int i;
            long j3;
            Object obj3;
            int i2;
            Object obj4;
            Object obj5;
            Iterator it;
            long j4;
            Integer num;
            long intValue;
            int hour;
            int minute;
            Integer num2;
            int i3;
            int i4;
            Object a = nb7.a();
            int i5 = this.label;
            long j5 = -1;
            if (i5 == 0) {
                t87.a(obj);
                yi7 yi72 = this.p$;
                Calendar instance = Calendar.getInstance();
                zd5.d(instance);
                ee7.a((Object) instance, "DateHelper.getStartOfDay(Calendar.getInstance())");
                long timeInMillis = instance.getTimeInMillis();
                int i6 = (Calendar.getInstance().get(11) * 60) + Calendar.getInstance().get(12);
                i2 = Calendar.getInstance().get(7);
                AlarmsRepository b = this.this$0.b();
                this.L$0 = yi72;
                this.J$0 = timeInMillis;
                this.I$0 = i6;
                this.I$1 = i2;
                this.J$1 = -1;
                this.label = 1;
                obj3 = b.getInComingActiveAlarms(this);
                if (obj3 == a) {
                    return a;
                }
                yi7 = yi72;
                j2 = timeInMillis;
                i = i6;
                j3 = -1;
            } else if (i5 == 1) {
                long j6 = this.J$1;
                i2 = this.I$1;
                int i7 = this.I$0;
                long j7 = this.J$0;
                t87.a(obj);
                yi7 = (yi7) this.L$0;
                j2 = j7;
                i = i7;
                j3 = j6;
                obj3 = obj;
            } else if (i5 == 2) {
                Alarm alarm = (Alarm) this.L$3;
                Alarm alarm2 = (Alarm) this.L$2;
                List list = (List) this.L$1;
                long j8 = this.J$1;
                yi7 yi73 = (yi7) this.L$0;
                t87.a(obj);
                j = j8;
                obj2 = obj;
                mFUser = (MFUser) obj2;
                if (mFUser != null || TextUtils.isEmpty(mFUser.getUserId())) {
                    return i97.a;
                }
                Bundle bundle = new Bundle();
                bundle.putString("DEF_ALARM_RECEIVER_USER_ID", mFUser.getUserId());
                bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
                Intent intent = new Intent(this.$context, AlarmReceiver.class);
                intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
                intent.putExtras(bundle);
                AlarmManager alarmManager = (AlarmManager) this.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
                if (alarmManager != null) {
                    FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler - setExactAlarm at " + new Date(j));
                    alarmManager.setExact(0, j, PendingIntent.getBroadcast(this.$context, 101, intent, 134217728));
                }
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj3;
            Iterator it2 = list2.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    obj4 = null;
                    break;
                }
                obj4 = it2.next();
                Alarm alarm3 = (Alarm) obj4;
                if (pb7.a(alarm3 != null && alarm3.getTotalMinutes() > i).booleanValue()) {
                    break;
                }
            }
            Alarm alarm4 = (Alarm) obj4;
            Iterator it3 = list2.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    obj5 = null;
                    break;
                }
                obj5 = it3.next();
                Alarm alarm5 = (Alarm) obj5;
                if (pb7.a(alarm5 != null && alarm5.getTotalMinutes() <= i).booleanValue()) {
                    break;
                }
            }
            Alarm alarm6 = (Alarm) obj5;
            if (alarm4 != null) {
                if (alarm4.getDays() != null) {
                    int[] days = alarm4.getDays();
                    if (days != null) {
                        if (days.length == 0) {
                            i4 = alarm4.getHour() * 60;
                            i3 = alarm4.getMinute();
                        } else {
                            int[] days2 = alarm4.getDays();
                            if (days2 == null) {
                                ee7.a();
                                throw null;
                            } else if (t97.a(days2, i2)) {
                                i4 = alarm4.getHour() * 60;
                                i3 = alarm4.getMinute();
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    i4 = alarm4.getHour() * 60;
                    i3 = alarm4.getMinute();
                }
                j3 = ((long) ((i4 + i3) * 60 * 1000)) + j2;
            } else if (alarm6 != null) {
                if (alarm6.getDays() != null) {
                    int[] days3 = alarm6.getDays();
                    if (days3 != null) {
                        if (days3.length == 0) {
                            j3 = j2 + LogBuilder.MAX_INTERVAL + ((long) (((alarm6.getHour() * 60) + alarm6.getMinute()) * 60 * 1000));
                        } else {
                            int[] days4 = alarm6.getDays();
                            if (days4 == null) {
                                ee7.a();
                                throw null;
                            } else if (t97.a(days4, i2)) {
                                j3 = j2 + LogBuilder.MAX_INTERVAL + ((long) (((alarm6.getHour() * 60) + alarm6.getMinute()) * 60 * 1000));
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    j3 = ((long) (((alarm6.getHour() * 60) + alarm6.getMinute()) * 60 * 1000)) + j2 + LogBuilder.MAX_INTERVAL;
                }
                j5 = -1;
            }
            if (j3 == j5) {
                Iterator it4 = list2.iterator();
                while (it4.hasNext()) {
                    Alarm alarm7 = (Alarm) it4.next();
                    if (!(alarm7 == null || alarm7.getDays() == null)) {
                        int[] days5 = alarm7.getDays();
                        if (days5 != null) {
                            if (!(days5.length == 0)) {
                                int[] days6 = alarm7.getDays();
                                if (days6 == null) {
                                    ee7.a();
                                    throw null;
                                } else if (!t97.a(days6, i2)) {
                                    int[] days7 = alarm7.getDays();
                                    if (days7 != null) {
                                        it = it4;
                                        int length = days7.length;
                                        j4 = j3;
                                        int i8 = 0;
                                        while (true) {
                                            if (i8 >= length) {
                                                num = null;
                                                break;
                                            }
                                            int i9 = days7[i8];
                                            if (pb7.a(i2 < pb7.a(i9).intValue()).booleanValue()) {
                                                num = pb7.a(i9);
                                                break;
                                            }
                                            i8++;
                                            length = length;
                                        }
                                        if (num == null) {
                                            int[] days8 = alarm7.getDays();
                                            if (days8 != null) {
                                                int length2 = days8.length;
                                                int i10 = 0;
                                                while (true) {
                                                    if (i10 >= length2) {
                                                        num2 = null;
                                                        break;
                                                    }
                                                    int i11 = days8[i10];
                                                    if (pb7.a(i2 > pb7.a(i11).intValue()).booleanValue()) {
                                                        num2 = pb7.a(i11);
                                                        break;
                                                    }
                                                    i10++;
                                                    days8 = days8;
                                                }
                                                if (num2 != null) {
                                                    intValue = (((long) (i2 - num2.intValue())) * LogBuilder.MAX_INTERVAL) + j2;
                                                    hour = alarm7.getHour() * 60;
                                                    minute = alarm7.getMinute();
                                                } else {
                                                    it4 = it;
                                                    j3 = j4;
                                                }
                                            } else {
                                                ee7.a();
                                                throw null;
                                            }
                                        } else {
                                            intValue = (((long) (num.intValue() - i2)) * LogBuilder.MAX_INTERVAL) + j2;
                                            hour = alarm7.getHour() * 60;
                                            minute = alarm7.getMinute();
                                        }
                                        j = intValue + ((long) ((hour + (minute - 1)) * 60 * 1000));
                                        break;
                                    }
                                    ee7.a();
                                    throw null;
                                }
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    it = it4;
                    j4 = j3;
                    it4 = it;
                    j3 = j4;
                }
            }
            j = j3;
            if (j == -1) {
                FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler - All alarms got active");
                return i97.a;
            }
            UserRepository c = this.this$0.c();
            this.L$0 = yi7;
            this.J$0 = j2;
            this.I$0 = i;
            this.I$1 = i2;
            this.J$1 = j;
            this.L$1 = list2;
            this.L$2 = alarm4;
            this.L$3 = alarm6;
            this.label = 2;
            obj2 = c.getCurrentUser(this);
            if (obj2 == a) {
                return a;
            }
            mFUser = (MFUser) obj2;
            if (mFUser != null) {
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.AlarmHelper", f = "AlarmHelper.kt", l = {250, 272}, m = "validateAlarms")
    public static final class d extends rb7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public long J$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pd5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(pd5 pd5, fb7 fb7) {
            super(fb7);
            this.this$0 = pd5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore
    public pd5(ch5 ch5, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        this.a = ch5;
        this.b = userRepository;
        this.c = alarmsRepository;
    }

    @DexIgnore
    public final void a() {
        int i = (Calendar.getInstance().get(11) * 60) + Calendar.getInstance().get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "endJobScheduler - currentMinute=" + i);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, i, null), 3, null);
    }

    @DexIgnore
    public final AlarmsRepository b() {
        return this.c;
    }

    @DexIgnore
    public final UserRepository c() {
        return this.b;
    }

    @DexIgnore
    public final void d(Context context) {
        ee7.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler");
        a(context);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new c(this, context, null), 3, null);
    }

    @DexIgnore
    public final void e(Context context) {
        ee7.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startRemindSyncAlarm");
        Calendar instance = Calendar.getInstance();
        if (Calendar.getInstance().get(11) >= 15) {
            instance.add(6, 1);
        }
        instance.set(11, 15);
        instance.set(12, 0);
        instance.set(13, 0);
        Bundle bundle = new Bundle();
        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 2);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtras(bundle);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 20, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            try {
                alarmManager.cancel(broadcast);
                ee7.a((Object) instance, "calendar");
                alarmManager.setRepeating(0, instance.getTimeInMillis(), LogBuilder.MAX_INTERVAL, broadcast);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("AlarmHelper", "schedule SyncAlarm at = " + new Date(instance.getTimeInMillis()));
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("AlarmHelper", "startSyncAlarm() - Exception: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public final void f(Context context) {
        ee7.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm");
        this.a.w("");
        Intent intent = new Intent(context, AlarmReceiver.class);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 0);
        intent2.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 134217728);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 1, intent2, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            alarmManager.cancel(broadcast);
            alarmManager.cancel(broadcast2);
            FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm success");
        }
    }

    @DexIgnore
    public final void b(Context context) {
        ee7.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startExactReplaceBatteryAlarm");
        Calendar instance = Calendar.getInstance();
        instance.set(11, 9);
        instance.set(12, 0);
        instance.set(13, 0);
        ee7.a((Object) instance, "triggerCalendar");
        long timeInMillis = instance.getTimeInMillis();
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 1, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                alarmManager.setExactAndAllowWhileIdle(0, timeInMillis, broadcast);
            } else {
                alarmManager.setExact(0, timeInMillis, broadcast);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "startExactReplaceBatteryAlarm - triggerAtMillis=" + timeInMillis);
        }
    }

    @DexIgnore
    public final void c(Context context) {
        ee7.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startHWLogScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("REQUEST_CODE", 10);
            PendingIntent broadcast = PendingIntent.getBroadcast(context, 10, intent, 134217728);
            alarmManager.cancel(broadcast);
            Calendar instance = Calendar.getInstance();
            instance.set(11, 10);
            instance.set(12, 0);
            instance.set(13, 0);
            ee7.a((Object) instance, "triggerCalendar");
            long timeInMillis = instance.getTimeInMillis();
            alarmManager.setInexactRepeating(0, timeInMillis, LogBuilder.MAX_INTERVAL, broadcast);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "startHWLogScheduler - setInexactRepeating: triggerAtMillis=" + timeInMillis + ", interval=" + LogBuilder.MAX_INTERVAL);
        }
    }

    @DexIgnore
    public final void a(Context context) {
        ee7.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "cancelJobScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 101, intent, 134217728);
        if (!(broadcast == null || alarmManager == null)) {
            alarmManager.cancel(broadcast);
        }
        AlarmManager alarmManager2 = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent2.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 102, intent2, 134217728);
        if (alarmManager2 != null) {
            alarmManager2.cancel(broadcast2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00b2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r28) {
        /*
            r27 = this;
            r0 = r27
            r1 = r28
            boolean r2 = r1 instanceof com.fossil.pd5.d
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.pd5$d r2 = (com.fossil.pd5.d) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.pd5$d r2 = new com.fossil.pd5$d
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            java.lang.String r5 = "AlarmHelper"
            r6 = 2
            r8 = 1
            if (r4 == 0) goto L_0x0079
            if (r4 == r8) goto L_0x006f
            if (r4 != r6) goto L_0x0067
            long r8 = r2.J$2
            long r8 = r2.J$1
            java.lang.Object r4 = r2.L$10
            java.util.Calendar r4 = (java.util.Calendar) r4
            java.lang.Object r4 = r2.L$9
            java.util.Calendar r4 = (java.util.Calendar) r4
            java.lang.Object r4 = r2.L$8
            com.portfolio.platform.data.source.local.alarm.Alarm r4 = (com.portfolio.platform.data.source.local.alarm.Alarm) r4
            java.lang.Object r8 = r2.L$7
            com.fossil.re7 r8 = (com.fossil.re7) r8
            java.lang.Object r8 = r2.L$6
            com.portfolio.platform.data.source.local.alarm.Alarm r8 = (com.portfolio.platform.data.source.local.alarm.Alarm) r8
            java.lang.Object r8 = r2.L$5
            java.lang.Object r8 = r2.L$4
            java.util.Iterator r8 = (java.util.Iterator) r8
            java.lang.Object r9 = r2.L$3
            java.lang.Iterable r9 = (java.lang.Iterable) r9
            java.lang.Object r10 = r2.L$2
            java.util.List r10 = (java.util.List) r10
            java.lang.Object r11 = r2.L$1
            java.util.List r11 = (java.util.List) r11
            long r12 = r2.J$0
            java.lang.Object r14 = r2.L$0
            com.fossil.pd5 r14 = (com.fossil.pd5) r14
            com.fossil.t87.a(r1)
            r21 = r5
            r0 = 0
            r5 = 2
            goto L_0x01ff
        L_0x0067:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x006f:
            long r9 = r2.J$0
            java.lang.Object r4 = r2.L$0
            com.fossil.pd5 r4 = (com.fossil.pd5) r4
            com.fossil.t87.a(r1)
            goto L_0x00a7
        L_0x0079:
            com.fossil.t87.a(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = "validateAlarms()"
            r1.d(r5, r4)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            com.fossil.zd5.d(r1)
            java.lang.String r4 = "DateHelper.getStartOfDay(Calendar.getInstance())"
            com.fossil.ee7.a(r1, r4)
            long r9 = r1.getTimeInMillis()
            com.portfolio.platform.data.source.AlarmsRepository r1 = r0.c
            r2.L$0 = r0
            r2.J$0 = r9
            r2.label = r8
            java.lang.Object r1 = r1.getInComingActiveAlarms(r2)
            if (r1 != r3) goto L_0x00a6
            return r3
        L_0x00a6:
            r4 = r0
        L_0x00a7:
            java.util.List r1 = (java.util.List) r1
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.Iterator r12 = r1.iterator()
        L_0x00b2:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x00ef
            java.lang.Object r13 = r12.next()
            r14 = r13
            com.portfolio.platform.data.source.local.alarm.Alarm r14 = (com.portfolio.platform.data.source.local.alarm.Alarm) r14
            r15 = 0
            if (r14 == 0) goto L_0x00c7
            int[] r16 = r14.getDays()
            goto L_0x00c9
        L_0x00c7:
            r16 = r15
        L_0x00c9:
            if (r16 == 0) goto L_0x00e0
            int[] r14 = r14.getDays()
            if (r14 == 0) goto L_0x00dc
            int r14 = r14.length
            if (r14 != 0) goto L_0x00d6
            r14 = 1
            goto L_0x00d7
        L_0x00d6:
            r14 = 0
        L_0x00d7:
            r14 = r14 ^ r8
            if (r14 == 0) goto L_0x00e0
            r14 = 1
            goto L_0x00e1
        L_0x00dc:
            com.fossil.ee7.a()
            throw r15
        L_0x00e0:
            r14 = 0
        L_0x00e1:
            java.lang.Boolean r14 = com.fossil.pb7.a(r14)
            boolean r14 = r14.booleanValue()
            if (r14 != 0) goto L_0x00b2
            r11.add(r13)
            goto L_0x00b2
        L_0x00ef:
            java.util.Iterator r8 = r11.iterator()
            r14 = r4
            r12 = r9
            r9 = r11
            r10 = r9
            r11 = r1
        L_0x00f8:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x022f
            java.lang.Object r1 = r8.next()
            r4 = r1
            com.portfolio.platform.data.source.local.alarm.Alarm r4 = (com.portfolio.platform.data.source.local.alarm.Alarm) r4
            com.fossil.re7 r15 = new com.fossil.re7
            r15.<init>()
            r6 = -1
            r15.element = r6
            if (r4 == 0) goto L_0x0226
            java.util.Calendar r6 = java.util.Calendar.getInstance()
            java.lang.String r7 = "updateTimeCal"
            com.fossil.ee7.a(r6, r7)
            java.lang.String r7 = r4.getUpdatedAt()
            java.util.Date r7 = com.fossil.zd5.d(r7)
            r6.setTime(r7)
            java.util.Calendar r7 = java.util.Calendar.getInstance()
            java.lang.String r0 = "startTimeCal"
            com.fossil.ee7.a(r7, r0)
            java.lang.String r0 = r4.getUpdatedAt()
            java.util.Date r0 = com.fossil.zd5.d(r0)
            r7.setTime(r0)
            com.fossil.zd5.d(r7)
            java.lang.String r0 = "DateHelper.getStartOfDay(startTimeCal)"
            com.fossil.ee7.a(r7, r0)
            r19 = r1
            long r0 = r7.getTimeInMillis()
            int r20 = r4.getHour()
            int r20 = r20 * 60
            int r21 = r4.getMinute()
            int r20 = r20 + r21
            r21 = r5
            int r5 = r20 * 60
            int r5 = r5 * 1000
            r22 = r7
            r20 = r8
            long r7 = (long) r5
            long r7 = r7 + r0
            long r23 = r12 - r0
            long r23 = java.lang.Math.abs(r23)
            r25 = 86400000(0x5265c00, double:4.2687272E-316)
            int r5 = (r23 > r25 ? 1 : (r23 == r25 ? 0 : -1))
            if (r5 > 0) goto L_0x01a4
            long r23 = r6.getTimeInMillis()
            int r5 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
            if (r5 < 0) goto L_0x018b
            long r25 = r0 + r25
            int r5 = r4.getHour()
            int r5 = r5 * 60
            int r23 = r4.getMinute()
            int r5 = r5 + r23
            int r5 = r5 * 60
            int r5 = r5 * 1000
            r23 = r7
            long r7 = (long) r5
            long r25 = r25 + r7
            goto L_0x019f
        L_0x018b:
            r23 = r7
            int r5 = r4.getHour()
            int r5 = r5 * 60
            int r7 = r4.getMinute()
            int r5 = r5 + r7
            int r5 = r5 * 60
            int r5 = r5 * 1000
            long r7 = (long) r5
            long r25 = r0 + r7
        L_0x019f:
            r7 = r25
            r15.element = r7
            goto L_0x01ac
        L_0x01a4:
            r23 = r7
            r4.getHour()
            r4.getMinute()
        L_0x01ac:
            long r7 = r15.element
            r17 = -1
            int r5 = (r7 > r17 ? 1 : (r7 == r17 ? 0 : -1))
            if (r5 == 0) goto L_0x0221
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            java.lang.String r7 = "Calendar.getInstance()"
            com.fossil.ee7.a(r5, r7)
            long r7 = r5.getTimeInMillis()
            r17 = r0
            long r0 = r15.element
            int r5 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r5 < 0) goto L_0x0221
            r0 = 0
            r4.setActive(r0)
            com.portfolio.platform.data.source.AlarmsRepository r1 = r14.c
            r2.L$0 = r14
            r2.J$0 = r12
            r2.L$1 = r11
            r2.L$2 = r10
            r2.L$3 = r9
            r8 = r20
            r2.L$4 = r8
            r5 = r19
            r2.L$5 = r5
            r2.L$6 = r4
            r2.L$7 = r15
            r2.L$8 = r4
            r2.L$9 = r6
            r5 = r22
            r2.L$10 = r5
            r5 = r17
            r2.J$1 = r5
            r5 = r23
            r2.J$2 = r5
            r5 = 2
            r2.label = r5
            java.lang.Object r1 = r1.updateAlarm(r4, r2)
            if (r1 != r3) goto L_0x01ff
            return r3
        L_0x01ff:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "validateAlarms() - alarm is updated ("
            r6.append(r7)
            r6.append(r4)
            r4 = 41
            r6.append(r4)
            java.lang.String r4 = r6.toString()
            r6 = r21
            r1.d(r6, r4)
            goto L_0x0229
        L_0x0221:
            r8 = r20
            r6 = r21
            goto L_0x0227
        L_0x0226:
            r6 = r5
        L_0x0227:
            r0 = 0
            r5 = 2
        L_0x0229:
            r0 = r27
            r5 = r6
            r6 = 2
            goto L_0x00f8
        L_0x022f:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pd5.a(com.fossil.fb7):java.lang.Object");
    }
}
