package com.fossil;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp3<TResult> extends no3<TResult> {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ ip3<TResult> b; // = new ip3<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public TResult e;
    @DexIgnore
    public Exception f;

    @DexIgnore
    @Override // com.fossil.no3
    public final <X extends Throwable> TResult a(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (cls.isInstance(this.f)) {
                throw cls.cast(this.f);
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new lo3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final TResult b() {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new lo3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final boolean c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @DexIgnore
    public final boolean f() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void g() {
        a72.b(this.c, "Task is not yet complete");
    }

    @DexIgnore
    public final void h() {
        a72.b(!this.c, "Task is already complete");
    }

    @DexIgnore
    public final void i() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    @DexIgnore
    public final void j() {
        synchronized (this.a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final <TContinuationResult> no3<TContinuationResult> b(fo3<TResult, no3<TContinuationResult>> fo3) {
        return b(po3.a, fo3);
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final <TContinuationResult> no3<TContinuationResult> b(Executor executor, fo3<TResult, no3<TContinuationResult>> fo3) {
        lp3 lp3 = new lp3();
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new uo3(executor, fo3, lp3));
        j();
        return lp3;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final Exception a() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(jo3<? super TResult> jo3) {
        a(po3.a, jo3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(Executor executor, jo3<? super TResult> jo3) {
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new ep3(executor, jo3));
        j();
        return this;
    }

    @DexIgnore
    public final boolean b(TResult tresult) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(io3 io3) {
        a(po3.a, io3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(Executor executor, io3 io3) {
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new bp3(executor, io3));
        j();
        return this;
    }

    @DexIgnore
    public final boolean b(Exception exc) {
        a72.a(exc, "Exception must not be null");
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(ho3<TResult> ho3) {
        a(po3.a, ho3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(Executor executor, ho3<TResult> ho3) {
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new ap3(executor, ho3));
        j();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final <TContinuationResult> no3<TContinuationResult> a(fo3<TResult, TContinuationResult> fo3) {
        return a(po3.a, fo3);
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final <TContinuationResult> no3<TContinuationResult> a(Executor executor, fo3<TResult, TContinuationResult> fo3) {
        lp3 lp3 = new lp3();
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new to3(executor, fo3, lp3));
        j();
        return lp3;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final no3<TResult> a(Executor executor, go3 go3) {
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new wo3(executor, go3));
        j();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final <TContinuationResult> no3<TContinuationResult> a(Executor executor, mo3<TResult, TContinuationResult> mo3) {
        lp3 lp3 = new lp3();
        ip3<TResult> ip3 = this.b;
        np3.a(executor);
        ip3.a(new fp3(executor, mo3, lp3));
        j();
        return lp3;
    }

    @DexIgnore
    @Override // com.fossil.no3
    public final <TContinuationResult> no3<TContinuationResult> a(mo3<TResult, TContinuationResult> mo3) {
        return a(po3.a, mo3);
    }

    @DexIgnore
    public final void a(TResult tresult) {
        synchronized (this.a) {
            h();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final void a(Exception exc) {
        a72.a(exc, "Exception must not be null");
        synchronized (this.a) {
            h();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }
}
