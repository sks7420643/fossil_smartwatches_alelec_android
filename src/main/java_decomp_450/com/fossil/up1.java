package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up1 extends v81 {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ long C; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ vc7<i97> E;
    @DexIgnore
    public vl1 F;
    @DexIgnore
    public /* final */ short G;
    @DexIgnore
    public /* final */ yx0 H;

    @DexIgnore
    public up1(short s, yx0 yx0, ri1 ri1) {
        super(qa1.O, ri1, 0, 4);
        this.G = s;
        this.H = yx0;
        this.E = new un1(this, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.B = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void b(rk1 rk1) {
        if (rk1.a == qk1.FTC) {
            byte[] bArr = rk1.b;
            if (bArr.length >= 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                if (this.G != order.getShort(1)) {
                    return;
                }
                if (g51.LEGACY_PUT_FILE_EOF.a == b) {
                    k();
                    sz0 a = sz0.f.a(tu0.j.a(order.get(3)));
                    ((v81) this).v = sz0.a(((v81) this).v, null, null, a.c, null, a.e, 11);
                    this.A = yz0.b(order.getInt(5));
                    ((v81) this).g.add(new zq0(0, rk1.a, rk1.b, yz0.a(new JSONObject(), r51.G0, Long.valueOf(this.A)), 1));
                    a(((v81) this).v);
                    return;
                }
                ((v81) this).g.add(new zq0(0, rk1.a, rk1.b, null, 9));
                a(sz0.a(((v81) this).v, null, null, ay0.d, null, null, 27));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void c(eo0 eo0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        ((v81) this).v = sz0.a(((v81) this).v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
        wr1 wr1 = ((v81) this).f;
        if (wr1 != null) {
            wr1.i = true;
        }
        wr1 wr12 = ((v81) this).f;
        if (!(wr12 == null || (jSONObject2 = wr12.m) == null)) {
            yz0.a(jSONObject2, r51.j, yz0.a(ay0.a));
        }
        ay0 ay0 = ((v81) this).v.c;
        ay0 ay02 = ay0.a;
        a(((v81) this).p);
        wr1 wr13 = ((v81) this).f;
        if (!(wr13 == null || (jSONObject = wr13.m) == null)) {
            yz0.a(jSONObject, r51.o3, Integer.valueOf(this.H.c()));
        }
        float min = Math.min((((float) this.H.c()) * 1.0f) / ((float) this.H.c), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.H.c() >= this.H.c) {
            this.D = min;
            a(min);
        }
        b();
    }

    @DexIgnore
    @Override // com.fossil.v81
    public eo0 d() {
        if (!(this.H.b.remaining() > 0)) {
            return null;
        }
        byte[] a = this.H.a();
        if (((v81) this).s) {
            a = ir0.b.b(((v81) this).y.u, this.H.f, a);
        }
        return new ze1(this.H.f, a, ((v81) this).y.w);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void f() {
        i();
        j();
        k();
        vl1 vl1 = new vl1(this, this.E);
        this.F = vl1;
        if (vl1 != null) {
            c().postDelayed(vl1, this.C);
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.o3, Integer.valueOf(this.H.c)), r51.n3, Integer.valueOf(((qz0) this.H).g));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.m3, Long.valueOf(this.A));
    }

    @DexIgnore
    public final void k() {
        vl1 vl1 = this.F;
        if (vl1 != null) {
            ((v81) this).j.removeCallbacks(vl1);
        }
        vl1 vl12 = this.F;
        if (vl12 != null) {
            ((dp0) vl12).a = true;
        }
        this.F = null;
    }
}
