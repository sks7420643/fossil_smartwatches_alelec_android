package com.fossil;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zd2 {
    @DexIgnore
    public static zd2 b;
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> a; // = new n4();

    @DexIgnore
    public zd2(Context context) {
    }

    @DexIgnore
    public static zd2 a(Context context) {
        zd2 zd2;
        synchronized (zd2.class) {
            if (b == null) {
                b = new zd2(context.getApplicationContext());
            }
            zd2 = b;
        }
        return zd2;
    }

    @DexIgnore
    public final synchronized void b(String str, String str2) {
        Map<String, Boolean> map = this.a.get(str2);
        if (map != null) {
            if ((map.remove(str) != null) && map.isEmpty()) {
                this.a.remove(str2);
            }
        }
    }

    @DexIgnore
    public final synchronized boolean c(String str, String str2) {
        Map<String, Boolean> map = this.a.get(str2);
        if (map == null) {
            return false;
        }
        Boolean bool = map.get(str);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final synchronized boolean a(String str, String str2) {
        Map<String, Boolean> map = this.a.get(str2);
        if (map == null) {
            map = new n4<>();
            this.a.put(str2, map);
        }
        if (map.put(str, false) == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final synchronized boolean a(String str) {
        return this.a.containsKey(str);
    }
}
