package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qo {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public qo(String str, int i) {
        this.a = str;
        this.b = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qo)) {
            return false;
        }
        qo qoVar = (qo) obj;
        if (this.b != qoVar.b) {
            return false;
        }
        return this.a.equals(qoVar.a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }
}
