package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k81 extends fe7 implements gd7<eu0, Boolean> {
    @DexIgnore
    public static /* final */ k81 a; // = new k81();

    @DexIgnore
    public k81() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public Boolean invoke(eu0 eu0) {
        eu0 eu02 = eu0;
        is0 is0 = eu02.b;
        return Boolean.valueOf(is0 == is0.INTERRUPTED || is0 == is0.CONNECTION_DROPPED || is0 == is0.BLUETOOTH_OFF || eu02.c.d.c.a == z51.GATT_NULL);
    }
}
