package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol5 extends ll5 {
    @DexIgnore
    public String d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ol5(String str, String str2, String str3) {
        super(str, str2);
        ee7.b(str, "tagName");
        ee7.b(str2, "title");
        ee7.b(str3, "text");
        this.d = str3;
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "<set-?>");
        this.d = str;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }
}
