package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw6 {
    @DexIgnore
    public static yw6 f;
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public ch5 b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public GuestApiService d;
    @DexIgnore
    public FirmwareFileRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(yw6 yw6) {
            yw6.f = yw6;
        }

        @DexIgnore
        public final yw6 b() {
            return yw6.f;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final synchronized yw6 a() {
            yw6 b;
            if (yw6.h.b() == null) {
                yw6.h.a(new yw6(null));
            }
            b = yw6.h.b();
            if (b == null) {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.util.DeviceUtils");
            }
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {57, 79, 84}, m = "downloadActiveDeviceFirmware")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yw6 yw6, fb7 fb7) {
            super(fb7);
            this.this$0 = yw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2", f = "DeviceUtils.kt", l = {100}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ zi5 $repoResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ yw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends fe7 implements gd7<Firmware, hj7<? extends Boolean>> {
            @DexIgnore
            public /* final */ /* synthetic */ yi7 $this_withContext;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yw6$c$a$a")
            @tb7(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1", f = "DeviceUtils.kt", l = {96}, m = "invokeSuspend")
            /* renamed from: com.fossil.yw6$c$a$a  reason: collision with other inner class name */
            public static final class C0258a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Firmware $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0258a(a aVar, Firmware firmware, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$it = firmware;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0258a aVar = new C0258a(this.this$0, this.$it, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                    return ((C0258a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        yw6 yw6 = this.this$0.this$0.this$0;
                        Firmware firmware = this.$it;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = yw6.a(firmware, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, yi7 yi7) {
                super(1);
                this.this$0 = cVar;
                this.$this_withContext = yi7;
            }

            @DexIgnore
            public final hj7<Boolean> invoke(Firmware firmware) {
                ee7.b(firmware, "it");
                return xh7.a(this.$this_withContext, null, null, new C0258a(this, firmware, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(yw6 yw6, zi5 zi5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = yw6;
            this.$repoResponse = zi5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$repoResponse, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x009e  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00f3  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 1
                if (r1 == 0) goto L_0x0041
                if (r1 != r2) goto L_0x0039
                java.lang.Object r1 = r12.L$8
                com.fossil.oe7 r1 = (com.fossil.oe7) r1
                java.lang.Object r3 = r12.L$7
                com.fossil.hj7 r3 = (com.fossil.hj7) r3
                java.lang.Object r3 = r12.L$5
                java.util.Iterator r3 = (java.util.Iterator) r3
                java.lang.Object r4 = r12.L$4
                com.fossil.hg7 r4 = (com.fossil.hg7) r4
                java.lang.Object r5 = r12.L$3
                com.fossil.oe7 r5 = (com.fossil.oe7) r5
                java.lang.Object r6 = r12.L$2
                com.fossil.ai5 r6 = (com.fossil.ai5) r6
                java.lang.Object r7 = r12.L$1
                java.util.List r7 = (java.util.List) r7
                java.lang.Object r8 = r12.L$0
                com.fossil.yi7 r8 = (com.fossil.yi7) r8
                com.fossil.t87.a(r13)
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r4
                r4 = r3
                r3 = r1
                r1 = r0
                r0 = r12
                goto L_0x00ce
            L_0x0039:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0041:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r13 = r12.p$
                com.fossil.zi5 r1 = r12.$repoResponse
                com.fossil.bj5 r1 = (com.fossil.bj5) r1
                java.lang.Object r1 = r1.a()
                com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
                if (r1 == 0) goto L_0x0057
                java.util.List r1 = r1.get_items()
                goto L_0x0058
            L_0x0057:
                r1 = 0
            L_0x0058:
                if (r1 == 0) goto L_0x0106
                com.fossil.ah5$a r3 = com.fossil.ah5.p
                com.fossil.ah5 r3 = r3.a()
                com.fossil.ai5 r3 = r3.d()
                com.fossil.oe7 r4 = new com.fossil.oe7
                r4.<init>()
                r4.element = r2
                java.util.Iterator r5 = r1.iterator()
            L_0x006f:
                boolean r6 = r5.hasNext()
                if (r6 == 0) goto L_0x007f
                java.lang.Object r6 = r5.next()
                com.portfolio.platform.data.model.Firmware r6 = (com.portfolio.platform.data.model.Firmware) r6
                r3.a(r6)
                goto L_0x006f
            L_0x007f:
                com.fossil.hg7 r5 = com.fossil.ea7.b(r1)
                com.fossil.yw6$c$a r6 = new com.fossil.yw6$c$a
                r6.<init>(r12, r13)
                com.fossil.hg7 r5 = com.fossil.og7.c(r5, r6)
                java.util.Iterator r6 = r5.iterator()
                r8 = r13
                r7 = r1
                r1 = r4
                r4 = r5
                r13 = r12
                r11 = r6
                r6 = r3
                r3 = r11
            L_0x0098:
                boolean r5 = r3.hasNext()
                if (r5 == 0) goto L_0x00f3
                java.lang.Object r5 = r3.next()
                r9 = r5
                com.fossil.hj7 r9 = (com.fossil.hj7) r9
                boolean r10 = r1.element
                if (r10 == 0) goto L_0x00e1
                r13.L$0 = r8
                r13.L$1 = r7
                r13.L$2 = r6
                r13.L$3 = r1
                r13.L$4 = r4
                r13.L$5 = r3
                r13.L$6 = r5
                r13.L$7 = r9
                r13.L$8 = r1
                r13.label = r2
                java.lang.Object r5 = r9.c(r13)
                if (r5 != r0) goto L_0x00c4
                return r0
            L_0x00c4:
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r1
                r1 = r0
                r0 = r13
                r13 = r5
                r5 = r4
                r4 = r3
                r3 = r6
            L_0x00ce:
                java.lang.Boolean r13 = (java.lang.Boolean) r13
                boolean r13 = r13.booleanValue()
                if (r13 == 0) goto L_0x00db
                r13 = r0
                r0 = r1
                r1 = r6
                r6 = 1
                goto L_0x00eb
            L_0x00db:
                r13 = r0
                r0 = r1
                r1 = r3
                r3 = r4
                r4 = r5
                goto L_0x00e5
            L_0x00e1:
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r1
            L_0x00e5:
                r5 = 0
                r5 = r4
                r4 = r3
                r3 = r1
                r1 = r6
                r6 = 0
            L_0x00eb:
                r3.element = r6
                r3 = r4
                r4 = r5
                r6 = r7
                r7 = r8
                r8 = r9
                goto L_0x0098
            L_0x00f3:
                boolean r13 = r1.element
                if (r13 != 0) goto L_0x0106
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.String r0 = com.fossil.yw6.g
                java.lang.String r1 = "downloadActiveDeviceFirmware - download detail fw FAILED in one or more parts. Retry later."
                r13.e(r0, r1)
            L_0x0106:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.yw6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$repoResponse$1", f = "DeviceUtils.kt", l = {79}, m = "invokeSuspend")
    public static final class d extends zb7 implements gd7<fb7<? super fv7<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ yw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(yw6 yw6, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = yw6;
            this.$model = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new d(this.this$0, this.$model, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<Firmware>>> fb7) {
            return ((d) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                GuestApiService a2 = this.this$0.a();
                String g = PortfolioApp.g0.c().g();
                String str = this.$model;
                ee7.a((Object) str, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                this.label = 1;
                obj = a2.getFirmwares(g, str, "android", false, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {133}, m = "downloadDetailFirmware")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(yw6 yw6, fb7 fb7) {
            super(fb7);
            this.this$0 = yw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Firmware) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.DeviceUtils$isDeviceDianaEV1Java$1", f = "DeviceUtils.kt", l = {}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ yw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(yw6 yw6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = yw6;
            this.$deviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$deviceSerial, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return pb7.a(this.this$0.a(this.$deviceSerial));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {200}, m = "isLatestFirmware")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(yw6 yw6, fb7 fb7) {
            super(fb7);
            this.this$0 = yw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, this);
        }
    }

    /*
    static {
        String simpleName = yw6.class.getSimpleName();
        ee7.a((Object) simpleName, "DeviceUtils::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public yw6() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final GuestApiService a() {
        GuestApiService guestApiService = this.d;
        if (guestApiService != null) {
            return guestApiService;
        }
        ee7.d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final boolean b(String str) {
        ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        return ((Boolean) wh7.a((ib7) null, new f(this, str, null), 1, (Object) null)).booleanValue();
    }

    @DexIgnore
    public /* synthetic */ yw6(zd7 zd7) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x017e  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0211  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r18) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            boolean r2 = r1 instanceof com.fossil.yw6.b
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.yw6$b r2 = (com.fossil.yw6.b) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.yw6$b r2 = new com.fossil.yw6$b
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 3
            r6 = 2
            r7 = 0
            r8 = 1
            if (r4 == 0) goto L_0x007c
            if (r4 == r8) goto L_0x0074
            if (r4 == r6) goto L_0x0055
            if (r4 != r5) goto L_0x004d
            java.lang.Object r4 = r2.L$5
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r4 = r2.L$4
            java.util.Iterator r4 = (java.util.Iterator) r4
            java.lang.Object r8 = r2.L$3
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r2.L$2
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r2.L$1
            java.util.ArrayList r9 = (java.util.ArrayList) r9
            java.lang.Object r10 = r2.L$0
            com.fossil.yw6 r10 = (com.fossil.yw6) r10
            com.fossil.t87.a(r1)
            goto L_0x01c6
        L_0x004d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0055:
            java.lang.Object r4 = r2.L$4
            java.util.Iterator r4 = (java.util.Iterator) r4
            java.lang.Object r8 = r2.L$3
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r2.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r10 = r2.L$1
            java.util.ArrayList r10 = (java.util.ArrayList) r10
            java.lang.Object r11 = r2.L$0
            com.fossil.yw6 r11 = (com.fossil.yw6) r11
            com.fossil.t87.a(r1)
            r16 = r10
            r10 = r8
            r8 = r9
            r9 = r16
            goto L_0x0178
        L_0x0074:
            java.lang.Object r4 = r2.L$0
            com.fossil.yw6 r4 = (com.fossil.yw6) r4
            com.fossil.t87.a(r1)
            goto L_0x009c
        L_0x007c:
            com.fossil.t87.a(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.fossil.yw6.g
            java.lang.String r9 = "downloadActiveDeviceFirmware"
            r1.d(r4, r9)
            com.portfolio.platform.data.source.UserRepository r1 = r0.c
            if (r1 == 0) goto L_0x0217
            r2.L$0 = r0
            r2.label = r8
            java.lang.Object r1 = r1.getCurrentUser(r2)
            if (r1 != r3) goto L_0x009b
            return r3
        L_0x009b:
            r4 = r0
        L_0x009c:
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            if (r1 == 0) goto L_0x0214
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r9 = r9.c()
            java.lang.String r9 = r9.c()
            int r10 = r9.length()
            if (r10 <= 0) goto L_0x00b7
            r10 = 1
            goto L_0x00b8
        L_0x00b7:
            r10 = 0
        L_0x00b8:
            if (r10 == 0) goto L_0x00fc
            com.fossil.be5$a r10 = com.fossil.be5.o
            java.util.List r10 = r10.c(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.fossil.yw6.g
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "active device skus "
            r13.append(r14)
            r13.append(r10)
            java.lang.String r13 = r13.toString()
            r11.d(r12, r13)
            boolean r11 = r10.isEmpty()
            r8 = r8 ^ r11
            if (r8 == 0) goto L_0x00e7
            r1.addAll(r10)
            goto L_0x0110
        L_0x00e7:
            com.fossil.be5$a r8 = com.fossil.be5.o
            java.lang.String[] r8 = r8.b()
            int r10 = r8.length
            java.lang.Object[] r8 = java.util.Arrays.copyOf(r8, r10)
            java.lang.String[] r8 = (java.lang.String[]) r8
            java.util.List r8 = java.util.Arrays.asList(r8)
            r1.addAll(r8)
            goto L_0x0110
        L_0x00fc:
            com.fossil.be5$a r8 = com.fossil.be5.o
            java.lang.String[] r8 = r8.b()
            int r10 = r8.length
            java.lang.Object[] r8 = java.util.Arrays.copyOf(r8, r10)
            java.lang.String[] r8 = (java.lang.String[]) r8
            java.util.List r8 = java.util.Arrays.asList(r8)
            r1.addAll(r8)
        L_0x0110:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r10 = com.fossil.yw6.g
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "download firmware for models "
            r11.append(r12)
            r11.append(r1)
            java.lang.String r11 = r11.toString()
            r8.d(r10, r11)
            java.util.Iterator r8 = r1.iterator()
        L_0x0130:
            boolean r10 = r8.hasNext()
            if (r10 == 0) goto L_0x0211
            java.lang.Object r10 = r8.next()
            java.lang.String r10 = (java.lang.String) r10
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.fossil.yw6.g
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "downloadActiveDeviceFirmware - get latest fw by model="
            r13.append(r14)
            r13.append(r10)
            java.lang.String r13 = r13.toString()
            r11.d(r12, r13)
            com.fossil.yw6$d r11 = new com.fossil.yw6$d
            r11.<init>(r4, r10, r7)
            r2.L$0 = r4
            r2.L$1 = r1
            r2.L$2 = r9
            r2.L$3 = r10
            r2.L$4 = r8
            r2.label = r6
            java.lang.Object r11 = com.fossil.aj5.a(r11, r2)
            if (r11 != r3) goto L_0x0170
            return r3
        L_0x0170:
            r16 = r9
            r9 = r1
            r1 = r11
            r11 = r4
            r4 = r8
            r8 = r16
        L_0x0178:
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            boolean r12 = r1 instanceof com.fossil.bj5
            if (r12 == 0) goto L_0x01cb
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = com.fossil.yw6.g
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "downloadActiveDeviceFirmware - get latest fw SUCCESS isFromCache "
            r14.append(r15)
            r15 = r1
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            boolean r6 = r15.b()
            r14.append(r6)
            java.lang.String r6 = r14.toString()
            r12.d(r13, r6)
            boolean r6 = r15.b()
            if (r6 != 0) goto L_0x020a
            com.fossil.ti7 r6 = com.fossil.qj7.b()
            com.fossil.yw6$c r12 = new com.fossil.yw6$c
            r12.<init>(r11, r1, r7)
            r2.L$0 = r11
            r2.L$1 = r9
            r2.L$2 = r8
            r2.L$3 = r10
            r2.L$4 = r4
            r2.L$5 = r1
            r2.label = r5
            java.lang.Object r1 = com.fossil.vh7.a(r6, r12, r2)
            if (r1 != r3) goto L_0x01c5
            return r3
        L_0x01c5:
            r10 = r11
        L_0x01c6:
            r1 = r9
            r9 = r8
            r8 = r4
            r4 = r10
            goto L_0x020e
        L_0x01cb:
            boolean r6 = r1 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x020a
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r10 = com.fossil.yw6.g
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "downloadActiveDeviceFirmware - get latest firmwares FAILED!!! {code="
            r12.append(r13)
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            int r13 = r1.a()
            r12.append(r13)
            java.lang.String r13 = ", message="
            r12.append(r13)
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x01fa
            java.lang.String r1 = r1.getMessage()
            goto L_0x01fb
        L_0x01fa:
            r1 = r7
        L_0x01fb:
            r12.append(r1)
            r1 = 125(0x7d, float:1.75E-43)
            r12.append(r1)
            java.lang.String r1 = r12.toString()
            r6.e(r10, r1)
        L_0x020a:
            r1 = r9
            r9 = r8
            r8 = r4
            r4 = r11
        L_0x020e:
            r6 = 2
            goto L_0x0130
        L_0x0211:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0214:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0217:
            java.lang.String r1 = "mUserRepository"
            com.fossil.ee7.d(r1)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yw6.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.portfolio.platform.data.model.Firmware r12, com.fossil.fb7<? super java.lang.Boolean> r13) {
        /*
            r11 = this;
            boolean r0 = r13 instanceof com.fossil.yw6.e
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.yw6$e r0 = (com.fossil.yw6.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.yw6$e r0 = new com.fossil.yw6$e
            r0.<init>(r11, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r12 = r0.L$3
            com.portfolio.platform.data.model.Firmware r12 = (com.portfolio.platform.data.model.Firmware) r12
            java.lang.Object r12 = r0.L$2
            com.fossil.ai5 r12 = (com.fossil.ai5) r12
            java.lang.Object r1 = r0.L$1
            com.portfolio.platform.data.model.Firmware r1 = (com.portfolio.platform.data.model.Firmware) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.yw6 r0 = (com.fossil.yw6) r0
            com.fossil.t87.a(r13)
            goto L_0x0142
        L_0x003a:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x0042:
            com.fossil.t87.a(r13)
            com.fossil.ah5$a r13 = com.fossil.ah5.p
            com.fossil.ah5 r13 = r13.a()
            com.fossil.ai5 r13 = r13.d()
            java.lang.String r2 = r12.getDeviceModel()
            com.portfolio.platform.data.model.Firmware r2 = r13.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.yw6.g
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "downloadDetailFirmware - latestFw="
            r6.append(r7)
            r6.append(r12)
            java.lang.String r7 = ", localFw="
            r6.append(r7)
            r6.append(r2)
            java.lang.String r7 = ", from URL="
            r6.append(r7)
            java.lang.String r7 = r12.getDownloadUrl()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r4.d(r5, r6)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r5 = r4.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r6 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r7 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            java.lang.String r8 = r4.c()
            java.lang.String r9 = com.fossil.yw6.g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r10 = "[FW Download] START DL detail FW of "
            r4.append(r10)
            java.lang.String r10 = r12.getVersionNumber()
            r4.append(r10)
            java.lang.String r10 = r4.toString()
            r5.i(r6, r7, r8, r9, r10)
            r4 = 0
            java.lang.String r5 = "mFirmwareFileRepository"
            if (r2 == 0) goto L_0x010f
            java.lang.String r6 = r2.getVersionNumber()
            java.lang.String r7 = r12.getVersionNumber()
            boolean r6 = com.fossil.mh7.b(r6, r7, r3)
            if (r6 == 0) goto L_0x010f
            com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r6 = r11.e
            if (r6 == 0) goto L_0x010b
            java.lang.String r7 = r2.getVersionNumber()
            java.lang.String r8 = "localFirmware.versionNumber"
            com.fossil.ee7.a(r7, r8)
            java.lang.String r8 = r2.getChecksum()
            java.lang.String r9 = "localFirmware.checksum"
            com.fossil.ee7.a(r8, r9)
            boolean r6 = r6.isDownloaded(r7, r8)
            if (r6 != 0) goto L_0x00e5
            goto L_0x010f
        L_0x00e5:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = com.fossil.yw6.g
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Local fw of model="
            r0.append(r1)
            java.lang.String r1 = r2.getDeviceModel()
            r0.append(r1)
            java.lang.String r1 = " is already latest, no need to re-download"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r12.d(r13, r0)
            goto L_0x0179
        L_0x010b:
            com.fossil.ee7.d(r5)
            throw r4
        L_0x010f:
            com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r6 = r11.e
            if (r6 == 0) goto L_0x017e
            java.lang.String r4 = r12.getVersionNumber()
            java.lang.String r5 = "latestFirmware.versionNumber"
            com.fossil.ee7.a(r4, r5)
            java.lang.String r5 = r12.getDownloadUrl()
            java.lang.String r7 = "latestFirmware.downloadUrl"
            com.fossil.ee7.a(r5, r7)
            java.lang.String r7 = r12.getChecksum()
            java.lang.String r8 = "latestFirmware.checksum"
            com.fossil.ee7.a(r7, r8)
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r13
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r0 = r6.downloadFirmware(r4, r5, r7, r0)
            if (r0 != r1) goto L_0x013f
            return r1
        L_0x013f:
            r1 = r12
            r12 = r13
            r13 = r0
        L_0x0142:
            java.io.File r13 = (java.io.File) r13
            if (r13 == 0) goto L_0x0178
            r12.a(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r4 = r12.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r5 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r6 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            com.portfolio.platform.PortfolioApp$a r12 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r12 = r12.c()
            java.lang.String r7 = r12.c()
            java.lang.String r8 = com.fossil.yw6.g
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "[FW Download] DONE DL detail FW of "
            r12.append(r13)
            java.lang.String r13 = r1.getVersionNumber()
            r12.append(r13)
            java.lang.String r9 = r12.toString()
            r4.i(r5, r6, r7, r8, r9)
            goto L_0x0179
        L_0x0178:
            r3 = 0
        L_0x0179:
            java.lang.Boolean r12 = com.fossil.pb7.a(r3)
            return r12
        L_0x017e:
            com.fossil.ee7.d(r5)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yw6.a(com.portfolio.platform.data.model.Firmware, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final boolean a(String str) {
        String firmwareRevision;
        ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
            return false;
        }
        DeviceRepository deviceRepository = this.a;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
            if (deviceBySerial == null || (firmwareRevision = deviceBySerial.getFirmwareRevision()) == null) {
                return false;
            }
            return mh7.c(firmwareRevision, "DN0.0.0.", true);
        }
        ee7.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r19, com.portfolio.platform.data.model.Device r20, com.fossil.fb7<? super java.lang.Boolean> r21) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            boolean r4 = r3 instanceof com.fossil.yw6.g
            if (r4 == 0) goto L_0x001b
            r4 = r3
            com.fossil.yw6$g r4 = (com.fossil.yw6.g) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.fossil.yw6$g r4 = new com.fossil.yw6$g
            r4.<init>(r0, r3)
        L_0x0020:
            java.lang.Object r3 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            r7 = 1
            if (r6 == 0) goto L_0x0052
            if (r6 != r7) goto L_0x004a
            java.lang.Object r1 = r4.L$5
            com.portfolio.platform.data.model.Firmware r1 = (com.portfolio.platform.data.model.Firmware) r1
            java.lang.Object r1 = r4.L$4
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r4.L$3
            com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
            java.lang.Object r1 = r4.L$2
            com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r4.L$0
            com.fossil.yw6 r1 = (com.fossil.yw6) r1
            com.fossil.t87.a(r3)
            goto L_0x0195
        L_0x004a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0052:
            com.fossil.t87.a(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r6 = com.fossil.yw6.g
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "isLatestFirmware - deviceSerial="
            r8.append(r9)
            r8.append(r1)
            java.lang.String r9 = ", activeDevice="
            r8.append(r9)
            r8.append(r2)
            java.lang.String r9 = ", isSkipOTA="
            r8.append(r9)
            com.fossil.ch5 r9 = r0.b
            java.lang.String r10 = "mSharePrefs"
            r11 = 0
            if (r9 == 0) goto L_0x01d3
            boolean r9 = r9.a0()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r3.d(r6, r8)
            com.fossil.ch5 r3 = r0.b
            if (r3 == 0) goto L_0x01cf
            boolean r3 = r3.a0()
            if (r3 == 0) goto L_0x009b
            java.lang.Boolean r1 = com.fossil.pb7.a(r7)
            return r1
        L_0x009b:
            if (r2 != 0) goto L_0x00ac
            com.portfolio.platform.data.source.DeviceRepository r3 = r0.a
            if (r3 == 0) goto L_0x00a6
            com.portfolio.platform.data.model.Device r3 = r3.getDeviceBySerial(r1)
            goto L_0x00ad
        L_0x00a6:
            java.lang.String r1 = "mDeviceRepository"
            com.fossil.ee7.d(r1)
            throw r11
        L_0x00ac:
            r3 = r2
        L_0x00ad:
            if (r3 != 0) goto L_0x00b4
            java.lang.Boolean r1 = com.fossil.pb7.a(r7)
            return r1
        L_0x00b4:
            java.lang.String r6 = r3.getFirmwareRevision()
            java.lang.String r8 = "release"
            boolean r8 = com.fossil.mh7.b(r8, r8, r7)
            if (r8 != 0) goto L_0x00f4
            com.fossil.ch5 r8 = r0.b
            if (r8 == 0) goto L_0x00f0
            boolean r8 = r8.H()
            if (r8 != 0) goto L_0x00cb
            goto L_0x00f4
        L_0x00cb:
            com.fossil.ch5 r8 = r0.b
            if (r8 == 0) goto L_0x00ec
            java.lang.String r9 = r3.getSku()
            com.portfolio.platform.data.model.Firmware r8 = r8.a(r9)
            if (r8 != 0) goto L_0x0142
            com.fossil.ah5$a r8 = com.fossil.ah5.p
            com.fossil.ah5 r8 = r8.a()
            com.fossil.ai5 r8 = r8.d()
            java.lang.String r9 = r3.getSku()
            com.portfolio.platform.data.model.Firmware r8 = r8.a(r9)
            goto L_0x0142
        L_0x00ec:
            com.fossil.ee7.d(r10)
            throw r11
        L_0x00f0:
            com.fossil.ee7.d(r10)
            throw r11
        L_0x00f4:
            com.fossil.ah5$a r8 = com.fossil.ah5.p
            com.fossil.ah5 r8 = r8.a()
            com.fossil.ai5 r8 = r8.d()
            java.lang.String r9 = r3.getSku()
            com.portfolio.platform.data.model.Firmware r8 = r8.a(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r12 = r9.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r13 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r14 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r9 = r9.c()
            java.lang.String r15 = r9.c()
            java.lang.String r16 = com.fossil.yw6.g
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "[Check FW] Latest FW of SKU "
            r9.append(r10)
            java.lang.String r10 = r3.getSku()
            r9.append(r10)
            java.lang.String r10 = " in DB "
            r9.append(r10)
            if (r8 == 0) goto L_0x0138
            java.lang.String r11 = r8.getVersionNumber()
        L_0x0138:
            r9.append(r11)
            java.lang.String r17 = r9.toString()
            r12.i(r13, r14, r15, r16, r17)
        L_0x0142:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.yw6.g
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "isLatestFirmware - latestFw="
            r11.append(r12)
            r11.append(r8)
            java.lang.String r11 = r11.toString()
            r9.d(r10, r11)
            if (r8 != 0) goto L_0x019a
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.yw6.g
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "isLatestFirmware - Error when update firmware, can't find latest fw of model="
            r11.append(r12)
            java.lang.String r12 = r3.getSku()
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            r9.e(r10, r11)
            r4.L$0 = r0
            r4.L$1 = r1
            r4.L$2 = r2
            r4.L$3 = r3
            r4.L$4 = r6
            r4.L$5 = r8
            r4.label = r7
            java.lang.Object r1 = r0.a(r4)
            if (r1 != r5) goto L_0x0195
            return r5
        L_0x0195:
            java.lang.Boolean r1 = com.fossil.pb7.a(r7)
            return r1
        L_0x019a:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.yw6.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isLatestFirmware - CheckFirmwareActiveDeviceUseCase currentDeviceFw="
            r3.append(r4)
            r3.append(r6)
            java.lang.String r4 = ", latestFw="
            r3.append(r4)
            java.lang.String r4 = r8.getVersionNumber()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            java.lang.String r1 = r8.getVersionNumber()
            boolean r1 = com.fossil.mh7.b(r1, r6, r7)
            java.lang.Boolean r1 = com.fossil.pb7.a(r1)
            return r1
        L_0x01cf:
            com.fossil.ee7.d(r10)
            throw r11
        L_0x01d3:
            com.fossil.ee7.d(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yw6.a(java.lang.String, com.portfolio.platform.data.model.Device, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.r87<java.lang.String, java.lang.String> a(java.lang.String r6, com.portfolio.platform.data.model.Device r7) {
        /*
            r5 = this;
            java.lang.String r0 = "deviceSerial"
            com.fossil.ee7.b(r6, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.yw6.g
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchFirmwaresInfo - deviceSerial="
            r2.append(r3)
            r2.append(r6)
            java.lang.String r3 = ", activeDevice="
            r2.append(r3)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            r0 = 0
            if (r7 != 0) goto L_0x003b
            com.portfolio.platform.data.source.DeviceRepository r7 = r5.a
            if (r7 == 0) goto L_0x0035
            com.portfolio.platform.data.model.Device r7 = r7.getDeviceBySerial(r6)
            goto L_0x003b
        L_0x0035:
            java.lang.String r6 = "mDeviceRepository"
            com.fossil.ee7.d(r6)
            throw r0
        L_0x003b:
            if (r7 != 0) goto L_0x0043
            com.fossil.r87 r6 = new com.fossil.r87
            r6.<init>(r0, r0)
            return r6
        L_0x0043:
            java.lang.String r6 = r7.getFirmwareRevision()
            r1 = 1
            java.lang.String r2 = "release"
            boolean r1 = com.fossil.mh7.b(r2, r2, r1)
            if (r1 != 0) goto L_0x0086
            com.fossil.ch5 r1 = r5.b
            java.lang.String r2 = "mSharePrefs"
            if (r1 == 0) goto L_0x0082
            boolean r1 = r1.H()
            if (r1 != 0) goto L_0x005d
            goto L_0x0086
        L_0x005d:
            com.fossil.ch5 r1 = r5.b
            if (r1 == 0) goto L_0x007e
            java.lang.String r2 = r7.getSku()
            com.portfolio.platform.data.model.Firmware r1 = r1.a(r2)
            if (r1 != 0) goto L_0x0098
            com.fossil.ah5$a r1 = com.fossil.ah5.p
            com.fossil.ah5 r1 = r1.a()
            com.fossil.ai5 r1 = r1.d()
            java.lang.String r7 = r7.getSku()
            com.portfolio.platform.data.model.Firmware r1 = r1.a(r7)
            goto L_0x0098
        L_0x007e:
            com.fossil.ee7.d(r2)
            throw r0
        L_0x0082:
            com.fossil.ee7.d(r2)
            throw r0
        L_0x0086:
            com.fossil.ah5$a r1 = com.fossil.ah5.p
            com.fossil.ah5 r1 = r1.a()
            com.fossil.ai5 r1 = r1.d()
            java.lang.String r7 = r7.getSku()
            com.portfolio.platform.data.model.Firmware r1 = r1.a(r7)
        L_0x0098:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r2 = com.fossil.yw6.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchFirmwaresInfo - latestFw="
            r3.append(r4)
            r3.append(r1)
            java.lang.String r3 = r3.toString()
            r7.d(r2, r3)
            com.fossil.r87 r7 = new com.fossil.r87
            if (r1 == 0) goto L_0x00bc
            java.lang.String r0 = r1.getVersionNumber()
        L_0x00bc:
            r7.<init>(r6, r0)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yw6.a(java.lang.String, com.portfolio.platform.data.model.Device):com.fossil.r87");
    }
}
