package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.nc;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class zb implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<zb> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ ArrayList<String> b;
    @DexIgnore
    public /* final */ int[] c;
    @DexIgnore
    public /* final */ int[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ CharSequence i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ CharSequence p;
    @DexIgnore
    public /* final */ ArrayList<String> q;
    @DexIgnore
    public /* final */ ArrayList<String> r;
    @DexIgnore
    public /* final */ boolean s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<zb> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public zb createFromParcel(Parcel parcel) {
            return new zb(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public zb[] newArray(int i) {
            return new zb[i];
        }
    }

    @DexIgnore
    public zb(yb ybVar) {
        int size = ((nc) ybVar).a.size();
        this.a = new int[(size * 5)];
        if (((nc) ybVar).g) {
            this.b = new ArrayList<>(size);
            this.c = new int[size];
            this.d = new int[size];
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                nc.a aVar = ((nc) ybVar).a.get(i2);
                int i4 = i3 + 1;
                this.a[i3] = aVar.a;
                ArrayList<String> arrayList = this.b;
                Fragment fragment = aVar.b;
                arrayList.add(fragment != null ? fragment.mWho : null);
                int[] iArr = this.a;
                int i5 = i4 + 1;
                iArr[i4] = aVar.c;
                int i6 = i5 + 1;
                iArr[i5] = aVar.d;
                int i7 = i6 + 1;
                iArr[i6] = aVar.e;
                iArr[i7] = aVar.f;
                this.c[i2] = aVar.g.ordinal();
                this.d[i2] = aVar.h.ordinal();
                i2++;
                i3 = i7 + 1;
            }
            this.e = ((nc) ybVar).f;
            this.f = ((nc) ybVar).i;
            this.g = ybVar.t;
            this.h = ((nc) ybVar).j;
            this.i = ((nc) ybVar).k;
            this.j = ((nc) ybVar).l;
            this.p = ((nc) ybVar).m;
            this.q = ((nc) ybVar).n;
            this.r = ((nc) ybVar).o;
            this.s = ((nc) ybVar).p;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    @DexIgnore
    public yb a(FragmentManager fragmentManager) {
        yb ybVar = new yb(fragmentManager);
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.a.length) {
            nc.a aVar = new nc.a();
            int i4 = i2 + 1;
            aVar.a = this.a[i2];
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Instantiate " + ybVar + " op #" + i3 + " base fragment #" + this.a[i4]);
            }
            String str = this.b.get(i3);
            if (str != null) {
                aVar.b = fragmentManager.a(str);
            } else {
                aVar.b = null;
            }
            aVar.g = Lifecycle.State.values()[this.c[i3]];
            aVar.h = Lifecycle.State.values()[this.d[i3]];
            int[] iArr = this.a;
            int i5 = i4 + 1;
            int i6 = iArr[i4];
            aVar.c = i6;
            int i7 = i5 + 1;
            int i8 = iArr[i5];
            aVar.d = i8;
            int i9 = i7 + 1;
            int i10 = iArr[i7];
            aVar.e = i10;
            int i11 = iArr[i9];
            aVar.f = i11;
            ((nc) ybVar).b = i6;
            ((nc) ybVar).c = i8;
            ((nc) ybVar).d = i10;
            ((nc) ybVar).e = i11;
            ybVar.a(aVar);
            i3++;
            i2 = i9 + 1;
        }
        ((nc) ybVar).f = this.e;
        ((nc) ybVar).i = this.f;
        ybVar.t = this.g;
        ((nc) ybVar).g = true;
        ((nc) ybVar).j = this.h;
        ((nc) ybVar).k = this.i;
        ((nc) ybVar).l = this.j;
        ((nc) ybVar).m = this.p;
        ((nc) ybVar).n = this.q;
        ((nc) ybVar).o = this.r;
        ((nc) ybVar).p = this.s;
        ybVar.a(1);
        return ybVar;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.a);
        parcel.writeStringList(this.b);
        parcel.writeIntArray(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeInt(this.j);
        TextUtils.writeToParcel(this.p, parcel, 0);
        parcel.writeStringList(this.q);
        parcel.writeStringList(this.r);
        parcel.writeInt(this.s ? 1 : 0);
    }

    @DexIgnore
    public zb(Parcel parcel) {
        this.a = parcel.createIntArray();
        this.b = parcel.createStringArrayList();
        this.c = parcel.createIntArray();
        this.d = parcel.createIntArray();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.readInt();
        this.p = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.q = parcel.createStringArrayList();
        this.r = parcel.createStringArrayList();
        this.s = parcel.readInt() != 0;
    }
}
