package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m67 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ Throwable b;

    @DexIgnore
    public m67(Context context, Throwable th) {
        this.a = context;
        this.b = th;
    }

    @DexIgnore
    public final void run() {
        try {
            if (w37.s()) {
                new t47(new d47(this.a, z37.a(this.a, false, (a47) null), 99, this.b, g47.m)).a();
            }
        } catch (Throwable th) {
            k57 f = z37.m;
            f.c("reportSdkSelfException error: " + th);
        }
    }
}
