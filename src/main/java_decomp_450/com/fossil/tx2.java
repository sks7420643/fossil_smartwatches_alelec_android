package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tx2 {
    @DexIgnore
    public static /* final */ rx2 a; // = c();
    @DexIgnore
    public static /* final */ rx2 b; // = new ux2();

    @DexIgnore
    public static rx2 a() {
        return a;
    }

    @DexIgnore
    public static rx2 b() {
        return b;
    }

    @DexIgnore
    public static rx2 c() {
        try {
            return (rx2) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
