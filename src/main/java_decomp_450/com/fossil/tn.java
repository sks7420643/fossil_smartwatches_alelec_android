package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tn extends vn<Boolean> {
    @DexIgnore
    public tn(Context context, vp vpVar) {
        super(ho.a(context, vpVar).a());
    }

    @DexIgnore
    @Override // com.fossil.vn
    public boolean a(zo zoVar) {
        return zoVar.j.g();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
