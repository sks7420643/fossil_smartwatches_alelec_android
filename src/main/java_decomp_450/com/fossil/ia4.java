package com.fossil;

import android.content.Intent;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ia4 implements Callable {
    @DexIgnore
    public /* final */ Intent a;

    @DexIgnore
    public ia4(Intent intent) {
        this.a = intent;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Integer.valueOf(ja4.c(this.a));
    }
}
