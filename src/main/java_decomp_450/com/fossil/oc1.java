package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc1 extends ey0 {
    @DexIgnore
    public int L; // = 20;
    @DexIgnore
    public /* final */ boolean M;

    @DexIgnore
    public oc1(ri1 ri1) {
        super(uh0.j, qa1.g, ri1, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 2) {
            int b = yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            this.L = b;
            yz0.a(jSONObject, r51.I0, Integer.valueOf(b));
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.a, null, null, 27);
        } else {
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.j, null, null, 27);
        }
        ((uh1) this).E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.I0, Integer.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean p() {
        return this.M;
    }
}
