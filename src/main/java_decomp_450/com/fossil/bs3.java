package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import android.util.Property;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bs3 {
    @DexIgnore
    public /* final */ SimpleArrayMap<String, cs3> a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, PropertyValuesHolder[]> b; // = new SimpleArrayMap<>();

    @DexIgnore
    public void a(String str, cs3 cs3) {
        this.a.put(str, cs3);
    }

    @DexIgnore
    public cs3 b(String str) {
        if (d(str)) {
            return this.a.get(str);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public boolean c(String str) {
        return this.b.get(str) != null;
    }

    @DexIgnore
    public boolean d(String str) {
        return this.a.get(str) != null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bs3)) {
            return false;
        }
        return this.a.equals(((bs3) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return '\n' + bs3.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " timings: " + this.a + "}\n";
    }

    @DexIgnore
    public PropertyValuesHolder[] a(String str) {
        if (c(str)) {
            return a(this.b.get(str));
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public void a(String str, PropertyValuesHolder[] propertyValuesHolderArr) {
        this.b.put(str, propertyValuesHolderArr);
    }

    @DexIgnore
    public final PropertyValuesHolder[] a(PropertyValuesHolder[] propertyValuesHolderArr) {
        PropertyValuesHolder[] propertyValuesHolderArr2 = new PropertyValuesHolder[propertyValuesHolderArr.length];
        for (int i = 0; i < propertyValuesHolderArr.length; i++) {
            propertyValuesHolderArr2[i] = propertyValuesHolderArr[i].clone();
        }
        return propertyValuesHolderArr2;
    }

    @DexIgnore
    public <T> ObjectAnimator a(String str, T t, Property<T, ?> property) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(t, a(str));
        ofPropertyValuesHolder.setProperty(property);
        b(str).a((Animator) ofPropertyValuesHolder);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public long a() {
        int size = this.a.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            cs3 e = this.a.e(i);
            j = Math.max(j, e.a() + e.b());
        }
        return j;
    }

    @DexIgnore
    public static bs3 a(Context context, TypedArray typedArray, int i) {
        int resourceId;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0) {
            return null;
        }
        return a(context, resourceId);
    }

    @DexIgnore
    public static bs3 a(Context context, int i) {
        try {
            Animator loadAnimator = AnimatorInflater.loadAnimator(context, i);
            if (loadAnimator instanceof AnimatorSet) {
                return a(((AnimatorSet) loadAnimator).getChildAnimations());
            }
            if (loadAnimator == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(loadAnimator);
            return a(arrayList);
        } catch (Exception e) {
            Log.w("MotionSpec", "Can't load animation resource ID #0x" + Integer.toHexString(i), e);
            return null;
        }
    }

    @DexIgnore
    public static bs3 a(List<Animator> list) {
        bs3 bs3 = new bs3();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(bs3, list.get(i));
        }
        return bs3;
    }

    @DexIgnore
    public static void a(bs3 bs3, Animator animator) {
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            bs3.a(objectAnimator.getPropertyName(), objectAnimator.getValues());
            bs3.a(objectAnimator.getPropertyName(), cs3.a((ValueAnimator) objectAnimator));
            return;
        }
        throw new IllegalArgumentException("Animator must be an ObjectAnimator: " + animator);
    }
}
