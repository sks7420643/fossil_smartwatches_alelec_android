package com.fossil;

import java.io.IOException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu2 {
    @DexIgnore
    public static int a(byte[] bArr, int i, ou2 ou2) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return a(b, bArr, i2, ou2);
        }
        ou2.a = b;
        return i2;
    }

    @DexIgnore
    public static int b(byte[] bArr, int i, ou2 ou2) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            ou2.b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b2 & Byte.MAX_VALUE)) << i4;
            b = b2;
            i3 = i5;
        }
        ou2.b = j2;
        return i3;
    }

    @DexIgnore
    public static double c(byte[] bArr, int i) {
        return Double.longBitsToDouble(b(bArr, i));
    }

    @DexIgnore
    public static float d(byte[] bArr, int i) {
        return Float.intBitsToFloat(a(bArr, i));
    }

    @DexIgnore
    public static int e(byte[] bArr, int i, ou2 ou2) throws iw2 {
        int a = a(bArr, i, ou2);
        int i2 = ou2.a;
        if (i2 < 0) {
            throw iw2.zzb();
        } else if (i2 > bArr.length - a) {
            throw iw2.zza();
        } else if (i2 == 0) {
            ou2.c = tu2.zza;
            return a;
        } else {
            ou2.c = tu2.zza(bArr, a, i2);
            return a + i2;
        }
    }

    @DexIgnore
    public static int c(byte[] bArr, int i, ou2 ou2) throws iw2 {
        int a = a(bArr, i, ou2);
        int i2 = ou2.a;
        if (i2 < 0) {
            throw iw2.zzb();
        } else if (i2 == 0) {
            ou2.c = "";
            return a;
        } else {
            ou2.c = new String(bArr, a, i2, ew2.a);
            return a + i2;
        }
    }

    @DexIgnore
    public static int d(byte[] bArr, int i, ou2 ou2) throws iw2 {
        int a = a(bArr, i, ou2);
        int i2 = ou2.a;
        if (i2 < 0) {
            throw iw2.zzb();
        } else if (i2 == 0) {
            ou2.c = "";
            return a;
        } else {
            ou2.c = dz2.b(bArr, a, i2);
            return a + i2;
        }
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, ou2 ou2) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            ou2.a = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            ou2.a = i5 | (b2 << DateTimeFieldType.HOUR_OF_HALFDAY);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << DateTimeFieldType.HOUR_OF_HALFDAY);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            ou2.a = i7 | (b3 << DateTimeFieldType.SECOND_OF_MINUTE);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << DateTimeFieldType.SECOND_OF_MINUTE);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            ou2.a = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                ou2.a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    @DexIgnore
    public static long b(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    @DexIgnore
    public static int a(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY);
    }

    @DexIgnore
    public static int a(cy2 cy2, byte[] bArr, int i, int i2, ou2 ou2) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        byte b2 = b;
        if (b < 0) {
            i3 = a(b, bArr, i3, ou2);
            b2 = ou2.a;
        }
        if (b2 < 0 || b2 > i2 - i3) {
            throw iw2.zza();
        }
        Object zza = cy2.zza();
        int i4 = b2 + i3;
        cy2.a(zza, bArr, i3, i4, ou2);
        cy2.zzc(zza);
        ou2.c = zza;
        return i4;
    }

    @DexIgnore
    public static int a(cy2 cy2, byte[] bArr, int i, int i2, int i3, ou2 ou2) throws IOException {
        nx2 nx2 = (nx2) cy2;
        Object zza = nx2.zza();
        int a = nx2.a(zza, bArr, i, i2, i3, ou2);
        nx2.zzc(zza);
        ou2.c = zza;
        return a;
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, jw2<?> jw2, ou2 ou2) {
        cw2 cw2 = (cw2) jw2;
        int a = a(bArr, i2, ou2);
        cw2.a(ou2.a);
        while (a < i3) {
            int a2 = a(bArr, a, ou2);
            if (i != ou2.a) {
                break;
            }
            a = a(bArr, a2, ou2);
            cw2.a(ou2.a);
        }
        return a;
    }

    @DexIgnore
    public static int a(byte[] bArr, int i, jw2<?> jw2, ou2 ou2) throws IOException {
        cw2 cw2 = (cw2) jw2;
        int a = a(bArr, i, ou2);
        int i2 = ou2.a + a;
        while (a < i2) {
            a = a(bArr, a, ou2);
            cw2.a(ou2.a);
        }
        if (a == i2) {
            return a;
        }
        throw iw2.zza();
    }

    @DexIgnore
    public static int a(cy2<?> cy2, int i, byte[] bArr, int i2, int i3, jw2<?> jw2, ou2 ou2) throws IOException {
        int a = a(cy2, bArr, i2, i3, ou2);
        jw2.add(ou2.c);
        while (a < i3) {
            int a2 = a(bArr, a, ou2);
            if (i != ou2.a) {
                break;
            }
            a = a(cy2, bArr, a2, i3, ou2);
            jw2.add(ou2.c);
        }
        return a;
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, ty2 ty2, ou2 ou2) throws iw2 {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int b = b(bArr, i2, ou2);
                ty2.a(i, Long.valueOf(ou2.b));
                return b;
            } else if (i4 == 1) {
                ty2.a(i, Long.valueOf(b(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int a = a(bArr, i2, ou2);
                int i5 = ou2.a;
                if (i5 < 0) {
                    throw iw2.zzb();
                } else if (i5 <= bArr.length - a) {
                    if (i5 == 0) {
                        ty2.a(i, tu2.zza);
                    } else {
                        ty2.a(i, tu2.zza(bArr, a, i5));
                    }
                    return a + i5;
                } else {
                    throw iw2.zza();
                }
            } else if (i4 == 3) {
                ty2 e = ty2.e();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int a2 = a(bArr, i2, ou2);
                    int i8 = ou2.a;
                    i7 = i8;
                    if (i8 == i6) {
                        i2 = a2;
                        break;
                    }
                    int a3 = a(i7, bArr, a2, i3, e, ou2);
                    i7 = i8;
                    i2 = a3;
                }
                if (i2 > i3 || i7 != i6) {
                    throw iw2.zzg();
                }
                ty2.a(i, e);
                return i2;
            } else if (i4 == 5) {
                ty2.a(i, Integer.valueOf(a(bArr, i2)));
                return i2 + 4;
            } else {
                throw iw2.zzd();
            }
        } else {
            throw iw2.zzd();
        }
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, ou2 ou2) throws iw2 {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return b(bArr, i2, ou2);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return a(bArr, i2, ou2) + ou2.a;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = a(bArr, i2, ou2);
                    i6 = ou2.a;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = a(i6, bArr, i2, i3, ou2);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw iw2.zzg();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw iw2.zzd();
            }
        } else {
            throw iw2.zzd();
        }
    }
}
