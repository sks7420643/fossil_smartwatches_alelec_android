package com.fossil;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz1 {
    @DexIgnore
    public static kz1 d;
    @DexIgnore
    public xy1 a;
    @DexIgnore
    public GoogleSignInAccount b;
    @DexIgnore
    public GoogleSignInOptions c; // = this.a.c();

    @DexIgnore
    public kz1(Context context) {
        xy1 a2 = xy1.a(context);
        this.a = a2;
        this.b = a2.b();
    }

    @DexIgnore
    public static synchronized kz1 a(Context context) {
        kz1 b2;
        synchronized (kz1.class) {
            b2 = b(context.getApplicationContext());
        }
        return b2;
    }

    @DexIgnore
    public static synchronized kz1 b(Context context) {
        kz1 kz1;
        synchronized (kz1.class) {
            if (d == null) {
                d = new kz1(context);
            }
            kz1 = d;
        }
        return kz1;
    }

    @DexIgnore
    public final synchronized void a() {
        this.a.a();
        this.b = null;
    }

    @DexIgnore
    public final synchronized GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public final synchronized void a(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        this.a.a(googleSignInAccount, googleSignInOptions);
        this.b = googleSignInAccount;
        this.c = googleSignInOptions;
    }
}
