package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class zb5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[ac5.values().length];
        a = iArr;
        iArr[ac5.RUNNING.ordinal()] = 1;
        a[ac5.CYCLING.ordinal()] = 2;
        a[ac5.TREADMILL.ordinal()] = 3;
        a[ac5.ELLIPTICAL.ordinal()] = 4;
        a[ac5.WEIGHTS.ordinal()] = 5;
        a[ac5.WORKOUT.ordinal()] = 6;
        a[ac5.WALKING.ordinal()] = 7;
        a[ac5.ROWING.ordinal()] = 8;
        a[ac5.HIKING.ordinal()] = 9;
        a[ac5.YOGA.ordinal()] = 10;
        a[ac5.SWIMMING.ordinal()] = 11;
        a[ac5.AEROBIC.ordinal()] = 12;
        a[ac5.SPINNING.ordinal()] = 13;
    }
    */
}
