package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke7 implements wd7 {
    @DexIgnore
    public /* final */ Class<?> a;

    @DexIgnore
    public ke7(Class<?> cls, String str) {
        ee7.b(cls, "jClass");
        ee7.b(str, "moduleName");
        this.a = cls;
    }

    @DexIgnore
    @Override // com.fossil.wd7
    public Class<?> a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof ke7) && ee7.a(a(), ((ke7) obj).a());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
