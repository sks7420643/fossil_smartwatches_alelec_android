package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r02 {
    @DexIgnore
    public static r02 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public r02(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static r02 a(Context context) {
        a72.a(context);
        synchronized (r02.class) {
            if (b == null) {
                ma2.a(context);
                b = new r02(context);
            }
        }
        return b;
    }

    @DexIgnore
    public boolean a(int i) {
        va2 va2;
        String[] a2 = ja2.b(this.a).a(i);
        if (a2 != null && a2.length != 0) {
            va2 = null;
            for (String str : a2) {
                va2 = a(str, i);
                if (va2.a) {
                    break;
                }
            }
        } else {
            va2 = va2.a("no pkgs");
        }
        va2.b();
        return va2.a;
    }

    @DexIgnore
    public static boolean a(PackageInfo packageInfo, boolean z) {
        na2 na2;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                na2 = a(packageInfo, sa2.a);
            } else {
                na2 = a(packageInfo, sa2.a[0]);
            }
            if (na2 != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        if (a(packageInfo, true)) {
            if (q02.e(this.a)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }

    @DexIgnore
    public final va2 a(String str, int i) {
        try {
            PackageInfo a2 = ja2.b(this.a).a(str, 64, i);
            boolean e = q02.e(this.a);
            if (a2 == null) {
                return va2.a("null pkg");
            }
            if (a2.signatures != null) {
                if (a2.signatures.length == 1) {
                    qa2 qa2 = new qa2(a2.signatures[0].toByteArray());
                    String str2 = a2.packageName;
                    va2 a3 = ma2.a(str2, qa2, e, false);
                    return (!a3.a || a2.applicationInfo == null || (a2.applicationInfo.flags & 2) == 0 || !ma2.a(str2, qa2, false, true).a) ? a3 : va2.a("debuggable release cert app rejected");
                }
            }
            return va2.a("single cert required");
        } catch (PackageManager.NameNotFoundException unused) {
            String valueOf = String.valueOf(str);
            return va2.a(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }

    @DexIgnore
    public static na2 a(PackageInfo packageInfo, na2... na2Arr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        qa2 qa2 = new qa2(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < na2Arr.length; i++) {
            if (na2Arr[i].equals(qa2)) {
                return na2Arr[i];
            }
        }
        return null;
    }
}
