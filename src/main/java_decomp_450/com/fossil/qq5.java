package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq5 extends fl4<c, d, b> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public c d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ AlarmsRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qq5.i;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ Alarm a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public b(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            ee7.b(arrayList, "mBLEErrorCodes");
            this.a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public c(String str, List<Alarm> list, Alarm alarm) {
            ee7.b(str, "deviceId");
            ee7.b(list, "alarms");
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ Alarm a;

        @DexIgnore
        public d(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1", f = "SetAlarms.kt", l = {38, 43, 45, 58}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Intent intent, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$intent, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00e4  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r23) {
                /*
                    r22 = this;
                    r0 = r22
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 4
                    r4 = 3
                    r5 = 2
                    r6 = 1
                    if (r2 == 0) goto L_0x0046
                    if (r2 == r6) goto L_0x003c
                    if (r2 == r5) goto L_0x002f
                    if (r2 == r4) goto L_0x002f
                    if (r2 != r3) goto L_0x0027
                    java.lang.Object r1 = r0.L$2
                    com.portfolio.platform.data.source.local.alarm.Alarm r1 = (com.portfolio.platform.data.source.local.alarm.Alarm) r1
                    java.lang.Object r2 = r0.L$1
                    com.portfolio.platform.data.source.local.alarm.Alarm r2 = (com.portfolio.platform.data.source.local.alarm.Alarm) r2
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r23)
                    goto L_0x0156
                L_0x0027:
                    java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                    java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                    r1.<init>(r2)
                    throw r1
                L_0x002f:
                    java.lang.Object r1 = r0.L$1
                    com.portfolio.platform.data.source.local.alarm.Alarm r1 = (com.portfolio.platform.data.source.local.alarm.Alarm) r1
                    java.lang.Object r1 = r0.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r23)
                    goto L_0x00da
                L_0x003c:
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r23)
                    r6 = r23
                    goto L_0x006e
                L_0x0046:
                    com.fossil.t87.a(r23)
                    com.fossil.yi7 r2 = r0.p$
                    com.fossil.qq5$e r7 = r0.this$0
                    com.fossil.qq5 r7 = com.fossil.qq5.this
                    com.portfolio.platform.data.source.AlarmsRepository r7 = r7.h
                    com.fossil.qq5$e r8 = r0.this$0
                    com.fossil.qq5 r8 = com.fossil.qq5.this
                    com.fossil.qq5$c r8 = r8.e()
                    com.portfolio.platform.data.source.local.alarm.Alarm r8 = r8.a()
                    java.lang.String r8 = r8.getUri()
                    r0.L$0 = r2
                    r0.label = r6
                    java.lang.Object r6 = r7.getAlarmById(r8, r0)
                    if (r6 != r1) goto L_0x006e
                    return r1
                L_0x006e:
                    com.portfolio.platform.data.source.local.alarm.Alarm r6 = (com.portfolio.platform.data.source.local.alarm.Alarm) r6
                    android.content.Intent r7 = r0.$intent
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r8 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r8 = r8.getSERVICE_ACTION_RESULT()
                    r9 = -1
                    int r7 = r7.getIntExtra(r8, r9)
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r8 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED
                    int r8 = r8.ordinal()
                    if (r7 != r8) goto L_0x0103
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                    com.fossil.qq5$a r7 = com.fossil.qq5.j
                    java.lang.String r7 = r7.a()
                    java.lang.String r8 = "onReceive success"
                    r3.d(r7, r8)
                    if (r6 != 0) goto L_0x00b9
                    com.fossil.qq5$e r3 = r0.this$0
                    com.fossil.qq5 r3 = com.fossil.qq5.this
                    com.portfolio.platform.data.source.AlarmsRepository r3 = r3.h
                    com.fossil.qq5$e r4 = r0.this$0
                    com.fossil.qq5 r4 = com.fossil.qq5.this
                    com.fossil.qq5$c r4 = r4.e()
                    com.portfolio.platform.data.source.local.alarm.Alarm r4 = r4.a()
                    r0.L$0 = r2
                    r0.L$1 = r6
                    r0.label = r5
                    java.lang.Object r2 = r3.insertAlarm(r4, r0)
                    if (r2 != r1) goto L_0x00da
                    return r1
                L_0x00b9:
                    com.fossil.qq5$e r3 = r0.this$0
                    com.fossil.qq5 r3 = com.fossil.qq5.this
                    com.portfolio.platform.data.source.AlarmsRepository r3 = r3.h
                    com.fossil.qq5$e r5 = r0.this$0
                    com.fossil.qq5 r5 = com.fossil.qq5.this
                    com.fossil.qq5$c r5 = r5.e()
                    com.portfolio.platform.data.source.local.alarm.Alarm r5 = r5.a()
                    r0.L$0 = r2
                    r0.L$1 = r6
                    r0.label = r4
                    java.lang.Object r2 = r3.updateAlarm(r5, r0)
                    if (r2 != r1) goto L_0x00da
                    return r1
                L_0x00da:
                    com.fossil.qd5$a r1 = com.fossil.qd5.f
                    java.lang.String r2 = "set_alarms"
                    com.fossil.if5 r1 = r1.c(r2)
                    if (r1 == 0) goto L_0x00e9
                    java.lang.String r3 = ""
                    r1.a(r3)
                L_0x00e9:
                    com.fossil.qd5$a r1 = com.fossil.qd5.f
                    r1.e(r2)
                    com.fossil.qq5$e r1 = r0.this$0
                    com.fossil.qq5 r1 = com.fossil.qq5.this
                    com.fossil.qq5$d r2 = new com.fossil.qq5$d
                    com.fossil.qq5$c r3 = r1.e()
                    com.portfolio.platform.data.source.local.alarm.Alarm r3 = r3.a()
                    r2.<init>(r3)
                    r1.a(r2)
                    goto L_0x0160
                L_0x0103:
                    com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                    com.fossil.qq5$a r5 = com.fossil.qq5.j
                    java.lang.String r5 = r5.a()
                    java.lang.String r7 = "onReceive failed"
                    r4.d(r5, r7)
                    com.fossil.qq5$e r4 = r0.this$0
                    com.fossil.qq5 r4 = com.fossil.qq5.this
                    com.fossil.qq5$c r4 = r4.e()
                    com.portfolio.platform.data.source.local.alarm.Alarm r7 = r4.a()
                    r8 = 0
                    r9 = 0
                    r10 = 0
                    r11 = 0
                    r12 = 0
                    r13 = 0
                    r14 = 0
                    r15 = 0
                    r16 = 0
                    r17 = 0
                    r18 = 0
                    r19 = 0
                    r20 = 4095(0xfff, float:5.738E-42)
                    r21 = 0
                    com.portfolio.platform.data.source.local.alarm.Alarm r4 = com.portfolio.platform.data.source.local.alarm.Alarm.copy$default(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
                    if (r6 != 0) goto L_0x0157
                    r5 = 0
                    r4.setActive(r5)
                    com.fossil.qq5$e r5 = r0.this$0
                    com.fossil.qq5 r5 = com.fossil.qq5.this
                    com.portfolio.platform.data.source.AlarmsRepository r5 = r5.h
                    r0.L$0 = r2
                    r0.L$1 = r6
                    r0.L$2 = r4
                    r0.label = r3
                    java.lang.Object r2 = r5.insertAlarm(r4, r0)
                    if (r2 != r1) goto L_0x0155
                    return r1
                L_0x0155:
                    r1 = r4
                L_0x0156:
                    r6 = r1
                L_0x0157:
                    com.fossil.qq5$e r1 = r0.this$0
                    com.fossil.qq5 r1 = com.fossil.qq5.this
                    android.content.Intent r2 = r0.$intent
                    r1.a(r2, r6)
                L_0x0160:
                    com.fossil.i97 r1 = com.fossil.i97.a
                    return r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.qq5.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(qq5.j.a(), "onReceive");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && qq5.this.d()) {
                qq5.this.a(false);
                ik7 unused = xh7.b(qq5.this.b(), null, null, new a(this, intent, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms", f = "SetAlarms.kt", l = {96, 100}, m = "run")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qq5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qq5 qq5, fb7 fb7) {
            super(fb7);
            this.this$0 = qq5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        String simpleName = qq5.class.getSimpleName();
        ee7.a((Object) simpleName, "SetAlarms::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public qq5(PortfolioApp portfolioApp, AlarmsRepository alarmsRepository) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        this.g = portfolioApp;
        this.h = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return i;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public final c e() {
        c cVar = this.d;
        if (cVar != null) {
            return cVar;
        }
        ee7.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void f() {
        nj5.d.a(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void g() {
        nj5.d.b(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void a(Intent intent, Alarm alarm) {
        ee7.b(intent, "intent");
        ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "processBLEErrorCodes() - errorCode=" + intExtra);
        ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
        if (integerArrayListExtra == null) {
            integerArrayListExtra = new ArrayList<>(intExtra);
        }
        if5 c2 = qd5.f.c("set_alarms");
        if (c2 != null) {
            c2.a(String.valueOf(intExtra));
        }
        qd5.f.e("set_alarms");
        a(new b(alarm, intExtra, integerArrayListExtra));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.qq5.c r10, com.fossil.fb7<java.lang.Object> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.fossil.qq5.f
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.qq5$f r0 = (com.fossil.qq5.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qq5$f r0 = new com.fossil.qq5$f
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0055
            if (r2 == r4) goto L_0x0045
            if (r2 != r3) goto L_0x003d
            java.lang.Object r10 = r0.L$3
            com.portfolio.platform.data.source.local.alarm.Alarm r10 = (com.portfolio.platform.data.source.local.alarm.Alarm) r10
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.alarm.Alarm r10 = (com.portfolio.platform.data.source.local.alarm.Alarm) r10
            java.lang.Object r1 = r0.L$1
            com.fossil.qq5$c r1 = (com.fossil.qq5.c) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.qq5 r0 = (com.fossil.qq5) r0
            com.fossil.t87.a(r11)
            goto L_0x00c7
        L_0x003d:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0045:
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.alarm.Alarm r10 = (com.portfolio.platform.data.source.local.alarm.Alarm) r10
            java.lang.Object r2 = r0.L$1
            com.fossil.qq5$c r2 = (com.fossil.qq5.c) r2
            java.lang.Object r5 = r0.L$0
            com.fossil.qq5 r5 = (com.fossil.qq5) r5
            com.fossil.t87.a(r11)
            goto L_0x0097
        L_0x0055:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = com.fossil.qq5.i
            java.lang.String r5 = "executeUseCase"
            r11.d(r2, r5)
            com.fossil.qd5$a r11 = com.fossil.qd5.f
            java.lang.String r2 = "set_alarms"
            com.fossil.if5 r11 = r11.b(r2)
            com.fossil.qd5$a r5 = com.fossil.qd5.f
            r5.a(r2, r11)
            r11.d()
            if (r10 == 0) goto L_0x00e2
            r9.d = r10
            com.portfolio.platform.data.source.local.alarm.Alarm r11 = r10.a()
            com.portfolio.platform.data.source.AlarmsRepository r2 = r9.h
            java.lang.String r5 = r11.getUri()
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.label = r4
            java.lang.Object r2 = r2.getAlarmById(r5, r0)
            if (r2 != r1) goto L_0x0092
            return r1
        L_0x0092:
            r5 = r9
            r8 = r2
            r2 = r10
            r10 = r11
            r11 = r8
        L_0x0097:
            com.portfolio.platform.data.source.local.alarm.Alarm r11 = (com.portfolio.platform.data.source.local.alarm.Alarm) r11
            if (r11 == 0) goto L_0x00cd
            boolean r6 = r11.isActive()
            if (r6 != 0) goto L_0x00cd
            boolean r6 = r10.isActive()
            if (r6 != 0) goto L_0x00cd
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r6 = com.fossil.qq5.i
            java.lang.String r7 = "alarm found and not active"
            r4.d(r6, r7)
            com.portfolio.platform.data.source.AlarmsRepository r4 = r5.h
            r0.L$0 = r5
            r0.L$1 = r2
            r0.L$2 = r10
            r0.L$3 = r11
            r0.label = r3
            java.lang.Object r11 = r4.updateAlarm(r10, r0)
            if (r11 != r1) goto L_0x00c7
            return r1
        L_0x00c7:
            com.fossil.qq5$d r11 = new com.fossil.qq5$d
            r11.<init>(r10)
            goto L_0x00e1
        L_0x00cd:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.fossil.qq5.i
            java.lang.String r0 = "alarm not found"
            r10.d(r11, r0)
            r5.e = r4
            r5.a(r2)
            com.fossil.i97 r11 = com.fossil.i97.a
        L_0x00e1:
            return r11
        L_0x00e2:
            com.fossil.ee7.a()
            r10 = 0
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qq5.a(com.fossil.qq5$c, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(c cVar) {
        List<Alarm> b2 = cVar.b();
        ArrayList arrayList = new ArrayList();
        for (T t : b2) {
            if (t.isActive()) {
                arrayList.add(t);
            }
        }
        this.g.a(cVar.c(), rc5.a(arrayList));
    }
}
