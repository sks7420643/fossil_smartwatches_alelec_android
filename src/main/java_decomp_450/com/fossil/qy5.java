package com.fossil;

import com.fossil.ql4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy5 extends ql4<c, d, b> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.a {
        @DexIgnore
        public b(String str) {
            ee7.b(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ql4.c {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.wearables.fsl.contact.ContactGroup> */
        /* JADX WARN: Multi-variable type inference failed */
        public d(List<? extends ContactGroup> list) {
            ee7.b(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = qy5.class.getSimpleName();
        ee7.a((Object) simpleName, "GetAllHybridContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public void a(c cVar) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = ah5.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (allContactGroups != null) {
            a().onSuccess(new d(allContactGroups));
        } else {
            a().a(new b("Get all contact group failed"));
        }
    }
}
