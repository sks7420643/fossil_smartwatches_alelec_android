package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<q80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final q80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new q80(bArr[0], bArr[1], bArr[2], bArr[3]);
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public q80 createFromParcel(Parcel parcel) {
            return new q80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public q80[] newArray(int i) {
            return new q80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public q80 m53createFromParcel(Parcel parcel) {
            return new q80(parcel, null);
        }
    }

    @DexIgnore
    public q80(byte b2, byte b3, byte b4, byte b5) throws IllegalArgumentException {
        super(o80.DO_NOT_DISTURB_SCHEDULE);
        this.b = b2;
        this.c = b3;
        this.d = b4;
        this.e = b5;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(this.b).put(this.c).put(this.d).put(this.e).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        byte b2 = this.b;
        boolean z = true;
        if (b2 >= 0 && 23 >= b2) {
            byte b3 = this.c;
            if (b3 >= 0 && 59 >= b3) {
                byte b4 = this.d;
                if (b4 >= 0 && 23 >= b4) {
                    byte b5 = this.e;
                    if (b5 < 0 || 59 < b5) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalArgumentException(yh0.a(yh0.b("stopMinute("), this.e, ") is out of range ", "[0, 59]."));
                    }
                    return;
                }
                throw new IllegalArgumentException(yh0.a(yh0.b("stopHour("), this.d, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("startMinute("), this.c, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("startHour("), this.b, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(q80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            q80 q80 = (q80) obj;
            return this.b == q80.b && this.c == q80.c && this.d == q80.d && this.e == q80.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DoNotDisturbScheduleConfig");
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.b;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.c;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.d;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return (((((((super.hashCode() * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.b));
            jSONObject.put("start_minute", Byte.valueOf(this.c));
            jSONObject.put("stop_hour", Byte.valueOf(this.d));
            jSONObject.put("stop_minute", Byte.valueOf(this.e));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ q80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        e();
    }
}
