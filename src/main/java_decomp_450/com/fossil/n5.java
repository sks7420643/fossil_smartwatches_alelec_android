package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.i5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n5 {
    @DexIgnore
    public static boolean[] a; // = new boolean[3];

    @DexIgnore
    public static void a(j5 j5Var, y4 y4Var, i5 i5Var) {
        if (((i5) j5Var).C[0] != i5.b.WRAP_CONTENT && i5Var.C[0] == i5.b.MATCH_PARENT) {
            int i = i5Var.s.e;
            int t = j5Var.t() - i5Var.u.e;
            h5 h5Var = i5Var.s;
            h5Var.i = y4Var.a(h5Var);
            h5 h5Var2 = i5Var.u;
            h5Var2.i = y4Var.a(h5Var2);
            y4Var.a(i5Var.s.i, i);
            y4Var.a(i5Var.u.i, t);
            i5Var.a = 2;
            i5Var.a(i, t);
        }
        if (((i5) j5Var).C[1] != i5.b.WRAP_CONTENT && i5Var.C[1] == i5.b.MATCH_PARENT) {
            int i2 = i5Var.t.e;
            int j = j5Var.j() - i5Var.v.e;
            h5 h5Var3 = i5Var.t;
            h5Var3.i = y4Var.a(h5Var3);
            h5 h5Var4 = i5Var.v;
            h5Var4.i = y4Var.a(h5Var4);
            y4Var.a(i5Var.t.i, i2);
            y4Var.a(i5Var.v.i, j);
            if (i5Var.Q > 0 || i5Var.s() == 8) {
                h5 h5Var5 = i5Var.w;
                h5Var5.i = y4Var.a(h5Var5);
                y4Var.a(i5Var.w.i, i5Var.Q + i2);
            }
            i5Var.b = 2;
            i5Var.e(i2, j);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(com.fossil.i5 r5, int r6) {
        /*
            com.fossil.i5$b[] r0 = r5.C
            r1 = r0[r6]
            com.fossil.i5$b r2 = com.fossil.i5.b.MATCH_CONSTRAINT
            r3 = 0
            if (r1 == r2) goto L_0x000a
            return r3
        L_0x000a:
            float r1 = r5.G
            r2 = 0
            r4 = 1
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x001d
            if (r6 != 0) goto L_0x0015
            goto L_0x0016
        L_0x0015:
            r4 = 0
        L_0x0016:
            r5 = r0[r4]
            com.fossil.i5$b r6 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r5 != r6) goto L_0x001c
        L_0x001c:
            return r3
        L_0x001d:
            if (r6 != 0) goto L_0x002d
            int r6 = r5.e
            if (r6 == 0) goto L_0x0024
            return r3
        L_0x0024:
            int r6 = r5.h
            if (r6 != 0) goto L_0x002c
            int r5 = r5.i
            if (r5 == 0) goto L_0x003b
        L_0x002c:
            return r3
        L_0x002d:
            int r6 = r5.f
            if (r6 == 0) goto L_0x0032
            return r3
        L_0x0032:
            int r6 = r5.k
            if (r6 != 0) goto L_0x003c
            int r5 = r5.l
            if (r5 == 0) goto L_0x003b
            goto L_0x003c
        L_0x003b:
            return r4
        L_0x003c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n5.a(com.fossil.i5, int):boolean");
    }

    @DexIgnore
    public static void a(int i, i5 i5Var) {
        i5Var.J();
        p5 d = i5Var.s.d();
        p5 d2 = i5Var.t.d();
        p5 d3 = i5Var.u.d();
        p5 d4 = i5Var.v.d();
        boolean z = (i & 8) == 8;
        boolean z2 = i5Var.C[0] == i5.b.MATCH_CONSTRAINT && a(i5Var, 0);
        if (!(d.h == 4 || d3.h == 4)) {
            if (i5Var.C[0] == i5.b.FIXED || (z2 && i5Var.s() == 8)) {
                if (i5Var.s.d == null && i5Var.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, i5Var.n());
                    } else {
                        d3.a(d, i5Var.t());
                    }
                } else if (i5Var.s.d != null && i5Var.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, i5Var.n());
                    } else {
                        d3.a(d, i5Var.t());
                    }
                } else if (i5Var.s.d == null && i5Var.u.d != null) {
                    d.b(1);
                    d3.b(1);
                    d.a(d3, -i5Var.t());
                    if (z) {
                        d.a(d3, -1, i5Var.n());
                    } else {
                        d.a(d3, -i5Var.t());
                    }
                } else if (!(i5Var.s.d == null || i5Var.u.d == null)) {
                    d.b(2);
                    d3.b(2);
                    if (z) {
                        i5Var.n().a(d);
                        i5Var.n().a(d3);
                        d.b(d3, -1, i5Var.n());
                        d3.b(d, 1, i5Var.n());
                    } else {
                        d.b(d3, (float) (-i5Var.t()));
                        d3.b(d, (float) i5Var.t());
                    }
                }
            } else if (z2) {
                int t = i5Var.t();
                d.b(1);
                d3.b(1);
                if (i5Var.s.d == null && i5Var.u.d == null) {
                    if (z) {
                        d3.a(d, 1, i5Var.n());
                    } else {
                        d3.a(d, t);
                    }
                } else if (i5Var.s.d == null || i5Var.u.d != null) {
                    if (i5Var.s.d != null || i5Var.u.d == null) {
                        if (!(i5Var.s.d == null || i5Var.u.d == null)) {
                            if (z) {
                                i5Var.n().a(d);
                                i5Var.n().a(d3);
                            }
                            if (i5Var.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d.b(3);
                                d3.b(3);
                                d.b(d3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d3.b(d, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                d.b(2);
                                d3.b(2);
                                d.b(d3, (float) (-t));
                                d3.b(d, (float) t);
                                i5Var.p(t);
                            }
                        }
                    } else if (z) {
                        d.a(d3, -1, i5Var.n());
                    } else {
                        d.a(d3, -t);
                    }
                } else if (z) {
                    d3.a(d, 1, i5Var.n());
                } else {
                    d3.a(d, t);
                }
            }
        }
        boolean z3 = i5Var.C[1] == i5.b.MATCH_CONSTRAINT && a(i5Var, 1);
        if (d2.h != 4 && d4.h != 4) {
            if (i5Var.C[1] == i5.b.FIXED || (z3 && i5Var.s() == 8)) {
                if (i5Var.t.d == null && i5Var.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, i5Var.m());
                    } else {
                        d4.a(d2, i5Var.j());
                    }
                    h5 h5Var = i5Var.w;
                    if (h5Var.d != null) {
                        h5Var.d().b(1);
                        d2.a(1, i5Var.w.d(), -i5Var.Q);
                    }
                } else if (i5Var.t.d != null && i5Var.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, i5Var.m());
                    } else {
                        d4.a(d2, i5Var.j());
                    }
                    if (i5Var.Q > 0) {
                        i5Var.w.d().a(1, d2, i5Var.Q);
                    }
                } else if (i5Var.t.d == null && i5Var.v.d != null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d2.a(d4, -1, i5Var.m());
                    } else {
                        d2.a(d4, -i5Var.j());
                    }
                    if (i5Var.Q > 0) {
                        i5Var.w.d().a(1, d2, i5Var.Q);
                    }
                } else if (i5Var.t.d != null && i5Var.v.d != null) {
                    d2.b(2);
                    d4.b(2);
                    if (z) {
                        d2.b(d4, -1, i5Var.m());
                        d4.b(d2, 1, i5Var.m());
                        i5Var.m().a(d2);
                        i5Var.n().a(d4);
                    } else {
                        d2.b(d4, (float) (-i5Var.j()));
                        d4.b(d2, (float) i5Var.j());
                    }
                    if (i5Var.Q > 0) {
                        i5Var.w.d().a(1, d2, i5Var.Q);
                    }
                }
            } else if (z3) {
                int j = i5Var.j();
                d2.b(1);
                d4.b(1);
                if (i5Var.t.d == null && i5Var.v.d == null) {
                    if (z) {
                        d4.a(d2, 1, i5Var.m());
                    } else {
                        d4.a(d2, j);
                    }
                } else if (i5Var.t.d == null || i5Var.v.d != null) {
                    if (i5Var.t.d != null || i5Var.v.d == null) {
                        if (i5Var.t.d != null && i5Var.v.d != null) {
                            if (z) {
                                i5Var.m().a(d2);
                                i5Var.n().a(d4);
                            }
                            if (i5Var.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d2.b(3);
                                d4.b(3);
                                d2.b(d4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d4.b(d2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                return;
                            }
                            d2.b(2);
                            d4.b(2);
                            d2.b(d4, (float) (-j));
                            d4.b(d2, (float) j);
                            i5Var.h(j);
                            if (i5Var.Q > 0) {
                                i5Var.w.d().a(1, d2, i5Var.Q);
                            }
                        }
                    } else if (z) {
                        d2.a(d4, -1, i5Var.m());
                    } else {
                        d2.a(d4, -j);
                    }
                } else if (z) {
                    d4.a(d2, 1, i5Var.m());
                } else {
                    d4.a(d2, j);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r7.e0 == 2) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r7.f0 == 2) goto L_0x0034;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(com.fossil.j5 r24, com.fossil.y4 r25, int r26, int r27, com.fossil.g5 r28) {
        /*
            r0 = r25
            r1 = r26
            r2 = r28
            com.fossil.i5 r3 = r2.a
            com.fossil.i5 r4 = r2.c
            com.fossil.i5 r5 = r2.b
            com.fossil.i5 r6 = r2.d
            com.fossil.i5 r7 = r2.e
            float r8 = r2.k
            com.fossil.i5 r9 = r2.f
            com.fossil.i5 r2 = r2.g
            r9 = r24
            com.fossil.i5$b[] r2 = r9.C
            r2 = r2[r1]
            com.fossil.i5$b r9 = com.fossil.i5.b.WRAP_CONTENT
            r2 = 2
            r10 = 1
            if (r1 != 0) goto L_0x0038
            int r11 = r7.e0
            if (r11 != 0) goto L_0x0028
            r11 = 1
            goto L_0x0029
        L_0x0028:
            r11 = 0
        L_0x0029:
            int r12 = r7.e0
            if (r12 != r10) goto L_0x002f
            r12 = 1
            goto L_0x0030
        L_0x002f:
            r12 = 0
        L_0x0030:
            int r7 = r7.e0
            if (r7 != r2) goto L_0x0036
        L_0x0034:
            r2 = 1
            goto L_0x004b
        L_0x0036:
            r2 = 0
            goto L_0x004b
        L_0x0038:
            int r11 = r7.f0
            if (r11 != 0) goto L_0x003e
            r11 = 1
            goto L_0x003f
        L_0x003e:
            r11 = 0
        L_0x003f:
            int r12 = r7.f0
            if (r12 != r10) goto L_0x0045
            r12 = 1
            goto L_0x0046
        L_0x0045:
            r12 = 0
        L_0x0046:
            int r7 = r7.f0
            if (r7 != r2) goto L_0x0036
            goto L_0x0034
        L_0x004b:
            r14 = r3
            r10 = 0
            r13 = 0
            r15 = 0
            r16 = 0
            r17 = 0
        L_0x0053:
            r7 = 8
            if (r13 != 0) goto L_0x010a
            int r9 = r14.s()
            if (r9 == r7) goto L_0x00a1
            int r15 = r15 + 1
            if (r1 != 0) goto L_0x0066
            int r9 = r14.t()
            goto L_0x006a
        L_0x0066:
            int r9 = r14.j()
        L_0x006a:
            float r9 = (float) r9
            float r16 = r16 + r9
            if (r14 == r5) goto L_0x007a
            com.fossil.h5[] r9 = r14.A
            r9 = r9[r27]
            int r9 = r9.b()
            float r9 = (float) r9
            float r16 = r16 + r9
        L_0x007a:
            if (r14 == r6) goto L_0x0089
            com.fossil.h5[] r9 = r14.A
            int r19 = r27 + 1
            r9 = r9[r19]
            int r9 = r9.b()
            float r9 = (float) r9
            float r16 = r16 + r9
        L_0x0089:
            com.fossil.h5[] r9 = r14.A
            r9 = r9[r27]
            int r9 = r9.b()
            float r9 = (float) r9
            float r17 = r17 + r9
            com.fossil.h5[] r9 = r14.A
            int r19 = r27 + 1
            r9 = r9[r19]
            int r9 = r9.b()
            float r9 = (float) r9
            float r17 = r17 + r9
        L_0x00a1:
            com.fossil.h5[] r9 = r14.A
            r9 = r9[r27]
            int r9 = r14.s()
            if (r9 == r7) goto L_0x00df
            com.fossil.i5$b[] r7 = r14.C
            r7 = r7[r1]
            com.fossil.i5$b r9 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r7 != r9) goto L_0x00df
            int r10 = r10 + 1
            if (r1 != 0) goto L_0x00c7
            int r7 = r14.e
            if (r7 == 0) goto L_0x00bd
            r7 = 0
            return r7
        L_0x00bd:
            r7 = 0
            int r9 = r14.h
            if (r9 != 0) goto L_0x00c6
            int r9 = r14.i
            if (r9 == 0) goto L_0x00d6
        L_0x00c6:
            return r7
        L_0x00c7:
            r7 = 0
            int r9 = r14.f
            if (r9 == 0) goto L_0x00cd
            return r7
        L_0x00cd:
            int r9 = r14.k
            if (r9 != 0) goto L_0x00de
            int r9 = r14.l
            if (r9 == 0) goto L_0x00d6
            goto L_0x00de
        L_0x00d6:
            float r9 = r14.G
            r18 = 0
            int r9 = (r9 > r18 ? 1 : (r9 == r18 ? 0 : -1))
            if (r9 == 0) goto L_0x00df
        L_0x00de:
            return r7
        L_0x00df:
            com.fossil.h5[] r7 = r14.A
            int r9 = r27 + 1
            r7 = r7[r9]
            com.fossil.h5 r7 = r7.d
            if (r7 == 0) goto L_0x0101
            com.fossil.i5 r7 = r7.b
            com.fossil.h5[] r9 = r7.A
            r20 = r7
            r7 = r9[r27]
            com.fossil.h5 r7 = r7.d
            if (r7 == 0) goto L_0x0101
            r7 = r9[r27]
            com.fossil.h5 r7 = r7.d
            com.fossil.i5 r7 = r7.b
            if (r7 == r14) goto L_0x00fe
            goto L_0x0101
        L_0x00fe:
            r9 = r20
            goto L_0x0102
        L_0x0101:
            r9 = 0
        L_0x0102:
            if (r9 == 0) goto L_0x0107
            r14 = r9
            goto L_0x0053
        L_0x0107:
            r13 = 1
            goto L_0x0053
        L_0x010a:
            com.fossil.h5[] r9 = r3.A
            r9 = r9[r27]
            com.fossil.p5 r9 = r9.d()
            com.fossil.h5[] r13 = r4.A
            int r19 = r27 + 1
            r13 = r13[r19]
            com.fossil.p5 r13 = r13.d()
            com.fossil.p5 r7 = r9.d
            if (r7 == 0) goto L_0x0385
            r21 = r3
            com.fossil.p5 r3 = r13.d
            if (r3 != 0) goto L_0x0128
            goto L_0x0385
        L_0x0128:
            int r7 = r7.b
            r0 = 1
            if (r7 != r0) goto L_0x0383
            int r3 = r3.b
            if (r3 == r0) goto L_0x0133
            goto L_0x0383
        L_0x0133:
            if (r10 <= 0) goto L_0x0139
            if (r10 == r15) goto L_0x0139
            r0 = 0
            return r0
        L_0x0139:
            if (r2 != 0) goto L_0x0142
            if (r11 != 0) goto L_0x0142
            if (r12 == 0) goto L_0x0140
            goto L_0x0142
        L_0x0140:
            r0 = 0
            goto L_0x015b
        L_0x0142:
            if (r5 == 0) goto L_0x014e
            com.fossil.h5[] r0 = r5.A
            r0 = r0[r27]
            int r0 = r0.b()
            float r0 = (float) r0
            goto L_0x014f
        L_0x014e:
            r0 = 0
        L_0x014f:
            if (r6 == 0) goto L_0x015b
            com.fossil.h5[] r3 = r6.A
            r3 = r3[r19]
            int r3 = r3.b()
            float r3 = (float) r3
            float r0 = r0 + r3
        L_0x015b:
            com.fossil.p5 r3 = r9.d
            float r3 = r3.g
            com.fossil.p5 r6 = r13.d
            float r6 = r6.g
            int r7 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r7 >= 0) goto L_0x0169
            float r6 = r6 - r3
            goto L_0x016b
        L_0x0169:
            float r6 = r3 - r6
        L_0x016b:
            float r6 = r6 - r16
            r22 = 1
            if (r10 <= 0) goto L_0x0225
            if (r10 != r15) goto L_0x0225
            com.fossil.i5 r0 = r14.l()
            if (r0 == 0) goto L_0x0187
            com.fossil.i5 r0 = r14.l()
            com.fossil.i5$b[] r0 = r0.C
            r0 = r0[r1]
            com.fossil.i5$b r2 = com.fossil.i5.b.WRAP_CONTENT
            if (r0 != r2) goto L_0x0187
            r0 = 0
            return r0
        L_0x0187:
            float r6 = r6 + r16
            float r6 = r6 - r17
            r0 = r3
            r3 = r21
        L_0x018e:
            if (r3 == 0) goto L_0x0223
            com.fossil.z4 r2 = com.fossil.y4.q
            if (r2 == 0) goto L_0x01a6
            long r11 = r2.z
            long r11 = r11 - r22
            r2.z = r11
            long r11 = r2.r
            long r11 = r11 + r22
            r2.r = r11
            long r11 = r2.x
            long r11 = r11 + r22
            r2.x = r11
        L_0x01a6:
            com.fossil.i5[] r2 = r3.i0
            r2 = r2[r1]
            if (r2 != 0) goto L_0x01b2
            if (r3 != r4) goto L_0x01af
            goto L_0x01b2
        L_0x01af:
            r7 = r25
            goto L_0x0220
        L_0x01b2:
            float r5 = (float) r10
            float r5 = r6 / r5
            r7 = 0
            int r11 = (r8 > r7 ? 1 : (r8 == r7 ? 0 : -1))
            if (r11 <= 0) goto L_0x01cc
            float[] r5 = r3.g0
            r7 = r5[r1]
            r11 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r7 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r7 != 0) goto L_0x01c7
            r18 = 0
            goto L_0x01ce
        L_0x01c7:
            r5 = r5[r1]
            float r5 = r5 * r6
            float r5 = r5 / r8
        L_0x01cc:
            r18 = r5
        L_0x01ce:
            int r5 = r3.s()
            r7 = 8
            if (r5 != r7) goto L_0x01d8
            r18 = 0
        L_0x01d8:
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r27]
            int r5 = r5.b()
            float r5 = (float) r5
            float r0 = r0 + r5
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r27]
            com.fossil.p5 r5 = r5.d()
            com.fossil.p5 r7 = r9.f
            r5.a(r7, r0)
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r19]
            com.fossil.p5 r5 = r5.d()
            com.fossil.p5 r7 = r9.f
            float r0 = r0 + r18
            r5.a(r7, r0)
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r27]
            com.fossil.p5 r5 = r5.d()
            r7 = r25
            r5.a(r7)
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r19]
            com.fossil.p5 r5 = r5.d()
            r5.a(r7)
            com.fossil.h5[] r3 = r3.A
            r3 = r3[r19]
            int r3 = r3.b()
            float r3 = (float) r3
            float r0 = r0 + r3
        L_0x0220:
            r3 = r2
            goto L_0x018e
        L_0x0223:
            r0 = 1
            return r0
        L_0x0225:
            r7 = r25
            r8 = 0
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 >= 0) goto L_0x022f
            r2 = 1
            r11 = 0
            r12 = 0
        L_0x022f:
            if (r2 == 0) goto L_0x02af
            float r6 = r6 - r0
            r2 = r21
            float r0 = r2.b(r1)
            float r6 = r6 * r0
            float r3 = r3 + r6
            r0 = r3
        L_0x023c:
            r3 = r2
            if (r3 == 0) goto L_0x02b6
            com.fossil.z4 r2 = com.fossil.y4.q
            if (r2 == 0) goto L_0x0255
            long r5 = r2.z
            long r5 = r5 - r22
            r2.z = r5
            long r5 = r2.r
            long r5 = r5 + r22
            r2.r = r5
            long r5 = r2.x
            long r5 = r5 + r22
            r2.x = r5
        L_0x0255:
            com.fossil.i5[] r2 = r3.i0
            r2 = r2[r1]
            if (r2 != 0) goto L_0x025d
            if (r3 != r4) goto L_0x023c
        L_0x025d:
            if (r1 != 0) goto L_0x0264
            int r5 = r3.t()
            goto L_0x0268
        L_0x0264:
            int r5 = r3.j()
        L_0x0268:
            float r5 = (float) r5
            com.fossil.h5[] r6 = r3.A
            r6 = r6[r27]
            int r6 = r6.b()
            float r6 = (float) r6
            float r0 = r0 + r6
            com.fossil.h5[] r6 = r3.A
            r6 = r6[r27]
            com.fossil.p5 r6 = r6.d()
            com.fossil.p5 r8 = r9.f
            r6.a(r8, r0)
            com.fossil.h5[] r6 = r3.A
            r6 = r6[r19]
            com.fossil.p5 r6 = r6.d()
            com.fossil.p5 r8 = r9.f
            float r0 = r0 + r5
            r6.a(r8, r0)
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r27]
            com.fossil.p5 r5 = r5.d()
            r5.a(r7)
            com.fossil.h5[] r5 = r3.A
            r5 = r5[r19]
            com.fossil.p5 r5 = r5.d()
            r5.a(r7)
            com.fossil.h5[] r3 = r3.A
            r3 = r3[r19]
            int r3 = r3.b()
            float r3 = (float) r3
            float r0 = r0 + r3
            goto L_0x023c
        L_0x02af:
            r2 = r21
            if (r11 != 0) goto L_0x02b9
            if (r12 == 0) goto L_0x02b6
            goto L_0x02b9
        L_0x02b6:
            r0 = 1
            goto L_0x0382
        L_0x02b9:
            if (r11 == 0) goto L_0x02bd
        L_0x02bb:
            float r6 = r6 - r0
            goto L_0x02c0
        L_0x02bd:
            if (r12 == 0) goto L_0x02c0
            goto L_0x02bb
        L_0x02c0:
            int r0 = r15 + 1
            float r0 = (float) r0
            float r0 = r6 / r0
            if (r12 == 0) goto L_0x02d2
            r8 = 1
            if (r15 <= r8) goto L_0x02ce
            int r0 = r15 + -1
            float r0 = (float) r0
            goto L_0x02d0
        L_0x02ce:
            r0 = 1073741824(0x40000000, float:2.0)
        L_0x02d0:
            float r0 = r6 / r0
        L_0x02d2:
            int r6 = r2.s()
            r8 = 8
            if (r6 == r8) goto L_0x02dd
            float r6 = r3 + r0
            goto L_0x02de
        L_0x02dd:
            r6 = r3
        L_0x02de:
            if (r12 == 0) goto L_0x02ed
            r8 = 1
            if (r15 <= r8) goto L_0x02ed
            com.fossil.h5[] r6 = r5.A
            r6 = r6[r27]
            int r6 = r6.b()
            float r6 = (float) r6
            float r6 = r6 + r3
        L_0x02ed:
            if (r11 == 0) goto L_0x02fb
            if (r5 == 0) goto L_0x02fb
            com.fossil.h5[] r3 = r5.A
            r3 = r3[r27]
            int r3 = r3.b()
            float r3 = (float) r3
            float r6 = r6 + r3
        L_0x02fb:
            r3 = r2
            if (r3 == 0) goto L_0x02b6
            com.fossil.z4 r2 = com.fossil.y4.q
            if (r2 == 0) goto L_0x0314
            long r10 = r2.z
            long r10 = r10 - r22
            r2.z = r10
            long r10 = r2.r
            long r10 = r10 + r22
            r2.r = r10
            long r10 = r2.x
            long r10 = r10 + r22
            r2.x = r10
        L_0x0314:
            com.fossil.i5[] r2 = r3.i0
            r2 = r2[r1]
            if (r2 != 0) goto L_0x0320
            if (r3 != r4) goto L_0x031d
            goto L_0x0320
        L_0x031d:
            r8 = 8
            goto L_0x02fb
        L_0x0320:
            if (r1 != 0) goto L_0x0327
            int r8 = r3.t()
            goto L_0x032b
        L_0x0327:
            int r8 = r3.j()
        L_0x032b:
            float r8 = (float) r8
            if (r3 == r5) goto L_0x0338
            com.fossil.h5[] r10 = r3.A
            r10 = r10[r27]
            int r10 = r10.b()
            float r10 = (float) r10
            float r6 = r6 + r10
        L_0x0338:
            com.fossil.h5[] r10 = r3.A
            r10 = r10[r27]
            com.fossil.p5 r10 = r10.d()
            com.fossil.p5 r11 = r9.f
            r10.a(r11, r6)
            com.fossil.h5[] r10 = r3.A
            r10 = r10[r19]
            com.fossil.p5 r10 = r10.d()
            com.fossil.p5 r11 = r9.f
            float r12 = r6 + r8
            r10.a(r11, r12)
            com.fossil.h5[] r10 = r3.A
            r10 = r10[r27]
            com.fossil.p5 r10 = r10.d()
            r10.a(r7)
            com.fossil.h5[] r10 = r3.A
            r10 = r10[r19]
            com.fossil.p5 r10 = r10.d()
            r10.a(r7)
            com.fossil.h5[] r3 = r3.A
            r3 = r3[r19]
            int r3 = r3.b()
            float r3 = (float) r3
            float r8 = r8 + r3
            float r6 = r6 + r8
            if (r2 == 0) goto L_0x031d
            int r3 = r2.s()
            r8 = 8
            if (r3 == r8) goto L_0x02fb
            float r6 = r6 + r0
            goto L_0x02fb
        L_0x0382:
            return r0
        L_0x0383:
            r0 = 0
            return r0
        L_0x0385:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n5.a(com.fossil.j5, com.fossil.y4, int, int, com.fossil.g5):boolean");
    }

    @DexIgnore
    public static void a(i5 i5Var, int i, int i2) {
        int i3 = i * 2;
        int i4 = i3 + 1;
        i5Var.A[i3].d().f = i5Var.l().s.d();
        i5Var.A[i3].d().g = (float) i2;
        ((r5) i5Var.A[i3].d()).b = 1;
        i5Var.A[i4].d().f = i5Var.A[i3].d();
        i5Var.A[i4].d().g = (float) i5Var.d(i);
        ((r5) i5Var.A[i4].d()).b = 1;
    }
}
