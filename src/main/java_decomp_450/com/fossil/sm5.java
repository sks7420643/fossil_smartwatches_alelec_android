package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm5 implements Factory<rm5> {
    @DexIgnore
    public /* final */ Provider<GoogleApiService> a;

    @DexIgnore
    public sm5(Provider<GoogleApiService> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static sm5 a(Provider<GoogleApiService> provider) {
        return new sm5(provider);
    }

    @DexIgnore
    public static rm5 a(GoogleApiService googleApiService) {
        return new rm5(googleApiService);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public rm5 get() {
        return a(this.a.get());
    }
}
