package com.fossil;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v77 extends AppCompatActivity implements u77 {
    @DexIgnore
    public t77<Object> a;

    @DexIgnore
    @Override // com.fossil.u77
    public m77<Object> a() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        l77.a(this);
        super.onCreate(bundle);
    }
}
