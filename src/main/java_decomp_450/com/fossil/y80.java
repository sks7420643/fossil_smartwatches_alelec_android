package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ z80[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final y80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length >= 6) {
                ArrayList arrayList = new ArrayList();
                jf7 a = qf7.a(t97.a(bArr), 6);
                int first = a.getFirst();
                int last = a.getLast();
                int a2 = a.a();
                if (a2 < 0 ? first >= last : first <= last) {
                    while (true) {
                        arrayList.add(z80.CREATOR.a(s97.a(bArr, first, first + 6)));
                        if (first == last) {
                            break;
                        }
                        first += a2;
                    }
                }
                Object[] array = arrayList.toArray(new z80[0]);
                if (array != null) {
                    return new y80((z80[]) array);
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require at least: 6").toString());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public y80 createFromParcel(Parcel parcel) {
            return new y80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public y80[] newArray(int i) {
            return new y80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public y80 m86createFromParcel(Parcel parcel) {
            return new y80(parcel, null);
        }
    }

    @DexIgnore
    public y80(z80[] z80Arr) {
        super(o80.AUTO_WORKOUT_DETECTION);
        this.b = z80Arr;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] bArr = new byte[0];
        for (z80 z80 : this.b) {
            bArr = s97.a(bArr, z80.b());
        }
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(y80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((y80) obj).b);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.AutoWorkoutDetectionConfig");
    }

    @DexIgnore
    public final z80[] getAutoWorkoutDetectionItems() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return Arrays.hashCode(this.b);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONArray d() {
        JSONArray jSONArray = new JSONArray();
        try {
            for (z80 z80 : this.b) {
                jSONArray.put(z80.a());
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONArray;
    }

    @DexIgnore
    public /* synthetic */ y80(Parcel parcel, zd7 zd7) {
        super(parcel);
        z80[] z80Arr = (z80[]) parcel.createTypedArray(z80.CREATOR);
        this.b = z80Arr == null ? new z80[0] : z80Arr;
    }
}
