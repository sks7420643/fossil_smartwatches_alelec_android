package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u7 extends Drawable.ConstantState {
    @DexIgnore
    public int a;
    @DexIgnore
    public Drawable.ConstantState b;
    @DexIgnore
    public ColorStateList c; // = null;
    @DexIgnore
    public PorterDuff.Mode d; // = s7.g;

    @DexIgnore
    public u7(u7 u7Var) {
        if (u7Var != null) {
            this.a = u7Var.a;
            this.b = u7Var.b;
            this.c = u7Var.c;
            this.d = u7Var.d;
        }
    }

    @DexIgnore
    public boolean a() {
        return this.b != null;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int i = this.a;
        Drawable.ConstantState constantState = this.b;
        return i | (constantState != null ? constantState.getChangingConfigurations() : 0);
    }

    @DexIgnore
    public Drawable newDrawable() {
        return newDrawable(null);
    }

    @DexIgnore
    public Drawable newDrawable(Resources resources) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new t7(this, resources);
        }
        return new s7(this, resources);
    }
}
