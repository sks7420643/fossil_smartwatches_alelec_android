package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee1 extends sj1 {
    @DexIgnore
    public /* final */ byte[] A;
    @DexIgnore
    public /* final */ xp0 B;

    @DexIgnore
    public ee1(ri1 ri1, xp0 xp0) {
        super(qa1.F, ri1);
        this.B = xp0;
        byte[] b = xp0.b();
        ByteBuffer order = ByteBuffer.allocate(b.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(cy0.b.a);
        order.put(xp0.a.a);
        order.put(xp0.b);
        order.put(b);
        byte[] array = order.array();
        ee7.a((Object) array, "ackData.array()");
        this.A = array;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.j1, this.B.a());
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new ze1(qk1.ASYNC, this.A, ((v81) this).y.w);
    }
}
