package com.fossil;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bi0 {
    @DexIgnore
    public final JSONArray a(File file) {
        byte[] bArr;
        JSONArray jSONArray = new JSONArray();
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
            bArr = new byte[((int) file.length())];
            try {
                dataInputStream.readFully(bArr);
            } catch (IOException e) {
                wl0.h.a(e);
                bArr = null;
            } catch (Throwable th) {
                try {
                    dataInputStream.close();
                } catch (IOException unused) {
                }
                throw th;
            }
            try {
                dataInputStream.close();
            } catch (IOException unused2) {
            }
        } catch (FileNotFoundException e2) {
            wl0.h.a(e2);
            bArr = null;
        }
        String str = bArr != null ? new String(bArr, b21.x.c()) : null;
        if (str != null) {
            List a = nh7.a((CharSequence) str, new String[]{b21.x.v()}, false, 0, 6, (Object) null);
            ArrayList<String> arrayList = new ArrayList();
            for (Object obj : a) {
                if (!mh7.a((CharSequence) ((String) obj))) {
                    arrayList.add(obj);
                }
            }
            for (String str2 : arrayList) {
                String a2 = yn0.h.a(str2);
                if (a2 != null) {
                    try {
                        jSONArray.put(new JSONObject(a2));
                    } catch (Exception unused3) {
                        i97 i97 = i97.a;
                    }
                }
            }
        }
        return jSONArray;
    }

    @DexIgnore
    public JSONObject b(File file) {
        JSONObject jSONObject = new JSONObject();
        JSONArray a = a(file);
        if (a.length() > 0) {
            jSONObject.put("data", a);
        }
        return jSONObject;
    }
}
