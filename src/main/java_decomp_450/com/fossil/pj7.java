package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pj7 {
    @DexIgnore
    public static final <T> void a(oj7<? super T> oj7, int i) {
        fb7<? super T> a = oj7.a();
        if (!b(i) || !(a instanceof lj7) || a(i) != a(oj7.c)) {
            a(oj7, a, i);
            return;
        }
        ti7 ti7 = ((lj7) a).g;
        ib7 context = a.getContext();
        if (ti7.b(context)) {
            ti7.a(context, oj7);
        } else {
            a(oj7);
        }
    }

    @DexIgnore
    public static final boolean a(int i) {
        return i == 1;
    }

    @DexIgnore
    public static final boolean b(int i) {
        return i == 0 || i == 1;
    }

    @DexIgnore
    public static final <T> void a(oj7<? super T> oj7, fb7<? super T> fb7, int i) {
        Object obj;
        Object b = oj7.b();
        Throwable a = oj7.a(b);
        if (a == null) {
            a = null;
        } else if (dj7.d() && (fb7 instanceof sb7)) {
            a = km7.b(a, (sb7) fb7);
        }
        if (a != null) {
            s87.a aVar = s87.Companion;
            obj = s87.m60constructorimpl(t87.a(a));
        } else {
            s87.a aVar2 = s87.Companion;
            obj = s87.m60constructorimpl(b);
        }
        if (i == 0) {
            fb7.resumeWith(obj);
        } else if (i == 1) {
            mj7.a(fb7, obj);
        } else if (i != 2) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        } else if (fb7 != null) {
            lj7 lj7 = (lj7) fb7;
            ib7 context = lj7.getContext();
            Object b2 = pm7.b(context, lj7.f);
            try {
                lj7.h.resumeWith(obj);
                i97 i97 = i97.a;
            } finally {
                pm7.a(context, b2);
            }
        } else {
            throw new x87("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        }
    }

    @DexIgnore
    public static final void a(oj7<?> oj7) {
        vj7 b = fl7.b.b();
        if (b.k()) {
            b.a(oj7);
            return;
        }
        b.c(true);
        try {
            a(oj7, oj7.a(), 2);
            do {
            } while (b.o());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
    }
}
