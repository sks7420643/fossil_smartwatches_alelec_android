package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua1 extends ey0 {
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public ab0 M; // = new ab0(0, WorkoutType.UNKNOWN, WorkoutState.END, 0, 0, 0, 0, 0, 0, 0, 0, false);

    @DexIgnore
    public ua1(ri1 ri1) {
        super(uh0.o, qa1.C, ri1, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        WorkoutState workoutState;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 29) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            WorkoutType c = yz0.c(yz0.b(order.get(4)));
            short b = yz0.b(order.get(5));
            WorkoutState[] values = WorkoutState.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    workoutState = null;
                    break;
                }
                WorkoutState workoutState2 = values[i];
                if (workoutState2.getValue() == b) {
                    workoutState = workoutState2;
                    break;
                }
                i++;
            }
            if (c == null || workoutState == null) {
                ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.k, null, null, 27);
            } else {
                ab0 ab0 = new ab0(yz0.b(order.getInt(0)), c, workoutState, yz0.b(order.getInt(6)), yz0.b(order.getInt(10)), yz0.b(order.getInt(14)), yz0.b(order.getInt(18)), yz0.b(order.getInt(22)), yz0.b(order.get(26)), yz0.b(order.get(27)), yz0.b(order.get(28)), bArr.length != 29 && ((yz0.b(order.get(29)) >> 0) & 1) == 1);
                this.M = ab0;
                yz0.a(jSONObject, r51.d0, ab0.a());
                ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.a, null, null, 27);
            }
        } else {
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.j, null, null, 27);
        }
        ((uh1) this).E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.d0, this.M.a());
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean p() {
        return this.L;
    }

    @DexIgnore
    public final ab0 r() {
        return this.M;
    }
}
