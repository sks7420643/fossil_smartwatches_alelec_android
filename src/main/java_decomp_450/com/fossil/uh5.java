package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.wearables.fsl.utils.TimeUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uh5 extends BroadcastReceiver {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        MFLogger.d("TimeTickReceiver", "onReceive - action=" + action);
        if (!TextUtils.isEmpty(action) && ee7.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
            Calendar instance = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
            ee7.a((Object) instance, "calendar");
            String format = simpleDateFormat.format(instance.getTime());
            ch5 ch5 = new ch5(context);
            String D = ch5.D();
            MFLogger.d("TimeTickReceiver", "onReceive - day= " + format + ", widgetsDateChanged= " + D);
            if (!mh7.b(D, format, false, 2, null) && context != null) {
                MFLogger.d("TimeTickReceiver", "onReceive - need to resetAllContentWidgetsUI");
                ch5.z(format);
            }
        }
    }
}
