package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k63 extends el2 implements i63 {
    @DexIgnore
    public k63(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationListener");
    }

    @DexIgnore
    @Override // com.fossil.i63
    public final void onLocationChanged(Location location) throws RemoteException {
        Parcel E = E();
        jm2.a(E, location);
        c(1, E);
    }
}
