package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("socialId")
    public String b;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @te4("points")
    public Integer e;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<un4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public un4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new un4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public un4[] newArray(int i) {
            return new un4[i];
        }
    }

    @DexIgnore
    public un4(String str, String str2, String str3, String str4, Integer num, String str5, boolean z, int i2, int i3) {
        ee7.b(str, "id");
        ee7.b(str2, "socialId");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = num;
        this.f = str5;
        this.g = z;
        this.h = i2;
        this.i = i3;
    }

    @DexIgnore
    public final boolean a() {
        return this.g;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final int c() {
        return this.i;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof un4)) {
            return false;
        }
        un4 un4 = (un4) obj;
        return ee7.a(this.a, un4.a) && ee7.a(this.b, un4.b) && ee7.a(this.c, un4.c) && ee7.a(this.d, un4.d) && ee7.a(this.e, un4.e) && ee7.a(this.f, un4.f) && this.g == un4.g && this.h == un4.h && this.i == un4.i;
    }

    @DexIgnore
    public final int f() {
        return this.h;
    }

    @DexIgnore
    public final Integer g() {
        return this.e;
    }

    @DexIgnore
    public final String h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Integer num = this.e;
        int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
        String str5 = this.f;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        int i3 = (hashCode5 + i2) * 31;
        boolean z = this.g;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return ((((i3 + i4) * 31) + this.h) * 31) + this.i;
    }

    @DexIgnore
    public final String i() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "Friend(id=" + this.a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", points=" + this.e + ", profilePicture=" + this.f + ", confirmation=" + this.g + ", pin=" + this.h + ", friendType=" + this.i + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeValue(this.e);
        parcel.writeString(this.f);
        parcel.writeByte(this.g ? (byte) 1 : 0);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public un4(android.os.Parcel r12) {
        /*
            r11 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r12, r0)
            java.lang.String r2 = r12.readString()
            r0 = 0
            if (r2 == 0) goto L_0x0056
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.ee7.a(r2, r1)
            java.lang.String r3 = r12.readString()
            if (r3 == 0) goto L_0x0052
            com.fossil.ee7.a(r3, r1)
            java.lang.String r4 = r12.readString()
            java.lang.String r5 = r12.readString()
            java.lang.Class r1 = java.lang.Integer.TYPE
            java.lang.ClassLoader r1 = r1.getClassLoader()
            java.lang.Object r1 = r12.readValue(r1)
            boolean r6 = r1 instanceof java.lang.Integer
            if (r6 != 0) goto L_0x0031
            goto L_0x0032
        L_0x0031:
            r0 = r1
        L_0x0032:
            r6 = r0
            java.lang.Integer r6 = (java.lang.Integer) r6
            java.lang.String r7 = r12.readString()
            byte r0 = r12.readByte()
            r1 = 0
            byte r8 = (byte) r1
            if (r0 == r8) goto L_0x0044
            r0 = 1
            r8 = 1
            goto L_0x0045
        L_0x0044:
            r8 = 0
        L_0x0045:
            int r9 = r12.readInt()
            int r10 = r12.readInt()
            r1 = r11
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            return
        L_0x0052:
            com.fossil.ee7.a()
            throw r0
        L_0x0056:
            com.fossil.ee7.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.un4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public final void a(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public final void b(int i2) {
        this.h = i2;
    }
}
