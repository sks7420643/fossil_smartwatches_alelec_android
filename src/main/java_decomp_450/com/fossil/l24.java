package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l24 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ c24<?> a;
        @DexIgnore
        public /* final */ Set<b> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<b> c; // = new HashSet();

        @DexIgnore
        public b(c24<?> c24) {
            this.a = c24;
        }

        @DexIgnore
        public void a(b bVar) {
            this.b.add(bVar);
        }

        @DexIgnore
        public void b(b bVar) {
            this.c.add(bVar);
        }

        @DexIgnore
        public void c(b bVar) {
            this.c.remove(bVar);
        }

        @DexIgnore
        public boolean d() {
            return this.c.isEmpty();
        }

        @DexIgnore
        public c24<?> a() {
            return this.a;
        }

        @DexIgnore
        public Set<b> b() {
            return this.b;
        }

        @DexIgnore
        public boolean c() {
            return this.b.isEmpty();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Class<?> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (!cVar.a.equals(this.a) || cVar.b != this.b) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.b).hashCode();
        }

        @DexIgnore
        public c(Class<?> cls, boolean z) {
            this.a = cls;
            this.b = z;
        }
    }

    @DexIgnore
    public static void a(List<c24<?>> list) {
        Set<b> b2 = b(list);
        Set<b> a2 = a(b2);
        int i = 0;
        while (!a2.isEmpty()) {
            b next = a2.iterator().next();
            a2.remove(next);
            i++;
            for (b bVar : next.b()) {
                bVar.c(next);
                if (bVar.d()) {
                    a2.add(bVar);
                }
            }
        }
        if (i != list.size()) {
            ArrayList arrayList = new ArrayList();
            for (b bVar2 : b2) {
                if (!bVar2.d() && !bVar2.c()) {
                    arrayList.add(bVar2.a());
                }
            }
            throw new n24(arrayList);
        }
    }

    @DexIgnore
    public static Set<b> b(List<c24<?>> list) {
        Set<b> set;
        HashMap hashMap = new HashMap(list.size());
        for (c24<?> c24 : list) {
            b bVar = new b(c24);
            Iterator<Class<? super Object>> it = c24.c().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class<? super Object> next = it.next();
                    c cVar = new c(next, !c24.g());
                    if (!hashMap.containsKey(cVar)) {
                        hashMap.put(cVar, new HashSet());
                    }
                    Set set2 = (Set) hashMap.get(cVar);
                    if (set2.isEmpty() || cVar.b) {
                        set2.add(bVar);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", next));
                    }
                }
            }
        }
        for (Set<b> set3 : hashMap.values()) {
            for (b bVar2 : set3) {
                for (m24 m24 : bVar2.a().a()) {
                    if (m24.b() && (set = (Set) hashMap.get(new c(m24.a(), m24.d()))) != null) {
                        for (b bVar3 : set) {
                            bVar2.a(bVar3);
                            bVar3.b(bVar2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set set4 : hashMap.values()) {
            hashSet.addAll(set4);
        }
        return hashSet;
    }

    @DexIgnore
    public static Set<b> a(Set<b> set) {
        HashSet hashSet = new HashSet();
        for (b bVar : set) {
            if (bVar.d()) {
                hashSet.add(bVar);
            }
        }
        return hashSet;
    }
}
