package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h63 extends el2 implements f63 {
    @DexIgnore
    public h63(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }

    @DexIgnore
    @Override // com.fossil.f63
    public final void a(LocationAvailability locationAvailability) throws RemoteException {
        Parcel E = E();
        jm2.a(E, locationAvailability);
        c(2, E);
    }

    @DexIgnore
    @Override // com.fossil.f63
    public final void a(LocationResult locationResult) throws RemoteException {
        Parcel E = E();
        jm2.a(E, locationResult);
        c(1, E);
    }
}
