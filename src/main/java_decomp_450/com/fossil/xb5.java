package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum xb5 {
    DEVICE("device"),
    MANUAL("manual");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final xb5 a(String str) {
            xb5 xb5;
            ee7.b(str, "value");
            xb5[] values = xb5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    xb5 = null;
                    break;
                }
                xb5 = values[i];
                String mValue = xb5.getMValue();
                String lowerCase = str.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (ee7.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return xb5 != null ? xb5 : xb5.DEVICE;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public xb5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
