package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class ko0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[s01.values().length];
        a = iArr;
        iArr[s01.GET.ordinal()] = 1;
        a[s01.PUT.ordinal()] = 2;
        int[] iArr2 = new int[pb1.values().length];
        b = iArr2;
        iArr2[pb1.OTA.ordinal()] = 1;
        b[pb1.ACTIVITY_FILE.ordinal()] = 2;
        b[pb1.DEVICE_INFO.ordinal()] = 3;
        b[pb1.HARDWARE_LOG.ordinal()] = 4;
        b[pb1.FONT.ordinal()] = 5;
        b[pb1.ALL_FILE.ordinal()] = 6;
        b[pb1.ASSET.ordinal()] = 7;
        b[pb1.DEVICE_CONFIG.ordinal()] = 8;
        b[pb1.ALARM.ordinal()] = 9;
        b[pb1.NOTIFICATION_FILTER.ordinal()] = 10;
        b[pb1.MUSIC_CONTROL.ordinal()] = 11;
        b[pb1.MICRO_APP.ordinal()] = 12;
        b[pb1.WATCH_PARAMETERS_FILE.ordinal()] = 13;
        b[pb1.REPLY_MESSAGES_FILE.ordinal()] = 14;
        b[pb1.DEPRECATED_UI_PACKAGE_FILE.ordinal()] = 15;
        b[pb1.UI_PACKAGE_FILE.ordinal()] = 16;
        b[pb1.NOTIFICATION.ordinal()] = 17;
        b[pb1.UI_SCRIPT.ordinal()] = 18;
        b[pb1.LUTS_FILE.ordinal()] = 19;
        b[pb1.RATE_FILE.ordinal()] = 20;
        b[pb1.DATA_COLLECTION_FILE.ordinal()] = 21;
        b[pb1.UI_ENCRYPTED_FILE.ordinal()] = 22;
    }
    */
}
