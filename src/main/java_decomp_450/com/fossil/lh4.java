package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lh4 implements ph4 {
    @DexIgnore
    public static void b(qh4 qh4, StringBuilder sb) {
        qh4.a(a(sb, 0));
        sb.delete(0, 3);
    }

    @DexIgnore
    public int a() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.ph4
    public void a(qh4 qh4) {
        int a;
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!qh4.i()) {
                break;
            }
            char c = qh4.c();
            qh4.f++;
            int a2 = a(c, sb);
            int a3 = qh4.a() + ((sb.length() / 3) << 1);
            qh4.c(a3);
            int a4 = qh4.g().a() - a3;
            if (qh4.i()) {
                if (sb.length() % 3 == 0 && (a = sh4.a(qh4.d(), qh4.f, a())) != a()) {
                    qh4.b(a);
                    break;
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                if (sb.length() % 3 == 2 && (a4 < 2 || a4 > 2)) {
                    a2 = a(qh4, sb, sb2, a2);
                }
                while (sb.length() % 3 == 1 && ((a2 <= 3 && a4 != 1) || a2 > 3)) {
                    a2 = a(qh4, sb, sb2, a2);
                }
            }
        }
        a(qh4, sb);
    }

    @DexIgnore
    public final int a(qh4 qh4, StringBuilder sb, StringBuilder sb2, int i) {
        int length = sb.length();
        sb.delete(length - i, length);
        qh4.f--;
        int a = a(qh4.c(), sb2);
        qh4.k();
        return a;
    }

    @DexIgnore
    public void a(qh4 qh4, StringBuilder sb) {
        int length = sb.length() % 3;
        int a = qh4.a() + ((sb.length() / 3) << 1);
        qh4.c(a);
        int a2 = qh4.g().a() - a;
        if (length == 2) {
            sb.append((char) 0);
            while (sb.length() >= 3) {
                b(qh4, sb);
            }
            if (qh4.i()) {
                qh4.a('\u00fe');
            }
        } else if (a2 == 1 && length == 1) {
            while (sb.length() >= 3) {
                b(qh4, sb);
            }
            if (qh4.i()) {
                qh4.a('\u00fe');
            }
            qh4.f--;
        } else if (length == 0) {
            while (sb.length() >= 3) {
                b(qh4, sb);
            }
            if (a2 > 0 || qh4.i()) {
                qh4.a('\u00fe');
            }
        } else {
            throw new IllegalStateException("Unexpected case. Please report!");
        }
        qh4.b(0);
    }

    @DexIgnore
    public int a(char c, StringBuilder sb) {
        if (c == ' ') {
            sb.append((char) 3);
            return 1;
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
            return 1;
        } else if (c >= 'A' && c <= 'Z') {
            sb.append((char) ((c - 'A') + 14));
            return 1;
        } else if (c >= 0 && c <= 31) {
            sb.append((char) 0);
            sb.append(c);
            return 2;
        } else if (c >= '!' && c <= '/') {
            sb.append((char) 1);
            sb.append((char) (c - '!'));
            return 2;
        } else if (c >= ':' && c <= '@') {
            sb.append((char) 1);
            sb.append((char) ((c - ':') + 15));
            return 2;
        } else if (c >= '[' && c <= '_') {
            sb.append((char) 1);
            sb.append((char) ((c - '[') + 22));
            return 2;
        } else if (c >= '`' && c <= '\u007f') {
            sb.append((char) 2);
            sb.append((char) (c - '`'));
            return 2;
        } else if (c >= '\u0080') {
            sb.append("\u0001\u001e");
            return a((char) (c - '\u0080'), sb) + 2;
        } else {
            throw new IllegalArgumentException("Illegal character: " + c);
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int charAt = (charSequence.charAt(i) * '\u0640') + (charSequence.charAt(i + 1) * '(') + charSequence.charAt(i + 2) + 1;
        return new String(new char[]{(char) (charAt / 256), (char) (charAt % 256)});
    }
}
