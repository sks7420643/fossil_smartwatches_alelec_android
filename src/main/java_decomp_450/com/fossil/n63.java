package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n63 {
    @DexIgnore
    public /* final */ w63 a;
    @DexIgnore
    public u63 b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onCancel();

        @DexIgnore
        void onFinish();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onCameraIdle();
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void onCameraMove();
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void onCameraMoveStarted(int i);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void onCircleClick(c93 c93);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void onInfoWindowClick(j93 j93);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void onMapClick(LatLng latLng);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void onMapLoaded();
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void onMapLongClick(LatLng latLng);
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        boolean onMarkerClick(j93 j93);
    }

    @DexIgnore
    public interface k {
        @DexIgnore
        void onMarkerDrag(j93 j93);

        @DexIgnore
        void onMarkerDragEnd(j93 j93);

        @DexIgnore
        void onMarkerDragStart(j93 j93);
    }

    @DexIgnore
    public interface l {
        @DexIgnore
        void onPolygonClick(m93 m93);
    }

    @DexIgnore
    public interface m {
        @DexIgnore
        void onPolylineClick(o93 o93);
    }

    @DexIgnore
    public interface n {
        @DexIgnore
        void onSnapshotReady(Bitmap bitmap);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends k83 {
        @DexIgnore
        public /* final */ a a;

        @DexIgnore
        public o(a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.j83
        public final void onCancel() {
            this.a.onCancel();
        }

        @DexIgnore
        @Override // com.fossil.j83
        public final void onFinish() {
            this.a.onFinish();
        }
    }

    @DexIgnore
    public n63(w63 w63) {
        a72.a(w63);
        this.a = w63;
    }

    @DexIgnore
    public final void a(l63 l63) {
        try {
            this.a.d(l63.a());
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final CameraPosition b() {
        try {
            return this.a.m();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final float c() {
        try {
            return this.a.C();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final float d() {
        try {
            return this.a.g();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final r63 e() {
        try {
            return new r63(this.a.y());
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final u63 f() {
        try {
            if (this.b == null) {
                this.b = new u63(this.a.w());
            }
            return this.b;
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final boolean g() {
        try {
            return this.a.x();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final boolean h() {
        try {
            return this.a.s();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void i() {
        try {
            this.a.u();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(l63 l63, a aVar) {
        o oVar;
        try {
            w63 w63 = this.a;
            ab2 a2 = l63.a();
            if (aVar == null) {
                oVar = null;
            } else {
                oVar = new o(aVar);
            }
            w63.a(a2, oVar);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void b(l63 l63) {
        try {
            this.a.a(l63.a());
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.a.setMyLocationEnabled(z);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        try {
            this.a.setTrafficEnabled(z);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final boolean b(boolean z) {
        try {
            return this.a.setIndoorEnabled(z);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void b(float f2) {
        try {
            this.a.c(f2);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final o93 a(p93 p93) {
        try {
            return new o93(this.a.a(p93));
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final m93 a(n93 n93) {
        try {
            return new m93(this.a.a(n93));
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final c93 a(d93 d93) {
        try {
            return new c93(this.a.a(d93));
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final j93 a(k93 k93) {
        try {
            en2 a2 = this.a.a(k93);
            if (a2 != null) {
                return new j93(a2);
            }
            return null;
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a() {
        try {
            this.a.clear();
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(int i2) {
        try {
            this.a.setMapType(i2);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.setBuildingsEnabled(z);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(d dVar) {
        if (dVar == null) {
            try {
                this.a.a((u83) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new xa3(this, dVar));
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        if (cVar == null) {
            try {
                this.a.a((s83) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new ya3(this, cVar));
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        if (bVar == null) {
            try {
                this.a.a((q83) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new za3(this, bVar));
        }
    }

    @DexIgnore
    public final void a(g gVar) {
        if (gVar == null) {
            try {
                this.a.a((i73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new ab3(this, gVar));
        }
    }

    @DexIgnore
    public final void a(i iVar) {
        if (iVar == null) {
            try {
                this.a.a((m73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new bb3(this, iVar));
        }
    }

    @DexIgnore
    public final void a(j jVar) {
        if (jVar == null) {
            try {
                this.a.a((q73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new pa3(this, jVar));
        }
    }

    @DexIgnore
    public final void a(k kVar) {
        if (kVar == null) {
            try {
                this.a.a((s73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new qa3(this, kVar));
        }
    }

    @DexIgnore
    public final void a(f fVar) {
        if (fVar == null) {
            try {
                this.a.a((g73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new ra3(this, fVar));
        }
    }

    @DexIgnore
    public final void a(h hVar) {
        if (hVar == null) {
            try {
                this.a.a((k73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new sa3(this, hVar));
        }
    }

    @DexIgnore
    public final void a(e eVar) {
        if (eVar == null) {
            try {
                this.a.a((w83) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new ta3(this, eVar));
        }
    }

    @DexIgnore
    public final void a(l lVar) {
        if (lVar == null) {
            try {
                this.a.a((v73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new ua3(this, lVar));
        }
    }

    @DexIgnore
    public final void a(m mVar) {
        if (mVar == null) {
            try {
                this.a.a((x73) null);
            } catch (RemoteException e2) {
                throw new r93(e2);
            }
        } else {
            this.a.a(new va3(this, mVar));
        }
    }

    @DexIgnore
    public final void a(n nVar) {
        a(nVar, (Bitmap) null);
    }

    @DexIgnore
    public final void a(n nVar, Bitmap bitmap) {
        try {
            this.a.a(new wa3(this, nVar), (cb2) (bitmap != null ? cb2.a(bitmap) : null));
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) {
        try {
            this.a.a(i2, i3, i4, i5);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final boolean a(i93 i93) {
        try {
            return this.a.a(i93);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(float f2) {
        try {
            this.a.b(f2);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }

    @DexIgnore
    public final void a(LatLngBounds latLngBounds) {
        try {
            this.a.a(latLngBounds);
        } catch (RemoteException e2) {
            throw new r93(e2);
        }
    }
}
