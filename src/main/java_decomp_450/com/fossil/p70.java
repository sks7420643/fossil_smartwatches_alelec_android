package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p70 extends s70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<p70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public p70 createFromParcel(Parcel parcel) {
            return new p70(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public p70[] newArray(int i) {
            return new p70[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public p70 m47createFromParcel(Parcel parcel) {
            return new p70(parcel, null);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ p70(se0 se0, int i, zd7 zd7) {
        this((i & 1) != 0 ? new se0(false) : se0);
    }

    @DexIgnore
    public final se0 getDataConfig() {
        re0 re0 = ((s70) this).b;
        if (re0 != null) {
            return (se0) re0;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.PercentageCircleComplicationDataConfig");
    }

    @DexIgnore
    public p70(se0 se0) {
        super(u70.BATTERY, se0, null, null, 12);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ p70(ue0 ue0, ve0 ve0, se0 se0, int i, zd7 zd7) {
        this(ue0, (i & 2) != 0 ? new ve0(ve0.CREATOR.a()) : ve0, (i & 4) != 0 ? new se0(false) : se0);
    }

    @DexIgnore
    public p70(ue0 ue0, ve0 ve0, se0 se0) {
        super(u70.BATTERY, se0, ue0, ve0);
    }

    @DexIgnore
    public /* synthetic */ p70(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
