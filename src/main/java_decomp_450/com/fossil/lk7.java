package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lk7 extends pk7 implements ki7 {
    @DexIgnore
    public /* final */ boolean b; // = n();

    @DexIgnore
    public lk7(ik7 ik7) {
        super(true);
        a(ik7);
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public boolean d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public boolean f() {
        return true;
    }

    @DexIgnore
    public final boolean n() {
        pk7 pk7;
        gi7 g = g();
        if (!(g instanceof hi7)) {
            g = null;
        }
        hi7 hi7 = (hi7) g;
        if (!(hi7 == null || (pk7 = (pk7) ((ok7) hi7).d) == null)) {
            while (!pk7.d()) {
                gi7 g2 = pk7.g();
                if (!(g2 instanceof hi7)) {
                    g2 = null;
                }
                hi7 hi72 = (hi7) g2;
                if (hi72 != null) {
                    pk7 = (pk7) ((ok7) hi72).d;
                    if (pk7 == null) {
                    }
                }
            }
            return true;
        }
        return false;
    }
}
