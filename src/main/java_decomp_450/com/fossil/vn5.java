package com.fossil;

import com.fossil.fl4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ jh5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vn5.e;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ WeakReference<cl5> a;

        @DexIgnore
        public b(WeakReference<cl5> weakReference) {
            ee7.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<cl5> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ i02 b;

        @DexIgnore
        public c(int i, i02 i02) {
            this.a = i;
            this.b = i02;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            ee7.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = vn5.class.getSimpleName();
        ee7.a((Object) simpleName, "LoginGoogleUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public vn5(jh5 jh5) {
        ee7.b(jh5, "mLoginGoogleManager");
        this.d = jh5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return e;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements mh5 {
        @DexIgnore
        public /* final */ /* synthetic */ vn5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(vn5 vn5) {
            this.a = vn5;
        }

        @DexIgnore
        @Override // com.fossil.mh5
        public void a(SignUpSocialAuth signUpSocialAuth) {
            ee7.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vn5.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        @Override // com.fossil.mh5
        public void a(int i, i02 i02, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vn5.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + i02);
            this.a.a(new c(i, i02));
        }
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.g0.c().z()) {
                return new c(601, null);
            }
            jh5 jh5 = this.d;
            WeakReference<cl5> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                jh5.a(a2, new e(this));
                return i97.a;
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, null);
        }
    }
}
