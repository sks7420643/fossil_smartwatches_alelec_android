package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi5 implements MembersInjector<ui5> {
    @DexIgnore
    public static void a(ui5 ui5, mk5 mk5) {
        ui5.a = mk5;
    }

    @DexIgnore
    public static void a(ui5 ui5, ch5 ch5) {
        ui5.b = ch5;
    }
}
