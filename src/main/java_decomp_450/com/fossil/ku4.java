package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku4 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore
    public ku4() {
        Gson a2 = new be4().a();
        ee7.a((Object) a2, "GsonBuilder().create()");
        this.a = a2;
    }

    @DexIgnore
    public final String a(Date date) {
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = zd5.m.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.format(date);
            }
            ee7.a();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "fromOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final Date b(String str) {
        if (str == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = zd5.m.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.parse(str);
            }
            ee7.a();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final eo4 a(String str) {
        ee7.b(str, "value");
        try {
            return (eo4) this.a.a(str, eo4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final String a(eo4 eo4) {
        ee7.b(eo4, "owner");
        try {
            return this.a.a(eo4, eo4.class);
        } catch (Exception unused) {
            return null;
        }
    }
}
