package com.fossil;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.m00;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v00<Data> implements m00<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", "content")));
    @DexIgnore
    public /* final */ c<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements n00<Uri, AssetFileDescriptor>, c<AssetFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, AssetFileDescriptor> a(q00 q00) {
            return new v00(this);
        }

        @DexIgnore
        @Override // com.fossil.v00.c
        public ix<AssetFileDescriptor> a(Uri uri) {
            return new fx(this.a, uri);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements n00<Uri, ParcelFileDescriptor>, c<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.v00.c
        public ix<ParcelFileDescriptor> a(Uri uri) {
            return new nx(this.a, uri);
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, ParcelFileDescriptor> a(q00 q00) {
            return new v00(this);
        }
    }

    @DexIgnore
    public interface c<Data> {
        @DexIgnore
        ix<Data> a(Uri uri);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements n00<Uri, InputStream>, c<InputStream> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public d(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.v00.c
        public ix<InputStream> a(Uri uri) {
            return new tx(this.a, uri);
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, InputStream> a(q00 q00) {
            return new v00(this);
        }
    }

    @DexIgnore
    public v00(c<Data> cVar) {
        this.a = cVar;
    }

    @DexIgnore
    public m00.a<Data> a(Uri uri, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(uri), this.a.a(uri));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
