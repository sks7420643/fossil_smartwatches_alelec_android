package com.fossil;

import com.fossil.s87;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj7 {
    @DexIgnore
    public static /* final */ lm7 a; // = new lm7("UNDEFINED");
    @DexIgnore
    public static /* final */ lm7 b; // = new lm7("REUSABLE_CLAIMED");

    @DexIgnore
    public static final <T> void a(fb7<? super T> fb7, Object obj) {
        boolean z;
        if (fb7 instanceof lj7) {
            lj7 lj7 = (lj7) fb7;
            Object a2 = mi7.a(obj);
            if (lj7.g.b(lj7.getContext())) {
                lj7.d = a2;
                ((oj7) lj7).c = 1;
                lj7.g.a(lj7.getContext(), lj7);
                return;
            }
            vj7 b2 = fl7.b.b();
            if (b2.k()) {
                lj7.d = a2;
                ((oj7) lj7).c = 1;
                b2.a((oj7<?>) lj7);
                return;
            }
            b2.c(true);
            try {
                ik7 ik7 = (ik7) lj7.getContext().get(ik7.o);
                if (ik7 == null || ik7.isActive()) {
                    z = false;
                } else {
                    CancellationException b3 = ik7.b();
                    s87.a aVar = s87.Companion;
                    lj7.resumeWith(s87.m60constructorimpl(t87.a((Throwable) b3)));
                    z = true;
                }
                if (!z) {
                    ib7 context = lj7.getContext();
                    Object b4 = pm7.b(context, lj7.f);
                    try {
                        lj7.h.resumeWith(obj);
                        i97 i97 = i97.a;
                    } finally {
                        pm7.a(context, b4);
                    }
                }
                do {
                } while (b2.o());
            } catch (Throwable th) {
                b2.a(true);
                throw th;
            }
            b2.a(true);
            return;
        }
        fb7.resumeWith(obj);
    }

    @DexIgnore
    public static final boolean a(lj7<? super i97> lj7) {
        i97 i97 = i97.a;
        vj7 b2 = fl7.b.b();
        if (b2.l()) {
            return false;
        }
        if (b2.k()) {
            lj7.d = i97;
            ((oj7) lj7).c = 1;
            b2.a((oj7<?>) lj7);
            return true;
        }
        b2.c(true);
        try {
            lj7.run();
            do {
            } while (b2.o());
        } catch (Throwable th) {
            b2.a(true);
            throw th;
        }
        b2.a(true);
        return false;
    }
}
