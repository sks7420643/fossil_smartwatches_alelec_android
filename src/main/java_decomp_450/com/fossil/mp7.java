package com.fossil;

import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mp7 implements Interceptor.Chain {
    @DexIgnore
    public /* final */ List<Interceptor> a;
    @DexIgnore
    public /* final */ fp7 b;
    @DexIgnore
    public /* final */ ip7 c;
    @DexIgnore
    public /* final */ bp7 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ lo7 f;
    @DexIgnore
    public /* final */ qn7 g;
    @DexIgnore
    public /* final */ co7 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexIgnore
    public mp7(List<Interceptor> list, fp7 fp7, ip7 ip7, bp7 bp7, int i2, lo7 lo7, qn7 qn7, co7 co7, int i3, int i4, int i5) {
        this.a = list;
        this.d = bp7;
        this.b = fp7;
        this.c = ip7;
        this.e = i2;
        this.f = lo7;
        this.g = qn7;
        this.h = co7;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int a() {
        return this.j;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int b() {
        return this.k;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public lo7 c() {
        return this.f;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public un7 d() {
        return this.d;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int e() {
        return this.i;
    }

    @DexIgnore
    public qn7 f() {
        return this.g;
    }

    @DexIgnore
    public co7 g() {
        return this.h;
    }

    @DexIgnore
    public ip7 h() {
        return this.c;
    }

    @DexIgnore
    public fp7 i() {
        return this.b;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public Response a(lo7 lo7) throws IOException {
        return a(lo7, this.b, this.c, this.d);
    }

    @DexIgnore
    public Response a(lo7 lo7, fp7 fp7, ip7 ip7, bp7 bp7) throws IOException {
        if (this.e < this.a.size()) {
            this.l++;
            if (this.c != null && !this.d.a(lo7.g())) {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must retain the same host and port");
            } else if (this.c == null || this.l <= 1) {
                mp7 mp7 = new mp7(this.a, fp7, ip7, bp7, this.e + 1, lo7, this.g, this.h, this.i, this.j, this.k);
                Interceptor interceptor = this.a.get(this.e);
                Response intercept = interceptor.intercept(mp7);
                if (ip7 != null && this.e + 1 < this.a.size() && mp7.l != 1) {
                    throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
                } else if (intercept == null) {
                    throw new NullPointerException("interceptor " + interceptor + " returned null");
                } else if (intercept.a() != null) {
                    return intercept;
                } else {
                    throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }
}
