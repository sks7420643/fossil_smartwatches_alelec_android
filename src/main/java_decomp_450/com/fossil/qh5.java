package com.fossil;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.core.app.TaskStackBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.uirenew.home.HomeActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qh5 {
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qh5$a$a")
        @tb7(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1", f = "FossilNotificationBar.kt", l = {42}, m = "invokeSuspend")
        /* renamed from: com.fossil.qh5$a$a  reason: collision with other inner class name */
        public static final class C0157a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Context $context;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qh5$a$a$a")
            @tb7(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1", f = "FossilNotificationBar.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.qh5$a$a$a  reason: collision with other inner class name */
            public static final class C0158a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ qh5 $fossilNotificationBar;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0157a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0158a(C0157a aVar, qh5 qh5, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$fossilNotificationBar = qh5;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0158a aVar = new C0158a(this.this$0, this.$fossilNotificationBar, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0158a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        a.a(qh5.c, this.this$0.$context, this.$fossilNotificationBar, false, 4, null);
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0157a(Context context, fb7 fb7) {
                super(2, fb7);
                this.$context = context;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0157a aVar = new C0157a(this.$context, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((C0157a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    String b = PortfolioApp.g0.c().b();
                    qh5 qh5 = new qh5(b, null, 2, null);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("FossilNotificationBar", "content " + b);
                    tk7 c = qj7.c();
                    C0158a aVar = new C0158a(this, qh5, null);
                    this.L$0 = yi7;
                    this.L$1 = b;
                    this.L$2 = qh5;
                    this.label = 1;
                    if (vh7.a(c, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    qh5 qh52 = (qh5) this.L$2;
                    String str = (String) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Context context, qh5 qh5, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.b(context, qh5, z);
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            try {
                boolean A = PortfolioApp.g0.c().A();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FossilNotificationBar", "updateData() - context=" + context + " hasWorkout " + A);
                if (A) {
                    r87<String, String> b = PortfolioApp.g0.c().u().b();
                    b(this, context, new qh5(b.getSecond(), b.getFirst()), false, 4, null);
                    return;
                }
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new C0157a(context, null), 3, null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void b(Context context, qh5 qh5, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotificationWithRichContent " + qh5.a);
            if (!TextUtils.isEmpty(PortfolioApp.g0.c().c())) {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, qh5.b, qh5.a, z);
            } else {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, qh5.b, qh5.a, z);
            }
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            ee7.b(context, "context");
            ee7.b(str, "title");
            ee7.b(str2, "content");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateData() - title=" + str + " - content=" + str2);
            try {
                b(this, context, mh7.a(str) ^ true ? new qh5(str2, str) : new qh5(str2, null, 2, null), false, 4, null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public final void a(Context context, Service service, boolean z) {
            ee7.b(context, "context");
            ee7.b(service, Constants.SERVICE);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "Service Trackinng - startForegroundNotification() - context=" + context + ", service=" + service + ", isStopForeground = " + z);
            try {
                NotificationUtils.Companion.getInstance().startForegroundNotification(context, service, "", "", z);
                a(context);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "startForegroundNotification() - ex=" + e);
            }
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, qh5 qh5, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.a(context, qh5, z);
        }

        @DexIgnore
        public final void a(Context context, qh5 qh5, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotification content " + qh5.a);
            if (!TextUtils.isEmpty(PortfolioApp.g0.c().c())) {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, qh5.a, a(context, Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
            } else {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, qh5.a, null, null, z);
            }
        }

        @DexIgnore
        public final PendingIntent a(Context context, int i, int i2) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", i2);
            TaskStackBuilder a = TaskStackBuilder.a(context);
            a.b(intent);
            ee7.a((Object) a, "TaskStackBuilder.create(\u2026ntWithParentStack(intent)");
            return a.a(i, 134217728);
        }

        @DexIgnore
        public final PendingIntent a(Context context, String str, int i) {
            Intent intent = new Intent(context, NotificationReceiver.class);
            intent.setAction(str);
            intent.putExtra("ACTION_EVENT", i);
            return PendingIntent.getBroadcast(context, Action.DisplayMode.DATE, intent, 134217728);
        }
    }

    @DexIgnore
    public qh5() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public qh5(String str, String str2) {
        ee7.b(str, "mContent");
        ee7.b(str2, "mTitle");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ qh5(java.lang.String r1, java.lang.String r2, int r3, com.fossil.zd7 r4) {
        /*
            r0 = this;
            r4 = r3 & 1
            if (r4 == 0) goto L_0x0016
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            r4 = 2131887005(0x7f12039d, float:1.9408605E38)
            java.lang.String r1 = com.fossil.ig5.a(r1, r4)
            java.lang.String r4 = "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)"
            com.fossil.ee7.a(r1, r4)
        L_0x0016:
            r3 = r3 & 2
            if (r3 == 0) goto L_0x001c
            java.lang.String r2 = ""
        L_0x001c:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qh5.<init>(java.lang.String, java.lang.String, int, com.fossil.zd7):void");
    }
}
