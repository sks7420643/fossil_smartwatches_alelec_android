package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s33 implements t33 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.service.ssaid_removal", true);
        dr2.a("measurement.id.ssaid_removal", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.t33
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
