package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum gm1 {
    ENCRYPT(1),
    DECRYPT(2);
    
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public gm1(int i) {
        this.a = i;
    }
}
