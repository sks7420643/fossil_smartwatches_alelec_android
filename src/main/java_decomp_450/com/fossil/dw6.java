package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw6 extends fl4<b, c, Object> {
    @DexIgnore
    public /* final */ cw6 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            ee7.b(str, "passphrase");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.GeneratePassphraseUseCase", f = "GeneratePassphraseUseCase.kt", l = {26}, m = "run")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ dw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(dw6 dw6, fb7 fb7) {
            super(fb7);
            this.this$0 = dw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public dw6(cw6 cw6) {
        ee7.b(cw6, "mEncryptValueKeyStoreUseCase");
        this.d = cw6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GeneratePassphraseUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.dw6.b r20, com.fossil.fb7<java.lang.Object> r21) {
        /*
            r19 = this;
            r0 = r19
            r1 = r21
            boolean r2 = r1 instanceof com.fossil.dw6.d
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.dw6$d r2 = (com.fossil.dw6.d) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.dw6$d r2 = new com.fossil.dw6$d
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 1
            if (r4 == 0) goto L_0x004a
            if (r4 != r5) goto L_0x0042
            java.lang.Object r3 = r2.L$4
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r2.L$3
            char[] r3 = (char[]) r3
            java.lang.Object r4 = r2.L$2
            java.security.SecureRandom r4 = (java.security.SecureRandom) r4
            java.lang.Object r4 = r2.L$1
            com.fossil.dw6$b r4 = (com.fossil.dw6.b) r4
            java.lang.Object r2 = r2.L$0
            com.fossil.dw6 r2 = (com.fossil.dw6) r2
            com.fossil.t87.a(r1)
            goto L_0x00d7
        L_0x0042:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004a:
            com.fossil.t87.a(r1)
            java.security.SecureRandom r1 = new java.security.SecureRandom
            r1.<init>()
            r4 = 128(0x80, float:1.794E-43)
            char[] r15 = new char[r4]
            r6 = 0
        L_0x0057:
            if (r6 >= r4) goto L_0x006a
            r7 = 36
            int r7 = r1.nextInt(r7)
            java.lang.String r8 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            char r7 = r8.charAt(r7)
            r15[r6] = r7
            int r6 = r6 + 1
            goto L_0x0057
        L_0x006a:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r6 = "success to create passphrase "
            r14.append(r6)
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 62
            r16 = 0
            java.lang.String r7 = ""
            r6 = r15
            r5 = r14
            r14 = r16
            java.lang.String r6 = com.fossil.t97.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = "XXX"
            r4.d(r6, r5)
            java.lang.String r4 = "Passphrase"
            com.fossil.cw6 r5 = r0.d
            com.fossil.cw6$b r14 = new com.fossil.cw6$b
            com.fossil.ya5 r13 = new com.fossil.ya5
            r13.<init>()
            r16 = 62
            r17 = 0
            java.lang.String r7 = ""
            r6 = r15
            r18 = r13
            r13 = r16
            r16 = r3
            r3 = r14
            r14 = r17
            java.lang.String r6 = com.fossil.t97.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r7 = r18
            r3.<init>(r4, r7, r6)
            r2.L$0 = r0
            r6 = r20
            r2.L$1 = r6
            r2.L$2 = r1
            r2.L$3 = r15
            r2.L$4 = r4
            r1 = 1
            r2.label = r1
            java.lang.Object r1 = com.fossil.gl4.a(r5, r3, r2)
            r2 = r16
            if (r1 != r2) goto L_0x00d5
            return r2
        L_0x00d5:
            r2 = r0
            r3 = r15
        L_0x00d7:
            com.fossil.dw6$c r1 = new com.fossil.dw6$c
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 62
            r11 = 0
            java.lang.String r4 = ""
            java.lang.String r3 = com.fossil.t97.a(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r1.<init>(r3)
            com.fossil.ik7 r1 = r2.a(r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dw6.a(com.fossil.dw6$b, com.fossil.fb7):java.lang.Object");
    }
}
