package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k54 extends v54.d.AbstractC0206d.a {
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.a.b a;
    @DexIgnore
    public /* final */ w54<v54.b> b;
    @DexIgnore
    public /* final */ Boolean c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.AbstractC0207a {
        @DexIgnore
        public v54.d.AbstractC0206d.a.b a;
        @DexIgnore
        public w54<v54.b> b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Integer d;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.AbstractC0207a
        public v54.d.AbstractC0206d.a.AbstractC0207a a(v54.d.AbstractC0206d.a.b bVar) {
            if (bVar != null) {
                this.a = bVar;
                return this;
            }
            throw new NullPointerException("Null execution");
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(v54.d.AbstractC0206d.a aVar) {
            this.a = aVar.c();
            this.b = aVar.b();
            this.c = aVar.a();
            this.d = Integer.valueOf(aVar.d());
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.AbstractC0207a
        public v54.d.AbstractC0206d.a.AbstractC0207a a(w54<v54.b> w54) {
            this.b = w54;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.AbstractC0207a
        public v54.d.AbstractC0206d.a.AbstractC0207a a(Boolean bool) {
            this.c = bool;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.AbstractC0207a
        public v54.d.AbstractC0206d.a.AbstractC0207a a(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.AbstractC0207a
        public v54.d.AbstractC0206d.a a() {
            String str = "";
            if (this.a == null) {
                str = str + " execution";
            }
            if (this.d == null) {
                str = str + " uiOrientation";
            }
            if (str.isEmpty()) {
                return new k54(this.a, this.b, this.c, this.d.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a
    public Boolean a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a
    public w54<v54.b> b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a
    public v54.d.AbstractC0206d.a.b c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a
    public int d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a
    public v54.d.AbstractC0206d.a.AbstractC0207a e() {
        return new b(this);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        w54<v54.b> w54;
        Boolean bool;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a)) {
            return false;
        }
        v54.d.AbstractC0206d.a aVar = (v54.d.AbstractC0206d.a) obj;
        if (!this.a.equals(aVar.c()) || ((w54 = this.b) != null ? !w54.equals(aVar.b()) : aVar.b() != null) || ((bool = this.c) != null ? !bool.equals(aVar.a()) : aVar.a() != null) || this.d != aVar.d()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        w54<v54.b> w54 = this.b;
        int i = 0;
        int hashCode2 = (hashCode ^ (w54 == null ? 0 : w54.hashCode())) * 1000003;
        Boolean bool = this.c;
        if (bool != null) {
            i = bool.hashCode();
        }
        return ((hashCode2 ^ i) * 1000003) ^ this.d;
    }

    @DexIgnore
    public String toString() {
        return "Application{execution=" + this.a + ", customAttributes=" + this.b + ", background=" + this.c + ", uiOrientation=" + this.d + "}";
    }

    @DexIgnore
    public k54(v54.d.AbstractC0206d.a.b bVar, w54<v54.b> w54, Boolean bool, int i) {
        this.a = bVar;
        this.b = w54;
        this.c = bool;
        this.d = i;
    }
}
