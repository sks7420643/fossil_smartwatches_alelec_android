package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.y62;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pj2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<pj2> CREATOR; // = new rj2();
    @DexIgnore
    public /* final */ List<DataType> a;

    @DexIgnore
    public pj2(List<DataType> list) {
        this.a = list;
    }

    @DexIgnore
    public final List<DataType> e() {
        return Collections.unmodifiableList(this.a);
    }

    @DexIgnore
    public final String toString() {
        y62.a a2 = y62.a(this);
        a2.a("dataTypes", this.a);
        return a2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.c(parcel, 1, Collections.unmodifiableList(this.a), false);
        k72.a(parcel, a2);
    }
}
