package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn4 {
    @DexIgnore
    public static final boolean a(qn4 qn4) {
        ee7.b(qn4, "$this$validate");
        if (ee7.a((Object) qn4.j(), (Object) "activity_best_result")) {
            if ((qn4.e().length() > 0) && qn4.e().length() <= 32 && qn4.b() >= 3600 && qn4.b() <= 259200) {
                return true;
            }
        }
        if (ee7.a((Object) qn4.j(), (Object) "activity_reach_goal")) {
            if (!(qn4.e().length() > 0) || qn4.e().length() > 32 || qn4.i() < 1000 || qn4.i() > 300000) {
                return false;
            }
            return true;
        }
        return false;
    }
}
