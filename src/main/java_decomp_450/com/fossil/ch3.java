package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ch3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ /* synthetic */ wg3 d;

    @DexIgnore
    public ch3(wg3 wg3, String str, String str2) {
        this.d = wg3;
        a72.b(str);
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.s().getString(this.a, null);
        }
        return this.c;
    }

    @DexIgnore
    public final void a(String str) {
        if (this.d.l().a(wb3.x0) || !jm3.c(str, this.c)) {
            SharedPreferences.Editor edit = this.d.s().edit();
            edit.putString(this.a, str);
            edit.apply();
            this.c = str;
        }
    }
}
