package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oj7<T> extends dn7 {
    @DexIgnore
    public int c;

    @DexIgnore
    public oj7(int i) {
        this.c = i;
    }

    @DexIgnore
    public abstract fb7<T> a();

    @DexIgnore
    public final Throwable a(Object obj) {
        if (!(obj instanceof li7)) {
            obj = null;
        }
        li7 li7 = (li7) obj;
        if (li7 != null) {
            return li7.a;
        }
        return null;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
    }

    @DexIgnore
    public abstract Object b();

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public <T> T c(Object obj) {
        return obj;
    }

    @DexIgnore
    public final void run() {
        Object obj;
        Object obj2;
        en7 en7 = ((dn7) this).b;
        try {
            fb7<T> a = a();
            if (a != null) {
                lj7 lj7 = (lj7) a;
                fb7<T> fb7 = lj7.h;
                ib7 context = fb7.getContext();
                Object b = b();
                Object b2 = pm7.b(context, lj7.f);
                try {
                    Throwable a2 = a(b);
                    ik7 ik7 = pj7.a(this.c) ? (ik7) context.get(ik7.o) : null;
                    if (a2 == null && ik7 != null && !ik7.isActive()) {
                        Throwable b3 = ik7.b();
                        a(b, b3);
                        s87.a aVar = s87.Companion;
                        if (dj7.d()) {
                            if (fb7 instanceof sb7) {
                                b3 = km7.b(b3, (sb7) fb7);
                            }
                        }
                        fb7.resumeWith(s87.m60constructorimpl(t87.a(b3)));
                    } else if (a2 != null) {
                        s87.a aVar2 = s87.Companion;
                        fb7.resumeWith(s87.m60constructorimpl(t87.a(a2)));
                    } else {
                        T c2 = c(b);
                        s87.a aVar3 = s87.Companion;
                        fb7.resumeWith(s87.m60constructorimpl(c2));
                    }
                    i97 i97 = i97.a;
                    try {
                        s87.a aVar4 = s87.Companion;
                        en7.a();
                        obj2 = s87.m60constructorimpl(i97.a);
                    } catch (Throwable th) {
                        s87.a aVar5 = s87.Companion;
                        obj2 = s87.m60constructorimpl(t87.a(th));
                    }
                    a((Throwable) null, s87.m63exceptionOrNullimpl(obj2));
                    return;
                } finally {
                    pm7.a(context, b2);
                }
            } else {
                throw new x87("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
            }
        } catch (Throwable th2) {
            s87.a aVar6 = s87.Companion;
            obj = s87.m60constructorimpl(t87.a(th2));
        }
        a(th, s87.m63exceptionOrNullimpl(obj));
    }

    @DexIgnore
    public final void a(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                i87.a(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            String str = "Fatal exception in coroutines machinery for " + this + ". " + "Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (th != null) {
                vi7.a(a().getContext(), new cj7(str, th));
                return;
            }
            ee7.a();
            throw null;
        }
    }
}
