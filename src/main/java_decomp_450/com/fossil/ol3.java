package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol3 {
    @DexIgnore
    public nl3 a;
    @DexIgnore
    public /* final */ /* synthetic */ il3 b;

    @DexIgnore
    public ol3(il3 il3) {
        this.b = il3;
    }

    @DexIgnore
    public final void a() {
        this.b.g();
        if (this.b.l().a(wb3.p0) && this.a != null) {
            this.b.c.removeCallbacks(this.a);
        }
        if (this.b.l().a(wb3.D0)) {
            this.b.k().w.a(false);
        }
    }

    @DexIgnore
    public final void a(long j) {
        if (this.b.l().a(wb3.p0)) {
            this.a = new nl3(this, this.b.zzm().b(), j);
            this.b.c.postDelayed(this.a, 2000);
        }
    }
}
