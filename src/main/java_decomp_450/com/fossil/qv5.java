package com.fossil;

import android.text.SpannableString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qv5 extends dl4<pv5> {
    @DexIgnore
    void H(boolean z);

    @DexIgnore
    void I(boolean z);

    @DexIgnore
    void Q(String str);

    @DexIgnore
    void c(SpannableString spannableString);

    @DexIgnore
    void close();

    @DexIgnore
    void d(SpannableString spannableString);

    @DexIgnore
    void e(boolean z);

    @DexIgnore
    void l(boolean z);

    @DexIgnore
    void p(boolean z);
}
