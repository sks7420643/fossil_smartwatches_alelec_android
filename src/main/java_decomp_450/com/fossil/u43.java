package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u43 extends kp2 implements r43 {
    @DexIgnore
    public u43() {
        super("com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    @Override // com.fossil.kp2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((Bundle) jo2.a(parcel, Bundle.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
