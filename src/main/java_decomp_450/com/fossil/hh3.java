package com.fossil;

import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh3 extends hi3 {
    @DexIgnore
    public static /* final */ AtomicLong l; // = new AtomicLong(Long.MIN_VALUE);
    @DexIgnore
    public lh3 c;
    @DexIgnore
    public lh3 d;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<mh3<?>> e; // = new PriorityBlockingQueue<>();
    @DexIgnore
    public /* final */ BlockingQueue<mh3<?>> f; // = new LinkedBlockingQueue();
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler g; // = new jh3(this, "Thread death: Uncaught exception on worker thread");
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler h; // = new jh3(this, "Thread death: Uncaught exception on network thread");
    @DexIgnore
    public /* final */ Object i; // = new Object();
    @DexIgnore
    public /* final */ Semaphore j; // = new Semaphore(2);
    @DexIgnore
    public volatile boolean k;

    @DexIgnore
    public hh3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public final <V> Future<V> a(Callable<V> callable) throws IllegalStateException {
        n();
        a72.a(callable);
        mh3<?> mh3 = new mh3<>(this, (Callable<?>) callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            if (!this.e.isEmpty()) {
                e().w().a("Callable skipped the worker queue.");
            }
            mh3.run();
        } else {
            a(mh3);
        }
        return mh3;
    }

    @DexIgnore
    public final <V> Future<V> b(Callable<V> callable) throws IllegalStateException {
        n();
        a72.a(callable);
        mh3<?> mh3 = new mh3<>(this, (Callable<?>) callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            mh3.run();
        } else {
            a(mh3);
        }
        return mh3;
    }

    @DexIgnore
    @Override // com.fossil.ii3
    public final void d() {
        if (Thread.currentThread() != this.d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    @DexIgnore
    @Override // com.fossil.ii3
    public final void g() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    @DexIgnore
    @Override // com.fossil.hi3
    public final boolean q() {
        return false;
    }

    @DexIgnore
    public final boolean s() {
        return Thread.currentThread() == this.c;
    }

    @DexIgnore
    public final void b(Runnable runnable) throws IllegalStateException {
        n();
        a72.a(runnable);
        mh3<?> mh3 = new mh3<>(this, runnable, false, "Task exception on network thread");
        synchronized (this.i) {
            this.f.add(mh3);
            if (this.d == null) {
                lh3 lh3 = new lh3(this, "Measurement Network", this.f);
                this.d = lh3;
                lh3.setUncaughtExceptionHandler(this.h);
                this.d.start();
            } else {
                this.d.a();
            }
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) throws IllegalStateException {
        n();
        a72.a(runnable);
        a(new mh3<>(this, runnable, false, "Task exception on worker thread"));
    }

    @DexIgnore
    public final <T> T a(AtomicReference<T> atomicReference, long j2, String str, Runnable runnable) {
        synchronized (atomicReference) {
            c().a(runnable);
            try {
                atomicReference.wait(j2);
            } catch (InterruptedException unused) {
                mg3 w = e().w();
                String valueOf = String.valueOf(str);
                w.a(valueOf.length() != 0 ? "Interrupted waiting for ".concat(valueOf) : new String("Interrupted waiting for "));
                return null;
            }
        }
        T t = atomicReference.get();
        if (t == null) {
            mg3 w2 = e().w();
            String valueOf2 = String.valueOf(str);
            w2.a(valueOf2.length() != 0 ? "Timed out waiting for ".concat(valueOf2) : new String("Timed out waiting for "));
        }
        return t;
    }

    @DexIgnore
    public final void a(mh3<?> mh3) {
        synchronized (this.i) {
            this.e.add(mh3);
            if (this.c == null) {
                lh3 lh3 = new lh3(this, "Measurement Worker", this.e);
                this.c = lh3;
                lh3.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                this.c.a();
            }
        }
    }
}
