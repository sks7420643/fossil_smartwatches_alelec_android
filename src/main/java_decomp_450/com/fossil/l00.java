package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l00<A, B> {
    @DexIgnore
    public /* final */ r50<b<A>, B> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends r50<b<A>, B> {
        @DexIgnore
        public a(l00 l00, long j) {
            super(j);
        }

        @DexIgnore
        @Override // com.fossil.r50
        public /* bridge */ /* synthetic */ void a(Object obj, Object obj2) {
            a((b) ((b) obj), obj2);
        }

        @DexIgnore
        public void a(b<A> bVar, B b) {
            bVar.a();
        }
    }

    @DexIgnore
    public l00(long j) {
        this.a = new a(this, j);
    }

    @DexIgnore
    public B a(A a2, int i, int i2) {
        b<A> b2 = b.b(a2, i, i2);
        B a3 = this.a.a(b2);
        b2.a();
        return a3;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<A> {
        @DexIgnore
        public static /* final */ Queue<b<?>> d; // = v50.a(0);
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public A c;

        @DexIgnore
        public static <A> b<A> b(A a2, int i, int i2) {
            b<A> bVar;
            b<A> bVar2;
            synchronized (d) {
                bVar = (b<A>) d.poll();
            }
            if (bVar == null) {
                bVar = new b<>();
            }
            bVar2.a(a2, i, i2);
            return bVar2;
        }

        @DexIgnore
        public final void a(A a2, int i, int i2) {
            this.c = a2;
            this.b = i;
            this.a = i2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b == bVar.b && this.a == bVar.a && this.c.equals(bVar.c)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
        }

        @DexIgnore
        public void a() {
            synchronized (d) {
                d.offer(this);
            }
        }
    }

    @DexIgnore
    public void a(A a2, int i, int i2, B b2) {
        this.a.b(b.b(a2, i, i2), b2);
    }
}
