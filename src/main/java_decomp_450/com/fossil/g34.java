package com.fossil;

import android.os.Bundle;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g34 implements e34, k34 {
    @DexIgnore
    public j34 a;

    @DexIgnore
    public static String b(String str, Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (String str2 : bundle.keySet()) {
            jSONObject2.put(str2, bundle.get(str2));
        }
        jSONObject.put("name", str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }

    @DexIgnore
    @Override // com.fossil.e34
    public void a(String str, Bundle bundle) {
        j34 j34 = this.a;
        if (j34 != null) {
            try {
                j34.a("$A$:" + b(str, bundle));
            } catch (JSONException unused) {
                z24.a().d("Unable to serialize Firebase Analytics event to breadcrumb.");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.k34
    public void a(j34 j34) {
        this.a = j34;
        z24.a().a("Registered Firebase Analytics event receiver for breadcrumbs");
    }
}
