package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z37 {
    @DexIgnore
    public static p57 a;
    @DexIgnore
    public static volatile Map<String, Properties> b; // = new ConcurrentHashMap();
    @DexIgnore
    public static volatile Map<Integer, Integer> c; // = new ConcurrentHashMap(10);
    @DexIgnore
    public static volatile long d; // = 0;
    @DexIgnore
    public static volatile long e; // = 0;
    @DexIgnore
    public static volatile long f; // = 0;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static volatile int h; // = 0;
    @DexIgnore
    public static volatile String i; // = "";
    @DexIgnore
    public static volatile String j; // = "";
    @DexIgnore
    public static Map<String, Long> k; // = new ConcurrentHashMap();
    @DexIgnore
    public static Map<String, Long> l; // = new ConcurrentHashMap();
    @DexIgnore
    public static k57 m; // = v57.b();
    @DexIgnore
    public static Thread.UncaughtExceptionHandler n; // = null;
    @DexIgnore
    public static volatile boolean o; // = true;
    @DexIgnore
    public static volatile int p; // = 0;
    @DexIgnore
    public static volatile long q; // = 0;
    @DexIgnore
    public static Context r; // = null;
    @DexIgnore
    public static volatile long s; // = 0;

    /*
    static {
        new ConcurrentHashMap();
    }
    */

    @DexIgnore
    public static int a(Context context, boolean z, a47 a47) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = true;
        boolean z3 = z && currentTimeMillis - e >= ((long) w37.m());
        e = currentTimeMillis;
        if (f == 0) {
            f = v57.c();
        }
        if (currentTimeMillis >= f) {
            f = v57.c();
            if (x47.b(context).a(context).d() != 1) {
                x47.b(context).a(context).a(1);
            }
            w37.b(0);
            p = 0;
            g = v57.a(0);
            z3 = true;
        }
        String str = g;
        if (v57.a(a47)) {
            str = a47.a() + g;
        }
        if (l.containsKey(str)) {
            z2 = z3;
        }
        if (z2) {
            if (v57.a(a47)) {
                a(context, a47);
            } else if (w37.c() < w37.f()) {
                v57.A(context);
                a(context, (a47) null);
            } else {
                m.c("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            l.put(str, 1L);
        }
        if (o) {
            h(context);
            o = false;
        }
        return h;
    }

    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (z37.class) {
            if (context != null) {
                if (a == null) {
                    if (b(context)) {
                        Context applicationContext = context.getApplicationContext();
                        r = applicationContext;
                        a = new p57();
                        g = v57.a(0);
                        d = System.currentTimeMillis() + w37.x;
                        a.a(new k67(applicationContext));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(Context context, int i2) {
        k57 k57;
        String str;
        if (w37.s()) {
            if (w37.q()) {
                k57 k572 = m;
                k572.e("commitEvents, maxNumber=" + i2);
            }
            Context g2 = g(context);
            if (g2 == null) {
                k57 = m;
                str = "The Context of StatService.commitEvents() can not be null!";
            } else if (i2 < -1 || i2 == 0) {
                k57 = m;
                str = "The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.";
            } else if (k47.a(r).f() && c(g2) != null) {
                a.a(new l47(g2, i2));
                return;
            } else {
                return;
            }
            k57.d(str);
        }
    }

    @DexIgnore
    public static void a(Context context, a47 a47) {
        if (c(context) != null) {
            if (w37.q()) {
                m.a("start new session.");
            }
            if (a47 == null || h == 0) {
                h = v57.a();
            }
            w37.a(0);
            w37.b();
            new t47(new j47(context, h, b(), a47)).a();
        }
    }

    @DexIgnore
    public static void a(Context context, String str, a47 a47) {
        if (w37.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new p67(str2, g2, a47));
            }
        }
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties, a47 a47) {
        k57 k57;
        String str2;
        if (w37.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                k57 = m;
                str2 = "The Context of StatService.trackCustomEvent() can not be null!";
            } else if (a(str)) {
                k57 = m;
                str2 = "The event_id of StatService.trackCustomEvent() can not be null or empty.";
            } else {
                c47 c47 = new c47(str, null, properties);
                if (c(g2) != null) {
                    a.a(new o67(g2, a47, c47));
                    return;
                }
                return;
            }
            k57.d(str2);
        }
    }

    @DexIgnore
    public static void a(Context context, Throwable th) {
        if (w37.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (c(g2) != null) {
                a.a(new m67(g2, th));
            }
        }
    }

    @DexIgnore
    public static boolean a() {
        if (p < 2) {
            return false;
        }
        q = System.currentTimeMillis();
        return true;
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2, a47 a47) {
        try {
            if (!w37.s()) {
                m.d("MTA StatService is disable.");
                return false;
            }
            if (w37.q()) {
                m.a("MTA SDK version, current: " + "2.0.3" + " ,required: " + str2);
            }
            if (context != null) {
                if (str2 != null) {
                    if (v57.b("2.0.3") < v57.b(str2)) {
                        m.d(("MTA SDK version conflicted, current: " + "2.0.3" + ",required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                        w37.b(false);
                        return false;
                    }
                    String d2 = w37.d(context);
                    if (d2 == null || d2.length() == 0) {
                        w37.b(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    }
                    if (str != null) {
                        w37.b(context, str);
                    }
                    if (c(context) == null) {
                        return true;
                    }
                    a.a(new q47(context, a47));
                    return true;
                }
            }
            m.d("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
            w37.b(false);
            return false;
        } catch (Throwable th) {
            m.a(th);
            return false;
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (w37.c.d != 0) {
                jSONObject2.put("v", w37.c.d);
            }
            jSONObject.put(Integer.toString(w37.c.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (w37.b.d != 0) {
                jSONObject3.put("v", w37.b.d);
            }
            jSONObject.put(Integer.toString(w37.b.a), jSONObject3);
        } catch (JSONException e2) {
            m.a((Throwable) e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public static void b(Context context, a47 a47) {
        if (w37.s() && c(context) != null) {
            a.a(new l67(context, a47));
        }
    }

    @DexIgnore
    public static void b(Context context, String str, a47 a47) {
        if (w37.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new o47(g2, str2, a47));
            }
        }
    }

    @DexIgnore
    public static boolean b(Context context) {
        boolean z;
        long a2 = z57.a(context, w37.n, 0L);
        long b2 = v57.b("2.0.3");
        boolean z2 = false;
        if (b2 <= a2) {
            k57 k57 = m;
            k57.d("MTA is disable for current version:" + b2 + ",wakeup version:" + a2);
            z = false;
        } else {
            z = true;
        }
        long a3 = z57.a(context, w37.o, 0L);
        if (a3 > System.currentTimeMillis()) {
            k57 k572 = m;
            k572.d("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + a3);
        } else {
            z2 = z;
        }
        w37.b(z2);
        return z2;
    }

    @DexIgnore
    public static p57 c(Context context) {
        if (a == null) {
            synchronized (z37.class) {
                if (a == null) {
                    try {
                        a(context);
                    } catch (Throwable th) {
                        m.b(th);
                        w37.b(false);
                    }
                }
            }
        }
        return a;
    }

    @DexIgnore
    public static void c() {
        p = 0;
        q = 0;
    }

    @DexIgnore
    public static void c(Context context, a47 a47) {
        if (w37.s() && c(context) != null) {
            a.a(new p47(context, a47));
        }
    }

    @DexIgnore
    public static Properties d(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static void d() {
        p++;
        q = System.currentTimeMillis();
        f(r);
    }

    @DexIgnore
    public static void d(Context context) {
        if (w37.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                h67.b(g2).a(new g47(g2), new n67());
            } catch (Throwable th) {
                m.a(th);
            }
        }
    }

    @DexIgnore
    public static void e(Context context) {
        s = System.currentTimeMillis() + ((long) (w37.l() * 60000));
        z57.b(context, "last_period_ts", s);
        a(context, -1);
    }

    @DexIgnore
    public static void f(Context context) {
        if (w37.s() && w37.J > 0) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else {
                x47.b(g2).b();
            }
        }
    }

    @DexIgnore
    public static Context g(Context context) {
        return context != null ? context : r;
    }

    @DexIgnore
    public static void h(Context context) {
        if (w37.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else if (c(g2) != null) {
                a.a(new m47(g2));
            }
        }
    }
}
