package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj5 implements Factory<lj5> {
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;

    @DexIgnore
    public mj5(Provider<AuthApiGuestService> provider, Provider<ch5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static mj5 a(Provider<AuthApiGuestService> provider, Provider<ch5> provider2) {
        return new mj5(provider, provider2);
    }

    @DexIgnore
    public static lj5 a(AuthApiGuestService authApiGuestService, ch5 ch5) {
        return new lj5(authApiGuestService, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public lj5 get() {
        return a(this.a.get(), this.b.get());
    }
}
