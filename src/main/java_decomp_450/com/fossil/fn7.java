package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn7 extends dn7 {
    @DexIgnore
    public /* final */ Runnable c;

    @DexIgnore
    public fn7(Runnable runnable, long j, en7 en7) {
        super(j, en7);
        this.c = runnable;
    }

    @DexIgnore
    public void run() {
        try {
            this.c.run();
        } finally {
            ((dn7) this).b.a();
        }
    }

    @DexIgnore
    public String toString() {
        return "Task[" + ej7.a(this.c) + '@' + ej7.b(this.c) + ", " + ((dn7) this).a + ", " + ((dn7) this).b + ']';
    }
}
