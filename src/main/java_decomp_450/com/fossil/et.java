package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.Lifecycle;
import com.fossil.it;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et extends it {
    @DexIgnore
    public /* final */ Drawable A;
    @DexIgnore
    public /* final */ Drawable B;
    @DexIgnore
    public /* final */ Drawable C;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ vt c;
    @DexIgnore
    public /* final */ Lifecycle d;
    @DexIgnore
    public /* final */ zt e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ List<String> g;
    @DexIgnore
    public /* final */ it.a h;
    @DexIgnore
    public /* final */ st i;
    @DexIgnore
    public /* final */ qt j;
    @DexIgnore
    public /* final */ pt k;
    @DexIgnore
    public /* final */ fr l;
    @DexIgnore
    public /* final */ ti7 m;
    @DexIgnore
    public /* final */ List<yt> n;
    @DexIgnore
    public /* final */ Bitmap.Config o;
    @DexIgnore
    public /* final */ ColorSpace p;
    @DexIgnore
    public /* final */ fo7 q;
    @DexIgnore
    public /* final */ ht r;
    @DexIgnore
    public /* final */ dt s;
    @DexIgnore
    public /* final */ dt t;
    @DexIgnore
    public /* final */ dt u;
    @DexIgnore
    public /* final */ boolean v;
    @DexIgnore
    public /* final */ boolean w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r27v0, resolved type: java.util.List<? extends com.fossil.yt> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public et(Context context, Object obj, vt vtVar, Lifecycle lifecycle, zt ztVar, String str, List<String> list, it.a aVar, st stVar, qt qtVar, pt ptVar, fr frVar, ti7 ti7, List<? extends yt> list2, Bitmap.Config config, ColorSpace colorSpace, fo7 fo7, ht htVar, dt dtVar, dt dtVar2, dt dtVar3, boolean z2, boolean z3, int i2, int i3, int i4, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        super(null);
        ee7.b(context, "context");
        ee7.b(list, "aliasKeys");
        ee7.b(ptVar, "precision");
        ee7.b(ti7, "dispatcher");
        ee7.b(list2, "transformations");
        ee7.b(config, "bitmapConfig");
        ee7.b(fo7, "headers");
        ee7.b(htVar, "parameters");
        ee7.b(dtVar, "networkCachePolicy");
        ee7.b(dtVar2, "diskCachePolicy");
        ee7.b(dtVar3, "memoryCachePolicy");
        this.a = context;
        this.b = obj;
        this.c = vtVar;
        this.d = lifecycle;
        this.e = ztVar;
        this.f = str;
        this.g = list;
        this.h = aVar;
        this.i = stVar;
        this.j = qtVar;
        this.k = ptVar;
        this.l = frVar;
        this.m = ti7;
        this.n = list2;
        this.o = config;
        this.p = colorSpace;
        this.q = fo7;
        this.r = htVar;
        this.s = dtVar;
        this.t = dtVar2;
        this.u = dtVar3;
        this.v = z2;
        this.w = z3;
        this.x = i2;
        this.y = i3;
        this.z = i4;
        this.A = drawable;
        this.B = drawable2;
        this.C = drawable3;
    }

    @DexIgnore
    @Override // com.fossil.it
    public List<String> a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.it
    public boolean b() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.it
    public boolean c() {
        return this.w;
    }

    @DexIgnore
    @Override // com.fossil.it
    public Bitmap.Config d() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.it
    public ColorSpace e() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.it
    public fr f() {
        return this.l;
    }

    @DexIgnore
    @Override // com.fossil.it
    public dt g() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.it
    public ti7 h() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.it
    public Drawable i() {
        return a(this.a, this.B, this.y);
    }

    @DexIgnore
    @Override // com.fossil.it
    public Drawable j() {
        return a(this.a, this.C, this.z);
    }

    @DexIgnore
    @Override // com.fossil.it
    public fo7 k() {
        return this.q;
    }

    @DexIgnore
    @Override // com.fossil.it
    public String l() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.it
    public it.a m() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.it
    public dt n() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.it
    public dt o() {
        return this.s;
    }

    @DexIgnore
    @Override // com.fossil.it
    public ht p() {
        return this.r;
    }

    @DexIgnore
    @Override // com.fossil.it
    public Drawable q() {
        return a(this.a, this.A, this.x);
    }

    @DexIgnore
    @Override // com.fossil.it
    public pt r() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.it
    public qt s() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.it
    public st t() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.it
    public vt u() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.it
    public List<yt> v() {
        return this.n;
    }

    @DexIgnore
    @Override // com.fossil.it
    public zt w() {
        return this.e;
    }

    @DexIgnore
    public final Context x() {
        return this.a;
    }

    @DexIgnore
    public Object y() {
        return this.b;
    }

    @DexIgnore
    public Lifecycle z() {
        return this.d;
    }

    @DexIgnore
    public final Drawable a(Context context, Drawable drawable, int i2) {
        if (drawable != null) {
            return drawable;
        }
        if (i2 != 0) {
            return fu.a(context, i2);
        }
        return null;
    }
}
