package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju4 {
    @DexIgnore
    public final String a(int[] iArr) {
        if (iArr == null) {
            return "[]";
        }
        String arrays = Arrays.toString(iArr);
        ee7.a((Object) arrays, "Arrays.toString(array)");
        return arrays;
    }

    @DexIgnore
    public final int[] a(String str) {
        if (str == null) {
            return new int[0];
        }
        if (nh7.a((CharSequence) str, (CharSequence) "null", false, 2, (Object) null)) {
            return new int[0];
        }
        try {
            List<String> a = nh7.a((CharSequence) nh7.a(str, (CharSequence) "[", (CharSequence) "]"), new String[]{","}, false, 0, 6, (Object) null);
            ArrayList arrayList = new ArrayList(x97.a(a, 10));
            for (String str2 : a) {
                int length = str2.length() - 1;
                int i = 0;
                boolean z = false;
                while (true) {
                    if (i > length) {
                        break;
                    }
                    boolean z2 = str2.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                arrayList.add(Integer.valueOf(Integer.parseInt(str2.subSequence(i, length + 1).toString())));
            }
            return ea7.c((Collection<Integer>) arrayList);
        } catch (Exception unused) {
            return new int[0];
        }
    }
}
