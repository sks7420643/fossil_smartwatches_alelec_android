package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wy2 extends uy2<ty2, ty2> {
    @DexIgnore
    public static void a(Object obj, ty2 ty2) {
        ((bw2) obj).zzb = ty2;
    }

    @DexIgnore
    @Override // com.fossil.uy2
    public final void b(Object obj) {
        ((bw2) obj).zzb.a();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.uy2
    public final /* synthetic */ int c(ty2 ty2) {
        return ty2.b();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.uy2
    public final /* synthetic */ int d(ty2 ty2) {
        return ty2.c();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.oz2] */
    @Override // com.fossil.uy2
    public final /* synthetic */ void a(ty2 ty2, oz2 oz2) throws IOException {
        ty2.b(oz2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.oz2] */
    @Override // com.fossil.uy2
    public final /* synthetic */ void b(ty2 ty2, oz2 oz2) throws IOException {
        ty2.a(oz2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.uy2
    public final /* synthetic */ ty2 c(ty2 ty2, ty2 ty22) {
        ty2 ty23 = ty2;
        ty2 ty24 = ty22;
        if (ty24.equals(ty2.d())) {
            return ty23;
        }
        return ty2.a(ty23, ty24);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.uy2
    public final /* synthetic */ ty2 a(Object obj) {
        return ((bw2) obj).zzb;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.uy2
    public final /* bridge */ /* synthetic */ void a(Object obj, ty2 ty2) {
        a(obj, ty2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.uy2
    public final /* synthetic */ void b(Object obj, ty2 ty2) {
        a(obj, ty2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.uy2
    public final /* synthetic */ ty2 a() {
        return ty2.e();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, com.fossil.tu2] */
    @Override // com.fossil.uy2
    public final /* synthetic */ void a(ty2 ty2, int i, tu2 tu2) {
        ty2.a((i << 3) | 2, tu2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, long] */
    @Override // com.fossil.uy2
    public final /* synthetic */ void a(ty2 ty2, int i, long j) {
        ty2.a(i << 3, Long.valueOf(j));
    }
}
