package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po3 {
    @DexIgnore
    public static /* final */ Executor a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new kp3();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler a; // = new x43(Looper.getMainLooper());

        @DexIgnore
        public final void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }
}
