package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up<V> extends sp<V> {
    @DexIgnore
    public static <V> up<V> e() {
        return new up<>();
    }

    @DexIgnore
    @Override // com.fossil.sp
    public boolean a(Throwable th) {
        return super.a(th);
    }

    @DexIgnore
    @Override // com.fossil.sp
    public boolean b(V v) {
        return super.b((Object) v);
    }

    @DexIgnore
    @Override // com.fossil.sp
    public boolean a(i14<? extends V> i14) {
        return super.a((i14) i14);
    }
}
