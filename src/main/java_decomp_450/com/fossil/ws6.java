package com.fossil;

import com.portfolio.platform.uirenew.signup.SignUpActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws6 implements MembersInjector<SignUpActivity> {
    @DexIgnore
    public static void a(SignUpActivity signUpActivity, et6 et6) {
        signUpActivity.y = et6;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, ih5 ih5) {
        signUpActivity.z = ih5;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, jh5 jh5) {
        signUpActivity.A = jh5;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, lh5 lh5) {
        signUpActivity.B = lh5;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, kh5 kh5) {
        signUpActivity.C = kh5;
    }
}
