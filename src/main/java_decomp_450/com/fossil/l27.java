package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l27 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public Bundle e;

        @DexIgnore
        public final String toString() {
            return "targetPkgName:" + this.a + ", targetClassName:" + this.b + ", content:" + this.c + ", flags:" + this.d + ", bundle:" + this.e;
        }
    }

    @DexIgnore
    public static boolean a(Context context, a aVar) {
        if (context == null) {
            p27.a("MicroMsg.SDK.MMessageAct", "send fail, invalid argument");
            return false;
        } else if (r27.a(aVar.a)) {
            p27.a("MicroMsg.SDK.MMessageAct", "send fail, invalid targetPkgName, targetPkgName = " + aVar.a);
            return false;
        } else {
            if (r27.a(aVar.b)) {
                aVar.b = aVar.a + ".wxapi.WXEntryActivity";
            }
            p27.d("MicroMsg.SDK.MMessageAct", "send, targetPkgName = " + aVar.a + ", targetClassName = " + aVar.b);
            Intent intent = new Intent();
            intent.setClassName(aVar.a, aVar.b);
            Bundle bundle = aVar.e;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 587268097);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", aVar.c);
            intent.putExtra("_mmessage_checksum", n27.a(aVar.c, 587268097, packageName));
            int i = aVar.d;
            if (i == -1) {
                intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY).addFlags(134217728);
            } else {
                intent.setFlags(i);
            }
            try {
                context.startActivity(intent);
                p27.d("MicroMsg.SDK.MMessageAct", "send mm message, intent=" + intent);
                return true;
            } catch (Exception e) {
                p27.a("MicroMsg.SDK.MMessageAct", "send fail, ex = %s", e.getMessage());
                return false;
            }
        }
    }
}
