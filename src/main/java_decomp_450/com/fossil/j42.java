package com.fossil;

import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j42 {
    @DexIgnore
    void a(s62 s62, Set<Scope> set);

    @DexIgnore
    void b(i02 i02);
}
