package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.TextView;
import com.facebook.LegacyTokenHelper;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ig5 {
    /*
    static {
        String str = "Localization_" + ig5.class.getSimpleName();
    }
    */

    @DexIgnore
    public static Locale a() {
        return Resources.getSystem().getConfiguration().locale;
    }

    @DexIgnore
    public static String b() {
        return Resources.getSystem().getConfiguration().locale.toString();
    }

    @DexIgnore
    public static void a(TextView textView, int i) {
        if (textView != null && i != 0) {
            textView.setText(a(textView.getContext(), i));
        }
    }

    @DexIgnore
    public static String a(Context context, int i) {
        if (context == null || i == 0) {
            return "";
        }
        try {
            String resourceEntryName = context.getResources().getResourceEntryName(i);
            if (TextUtils.isEmpty(resourceEntryName)) {
                return "";
            }
            String d = hg5.b().d(resourceEntryName);
            return (d == null || d.isEmpty()) ? context.getResources().getString(i) : d;
        } catch (Resources.NotFoundException unused) {
            return "";
        }
    }

    @DexIgnore
    public static String a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str)) {
            String d = hg5.b().d(str);
            if (d != null && !d.isEmpty()) {
                return d;
            }
            try {
                int identifier = context.getResources().getIdentifier(str, LegacyTokenHelper.TYPE_STRING, context.getPackageName());
                if (identifier == 0) {
                    return str2;
                }
                return context.getResources().getString(identifier);
            } catch (Exception unused) {
            }
        }
        return str2;
    }
}
