package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow5 implements Factory<nw5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ ow5 a; // = new ow5();
    }

    @DexIgnore
    public static ow5 a() {
        return a.a;
    }

    @DexIgnore
    public static nw5 b() {
        return new nw5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nw5 get() {
        return b();
    }
}
