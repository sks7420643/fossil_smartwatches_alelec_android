package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ee7 {
    @DexIgnore
    public static int a(int i, int i2) {
        if (i < i2) {
            return -1;
        }
        return i == i2 ? 0 : 1;
    }

    @DexIgnore
    public static String a(String str, Object obj) {
        return str + obj;
    }

    @DexIgnore
    public static void b(Object obj, String str) {
        if (obj == null) {
            a(str);
            throw null;
        }
    }

    @DexIgnore
    public static void c(String str) {
        h97 h97 = new h97(str);
        a(h97);
        throw h97;
    }

    @DexIgnore
    public static void d(String str) {
        c("lateinit property " + str + " has not been initialized");
        throw null;
    }

    @DexIgnore
    public static void a() {
        l87 l87 = new l87();
        a(l87);
        throw l87;
    }

    @DexIgnore
    public static void b() {
        b("This function has a reified type parameter and thus can only be inlined at compilation time, not called directly.");
        throw null;
    }

    @DexIgnore
    public static void a(Object obj, String str) {
        if (obj == null) {
            IllegalStateException illegalStateException = new IllegalStateException(str + " must not be null");
            a(illegalStateException);
            throw illegalStateException;
        }
    }

    @DexIgnore
    public static void b(String str) {
        throw new UnsupportedOperationException(str);
    }

    @DexIgnore
    public static void a(String str) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
        String className = stackTraceElement.getClassName();
        String methodName = stackTraceElement.getMethodName();
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Parameter specified as non-null is null: method " + className + CodelessMatcher.CURRENT_CLASS_NAME + methodName + ", parameter " + str);
        a(illegalArgumentException);
        throw illegalArgumentException;
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @DexIgnore
    public static void a(int i, String str) {
        b();
        throw null;
    }

    @DexIgnore
    public static <T extends Throwable> T a(T t) {
        a((Throwable) t, ee7.class.getName());
        return t;
    }

    @DexIgnore
    public static <T extends Throwable> T a(T t, String str) {
        StackTraceElement[] stackTrace = t.getStackTrace();
        int length = stackTrace.length;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            if (str.equals(stackTrace[i2].getClassName())) {
                i = i2;
            }
        }
        t.setStackTrace((StackTraceElement[]) Arrays.copyOfRange(stackTrace, i + 1, length));
        return t;
    }
}
