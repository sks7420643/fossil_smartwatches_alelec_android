package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu0 implements Parcelable.Creator<km1> {
    @DexIgnore
    public /* synthetic */ gu0(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public km1 createFromParcel(Parcel parcel) {
        sx0 sx0 = sx0.c;
        Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
        if (readParcelable != null) {
            BluetoothDevice bluetoothDevice = (BluetoothDevice) readParcelable;
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                return sx0.a(bluetoothDevice, readString);
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public km1[] newArray(int i) {
        return new km1[i];
    }
}
