package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jz3<T> implements Comparator<T> {
    @DexIgnore
    public static /* final */ int LEFT_IS_GREATER; // = 1;
    @DexIgnore
    public static /* final */ int RIGHT_IS_GREATER; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jz3<Object> {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(0);
        @DexIgnore
        public /* final */ ConcurrentMap<Object, Integer> b;

        @DexIgnore
        public a() {
            vy3 vy3 = new vy3();
            lz3.a(vy3);
            this.b = vy3.f();
        }

        @DexIgnore
        public final Integer a(Object obj) {
            Integer num = this.b.get(obj);
            if (num != null) {
                return num;
            }
            Integer valueOf = Integer.valueOf(this.a.getAndIncrement());
            Integer putIfAbsent = this.b.putIfAbsent(obj, valueOf);
            return putIfAbsent != null ? putIfAbsent : valueOf;
        }

        @DexIgnore
        public int b(Object obj) {
            return System.identityHashCode(obj);
        }

        @DexIgnore
        @Override // com.fossil.jz3, java.util.Comparator
        public int compare(Object obj, Object obj2) {
            if (obj == obj2) {
                return 0;
            }
            if (obj == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            int b2 = b(obj);
            int b3 = b(obj2);
            if (b2 == b3) {
                int compareTo = a(obj).compareTo(a(obj2));
                if (compareTo != 0) {
                    return compareTo;
                }
                throw new AssertionError();
            } else if (b2 < b3) {
                return -1;
            } else {
                return 1;
            }
        }

        @DexIgnore
        public String toString() {
            return "Ordering.arbitrary()";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ jz3<Object> a; // = new a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ClassCastException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object value;

        @DexIgnore
        public c(Object obj) {
            super("Cannot compare value: " + obj);
            this.value = obj;
        }
    }

    @DexIgnore
    public static jz3<Object> allEqual() {
        return yw3.INSTANCE;
    }

    @DexIgnore
    public static jz3<Object> arbitrary() {
        return b.a;
    }

    @DexIgnore
    public static <T> jz3<T> explicit(List<T> list) {
        return new kx3(list);
    }

    @DexIgnore
    public static <T> jz3<T> from(Comparator<T> comparator) {
        return comparator instanceof jz3 ? (jz3) comparator : new dx3(comparator);
    }

    @DexIgnore
    public static <C extends Comparable> jz3<C> natural() {
        return fz3.INSTANCE;
    }

    @DexIgnore
    public static jz3<Object> usingToString() {
        return l04.INSTANCE;
    }

    @DexIgnore
    @Deprecated
    public int binarySearch(List<? extends T> list, T t) {
        return Collections.binarySearch(list, t, this);
    }

    @DexIgnore
    @Override // java.util.Comparator
    @CanIgnoreReturnValue
    public abstract int compare(T t, T t2);

    @DexIgnore
    public <U extends T> jz3<U> compound(Comparator<? super U> comparator) {
        jw3.a(comparator);
        return new ex3(this, comparator);
    }

    @DexIgnore
    public <E extends T> List<E> greatestOf(Iterable<E> iterable, int i) {
        return reverse().leastOf(iterable, i);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> zx3<E> immutableSortedCopy(Iterable<E> iterable) {
        Object[] c2 = py3.c(iterable);
        for (Object obj : c2) {
            jw3.a(obj);
        }
        Arrays.sort(c2, this);
        return zx3.asImmutableList(c2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.jz3<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean isOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        Object next = it.next();
        while (it.hasNext()) {
            Object next2 = it.next();
            if (compare(next, next2) > 0) {
                return false;
            }
            next = next2;
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.jz3<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean isStrictlyOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        Object next = it.next();
        while (it.hasNext()) {
            Object next2 = it.next();
            if (compare(next, next2) >= 0) {
                return false;
            }
            next = next2;
        }
        return true;
    }

    @DexIgnore
    public <E extends T> List<E> leastOf(Iterable<E> iterable, int i) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (((long) collection.size()) <= ((long) i) * 2) {
                Object[] array = collection.toArray();
                Arrays.sort(array, this);
                if (array.length > i) {
                    array = iz3.a(array, i);
                }
                return Collections.unmodifiableList(Arrays.asList(array));
            }
        }
        return leastOf(iterable.iterator(), i);
    }

    @DexIgnore
    public <S extends T> jz3<Iterable<S>> lexicographical() {
        return new ry3(this);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E max(Iterator<E> it) {
        E e;
        E next = it.next();
        while (it.hasNext()) {
            next = (E) max(e, it.next());
        }
        return e;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E min(Iterator<E> it) {
        E e;
        E next = it.next();
        while (it.hasNext()) {
            next = (E) min(e, it.next());
        }
        return e;
    }

    @DexIgnore
    public <S extends T> jz3<S> nullsFirst() {
        return new gz3(this);
    }

    @DexIgnore
    public <S extends T> jz3<S> nullsLast() {
        return new hz3(this);
    }

    @DexIgnore
    public <T2 extends T> jz3<Map.Entry<T2, ?>> onKeys() {
        return onResultOf(yy3.a());
    }

    @DexIgnore
    public <F> jz3<F> onResultOf(cw3<F, ? extends T> cw3) {
        return new ax3(cw3, this);
    }

    @DexIgnore
    public <S extends T> jz3<S> reverse() {
        return new vz3(this);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> List<E> sortedCopy(Iterable<E> iterable) {
        Object[] c2 = py3.c(iterable);
        Arrays.sort(c2, this);
        return uy3.a(Arrays.asList(c2));
    }

    @DexIgnore
    public static <T> jz3<T> compound(Iterable<? extends Comparator<? super T>> iterable) {
        return new ex3(iterable);
    }

    @DexIgnore
    public static <T> jz3<T> explicit(T t, T... tArr) {
        return explicit(uy3.a((Object) t, (Object[]) tArr));
    }

    @DexIgnore
    @Deprecated
    public static <T> jz3<T> from(jz3<T> jz3) {
        jw3.a(jz3);
        return jz3;
    }

    @DexIgnore
    public <E extends T> List<E> greatestOf(Iterator<E> it, int i) {
        return reverse().leastOf(it, i);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E max(Iterable<E> iterable) {
        return (E) max(iterable.iterator());
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E min(Iterable<E> iterable) {
        return (E) min(iterable.iterator());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: E extends T */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: E extends T */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public <E extends T> E max(E e, E e2) {
        return compare(e, e2) >= 0 ? e : e2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: E extends T */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: E extends T */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public <E extends T> E min(E e, E e2) {
        return compare(e, e2) <= 0 ? e : e2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E max(E e, E e2, E e3, E... eArr) {
        E e4;
        E e5 = (E) max(max(e, e2), e3);
        for (E e6 : eArr) {
            e5 = (E) max(e4, e6);
        }
        return e4;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <E extends T> E min(E e, E e2, E e3, E... eArr) {
        E e4;
        E e5 = (E) min(min(e, e2), e3);
        for (E e6 : eArr) {
            e5 = (E) min(e4, e6);
        }
        return e4;
    }

    @DexIgnore
    public <E extends T> List<E> leastOf(Iterator<E> it, int i) {
        jw3.a(it);
        bx3.a(i, "k");
        if (i == 0 || !it.hasNext()) {
            return zx3.of();
        }
        if (i >= 1073741823) {
            ArrayList a2 = uy3.a(it);
            Collections.sort(a2, this);
            if (a2.size() > i) {
                a2.subList(i, a2.size()).clear();
            }
            a2.trimToSize();
            return Collections.unmodifiableList(a2);
        }
        g04 a3 = g04.a(i, this);
        a3.a((Iterator) it);
        return a3.a();
    }
}
