package com.fossil;

import com.fossil.ai7;
import com.fossil.bm7;
import com.fossil.s87;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ln7 implements kn7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(ln7.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends b {
        @DexIgnore
        public /* final */ ai7<i97> e;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ai7<? super com.fossil.i97> */
        /* JADX WARN: Multi-variable type inference failed */
        public a(Object obj, ai7<? super i97> ai7) {
            super(obj);
            this.e = ai7;
        }

        @DexIgnore
        @Override // com.fossil.ln7.b
        public void a(Object obj) {
            this.e.b(obj);
        }

        @DexIgnore
        @Override // com.fossil.ln7.b
        public Object k() {
            return ai7.a.a(this.e, i97.a, null, 2, null);
        }

        @DexIgnore
        @Override // com.fossil.bm7
        public String toString() {
            return "LockCont[" + ((b) this).d + ", " + this.e + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b extends bm7 implements rj7 {
        @DexIgnore
        public /* final */ Object d;

        @DexIgnore
        public b(Object obj) {
            this.d = obj;
        }

        @DexIgnore
        public abstract void a(Object obj);

        @DexIgnore
        @Override // com.fossil.rj7
        public final void dispose() {
            g();
        }

        @DexIgnore
        public abstract Object k();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zl7 {
        @DexIgnore
        public Object d;

        @DexIgnore
        public c(Object obj) {
            this.d = obj;
        }

        @DexIgnore
        @Override // com.fossil.bm7
        public String toString() {
            return "LockedQueue[" + this.d + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends hm7 {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public d(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.hm7
        public ul7<?> a() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.hm7
        public Object a(Object obj) {
            Object b = this.a.k() ? mn7.e : this.a;
            if (obj != null) {
                ln7 ln7 = (ln7) obj;
                ln7.a.compareAndSet(ln7, this, b);
                if (ln7._state == this.a) {
                    return mn7.a;
                }
                return null;
            }
            throw new x87("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends bm7.a {
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ ln7 e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(bm7 bm7, bm7 bm72, Object obj, ai7 ai7, a aVar, ln7 ln7, Object obj2) {
            super(bm72);
            this.d = obj;
            this.e = ln7;
            this.f = obj2;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(bm7 bm7) {
            if (this.e._state == this.d) {
                return null;
            }
            return am7.a();
        }
    }

    @DexIgnore
    public ln7(boolean z) {
        this._state = z ? mn7.d : mn7.e;
    }

    @DexIgnore
    @Override // com.fossil.kn7
    public Object a(Object obj, fb7<? super i97> fb7) {
        if (b(obj)) {
            return i97.a;
        }
        Object b2 = b(obj, fb7);
        return b2 == nb7.a() ? b2 : i97.a;
    }

    @DexIgnore
    public boolean b(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof jn7) {
                if (((jn7) obj2).a != mn7.c) {
                    return false;
                }
                if (a.compareAndSet(this, obj2, obj == null ? mn7.d : new jn7(obj))) {
                    return true;
                }
            } else if (obj2 instanceof c) {
                if (((c) obj2).d == obj) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                throw new IllegalStateException(("Already locked by " + obj).toString());
            } else if (obj2 instanceof hm7) {
                ((hm7) obj2).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof jn7) {
                return "Mutex[" + ((jn7) obj).a + ']';
            } else if (obj instanceof hm7) {
                ((hm7) obj).a(this);
            } else if (obj instanceof c) {
                return "Mutex[" + ((c) obj).d + ']';
            } else {
                throw new IllegalStateException(("Illegal state " + obj).toString());
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.kn7
    public void a(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof jn7) {
                if (obj == null) {
                    if (((jn7) obj2).a == mn7.c) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    jn7 jn7 = (jn7) obj2;
                    if (jn7.a != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + jn7.a + " but expected " + obj).toString());
                    }
                }
                if (a.compareAndSet(this, obj2, mn7.e)) {
                    return;
                }
            } else if (obj2 instanceof hm7) {
                ((hm7) obj2).a(this);
            } else if (obj2 instanceof c) {
                if (obj != null) {
                    c cVar = (c) obj2;
                    if (cVar.d != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + cVar.d + " but expected " + obj).toString());
                    }
                }
                c cVar2 = (c) obj2;
                bm7 h = cVar2.h();
                if (h == null) {
                    d dVar = new d(cVar2);
                    if (a.compareAndSet(this, obj2, dVar) && dVar.a(this) == null) {
                        return;
                    }
                } else {
                    b bVar = (b) h;
                    Object k = bVar.k();
                    if (k != null) {
                        Object obj3 = bVar.d;
                        if (obj3 == null) {
                            obj3 = mn7.b;
                        }
                        cVar2.d = obj3;
                        bVar.a(k);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Object b(Object obj, fb7<? super i97> fb7) {
        bi7 a2 = di7.a(mb7.a(fb7));
        a aVar = new a(obj, a2);
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof jn7) {
                jn7 jn7 = (jn7) obj2;
                if (jn7.a != mn7.c) {
                    a.compareAndSet(this, obj2, new c(jn7.a));
                } else {
                    if (a.compareAndSet(this, obj2, obj == null ? mn7.d : new jn7(obj))) {
                        i97 i97 = i97.a;
                        s87.a aVar2 = s87.Companion;
                        a2.resumeWith(s87.m60constructorimpl(i97));
                        break;
                    }
                }
            } else if (obj2 instanceof c) {
                c cVar = (c) obj2;
                boolean z = false;
                if (cVar.d != obj) {
                    e eVar = new e(aVar, aVar, obj2, a2, aVar, this, obj);
                    while (true) {
                        int a3 = cVar.d().a(aVar, cVar, eVar);
                        if (a3 != 1) {
                            if (a3 == 2) {
                                break;
                            }
                        } else {
                            z = true;
                            break;
                        }
                    }
                    if (z) {
                        di7.a((ai7<?>) a2, (bm7) aVar);
                        break;
                    }
                } else {
                    throw new IllegalStateException(("Already locked by " + obj).toString());
                }
            } else if (obj2 instanceof hm7) {
                ((hm7) obj2).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
        Object g = a2.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }
}
