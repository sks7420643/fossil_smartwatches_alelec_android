package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r33 implements o33 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.scheduler.task_thread.cleanup_on_exit", false);

    @DexIgnore
    @Override // com.fossil.o33
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
