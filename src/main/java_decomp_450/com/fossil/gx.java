package com.fossil;

import android.content.res.AssetManager;
import android.util.Log;
import com.fossil.ix;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gx<T> implements ix<T> {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ AssetManager b;
    @DexIgnore
    public T c;

    @DexIgnore
    public gx(AssetManager assetManager, String str) {
        this.b = assetManager;
        this.a = str;
    }

    @DexIgnore
    public abstract T a(AssetManager assetManager, String str) throws IOException;

    @DexIgnore
    @Override // com.fossil.ix
    public void a(ew ewVar, ix.a<? super T> aVar) {
        try {
            T a2 = a(this.b, this.a);
            this.c = a2;
            aVar.a((Object) a2);
        } catch (IOException e) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e);
            }
            aVar.a((Exception) e);
        }
    }

    @DexIgnore
    public abstract void a(T t) throws IOException;

    @DexIgnore
    @Override // com.fossil.ix
    public sw b() {
        return sw.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void a() {
        T t = this.c;
        if (t != null) {
            try {
                a(t);
            } catch (IOException unused) {
            }
        }
    }
}
