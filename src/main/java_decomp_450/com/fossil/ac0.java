package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ac0 extends yb0 {
    @DexIgnore
    public /* final */ rg0 d;

    @DexIgnore
    public ac0(cb0 cb0, byte b, rg0 rg0) {
        super(cb0, b, yz0.b(rg0.getRequestId()));
        this.d = rg0;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        JSONObject a = super.a();
        try {
            yz0.a(a, r51.t3, this.d);
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return a;
    }

    @DexIgnore
    public final rg0 d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(getClass(), obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.d, ((ac0) obj).d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public ac0(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(rg0.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (rg0) readParcelable;
        } else {
            ee7.a();
            throw null;
        }
    }
}
