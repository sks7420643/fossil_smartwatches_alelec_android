package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr4 extends he {
    @DexIgnore
    public /* final */ fp4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        ee7.a((Object) yr4.class.getSimpleName(), "BCTabsViewModel::class.java.simpleName");
    }
    */

    @DexIgnore
    public yr4(ch5 ch5, fp4 fp4) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(fp4, "mSocialProfileRepository");
        this.a = fp4;
        new MutableLiveData();
        this.a.c();
    }
}
