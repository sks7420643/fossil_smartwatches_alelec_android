package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pe1 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ vc1 CREATOR; // = new vc1(null);
    @DexIgnore
    public /* final */ zp1 a;

    @DexIgnore
    public pe1(zp1 zp1) {
        this.a = zp1;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(new JSONObject(), r51.O3, yz0.a(this.a));
    }

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((pe1) obj).a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.Instruction");
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.a.a);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public pe1(android.os.Parcel r2) {
        /*
            r1 = this;
            java.lang.String r2 = r2.readString()
            if (r2 == 0) goto L_0x0013
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.ee7.a(r2, r0)
            com.fossil.zp1 r2 = com.fossil.zp1.valueOf(r2)
            r1.<init>(r2)
            return
        L_0x0013:
            com.fossil.ee7.a()
            r2 = 0
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pe1.<init>(android.os.Parcel):void");
    }
}
