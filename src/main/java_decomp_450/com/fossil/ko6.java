package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko6 {
    @DexIgnore
    public /* final */ cl5 a;
    @DexIgnore
    public /* final */ io6 b;

    @DexIgnore
    public ko6(cl5 cl5, io6 io6) {
        ee7.b(cl5, "baseActivity");
        ee7.b(io6, "mView");
        this.a = cl5;
        this.b = io6;
    }

    @DexIgnore
    public final cl5 a() {
        return this.a;
    }

    @DexIgnore
    public final io6 b() {
        return this.b;
    }
}
