package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ib7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ib7$a$a")
        /* renamed from: com.fossil.ib7$a$a  reason: collision with other inner class name */
        public static final class C0081a extends fe7 implements kd7<ib7, b, ib7> {
            @DexIgnore
            public static /* final */ C0081a INSTANCE; // = new C0081a();

            @DexIgnore
            public C0081a() {
                super(2);
            }

            @DexIgnore
            public final ib7 invoke(ib7 ib7, b bVar) {
                ee7.b(ib7, "acc");
                ee7.b(bVar, "element");
                ib7 minusKey = ib7.minusKey(bVar.getKey());
                if (minusKey == jb7.INSTANCE) {
                    return bVar;
                }
                gb7 gb7 = (gb7) minusKey.get(gb7.m);
                if (gb7 == null) {
                    return new eb7(minusKey, bVar);
                }
                ib7 minusKey2 = minusKey.minusKey(gb7.m);
                if (minusKey2 == jb7.INSTANCE) {
                    return new eb7(bVar, gb7);
                }
                return new eb7(new eb7(minusKey2, bVar), gb7);
            }
        }

        @DexIgnore
        public static ib7 a(ib7 ib7, ib7 ib72) {
            ee7.b(ib72, "context");
            return ib72 == jb7.INSTANCE ? ib7 : (ib7) ib72.fold(ib7, C0081a.INSTANCE);
        }
    }

    @DexIgnore
    public interface b extends ib7 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.ib7$b */
            /* JADX WARN: Multi-variable type inference failed */
            public static <E extends b> E a(b bVar, c<E> cVar) {
                ee7.b(cVar, "key");
                if (!ee7.a(bVar.getKey(), cVar)) {
                    return null;
                }
                if (bVar != 0) {
                    return bVar;
                }
                throw new x87("null cannot be cast to non-null type E");
            }

            @DexIgnore
            public static ib7 a(b bVar, ib7 ib7) {
                ee7.b(ib7, "context");
                return a.a(bVar, ib7);
            }

            @DexIgnore
            public static ib7 b(b bVar, c<?> cVar) {
                ee7.b(cVar, "key");
                return ee7.a(bVar.getKey(), cVar) ? jb7.INSTANCE : bVar;
            }

            @DexIgnore
            public static <R> R a(b bVar, R r, kd7<? super R, ? super b, ? extends R> kd7) {
                ee7.b(kd7, "operation");
                return (R) kd7.invoke(r, bVar);
            }
        }

        @DexIgnore
        @Override // com.fossil.ib7
        <E extends b> E get(c<E> cVar);

        @DexIgnore
        c<?> getKey();
    }

    @DexIgnore
    public interface c<E extends b> {
    }

    @DexIgnore
    <R> R fold(R r, kd7<? super R, ? super b, ? extends R> kd7);

    @DexIgnore
    <E extends b> E get(c<E> cVar);

    @DexIgnore
    ib7 minusKey(c<?> cVar);

    @DexIgnore
    ib7 plus(ib7 ib7);
}
