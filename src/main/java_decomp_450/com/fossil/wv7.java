package com.fossil;

import com.fossil.tu7;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv7 extends tu7.a {
    @DexIgnore
    public static wv7 a() {
        return new wv7();
    }

    @DexIgnore
    @Override // com.fossil.tu7.a
    public tu7<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (type == String.class || type == Boolean.TYPE || type == Boolean.class || type == Byte.TYPE || type == Byte.class || type == Character.TYPE || type == Character.class || type == Double.TYPE || type == Double.class || type == Float.TYPE || type == Float.class || type == Integer.TYPE || type == Integer.class || type == Long.TYPE || type == Long.class || type == Short.TYPE || type == Short.class) {
            return mv7.a;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.tu7.a
    public tu7<mo7, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == String.class) {
            return vv7.a;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return nv7.a;
        }
        if (type == Byte.class || type == Byte.TYPE) {
            return ov7.a;
        }
        if (type == Character.class || type == Character.TYPE) {
            return pv7.a;
        }
        if (type == Double.class || type == Double.TYPE) {
            return qv7.a;
        }
        if (type == Float.class || type == Float.TYPE) {
            return rv7.a;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return sv7.a;
        }
        if (type == Long.class || type == Long.TYPE) {
            return tv7.a;
        }
        if (type == Short.class || type == Short.TYPE) {
            return uv7.a;
        }
        return null;
    }
}
