package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nq2 implements lq2 {
    @DexIgnore
    public /* final */ oq2 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public nq2(oq2 oq2, String str) {
        this.a = oq2;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.lq2
    public final Object zza() {
        return this.a.a(this.b);
    }
}
