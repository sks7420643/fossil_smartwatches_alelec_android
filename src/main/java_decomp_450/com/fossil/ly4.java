package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ly4 extends ky4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362562, 1);
        F.put(2131362517, 2);
        F.put(2131363365, 3);
        F.put(2131361902, 4);
        F.put(2131362743, 5);
        F.put(2131362125, 6);
        F.put(2131362755, 7);
        F.put(2131362711, 8);
        F.put(2131362751, 9);
        F.put(2131362333, 10);
        F.put(2131362785, 11);
        F.put(2131362999, 12);
    }
    */

    @DexIgnore
    public ly4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 13, E, F));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public ly4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleAutoCompleteTextView) objArr[4], (ImageView) objArr[6], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (ImageView) objArr[8], (ConstraintLayout) objArr[5], (View) objArr[9], (ImageView) objArr[7], (LinearLayout) objArr[11], (ConstraintLayout) objArr[0], (RecyclerView) objArr[12], (View) objArr[3]);
        this.D = -1;
        ((ky4) this).A.setTag(null);
        a(view);
        f();
    }
}
