package com.fossil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p57 {
    @DexIgnore
    public ExecutorService a;

    @DexIgnore
    public p57() {
        this.a = null;
        this.a = Executors.newSingleThreadExecutor();
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.execute(runnable);
    }
}
