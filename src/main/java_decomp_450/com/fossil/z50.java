package com.fossil;

import android.util.Log;
import com.crashlytics.android.answers.Answers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z50 implements c60 {
    @DexIgnore
    public /* final */ Answers a;

    @DexIgnore
    public z50(Answers answers) {
        this.a = answers;
    }

    @DexIgnore
    public static z50 a() throws NoClassDefFoundError, IllegalStateException {
        return a(Answers.getInstance());
    }

    @DexIgnore
    public static z50 a(Answers answers) throws IllegalStateException {
        if (answers != null) {
            return new z50(answers);
        }
        throw new IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    @Override // com.fossil.c60
    public void a(b60 b60) {
        try {
            this.a.logCustom(b60.a());
        } catch (Throwable th) {
            Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
