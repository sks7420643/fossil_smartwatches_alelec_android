package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc6 implements Factory<wc6> {
    @DexIgnore
    public static wc6 a(lc6 lc6) {
        wc6 c = lc6.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
