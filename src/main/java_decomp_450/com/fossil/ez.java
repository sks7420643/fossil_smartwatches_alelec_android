package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ez implements dz {
    @DexIgnore
    @Override // com.fossil.dz
    public void a() {
    }

    @DexIgnore
    @Override // com.fossil.dz
    public void a(int i) {
    }

    @DexIgnore
    @Override // com.fossil.dz
    public void a(Bitmap bitmap) {
        bitmap.recycle();
    }

    @DexIgnore
    @Override // com.fossil.dz
    public Bitmap b(int i, int i2, Bitmap.Config config) {
        return a(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.dz
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }
}
