package com.fossil;

import android.util.Log;
import com.fossil.r37;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q37 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ r37.a a;

    @DexIgnore
    public q37(r37.a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public void run() {
        if (r37.e != null && !this.a.a) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onForeground");
            y37.a(this.a.c, "onForeground_WX", (Properties) null);
            boolean unused = this.a.a = true;
        }
    }
}
