package com.fossil;

import java.util.List;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm7 {
    @DexIgnore
    public static /* final */ boolean a; // = true;

    @DexIgnore
    public static final tk7 a(MainDispatcherFactory mainDispatcherFactory, List<? extends MainDispatcherFactory> list) {
        try {
            return mainDispatcherFactory.a(list);
        } catch (Throwable th) {
            return a(th, mainDispatcherFactory.a());
        }
    }

    @DexIgnore
    public static /* synthetic */ gm7 a(Throwable th, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            th = null;
        }
        if ((i & 2) != 0) {
            str = null;
        }
        return a(th, str);
    }

    @DexIgnore
    public static final gm7 a(Throwable th, String str) {
        if (a) {
            return new gm7(th, str);
        }
        if (th != null) {
            throw th;
        }
        a();
        throw null;
    }

    @DexIgnore
    public static final Void a() {
        throw new IllegalStateException("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android' and ensure it has the same version as 'kotlinx-coroutines-core'");
    }
}
