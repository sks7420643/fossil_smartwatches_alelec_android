package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class tn0 extends Enum<tn0> {
    @DexIgnore
    public static /* final */ tn0 b;
    @DexIgnore
    public static /* final */ /* synthetic */ tn0[] c;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        tn0 tn0 = new tn0("ERROR", 2, (byte) 2);
        b = tn0;
        c = new tn0[]{new tn0("NONE", 0, (byte) 0), new tn0("OPEN", 1, (byte) 1), tn0, new tn0("READY", 3, (byte) 3), new tn0("START", 4, (byte) 4), new tn0("STOP", 5, (byte) 5), new tn0("RESET", 6, (byte) 6)};
    }
    */

    @DexIgnore
    public tn0(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static tn0 valueOf(String str) {
        return (tn0) Enum.valueOf(tn0.class, str);
    }

    @DexIgnore
    public static tn0[] values() {
        return (tn0[]) c.clone();
    }
}
