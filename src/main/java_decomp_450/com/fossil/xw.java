package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream a;

        @DexIgnore
        public a(InputStream inputStream) {
            this.a = inputStream;
        }

        @DexIgnore
        @Override // com.fossil.xw.g
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            try {
                return imageHeaderParser.a(this.a);
            } finally {
                this.a.reset();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements g {
        @DexIgnore
        public /* final */ /* synthetic */ ByteBuffer a;

        @DexIgnore
        public b(ByteBuffer byteBuffer) {
            this.a = byteBuffer;
        }

        @DexIgnore
        @Override // com.fossil.xw.g
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            return imageHeaderParser.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements g {
        @DexIgnore
        public /* final */ /* synthetic */ rx a;
        @DexIgnore
        public /* final */ /* synthetic */ az b;

        @DexIgnore
        public c(rx rxVar, az azVar) {
            this.a = rxVar;
            this.b = azVar;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x002a A[SYNTHETIC, Splitter:B:14:0x002a] */
        @Override // com.fossil.xw.g
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.bumptech.glide.load.ImageHeaderParser.ImageType a(com.bumptech.glide.load.ImageHeaderParser r5) throws java.io.IOException {
            /*
                r4 = this;
                r0 = 0
                com.fossil.c20 r1 = new com.fossil.c20     // Catch:{ all -> 0x0027 }
                java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0027 }
                com.fossil.rx r3 = r4.a     // Catch:{ all -> 0x0027 }
                android.os.ParcelFileDescriptor r3 = r3.b()     // Catch:{ all -> 0x0027 }
                java.io.FileDescriptor r3 = r3.getFileDescriptor()     // Catch:{ all -> 0x0027 }
                r2.<init>(r3)     // Catch:{ all -> 0x0027 }
                com.fossil.az r3 = r4.b     // Catch:{ all -> 0x0027 }
                r1.<init>(r2, r3)     // Catch:{ all -> 0x0027 }
                com.bumptech.glide.load.ImageHeaderParser$ImageType r5 = r5.a(r1)     // Catch:{ all -> 0x0024 }
                r1.close()     // Catch:{ IOException -> 0x001e }
            L_0x001e:
                com.fossil.rx r0 = r4.a
                r0.b()
                return r5
            L_0x0024:
                r5 = move-exception
                r0 = r1
                goto L_0x0028
            L_0x0027:
                r5 = move-exception
            L_0x0028:
                if (r0 == 0) goto L_0x002d
                r0.close()     // Catch:{ IOException -> 0x002d }
            L_0x002d:
                com.fossil.rx r0 = r4.a
                r0.b()
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xw.c.a(com.bumptech.glide.load.ImageHeaderParser):com.bumptech.glide.load.ImageHeaderParser$ImageType");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements f {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream a;
        @DexIgnore
        public /* final */ /* synthetic */ az b;

        @DexIgnore
        public d(InputStream inputStream, az azVar) {
            this.a = inputStream;
            this.b = azVar;
        }

        @DexIgnore
        @Override // com.fossil.xw.f
        public int a(ImageHeaderParser imageHeaderParser) throws IOException {
            try {
                return imageHeaderParser.a(this.a, this.b);
            } finally {
                this.a.reset();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements f {
        @DexIgnore
        public /* final */ /* synthetic */ rx a;
        @DexIgnore
        public /* final */ /* synthetic */ az b;

        @DexIgnore
        public e(rx rxVar, az azVar) {
            this.a = rxVar;
            this.b = azVar;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x002c A[SYNTHETIC, Splitter:B:14:0x002c] */
        @Override // com.fossil.xw.f
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int a(com.bumptech.glide.load.ImageHeaderParser r5) throws java.io.IOException {
            /*
                r4 = this;
                r0 = 0
                com.fossil.c20 r1 = new com.fossil.c20     // Catch:{ all -> 0x0029 }
                java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0029 }
                com.fossil.rx r3 = r4.a     // Catch:{ all -> 0x0029 }
                android.os.ParcelFileDescriptor r3 = r3.b()     // Catch:{ all -> 0x0029 }
                java.io.FileDescriptor r3 = r3.getFileDescriptor()     // Catch:{ all -> 0x0029 }
                r2.<init>(r3)     // Catch:{ all -> 0x0029 }
                com.fossil.az r3 = r4.b     // Catch:{ all -> 0x0029 }
                r1.<init>(r2, r3)     // Catch:{ all -> 0x0029 }
                com.fossil.az r0 = r4.b     // Catch:{ all -> 0x0026 }
                int r5 = r5.a(r1, r0)     // Catch:{ all -> 0x0026 }
                r1.close()     // Catch:{ IOException -> 0x0020 }
            L_0x0020:
                com.fossil.rx r0 = r4.a
                r0.b()
                return r5
            L_0x0026:
                r5 = move-exception
                r0 = r1
                goto L_0x002a
            L_0x0029:
                r5 = move-exception
            L_0x002a:
                if (r0 == 0) goto L_0x002f
                r0.close()     // Catch:{ IOException -> 0x002f }
            L_0x002f:
                com.fossil.rx r0 = r4.a
                r0.b()
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xw.e.a(com.bumptech.glide.load.ImageHeaderParser):int");
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        int a(ImageHeaderParser imageHeaderParser) throws IOException;
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException;
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType a(List<ImageHeaderParser> list, ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        return a(list, new b(byteBuffer));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType b(List<ImageHeaderParser> list, InputStream inputStream, az azVar) throws IOException {
        if (inputStream == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        if (!inputStream.markSupported()) {
            inputStream = new c20(inputStream, azVar);
        }
        inputStream.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
        return a(list, new a(inputStream));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType a(List<ImageHeaderParser> list, g gVar) throws IOException {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ImageHeaderParser.ImageType a2 = gVar.a(list.get(i));
            if (a2 != ImageHeaderParser.ImageType.UNKNOWN) {
                return a2;
            }
        }
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType b(List<ImageHeaderParser> list, rx rxVar, az azVar) throws IOException {
        return a(list, new c(rxVar, azVar));
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, InputStream inputStream, az azVar) throws IOException {
        if (inputStream == null) {
            return -1;
        }
        if (!inputStream.markSupported()) {
            inputStream = new c20(inputStream, azVar);
        }
        inputStream.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
        return a(list, new d(inputStream, azVar));
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, rx rxVar, az azVar) throws IOException {
        return a(list, new e(rxVar, azVar));
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, f fVar) throws IOException {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            int a2 = fVar.a(list.get(i));
            if (a2 != -1) {
                return a2;
            }
        }
        return -1;
    }
}
