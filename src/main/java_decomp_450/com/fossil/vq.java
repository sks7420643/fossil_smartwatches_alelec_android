package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq implements uq {
    @DexIgnore
    public static /* final */ Bitmap.Config[] d;
    @DexIgnore
    public static /* final */ Bitmap.Config[] e;
    @DexIgnore
    public static /* final */ Bitmap.Config[] f; // = {Bitmap.Config.RGB_565};
    @DexIgnore
    public static /* final */ Bitmap.Config[] g; // = {Bitmap.Config.ARGB_4444};
    @DexIgnore
    public static /* final */ Bitmap.Config[] h; // = {Bitmap.Config.ALPHA_8};
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public /* final */ yq<b, Bitmap> b; // = new yq<>();
    @DexIgnore
    public /* final */ HashMap<Bitmap.Config, NavigableMap<Integer, Integer>> c; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Bitmap.Config b;

        @DexIgnore
        public b(int i, Bitmap.Config config) {
            ee7.b(config, "config");
            this.a = i;
            this.b = config;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && ee7.a(this.b, bVar.b);
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a * 31;
            Bitmap.Config config = this.b;
            return i + (config != null ? config.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            a aVar = vq.i;
            int i = this.a;
            Bitmap.Config config = this.b;
            return '[' + i + "](" + config + ')';
        }
    }

    /*
    static {
        Bitmap.Config[] configArr = Build.VERSION.SDK_INT >= 26 ? new Bitmap.Config[]{Bitmap.Config.ARGB_8888, Bitmap.Config.RGBA_F16} : new Bitmap.Config[]{Bitmap.Config.ARGB_8888};
        d = configArr;
        e = configArr;
    }
    */

    @DexIgnore
    @Override // com.fossil.uq
    public void a(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int a2 = iu.a(bitmap);
        Bitmap.Config config = bitmap.getConfig();
        ee7.a((Object) config, "bitmap.config");
        b bVar = new b(a2, config);
        this.b.a(bVar, bitmap);
        NavigableMap<Integer, Integer> b2 = b(bitmap.getConfig());
        Integer num = (Integer) b2.get(Integer.valueOf(bVar.a()));
        Integer valueOf = Integer.valueOf(bVar.a());
        int i2 = 1;
        if (num != null) {
            i2 = 1 + num.intValue();
        }
        b2.put(valueOf, Integer.valueOf(i2));
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> b(Bitmap.Config config) {
        HashMap<Bitmap.Config, NavigableMap<Integer, Integer>> hashMap = this.c;
        NavigableMap<Integer, Integer> navigableMap = hashMap.get(config);
        if (navigableMap == null) {
            navigableMap = new TreeMap<>();
            hashMap.put(config, navigableMap);
        }
        return navigableMap;
    }

    @DexIgnore
    @Override // com.fossil.uq
    public Bitmap removeLast() {
        Bitmap a2 = this.b.a();
        if (a2 != null) {
            a(iu.a(a2), a2);
        }
        return a2;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SizeConfigStrategy: groupedMap=");
        sb.append(this.b);
        sb.append(", sortedSizes=(");
        for (Map.Entry<Bitmap.Config, NavigableMap<Integer, Integer>> entry : this.c.entrySet()) {
            sb.append(entry.getKey());
            sb.append("[");
            sb.append(entry.getValue());
            sb.append("], ");
        }
        if (!this.c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")");
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.fossil.uq
    public Bitmap a(int i2, int i3, Bitmap.Config config) {
        ee7.b(config, "config");
        b a2 = a(ku.a.a(i2, i3, config), config);
        Bitmap a3 = this.b.a(a2);
        if (a3 != null) {
            a(a2.a(), a3);
            a3.reconfigure(i2, i3, config);
        }
        return a3;
    }

    @DexIgnore
    @Override // com.fossil.uq
    public String b(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int a2 = iu.a(bitmap);
        Bitmap.Config config = bitmap.getConfig();
        ee7.a((Object) config, "bitmap.config");
        return '[' + a2 + "](" + config + ')';
    }

    @DexIgnore
    @Override // com.fossil.uq
    public String b(int i2, int i3, Bitmap.Config config) {
        ee7.b(config, "config");
        int a2 = ku.a.a(i2, i3, config);
        return '[' + a2 + "](" + config + ')';
    }

    @DexIgnore
    public final b a(int i2, Bitmap.Config config) {
        Bitmap.Config[] a2 = a(config);
        for (Bitmap.Config config2 : a2) {
            Integer ceilingKey = b(config2).ceilingKey(Integer.valueOf(i2));
            if (ceilingKey != null && ceilingKey.intValue() <= i2 * 8) {
                return new b(ceilingKey.intValue(), config2);
            }
        }
        return new b(i2, config);
    }

    @DexIgnore
    public final void a(int i2, Bitmap bitmap) {
        NavigableMap<Integer, Integer> b2 = b(bitmap.getConfig());
        Object obj = b2.get(Integer.valueOf(i2));
        if (obj != null) {
            int intValue = ((Number) obj).intValue();
            if (intValue == 1) {
                b2.remove(Integer.valueOf(i2));
            } else {
                b2.put(Integer.valueOf(i2), Integer.valueOf(intValue - 1));
            }
        } else {
            throw new IllegalStateException(("Tried to decrement empty size, size: " + i2 + ", removed: " + b(bitmap) + ", this: " + this).toString());
        }
    }

    @DexIgnore
    public final Bitmap.Config[] a(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && Bitmap.Config.RGBA_F16 == config) {
            return e;
        }
        if (config == Bitmap.Config.ARGB_8888) {
            return d;
        }
        if (config == Bitmap.Config.RGB_565) {
            return f;
        }
        if (config == Bitmap.Config.ARGB_4444) {
            return g;
        }
        if (config == Bitmap.Config.ALPHA_8) {
            return h;
        }
        return new Bitmap.Config[]{config};
    }
}
