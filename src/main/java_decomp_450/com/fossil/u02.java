package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u02 extends Exception {
    @DexIgnore
    public /* final */ Intent mIntent;

    @DexIgnore
    public u02(String str, Intent intent) {
        super(str);
        this.mIntent = intent;
    }

    @DexIgnore
    public Intent getIntent() {
        return new Intent(this.mIntent);
    }
}
