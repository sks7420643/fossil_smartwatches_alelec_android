package com.fossil;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.wo5;
import com.fossil.yo5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k06 extends go5 implements j06, cy6.g {
    @DexIgnore
    public qw6<sy4> f;
    @DexIgnore
    public i06 g;
    @DexIgnore
    public wo5 h;
    @DexIgnore
    public yo5 i;
    @DexIgnore
    public rj4 j;
    @DexIgnore
    public a06 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements yo5.c {
        @DexIgnore
        public /* final */ /* synthetic */ k06 a;

        @DexIgnore
        public b(k06 k06) {
            this.a = k06;
        }

        @DexIgnore
        @Override // com.fossil.yo5.c
        public void a(Complication complication) {
            ee7.b(complication, "complication");
            k06.a(this.a).b(complication.getComplicationId());
        }

        @DexIgnore
        @Override // com.fossil.yo5.c
        public void b(Complication complication) {
            ee7.b(complication, "complication");
            xg5.a(xg5.b, this.a.getContext(), ve5.a.a(complication.getComplicationId()), false, false, false, (Integer) null, 60, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements wo5.c {
        @DexIgnore
        public /* final */ /* synthetic */ k06 a;

        @DexIgnore
        public c(k06 k06) {
            this.a = k06;
        }

        @DexIgnore
        @Override // com.fossil.wo5.c
        public void a() {
            k06.a(this.a).h();
        }

        @DexIgnore
        @Override // com.fossil.wo5.c
        public void a(Category category) {
            ee7.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "onItemClicked category=" + category);
            k06.a(this.a).a(category.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ k06 a;

        @DexIgnore
        public d(k06 k06) {
            this.a = k06;
        }

        @DexIgnore
        public final void onClick(View view) {
            k06.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sy4 a;
        @DexIgnore
        public /* final */ /* synthetic */ k06 b;

        @DexIgnore
        public e(sy4 sy4, k06 k06) {
            this.a = sy4;
            this.b = k06;
        }

        @DexIgnore
        public final void onClick(View view) {
            i06 a2 = k06.a(this.b);
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.w;
            ee7.a((Object) flexibleSwitchCompat, "binding.switchRing");
            a2.a(flexibleSwitchCompat.isChecked());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ i06 a(k06 k06) {
        i06 i06 = k06.g;
        if (i06 != null) {
            return i06;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void b(Complication complication) {
        if (complication != null) {
            yo5 yo5 = this.i;
            if (yo5 != null) {
                yo5.b(complication.getComplicationId());
                c(complication);
                return;
            }
            ee7.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(Complication complication) {
        qw6<sy4> qw6 = this.f;
        if (qw6 != null) {
            sy4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.B;
                ee7.a((Object) flexibleTextView, "binding.tvSelectedComplication");
                flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), complication.getNameKey(), complication.getName()));
                FlexibleTextView flexibleTextView2 = a2.x;
                ee7.a((Object) flexibleTextView2, "binding.tvComplicationDetail");
                flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), complication.getDescriptionKey(), complication.getDescription()));
                yo5 yo5 = this.i;
                if (yo5 != null) {
                    int a3 = yo5.a(complication.getComplicationId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "updateDetailComplication complicationId=" + complication.getComplicationId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.v.scrollToPosition(a3);
                        return;
                    }
                    return;
                }
                ee7.d("mComplicationAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void d(String str) {
        ee7.b(str, "category");
        qw6<sy4> qw6 = this.f;
        if (qw6 != null) {
            sy4 a2 = qw6.a();
            if (a2 != null) {
                wo5 wo5 = this.h;
                if (wo5 != null) {
                    int a3 = wo5.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        wo5 wo52 = this.h;
                        if (wo52 != null) {
                            wo52.a(a3);
                            a2.u.smoothScrollToPosition(a3);
                            return;
                        }
                        ee7.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                ee7.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ComplicationsFragment";
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void e(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.z.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final void f1() {
        if (isActive()) {
            yo5 yo5 = this.i;
            if (yo5 != null) {
                yo5.c();
            } else {
                ee7.d("mComplicationAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void h(List<Complication> list) {
        ee7.b(list, "complications");
        yo5 yo5 = this.i;
        if (yo5 != null) {
            yo5.a(list);
        } else {
            ee7.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            rj4 rj4 = this.j;
            if (rj4 != null) {
                he a2 = je.a(dianaCustomizeEditActivity, rj4).a(a06.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                a06 a06 = (a06) a2;
                this.p = a06;
                i06 i06 = this.g;
                if (i06 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (a06 != null) {
                    i06.a(a06);
                } else {
                    ee7.d("mShareViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        CommuteTimeSetting commuteTimeSetting;
        super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onActivityResult requestCode " + i2);
        if (i2 != 100) {
            if (i2 != 102) {
                if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                    i06 i06 = this.g;
                    if (i06 != null) {
                        i06.a("commute-time", commuteTimeSetting);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_COMPLICATION_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    i06 i062 = this.g;
                    if (i062 != null) {
                        ee7.a((Object) stringExtra, "selectedComplicationId");
                        i062.b(stringExtra);
                        return;
                    }
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            i06 i063 = this.g;
            if (i063 != null) {
                i063.a("second-timezone", secondTimezoneSetting);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        sy4 sy4 = (sy4) qb.a(layoutInflater, 2131558521, viewGroup, false, a1());
        PortfolioApp.g0.c().f().a(new m06(this)).a(this);
        this.f = new qw6<>(this, sy4);
        ee7.a((Object) sy4, "binding");
        return sy4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        i06 i06 = this.g;
        if (i06 != null) {
            i06.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        i06 i06 = this.g;
        if (i06 != null) {
            i06.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        yo5 yo5 = new yo5(null, null, 3, null);
        yo5.a(new b(this));
        this.i = yo5;
        wo5 wo5 = new wo5(null, null, 3, null);
        wo5.a(new c(this));
        this.h = wo5;
        qw6<sy4> qw6 = this.f;
        if (qw6 != null) {
            sy4 a2 = qw6.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                wo5 wo52 = this.h;
                if (wo52 != null) {
                    recyclerView.setAdapter(wo52);
                    RecyclerView recyclerView2 = a2.v;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    yo5 yo52 = this.i;
                    if (yo52 != null) {
                        recyclerView2.setAdapter(yo52);
                        a2.z.setOnClickListener(new d(this));
                        a2.w.setOnClickListener(new e(a2, this));
                        return;
                    }
                    ee7.d("mComplicationAdapter");
                    throw null;
                }
                ee7.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void z(String str) {
        ee7.b(str, "content");
        qw6<sy4> qw6 = this.f;
        if (qw6 != null) {
            sy4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                ee7.a((Object) flexibleTextView, "it.tvComplicationDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(i06 i06) {
        ee7.b(i06, "presenter");
        this.g = i06;
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void a(List<Category> list) {
        ee7.b(list, "categories");
        wo5 wo5 = this.h;
        if (wo5 != null) {
            wo5.a(list);
        } else {
            ee7.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void a(boolean z, String str, String str2, Parcelable parcelable) {
        ee7.b(str, "complicationId");
        ee7.b(str2, "emptySettingRequestContent");
        qw6<sy4> qw6 = this.f;
        if (qw6 != null) {
            sy4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.A;
                ee7.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    ee7.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(0);
                    a2.z.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    FlexibleTextView flexibleTextView2 = a2.z;
                    ee7.a((Object) flexibleTextView2, "it.tvComplicationSetting");
                    FragmentActivity activity = getActivity();
                    Resources resources = activity != null ? activity.getResources() : null;
                    if (resources != null) {
                        flexibleTextView2.setCompoundDrawablePadding((int) resources.getDimension(2131165419));
                        int hashCode = str.hashCode();
                        if (hashCode != -829740640) {
                            if (hashCode == 134170930 && str.equals("second-timezone")) {
                                FlexibleSwitchCompat flexibleSwitchCompat = a2.w;
                                ee7.a((Object) flexibleSwitchCompat, "it.switchRing");
                                flexibleSwitchCompat.setVisibility(8);
                                ImageView imageView = a2.s;
                                ee7.a((Object) imageView, "it.ivComplicationSetting");
                                imageView.setVisibility(8);
                                a2.z.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2131231045, 0);
                                if (parcelable != null) {
                                    FlexibleTextView flexibleTextView3 = a2.z;
                                    ee7.a((Object) flexibleTextView3, "it.tvComplicationSetting");
                                    flexibleTextView3.setText(((SecondTimezoneSetting) parcelable).getTimeZoneName());
                                    return;
                                }
                                FlexibleTextView flexibleTextView4 = a2.z;
                                ee7.a((Object) flexibleTextView4, "it.tvComplicationSetting");
                                flexibleTextView4.setText(str2);
                            }
                        } else if (str.equals("commute-time")) {
                            FlexibleSwitchCompat flexibleSwitchCompat2 = a2.w;
                            ee7.a((Object) flexibleSwitchCompat2, "it.switchRing");
                            flexibleSwitchCompat2.setVisibility(8);
                            ImageView imageView2 = a2.s;
                            ee7.a((Object) imageView2, "it.ivComplicationSetting");
                            imageView2.setVisibility(8);
                            a2.z.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2131231045, 0);
                            if (parcelable != null) {
                                FlexibleTextView flexibleTextView5 = a2.z;
                                ee7.a((Object) flexibleTextView5, "it.tvComplicationSetting");
                                flexibleTextView5.setText(((CommuteTimeSetting) parcelable).getAddress());
                                return;
                            }
                            FlexibleTextView flexibleTextView6 = a2.z;
                            ee7.a((Object) flexibleTextView6, "it.tvComplicationSetting");
                            flexibleTextView6.setText(str2);
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ConstraintLayout constraintLayout2 = a2.q;
                    ee7.a((Object) constraintLayout2, "it.clComplicationSetting");
                    constraintLayout2.setVisibility(8);
                    FlexibleSwitchCompat flexibleSwitchCompat3 = a2.w;
                    ee7.a((Object) flexibleSwitchCompat3, "it.switchRing");
                    flexibleSwitchCompat3.setVisibility(8);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void c(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.z.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        ee7.b(str, "tag");
        if (ee7.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363307 && (activity3 = getActivity()) != null) {
                if (activity3 != null) {
                    ((cl5) activity3).k();
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (ee7.a((Object) str, (Object) bx6.c.a())) {
            if (i2 == 2131363307 && (activity2 = getActivity()) != null) {
                if (!ku7.a(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    px6.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity2 != null) {
                    ((cl5) activity2).k();
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (ee7.a((Object) str, (Object) "WARNING_DIALOG") && i2 == 2131363307 && (activity = getActivity()) != null) {
            if (activity != null) {
                ((cl5) activity).k();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    @Override // com.fossil.j06
    public void a(String str, String str2, String str3, String str4) {
        ee7.b(str, "topComplication");
        ee7.b(str2, "bottomComplication");
        ee7.b(str3, "rightComlication");
        ee7.b(str4, "leftComplication");
        ComplicationSearchActivity.z.a(this, str, str2, str4, str3);
    }
}
