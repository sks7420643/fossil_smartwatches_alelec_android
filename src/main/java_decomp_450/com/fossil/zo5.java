package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.be5;
import com.fossil.oh6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo5 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ ch5 a;
    @DexIgnore
    public /* final */ ArrayList<oh6.b> b;
    @DexIgnore
    public /* final */ iw c;
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ImageView a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ ImageView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ CardView h;
        @DexIgnore
        public /* final */ /* synthetic */ zo5 i;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zo5$a$a")
        /* renamed from: com.fossil.zo5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0262a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0262a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b c;
                if (this.a.i.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (c = this.a.i.d) != null) {
                    c.g(((oh6.b) this.a.i.b.get(this.a.getAdapterPosition())).c());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                ee7.b(str, "serial");
                ee7.b(str2, "filePath");
                this.a.i.c.a(str2).a(this.a.a);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(zo5 zo5, View view) {
            super(view);
            ee7.b(view, "view");
            this.i = zo5;
            this.a = (ImageView) view.findViewById(2131362661);
            this.b = (ImageView) view.findViewById(2131362719);
            this.c = (FlexibleTextView) view.findViewById(2131363257);
            this.d = (FlexibleTextView) view.findViewById(2131363282);
            this.e = (ConstraintLayout) view.findViewById(2131362049);
            this.f = (ImageView) view.findViewById(2131362721);
            this.g = (FlexibleTextView) view.findViewById(2131363337);
            this.h = (CardView) view.findViewById(2131361986);
            this.f.setOnClickListener(new View$OnClickListenerC0262a(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(oh6.b bVar) {
            ee7.b(bVar, "device");
            FlexibleTextView flexibleTextView = this.c;
            ee7.a((Object) flexibleTextView, "mTvDeviceName");
            flexibleTextView.setText(ux6.d(bVar.b()));
            if (bVar.d()) {
                CardView cardView = this.h;
                ee7.a((Object) cardView, "mCardView");
                cardView.setRadius(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                CardView cardView2 = this.h;
                ee7.a((Object) cardView2, "mCardView");
                cardView2.setCardElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.b.setImageResource(2131230957);
                ConstraintLayout constraintLayout = this.e;
                ee7.a((Object) constraintLayout, "mClContainer");
                constraintLayout.setAlpha(1.0f);
                ConstraintLayout constraintLayout2 = this.e;
                ee7.a((Object) constraintLayout2, "mClContainer");
                constraintLayout2.setBackground(v6.c(this.i.e, 2131230832));
                if (bVar.a() >= 0) {
                    FlexibleTextView flexibleTextView2 = this.g;
                    ee7.a((Object) flexibleTextView2, "mTvDeviceStatus");
                    StringBuilder sb = new StringBuilder();
                    sb.append(bVar.a());
                    sb.append('%');
                    flexibleTextView2.setText(sb.toString());
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, v6.c(this.i.e, be5.o.a(bVar.a())), (Drawable) null);
                } else {
                    FlexibleTextView flexibleTextView3 = this.g;
                    ee7.a((Object) flexibleTextView3, "mTvDeviceStatus");
                    flexibleTextView3.setText("");
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                u5 u5Var = new u5();
                ImageView imageView = this.f;
                ee7.a((Object) imageView, "mIvSetting");
                imageView.setVisibility(0);
                u5Var.c(this.e);
                FlexibleTextView flexibleTextView4 = this.d;
                ee7.a((Object) flexibleTextView4, "mTvLastSyncedTime");
                int id = flexibleTextView4.getId();
                ImageView imageView2 = this.f;
                ee7.a((Object) imageView2, "mIvSetting");
                u5Var.a(id, 7, imageView2.getId(), 6);
                u5Var.a(this.e);
                if (!bVar.e()) {
                    FlexibleTextView flexibleTextView5 = this.g;
                    ee7.a((Object) flexibleTextView5, "mTvDeviceStatus");
                    flexibleTextView5.setText(ig5.a(this.i.e, 2131887120));
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                FlexibleTextView flexibleTextView6 = this.d;
                ee7.a((Object) flexibleTextView6, "mTvLastSyncedTime");
                flexibleTextView6.setText(this.i.a(bVar.c(), true));
            } else {
                CardView cardView3 = this.h;
                ee7.a((Object) cardView3, "mCardView");
                cardView3.setRadius(2.0f);
                CardView cardView4 = this.h;
                ee7.a((Object) cardView4, "mCardView");
                cardView4.setCardElevation(2.0f);
                this.b.setImageResource(2131230956);
                ConstraintLayout constraintLayout3 = this.e;
                ee7.a((Object) constraintLayout3, "mClContainer");
                constraintLayout3.setAlpha(0.7f);
                ConstraintLayout constraintLayout4 = this.e;
                ee7.a((Object) constraintLayout4, "mClContainer");
                constraintLayout4.setBackground(v6.c(this.i.e, 2131230857));
                FlexibleTextView flexibleTextView7 = this.g;
                ee7.a((Object) flexibleTextView7, "mTvDeviceStatus");
                flexibleTextView7.setText(ig5.a(this.i.e, 2131887120));
                this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.c.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                FlexibleTextView flexibleTextView8 = this.d;
                ee7.a((Object) flexibleTextView8, "mTvLastSyncedTime");
                flexibleTextView8.setText(this.i.a(bVar.c(), false));
            }
            String b2 = eh5.l.a().b("nonBrandSurface");
            if (!TextUtils.isEmpty(b2)) {
                this.h.setBackgroundColor(Color.parseColor(b2));
            }
            CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(bVar.c()).setSerialPrefix(be5.o.b(bVar.c())).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView3 = this.a;
            ee7.a((Object) imageView3, "mIvDevice");
            type.setPlaceHolder(imageView3, be5.o.b(bVar.c(), be5.b.SMALL)).setImageCallback(new b(this)).download();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void g(String str);
    }

    @DexIgnore
    public zo5(ArrayList<oh6.b> arrayList, iw iwVar, b bVar, PortfolioApp portfolioApp) {
        ee7.b(arrayList, "mData");
        ee7.b(iwVar, "mRequestManager");
        ee7.b(portfolioApp, "mApp");
        this.b = arrayList;
        this.c = iwVar;
        this.d = bVar;
        this.e = portfolioApp;
        this.a = portfolioApp.v();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final void c() {
        if (getItemCount() > 0) {
            notifyItemChanged(0);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558695, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026le_device, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(ArrayList<oh6.b> arrayList) {
        ee7.b(arrayList, "data");
        this.b.clear();
        this.b.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            oh6.b bVar = this.b.get(i);
            ee7.a((Object) bVar, "mData[position]");
            aVar.a(bVar);
        }
    }

    @DexIgnore
    public final String a(String str, boolean z) {
        String str2;
        if (!z || !this.e.g(str)) {
            long e2 = this.a.e(str);
            if (e2 == 0) {
                str2 = "";
            } else if (System.currentTimeMillis() - e2 < 60000) {
                we7 we7 = we7.a;
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131887136);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                str2 = String.format(a2, Arrays.copyOf(new Object[]{1}, 1));
                ee7.a((Object) str2, "java.lang.String.format(format, *args)");
            } else {
                str2 = zd5.a(e2);
            }
            String string = this.e.getString(2131887122, new Object[]{str2});
            ee7.a((Object) string, "mApp.getString(R.string.\u2026ayTime, lastSyncTimeText)");
            return string;
        }
        String string2 = this.e.getString(2131886733);
        ee7.a((Object) string2, "mApp.getString(R.string.\u2026ded_Text__SyncInProgress)");
        return string2;
    }
}
