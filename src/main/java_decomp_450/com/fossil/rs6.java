package com.fossil;

import com.portfolio.platform.data.model.PermissionData;
import dagger.internal.Factory;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs6 implements Factory<List<PermissionData>> {
    @DexIgnore
    public static List<PermissionData> a(qs6 qs6) {
        List<PermissionData> a = qs6.a();
        c87.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
