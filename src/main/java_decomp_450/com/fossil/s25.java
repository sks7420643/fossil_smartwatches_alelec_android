package com.fossil;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s25 extends ViewDataBinding {
    @DexIgnore
    public /* final */ TabLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ ImageButton v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ ProgressBar x;
    @DexIgnore
    public /* final */ RelativeLayout y;
    @DexIgnore
    public /* final */ ViewPager2 z;

    @DexIgnore
    public s25(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton, ImageButton imageButton, RTLImageView rTLImageView, ProgressBar progressBar, RelativeLayout relativeLayout, ViewPager2 viewPager2, TabLayout tabLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = flexibleButton;
        this.v = imageButton;
        this.w = rTLImageView;
        this.x = progressBar;
        this.y = relativeLayout;
        this.z = viewPager2;
        this.A = tabLayout;
        this.B = flexibleTextView3;
        this.C = flexibleTextView4;
        this.D = flexibleTextView5;
    }
}
