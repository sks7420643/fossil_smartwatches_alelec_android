package com.fossil;

import android.util.Log;
import com.fossil.kw;
import com.fossil.nz;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rz implements nz {
    @DexIgnore
    public /* final */ wz a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ pz d; // = new pz();
    @DexIgnore
    public kw e;

    @DexIgnore
    @Deprecated
    public rz(File file, long j) {
        this.b = file;
        this.c = j;
        this.a = new wz();
    }

    @DexIgnore
    public static nz a(File file, long j) {
        return new rz(file, j);
    }

    @DexIgnore
    public final synchronized kw a() throws IOException {
        if (this.e == null) {
            this.e = kw.a(this.b, 1, 1, this.c);
        }
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.nz
    public File a(yw ywVar) {
        String b2 = this.a.b(ywVar);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + b2 + " for for Key: " + ywVar);
        }
        try {
            kw.e c2 = a().c(b2);
            if (c2 != null) {
                return c2.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.nz
    public void a(yw ywVar, nz.b bVar) {
        String b2 = this.a.b(ywVar);
        this.d.a(b2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + b2 + " for for Key: " + ywVar);
            }
            try {
                kw a2 = a();
                if (a2.c(b2) == null) {
                    kw.c b3 = a2.b(b2);
                    if (b3 != null) {
                        try {
                            if (bVar.a(b3.a(0))) {
                                b3.c();
                            }
                            this.d.b(b2);
                        } finally {
                            b3.b();
                        }
                    } else {
                        throw new IllegalStateException("Had two simultaneous puts for: " + b2);
                    }
                }
            } catch (IOException e2) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e2);
                }
            }
        } finally {
            this.d.b(b2);
        }
    }
}
