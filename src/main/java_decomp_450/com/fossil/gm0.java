package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gm0 extends xp0 {
    @DexIgnore
    public static /* final */ js1 CREATOR; // = new js1(null);
    @DexIgnore
    public jk0 d;

    @DexIgnore
    public gm0(byte b) {
        super(ru0.TIME_SYNC_EVENT, b, false, 4);
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        JSONObject a = super.a();
        r51 r51 = r51.x0;
        jk0 jk0 = this.d;
        return yz0.a(a, r51, jk0 != null ? jk0.a() : null);
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public byte[] b() {
        this.d = new jk0(System.currentTimeMillis());
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        jk0 jk0 = this.d;
        if (jk0 != null) {
            ByteBuffer putInt = order.putInt((int) jk0.a);
            jk0 jk02 = this.d;
            if (jk02 != null) {
                ByteBuffer putShort = putInt.putShort((short) ((int) jk02.b));
                jk0 jk03 = this.d;
                if (jk03 != null) {
                    byte[] array = putShort.putShort((short) jk03.c).array();
                    ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
                    return array;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ gm0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
