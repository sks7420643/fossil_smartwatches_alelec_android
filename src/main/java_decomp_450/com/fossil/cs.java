package com.fossil;

import android.net.Uri;
import com.fossil.yr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs implements yr<String, Uri> {
    @DexIgnore
    public boolean a(String str) {
        ee7.b(str, "data");
        return yr.a.a(this, str);
    }

    @DexIgnore
    public Uri b(String str) {
        ee7.b(str, "data");
        Uri parse = Uri.parse(str);
        ee7.a((Object) parse, "Uri.parse(this)");
        return parse;
    }
}
