package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x47 {
    @DexIgnore
    public static k57 l; // = v57.b();
    @DexIgnore
    public static Context m; // = null;
    @DexIgnore
    public static x47 n; // = null;
    @DexIgnore
    public g57 a; // = null;
    @DexIgnore
    public g57 b; // = null;
    @DexIgnore
    public p57 c; // = null;
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public volatile int f; // = 0;
    @DexIgnore
    public l57 g; // = null;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public ConcurrentHashMap<e47, String> i; // = null;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public HashMap<String, String> k; // = new HashMap<>();

    @DexIgnore
    public x47(Context context) {
        try {
            this.c = new p57();
            m = context.getApplicationContext();
            this.i = new ConcurrentHashMap<>();
            this.d = v57.u(context);
            this.e = "pri_" + v57.u(context);
            this.a = new g57(m, this.d);
            this.b = new g57(m, this.e);
            a(true);
            a(false);
            d();
            a(m);
            c();
            h();
        } catch (Throwable th) {
            l.a(th);
        }
    }

    @DexIgnore
    public static x47 b(Context context) {
        if (n == null) {
            synchronized (x47.class) {
                if (n == null) {
                    n = new x47(context);
                }
            }
        }
        return n;
    }

    @DexIgnore
    public static x47 i() {
        return n;
    }

    @DexIgnore
    public int a() {
        return this.f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b3 A[Catch:{ all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0120 A[Catch:{ all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0133 A[Catch:{ all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01d8 A[SYNTHETIC, Splitter:B:83:0x01d8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.fossil.l57 a(android.content.Context r19) {
        /*
            r18 = this;
            r1 = r18
            monitor-enter(r18)
            com.fossil.l57 r0 = r1.g     // Catch:{ all -> 0x0205 }
            if (r0 == 0) goto L_0x000b
            com.fossil.l57 r0 = r1.g     // Catch:{ all -> 0x0205 }
            monitor-exit(r18)
            return r0
        L_0x000b:
            com.fossil.g57 r0 = r1.a     // Catch:{ all -> 0x01ce }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x01ce }
            r0.beginTransaction()     // Catch:{ all -> 0x01ce }
            boolean r0 = com.fossil.w37.q()     // Catch:{ all -> 0x01ce }
            if (r0 == 0) goto L_0x0026
            com.fossil.k57 r0 = com.fossil.x47.l     // Catch:{ all -> 0x0022 }
            java.lang.String r3 = "try to load user info from db."
            r0.e(r3)     // Catch:{ all -> 0x0022 }
            goto L_0x0026
        L_0x0022:
            r0 = move-exception
            r2 = 0
            goto L_0x01d1
        L_0x0026:
            com.fossil.g57 r0 = r1.a
            android.database.sqlite.SQLiteDatabase r3 = r0.getReadableDatabase()
            java.lang.String r4 = "user"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10, r11)
            boolean r0 = r3.moveToNext()     // Catch:{ all -> 0x01cb }
            r4 = 1000(0x3e8, double:4.94E-321)
            r6 = 0
            r7 = 1
            if (r0 == 0) goto L_0x0140
            java.lang.String r0 = r3.getString(r6)     // Catch:{ all -> 0x01cb }
            java.lang.String r8 = com.fossil.a67.a(r0)     // Catch:{ all -> 0x01cb }
            int r9 = r3.getInt(r7)     // Catch:{ all -> 0x01cb }
            r10 = 2
            java.lang.String r11 = r3.getString(r10)     // Catch:{ all -> 0x01cb }
            r12 = 3
            long r12 = r3.getLong(r12)     // Catch:{ all -> 0x01cb }
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x01cb }
            long r14 = r14 / r4
            if (r9 == r7) goto L_0x0074
            long r12 = r12 * r4
            java.lang.String r12 = com.fossil.v57.a(r12)     // Catch:{ all -> 0x01cb }
            long r16 = r14 * r4
            java.lang.String r13 = com.fossil.v57.a(r16)     // Catch:{ all -> 0x01cb }
            boolean r12 = r12.equals(r13)     // Catch:{ all -> 0x01cb }
            if (r12 != 0) goto L_0x0074
            r12 = 1
            goto L_0x0075
        L_0x0074:
            r12 = r9
        L_0x0075:
            java.lang.String r13 = com.fossil.v57.q(r19)     // Catch:{ all -> 0x01cb }
            boolean r11 = r11.equals(r13)     // Catch:{ all -> 0x01cb }
            if (r11 != 0) goto L_0x0081
            r12 = r12 | 2
        L_0x0081:
            java.lang.String r11 = ","
            java.lang.String[] r11 = r8.split(r11)     // Catch:{ all -> 0x01cb }
            if (r11 == 0) goto L_0x00ab
            int r13 = r11.length     // Catch:{ all -> 0x01cb }
            if (r13 <= 0) goto L_0x00ab
            r13 = r11[r6]     // Catch:{ all -> 0x01cb }
            if (r13 == 0) goto L_0x009b
            int r4 = r13.length()     // Catch:{ all -> 0x01cb }
            r5 = 11
            if (r4 >= r5) goto L_0x0099
            goto L_0x009b
        L_0x0099:
            r2 = 0
            goto L_0x00b1
        L_0x009b:
            java.lang.String r4 = com.fossil.a67.a(r19)     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x0099
            int r5 = r4.length()     // Catch:{ all -> 0x01cb }
            r2 = 10
            if (r5 <= r2) goto L_0x0099
            r13 = r4
            goto L_0x00b0
        L_0x00ab:
            java.lang.String r8 = com.fossil.v57.e(r19)     // Catch:{ all -> 0x01cb }
            r13 = r8
        L_0x00b0:
            r2 = 1
        L_0x00b1:
            if (r11 == 0) goto L_0x00cd
            int r4 = r11.length     // Catch:{ all -> 0x01cb }
            if (r4 < r10) goto L_0x00cd
            r4 = r11[r7]     // Catch:{ all -> 0x01cb }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x01cb }
            r5.<init>()     // Catch:{ all -> 0x01cb }
            r5.append(r13)     // Catch:{ all -> 0x01cb }
            java.lang.String r8 = ","
            r5.append(r8)     // Catch:{ all -> 0x01cb }
            r5.append(r4)     // Catch:{ all -> 0x01cb }
            java.lang.String r8 = r5.toString()     // Catch:{ all -> 0x01cb }
            goto L_0x00ee
        L_0x00cd:
            java.lang.String r4 = com.fossil.v57.f(r19)     // Catch:{ all -> 0x01cb }
            if (r4 == 0) goto L_0x00ee
            int r5 = r4.length()     // Catch:{ all -> 0x01cb }
            if (r5 <= 0) goto L_0x00ee
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01cb }
            r2.<init>()     // Catch:{ all -> 0x01cb }
            r2.append(r13)     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = ","
            r2.append(r5)     // Catch:{ all -> 0x01cb }
            r2.append(r4)     // Catch:{ all -> 0x01cb }
            java.lang.String r8 = r2.toString()     // Catch:{ all -> 0x01cb }
            r2 = 1
        L_0x00ee:
            com.fossil.l57 r5 = new com.fossil.l57     // Catch:{ all -> 0x01cb }
            r5.<init>(r13, r4, r12)     // Catch:{ all -> 0x01cb }
            r1.g = r5     // Catch:{ all -> 0x01cb }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ all -> 0x01cb }
            r4.<init>()     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = com.fossil.a67.b(r8)     // Catch:{ all -> 0x01cb }
            java.lang.String r8 = "uid"
            r4.put(r8, r5)     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "user_type"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x01cb }
            r4.put(r5, r8)     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "app_ver"
            java.lang.String r8 = com.fossil.v57.q(r19)     // Catch:{ all -> 0x01cb }
            r4.put(r5, r8)     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "ts"
            java.lang.Long r8 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x01cb }
            r4.put(r5, r8)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0131
            com.fossil.g57 r2 = r1.a     // Catch:{ all -> 0x01cb }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "user"
            java.lang.String r8 = "uid=?"
            java.lang.String[] r10 = new java.lang.String[r7]     // Catch:{ all -> 0x01cb }
            r10[r6] = r0     // Catch:{ all -> 0x01cb }
            r2.update(r5, r4, r8, r10)     // Catch:{ all -> 0x01cb }
        L_0x0131:
            if (r12 == r9) goto L_0x0141
            com.fossil.g57 r0 = r1.a     // Catch:{ all -> 0x01cb }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = "user"
            r5 = 0
            r0.replace(r2, r5, r4)     // Catch:{ all -> 0x01cb }
            goto L_0x0141
        L_0x0140:
            r7 = 0
        L_0x0141:
            if (r7 != 0) goto L_0x01ac
            java.lang.String r0 = com.fossil.v57.e(r19)     // Catch:{ all -> 0x01cb }
            java.lang.String r2 = com.fossil.v57.f(r19)     // Catch:{ all -> 0x01cb }
            if (r2 == 0) goto L_0x0168
            int r4 = r2.length()     // Catch:{ all -> 0x01cb }
            if (r4 <= 0) goto L_0x0168
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x01cb }
            r4.<init>()     // Catch:{ all -> 0x01cb }
            r4.append(r0)     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = ","
            r4.append(r5)     // Catch:{ all -> 0x01cb }
            r4.append(r2)     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x01cb }
            goto L_0x0169
        L_0x0168:
            r4 = r0
        L_0x0169:
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x01cb }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r7 = r7 / r9
            java.lang.String r5 = com.fossil.v57.q(r19)     // Catch:{ all -> 0x01cb }
            android.content.ContentValues r9 = new android.content.ContentValues     // Catch:{ all -> 0x01cb }
            r9.<init>()     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = com.fossil.a67.b(r4)     // Catch:{ all -> 0x01cb }
            java.lang.String r10 = "uid"
            r9.put(r10, r4)     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "user_type"
            java.lang.Integer r10 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x01cb }
            r9.put(r4, r10)     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "app_ver"
            r9.put(r4, r5)     // Catch:{ all -> 0x01cb }
            java.lang.String r4 = "ts"
            java.lang.Long r5 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x01cb }
            r9.put(r4, r5)     // Catch:{ all -> 0x01cb }
            com.fossil.g57 r4 = r1.a     // Catch:{ all -> 0x01cb }
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "user"
            r7 = 0
            r4.insert(r5, r7, r9)     // Catch:{ all -> 0x01cb }
            com.fossil.l57 r4 = new com.fossil.l57     // Catch:{ all -> 0x01cb }
            r4.<init>(r0, r2, r6)     // Catch:{ all -> 0x01cb }
            r1.g = r4     // Catch:{ all -> 0x01cb }
        L_0x01ac:
            com.fossil.g57 r0 = r1.a     // Catch:{ all -> 0x01cb }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x01cb }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x01cb }
            if (r3 == 0) goto L_0x01ba
            r3.close()     // Catch:{ all -> 0x01c4 }
        L_0x01ba:
            com.fossil.g57 r0 = r1.a     // Catch:{ all -> 0x01c4 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x01c4 }
            r0.endTransaction()     // Catch:{ all -> 0x01c4 }
            goto L_0x01e9
        L_0x01c4:
            r0 = move-exception
            com.fossil.k57 r2 = com.fossil.x47.l
        L_0x01c7:
            r2.a(r0)
            goto L_0x01e9
        L_0x01cb:
            r0 = move-exception
            r2 = r3
            goto L_0x01d1
        L_0x01ce:
            r0 = move-exception
            r7 = 0
            r2 = r7
        L_0x01d1:
            com.fossil.k57 r3 = com.fossil.x47.l     // Catch:{ all -> 0x01ed }
            r3.a(r0)     // Catch:{ all -> 0x01ed }
            if (r2 == 0) goto L_0x01db
            r2.close()     // Catch:{ all -> 0x01e5 }
        L_0x01db:
            com.fossil.g57 r0 = r1.a     // Catch:{ all -> 0x01e5 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x01e5 }
            r0.endTransaction()     // Catch:{ all -> 0x01e5 }
            goto L_0x01e9
        L_0x01e5:
            r0 = move-exception
            com.fossil.k57 r2 = com.fossil.x47.l
            goto L_0x01c7
        L_0x01e9:
            com.fossil.l57 r0 = r1.g
            monitor-exit(r18)
            return r0
        L_0x01ed:
            r0 = move-exception
            r3 = r0
            if (r2 == 0) goto L_0x01f4
            r2.close()     // Catch:{ all -> 0x01fe }
        L_0x01f4:
            com.fossil.g57 r0 = r1.a     // Catch:{ all -> 0x01fe }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x01fe }
            r0.endTransaction()     // Catch:{ all -> 0x01fe }
            goto L_0x0204
        L_0x01fe:
            r0 = move-exception
            com.fossil.k57 r2 = com.fossil.x47.l
            r2.a(r0)
        L_0x0204:
            throw r3
        L_0x0205:
            r0 = move-exception
            monitor-exit(r18)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.a(android.content.Context):com.fossil.l57");
    }

    @DexIgnore
    public final String a(List<h57> list) {
        StringBuilder sb = new StringBuilder(list.size() * 3);
        sb.append("event_id in (");
        int size = list.size();
        int i2 = 0;
        for (h57 h57 : list) {
            sb.append(h57.a);
            if (i2 != size - 1) {
                sb.append(",");
            }
            i2++;
        }
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public void a(int i2) {
        this.c.a(new f57(this, i2));
    }

    @DexIgnore
    public final synchronized void a(int i2, boolean z) {
        try {
            if (this.f > 0 && i2 > 0) {
                if (!z37.a()) {
                    if (w37.q()) {
                        k57 k57 = l;
                        k57.e("Load " + this.f + " unsent events");
                    }
                    ArrayList arrayList = new ArrayList(i2);
                    b(arrayList, i2, z);
                    if (arrayList.size() > 0) {
                        if (w37.q()) {
                            k57 k572 = l;
                            k572.e("Peek " + arrayList.size() + " unsent events.");
                        }
                        a(arrayList, 2, z);
                        h67.b(m).b(arrayList, new e57(this, arrayList, z));
                    }
                }
            }
        } catch (Throwable th) {
            l.a(th);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.e47 r9, com.fossil.g67 r10, boolean r11) {
        /*
            r8 = this;
            r0 = 1
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r8.c(r11)     // Catch:{ all -> 0x008c }
            r2.beginTransaction()     // Catch:{ all -> 0x0089 }
            java.lang.String r3 = "events"
            if (r11 != 0) goto L_0x002d
            int r11 = r8.f
            int r4 = com.fossil.w37.j()
            if (r11 <= r4) goto L_0x002d
            com.fossil.k57 r11 = com.fossil.x47.l
            java.lang.String r4 = "Too many events stored in db."
            r11.h(r4)
            int r11 = r8.f
            com.fossil.g57 r4 = r8.a
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()
            java.lang.String r5 = "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)"
            int r4 = r4.delete(r3, r5, r1)
            int r11 = r11 - r4
            r8.f = r11
        L_0x002d:
            android.content.ContentValues r11 = new android.content.ContentValues
            r11.<init>()
            java.lang.String r4 = r9.f()
            boolean r5 = com.fossil.w37.q()
            if (r5 == 0) goto L_0x004f
            com.fossil.k57 r5 = com.fossil.x47.l
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "insert 1 event, content:"
            r6.<init>(r7)
            r6.append(r4)
            java.lang.String r6 = r6.toString()
            r5.e(r6)
        L_0x004f:
            java.lang.String r4 = com.fossil.a67.b(r4)
            java.lang.String r5 = "content"
            r11.put(r5, r4)
            java.lang.String r4 = "send_count"
            java.lang.String r5 = "0"
            r11.put(r4, r5)
            java.lang.String r4 = "status"
            java.lang.String r5 = java.lang.Integer.toString(r0)
            r11.put(r4, r5)
            java.lang.String r4 = "timestamp"
            long r5 = r9.b()
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r11.put(r4, r5)
            long r3 = r2.insert(r3, r1, r11)
            r2.setTransactionSuccessful()
            if (r2 == 0) goto L_0x0099
            r2.endTransaction()     // Catch:{ all -> 0x0082 }
            goto L_0x0099
        L_0x0082:
            r11 = move-exception
            com.fossil.k57 r1 = com.fossil.x47.l
            r1.a(r11)
            goto L_0x0099
        L_0x0089:
            r11 = move-exception
            r1 = r2
            goto L_0x008d
        L_0x008c:
            r11 = move-exception
        L_0x008d:
            r3 = -1
            com.fossil.k57 r2 = com.fossil.x47.l     // Catch:{ all -> 0x00df }
            r2.a(r11)     // Catch:{ all -> 0x00df }
            if (r1 == 0) goto L_0x0099
            r1.endTransaction()
        L_0x0099:
            r1 = 0
            int r11 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r11 <= 0) goto L_0x00c7
            int r11 = r8.f
            int r11 = r11 + r0
            r8.f = r11
            boolean r11 = com.fossil.w37.q()
            if (r11 == 0) goto L_0x00c1
            com.fossil.k57 r11 = com.fossil.x47.l
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "directStoreEvent insert event to db, event:"
            r0.<init>(r1)
            java.lang.String r9 = r9.f()
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            r11.a(r9)
        L_0x00c1:
            if (r10 == 0) goto L_0x00de
            r10.a()
            return
        L_0x00c7:
            com.fossil.k57 r10 = com.fossil.x47.l
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r0 = "Failed to store event:"
            r11.<init>(r0)
            java.lang.String r9 = r9.f()
            r11.append(r9)
            java.lang.String r9 = r11.toString()
            r10.d(r9)
        L_0x00de:
            return
        L_0x00df:
            r9 = move-exception
            if (r1 == 0) goto L_0x00ec
            r1.endTransaction()     // Catch:{ all -> 0x00e6 }
            goto L_0x00ec
        L_0x00e6:
            r10 = move-exception
            com.fossil.k57 r11 = com.fossil.x47.l
            r11.a(r10)
        L_0x00ec:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.a(com.fossil.e47, com.fossil.g67, boolean):void");
    }

    @DexIgnore
    public void a(e47 e47, g67 g67, boolean z, boolean z2) {
        p57 p57 = this.c;
        if (p57 != null) {
            p57.a(new b57(this, e47, g67, z, z2));
        }
    }

    @DexIgnore
    public void a(e67 e67) {
        if (e67 != null) {
            this.c.a(new c57(this, e67));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00bb A[SYNTHETIC, Splitter:B:39:0x00bb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.util.List<com.fossil.h57> r5, int r6, boolean r7) {
        /*
            r4 = this;
            monitor-enter(r4)
            int r0 = r5.size()     // Catch:{ all -> 0x00d8 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r4)
            return
        L_0x0009:
            int r0 = r4.b(r7)
            r1 = 0
            android.database.sqlite.SQLiteDatabase r7 = r4.c(r7)     // Catch:{ all -> 0x00b3 }
            r2 = 2
            if (r6 != r2) goto L_0x0030
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            java.lang.String r2 = "update events set status="
            r0.<init>(r2)     // Catch:{ all -> 0x00b0 }
            r0.append(r6)     // Catch:{ all -> 0x00b0 }
            java.lang.String r6 = ", send_count=send_count+1  where "
            r0.append(r6)     // Catch:{ all -> 0x00b0 }
            java.lang.String r5 = r4.a(r5)     // Catch:{ all -> 0x00b0 }
            r0.append(r5)     // Catch:{ all -> 0x00b0 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x00b0 }
            goto L_0x0064
        L_0x0030:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            java.lang.String r3 = "update events set status="
            r2.<init>(r3)     // Catch:{ all -> 0x00b0 }
            r2.append(r6)     // Catch:{ all -> 0x00b0 }
            java.lang.String r6 = " where "
            r2.append(r6)     // Catch:{ all -> 0x00b0 }
            java.lang.String r5 = r4.a(r5)     // Catch:{ all -> 0x00b0 }
            r2.append(r5)     // Catch:{ all -> 0x00b0 }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x00b0 }
            int r6 = r4.h     // Catch:{ all -> 0x00b0 }
            int r6 = r6 % 3
            if (r6 != 0) goto L_0x005e
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            java.lang.String r1 = "delete from events where send_count>"
            r6.<init>(r1)     // Catch:{ all -> 0x00b0 }
            r6.append(r0)     // Catch:{ all -> 0x00b0 }
            java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x00b0 }
        L_0x005e:
            int r6 = r4.h     // Catch:{ all -> 0x00b0 }
            int r6 = r6 + 1
            r4.h = r6     // Catch:{ all -> 0x00b0 }
        L_0x0064:
            boolean r6 = com.fossil.w37.q()     // Catch:{ all -> 0x00b0 }
            if (r6 == 0) goto L_0x007d
            com.fossil.k57 r6 = com.fossil.x47.l     // Catch:{ all -> 0x00b0 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            java.lang.String r2 = "update sql:"
            r0.<init>(r2)     // Catch:{ all -> 0x00b0 }
            r0.append(r5)     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00b0 }
            r6.e(r0)     // Catch:{ all -> 0x00b0 }
        L_0x007d:
            r7.beginTransaction()     // Catch:{ all -> 0x00b0 }
            r7.execSQL(r5)     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x009e
            com.fossil.k57 r5 = com.fossil.x47.l     // Catch:{ all -> 0x00b0 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = "update for delete sql:"
            r6.<init>(r0)     // Catch:{ all -> 0x00b0 }
            r6.append(r1)     // Catch:{ all -> 0x00b0 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00b0 }
            r5.e(r6)     // Catch:{ all -> 0x00b0 }
            r7.execSQL(r1)     // Catch:{ all -> 0x00b0 }
            r4.d()     // Catch:{ all -> 0x00b0 }
        L_0x009e:
            r7.setTransactionSuccessful()     // Catch:{ all -> 0x00b0 }
            if (r7 == 0) goto L_0x00c8
            r7.endTransaction()     // Catch:{ all -> 0x00a8 }
            monitor-exit(r4)
            return
        L_0x00a8:
            r5 = move-exception
            com.fossil.k57 r6 = com.fossil.x47.l
            r6.a(r5)
            monitor-exit(r4)
            return
        L_0x00b0:
            r5 = move-exception
            r1 = r7
            goto L_0x00b4
        L_0x00b3:
            r5 = move-exception
        L_0x00b4:
            com.fossil.k57 r6 = com.fossil.x47.l     // Catch:{ all -> 0x00ca }
            r6.a(r5)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x00c8
            r1.endTransaction()     // Catch:{ all -> 0x00c0 }
            monitor-exit(r4)
            return
        L_0x00c0:
            r5 = move-exception
            com.fossil.k57 r6 = com.fossil.x47.l
            r6.a(r5)
            monitor-exit(r4)
            return
        L_0x00c8:
            monitor-exit(r4)
            return
        L_0x00ca:
            r5 = move-exception
            if (r1 == 0) goto L_0x00d7
            r1.endTransaction()     // Catch:{ all -> 0x00d1 }
            goto L_0x00d7
        L_0x00d1:
            r6 = move-exception
            com.fossil.k57 r7 = com.fossil.x47.l
            r7.a(r6)
        L_0x00d7:
            throw r5
        L_0x00d8:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.a(java.util.List, int, boolean):void");
    }

    @DexIgnore
    public void a(List<h57> list, int i2, boolean z, boolean z2) {
        p57 p57 = this.c;
        if (p57 != null) {
            p57.a(new y47(this, list, i2, z, z2));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cf A[SYNTHETIC, Splitter:B:39:0x00cf] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.util.List<com.fossil.h57> r7, boolean r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            int r0 = r7.size()     // Catch:{ all -> 0x00ec }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r6)
            return
        L_0x0009:
            boolean r0 = com.fossil.w37.q()
            if (r0 == 0) goto L_0x002e
            com.fossil.k57 r0 = com.fossil.x47.l
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Delete "
            r1.<init>(r2)
            int r2 = r7.size()
            r1.append(r2)
            java.lang.String r2 = " events, important:"
            r1.append(r2)
            r1.append(r8)
            java.lang.String r1 = r1.toString()
            r0.e(r1)
        L_0x002e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            int r1 = r7.size()
            int r1 = r1 * 3
            r0.<init>(r1)
            java.lang.String r1 = "event_id in ("
            r0.append(r1)
            r1 = 0
            int r2 = r7.size()
            java.util.Iterator r7 = r7.iterator()
        L_0x0047:
            boolean r3 = r7.hasNext()
            if (r3 == 0) goto L_0x0064
            java.lang.Object r3 = r7.next()
            com.fossil.h57 r3 = (com.fossil.h57) r3
            long r3 = r3.a
            r0.append(r3)
            int r3 = r2 + -1
            if (r1 == r3) goto L_0x0061
            java.lang.String r3 = ","
            r0.append(r3)
        L_0x0061:
            int r1 = r1 + 1
            goto L_0x0047
        L_0x0064:
            java.lang.String r7 = ")"
            r0.append(r7)
            r7 = 0
            android.database.sqlite.SQLiteDatabase r8 = r6.c(r8)     // Catch:{ all -> 0x00c4 }
            r8.beginTransaction()     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = "events"
            java.lang.String r3 = r0.toString()     // Catch:{ all -> 0x00c2 }
            int r7 = r8.delete(r1, r3, r7)     // Catch:{ all -> 0x00c2 }
            boolean r1 = com.fossil.w37.q()     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x00a8
            com.fossil.k57 r1 = com.fossil.x47.l     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "delete "
            r3.<init>(r4)     // Catch:{ all -> 0x00c2 }
            r3.append(r2)     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = " event "
            r3.append(r2)     // Catch:{ all -> 0x00c2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c2 }
            r3.append(r0)     // Catch:{ all -> 0x00c2 }
            java.lang.String r0 = ", success delete:"
            r3.append(r0)     // Catch:{ all -> 0x00c2 }
            r3.append(r7)     // Catch:{ all -> 0x00c2 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x00c2 }
            r1.e(r0)     // Catch:{ all -> 0x00c2 }
        L_0x00a8:
            int r0 = r6.f     // Catch:{ all -> 0x00c2 }
            int r0 = r0 - r7
            r6.f = r0     // Catch:{ all -> 0x00c2 }
            r8.setTransactionSuccessful()     // Catch:{ all -> 0x00c2 }
            r6.d()     // Catch:{ all -> 0x00c2 }
            if (r8 == 0) goto L_0x00dc
            r8.endTransaction()     // Catch:{ all -> 0x00ba }
            monitor-exit(r6)
            return
        L_0x00ba:
            r7 = move-exception
            com.fossil.k57 r8 = com.fossil.x47.l
            r8.a(r7)
            monitor-exit(r6)
            return
        L_0x00c2:
            r7 = move-exception
            goto L_0x00c8
        L_0x00c4:
            r8 = move-exception
            r5 = r8
            r8 = r7
            r7 = r5
        L_0x00c8:
            com.fossil.k57 r0 = com.fossil.x47.l     // Catch:{ all -> 0x00de }
            r0.a(r7)     // Catch:{ all -> 0x00de }
            if (r8 == 0) goto L_0x00dc
            r8.endTransaction()     // Catch:{ all -> 0x00d4 }
            monitor-exit(r6)
            return
        L_0x00d4:
            r7 = move-exception
            com.fossil.k57 r8 = com.fossil.x47.l
            r8.a(r7)
            monitor-exit(r6)
            return
        L_0x00dc:
            monitor-exit(r6)
            return
        L_0x00de:
            r7 = move-exception
            if (r8 == 0) goto L_0x00eb
            r8.endTransaction()     // Catch:{ all -> 0x00e5 }
            goto L_0x00eb
        L_0x00e5:
            r8 = move-exception
            com.fossil.k57 r0 = com.fossil.x47.l
            r0.a(r8)
        L_0x00eb:
            throw r7
        L_0x00ec:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.a(java.util.List, boolean):void");
    }

    @DexIgnore
    public void a(List<h57> list, boolean z, boolean z2) {
        p57 p57 = this.c;
        if (p57 != null) {
            p57.a(new z47(this, list, z, z2));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(boolean r8) {
        /*
            r7 = this;
            android.database.sqlite.SQLiteDatabase r8 = r7.c(r8)     // Catch:{ all -> 0x0059 }
            r8.beginTransaction()     // Catch:{ all -> 0x0057 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x0057 }
            r0.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "status"
            r2 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0057 }
            r0.put(r1, r3)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "events"
            java.lang.String r3 = "status=?"
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ all -> 0x0057 }
            r4 = 0
            r5 = 2
            java.lang.String r5 = java.lang.Long.toString(r5)     // Catch:{ all -> 0x0057 }
            r2[r4] = r5     // Catch:{ all -> 0x0057 }
            int r0 = r8.update(r1, r0, r3, r2)     // Catch:{ all -> 0x0057 }
            boolean r1 = com.fossil.w37.q()     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0047
            com.fossil.k57 r1 = com.fossil.x47.l     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            java.lang.String r3 = "update "
            r2.<init>(r3)     // Catch:{ all -> 0x0057 }
            r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r0 = " unsent events."
            r2.append(r0)     // Catch:{ all -> 0x0057 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0057 }
            r1.e(r0)     // Catch:{ all -> 0x0057 }
        L_0x0047:
            r8.setTransactionSuccessful()     // Catch:{ all -> 0x0057 }
            if (r8 == 0) goto L_0x0056
            r8.endTransaction()     // Catch:{ all -> 0x0050 }
            goto L_0x0056
        L_0x0050:
            r8 = move-exception
            com.fossil.k57 r0 = com.fossil.x47.l
            r0.a(r8)
        L_0x0056:
            return
        L_0x0057:
            r0 = move-exception
            goto L_0x005b
        L_0x0059:
            r0 = move-exception
            r8 = 0
        L_0x005b:
            com.fossil.k57 r1 = com.fossil.x47.l     // Catch:{ all -> 0x0066 }
            r1.a(r0)     // Catch:{ all -> 0x0066 }
            if (r8 == 0) goto L_0x0065
            r8.endTransaction()
        L_0x0065:
            return
        L_0x0066:
            r0 = move-exception
            if (r8 == 0) goto L_0x0073
            r8.endTransaction()     // Catch:{ all -> 0x006d }
            goto L_0x0073
        L_0x006d:
            r8 = move-exception
            com.fossil.k57 r1 = com.fossil.x47.l
            r1.a(r8)
        L_0x0073:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.a(boolean):void");
    }

    @DexIgnore
    public final int b(boolean z) {
        return !z ? w37.i() : w37.g();
    }

    @DexIgnore
    public void b() {
        if (w37.s()) {
            try {
                this.c.a(new a57(this));
            } catch (Throwable th) {
                l.a(th);
            }
        }
    }

    @DexIgnore
    public final void b(int i2, boolean z) {
        if (i2 == -1) {
            i2 = !z ? e() : f();
        }
        if (i2 > 0) {
            int l2 = w37.l() * 60 * w37.k();
            if (i2 > l2 && l2 > 0) {
                i2 = l2;
            }
            int a2 = w37.a();
            int i3 = i2 / a2;
            int i4 = i2 % a2;
            if (w37.q()) {
                k57 k57 = l;
                k57.e("sentStoreEventsByDb sendNumbers=" + i2 + ",important=" + z + ",maxSendNumPerFor1Period=" + l2 + ",maxCount=" + i3 + ",restNumbers=" + i4);
            }
            for (int i5 = 0; i5 < i3; i5++) {
                a(a2, z);
            }
            if (i4 > 0) {
                a(i4, z);
            }
        }
    }

    @DexIgnore
    public final synchronized void b(e47 e47, g67 g67, boolean z, boolean z2) {
        if (w37.j() > 0) {
            if (w37.J > 0 && !z) {
                if (!z2) {
                    if (w37.J > 0) {
                        if (w37.q()) {
                            k57 k57 = l;
                            k57.e("cacheEventsInMemory.size():" + this.i.size() + ",numEventsCachedInMemory:" + w37.J + ",numStoredEvents:" + this.f);
                            k57 k572 = l;
                            StringBuilder sb = new StringBuilder("cache event:");
                            sb.append(e47.f());
                            k572.e(sb.toString());
                        }
                        this.i.put(e47, "");
                        if (this.i.size() >= w37.J) {
                            g();
                        }
                        if (g67 != null) {
                            if (this.i.size() > 0) {
                                g();
                            }
                            g67.a();
                        }
                    }
                }
            }
            a(e47, g67, z);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(com.fossil.e67 r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            r0 = 0
            java.lang.String r1 = r13.a()     // Catch:{ all -> 0x00d8 }
            java.lang.String r2 = com.fossil.v57.a(r1)     // Catch:{ all -> 0x00d8 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x00d8 }
            r3.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "content"
            org.json.JSONObject r5 = r13.b     // Catch:{ all -> 0x00d8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00d8 }
            r3.put(r4, r5)     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "md5sum"
            r3.put(r4, r2)     // Catch:{ all -> 0x00d8 }
            r13.c = r2     // Catch:{ all -> 0x00d8 }
            java.lang.String r2 = "version"
            int r4 = r13.d     // Catch:{ all -> 0x00d8 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00d8 }
            r3.put(r2, r4)     // Catch:{ all -> 0x00d8 }
            com.fossil.g57 r2 = r12.a     // Catch:{ all -> 0x00d8 }
            android.database.sqlite.SQLiteDatabase r4 = r2.getReadableDatabase()     // Catch:{ all -> 0x00d8 }
            java.lang.String r5 = "config"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r2 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x00d8 }
        L_0x003e:
            boolean r4 = r2.moveToNext()     // Catch:{ all -> 0x00d5 }
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x0050
            int r4 = r2.getInt(r6)     // Catch:{ all -> 0x00d5 }
            int r7 = r13.a     // Catch:{ all -> 0x00d5 }
            if (r4 != r7) goto L_0x003e
            r4 = 1
            goto L_0x0051
        L_0x0050:
            r4 = 0
        L_0x0051:
            com.fossil.g57 r7 = r12.a     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r7 = r7.getWritableDatabase()     // Catch:{ all -> 0x00d5 }
            r7.beginTransaction()     // Catch:{ all -> 0x00d5 }
            if (r5 != r4) goto L_0x0076
            com.fossil.g57 r0 = r12.a     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = "config"
            java.lang.String r7 = "type=?"
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ all -> 0x00d5 }
            int r13 = r13.a     // Catch:{ all -> 0x00d5 }
            java.lang.String r13 = java.lang.Integer.toString(r13)     // Catch:{ all -> 0x00d5 }
            r5[r6] = r13     // Catch:{ all -> 0x00d5 }
            int r13 = r0.update(r4, r3, r7, r5)     // Catch:{ all -> 0x00d5 }
            long r3 = (long) r13     // Catch:{ all -> 0x00d5 }
            goto L_0x008d
        L_0x0076:
            java.lang.String r4 = "type"
            int r13 = r13.a     // Catch:{ all -> 0x00d5 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x00d5 }
            r3.put(r4, r13)     // Catch:{ all -> 0x00d5 }
            com.fossil.g57 r13 = r12.a     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = "config"
            long r3 = r13.insert(r4, r0, r3)     // Catch:{ all -> 0x00d5 }
        L_0x008d:
            r5 = -1
            int r13 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x00a7
            com.fossil.k57 r13 = com.fossil.x47.l     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d5 }
            java.lang.String r3 = "Failed to store cfg:"
            r0.<init>(r3)     // Catch:{ all -> 0x00d5 }
            r0.append(r1)     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d5 }
            r13.c(r0)     // Catch:{ all -> 0x00d5 }
            goto L_0x00ba
        L_0x00a7:
            com.fossil.k57 r13 = com.fossil.x47.l     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d5 }
            java.lang.String r3 = "Sucessed to store cfg:"
            r0.<init>(r3)     // Catch:{ all -> 0x00d5 }
            r0.append(r1)     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d5 }
            r13.a(r0)     // Catch:{ all -> 0x00d5 }
        L_0x00ba:
            com.fossil.g57 r13 = r12.a     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ all -> 0x00d5 }
            r13.setTransactionSuccessful()     // Catch:{ all -> 0x00d5 }
            if (r2 == 0) goto L_0x00c8
            r2.close()     // Catch:{ all -> 0x0100 }
        L_0x00c8:
            com.fossil.g57 r13 = r12.a     // Catch:{ Exception -> 0x00d3 }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ Exception -> 0x00d3 }
            r13.endTransaction()     // Catch:{ Exception -> 0x00d3 }
            monitor-exit(r12)
            return
        L_0x00d3:
            monitor-exit(r12)
            return
        L_0x00d5:
            r13 = move-exception
            r0 = r2
            goto L_0x00d9
        L_0x00d8:
            r13 = move-exception
        L_0x00d9:
            com.fossil.k57 r1 = com.fossil.x47.l     // Catch:{ all -> 0x00f0 }
            r1.a(r13)     // Catch:{ all -> 0x00f0 }
            if (r0 == 0) goto L_0x00e3
            r0.close()
        L_0x00e3:
            com.fossil.g57 r13 = r12.a     // Catch:{ Exception -> 0x00ee }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ Exception -> 0x00ee }
            r13.endTransaction()     // Catch:{ Exception -> 0x00ee }
            monitor-exit(r12)
            return
        L_0x00ee:
            monitor-exit(r12)
            return
        L_0x00f0:
            r13 = move-exception
            if (r0 == 0) goto L_0x00f6
            r0.close()
        L_0x00f6:
            com.fossil.g57 r0 = r12.a     // Catch:{ Exception -> 0x0102 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x0102 }
            r0.endTransaction()     // Catch:{ Exception -> 0x0102 }
            goto L_0x0102
        L_0x0100:
            r13 = move-exception
            goto L_0x0103
        L_0x0102:
            throw r13
        L_0x0103:
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.b(com.fossil.e67):void");
    }

    @DexIgnore
    public final void b(List<h57> list, int i2, boolean z) {
        Cursor cursor = null;
        try {
            Cursor query = d(z).query("events", null, "status=?", new String[]{Integer.toString(1)}, null, null, null, Integer.toString(i2));
            while (query.moveToNext()) {
                long j2 = query.getLong(0);
                String string = query.getString(1);
                if (!w37.v) {
                    string = a67.a(string);
                }
                int i3 = query.getInt(2);
                int i4 = query.getInt(3);
                h57 h57 = new h57(j2, string, i3, i4);
                if (w37.q()) {
                    k57 k57 = l;
                    k57.e("peek event, id=" + j2 + ",send_count=" + i4 + ",timestamp=" + query.getLong(4));
                }
                list.add(h57);
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final SQLiteDatabase c(boolean z) {
        return (!z ? this.a : this.b).getWritableDatabase();
    }

    @DexIgnore
    public void c() {
        Cursor cursor = null;
        try {
            Cursor query = this.a.getReadableDatabase().query("config", null, null, null, null, null, null);
            while (query.moveToNext()) {
                int i2 = query.getInt(0);
                String string = query.getString(1);
                query.getString(2);
                int i3 = query.getInt(3);
                e67 e67 = new e67(i2);
                e67.a = i2;
                e67.b = new JSONObject(string);
                e67.d = i3;
                w37.a(m, e67);
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final SQLiteDatabase d(boolean z) {
        return (!z ? this.a : this.b).getReadableDatabase();
    }

    @DexIgnore
    public final void d() {
        this.f = e() + f();
    }

    @DexIgnore
    public final int e() {
        return (int) DatabaseUtils.queryNumEntries(this.a.getReadableDatabase(), "events");
    }

    @DexIgnore
    public final int f() {
        return (int) DatabaseUtils.queryNumEntries(this.b.getReadableDatabase(), "events");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00dd A[SYNTHETIC, Splitter:B:37:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g() {
        /*
            r11 = this;
            boolean r0 = r11.j
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            java.util.concurrent.ConcurrentHashMap<com.fossil.e47, java.lang.String> r0 = r11.i
            monitor-enter(r0)
            java.util.concurrent.ConcurrentHashMap<com.fossil.e47, java.lang.String> r1 = r11.i     // Catch:{ all -> 0x0131 }
            int r1 = r1.size()     // Catch:{ all -> 0x0131 }
            if (r1 != 0) goto L_0x0012
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            return
        L_0x0012:
            r1 = 1
            r11.j = r1     // Catch:{ all -> 0x0131 }
            boolean r2 = com.fossil.w37.q()     // Catch:{ all -> 0x0131 }
            if (r2 == 0) goto L_0x0048
            com.fossil.k57 r2 = com.fossil.x47.l     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = "insert "
            r3.<init>(r4)     // Catch:{ all -> 0x0131 }
            java.util.concurrent.ConcurrentHashMap<com.fossil.e47, java.lang.String> r4 = r11.i     // Catch:{ all -> 0x0131 }
            int r4 = r4.size()     // Catch:{ all -> 0x0131 }
            r3.append(r4)     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = " events ,numEventsCachedInMemory:"
            r3.append(r4)     // Catch:{ all -> 0x0131 }
            int r4 = com.fossil.w37.J     // Catch:{ all -> 0x0131 }
            r3.append(r4)     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = ",numStoredEvents:"
            r3.append(r4)     // Catch:{ all -> 0x0131 }
            int r4 = r11.f     // Catch:{ all -> 0x0131 }
            r3.append(r4)     // Catch:{ all -> 0x0131 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0131 }
            r2.e(r3)     // Catch:{ all -> 0x0131 }
        L_0x0048:
            r2 = 0
            com.fossil.g57 r3 = r11.a     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ all -> 0x00d5 }
            r3.beginTransaction()     // Catch:{ all -> 0x00d2 }
            java.util.concurrent.ConcurrentHashMap<com.fossil.e47, java.lang.String> r4 = r11.i     // Catch:{ all -> 0x00d2 }
            java.util.Set r4 = r4.entrySet()     // Catch:{ all -> 0x00d2 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x00d2 }
        L_0x005c:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x00d2 }
            if (r5 == 0) goto L_0x00bf
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x00d2 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x00d2 }
            java.lang.Object r5 = r5.getKey()     // Catch:{ all -> 0x00d2 }
            com.fossil.e47 r5 = (com.fossil.e47) r5     // Catch:{ all -> 0x00d2 }
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ all -> 0x00d2 }
            r6.<init>()     // Catch:{ all -> 0x00d2 }
            java.lang.String r7 = r5.f()     // Catch:{ all -> 0x00d2 }
            boolean r8 = com.fossil.w37.q()     // Catch:{ all -> 0x00d2 }
            if (r8 == 0) goto L_0x0090
            com.fossil.k57 r8 = com.fossil.x47.l     // Catch:{ all -> 0x00d2 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d2 }
            java.lang.String r10 = "insert content:"
            r9.<init>(r10)     // Catch:{ all -> 0x00d2 }
            r9.append(r7)     // Catch:{ all -> 0x00d2 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00d2 }
            r8.e(r9)     // Catch:{ all -> 0x00d2 }
        L_0x0090:
            java.lang.String r7 = com.fossil.a67.b(r7)     // Catch:{ all -> 0x00d2 }
            java.lang.String r8 = "content"
            r6.put(r8, r7)     // Catch:{ all -> 0x00d2 }
            java.lang.String r7 = "send_count"
            java.lang.String r8 = "0"
            r6.put(r7, r8)     // Catch:{ all -> 0x00d2 }
            java.lang.String r7 = "status"
            java.lang.String r8 = java.lang.Integer.toString(r1)     // Catch:{ all -> 0x00d2 }
            r6.put(r7, r8)     // Catch:{ all -> 0x00d2 }
            java.lang.String r7 = "timestamp"
            long r8 = r5.b()     // Catch:{ all -> 0x00d2 }
            java.lang.Long r5 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x00d2 }
            r6.put(r7, r5)     // Catch:{ all -> 0x00d2 }
            java.lang.String r5 = "events"
            r3.insert(r5, r2, r6)     // Catch:{ all -> 0x00d2 }
            r4.remove()     // Catch:{ all -> 0x00d2 }
            goto L_0x005c
        L_0x00bf:
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x00d2 }
            if (r3 == 0) goto L_0x00e8
            r3.endTransaction()     // Catch:{ all -> 0x00cb }
            r11.d()     // Catch:{ all -> 0x00cb }
            goto L_0x00e8
        L_0x00cb:
            r1 = move-exception
            com.fossil.k57 r2 = com.fossil.x47.l
        L_0x00ce:
            r2.a(r1)
            goto L_0x00e8
        L_0x00d2:
            r1 = move-exception
            r2 = r3
            goto L_0x00d6
        L_0x00d5:
            r1 = move-exception
        L_0x00d6:
            com.fossil.k57 r3 = com.fossil.x47.l     // Catch:{ all -> 0x0120 }
            r3.a(r1)     // Catch:{ all -> 0x0120 }
            if (r2 == 0) goto L_0x00e8
            r2.endTransaction()     // Catch:{ all -> 0x00e4 }
            r11.d()     // Catch:{ all -> 0x00e4 }
            goto L_0x00e8
        L_0x00e4:
            r1 = move-exception
            com.fossil.k57 r2 = com.fossil.x47.l
            goto L_0x00ce
        L_0x00e8:
            r1 = 0
            r11.j = r1
            boolean r1 = com.fossil.w37.q()
            if (r1 == 0) goto L_0x011e
            com.fossil.k57 r1 = com.fossil.x47.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "after insert, cacheEventsInMemory.size():"
            r2.<init>(r3)
            java.util.concurrent.ConcurrentHashMap<com.fossil.e47, java.lang.String> r3 = r11.i
            int r3 = r3.size()
            r2.append(r3)
            java.lang.String r3 = ",numEventsCachedInMemory:"
            r2.append(r3)
            int r3 = com.fossil.w37.J
            r2.append(r3)
            java.lang.String r3 = ",numStoredEvents:"
            r2.append(r3)
            int r3 = r11.f
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.e(r2)
        L_0x011e:
            monitor-exit(r0)
            return
        L_0x0120:
            r1 = move-exception
            if (r2 == 0) goto L_0x0130
            r2.endTransaction()     // Catch:{ all -> 0x012a }
            r11.d()     // Catch:{ all -> 0x012a }
            goto L_0x0130
        L_0x012a:
            r2 = move-exception
            com.fossil.k57 r3 = com.fossil.x47.l
            r3.a(r2)
        L_0x0130:
            throw r1
        L_0x0131:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x47.g():void");
    }

    @DexIgnore
    public final void h() {
        Cursor cursor = null;
        try {
            Cursor query = this.a.getReadableDatabase().query("keyvalues", null, null, null, null, null, null);
            while (query.moveToNext()) {
                this.k.put(query.getString(0), query.getString(1));
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }
}
