package com.fossil;

import android.view.View;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se5 implements View.OnClickListener {
    @DexIgnore
    public AtomicBoolean a; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ long b; // = 1000;
    @DexIgnore
    public /* final */ View.OnClickListener c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ se5 a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public a(se5 se5, View view) {
            this.a = se5;
            this.b = view;
        }

        @DexIgnore
        public final void run() {
            this.a.a.set(true);
        }
    }

    @DexIgnore
    public se5(View.OnClickListener onClickListener) {
        ee7.b(onClickListener, "clickListener");
        this.c = onClickListener;
    }

    @DexIgnore
    public void onClick(View view) {
        if (this.a.getAndSet(false) && view != null) {
            view.postDelayed(new a(this, view), this.b);
            this.c.onClick(view);
        }
    }
}
