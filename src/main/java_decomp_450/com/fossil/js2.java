package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js2 extends AbstractSet<K> {
    @DexIgnore
    public /* final */ /* synthetic */ ds2 a;

    @DexIgnore
    public js2(ds2 ds2) {
        this.a = ds2;
    }

    @DexIgnore
    public final void clear() {
        this.a.clear();
    }

    @DexIgnore
    public final boolean contains(Object obj) {
        return this.a.containsKey(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<K> iterator() {
        return this.a.zze();
    }

    @DexIgnore
    public final boolean remove(@NullableDecl Object obj) {
        Map zzb = this.a.zzb();
        if (zzb != null) {
            return zzb.keySet().remove(obj);
        }
        return this.a.c(obj) != ds2.g;
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }
}
