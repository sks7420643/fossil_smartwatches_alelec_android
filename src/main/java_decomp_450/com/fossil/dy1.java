package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy1 implements Factory<by1> {
    @DexIgnore
    public static /* final */ dy1 a; // = new dy1();

    @DexIgnore
    public static dy1 a() {
        return a;
    }

    @DexIgnore
    public static by1 b() {
        by1 a2 = cy1.a();
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public by1 get() {
        return b();
    }
}
