package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x03 implements u03 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.upload.file_truncate_fix", false);

    @DexIgnore
    @Override // com.fossil.u03
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
