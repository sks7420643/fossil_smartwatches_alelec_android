package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hs6 implements Factory<gs6> {
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> d;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> e;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> f;
    @DexIgnore
    public /* final */ Provider<nw5> g;
    @DexIgnore
    public /* final */ Provider<vu5> h;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> i;
    @DexIgnore
    public /* final */ Provider<ch5> j;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;

    @DexIgnore
    public hs6(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchFaceRepository> provider5, Provider<RingStyleRepository> provider6, Provider<nw5> provider7, Provider<vu5> provider8, Provider<NotificationSettingsDatabase> provider9, Provider<ch5> provider10, Provider<WatchLocalizationRepository> provider11, Provider<AlarmsRepository> provider12) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
    }

    @DexIgnore
    public static hs6 a(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchFaceRepository> provider5, Provider<RingStyleRepository> provider6, Provider<nw5> provider7, Provider<vu5> provider8, Provider<NotificationSettingsDatabase> provider9, Provider<ch5> provider10, Provider<WatchLocalizationRepository> provider11, Provider<AlarmsRepository> provider12) {
        return new hs6(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12);
    }

    @DexIgnore
    public static gs6 a(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, CategoryRepository categoryRepository, WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository, nw5 nw5, vu5 vu5, NotificationSettingsDatabase notificationSettingsDatabase, ch5 ch5, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository) {
        return new gs6(watchAppRepository, complicationRepository, dianaPresetRepository, categoryRepository, watchFaceRepository, ringStyleRepository, nw5, vu5, notificationSettingsDatabase, ch5, watchLocalizationRepository, alarmsRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public gs6 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get());
    }
}
