package com.fossil;

import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o60 extends je0 {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ p60 b;
    @DexIgnore
    public /* final */ eu0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final o60 a(eu0 eu0, HashMap<b, Object> hashMap) {
            return new o60(p60.d.a(eu0, hashMap), eu0);
        }
    }

    @DexIgnore
    public enum b {
        HAS_SERVICE_CHANGED
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ o60(p60 p60, eu0 eu0, int i) {
        this(p60, (i & 2) != 0 ? new eu0(null, is0.SUCCESS, null, 5) : eu0);
    }

    @DexIgnore
    @Override // com.fossil.je0, com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = super.a();
        eu0 eu0 = this.c;
        if (eu0.b != is0.SUCCESS) {
            yz0.a(a2, r51.z1, eu0.a());
        }
        return a2;
    }

    @DexIgnore
    public final eu0 b() {
        return this.c;
    }

    @DexIgnore
    public o60(p60 p60, eu0 eu0) {
        super(p60);
        this.b = p60;
        this.c = eu0;
    }

    @DexIgnore
    @Override // com.fossil.je0
    public p60 getErrorCode() {
        return this.b;
    }
}
