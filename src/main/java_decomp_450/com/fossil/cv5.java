package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv5 implements Factory<bv5> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;

    @DexIgnore
    public cv5(Provider<QuickResponseRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static cv5 a(Provider<QuickResponseRepository> provider) {
        return new cv5(provider);
    }

    @DexIgnore
    public static bv5 a(QuickResponseRepository quickResponseRepository) {
        return new bv5(quickResponseRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public bv5 get() {
        return a(this.a.get());
    }
}
