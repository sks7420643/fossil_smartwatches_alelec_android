package com.fossil;

import android.app.Activity;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a82 extends l62 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ Activity b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;

    @DexIgnore
    public a82(Intent intent, Activity activity, int i) {
        this.a = intent;
        this.b = activity;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.l62
    public final void a() {
        Intent intent = this.a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
