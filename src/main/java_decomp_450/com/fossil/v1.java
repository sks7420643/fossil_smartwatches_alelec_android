package com.fossil;

import android.content.Context;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(p1 p1Var, boolean z);

        @DexIgnore
        boolean a(p1 p1Var);
    }

    @DexIgnore
    void a(Context context, p1 p1Var);

    @DexIgnore
    void a(Parcelable parcelable);

    @DexIgnore
    void a(p1 p1Var, boolean z);

    @DexIgnore
    void a(a aVar);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    boolean a(a2 a2Var);

    @DexIgnore
    boolean a(p1 p1Var, r1 r1Var);

    @DexIgnore
    boolean b();

    @DexIgnore
    boolean b(p1 p1Var, r1 r1Var);

    @DexIgnore
    Parcelable c();

    @DexIgnore
    int getId();
}
