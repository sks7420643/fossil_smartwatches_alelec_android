package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t30 implements Handler.Callback {
    @DexIgnore
    public static /* final */ b i; // = new a();
    @DexIgnore
    public volatile iw a;
    @DexIgnore
    public /* final */ Map<FragmentManager, s30> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<androidx.fragment.app.FragmentManager, w30> c; // = new HashMap();
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ n4<View, Fragment> f; // = new n4<>();
    @DexIgnore
    public /* final */ n4<View, android.app.Fragment> g; // = new n4<>();
    @DexIgnore
    public /* final */ Bundle h; // = new Bundle();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        @Override // com.fossil.t30.b
        public iw a(aw awVar, p30 p30, u30 u30, Context context) {
            return new iw(awVar, p30, u30, context);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        iw a(aw awVar, p30 p30, u30 u30, Context context);
    }

    @DexIgnore
    public t30(b bVar) {
        this.e = bVar == null ? i : bVar;
        this.d = new Handler(Looper.getMainLooper(), this);
    }

    @DexIgnore
    public static Activity c(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return c(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    @DexIgnore
    public static boolean d(Context context) {
        Activity c2 = c(context);
        return c2 == null || !c2.isFinishing();
    }

    @DexIgnore
    public iw a(Context context) {
        if (context != null) {
            if (v50.d() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return a((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return a((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return a(contextWrapper.getBaseContext());
                    }
                }
            }
            return b(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    @DexIgnore
    public final iw b(Context context) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = this.e.a(aw.a(context.getApplicationContext()), new j30(), new o30(), context.getApplicationContext());
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        Object obj;
        Object obj2;
        Object obj3;
        int i2 = message.what;
        Object obj4 = null;
        boolean z = true;
        if (i2 == 1) {
            obj3 = (FragmentManager) message.obj;
            obj2 = this.b.remove(obj3);
        } else if (i2 != 2) {
            z = false;
            obj = null;
            if (z && obj4 == null && Log.isLoggable("RMRetriever", 5)) {
                Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
            }
            return z;
        } else {
            obj3 = (androidx.fragment.app.FragmentManager) message.obj;
            obj2 = this.c.remove(obj3);
        }
        obj4 = obj2;
        obj = obj3;
        Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
        return z;
    }

    @DexIgnore
    @TargetApi(17)
    public static void c(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    @DexIgnore
    @Deprecated
    public final void b(FragmentManager fragmentManager, n4<View, android.app.Fragment> n4Var) {
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            this.h.putInt("key", i2);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.h, "key");
            } catch (Exception unused) {
            }
            if (fragment != null) {
                if (fragment.getView() != null) {
                    n4Var.put(fragment.getView(), fragment);
                    if (Build.VERSION.SDK_INT >= 17) {
                        a(fragment.getChildFragmentManager(), n4Var);
                    }
                }
                i2 = i3;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public iw a(FragmentActivity fragmentActivity) {
        if (v50.c()) {
            return a(fragmentActivity.getApplicationContext());
        }
        c((Activity) fragmentActivity);
        return a(fragmentActivity, fragmentActivity.getSupportFragmentManager(), (Fragment) null, d(fragmentActivity));
    }

    @DexIgnore
    public iw a(Fragment fragment) {
        u50.a(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (v50.c()) {
            return a(fragment.getContext().getApplicationContext());
        }
        return a(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    @DexIgnore
    @Deprecated
    public s30 b(Activity activity) {
        return a(activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
    }

    @DexIgnore
    public iw a(Activity activity) {
        if (v50.c()) {
            return a(activity.getApplicationContext());
        }
        c(activity);
        return a(activity, activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
    }

    @DexIgnore
    public iw a(View view) {
        if (v50.c()) {
            return a(view.getContext().getApplicationContext());
        }
        u50.a(view);
        u50.a(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        Activity c2 = c(view.getContext());
        if (c2 == null) {
            return a(view.getContext().getApplicationContext());
        }
        if (c2 instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) c2;
            Fragment a2 = a(view, fragmentActivity);
            return a2 != null ? a(a2) : a(fragmentActivity);
        }
        android.app.Fragment a3 = a(view, c2);
        if (a3 == null) {
            return a(c2);
        }
        return a(a3);
    }

    @DexIgnore
    public static void a(Collection<Fragment> collection, Map<View, Fragment> map) {
        if (collection != null) {
            for (Fragment fragment : collection) {
                if (!(fragment == null || fragment.getView() == null)) {
                    map.put(fragment.getView(), fragment);
                    a(fragment.getChildFragmentManager().v(), map);
                }
            }
        }
    }

    @DexIgnore
    public final Fragment a(View view, FragmentActivity fragmentActivity) {
        this.f.clear();
        a(fragmentActivity.getSupportFragmentManager().v(), this.f);
        View findViewById = fragmentActivity.findViewById(16908290);
        Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.f.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.f.clear();
        return fragment;
    }

    @DexIgnore
    @Deprecated
    public final android.app.Fragment a(View view, Activity activity) {
        this.g.clear();
        a(activity.getFragmentManager(), this.g);
        View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.g.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.g.clear();
        return fragment;
    }

    @DexIgnore
    @TargetApi(26)
    @Deprecated
    public final void a(FragmentManager fragmentManager, n4<View, android.app.Fragment> n4Var) {
        if (Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment fragment : fragmentManager.getFragments()) {
                if (fragment.getView() != null) {
                    n4Var.put(fragment.getView(), fragment);
                    a(fragment.getChildFragmentManager(), n4Var);
                }
            }
            return;
        }
        b(fragmentManager, n4Var);
    }

    @DexIgnore
    @TargetApi(17)
    @Deprecated
    public iw a(android.app.Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (v50.c() || Build.VERSION.SDK_INT < 17) {
            return a(fragment.getActivity().getApplicationContext());
        } else {
            return a(fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
    }

    @DexIgnore
    public final s30 a(FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        s30 s30 = (s30) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (s30 == null && (s30 = this.b.get(fragmentManager)) == null) {
            s30 = new s30();
            s30.b(fragment);
            if (z) {
                s30.b().b();
            }
            this.b.put(fragmentManager, s30);
            fragmentManager.beginTransaction().add(s30, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.d.obtainMessage(1, fragmentManager).sendToTarget();
        }
        return s30;
    }

    @DexIgnore
    @Deprecated
    public final iw a(Context context, FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        s30 a2 = a(fragmentManager, fragment, z);
        iw d2 = a2.d();
        if (d2 != null) {
            return d2;
        }
        iw a3 = this.e.a(aw.a(context), a2.b(), a2.e(), context);
        a2.a(a3);
        return a3;
    }

    @DexIgnore
    public w30 a(Context context, androidx.fragment.app.FragmentManager fragmentManager) {
        return a(fragmentManager, (Fragment) null, d(context));
    }

    @DexIgnore
    public final w30 a(androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        w30 w30 = (w30) fragmentManager.b("com.bumptech.glide.manager");
        if (w30 == null && (w30 = this.c.get(fragmentManager)) == null) {
            w30 = new w30();
            w30.b(fragment);
            if (z) {
                w30.a1().b();
            }
            this.c.put(fragmentManager, w30);
            nc b2 = fragmentManager.b();
            b2.a(w30, "com.bumptech.glide.manager");
            b2.b();
            this.d.obtainMessage(2, fragmentManager).sendToTarget();
        }
        return w30;
    }

    @DexIgnore
    public final iw a(Context context, androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        w30 a2 = a(fragmentManager, fragment, z);
        iw c1 = a2.c1();
        if (c1 != null) {
            return c1;
        }
        iw a3 = this.e.a(aw.a(context), a2.a1(), a2.d1(), context);
        a2.a(a3);
        return a3;
    }
}
