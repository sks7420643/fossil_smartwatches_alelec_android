package com.fossil;

import com.fossil.xt1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt1 extends xt1 {
    @DexIgnore
    public /* final */ xt1.c a;
    @DexIgnore
    public /* final */ xt1.b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xt1.a {
        @DexIgnore
        public xt1.c a;
        @DexIgnore
        public xt1.b b;

        @DexIgnore
        @Override // com.fossil.xt1.a
        public xt1.a a(xt1.c cVar) {
            this.a = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.xt1.a
        public xt1.a a(xt1.b bVar) {
            this.b = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.xt1.a
        public xt1 a() {
            return new rt1(this.a, this.b, null);
        }
    }

    @DexIgnore
    public /* synthetic */ rt1(xt1.c cVar, xt1.b bVar, a aVar) {
        this.a = cVar;
        this.b = bVar;
    }

    @DexIgnore
    @Override // com.fossil.xt1
    public xt1.b a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.xt1
    public xt1.c b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof xt1)) {
            return false;
        }
        xt1.c cVar = this.a;
        if (cVar != null ? cVar.equals(((rt1) obj).a) : ((rt1) obj).a == null) {
            xt1.b bVar = this.b;
            if (bVar == null) {
                if (((rt1) obj).b == null) {
                    return true;
                }
            } else if (bVar.equals(((rt1) obj).b)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        xt1.c cVar = this.a;
        int i = 0;
        int hashCode = ((cVar == null ? 0 : cVar.hashCode()) ^ 1000003) * 1000003;
        xt1.b bVar = this.b;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "NetworkConnectionInfo{networkType=" + this.a + ", mobileSubtype=" + this.b + "}";
    }
}
