package com.fossil;

import com.fossil.bw2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xp2 extends bw2<xp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ xp2 zzg;
    @DexIgnore
    public static volatile wx2<xp2> zzh;
    @DexIgnore
    public gw2 zzc; // = bw2.n();
    @DexIgnore
    public gw2 zzd; // = bw2.n();
    @DexIgnore
    public jw2<qp2> zze; // = bw2.o();
    @DexIgnore
    public jw2<yp2> zzf; // = bw2.o();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<xp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(xp2.zzg);
        }

        @DexIgnore
        public final a a(Iterable<? extends Long> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).a(iterable);
            return this;
        }

        @DexIgnore
        public final a b(Iterable<? extends Long> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).b(iterable);
            return this;
        }

        @DexIgnore
        public final a c(Iterable<? extends qp2> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).c(iterable);
            return this;
        }

        @DexIgnore
        public final a d(Iterable<? extends yp2> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).d(iterable);
            return this;
        }

        @DexIgnore
        public final a o() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).x();
            return this;
        }

        @DexIgnore
        public final a zza() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).w();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).d(i);
            return this;
        }

        @DexIgnore
        public final a b(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((xp2) ((bw2.a) this).b).e(i);
            return this;
        }
    }

    /*
    static {
        xp2 xp2 = new xp2();
        zzg = xp2;
        bw2.a(xp2.class, xp2);
    }
    */

    @DexIgnore
    public static a A() {
        return (a) zzg.e();
    }

    @DexIgnore
    public static xp2 B() {
        return zzg;
    }

    @DexIgnore
    public final void a(Iterable<? extends Long> iterable) {
        gw2 gw2 = this.zzc;
        if (!gw2.zza()) {
            this.zzc = bw2.a(gw2);
        }
        ju2.a(iterable, this.zzc);
    }

    @DexIgnore
    public final void b(Iterable<? extends Long> iterable) {
        gw2 gw2 = this.zzd;
        if (!gw2.zza()) {
            this.zzd = bw2.a(gw2);
        }
        ju2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final void c(Iterable<? extends qp2> iterable) {
        y();
        ju2.a(iterable, this.zze);
    }

    @DexIgnore
    public final void d(int i) {
        y();
        this.zze.remove(i);
    }

    @DexIgnore
    public final void e(int i) {
        z();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final int p() {
        return this.zzc.size();
    }

    @DexIgnore
    public final List<Long> q() {
        return this.zzd;
    }

    @DexIgnore
    public final int r() {
        return this.zzd.size();
    }

    @DexIgnore
    public final List<qp2> s() {
        return this.zze;
    }

    @DexIgnore
    public final int t() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<yp2> u() {
        return this.zzf;
    }

    @DexIgnore
    public final int v() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void w() {
        this.zzc = bw2.n();
    }

    @DexIgnore
    public final void x() {
        this.zzd = bw2.n();
    }

    @DexIgnore
    public final void y() {
        jw2<qp2> jw2 = this.zze;
        if (!jw2.zza()) {
            this.zze = bw2.a(jw2);
        }
    }

    @DexIgnore
    public final void z() {
        jw2<yp2> jw2 = this.zzf;
        if (!jw2.zza()) {
            this.zzf = bw2.a(jw2);
        }
    }

    @DexIgnore
    public final List<Long> zza() {
        return this.zzc;
    }

    @DexIgnore
    public final yp2 c(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final void d(Iterable<? extends yp2> iterable) {
        z();
        ju2.a(iterable, this.zzf);
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new xp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzg, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zzc", "zzd", "zze", qp2.class, "zzf", yp2.class});
            case 4:
                return zzg;
            case 5:
                wx2<xp2> wx2 = zzh;
                if (wx2 == null) {
                    synchronized (xp2.class) {
                        wx2 = zzh;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzg);
                            zzh = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final qp2 b(int i) {
        return this.zze.get(i);
    }
}
