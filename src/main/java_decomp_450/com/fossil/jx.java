package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jx<T> {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        jx<T> a(T t);

        @DexIgnore
        Class<T> getDataClass();
    }

    @DexIgnore
    void a();

    @DexIgnore
    T b() throws IOException;
}
