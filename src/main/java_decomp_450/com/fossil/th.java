package com.fossil;

import android.content.Context;
import com.fossil.ci;
import com.fossil.xi;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class th {
    @DexIgnore
    public /* final */ xi.c a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ ci.d d;
    @DexIgnore
    public /* final */ List<ci.b> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ ci.c g;
    @DexIgnore
    public /* final */ Executor h;
    @DexIgnore
    public /* final */ Executor i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Set<Integer> m;

    @DexIgnore
    public th(Context context, String str, xi.c cVar, ci.d dVar, List<ci.b> list, boolean z, ci.c cVar2, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file) {
        this.a = cVar;
        this.b = context;
        this.c = str;
        this.d = dVar;
        this.e = list;
        this.f = z;
        this.g = cVar2;
        this.h = executor;
        this.i = executor2;
        this.j = z2;
        this.k = z3;
        this.l = z4;
        this.m = set;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        Set<Integer> set;
        if ((!(i2 > i3) || !this.l) && this.k && ((set = this.m) == null || !set.contains(Integer.valueOf(i2)))) {
            return true;
        }
        return false;
    }
}
