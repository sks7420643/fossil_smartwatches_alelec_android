package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n56 implements MembersInjector<HybridCustomizeEditActivity> {
    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, v56 v56) {
        hybridCustomizeEditActivity.y = v56;
    }

    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, rj4 rj4) {
        hybridCustomizeEditActivity.z = rj4;
    }
}
