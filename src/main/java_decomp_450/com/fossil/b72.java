package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b72 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<b72> CREATOR; // = new j82();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Account b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ GoogleSignInAccount d;

    @DexIgnore
    public b72(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.a = i;
        this.b = account;
        this.c = i2;
        this.d = googleSignInAccount;
    }

    @DexIgnore
    public Account c() {
        return this.b;
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public GoogleSignInAccount g() {
        return this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, (Parcelable) c(), i, false);
        k72.a(parcel, 3, e());
        k72.a(parcel, 4, (Parcelable) g(), i, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public b72(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
