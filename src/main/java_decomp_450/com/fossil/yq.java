package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yq<K, V> {
    @DexIgnore
    public /* final */ a<K, V> a; // = new a<>(null, 1, null);
    @DexIgnore
    public /* final */ HashMap<K, a<K, V>> b; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> {
        @DexIgnore
        public List<V> a;
        @DexIgnore
        public a<K, V> b;
        @DexIgnore
        public a<K, V> c;
        @DexIgnore
        public /* final */ K d;

        @DexIgnore
        public a() {
            this(null, 1, null);
        }

        @DexIgnore
        public a(K k) {
            this.d = k;
            this.b = this;
            this.c = this;
        }

        @DexIgnore
        public final K a() {
            return this.d;
        }

        @DexIgnore
        public final void b(a<K, V> aVar) {
            ee7.b(aVar, "<set-?>");
            this.b = aVar;
        }

        @DexIgnore
        public final a<K, V> c() {
            return this.b;
        }

        @DexIgnore
        public final V d() {
            List<V> list = this.a;
            if (list == null || !(!list.isEmpty())) {
                return null;
            }
            return list.remove(w97.a((List) list));
        }

        @DexIgnore
        public final int e() {
            List<V> list = this.a;
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        @DexIgnore
        public final void a(a<K, V> aVar) {
            ee7.b(aVar, "<set-?>");
            this.c = aVar;
        }

        @DexIgnore
        public final a<K, V> b() {
            return this.c;
        }

        @DexIgnore
        public final void a(V v) {
            List<V> list = this.a;
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(v);
            this.a = list;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Object obj, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : obj);
        }
    }

    @DexIgnore
    public final void a(K k, V v) {
        HashMap<K, a<K, V>> hashMap = this.b;
        a<K, V> aVar = hashMap.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            b(aVar);
            hashMap.put(k, aVar);
        }
        aVar.a((Object) v);
    }

    @DexIgnore
    public final void b(a<K, V> aVar) {
        c(aVar);
        aVar.b(this.a.c());
        aVar.a((a) this.a);
        d(aVar);
    }

    @DexIgnore
    public final <K, V> void c(a<K, V> aVar) {
        aVar.c().a((a) aVar.b());
        aVar.b().b(aVar.c());
    }

    @DexIgnore
    public final <K, V> void d(a<K, V> aVar) {
        aVar.b().b(aVar);
        aVar.c().a((a) aVar);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GroupedLinkedMap( ");
        a<K, V> b2 = this.a.b();
        boolean z = false;
        while (!ee7.a(b2, this.a)) {
            sb.append('{');
            sb.append((Object) b2.a());
            sb.append(':');
            sb.append(b2.e());
            sb.append("}, ");
            b2 = b2.b();
            z = true;
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    @DexIgnore
    public final V a(K k) {
        HashMap<K, a<K, V>> hashMap = this.b;
        a<K, V> aVar = hashMap.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            hashMap.put(k, aVar);
        }
        a<K, V> aVar2 = aVar;
        a((a) aVar2);
        return aVar2.d();
    }

    @DexIgnore
    public final V a() {
        a<K, V> c = this.a.c();
        while (!ee7.a(c, this.a)) {
            V d = c.d();
            if (d != null) {
                return d;
            }
            c(c);
            HashMap<K, a<K, V>> hashMap = this.b;
            K a2 = c.a();
            if (hashMap != null) {
                xe7.c(hashMap).remove(a2);
                c = c.c();
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>");
            }
        }
        return null;
    }

    @DexIgnore
    public final void a(a<K, V> aVar) {
        c(aVar);
        aVar.b(this.a);
        aVar.a((a) this.a.b());
        d(aVar);
    }
}
