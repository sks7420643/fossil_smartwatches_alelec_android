package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class bz0 extends Enum<bz0> {
    @DexIgnore
    public static /* final */ bz0 b;
    @DexIgnore
    public static /* final */ bz0 c;
    @DexIgnore
    public static /* final */ bz0 d;
    @DexIgnore
    public static /* final */ bz0 e;
    @DexIgnore
    public static /* final */ bz0 f;
    @DexIgnore
    public static /* final */ bz0 g;
    @DexIgnore
    public static /* final */ bz0 h;
    @DexIgnore
    public static /* final */ /* synthetic */ bz0[] i;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        bz0 bz0 = new bz0("SENDER_NAME", 1, (byte) 2);
        b = bz0;
        bz0 bz02 = new bz0("APP_BUNDLE_CRC32", 2, (byte) 4);
        c = bz02;
        bz0 bz03 = new bz0("GROUP_ID", 3, (byte) 128);
        d = bz03;
        bz0 bz04 = new bz0("ICON_IMAGE", 5, (byte) 130);
        e = bz04;
        bz0 bz05 = new bz0("PRIORITY", 6, (byte) 193);
        f = bz05;
        bz0 bz06 = new bz0("HAND_MOVING", 7, (byte) 194);
        g = bz06;
        bz0 bz07 = new bz0("VIBE", 8, (byte) 195);
        h = bz07;
        i = new bz0[]{new bz0("APPLICATION_NAME", 0, (byte) 1), bz0, bz02, bz03, new bz0("APP_DISPLAY_NAME", 4, (byte) 129), bz04, bz05, bz06, bz07};
    }
    */

    @DexIgnore
    public bz0(String str, int i2, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static bz0 valueOf(String str) {
        return (bz0) Enum.valueOf(bz0.class, str);
    }

    @DexIgnore
    public static bz0[] values() {
        return (bz0[]) i.clone();
    }
}
