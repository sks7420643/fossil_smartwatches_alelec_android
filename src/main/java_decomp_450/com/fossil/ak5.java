package com.fossil;

import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ak5 implements MembersInjector<FossilFirebaseMessagingService> {
    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, co6 co6) {
        fossilFirebaseMessagingService.g = co6;
    }

    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, uj5 uj5) {
        fossilFirebaseMessagingService.h = uj5;
    }

    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, ch5 ch5) {
        fossilFirebaseMessagingService.i = ch5;
    }

    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, to4 to4) {
        fossilFirebaseMessagingService.j = to4;
    }

    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, lm4 lm4) {
        fossilFirebaseMessagingService.p = lm4;
    }
}
