package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il3 extends kh3 {
    @DexIgnore
    public Handler c;
    @DexIgnore
    public /* final */ rl3 d; // = new rl3(this);
    @DexIgnore
    public /* final */ pl3 e; // = new pl3(this);
    @DexIgnore
    public /* final */ ol3 f; // = new ol3(this);

    @DexIgnore
    public il3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public final void A() {
        g();
        if (this.c == null) {
            this.c = new b43(Looper.getMainLooper());
        }
    }

    @DexIgnore
    public final boolean a(boolean z, boolean z2, long j) {
        return this.e.a(z, z2, j);
    }

    @DexIgnore
    public final void b(long j) {
        g();
        A();
        e().B().a("Activity resumed, time", Long.valueOf(j));
        if (l().a(wb3.D0)) {
            if (l().r().booleanValue() || k().w.a()) {
                this.e.a(j);
            }
            this.f.a();
        } else {
            this.f.a();
            if (l().r().booleanValue()) {
                this.e.a(j);
            }
        }
        rl3 rl3 = this.d;
        rl3.a.g();
        if (((ii3) rl3.a).a.g()) {
            if (!rl3.a.l().a(wb3.D0)) {
                rl3.a.k().w.a(false);
            }
            rl3.a(rl3.a.zzm().b(), false);
        }
    }

    @DexIgnore
    public final void c(long j) {
        g();
        A();
        e().B().a("Activity paused, time", Long.valueOf(j));
        this.f.a(j);
        if (l().r().booleanValue()) {
            this.e.b(j);
        }
        rl3 rl3 = this.d;
        if (!rl3.a.l().a(wb3.D0)) {
            rl3.a.k().w.a(true);
        }
    }

    @DexIgnore
    @Override // com.fossil.kh3
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final long a(long j) {
        return this.e.c(j);
    }
}
