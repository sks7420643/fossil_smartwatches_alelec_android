package com.fossil;

import android.content.Context;
import android.util.Log;
import com.fossil.k30;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n30 implements l30 {
    @DexIgnore
    @Override // com.fossil.l30
    public k30 a(Context context, k30.a aVar) {
        boolean z = v6.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        if (Log.isLoggable("ConnectivityMonitor", 3)) {
            Log.d("ConnectivityMonitor", z ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor" : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
        }
        if (z) {
            return new m30(context, aVar);
        }
        return new r30();
    }
}
