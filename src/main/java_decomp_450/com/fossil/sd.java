package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface sd extends LifecycleOwner {
    @DexIgnore
    @Override // androidx.lifecycle.LifecycleOwner
    LifecycleRegistry getLifecycle();
}
