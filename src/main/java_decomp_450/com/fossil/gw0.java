package com.fossil;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gw0 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.AUTHENTICATION}));
    @DexIgnore
    public Boolean D;
    @DexIgnore
    public /* final */ byte[] E; // = new byte[8];
    @DexIgnore
    public /* final */ byte[] F;
    @DexIgnore
    public /* final */ byte[] G;

    @DexIgnore
    public gw0(ri1 ri1, en0 en0, byte[] bArr) {
        super(ri1, en0, wm0.T, null, false, 24);
        this.G = bArr;
        byte[] copyOf = Arrays.copyOf(this.G, 16);
        ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
        this.F = copyOf;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        Boolean bool = this.D;
        return Boolean.valueOf(bool != null ? bool.booleanValue() : false);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        if (this.G.length < 16) {
            a(is0.INVALID_PARAMETER);
            return;
        }
        new SecureRandom().nextBytes(this.E);
        zk0.a(this, new fp0(((zk0) this).w, b21.x.b(), this.E), new ss0(this), new nu0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.r2, Long.valueOf(ik1.a.a(this.G, ng1.CRC32)));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(super.k(), r51.z3, this.D);
    }

    @DexIgnore
    public final void a(byte[] bArr) {
        is0 is0;
        if (bArr.length != 16) {
            is0 = is0.INVALID_DATA_LENGTH;
        } else {
            byte[] a = f31.f.a(b21.x.b(), fq1.a.a(um1.K.b(), this.F, um1.K.a(), bArr));
            if (a.length != 16) {
                is0 = is0.INVALID_DATA_LENGTH;
            } else {
                if (Arrays.equals(this.E, s97.a(a, 8, 16))) {
                    this.D = true;
                }
                is0 = is0.SUCCESS;
            }
        }
        a(eu0.a(((zk0) this).v, null, is0, null, 5));
    }
}
