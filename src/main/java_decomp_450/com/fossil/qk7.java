package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qk7 {
    @DexIgnore
    public static /* final */ lm7 a; // = new lm7("COMPLETING_ALREADY");
    @DexIgnore
    public static /* final */ lm7 b; // = new lm7("COMPLETING_WAITING_CHILDREN");
    @DexIgnore
    public static /* final */ lm7 c; // = new lm7("COMPLETING_RETRY");
    @DexIgnore
    public static /* final */ lm7 d; // = new lm7("TOO_LATE_TO_CANCEL");
    @DexIgnore
    public static /* final */ lm7 e; // = new lm7("SEALED");
    @DexIgnore
    public static /* final */ uj7 f; // = new uj7(false);
    @DexIgnore
    public static /* final */ uj7 g; // = new uj7(true);

    @DexIgnore
    public static final Object a(Object obj) {
        return obj instanceof dk7 ? new ek7((dk7) obj) : obj;
    }

    @DexIgnore
    public static final Object b(Object obj) {
        dk7 dk7;
        ek7 ek7 = (ek7) (!(obj instanceof ek7) ? null : obj);
        return (ek7 == null || (dk7 = ek7.a) == null) ? obj : dk7;
    }
}
