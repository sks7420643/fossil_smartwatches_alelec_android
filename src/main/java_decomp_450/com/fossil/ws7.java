package com.fossil;

import com.facebook.LegacyTokenHelper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ws7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ObjectInputStream {
        @DexIgnore
        public static /* final */ Map<String, Class<?>> b;
        @DexIgnore
        public /* final */ ClassLoader a;

        /*
        static {
            HashMap hashMap = new HashMap();
            b = hashMap;
            hashMap.put(LegacyTokenHelper.TYPE_BYTE, Byte.TYPE);
            b.put(LegacyTokenHelper.TYPE_SHORT, Short.TYPE);
            b.put(LegacyTokenHelper.TYPE_INTEGER, Integer.TYPE);
            b.put(LegacyTokenHelper.TYPE_LONG, Long.TYPE);
            b.put(LegacyTokenHelper.TYPE_FLOAT, Float.TYPE);
            b.put(LegacyTokenHelper.TYPE_DOUBLE, Double.TYPE);
            b.put("boolean", Boolean.TYPE);
            b.put(LegacyTokenHelper.TYPE_CHAR, Character.TYPE);
            b.put("void", Void.TYPE);
        }
        */

        @DexIgnore
        public a(InputStream inputStream, ClassLoader classLoader) throws IOException {
            super(inputStream);
            this.a = classLoader;
        }

        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:1:0x0005 */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: java.lang.String */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v1, types: [java.lang.String] */
        /* JADX WARN: Type inference failed for: r3v7, types: [java.lang.Class<?>, java.lang.Class] */
        /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
            return r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
            return java.lang.Class.forName(r3, false, java.lang.Thread.currentThread().getContextClassLoader());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
            r3 = com.fossil.ws7.a.b.get(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
            if (r3 != null) goto L_0x0024;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x000c */
        @Override // java.io.ObjectInputStream
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Class<?> resolveClass(java.io.ObjectStreamClass r3) throws java.io.IOException, java.lang.ClassNotFoundException {
            /*
                r2 = this;
                java.lang.String r3 = r3.getName()
                r0 = 0
                java.lang.ClassLoader r1 = r2.a     // Catch:{ ClassNotFoundException -> 0x000c }
                java.lang.Class r3 = java.lang.Class.forName(r3, r0, r1)     // Catch:{ ClassNotFoundException -> 0x000c }
                return r3
            L_0x000c:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ ClassNotFoundException -> 0x0019 }
                java.lang.ClassLoader r1 = r1.getContextClassLoader()     // Catch:{ ClassNotFoundException -> 0x0019 }
                java.lang.Class r3 = java.lang.Class.forName(r3, r0, r1)     // Catch:{ ClassNotFoundException -> 0x0019 }
                return r3
            L_0x0019:
                r0 = move-exception
                java.util.Map<java.lang.String, java.lang.Class<?>> r1 = com.fossil.ws7.a.b
                java.lang.Object r3 = r1.get(r3)
                java.lang.Class r3 = (java.lang.Class) r3
                if (r3 == 0) goto L_0x0025
                return r3
            L_0x0025:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ws7.a.resolveClass(java.io.ObjectStreamClass):java.lang.Class");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c A[SYNTHETIC, Splitter:B:29:0x004c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T extends java.io.Serializable> T a(T r4) {
        /*
            java.lang.String r0 = "IOException on closing cloned object data InputStream."
            r1 = 0
            if (r4 != 0) goto L_0x0006
            return r1
        L_0x0006:
            byte[] r2 = b(r4)
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream
            r3.<init>(r2)
            com.fossil.ws7$a r2 = new com.fossil.ws7$a     // Catch:{ ClassNotFoundException -> 0x0041, IOException -> 0x0038 }
            java.lang.Class r4 = r4.getClass()     // Catch:{ ClassNotFoundException -> 0x0041, IOException -> 0x0038 }
            java.lang.ClassLoader r4 = r4.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x0041, IOException -> 0x0038 }
            r2.<init>(r3, r4)     // Catch:{ ClassNotFoundException -> 0x0041, IOException -> 0x0038 }
            java.lang.Object r4 = r2.readObject()     // Catch:{ ClassNotFoundException -> 0x0033, IOException -> 0x0030, all -> 0x002d }
            java.io.Serializable r4 = (java.io.Serializable) r4     // Catch:{ ClassNotFoundException -> 0x0033, IOException -> 0x0030, all -> 0x002d }
            r2.close()     // Catch:{ IOException -> 0x0026 }
            return r4
        L_0x0026:
            r4 = move-exception
            com.fossil.vs7 r1 = new com.fossil.vs7
            r1.<init>(r0, r4)
            throw r1
        L_0x002d:
            r4 = move-exception
            r1 = r2
            goto L_0x004a
        L_0x0030:
            r4 = move-exception
            r1 = r2
            goto L_0x0039
        L_0x0033:
            r4 = move-exception
            r1 = r2
            goto L_0x0042
        L_0x0036:
            r4 = move-exception
            goto L_0x004a
        L_0x0038:
            r4 = move-exception
        L_0x0039:
            com.fossil.vs7 r2 = new com.fossil.vs7     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "IOException while reading cloned object data"
            r2.<init>(r3, r4)     // Catch:{ all -> 0x0036 }
            throw r2     // Catch:{ all -> 0x0036 }
        L_0x0041:
            r4 = move-exception
        L_0x0042:
            com.fossil.vs7 r2 = new com.fossil.vs7     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "ClassNotFoundException while reading cloned object data"
            r2.<init>(r3, r4)     // Catch:{ all -> 0x0036 }
            throw r2     // Catch:{ all -> 0x0036 }
        L_0x004a:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x0057
        L_0x0050:
            r4 = move-exception
            com.fossil.vs7 r1 = new com.fossil.vs7
            r1.<init>(r0, r4)
            throw r1
        L_0x0057:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ws7.a(java.io.Serializable):java.io.Serializable");
    }

    @DexIgnore
    public static byte[] b(Serializable serializable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        a(serializable, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0020 A[SYNTHETIC, Splitter:B:19:0x0020] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.io.Serializable r2, java.io.OutputStream r3) {
        /*
            if (r3 == 0) goto L_0x0024
            r0 = 0
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0017 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0017 }
            r1.writeObject(r2)     // Catch:{ IOException -> 0x0012, all -> 0x000f }
            r1.close()     // Catch:{ IOException -> 0x000e }
        L_0x000e:
            return
        L_0x000f:
            r2 = move-exception
            r0 = r1
            goto L_0x001e
        L_0x0012:
            r2 = move-exception
            r0 = r1
            goto L_0x0018
        L_0x0015:
            r2 = move-exception
            goto L_0x001e
        L_0x0017:
            r2 = move-exception
        L_0x0018:
            com.fossil.vs7 r3 = new com.fossil.vs7     // Catch:{ all -> 0x0015 }
            r3.<init>(r2)     // Catch:{ all -> 0x0015 }
            throw r3     // Catch:{ all -> 0x0015 }
        L_0x001e:
            if (r0 == 0) goto L_0x0023
            r0.close()     // Catch:{ IOException -> 0x0023 }
        L_0x0023:
            throw r2
        L_0x0024:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "The OutputStream must not be null"
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ws7.a(java.io.Serializable, java.io.OutputStream):void");
    }
}
