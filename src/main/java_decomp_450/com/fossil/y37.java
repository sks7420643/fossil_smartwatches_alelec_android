package com.fossil;

import android.content.Context;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y37 {
    @DexIgnore
    public static void a(Context context) {
        z37.b(context, null);
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties) {
        z37.a(context, str, properties, (a47) null);
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2) {
        return z37.a(context, str, str2, (a47) null);
    }

    @DexIgnore
    public static void b(Context context) {
        z37.c(context, null);
    }
}
