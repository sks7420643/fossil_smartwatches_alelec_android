package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public int a;
    @DexIgnore
    @te4("id")
    public int b;
    @DexIgnore
    @te4("encryptMethod")
    public int c;
    @DexIgnore
    @te4("encryptedData")
    public String d;
    @DexIgnore
    @te4("keyType")
    public int e;
    @DexIgnore
    @te4("sequence")
    public String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<in4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public in4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new in4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public in4[] newArray(int i) {
            return new in4[i];
        }
    }

    @DexIgnore
    public in4(int i, int i2, String str, int i3, String str2) {
        ee7.b(str, "encryptedData");
        ee7.b(str2, "sequence");
        this.b = i;
        this.c = i2;
        this.d = str;
        this.e = i3;
        this.f = str2;
        this.a = 1;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof in4)) {
            return false;
        }
        in4 in4 = (in4) obj;
        return this.b == in4.b && this.c == in4.c && ee7.a(this.d, in4.d) && this.e == in4.e && ee7.a(this.f, in4.f);
    }

    @DexIgnore
    public final String f() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = ((this.b * 31) + this.c) * 31;
        String str = this.d;
        int i2 = 0;
        int hashCode = (((i + (str != null ? str.hashCode() : 0)) * 31) + this.e) * 31;
        String str2 = this.f;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    @DexIgnore
    public String toString() {
        return "BCFitnessData(id=" + this.b + ", encryptMethod=" + this.c + ", encryptedData=" + this.d + ", keyType=" + this.e + ", sequence=" + this.f + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
    }

    @DexIgnore
    public final void a(int i) {
        this.a = i;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public in4(android.os.Parcel r8) {
        /*
            r7 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r8, r0)
            int r2 = r8.readInt()
            int r3 = r8.readInt()
            java.lang.String r4 = r8.readString()
            r0 = 0
            if (r4 == 0) goto L_0x002f
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.ee7.a(r4, r1)
            int r5 = r8.readInt()
            java.lang.String r6 = r8.readString()
            if (r6 == 0) goto L_0x002b
            com.fossil.ee7.a(r6, r1)
            r1 = r7
            r1.<init>(r2, r3, r4, r5, r6)
            return
        L_0x002b:
            com.fossil.ee7.a()
            throw r0
        L_0x002f:
            com.fossil.ee7.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.in4.<init>(android.os.Parcel):void");
    }
}
