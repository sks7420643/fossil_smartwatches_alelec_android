package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.qn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr extends rr<Uri> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sr(qn7.a aVar) {
        super(aVar);
        ee7.b(aVar, "callFactory");
    }

    @DexIgnore
    public boolean a(Uri uri) {
        ee7.b(uri, "data");
        return ee7.a(uri.getScheme(), "http") || ee7.a(uri.getScheme(), Utility.URL_SCHEME);
    }

    @DexIgnore
    public String b(Uri uri) {
        ee7.b(uri, "data");
        String uri2 = uri.toString();
        ee7.a((Object) uri2, "data.toString()");
        return uri2;
    }

    @DexIgnore
    public go7 c(Uri uri) {
        ee7.b(uri, "$this$toHttpUrl");
        go7 d = go7.d(uri.toString());
        ee7.a((Object) d, "HttpUrl.get(toString())");
        return d;
    }
}
