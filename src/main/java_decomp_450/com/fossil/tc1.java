package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc1 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ za1 CREATOR; // = new za1(null);
    @DexIgnore
    public /* final */ jg0[] a;
    @DexIgnore
    public /* final */ r60 b;
    @DexIgnore
    public /* final */ List<byte[]> c; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> d; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> e; // = new ArrayList();

    @DexIgnore
    public tc1(jg0[] jg0Arr, r60 r60) {
        this.a = jg0Arr;
        this.b = r60;
        for (jg0 jg0 : jg0Arr) {
            ArrayList<ne1> arrayList = new ArrayList();
            mg0[] microAppDeclarations = jg0.getMicroAppDeclarations();
            for (mg0 mg0 : microAppDeclarations) {
                o31 o31 = new o31(mg0.b(), mg0.getMicroAppVersion(), mg0.getVariationNumber());
                arrayList.add(new ne1(o31));
                this.c.add(mg0.getData());
                lg0 customization = mg0.getCustomization();
                if (customization != null) {
                    customization.a(o31);
                    this.d.add(customization.b());
                }
            }
            ng0 microAppButton = jg0.getMicroAppButton();
            byte[] bArr = new byte[0];
            for (ne1 ne1 : arrayList) {
                bArr = yz0.a(bArr, ne1.a);
            }
            ByteBuffer order = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN);
            ee7.a((Object) order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
            order.put(microAppButton.a());
            order.put((byte) arrayList.size());
            order.put(bArr);
            byte[] array = order.array();
            ee7.a((Object) array, "byteBuffer.array()");
            this.e.add(array);
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (jg0 jg0 : this.a) {
            jSONArray.put(jg0.a());
        }
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.X3, this.b.a()), r51.Y3, jSONArray), r51.Z3, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(tc1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            tc1 tc1 = (tc1) obj;
            return !(ee7.a(this.b, tc1.b) ^ true) && Arrays.equals(this.a, tc1.a);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.configuration.MicroAppConfiguration");
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedArray(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }

    @DexIgnore
    public final byte[] a(List<byte[]> list) {
        byte[] bArr = new byte[0];
        for (byte[] bArr2 : list) {
            bArr = yz0.a(bArr, bArr2);
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 1).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) list.size());
        order.put(bArr);
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }
}
