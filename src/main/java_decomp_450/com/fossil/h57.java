package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h57 {
    @DexIgnore
    public long a;
    @DexIgnore
    public String b;

    @DexIgnore
    public h57(long j, String str, int i, int i2) {
        this.a = j;
        this.b = str;
    }

    @DexIgnore
    public String toString() {
        return this.b;
    }
}
