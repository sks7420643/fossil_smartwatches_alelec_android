package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l10 implements ex<Bitmap> {
    @DexIgnore
    public abstract Bitmap a(dz dzVar, Bitmap bitmap, int i, int i2);

    @DexIgnore
    @Override // com.fossil.ex
    public final uy<Bitmap> a(Context context, uy<Bitmap> uyVar, int i, int i2) {
        if (v50.b(i, i2)) {
            dz c = aw.a(context).c();
            Bitmap bitmap = uyVar.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap a = a(c, bitmap, i, i2);
            return bitmap.equals(a) ? uyVar : k10.a(a, c);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
