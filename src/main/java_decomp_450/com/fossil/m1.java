package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.v1;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m1 extends t1 implements v1, View.OnKeyListener, PopupWindow.OnDismissListener {
    @DexIgnore
    public static /* final */ int G; // = e0.abc_cascading_menu_item_layout;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public v1.a C;
    @DexIgnore
    public ViewTreeObserver D;
    @DexIgnore
    public PopupWindow.OnDismissListener E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ Handler g;
    @DexIgnore
    public /* final */ List<p1> h; // = new ArrayList();
    @DexIgnore
    public /* final */ List<d> i; // = new ArrayList();
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener j; // = new a();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener p; // = new b();
    @DexIgnore
    public /* final */ u2 q; // = new c();
    @DexIgnore
    public int r; // = 0;
    @DexIgnore
    public int s; // = 0;
    @DexIgnore
    public View t;
    @DexIgnore
    public View u;
    @DexIgnore
    public int v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (m1.this.a() && m1.this.i.size() > 0 && !m1.this.i.get(0).a.l()) {
                View view = m1.this.u;
                if (view == null || !view.isShown()) {
                    m1.this.dismiss();
                    return;
                }
                for (d dVar : m1.this.i) {
                    dVar.a.show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = m1.this.D;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    m1.this.D = view.getViewTreeObserver();
                }
                m1 m1Var = m1.this;
                m1Var.D.removeGlobalOnLayoutListener(m1Var.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements u2 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ d a;
            @DexIgnore
            public /* final */ /* synthetic */ MenuItem b;
            @DexIgnore
            public /* final */ /* synthetic */ p1 c;

            @DexIgnore
            public a(d dVar, MenuItem menuItem, p1 p1Var) {
                this.a = dVar;
                this.b = menuItem;
                this.c = p1Var;
            }

            @DexIgnore
            public void run() {
                d dVar = this.a;
                if (dVar != null) {
                    m1.this.F = true;
                    dVar.b.a(false);
                    m1.this.F = false;
                }
                if (this.b.isEnabled() && this.b.hasSubMenu()) {
                    this.c.a(this.b, 4);
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.u2
        public void a(p1 p1Var, MenuItem menuItem) {
            d dVar = null;
            m1.this.g.removeCallbacksAndMessages(null);
            int size = m1.this.i.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (p1Var == m1.this.i.get(i).b) {
                    break;
                } else {
                    i++;
                }
            }
            if (i != -1) {
                int i2 = i + 1;
                if (i2 < m1.this.i.size()) {
                    dVar = m1.this.i.get(i2);
                }
                m1.this.g.postAtTime(new a(dVar, menuItem, p1Var), p1Var, SystemClock.uptimeMillis() + 200);
            }
        }

        @DexIgnore
        @Override // com.fossil.u2
        public void b(p1 p1Var, MenuItem menuItem) {
            m1.this.g.removeCallbacksAndMessages(p1Var);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ v2 a;
        @DexIgnore
        public /* final */ p1 b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(v2 v2Var, p1 p1Var, int i) {
            this.a = v2Var;
            this.b = p1Var;
            this.c = i;
        }

        @DexIgnore
        public ListView a() {
            return this.a.e();
        }
    }

    @DexIgnore
    public m1(Context context, View view, int i2, int i3, boolean z2) {
        this.b = context;
        this.t = view;
        this.d = i2;
        this.e = i3;
        this.f = z2;
        this.A = false;
        this.v = h();
        Resources resources = context.getResources();
        this.c = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(b0.abc_config_prefDialogWidth));
        this.g = new Handler();
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Parcelable parcelable) {
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(p1 p1Var) {
        p1Var.a(this, this.b);
        if (a()) {
            d(p1Var);
        } else {
            this.h.add(p1Var);
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void b(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b() {
        return false;
    }

    @DexIgnore
    public final int c(p1 p1Var) {
        int size = this.i.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (p1Var == this.i.get(i2).b) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public Parcelable c() {
        return null;
    }

    @DexIgnore
    public final int d(int i2) {
        List<d> list = this.i;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.u.getWindowVisibleDisplayFrame(rect);
        if (this.v == 1) {
            if (iArr[0] + a2.getWidth() + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.y1
    public void dismiss() {
        int size = this.i.size();
        if (size > 0) {
            d[] dVarArr = (d[]) this.i.toArray(new d[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                d dVar = dVarArr[i2];
                if (dVar.a.a()) {
                    dVar.a.dismiss();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.y1
    public ListView e() {
        if (this.i.isEmpty()) {
            return null;
        }
        List<d> list = this.i;
        return list.get(list.size() - 1).a();
    }

    @DexIgnore
    public final v2 g() {
        v2 v2Var = new v2(this.b, null, this.d, this.e);
        v2Var.a(this.q);
        v2Var.a((AdapterView.OnItemClickListener) this);
        v2Var.a((PopupWindow.OnDismissListener) this);
        v2Var.a(this.t);
        v2Var.f(this.s);
        v2Var.a(true);
        v2Var.g(2);
        return v2Var;
    }

    @DexIgnore
    public final int h() {
        return da.p(this.t) == 1 ? 0 : 1;
    }

    @DexIgnore
    public void onDismiss() {
        d dVar;
        int size = this.i.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                dVar = null;
                break;
            }
            dVar = this.i.get(i2);
            if (!dVar.a.a()) {
                break;
            }
            i2++;
        }
        if (dVar != null) {
            dVar.b.a(false);
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.y1
    public void show() {
        if (!a()) {
            for (p1 p1Var : this.h) {
                d(p1Var);
            }
            this.h.clear();
            View view = this.t;
            this.u = view;
            if (view != null) {
                boolean z2 = this.D == null;
                ViewTreeObserver viewTreeObserver = this.u.getViewTreeObserver();
                this.D = viewTreeObserver;
                if (z2) {
                    viewTreeObserver.addOnGlobalLayoutListener(this.j);
                }
                this.u.addOnAttachStateChangeListener(this.p);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void b(int i2) {
        this.w = true;
        this.y = i2;
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void c(int i2) {
        this.x = true;
        this.z = i2;
    }

    @DexIgnore
    public final MenuItem a(p1 p1Var, p1 p1Var2) {
        int size = p1Var.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = p1Var.getItem(i2);
            if (item.hasSubMenu() && p1Var2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void c(boolean z2) {
        this.B = z2;
    }

    @DexIgnore
    public final View a(d dVar, p1 p1Var) {
        int i2;
        o1 o1Var;
        int firstVisiblePosition;
        MenuItem a2 = a(dVar.b, p1Var);
        if (a2 == null) {
            return null;
        }
        ListView a3 = dVar.a();
        ListAdapter adapter = a3.getAdapter();
        int i3 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            o1Var = (o1) headerViewListAdapter.getWrappedAdapter();
        } else {
            o1Var = (o1) adapter;
            i2 = 0;
        }
        int count = o1Var.getCount();
        while (true) {
            if (i3 >= count) {
                i3 = -1;
                break;
            } else if (a2 == o1Var.getItem(i3)) {
                break;
            } else {
                i3++;
            }
        }
        if (i3 != -1 && (firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < a3.getChildCount()) {
            return a3.getChildAt(firstVisiblePosition);
        }
        return null;
    }

    @DexIgnore
    public final void d(p1 p1Var) {
        View view;
        d dVar;
        int i2;
        int i3;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.b);
        o1 o1Var = new o1(p1Var, from, this.f, G);
        if (!a() && this.A) {
            o1Var.a(true);
        } else if (a()) {
            o1Var.a(t1.b(p1Var));
        }
        int a2 = t1.a(o1Var, null, this.b, this.c);
        v2 g2 = g();
        g2.a((ListAdapter) o1Var);
        g2.e(a2);
        g2.f(this.s);
        if (this.i.size() > 0) {
            List<d> list = this.i;
            dVar = list.get(list.size() - 1);
            view = a(dVar, p1Var);
        } else {
            dVar = null;
            view = null;
        }
        if (view != null) {
            g2.d(false);
            g2.a((Object) null);
            int d2 = d(a2);
            boolean z2 = d2 == 1;
            this.v = d2;
            if (Build.VERSION.SDK_INT >= 26) {
                g2.a(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.t.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.s & 7) == 5) {
                    iArr[0] = iArr[0] + this.t.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.s & 5) != 5) {
                if (z2) {
                    a2 = view.getWidth();
                }
                i4 = i2 - a2;
                g2.a(i4);
                g2.b(true);
                g2.b(i3);
            } else if (!z2) {
                a2 = view.getWidth();
                i4 = i2 - a2;
                g2.a(i4);
                g2.b(true);
                g2.b(i3);
            }
            i4 = i2 + a2;
            g2.a(i4);
            g2.b(true);
            g2.b(i3);
        } else {
            if (this.w) {
                g2.a(this.y);
            }
            if (this.x) {
                g2.b(this.z);
            }
            g2.a(f());
        }
        this.i.add(new d(g2, p1Var, this.v));
        g2.show();
        ListView e2 = g2.e();
        e2.setOnKeyListener(this);
        if (dVar == null && this.B && p1Var.h() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(e0.abc_popup_menu_header_item_layout, (ViewGroup) e2, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(p1Var.h());
            e2.addHeaderView(frameLayout, null, false);
            g2.show();
        }
    }

    @DexIgnore
    @Override // com.fossil.y1
    public boolean a() {
        return this.i.size() > 0 && this.i.get(0).a.a();
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(boolean z2) {
        for (d dVar : this.i) {
            t1.a(dVar.a().getAdapter()).notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(v1.a aVar) {
        this.C = aVar;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(a2 a2Var) {
        for (d dVar : this.i) {
            if (a2Var == dVar.b) {
                dVar.a().requestFocus();
                return true;
            }
        }
        if (!a2Var.hasVisibleItems()) {
            return false;
        }
        a((p1) a2Var);
        v1.a aVar = this.C;
        if (aVar != null) {
            aVar.a(a2Var);
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(p1 p1Var, boolean z2) {
        int c2 = c(p1Var);
        if (c2 >= 0) {
            int i2 = c2 + 1;
            if (i2 < this.i.size()) {
                this.i.get(i2).b.a(false);
            }
            d remove = this.i.remove(c2);
            remove.b.b(this);
            if (this.F) {
                remove.a.b((Object) null);
                remove.a.d(0);
            }
            remove.a.dismiss();
            int size = this.i.size();
            if (size > 0) {
                this.v = this.i.get(size - 1).c;
            } else {
                this.v = h();
            }
            if (size == 0) {
                dismiss();
                v1.a aVar = this.C;
                if (aVar != null) {
                    aVar.a(p1Var, true);
                }
                ViewTreeObserver viewTreeObserver = this.D;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.D.removeGlobalOnLayoutListener(this.j);
                    }
                    this.D = null;
                }
                this.u.removeOnAttachStateChangeListener(this.p);
                this.E.onDismiss();
            } else if (z2) {
                this.i.get(0).b.a(false);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(int i2) {
        if (this.r != i2) {
            this.r = i2;
            this.s = l9.a(i2, da.p(this.t));
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(View view) {
        if (this.t != view) {
            this.t = view;
            this.s = l9.a(this.r, da.p(view));
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.E = onDismissListener;
    }
}
