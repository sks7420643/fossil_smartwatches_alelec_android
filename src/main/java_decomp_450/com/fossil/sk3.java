package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ wm3 c;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 d;
    @DexIgnore
    public /* final */ /* synthetic */ wm3 e;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 f;

    @DexIgnore
    public sk3(ek3 ek3, boolean z, boolean z2, wm3 wm3, nm3 nm3, wm3 wm32) {
        this.f = ek3;
        this.a = z;
        this.b = z2;
        this.c = wm3;
        this.d = nm3;
        this.e = wm32;
    }

    @DexIgnore
    public final void run() {
        bg3 d2 = this.f.d;
        if (d2 == null) {
            this.f.e().t().a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.a) {
            this.f.a(d2, this.b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e.a)) {
                    d2.a(this.c, this.d);
                } else {
                    d2.a(this.c);
                }
            } catch (RemoteException e2) {
                this.f.e().t().a("Failed to send conditional user property to the service", e2);
            }
        }
        this.f.J();
    }
}
