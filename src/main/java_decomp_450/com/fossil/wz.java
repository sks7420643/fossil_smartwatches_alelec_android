package com.fossil;

import com.fossil.w50;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wz {
    @DexIgnore
    public /* final */ r50<yw, String> a; // = new r50<>(1000);
    @DexIgnore
    public /* final */ b9<b> b; // = w50.a(10, new a(this));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements w50.d<b> {
        @DexIgnore
        public a(wz wzVar) {
        }

        @DexIgnore
        @Override // com.fossil.w50.d
        public b create() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements w50.f {
        @DexIgnore
        public /* final */ MessageDigest a;
        @DexIgnore
        public /* final */ y50 b; // = y50.b();

        @DexIgnore
        public b(MessageDigest messageDigest) {
            this.a = messageDigest;
        }

        @DexIgnore
        @Override // com.fossil.w50.f
        public y50 a() {
            return this.b;
        }
    }

    @DexIgnore
    public final String a(yw ywVar) {
        b a2 = this.b.a();
        u50.a(a2);
        b bVar = a2;
        try {
            ywVar.a(bVar.a);
            return v50.a(bVar.a.digest());
        } finally {
            this.b.a(bVar);
        }
    }

    @DexIgnore
    public String b(yw ywVar) {
        String a2;
        synchronized (this.a) {
            a2 = this.a.a(ywVar);
        }
        if (a2 == null) {
            a2 = a(ywVar);
        }
        synchronized (this.a) {
            this.a.b(ywVar, a2);
        }
        return a2;
    }
}
