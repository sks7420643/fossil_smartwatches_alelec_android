package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq7 extends IOException {
    @DexIgnore
    public /* final */ tp7 errorCode;

    @DexIgnore
    public fq7(tp7 tp7) {
        super("stream was reset: " + tp7);
        this.errorCode = tp7;
    }
}
