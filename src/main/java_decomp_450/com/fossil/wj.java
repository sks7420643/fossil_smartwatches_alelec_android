package com.fossil;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.util.Property;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wj<T> extends Property<T, Float> {
    @DexIgnore
    public /* final */ Property<T, PointF> a;
    @DexIgnore
    public /* final */ PathMeasure b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float[] d; // = new float[2];
    @DexIgnore
    public /* final */ PointF e; // = new PointF();
    @DexIgnore
    public float f;

    @DexIgnore
    public wj(Property<T, PointF> property, Path path) {
        super(Float.class, property.getName());
        this.a = property;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        this.b = pathMeasure;
        this.c = pathMeasure.getLength();
    }

    @DexIgnore
    /* renamed from: a */
    public void set(T t, Float f2) {
        this.f = f2.floatValue();
        this.b.getPosTan(this.c * f2.floatValue(), this.d, null);
        PointF pointF = this.e;
        float[] fArr = this.d;
        pointF.x = fArr[0];
        pointF.y = fArr[1];
        this.a.set(t, pointF);
    }

    @DexIgnore
    @Override // android.util.Property
    public Float get(T t) {
        return Float.valueOf(this.f);
    }
}
