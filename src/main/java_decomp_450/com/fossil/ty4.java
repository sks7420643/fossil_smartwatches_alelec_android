package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ty4 extends sy4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362049, 1);
        E.put(2131362981, 2);
        E.put(2131362982, 3);
        E.put(2131363323, 4);
        E.put(2131362046, 5);
        E.put(2131362657, 6);
        E.put(2131363234, 7);
        E.put(2131363114, 8);
        E.put(2131363308, 9);
        E.put(2131363233, 10);
        E.put(2131363231, 11);
    }
    */

    @DexIgnore
    public ty4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public ty4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[1], (ImageView) objArr[6], (ScrollView) objArr[0], (RecyclerView) objArr[2], (RecyclerView) objArr[3], (FlexibleSwitchCompat) objArr[8], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[4]);
        this.C = -1;
        ((sy4) this).t.setTag(null);
        a(view);
        f();
    }
}
