package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju1 {
    @DexIgnore
    public /* final */ bt1 a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public ju1(bt1 bt1, byte[] bArr) {
        if (bt1 == null) {
            throw new NullPointerException("encoding is null");
        } else if (bArr != null) {
            this.a = bt1;
            this.b = bArr;
        } else {
            throw new NullPointerException("bytes is null");
        }
    }

    @DexIgnore
    public byte[] a() {
        return this.b;
    }

    @DexIgnore
    public bt1 b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ju1)) {
            return false;
        }
        ju1 ju1 = (ju1) obj;
        if (!this.a.equals(ju1.a)) {
            return false;
        }
        return Arrays.equals(this.b, ju1.b);
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "EncodedPayload{encoding=" + this.a + ", bytes=[...]}";
    }
}
