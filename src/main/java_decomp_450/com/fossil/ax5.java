package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cp5;
import com.fossil.cy6;
import com.fossil.vw5;
import com.fossil.wearables.fsl.contact.Contact;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax5 extends ho5 implements zw5, cy6.g {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public yw5 g;
    @DexIgnore
    public qw6<u35> h;
    @DexIgnore
    public cp5 i;
    @DexIgnore
    public vw5 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ax5.q;
        }

        @DexIgnore
        public final ax5 b() {
            return new ax5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements vw5.b {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        @Override // com.fossil.vw5.b
        public void a(int i, boolean z, boolean z2) {
            ax5.b(this.a).a(i, z, z2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        public d(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        public e(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.g1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        public f(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ax5.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        public g(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ax5.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        public h(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ax5.b(this.a).l();
        }
    }

    /*
    static {
        String simpleName = ax5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationContactsAndA\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yw5 b(ax5 ax5) {
        yw5 yw5 = ax5.g;
        if (yw5 != null) {
            return yw5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void D(boolean z) {
        ImageView imageView;
        if (isActive()) {
            qw6<u35> qw6 = this.h;
            if (qw6 != null) {
                u35 a2 = qw6.a();
                if (a2 != null && (imageView = a2.y) != null) {
                    ee7.a((Object) imageView, "doneButton");
                    imageView.setEnabled(z);
                    imageView.setClickable(z);
                    if (z) {
                        imageView.setBackground(v6.c(requireContext(), 2131099971));
                    } else {
                        imageView.setBackground(v6.c(requireContext(), 2131099820));
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void c(int i2, ArrayList<bt5> arrayList) {
        ee7.b(arrayList, "contactWrappersSelected");
        NotificationHybridContactActivity.z.a(this, i2, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        f1();
        return true;
    }

    @DexIgnore
    public void f1() {
        qw6<u35> qw6 = this.h;
        if (qw6 == null) {
            ee7.d("mBinding");
            throw null;
        } else if (qw6.a() != null) {
            yw5 yw5 = this.g;
            if (yw5 == null) {
                ee7.d("mPresenter");
                throw null;
            } else if (!yw5.i()) {
                close();
            } else {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.b(childFragmentManager);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void g(List<Object> list) {
        ee7.b(list, "contactAndAppData");
        cp5 cp5 = this.i;
        if (cp5 != null) {
            cp5.a(list);
            if (!isActive()) {
                return;
            }
            if (list.isEmpty()) {
                qw6<u35> qw6 = this.h;
                if (qw6 != null) {
                    u35 a2 = qw6.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.r;
                        ee7.a((Object) flexibleTextView, "it.ftvAssignSection");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.D;
                        ee7.a((Object) recyclerView, "it.rvAssign");
                        recyclerView.setVisibility(8);
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            qw6<u35> qw62 = this.h;
            if (qw62 != null) {
                u35 a3 = qw62.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.r;
                    ee7.a((Object) flexibleTextView2, "it.ftvAssignSection");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a3.D;
                    ee7.a((Object) recyclerView2, "it.rvAssign");
                    recyclerView2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        cp5 cp5 = this.i;
        if (cp5 == null) {
            ee7.d("mAdapter");
            throw null;
        } else if (cp5.c()) {
            yw5 yw5 = this.g;
            if (yw5 != null) {
                yw5.m();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void j() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void k(int i2) {
        FlexibleTextView flexibleTextView;
        qw6<u35> qw6 = this.h;
        if (qw6 != null) {
            u35 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.w) != null) {
                we7 we7 = we7.a;
                String a3 = ig5.a(getContext(), 2131886171);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026ct_Title__AssignToNumber)");
                String format = String.format(a3, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void l() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 != 4567) {
            if (i2 != 5678) {
                if (i2 == 6789 && i3 == -1 && intent != null) {
                    Serializable serializableExtra = intent.getSerializableExtra("CONTACT_DATA");
                    if (serializableExtra != null) {
                        ArrayList<bt5> arrayList = (ArrayList) serializableExtra;
                        yw5 yw5 = this.g;
                        if (yw5 != null) {
                            yw5.b(arrayList);
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                    }
                }
            } else if (i3 == -1 && intent != null) {
                Serializable serializableExtra2 = intent.getSerializableExtra("CONTACT_DATA");
                if (serializableExtra2 != null) {
                    ArrayList<bt5> arrayList2 = (ArrayList) serializableExtra2;
                    yw5 yw52 = this.g;
                    if (yw52 != null) {
                        yw52.a(arrayList2);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                }
            }
        } else if (i3 == -1 && intent != null) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("APP_DATA");
            yw5 yw53 = this.g;
            if (yw53 != null) {
                ee7.a((Object) stringArrayListExtra, "stringAppsSelected");
                yw53.c(stringArrayListExtra);
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        u35 u35 = (u35) qb.a(layoutInflater, 2131558590, viewGroup, false, a1());
        vw5 vw5 = (vw5) getChildFragmentManager().b(vw5.y.a());
        this.j = vw5;
        if (vw5 == null) {
            this.j = vw5.y.b();
        }
        vw5 vw52 = this.j;
        if (vw52 != null) {
            vw52.a(new c(this));
        }
        FlexibleTextView flexibleTextView = u35.r;
        ee7.a((Object) flexibleTextView, "binding.ftvAssignSection");
        flexibleTextView.setVisibility(8);
        RecyclerView recyclerView = u35.D;
        ee7.a((Object) recyclerView, "binding.rvAssign");
        recyclerView.setVisibility(8);
        ImageView imageView = u35.y;
        ee7.a((Object) imageView, "binding.ivDone");
        imageView.setBackground(v6.c(requireContext(), 2131230886));
        u35.x.setOnClickListener(new d(this));
        u35.y.setOnClickListener(new e(this));
        u35.B.setOnClickListener(new f(this));
        u35.A.setOnClickListener(new g(this));
        u35.z.setOnClickListener(new h(this));
        cp5 cp5 = new cp5();
        cp5.a(new b(this));
        this.i = cp5;
        RecyclerView recyclerView2 = u35.D;
        recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
        recyclerView2.setHasFixedSize(true);
        cp5 cp52 = this.i;
        if (cp52 != null) {
            recyclerView2.setAdapter(cp52);
            String b2 = eh5.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                int parseColor = Color.parseColor(b2);
                u35.E.setBackgroundColor(parseColor);
                u35.F.setBackgroundColor(parseColor);
                u35.G.setBackgroundColor(parseColor);
            }
            this.h = new qw6<>(this, u35);
            ee7.a((Object) u35, "binding");
            return u35.d();
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        yw5 yw5 = this.g;
        if (yw5 != null) {
            yw5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yw5 yw5 = this.g;
        if (yw5 != null) {
            yw5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void b(int i2, ArrayList<String> arrayList) {
        ee7.b(arrayList, "stringAppsSelected");
        NotificationHybridAppActivity.z.a(this, i2, arrayList);
    }

    @DexIgnore
    public void a(yw5 yw5) {
        ee7.b(yw5, "presenter");
        this.g = yw5;
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void a(int i2, ArrayList<bt5> arrayList) {
        ee7.b(arrayList, "contactWrappersSelected");
        NotificationHybridEveryoneActivity.z.a(this, i2, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            return;
        }
        if (i2 == 2131363229) {
            close();
        } else if (i2 == 2131363307) {
            g1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements cp5.c {
        @DexIgnore
        public /* final */ /* synthetic */ ax5 a;

        @DexIgnore
        public b(ax5 ax5) {
            this.a = ax5;
        }

        @DexIgnore
        @Override // com.fossil.cp5.c
        public void a(bt5 bt5) {
            ee7.b(bt5, "contactWrapper");
            vw5 a2 = this.a.j;
            if (a2 != null) {
                Contact contact = bt5.getContact();
                Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Contact contact2 = bt5.getContact();
                    Boolean valueOf2 = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                    if (valueOf2 != null) {
                        boolean booleanValue = valueOf2.booleanValue();
                        Contact contact3 = bt5.getContact();
                        Boolean valueOf3 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                        if (valueOf3 != null) {
                            a2.a(intValue, booleanValue, valueOf3.booleanValue());
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            vw5 a3 = this.a.j;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, vw5.y.a());
            }
        }

        @DexIgnore
        @Override // com.fossil.cp5.c
        public void a() {
            ax5.b(this.a).h();
        }
    }
}
