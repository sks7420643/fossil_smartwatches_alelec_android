package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ Uri b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ pj3 e;

    @DexIgnore
    public oj3(pj3 pj3, boolean z, Uri uri, String str, String str2) {
        this.e = pj3;
        this.a = z;
        this.b = uri;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.a, this.b, this.c, this.d);
    }
}
