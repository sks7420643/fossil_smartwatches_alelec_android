package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mq3 extends y43 implements lq3 {
    @DexIgnore
    public mq3() {
        super("com.google.android.gms.wearable.internal.IWearableListener");
    }

    @DexIgnore
    @Override // com.fossil.y43
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((DataHolder) z43.a(parcel, DataHolder.CREATOR));
                return true;
            case 2:
                a((nq3) z43.a(parcel, nq3.CREATOR));
                return true;
            case 3:
                a((pq3) z43.a(parcel, pq3.CREATOR));
                return true;
            case 4:
                b((pq3) z43.a(parcel, pq3.CREATOR));
                return true;
            case 5:
                b(parcel.createTypedArrayList(pq3.CREATOR));
                return true;
            case 6:
                a((uq3) z43.a(parcel, uq3.CREATOR));
                return true;
            case 7:
                a((eq3) z43.a(parcel, eq3.CREATOR));
                return true;
            case 8:
                a((aq3) z43.a(parcel, aq3.CREATOR));
                return true;
            case 9:
                a((sq3) z43.a(parcel, sq3.CREATOR));
                return true;
            default:
                return false;
        }
    }
}
