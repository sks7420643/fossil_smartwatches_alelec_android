package com.fossil;

import com.facebook.GraphRequest;
import java.io.EOFException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ly6 implements Interceptor {
    @DexIgnore
    public static /* final */ Charset b; // = StandardCharsets.UTF_8;
    @DexIgnore
    public volatile HttpLoggingInterceptor.a a; // = HttpLoggingInterceptor.a.NONE;

    @DexIgnore
    public ly6 a(HttpLoggingInterceptor.a aVar) {
        if (aVar != null) {
            this.a = aVar;
            return this;
        }
        throw new NullPointerException("level == null. Use Level.EMPTY instead.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f2  */
    @Override // okhttp3.Interceptor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Response intercept(okhttp3.Interceptor.Chain r27) throws java.io.IOException {
        /*
            r26 = this;
            r1 = r26
            r0 = r27
            okhttp3.logging.HttpLoggingInterceptor$a r2 = r1.a
            com.fossil.lo7 r3 = r27.c()
            okhttp3.logging.HttpLoggingInterceptor$a r4 = okhttp3.logging.HttpLoggingInterceptor.a.NONE
            if (r2 != r4) goto L_0x0013
            okhttp3.Response r0 = r0.a(r3)
            return r0
        L_0x0013:
            okhttp3.logging.HttpLoggingInterceptor$a r4 = okhttp3.logging.HttpLoggingInterceptor.a.BODY
            r5 = 1
            if (r2 != r4) goto L_0x0036
            com.fossil.go7 r4 = r3.g()
            java.lang.String r4 = r4.toString()
            java.lang.String r7 = "workout-sessions"
            boolean r4 = r4.contains(r7)
            if (r4 == 0) goto L_0x0034
            java.lang.String r4 = r3.e()
            java.lang.String r7 = "patch"
            boolean r4 = r4.equalsIgnoreCase(r7)
            if (r4 == 0) goto L_0x0036
        L_0x0034:
            r4 = 1
            goto L_0x0037
        L_0x0036:
            r4 = 0
        L_0x0037:
            if (r4 != 0) goto L_0x0040
            okhttp3.logging.HttpLoggingInterceptor$a r7 = okhttp3.logging.HttpLoggingInterceptor.a.HEADERS
            if (r2 != r7) goto L_0x003e
            goto L_0x0040
        L_0x003e:
            r2 = 0
            goto L_0x0041
        L_0x0040:
            r2 = 1
        L_0x0041:
            okhttp3.RequestBody r7 = r3.a()
            if (r7 == 0) goto L_0x0048
            goto L_0x0049
        L_0x0048:
            r5 = 0
        L_0x0049:
            com.fossil.un7 r8 = r27.d()
            if (r8 == 0) goto L_0x0054
            com.fossil.jo7 r8 = r8.a()
            goto L_0x0056
        L_0x0054:
            com.fossil.jo7 r8 = com.fossil.jo7.HTTP_1_1
        L_0x0056:
            long r9 = java.lang.System.nanoTime()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "--> Id: "
            r11.append(r12)
            r11.append(r9)
            java.lang.String r12 = "\n"
            r11.append(r12)
            java.lang.String r13 = "--> "
            r11.append(r13)
            java.lang.String r13 = r3.e()
            r11.append(r13)
            r13 = 32
            r11.append(r13)
            com.fossil.go7 r14 = r3.g()
            r11.append(r14)
            r11.append(r13)
            r11.append(r8)
            java.lang.String r8 = " ("
            java.lang.String r14 = "-byte body)"
            if (r2 != 0) goto L_0x00a1
            if (r5 == 0) goto L_0x00a1
            r11.append(r8)
            r15 = r9
            long r9 = r7.a()
            r11.append(r9)
            r11.append(r14)
            goto L_0x00a2
        L_0x00a1:
            r15 = r9
        L_0x00a2:
            r11.append(r12)
            java.lang.String r9 = "empty"
            java.lang.String r10 = "-byte body omitted)"
            r17 = -1
            if (r2 == 0) goto L_0x01c9
            if (r5 == 0) goto L_0x00de
            com.fossil.ho7 r19 = r7.b()
            if (r19 == 0) goto L_0x00c4
            java.lang.String r6 = "--> Content-Type: "
            r11.append(r6)
            com.fossil.ho7 r6 = r7.b()
            r11.append(r6)
            r11.append(r12)
        L_0x00c4:
            long r20 = r7.a()
            int r6 = (r20 > r17 ? 1 : (r20 == r17 ? 0 : -1))
            if (r6 == 0) goto L_0x00de
            java.lang.String r6 = "--> Content-Length: "
            r11.append(r6)
            r20 = r14
            long r13 = r7.a()
            r11.append(r13)
            r11.append(r12)
            goto L_0x00e0
        L_0x00de:
            r20 = r14
        L_0x00e0:
            com.fossil.fo7 r13 = r3.c()
            com.fossil.ie4 r14 = new com.fossil.ie4
            r14.<init>()
            int r6 = r13.b()
            r22 = r9
            r9 = 0
        L_0x00f0:
            if (r9 >= r6) goto L_0x0118
            r23 = r6
            java.lang.String r6 = r13.a(r9)
            r24 = r15
            java.lang.String r15 = "Content-Type"
            boolean r15 = r15.equalsIgnoreCase(r6)
            if (r15 != 0) goto L_0x0111
            java.lang.String r15 = "Content-Length"
            boolean r15 = r15.equalsIgnoreCase(r6)
            if (r15 != 0) goto L_0x0111
            java.lang.String r15 = r13.b(r9)
            r14.a(r6, r15)
        L_0x0111:
            int r9 = r9 + 1
            r6 = r23
            r15 = r24
            goto L_0x00f0
        L_0x0118:
            r24 = r15
            java.lang.String r6 = "--> Headers: "
            r11.append(r6)
            r11.append(r14)
            r11.append(r12)
            java.lang.String r6 = "--> END "
            if (r4 == 0) goto L_0x01b9
            if (r5 != 0) goto L_0x012d
            goto L_0x01b9
        L_0x012d:
            com.fossil.fo7 r5 = r3.c()
            boolean r5 = r1.a(r5)
            if (r5 == 0) goto L_0x014d
            r11.append(r6)
            java.lang.String r5 = r3.e()
            r11.append(r5)
            java.lang.String r5 = " (encoded body omitted)"
            r11.append(r5)
            r11.append(r12)
            r5 = r20
            goto L_0x01ce
        L_0x014d:
            com.fossil.yq7 r5 = new com.fossil.yq7
            r5.<init>()
            r7.a(r5)
            java.nio.charset.Charset r9 = com.fossil.ly6.b
            com.fossil.ho7 r13 = r7.b()
            if (r13 == 0) goto L_0x0163
            java.nio.charset.Charset r9 = com.fossil.ly6.b
            java.nio.charset.Charset r9 = r13.a(r9)
        L_0x0163:
            boolean r13 = a(r5)
            if (r13 == 0) goto L_0x019a
            java.lang.String r13 = "--> Body: "
            r11.append(r13)
            if (r9 == 0) goto L_0x0175
            java.lang.String r5 = r5.a(r9)
            goto L_0x0177
        L_0x0175:
            r5 = r22
        L_0x0177:
            r11.append(r5)
            r11.append(r12)
            r11.append(r6)
            java.lang.String r5 = r3.e()
            r11.append(r5)
            r11.append(r8)
            long r5 = r7.a()
            r11.append(r5)
            r5 = r20
            r11.append(r5)
            r11.append(r12)
            goto L_0x01ce
        L_0x019a:
            r5 = r20
            r11.append(r6)
            java.lang.String r6 = r3.e()
            r11.append(r6)
            java.lang.String r6 = "(binary "
            r11.append(r6)
            long r6 = r7.a()
            r11.append(r6)
            r11.append(r10)
            r11.append(r12)
            goto L_0x01ce
        L_0x01b9:
            r5 = r20
            r11.append(r6)
            java.lang.String r6 = r3.e()
            r11.append(r6)
            r11.append(r12)
            goto L_0x01ce
        L_0x01c9:
            r22 = r9
            r5 = r14
            r24 = r15
        L_0x01ce:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = r11.toString()
            java.lang.String r9 = "OkHttp"
            r6.d(r9, r7)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "<-- Id: "
            r6.append(r7)
            r13 = r24
            r6.append(r13)
            r6.append(r12)
            long r13 = java.lang.System.nanoTime()
            okhttp3.Response r0 = r0.a(r3)     // Catch:{ Exception -> 0x0375 }
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r15 = java.lang.System.nanoTime()
            long r13 = r15 - r13
            long r13 = r3.toMillis(r13)
            com.fossil.mo7 r3 = r0.a()
            if (r3 == 0) goto L_0x0211
            long r15 = r3.contentLength()
            r11 = r9
            r7 = r10
            r9 = r15
            goto L_0x0215
        L_0x0211:
            r11 = r9
            r7 = r10
            r9 = r17
        L_0x0215:
            int r15 = (r9 > r17 ? 1 : (r9 == r17 ? 0 : -1))
            if (r15 != 0) goto L_0x021e
            java.lang.String r15 = "unknown-length"
            r20 = r5
            goto L_0x0231
        L_0x021e:
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r15.append(r9)
            r20 = r5
            java.lang.String r5 = "-byte"
            r15.append(r5)
            java.lang.String r15 = r15.toString()
        L_0x0231:
            if (r2 == 0) goto L_0x0238
            java.lang.String r5 = ""
            r16 = r9
            goto L_0x0250
        L_0x0238:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r16 = r9
            java.lang.String r9 = ", "
            r5.append(r9)
            r5.append(r15)
            java.lang.String r9 = " body"
            r5.append(r9)
            java.lang.String r5 = r5.toString()
        L_0x0250:
            java.lang.String r9 = "<-- Code "
            r6.append(r9)
            int r9 = r0.e()
            r6.append(r9)
            r9 = 32
            r6.append(r9)
            java.lang.String r10 = r0.n()
            r6.append(r10)
            r6.append(r9)
            com.fossil.lo7 r9 = r0.x()
            com.fossil.go7 r9 = r9.g()
            r6.append(r9)
            r6.append(r8)
            r6.append(r13)
            java.lang.String r8 = "ms"
            r6.append(r8)
            r6.append(r5)
            r5 = 41
            r6.append(r5)
            r6.append(r12)
            if (r2 == 0) goto L_0x0367
            com.fossil.fo7 r2 = r0.k()
            com.fossil.ie4 r5 = new com.fossil.ie4
            r5.<init>()
            int r8 = r2.b()
            r9 = 0
        L_0x029c:
            if (r9 >= r8) goto L_0x02ac
            java.lang.String r10 = r2.a(r9)
            java.lang.String r13 = r2.b(r9)
            r5.a(r10, r13)
            int r9 = r9 + 1
            goto L_0x029c
        L_0x02ac:
            java.lang.String r2 = "<-- Headers: "
            r6.append(r2)
            r6.append(r5)
            r6.append(r12)
            if (r4 == 0) goto L_0x035f
            boolean r2 = com.fossil.kp7.b(r0)
            if (r2 != 0) goto L_0x02c1
            goto L_0x035f
        L_0x02c1:
            com.fossil.fo7 r2 = r0.k()
            boolean r2 = r1.a(r2)
            if (r2 == 0) goto L_0x02d5
            java.lang.String r2 = "<-- END HTTP (encoded body omitted)"
            r6.append(r2)
            r6.append(r12)
            goto L_0x0367
        L_0x02d5:
            java.lang.String r2 = "<-- END HTTP ("
            if (r3 == 0) goto L_0x034e
            com.fossil.ar7 r4 = r3.source()
            r8 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r4.d(r8)
            com.fossil.yq7 r4 = r4.buffer()
            java.nio.charset.Charset r5 = com.fossil.ly6.b
            com.fossil.ho7 r3 = r3.contentType()
            if (r3 == 0) goto L_0x02f7
            java.nio.charset.Charset r5 = com.fossil.ly6.b
            java.nio.charset.Charset r5 = r3.a(r5)
        L_0x02f7:
            boolean r3 = a(r4)
            if (r3 != 0) goto L_0x031d
            java.lang.String r2 = "<-- END HTTP (binary "
            r6.append(r2)
            long r2 = r4.x()
            r6.append(r2)
            r6.append(r7)
            r6.append(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.toString()
            r2.d(r11, r3)
            return r0
        L_0x031d:
            r7 = 0
            int r3 = (r16 > r7 ? 1 : (r16 == r7 ? 0 : -1))
            if (r3 == 0) goto L_0x033b
            java.lang.String r3 = "<-- Body: "
            r6.append(r3)
            if (r5 == 0) goto L_0x0333
            com.fossil.yq7 r3 = r4.clone()
            java.lang.String r9 = r3.a(r5)
            goto L_0x0335
        L_0x0333:
            r9 = r22
        L_0x0335:
            r6.append(r9)
            r6.append(r12)
        L_0x033b:
            r6.append(r2)
            long r2 = r4.x()
            r6.append(r2)
            r3 = r20
            r6.append(r3)
            r6.append(r12)
            goto L_0x0367
        L_0x034e:
            r3 = r20
            r6.append(r2)
            java.lang.String r2 = "-1"
            r6.append(r2)
            r6.append(r3)
            r6.append(r12)
            goto L_0x0367
        L_0x035f:
            java.lang.String r2 = "<-- END HTTP"
            r6.append(r2)
            r6.append(r12)
        L_0x0367:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.toString()
            r2.d(r11, r3)
            return r0
        L_0x0375:
            r0 = move-exception
            r11 = r9
            r2 = r0
            java.lang.String r0 = "<-- HTTP FAILED: "
            r6.append(r0)
            r6.append(r2)
            r6.append(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = r6.toString()
            r0.d(r11, r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ly6.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }

    @DexIgnore
    public static boolean a(yq7 yq7) {
        try {
            yq7 yq72 = new yq7();
            yq7.a(yq72, 0, yq7.x() < 64 ? yq7.x() : 64);
            for (int i = 0; i < 16; i++) {
                if (yq72.h()) {
                    return true;
                }
                int w = yq72.w();
                if (Character.isISOControl(w) && !Character.isWhitespace(w)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }

    @DexIgnore
    public final boolean a(fo7 fo7) {
        String a2 = fo7.a(GraphRequest.CONTENT_ENCODING_HEADER);
        return a2 != null && !a2.equalsIgnoreCase("identity");
    }
}
