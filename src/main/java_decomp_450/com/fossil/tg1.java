package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg1 extends fe7 implements gd7<BluetoothGattCharacteristic, String> {
    @DexIgnore
    public static /* final */ tg1 a; // = new tg1();

    @DexIgnore
    public tg1() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public String invoke(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic2 = bluetoothGattCharacteristic;
        StringBuilder sb = new StringBuilder();
        sb.append("    ");
        ee7.a((Object) bluetoothGattCharacteristic2, "characteristic");
        sb.append(bluetoothGattCharacteristic2.getUuid().toString());
        return sb.toString();
    }
}
