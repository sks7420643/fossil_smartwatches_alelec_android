package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t76 implements MembersInjector<ActiveTimeOverviewFragment> {
    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, q76 q76) {
        activeTimeOverviewFragment.g = q76;
    }

    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, h86 h86) {
        activeTimeOverviewFragment.h = h86;
    }

    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, b86 b86) {
        activeTimeOverviewFragment.i = b86;
    }
}
