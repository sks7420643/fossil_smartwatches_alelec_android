package com.fossil;

import com.fossil.v02;
import com.fossil.v02.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p12<O extends v02.d> {
    @DexIgnore
    public /* final */ boolean a; // = true;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ v02<O> c;
    @DexIgnore
    public /* final */ O d;

    @DexIgnore
    public p12(v02<O> v02, O o) {
        this.c = v02;
        this.d = o;
        this.b = y62.a(v02, o);
    }

    @DexIgnore
    public static <O extends v02.d> p12<O> a(v02<O> v02, O o) {
        return new p12<>(v02, o);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof p12)) {
            return false;
        }
        p12 p12 = (p12) obj;
        return !this.a && !p12.a && y62.a(this.c, p12.c) && y62.a(this.d, p12.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b;
    }

    @DexIgnore
    public static <O extends v02.d> p12<O> a(v02<O> v02) {
        return new p12<>(v02);
    }

    @DexIgnore
    public final String a() {
        return this.c.b();
    }

    @DexIgnore
    public p12(v02<O> v02) {
        this.c = v02;
        this.d = null;
        this.b = System.identityHashCode(this);
    }
}
