package com.fossil;

import com.fossil.s87;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig7<T> extends jg7<T> implements Iterator<T>, fb7<i97>, ye7 {
    @DexIgnore
    public int a;
    @DexIgnore
    public T b;
    @DexIgnore
    public Iterator<? extends T> c;
    @DexIgnore
    public fb7<? super i97> d;

    @DexIgnore
    public final void a(fb7<? super i97> fb7) {
        this.d = fb7;
    }

    @DexIgnore
    public final T b() {
        if (hasNext()) {
            return next();
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        return jb7.INSTANCE;
    }

    @DexIgnore
    public boolean hasNext() {
        while (true) {
            int i = this.a;
            if (i != 0) {
                if (i == 1) {
                    Iterator<? extends T> it = this.c;
                    if (it == null) {
                        ee7.a();
                        throw null;
                    } else if (it.hasNext()) {
                        this.a = 2;
                        return true;
                    } else {
                        this.c = null;
                    }
                } else if (i == 2 || i == 3) {
                    return true;
                } else {
                    if (i == 4) {
                        return false;
                    }
                    throw a();
                }
            }
            this.a = 5;
            fb7<? super i97> fb7 = this.d;
            if (fb7 != null) {
                this.d = null;
                i97 i97 = i97.a;
                s87.a aVar = s87.Companion;
                fb7.resumeWith(s87.m60constructorimpl(i97));
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        int i = this.a;
        if (i == 0 || i == 1) {
            return b();
        }
        if (i == 2) {
            this.a = 1;
            Iterator<? extends T> it = this.c;
            if (it != null) {
                return (T) it.next();
            }
            ee7.a();
            throw null;
        } else if (i == 3) {
            this.a = 0;
            T t = this.b;
            this.b = null;
            return t;
        } else {
            throw a();
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public void resumeWith(Object obj) {
        t87.a(obj);
        this.a = 4;
    }

    @DexIgnore
    public final Throwable a() {
        int i = this.a;
        if (i == 4) {
            return new NoSuchElementException();
        }
        if (i == 5) {
            return new IllegalStateException("Iterator has failed.");
        }
        return new IllegalStateException("Unexpected state of the iterator: " + this.a);
    }

    @DexIgnore
    @Override // com.fossil.jg7
    public Object a(T t, fb7<? super i97> fb7) {
        this.b = t;
        this.a = 3;
        this.d = fb7;
        Object a2 = nb7.a();
        if (a2 == nb7.a()) {
            vb7.c(fb7);
        }
        return a2 == nb7.a() ? a2 : i97.a;
    }
}
