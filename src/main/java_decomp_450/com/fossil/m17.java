package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m17 extends m07<Target> {
    @DexIgnore
    public m17(Picasso picasso, Target target, g17 g17, int i, int i2, Drawable drawable, String str, Object obj, int i3) {
        super(picasso, target, g17, i, i2, i3, drawable, str, obj, false);
    }

    @DexIgnore
    @Override // com.fossil.m07
    public void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            Target target = (Target) j();
            if (target != null) {
                target.onBitmapLoaded(bitmap, loadedFrom);
                if (bitmap.isRecycled()) {
                    throw new IllegalStateException("Target callback must not recycle bitmap!");
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    @DexIgnore
    @Override // com.fossil.m07
    public void b() {
        Target target = (Target) j();
        if (target == null) {
            return;
        }
        if (((m07) this).g != 0) {
            target.onBitmapFailed(((m07) this).a.e.getResources().getDrawable(((m07) this).g));
        } else {
            target.onBitmapFailed(((m07) this).h);
        }
    }
}
