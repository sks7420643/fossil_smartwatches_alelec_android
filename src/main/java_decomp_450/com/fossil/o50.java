package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o50 extends InputStream {
    @DexIgnore
    public static /* final */ Queue<o50> c; // = v50.a(0);
    @DexIgnore
    public InputStream a;
    @DexIgnore
    public IOException b;

    @DexIgnore
    public static o50 b(InputStream inputStream) {
        o50 poll;
        synchronized (c) {
            poll = c.poll();
        }
        if (poll == null) {
            poll = new o50();
        }
        poll.a(inputStream);
        return poll;
    }

    @DexIgnore
    public void a(InputStream inputStream) {
        this.a = inputStream;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int available() throws IOException {
        return this.a.available();
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public void mark(int i) {
        this.a.mark(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.a.markSupported();
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        try {
            return this.a.read(bArr);
        } catch (IOException e) {
            this.b = e;
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.InputStream
    public synchronized void reset() throws IOException {
        this.a.reset();
    }

    @DexIgnore
    @Override // java.io.InputStream
    public long skip(long j) {
        try {
            return this.a.skip(j);
        } catch (IOException e) {
            this.b = e;
            return 0;
        }
    }

    @DexIgnore
    public IOException a() {
        return this.b;
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.a.read(bArr, i, i2);
        } catch (IOException e) {
            this.b = e;
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.InputStream
    public int read() {
        try {
            return this.a.read();
        } catch (IOException e) {
            this.b = e;
            return -1;
        }
    }

    @DexIgnore
    public void b() {
        this.b = null;
        this.a = null;
        synchronized (c) {
            c.offer(this);
        }
    }
}
