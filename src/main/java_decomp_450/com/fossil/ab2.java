package com.fossil;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ab2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends dg2 implements ab2 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ab2$a$a")
        /* renamed from: com.fossil.ab2$a$a  reason: collision with other inner class name */
        public static class C0008a extends eg2 implements ab2 {
            @DexIgnore
            public C0008a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        @DexIgnore
        public a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        @DexIgnore
        public static ab2 a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            if (queryLocalInterface instanceof ab2) {
                return (ab2) queryLocalInterface;
            }
            return new C0008a(iBinder);
        }
    }
}
