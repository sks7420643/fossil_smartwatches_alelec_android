package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi4 implements sg4 {
    @DexIgnore
    public /* final */ gi4 a; // = new gi4();

    @DexIgnore
    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (mg4 == mg4.UPC_A) {
            return this.a.a(a(str), mg4.EAN_13, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode UPC-A, but got " + mg4);
    }

    @DexIgnore
    public static String a(String str) {
        int length = str.length();
        if (length == 11) {
            int i = 0;
            for (int i2 = 0; i2 < 11; i2++) {
                i += (str.charAt(i2) - '0') * (i2 % 2 == 0 ? 3 : 1);
            }
            str = str + ((1000 - i) % 10);
        } else if (length != 12) {
            throw new IllegalArgumentException("Requested contents should be 11 or 12 digits long, but got " + str.length());
        }
        return "0" + str;
    }
}
