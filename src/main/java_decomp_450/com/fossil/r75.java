package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r75 extends q75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362923, 1);
        x.put(2131362061, 2);
        x.put(2131362400, 3);
        x.put(2131361970, 4);
    }
    */

    @DexIgnore
    public r75(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 5, w, x));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public r75(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[4], (ConstraintLayout) objArr[2], (FrameLayout) objArr[0], (FlexibleTextView) objArr[3], (ProgressBar) objArr[1]);
        this.v = -1;
        ((q75) this).s.setTag(null);
        a(view);
        f();
    }
}
