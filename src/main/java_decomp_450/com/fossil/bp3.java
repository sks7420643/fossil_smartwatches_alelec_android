package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp3<TResult> implements hp3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public io3 c;

    @DexIgnore
    public bp3(Executor executor, io3 io3) {
        this.a = executor;
        this.c = io3;
    }

    @DexIgnore
    @Override // com.fossil.hp3
    public final void a(no3<TResult> no3) {
        if (!no3.e() && !no3.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new cp3(this, no3));
                }
            }
        }
    }
}
