package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.os;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps extends ts implements os {
    @DexIgnore
    public /* final */ ut<?> a;
    @DexIgnore
    public /* final */ ds b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.memory.PoolableTargetDelegate", f = "TargetDelegate.kt", l = {234}, m = "error")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ps this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ps psVar, fb7 fb7) {
            super(fb7);
            this.this$0 = psVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.memory.PoolableTargetDelegate", f = "TargetDelegate.kt", l = {213}, m = "success")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ps this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ps psVar, fb7 fb7) {
            super(fb7);
            this.this$0 = psVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Drawable) null, false, (zt) null, (fb7<? super i97>) this);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ps(ut<?> utVar, ds dsVar) {
        super(null);
        ee7.b(utVar, "target");
        ee7.b(dsVar, "referenceCounter");
        this.a = utVar;
        this.b = dsVar;
    }

    @DexIgnore
    @Override // com.fossil.os
    public void a(Bitmap bitmap) {
        os.a.a(this, bitmap);
    }

    @DexIgnore
    @Override // com.fossil.os
    public void b(Bitmap bitmap) {
        os.a.b(this, bitmap);
    }

    @DexIgnore
    @Override // com.fossil.ts
    public void c() {
        b(null);
        b().a();
        a(null);
    }

    @DexIgnore
    @Override // com.fossil.os
    public ds a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.os
    public ut<?> b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ts
    public void a(BitmapDrawable bitmapDrawable, Drawable drawable) {
        Bitmap bitmap = bitmapDrawable != null ? bitmapDrawable.getBitmap() : null;
        b(bitmap);
        b().c(drawable);
        a(bitmap);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // com.fossil.ts
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(android.graphics.drawable.Drawable r7, boolean r8, com.fossil.zt r9, com.fossil.fb7<? super com.fossil.i97> r10) {
        /*
            r6 = this;
            boolean r0 = r10 instanceof com.fossil.ps.b
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.ps$b r0 = (com.fossil.ps.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ps$b r0 = new com.fossil.ps$b
            r0.<init>(r6, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x005a
            if (r2 != r3) goto L_0x0052
            java.lang.Object r7 = r0.L$8
            com.fossil.zt r7 = (com.fossil.zt) r7
            boolean r7 = r0.Z$1
            java.lang.Object r7 = r0.L$7
            android.graphics.drawable.Drawable r7 = (android.graphics.drawable.Drawable) r7
            java.lang.Object r7 = r0.L$6
            com.fossil.vt r7 = (com.fossil.vt) r7
            java.lang.Object r7 = r0.L$5
            com.fossil.ut r7 = (com.fossil.ut) r7
            java.lang.Object r7 = r0.L$4
            android.graphics.Bitmap r7 = (android.graphics.Bitmap) r7
            java.lang.Object r8 = r0.L$3
            com.fossil.ps r8 = (com.fossil.ps) r8
            java.lang.Object r9 = r0.L$2
            com.fossil.zt r9 = (com.fossil.zt) r9
            boolean r9 = r0.Z$0
            java.lang.Object r9 = r0.L$1
            android.graphics.drawable.Drawable r9 = (android.graphics.drawable.Drawable) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.ps r9 = (com.fossil.ps) r9
            com.fossil.t87.a(r10)
            goto L_0x00d4
        L_0x0052:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x005a:
            com.fossil.t87.a(r10)
            android.graphics.Bitmap r10 = com.fossil.us.b(r7)
            r6.b(r10)
            com.fossil.ut r2 = r6.b()
            if (r9 != 0) goto L_0x006e
            r2.a(r7)
            goto L_0x00a9
        L_0x006e:
            boolean r4 = r2 instanceof com.fossil.bu
            if (r4 != 0) goto L_0x00ab
            r8 = 5
            com.fossil.cu r0 = com.fossil.cu.c
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00a6
            com.fossil.cu r0 = com.fossil.cu.c
            int r0 = r0.b()
            if (r0 > r8) goto L_0x00a6
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Ignoring '"
            r0.append(r1)
            r0.append(r9)
            java.lang.String r9 = "' as '"
            r0.append(r9)
            r0.append(r2)
            java.lang.String r9 = "' does not implement coil.transition.TransitionTarget."
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            java.lang.String r0 = "TargetDelegate"
            android.util.Log.println(r8, r0, r9)
        L_0x00a6:
            r2.a(r7)
        L_0x00a9:
            r8 = r6
            goto L_0x00d5
        L_0x00ab:
            r4 = r2
            com.fossil.bu r4 = (com.fossil.bu) r4
            com.fossil.au$b r5 = new com.fossil.au$b
            r5.<init>(r7, r8)
            r0.L$0 = r6
            r0.L$1 = r7
            r0.Z$0 = r8
            r0.L$2 = r9
            r0.L$3 = r6
            r0.L$4 = r10
            r0.L$5 = r2
            r0.L$6 = r2
            r0.L$7 = r7
            r0.Z$1 = r8
            r0.L$8 = r9
            r0.label = r3
            java.lang.Object r7 = r9.a(r4, r5, r0)
            if (r7 != r1) goto L_0x00d2
            return r1
        L_0x00d2:
            r8 = r6
            r7 = r10
        L_0x00d4:
            r10 = r7
        L_0x00d5:
            r8.a(r10)
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ps.a(android.graphics.drawable.Drawable, boolean, com.fossil.zt, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    @Override // com.fossil.ts
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(android.graphics.drawable.Drawable r7, com.fossil.zt r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r6 = this;
            boolean r0 = r9 instanceof com.fossil.ps.a
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.ps$a r0 = (com.fossil.ps.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ps$a r0 = new com.fossil.ps$a
            r0.<init>(r6, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0053
            if (r2 != r4) goto L_0x004b
            java.lang.Object r7 = r0.L$7
            com.fossil.zt r7 = (com.fossil.zt) r7
            java.lang.Object r7 = r0.L$6
            android.graphics.drawable.Drawable r7 = (android.graphics.drawable.Drawable) r7
            java.lang.Object r7 = r0.L$5
            com.fossil.vt r7 = (com.fossil.vt) r7
            java.lang.Object r7 = r0.L$4
            com.fossil.ut r7 = (com.fossil.ut) r7
            java.lang.Object r7 = r0.L$3
            com.fossil.ps r7 = (com.fossil.ps) r7
            java.lang.Object r8 = r0.L$2
            com.fossil.zt r8 = (com.fossil.zt) r8
            java.lang.Object r8 = r0.L$1
            android.graphics.drawable.Drawable r8 = (android.graphics.drawable.Drawable) r8
            java.lang.Object r8 = r0.L$0
            com.fossil.ps r8 = (com.fossil.ps) r8
            com.fossil.t87.a(r9)
            goto L_0x00c1
        L_0x004b:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x0053:
            com.fossil.t87.a(r9)
            r6.b(r3)
            com.fossil.ut r9 = r6.b()
            if (r8 != 0) goto L_0x0063
            r9.b(r7)
            goto L_0x009e
        L_0x0063:
            boolean r2 = r9 instanceof com.fossil.bu
            if (r2 != 0) goto L_0x00a0
            r0 = 5
            com.fossil.cu r1 = com.fossil.cu.c
            boolean r1 = r1.a()
            if (r1 == 0) goto L_0x009b
            com.fossil.cu r1 = com.fossil.cu.c
            int r1 = r1.b()
            if (r1 > r0) goto L_0x009b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Ignoring '"
            r1.append(r2)
            r1.append(r8)
            java.lang.String r8 = "' as '"
            r1.append(r8)
            r1.append(r9)
            java.lang.String r8 = "' does not implement coil.transition.TransitionTarget."
            r1.append(r8)
            java.lang.String r8 = r1.toString()
            java.lang.String r1 = "TargetDelegate"
            android.util.Log.println(r0, r1, r8)
        L_0x009b:
            r9.b(r7)
        L_0x009e:
            r7 = r6
            goto L_0x00c1
        L_0x00a0:
            r2 = r9
            com.fossil.bu r2 = (com.fossil.bu) r2
            com.fossil.au$a r5 = new com.fossil.au$a
            r5.<init>(r7)
            r0.L$0 = r6
            r0.L$1 = r7
            r0.L$2 = r8
            r0.L$3 = r6
            r0.L$4 = r9
            r0.L$5 = r9
            r0.L$6 = r7
            r0.L$7 = r8
            r0.label = r4
            java.lang.Object r7 = r8.a(r2, r5, r0)
            if (r7 != r1) goto L_0x009e
            return r1
        L_0x00c1:
            r7.a(r3)
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ps.a(android.graphics.drawable.Drawable, com.fossil.zt, com.fossil.fb7):java.lang.Object");
    }
}
