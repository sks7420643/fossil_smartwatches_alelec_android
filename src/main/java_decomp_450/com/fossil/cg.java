package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cg {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968707;
    @DexIgnore
    public static /* final */ int fastScrollEnabled; // = 2130969178;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalThumbDrawable; // = 2130969179;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalTrackDrawable; // = 2130969180;
    @DexIgnore
    public static /* final */ int fastScrollVerticalThumbDrawable; // = 2130969181;
    @DexIgnore
    public static /* final */ int fastScrollVerticalTrackDrawable; // = 2130969182;
    @DexIgnore
    public static /* final */ int font; // = 2130969196;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969198;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969199;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969200;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969201;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969202;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969203;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969204;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969205;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969206;
    @DexIgnore
    public static /* final */ int layoutManager; // = 2130969329;
    @DexIgnore
    public static /* final */ int recyclerViewStyle; // = 2130969542;
    @DexIgnore
    public static /* final */ int reverseLayout; // = 2130969549;
    @DexIgnore
    public static /* final */ int spanCount; // = 2130969679;
    @DexIgnore
    public static /* final */ int stackFromEnd; // = 2130969686;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969842;
}
