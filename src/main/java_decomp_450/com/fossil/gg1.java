package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Handler;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gg1 {
    @DexIgnore
    public gv0 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ ArrayList<File> c; // = new ArrayList<>();
    @DexIgnore
    public d91 d;
    @DexIgnore
    public db1 e;
    @DexIgnore
    public long f; // = 60000;
    @DexIgnore
    public /* final */ g71 g;
    @DexIgnore
    public dh0 h;
    @DexIgnore
    public /* final */ bi0 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public boolean k;

    @DexIgnore
    public gg1(g71 g71, dh0 dh0, bi0 bi0, Handler handler, boolean z) {
        this.g = g71;
        this.h = dh0;
        this.i = bi0;
        this.j = handler;
        this.k = z;
        a(this.h);
    }

    @DexIgnore
    public final void a() {
        Context a2 = u31.g.a();
        Boolean bool = null;
        ActivityManager activityManager = (ActivityManager) (a2 != null ? a2.getSystemService(Constants.ACTIVITY) : null);
        if (activityManager != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            bool = Boolean.valueOf(memoryInfo.lowMemory);
        }
        if (!(!ee7.a((Object) bool, (Object) false))) {
            synchronized (Boolean.valueOf(this.b)) {
                if (!this.b) {
                    this.b = true;
                    this.c.clear();
                    ba7.a(this.c, t97.a((Comparable[]) this.g.b()));
                    c();
                }
            }
        }
    }

    @DexIgnore
    public final void b() {
        synchronized (Boolean.valueOf(this.b)) {
            this.c.clear();
            this.b = false;
            i97 i97 = i97.a;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a1, code lost:
        if (r4.hasTransport(1) != false) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00c9, code lost:
        if (r5.getNetworkId() != -1) goto L_0x00cb;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r8 = this;
            com.fossil.gv0 r0 = r8.a
            r1 = 1
            if (r0 != 0) goto L_0x0033
            com.fossil.dh0 r2 = r8.h
            java.lang.String r2 = r2.c()
            boolean r2 = com.fossil.mh7.a(r2)
            r2 = r2 ^ r1
            if (r2 == 0) goto L_0x0033
            com.fossil.dh0 r2 = r8.h
            java.lang.String r2 = r2.a()
            boolean r2 = com.fossil.mh7.a(r2)
            r2 = r2 ^ r1
            if (r2 == 0) goto L_0x0033
            com.fossil.dh0 r2 = r8.h
            java.lang.String r2 = r2.b()
            boolean r2 = com.fossil.mh7.a(r2)
            r2 = r2 ^ r1
            if (r2 == 0) goto L_0x0033
            com.fossil.t11 r2 = com.fossil.t11.a
            com.fossil.dh0 r2 = r8.h
            r8.a(r2)
        L_0x0033:
            r2 = 0
            r3 = 0
            if (r0 != 0) goto L_0x003b
            java.lang.String r4 = "Invalid end point"
            goto L_0x00d8
        L_0x003b:
            com.fossil.u31 r4 = com.fossil.u31.g
            android.content.Context r4 = r4.a()
            java.lang.String r5 = "connectivity"
            if (r4 == 0) goto L_0x004a
            java.lang.Object r4 = r4.getSystemService(r5)
            goto L_0x004b
        L_0x004a:
            r4 = r3
        L_0x004b:
            android.net.ConnectivityManager r4 = (android.net.ConnectivityManager) r4
            int r6 = android.os.Build.VERSION.SDK_INT
            r7 = 23
            if (r6 < r7) goto L_0x006b
            if (r4 == 0) goto L_0x0070
            android.net.Network r6 = r4.getActiveNetwork()
            if (r6 == 0) goto L_0x0070
            android.net.NetworkCapabilities r4 = r4.getNetworkCapabilities(r6)
            if (r4 == 0) goto L_0x0070
            r6 = 18
            boolean r4 = r4.hasCapability(r6)
            if (r4 == 0) goto L_0x0070
            r4 = 1
            goto L_0x0071
        L_0x006b:
            if (r4 == 0) goto L_0x0070
            r4.getActiveNetworkInfo()
        L_0x0070:
            r4 = 0
        L_0x0071:
            if (r4 != 0) goto L_0x0077
            java.lang.String r4 = "Network is not available"
            goto L_0x00d8
        L_0x0077:
            boolean r4 = r8.k
            if (r4 == 0) goto L_0x00d3
            com.fossil.u31 r4 = com.fossil.u31.g
            android.content.Context r4 = r4.a()
            int r6 = android.os.Build.VERSION.SDK_INT
            if (r6 < r7) goto L_0x00a4
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r4.getSystemService(r5)
            goto L_0x008d
        L_0x008c:
            r4 = r3
        L_0x008d:
            android.net.ConnectivityManager r4 = (android.net.ConnectivityManager) r4
            if (r4 == 0) goto L_0x00cd
            android.net.Network r5 = r4.getActiveNetwork()
            if (r5 == 0) goto L_0x00cd
            android.net.NetworkCapabilities r4 = r4.getNetworkCapabilities(r5)
            if (r4 == 0) goto L_0x00cd
            boolean r4 = r4.hasTransport(r1)
            if (r4 == 0) goto L_0x00cd
            goto L_0x00cb
        L_0x00a4:
            if (r4 == 0) goto L_0x00b3
            android.content.Context r4 = r4.getApplicationContext()
            if (r4 == 0) goto L_0x00b3
            java.lang.String r5 = "wifi"
            java.lang.Object r4 = r4.getSystemService(r5)
            goto L_0x00b4
        L_0x00b3:
            r4 = r3
        L_0x00b4:
            android.net.wifi.WifiManager r4 = (android.net.wifi.WifiManager) r4
            if (r4 == 0) goto L_0x00cd
            android.net.wifi.WifiInfo r5 = r4.getConnectionInfo()
            boolean r4 = r4.isWifiEnabled()
            if (r4 == 0) goto L_0x00cd
            if (r5 == 0) goto L_0x00cd
            int r4 = r5.getNetworkId()
            r5 = -1
            if (r4 == r5) goto L_0x00cd
        L_0x00cb:
            r4 = 1
            goto L_0x00ce
        L_0x00cd:
            r4 = 0
        L_0x00ce:
            if (r4 != 0) goto L_0x00d3
            java.lang.String r4 = "Wifi is not available"
            goto L_0x00d8
        L_0x00d3:
            java.lang.String r4 = new java.lang.String
            r4.<init>()
        L_0x00d8:
            int r4 = r4.length()
            if (r4 <= 0) goto L_0x00e0
            r4 = 1
            goto L_0x00e1
        L_0x00e0:
            r4 = 0
        L_0x00e1:
            if (r4 != 0) goto L_0x0196
            if (r0 != 0) goto L_0x00e7
            goto L_0x0196
        L_0x00e7:
            java.util.ArrayList<java.io.File> r4 = r8.c
            java.lang.Object r4 = com.fossil.ea7.e(r4)
            java.io.File r4 = (java.io.File) r4
            if (r4 == 0) goto L_0x0192
            com.fossil.u31 r5 = com.fossil.u31.g     // Catch:{ OutOfMemoryError -> 0x018e }
            android.content.Context r5 = r5.a()     // Catch:{ OutOfMemoryError -> 0x018e }
            java.lang.String r6 = "activity"
            if (r5 == 0) goto L_0x0100
            java.lang.Object r5 = r5.getSystemService(r6)
            goto L_0x0101
        L_0x0100:
            r5 = r3
        L_0x0101:
            android.app.ActivityManager r5 = (android.app.ActivityManager) r5
            if (r5 == 0) goto L_0x0114
            android.app.ActivityManager$MemoryInfo r7 = new android.app.ActivityManager$MemoryInfo
            r7.<init>()
            r5.getMemoryInfo(r7)
            boolean r5 = r7.lowMemory
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            goto L_0x0115
        L_0x0114:
            r5 = r3
        L_0x0115:
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r2)
            boolean r5 = com.fossil.ee7.a(r5, r7)
            r5 = r5 ^ r1
            if (r5 == 0) goto L_0x0124
            r8.b()
            return
        L_0x0124:
            com.fossil.bi0 r5 = r8.i
            org.json.JSONObject r5 = r5.b(r4)
            int r7 = r5.length()
            if (r7 <= 0) goto L_0x017b
            com.fossil.u31 r7 = com.fossil.u31.g
            android.content.Context r7 = r7.a()
            if (r7 == 0) goto L_0x013d
            java.lang.Object r6 = r7.getSystemService(r6)
            goto L_0x013e
        L_0x013d:
            r6 = r3
        L_0x013e:
            android.app.ActivityManager r6 = (android.app.ActivityManager) r6
            if (r6 == 0) goto L_0x0150
            android.app.ActivityManager$MemoryInfo r3 = new android.app.ActivityManager$MemoryInfo
            r3.<init>()
            r6.getMemoryInfo(r3)
            boolean r3 = r3.lowMemory
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
        L_0x0150:
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            boolean r2 = com.fossil.ee7.a(r3, r2)
            r1 = r1 ^ r2
            if (r1 == 0) goto L_0x015f
            r8.b()
            return
        L_0x015f:
            java.lang.String r1 = r5.toString()
            java.lang.String r2 = "logFileInJSON.toString()"
            com.fossil.ee7.a(r1, r2)
            com.fossil.dh0 r2 = r8.h
            java.lang.String r2 = r2.c()
            retrofit2.Call r0 = r0.a(r2, r1)
            com.fossil.sc1 r1 = new com.fossil.sc1
            r1.<init>(r8, r4)
            r0.a(r1)
            goto L_0x0195
        L_0x017b:
            java.util.ArrayList<java.io.File> r0 = r8.c
            r0.remove(r4)
            r4.delete()
            android.os.Handler r0 = r8.j
            com.fossil.me1 r1 = new com.fossil.me1
            r1.<init>(r8)
            r0.post(r1)
            goto L_0x0195
        L_0x018e:
            r8.b()
            goto L_0x0195
        L_0x0192:
            r8.b()
        L_0x0195:
            return
        L_0x0196:
            com.fossil.t11 r0 = com.fossil.t11.a
            r8.b()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gg1.c():void");
    }

    @DexIgnore
    public final synchronized boolean a(dh0 dh0) {
        gv0 gv0;
        t11 t11 = t11.a;
        StringBuilder b2 = yh0.b("updateEndPoint: url=");
        b2.append(dh0.c());
        b2.append(", ");
        b2.append("access=");
        b2.append(dh0.a());
        b2.append(", secret=");
        b2.append(dh0.b());
        b2.append('.');
        b2.toString();
        if (dh0.c().length() == 0) {
            return false;
        }
        this.h = dh0;
        String b3 = dh0.b();
        String a2 = dh0.a();
        OkHttpClient.b bVar = new OkHttpClient.b();
        bVar.b(new yw0(a2, b3));
        bVar.b(30000, TimeUnit.MILLISECONDS);
        bVar.c(60000, TimeUnit.MILLISECONDS);
        try {
            Retrofit.b bVar2 = new Retrofit.b();
            bVar2.a(bVar.a());
            bVar2.a(wv7.a());
            bVar2.a("http://localhost/");
            gv0 = (gv0) bVar2.a().a(gv0.class);
        } catch (Exception e2) {
            wl0.h.a(e2);
            gv0 = null;
        }
        if (gv0 != null) {
            this.a = gv0;
            t11 t112 = t11.a;
            StringBuilder b4 = yh0.b("updateEndPoint: success, client=");
            b4.append(this.a);
            b4.toString();
            return true;
        }
        t11 t113 = t11.a;
        return false;
    }
}
