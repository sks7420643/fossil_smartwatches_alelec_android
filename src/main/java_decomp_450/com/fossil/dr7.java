package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dr7 implements sr7 {
    @DexIgnore
    public /* final */ sr7 a;

    @DexIgnore
    public dr7(sr7 sr7) {
        ee7.b(sr7, "delegate");
        this.a = sr7;
    }

    @DexIgnore
    public final sr7 a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public long b(yq7 yq7, long j) throws IOException {
        ee7.b(yq7, "sink");
        return this.a.b(yq7, j);
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public tr7 d() {
        return this.a.d();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '(' + this.a + ')';
    }
}
