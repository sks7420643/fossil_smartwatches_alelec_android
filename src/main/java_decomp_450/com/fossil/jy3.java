package com.fossil;

import com.fossil.by3;
import com.fossil.gy3;
import com.fossil.iy3;
import com.fossil.ny3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jy3<K, V> extends gy3<K, V> implements xz3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient iy3<V> f;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient jy3<V, K> g;
    @DexIgnore
    public transient iy3<Map.Entry<K, V>> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends iy3<Map.Entry<K, V>> {
        @DexIgnore
        @Weak
        public /* final */ transient jy3<K, V> b;

        @DexIgnore
        public b(jy3<K, V> jy3) {
            this.b = jy3;
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.b.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        public int size() {
            return this.b.size();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
        public j04<Map.Entry<K, V>> iterator() {
            return this.b.entryIterator();
        }
    }

    @DexIgnore
    public jy3(by3<K, iy3<V>> by3, int i, Comparator<? super V> comparator) {
        super(by3, i);
        this.f = a(comparator);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.by3$b */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> jy3<K, V> a(zy3<? extends K, ? extends V> zy3, Comparator<? super V> comparator) {
        jw3.a(zy3);
        if (zy3.isEmpty() && comparator == null) {
            return of();
        }
        if (zy3 instanceof jy3) {
            jy3<K, V> jy3 = (jy3) zy3;
            if (!jy3.isPartialView()) {
                return jy3;
            }
        }
        by3.b bVar = new by3.b(zy3.asMap().size());
        int i = 0;
        for (Map.Entry<? extends K, Collection<? extends V>> entry : zy3.asMap().entrySet()) {
            Object key = entry.getKey();
            iy3 a2 = a(comparator, entry.getValue());
            if (!a2.isEmpty()) {
                bVar.a(key, a2);
                i += a2.size();
            }
        }
        return new jy3<>(bVar.a(), i, comparator);
    }

    @DexIgnore
    public static <V> iy3.a<V> b(Comparator<? super V> comparator) {
        return comparator == null ? new iy3.a<>() : new ny3.a(comparator);
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> copyOf(zy3<? extends K, ? extends V> zy3) {
        return a(zy3, (Comparator) null);
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> of() {
        return ix3.INSTANCE;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: com.fossil.by3$b */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        Comparator comparator = (Comparator) objectInputStream.readObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            by3.b builder = by3.builder();
            int i = 0;
            int i2 = 0;
            while (i < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    iy3.a b2 = b(comparator);
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        b2.a(objectInputStream.readObject());
                    }
                    iy3 a2 = b2.a();
                    if (a2.size() == readInt2) {
                        builder.a(readObject, a2);
                        i2 += readInt2;
                        i++;
                    } else {
                        throw new InvalidObjectException("Duplicate key-value pairs exist for key " + readObject);
                    }
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                gy3.e.a.a(this, builder.a());
                gy3.e.b.a(this, i2);
                gy3.e.c.a(this, a(comparator));
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(valueComparator());
        wz3.a(this, objectOutputStream);
    }

    @DexIgnore
    public Comparator<? super V> valueComparator() {
        iy3<V> iy3 = this.f;
        if (iy3 instanceof ny3) {
            return ((ny3) iy3).comparator();
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends gy3.c<K, V> {
        @DexIgnore
        public a() {
            super(bz3.a().c().b());
        }

        @DexIgnore
        @Override // com.fossil.gy3.c
        @CanIgnoreReturnValue
        public a<K, V> a(K k, V v) {
            zy3<K, V> zy3 = ((gy3.c) this).a;
            jw3.a(k);
            jw3.a(v);
            zy3.put(k, v);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.zy3<K, V> */
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.gy3.c
        @CanIgnoreReturnValue
        public a<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            Object key = entry.getKey();
            jw3.a(key);
            Object value = entry.getValue();
            jw3.a(value);
            ((gy3.c) this).a.put(key, value);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.gy3.c
        @CanIgnoreReturnValue
        public a<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a((Iterable) iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.gy3.c
        public jy3<K, V> a() {
            if (((gy3.c) this).b != null) {
                xz3<K, V> b = bz3.a().c().b();
                for (Map.Entry entry : jz3.from(((gy3.c) this).b).onKeys().immutableSortedCopy(((gy3.c) this).a.asMap().entrySet())) {
                    b.putAll((K) entry.getKey(), (Iterable) entry.getValue());
                }
                ((gy3.c) this).a = b;
            }
            return jy3.a(((gy3.c) this).a, ((gy3.c) this).c);
        }
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        a aVar = new a();
        aVar.a((Iterable) iterable);
        return aVar.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> of(K k, V v) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public jy3<V, K> inverse() {
        jy3<V, K> jy3 = this.g;
        if (jy3 != null) {
            return jy3;
        }
        jy3<V, K> a2 = a();
        this.g = a2;
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3, com.fossil.vw3, com.fossil.zy3
    public iy3<Map.Entry<K, V>> entries() {
        iy3<Map.Entry<K, V>> iy3 = this.h;
        if (iy3 != null) {
            return iy3;
        }
        b bVar = new b(this);
        this.h = bVar;
        return bVar;
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3, com.fossil.zy3
    public iy3<V> get(K k) {
        return (iy3) fw3.a((iy3) ((gy3) this).map.get(k), this.f);
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3
    @CanIgnoreReturnValue
    @Deprecated
    public iy3<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3, com.fossil.vw3
    @CanIgnoreReturnValue
    @Deprecated
    public iy3<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> of(K k, V v, K k2, V v2) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        builder.a((Object) k4, (Object) v4);
        return builder.a();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.jy3<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.jy3$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final jy3<V, K> a() {
        a builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.a(entry.getValue(), entry.getKey());
        }
        jy3<V, K> a2 = builder.a();
        a2.g = this;
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> jy3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        builder.a((Object) k4, (Object) v4);
        builder.a((Object) k5, (Object) v5);
        return builder.a();
    }

    @DexIgnore
    public static <V> iy3<V> a(Comparator<? super V> comparator, Collection<? extends V> collection) {
        return comparator == null ? iy3.copyOf((Collection) collection) : ny3.copyOf((Comparator) comparator, (Collection) collection);
    }

    @DexIgnore
    public static <V> iy3<V> a(Comparator<? super V> comparator) {
        return comparator == null ? iy3.of() : ny3.emptySet(comparator);
    }
}
