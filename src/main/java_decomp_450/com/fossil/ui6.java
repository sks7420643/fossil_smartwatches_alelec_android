package com.fossil;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.am5;
import com.fossil.fl4;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui6 extends el4 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ b s; // = new b(null);
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ MutableLiveData<dx6<e>> g;
    @DexIgnore
    public /* final */ LiveData<dx6<e>> h;
    @DexIgnore
    public c i; // = new c(0, 0, 0, 0, 0, false, 63, null);
    @DexIgnore
    public c j; // = new c(0, 0, 0, 0, 0, false, 63, null);
    @DexIgnore
    public /* final */ MutableLiveData<c> k;
    @DexIgnore
    public /* final */ LiveData<c> l;
    @DexIgnore
    public /* final */ SummariesRepository m;
    @DexIgnore
    public /* final */ SleepSummariesRepository n;
    @DexIgnore
    public /* final */ GoalTrackingRepository o;
    @DexIgnore
    public /* final */ UserRepository p;
    @DexIgnore
    public /* final */ am5 q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1", f = "ProfileGoalEditViewModel.kt", l = {61}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ui6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$a$a")
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1", f = "ProfileGoalEditViewModel.kt", l = {82, 83, 84, 85, 86, 87}, m = "invokeSuspend")
        /* renamed from: com.fossil.ui6$a$a  reason: collision with other inner class name */
        public static final class C0197a extends zb7 implements kd7<yi7, fb7<? super c>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public int I$4;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$a$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$activitySettings$1", f = "ProfileGoalEditViewModel.kt", l = {63}, m = "invokeSuspend")
            /* renamed from: com.fossil.ui6$a$a$a  reason: collision with other inner class name */
            public static final class C0198a extends zb7 implements kd7<yi7, fb7<? super ActivitySettings>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0197a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0198a(C0197a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0198a aVar = new C0198a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ActivitySettings> fb7) {
                    return ((C0198a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        SummariesRepository e = this.this$0.this$0.this$0.m;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = e.getActivitySettings(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ActivitySettings activitySettings = (ActivitySettings) obj;
                    return activitySettings != null ? activitySettings : new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$a$a$b")
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$goalRingEnabled$1", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ui6$a$a$b */
            public static final class b extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0197a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(C0197a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Boolean a;
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        UserSettings userSetting = this.this$0.this$0.this$0.p.getUserSetting();
                        return pb7.a((userSetting == null || (a = pb7.a(userSetting.isShowGoalRing())) == null) ? true : a.booleanValue());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$a$a$c")
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$goalTracking$1", f = "ProfileGoalEditViewModel.kt", l = {73}, m = "invokeSuspend")
            /* renamed from: com.fossil.ui6$a$a$c */
            public static final class c extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0197a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(C0197a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    c cVar = new c(this.this$0, fb7);
                    cVar.p$ = (yi7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                    return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        GoalTrackingRepository b = this.this$0.this$0.this$0.o;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = b.getLastGoalSettings(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Integer num = (Integer) obj;
                    return pb7.a(num != null ? num.intValue() : 8);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$a$a$d")
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$sleepGoal$1", f = "ProfileGoalEditViewModel.kt", l = {68}, m = "invokeSuspend")
            /* renamed from: com.fossil.ui6$a$a$d */
            public static final class d extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0197a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(C0197a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    d dVar = new d(this.this$0, fb7);
                    dVar.p$ = (yi7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                    return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        SleepSummariesRepository d = this.this$0.this$0.this$0.n;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = d.getLastSleepGoal(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Integer num = (Integer) obj;
                    return pb7.a(num != null ? num.intValue() : 480);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0197a(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0197a aVar = new C0197a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super c> fb7) {
                return ((C0197a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0145 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0146  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x016a A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x016b  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0190 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0191  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x01b9 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x01ba  */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x01e3 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x01e4  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r17) {
                /*
                    r16 = this;
                    r0 = r16
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    switch(r2) {
                        case 0: goto L_0x00e7;
                        case 1: goto L_0x00cd;
                        case 2: goto L_0x00aa;
                        case 3: goto L_0x0087;
                        case 4: goto L_0x0060;
                        case 5: goto L_0x003d;
                        case 6: goto L_0x0013;
                        default: goto L_0x000b;
                    }
                L_0x000b:
                    java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                    java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                    r1.<init>(r2)
                    throw r1
                L_0x0013:
                    int r1 = r0.I$4
                    int r2 = r0.I$3
                    int r3 = r0.I$2
                    int r4 = r0.I$1
                    int r5 = r0.I$0
                    java.lang.Object r6 = r0.L$4
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r6 = r0.L$3
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r6 = r0.L$2
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r6 = r0.L$1
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r6 = r0.L$0
                    com.fossil.yi7 r6 = (com.fossil.yi7) r6
                    com.fossil.t87.a(r17)
                    r6 = r17
                    r12 = r1
                    r11 = r2
                    r10 = r3
                    r9 = r4
                    r8 = r5
                    goto L_0x01e9
                L_0x003d:
                    int r2 = r0.I$3
                    int r3 = r0.I$2
                    int r4 = r0.I$1
                    int r5 = r0.I$0
                    java.lang.Object r6 = r0.L$4
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r7 = r0.L$3
                    com.fossil.hj7 r7 = (com.fossil.hj7) r7
                    java.lang.Object r8 = r0.L$2
                    com.fossil.hj7 r8 = (com.fossil.hj7) r8
                    java.lang.Object r9 = r0.L$1
                    com.fossil.hj7 r9 = (com.fossil.hj7) r9
                    java.lang.Object r10 = r0.L$0
                    com.fossil.yi7 r10 = (com.fossil.yi7) r10
                    com.fossil.t87.a(r17)
                    r11 = r17
                    goto L_0x01c0
                L_0x0060:
                    int r2 = r0.I$2
                    int r3 = r0.I$1
                    int r4 = r0.I$0
                    java.lang.Object r5 = r0.L$4
                    com.fossil.hj7 r5 = (com.fossil.hj7) r5
                    java.lang.Object r6 = r0.L$3
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r7 = r0.L$2
                    com.fossil.hj7 r7 = (com.fossil.hj7) r7
                    java.lang.Object r8 = r0.L$1
                    com.fossil.hj7 r8 = (com.fossil.hj7) r8
                    java.lang.Object r9 = r0.L$0
                    com.fossil.yi7 r9 = (com.fossil.yi7) r9
                    com.fossil.t87.a(r17)
                    r10 = r17
                    r14 = r6
                    r6 = r5
                    r5 = r9
                    r9 = r8
                    r8 = r7
                    r7 = r14
                    goto L_0x0198
                L_0x0087:
                    int r2 = r0.I$1
                    int r3 = r0.I$0
                    java.lang.Object r4 = r0.L$4
                    com.fossil.hj7 r4 = (com.fossil.hj7) r4
                    java.lang.Object r5 = r0.L$3
                    com.fossil.hj7 r5 = (com.fossil.hj7) r5
                    java.lang.Object r6 = r0.L$2
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r7 = r0.L$1
                    com.fossil.hj7 r7 = (com.fossil.hj7) r7
                    java.lang.Object r8 = r0.L$0
                    com.fossil.yi7 r8 = (com.fossil.yi7) r8
                    com.fossil.t87.a(r17)
                    r9 = r17
                    r14 = r3
                    r3 = r2
                    r2 = r4
                    r4 = r14
                    goto L_0x0171
                L_0x00aa:
                    int r2 = r0.I$0
                    java.lang.Object r3 = r0.L$4
                    com.fossil.hj7 r3 = (com.fossil.hj7) r3
                    java.lang.Object r4 = r0.L$3
                    com.fossil.hj7 r4 = (com.fossil.hj7) r4
                    java.lang.Object r5 = r0.L$2
                    com.fossil.hj7 r5 = (com.fossil.hj7) r5
                    java.lang.Object r6 = r0.L$1
                    com.fossil.hj7 r6 = (com.fossil.hj7) r6
                    java.lang.Object r7 = r0.L$0
                    com.fossil.yi7 r7 = (com.fossil.yi7) r7
                    com.fossil.t87.a(r17)
                    r8 = r17
                    r14 = r4
                    r4 = r3
                    r3 = r7
                    r7 = r6
                    r6 = r5
                    r5 = r14
                    goto L_0x014d
                L_0x00cd:
                    java.lang.Object r2 = r0.L$4
                    com.fossil.hj7 r2 = (com.fossil.hj7) r2
                    java.lang.Object r3 = r0.L$3
                    com.fossil.hj7 r3 = (com.fossil.hj7) r3
                    java.lang.Object r4 = r0.L$2
                    com.fossil.hj7 r4 = (com.fossil.hj7) r4
                    java.lang.Object r5 = r0.L$1
                    com.fossil.hj7 r5 = (com.fossil.hj7) r5
                    java.lang.Object r6 = r0.L$0
                    com.fossil.yi7 r6 = (com.fossil.yi7) r6
                    com.fossil.t87.a(r17)
                    r7 = r17
                    goto L_0x012a
                L_0x00e7:
                    com.fossil.t87.a(r17)
                    com.fossil.yi7 r6 = r0.p$
                    r8 = 0
                    r9 = 0
                    com.fossil.ui6$a$a$a r10 = new com.fossil.ui6$a$a$a
                    r2 = 0
                    r10.<init>(r0, r2)
                    r11 = 3
                    r12 = 0
                    r7 = r6
                    com.fossil.hj7 r5 = com.fossil.xh7.a(r7, r8, r9, r10, r11, r12)
                    com.fossil.ui6$a$a$d r10 = new com.fossil.ui6$a$a$d
                    r10.<init>(r0, r2)
                    com.fossil.hj7 r4 = com.fossil.xh7.a(r7, r8, r9, r10, r11, r12)
                    com.fossil.ui6$a$a$c r10 = new com.fossil.ui6$a$a$c
                    r10.<init>(r0, r2)
                    com.fossil.hj7 r3 = com.fossil.xh7.a(r7, r8, r9, r10, r11, r12)
                    com.fossil.ui6$a$a$b r10 = new com.fossil.ui6$a$a$b
                    r10.<init>(r0, r2)
                    com.fossil.hj7 r2 = com.fossil.xh7.a(r7, r8, r9, r10, r11, r12)
                    r0.L$0 = r6
                    r0.L$1 = r5
                    r0.L$2 = r4
                    r0.L$3 = r3
                    r0.L$4 = r2
                    r7 = 1
                    r0.label = r7
                    java.lang.Object r7 = r5.c(r0)
                    if (r7 != r1) goto L_0x012a
                    return r1
                L_0x012a:
                    com.portfolio.platform.data.model.room.fitness.ActivitySettings r7 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r7
                    int r7 = r7.getCurrentStepGoal()
                    r0.L$0 = r6
                    r0.L$1 = r5
                    r0.L$2 = r4
                    r0.L$3 = r3
                    r0.L$4 = r2
                    r0.I$0 = r7
                    r8 = 2
                    r0.label = r8
                    java.lang.Object r8 = r5.c(r0)
                    if (r8 != r1) goto L_0x0146
                    return r1
                L_0x0146:
                    r14 = r4
                    r4 = r2
                    r2 = r7
                    r7 = r5
                    r5 = r3
                    r3 = r6
                    r6 = r14
                L_0x014d:
                    com.portfolio.platform.data.model.room.fitness.ActivitySettings r8 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r8
                    int r8 = r8.getCurrentActiveTimeGoal()
                    r0.L$0 = r3
                    r0.L$1 = r7
                    r0.L$2 = r6
                    r0.L$3 = r5
                    r0.L$4 = r4
                    r0.I$0 = r2
                    r0.I$1 = r8
                    r9 = 3
                    r0.label = r9
                    java.lang.Object r9 = r7.c(r0)
                    if (r9 != r1) goto L_0x016b
                    return r1
                L_0x016b:
                    r14 = r4
                    r4 = r2
                    r2 = r14
                    r15 = r8
                    r8 = r3
                    r3 = r15
                L_0x0171:
                    com.portfolio.platform.data.model.room.fitness.ActivitySettings r9 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r9
                    int r9 = r9.getCurrentCaloriesGoal()
                    r0.L$0 = r8
                    r0.L$1 = r7
                    r0.L$2 = r6
                    r0.L$3 = r5
                    r0.L$4 = r2
                    r0.I$0 = r4
                    r0.I$1 = r3
                    r0.I$2 = r9
                    r10 = 4
                    r0.label = r10
                    java.lang.Object r10 = r6.c(r0)
                    if (r10 != r1) goto L_0x0191
                    return r1
                L_0x0191:
                    r14 = r6
                    r6 = r2
                    r2 = r9
                    r9 = r7
                    r7 = r5
                    r5 = r8
                    r8 = r14
                L_0x0198:
                    java.lang.Number r10 = (java.lang.Number) r10
                    int r10 = r10.intValue()
                    r0.L$0 = r5
                    r0.L$1 = r9
                    r0.L$2 = r8
                    r0.L$3 = r7
                    r0.L$4 = r6
                    r0.I$0 = r4
                    r0.I$1 = r3
                    r0.I$2 = r2
                    r0.I$3 = r10
                    r11 = 5
                    r0.label = r11
                    java.lang.Object r11 = r7.c(r0)
                    if (r11 != r1) goto L_0x01ba
                    return r1
                L_0x01ba:
                    r14 = r3
                    r3 = r2
                    r2 = r10
                    r10 = r5
                    r5 = r4
                    r4 = r14
                L_0x01c0:
                    java.lang.Number r11 = (java.lang.Number) r11
                    int r11 = r11.intValue()
                    r0.L$0 = r10
                    r0.L$1 = r9
                    r0.L$2 = r8
                    r0.L$3 = r7
                    r0.L$4 = r6
                    r0.I$0 = r5
                    r0.I$1 = r4
                    r0.I$2 = r3
                    r0.I$3 = r2
                    r0.I$4 = r11
                    r7 = 6
                    r0.label = r7
                    java.lang.Object r6 = r6.c(r0)
                    if (r6 != r1) goto L_0x01e4
                    return r1
                L_0x01e4:
                    r10 = r3
                    r9 = r4
                    r8 = r5
                    r12 = r11
                    r11 = r2
                L_0x01e9:
                    java.lang.Boolean r6 = (java.lang.Boolean) r6
                    boolean r13 = r6.booleanValue()
                    com.fossil.ui6$c r1 = new com.fossil.ui6$c
                    r7 = r1
                    r7.<init>(r8, r9, r10, r11, r12, r13)
                    return r1
                    switch-data {0->0x00e7, 1->0x00cd, 2->0x00aa, 3->0x0087, 4->0x0060, 5->0x003d, 6->0x0013, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ui6.a.C0197a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ui6 ui6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ui6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            ui6 ui6;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                el4.a(this.this$0, true, false, 2, null);
                ui6 ui62 = this.this$0;
                ti7 b = qj7.b();
                C0197a aVar = new C0197a(this, null);
                this.L$0 = yi7;
                this.L$1 = ui62;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a) {
                    return a;
                }
                ui6 = ui62;
            } else if (i == 1) {
                ui6 = (ui6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ui6.j = (c) obj;
            ui6 ui63 = this.this$0;
            ui63.i = c.a(ui63.j, 0, 0, 0, 0, 0, false, 63, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ui6.s.a();
            local.d(a2, "init - mUiModel: " + this.this$0.i);
            this.this$0.e();
            el4.a(this.this$0, false, true, 1, null);
            this.this$0.h();
            this.this$0.q.d();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final String a() {
            return ui6.r;
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public c() {
            this(0, 0, 0, 0, 0, false, 63, null);
        }

        @DexIgnore
        public c(int i, int i2, int i3, int i4, int i5, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = z;
        }

        @DexIgnore
        public static /* synthetic */ c a(c cVar, int i, int i2, int i3, int i4, int i5, boolean z, int i6, Object obj) {
            if ((i6 & 1) != 0) {
                i = cVar.a;
            }
            if ((i6 & 2) != 0) {
                i2 = cVar.b;
            }
            if ((i6 & 4) != 0) {
                i3 = cVar.c;
            }
            if ((i6 & 8) != 0) {
                i4 = cVar.d;
            }
            if ((i6 & 16) != 0) {
                i5 = cVar.e;
            }
            if ((i6 & 32) != 0) {
                z = cVar.f;
            }
            return cVar.a(i, i2, i3, i4, i5, z);
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final c a(int i, int i2, int i3, int i4, int i5, boolean z) {
            return new c(i, i2, i3, i4, i5, z);
        }

        @DexIgnore
        public final void b(int i) {
            this.c = i;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }

        @DexIgnore
        public final void d(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void e(int i) {
            this.a = i;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.a == cVar.a && this.b == cVar.b && this.c == cVar.c && this.d == cVar.d && this.e == cVar.e && this.f == cVar.f;
        }

        @DexIgnore
        public final int f() {
            return this.d;
        }

        @DexIgnore
        public final int g() {
            return this.a;
        }

        @DexIgnore
        public int hashCode() {
            int i = ((((((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e) * 31;
            boolean z = this.f;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return i + i2;
        }

        @DexIgnore
        public String toString() {
            return "UiModelWrapper(stepGoal=" + this.a + ", activeTimeGoal=" + this.b + ", caloriesGoal=" + this.c + ", sleepGoal=" + this.d + ", goalTracking=" + this.e + ", goalRingEnabled=" + this.f + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, int i2, int i3, int i4, int i5, boolean z, int i6, zd7 zd7) {
            this((i6 & 1) != 0 ? 0 : i, (i6 & 2) != 0 ? 0 : i2, (i6 & 4) != 0 ? 0 : i3, (i6 & 8) != 0 ? 0 : i4, (i6 & 16) == 0 ? i5 : 0, (i6 & 32) != 0 ? true : z);
        }

        @DexIgnore
        public final void a(int i) {
            this.b = i;
        }

        @DexIgnore
        public final ActivitySettings b() {
            return new ActivitySettings(this.a, this.c, this.b);
        }

        @DexIgnore
        public final void c(int i) {
            this.e = i;
        }

        @DexIgnore
        public final boolean d() {
            return this.f;
        }

        @DexIgnore
        public final int e() {
            return this.e;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.f = z;
        }

        @DexIgnore
        public final v87<Boolean, Boolean, Boolean> a(c cVar) {
            ee7.b(cVar, "that");
            boolean z = true;
            boolean z2 = !ee7.a(b(), cVar.b());
            boolean z3 = this.d != cVar.d;
            if (this.e == cVar.e) {
                z = false;
            }
            return new v87<>(Boolean.valueOf(z2), Boolean.valueOf(z3), Boolean.valueOf(z));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Throwable {
        @DexIgnore
        public /* final */ int errorCode;
        @DexIgnore
        public /* final */ String errorMessage;

        @DexIgnore
        public d(int i, String str) {
            super(str);
            this.errorCode = i;
            this.errorMessage = str;
        }

        @DexIgnore
        public final int getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getErrorMessage() {
            return this.errorMessage;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends e {
            @DexIgnore
            public static /* final */ a a; // = new a();

            @DexIgnore
            public a() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends e {
            @DexIgnore
            public static /* final */ b a; // = new b();

            @DexIgnore
            public b() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends e {
            @DexIgnore
            public static /* final */ c a; // = new c();

            @DexIgnore
            public c() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends e {
            @DexIgnore
            public /* final */ int a;
            @DexIgnore
            public /* final */ String b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(int i, String str) {
                super(null);
                ee7.b(str, "message");
                this.a = i;
                this.b = str;
            }

            @DexIgnore
            public final int a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof d)) {
                    return false;
                }
                d dVar = (d) obj;
                return this.a == dVar.a && ee7.a(this.b, dVar.b);
            }

            @DexIgnore
            public int hashCode() {
                int i = this.a * 31;
                String str = this.b;
                return i + (str != null ? str.hashCode() : 0);
            }

            @DexIgnore
            public String toString() {
                return "ShowErrorDialog(code=" + this.a + ", message=" + this.b + ")";
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$e$e")
        /* renamed from: com.fossil.ui6$e$e  reason: collision with other inner class name */
        public static final class C0199e extends e {
            @DexIgnore
            public /* final */ List<ib5> a;

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.ib5> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0199e(List<? extends ib5> list) {
                super(null);
                ee7.b(list, "permissionCodes");
                this.a = list;
            }

            @DexIgnore
            public final List<ib5> a() {
                return this.a;
            }

            @DexIgnore
            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0199e) && ee7.a(this.a, ((C0199e) obj).a);
                }
                return true;
            }

            @DexIgnore
            public int hashCode() {
                List<ib5> list = this.a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            @DexIgnore
            public String toString() {
                return "ShowPermissionPopups(permissionCodes=" + this.a + ")";
            }
        }

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public /* synthetic */ e(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$checkUserUsingDefaultGoal$2", f = "ProfileGoalEditViewModel.kt", l = {267, 270}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ui6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ui6 ui6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ui6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                UserRepository g = this.this$0.p;
                this.L$0 = yi7;
                this.label = 1;
                obj = g.getCurrentUser(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                MFUser mFUser = (MFUser) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser2 = (MFUser) obj;
            if (mFUser2 == null) {
                return null;
            }
            if (mFUser2.getUseDefaultGoals()) {
                mFUser2.setUseDefaultGoals(false);
                UserRepository g2 = this.this$0.p;
                this.L$0 = yi7;
                this.L$1 = mFUser2;
                this.label = 2;
                if (g2.updateUser(mFUser2, true, this) == a) {
                    return a;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1", f = "ProfileGoalEditViewModel.kt", l = {317, 318, 319, 135, 136, 137, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ui6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$1", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super hj7<? extends zi5<? extends ActivitySettings>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isActivityGoalChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ui6$g$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$1$1", f = "ProfileGoalEditViewModel.kt", l = {318}, m = "invokeSuspend")
            /* renamed from: com.fossil.ui6$g$a$a  reason: collision with other inner class name */
            public static final class C0200a extends zb7 implements kd7<yi7, fb7<? super zi5<? extends ActivitySettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0200a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0200a aVar = new C0200a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super zi5<? extends ActivitySettings>> fb7) {
                    return ((C0200a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        if (!this.this$0.$changed) {
                            return new bj5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ui6.s.a();
                        local.d(a2, "onSave - isActivityGoalChanged = " + this.this$0.$isActivityGoalChanged$inlined);
                        SummariesRepository e = this.this$0.this$0.this$0.m;
                        ActivitySettings b = this.this$0.this$0.this$0.i.b();
                        this.L$0 = yi7;
                        this.L$1 = this;
                        this.label = 1;
                        obj = e.updateActivitySettings(b, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        fb7 fb7 = (fb7) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (zi5) obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(boolean z, fb7 fb7, g gVar, boolean z2) {
                super(2, fb7);
                this.$changed = z;
                this.this$0 = gVar;
                this.$isActivityGoalChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$changed, fb7, this.this$0, this.$isActivityGoalChanged$inlined);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super hj7<? extends zi5<? extends ActivitySettings>>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return xh7.a(this.p$, null, null, new C0200a(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$2", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super hj7<? extends zi5<? extends MFSleepSettings>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isSleepGoalChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$2$1", f = "ProfileGoalEditViewModel.kt", l = {318}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFSleepSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFSleepSettings>> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        if (!this.this$0.$changed) {
                            return new bj5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ui6.s.a();
                        local.d(a2, "onSave - isSleepGoalChanged = " + this.this$0.$isSleepGoalChanged$inlined);
                        SleepSummariesRepository d = this.this$0.this$0.this$0.n;
                        int f = this.this$0.this$0.this$0.i.f();
                        this.L$0 = yi7;
                        this.L$1 = this;
                        this.label = 1;
                        obj = d.updateLastSleepGoal(f, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        fb7 fb7 = (fb7) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (zi5) obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(boolean z, fb7 fb7, g gVar, boolean z2) {
                super(2, fb7);
                this.$changed = z;
                this.this$0 = gVar;
                this.$isSleepGoalChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.$changed, fb7, this.this$0, this.$isSleepGoalChanged$inlined);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super hj7<? extends zi5<? extends MFSleepSettings>>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return xh7.a(this.p$, null, null, new a(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$3", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super hj7<? extends zi5<? extends GoalSetting>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isGoalTrackingChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$3$1", f = "ProfileGoalEditViewModel.kt", l = {318}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super zi5<? extends GoalSetting>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = cVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super zi5<? extends GoalSetting>> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        if (!this.this$0.$changed) {
                            return new bj5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ui6.s.a();
                        local.d(a2, "onSave - isGoalTrackingChanged = " + this.this$0.$isGoalTrackingChanged$inlined);
                        GoalTrackingRepository b = this.this$0.this$0.this$0.o;
                        GoalSetting goalSetting = new GoalSetting(this.this$0.this$0.this$0.i.e());
                        this.L$0 = yi7;
                        this.L$1 = this;
                        this.label = 1;
                        obj = b.updateGoalSetting(goalSetting, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        fb7 fb7 = (fb7) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (zi5) obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(boolean z, fb7 fb7, g gVar, boolean z2) {
                super(2, fb7);
                this.$changed = z;
                this.this$0 = gVar;
                this.$isGoalTrackingChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.$changed, fb7, this.this$0, this.$isGoalTrackingChanged$inlined);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super hj7<? extends zi5<? extends GoalSetting>>> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return xh7.a(this.p$, null, null, new a(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ui6 ui6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ui6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0150 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0151  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0179 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x017a  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x01a0 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x01a9  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01cd  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x01f2  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x01f7  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 2
                r3 = 0
                r4 = 0
                r5 = 1
                switch(r1) {
                    case 0: goto L_0x00d3;
                    case 1: goto L_0x00c1;
                    case 2: goto L_0x00aa;
                    case 3: goto L_0x0089;
                    case 4: goto L_0x006a;
                    case 5: goto L_0x004b;
                    case 6: goto L_0x002c;
                    case 7: goto L_0x0015;
                    default: goto L_0x000d;
                }
            L_0x000d:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0015:
                java.lang.Object r0 = r14.L$3
                com.fossil.hj7 r0 = (com.fossil.hj7) r0
                java.lang.Object r0 = r14.L$2
                com.fossil.hj7 r0 = (com.fossil.hj7) r0
                java.lang.Object r0 = r14.L$1
                com.fossil.hj7 r0 = (com.fossil.hj7) r0
                boolean r0 = r14.Z$0
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)     // Catch:{ d -> 0x024d }
                goto L_0x0212
            L_0x002c:
                java.lang.Object r1 = r14.L$4
                com.fossil.ui6 r1 = (com.fossil.ui6) r1
                java.lang.Object r2 = r14.L$3
                com.fossil.hj7 r2 = (com.fossil.hj7) r2
                java.lang.Object r6 = r14.L$2
                com.fossil.hj7 r6 = (com.fossil.hj7) r6
                java.lang.Object r7 = r14.L$1
                com.fossil.hj7 r7 = (com.fossil.hj7) r7
                boolean r8 = r14.Z$2
                boolean r9 = r14.Z$1
                boolean r10 = r14.Z$0
                java.lang.Object r11 = r14.L$0
                com.fossil.yi7 r11 = (com.fossil.yi7) r11
                com.fossil.t87.a(r15)
                goto L_0x01e9
            L_0x004b:
                java.lang.Object r1 = r14.L$4
                com.fossil.ui6 r1 = (com.fossil.ui6) r1
                java.lang.Object r2 = r14.L$3
                com.fossil.hj7 r2 = (com.fossil.hj7) r2
                java.lang.Object r6 = r14.L$2
                com.fossil.hj7 r6 = (com.fossil.hj7) r6
                java.lang.Object r7 = r14.L$1
                com.fossil.hj7 r7 = (com.fossil.hj7) r7
                boolean r8 = r14.Z$2
                boolean r9 = r14.Z$1
                boolean r10 = r14.Z$0
                java.lang.Object r11 = r14.L$0
                com.fossil.yi7 r11 = (com.fossil.yi7) r11
                com.fossil.t87.a(r15)
                goto L_0x01c5
            L_0x006a:
                java.lang.Object r1 = r14.L$4
                com.fossil.ui6 r1 = (com.fossil.ui6) r1
                java.lang.Object r2 = r14.L$3
                com.fossil.hj7 r2 = (com.fossil.hj7) r2
                java.lang.Object r6 = r14.L$2
                com.fossil.hj7 r6 = (com.fossil.hj7) r6
                java.lang.Object r7 = r14.L$1
                com.fossil.hj7 r7 = (com.fossil.hj7) r7
                boolean r8 = r14.Z$2
                boolean r9 = r14.Z$1
                boolean r10 = r14.Z$0
                java.lang.Object r11 = r14.L$0
                com.fossil.yi7 r11 = (com.fossil.yi7) r11
                com.fossil.t87.a(r15)
                goto L_0x01a1
            L_0x0089:
                java.lang.Object r1 = r14.L$3
                com.fossil.ui6 r1 = (com.fossil.ui6) r1
                java.lang.Object r1 = r14.L$2
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                java.lang.Object r2 = r14.L$1
                com.fossil.hj7 r2 = (com.fossil.hj7) r2
                boolean r6 = r14.Z$2
                boolean r7 = r14.Z$1
                boolean r8 = r14.Z$0
                java.lang.Object r9 = r14.L$0
                com.fossil.yi7 r9 = (com.fossil.yi7) r9
                com.fossil.t87.a(r15)
                r10 = r8
                r11 = r9
                r8 = r6
                r9 = r7
                r6 = r1
                r7 = r2
                goto L_0x0182
            L_0x00aa:
                java.lang.Object r1 = r14.L$2
                com.fossil.ui6 r1 = (com.fossil.ui6) r1
                java.lang.Object r1 = r14.L$1
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                boolean r2 = r14.Z$2
                boolean r6 = r14.Z$1
                boolean r7 = r14.Z$0
                java.lang.Object r8 = r14.L$0
                com.fossil.yi7 r8 = (com.fossil.yi7) r8
                com.fossil.t87.a(r15)
                goto L_0x0155
            L_0x00c1:
                java.lang.Object r1 = r14.L$1
                com.fossil.ui6 r1 = (com.fossil.ui6) r1
                boolean r1 = r14.Z$2
                boolean r6 = r14.Z$1
                boolean r7 = r14.Z$0
                java.lang.Object r8 = r14.L$0
                com.fossil.yi7 r8 = (com.fossil.yi7) r8
                com.fossil.t87.a(r15)
                goto L_0x012f
            L_0x00d3:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r15 = r14.p$
                com.fossil.ui6 r1 = r14.this$0
                com.fossil.el4.a(r1, r5, r3, r2, r4)
                com.fossil.ui6 r1 = r14.this$0
                com.fossil.ui6$c r1 = r1.i
                com.fossil.ui6 r6 = r14.this$0
                com.fossil.ui6$c r6 = r6.j
                com.fossil.v87 r1 = r1.a(r6)
                java.lang.Object r6 = r1.component1()
                java.lang.Boolean r6 = (java.lang.Boolean) r6
                boolean r6 = r6.booleanValue()
                java.lang.Object r7 = r1.component2()
                java.lang.Boolean r7 = (java.lang.Boolean) r7
                boolean r7 = r7.booleanValue()
                java.lang.Object r1 = r1.component3()
                java.lang.Boolean r1 = (java.lang.Boolean) r1
                boolean r1 = r1.booleanValue()
                com.fossil.ui6 r8 = r14.this$0
                com.fossil.ti7 r9 = com.fossil.qj7.b()
                com.fossil.ui6$g$a r10 = new com.fossil.ui6$g$a
                r10.<init>(r6, r4, r14, r6)
                r14.L$0 = r15
                r14.Z$0 = r6
                r14.Z$1 = r7
                r14.Z$2 = r1
                r14.L$1 = r8
                r14.label = r5
                java.lang.Object r8 = com.fossil.vh7.a(r9, r10, r14)
                if (r8 != r0) goto L_0x0129
                return r0
            L_0x0129:
                r12 = r8
                r8 = r15
                r15 = r12
                r13 = r7
                r7 = r6
                r6 = r13
            L_0x012f:
                com.fossil.hj7 r15 = (com.fossil.hj7) r15
                com.fossil.ui6 r9 = r14.this$0
                com.fossil.ti7 r10 = com.fossil.qj7.b()
                com.fossil.ui6$g$b r11 = new com.fossil.ui6$g$b
                r11.<init>(r6, r4, r14, r6)
                r14.L$0 = r8
                r14.Z$0 = r7
                r14.Z$1 = r6
                r14.Z$2 = r1
                r14.L$1 = r15
                r14.L$2 = r9
                r14.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r10, r11, r14)
                if (r2 != r0) goto L_0x0151
                return r0
            L_0x0151:
                r12 = r1
                r1 = r15
                r15 = r2
                r2 = r12
            L_0x0155:
                com.fossil.hj7 r15 = (com.fossil.hj7) r15
                com.fossil.ui6 r9 = r14.this$0
                com.fossil.ti7 r10 = com.fossil.qj7.b()
                com.fossil.ui6$g$c r11 = new com.fossil.ui6$g$c
                r11.<init>(r2, r4, r14, r2)
                r14.L$0 = r8
                r14.Z$0 = r7
                r14.Z$1 = r6
                r14.Z$2 = r2
                r14.L$1 = r1
                r14.L$2 = r15
                r14.L$3 = r9
                r9 = 3
                r14.label = r9
                java.lang.Object r9 = com.fossil.vh7.a(r10, r11, r14)
                if (r9 != r0) goto L_0x017a
                return r0
            L_0x017a:
                r10 = r7
                r11 = r8
                r7 = r1
                r8 = r2
                r12 = r6
                r6 = r15
                r15 = r9
                r9 = r12
            L_0x0182:
                r2 = r15
                com.fossil.hj7 r2 = (com.fossil.hj7) r2
                com.fossil.ui6 r1 = r14.this$0
                r14.L$0 = r11
                r14.Z$0 = r10
                r14.Z$1 = r9
                r14.Z$2 = r8
                r14.L$1 = r7
                r14.L$2 = r6
                r14.L$3 = r2
                r14.L$4 = r1
                r15 = 4
                r14.label = r15
                java.lang.Object r15 = r7.c(r14)
                if (r15 != r0) goto L_0x01a1
                return r0
            L_0x01a1:
                com.fossil.zi5 r15 = (com.fossil.zi5) r15
                boolean r15 = r1.a(r15)
                if (r15 != 0) goto L_0x01f4
                com.fossil.ui6 r1 = r14.this$0
                r14.L$0 = r11
                r14.Z$0 = r10
                r14.Z$1 = r9
                r14.Z$2 = r8
                r14.L$1 = r7
                r14.L$2 = r6
                r14.L$3 = r2
                r14.L$4 = r1
                r15 = 5
                r14.label = r15
                java.lang.Object r15 = r6.c(r14)
                if (r15 != r0) goto L_0x01c5
                return r0
            L_0x01c5:
                com.fossil.zi5 r15 = (com.fossil.zi5) r15
                boolean r15 = r1.a(r15)
                if (r15 != 0) goto L_0x01f4
                com.fossil.ui6 r1 = r14.this$0
                r14.L$0 = r11
                r14.Z$0 = r10
                r14.Z$1 = r9
                r14.Z$2 = r8
                r14.L$1 = r7
                r14.L$2 = r6
                r14.L$3 = r2
                r14.L$4 = r1
                r15 = 6
                r14.label = r15
                java.lang.Object r15 = r2.c(r14)
                if (r15 != r0) goto L_0x01e9
                return r0
            L_0x01e9:
                com.fossil.zi5 r15 = (com.fossil.zi5) r15
                boolean r15 = r1.a(r15)
                if (r15 == 0) goto L_0x01f2
                goto L_0x01f4
            L_0x01f2:
                r15 = 0
                goto L_0x01f5
            L_0x01f4:
                r15 = 1
            L_0x01f5:
                if (r15 == 0) goto L_0x0290
                com.fossil.ui6 r15 = r14.this$0
                r14.L$0 = r11
                r14.Z$0 = r10
                r14.Z$1 = r9
                r14.Z$2 = r8
                r14.L$1 = r7
                r14.L$2 = r6
                r14.L$3 = r2
                r1 = 7
                r14.label = r1
                java.lang.Object r15 = r15.a(r14)
                if (r15 != r0) goto L_0x0211
                return r0
            L_0x0211:
                r0 = r10
            L_0x0212:
                if (r0 != 0) goto L_0x0236
                com.fossil.ui6 r15 = r14.this$0
                com.fossil.ui6$c r15 = r15.i
                boolean r15 = r15.d()
                com.fossil.ui6 r0 = r14.this$0
                com.fossil.ui6$c r0 = r0.j
                boolean r0 = r0.d()
                if (r15 == r0) goto L_0x022b
                goto L_0x0236
            L_0x022b:
                com.fossil.ui6 r15 = r14.this$0
                com.fossil.el4.a(r15, r3, r5, r5, r4)
                com.fossil.ui6 r15 = r14.this$0
                r15.m()
                goto L_0x0290
            L_0x0236:
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                com.fossil.ui6$b r0 = com.fossil.ui6.s
                java.lang.String r0 = r0.a()
                java.lang.String r1 = "onSave - run SetActivityGoalUserCase"
                r15.d(r0, r1)
                com.fossil.ui6 r15 = r14.this$0
                r15.n()
                goto L_0x0290
            L_0x024d:
                r15 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.ui6$b r1 = com.fossil.ui6.s
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "onSave - UpdateGoalError: "
                r2.append(r3)
                int r3 = r15.getErrorCode()
                r2.append(r3)
                java.lang.String r3 = " - "
                r2.append(r3)
                java.lang.String r3 = r15.getErrorMessage()
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                com.fossil.ui6 r0 = r14.this$0
                int r1 = r15.getErrorCode()
                java.lang.String r15 = r15.getErrorMessage()
                if (r15 == 0) goto L_0x028b
                goto L_0x028d
            L_0x028b:
                java.lang.String r15 = ""
            L_0x028d:
                r0.a(r1, r15)
            L_0x0290:
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
                switch-data {0->0x00d3, 1->0x00c1, 2->0x00aa, 3->0x0089, 4->0x006a, 5->0x004b, 6->0x002c, 7->0x0015, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ui6.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements fl4.e<am5.d, am5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ui6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(ui6 ui6) {
            this.a = ui6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(am5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ui6.s.a(), "executeSetActivityUseCase success");
            el4.a(this.a, false, true, 1, null);
            this.a.m();
        }

        @DexIgnore
        public void a(am5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ui6.s.a();
            local.d(a2, "executeSetActivityUseCase failed, errorCode=" + cVar.b());
            boolean z = false;
            el4.a(this.a, false, true, 1, null);
            ArrayList<Integer> a3 = cVar.a();
            if (a3 == null || a3.isEmpty()) {
                z = true;
            }
            if (!z) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(cVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026errorValue.bleErrorCodes)");
                this.a.a(convertBLEPermissionErrorCode);
                return;
            }
            ui6 ui6 = this.a;
            int b = cVar.b();
            String c = cVar.c();
            if (c == null) {
                c = "";
            }
            ui6.a(b, c);
        }
    }

    /*
    static {
        String simpleName = ui6.class.getSimpleName();
        ee7.a((Object) simpleName, "ProfileGoalEditViewModel::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public ui6(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, am5 am5) {
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(am5, "mSetActivityGoalUserCase");
        this.m = summariesRepository;
        this.n = sleepSummariesRepository;
        this.o = goalTrackingRepository;
        this.p = userRepository;
        this.q = am5;
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.g0.c().c());
        this.e = deviceBySerial;
        this.f = be5.o.a(deviceBySerial);
        MutableLiveData<dx6<e>> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        this.h = mutableLiveData;
        MutableLiveData<c> mutableLiveData2 = new MutableLiveData<>(this.i);
        this.k = mutableLiveData2;
        this.l = mutableLiveData2;
        ik7 unused = xh7.b(ie.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final void k() {
        if (j()) {
            o();
        } else {
            m();
        }
    }

    @DexIgnore
    public final void l() {
        ik7 unused = xh7.b(ie.a(this), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    public final void m() {
        this.g.a(new dx6<>(e.b.a));
    }

    @DexIgnore
    public final void n() {
        String c2 = PortfolioApp.g0.c().c();
        if (!TextUtils.isEmpty(c2)) {
            this.q.a(new am5.b(c2, this.i.b(), this.i.d()), new h(this));
        }
    }

    @DexIgnore
    public final void o() {
        this.g.a(new dx6<>(e.c.a));
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        this.q.e();
    }

    @DexIgnore
    public final void e() {
        this.k.a(this.i);
    }

    @DexIgnore
    public final LiveData<dx6<e>> f() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<c> g() {
        return this.l;
    }

    @DexIgnore
    public final void h() {
        this.g.a(new dx6<>(e.a.a));
    }

    @DexIgnore
    public final boolean i() {
        return this.f;
    }

    @DexIgnore
    public final boolean j() {
        return !ee7.a(this.i, this.j);
    }

    @DexIgnore
    public final void a(int i2, gb5 gb5) {
        ee7.b(gb5, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "setSettingValue value=" + i2 + " type=" + gb5);
        int i3 = vi6.a[gb5.ordinal()];
        if (i3 == 1) {
            this.i.e(i2);
        } else if (i3 == 2) {
            this.i.b(i2);
        } else if (i3 == 3) {
            this.i.a(i2);
        } else if (i3 == 4) {
            this.i.d(i2);
        } else if (i3 == 5) {
            this.i.c(i2);
        }
        e();
    }

    @DexIgnore
    public final void a(boolean z) {
        this.i.a(z);
        e();
    }

    @DexIgnore
    public final void a(int i2, String str) {
        this.g.a(new dx6<>(new e.d(i2, str)));
    }

    @DexIgnore
    public final void a(List<? extends ib5> list) {
        this.g.a(new dx6<>(new e.C0199e(list)));
    }

    @DexIgnore
    public final <T> boolean a(zi5<T> zi5) {
        String str;
        if (!(zi5 instanceof yi5)) {
            return true;
        }
        yi5 yi5 = (yi5) zi5;
        int a2 = yi5.a();
        ServerError c2 = yi5.c();
        if (c2 == null || (str = c2.getMessage()) == null) {
            str = "";
        }
        throw new d(a2, str);
    }

    @DexIgnore
    public final /* synthetic */ Object a(fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new f(this, null), fb7);
        if (a2 == nb7.a()) {
            return a2;
        }
        return i97.a;
    }
}
