package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ux4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ s85 s;
    @DexIgnore
    public /* final */ s85 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public ux4(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, s85 s85, s85 s852, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = s85;
        a((ViewDataBinding) s85);
        this.t = s852;
        a((ViewDataBinding) s852);
        this.u = linearLayout;
    }
}
