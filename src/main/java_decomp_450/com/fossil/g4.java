package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.fossil.m4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g4 extends i4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements m4.a {
        @DexIgnore
        public a(g4 g4Var) {
        }

        @DexIgnore
        @Override // com.fossil.m4.a
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @DexIgnore
    @Override // com.fossil.k4, com.fossil.i4
    public void a() {
        m4.r = new a(this);
    }
}
