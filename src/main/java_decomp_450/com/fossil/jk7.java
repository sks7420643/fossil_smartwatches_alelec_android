package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jk7 extends CancellationException implements ri7<jk7> {
    @DexIgnore
    public /* final */ ik7 job;

    @DexIgnore
    public jk7(String str, Throwable th, ik7 ik7) {
        super(str);
        this.job = ik7;
        if (th != null) {
            initCause(th);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof jk7) {
                jk7 jk7 = (jk7) obj;
                if (!ee7.a((Object) jk7.getMessage(), (Object) getMessage()) || !ee7.a(jk7.job, this.job) || !ee7.a(jk7.getCause(), getCause())) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public Throwable fillInStackTrace() {
        return dj7.c() ? super.fillInStackTrace() : this;
    }

    @DexIgnore
    public int hashCode() {
        String message = getMessage();
        if (message != null) {
            int hashCode = ((message.hashCode() * 31) + this.job.hashCode()) * 31;
            Throwable cause = getCause();
            return hashCode + (cause != null ? cause.hashCode() : 0);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "; job=" + this.job;
    }

    @DexIgnore
    @Override // com.fossil.ri7
    public jk7 createCopy() {
        if (!dj7.c()) {
            return null;
        }
        String message = getMessage();
        if (message != null) {
            return new jk7(message, this, this.job);
        }
        ee7.a();
        throw null;
    }
}
