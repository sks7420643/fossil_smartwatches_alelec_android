package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np2 extends bw2<np2, a> implements lx2 {
    @DexIgnore
    public static /* final */ np2 zzf;
    @DexIgnore
    public static volatile wx2<np2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<np2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(np2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(jp2 jp2) {
            this();
        }
    }

    /*
    static {
        np2 np2 = new np2();
        zzf = np2;
        bw2.a(np2.class, np2);
    }
    */

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (jp2.a[i - 1]) {
            case 1:
                return new np2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1008\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                wx2<np2> wx2 = zzg;
                if (wx2 == null) {
                    synchronized (np2.class) {
                        wx2 = zzg;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzf);
                            zzg = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final String p() {
        return this.zze;
    }

    @DexIgnore
    public final String zza() {
        return this.zzd;
    }
}
