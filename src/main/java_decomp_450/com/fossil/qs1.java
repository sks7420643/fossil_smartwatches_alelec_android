package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs1 extends wi1<byte[], byte[]> {
    @DexIgnore
    public static /* final */ y71<byte[]>[] b; // = {new ro1(), new rq1()};
    @DexIgnore
    public static /* final */ uk1<byte[]>[] c; // = new uk1[0];
    @DexIgnore
    public static /* final */ qs1 d; // = new qs1();

    @DexIgnore
    public final byte[] b(byte[] bArr) {
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<byte[]>[] b() {
        return c;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<byte[]>[] a() {
        return b;
    }
}
