package com.fossil;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey2 {
    @DexIgnore
    public static /* final */ Class<?> a; // = d();
    @DexIgnore
    public static /* final */ uy2<?, ?> b; // = a(false);
    @DexIgnore
    public static /* final */ uy2<?, ?> c; // = a(true);
    @DexIgnore
    public static /* final */ uy2<?, ?> d; // = new wy2();

    @DexIgnore
    public static void a(Class<?> cls) {
        Class<?> cls2;
        if (!bw2.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    @DexIgnore
    public static void b(int i, List<Float> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzf(i, list, z);
        }
    }

    @DexIgnore
    public static void c(int i, List<Long> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzc(i, list, z);
        }
    }

    @DexIgnore
    public static void d(int i, List<Long> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzd(i, list, z);
        }
    }

    @DexIgnore
    public static void e(int i, List<Long> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzn(i, list, z);
        }
    }

    @DexIgnore
    public static void f(int i, List<Long> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zze(i, list, z);
        }
    }

    @DexIgnore
    public static void g(int i, List<Long> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzl(i, list, z);
        }
    }

    @DexIgnore
    public static void h(int i, List<Integer> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zza(i, list, z);
        }
    }

    @DexIgnore
    public static void i(int i, List<Integer> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzj(i, list, z);
        }
    }

    @DexIgnore
    public static void j(int i, List<Integer> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzm(i, list, z);
        }
    }

    @DexIgnore
    public static void k(int i, List<Integer> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzb(i, list, z);
        }
    }

    @DexIgnore
    public static void l(int i, List<Integer> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzk(i, list, z);
        }
    }

    @DexIgnore
    public static void m(int i, List<Integer> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzh(i, list, z);
        }
    }

    @DexIgnore
    public static void n(int i, List<Boolean> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzi(i, list, z);
        }
    }

    @DexIgnore
    public static void b(int i, List<tu2> list, oz2 oz2) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzb(i, list);
        }
    }

    @DexIgnore
    public static int c(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof ww2) {
            ww2 ww2 = (ww2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.f(ww2.zzb(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof cw2) {
            cw2 cw2 = (cw2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.k(cw2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.k(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof cw2) {
            cw2 cw2 = (cw2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.f(cw2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.f(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int f(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof cw2) {
            cw2 cw2 = (cw2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.g(cw2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.g(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int g(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof cw2) {
            cw2 cw2 = (cw2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.h(cw2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.h(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int h(List<?> list) {
        return list.size() << 2;
    }

    @DexIgnore
    public static int i(List<?> list) {
        return list.size() << 3;
    }

    @DexIgnore
    public static int j(List<?> list) {
        return list.size();
    }

    @DexIgnore
    public static void a(int i, List<Double> list, oz2 oz2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zzg(i, list, z);
        }
    }

    @DexIgnore
    public static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * iv2.i(i, 0);
    }

    @DexIgnore
    public static int i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * iv2.g(i, 0L);
    }

    @DexIgnore
    public static int j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * iv2.b(i, true);
    }

    @DexIgnore
    public static void b(int i, List<?> list, oz2 oz2, cy2 cy2) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.a(i, list, cy2);
        }
    }

    @DexIgnore
    public static void a(int i, List<String> list, oz2 oz2) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.zza(i, list);
        }
    }

    @DexIgnore
    public static int b(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof ww2) {
            ww2 ww2 = (ww2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.e(ww2.zzb(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static void a(int i, List<?> list, oz2 oz2, cy2 cy2) throws IOException {
        if (list != null && !list.isEmpty()) {
            oz2.b(i, list, cy2);
        }
    }

    @DexIgnore
    public static int c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return c(list) + (size * iv2.e(i));
    }

    @DexIgnore
    public static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return d(list) + (size * iv2.e(i));
    }

    @DexIgnore
    public static int e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return e(list) + (size * iv2.e(i));
    }

    @DexIgnore
    public static int f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return f(list) + (size * iv2.e(i));
    }

    @DexIgnore
    public static int g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return g(list) + (size * iv2.e(i));
    }

    @DexIgnore
    public static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof ww2) {
            ww2 ww2 = (ww2) list;
            i = 0;
            while (i2 < size) {
                i += iv2.d(ww2.zzb(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + iv2.d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static uy2<?, ?> c() {
        return d;
    }

    @DexIgnore
    public static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static int b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return b(list) + (size * iv2.e(i));
    }

    @DexIgnore
    public static int a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return a(list) + (list.size() * iv2.e(i));
    }

    @DexIgnore
    public static int b(int i, List<tu2> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * iv2.e(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            e += iv2.b(list.get(i2));
        }
        return e;
    }

    @DexIgnore
    public static int a(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int e = iv2.e(i) * size;
        if (list instanceof tw2) {
            tw2 tw2 = (tw2) list;
            while (i4 < size) {
                Object zzb = tw2.zzb(i4);
                if (zzb instanceof tu2) {
                    i3 = iv2.b((tu2) zzb);
                } else {
                    i3 = iv2.b((String) zzb);
                }
                e += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof tu2) {
                    i2 = iv2.b((tu2) obj);
                } else {
                    i2 = iv2.b((String) obj);
                }
                e += i2;
                i4++;
            }
        }
        return e;
    }

    @DexIgnore
    public static int b(int i, List<jx2> list, cy2 cy2) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += iv2.c(i, list.get(i3), cy2);
        }
        return i2;
    }

    @DexIgnore
    public static uy2<?, ?> b() {
        return c;
    }

    @DexIgnore
    public static int a(int i, Object obj, cy2 cy2) {
        if (obj instanceof rw2) {
            return iv2.a(i, (rw2) obj);
        }
        return iv2.b(i, (jx2) obj, cy2);
    }

    @DexIgnore
    public static int a(int i, List<?> list, cy2 cy2) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = iv2.e(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof rw2) {
                i2 = iv2.a((rw2) obj);
            } else {
                i2 = iv2.a((jx2) obj, cy2);
            }
            e += i2;
        }
        return e;
    }

    @DexIgnore
    public static uy2<?, ?> a() {
        return b;
    }

    @DexIgnore
    public static uy2<?, ?> a(boolean z) {
        try {
            Class<?> e = e();
            if (e == null) {
                return null;
            }
            return (uy2) e.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @DexIgnore
    public static <T> void a(gx2 gx2, T t, T t2, long j) {
        bz2.a(t, j, gx2.zza(bz2.f(t, j), bz2.f(t2, j)));
    }

    @DexIgnore
    public static <T, FT extends sv2<FT>> void a(pv2<FT> pv2, T t, T t2) {
        qv2<FT> a2 = pv2.a((Object) t2);
        if (!a2.a.isEmpty()) {
            pv2.b(t).a(a2);
        }
    }

    @DexIgnore
    public static <T, UT, UB> void a(uy2<UT, UB> uy2, T t, T t2) {
        uy2.a(t, uy2.c(uy2.a(t), uy2.a(t2)));
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, List<Integer> list, fw2 fw2, UB ub, uy2<UT, UB> uy2) {
        UB ub2;
        UB ub3;
        if (fw2 == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                if (fw2.zza(intValue)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue));
                    }
                    i2++;
                } else {
                    ub3 = (UB) a(i, intValue, ub3, uy2);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!fw2.zza(intValue2)) {
                    ub3 = (UB) a(i, intValue2, ub3, uy2);
                    it.remove();
                }
            }
        }
        return ub2;
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, int i2, UB ub, uy2<UT, UB> uy2) {
        UB ub2;
        if (ub == null) {
            ub = uy2.a();
        }
        uy2.a(ub2, i, (long) i2);
        return ub2;
    }
}
