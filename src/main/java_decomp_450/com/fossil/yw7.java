package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw7 {
    @DexIgnore
    public /* final */ HashMap<String, zw7> a; // = new HashMap<>();

    @DexIgnore
    public final void a(zw7 zw7) {
        ee7.b(zw7, "assetEntity");
        this.a.put(zw7.e(), zw7);
    }

    @DexIgnore
    public final zw7 a(String str) {
        ee7.b(str, "id");
        return this.a.get(str);
    }

    @DexIgnore
    public final void a() {
        this.a.clear();
    }
}
