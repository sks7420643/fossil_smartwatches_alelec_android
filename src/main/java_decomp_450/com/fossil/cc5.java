package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum cc5 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    SPINNING("Spinning"),
    OUTDOOR_CYCLING("outdoor_cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout"),
    WALK("walk"),
    ROW_MACHINE("row_machine"),
    HIKING("hiking");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final cc5 a(ac5 ac5, ub5 ub5) {
            if (ac5 != null) {
                switch (bc5.b[ac5.ordinal()]) {
                    case 1:
                        return cc5.UNKNOWN;
                    case 2:
                        return cc5.RUNNING;
                    case 3:
                        if (ub5 != null) {
                            int i = bc5.a[ub5.ordinal()];
                            if (i == 1) {
                                return cc5.SPINNING;
                            }
                            if (i == 2) {
                                return cc5.OUTDOOR_CYCLING;
                            }
                        }
                        return cc5.OUTDOOR_CYCLING;
                    case 4:
                        return cc5.SPINNING;
                    case 5:
                        return cc5.TREADMILL;
                    case 6:
                        return cc5.ELLIPTICAL;
                    case 7:
                        return cc5.WEIGHTS;
                    case 8:
                        return cc5.WORKOUT;
                    case 9:
                        return cc5.UNKNOWN;
                    case 10:
                        return cc5.WALK;
                    case 11:
                        return cc5.ROW_MACHINE;
                    case 12:
                        return cc5.UNKNOWN;
                    case 13:
                        return cc5.UNKNOWN;
                    case 14:
                        return cc5.HIKING;
                }
            }
            return cc5.UNKNOWN;
        }

        @DexIgnore
        public final ub5 b(cc5 cc5) {
            ee7.b(cc5, "workoutWrapperType");
            switch (bc5.d[cc5.ordinal()]) {
                case 1:
                    return ub5.INDOOR;
                case 2:
                    return ub5.INDOOR;
                case 3:
                    return ub5.OUTDOOR;
                case 4:
                    return ub5.INDOOR;
                case 5:
                    return ub5.INDOOR;
                case 6:
                    return ub5.INDOOR;
                case 7:
                    return ub5.INDOOR;
                case 8:
                    return ub5.INDOOR;
                default:
                    return null;
            }
        }

        @DexIgnore
        public final ac5 c(cc5 cc5) {
            ee7.b(cc5, "workoutWrapperType");
            switch (bc5.c[cc5.ordinal()]) {
                case 1:
                    return ac5.UNKNOWN;
                case 2:
                    return ac5.RUNNING;
                case 3:
                    return ac5.SPINNING;
                case 4:
                    return ac5.CYCLING;
                case 5:
                    return ac5.TREADMILL;
                case 6:
                    return ac5.ELLIPTICAL;
                case 7:
                    return ac5.WEIGHTS;
                case 8:
                    return ac5.WORKOUT;
                case 9:
                    return ac5.WALKING;
                case 10:
                    return ac5.ROWING;
                case 11:
                    return ac5.HIKING;
                default:
                    throw new p87();
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final r87<Integer, Integer> a(cc5 cc5) {
            if (cc5 == null) {
                cc5 = cc5.UNKNOWN;
            }
            if (cc5 != null) {
                switch (bc5.e[cc5.ordinal()]) {
                    case 1:
                        return new r87<>(2131231199, 2131886570);
                    case 2:
                        return new r87<>(2131231200, 2131886571);
                    case 3:
                        return new r87<>(2131231194, 2131886565);
                    case 4:
                        return new r87<>(2131231201, 2131886572);
                    case 5:
                        return new r87<>(2131231195, 2131886566);
                    case 6:
                        return new r87<>(2131231203, 2131886574);
                    case 7:
                        return new r87<>(2131231204, 2131886575);
                    case 8:
                        return new r87<>(2131231202, 2131886573);
                    case 9:
                        return new r87<>(2131231198, 2131886569);
                    case 10:
                        return new r87<>(2131231197, 2131886567);
                    case 11:
                        return new r87<>(2131231196, 2131886575);
                }
            }
            throw new p87();
        }
    }

    @DexIgnore
    public cc5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
