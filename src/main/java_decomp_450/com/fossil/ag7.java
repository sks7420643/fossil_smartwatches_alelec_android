package com.fossil;

import com.fossil.zf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ag7<R> extends zf7<R>, vc7<R> {

    @DexIgnore
    public interface a<R> extends zf7.a<R>, vc7<R> {
    }

    @DexIgnore
    R get();

    @DexIgnore
    Object getDelegate();

    @DexIgnore
    a<R> getGetter();
}
