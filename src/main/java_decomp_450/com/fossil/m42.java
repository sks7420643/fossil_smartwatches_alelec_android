package com.fossil;

import android.util.Log;
import com.fossil.i12;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m42<R extends i12> extends m12<R> implements j12<R> {
    @DexIgnore
    public l12<? super R, ? extends i12> a;
    @DexIgnore
    public m42<? extends i12> b;
    @DexIgnore
    public volatile k12<? super R> c;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public Status e;
    @DexIgnore
    public /* final */ WeakReference<a12> f;
    @DexIgnore
    public /* final */ o42 g;

    @DexIgnore
    @Override // com.fossil.j12
    public final void a(R r) {
        synchronized (this.d) {
            if (!r.a().y()) {
                a(r.a());
                b(r);
            } else if (this.a != null) {
                f42.a().submit(new l42(this, r));
            } else if (b()) {
                this.c.b(r);
            }
        }
    }

    @DexIgnore
    public final void b(Status status) {
        synchronized (this.d) {
            if (this.a != null) {
                Status a2 = this.a.a(status);
                a72.a(a2, "onFailure must not return null");
                this.b.a(a2);
            } else if (b()) {
                this.c.a(status);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return (this.c == null || this.f.get() == null) ? false : true;
    }

    @DexIgnore
    public static void b(i12 i12) {
        if (i12 instanceof e12) {
            try {
                ((e12) i12).release();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(i12);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e2);
            }
        }
    }

    @DexIgnore
    public final void a(Status status) {
        synchronized (this.d) {
            this.e = status;
            b(status);
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
    }
}
