package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xv {
    @DexIgnore
    public static zu a(Context context, iv ivVar) {
        jv jvVar;
        jv jvVar2;
        String str;
        if (ivVar != null) {
            jvVar2 = new jv(ivVar);
        } else if (Build.VERSION.SDK_INT >= 9) {
            jvVar = new jv((iv) new qv());
            return a(context, jvVar);
        } else {
            try {
                String packageName = context.getPackageName();
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                str = packageName + "/" + packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
                str = "volley/0";
            }
            jvVar2 = new jv(new mv(AndroidHttpClient.newInstance(str)));
        }
        jvVar = jvVar2;
        return a(context, jvVar);
    }

    @DexIgnore
    public static zu a(Context context, su suVar) {
        zu zuVar = new zu(new lv(new File(context.getCacheDir(), "volley")), suVar);
        zuVar.b();
        return zuVar;
    }

    @DexIgnore
    public static zu a(Context context) {
        return a(context, (iv) null);
    }
}
