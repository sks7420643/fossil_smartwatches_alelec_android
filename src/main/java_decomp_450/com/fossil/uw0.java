package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw0 implements Parcelable.Creator<my0> {
    @DexIgnore
    public /* synthetic */ uw0(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public my0 createFromParcel(Parcel parcel) {
        return new my0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public my0[] newArray(int i) {
        return new my0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public my0 m78createFromParcel(Parcel parcel) {
        return new my0(parcel, null);
    }
}
