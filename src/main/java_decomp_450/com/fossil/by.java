package com.fossil;

import com.fossil.ey;
import com.fossil.ix;
import com.fossil.m00;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class by implements ey, ix.a<Object> {
    @DexIgnore
    public /* final */ List<yw> a;
    @DexIgnore
    public /* final */ fy<?> b;
    @DexIgnore
    public /* final */ ey.a c;
    @DexIgnore
    public int d;
    @DexIgnore
    public yw e;
    @DexIgnore
    public List<m00<File, ?>> f;
    @DexIgnore
    public int g;
    @DexIgnore
    public volatile m00.a<?> h;
    @DexIgnore
    public File i;

    @DexIgnore
    public by(fy<?> fyVar, ey.a aVar) {
        this(fyVar.c(), fyVar, aVar);
    }

    @DexIgnore
    @Override // com.fossil.ey
    public boolean a() {
        while (true) {
            boolean z = false;
            if (this.f == null || !b()) {
                int i2 = this.d + 1;
                this.d = i2;
                if (i2 >= this.a.size()) {
                    return false;
                }
                yw ywVar = this.a.get(this.d);
                File a2 = this.b.d().a(new cy(ywVar, this.b.l()));
                this.i = a2;
                if (a2 != null) {
                    this.e = ywVar;
                    this.f = this.b.a(a2);
                    this.g = 0;
                }
            } else {
                this.h = null;
                while (!z && b()) {
                    List<m00<File, ?>> list = this.f;
                    int i3 = this.g;
                    this.g = i3 + 1;
                    this.h = list.get(i3).a(this.i, this.b.n(), this.b.f(), this.b.i());
                    if (this.h != null && this.b.c(this.h.c.getDataClass())) {
                        this.h.c.a(this.b.j(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.g < this.f.size();
    }

    @DexIgnore
    @Override // com.fossil.ey
    public void cancel() {
        m00.a<?> aVar = this.h;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public by(List<yw> list, fy<?> fyVar, ey.a aVar) {
        this.d = -1;
        this.a = list;
        this.b = fyVar;
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ix.a
    public void a(Object obj) {
        this.c.a(this.e, obj, this.h.c, sw.DATA_DISK_CACHE, this.e);
    }

    @DexIgnore
    @Override // com.fossil.ix.a
    public void a(Exception exc) {
        this.c.a(this.e, exc, this.h.c, sw.DATA_DISK_CACHE);
    }
}
