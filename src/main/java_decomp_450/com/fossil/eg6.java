package com.fossil;

import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.te5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutScreenshotWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg6 extends zf6 implements te5.a {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<r87<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutTetherScreenShotManager h;
    @DexIgnore
    public List<DailyHeartRateSummary> i;
    @DexIgnore
    public List<HeartRateSample> j;
    @DexIgnore
    public DailyHeartRateSummary k;
    @DexIgnore
    public List<HeartRateSample> l;
    @DexIgnore
    public ob5 m;
    @DexIgnore
    public LiveData<qx6<List<DailyHeartRateSummary>>> n;
    @DexIgnore
    public LiveData<qx6<List<HeartRateSample>>> o;
    @DexIgnore
    public Listing<WorkoutSession> p;
    @DexIgnore
    public /* final */ ag6 q;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository r;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ UserRepository t;
    @DexIgnore
    public /* final */ WorkoutSessionRepository u;
    @DexIgnore
    public /* final */ pj4 v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<HeartRateSample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(HeartRateSample heartRateSample) {
            return Boolean.valueOf(invoke(heartRateSample));
        }

        @DexIgnore
        public final boolean invoke(HeartRateSample heartRateSample) {
            ee7.b(heartRateSample, "it");
            return zd5.d(heartRateSample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$observeWorkoutSessionData$1", f = "HeartRateDetailPresenter.kt", l = {93}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ eg6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements zd<qf<WorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.eg6$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$observeWorkoutSessionData$1$1$1", f = "HeartRateDetailPresenter.kt", l = {104}, m = "invokeSuspend")
            /* renamed from: com.fossil.eg6$c$a$a  reason: collision with other inner class name */
            public static final class C0054a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ qf $pageList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.eg6$c$a$a$a")
                /* renamed from: com.fossil.eg6$c$a$a$a  reason: collision with other inner class name */
                public static final class C0055a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0054a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0055a(fb7 fb7, C0054a aVar) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0055a aVar = new C0055a(fb7, this.this$0);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0055a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            Iterator it = this.this$0.$pageList.iterator();
                            while (it.hasNext()) {
                                WorkoutSession workoutSession = (WorkoutSession) it.next();
                                List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
                                boolean z = false;
                                boolean z2 = workoutGpsPoints != null && (workoutGpsPoints.isEmpty() ^ true);
                                boolean z3 = workoutSession.getMode() != ub5.INDOOR;
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                local.d("HeartRateDetailPresenter", workoutSession.getId() + "-isHasGpsDataPoint " + z2 + " isOutdoorMode " + z3);
                                if (z2 && z3) {
                                    String screenShotUri = workoutSession.getScreenShotUri();
                                    if (screenShotUri != null) {
                                        if (screenShotUri.length() > 0) {
                                            z = true;
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    local2.d("HeartRateDetailPresenter", workoutSession.getId() + "-isScreenshotUriExist " + z);
                                    if (!z) {
                                        String a = this.this$0.this$0.a.this$0.h.a(workoutSession.getId());
                                        if (ee5.a.a(a)) {
                                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                            local3.d("HeartRateDetailPresenter", workoutSession.getId() + "-Found in internal cache with path " + a);
                                            workoutSession.setScreenShotUri(a);
                                        } else {
                                            WorkoutTetherScreenShotManager p = this.this$0.this$0.a.this$0.h;
                                            ee7.a((Object) workoutSession, "workoutSession");
                                            WorkoutScreenshotWrapper a2 = p.a(workoutSession);
                                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                            local4.d("HeartRateDetailPresenter", workoutSession.getId() + "-Enqueue to generate screenshot " + a2);
                                            this.this$0.this$0.a.this$0.h.a(a2);
                                        }
                                    }
                                }
                            }
                            return i97.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0054a(a aVar, qf qfVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$pageList = qfVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0054a aVar = new C0054a(this.this$0, this.$pageList, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0054a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        FragmentActivity activity = ((bg6) this.this$0.a.this$0.q).getActivity();
                        if (activity != null) {
                            WorkoutTetherScreenShotManager p = this.this$0.a.this$0.h;
                            ee7.a((Object) activity, "it");
                            p.a(activity);
                            ti7 a2 = qj7.a();
                            C0055a aVar = new C0055a(null, this);
                            this.L$0 = yi7;
                            this.L$1 = activity;
                            this.label = 1;
                            if (vh7.a(a2, aVar, this) == a) {
                                return a;
                            }
                        }
                    } else if (i == 1) {
                        FragmentActivity fragmentActivity = (FragmentActivity) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ag6 o = this.this$0.a.this$0.q;
                    ob5 d = this.this$0.a.this$0.m;
                    qf<WorkoutSession> qfVar = this.$pageList;
                    ee7.a((Object) qfVar, "pageList");
                    o.a(true, d, qfVar);
                    return i97.a;
                }
            }

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<WorkoutSession> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "getWorkoutSessionsPaging observed size = " + qfVar.size());
                if (be5.o.g(PortfolioApp.g0.c().c())) {
                    ee7.a((Object) qfVar, "pageList");
                    if (ea7.d((Collection) qfVar).isEmpty()) {
                        this.a.this$0.q.a(false, this.a.this$0.m, qfVar);
                        return;
                    }
                }
                ik7 unused = xh7.b(this.a.this$0.e(), null, null, new C0054a(this, qfVar, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(eg6 eg6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = eg6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$date, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            eg6 eg6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                eg6 eg62 = this.this$0;
                WorkoutSessionRepository q = eg62.u;
                Date date = this.$date;
                WorkoutSessionRepository q2 = this.this$0.u;
                pj4 a3 = this.this$0.v;
                eg6 eg63 = this.this$0;
                this.L$0 = yi7;
                this.L$1 = eg62;
                this.label = 1;
                obj = q.getWorkoutSessionsPaging(date, q2, a3, eg63, this);
                if (obj == a2) {
                    return a2;
                }
                eg6 = eg62;
            } else if (i == 1) {
                eg6 = (eg6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            eg6.p = (Listing) obj;
            Listing k = this.this$0.p;
            if (k != null) {
                LiveData pagedList = k.getPagedList();
                ag6 o = this.this$0.q;
                if (o != null) {
                    pagedList.a((bg6) o, new a(this));
                    return i97.a;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ eg6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$sampleTransformations$1$1", f = "HeartRateDetailPresenter.kt", l = {72, 72}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<HeartRateSample>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<HeartRateSample>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    HeartRateSampleRepository e = this.this$0.a.s;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = e.getHeartRateSamples(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(eg6 eg6) {
            this.a = eg6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<HeartRateSample>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1", f = "HeartRateDetailPresenter.kt", l = {176}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ eg6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1$1", f = "HeartRateDetailPresenter.kt", l = {176}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Date> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.g(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(eg6 eg6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = eg6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$date, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Pair<Date, Date> a2;
            r87 r87;
            eg6 eg6;
            Object a3 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.this$0.e == null) {
                    eg6 eg62 = this.this$0;
                    ti7 b = eg62.b();
                    a aVar = new a(null);
                    this.L$0 = yi7;
                    this.L$1 = eg62;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a3) {
                        return a3;
                    }
                    eg6 = eg62;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
                this.this$0.f = this.$date;
                Boolean w = zd5.w(this.$date);
                ag6 o = this.this$0.q;
                Date date = this.$date;
                ee7.a((Object) w, "isToday");
                o.a(date, zd5.c(this.this$0.e, this.$date), w.booleanValue(), !zd5.c(new Date(), this.$date));
                a2 = zd5.a(this.$date, this.this$0.e);
                ee7.a((Object) a2, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                r87 = (r87) this.this$0.g.a();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + r87 + ", newRange=" + new r87(a2.first, a2.second));
                if (r87 != null || !zd5.d((Date) r87.getFirst(), (Date) a2.first) || !zd5.d((Date) r87.getSecond(), (Date) a2.second)) {
                    this.this$0.g.a(new r87(a2.first, a2.second));
                } else {
                    ik7 unused = this.this$0.n();
                    ik7 unused2 = this.this$0.o();
                    eg6 eg63 = this.this$0;
                    eg63.c(eg63.f);
                }
                return i97.a;
            } else if (i == 1) {
                eg6 = (eg6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            eg6.e = (Date) obj;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
            this.this$0.f = this.$date;
            Boolean w2 = zd5.w(this.$date);
            ag6 o2 = this.this$0.q;
            Date date2 = this.$date;
            ee7.a((Object) w2, "isToday");
            o2.a(date2, zd5.c(this.this$0.e, this.$date), w2.booleanValue(), !zd5.c(new Date(), this.$date));
            a2 = zd5.a(this.$date, this.this$0.e);
            ee7.a((Object) a2, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            r87 = (r87) this.this$0.g.a();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + r87 + ", newRange=" + new r87(a2.first, a2.second));
            if (r87 != null) {
            }
            this.this$0.g.a(new r87(a2.first, a2.second));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1", f = "HeartRateDetailPresenter.kt", l = {225}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ eg6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super DailyHeartRateSummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super DailyHeartRateSummary> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    eg6 eg6 = this.this$0.this$0;
                    return eg6.b(eg6.f, this.this$0.this$0.i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(eg6 eg6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = eg6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Integer a2;
            Resting resting;
            Integer a3;
            Object a4 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a4) {
                    return a4;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
            if (this.this$0.k == null || (!ee7.a(this.this$0.k, dailyHeartRateSummary))) {
                this.this$0.k = dailyHeartRateSummary;
                DailyHeartRateSummary i2 = this.this$0.k;
                int i3 = 0;
                int intValue = (i2 == null || (resting = i2.getResting()) == null || (a3 = pb7.a(resting.getValue())) == null) ? 0 : a3.intValue();
                DailyHeartRateSummary i4 = this.this$0.k;
                if (!(i4 == null || (a2 = pb7.a(i4.getMax())) == null)) {
                    i3 = a2.intValue();
                }
                this.this$0.q.c(intValue, i3);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1", f = "HeartRateDetailPresenter.kt", l = {238, 242, 251, 258}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ eg6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.eg6$g$a$a")
            /* renamed from: com.fossil.eg6$g$a$a  reason: collision with other inner class name */
            public static final class C0056a<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return bb7.a(Long.valueOf(t.getStartTimeId().getMillis()), Long.valueOf(t2.getStartTimeId().getMillis()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List g = this.this$0.this$0.l;
                    if (g == null) {
                        return null;
                    }
                    if (g.size() > 1) {
                        aa7.a(g, new C0056a());
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listTimeZoneChange;
            @DexIgnore
            public /* final */ /* synthetic */ List $listTodayHeartRateModel;
            @DexIgnore
            public /* final */ /* synthetic */ int $maxHR;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $previousTimeZoneOffset;
            @DexIgnore
            public /* final */ /* synthetic */ long $startOfDay;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(g gVar, long j, qe7 qe7, List list, List list2, int i, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$startOfDay = j;
                this.$previousTimeZoneOffset = qe7;
                this.$listTimeZoneChange = list;
                this.$listTodayHeartRateModel = list2;
                this.$maxHR = i;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Iterator it;
                char c;
                StringBuilder sb;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List g = this.this$0.this$0.l;
                    if (g == null) {
                        return null;
                    }
                    for (Iterator it2 = g.iterator(); it2.hasNext(); it2 = it) {
                        HeartRateSample heartRateSample = (HeartRateSample) it2.next();
                        long j = (long) 60000;
                        long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / j;
                        long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / j;
                        long j2 = (millis2 + millis) / ((long) 2);
                        if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                            int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                            String b = ye5.b(hourOfDay);
                            we7 we7 = we7.a;
                            String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{pb7.a(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), pb7.a(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))}, 2));
                            ee7.a((Object) format, "java.lang.String.format(format, *args)");
                            List list = this.$listTimeZoneChange;
                            Integer a = pb7.a((int) j2);
                            it = it2;
                            r87 r87 = new r87(pb7.a(hourOfDay), pb7.a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(b);
                            if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                                sb = new StringBuilder();
                                c = '+';
                            } else {
                                sb = new StringBuilder();
                                c = '-';
                            }
                            sb.append(c);
                            sb.append(format);
                            sb2.append(sb.toString());
                            list.add(new v87(a, r87, sb2.toString()));
                            this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                        } else {
                            it = it2;
                        }
                        if (!this.$listTodayHeartRateModel.isEmpty()) {
                            fz6 fz6 = (fz6) ea7.f(this.$listTodayHeartRateModel);
                            if (millis - ((long) fz6.a()) > 1) {
                                this.$listTodayHeartRateModel.add(new fz6(0, 0, 0, fz6.a(), (int) millis, (int) j2));
                            }
                        }
                        int average = (int) heartRateSample.getAverage();
                        if (heartRateSample.getMax() == this.$maxHR) {
                            average = heartRateSample.getMax();
                        }
                        this.$listTodayHeartRateModel.add(new fz6(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j2));
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v7 */
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                int i;
                HeartRateSample heartRateSample;
                Integer a;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List g = this.this$0.this$0.l;
                    if (g != null) {
                        Iterator it = g.iterator();
                        if (!it.hasNext()) {
                            heartRateSample = null;
                        } else {
                            Object next = it.next();
                            if (!it.hasNext()) {
                                heartRateSample = next;
                            } else {
                                Integer a2 = pb7.a(((HeartRateSample) next).getMax());
                                do {
                                    Object next2 = it.next();
                                    Integer a3 = pb7.a(((HeartRateSample) next2).getMax());
                                    if (a2.compareTo((Object) a3) < 0) {
                                        next = next2;
                                        a2 = a3;
                                    }
                                } while (it.hasNext());
                            }
                            heartRateSample = next;
                        }
                        HeartRateSample heartRateSample2 = heartRateSample;
                        if (!(heartRateSample2 == null || (a = pb7.a(heartRateSample2.getMax())) == null)) {
                            i = a.intValue();
                            return pb7.a(i);
                        }
                    }
                    i = 0;
                    return pb7.a(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super List<HeartRateSample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<HeartRateSample>> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    eg6 eg6 = this.this$0.this$0;
                    return eg6.a(eg6.f, this.this$0.this$0.j);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(eg6 eg6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = eg6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00d7  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x013c A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x013d  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x019b A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x019c  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r24) {
            /*
                r23 = this;
                r9 = r23
                java.lang.Object r10 = com.fossil.nb7.a()
                int r0 = r9.label
                r11 = 4
                r1 = 3
                r2 = 2
                java.lang.String r12 = "HeartRateDetailPresenter"
                r3 = 0
                r13 = 1
                r14 = 0
                if (r0 == 0) goto L_0x006b
                if (r0 == r13) goto L_0x0061
                if (r0 == r2) goto L_0x0054
                if (r0 == r1) goto L_0x003b
                if (r0 != r11) goto L_0x0033
                java.lang.Object r0 = r9.L$4
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r9.L$3
                com.fossil.qe7 r1 = (com.fossil.qe7) r1
                java.lang.Object r1 = r9.L$2
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r2 = r9.L$1
                java.util.List r2 = (java.util.List) r2
                java.lang.Object r2 = r9.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r24)
                goto L_0x019d
            L_0x0033:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003b:
                long r0 = r9.J$0
                java.lang.Object r2 = r9.L$2
                java.util.List r2 = (java.util.List) r2
                java.lang.Object r3 = r9.L$1
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r4 = r9.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r24)
                r7 = r0
                r15 = r2
                r6 = r3
                r5 = r4
                r1 = r24
                goto L_0x0141
            L_0x0054:
                java.lang.Object r0 = r9.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r2 = r9.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r24)
                r4 = r2
                goto L_0x00ca
            L_0x0061:
                java.lang.Object r0 = r9.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r24)
                r4 = r24
                goto L_0x0091
            L_0x006b:
                com.fossil.t87.a(r24)
                com.fossil.yi7 r0 = r9.p$
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                java.lang.String r5 = "showDetailChart"
                r4.d(r12, r5)
                com.fossil.eg6 r4 = r9.this$0
                com.fossil.ti7 r4 = r4.b()
                com.fossil.eg6$g$d r5 = new com.fossil.eg6$g$d
                r5.<init>(r9, r3)
                r9.L$0 = r0
                r9.label = r13
                java.lang.Object r4 = com.fossil.vh7.a(r4, r5, r9)
                if (r4 != r10) goto L_0x0091
                return r10
            L_0x0091:
                java.util.List r4 = (java.util.List) r4
                com.fossil.eg6 r5 = r9.this$0
                java.util.List r5 = r5.l
                if (r5 == 0) goto L_0x00a8
                com.fossil.eg6 r5 = r9.this$0
                java.util.List r5 = r5.l
                boolean r5 = com.fossil.ee7.a(r5, r4)
                r5 = r5 ^ r13
                if (r5 == 0) goto L_0x03e3
            L_0x00a8:
                com.fossil.eg6 r5 = r9.this$0
                r5.l = r4
                com.fossil.eg6 r5 = r9.this$0
                com.fossil.ti7 r5 = r5.b()
                com.fossil.eg6$g$a r6 = new com.fossil.eg6$g$a
                r6.<init>(r9, r3)
                r9.L$0 = r0
                r9.L$1 = r4
                r9.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r5, r6, r9)
                if (r2 != r10) goto L_0x00c5
                return r10
            L_0x00c5:
                r22 = r4
                r4 = r0
                r0 = r22
            L_0x00ca:
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>()
                com.fossil.eg6 r5 = r9.this$0
                java.util.List r5 = r5.l
                if (r5 == 0) goto L_0x010e
                com.fossil.eg6 r5 = r9.this$0
                java.util.List r5 = r5.l
                if (r5 == 0) goto L_0x010a
                boolean r5 = r5.isEmpty()
                r5 = r5 ^ r13
                if (r5 == 0) goto L_0x010e
                com.fossil.eg6 r5 = r9.this$0
                java.util.List r5 = r5.l
                if (r5 == 0) goto L_0x0106
                java.lang.Object r5 = r5.get(r14)
                com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r5 = (com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) r5
                org.joda.time.DateTime r5 = r5.getStartTimeId()
                org.joda.time.DateTime r5 = r5.withTimeAtStartOfDay()
                java.lang.String r6 = "mHeartRateSamplesOfDate!\u2026().withTimeAtStartOfDay()"
                com.fossil.ee7.a(r5, r6)
                long r5 = r5.getMillis()
                goto L_0x0121
            L_0x0106:
                com.fossil.ee7.a()
                throw r3
            L_0x010a:
                com.fossil.ee7.a()
                throw r3
            L_0x010e:
                com.fossil.eg6 r5 = r9.this$0
                java.util.Date r5 = r5.f
                java.util.Date r5 = com.fossil.zd5.q(r5)
                java.lang.String r6 = "DateHelper.getStartOfDay(mDate)"
                com.fossil.ee7.a(r5, r6)
                long r5 = r5.getTime()
            L_0x0121:
                com.fossil.eg6 r7 = r9.this$0
                com.fossil.ti7 r7 = r7.b()
                com.fossil.eg6$g$c r8 = new com.fossil.eg6$g$c
                r8.<init>(r9, r3)
                r9.L$0 = r4
                r9.L$1 = r0
                r9.L$2 = r2
                r9.J$0 = r5
                r9.label = r1
                java.lang.Object r1 = com.fossil.vh7.a(r7, r8, r9)
                if (r1 != r10) goto L_0x013d
                return r10
            L_0x013d:
                r15 = r2
                r7 = r5
                r6 = r0
                r5 = r4
            L_0x0141:
                java.lang.Number r1 = (java.lang.Number) r1
                int r4 = r1.intValue()
                com.fossil.qe7 r2 = new com.fossil.qe7
                r2.<init>()
                r0 = -1
                r2.element = r0
                java.util.ArrayList r3 = new java.util.ArrayList
                r3.<init>()
                com.fossil.eg6 r0 = r9.this$0
                com.fossil.ti7 r1 = r0.b()
                com.fossil.eg6$g$b r0 = new com.fossil.eg6$g$b
                r16 = 0
                r24 = r0
                r14 = r1
                r1 = r23
                r17 = r2
                r18 = r3
                r2 = r7
                r19 = r4
                r4 = r17
                r13 = r5
                r5 = r18
                r11 = r6
                r6 = r15
                r20 = r7
                r7 = r19
                r8 = r16
                r0.<init>(r1, r2, r4, r5, r6, r7, r8)
                r9.L$0 = r13
                r9.L$1 = r11
                r9.L$2 = r15
                r0 = r20
                r9.J$0 = r0
                r0 = r19
                r9.I$0 = r0
                r0 = r17
                r9.L$3 = r0
                r0 = r18
                r9.L$4 = r0
                r1 = 4
                r9.label = r1
                r1 = r24
                java.lang.Object r1 = com.fossil.vh7.a(r14, r1, r9)
                if (r1 != r10) goto L_0x019c
                return r10
            L_0x019c:
                r1 = r15
            L_0x019d:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "listTimeZoneChange="
                r3.append(r4)
                java.lang.String r4 = com.fossil.wc5.a(r0)
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r2.d(r12, r3)
                boolean r2 = r0.isEmpty()
                r3 = 1
                r2 = r2 ^ r3
                if (r2 == 0) goto L_0x03b9
                int r2 = r0.size()
                if (r2 <= r3) goto L_0x03b9
                r2 = 0
                java.lang.Object r4 = r0.get(r2)
                com.fossil.v87 r4 = (com.fossil.v87) r4
                java.lang.Object r2 = r4.getSecond()
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r2 = r2.getSecond()
                java.lang.Number r2 = (java.lang.Number) r2
                float r2 = r2.floatValue()
                int r4 = com.fossil.w97.a(r0)
                java.lang.Object r4 = r0.get(r4)
                com.fossil.v87 r4 = (com.fossil.v87) r4
                java.lang.Object r4 = r4.getSecond()
                com.fossil.r87 r4 = (com.fossil.r87) r4
                java.lang.Object r4 = r4.getSecond()
                java.lang.Number r4 = (java.lang.Number) r4
                float r4 = r4.floatValue()
                r5 = 0
                float r6 = (float) r5
                r5 = 60
                int r7 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
                if (r7 >= 0) goto L_0x0206
                int r7 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r7 < 0) goto L_0x020e
            L_0x0206:
                int r7 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
                if (r7 < 0) goto L_0x0215
                int r7 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r7 < 0) goto L_0x0215
            L_0x020e:
                float r6 = r2 - r4
                float r5 = (float) r5
                float r6 = r6 * r5
                int r5 = (int) r6
                goto L_0x0224
            L_0x0215:
                float r7 = r2 - r4
                float r5 = (float) r5
                float r7 = r7 * r5
                int r5 = (int) r7
                int r5 = java.lang.Math.abs(r5)
                int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r6 >= 0) goto L_0x0224
                int r5 = -r5
            L_0x0224:
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
                java.util.Iterator r7 = r0.iterator()
            L_0x022d:
                boolean r8 = r7.hasNext()
                if (r8 == 0) goto L_0x025f
                java.lang.Object r8 = r7.next()
                r10 = r8
                com.fossil.v87 r10 = (com.fossil.v87) r10
                java.lang.Object r10 = r10.getSecond()
                com.fossil.r87 r10 = (com.fossil.r87) r10
                java.lang.Object r10 = r10.getSecond()
                java.lang.Number r10 = (java.lang.Number) r10
                float r10 = r10.floatValue()
                int r10 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
                if (r10 != 0) goto L_0x0250
                r10 = 1
                goto L_0x0251
            L_0x0250:
                r10 = 0
            L_0x0251:
                java.lang.Boolean r10 = com.fossil.pb7.a(r10)
                boolean r10 = r10.booleanValue()
                if (r10 == 0) goto L_0x022d
                r6.add(r8)
                goto L_0x022d
            L_0x025f:
                java.util.ArrayList r7 = new java.util.ArrayList
                r8 = 10
                int r10 = com.fossil.x97.a(r6, r8)
                r7.<init>(r10)
                java.util.Iterator r6 = r6.iterator()
            L_0x026e:
                boolean r10 = r6.hasNext()
                if (r10 == 0) goto L_0x0284
                java.lang.Object r10 = r6.next()
                com.fossil.v87 r10 = (com.fossil.v87) r10
                java.lang.Object r10 = r10.getSecond()
                com.fossil.r87 r10 = (com.fossil.r87) r10
                r7.add(r10)
                goto L_0x026e
            L_0x0284:
                java.util.ArrayList r6 = new java.util.ArrayList
                int r10 = com.fossil.x97.a(r7, r8)
                r6.<init>(r10)
                java.util.Iterator r7 = r7.iterator()
            L_0x0291:
                boolean r10 = r7.hasNext()
                if (r10 == 0) goto L_0x02af
                java.lang.Object r10 = r7.next()
                com.fossil.r87 r10 = (com.fossil.r87) r10
                java.lang.Object r10 = r10.getFirst()
                java.lang.Number r10 = (java.lang.Number) r10
                int r10 = r10.intValue()
                java.lang.Integer r10 = com.fossil.pb7.a(r10)
                r6.add(r10)
                goto L_0x0291
            L_0x02af:
                r10 = 0
                java.lang.Integer r7 = com.fossil.pb7.a(r10)
                boolean r6 = r6.contains(r7)
                java.lang.String r7 = "12a"
                if (r6 != 0) goto L_0x02ee
                com.fossil.eg6 r6 = r9.this$0
                java.lang.Float r11 = com.fossil.pb7.a(r2)
                java.lang.String r6 = r6.a(r11)
                com.fossil.v87 r11 = new com.fossil.v87
                java.lang.Integer r13 = com.fossil.pb7.a(r10)
                com.fossil.r87 r14 = new com.fossil.r87
                java.lang.Integer r15 = com.fossil.pb7.a(r10)
                java.lang.Float r2 = com.fossil.pb7.a(r2)
                r14.<init>(r15, r2)
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                r2.append(r7)
                r2.append(r6)
                java.lang.String r2 = r2.toString()
                r11.<init>(r13, r14, r2)
                r0.add(r10, r11)
            L_0x02ee:
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>()
                java.util.Iterator r6 = r0.iterator()
            L_0x02f7:
                boolean r11 = r6.hasNext()
                if (r11 == 0) goto L_0x0329
                java.lang.Object r11 = r6.next()
                r13 = r11
                com.fossil.v87 r13 = (com.fossil.v87) r13
                java.lang.Object r13 = r13.getSecond()
                com.fossil.r87 r13 = (com.fossil.r87) r13
                java.lang.Object r13 = r13.getSecond()
                java.lang.Number r13 = (java.lang.Number) r13
                float r13 = r13.floatValue()
                int r13 = (r13 > r4 ? 1 : (r13 == r4 ? 0 : -1))
                if (r13 != 0) goto L_0x031a
                r13 = 1
                goto L_0x031b
            L_0x031a:
                r13 = 0
            L_0x031b:
                java.lang.Boolean r13 = com.fossil.pb7.a(r13)
                boolean r13 = r13.booleanValue()
                if (r13 == 0) goto L_0x02f7
                r2.add(r11)
                goto L_0x02f7
            L_0x0329:
                java.util.ArrayList r3 = new java.util.ArrayList
                int r6 = com.fossil.x97.a(r2, r8)
                r3.<init>(r6)
                java.util.Iterator r2 = r2.iterator()
            L_0x0336:
                boolean r6 = r2.hasNext()
                if (r6 == 0) goto L_0x034c
                java.lang.Object r6 = r2.next()
                com.fossil.v87 r6 = (com.fossil.v87) r6
                java.lang.Object r6 = r6.getSecond()
                com.fossil.r87 r6 = (com.fossil.r87) r6
                r3.add(r6)
                goto L_0x0336
            L_0x034c:
                java.util.ArrayList r2 = new java.util.ArrayList
                int r6 = com.fossil.x97.a(r3, r8)
                r2.<init>(r6)
                java.util.Iterator r3 = r3.iterator()
            L_0x0359:
                boolean r6 = r3.hasNext()
                if (r6 == 0) goto L_0x0377
                java.lang.Object r6 = r3.next()
                com.fossil.r87 r6 = (com.fossil.r87) r6
                java.lang.Object r6 = r6.getFirst()
                java.lang.Number r6 = (java.lang.Number) r6
                int r6 = r6.intValue()
                java.lang.Integer r6 = com.fossil.pb7.a(r6)
                r2.add(r6)
                goto L_0x0359
            L_0x0377:
                r3 = 24
                java.lang.Integer r6 = com.fossil.pb7.a(r3)
                boolean r2 = r2.contains(r6)
                if (r2 != 0) goto L_0x03b7
                com.fossil.eg6 r2 = r9.this$0
                java.lang.Float r6 = com.fossil.pb7.a(r4)
                java.lang.String r2 = r2.a(r6)
                com.fossil.v87 r6 = new com.fossil.v87
                int r8 = r5 + 1440
                java.lang.Integer r8 = com.fossil.pb7.a(r8)
                com.fossil.r87 r10 = new com.fossil.r87
                java.lang.Integer r3 = com.fossil.pb7.a(r3)
                java.lang.Float r4 = com.fossil.pb7.a(r4)
                r10.<init>(r3, r4)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r7)
                r3.append(r2)
                java.lang.String r2 = r3.toString()
                r6.<init>(r8, r10, r2)
                r0.add(r6)
            L_0x03b7:
                r14 = r5
                goto L_0x03be
            L_0x03b9:
                r10 = 0
                r0.clear()
                r14 = 0
            L_0x03be:
                int r14 = r14 + 1440
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "minutesOfDay="
                r3.append(r4)
                r3.append(r14)
                java.lang.String r3 = r3.toString()
                r2.d(r12, r3)
                com.fossil.eg6 r2 = r9.this$0
                com.fossil.ag6 r2 = r2.q
                r2.a(r14, r1, r0)
            L_0x03e3:
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.eg6.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1", f = "HeartRateDetailPresenter.kt", l = {143}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ eg6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements zd<qx6<? extends List<DailyHeartRateSummary>>> {
            @DexIgnore
            public /* final */ /* synthetic */ h a;

            @DexIgnore
            public a(h hVar) {
                this.a = hVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<? extends List<DailyHeartRateSummary>> qx6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - summaryTransformations -- status=");
                lb5 lb5 = null;
                sb.append(qx6 != null ? qx6.d() : null);
                sb.append(", resource=");
                sb.append(qx6 != null ? (List) qx6.c() : null);
                sb.append(", status=");
                if (qx6 != null) {
                    lb5 = qx6.d();
                }
                sb.append(lb5);
                local.d("HeartRateDetailPresenter", sb.toString());
                if (qx6 != null && qx6.d() != lb5.DATABASE_LOADING) {
                    this.a.this$0.i = (List) qx6.c();
                    ik7 unused = this.a.this$0.n();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements zd<qx6<? extends List<HeartRateSample>>> {
            @DexIgnore
            public /* final */ /* synthetic */ h a;

            @DexIgnore
            public b(h hVar) {
                this.a = hVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<? extends List<HeartRateSample>> qx6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - sampleTransformations -- status=");
                lb5 lb5 = null;
                sb.append(qx6 != null ? qx6.d() : null);
                sb.append(", resource=");
                sb.append(qx6 != null ? (List) qx6.c() : null);
                sb.append(", status=");
                if (qx6 != null) {
                    lb5 = qx6.d();
                }
                sb.append(lb5);
                local.d("HeartRateDetailPresenter", sb.toString());
                if (qx6 != null && qx6.d() != lb5.DATABASE_LOADING) {
                    this.a.this$0.j = (List) qx6.c();
                    ik7 unused = this.a.this$0.o();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(eg6 eg6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = eg6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            eg6 eg6;
            String str;
            MFUser.UnitGroup unitGroup;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                eg6 eg62 = this.this$0;
                UserRepository n = eg62.t;
                this.L$0 = yi7;
                this.L$1 = eg62;
                this.label = 1;
                obj = n.getCurrentUser(this);
                if (obj == a2) {
                    return a2;
                }
                eg6 = eg62;
            } else if (i == 1) {
                eg6 = (eg6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser == null || (unitGroup = mFUser.getUnitGroup()) == null || (str = unitGroup.getDistance()) == null) {
                str = ob5.METRIC.getValue();
            }
            ob5 fromString = ob5.fromString(str);
            ee7.a((Object) fromString, "Unit.fromString(mUserRep\u2026    ?: Unit.METRIC.value)");
            eg6.m = fromString;
            LiveData s = this.this$0.n;
            ag6 o = this.this$0.q;
            if (o != null) {
                s.a((bg6) o, new a(this));
                this.this$0.o.a((LifecycleOwner) this.this$0.q, new b(this));
                return i97.a;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ eg6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$summaryTransformations$1$1", f = "HeartRateDetailPresenter.kt", l = {68, 68}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<DailyHeartRateSummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<DailyHeartRateSummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    HeartRateSummaryRepository j = this.this$0.a.r;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = j.getHeartRateSummaries(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public i(eg6 eg6) {
            this.a = eg6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<DailyHeartRateSummary>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public eg6(ag6 ag6, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, pj4 pj4) {
        ee7.b(ag6, "mView");
        ee7.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        ee7.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        ee7.b(fileRepository, "mFileRepository");
        ee7.b(pj4, "appExecutors");
        this.q = ag6;
        this.r = heartRateSummaryRepository;
        this.s = heartRateSampleRepository;
        this.t = userRepository;
        this.u = workoutSessionRepository;
        this.v = pj4;
        this.h = new WorkoutTetherScreenShotManager(this.u, fileRepository);
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.m = ob5.METRIC;
        LiveData<qx6<List<DailyHeartRateSummary>>> b2 = ge.b(this.g, new i(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.n = b2;
        LiveData<qx6<List<HeartRateSample>>> b3 = ge.b(this.g, new d(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026st, second, true))}\n    }");
        this.o = b3;
    }

    @DexIgnore
    @Override // com.fossil.te5.a
    public void a(te5.g gVar) {
        ee7.b(gVar, "report");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ik7 unused = xh7.b(e(), null, null, new h(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "stop");
        LiveData<qx6<List<HeartRateSample>>> liveData = this.o;
        ag6 ag6 = this.q;
        if (ag6 != null) {
            liveData.a((bg6) ag6);
            this.n.a((LifecycleOwner) this.q);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public ob5 h() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void j() {
        LiveData<qf<WorkoutSession>> pagedList;
        try {
            Listing<WorkoutSession> listing = this.p;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                ag6 ag6 = this.q;
                if (ag6 != null) {
                    pagedList.a((bg6) ag6);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
                }
            }
            this.u.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("HeartRateDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void k() {
        Date o2 = zd5.o(this.f);
        ee7.a((Object) o2, "DateHelper.getNextDate(mDate)");
        b(o2);
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void l() {
        Date p2 = zd5.p(this.f);
        ee7.a((Object) p2, "DateHelper.getPrevDate(mDate)");
        b(p2);
    }

    @DexIgnore
    public void m() {
        this.q.a(this);
    }

    @DexIgnore
    public final ik7 n() {
        return xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final ik7 o() {
        return xh7.b(e(), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public WorkoutTetherScreenShotManager i() {
        return this.h;
    }

    @DexIgnore
    public final void c(Date date) {
        j();
        ik7 unused = xh7.b(e(), null, null, new c(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void b(Date date) {
        ee7.b(date, "date");
        ik7 unused = xh7.b(e(), null, null, new e(this, date, null), 3, null);
    }

    @DexIgnore
    public final DailyHeartRateSummary b(Date date, List<DailyHeartRateSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (zd5.d(next.getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void a(Date date) {
        ee7.b(date, "date");
        c(date);
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void a(Bundle bundle) {
        ee7.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    public final List<HeartRateSample> a(Date date, List<HeartRateSample> list) {
        hg7 b2;
        hg7 a2;
        if (list == null || (b2 = ea7.b((Iterable) list)) == null || (a2 = og7.a(b2, new b(date))) == null) {
            return null;
        }
        return og7.g(a2);
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        we7 we7 = we7.a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)}, 2));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }
}
