package com.fossil;

import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sg2 {
    @DexIgnore
    Object a(qg2 qg2, Message message);

    @DexIgnore
    void a(qg2 qg2, Message message, long j);

    @DexIgnore
    void a(qg2 qg2, Message message, Object obj);

    @DexIgnore
    void a(qg2 qg2, Throwable th, Object obj);
}
