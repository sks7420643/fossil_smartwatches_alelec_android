package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg7 extends lg7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements hg7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public a(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        @Override // com.fossil.hg7
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    @DexIgnore
    public static final <T> hg7<T> a(Iterator<? extends T> it) {
        ee7.b(it, "$this$asSequence");
        return a(new a(it));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.hg7<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> hg7<T> a(hg7<? extends T> hg7) {
        ee7.b(hg7, "$this$constrainOnce");
        return hg7 instanceof eg7 ? hg7 : new eg7(hg7);
    }

    @DexIgnore
    public static final <T> hg7<T> a(vc7<? extends T> vc7, gd7<? super T, ? extends T> gd7) {
        ee7.b(vc7, "seedFunction");
        ee7.b(gd7, "nextFunction");
        return new gg7(vc7, gd7);
    }
}
