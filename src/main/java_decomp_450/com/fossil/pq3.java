package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pq3 extends i72 implements yp3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<pq3> CREATOR; // = new qq3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public pq3(String str, String str2, int i, boolean z) {
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = z;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof pq3)) {
            return false;
        }
        return ((pq3) obj).a.equals(this.a);
    }

    @DexIgnore
    public final String g() {
        return this.a;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String str2 = this.a;
        int i = this.c;
        boolean z = this.d;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 45 + String.valueOf(str2).length());
        sb.append("Node{");
        sb.append(str);
        sb.append(", id=");
        sb.append(str2);
        sb.append(", hops=");
        sb.append(i);
        sb.append(", isNearby=");
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final boolean v() {
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, g(), false);
        k72.a(parcel, 3, e(), false);
        k72.a(parcel, 4, this.c);
        k72.a(parcel, 5, v());
        k72.a(parcel, a2);
    }
}
