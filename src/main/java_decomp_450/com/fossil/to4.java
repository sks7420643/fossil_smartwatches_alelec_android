package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b; // = "deviceToken";
    @DexIgnore
    public /* final */ ch5 c;
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {30}, m = "registerFCM")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ to4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(to4 to4, fb7 fb7) {
            super(fb7);
            this.this$0 = to4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository$registerFCM$response$1", f = "FCMRepository.kt", l = {30}, m = "invokeSuspend")
    public static final class b extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $body;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ to4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(to4 to4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = to4;
            this.$body = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b(this.this$0, this.$body, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((b) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.d;
                ie4 ie4 = this.$body;
                this.label = 1;
                obj = a2.pushFCMToken(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {52}, m = "retry")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ to4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(to4 to4, fb7 fb7) {
            super(fb7);
            this.this$0 = to4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {72}, m = "unregisterFCM")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ to4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(to4 to4, fb7 fb7) {
            super(fb7);
            this.this$0 = to4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository$unregisterFCM$response$1", f = "FCMRepository.kt", l = {72}, m = "invokeSuspend")
    public static final class e extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $body;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ to4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(to4 to4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = to4;
            this.$body = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new e(this.this$0, this.$body, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((e) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.d;
                ie4 ie4 = this.$body;
                this.label = 1;
                obj = a2.removeFCMToken(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    public to4(ch5 ch5, ApiServiceV2 apiServiceV2) {
        ee7.b(ch5, "shared");
        ee7.b(apiServiceV2, "api");
        this.c = ch5;
        this.d = apiServiceV2;
        String simpleName = to4.class.getSimpleName();
        ee7.a((Object) simpleName, "FCMRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.fossil.ko4<java.lang.Boolean>> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof com.fossil.to4.d
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.to4$d r0 = (com.fossil.to4.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.to4$d r0 = new com.fossil.to4$d
            r0.<init>(r9, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L_0x003f
            if (r2 != r4) goto L_0x0037
            java.lang.Object r1 = r0.L$2
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.to4 r0 = (com.fossil.to4) r0
            com.fossil.t87.a(r10)
            goto L_0x0095
        L_0x0037:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x003f:
            com.fossil.t87.a(r10)
            com.fossil.ch5 r10 = r9.c
            java.lang.String r10 = r10.g()
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r6 = r9.a
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "unregisterFCM - authorisedFireBaseToken: "
            r7.append(r8)
            r7.append(r10)
            java.lang.String r7 = r7.toString()
            r2.e(r6, r7)
            if (r10 == 0) goto L_0x006f
            int r2 = r10.length()
            if (r2 != 0) goto L_0x006d
            goto L_0x006f
        L_0x006d:
            r2 = 0
            goto L_0x0070
        L_0x006f:
            r2 = 1
        L_0x0070:
            if (r2 == 0) goto L_0x007c
            com.fossil.ko4 r10 = new com.fossil.ko4
            java.lang.Boolean r0 = com.fossil.pb7.a(r4)
            r10.<init>(r0, r5, r3, r5)
            return r10
        L_0x007c:
            com.fossil.ie4 r2 = r9.a(r10)
            com.fossil.to4$e r6 = new com.fossil.to4$e
            r6.<init>(r9, r2, r5)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r2
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r6, r0)
            if (r10 != r1) goto L_0x0094
            return r1
        L_0x0094:
            r0 = r9
        L_0x0095:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r1 = r10 instanceof com.fossil.bj5
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x00b9
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r1 = r0.a
            java.lang.String r6 = "unregisterFCM - success"
            r10.e(r1, r6)
            com.fossil.ch5 r10 = r0.c
            r10.s(r2)
            com.fossil.ko4 r10 = new com.fossil.ko4
            java.lang.Boolean r0 = com.fossil.pb7.a(r4)
            r10.<init>(r0, r5, r3, r5)
            goto L_0x011b
        L_0x00b9:
            boolean r1 = r10 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x011c
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r6 = r0.a
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "unregisterFCMToken() failed, responseCode="
            r7.append(r8)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r8 = r10.a()
            r7.append(r8)
            java.lang.String r8 = ",serverError="
            r7.append(r8)
            com.portfolio.platform.data.model.ServerError r8 = r10.c()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r1.e(r6, r7)
            int r1 = r10.a()
            r6 = 404(0x194, float:5.66E-43)
            if (r1 != r6) goto L_0x0102
            com.fossil.ch5 r10 = r0.c
            r10.s(r2)
            com.fossil.ko4 r10 = new com.fossil.ko4
            java.lang.Boolean r0 = com.fossil.pb7.a(r4)
            r10.<init>(r0, r5, r3, r5)
            goto L_0x011b
        L_0x0102:
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            int r2 = r10.a()
            com.portfolio.platform.data.model.ServerError r10 = r10.c()
            if (r10 == 0) goto L_0x0114
            java.lang.String r5 = r10.getMessage()
        L_0x0114:
            r1.<init>(r2, r5)
            r0.<init>(r1)
            r10 = r0
        L_0x011b:
            return r10
        L_0x011c:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.to4.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, com.fossil.fb7<? super com.fossil.ko4<java.lang.Boolean>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.to4.a
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.to4$a r0 = (com.fossil.to4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.to4$a r0 = new com.fossil.to4$a
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 0
            r5 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r5) goto L_0x0037
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r0 = r0.L$0
            com.fossil.to4 r0 = (com.fossil.to4) r0
            com.fossil.t87.a(r10)
            goto L_0x0092
        L_0x0037:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003f:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = r8.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "registerFCM - deviceToken: "
            r6.append(r7)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            r10.e(r2, r6)
            if (r9 == 0) goto L_0x0069
            int r10 = r9.length()
            if (r10 != 0) goto L_0x0067
            goto L_0x0069
        L_0x0067:
            r10 = 0
            goto L_0x006a
        L_0x0069:
            r10 = 1
        L_0x006a:
            if (r10 == 0) goto L_0x0079
            com.fossil.ko4 r9 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r10 = new com.portfolio.platform.data.model.ServerError
            java.lang.String r0 = "deviceToken is empty"
            r10.<init>(r3, r0)
            r9.<init>(r10)
            return r9
        L_0x0079:
            com.fossil.ie4 r10 = r8.a(r9)
            com.fossil.to4$b r2 = new com.fossil.to4$b
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r5
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x0091
            return r1
        L_0x0091:
            r0 = r8
        L_0x0092:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r1 = r10 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00c9
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r1 = r0.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r6 = "registerFCM -  success - deviceToken: "
            r2.append(r6)
            r2.append(r9)
            java.lang.String r2 = r2.toString()
            r10.e(r1, r2)
            com.fossil.ch5 r10 = r0.c
            r10.s(r9)
            com.fossil.ch5 r9 = r0.c
            r9.x(r3)
            com.fossil.ko4 r9 = new com.fossil.ko4
            java.lang.Boolean r10 = com.fossil.pb7.a(r5)
            r0 = 2
            r9.<init>(r10, r4, r0, r4)
            goto L_0x0107
        L_0x00c9:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0108
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "registerFCM -  error: "
            r1.append(r2)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.e(r0, r1)
            com.fossil.ko4 r9 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r10 = r10.c()
            if (r10 == 0) goto L_0x0101
            java.lang.String r4 = r10.getMessage()
        L_0x0101:
            r0.<init>(r1, r4)
            r9.<init>(r0)
        L_0x0107:
            return r9
        L_0x0108:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.to4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.ko4<java.lang.Boolean>> r9) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof com.fossil.to4.c
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.to4$c r0 = (com.fossil.to4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.to4$c r0 = new com.fossil.to4$c
            r0.<init>(r8, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r1 = r0.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.to4 r0 = (com.fossil.to4) r0
            com.fossil.t87.a(r9)
            goto L_0x0093
        L_0x0035:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x003d:
            com.fossil.t87.a(r9)
            com.fossil.ch5 r9 = r8.c
            java.lang.String r9 = r9.j()
            com.fossil.ch5 r2 = r8.c
            java.lang.String r2 = r2.g()
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r8.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "retry - consider - deviceToken: "
            r6.append(r7)
            r6.append(r9)
            java.lang.String r7 = " - authorisedToken: "
            r6.append(r7)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            r4.e(r5, r6)
            boolean r4 = com.fossil.ee7.a(r9, r2)
            r4 = r4 ^ r3
            if (r4 == 0) goto L_0x0096
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r8.a
            java.lang.String r6 = "do retry"
            r4.e(r5, r6)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r2
            r0.label = r3
            java.lang.Object r9 = r8.a(r9, r0)
            if (r9 != r1) goto L_0x0093
            return r1
        L_0x0093:
            com.fossil.ko4 r9 = (com.fossil.ko4) r9
            goto L_0x00bd
        L_0x0096:
            r0 = 0
            if (r9 == 0) goto L_0x00a2
            int r9 = r9.length()
            if (r9 != 0) goto L_0x00a0
            goto L_0x00a2
        L_0x00a0:
            r9 = 0
            goto L_0x00a3
        L_0x00a2:
            r9 = 1
        L_0x00a3:
            if (r9 == 0) goto L_0x00b2
            com.fossil.ko4 r9 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            java.lang.String r2 = "deviceToken is empty"
            r1.<init>(r0, r2)
            r9.<init>(r1)
            goto L_0x00bd
        L_0x00b2:
            com.fossil.ko4 r9 = new com.fossil.ko4
            java.lang.Boolean r0 = com.fossil.pb7.a(r3)
            r1 = 2
            r2 = 0
            r9.<init>(r0, r2, r1, r2)
        L_0x00bd:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.to4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final ie4 a(String str) {
        ie4 ie4 = new ie4();
        ie4.a(this.b, str);
        return ie4;
    }
}
