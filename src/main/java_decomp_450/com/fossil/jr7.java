package com.fossil;

import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr7 implements qr7 {
    @DexIgnore
    public /* final */ OutputStream a;
    @DexIgnore
    public /* final */ tr7 b;

    @DexIgnore
    public jr7(OutputStream outputStream, tr7 tr7) {
        ee7.b(outputStream, "out");
        ee7.b(tr7, "timeout");
        this.a = outputStream;
        this.b = tr7;
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public void a(yq7 yq7, long j) {
        ee7.b(yq7, "source");
        vq7.a(yq7.x(), 0, j);
        while (j > 0) {
            this.b.e();
            nr7 nr7 = yq7.a;
            if (nr7 != null) {
                int min = (int) Math.min(j, (long) (nr7.c - nr7.b));
                this.a.write(nr7.a, nr7.b, min);
                nr7.b += min;
                long j2 = (long) min;
                j -= j2;
                yq7.j(yq7.x() - j2);
                if (nr7.b == nr7.c) {
                    yq7.a = nr7.b();
                    or7.c.a(nr7);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public tr7 d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.qr7, java.io.Flushable
    public void flush() {
        this.a.flush();
    }

    @DexIgnore
    public String toString() {
        return "sink(" + this.a + ')';
    }
}
