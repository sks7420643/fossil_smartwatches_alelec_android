package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rw3<K, V> extends sw3<K, V> implements ty3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 6588350623831699109L;

    @DexIgnore
    public rw3(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    @Override // com.fossil.sw3
    public abstract List<V> createCollection();

    @DexIgnore
    @Override // com.fossil.vw3
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.vw3, com.fossil.zy3
    @CanIgnoreReturnValue
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    @Override // com.fossil.sw3
    public List<V> createUnmodifiableEmptyCollection() {
        return zx3.of();
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.zy3
    public List<V> get(K k) {
        return (List) super.get((Object) k);
    }

    @DexIgnore
    @Override // com.fossil.sw3
    @CanIgnoreReturnValue
    public List<V> removeAll(Object obj) {
        return (List) super.removeAll(obj);
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.vw3
    @CanIgnoreReturnValue
    public List<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (List) super.replaceValues((Object) k, (Iterable) iterable);
    }
}
