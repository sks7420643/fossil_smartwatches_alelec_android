package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum uf0 {
    IN_PROGRESS("in_progress"),
    END("end"),
    ERROR("error"),
    SUCCESS("success");
    
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public uf0(String str) {
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }
}
