package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface on7 {
    @DexIgnore
    public static final on7 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements on7 {
        @DexIgnore
        @Override // com.fossil.on7
        public lo7 authenticate(no7 no7, Response response) {
            return null;
        }
    }

    @DexIgnore
    lo7 authenticate(no7 no7, Response response) throws IOException;
}
