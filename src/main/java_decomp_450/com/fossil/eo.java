package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eo<T> {
    @DexIgnore
    public static /* final */ String f; // = im.a("ConstraintTracker");
    @DexIgnore
    public /* final */ vp a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public /* final */ Set<pn<T>> d; // = new LinkedHashSet();
    @DexIgnore
    public T e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List a;

        @DexIgnore
        public a(List list) {
            this.a = list;
        }

        @DexIgnore
        public void run() {
            for (pn pnVar : this.a) {
                pnVar.a(eo.this.e);
            }
        }
    }

    @DexIgnore
    public eo(Context context, vp vpVar) {
        this.b = context.getApplicationContext();
        this.a = vpVar;
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(pn<T> pnVar) {
        synchronized (this.c) {
            if (this.d.add(pnVar)) {
                if (this.d.size() == 1) {
                    this.e = a();
                    im.a().a(f, String.format("%s: initial state = %s", getClass().getSimpleName(), this.e), new Throwable[0]);
                    b();
                }
                pnVar.a(this.e);
            }
        }
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public void b(pn<T> pnVar) {
        synchronized (this.c) {
            if (this.d.remove(pnVar) && this.d.isEmpty()) {
                c();
            }
        }
    }

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public void a(T t) {
        synchronized (this.c) {
            if (this.e != t) {
                if (this.e == null || !this.e.equals(t)) {
                    this.e = t;
                    this.a.a().execute(new a(new ArrayList(this.d)));
                }
            }
        }
    }
}
