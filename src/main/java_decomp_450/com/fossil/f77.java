package com.fossil;

import com.zendesk.service.ErrorResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f77 implements ErrorResponse {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public f77(String str) {
        this.a = str;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public int a() {
        return -1;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public boolean c() {
        return false;
    }
}
