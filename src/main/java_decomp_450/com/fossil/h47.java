package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h47 extends e47 {
    @DexIgnore
    public static String o;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;

    @DexIgnore
    public h47(Context context, int i, a47 a47) {
        super(context, i, a47);
        this.m = k47.a(context).b();
        if (o == null) {
            o = v57.l(context);
        }
    }

    @DexIgnore
    @Override // com.fossil.e47
    public f47 a() {
        return f47.h;
    }

    @DexIgnore
    public void a(String str) {
        this.n = str;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public boolean a(JSONObject jSONObject) {
        a67.a(jSONObject, "op", o);
        a67.a(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }
}
