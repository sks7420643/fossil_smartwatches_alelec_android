package com.fossil;

import com.facebook.internal.FileLruCache;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iv2 extends qu2 {
    @DexIgnore
    public static /* final */ Logger b; // = Logger.getLogger(iv2.class.getName());
    @DexIgnore
    public static /* final */ boolean c; // = bz2.a();
    @DexIgnore
    public kv2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends iv2 {
        @DexIgnore
        public /* final */ byte[] d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public a(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i2 + 0;
                if ((i2 | 0 | (bArr.length - i3)) >= 0) {
                    this.d = bArr;
                    this.f = 0;
                    this.e = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)));
            }
            throw new NullPointerException(FileLruCache.BufferFile.FILE_NAME_PREFIX);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, int i2) throws IOException {
            b((i << 3) | i2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void b(int i, int i2) throws IOException {
            a(i, 0);
            a(i2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void c(int i, int i2) throws IOException {
            a(i, 0);
            b(i2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void d(int i) throws IOException {
            try {
                byte[] bArr = this.d;
                int i2 = this.f;
                int i3 = i2 + 1;
                this.f = i3;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.d;
                int i4 = i3 + 1;
                this.f = i4;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.d;
                int i5 = i4 + 1;
                this.f = i5;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.d;
                this.f = i5 + 1;
                bArr4[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void e(int i, int i2) throws IOException {
            a(i, 5);
            d(i2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, long j) throws IOException {
            a(i, 0);
            a(j);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void b(byte[] bArr, int i, int i2) throws IOException {
            b(i2);
            c(bArr, 0, i2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void c(int i, long j) throws IOException {
            a(i, 1);
            c(j);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, boolean z) throws IOException {
            a(i, 0);
            a(z ? (byte) 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void b(int i, tu2 tu2) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, tu2);
            a(1, 4);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void c(long j) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                int i2 = i + 1;
                this.f = i2;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.d;
                int i3 = i2 + 1;
                this.f = i3;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.d;
                int i4 = i3 + 1;
                this.f = i4;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.d;
                int i5 = i4 + 1;
                this.f = i5;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.d;
                int i6 = i5 + 1;
                this.f = i6;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.d;
                int i7 = i6 + 1;
                this.f = i7;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.d;
                int i8 = i7 + 1;
                this.f = i8;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.d;
                this.f = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, String str) throws IOException {
            a(i, 2);
            a(str);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, tu2 tu2) throws IOException {
            a(i, 2);
            a(tu2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void b(int i) throws IOException {
            if (!iv2.c || mu2.a() || a() < 5) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.d;
                    int i2 = this.f;
                    this.f = i2 + 1;
                    bArr[i2] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i3 = this.f;
                    this.f = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e2) {
                    throw new b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
                }
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                bz2.a(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.d;
                int i5 = this.f;
                this.f = i5 + 1;
                bz2.a(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.d;
                    int i7 = this.f;
                    this.f = i7 + 1;
                    bz2.a(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.d;
                int i8 = this.f;
                this.f = i8 + 1;
                bz2.a(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.d;
                    int i10 = this.f;
                    this.f = i10 + 1;
                    bz2.a(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.d;
                int i11 = this.f;
                this.f = i11 + 1;
                bz2.a(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.d;
                    int i13 = this.f;
                    this.f = i13 + 1;
                    bz2.a(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.d;
                int i14 = this.f;
                this.f = i14 + 1;
                bz2.a(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.d;
                int i15 = this.f;
                this.f = i15 + 1;
                bz2.a(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(tu2 tu2) throws IOException {
            b(tu2.zza());
            tu2.zza(this);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, jx2 jx2, cy2 cy2) throws IOException {
            a(i, 2);
            ju2 ju2 = (ju2) jx2;
            int c = ju2.c();
            if (c == -1) {
                c = cy2.zzb(ju2);
                ju2.a(c);
            }
            b(c);
            cy2.a(jx2, ((iv2) this).a);
        }

        @DexIgnore
        public final void c(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.d, this.f, i2);
                this.f += i2;
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), Integer.valueOf(i2)), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i, jx2 jx2) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, 2);
            a(jx2);
            a(1, 4);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(jx2 jx2) throws IOException {
            b(jx2.k());
            jx2.a(this);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(byte b) throws IOException {
            try {
                byte[] bArr = this.d;
                int i = this.f;
                this.f = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(int i) throws IOException {
            if (i >= 0) {
                b(i);
            } else {
                a((long) i);
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(long j) throws IOException {
            if (!iv2.c || a() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.d;
                    int i = this.f;
                    this.f = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.d;
                    int i2 = this.f;
                    this.f = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e2) {
                    throw new b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f), Integer.valueOf(this.e), 1), e2);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.d;
                    int i3 = this.f;
                    this.f = i3 + 1;
                    bz2.a(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.d;
                int i4 = this.f;
                this.f = i4 + 1;
                bz2.a(bArr4, (long) i4, (byte) ((int) j));
            }
        }

        @DexIgnore
        @Override // com.fossil.qu2
        public final void a(byte[] bArr, int i, int i2) throws IOException {
            c(bArr, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final void a(String str) throws IOException {
            int i = this.f;
            try {
                int g = iv2.g(str.length() * 3);
                int g2 = iv2.g(str.length());
                if (g2 == g) {
                    int i2 = i + g2;
                    this.f = i2;
                    int a = dz2.a(str, this.d, i2, a());
                    this.f = i;
                    b((a - i) - g2);
                    this.f = a;
                    return;
                }
                b(dz2.a(str));
                this.f = dz2.a(str, this.d, this.f, a());
            } catch (hz2 e2) {
                this.f = i;
                a(str, e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new b(e3);
            }
        }

        @DexIgnore
        @Override // com.fossil.iv2
        public final int a() {
            return this.e - this.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends IOException {
        @DexIgnore
        public b() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        @DexIgnore
        public b(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public b(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv2.b.<init>(java.lang.String, java.lang.Throwable):void");
        }
    }

    @DexIgnore
    public iv2() {
    }

    @DexIgnore
    public static iv2 a(byte[] bArr) {
        return new a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static int b(double d) {
        return 8;
    }

    @DexIgnore
    public static int b(float f) {
        return 4;
    }

    @DexIgnore
    public static int b(boolean z) {
        return 1;
    }

    @DexIgnore
    public static int e(int i, long j) {
        return e(i) + e(j);
    }

    @DexIgnore
    public static int e(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    @DexIgnore
    public static int f(int i, int i2) {
        return e(i) + f(i2);
    }

    @DexIgnore
    public static int g(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    @DexIgnore
    public static int g(int i, int i2) {
        return e(i) + g(i2);
    }

    @DexIgnore
    public static int g(long j) {
        return 8;
    }

    @DexIgnore
    public static int h(int i, int i2) {
        return e(i) + g(m(i2));
    }

    @DexIgnore
    public static int h(long j) {
        return 8;
    }

    @DexIgnore
    public static int i(int i) {
        return 4;
    }

    @DexIgnore
    public static int i(int i, int i2) {
        return e(i) + 4;
    }

    @DexIgnore
    public static long i(long j) {
        return (j >> 63) ^ (j << 1);
    }

    @DexIgnore
    public static int j(int i) {
        return 4;
    }

    @DexIgnore
    public static int j(int i, int i2) {
        return e(i) + 4;
    }

    @DexIgnore
    public static int k(int i, int i2) {
        return e(i) + f(i2);
    }

    @DexIgnore
    @Deprecated
    public static int l(int i) {
        return g(i);
    }

    @DexIgnore
    public static int m(int i) {
        return (i >> 31) ^ (i << 1);
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract void a(byte b2) throws IOException;

    @DexIgnore
    public abstract void a(int i) throws IOException;

    @DexIgnore
    public abstract void a(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void a(int i, long j) throws IOException;

    @DexIgnore
    public abstract void a(int i, jx2 jx2) throws IOException;

    @DexIgnore
    public abstract void a(int i, jx2 jx2, cy2 cy2) throws IOException;

    @DexIgnore
    public abstract void a(int i, tu2 tu2) throws IOException;

    @DexIgnore
    public abstract void a(int i, String str) throws IOException;

    @DexIgnore
    public abstract void a(int i, boolean z) throws IOException;

    @DexIgnore
    public abstract void a(long j) throws IOException;

    @DexIgnore
    public abstract void a(jx2 jx2) throws IOException;

    @DexIgnore
    public abstract void a(tu2 tu2) throws IOException;

    @DexIgnore
    public abstract void a(String str) throws IOException;

    @DexIgnore
    public abstract void b(int i) throws IOException;

    @DexIgnore
    public abstract void b(int i, int i2) throws IOException;

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        a(i, i(j));
    }

    @DexIgnore
    public abstract void b(int i, tu2 tu2) throws IOException;

    @DexIgnore
    public abstract void b(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    public final void c(int i) throws IOException {
        b(m(i));
    }

    @DexIgnore
    public abstract void c(int i, int i2) throws IOException;

    @DexIgnore
    public abstract void c(int i, long j) throws IOException;

    @DexIgnore
    public abstract void c(long j) throws IOException;

    @DexIgnore
    public abstract void d(int i) throws IOException;

    @DexIgnore
    public final void d(int i, int i2) throws IOException {
        c(i, m(i2));
    }

    @DexIgnore
    public abstract void e(int i, int i2) throws IOException;

    @DexIgnore
    public static int c(int i, tu2 tu2) {
        int e = e(i);
        int zza = tu2.zza();
        return e + g(zza) + zza;
    }

    @DexIgnore
    public static int d(int i, long j) {
        return e(i) + e(j);
    }

    @DexIgnore
    public static int e(int i) {
        return g(i << 3);
    }

    @DexIgnore
    public static int f(int i, long j) {
        return e(i) + e(i(j));
    }

    @DexIgnore
    public static int g(int i, long j) {
        return e(i) + 8;
    }

    @DexIgnore
    public final void b(long j) throws IOException {
        a(i(j));
    }

    @DexIgnore
    public static int b(int i, float f) {
        return e(i) + 4;
    }

    @DexIgnore
    public static int h(int i, long j) {
        return e(i) + 8;
    }

    @DexIgnore
    public static int k(int i) {
        return f(i);
    }

    @DexIgnore
    public final void a(int i, float f) throws IOException {
        e(i, Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public static int b(int i, double d) {
        return e(i) + 8;
    }

    @DexIgnore
    public static int d(int i, tu2 tu2) {
        return (e(1) << 1) + g(2, i) + c(3, tu2);
    }

    @DexIgnore
    public static int f(int i) {
        if (i >= 0) {
            return g(i);
        }
        return 10;
    }

    @DexIgnore
    public static int h(int i) {
        return g(m(i));
    }

    @DexIgnore
    public final void a(int i, double d) throws IOException {
        c(i, Double.doubleToRawLongBits(d));
    }

    @DexIgnore
    public static int b(int i, boolean z) {
        return e(i) + 1;
    }

    @DexIgnore
    @Deprecated
    public static int c(int i, jx2 jx2, cy2 cy2) {
        int e = e(i) << 1;
        ju2 ju2 = (ju2) jx2;
        int c2 = ju2.c();
        if (c2 == -1) {
            c2 = cy2.zzb(ju2);
            ju2.a(c2);
        }
        return e + c2;
    }

    @DexIgnore
    public static int f(long j) {
        return e(i(j));
    }

    @DexIgnore
    public final void a(float f) throws IOException {
        d(Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public static int b(int i, String str) {
        return e(i) + b(str);
    }

    @DexIgnore
    public final void a(double d) throws IOException {
        c(Double.doubleToRawLongBits(d));
    }

    @DexIgnore
    public static int b(int i, jx2 jx2, cy2 cy2) {
        return e(i) + a(jx2, cy2);
    }

    @DexIgnore
    public static int d(long j) {
        return e(j);
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        a(z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static int a(int i, rw2 rw2) {
        int e = e(i);
        int a2 = rw2.a();
        return e + g(a2) + a2;
    }

    @DexIgnore
    public static int b(int i, jx2 jx2) {
        return (e(1) << 1) + g(2, i) + e(3) + b(jx2);
    }

    @DexIgnore
    @Deprecated
    public static int c(jx2 jx2) {
        return jx2.k();
    }

    @DexIgnore
    public static int a(rw2 rw2) {
        int a2 = rw2.a();
        return g(a2) + a2;
    }

    @DexIgnore
    public static int b(int i, rw2 rw2) {
        return (e(1) << 1) + g(2, i) + a(3, rw2);
    }

    @DexIgnore
    public static int a(jx2 jx2, cy2 cy2) {
        ju2 ju2 = (ju2) jx2;
        int c2 = ju2.c();
        if (c2 == -1) {
            c2 = cy2.zzb(ju2);
            ju2.a(c2);
        }
        return g(c2) + c2;
    }

    @DexIgnore
    public static int b(String str) {
        int i;
        try {
            i = dz2.a(str);
        } catch (hz2 unused) {
            i = str.getBytes(ew2.a).length;
        }
        return g(i) + i;
    }

    @DexIgnore
    public static int b(tu2 tu2) {
        int zza = tu2.zza();
        return g(zza) + zza;
    }

    @DexIgnore
    public final void a(String str, hz2 hz2) throws IOException {
        b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) hz2);
        byte[] bytes = str.getBytes(ew2.a);
        try {
            b(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new b(e);
        } catch (b e2) {
            throw e2;
        }
    }

    @DexIgnore
    public static int b(byte[] bArr) {
        int length = bArr.length;
        return g(length) + length;
    }

    @DexIgnore
    public static int b(jx2 jx2) {
        int k = jx2.k();
        return g(k) + k;
    }

    @DexIgnore
    public final void b() {
        if (a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }
}
