package com.fossil;

import android.database.sqlite.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fj extends ej implements aj {
    @DexIgnore
    public /* final */ SQLiteStatement b;

    @DexIgnore
    public fj(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.b = sQLiteStatement;
    }

    @DexIgnore
    @Override // com.fossil.aj
    public long executeInsert() {
        return this.b.executeInsert();
    }

    @DexIgnore
    @Override // com.fossil.aj
    public int executeUpdateDelete() {
        return this.b.executeUpdateDelete();
    }
}
