package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lg5 {
    @DexIgnore
    public static /* final */ String b; // = ("Localization_" + lg5.class.getSimpleName());
    @DexIgnore
    public Map<String, String> a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public a(lg5 lg5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TypeToken<HashMap<String, String>> {
        @DexIgnore
        public b(lg5 lg5) {
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "parseJSONFile() called with: filePath = [" + str + "], isDownloaded = [" + z + "]");
        try {
            Gson gson = new Gson();
            if (!z) {
                InputStream open = PortfolioApp.c0.getAssets().open(str);
                InputStreamReader inputStreamReader = new InputStreamReader(open, "UTF-8");
                try {
                    this.a.putAll((Map) gson.a((Reader) inputStreamReader, new a(this).getType()));
                    inputStreamReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    inputStreamReader.close();
                } catch (Throwable th) {
                    inputStreamReader.close();
                    open.close();
                    throw th;
                }
                open.close();
                return;
            }
            FileReader fileReader = new FileReader(new File(str));
            try {
                this.a.putAll((Map) gson.a((Reader) fileReader, new b(this).getType()));
            } catch (Exception e2) {
                e2.printStackTrace();
            } catch (Throwable th2) {
                fileReader.close();
                throw th2;
            }
            fileReader.close();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.e(str3, "parseJSONFile failed exception=" + e3);
        }
    }

    @DexIgnore
    public Map<String, String> b() {
        return this.a;
    }

    @DexIgnore
    public void a() {
        Map<String, String> map = this.a;
        if (map != null) {
            map.clear();
        }
    }

    @DexIgnore
    public String a(String str) {
        return (this.a == null || TextUtils.isEmpty(str)) ? "" : this.a.get(str);
    }
}
