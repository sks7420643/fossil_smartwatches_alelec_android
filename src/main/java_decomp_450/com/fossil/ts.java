package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ts {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ts() {
    }

    @DexIgnore
    public Object a(Drawable drawable, zt ztVar, fb7<? super i97> fb7) {
        return i97.a;
    }

    @DexIgnore
    public Object a(Drawable drawable, boolean z, zt ztVar, fb7<? super i97> fb7) {
        return i97.a;
    }

    @DexIgnore
    public void a(BitmapDrawable bitmapDrawable, Drawable drawable) {
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public /* synthetic */ ts(zd7 zd7) {
        this();
    }
}
