package com.fossil;

import com.fossil.c24;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qd4 implements vd4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ rd4 b;

    @DexIgnore
    public qd4(Set<td4> set, rd4 rd4) {
        this.a = a(set);
        this.b = rd4;
    }

    @DexIgnore
    public static c24<vd4> b() {
        c24.b a2 = c24.a(vd4.class);
        a2.a(m24.d(td4.class));
        a2.a(pd4.a());
        return a2.b();
    }

    @DexIgnore
    @Override // com.fossil.vd4
    public String a() {
        if (this.b.a().isEmpty()) {
            return this.a;
        }
        return this.a + ' ' + a(this.b.a());
    }

    @DexIgnore
    public static String a(Set<td4> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<td4> it = set.iterator();
        while (it.hasNext()) {
            td4 next = it.next();
            sb.append(next.a());
            sb.append('/');
            sb.append(next.b());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static /* synthetic */ vd4 a(d24 d24) {
        return new qd4(d24.c(td4.class), rd4.b());
    }
}
