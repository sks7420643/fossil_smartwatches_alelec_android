package com.fossil;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc1<T> extends CopyOnWriteArrayList<T> {
    @DexIgnore
    public /* final */ Comparator<T> a;

    @DexIgnore
    public xc1(Comparator<T> comparator) {
        this.a = comparator;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Comparator<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final int a(T t) {
        synchronized (this) {
            int size = super.size();
            for (int i = 0; i < size; i++) {
                Object a2 = ea7.a((List) this, i);
                if (a2 == null || this.a.compare(t, a2) > 0) {
                    return i;
                }
            }
            return super.size();
        }
    }

    @DexIgnore
    public final boolean b(T t) {
        synchronized (this) {
            if (indexOf(t) < 0) {
                try {
                    add(a(t), t);
                } catch (IndexOutOfBoundsException unused) {
                    add(t);
                }
            }
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.List, java.util.concurrent.CopyOnWriteArrayList
    public final T remove(int i) {
        return (T) super.remove(i);
    }

    @DexIgnore
    public final int size() {
        return super.size();
    }
}
