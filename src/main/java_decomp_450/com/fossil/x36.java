package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.e46;
import com.fossil.to5;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x36 extends go5 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public e46 f;
    @DexIgnore
    public rj4 g;
    @DexIgnore
    public to5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final x36 a(String str) {
            ee7.b(str, MicroAppSetting.SETTING);
            x36 x36 = new x36();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_SETTING, str);
            x36.setArguments(bundle);
            return x36;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements to5.b {
        @DexIgnore
        public /* final */ /* synthetic */ x36 a;

        @DexIgnore
        public b(x36 x36) {
            this.a = x36;
        }

        @DexIgnore
        @Override // com.fossil.to5.b
        public void a(AddressWrapper addressWrapper) {
            ee7.b(addressWrapper, "address");
            this.a.a(addressWrapper, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends sz6 {
        @DexIgnore
        public /* final */ /* synthetic */ x36 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, int i2, x36 x36) {
            super(i, i2);
            this.f = x36;
        }

        @DexIgnore
        @Override // com.fossil.qg.f
        public void b(RecyclerView.ViewHolder viewHolder, int i) {
            ee7.b(viewHolder, "viewHolder");
            int adapterPosition = viewHolder.getAdapterPosition();
            to5 b = this.f.h;
            x36.c(this.f).b(b != null ? b.a(adapterPosition) : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x36 a;

        @DexIgnore
        public d(x36 x36) {
            this.a = x36;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x36 a;

        @DexIgnore
        public e(x36 x36) {
            this.a = x36;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<e46.b> {
        @DexIgnore
        public /* final */ /* synthetic */ x36 a;

        @DexIgnore
        public f(x36 x36) {
            this.a = x36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(e46.b bVar) {
            List<AddressWrapper> a2 = bVar.a();
            if (a2 != null) {
                this.a.x(a2);
            }
        }
    }

    /*
    static {
        ee7.a((Object) x36.class.getSimpleName(), "CommuteTimeWatchAppSetti\u2026nt::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ e46 c(x36 x36) {
        e46 e46 = x36.f;
        if (e46 != null) {
            return e46;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        h(true);
        return true;
    }

    @DexIgnore
    public final void f1() {
        e46 e46 = this.f;
        if (e46 == null) {
            ee7.d("mViewModel");
            throw null;
        } else if (e46.d()) {
            a(new AddressWrapper(), true);
        } else {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.v(childFragmentManager);
        }
    }

    @DexIgnore
    public final void h(boolean z) {
        if (z) {
            Intent intent = new Intent();
            e46 e46 = this.f;
            if (e46 != null) {
                intent.putExtra("COMMUTE_TIME_WATCH_APP_SETTING", e46.b());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 113) {
            AddressWrapper addressWrapper = (AddressWrapper) intent.getParcelableExtra("KEY_SELECTED_ADDRESS");
            e46 e46 = this.f;
            if (e46 != null) {
                e46.a(addressWrapper);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().y().a(this);
        rj4 rj4 = this.g;
        String str = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(e46.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ngsViewModel::class.java)");
            e46 e46 = (e46) a2;
            this.f = e46;
            if (e46 != null) {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    str = arguments.getString(Constants.USER_SETTING);
                }
                e46.a(str);
                return;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        oy4 oy4 = (oy4) qb.a(layoutInflater, 2131558518, viewGroup, false, a1());
        oy4.s.setOnClickListener(new d(this));
        oy4.r.setOnClickListener(new e(this));
        to5 to5 = new to5();
        to5.a(new b(this));
        this.h = to5;
        RecyclerView recyclerView = oy4.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        new qg(new c(0, 4, this)).a(recyclerView);
        e46 e46 = this.f;
        if (e46 != null) {
            e46.c().a(getViewLifecycleOwner(), new f(this));
            new qw6(this, oy4);
            ee7.a((Object) oy4, "binding");
            return oy4.d();
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        e46 e46 = this.f;
        if (e46 != null) {
            e46.e();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void x(List<AddressWrapper> list) {
        to5 to5 = this.h;
        if (to5 != null) {
            to5.a(ea7.d((Collection) list));
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY_SELECTED_ADDRESS", addressWrapper);
        e46 e46 = this.f;
        ArrayList<String> arrayList = null;
        if (e46 != null) {
            CommuteTimeWatchAppSetting a2 = e46.a();
            if (a2 != null) {
                arrayList = a2.getListAddressNameExceptOf(addressWrapper);
            }
            bundle.putStringArrayList("KEY_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_HAVING_MAP_RESULT", !z);
            CommuteTimeSettingsDetailActivity.y.a(this, bundle, 113);
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }
}
