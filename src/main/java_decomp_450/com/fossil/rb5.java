package com.fossil;

import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum rb5 {
    CLEAR_DAY(WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY, "clear-day"),
    CLEAR_NIGHT(WeatherComplicationAppInfo.WeatherCondition.CLEAR_NIGHT, "clear-night"),
    RAIN(WeatherComplicationAppInfo.WeatherCondition.RAIN, "rain"),
    SNOW(WeatherComplicationAppInfo.WeatherCondition.SNOW, "snow"),
    SLEET(WeatherComplicationAppInfo.WeatherCondition.SLEET, "sleet"),
    WIND(WeatherComplicationAppInfo.WeatherCondition.WIND, "wind"),
    FOG(WeatherComplicationAppInfo.WeatherCondition.FOG, "fog"),
    CLOUDY(WeatherComplicationAppInfo.WeatherCondition.CLOUDY, "cloudy"),
    PARTLY_CLOUDY_DAY(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_DAY, "partly-cloudy-day"),
    PARTLY_CLOUDY_NIGHT(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_NIGHT, "partly-cloudy-night");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ String mConditionDescription;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition mConditionValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final WeatherComplicationAppInfo.WeatherCondition a(String str) {
            rb5 rb5;
            WeatherComplicationAppInfo.WeatherCondition access$getMConditionValue$p;
            rb5[] values = rb5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    rb5 = null;
                    break;
                }
                rb5 = values[i];
                if (ee7.a((Object) rb5.mConditionDescription, (Object) str)) {
                    break;
                }
                i++;
            }
            return (rb5 == null || (access$getMConditionValue$p = rb5.mConditionValue) == null) ? WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY : access$getMConditionValue$p;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public rb5(WeatherComplicationAppInfo.WeatherCondition weatherCondition, String str) {
        this.mConditionValue = weatherCondition;
        this.mConditionDescription = str;
    }

    @DexIgnore
    public static final WeatherComplicationAppInfo.WeatherCondition getConditionValue(String str) {
        return Companion.a(str);
    }
}
