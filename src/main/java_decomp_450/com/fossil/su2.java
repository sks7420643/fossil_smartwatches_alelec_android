package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su2 extends uu2 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public /* final */ int b; // = this.c.zza();
    @DexIgnore
    public /* final */ /* synthetic */ tu2 c;

    @DexIgnore
    public su2(tu2 tu2) {
        this.c = tu2;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a < this.b;
    }

    @DexIgnore
    @Override // com.fossil.yu2
    public final byte zza() {
        int i = this.a;
        if (i < this.b) {
            this.a = i + 1;
            return this.c.zzb(i);
        }
        throw new NoSuchElementException();
    }
}
