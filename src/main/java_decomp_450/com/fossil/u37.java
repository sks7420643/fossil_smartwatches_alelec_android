package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u37 extends Exception {
    @DexIgnore
    public u37() {
    }

    @DexIgnore
    public u37(String str) {
        super(str);
    }

    @DexIgnore
    public u37(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public u37(Throwable th) {
        super(th);
    }
}
