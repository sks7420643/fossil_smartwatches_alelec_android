package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fb5 {
    FEMALE(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX),
    MALE("M"),
    OTHER("O");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final fb5 a(String str) {
            if (TextUtils.isEmpty(str)) {
                return fb5.OTHER;
            }
            fb5[] values = fb5.values();
            for (fb5 fb5 : values) {
                if (mh7.b(str, fb5.getValue(), true)) {
                    return fb5;
                }
            }
            return fb5.OTHER;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public fb5(String str) {
        this.value = str;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public String toString() {
        return this.value;
    }
}
