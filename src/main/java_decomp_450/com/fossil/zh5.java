package com.fossil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zh5 extends BaseDbProvider implements yh5 {
    @DexIgnore
    public static /* final */ String a; // = "zh5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zh5$a$a")
        /* renamed from: com.fossil.zh5$a$a  reason: collision with other inner class name */
        public class C0261a implements UpgradeCommand {
            @DexIgnore
            public C0261a(a aVar) {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                new b(sQLiteDatabase).execute(new Void[0]);
            }
        }

        @DexIgnore
        public a() {
            put(2, new C0261a(this));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public /* final */ SQLiteDatabase a;

        @DexIgnore
        public b(SQLiteDatabase sQLiteDatabase) {
            this.a = sQLiteDatabase;
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            try {
                FLogger.INSTANCE.getLocal().d(zh5.a, "Inside upgrade db from 1 to 2");
                Cursor query = this.a.query(true, "hourNotification", new String[]{"extraId", "createdAt", AppFilter.COLUMN_HOUR, AppFilter.COLUMN_IS_VIBRATION_ONLY}, null, null, null, null, null, null);
                List<xh5> arrayList = new ArrayList();
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex("extraId"));
                        String string2 = query.getString(query.getColumnIndex("createdAt"));
                        int i = query.getInt(query.getColumnIndex(AppFilter.COLUMN_HOUR));
                        int i2 = query.getInt(query.getColumnIndex(AppFilter.COLUMN_IS_VIBRATION_ONLY));
                        xh5 xh5 = new xh5();
                        xh5.b(string);
                        xh5.a(Long.valueOf(string2).longValue());
                        xh5.a(i);
                        boolean z = true;
                        if (i2 != 1) {
                            z = false;
                        }
                        xh5.a(z);
                        arrayList.add(xh5);
                        query.moveToNext();
                    }
                    query.close();
                }
                FLogger.INSTANCE.getLocal().d(zh5.a, "Inside upgrade db from 1 to 2, creating hour notification copy table");
                this.a.execSQL("CREATE TABLE hour_notification_copy (id VARCHAR PRIMARY KEY, extraId VARCHAR, hour INTEGER, createdAt VARCHAR, isVibrationOnly INTEGER, deviceFamily VARCHAR);");
                if (!arrayList.isEmpty()) {
                    arrayList = zh5.b(arrayList);
                }
                if (!arrayList.isEmpty()) {
                    for (xh5 xh52 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("extraId", xh52.c());
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(xh52.d()));
                        contentValues.put("createdAt", Long.valueOf(xh52.a()));
                        contentValues.put("deviceFamily", xh52.b());
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(xh52.f()));
                        contentValues.put("id", xh52.e());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = zh5.a;
                        local.d(str, "Insert new values " + contentValues + " into copy table");
                        this.a.insert("hour_notification_copy", null, contentValues);
                    }
                }
                this.a.execSQL("DROP TABLE hourNotification;");
                this.a.execSQL("ALTER TABLE hour_notification_copy RENAME TO hourNotification;");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = zh5.a;
                local2.e(str2, "Error inside " + zh5.a + ".upgrade - e=" + e);
            }
            return null;
        }
    }

    @DexIgnore
    public zh5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public static xh5 a(String str, List<xh5> list) {
        for (xh5 xh5 : list) {
            if (xh5.c().equalsIgnoreCase(str)) {
                return xh5;
            }
        }
        return null;
    }

    @DexIgnore
    public static List<xh5> b(List<xh5> list) {
        xh5 a2;
        ArrayList arrayList = new ArrayList();
        if (list != null && !list.isEmpty()) {
            MFDeviceFamily[] a3 = be5.o.a();
            for (MFDeviceFamily mFDeviceFamily : a3) {
                if (mFDeviceFamily != MFDeviceFamily.DEVICE_FAMILY_Q_MOTION) {
                    List<AppFilter> a4 = nx6.b.a(mFDeviceFamily);
                    List<ContactGroup> allContactGroups = ah5.p.a().b().getAllContactGroups(mFDeviceFamily.ordinal());
                    if (a4 != null && !a4.isEmpty()) {
                        for (AppFilter appFilter : a4) {
                            xh5 a5 = a(appFilter.getType(), list);
                            if (a5 != null) {
                                xh5 xh5 = new xh5(a5.d(), a5.f(), a5.c(), a5.b());
                                FLogger.INSTANCE.getLocal().d(a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + " Found hands setting of app filter=" + appFilter.getType());
                                xh5.a(mFDeviceFamily.name());
                                StringBuilder sb = new StringBuilder();
                                sb.append(appFilter.getType());
                                sb.append(mFDeviceFamily);
                                xh5.c(sb.toString());
                                arrayList.add(xh5);
                            }
                        }
                    }
                    if (allContactGroups != null && !allContactGroups.isEmpty()) {
                        for (ContactGroup contactGroup : allContactGroups) {
                            if (!(contactGroup.getContacts() == null || contactGroup.getContacts().isEmpty() || (a2 = a(String.valueOf(contactGroup.getContacts().get(0).getContactId()), list)) == null)) {
                                FLogger.INSTANCE.getLocal().d(a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + "Found hands setting of contactId=" + contactGroup.getContacts().get(0).getContactId());
                                xh5 xh52 = new xh5(a2.d(), a2.f(), a2.c(), a2.b());
                                xh52.a(mFDeviceFamily.name());
                                xh52.c(contactGroup.getContacts().get(0).getContactId() + mFDeviceFamily.name());
                                arrayList.add(xh52);
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final xh5 d(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "getHourNotificationByExtraIdOnly() id = " + str);
        try {
            QueryBuilder<xh5, Integer> queryBuilder = e().queryBuilder();
            queryBuilder.where().eq("extraId", str);
            xh5 queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst != null) {
                return queryForFirst;
            }
            FLogger.INSTANCE.getLocal().d(a, "getHourNotificationByExtraIdOnly() - notification is null - return default notification for this action");
            return new xh5(1, false, str, be5.o.a(PortfolioApp.c0.c()).name());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local2.e(str3, "Error inside " + a + ".getHourNotificationByExtraIdOnly - e=" + e);
            return new xh5(1, false, str, be5.o.a(PortfolioApp.c0.c()).name());
        }
    }

    @DexIgnore
    public final Dao<xh5, Integer> e() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(xh5.class);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{xh5.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new a();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    @Override // com.fossil.yh5
    public xh5 a(String str, String str2) {
        try {
            QueryBuilder<xh5, Integer> queryBuilder = e().queryBuilder();
            queryBuilder.where().eq("extraId", str).and().eq("deviceFamily", str2);
            xh5 queryForFirst = queryBuilder.queryForFirst();
            return queryForFirst == null ? new xh5(1, false, str, be5.o.a(PortfolioApp.c0.c()).name()) : queryForFirst;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local.e(str3, "Error inside " + a + ".getHourNotificationByExtraId - e=" + e);
            return d(str);
        }
    }
}
