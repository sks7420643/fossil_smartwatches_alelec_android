package com.fossil;

import android.content.Context;
import com.fossil.vu1;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu1 extends vu1 {
    @DexIgnore
    public Provider<Executor> a;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider c;
    @DexIgnore
    public Provider d;
    @DexIgnore
    public Provider e;
    @DexIgnore
    public Provider<rx1> f;
    @DexIgnore
    public Provider<dw1> g;
    @DexIgnore
    public Provider<pw1> h;
    @DexIgnore
    public Provider<qv1> i;
    @DexIgnore
    public Provider<jw1> j;
    @DexIgnore
    public Provider<nw1> p;
    @DexIgnore
    public Provider<uu1> q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements vu1.a {
        @DexIgnore
        public Context a;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.vu1.a
        public vu1 build() {
            c87.a(this.a, Context.class);
            return new gu1(this.a);
        }

        @DexIgnore
        @Override // com.fossil.vu1.a
        public b a(Context context) {
            c87.a(context);
            this.a = context;
            return this;
        }
    }

    @DexIgnore
    public static vu1.a c() {
        return new b();
    }

    @DexIgnore
    public final void a(Context context) {
        this.a = z77.a(mu1.a());
        Factory a2 = a87.a(context);
        this.b = a2;
        gv1 a3 = gv1.a(a2, dy1.a(), ey1.a());
        this.c = a3;
        this.d = z77.a(iv1.a(this.b, a3));
        this.e = yx1.a(this.b, vw1.a(), ww1.a());
        this.f = z77.a(sx1.a(dy1.a(), ey1.a(), xw1.a(), this.e));
        uv1 a4 = uv1.a(dy1.a());
        this.g = a4;
        wv1 a5 = wv1.a(this.b, this.f, a4, ey1.a());
        this.h = a5;
        Provider<Executor> provider = this.a;
        Provider provider2 = this.d;
        Provider<rx1> provider3 = this.f;
        this.i = rv1.a(provider, provider2, a5, provider3, provider3);
        Provider<Context> provider4 = this.b;
        Provider provider5 = this.d;
        Provider<rx1> provider6 = this.f;
        this.j = kw1.a(provider4, provider5, provider6, this.h, this.a, provider6, dy1.a());
        Provider<Executor> provider7 = this.a;
        Provider<rx1> provider8 = this.f;
        this.p = ow1.a(provider7, provider8, this.h, provider8);
        this.q = z77.a(wu1.a(dy1.a(), ey1.a(), this.i, this.j, this.p));
    }

    @DexIgnore
    @Override // com.fossil.vu1
    public uu1 b() {
        return this.q.get();
    }

    @DexIgnore
    public gu1(Context context) {
        a(context);
    }

    @DexIgnore
    @Override // com.fossil.vu1
    public sw1 a() {
        return this.f.get();
    }
}
