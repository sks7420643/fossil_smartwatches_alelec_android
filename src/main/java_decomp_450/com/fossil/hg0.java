package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg0 extends yg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hg0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                return new hg0(createByteArray);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hg0[] newArray(int i) {
            return new hg0[i];
        }
    }

    @DexIgnore
    public hg0(byte[] bArr) {
        super(bArr);
        if (e().a == cs1.c) {
            this.m = c()[0].a;
            return;
        }
        throw new IllegalArgumentException("Invalid Watch App data.".toString());
    }

    @DexIgnore
    @Override // com.fossil.yg0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.S0, yz0.a((int) d())), r51.M4, this.m);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hg0)) {
            return false;
        }
        hg0 hg0 = (hg0) obj;
        return !(ee7.a(this.m, hg0.m) ^ true) && d() == hg0.d();
    }

    @DexIgnore
    public final String getBundleId() {
        return this.m;
    }

    @DexIgnore
    public final byte[] getData() {
        return f();
    }

    @DexIgnore
    public int hashCode() {
        return Long.valueOf(d()).hashCode() + (this.m.hashCode() * 31);
    }
}
