package com.fossil;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ea7 extends da7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements hg7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable a;

        @DexIgnore
        public a(Iterable iterable) {
            this.a = iterable;
        }

        @DexIgnore
        @Override // com.fossil.hg7
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, T t) {
        ee7.b(iterable, "$this$contains");
        if (iterable instanceof Collection) {
            return ((Collection) iterable).contains(t);
        }
        return b(iterable, t) >= 0;
    }

    @DexIgnore
    public static final <T> int b(Iterable<? extends T> iterable, T t) {
        ee7.b(iterable, "$this$indexOf");
        if (iterable instanceof List) {
            return ((List) iterable).indexOf(t);
        }
        int i = 0;
        for (Object obj : iterable) {
            if (i < 0) {
                w97.c();
                throw null;
            } else if (ee7.a((Object) t, obj)) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> List<T> c(Iterable<? extends T> iterable, int i) {
        ArrayList arrayList;
        ee7.b(iterable, "$this$drop");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return n(iterable);
        } else {
            if (iterable instanceof Collection) {
                Collection collection = (Collection) iterable;
                int size = collection.size() - i;
                if (size <= 0) {
                    return w97.a();
                }
                if (size == 1) {
                    return v97.a(f(iterable));
                }
                arrayList = new ArrayList(size);
                if (iterable instanceof List) {
                    if (iterable instanceof RandomAccess) {
                        int size2 = collection.size();
                        while (i < size2) {
                            arrayList.add(((List) iterable).get(i));
                            i++;
                        }
                    } else {
                        ListIterator listIterator = ((List) iterable).listIterator(i);
                        while (listIterator.hasNext()) {
                            arrayList.add(listIterator.next());
                        }
                    }
                    return arrayList;
                }
            } else {
                arrayList = new ArrayList();
            }
            for (Object obj : iterable) {
                if (i2 >= i) {
                    arrayList.add(obj);
                } else {
                    i2++;
                }
            }
            return w97.b(arrayList);
        }
    }

    @DexIgnore
    public static final <T> T d(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$first");
        if (iterable instanceof List) {
            return (T) d((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            return (T) it.next();
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T> T e(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$firstOrNull");
        if (iterable instanceof List) {
            List list = (List) iterable;
            if (list.isEmpty()) {
                return null;
            }
            return (T) list.get(0);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        return (T) it.next();
    }

    @DexIgnore
    public static final <T> T f(Iterable<? extends T> iterable) {
        T t;
        ee7.b(iterable, "$this$last");
        if (iterable instanceof List) {
            return (T) f((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T t2 = (T) it.next();
            while (it.hasNext()) {
                t2 = (T) it.next();
            }
            return t;
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T> T g(Iterable<? extends T> iterable) {
        T t;
        ee7.b(iterable, "$this$lastOrNull");
        if (iterable instanceof List) {
            List list = (List) iterable;
            if (list.isEmpty()) {
                return null;
            }
            return (T) list.get(list.size() - 1);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        T t2 = (T) it.next();
        while (it.hasNext()) {
            t2 = (T) it.next();
        }
        return t;
    }

    @DexIgnore
    public static final <T> T h(List<? extends T> list) {
        ee7.b(list, "$this$single");
        int size = list.size();
        if (size == 0) {
            throw new NoSuchElementException("List is empty.");
        } else if (size == 1) {
            return (T) list.get(0);
        } else {
            throw new IllegalArgumentException("List has more than one element.");
        }
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T i(java.lang.Iterable<? extends T> r3) {
        /*
            java.lang.String r0 = "$this$min"
            com.fossil.ee7.b(r3, r0)
            java.util.Iterator r3 = r3.iterator()
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0011
            r3 = 0
            return r3
        L_0x0011:
            java.lang.Object r0 = r3.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
        L_0x0017:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r3.next()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r2 = r0.compareTo(r1)
            if (r2 <= 0) goto L_0x0017
            r0 = r1
            goto L_0x0017
        L_0x002b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ea7.i(java.lang.Iterable):java.lang.Comparable");
    }

    @DexIgnore
    public static final <T> T j(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$single");
        if (iterable instanceof List) {
            return (T) h((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T t = (T) it.next();
            if (!it.hasNext()) {
                return t;
            }
            throw new IllegalArgumentException("Collection has more than one element.");
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> List<T> k(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$sorted");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return n(iterable);
            }
            Object[] array = collection.toArray(new Comparable[0]);
            if (array == null) {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                Comparable[] comparableArr = (Comparable[]) array;
                if (comparableArr != null) {
                    s97.c(comparableArr);
                    return s97.b(comparableArr);
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> o = o(iterable);
            aa7.c(o);
            return o;
        }
    }

    @DexIgnore
    public static final int l(Iterable<Integer> iterable) {
        ee7.b(iterable, "$this$sum");
        int i = 0;
        for (Integer num : iterable) {
            i += num.intValue();
        }
        return i;
    }

    @DexIgnore
    public static final <T> HashSet<T> m(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$toHashSet");
        HashSet<T> hashSet = new HashSet<>(na7.a(x97.a(iterable, 12)));
        a((Iterable) iterable, (Collection) hashSet);
        return hashSet;
    }

    @DexIgnore
    public static final <T> List<T> n(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$toList");
        if (!(iterable instanceof Collection)) {
            return w97.b(o(iterable));
        }
        Collection collection = (Collection) iterable;
        int size = collection.size();
        if (size == 0) {
            return w97.a();
        }
        if (size != 1) {
            return d(collection);
        }
        return v97.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
    }

    @DexIgnore
    public static final <T> List<T> o(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$toMutableList");
        if (iterable instanceof Collection) {
            return d((Collection) iterable);
        }
        ArrayList arrayList = new ArrayList();
        a((Iterable) iterable, (Collection) arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T> Set<T> p(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$toMutableSet");
        if (iterable instanceof Collection) {
            return new LinkedHashSet((Collection) iterable);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        a((Iterable) iterable, (Collection) linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> Set<T> q(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$toSet");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return sa7.a();
            }
            if (size != 1) {
                LinkedHashSet linkedHashSet = new LinkedHashSet(na7.a(collection.size()));
                a((Iterable) iterable, (Collection) linkedHashSet);
                return linkedHashSet;
            }
            return ra7.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
        }
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        a((Iterable) iterable, (Collection) linkedHashSet2);
        return sa7.a((Set) linkedHashSet2);
    }

    @DexIgnore
    public static final <T> T a(List<? extends T> list, int i) {
        ee7.b(list, "$this$getOrNull");
        if (i < 0 || i > w97.a((List) list)) {
            return null;
        }
        return (T) list.get(i);
    }

    @DexIgnore
    public static final <T> int a(List<? extends T> list, T t) {
        ee7.b(list, "$this$indexOf");
        return list.indexOf(t);
    }

    @DexIgnore
    public static final <T> List<List<T>> b(Iterable<? extends T> iterable, int i) {
        ee7.b(iterable, "$this$chunked");
        return a(iterable, i, i, true);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T h(java.lang.Iterable<? extends T> r3) {
        /*
            java.lang.String r0 = "$this$max"
            com.fossil.ee7.b(r3, r0)
            java.util.Iterator r3 = r3.iterator()
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0011
            r3 = 0
            return r3
        L_0x0011:
            java.lang.Object r0 = r3.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
        L_0x0017:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r3.next()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r2 = r0.compareTo(r1)
            if (r2 >= 0) goto L_0x0017
            r0 = r1
            goto L_0x0017
        L_0x002b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ea7.h(java.lang.Iterable):java.lang.Comparable");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: java.util.Collection */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> a(Iterable<? extends T> iterable, Comparator<? super T> comparator) {
        ee7.b(iterable, "$this$sortedWith");
        ee7.b(comparator, "comparator");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return n(iterable);
            }
            Object[] array = collection.toArray(new Object[0]);
            if (array == null) {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                s97.a(array, comparator);
                return s97.b(array);
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> o = o(iterable);
            aa7.a(o, comparator);
            return o;
        }
    }

    @DexIgnore
    public static final <T> List<T> b(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        ee7.b(iterable, "$this$minus");
        ee7.b(iterable2, MessengerShareContentUtility.ELEMENTS);
        Collection a2 = x97.a(iterable2, iterable);
        if (a2.isEmpty()) {
            return n(iterable);
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : iterable) {
            if (!a2.contains(obj)) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final <T> T d(List<? extends T> list) {
        ee7.b(list, "$this$first");
        if (!list.isEmpty()) {
            return (T) list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    @DexIgnore
    public static final <T> T e(List<? extends T> list) {
        ee7.b(list, "$this$firstOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return (T) list.get(0);
    }

    @DexIgnore
    public static final <T> T g(List<? extends T> list) {
        ee7.b(list, "$this$lastOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return (T) list.get(list.size() - 1);
    }

    @DexIgnore
    public static final <T> T f(List<? extends T> list) {
        ee7.b(list, "$this$last");
        if (!list.isEmpty()) {
            return (T) list.get(w97.a((List) list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    @DexIgnore
    public static final <T> List<T> d(Iterable<? extends T> iterable, int i) {
        ee7.b(iterable, "$this$take");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return w97.a();
        } else {
            if (iterable instanceof Collection) {
                if (i >= ((Collection) iterable).size()) {
                    return n(iterable);
                }
                if (i == 1) {
                    return v97.a(d(iterable));
                }
            }
            ArrayList arrayList = new ArrayList(i);
            Iterator<? extends T> it = iterable.iterator();
            while (it.hasNext()) {
                arrayList.add(it.next());
                i2++;
                if (i2 == i) {
                    break;
                }
            }
            return w97.b(arrayList);
        }
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C a(Iterable<? extends T> iterable, C c) {
        ee7.b(iterable, "$this$toCollection");
        ee7.b(c, ShareConstants.DESTINATION);
        Iterator<? extends T> it = iterable.iterator();
        while (it.hasNext()) {
            c.add(it.next());
        }
        return c;
    }

    @DexIgnore
    public static final <T> List<T> b(Collection<? extends T> collection, Iterable<? extends T> iterable) {
        ee7.b(collection, "$this$plus");
        ee7.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            Collection collection2 = (Collection) iterable;
            ArrayList arrayList = new ArrayList(collection.size() + collection2.size());
            arrayList.addAll(collection);
            arrayList.addAll(collection2);
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList(collection);
        ba7.a((Collection) arrayList2, (Iterable) iterable);
        return arrayList2;
    }

    @DexIgnore
    public static final <T> List<T> a(Collection<? extends T> collection, T t) {
        ee7.b(collection, "$this$plus");
        ArrayList arrayList = new ArrayList(collection.size() + 1);
        arrayList.addAll(collection);
        arrayList.add(t);
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<List<T>> a(Iterable<? extends T> iterable, int i, int i2, boolean z) {
        ee7.b(iterable, "$this$windowed");
        ta7.a(i, i2);
        if (!(iterable instanceof RandomAccess) || !(iterable instanceof List)) {
            ArrayList arrayList = new ArrayList();
            Iterator a2 = ta7.a(iterable.iterator(), i, i2, z, false);
            while (a2.hasNext()) {
                arrayList.add((List) a2.next());
            }
            return arrayList;
        }
        List list = (List) iterable;
        int size = list.size();
        ArrayList arrayList2 = new ArrayList((size / i2) + (size % i2 == 0 ? 0 : 1));
        int i3 = 0;
        while (i3 >= 0 && size > i3) {
            int b = qf7.b(i, size - i3);
            if (b < i && !z) {
                break;
            }
            ArrayList arrayList3 = new ArrayList(b);
            for (int i4 = 0; i4 < b; i4++) {
                arrayList3.add(list.get(i4 + i3));
            }
            arrayList2.add(arrayList3);
            i3 += i2;
        }
        return arrayList2;
    }

    @DexIgnore
    public static final <T> hg7<T> b(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$asSequence");
        return new a(iterable);
    }

    @DexIgnore
    public static final int[] c(Collection<Integer> collection) {
        ee7.b(collection, "$this$toIntArray");
        int[] iArr = new int[collection.size()];
        int i = 0;
        for (Integer num : collection) {
            iArr[i] = num.intValue();
            i++;
        }
        return iArr;
    }

    @DexIgnore
    public static final <T> List<T> d(Collection<? extends T> collection) {
        ee7.b(collection, "$this$toMutableList");
        return new ArrayList(collection);
    }

    @DexIgnore
    public static final <T> List<T> c(Iterable<? extends T> iterable) {
        ee7.b(iterable, "$this$distinct");
        return n(p(iterable));
    }

    @DexIgnore
    public static final <T, A extends Appendable> A a(Iterable<? extends T> iterable, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7<? super T, ? extends CharSequence> gd7) {
        ee7.b(iterable, "$this$joinTo");
        ee7.b(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        ee7.b(charSequence, "separator");
        ee7.b(charSequence2, "prefix");
        ee7.b(charSequence3, "postfix");
        ee7.b(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object obj : iterable) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            eh7.a(a2, obj, gd7);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static /* synthetic */ String a(Iterable iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7 gd7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        if ((i2 & 32) != 0) {
            gd7 = null;
        }
        return a(iterable, charSequence, charSequence6, charSequence5, i3, charSequence4, gd7);
    }

    @DexIgnore
    public static final <T> String a(Iterable<? extends T> iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7<? super T, ? extends CharSequence> gd7) {
        ee7.b(iterable, "$this$joinToString");
        ee7.b(charSequence, "separator");
        ee7.b(charSequence2, "prefix");
        ee7.b(charSequence3, "postfix");
        ee7.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(iterable, sb, charSequence, charSequence2, charSequence3, i, charSequence4, gd7);
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
