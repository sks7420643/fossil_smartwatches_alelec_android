package com.fossil;

import android.os.Bundle;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg3 extends hi3 {
    @DexIgnore
    public static /* final */ AtomicReference<String[]> c; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> d; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> e; // = new AtomicReference<>();

    @DexIgnore
    public hg3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public final String a(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        return a(str, ni3.b, ni3.a, c);
    }

    @DexIgnore
    public final String b(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        return a(str, mi3.b, mi3.a, d);
    }

    @DexIgnore
    public final String c(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return a(str, pi3.b, pi3.a, e);
        }
        return "experiment_id" + "(" + str + ")";
    }

    @DexIgnore
    @Override // com.fossil.hi3
    public final boolean q() {
        return false;
    }

    @DexIgnore
    public final boolean s() {
        b();
        return ((ii3) this).a.y() && ((ii3) this).a.e().a(3);
    }

    @DexIgnore
    public static String a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        a72.a(strArr);
        a72.a(strArr2);
        a72.a(atomicReference);
        a72.a(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (jm3.c(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str2 = strArr3[i];
                }
                return str2;
            }
        }
        return str;
    }

    @DexIgnore
    public final String a(ub3 ub3) {
        String str = null;
        if (ub3 == null) {
            return null;
        }
        if (!s()) {
            return ub3.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("origin=");
        sb.append(ub3.c);
        sb.append(",name=");
        sb.append(a(ub3.a));
        sb.append(",params=");
        tb3 tb3 = ub3.b;
        if (tb3 != null) {
            if (!s()) {
                str = tb3.toString();
            } else {
                str = a(tb3.zzb());
            }
        }
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public final String a(Bundle bundle) {
        String str;
        if (bundle == null) {
            return null;
        }
        if (!s()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Bundle[{");
        for (String str2 : bundle.keySet()) {
            if (sb.length() != 8) {
                sb.append(", ");
            }
            sb.append(b(str2));
            sb.append(SimpleComparison.EQUAL_TO_OPERATION);
            if (!p03.a() || !l().a(wb3.E0)) {
                sb.append(bundle.get(str2));
            } else {
                Object obj = bundle.get(str2);
                if (obj instanceof Bundle) {
                    str = a(new Object[]{obj});
                } else if (obj instanceof Object[]) {
                    str = a((Object[]) obj);
                } else if (obj instanceof ArrayList) {
                    str = a(((ArrayList) obj).toArray());
                } else {
                    str = String.valueOf(obj);
                }
                sb.append(str);
            }
        }
        sb.append("}]");
        return sb.toString();
    }

    @DexIgnore
    public final String a(Object[] objArr) {
        String str;
        if (objArr == null) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object obj : objArr) {
            if (obj instanceof Bundle) {
                str = a((Bundle) obj);
            } else {
                str = String.valueOf(obj);
            }
            if (str != null) {
                if (sb.length() != 1) {
                    sb.append(", ");
                }
                sb.append(str);
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
