package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv2 implements zu2 {
    @DexIgnore
    public dv2() {
    }

    @DexIgnore
    @Override // com.fossil.zu2
    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    @DexIgnore
    public /* synthetic */ dv2(su2 su2) {
        this();
    }
}
