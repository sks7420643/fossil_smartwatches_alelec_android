package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y24 {
    @DexIgnore
    void a(String str, int i, String str2, int i2, long j, long j2, boolean z, int i3, String str3, String str4);

    @DexIgnore
    void a(String str, String str2, long j);

    @DexIgnore
    void a(String str, String str2, String str3, String str4, String str5, int i, String str6);

    @DexIgnore
    void a(String str, String str2, String str3, boolean z);

    @DexIgnore
    boolean a(String str);

    @DexIgnore
    b34 b(String str);

    @DexIgnore
    boolean c(String str);

    @DexIgnore
    boolean d(String str);
}
