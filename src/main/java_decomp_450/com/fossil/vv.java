package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vv extends ByteArrayOutputStream {
    @DexIgnore
    public /* final */ kv a;

    @DexIgnore
    public vv(kv kvVar, int i) {
        this.a = kvVar;
        ((ByteArrayOutputStream) this).buf = kvVar.a(Math.max(i, 256));
    }

    @DexIgnore
    public final void a(int i) {
        int i2 = ((ByteArrayOutputStream) this).count;
        if (i2 + i > ((ByteArrayOutputStream) this).buf.length) {
            byte[] a2 = this.a.a((i2 + i) * 2);
            System.arraycopy(((ByteArrayOutputStream) this).buf, 0, a2, 0, ((ByteArrayOutputStream) this).count);
            this.a.a(((ByteArrayOutputStream) this).buf);
            ((ByteArrayOutputStream) this).buf = a2;
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.a(((ByteArrayOutputStream) this).buf);
        ((ByteArrayOutputStream) this).buf = null;
        super.close();
    }

    @DexIgnore
    @Override // java.lang.Object
    public void finalize() {
        this.a.a(((ByteArrayOutputStream) this).buf);
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public synchronized void write(byte[] bArr, int i, int i2) {
        a(i2);
        super.write(bArr, i, i2);
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream
    public synchronized void write(int i) {
        a(1);
        super.write(i);
    }
}
