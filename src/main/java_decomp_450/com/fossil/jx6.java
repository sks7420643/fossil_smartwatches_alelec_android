package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jx6 {
    @DexIgnore
    public static jx6 a;

    @DexIgnore
    public static SharedPreferences u(Context context) {
        return context.getSharedPreferences(Constants.FRAMEWORKS_SHAREPREFS, 0);
    }

    @DexIgnore
    public String a(Context context) {
        return a(context, "com.misfit.frameworks.profile.createdAt");
    }

    @DexIgnore
    public void b(Context context, String str) {
        a(context, "com.misfit.frameworks.profile.accessToken", str);
    }

    @DexIgnore
    public String c(Context context) {
        return a(context, "com.misfit.frameworks.profile.registration");
    }

    @DexIgnore
    public String d(Context context) {
        return a(context, "com.misfit.frameworks.profile.registerDate");
    }

    @DexIgnore
    public String e(Context context) {
        return a(context, "com.misfit.frameworks.profile.updateAt");
    }

    @DexIgnore
    public String f(Context context) {
        return a(context, "com.misfit.frameworks.profile.accessToken");
    }

    @DexIgnore
    public String g(Context context) {
        return a(context, "com.misfit.frameworks.profile.authType");
    }

    @DexIgnore
    public String h(Context context) {
        return a(context, "com.misfit.frameworks.profile.birthday");
    }

    @DexIgnore
    public String i(Context context) {
        return a(context, "com.misfit.frameworks.profile.email");
    }

    @DexIgnore
    public String j(Context context) {
        return a(context, "com.misfit.frameworks.profile.firstname");
    }

    @DexIgnore
    public fb5 k(Context context) {
        return fb5.Companion.a(a(context, "com.misfit.frameworks.profile.gender"));
    }

    @DexIgnore
    public String l(Context context) {
        return a(context, "com.misfit.frameworks.profile.height");
    }

    @DexIgnore
    public String m(Context context) {
        return a(context, "com.misfit.frameworks.profile.userId");
    }

    @DexIgnore
    public String n(Context context) {
        return a(context, "com.misfit.frameworks.profile.lastname");
    }

    @DexIgnore
    public String o(Context context) {
        return a(context, "com.misfit.frameworks.profile.profilePic");
    }

    @DexIgnore
    public String p(Context context) {
        return a(context, "com.misfit.frameworks.profile.units.distance");
    }

    @DexIgnore
    public String q(Context context) {
        return a(context, "com.misfit.frameworks.profile.units.height");
    }

    @DexIgnore
    public String r(Context context) {
        return a(context, "com.misfit.frameworks.profile.units.weight");
    }

    @DexIgnore
    public String s(Context context) {
        return a(context, "com.misfit.frameworks.profile.weight");
    }

    @DexIgnore
    public boolean t(Context context) {
        return a(context, "com.misfit.frameworks.profile.diagnosticEnable", true);
    }

    @DexIgnore
    public static synchronized jx6 a() {
        jx6 jx6;
        synchronized (jx6.class) {
            if (a == null) {
                a = new jx6();
            }
            jx6 = a;
        }
        return jx6;
    }

    @DexIgnore
    public String b(Context context) {
        return a(context, "com.misfit.frameworks.profile.integrations");
    }

    @DexIgnore
    public final String a(Context context, String str) {
        SharedPreferences u = u(context);
        if (u != null) {
            return u.getString(str, "");
        }
        return "";
    }

    @DexIgnore
    public final boolean a(Context context, String str, boolean z) {
        SharedPreferences u = u(context);
        return u != null && u.getBoolean(str, z);
    }

    @DexIgnore
    public final void a(Context context, String str, String str2) {
        SharedPreferences u = u(context);
        if (u != null) {
            SharedPreferences.Editor edit = u.edit();
            edit.putString(str, str2);
            edit.apply();
        }
    }
}
