package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma3 implements Parcelable.Creator<StreetViewPanoramaOptions> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ StreetViewPanoramaOptions createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        StreetViewPanoramaCamera streetViewPanoramaCamera = null;
        String str = null;
        LatLng latLng = null;
        Integer num = null;
        u93 u93 = null;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        byte b6 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    streetViewPanoramaCamera = (StreetViewPanoramaCamera) j72.a(parcel, a, StreetViewPanoramaCamera.CREATOR);
                    break;
                case 3:
                    str = j72.e(parcel, a);
                    break;
                case 4:
                    latLng = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
                    break;
                case 5:
                    num = j72.r(parcel, a);
                    break;
                case 6:
                    b2 = j72.k(parcel, a);
                    break;
                case 7:
                    b3 = j72.k(parcel, a);
                    break;
                case 8:
                    b4 = j72.k(parcel, a);
                    break;
                case 9:
                    b5 = j72.k(parcel, a);
                    break;
                case 10:
                    b6 = j72.k(parcel, a);
                    break;
                case 11:
                    u93 = (u93) j72.a(parcel, a, u93.CREATOR);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new StreetViewPanoramaOptions(streetViewPanoramaCamera, str, latLng, num, b2, b3, b4, b5, b6, u93);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ StreetViewPanoramaOptions[] newArray(int i) {
        return new StreetViewPanoramaOptions[i];
    }
}
