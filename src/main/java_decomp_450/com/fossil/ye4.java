package com.fossil;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ye4 {
    @DexIgnore
    public /* final */ Map<Type, ce4<?>> a;
    @DexIgnore
    public /* final */ nf4 b; // = nf4.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements df4<T> {
        @DexIgnore
        public a(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new ConcurrentHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements df4<T> {
        @DexIgnore
        public b(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new TreeMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements df4<T> {
        @DexIgnore
        public c(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new LinkedHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements df4<T> {
        @DexIgnore
        public d(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new cf4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements df4<T> {
        @DexIgnore
        public /* final */ hf4 a; // = hf4.a();
        @DexIgnore
        public /* final */ /* synthetic */ Class b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public e(ye4 ye4, Class cls, Type type) {
            this.b = cls;
            this.c = type;
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            try {
                return (T) this.a.a(this.b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.c + ". Registering an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements df4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ ce4 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public f(ye4 ye4, ce4 ce4, Type type) {
            this.a = ce4;
            this.b = type;
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) this.a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements df4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ ce4 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public g(ye4 ye4, ce4 ce4, Type type) {
            this.a = ce4;
            this.b = type;
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) this.a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements df4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor a;

        @DexIgnore
        public h(ye4 ye4, Constructor constructor) {
            this.a = constructor;
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            try {
                return (T) this.a.newInstance(null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements df4<T> {
        @DexIgnore
        public i(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new TreeSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements df4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;

        @DexIgnore
        public j(ye4 ye4, Type type) {
            this.a = type;
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            Type type = this.a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return (T) EnumSet.noneOf((Class) type2);
                }
                throw new ge4("Invalid EnumSet type: " + this.a.toString());
            }
            throw new ge4("Invalid EnumSet type: " + this.a.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k implements df4<T> {
        @DexIgnore
        public k(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new LinkedHashSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements df4<T> {
        @DexIgnore
        public l(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new ArrayDeque();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements df4<T> {
        @DexIgnore
        public m(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new ArrayList();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements df4<T> {
        @DexIgnore
        public n(ye4 ye4) {
        }

        @DexIgnore
        @Override // com.fossil.df4
        public T a() {
            return (T) new ConcurrentSkipListMap();
        }
    }

    @DexIgnore
    public ye4(Map<Type, ce4<?>> map) {
        this.a = map;
    }

    @DexIgnore
    public <T> df4<T> a(TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        ce4<?> ce4 = this.a.get(type);
        if (ce4 != null) {
            return new f(this, ce4, type);
        }
        ce4<?> ce42 = this.a.get(rawType);
        if (ce42 != null) {
            return new g(this, ce42, type);
        }
        df4<T> a2 = a(rawType);
        if (a2 != null) {
            return a2;
        }
        df4<T> a3 = a(type, rawType);
        if (a3 != null) {
            return a3;
        }
        return b(type, rawType);
    }

    @DexIgnore
    public final <T> df4<T> b(Type type, Class<? super T> cls) {
        return new e(this, cls, type);
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public final <T> df4<T> a(Class<? super T> cls) {
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.b.a(declaredConstructor);
            }
            return new h(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    @DexIgnore
    public final <T> df4<T> a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            if (SortedSet.class.isAssignableFrom(cls)) {
                return new i(this);
            }
            if (EnumSet.class.isAssignableFrom(cls)) {
                return new j(this, type);
            }
            if (Set.class.isAssignableFrom(cls)) {
                return new k(this);
            }
            if (Queue.class.isAssignableFrom(cls)) {
                return new l(this);
            }
            return new m(this);
        } else if (!Map.class.isAssignableFrom(cls)) {
            return null;
        } else {
            if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
                return new n(this);
            }
            if (ConcurrentMap.class.isAssignableFrom(cls)) {
                return new a(this);
            }
            if (SortedMap.class.isAssignableFrom(cls)) {
                return new b(this);
            }
            if (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) {
                return new d(this);
            }
            return new c(this);
        }
    }
}
