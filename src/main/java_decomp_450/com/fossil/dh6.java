package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutEditActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dh6 extends dy6 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public WorkoutSession j;
    @DexIgnore
    public /* final */ pb p; // = new pm4(this);
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return dh6.r;
        }

        @DexIgnore
        public final dh6 b() {
            return new dh6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dh6 a;

        @DexIgnore
        public b(dh6 dh6) {
            this.a = dh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.a.getDialog();
            if (dialog != null) {
                dh6 dh6 = this.a;
                ee7.a((Object) dialog, "it");
                dh6.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dh6 a;

        @DexIgnore
        public c(dh6 dh6) {
            this.a = dh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                WorkoutEditActivity.a aVar = WorkoutEditActivity.y;
                ee7.a((Object) activity, "it");
                WorkoutSession a2 = this.a.j;
                if (a2 != null) {
                    aVar.a(activity, a2.getId());
                } else {
                    ee7.a();
                    throw null;
                }
            }
            Dialog dialog = this.a.getDialog();
            if (dialog != null) {
                dh6 dh6 = this.a;
                ee7.a((Object) dialog, "it");
                dh6.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dh6 a;

        @DexIgnore
        public d(dh6 dh6) {
            this.a = dh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.a.getDialog();
            if (dialog != null) {
                dh6 dh6 = this.a;
                ee7.a((Object) dialog, "it");
                dh6.onDismiss(dialog);
            }
        }
    }

    /*
    static {
        String simpleName = dh6.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutMenuFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void c(WorkoutSession workoutSession) {
        ee7.b(workoutSession, "workoutSession");
        this.j = workoutSession;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        m75 m75 = (m75) qb.a(layoutInflater, 2131558642, viewGroup, false, this.p);
        m75.t.setOnClickListener(new b(this));
        new qw6(this, m75);
        m75.r.setOnClickListener(new c(this));
        m75.q.setOnClickListener(new d(this));
        ee7.a((Object) m75, "binding");
        return m75.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }
}
