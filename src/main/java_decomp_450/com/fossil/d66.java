package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d66 implements Factory<c66> {
    @DexIgnore
    public static c66 a(a66 a66, CategoryRepository categoryRepository) {
        return new c66(a66, categoryRepository);
    }
}
