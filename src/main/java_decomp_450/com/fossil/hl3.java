package com.fossil;

import android.app.job.JobParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class hl3 implements Runnable {
    @DexIgnore
    public /* final */ fl3 a;
    @DexIgnore
    public /* final */ jg3 b;
    @DexIgnore
    public /* final */ JobParameters c;

    @DexIgnore
    public hl3(fl3 fl3, jg3 jg3, JobParameters jobParameters) {
        this.a = fl3;
        this.b = jg3;
        this.c = jobParameters;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
