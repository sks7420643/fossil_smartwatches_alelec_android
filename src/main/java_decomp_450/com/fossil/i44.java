package com.fossil;

import com.fossil.v54;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i44 implements n44 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public i44(String str, String str2, File file) {
        this.b = str;
        this.c = str2;
        this.a = file;
    }

    @DexIgnore
    @Override // com.fossil.n44
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.n44
    public InputStream b() {
        if (this.a.exists() && this.a.isFile()) {
            try {
                return new FileInputStream(this.a);
            } catch (FileNotFoundException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.n44
    public v54.c.b c() {
        byte[] d = d();
        if (d == null) {
            return null;
        }
        v54.c.b.a c2 = v54.c.b.c();
        c2.a(d);
        c2.a(this.b);
        return c2.a();
    }

    @DexIgnore
    public final byte[] d() {
        byte[] bArr = new byte[8192];
        try {
            InputStream b2 = b();
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                    if (b2 == null) {
                        gZIPOutputStream.close();
                        byteArrayOutputStream.close();
                        if (b2 != null) {
                            b2.close();
                        }
                        return null;
                    }
                    while (true) {
                        try {
                            int read = b2.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            gZIPOutputStream.write(bArr, 0, read);
                        } catch (Throwable unused) {
                        }
                    }
                    gZIPOutputStream.finish();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    gZIPOutputStream.close();
                    byteArrayOutputStream.close();
                    if (b2 != null) {
                        b2.close();
                    }
                    return byteArray;
                } catch (Throwable unused2) {
                }
                throw th;
                throw th;
                throw th;
            } catch (Throwable unused3) {
            }
        } catch (IOException unused4) {
            return null;
        }
    }
}
