package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ul4;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw5 extends ho5 implements qw5 {
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<k25> g;
    @DexIgnore
    public pw5 h;
    @DexIgnore
    public ul4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final rw5 a() {
            return new rw5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ul4.b {
        @DexIgnore
        public /* final */ /* synthetic */ rw5 a;

        @DexIgnore
        public b(rw5 rw5) {
            this.a = rw5;
        }

        @DexIgnore
        @Override // com.fossil.ul4.b
        public void a(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            rw5.a(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        @Override // com.fossil.ul4.b
        public void b(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            rw5.a(this.a).a(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rw5 a;

        @DexIgnore
        public c(rw5 rw5) {
            this.a = rw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
                ee7.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rw5 a;

        @DexIgnore
        public d(rw5 rw5) {
            this.a = rw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rw5.a(this.a).a(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rw5 a;

        @DexIgnore
        public e(rw5 rw5) {
            this.a = rw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (xg5.a(xg5.b, this.a.getContext(), xg5.a.NOTIFICATION_GET_CONTACTS, false, false, false, (Integer) null, 60, (Object) null)) {
                NotificationDialLandingActivity.z.a(this.a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rw5 a;

        @DexIgnore
        public f(rw5 rw5) {
            this.a = rw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rw5.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rw5 a;

        @DexIgnore
        public g(rw5 rw5) {
            this.a = rw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            rw5.a(this.a).h();
        }
    }

    @DexIgnore
    public static final /* synthetic */ pw5 a(rw5 rw5) {
        pw5 pw5 = rw5.h;
        if (pw5 != null) {
            return pw5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void B(boolean z) {
        ConstraintLayout constraintLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeAlertsHybridFragment", "updateAssignNotificationState() - isEnable = " + z);
        qw6<k25> qw6 = this.g;
        if (qw6 != null) {
            k25 a2 = qw6.a();
            if (a2 != null && (constraintLayout = a2.K) != null) {
                ee7.a((Object) constraintLayout, "it");
                constraintLayout.setAlpha(z ? 0.5f : 1.0f);
                constraintLayout.setClickable(!z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void G(boolean z) {
        qw6<k25> qw6 = this.g;
        if (qw6 != null) {
            k25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerView recyclerView = a2.O;
                ee7.a((Object) recyclerView, "binding.rvAlarms");
                recyclerView.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.v;
                ee7.a((Object) flexibleTextView, "binding.ftvAlarmsSection");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = a2.s;
                ee7.a((Object) flexibleButton, "binding.btnAdd");
                flexibleButton.setVisibility(0);
                return;
            }
            RecyclerView recyclerView2 = a2.O;
            ee7.a((Object) recyclerView2, "binding.rvAlarms");
            recyclerView2.setVisibility(8);
            FlexibleTextView flexibleTextView2 = a2.v;
            ee7.a((Object) flexibleTextView2, "binding.ftvAlarmsSection");
            flexibleTextView2.setVisibility(8);
            FlexibleButton flexibleButton2 = a2.s;
            ee7.a((Object) flexibleButton2, "binding.btnAdd");
            flexibleButton2.setVisibility(8);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void d(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            qw6<k25> qw6 = this.g;
            if (qw6 != null) {
                k25 a2 = qw6.a();
                if (a2 != null && (rTLImageView2 = a2.I) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<k25> qw62 = this.g;
        if (qw62 != null) {
            k25 a3 = qw62.a();
            if (a3 != null && (rTLImageView = a3.I) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HomeAlertsHybridFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void f(List<Alarm> list) {
        ee7.b(list, "alarms");
        ul4 ul4 = this.i;
        if (ul4 != null) {
            ul4.a(list);
        } else {
            ee7.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void o(boolean z) {
        Context context;
        int i2;
        qw6<k25> qw6 = this.g;
        if (qw6 != null) {
            k25 a2 = qw6.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.P;
                ee7.a((Object) flexibleSwitchCompat, "it.swScheduled");
                flexibleSwitchCompat.setChecked(z);
                FlexibleTextView flexibleTextView = a2.C;
                if (z) {
                    context = requireContext();
                    i2 = 2131099972;
                } else {
                    context = requireContext();
                    i2 = 2131100359;
                }
                flexibleTextView.setTextColor(v6.a(context, i2));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        k25 k25 = (k25) qb.a(layoutInflater, 2131558571, viewGroup, false, a1());
        V("alert_view");
        String b2 = eh5.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            k25.Q.setBackgroundColor(parseColor);
            k25.R.setBackgroundColor(parseColor);
            k25.q.setBackgroundColor(parseColor);
            k25.r.setBackgroundColor(parseColor);
        }
        String b3 = eh5.l.a().b("onPrimaryButton");
        if (!TextUtils.isEmpty(b3)) {
            int parseColor2 = Color.parseColor(b3);
            Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230983);
            if (drawable != null) {
                drawable.setTint(parseColor2);
                k25.s.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        k25.s.setOnClickListener(new d(this));
        k25.K.setOnClickListener(new e(this));
        k25.P.setOnClickListener(new f(this));
        ua5 ua5 = k25.H;
        if (ua5 != null) {
            ConstraintLayout constraintLayout = ua5.q;
            ee7.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            ua5.t.setImageResource(2131230818);
            FlexibleTextView flexibleTextView = ua5.r;
            ee7.a((Object) flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(ig5.a(getContext(), 2131886996));
            ua5.s.setOnClickListener(new c(this));
        }
        k25.I.setOnClickListener(new g(this));
        ul4 ul4 = new ul4();
        ul4.a(new b(this));
        this.i = ul4;
        RecyclerView recyclerView = k25.O;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        ul4 ul42 = this.i;
        if (ul42 != null) {
            recyclerView.setAdapter(ul42);
            qw6<k25> qw6 = new qw6<>(this, k25);
            this.g = qw6;
            k25 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        pw5 pw5 = this.h;
        if (pw5 != null) {
            if (pw5 != null) {
                pw5.g();
                jf5 c1 = c1();
                if (c1 != null) {
                    c1.a("");
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        super.onPause();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pw5 pw5 = this.h;
        if (pw5 == null) {
            return;
        }
        if (pw5 != null) {
            pw5.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void r() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.I(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void t() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsHybridFragment", "notifyListAlarm()");
        ul4 ul4 = this.i;
        if (ul4 != null) {
            ul4.notifyDataSetChanged();
        } else {
            ee7.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void z() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.S(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(pw5 pw5) {
        ee7.b(pw5, "presenter");
        this.h = pw5;
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        ee7.b(str, "deviceId");
        ee7.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.z;
        Context requireContext = requireContext();
        ee7.a((Object) requireContext, "requireContext()");
        aVar.a(requireContext, str, arrayList, alarm);
    }

    @DexIgnore
    @Override // com.fossil.qw5
    public void a(boolean z) {
        ConstraintLayout constraintLayout;
        qw6<k25> qw6 = this.g;
        if (qw6 != null) {
            k25 a2 = qw6.a();
            if (a2 != null && (constraintLayout = a2.t) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
