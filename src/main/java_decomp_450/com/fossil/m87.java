package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m87 implements Comparable<m87> {
    @DexIgnore
    public static /* final */ m87 e; // = new m87(1, 3, 72);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public m87(int i, int i2, int i3) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.a = a(i, i2, i3);
    }

    @DexIgnore
    public final int a(int i, int i2, int i3) {
        if (i >= 0 && 255 >= i && i2 >= 0 && 255 >= i2 && i3 >= 0 && 255 >= i3) {
            return (i << 16) + (i2 << 8) + i3;
        }
        throw new IllegalArgumentException(("Version components are out of range: " + i + '.' + i2 + '.' + i3).toString());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m87)) {
            obj = null;
        }
        m87 m87 = (m87) obj;
        if (m87 == null || this.a != m87.a) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append('.');
        sb.append(this.c);
        sb.append('.');
        sb.append(this.d);
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(m87 m87) {
        ee7.b(m87, FacebookRequestErrorClassification.KEY_OTHER);
        return this.a - m87.a;
    }
}
