package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum cg0 {
    OPEN("open"),
    CLOSE("close");
    
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public cg0(String str) {
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }
}
