package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pv3 extends tv3 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new b();
    @DexIgnore
    public AnimatorSet f;
    @DexIgnore
    public ValueAnimator g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (!pv3.b(editable)) {
                pv3.this.f.cancel();
                pv3.this.g.start();
            } else if (!((tv3) pv3.this).a.o()) {
                pv3.this.g.cancel();
                pv3.this.f.start();
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements TextInputLayout.f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(pv3.b(editText.getText()));
            textInputLayout.setEndIconCheckable(false);
            editText.removeTextChangedListener(pv3.this.d);
            editText.addTextChangedListener(pv3.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(View view) {
            ((tv3) pv3.this).a.getEditText().setText((CharSequence) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AnimatorListenerAdapter {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            ((tv3) pv3.this).a.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AnimatorListenerAdapter {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ((tv3) pv3.this).a.setEndIconVisible(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ((tv3) pv3.this).c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            ((tv3) pv3.this).c.setScaleX(floatValue);
            ((tv3) pv3.this).c.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public pv3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public final void d() {
        ValueAnimator c2 = c();
        ValueAnimator a2 = a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        this.f = animatorSet;
        animatorSet.playTogether(c2, a2);
        this.f.addListener(new d());
        ValueAnimator a3 = a(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = a3;
        a3.addListener(new e());
    }

    @DexIgnore
    public static boolean b(Editable editable) {
        return editable.length() > 0;
    }

    @DexIgnore
    public final ValueAnimator c() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.8f, 1.0f);
        ofFloat.setInterpolator(ur3.d);
        ofFloat.setDuration(150L);
        ofFloat.addUpdateListener(new g());
        return ofFloat;
    }

    @DexIgnore
    @Override // com.fossil.tv3
    public void a() {
        ((tv3) this).a.setEndIconDrawable(t0.c(((tv3) this).b, mr3.mtrl_ic_cancel));
        TextInputLayout textInputLayout = ((tv3) this).a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(rr3.clear_text_end_icon_content_description));
        ((tv3) this).a.setEndIconOnClickListener(new c());
        ((tv3) this).a.a(this.e);
        d();
    }

    @DexIgnore
    public final ValueAnimator a(float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(ur3.a);
        ofFloat.setDuration(100L);
        ofFloat.addUpdateListener(new f());
        return ofFloat;
    }
}
