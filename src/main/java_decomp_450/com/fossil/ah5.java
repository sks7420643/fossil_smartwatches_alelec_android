package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah5 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static ah5 o;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public String a; // = "Anonymous";
    @DexIgnore
    public ContactProvider b;
    @DexIgnore
    public LocationProvider c;
    @DexIgnore
    public AppFilterProvider d;
    @DexIgnore
    public DeviceProvider e;
    @DexIgnore
    public ci5 f;
    @DexIgnore
    public MFSleepSessionProvider g;
    @DexIgnore
    public yh5 h;
    @DexIgnore
    public gi5 i;
    @DexIgnore
    public ai5 j;
    @DexIgnore
    public SecondTimezoneProvider k;
    @DexIgnore
    public ei5 l;
    @DexIgnore
    public ii5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ah5 a() {
            if (ah5.o == null) {
                ah5.o = new ah5();
            }
            ah5 n = ah5.o;
            if (n != null) {
                return n;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ah5.class.getSimpleName();
        ee7.a((Object) simpleName, "ProviderManager::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public ah5() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final synchronized ContactProvider b() {
        ContactProvider contactProvider;
        String str = this.a;
        if (this.b == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + ContactProviderImpl.DB_NAME;
            FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider newPath=" + str2);
            this.b = new wh5(PortfolioApp.g0.c().getApplicationContext(), str2);
        } else {
            String str3 = str + LocaleConverter.LOCALE_DELIMITER + ContactProviderImpl.DB_NAME;
            ContactProvider contactProvider2 = this.b;
            if (contactProvider2 != null) {
                String dbPath = contactProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str3, (Object) dbPath))) {
                    this.b = new wh5(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        contactProvider = this.b;
        if (contactProvider == null) {
            ee7.a();
            throw null;
        }
        return contactProvider;
    }

    @DexIgnore
    public final synchronized DeviceProvider c() {
        DeviceProvider deviceProvider;
        DeviceProviderImp deviceProviderImp;
        String str = this.a;
        FLogger.INSTANCE.getLocal().d(n, "Inside .getDeviceProvider with userId=" + str);
        if (this.e == null) {
            this.e = new DeviceProviderImp(PortfolioApp.g0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME);
        } else {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME;
            try {
                DeviceProvider deviceProvider2 = this.e;
                if (deviceProvider2 != null) {
                    String dbPath = deviceProvider2.getDbPath();
                    if (TextUtils.isEmpty(dbPath) || (!ee7.a((Object) str2, (Object) dbPath))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.g0.c().getApplicationContext(), str2);
                        this.e = deviceProviderImp;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception e2) {
                FLogger.INSTANCE.getLocal().e(n, "getDeviceProvider - ex=" + e2);
                if (TextUtils.isEmpty(null) || (!ee7.a((Object) str2, (Object) null))) {
                    deviceProviderImp = new DeviceProviderImp(PortfolioApp.g0.c().getApplicationContext(), str2);
                }
            } catch (Throwable th) {
                if (TextUtils.isEmpty(null) || (!ee7.a((Object) str2, (Object) null))) {
                    this.e = new DeviceProviderImp(PortfolioApp.g0.c().getApplicationContext(), str2);
                }
                throw th;
            }
        }
        deviceProvider = this.e;
        if (deviceProvider == null) {
            ee7.a();
            throw null;
        }
        return deviceProvider;
    }

    @DexIgnore
    public final synchronized ai5 d() {
        ai5 ai5;
        String str = this.a;
        if (this.j == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + "firmwares.db";
            this.j = new bi5(PortfolioApp.g0.c().getApplicationContext(), str2);
            FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider dbPath=" + str2);
        } else {
            String str3 = str + LocaleConverter.LOCALE_DELIMITER + "firmwares.db";
            ai5 ai52 = this.j;
            if (ai52 != null) {
                String dbPath = ai52.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str3, (Object) dbPath))) {
                    this.j = new bi5(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        ai5 = this.j;
        if (ai5 == null) {
            ee7.a();
            throw null;
        }
        return ai5;
    }

    @DexIgnore
    public final synchronized yh5 e() {
        yh5 yh5;
        if (this.h == null) {
            String str = this.a;
            this.h = new zh5(PortfolioApp.g0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + "hourNotification.db");
        }
        yh5 = this.h;
        if (yh5 == null) {
            ee7.a();
            throw null;
        }
        return yh5;
    }

    @DexIgnore
    public final synchronized LocationProvider f() {
        LocationProvider locationProvider;
        String str = this.a;
        if (this.c == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + LocationProviderImpl.DB_NAME;
            this.c = new LocationProviderImpl(PortfolioApp.g0.c().getApplicationContext(), str2);
            FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider path " + str2);
        } else {
            String str3 = str + LocaleConverter.LOCALE_DELIMITER + LocationProviderImpl.DB_NAME;
            LocationProvider locationProvider2 = this.c;
            if (locationProvider2 != null) {
                String dbPath = locationProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str3, (Object) dbPath))) {
                    this.c = new LocationProviderImpl(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        locationProvider = this.c;
        if (locationProvider == null) {
            ee7.a();
            throw null;
        }
        return locationProvider;
    }

    @DexIgnore
    public final synchronized ci5 g() {
        ci5 ci5;
        String str = this.a;
        if (this.f == null) {
            this.f = new di5(PortfolioApp.g0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + "microAppSetting.db");
        } else {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + "microAppSetting.db";
            ci5 ci52 = this.f;
            if (ci52 != null) {
                String dbPath = ci52.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getMicroAppSettingProvider previousDbPath=" + dbPath + ", newPath=" + str2);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str2, (Object) dbPath))) {
                    this.f = new di5(PortfolioApp.g0.c().getApplicationContext(), str2);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        ci5 = this.f;
        if (ci5 == null) {
            ee7.a();
            throw null;
        }
        return ci5;
    }

    @DexIgnore
    public final ei5 h() {
        if (this.l == null) {
            this.l = new fi5(PortfolioApp.g0.c().getApplicationContext(), "phone_favorites_contact.db");
        }
        ei5 ei5 = this.l;
        if (ei5 != null) {
            return ei5;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final synchronized gi5 i() {
        gi5 gi5;
        hi5 hi5;
        String str = this.a;
        if (this.i == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + "pin.db";
            this.i = new hi5(PortfolioApp.g0.c().getApplicationContext(), str2);
            FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider dbPath=" + str2);
        } else {
            String str3 = str + LocaleConverter.LOCALE_DELIMITER + "pin.db";
            try {
                gi5 gi52 = this.i;
                if (gi52 != null) {
                    String dbPath = gi52.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (rt7.a(dbPath) || (!ee7.a((Object) str3, (Object) dbPath))) {
                        hi5 = new hi5(PortfolioApp.g0.c().getApplicationContext(), str3);
                        this.i = hi5;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } catch (Exception e2) {
                FLogger.INSTANCE.getLocal().e(n, "getPinProvider - ex=" + e2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + ((String) null) + ", newPath=" + str3);
                if (rt7.a(null) || (!ee7.a((Object) str3, (Object) null))) {
                    hi5 = new hi5(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
            } catch (Throwable th) {
                FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + ((String) null) + ", newPath=" + str3);
                if (rt7.a(null) || (!ee7.a((Object) str3, (Object) null))) {
                    this.i = new hi5(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
                throw th;
            }
        }
        gi5 = this.i;
        if (gi5 == null) {
            ee7.a();
            throw null;
        }
        return gi5;
    }

    @DexIgnore
    public final synchronized SecondTimezoneProvider j() {
        SecondTimezoneProvider secondTimezoneProvider;
        String str = this.a;
        if (this.k == null) {
            this.k = new SecondTimezoneProviderImp(PortfolioApp.g0.c().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + SecondTimezoneProviderImp.DB_NAME);
        } else {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + SecondTimezoneProviderImp.DB_NAME;
            SecondTimezoneProvider secondTimezoneProvider2 = this.k;
            if (secondTimezoneProvider2 != null) {
                String dbPath = secondTimezoneProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSecondTimezoneProvider previousDbPath=" + dbPath + ", newPath=" + str2);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str2, (Object) dbPath))) {
                    this.k = new SecondTimezoneProviderImp(PortfolioApp.g0.c().getApplicationContext(), str2);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        secondTimezoneProvider = this.k;
        if (secondTimezoneProvider == null) {
            ee7.a();
            throw null;
        }
        return secondTimezoneProvider;
    }

    @DexIgnore
    public final ii5 k() {
        if (this.m == null) {
            synchronized (ah5.class) {
                if (this.m == null) {
                    String str = this.a + LocaleConverter.LOCALE_DELIMITER + "serverSetting.db";
                    FLogger.INSTANCE.getLocal().d(n, "getServerSettingProvider dbPath: " + str);
                    Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                    ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    this.m = new ji5(applicationContext, str);
                }
                i97 i97 = i97.a;
            }
        }
        ii5 ii5 = this.m;
        if (ii5 != null) {
            return ii5;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final synchronized MFSleepSessionProvider l() {
        MFSleepSessionProvider mFSleepSessionProvider;
        String str = this.a;
        if (this.g == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + MFSleepSessionProviderImp.DB_NAME;
            this.g = new MFSleepSessionProviderImp(PortfolioApp.g0.c().getApplicationContext(), str2);
            FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider dbPath " + str2);
        } else {
            String str3 = str + LocaleConverter.LOCALE_DELIMITER + MFSleepSessionProviderImp.DB_NAME;
            MFSleepSessionProvider mFSleepSessionProvider2 = this.g;
            if (mFSleepSessionProvider2 != null) {
                String dbPath = mFSleepSessionProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str3, (Object) dbPath))) {
                    this.g = new MFSleepSessionProviderImp(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        mFSleepSessionProvider = this.g;
        if (mFSleepSessionProvider == null) {
            ee7.a();
            throw null;
        }
        return mFSleepSessionProvider;
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(n, "reset database");
        this.b = null;
        this.c = null;
        this.d = null;
        this.g = null;
        this.i = null;
        this.e = null;
        this.h = null;
        this.f = null;
        this.m = null;
    }

    @DexIgnore
    public final synchronized DeviceProvider a(String str) {
        DeviceProvider deviceProvider;
        ee7.b(str, ButtonService.USER_ID);
        if (this.e == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME;
            FLogger.INSTANCE.getLocal().e(n, "Get device provider, current path " + str2);
            this.e = new DeviceProviderImp(PortfolioApp.g0.c().getApplicationContext(), str2);
        }
        deviceProvider = this.e;
        if (deviceProvider == null) {
            ee7.a();
            throw null;
        }
        return deviceProvider;
    }

    @DexIgnore
    public final synchronized AppFilterProvider a() {
        AppFilterProvider appFilterProvider;
        String str = this.a;
        if (this.d == null) {
            String str2 = str + LocaleConverter.LOCALE_DELIMITER + AppFilterProviderImpl.DB_NAME;
            this.d = new vh5(PortfolioApp.g0.c().getApplicationContext(), str2);
            FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider dbPath " + str2);
        } else {
            String str3 = str + LocaleConverter.LOCALE_DELIMITER + AppFilterProviderImpl.DB_NAME;
            AppFilterProvider appFilterProvider2 = this.d;
            if (appFilterProvider2 != null) {
                String dbPath = appFilterProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                if (!TextUtils.isEmpty(dbPath) && (!ee7.a((Object) str3, (Object) dbPath))) {
                    this.d = new vh5(PortfolioApp.g0.c().getApplicationContext(), str3);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        appFilterProvider = this.d;
        if (appFilterProvider == null) {
            ee7.a();
            throw null;
        }
        return appFilterProvider;
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, ButtonService.USER_ID);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateUserId current " + str + " updated " + str);
        if (TextUtils.isEmpty(str)) {
            str = "Anonymous";
        }
        this.a = str;
    }
}
