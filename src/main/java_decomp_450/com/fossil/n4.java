package com.fossil;

import androidx.collection.SimpleArrayMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n4<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    @DexIgnore
    public t4<K, V> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends t4<K, V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.t4
        public Object a(int i, int i2) {
            return ((SimpleArrayMap) n4.this).b[(i << 1) + i2];
        }

        @DexIgnore
        @Override // com.fossil.t4
        public int b(Object obj) {
            return n4.this.c(obj);
        }

        @DexIgnore
        @Override // com.fossil.t4
        public int c() {
            return ((SimpleArrayMap) n4.this).c;
        }

        @DexIgnore
        @Override // com.fossil.t4
        public int a(Object obj) {
            return n4.this.b(obj);
        }

        @DexIgnore
        @Override // com.fossil.t4
        public Map<K, V> b() {
            return n4.this;
        }

        @DexIgnore
        @Override // com.fossil.t4
        public void a(K k, V v) {
            n4.this.put(k, v);
        }

        @DexIgnore
        @Override // com.fossil.t4
        public V a(int i, V v) {
            return (V) n4.this.a(i, v);
        }

        @DexIgnore
        @Override // com.fossil.t4
        public void a(int i) {
            n4.this.d(i);
        }

        @DexIgnore
        @Override // com.fossil.t4
        public void a() {
            n4.this.clear();
        }
    }

    @DexIgnore
    public n4() {
    }

    @DexIgnore
    public boolean a(Collection<?> collection) {
        return t4.c(this, collection);
    }

    @DexIgnore
    public final t4<K, V> b() {
        if (this.h == null) {
            this.h = new a();
        }
        return this.h;
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        return b().d();
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<K> keySet() {
        return b().e();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.n4<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        b(((SimpleArrayMap) this).c + map.size());
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map
    public Collection<V> values() {
        return b().f();
    }

    @DexIgnore
    public n4(int i) {
        super(i);
    }

    @DexIgnore
    public n4(SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }
}
