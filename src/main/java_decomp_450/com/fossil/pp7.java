package com.fossil;

import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.lo7;
import com.misfit.frameworks.common.enums.Action;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import net.sqlcipher.database.SQLiteDatabase;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp7 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public volatile fp7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public volatile boolean d;

    @DexIgnore
    public pp7(OkHttpClient okHttpClient, boolean z) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public void a() {
        this.d = true;
        fp7 fp7 = this.b;
        if (fp7 != null) {
            fp7.a();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        lo7 c2 = chain.c();
        mp7 mp7 = (mp7) chain;
        qn7 f = mp7.f();
        co7 g = mp7.g();
        fp7 fp7 = new fp7(this.a.j(), a(c2.g()), f, g, this.c);
        this.b = fp7;
        Response response = null;
        int i = 0;
        while (!this.d) {
            try {
                Response a2 = mp7.a(c2, fp7, null, null);
                if (response != null) {
                    Response.a q = a2.q();
                    Response.a q2 = response.q();
                    q2.a((mo7) null);
                    q.d(q2.a());
                    a2 = q.a();
                }
                try {
                    lo7 a3 = a(a2, fp7.h());
                    if (a3 == null) {
                        fp7.f();
                        return a2;
                    }
                    ro7.a(a2.a());
                    int i2 = i + 1;
                    if (i2 <= 20) {
                        a3.a();
                        if (!a(a2, a3.g())) {
                            fp7.f();
                            fp7 = new fp7(this.a.j(), a(a3.g()), f, g, this.c);
                            this.b = fp7;
                        } else if (fp7.b() != null) {
                            throw new IllegalStateException("Closing the body of " + a2 + " didn't close its backing stream. Bad interceptor?");
                        }
                        response = a2;
                        c2 = a3;
                        i = i2;
                    } else {
                        fp7.f();
                        throw new ProtocolException("Too many follow-up requests: " + i2);
                    }
                } catch (IOException e) {
                    fp7.f();
                    throw e;
                }
            } catch (dp7 e2) {
                if (!a(e2.getLastConnectException(), fp7, false, c2)) {
                    throw e2.getFirstConnectException();
                }
            } catch (IOException e3) {
                if (!a(e3, fp7, !(e3 instanceof sp7), c2)) {
                    throw e3;
                }
            } catch (Throwable th) {
                fp7.a((IOException) null);
                fp7.f();
                throw th;
            }
        }
        fp7.f();
        throw new IOException("Canceled");
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj;
    }

    @DexIgnore
    public final nn7 a(go7 go7) {
        sn7 sn7;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (go7.h()) {
            SSLSocketFactory E = this.a.E();
            hostnameVerifier = this.a.r();
            sSLSocketFactory = E;
            sn7 = this.a.h();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            sn7 = null;
        }
        return new nn7(go7.g(), go7.k(), this.a.n(), this.a.D(), sSLSocketFactory, hostnameVerifier, sn7, this.a.z(), this.a.y(), this.a.x(), this.a.k(), this.a.A());
    }

    @DexIgnore
    public final boolean a(IOException iOException, fp7 fp7, boolean z, lo7 lo7) {
        fp7.a(iOException);
        if (!this.a.C()) {
            return false;
        }
        if ((!z || !a(iOException, lo7)) && a(iOException, z) && fp7.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean a(IOException iOException, lo7 lo7) {
        lo7.a();
        return iOException instanceof FileNotFoundException;
    }

    @DexIgnore
    public final boolean a(IOException iOException, boolean z) {
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!(iOException instanceof SocketTimeoutException) || z) {
                return false;
            }
            return true;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final lo7 a(Response response, no7 no7) throws IOException {
        String b2;
        go7 b3;
        if (response != null) {
            int e = response.e();
            String e2 = response.x().e();
            RequestBody requestBody = null;
            if (e == 307 || e == 308) {
                if (!e2.equals("GET") && !e2.equals("HEAD")) {
                    return null;
                }
            } else if (e == 401) {
                return this.a.d().authenticate(no7, response);
            } else {
                if (e != 503) {
                    if (e != 407) {
                        if (e != 408) {
                            switch (e) {
                                case SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS /*{ENCODED_INT: 300}*/:
                                case Action.Presenter.NEXT /*{ENCODED_INT: 301}*/:
                                case Action.Presenter.PREVIOUS /*{ENCODED_INT: 302}*/:
                                case Action.Presenter.BLACKOUT /*{ENCODED_INT: 303}*/:
                                    break;
                                default:
                                    return null;
                            }
                        } else if (!this.a.C()) {
                            return null;
                        } else {
                            response.x().a();
                            if ((response.r() == null || response.r().e() != 408) && a(response, 0) <= 0) {
                                return response.x();
                            }
                            return null;
                        }
                    } else if (no7.b().type() == Proxy.Type.HTTP) {
                        return this.a.z().authenticate(no7, response);
                    } else {
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    }
                } else if ((response.r() == null || response.r().e() != 503) && a(response, Integer.MAX_VALUE) == 0) {
                    return response.x();
                } else {
                    return null;
                }
            }
            if (!this.a.p() || (b2 = response.b("Location")) == null || (b3 = response.x().g().b(b2)) == null) {
                return null;
            }
            if (!b3.n().equals(response.x().g().n()) && !this.a.q()) {
                return null;
            }
            lo7.a f = response.x().f();
            if (lp7.b(e2)) {
                boolean d2 = lp7.d(e2);
                if (lp7.c(e2)) {
                    f.a("GET", (RequestBody) null);
                } else {
                    if (d2) {
                        requestBody = response.x().a();
                    }
                    f.a(e2, requestBody);
                }
                if (!d2) {
                    f.a("Transfer-Encoding");
                    f.a(HttpHeaders.CONTENT_LENGTH);
                    f.a("Content-Type");
                }
            }
            if (!a(response, b3)) {
                f.a("Authorization");
            }
            f.a(b3);
            return f.a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final int a(Response response, int i) {
        String b2 = response.b("Retry-After");
        if (b2 == null) {
            return i;
        }
        if (b2.matches("\\d+")) {
            return Integer.valueOf(b2).intValue();
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public final boolean a(Response response, go7 go7) {
        go7 g = response.x().g();
        return g.g().equals(go7.g()) && g.k() == go7.k() && g.n().equals(go7.n());
    }
}
