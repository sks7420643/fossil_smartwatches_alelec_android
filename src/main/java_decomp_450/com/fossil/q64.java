package com.fossil;

import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class q64 implements FilenameFilter {
    @DexIgnore
    public static /* final */ q64 a; // = new q64();

    @DexIgnore
    public static FilenameFilter a() {
        return a;
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return u64.b(file, str);
    }
}
