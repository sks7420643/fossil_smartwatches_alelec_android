package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ao3 {
    @DexIgnore
    public static ScheduledExecutorService l;
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ PowerManager.WakeLock b;
    @DexIgnore
    public WorkSource c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ Map<String, Integer[]> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public AtomicInteger k;

    /*
    static {
        new bo3();
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ao3(Context context, int i2, String str) {
        this(context, i2, str, null, context == null ? null : context.getPackageName());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (r2 == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005c, code lost:
        if (r13.j == 0) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005e, code lost:
        com.fossil.h92.a().a(r13.g, com.fossil.g92.a(r13.b, r6), 7, r13.e, r6, null, r13.d, b(), r14);
        r13.j++;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(long r14) {
        /*
            r13 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r13.k
            r0.incrementAndGet()
            r0 = 0
            java.lang.String r6 = r13.a(r0)
            java.lang.Object r0 = r13.a
            monitor-enter(r0)
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch:{ all -> 0x0096 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0096 }
            r2 = 0
            if (r1 == 0) goto L_0x001a
            int r1 = r13.j     // Catch:{ all -> 0x0096 }
            if (r1 <= 0) goto L_0x0029
        L_0x001a:
            android.os.PowerManager$WakeLock r1 = r13.b     // Catch:{ all -> 0x0096 }
            boolean r1 = r1.isHeld()     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0029
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch:{ all -> 0x0096 }
            r1.clear()     // Catch:{ all -> 0x0096 }
            r13.j = r2     // Catch:{ all -> 0x0096 }
        L_0x0029:
            boolean r1 = r13.h     // Catch:{ all -> 0x0096 }
            r12 = 1
            if (r1 == 0) goto L_0x0056
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch:{ all -> 0x0096 }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x0096 }
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0047
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch:{ all -> 0x0096 }
            java.lang.Integer[] r3 = new java.lang.Integer[r12]     // Catch:{ all -> 0x0096 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x0096 }
            r3[r2] = r4     // Catch:{ all -> 0x0096 }
            r1.put(r6, r3)     // Catch:{ all -> 0x0096 }
            r2 = 1
            goto L_0x0054
        L_0x0047:
            r3 = r1[r2]     // Catch:{ all -> 0x0096 }
            int r3 = r3.intValue()     // Catch:{ all -> 0x0096 }
            int r3 = r3 + r12
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0096 }
            r1[r2] = r3     // Catch:{ all -> 0x0096 }
        L_0x0054:
            if (r2 != 0) goto L_0x005e
        L_0x0056:
            boolean r1 = r13.h     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x007d
            int r1 = r13.j     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x007d
        L_0x005e:
            com.fossil.h92 r1 = com.fossil.h92.a()     // Catch:{ all -> 0x0096 }
            android.content.Context r2 = r13.g     // Catch:{ all -> 0x0096 }
            android.os.PowerManager$WakeLock r3 = r13.b     // Catch:{ all -> 0x0096 }
            java.lang.String r3 = com.fossil.g92.a(r3, r6)     // Catch:{ all -> 0x0096 }
            r4 = 7
            java.lang.String r5 = r13.e     // Catch:{ all -> 0x0096 }
            r7 = 0
            int r8 = r13.d     // Catch:{ all -> 0x0096 }
            java.util.List r9 = r13.b()     // Catch:{ all -> 0x0096 }
            r10 = r14
            r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0096 }
            int r1 = r13.j     // Catch:{ all -> 0x0096 }
            int r1 = r1 + r12
            r13.j = r1     // Catch:{ all -> 0x0096 }
        L_0x007d:
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            android.os.PowerManager$WakeLock r0 = r13.b
            r0.acquire()
            r0 = 0
            int r2 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0095
            java.util.concurrent.ScheduledExecutorService r0 = com.fossil.ao3.l
            com.fossil.co3 r1 = new com.fossil.co3
            r1.<init>(r13)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.schedule(r1, r14, r2)
        L_0x0095:
            return
        L_0x0096:
            r14 = move-exception
            monitor-exit(r0)
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ao3.a(long):void");
    }

    @DexIgnore
    public final List<String> b() {
        return z92.a(this.c);
    }

    @DexIgnore
    public ao3(Context context, int i2, String str, String str2, String str3) {
        this(context, i2, str, null, str3, null);
    }

    @DexIgnore
    @SuppressLint({"UnwrappedWakeLock"})
    public ao3(Context context, int i2, String str, String str2, String str3, String str4) {
        this.a = this;
        this.h = true;
        this.i = new HashMap();
        Collections.synchronizedSet(new HashSet());
        this.k = new AtomicInteger(0);
        a72.a(context, "WakeLock: context must not be null");
        a72.a(str, (Object) "WakeLock: wakeLockName must not be empty");
        this.d = i2;
        this.f = null;
        this.g = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(str);
            this.e = valueOf.length() != 0 ? "*gcore*:".concat(valueOf) : new String("*gcore*:");
        } else {
            this.e = str;
        }
        this.b = ((PowerManager) context.getSystemService("power")).newWakeLock(i2, str);
        if (z92.a(context)) {
            WorkSource a2 = z92.a(context, x92.a(str3) ? context.getPackageName() : str3);
            this.c = a2;
            if (a2 != null && z92.a(this.g)) {
                WorkSource workSource = this.c;
                if (workSource != null) {
                    workSource.add(a2);
                } else {
                    this.c = a2;
                }
                try {
                    this.b.setWorkSource(this.c);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e2) {
                    Log.wtf("WakeLock", e2.toString());
                }
            }
        }
        if (l == null) {
            l = c92.a().a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        if (r1 != false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r12.j == 1) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
        com.fossil.h92.a().a(r12.g, com.fossil.g92.a(r12.b, r6), 8, r12.e, r6, null, r12.d, b());
        r12.j--;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r12 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r12.k
            int r0 = r0.decrementAndGet()
            if (r0 >= 0) goto L_0x0019
            java.lang.String r0 = r12.e
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r1 = " release without a matched acquire!"
            java.lang.String r0 = r0.concat(r1)
            java.lang.String r1 = "WakeLock"
            android.util.Log.e(r1, r0)
        L_0x0019:
            r0 = 0
            java.lang.String r6 = r12.a(r0)
            java.lang.Object r0 = r12.a
            monitor-enter(r0)
            boolean r1 = r12.h     // Catch:{ all -> 0x007e }
            r10 = 1
            r11 = 0
            if (r1 == 0) goto L_0x0052
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r12.i     // Catch:{ all -> 0x007e }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x007e }
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1     // Catch:{ all -> 0x007e }
            if (r1 != 0) goto L_0x0033
        L_0x0031:
            r1 = 0
            goto L_0x0050
        L_0x0033:
            r2 = r1[r11]     // Catch:{ all -> 0x007e }
            int r2 = r2.intValue()     // Catch:{ all -> 0x007e }
            if (r2 != r10) goto L_0x0042
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r12.i     // Catch:{ all -> 0x007e }
            r1.remove(r6)     // Catch:{ all -> 0x007e }
            r1 = 1
            goto L_0x0050
        L_0x0042:
            r2 = r1[r11]     // Catch:{ all -> 0x007e }
            int r2 = r2.intValue()     // Catch:{ all -> 0x007e }
            int r2 = r2 - r10
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x007e }
            r1[r11] = r2     // Catch:{ all -> 0x007e }
            goto L_0x0031
        L_0x0050:
            if (r1 != 0) goto L_0x005a
        L_0x0052:
            boolean r1 = r12.h     // Catch:{ all -> 0x007e }
            if (r1 != 0) goto L_0x0079
            int r1 = r12.j     // Catch:{ all -> 0x007e }
            if (r1 != r10) goto L_0x0079
        L_0x005a:
            com.fossil.h92 r1 = com.fossil.h92.a()     // Catch:{ all -> 0x007e }
            android.content.Context r2 = r12.g     // Catch:{ all -> 0x007e }
            android.os.PowerManager$WakeLock r3 = r12.b     // Catch:{ all -> 0x007e }
            java.lang.String r3 = com.fossil.g92.a(r3, r6)     // Catch:{ all -> 0x007e }
            r4 = 8
            java.lang.String r5 = r12.e     // Catch:{ all -> 0x007e }
            r7 = 0
            int r8 = r12.d     // Catch:{ all -> 0x007e }
            java.util.List r9 = r12.b()     // Catch:{ all -> 0x007e }
            r1.a(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x007e }
            int r1 = r12.j     // Catch:{ all -> 0x007e }
            int r1 = r1 - r10
            r12.j = r1     // Catch:{ all -> 0x007e }
        L_0x0079:
            monitor-exit(r0)     // Catch:{ all -> 0x007e }
            r12.a(r11)
            return
        L_0x007e:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ao3.a():void");
    }

    @DexIgnore
    public final void a(int i2) {
        if (this.b.isHeld()) {
            try {
                this.b.release();
            } catch (RuntimeException e2) {
                if (e2.getClass().equals(RuntimeException.class)) {
                    Log.e("WakeLock", String.valueOf(this.e).concat(" was already released!"), e2);
                } else {
                    throw e2;
                }
            }
            this.b.isHeld();
        }
    }

    @DexIgnore
    public final String a(String str) {
        if (this.h) {
            return !TextUtils.isEmpty(str) ? str : this.f;
        }
        return this.f;
    }

    @DexIgnore
    public void a(boolean z) {
        this.b.setReferenceCounted(z);
        this.h = z;
    }
}
