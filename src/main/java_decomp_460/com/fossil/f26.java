package com.fossil;

import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f26 extends tq4<a, b, tq4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ContactGroup f1041a;

        @DexIgnore
        public a(ContactGroup contactGroup) {
            pq7.c(contactGroup, "contactGroup");
            this.f1041a = contactGroup;
        }

        @DexIgnore
        public final ContactGroup a() {
            return this.f1041a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.c {
        @DexIgnore
        public b(boolean z) {
        }
    }

    /*
    static {
        String simpleName = f26.class.getSimpleName();
        pq7.b(simpleName, "RemoveContactGroup::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public f26(NotificationsRepository notificationsRepository) {
        pq7.c(notificationsRepository, "notificationsRepository");
        i14.o(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        pq7.b(notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    /* renamed from: f */
    public void a(a aVar) {
        pq7.c(aVar, "requestValues");
        g(aVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .RemoveContactGroup done");
        b().onSuccess(new b(true));
    }

    @DexIgnore
    public final void g(ContactGroup contactGroup) {
        Contact contact = contactGroup.getContacts().get(0);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        StringBuilder sb = new StringBuilder();
        sb.append("Removed contact = ");
        pq7.b(contact, "contact");
        sb.append(contact.getFirstName());
        sb.append(" row id = ");
        sb.append(contact.getDbRowId());
        local.d(str, sb.toString());
        ArrayList arrayList = new ArrayList();
        for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
            pq7.b(phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            arrayList.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
        }
        this.d.removeContact(contact);
        this.d.removeContactGroup(contactGroup);
        h(arrayList);
    }

    @DexIgnore
    public final void h(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(phoneFavoritesContact);
        }
    }
}
