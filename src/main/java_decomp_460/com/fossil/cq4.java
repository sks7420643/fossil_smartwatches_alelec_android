package com.fossil;

import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cq4 implements Factory<DownloadServiceApi> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f639a;

    @DexIgnore
    public cq4(uo4 uo4) {
        this.f639a = uo4;
    }

    @DexIgnore
    public static cq4 a(uo4 uo4) {
        return new cq4(uo4);
    }

    @DexIgnore
    public static DownloadServiceApi c(uo4 uo4) {
        DownloadServiceApi J = uo4.J();
        lk7.c(J, "Cannot return null from a non-@Nullable @Provides method");
        return J;
    }

    @DexIgnore
    /* renamed from: b */
    public DownloadServiceApi get() {
        return c(this.f639a);
    }
}
