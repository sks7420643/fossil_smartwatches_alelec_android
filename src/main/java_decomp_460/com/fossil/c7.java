package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c7 extends h7 {
    @DexIgnore
    public /* final */ UUID[] b;
    @DexIgnore
    public /* final */ n6[] c;

    @DexIgnore
    public c7(g7 g7Var, UUID[] uuidArr, n6[] n6VarArr) {
        super(g7Var);
        this.b = uuidArr;
        this.c = n6VarArr;
    }
}
