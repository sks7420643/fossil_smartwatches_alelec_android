package com.fossil;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.StrictMode;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv2 implements nv2 {
    @DexIgnore
    public static /* final */ Map<Uri, jv2> g; // = new zi0();
    @DexIgnore
    public static /* final */ String[] h; // = {"key", "value"};

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ContentResolver f1813a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentObserver c; // = new lv2(this, null);
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public volatile Map<String, String> e;
    @DexIgnore
    public /* final */ List<ov2> f; // = new ArrayList();

    @DexIgnore
    public jv2(ContentResolver contentResolver, Uri uri) {
        this.f1813a = contentResolver;
        this.b = uri;
        contentResolver.registerContentObserver(uri, false, this.c);
    }

    @DexIgnore
    public static jv2 a(ContentResolver contentResolver, Uri uri) {
        jv2 jv2;
        synchronized (jv2.class) {
            try {
                jv2 = g.get(uri);
                if (jv2 == null) {
                    try {
                        jv2 jv22 = new jv2(contentResolver, uri);
                        try {
                            g.put(uri, jv22);
                            jv2 = jv22;
                        } catch (SecurityException e2) {
                            jv2 = jv22;
                        }
                    } catch (SecurityException e3) {
                    }
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return jv2;
    }

    @DexIgnore
    public static void d() {
        synchronized (jv2.class) {
            try {
                for (jv2 jv2 : g.values()) {
                    jv2.f1813a.unregisterContentObserver(jv2.c);
                }
                g.clear();
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public final Map<String, String> b() {
        Map<String, String> map = this.e;
        if (map == null) {
            synchronized (this.d) {
                map = this.e;
                if (map == null) {
                    map = f();
                    this.e = map;
                }
            }
        }
        return map != null ? map : Collections.emptyMap();
    }

    @DexIgnore
    public final void c() {
        synchronized (this.d) {
            this.e = null;
            xv2.g();
        }
        synchronized (this) {
            for (ov2 ov2 : this.f) {
                ov2.zza();
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Map e() {
        Cursor query = this.f1813a.query(this.b, h, null, null, null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            Map zi0 = count <= 256 ? new zi0(count) : new HashMap(count, 1.0f);
            while (query.moveToNext()) {
                zi0.put(query.getString(0), query.getString(1));
            }
            query.close();
            return zi0;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final Map<String, String> f() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            Map<String, String> map = (Map) qv2.a(new mv2(this));
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return map;
        } catch (SQLiteException | IllegalStateException | SecurityException e2) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return null;
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.nv2
    public final /* synthetic */ Object zza(String str) {
        return b().get(str);
    }
}
