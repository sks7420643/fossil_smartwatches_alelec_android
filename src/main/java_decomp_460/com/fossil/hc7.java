package com.fossil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ HashMap<Object, List<Object>> f1462a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<Object, gc7> b; // = new HashMap<>();
    @DexIgnore
    public static /* final */ hc7 c; // = new hc7();

    @DexIgnore
    public final void a(Object obj) {
        pq7.c(obj, "root");
        if (b.get(obj) == null) {
            b.put(obj, new gc7());
        }
    }

    @DexIgnore
    public final void b(Object obj, Object obj2) {
        pq7.c(obj, "root");
        pq7.c(obj2, "branch");
        List<Object> list = f1462a.get(obj);
        if (list == null) {
            f1462a.put(obj, hm7.i(obj2));
            return;
        }
        list.add(obj2);
        f1462a.put(obj, list);
    }

    @DexIgnore
    public final void c(Object obj) {
        pq7.c(obj, "root");
        f1462a.remove(obj);
        b.remove(obj);
    }

    @DexIgnore
    public final gc7 d(Object obj) {
        pq7.c(obj, "rootOrBranch");
        gc7 f = f(obj);
        return f != null ? f : e(obj);
    }

    @DexIgnore
    public final gc7 e(Object obj) {
        pq7.c(obj, "branch");
        for (Map.Entry<Object, List<Object>> entry : f1462a.entrySet()) {
            List<Object> value = entry.getValue();
            if (value != null && value.contains(obj)) {
                return b.get(entry.getKey());
            }
        }
        return null;
    }

    @DexIgnore
    public final gc7 f(Object obj) {
        pq7.c(obj, "root");
        return b.get(obj);
    }
}
