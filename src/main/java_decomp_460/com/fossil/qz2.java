package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Class<?> f3052a; // = a("libcore.io.Memory");
    @DexIgnore
    public static /* final */ boolean b; // = (a("org.robolectric.Robolectric") != null);

    @DexIgnore
    public static <T> Class<T> a(String str) {
        try {
            return (Class<T>) Class.forName(str);
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static boolean b() {
        return f3052a != null && !b;
    }

    @DexIgnore
    public static Class<?> c() {
        return f3052a;
    }
}
