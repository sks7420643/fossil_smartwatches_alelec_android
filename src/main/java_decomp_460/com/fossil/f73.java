package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f73 implements g73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1063a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.collection.firebase_global_collection_flag_enabled", true);

    @DexIgnore
    @Override // com.fossil.g73
    public final boolean zza() {
        return f1063a.o().booleanValue();
    }
}
