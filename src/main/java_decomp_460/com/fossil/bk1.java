package com.fossil;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bk1 extends FilterInputStream {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public int c;

    @DexIgnore
    public bk1(InputStream inputStream, long j) {
        super(inputStream);
        this.b = j;
    }

    @DexIgnore
    public static InputStream b(InputStream inputStream, long j) {
        return new bk1(inputStream, j);
    }

    @DexIgnore
    public final int a(int i) throws IOException {
        if (i >= 0) {
            this.c += i;
        } else if (this.b - ((long) this.c) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.b + ", but read: " + this.c);
        }
        return i;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() throws IOException {
        int max;
        synchronized (this) {
            max = (int) Math.max(this.b - ((long) this.c), (long) ((FilterInputStream) this).in.available());
        }
        return max;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        int read;
        synchronized (this) {
            read = super.read();
            a(read >= 0 ? 1 : -1);
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read;
        synchronized (this) {
            read = super.read(bArr, i, i2);
            a(read);
        }
        return read;
    }
}
