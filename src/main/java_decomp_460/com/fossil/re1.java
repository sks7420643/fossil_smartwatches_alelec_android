package com.fossil;

import android.util.Base64;
import com.fossil.af1;
import com.fossil.wb1;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class re1<Model, Data> implements af1<Model, Data> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a<Data> f3101a;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Data b(String str) throws IllegalArgumentException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<Data> implements wb1<Data> {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ a<Data> c;
        @DexIgnore
        public Data d;

        @DexIgnore
        public b(String str, a<Data> aVar) {
            this.b = str;
            this.c = aVar;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
            try {
                this.c.a(this.d);
            } catch (IOException e) {
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super Data> aVar) {
            try {
                Data b2 = this.c.b(this.b);
                this.d = b2;
                aVar.e(b2);
            } catch (IllegalArgumentException e) {
                aVar.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<Data> getDataClass() {
            return this.c.getDataClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Model> implements bf1<Model, InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ a<InputStream> f3102a; // = new a(this);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements a<InputStream> {
            @DexIgnore
            public a(c cVar) {
            }

            @DexIgnore
            /* renamed from: c */
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            @DexIgnore
            /* renamed from: d */
            public InputStream b(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            @DexIgnore
            @Override // com.fossil.re1.a
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Model, InputStream> b(ef1 ef1) {
            return new re1(this.f3102a);
        }
    }

    @DexIgnore
    public re1(a<Data> aVar) {
        this.f3101a = aVar;
    }

    @DexIgnore
    @Override // com.fossil.af1
    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }

    @DexIgnore
    @Override // com.fossil.af1
    public af1.a<Data> b(Model model, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(model), new b(model.toString(), this.f3101a));
    }
}
