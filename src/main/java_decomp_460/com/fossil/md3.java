package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface md3 extends IInterface {
    @DexIgnore
    ac3 F2(rg2 rg2) throws RemoteException;

    @DexIgnore
    void p2(rg2 rg2, int i) throws RemoteException;

    @DexIgnore
    bc3 q0(rg2 rg2, GoogleMapOptions googleMapOptions) throws RemoteException;

    @DexIgnore
    ec3 t1(rg2 rg2, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException;

    @DexIgnore
    yb3 zze() throws RemoteException;

    @DexIgnore
    fs2 zzf() throws RemoteException;
}
