package com.fossil;

import com.fossil.v18;
import com.misfit.frameworks.common.constants.Constants;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc0 implements Interceptor {
    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        v18.a h = chain.c().h();
        h.a("Content-Type", Constants.CONTENT_TYPE);
        h.a("User-Agent", hd0.y.y());
        h.a("locale", hd0.y.n());
        Response d = chain.d(h.b());
        pq7.b(d, "chain.proceed(requestBuilder.build())");
        return d;
    }
}
