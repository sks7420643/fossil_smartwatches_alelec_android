package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.fitness.data.MapValue;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ai2> CREATOR; // = new gi2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public float d;
    @DexIgnore
    public String e;
    @DexIgnore
    public Map<String, MapValue> f;
    @DexIgnore
    public int[] g;
    @DexIgnore
    public float[] h;
    @DexIgnore
    public byte[] i;

    @DexIgnore
    public ai2(int i2) {
        this(i2, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, null, null, null, null);
    }

    @DexIgnore
    public ai2(int i2, boolean z, float f2, String str, Bundle bundle, int[] iArr, float[] fArr, byte[] bArr) {
        zi0 zi0;
        this.b = i2;
        this.c = z;
        this.d = f2;
        this.e = str;
        if (bundle == null) {
            zi0 = null;
        } else {
            bundle.setClassLoader(MapValue.class.getClassLoader());
            zi0 zi02 = new zi0(bundle.size());
            for (String str2 : bundle.keySet()) {
                zi02.put(str2, (MapValue) bundle.getParcelable(str2));
            }
            zi0 = zi02;
        }
        this.f = zi0;
        this.g = iArr;
        this.h = fArr;
        this.i = bArr;
    }

    @DexIgnore
    @Deprecated
    public final void A(String str) {
        F(hp2.a(str));
    }

    @DexIgnore
    @Deprecated
    public final void D(float f2) {
        rc2.o(this.b == 2, "Attempting to set an float value to a field that is not in FLOAT format.  Please check the data type definition and use the right format.");
        this.c = true;
        this.d = f2;
    }

    @DexIgnore
    @Deprecated
    public final void F(int i2) {
        rc2.o(this.b == 1, "Attempting to set an int value to a field that is not in INT32 format.  Please check the data type definition and use the right format.");
        this.c = true;
        this.d = Float.intBitsToFloat(i2);
    }

    @DexIgnore
    public final float c() {
        rc2.o(this.b == 2, "Value is not in float format");
        return this.d;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ai2)) {
            return false;
        }
        ai2 ai2 = (ai2) obj;
        int i2 = this.b;
        if (i2 == ai2.b && this.c == ai2.c) {
            switch (i2) {
                case 1:
                    if (f() == ai2.f()) {
                        return true;
                    }
                    break;
                case 2:
                    return this.d == ai2.d;
                case 3:
                    return pc2.a(this.e, ai2.e);
                case 4:
                    return pc2.a(this.f, ai2.f);
                case 5:
                    return Arrays.equals(this.g, ai2.g);
                case 6:
                    return Arrays.equals(this.h, ai2.h);
                case 7:
                    return Arrays.equals(this.i, ai2.i);
                default:
                    if (this.d == ai2.d) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    @DexIgnore
    public final int f() {
        boolean z = true;
        if (this.b != 1) {
            z = false;
        }
        rc2.o(z, "Value is not in int format");
        return Float.floatToRawIntBits(this.d);
    }

    @DexIgnore
    public final int h() {
        return this.b;
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(Float.valueOf(this.d), this.e, this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    public final boolean k() {
        return this.c;
    }

    @DexIgnore
    public final String toString() {
        if (!this.c) {
            return "unset";
        }
        switch (this.b) {
            case 1:
                return Integer.toString(f());
            case 2:
                return Float.toString(this.d);
            case 3:
                return this.e;
            case 4:
                return new TreeMap(this.f).toString();
            case 5:
                return Arrays.toString(this.g);
            case 6:
                return Arrays.toString(this.h);
            case 7:
                byte[] bArr = this.i;
                return kf2.a(bArr, 0, bArr.length, false);
            default:
                return "unknown";
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        Bundle bundle;
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, h());
        bd2.c(parcel, 2, k());
        bd2.j(parcel, 3, this.d);
        bd2.u(parcel, 4, this.e, false);
        if (this.f == null) {
            bundle = null;
        } else {
            Bundle bundle2 = new Bundle(this.f.size());
            for (Map.Entry<String, MapValue> entry : this.f.entrySet()) {
                bundle2.putParcelable(entry.getKey(), entry.getValue());
            }
            bundle = bundle2;
        }
        bd2.e(parcel, 5, bundle, false);
        bd2.o(parcel, 6, this.g, false);
        bd2.k(parcel, 7, this.h, false);
        bd2.g(parcel, 8, this.i, false);
        bd2.b(parcel, a2);
    }
}
