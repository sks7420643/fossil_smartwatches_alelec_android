package com.fossil;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zi0<g72<?>, z52> f2807a; // = new zi0<>();
    @DexIgnore
    public /* final */ zi0<g72<?>, String> b; // = new zi0<>();
    @DexIgnore
    public /* final */ ot3<Map<g72<?>, String>> c; // = new ot3<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public pa2(Iterable<? extends s62<?>> iterable) {
        Iterator<? extends s62<?>> it = iterable.iterator();
        while (it.hasNext()) {
            this.f2807a.put(((s62) it.next()).a(), null);
        }
        this.d = this.f2807a.keySet().size();
    }

    @DexIgnore
    public final nt3<Map<g72<?>, String>> a() {
        return this.c.a();
    }

    @DexIgnore
    public final void b(g72<?> g72, z52 z52, String str) {
        this.f2807a.put(g72, z52);
        this.b.put(g72, str);
        this.d--;
        if (!z52.A()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.b(new o62(this.f2807a));
            return;
        }
        this.c.c(this.b);
    }

    @DexIgnore
    public final Set<g72<?>> c() {
        return this.f2807a.keySet();
    }
}
