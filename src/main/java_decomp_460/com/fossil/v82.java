package com.fossil;

import android.os.Bundle;
import com.fossil.r62;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v82 implements r62.b {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ v72 c;
    @DexIgnore
    public /* final */ /* synthetic */ t82 d;

    @DexIgnore
    public v82(t82 t82, AtomicReference atomicReference, v72 v72) {
        this.d = t82;
        this.b = atomicReference;
        this.c = v72;
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void e(Bundle bundle) {
        this.d.y((r62) this.b.get(), this.c, true);
    }
}
