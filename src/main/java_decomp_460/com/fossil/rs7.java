package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs7<T> implements ts7<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gp7<T> f3162a;
    @DexIgnore
    public /* final */ rp7<T, T> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, jr7 {
        @DexIgnore
        public T b;
        @DexIgnore
        public int c; // = -2;
        @DexIgnore
        public /* final */ /* synthetic */ rs7 d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(rs7 rs7) {
            this.d = rs7;
        }

        @DexIgnore
        public final void a() {
            T t;
            if (this.c == -2) {
                t = (T) this.d.f3162a.invoke();
            } else {
                rp7 rp7 = this.d.b;
                T t2 = this.b;
                if (t2 != null) {
                    t = (T) rp7.invoke(t2);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.b = t;
            this.c = t == null ? 0 : 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.c < 0) {
                a();
            }
            return this.c == 1;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (this.c < 0) {
                a();
            }
            if (this.c != 0) {
                T t = this.b;
                if (t != null) {
                    this.c = -1;
                    return t;
                }
                throw new il7("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.gp7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.rp7<? super T, ? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public rs7(gp7<? extends T> gp7, rp7<? super T, ? extends T> rp7) {
        pq7.c(gp7, "getInitialValue");
        pq7.c(rp7, "getNextValue");
        this.f3162a = gp7;
        this.b = rp7;
    }

    @DexIgnore
    @Override // com.fossil.ts7
    public Iterator<T> iterator() {
        return new a(this);
    }
}
