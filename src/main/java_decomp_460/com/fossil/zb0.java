package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zb0 implements Parcelable.Creator<ac0> {
    @DexIgnore
    public /* synthetic */ zb0(kq7 kq7) {
    }

    @DexIgnore
    public final ac0 a(byte[] bArr) {
        try {
            return new ac0(bArr);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ac0 createFromParcel(Parcel parcel) {
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            pq7.b(createByteArray, "parcel.createByteArray()!!");
            return new ac0(createByteArray);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ac0[] newArray(int i) {
        return new ac0[i];
    }
}
