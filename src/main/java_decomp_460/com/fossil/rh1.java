package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rh1 implements th1<Drawable, byte[]> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rd1 f3112a;
    @DexIgnore
    public /* final */ th1<Bitmap, byte[]> b;
    @DexIgnore
    public /* final */ th1<hh1, byte[]> c;

    @DexIgnore
    public rh1(rd1 rd1, th1<Bitmap, byte[]> th1, th1<hh1, byte[]> th12) {
        this.f3112a = rd1;
        this.b = th1;
        this.c = th12;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.id1<android.graphics.drawable.Drawable> */
    /* JADX WARN: Multi-variable type inference failed */
    public static id1<hh1> b(id1<Drawable> id1) {
        return id1;
    }

    @DexIgnore
    @Override // com.fossil.th1
    public id1<byte[]> a(id1<Drawable> id1, ob1 ob1) {
        Drawable drawable = id1.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(yf1.f(((BitmapDrawable) drawable).getBitmap(), this.f3112a), ob1);
        }
        if (!(drawable instanceof hh1)) {
            return null;
        }
        th1<hh1, byte[]> th1 = this.c;
        b(id1);
        return th1.a(id1, ob1);
    }
}
