package com.fossil;

import android.database.Cursor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e31 implements d31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f873a;
    @DexIgnore
    public /* final */ jw0<c31> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<c31> {
        @DexIgnore
        public a(e31 e31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, c31 c31) {
            String str = c31.f550a;
            if (str == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, str);
            }
            Long l = c31.b;
            if (l == null) {
                px0.bindNull(2);
            } else {
                px0.bindLong(2, l.longValue());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public e31(qw0 qw0) {
        this.f873a = qw0;
        this.b = new a(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.d31
    public Long a(String str) {
        Long l = null;
        tw0 f = tw0.f("SELECT long_value FROM Preference where `key`=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.f873a.assertNotSuspendingTransaction();
        Cursor b2 = ex0.b(this.f873a, f, false, null);
        try {
            if (b2.moveToFirst() && !b2.isNull(0)) {
                l = Long.valueOf(b2.getLong(0));
            }
            return l;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.d31
    public void b(c31 c31) {
        this.f873a.assertNotSuspendingTransaction();
        this.f873a.beginTransaction();
        try {
            this.b.insert((jw0<c31>) c31);
            this.f873a.setTransactionSuccessful();
        } finally {
            this.f873a.endTransaction();
        }
    }
}
