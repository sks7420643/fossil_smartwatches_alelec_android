package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.fu6;
import com.fossil.iq4;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu6 extends hq4 {
    @DexIgnore
    public /* final */ LiveData<List<WorkoutSetting>> h; // = or0.c(null, 0, new c(this, null), 3, null);
    @DexIgnore
    public /* final */ MutableLiveData<a> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ fu6 j;
    @DexIgnore
    public /* final */ WorkoutSettingRepository k;

    @DexIgnore
    public enum a {
        WAITING_FOR_CONFIRMATION,
        SKIP,
        SUCCESS,
        FAIL_DEVICE_DISCONNECT,
        FAIL_UNKNOWN
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel$checkDataChanged$1", f = "WorkoutSettingViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $newList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mu6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(mu6 mu6, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mu6;
            this.$newList = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$newList, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object workoutSettingList;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                WorkoutSettingRepository workoutSettingRepository = this.this$0.k;
                this.L$0 = iv7;
                this.label = 1;
                workoutSettingList = workoutSettingRepository.getWorkoutSettingList(this);
                if (workoutSettingList == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                workoutSettingList = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.t((List) workoutSettingList, this.$newList)) {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "data is changed, ask user to set data to watch");
                hq4.d(this.this$0, false, true, null, 5, null);
                this.this$0.r().l(a.WAITING_FOR_CONFIRMATION);
            } else {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "data is not changed, just skip");
                hq4.d(this.this$0, false, true, null, 5, null);
                this.this$0.r().l(a.SKIP);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel$mWorkoutSettingsLiveData$1", f = "WorkoutSettingViewModel.kt", l = {20, 20}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<hs0<List<? extends WorkoutSetting>>, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mu6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mu6 mu6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mu6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (hs0) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(hs0<List<? extends WorkoutSetting>> hs0, qn7<? super tl7> qn7) {
            return ((c) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0038
                if (r0 == r2) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                com.fossil.el7.b(r6)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$1
                com.fossil.hs0 r0 = (com.fossil.hs0) r0
                java.lang.Object r1 = r5.L$0
                com.fossil.hs0 r1 = (com.fossil.hs0) r1
                com.fossil.el7.b(r6)
                r2 = r1
            L_0x002c:
                r5.L$0 = r2
                r5.label = r4
                java.lang.Object r0 = r0.b(r6, r5)
                if (r0 != r3) goto L_0x0015
                r0 = r3
                goto L_0x0017
            L_0x0038:
                com.fossil.el7.b(r6)
                com.fossil.hs0 r1 = r5.p$
                com.fossil.mu6 r0 = r5.this$0
                com.portfolio.platform.data.source.WorkoutSettingRepository r0 = com.fossil.mu6.o(r0)
                r5.L$0 = r1
                r5.L$1 = r1
                r5.label = r2
                java.lang.Object r6 = r0.getWorkoutSettingList(r5)
                if (r6 != r3) goto L_0x0051
                r0 = r3
                goto L_0x0017
            L_0x0051:
                r0 = r1
                r2 = r1
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mu6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.e<fu6.c, fu6.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ mu6 f2423a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(mu6 mu6) {
            this.f2423a = mu6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(fu6.b bVar) {
            pq7.c(bVar, "errorValue");
            hq4.d(this.f2423a, false, true, null, 5, null);
            if (!jn5.b.m(PortfolioApp.h0.c().getApplicationContext(), jn5.c.BLUETOOTH_CONNECTION)) {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch fail, missed permissions");
                this.f2423a.e(uh5.BLUETOOTH_OFF);
                return;
            }
            int b = bVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutSettingViewModel", "set WorkoutSetting to Watch - onError - lastErrorCode = " + b);
            if (b == 1101) {
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026de(errorValue.errorCodes)");
                mu6 mu6 = this.f2423a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    mu6.e((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (b != 8888) {
                this.f2423a.r().l(a.FAIL_UNKNOWN);
            } else {
                this.f2423a.r().l(a.FAIL_DEVICE_DISCONNECT);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(fu6.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch success");
            hq4.d(this.f2423a, false, true, null, 5, null);
            this.f2423a.r().l(a.SUCCESS);
        }
    }

    @DexIgnore
    public mu6(fu6 fu6, WorkoutSettingRepository workoutSettingRepository) {
        pq7.c(fu6, "mSetWorkoutSetting");
        pq7.c(workoutSettingRepository, "mWorkoutSettingRepository");
        this.j = fu6;
        this.k = workoutSettingRepository;
        this.j.p();
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        this.j.r();
        super.onCleared();
    }

    @DexIgnore
    public final void q(List<WorkoutSetting> list) {
        pq7.c(list, "newList");
        FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "checkDataChanged()");
        hq4.d(this, true, false, null, 6, null);
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, list, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<a> r() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<List<WorkoutSetting>> s() {
        return this.h;
    }

    @DexIgnore
    public final boolean t(List<WorkoutSetting> list, List<WorkoutSetting> list2) {
        loop0:
        for (WorkoutSetting workoutSetting : list2) {
            Iterator<WorkoutSetting> it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    WorkoutSetting next = it.next();
                    if (workoutSetting.getType() != next.getType() || (workoutSetting.getAskMeFirst() == next.getAskMeFirst() && workoutSetting.getEnable() == next.getEnable())) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void u(List<WorkoutSetting> list) {
        pq7.c(list, "workoutSettingList");
        FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch()");
        hq4.d(this, true, false, null, 6, null);
        this.j.e(new fu6.a(list), new d(this));
    }
}
