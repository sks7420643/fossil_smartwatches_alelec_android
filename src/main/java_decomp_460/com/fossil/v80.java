package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class v80 extends Enum<v80> {
    @DexIgnore
    public static /* final */ v80 b;
    @DexIgnore
    public static /* final */ v80 c;
    @DexIgnore
    public static /* final */ v80 d;
    @DexIgnore
    public static /* final */ v80 e;
    @DexIgnore
    public static /* final */ v80 f;
    @DexIgnore
    public static /* final */ v80 g;
    @DexIgnore
    public static /* final */ v80 h;
    @DexIgnore
    public static /* final */ v80 i;
    @DexIgnore
    public static /* final */ v80 j;
    @DexIgnore
    public static /* final */ /* synthetic */ v80[] k;

    /*
    static {
        v80 v80 = new v80("PHASE_INIT", 0);
        b = v80;
        v80 v802 = new v80("PHASE_START", 1);
        c = v802;
        v80 v803 = new v80("PHASE_END", 2);
        d = v803;
        v80 v804 = new v80("REQUEST", 3);
        e = v804;
        v80 v805 = new v80("RESPONSE", 4);
        f = v805;
        v80 v806 = new v80("SYSTEM_EVENT", 5);
        g = v806;
        v80 v807 = new v80("CENTRAL_EVENT", 6);
        h = v807;
        v80 v808 = new v80("DEVICE_EVENT", 7);
        i = v808;
        v80 v809 = new v80("GATT_SERVER_EVENT", 8);
        v80 v8010 = new v80("EXCEPTION", 9);
        v80 v8011 = new v80("DATABASE", 10);
        v80 v8012 = new v80("MSL", 11);
        j = v8012;
        k = new v80[]{v80, v802, v803, v804, v805, v806, v807, v808, v809, v8010, v8011, v8012};
    }
    */

    @DexIgnore
    public v80(String str, int i2) {
    }

    @DexIgnore
    public static v80 valueOf(String str) {
        return (v80) Enum.valueOf(v80.class, str);
    }

    @DexIgnore
    public static v80[] values() {
        return (v80[]) k.clone();
    }
}
