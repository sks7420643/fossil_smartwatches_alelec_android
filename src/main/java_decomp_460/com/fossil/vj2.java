package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj2 extends vp2 implements uj2 {
    @DexIgnore
    public vj2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gcm.INetworkTaskCallback");
    }

    @DexIgnore
    @Override // com.fossil.uj2
    public final void d0(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        e(2, d);
    }
}
