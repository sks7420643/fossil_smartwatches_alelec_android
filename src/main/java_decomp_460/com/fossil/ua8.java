package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ua8 extends va8<Bitmap> {
    @DexIgnore
    public Bitmap e;

    @DexIgnore
    public ua8(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void c(Bitmap bitmap, tj1<? super Bitmap> tj1) {
        pq7.c(bitmap, "resource");
        this.e = bitmap;
    }

    @DexIgnore
    @Override // com.fossil.va8, com.fossil.ei1
    public void onDestroy() {
        Bitmap bitmap;
        super.onDestroy();
        Bitmap bitmap2 = this.e;
        if (bitmap2 != null && !bitmap2.isRecycled() && (bitmap = this.e) != null) {
            bitmap.recycle();
        }
    }
}
