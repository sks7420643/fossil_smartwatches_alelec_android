package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c22 implements Factory<b22> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Context> f549a;
    @DexIgnore
    public /* final */ Provider<t02> b;
    @DexIgnore
    public /* final */ Provider<k22> c;
    @DexIgnore
    public /* final */ Provider<h22> d;
    @DexIgnore
    public /* final */ Provider<Executor> e;
    @DexIgnore
    public /* final */ Provider<s32> f;
    @DexIgnore
    public /* final */ Provider<t32> g;

    @DexIgnore
    public c22(Provider<Context> provider, Provider<t02> provider2, Provider<k22> provider3, Provider<h22> provider4, Provider<Executor> provider5, Provider<s32> provider6, Provider<t32> provider7) {
        this.f549a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static c22 a(Provider<Context> provider, Provider<t02> provider2, Provider<k22> provider3, Provider<h22> provider4, Provider<Executor> provider5, Provider<s32> provider6, Provider<t32> provider7) {
        return new c22(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    /* renamed from: b */
    public b22 get() {
        return new b22(this.f549a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }
}
