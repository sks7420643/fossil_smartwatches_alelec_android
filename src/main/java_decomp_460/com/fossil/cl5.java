package com.fossil;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;
import com.fossil.zk0;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ NotificationManager f628a;
    @DexIgnore
    public static NotificationChannel b;
    @DexIgnore
    public static /* final */ cl5 c; // = new cl5();

    /*
    static {
        Object systemService = PortfolioApp.h0.c().getSystemService("notification");
        if (systemService != null) {
            f628a = (NotificationManager) systemService;
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
    }
    */

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0161  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.app.Notification a(com.fossil.ws4 r12, android.content.Context r13) {
        /*
        // Method dump skipped, instructions count: 359
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cl5.a(com.fossil.ws4, android.content.Context):android.app.Notification");
    }

    @DexIgnore
    public final Notification b(ws4 ws4) {
        String str;
        xs4 xs4;
        hz4 hz4 = hz4.f1561a;
        lt4 d = ws4.d();
        String b2 = d != null ? d.b() : null;
        lt4 d2 = ws4.d();
        String d3 = d2 != null ? d2.d() : null;
        lt4 d4 = ws4.d();
        if (d4 == null || (str = d4.e()) == null) {
            str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        String a2 = hz4.a(b2, d3, str);
        String c2 = um5.c(PortfolioApp.h0.c(), 2131887229);
        hr7 hr7 = hr7.f1520a;
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886195);
        pq7.b(c3, "LanguageHelper.getString\u2026Body_Send_Friend_Request)");
        String format = String.format(c3, Arrays.copyOf(new Object[]{a2}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        lt4 d5 = ws4.d();
        if (d5 != null) {
            String c4 = d5.c();
            String e = d5.e();
            if (e == null) {
                e = "";
            }
            String b3 = d5.b();
            if (b3 == null) {
                b3 = "";
            }
            String d6 = d5.d();
            if (d6 == null) {
                d6 = "";
            }
            String a3 = d5.a();
            if (a3 == null) {
                a3 = "";
            }
            xs4 = new xs4(c4, e, b3, d6, null, a3, false, 0, 2);
        } else {
            xs4 = null;
        }
        Intent intent = new Intent(PortfolioApp.h0.c(), BCNotificationActionReceiver.class);
        intent.setAction("com.buddy_challenge.friend.accept");
        intent.putExtra("friend_extra", xs4);
        PortfolioApp c5 = PortfolioApp.h0.c();
        String c6 = ws4.c();
        PendingIntent broadcast = PendingIntent.getBroadcast(c5, c6 != null ? c6.hashCode() : 0, intent, SQLiteDatabase.CREATE_IF_NECESSARY);
        Intent intent2 = new Intent(PortfolioApp.h0.c(), BCNotificationActionReceiver.class);
        intent2.setAction("com.buddy_challenge.friend.decline");
        intent2.putExtra("friend_extra", xs4);
        PortfolioApp c7 = PortfolioApp.h0.c();
        String c8 = ws4.c();
        PendingIntent broadcast2 = PendingIntent.getBroadcast(c7, c8 != null ? c8.hashCode() : 0, intent2, SQLiteDatabase.CREATE_IF_NECESSARY);
        String string = PortfolioApp.h0.c().getString(2131886294);
        pq7.b(string, "PortfolioApp.instance.ge\u2026Friends_List_CTA__Accept)");
        String string2 = PortfolioApp.h0.c().getString(2131886295);
        pq7.b(string2, "PortfolioApp.instance.ge\u2026riends_List_CTA__Decline)");
        BitmapFactory.decodeResource(PortfolioApp.h0.c().getResources(), 2131230909);
        zk0.e eVar = new zk0.e(PortfolioApp.h0.c(), "FOSSIL_NOTIFICATION_CHANNEL_ID");
        eVar.n(c2);
        eVar.m(format);
        eVar.y(R.drawable.ic_launcher_transparent);
        eVar.a(0, string, broadcast);
        eVar.a(0, string2, broadcast2);
        eVar.h(0);
        eVar.A(null);
        eVar.v(false);
        eVar.w(2);
        Notification c9 = eVar.c();
        pq7.b(c9, "NotificationCompat.Build\u2026\n                .build()");
        return c9;
    }

    @DexIgnore
    public final void c() {
        Object systemService = PortfolioApp.h0.c().getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).cancelAll();
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void d(Context context, int i) {
        pq7.c(context, "context");
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).cancel(i);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void e() {
        d(PortfolioApp.h0.c(), 999);
    }

    @DexIgnore
    public final void f() {
        if (b == null) {
            NotificationChannel notificationChannel = new NotificationChannel("FOSSIL_NOTIFICATION_CHANNEL_ID", "Fossil Smartwatches Notification", 2);
            b = notificationChannel;
            if (notificationChannel != null) {
                notificationChannel.enableLights(true);
                NotificationChannel notificationChannel2 = b;
                if (notificationChannel2 != null) {
                    notificationChannel2.enableVibration(true);
                    NotificationChannel notificationChannel3 = b;
                    if (notificationChannel3 != null) {
                        notificationChannel3.setImportance(4);
                        NotificationChannel notificationChannel4 = b;
                        if (notificationChannel4 != null) {
                            notificationChannel4.setShowBadge(true);
                            NotificationManager notificationManager = f628a;
                            NotificationChannel notificationChannel5 = b;
                            if (notificationChannel5 != null) {
                                notificationManager.createNotificationChannel(notificationChannel5);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void g(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends zk0.a> list) {
        pq7.c(context, "context");
        pq7.c(str, "title");
        pq7.c(str2, "text");
        zk0.c cVar = new zk0.c();
        cVar.g(str2);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        if (Build.VERSION.SDK_INT >= 26) {
            f();
        }
        zk0.e eVar = new zk0.e(context);
        eVar.j("FOSSIL_NOTIFICATION_CHANNEL_ID");
        eVar.n(str);
        eVar.m(str2);
        eVar.y(R.drawable.ic_launcher_transparent);
        eVar.A(cVar);
        eVar.w(2);
        eVar.C(new long[]{1000, 1000, 1000});
        eVar.z(defaultUri);
        eVar.l(pendingIntent);
        eVar.g(true);
        Notification c2 = eVar.c();
        c2.flags |= 16;
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = PortfolioApp.h0.c().getResources();
            Package r3 = android.R.class.getPackage();
            if (r3 != null) {
                pq7.b(r3, "android.R::class.java.`package`!!");
                int identifier = resources.getIdentifier("right_icon", "id", r3.getName());
                if (identifier != 0) {
                    RemoteViews remoteViews = c2.contentView;
                    if (remoteViews != null) {
                        remoteViews.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews2 = c2.headsUpContentView;
                    if (remoteViews2 != null) {
                        remoteViews2.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews3 = c2.bigContentView;
                    if (remoteViews3 != null) {
                        remoteViews3.setViewVisibility(identifier, 4);
                    }
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        if (list != null) {
            for (zk0.a aVar : list) {
                eVar.b(aVar);
            }
        }
        f628a.notify(i, c2);
    }

    @DexIgnore
    public final void h(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends zk0.a> list) {
        pq7.c(context, "context");
        pq7.c(str, "title");
        pq7.c(str2, "text");
        pq7.c(pendingIntent, "pendingIntent");
        zk0.c cVar = new zk0.c();
        cVar.g(str2);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), 2131689472);
        zk0.e eVar = new zk0.e(context);
        eVar.n(str);
        eVar.m(str2);
        eVar.y(2131689472);
        eVar.r(decodeResource);
        eVar.A(cVar);
        eVar.C(new long[]{1000, 1000, 1000});
        eVar.z(defaultUri);
        eVar.l(pendingIntent);
        Notification c2 = eVar.c();
        c2.flags |= 16;
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = PortfolioApp.h0.c().getResources();
            Package r2 = android.R.class.getPackage();
            if (r2 != null) {
                pq7.b(r2, "android.R::class.java.`package`!!");
                int identifier = resources.getIdentifier("right_icon", "id", r2.getName());
                if (identifier != 0) {
                    RemoteViews remoteViews = c2.contentView;
                    if (remoteViews != null) {
                        remoteViews.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews2 = c2.headsUpContentView;
                    if (remoteViews2 != null) {
                        remoteViews2.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews3 = c2.bigContentView;
                    if (remoteViews3 != null) {
                        remoteViews3.setViewVisibility(identifier, 4);
                    }
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        if (list != null) {
            for (zk0.a aVar : list) {
                eVar.b(aVar);
            }
        }
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).notify(i, c2);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void i(ws4 ws4, Context context) {
        int hashCode;
        Notification b2;
        String a2;
        pq7.c(ws4, "notification");
        pq7.c(context, "context");
        ss4 b3 = ws4.b();
        if (b3 == null || (a2 = b3.a()) == null) {
            lt4 d = ws4.d();
            String c2 = d != null ? d.c() : null;
            hashCode = c2 != null ? c2.hashCode() : 0;
        } else {
            hashCode = a2.hashCode();
        }
        if (Build.VERSION.SDK_INT >= 26) {
            f();
        }
        String e = ws4.e();
        int hashCode2 = e.hashCode();
        if (hashCode2 != 1433363166) {
            if (hashCode2 == 1898363949 && e.equals("Title_Send_Invitation_Challenge")) {
                b2 = a(ws4, context);
            }
            throw new Exception("Wrong type");
        }
        if (e.equals("Title_Send_Friend_Request")) {
            b2 = b(ws4);
        }
        throw new Exception("Wrong type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("Helper", "notificationID: " + hashCode);
        f628a.notify(hashCode, b2);
    }

    @DexIgnore
    public final void j() {
        Intent intent = new Intent(PortfolioApp.h0.c(), BCNotificationActionReceiver.class);
        intent.setAction("com.buddy_challenge.set_sync_data");
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.h0.c(), 9, intent, SQLiteDatabase.CREATE_IF_NECESSARY);
        zk0.e eVar = new zk0.e(PortfolioApp.h0.c(), "FOSSIL_NOTIFICATION_CHANNEL_ID");
        eVar.n(um5.c(PortfolioApp.h0.c(), 2131886223));
        eVar.y(R.drawable.ic_launcher_transparent);
        eVar.a(0, um5.c(PortfolioApp.h0.c(), 2131886130), broadcast);
        eVar.h(0);
        zk0.c cVar = new zk0.c();
        cVar.g(um5.c(PortfolioApp.h0.c(), 2131886225));
        eVar.A(cVar);
        eVar.v(false);
        eVar.w(2);
        Notification c2 = eVar.c();
        Object systemService = PortfolioApp.h0.c().getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).notify(999, c2);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
    }
}
