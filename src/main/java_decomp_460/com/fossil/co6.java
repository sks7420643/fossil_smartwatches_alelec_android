package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class co6 implements Factory<bo6> {
    @DexIgnore
    public static bo6 a(WeakReference<ao6> weakReference, PortfolioApp portfolioApp, vu5 vu5, xu5 xu5, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, zu5 zu5, hu4 hu4, tt4 tt4, on5 on5) {
        return new bo6(weakReference, portfolioApp, vu5, xu5, deviceRepository, userRepository, summariesRepository, sleepSummariesRepository, zu5, hu4, tt4, on5);
    }
}
