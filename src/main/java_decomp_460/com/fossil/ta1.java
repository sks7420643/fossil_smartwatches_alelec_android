package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta1 {
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int async; // = 2131361895;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361918;
    @DexIgnore
    public static /* final */ int bottom; // = 2131361919;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362026;
    @DexIgnore
    public static /* final */ int end; // = 2131362225;
    @DexIgnore
    public static /* final */ int forever; // = 2131362328;
    @DexIgnore
    public static /* final */ int glide_custom_view_target_tag; // = 2131362567;
    @DexIgnore
    public static /* final */ int icon; // = 2131362608;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362609;
    @DexIgnore
    public static /* final */ int info; // = 2131362633;
    @DexIgnore
    public static /* final */ int italic; // = 2131362649;
    @DexIgnore
    public static /* final */ int left; // = 2131362781;
    @DexIgnore
    public static /* final */ int line1; // = 2131362784;
    @DexIgnore
    public static /* final */ int line3; // = 2131362786;
    @DexIgnore
    public static /* final */ int none; // = 2131362882;
    @DexIgnore
    public static /* final */ int normal; // = 2131362883;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362884;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362885;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362886;
    @DexIgnore
    public static /* final */ int right; // = 2131362996;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362997;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362998;
    @DexIgnore
    public static /* final */ int start; // = 2131363146;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363181;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363182;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363183;
    @DexIgnore
    public static /* final */ int text; // = 2131363190;
    @DexIgnore
    public static /* final */ int text2; // = 2131363191;
    @DexIgnore
    public static /* final */ int time; // = 2131363213;
    @DexIgnore
    public static /* final */ int title; // = 2131363215;
    @DexIgnore
    public static /* final */ int top; // = 2131363225;
}
