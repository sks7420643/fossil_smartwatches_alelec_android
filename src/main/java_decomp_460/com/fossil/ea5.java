package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ea5 extends da5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362730, 1);
        x.put(2131362546, 2);
        x.put(2131363047, 3);
        x.put(2131361959, 4);
    }
    */

    @DexIgnore
    public ea5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 5, w, x));
    }

    @DexIgnore
    public ea5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[4], (FlexibleTextView) objArr[2], (ImageView) objArr[1], (ConstraintLayout) objArr[0], (RecyclerView) objArr[3]);
        this.v = -1;
        this.t.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.v != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.v = 1;
        }
        w();
    }
}
