package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hf6 implements MembersInjector<ActivityOverviewFragment> {
    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, ef6 ef6) {
        activityOverviewFragment.h = ef6;
    }

    @DexIgnore
    public static void b(ActivityOverviewFragment activityOverviewFragment, pf6 pf6) {
        activityOverviewFragment.j = pf6;
    }

    @DexIgnore
    public static void c(ActivityOverviewFragment activityOverviewFragment, vf6 vf6) {
        activityOverviewFragment.i = vf6;
    }
}
