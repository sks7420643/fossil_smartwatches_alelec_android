package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConcurrentMap<Class<?>, Set<pc7>> f2502a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, qc7> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ uc7 d;
    @DexIgnore
    public /* final */ rc7 e;
    @DexIgnore
    public /* final */ ThreadLocal<ConcurrentLinkedQueue<c>> f;
    @DexIgnore
    public /* final */ ThreadLocal<Boolean> g;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<Class<?>>> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ThreadLocal<ConcurrentLinkedQueue<c>> {
        @DexIgnore
        public a(nc7 nc7) {
        }

        @DexIgnore
        /* renamed from: a */
        public ConcurrentLinkedQueue<c> initialValue() {
            return new ConcurrentLinkedQueue<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ThreadLocal<Boolean> {
        @DexIgnore
        public b(nc7 nc7) {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean initialValue() {
            return Boolean.FALSE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f2503a;
        @DexIgnore
        public /* final */ pc7 b;

        @DexIgnore
        public c(Object obj, pc7 pc7) {
            this.f2503a = obj;
            this.b = pc7;
        }
    }

    @DexIgnore
    public nc7(uc7 uc7) {
        this(uc7, Endpoints.DEFAULT_NAME);
    }

    @DexIgnore
    public nc7(uc7 uc7, String str) {
        this(uc7, str, rc7.f3099a);
    }

    @DexIgnore
    public nc7(uc7 uc7, String str, rc7 rc7) {
        this.f2502a = new ConcurrentHashMap();
        this.b = new ConcurrentHashMap();
        this.f = new a(this);
        this.g = new b(this);
        this.h = new ConcurrentHashMap();
        this.d = uc7;
        this.c = str;
        this.e = rc7;
    }

    @DexIgnore
    public static void k(String str, InvocationTargetException invocationTargetException) {
        Throwable cause = invocationTargetException.getCause();
        if (cause != null) {
            throw new RuntimeException(str + ": " + cause.getMessage(), cause);
        }
        throw new RuntimeException(str + ": " + invocationTargetException.getMessage(), invocationTargetException);
    }

    @DexIgnore
    public void a(Object obj, pc7 pc7) {
        try {
            pc7.a(obj);
        } catch (InvocationTargetException e2) {
            k("Could not dispatch event: " + obj.getClass() + " to handler " + pc7, e2);
            throw null;
        }
    }

    @DexIgnore
    public final void b(pc7 pc7, qc7 qc7) {
        try {
            Object c2 = qc7.c();
            if (c2 != null) {
                a(c2, pc7);
            }
        } catch (InvocationTargetException e2) {
            k("Producer " + qc7 + " threw an exception.", e2);
            throw null;
        }
    }

    @DexIgnore
    public void c() {
        if (!this.g.get().booleanValue()) {
            this.g.set(Boolean.TRUE);
            while (true) {
                try {
                    c poll = this.f.get().poll();
                    if (poll != null) {
                        if (poll.b.c()) {
                            a(poll.f2503a, poll.b);
                        }
                    } else {
                        return;
                    }
                } finally {
                    this.g.set(Boolean.FALSE);
                }
            }
        }
    }

    @DexIgnore
    public void d(Object obj, pc7 pc7) {
        this.f.get().offer(new c(obj, pc7));
    }

    @DexIgnore
    public Set<Class<?>> e(Class<?> cls) {
        Set<Class<?>> set = this.h.get(cls);
        if (set != null) {
            return set;
        }
        Set<Class<?>> f2 = f(cls);
        Set<Class<?>> putIfAbsent = this.h.putIfAbsent(cls, f2);
        return putIfAbsent == null ? f2 : putIfAbsent;
    }

    @DexIgnore
    public final Set<Class<?>> f(Class<?> cls) {
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        linkedList.add(cls);
        while (!linkedList.isEmpty()) {
            Class cls2 = (Class) linkedList.remove(0);
            hashSet.add(cls2);
            Class superclass = cls2.getSuperclass();
            if (superclass != null) {
                linkedList.add(superclass);
            }
        }
        return hashSet;
    }

    @DexIgnore
    public Set<pc7> g(Class<?> cls) {
        return this.f2502a.get(cls);
    }

    @DexIgnore
    public qc7 h(Class<?> cls) {
        return this.b.get(cls);
    }

    @DexIgnore
    public void i(Object obj) {
        if (obj != null) {
            this.d.a(this);
            boolean z = false;
            for (Class<?> cls : e(obj.getClass())) {
                Set<pc7> g2 = g(cls);
                if (g2 != null && !g2.isEmpty()) {
                    z = true;
                    for (pc7 pc7 : g2) {
                        d(obj, pc7);
                    }
                }
            }
            if (!z && !(obj instanceof oc7)) {
                i(new oc7(this, obj));
            }
            c();
            return;
        }
        throw new NullPointerException("Event to post must not be null.");
    }

    @DexIgnore
    public void j(Object obj) {
        Set<pc7> set;
        if (obj != null) {
            this.d.a(this);
            Map<Class<?>, qc7> b2 = this.e.b(obj);
            for (Class<?> cls : b2.keySet()) {
                qc7 qc7 = b2.get(cls);
                qc7 putIfAbsent = this.b.putIfAbsent(cls, qc7);
                if (putIfAbsent == null) {
                    Set<pc7> set2 = this.f2502a.get(cls);
                    if (set2 != null && !set2.isEmpty()) {
                        for (pc7 pc7 : set2) {
                            b(pc7, qc7);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Producer method for type " + cls + " found on type " + qc7.f2961a.getClass() + ", but already registered by type " + putIfAbsent.f2961a.getClass() + CodelessMatcher.CURRENT_CLASS_NAME);
                }
            }
            Map<Class<?>, Set<pc7>> a2 = this.e.a(obj);
            for (Class<?> cls2 : a2.keySet()) {
                Set<pc7> set3 = this.f2502a.get(cls2);
                if (set3 == null) {
                    set = new CopyOnWriteArraySet<>();
                    Set<pc7> putIfAbsent2 = this.f2502a.putIfAbsent(cls2, set);
                    if (putIfAbsent2 != null) {
                        set = putIfAbsent2;
                    }
                } else {
                    set = set3;
                }
                if (!set.addAll(a2.get(cls2))) {
                    throw new IllegalArgumentException("Object already registered.");
                }
            }
            for (Map.Entry<Class<?>, Set<pc7>> entry : a2.entrySet()) {
                qc7 qc72 = this.b.get(entry.getKey());
                if (qc72 != null && qc72.b()) {
                    for (pc7 pc72 : entry.getValue()) {
                        if (!qc72.b()) {
                            break;
                        } else if (pc72.c()) {
                            b(pc72, qc72);
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("Object to register must not be null.");
    }

    @DexIgnore
    public void l(Object obj) {
        if (obj != null) {
            this.d.a(this);
            for (Map.Entry<Class<?>, qc7> entry : this.e.b(obj).entrySet()) {
                Class<?> key = entry.getKey();
                qc7 h2 = h(key);
                qc7 value = entry.getValue();
                if (value == null || !value.equals(h2)) {
                    throw new IllegalArgumentException("Missing event producer for an annotated method. Is " + obj.getClass() + " registered?");
                }
                this.b.remove(key).a();
            }
            for (Map.Entry<Class<?>, Set<pc7>> entry2 : this.e.a(obj).entrySet()) {
                Set<pc7> g2 = g(entry2.getKey());
                Set<pc7> value2 = entry2.getValue();
                if (g2 == null || !g2.containsAll(value2)) {
                    throw new IllegalArgumentException("Missing event handler for an annotated method. Is " + obj.getClass() + " registered?");
                }
                for (pc7 pc7 : g2) {
                    if (value2.contains(pc7)) {
                        pc7.b();
                    }
                }
                g2.removeAll(value2);
            }
            return;
        }
        throw new NullPointerException("Object to unregister must not be null.");
    }

    @DexIgnore
    public String toString() {
        return "[Bus \"" + this.c + "\"]";
    }
}
