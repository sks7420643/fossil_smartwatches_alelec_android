package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de2 implements Parcelable.Creator<ee2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ee2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        b62[] b62Arr = null;
        Bundle bundle = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                bundle = ad2.a(parcel, t);
            } else if (l == 2) {
                b62Arr = (b62[]) ad2.i(parcel, t, b62.CREATOR);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                i = ad2.v(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ee2(bundle, b62Arr, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ee2[] newArray(int i) {
        return new ee2[i];
    }
}
