package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.oi5;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pi6 extends pv5 implements oi6 {
    @DexIgnore
    public g37<d75> g;
    @DexIgnore
    public ni6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        public final void onClick(View view) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.C;
            Date date = new Date();
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HeartRateOverviewDayFragment";
    }

    @DexIgnore
    public final void K6(bf5 bf5, WorkoutSession workoutSession) {
        int i2 = 0;
        if (bf5 != null) {
            View n = bf5.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            oi5.a aVar = oi5.Companion;
            cl7<Integer, Integer> a2 = aVar.a(aVar.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = um5.c(context, a2.getSecond().intValue());
            bf5.w.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = bf5.v;
            pq7.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            FlexibleTextView flexibleTextView2 = bf5.t;
            pq7.b(flexibleTextView2, "it.ftvRestingValue");
            WorkoutHeartRate heartRate = workoutSession.getHeartRate();
            flexibleTextView2.setText(String.valueOf(heartRate != null ? lr7.b(heartRate.getAverage()) : 0));
            FlexibleTextView flexibleTextView3 = bf5.r;
            pq7.b(flexibleTextView3, "it.ftvMaxValue");
            WorkoutHeartRate heartRate2 = workoutSession.getHeartRate();
            if (heartRate2 != null) {
                i2 = heartRate2.getMax();
            }
            flexibleTextView3.setText(String.valueOf(i2));
            FlexibleTextView flexibleTextView4 = bf5.u;
            pq7.b(flexibleTextView4, "it.ftvWorkoutTime");
            flexibleTextView4.setText(lk5.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            String d = qn5.l.a().d("dianaHeartRateTab");
            if (d != null) {
                bf5.w.setColorFilter(Color.parseColor(d));
            }
            String d2 = qn5.l.a().d("nonBrandLineColor");
            if (d2 != null) {
                bf5.x.setBackgroundColor(Color.parseColor(d2));
            }
        }
    }

    @DexIgnore
    public final void L6() {
        d75 a2;
        TodayHeartRateChart todayHeartRateChart;
        g37<d75> g37 = this.g;
        if (g37 == null) {
            pq7.n("mBinding");
            throw null;
        } else if (g37 != null && (a2 = g37.a()) != null && (todayHeartRateChart = a2.w) != null) {
            todayHeartRateChart.o("maxHeartRate", "lowestHeartRate");
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(ni6 ni6) {
        pq7.c(ni6, "presenter");
        this.h = ni6;
    }

    @DexIgnore
    @Override // com.fossil.oi6
    public void b0(int i2, List<w57> list, List<gl7<Integer, cl7<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        pq7.c(list, "listTodayHeartRateModel");
        pq7.c(list2, "listTimeZoneChange");
        g37<d75> g37 = this.g;
        if (g37 != null) {
            d75 a2 = g37.a();
            if (a2 != null && (todayHeartRateChart = a2.w) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i2);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.m(list);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewDayFragment", "onCreateView");
        d75 d75 = (d75) aq0.f(layoutInflater, 2131558565, viewGroup, false, A6());
        d75.u.setOnClickListener(a.b);
        this.g = new g37<>(this, d75);
        L6();
        g37<d75> g37 = this.g;
        if (g37 != null) {
            d75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ni6 ni6 = this.h;
        if (ni6 != null) {
            ni6.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        L6();
        ni6 ni6 = this.h;
        if (ni6 != null) {
            ni6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oi6
    public void v(boolean z, List<WorkoutSession> list) {
        View n;
        View n2;
        pq7.c(list, "workoutSessions");
        g37<d75> g37 = this.g;
        if (g37 != null) {
            d75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.v;
                pq7.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.r, list.get(0));
                if (size == 1) {
                    bf5 bf5 = a2.s;
                    if (bf5 != null && (n2 = bf5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                bf5 bf52 = a2.s;
                if (!(bf52 == null || (n = bf52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.s, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.v;
            pq7.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
