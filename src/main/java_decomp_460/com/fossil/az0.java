package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class az0 implements bz0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ViewGroupOverlay f360a;

    @DexIgnore
    public az0(ViewGroup viewGroup) {
        this.f360a = viewGroup.getOverlay();
    }

    @DexIgnore
    @Override // com.fossil.gz0
    public void a(Drawable drawable) {
        this.f360a.add(drawable);
    }

    @DexIgnore
    @Override // com.fossil.gz0
    public void b(Drawable drawable) {
        this.f360a.remove(drawable);
    }

    @DexIgnore
    @Override // com.fossil.bz0
    public void c(View view) {
        this.f360a.add(view);
    }

    @DexIgnore
    @Override // com.fossil.bz0
    public void d(View view) {
        this.f360a.remove(view);
    }
}
