package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q67 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ float f2929a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public q67(float f, float f2) {
        this.f2929a = f;
        this.b = f2;
    }

    @DexIgnore
    public final float a(q67 q67) {
        pq7.c(q67, "that");
        float f = this.f2929a - q67.f2929a;
        float f2 = this.b - q67.b;
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof q67) {
                q67 q67 = (q67) obj;
                if (!(Float.compare(this.f2929a, q67.f2929a) == 0 && Float.compare(this.b, q67.b) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (Float.floatToIntBits(this.f2929a) * 31) + Float.floatToIntBits(this.b);
    }

    @DexIgnore
    public String toString() {
        return "Point(x=" + this.f2929a + ", y=" + this.b + ")";
    }
}
