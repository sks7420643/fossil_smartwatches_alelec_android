package com.fossil;

import android.os.Looper;
import com.fossil.yb2;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j82 implements yb2.c {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ WeakReference<h82> f1724a;
    @DexIgnore
    public /* final */ m62<?> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public j82(h82 h82, m62<?> m62, boolean z) {
        this.f1724a = new WeakReference<>(h82);
        this.b = m62;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.yb2.c
    public final void a(z52 z52) {
        boolean z = false;
        h82 h82 = this.f1724a.get();
        if (h82 != null) {
            if (Looper.myLooper() == h82.f1448a.t.m()) {
                z = true;
            }
            rc2.o(z, "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            h82.b.lock();
            try {
                if (h82.z(0)) {
                    if (!z52.A()) {
                        h82.v(z52, this.b, this.c);
                    }
                    if (h82.o()) {
                        h82.p();
                    }
                    h82.b.unlock();
                }
            } finally {
                h82.b.unlock();
            }
        }
    }
}
