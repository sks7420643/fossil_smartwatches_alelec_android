package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vg5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f3766a;
    @DexIgnore
    public /* final */ FlexibleButton b;
    @DexIgnore
    public /* final */ FlexibleTextView c;

    @DexIgnore
    public vg5(ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView) {
        this.f3766a = constraintLayout;
        this.b = flexibleButton;
        this.c = flexibleTextView;
    }

    @DexIgnore
    public static vg5 a(View view) {
        int i;
        FlexibleButton flexibleButton = (FlexibleButton) view.findViewById(2131361973);
        if (flexibleButton != null) {
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362426);
            if (flexibleTextView != null) {
                return new vg5((ConstraintLayout) view, flexibleButton, flexibleTextView);
            }
            i = 2131362426;
        } else {
            i = 2131361973;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static vg5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558858, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f3766a;
    }
}
