package com.fossil;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ui1 {
    @DexIgnore
    public static /* final */ gd1<?, ?, ?> c; // = new gd1<>(Object.class, Object.class, Object.class, Collections.singletonList(new vc1(Object.class, Object.class, Object.class, Collections.emptyList(), new vh1(), null)), null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zi0<hk1, gd1<?, ?, ?>> f3595a; // = new zi0<>();
    @DexIgnore
    public /* final */ AtomicReference<hk1> b; // = new AtomicReference<>();

    @DexIgnore
    public <Data, TResource, Transcode> gd1<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        gd1<Data, TResource, Transcode> gd1;
        hk1 b2 = b(cls, cls2, cls3);
        synchronized (this.f3595a) {
            gd1 = (gd1<Data, TResource, Transcode>) this.f3595a.get(b2);
        }
        this.b.set(b2);
        return gd1;
    }

    @DexIgnore
    public final hk1 b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        hk1 andSet = this.b.getAndSet(null);
        if (andSet == null) {
            andSet = new hk1();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    public boolean c(gd1<?, ?, ?> gd1) {
        return c.equals(gd1);
    }

    @DexIgnore
    public void d(Class<?> cls, Class<?> cls2, Class<?> cls3, gd1<?, ?, ?> gd1) {
        synchronized (this.f3595a) {
            zi0<hk1, gd1<?, ?, ?>> zi0 = this.f3595a;
            hk1 hk1 = new hk1(cls, cls2, cls3);
            if (gd1 == null) {
                gd1 = c;
            }
            zi0.put(hk1, gd1);
        }
    }
}
