package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.jc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tc2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<tc2> CREATOR; // = new be2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public IBinder c;
    @DexIgnore
    public z52 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public tc2(int i, IBinder iBinder, z52 z52, boolean z, boolean z2) {
        this.b = i;
        this.c = iBinder;
        this.d = z52;
        this.e = z;
        this.f = z2;
    }

    @DexIgnore
    public jc2 c() {
        return jc2.a.e(this.c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof tc2)) {
            return false;
        }
        tc2 tc2 = (tc2) obj;
        return this.d.equals(tc2.d) && c().equals(tc2.c());
    }

    @DexIgnore
    public z52 f() {
        return this.d;
    }

    @DexIgnore
    public boolean h() {
        return this.e;
    }

    @DexIgnore
    public boolean k() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.m(parcel, 2, this.c, false);
        bd2.t(parcel, 3, f(), i, false);
        bd2.c(parcel, 4, h());
        bd2.c(parcel, 5, k());
        bd2.b(parcel, a2);
    }
}
