package com.fossil;

import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z78 extends w78<Fragment> {
    @DexIgnore
    public z78(Fragment fragment) {
        super(fragment);
    }

    @DexIgnore
    @Override // com.fossil.y78
    public boolean d(String str) {
        return ((Fragment) a()).shouldShowRequestPermissionRationale(str);
    }
}
