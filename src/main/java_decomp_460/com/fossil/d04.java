package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d04 {
    @DexIgnore
    public static zz3 a(int i) {
        return i != 0 ? i != 1 ? b() : new a04() : new f04();
    }

    @DexIgnore
    public static zz3 b() {
        return new f04();
    }

    @DexIgnore
    public static b04 c() {
        return new b04();
    }

    @DexIgnore
    public static void d(View view, float f) {
        Drawable background = view.getBackground();
        if (background instanceof c04) {
            ((c04) background).U(f);
        }
    }

    @DexIgnore
    public static void e(View view) {
        Drawable background = view.getBackground();
        if (background instanceof c04) {
            f(view, (c04) background);
        }
    }

    @DexIgnore
    public static void f(View view, c04 c04) {
        if (c04.O()) {
            c04.Z(kz3.c(view));
        }
    }
}
