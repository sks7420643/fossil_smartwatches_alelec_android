package com.fossil;

import android.text.TextUtils;
import com.fossil.jh4;
import com.fossil.kh4;
import com.fossil.vg4;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tg4 implements ug4 {
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public static /* final */ ThreadFactory m; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j64 f3407a;
    @DexIgnore
    public /* final */ ih4 b;
    @DexIgnore
    public /* final */ eh4 c;
    @DexIgnore
    public /* final */ bh4 d;
    @DexIgnore
    public /* final */ dh4 e;
    @DexIgnore
    public /* final */ zg4 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ ExecutorService h;
    @DexIgnore
    public /* final */ ExecutorService i;
    @DexIgnore
    public String j;
    @DexIgnore
    public /* final */ List<ah4> k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AtomicInteger f3408a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format("firebase-installations-executor-%d", Integer.valueOf(this.f3408a.getAndIncrement())));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3409a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[kh4.b.values().length];
            b = iArr;
            try {
                iArr[kh4.b.OK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[kh4.b.BAD_CONFIG.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[kh4.b.AUTH_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            int[] iArr2 = new int[jh4.b.values().length];
            f3409a = iArr2;
            try {
                iArr2[jh4.b.OK.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f3409a[jh4.b.BAD_CONFIG.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
        }
        */
    }

    @DexIgnore
    public tg4(j64 j64, ti4 ti4, je4 je4) {
        this(new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), m), j64, new ih4(j64.g(), ti4, je4), new eh4(j64), new bh4(), new dh4(j64), new zg4());
    }

    @DexIgnore
    public tg4(ExecutorService executorService, j64 j64, ih4 ih4, eh4 eh4, bh4 bh4, dh4 dh4, zg4 zg4) {
        this.g = new Object();
        this.j = null;
        this.k = new ArrayList();
        this.f3407a = j64;
        this.b = ih4;
        this.c = eh4;
        this.d = bh4;
        this.e = dh4;
        this.f = zg4;
        this.h = executorService;
        this.i = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), m);
    }

    @DexIgnore
    public static tg4 k() {
        return l(j64.h());
    }

    @DexIgnore
    public static tg4 l(j64 j64) {
        rc2.b(j64 != null, "Null is not a valid value of FirebaseApp.");
        return (tg4) j64.f(ug4.class);
    }

    @DexIgnore
    @Override // com.fossil.ug4
    public nt3<yg4> a(boolean z) {
        t();
        nt3<yg4> c2 = c();
        this.h.execute(pg4.a(this, z));
        return c2;
    }

    @DexIgnore
    public final nt3<yg4> c() {
        ot3 ot3 = new ot3();
        xg4 xg4 = new xg4(this.d, ot3);
        synchronized (this.g) {
            this.k.add(xg4);
        }
        return ot3.a();
    }

    @DexIgnore
    public final Void d() throws vg4, IOException {
        this.j = null;
        fh4 m2 = m();
        if (m2.k()) {
            try {
                this.b.e(i(), m2.d(), o(), m2.f());
            } catch (k64 e2) {
                throw new vg4("Failed to delete a Firebase Installation.", vg4.a.BAD_CONFIG);
            }
        }
        p(m2.r());
        return null;
    }

    @DexIgnore
    @Override // com.fossil.ug4
    public nt3<Void> delete() {
        return qt3.c(this.h, qg4.a(this));
    }

    @DexIgnore
    public final void e(boolean z) {
        fh4 n = n();
        if (z) {
            n = n.p();
        }
        x(n);
        this.i.execute(sg4.a(this, z));
    }

    @DexIgnore
    public final String f() {
        String str = this.j;
        if (str != null) {
            return str;
        }
        fh4 n = n();
        this.i.execute(rg4.a(this));
        return n.d();
    }

    @DexIgnore
    public final void g(boolean z) {
        fh4 fh4;
        fh4 m2 = m();
        try {
            if (m2.i() || m2.l()) {
                fh4 = v(m2);
            } else if (z || this.d.b(m2)) {
                fh4 = h(m2);
            } else {
                return;
            }
            p(fh4);
            if (fh4.k()) {
                this.j = fh4.d();
            }
            if (fh4.i()) {
                w(fh4, new vg4(vg4.a.BAD_CONFIG));
            } else if (fh4.j()) {
                w(fh4, new IOException("cleared fid due to auth error"));
            } else {
                x(fh4);
            }
        } catch (IOException e2) {
            w(m2, e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ug4
    public nt3<String> getId() {
        t();
        ot3 ot3 = new ot3();
        ot3.e(f());
        return ot3.a();
    }

    @DexIgnore
    public final fh4 h(fh4 fh4) throws IOException {
        kh4 f2 = this.b.f(i(), fh4.d(), o(), fh4.f());
        int i2 = b.b[f2.b().ordinal()];
        if (i2 == 1) {
            return fh4.o(f2.c(), f2.d(), this.d.a());
        }
        if (i2 == 2) {
            return fh4.q("BAD CONFIG");
        }
        if (i2 == 3) {
            this.j = null;
            return fh4.r();
        }
        throw new IOException();
    }

    @DexIgnore
    public String i() {
        return this.f3407a.j().b();
    }

    @DexIgnore
    public String j() {
        return this.f3407a.j().c();
    }

    @DexIgnore
    public final fh4 m() {
        fh4 c2;
        synchronized (l) {
            og4 a2 = og4.a(this.f3407a.g(), "generatefid.lock");
            try {
                c2 = this.c.c();
            } finally {
                if (a2 != null) {
                    a2.b();
                }
            }
        }
        return c2;
    }

    @DexIgnore
    public final fh4 n() {
        fh4 c2;
        synchronized (l) {
            og4 a2 = og4.a(this.f3407a.g(), "generatefid.lock");
            try {
                c2 = this.c.c();
                if (c2.j()) {
                    String u = u(c2);
                    eh4 eh4 = this.c;
                    c2 = c2.t(u);
                    eh4.a(c2);
                }
            } finally {
                if (a2 != null) {
                    a2.b();
                }
            }
        }
        return c2;
    }

    @DexIgnore
    public String o() {
        return this.f3407a.j().e();
    }

    @DexIgnore
    public final void p(fh4 fh4) {
        synchronized (l) {
            og4 a2 = og4.a(this.f3407a.g(), "generatefid.lock");
            try {
                this.c.a(fh4);
            } finally {
                if (a2 != null) {
                    a2.b();
                }
            }
        }
    }

    @DexIgnore
    public final void t() {
        rc2.g(j());
        rc2.g(o());
        rc2.g(i());
        rc2.b(bh4.d(j()), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        rc2.b(bh4.c(i()), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }

    @DexIgnore
    public final String u(fh4 fh4) {
        if ((!this.f3407a.i().equals("CHIME_ANDROID_SDK") && !this.f3407a.q()) || !fh4.m()) {
            return this.f.a();
        }
        String f2 = this.e.f();
        return TextUtils.isEmpty(f2) ? this.f.a() : f2;
    }

    @DexIgnore
    public final fh4 v(fh4 fh4) throws IOException {
        jh4 d2 = this.b.d(i(), fh4.d(), o(), j(), fh4.d().length() == 11 ? this.e.i() : null);
        int i2 = b.f3409a[d2.e().ordinal()];
        if (i2 == 1) {
            return fh4.s(d2.c(), d2.d(), this.d.a(), d2.b().c(), d2.b().d());
        }
        if (i2 == 2) {
            return fh4.q("BAD CONFIG");
        }
        throw new IOException();
    }

    @DexIgnore
    public final void w(fh4 fh4, Exception exc) {
        synchronized (this.g) {
            Iterator<ah4> it = this.k.iterator();
            while (it.hasNext()) {
                if (it.next().a(fh4, exc)) {
                    it.remove();
                }
            }
        }
    }

    @DexIgnore
    public final void x(fh4 fh4) {
        synchronized (this.g) {
            Iterator<ah4> it = this.k.iterator();
            while (it.hasNext()) {
                if (it.next().b(fh4)) {
                    it.remove();
                }
            }
        }
    }
}
