package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xi1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<a<?>> f4129a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<T> f4130a;
        @DexIgnore
        public /* final */ rb1<T> b;

        @DexIgnore
        public a(Class<T> cls, rb1<T> rb1) {
            this.f4130a = cls;
            this.b = rb1;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.f4130a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public <Z> void a(Class<Z> cls, rb1<Z> rb1) {
        synchronized (this) {
            this.f4129a.add(new a<>(cls, rb1));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v6. Raw type applied. Possible types: com.fossil.rb1<T>, com.fossil.rb1<Z> */
    public <Z> rb1<Z> b(Class<Z> cls) {
        synchronized (this) {
            int size = this.f4129a.size();
            for (int i = 0; i < size; i++) {
                a<?> aVar = this.f4129a.get(i);
                if (aVar.a(cls)) {
                    return (rb1<T>) aVar.b;
                }
            }
            return null;
        }
    }
}
