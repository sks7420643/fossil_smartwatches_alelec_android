package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cj5 extends nc7 {
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public a(Object obj) {
            this.b = obj;
        }

        @DexIgnore
        public void run() {
            cj5.super.i(this.b);
        }
    }

    @DexIgnore
    public cj5(uc7 uc7) {
        super(uc7);
    }

    @DexIgnore
    @Override // com.fossil.nc7
    public void i(Object obj) {
        if (Objects.equals(Looper.myLooper(), Looper.getMainLooper())) {
            super.i(obj);
        } else {
            this.i.post(new a(obj));
        }
    }
}
