package com.fossil;

import android.content.Context;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ji4 implements Callable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1764a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public /* final */ FirebaseInstanceId c;
    @DexIgnore
    public /* final */ rf4 d;
    @DexIgnore
    public /* final */ ef4 e;

    @DexIgnore
    public ji4(Context context, ScheduledExecutorService scheduledExecutorService, FirebaseInstanceId firebaseInstanceId, rf4 rf4, ef4 ef4) {
        this.f1764a = context;
        this.b = scheduledExecutorService;
        this.c = firebaseInstanceId;
        this.d = rf4;
        this.e = ef4;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return ki4.i(this.f1764a, this.b, this.c, this.d, this.e);
    }
}
