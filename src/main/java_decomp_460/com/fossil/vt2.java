package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ sn3 f;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vt2(zs2 zs2, sn3 sn3) {
        super(zs2);
        this.g = zs2;
        this.f = sn3;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        for (int i = 0; i < this.g.e.size(); i++) {
            if (this.f.equals(((Pair) this.g.e.get(i)).first)) {
                Log.w(this.g.f4521a, "OnEventListener already registered.");
                return;
            }
        }
        zs2.c cVar = new zs2.c(this.f);
        this.g.e.add(new Pair(this.f, cVar));
        this.g.h.registerOnMeasurementEventListener(cVar);
    }
}
