package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hv extends dv {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ hv(short s, k5 k5Var, int i, int i2) {
        super(iu.g, s, hs.o, k5Var, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(g80.k(super.A(), jd0.I0, Long.valueOf(this.M)), jd0.J0, Long.valueOf(this.N));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = hy1.o(order.getInt(0));
            this.N = hy1.o(order.getInt(4));
            g80.k(g80.k(jSONObject, jd0.I0, Long.valueOf(this.M)), jd0.J0, Long.valueOf(this.N));
        }
        return jSONObject;
    }
}
