package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum gg7 {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6),
    ONLY_WIFI_NO_CACHE(7);
    

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1304a;

    @DexIgnore
    public gg7(int i) {
        this.f1304a = i;
    }

    @DexIgnore
    public static gg7 getStatReportStrategy(int i) {
        gg7[] values = values();
        for (gg7 gg7 : values) {
            if (i == gg7.a()) {
                return gg7;
            }
        }
        return null;
    }

    @DexIgnore
    public final int a() {
        return this.f1304a;
    }
}
