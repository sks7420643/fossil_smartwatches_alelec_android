package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3077a;

    /*
    static {
        int[] iArr = new int[rh5.values().length];
        f3077a = iArr;
        iArr[rh5.TOTAL_STEPS.ordinal()] = 1;
        f3077a[rh5.CALORIES.ordinal()] = 2;
        f3077a[rh5.ACTIVE_TIME.ordinal()] = 3;
        f3077a[rh5.TOTAL_SLEEP.ordinal()] = 4;
        f3077a[rh5.GOAL_TRACKING.ordinal()] = 5;
    }
    */
}
