package com.fossil;

import com.facebook.internal.Utility;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ea1 extends w91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f900a;
    @DexIgnore
    public /* final */ SSLSocketFactory b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends FilterInputStream {
        @DexIgnore
        public /* final */ HttpURLConnection b;

        @DexIgnore
        public a(HttpURLConnection httpURLConnection) {
            super(ea1.i(httpURLConnection));
            this.b = httpURLConnection;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() throws IOException {
            super.close();
            this.b.disconnect();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        String a(String str);
    }

    @DexIgnore
    public ea1() {
        this(null);
    }

    @DexIgnore
    public ea1(b bVar) {
        this(bVar, null);
    }

    @DexIgnore
    public ea1(b bVar, SSLSocketFactory sSLSocketFactory) {
        this.f900a = bVar;
        this.b = sSLSocketFactory;
    }

    @DexIgnore
    public static void d(HttpURLConnection httpURLConnection, m91<?> m91, byte[] bArr) throws IOException {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey("Content-Type")) {
            httpURLConnection.setRequestProperty("Content-Type", m91.getBodyContentType());
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(bArr);
        dataOutputStream.close();
    }

    @DexIgnore
    public static void e(HttpURLConnection httpURLConnection, m91<?> m91) throws IOException, z81 {
        byte[] body = m91.getBody();
        if (body != null) {
            d(httpURLConnection, m91, body);
        }
    }

    @DexIgnore
    public static List<f91> f(Map<String, List<String>> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            if (entry.getKey() != null) {
                for (String str : entry.getValue()) {
                    arrayList.add(new f91(entry.getKey(), str));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static boolean h(int i, int i2) {
        return (i == 4 || (100 <= i2 && i2 < 200) || i2 == 204 || i2 == 304) ? false : true;
    }

    @DexIgnore
    public static InputStream i(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException e) {
            return httpURLConnection.getErrorStream();
        }
    }

    @DexIgnore
    public static void k(HttpURLConnection httpURLConnection, m91<?> m91) throws IOException, z81 {
        switch (m91.getMethod()) {
            case -1:
                byte[] postBody = m91.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setRequestMethod("POST");
                    d(httpURLConnection, m91, postBody);
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                e(httpURLConnection, m91);
                return;
            case 2:
                httpURLConnection.setRequestMethod("PUT");
                e(httpURLConnection, m91);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                return;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                return;
            case 7:
                httpURLConnection.setRequestMethod("PATCH");
                e(httpURLConnection, m91);
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }

    @DexIgnore
    @Override // com.fossil.w91
    public ca1 b(m91<?> m91, Map<String, String> map) throws IOException, z81 {
        String str;
        boolean z;
        Throwable th;
        String url = m91.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.putAll(m91.getHeaders());
        b bVar = this.f900a;
        if (bVar != null) {
            str = bVar.a(url);
            if (str == null) {
                throw new IOException("URL blocked by rewriter: " + url);
            }
        } else {
            str = url;
        }
        HttpURLConnection j = j(new URL(str), m91);
        try {
            for (String str2 : hashMap.keySet()) {
                j.setRequestProperty(str2, (String) hashMap.get(str2));
            }
            k(j, m91);
            int responseCode = j.getResponseCode();
            if (responseCode == -1) {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            } else if (!h(m91.getMethod(), responseCode)) {
                ca1 ca1 = new ca1(responseCode, f(j.getHeaderFields()));
                j.disconnect();
                return ca1;
            } else {
                z = true;
                try {
                    return new ca1(responseCode, f(j.getHeaderFields()), j.getContentLength(), new a(j));
                } catch (Throwable th2) {
                    th = th2;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            z = false;
            if (!z) {
                j.disconnect();
            }
            throw th;
        }
    }

    @DexIgnore
    public HttpURLConnection g(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }

    @DexIgnore
    public final HttpURLConnection j(URL url, m91<?> m91) throws IOException {
        SSLSocketFactory sSLSocketFactory;
        HttpURLConnection g = g(url);
        int timeoutMs = m91.getTimeoutMs();
        g.setConnectTimeout(timeoutMs);
        g.setReadTimeout(timeoutMs);
        g.setUseCaches(false);
        g.setDoInput(true);
        if (Utility.URL_SCHEME.equals(url.getProtocol()) && (sSLSocketFactory = this.b) != null) {
            ((HttpsURLConnection) g).setSSLSocketFactory(sSLSocketFactory);
        }
        return g;
    }
}
