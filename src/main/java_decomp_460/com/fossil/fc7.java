package com.fossil;

import android.os.Bundle;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ fc7 f1105a; // = new fc7();

    @DexIgnore
    public static /* synthetic */ Fragment b(fc7 fc7, Class cls, String str, int i, String str2, boolean z, boolean z2, int i2, Object obj) {
        boolean z3 = false;
        int i3 = (i2 & 4) != 0 ? 2 : i;
        String str3 = (i2 & 8) != 0 ? null : str2;
        boolean z4 = (i2 & 16) != 0 ? false : z;
        if ((i2 & 32) == 0) {
            z3 = z2;
        }
        return fc7.a(cls, str, i3, str3, z4, z3);
    }

    @DexIgnore
    public final <T extends Fragment> T a(Class<T> cls, String str, int i, String str2, boolean z, boolean z2) {
        pq7.c(cls, "cl");
        T newInstance = cls.getConstructor(new Class[0]).newInstance(new Object[0]);
        T t = newInstance;
        Bundle bundle = new Bundle();
        bundle.putString("preset_id", str);
        bundle.putString("order_id", str2);
        bundle.putBoolean("from_faces", z);
        bundle.putBoolean("sharing_flow_extra", z2);
        bundle.putInt("TAB_EXTRA", i);
        t.setArguments(bundle);
        pq7.b(newInstance, "cl.getConstructor().newI\u2026uments = bundle\n        }");
        return t;
    }
}
