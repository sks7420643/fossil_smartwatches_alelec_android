package com.fossil;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ys4 f4173a;

    @DexIgnore
    public xt4(ys4 ys4) {
        pq7.c(ys4, "dao");
        this.f4173a = ys4;
    }

    @DexIgnore
    public final void a() {
        this.f4173a.a();
    }

    @DexIgnore
    public final int b() {
        return this.f4173a.e();
    }

    @DexIgnore
    public final int c(String str) {
        pq7.c(str, "profileId");
        return this.f4173a.b(str);
    }

    @DexIgnore
    public final void d() {
        this.f4173a.j();
    }

    @DexIgnore
    public final void e() {
        this.f4173a.g();
    }

    @DexIgnore
    public final void f() {
        this.f4173a.h();
    }

    @DexIgnore
    public final int g(String[] strArr) {
        pq7.c(strArr, "ids");
        return this.f4173a.m(strArr);
    }

    @DexIgnore
    public final xs4 h(String str) {
        pq7.c(str, "id");
        return this.f4173a.p(str);
    }

    @DexIgnore
    public final List<xs4> i() {
        return this.f4173a.n();
    }

    @DexIgnore
    public final List<xs4> j() {
        return this.f4173a.k();
    }

    @DexIgnore
    public final List<xs4> k() {
        return this.f4173a.l();
    }

    @DexIgnore
    public final LiveData<List<xs4>> l() {
        return this.f4173a.c();
    }

    @DexIgnore
    public final List<xs4> m() {
        return this.f4173a.i();
    }

    @DexIgnore
    public final List<xs4> n() {
        return this.f4173a.f();
    }

    @DexIgnore
    public final List<xs4> o() {
        return this.f4173a.d();
    }

    @DexIgnore
    public final long p(xs4 xs4) {
        pq7.c(xs4, "friend");
        return this.f4173a.o(xs4);
    }

    @DexIgnore
    public final Long[] q(List<xs4> list) {
        pq7.c(list, NativeProtocol.AUDIENCE_FRIENDS);
        return this.f4173a.insert(list);
    }
}
