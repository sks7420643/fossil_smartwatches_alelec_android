package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e6 extends m5 {
    @DexIgnore
    public byte[] m; // = new byte[0];

    @DexIgnore
    public e6(n6 n6Var, n4 n4Var) {
        super(v5.f, n6Var, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.p(this.l);
        this.k = true;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return (h7Var instanceof i7) && ((i7) h7Var).b == this.l;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.d;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.m = ((i7) h7Var).c;
    }
}
