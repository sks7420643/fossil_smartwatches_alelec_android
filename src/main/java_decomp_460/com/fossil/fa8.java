package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f1093a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ MethodChannel c; // = new MethodChannel(this.d.messenger(), "top.kikt/photo_manager/notify");
    @DexIgnore
    public /* final */ PluginRegistry.Registrar d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fa8 f1094a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fa8 fa8, Handler handler) {
            super(handler);
            pq7.c(handler, "handler");
            this.f1094a = fa8;
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            super.onChange(z, uri);
            this.f1094a.b(z, uri);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends ContentObserver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fa8 f1095a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fa8 fa8, Handler handler) {
            super(handler);
            pq7.c(handler, "handler");
            this.f1095a = fa8;
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            super.onChange(z, uri);
            this.f1095a.b(z, uri);
        }
    }

    @DexIgnore
    public fa8(PluginRegistry.Registrar registrar, Handler handler) {
        pq7.c(registrar, "registry");
        pq7.c(handler, "handler");
        this.d = registrar;
        this.f1093a = new b(this, handler);
        this.b = new a(this, handler);
    }

    @DexIgnore
    public final Context a() {
        Context context = this.d.context();
        pq7.b(context, "registry.context()");
        return context.getApplicationContext();
    }

    @DexIgnore
    public final void b(boolean z, Uri uri) {
        this.c.invokeMethod("change", zm7.j(hl7.a("android-self", Boolean.valueOf(z)), hl7.a("android-uri", String.valueOf(uri))));
    }

    @DexIgnore
    public final void c(boolean z) {
        this.c.invokeMethod("setAndroidQExperimental", ym7.c(hl7.a("open", Boolean.valueOf(z))));
    }

    @DexIgnore
    public final void d() {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Uri uri2 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        Context a2 = a();
        pq7.b(a2, "context");
        a2.getContentResolver().registerContentObserver(uri, false, this.b);
        Context a3 = a();
        pq7.b(a3, "context");
        a3.getContentResolver().registerContentObserver(uri2, false, this.f1093a);
    }

    @DexIgnore
    public final void e() {
        Context a2 = a();
        pq7.b(a2, "context");
        a2.getContentResolver().unregisterContentObserver(this.b);
        Context a3 = a();
        pq7.b(a3, "context");
        a3.getContentResolver().unregisterContentObserver(this.f1093a);
    }
}
