package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l13 extends IOException {
    @DexIgnore
    public m23 zza; // = null;

    @DexIgnore
    public l13(String str) {
        super(str);
    }

    @DexIgnore
    public static l13 zza() {
        return new l13("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    @DexIgnore
    public static l13 zzb() {
        return new l13("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static l13 zzc() {
        return new l13("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static l13 zzd() {
        return new l13("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static l13 zze() {
        return new l13("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static o13 zzf() {
        return new o13("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static l13 zzg() {
        return new l13("Failed to parse the message.");
    }

    @DexIgnore
    public static l13 zzh() {
        return new l13("Protocol message had invalid UTF-8.");
    }
}
