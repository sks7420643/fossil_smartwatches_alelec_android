package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import com.fossil.ar1;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.SyncMode;
import com.fossil.fitness.WorkoutSessionManager;
import com.fossil.ix1;
import com.fossil.tk1;
import com.fossil.yk1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e60 implements yk1, bs1, or1, tr1, vr1, xq1, xr1, ds1, ar1, br1, cr1, er1, gs1, gr1, hr1, jr1, mr1, nr1, od, pd, qd, ir1, hs1, yq1, qr1, rr1, js1, sr1, ps1, ts1, ls1, lr1, dr1, wr1, zr1, as1, is1, cs1, es1, qs1, rs1, ns1, kr1, os1, zq1, pr1, vq1, ur1, wq1, ks1, yr1, ms1, fr1, fs1, ss1 {
    @DexIgnore
    public static /* final */ uz CREATOR; // = new uz(null);
    @DexIgnore
    public /* final */ i60 b; // = new i60(this);
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public /* final */ k5 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ qe g;
    @DexIgnore
    public /* final */ uc h;
    @DexIgnore
    public /* final */ p50 i;
    @DexIgnore
    public /* final */ rp7<c2, tl7> j;
    @DexIgnore
    public /* final */ vp7<byte[], n6, tl7> k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ Object m;
    @DexIgnore
    public ar1.a s;
    @DexIgnore
    public WorkoutSessionManager t;
    @DexIgnore
    public zk1 u;
    @DexIgnore
    public yk1.c v;
    @DexIgnore
    public yk1.b w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public /* final */ BluetoothDevice y;
    @DexIgnore
    public /* final */ String z;

    @DexIgnore
    public /* synthetic */ e60(BluetoothDevice bluetoothDevice, String str, kq7 kq7) {
        this.y = bluetoothDevice;
        this.z = str;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.c = new Handler(myLooper);
            this.d = x4.b.a(this.y);
            this.g = new qe(new nw(new Hashtable(), null));
            this.h = new uc();
            this.i = new p50(this);
            this.j = new yw(this);
            this.k = new gd(this);
            this.l = e.a("UUID.randomUUID().toString()");
            this.m = new Object();
            this.u = new zk1(this.d.C(), this.d.x, this.z, "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
            this.v = yk1.c.DISCONNECTED;
            k5 k5Var = this.d;
            p50 p50 = this.i;
            if (!k5Var.k.contains(p50)) {
                k5Var.k.add(p50);
            }
            zk1 zk1 = this.u;
            c90.e.d(zk1.getMacAddress(), zk1);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ oy1 e0(e60 e60, boolean z2, boolean z3, a9 a9Var, int i2) {
        if ((i2 & 2) != 0) {
            z3 = false;
        }
        if ((i2 & 4) != 0) {
            a9Var = z3 ? a9.LOW : a9.NORMAL;
        }
        return e60.h0(z2, z3, a9Var);
    }

    @DexIgnore
    public static final /* synthetic */ void p0(e60 e60, ky1 ky1, String str, String str2, Object... objArr) {
    }

    @DexIgnore
    public static final /* synthetic */ boolean w0(e60 e60) {
        T t2;
        boolean z2;
        if (e60.d.w == f5.CONNECTED) {
            Iterator<T> it = e60.g.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (t2.y == yp.g) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            if (t2 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static /* synthetic */ oy1 x0(e60 e60, boolean z2, boolean z3, a9 a9Var, int i2) {
        if ((i2 & 2) != 0) {
            z3 = false;
        }
        if ((i2 & 4) != 0) {
            a9Var = z3 ? a9.LOW : a9.NORMAL;
        }
        return e60.y0(z2, z3, a9Var);
    }

    @DexIgnore
    @Override // com.fossil.lr1
    public byte[] A() {
        return ix.b.a(this.d.x).f1550a;
    }

    @DexIgnore
    @Override // com.fossil.yk1
    public void B(yk1.b bVar) {
        this.w = bVar;
    }

    @DexIgnore
    public final boolean B0() {
        boolean z2;
        synchronized (Boolean.valueOf(this.f)) {
            z2 = this.f;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.yq1
    public oy1<tl7> C(tu1[] tu1Arr) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(tu1Arr).toString(2);
        sm smVar = new sm(this.d, this.b, tu1Arr);
        synchronized (this.m) {
            cl1 d0 = d0(smVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                smVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new z50(this, smVar));
                r60 r60 = new r60(this, smVar);
                if (!smVar.t) {
                    smVar.d.add(r60);
                }
                a80 a80 = new a80(oy1, this, smVar);
                if (!smVar.t) {
                    smVar.h.add(a80);
                }
                smVar.a(new wy(oy1, this, smVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                smVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final void C0() {
        ky1 ky1 = ky1.DEBUG;
        this.g.c(zq.INTERRUPTED, new yp[]{yp.g});
        if (!(this.v == yk1.c.DISCONNECTED && this.d.w == f5.DISCONNECTED) && !this.e) {
            km kmVar = new km(this.d, this.b);
            kmVar.a(new o50(this));
            kmVar.F();
        }
    }

    @DexIgnore
    @Override // com.fossil.is1
    public oy1<tl7> D(jo1[] jo1Arr) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(jo1Arr).toString(2);
        ig igVar = new ig(this.d, this.b, jo1Arr, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(igVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                igVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new y70(this, igVar));
                v0 v0Var = new v0(this, igVar);
                if (!igVar.t) {
                    igVar.d.add(v0Var);
                }
                i3 i3Var = new i3(oy1, this, igVar);
                if (!igVar.t) {
                    igVar.h.add(i3Var);
                }
                igVar.a(new r0(oy1, this, igVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                igVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final void D0() {
        this.u = new zk1(this.u.getName(), this.u.getMacAddress(), this.u.getSerialNumber(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
    }

    @DexIgnore
    @Override // com.fossil.rr1
    public oy1<String> E(byte[] bArr, String str) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        int length = bArr.length;
        ko koVar = new ko(this.d, this.b, bArr, str);
        y20 y20 = new y20(this);
        if (!koVar.t) {
            koVar.d.add(y20);
        }
        koVar.v(new p30(this));
        koVar.s(new g40(this));
        synchronized (this.m) {
            cl1 d0 = d0(koVar);
            oy1 = new oy1();
            if (d0 != null) {
                koVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new z0(this, koVar));
                s1 s1Var = new s1(this, koVar);
                if (!koVar.t) {
                    koVar.d.add(s1Var);
                }
                d4 d4Var = new d4(oy1, this, koVar);
                if (!koVar.t) {
                    koVar.h.add(d4Var);
                }
                koVar.a(new i20(oy1, this, koVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                koVar.F();
            }
        }
        return oy1.t(x40.b);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r1v2. Raw type applied. Possible types: com.fossil.vp7<byte[], com.fossil.n6, com.fossil.tl7>, com.fossil.vp7<? super byte[], ? super com.fossil.n6, com.fossil.tl7> */
    public final void E0() {
        kk kkVar = new kk(this.d, this.b);
        kkVar.C = this.j;
        kkVar.D = this.k;
        kkVar.v(s0.b);
        kkVar.s(new l1(this));
        kkVar.F();
    }

    @DexIgnore
    @Override // com.fossil.cr1
    public boolean F() {
        ky1 ky1 = ky1.DEBUG;
        if (!isActive()) {
            zw.i.i(this);
        }
        d90.i.d(new a90(ey1.a(i00.b), v80.e, this.d.x, ey1.a(i00.b), e.a("UUID.randomUUID().toString()"), true, this.l, null, null, null, 896));
        I0(true);
        return true;
    }

    @DexIgnore
    public final qy1<tl7> F0() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        tr trVar = new tr(this.d, this.b, null, 4);
        synchronized (this.m) {
            cl1 d0 = d0(trVar);
            oy1 = new oy1();
            if (d0 != null) {
                trVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new v20(this, trVar));
                id idVar = new id(this, trVar);
                if (!trVar.t) {
                    trVar.d.add(idVar);
                }
                vz vzVar = new vz(oy1, this, trVar);
                if (!trVar.t) {
                    trVar.h.add(vzVar);
                }
                trVar.a(new v50(oy1, this, trVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                trVar.F();
            }
        }
        return oy1.t(n60.b);
    }

    @DexIgnore
    public boolean G0() {
        boolean z2;
        synchronized (Boolean.valueOf(this.x)) {
            z2 = this.x;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.sr1
    public qy1<Integer> H() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        uj ujVar = new uj(this.d, this.b);
        synchronized (this.m) {
            cl1 d0 = d0(ujVar);
            oy1 = new oy1();
            if (d0 != null) {
                ujVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new a1(this, ujVar));
                t1 t1Var = new t1(this, ujVar);
                if (!ujVar.t) {
                    ujVar.d.add(t1Var);
                }
                e4 e4Var = new e4(oy1, this, ujVar);
                if (!ujVar.t) {
                    ujVar.h.add(e4Var);
                }
                ujVar.a(new h40(oy1, this, ujVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                ujVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public yk1.d H0() {
        return yk1.d.g.a(this.d.D());
    }

    @DexIgnore
    @Override // com.fossil.dr1
    public qy1<byte[]> I(byte[] bArr) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        dy1.e(bArr, null, 1, null);
        fq fqVar = new fq(this.d, this.b, bArr);
        fqVar.v(new x60(this));
        synchronized (this.m) {
            cl1 d0 = d0(fqVar);
            oy1 = new oy1();
            if (d0 != null) {
                fqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new ic(this, fqVar));
                jd jdVar = new jd(this, fqVar);
                if (!fqVar.t) {
                    fqVar.d.add(jdVar);
                }
                cx cxVar = new cx(oy1, this, fqVar);
                if (!fqVar.t) {
                    fqVar.h.add(cxVar);
                }
                fqVar.a(new f60(oy1, this, fqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                fqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public void I0(boolean z2) {
        d90.i.d(new a90(ey1.a(i00.g), v80.i, this.d.x, "", "", true, null, null, null, g80.k(new JSONObject(), jd0.z0, Boolean.valueOf(z2)), 448));
        synchronized (Boolean.valueOf(this.x)) {
            this.x = z2;
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    @Override // com.fossil.es1
    public oy1<zm1[]> J(ym1[] ym1Arr) {
        oy1<zm1[]> oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(ym1Arr).toString(2);
        ArrayList arrayList = new ArrayList();
        for (ym1 ym1 : ym1Arr) {
            if (em7.B(this.u.g(), ym1.getKey())) {
                arrayList.add(ym1);
            }
        }
        if (arrayList.isEmpty()) {
            oy1<zm1[]> oy12 = new oy1<>();
            oy12.o(new zm1[0]);
            return oy12;
        }
        k5 k5Var = this.d;
        i60 i60 = this.b;
        Object[] array = arrayList.toArray(new ym1[0]);
        if (array != null) {
            fm fmVar = new fm(k5Var, i60, (ym1[]) array, 0, null, 24);
            synchronized (this.m) {
                cl1 d0 = d0(fmVar);
                oy1 = new oy1<>();
                if (d0 != null) {
                    fmVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                    oy1.n(new bl1(d0, null, 2));
                } else {
                    oy1.q(new u20(this, fmVar));
                    k30 k30 = new k30(this, fmVar);
                    if (!fmVar.t) {
                        fmVar.d.add(k30);
                    }
                    r40 r40 = new r40(oy1, this, fmVar);
                    if (!fmVar.t) {
                        fmVar.h.add(r40);
                    }
                    fmVar.a(new d8(oy1, this, fmVar));
                    if (this.v != yk1.c.CONNECTED && !this.e) {
                        z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                    }
                    fmVar.F();
                }
            }
            return oy1;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.wq1
    public oy1<tl7> K(long j2) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        String str = "setBuddyChallengeMinimumStepThreshold invoked: minimumStepThreshold=" + j2 + '.';
        wi wiVar = new wi(this.d, this.b, j2, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(wiVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                wiVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new ld(this, wiVar));
                fe feVar = new fe(this, wiVar);
                if (!wiVar.t) {
                    wiVar.d.add(feVar);
                }
                zx zxVar = new zx(oy1, this, wiVar);
                if (!wiVar.t) {
                    wiVar.h.add(zxVar);
                }
                wiVar.a(new t50(oy1, this, wiVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                wiVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.tr1
    public qy1<tl7> L() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        k5 k5Var = this.d;
        cq cqVar = new cq(k5Var, this.b, yp.s, new pu(k5Var));
        synchronized (this.m) {
            cl1 d0 = d0(cqVar);
            oy1 = new oy1();
            if (d0 != null) {
                cqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new dy(this, cqVar));
                vy vyVar = new vy(this, cqVar);
                if (!cqVar.t) {
                    cqVar.d.add(vyVar);
                }
                b00 b00 = new b00(oy1, this, cqVar);
                if (!cqVar.t) {
                    cqVar.h.add(b00);
                }
                cqVar.a(new a70(oy1, this, cqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                cqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.yk1
    public zk1 M() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.ss1
    public oy1<tl7> N(lw1 lw1) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        lw1.toJSONString(2);
        oy1<tl7> oy12 = new oy1<>();
        bg bgVar = new bg(this.d, this.b, lw1.j());
        synchronized (this.m) {
            cl1 d0 = d0(bgVar);
            oy1 = new oy1();
            if (d0 != null) {
                bgVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new zz(this, bgVar));
                n00 n00 = new n00(this, bgVar);
                if (!bgVar.t) {
                    bgVar.d.add(n00);
                }
                r10 r10 = new r10(oy1, this, bgVar);
                if (!bgVar.t) {
                    bgVar.h.add(r10);
                }
                bgVar.a(new bc(oy1, this, bgVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                bgVar.F();
            }
        }
        oy1.s(new bd(oy12)).r(new vd(oy12)).w(new tw(oy12));
        return oy12;
    }

    @DexIgnore
    @Override // com.fossil.vr1
    public qy1<tl7> O() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        k5 k5Var = this.d;
        cq cqVar = new cq(k5Var, this.b, yp.r, new qu(k5Var));
        synchronized (this.m) {
            cl1 d0 = d0(cqVar);
            oy1 = new oy1();
            if (d0 != null) {
                cqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new u1(this, cqVar));
                o3 o3Var = new o3(this, cqVar);
                if (!cqVar.t) {
                    cqVar.d.add(o3Var);
                }
                k8 k8Var = new k8(oy1, this, cqVar);
                if (!cqVar.t) {
                    cqVar.h.add(k8Var);
                }
                cqVar.a(new c3(oy1, this, cqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                cqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.yk1
    public yk1.a P() {
        return yk1.a.c.b(this.d.H());
    }

    @DexIgnore
    @Override // com.fossil.hr1
    public oy1<HashMap<zm1, ym1>> R() {
        oy1<HashMap<zm1, ym1>> oy1;
        ky1 ky1 = ky1.DEBUG;
        k5 k5Var = this.d;
        lm lmVar = new lm(k5Var, this.b, ke.b.a(k5Var.x, ob.DEVICE_CONFIG));
        synchronized (this.m) {
            cl1 d0 = d0(lmVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                lmVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new j50(this, lmVar));
                c60 c60 = new c60(this, lmVar);
                if (!lmVar.t) {
                    lmVar.d.add(c60);
                }
                m70 m70 = new m70(oy1, this, lmVar);
                if (!lmVar.t) {
                    lmVar.h.add(m70);
                }
                lmVar.a(new fy(oy1, this, lmVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                lmVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.ss1
    public oy1<tl7> U(kw1 kw1) {
        ky1 ky1 = ky1.DEBUG;
        kw1.d();
        oy1<tl7> oy1 = new oy1<>();
        xw7 unused = gu7.d(id0.i.e(), null, null, new oz(this, kw1, oy1, null), 3, null);
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.mr1
    public qy1<lp1> V() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        zk zkVar = new zk(this.d, this.b);
        synchronized (this.m) {
            cl1 d0 = d0(zkVar);
            oy1 = new oy1();
            if (d0 != null) {
                zkVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new bx(this, zkVar));
                xx xxVar = new xx(this, zkVar);
                if (!zkVar.t) {
                    zkVar.d.add(xxVar);
                }
                gz gzVar = new gz(oy1, this, zkVar);
                if (!zkVar.t) {
                    zkVar.h.add(gzVar);
                }
                zkVar.a(new w20(oy1, this, zkVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                zkVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.cs1
    public qy1<tl7> W() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        k5 k5Var = this.d;
        cq cqVar = new cq(k5Var, this.b, yp.u, new ru(k5Var));
        synchronized (this.m) {
            cl1 d0 = d0(cqVar);
            oy1 = new oy1();
            if (d0 != null) {
                cqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new p00(this, cqVar));
                e10 e10 = new e10(this, cqVar);
                if (!cqVar.t) {
                    cqVar.d.add(e10);
                }
                g20 g20 = new g20(oy1, this, cqVar);
                if (!cqVar.t) {
                    cqVar.h.add(g20);
                }
                cqVar.a(new s50(oy1, this, cqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                cqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.zr1
    public oy1<tl7> Y(yn1 yn1) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        yn1.toJSONString(2);
        ao aoVar = new ao(this.d, this.b, yn1, 0, null, 24);
        synchronized (this.m) {
            cl1 d0 = d0(aoVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                aoVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new j00(this, aoVar));
                x00 x00 = new x00(this, aoVar);
                if (!aoVar.t) {
                    aoVar.d.add(x00);
                }
                a20 a20 = new a20(oy1, this, aoVar);
                if (!aoVar.t) {
                    aoVar.h.add(a20);
                }
                aoVar.a(new jy(oy1, this, aoVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                aoVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.ls1
    public void Z(byte[] bArr) {
        ky1 ky1 = ky1.DEBUG;
        dy1.e(bArr, null, 1, null);
        d90.i.d(new a90(ey1.a(i00.d), v80.e, this.d.x, ey1.a(i00.d), e.a("UUID.randomUUID().toString()"), true, this.l, null, null, g80.k(new JSONObject(), jd0.t2, Long.valueOf(ix1.f1688a.b(bArr, ix1.a.CRC32))), 384));
        ix.b.b(this.d.x, bArr);
    }

    @DexIgnore
    @Override // com.fossil.br1
    public boolean a() {
        ky1 ky1 = ky1.DEBUG;
        if (isActive()) {
            zw.i.j(this);
        }
        d90.i.d(new a90(ey1.a(i00.c), v80.e, this.d.x, ey1.a(i00.c), e.a("UUID.randomUUID().toString()"), true, this.l, null, null, null, 896));
        C0();
        G0();
        I0(false);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.os1
    public oy1<tl7> a0(su1 su1) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        su1.toJSONString(2);
        ef efVar = new ef(this.d, this.b, su1);
        efVar.v(new c50(this));
        synchronized (this.m) {
            cl1 d0 = d0(efVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                efVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new x0(this, efVar));
                q1 q1Var = new q1(this, efVar);
                if (!efVar.t) {
                    efVar.d.add(q1Var);
                }
                b4 b4Var = new b4(oy1, this, efVar);
                if (!efVar.t) {
                    efVar.h.add(b4Var);
                }
                efVar.a(new k40(oy1, this, efVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                efVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.ps1
    public qy1<byte[]> b(byte[] bArr) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        dy1.e(bArr, null, 1, null);
        hf hfVar = new hf(this.d, this.b, rt.PRE_SHARED_KEY, bArr);
        synchronized (this.m) {
            cl1 d0 = d0(hfVar);
            oy1 = new oy1();
            if (d0 != null) {
                hfVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new p1(this, hfVar));
                j3 j3Var = new j3(this, hfVar);
                if (!hfVar.t) {
                    hfVar.d.add(j3Var);
                }
                h8 h8Var = new h8(oy1, this, hfVar);
                if (!hfVar.t) {
                    hfVar.h.add(h8Var);
                }
                hfVar.a(new v70(oy1, this, hfVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                hfVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.nr1
    public qy1<tl7> c() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        mq mqVar = new mq(this.d, this.b);
        synchronized (this.m) {
            cl1 d0 = d0(mqVar);
            oy1 = new oy1();
            if (d0 != null) {
                mqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new lc(this, mqVar));
                md mdVar = new md(this, mqVar);
                if (!mqVar.t) {
                    mqVar.d.add(mdVar);
                }
                fx fxVar = new fx(oy1, this, mqVar);
                if (!mqVar.t) {
                    mqVar.h.add(fxVar);
                }
                mqVar.a(new g1(oy1, this, mqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                mqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.ks1
    public oy1<tl7> c0(fv1[] fv1Arr) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(fv1Arr).toString(2);
        pr prVar = new pr(this.d, this.b, fv1Arr, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(prVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                prVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new ry(this, prVar));
                iz izVar = new iz(this, prVar);
                if (!prVar.t) {
                    prVar.d.add(izVar);
                }
                m00 m00 = new m00(oy1, this, prVar);
                if (!prVar.t) {
                    prVar.h.add(m00);
                }
                prVar.a(new xw(oy1, this, prVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                prVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.xq1
    public oy1<tl7> cleanUp() {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        gm gmVar = new gm(this.d, this.b);
        synchronized (this.m) {
            cl1 d0 = d0(gmVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                gmVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new by(this, gmVar));
                ty tyVar = new ty(this, gmVar);
                if (!gmVar.t) {
                    gmVar.d.add(tyVar);
                }
                a00 a00 = new a00(oy1, this, gmVar);
                if (!gmVar.t) {
                    gmVar.h.add(a00);
                }
                gmVar.a(new yc(oy1, this, gmVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                gmVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.vq1
    public qy1<tl7> d(long j2) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        pq pqVar = new pq(this.d, this.b, j2);
        synchronized (this.m) {
            cl1 d0 = d0(pqVar);
            oy1 = new oy1();
            if (d0 != null) {
                pqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new p40(this, pqVar));
                h50 h50 = new h50(this, pqVar);
                if (!pqVar.t) {
                    pqVar.d.add(h50);
                }
                s60 s60 = new s60(oy1, this, pqVar);
                if (!pqVar.t) {
                    pqVar.h.add(s60);
                }
                pqVar.a(new f10(oy1, this, pqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                pqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final cl1 d0(lp lpVar) {
        T t2;
        boolean z2;
        if (tk1.k.w() != tk1.c.ENABLED) {
            return cl1.BLUETOOTH_OFF;
        }
        int i2 = l10.e[lpVar.y.ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
            return null;
        }
        if (i2 != 5) {
            return this.v != yk1.c.UPGRADING_FIRMWARE ? null : cl1.DEVICE_BUSY;
        }
        ArrayList<lp> a2 = this.g.a();
        if (a2.isEmpty()) {
            return null;
        }
        Iterator<T> it = a2.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            T t3 = t2;
            yp ypVar = t3.y;
            if (ypVar == yp.g || ypVar == yp.c || t3.y().compareTo(a9.LOW) <= 0) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
            if (z2) {
                break;
            }
        }
        if (t2 != null) {
            return cl1.DEVICE_BUSY;
        }
        return null;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.hs1
    public oy1<String> e(pu1 pu1) {
        oy1<String> oy1;
        ky1 ky1 = ky1.DEBUG;
        pu1.toJSONString(2);
        or orVar = new or(this.d, this.b, pu1);
        orVar.v(new a30(this));
        synchronized (this.m) {
            cl1 d0 = d0(orVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                orVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new l00(this, orVar));
                z00 z00 = new z00(this, orVar);
                if (!orVar.t) {
                    orVar.d.add(z00);
                }
                b20 b20 = new b20(oy1, this, orVar);
                if (!orVar.t) {
                    orVar.h.add(b20);
                }
                orVar.a(new k20(oy1, this, orVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                orVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.gs1
    public oy1<tl7> f(boolean z2) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        aq aqVar = new aq(this.d, this.b, z2, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(aqVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                aqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new f50(this, aqVar));
                x50 x50 = new x50(this, aqVar);
                if (!aqVar.t) {
                    aqVar.d.add(x50);
                }
                g70 g70 = new g70(oy1, this, aqVar);
                if (!aqVar.t) {
                    aqVar.h.add(g70);
                }
                aqVar.a(new g00(oy1, this, aqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                aqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final oy1<tl7> f0(mt1 mt1) {
        ob obVar;
        oy1<tl7> oy1;
        if ((mt1 instanceof it1) || (mt1 instanceof xt1) || (mt1 instanceof tt1) || (mt1 instanceof yt1) || (mt1 instanceof lt1) || (mt1 instanceof gt1) || (mt1 instanceof ht1) || (mt1 instanceof ot1) || (mt1 instanceof ut1) || (mt1 instanceof rt1) || (mt1 instanceof qt1) || (mt1 instanceof vt1) || (mt1 instanceof wt1) || (mt1 instanceof zt1) || (mt1 instanceof au1)) {
            obVar = ob.UI_SCRIPT;
        } else if (!(mt1 instanceof st1) && !(mt1 instanceof kt1) && !(mt1 instanceof jt1) && !(mt1 instanceof pt1)) {
            oy1<tl7> oy12 = new oy1<>();
            oy12.n(new bl1(cl1.INVALID_PARAMETERS, null, 2));
            return oy12;
        } else {
            obVar = ob.MICRO_APP;
        }
        cn cnVar = new cn(this.d, this.b, mt1, obVar, null, 16);
        synchronized (this.m) {
            cl1 d0 = d0(cnVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                cnVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new v60(this, cnVar));
                n70 n70 = new n70(this, cnVar);
                if (!cnVar.t) {
                    cnVar.d.add(n70);
                }
                b1 b1Var = new b1(oy1, this, cnVar);
                if (!cnVar.t) {
                    cnVar.h.add(b1Var);
                }
                cnVar.a(new d3(oy1, this, cnVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                cnVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.rs1
    public oy1<FitnessData[]> g(nm1 nm1) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        nm1.toJSONString(2);
        rq rqVar = new rq(this.d, this.b, nm1, new HashMap());
        synchronized (this.m) {
            cl1 d0 = d0(rqVar);
            oy1 = new oy1();
            if (d0 != null) {
                rqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new o00(this, rqVar));
                d10 d10 = new d10(this, rqVar);
                if (!rqVar.t) {
                    rqVar.d.add(d10);
                }
                f20 f20 = new f20(oy1, this, rqVar);
                if (!rqVar.t) {
                    rqVar.h.add(f20);
                }
                rqVar.a(new y10(oy1, this, rqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                rqVar.F();
            }
        }
        return oy1.t(c30.b);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.e60 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.yk1
    public <T> T g0(Class<T> cls) {
        if (em7.B(this.u.getDeviceType().a(), cls)) {
            ky1 ky1 = ky1.DEBUG;
            cls.getSimpleName();
            return this;
        }
        ky1 ky12 = ky1.DEBUG;
        cls.getSimpleName();
        return null;
    }

    @DexIgnore
    @Override // com.fossil.yk1
    public yk1.c getState() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.js1
    public oy1<tl7> h(gv1 gv1) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        gv1.toJSONString(2);
        np npVar = new np(this.d, this.b, gv1, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(npVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                npVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new q20(this, npVar));
                g30 g30 = new g30(this, npVar);
                if (!npVar.t) {
                    npVar.d.add(g30);
                }
                o40 o40 = new o40(oy1, this, npVar);
                if (!npVar.t) {
                    npVar.h.add(o40);
                }
                npVar.a(new e8(oy1, this, npVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                npVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final oy1<byte[][]> h0(boolean z2, boolean z3, a9 a9Var) {
        lp ylVar;
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        if (z3) {
            ylVar = new ml(this.d, this.b, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.FALSE), hl7.a(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.TRUE)));
        } else {
            ylVar = new yl(this.d, this.b, yp.q0, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.valueOf(z2)), hl7.a(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z2))), e.a("UUID.randomUUID().toString()"));
            ylVar.f(a9Var);
        }
        synchronized (this.m) {
            cl1 d0 = d0(ylVar);
            oy1 = new oy1();
            if (d0 != null) {
                ylVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new g50(this, ylVar));
                jc jcVar = new jc(this, ylVar);
                if (!ylVar.t) {
                    ylVar.d.add(jcVar);
                }
                h30 h30 = new h30(oy1, this, ylVar);
                if (!ylVar.t) {
                    ylVar.h.add(h30);
                }
                ylVar.a(new n50(oy1, this, ylVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                ylVar.F();
            }
        }
        return oy1.t(g60.b);
    }

    @DexIgnore
    @Override // com.fossil.qr1
    public qy1<tl7> i(xn1 xn1) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        xn1.toJSONString(2);
        fi fiVar = new fi(this.d, this.b, xn1);
        synchronized (this.m) {
            cl1 d0 = d0(fiVar);
            oy1 = new oy1();
            if (d0 != null) {
                fiVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new o20(this, fiVar));
                e30 e30 = new e30(this, fiVar);
                if (!fiVar.t) {
                    fiVar.d.add(e30);
                }
                n40 n40 = new n40(oy1, this, fiVar);
                if (!fiVar.t) {
                    fiVar.h.add(n40);
                }
                fiVar.a(new d00(oy1, this, fiVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                fiVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.yk1
    public boolean isActive() {
        return zw.i.f(this);
    }

    @DexIgnore
    public final qy1<tl7> j0() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        k5 k5Var = this.d;
        ji jiVar = new ji(k5Var, this.b, rt.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY, ix.b.a(k5Var.x).c(), e.a("UUID.randomUUID().toString()"));
        jiVar.v(new y2(this));
        synchronized (this.m) {
            cl1 d0 = d0(jiVar);
            oy1 = new oy1();
            if (d0 != null) {
                jiVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new y50(this, jiVar));
                q60 q60 = new q60(this, jiVar);
                if (!jiVar.t) {
                    jiVar.d.add(q60);
                }
                z70 z70 = new z70(oy1, this, jiVar);
                if (!jiVar.t) {
                    jiVar.h.add(z70);
                }
                jiVar.a(new d1(oy1, this, jiVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                jiVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.ss1
    public oy1<lw1> k(lw1 lw1) {
        oy1<lw1> oy1;
        ky1 ky1 = ky1.DEBUG;
        lw1.toJSONString(2);
        bg bgVar = new bg(this.d, this.b, lw1);
        synchronized (this.m) {
            cl1 d0 = d0(bgVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                bgVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new e20(this, bgVar));
                t20 t20 = new t20(this, bgVar);
                if (!bgVar.t) {
                    bgVar.d.add(t20);
                }
                z30 z30 = new z30(oy1, this, bgVar);
                if (!bgVar.t) {
                    bgVar.h.add(z30);
                }
                bgVar.a(new v30(oy1, this, bgVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                bgVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final qy1<tl7> k0(c2 c2Var) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        c2Var.toJSONString(2);
        em emVar = new em(this.d, this.b, c2Var);
        synchronized (this.m) {
            cl1 d0 = d0(emVar);
            oy1 = new oy1();
            if (d0 != null) {
                emVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new y30(this, emVar));
                q40 q40 = new q40(this, emVar);
                if (!emVar.t) {
                    emVar.d.add(q40);
                }
                j8 j8Var = new j8(oy1, this, emVar);
                if (!emVar.t) {
                    emVar.h.add(j8Var);
                }
                emVar.a(new w10(oy1, this, emVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                emVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.yk1
    public qy1<tl7> l() {
        ky1 ky1 = ky1.DEBUG;
        d90.i.d(new a90(ey1.a(i00.f), v80.i, this.d.x, "", "", true, null, null, null, null, 960));
        qy1<tl7> qy1 = new qy1<>();
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new rd(this, qy1, null), 3, null);
        return qy1;
    }

    @DexIgnore
    public final qy1<FitnessData[]> l0(nm1 nm1, SyncMode syncMode) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        syncMode.name();
        nm1.toJSONString(2);
        mi miVar = new mi(this.d, this.b, nm1, null, null, syncMode, 24);
        synchronized (this.m) {
            cl1 d0 = d0(miVar);
            oy1 = new oy1();
            if (d0 != null) {
                miVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new de(this, miVar));
                p20 p20 = new p20(this, miVar);
                if (!miVar.t) {
                    miVar.d.add(p20);
                }
                w0 w0Var = new w0(oy1, this, miVar);
                if (!miVar.t) {
                    miVar.h.add(w0Var);
                }
                miVar.a(new tz(oy1, this, miVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                miVar.F();
            }
        }
        return oy1.t(m20.b);
    }

    @DexIgnore
    @Override // com.fossil.er1
    public oy1<zk1> m() {
        oy1<zk1> oy1;
        ky1 ky1 = ky1.DEBUG;
        if (this.v == yk1.c.CONNECTED) {
            oy1<zk1> oy12 = new oy1<>();
            oy12.y(1.0f);
            oy12.o(this.u);
            return oy12;
        }
        ah ahVar = new ah(this.d, this.b, e.a("UUID.randomUUID().toString()"));
        synchronized (this.m) {
            cl1 d0 = d0(ahVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                ahVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new b60(this, ahVar));
                t60 t60 = new t60(this, ahVar);
                if (!ahVar.t) {
                    ahVar.d.add(t60);
                }
                d80 d80 = new d80(oy1, this, ahVar);
                if (!ahVar.t) {
                    ahVar.h.add(d80);
                }
                ahVar.a(new z2(oy1, this, ahVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                ahVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final qy1<tl7> m0(HashMap<dr, Object> hashMap) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        ni niVar = new ni(this.d, this.b, hashMap);
        synchronized (this.m) {
            cl1 d0 = d0(niVar);
            oy1 = new oy1();
            if (d0 != null) {
                niVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new b10(this, niVar));
                k70 k70 = new k70(this, niVar);
                if (!niVar.t) {
                    niVar.d.add(k70);
                }
                jz jzVar = new jz(oy1, this, niVar);
                if (!niVar.t) {
                    niVar.h.add(jzVar);
                }
                niVar.a(new w40(oy1, this, niVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                niVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.as1
    public oy1<tl7> n(fl1[] fl1Arr) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(fl1Arr).toString(2);
        mo moVar = new mo(this.d, this.b, fl1Arr, 0, null, 24);
        synchronized (this.m) {
            cl1 d0 = d0(moVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                moVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new e70(this, moVar));
                w70 w70 = new w70(this, moVar);
                if (!moVar.t) {
                    moVar.d.add(w70);
                }
                n1 n1Var = new n1(oy1, this, moVar);
                if (!moVar.t) {
                    moVar.h.add(n1Var);
                }
                moVar.a(new t00(oy1, this, moVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                moVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void n0(com.fossil.nr r5) {
        /*
            r4 = this;
            r1 = 1
            com.fossil.zq r0 = r5.c
            com.fossil.zq r2 = com.fossil.zq.REQUEST_ERROR
            if (r0 != r2) goto L_0x002a
            com.fossil.mw r0 = r5.d
            com.fossil.lw r2 = r0.d
            com.fossil.lw r3 = com.fossil.lw.d
            if (r2 != r3) goto L_0x002a
            com.fossil.s5 r0 = r0.e
            com.fossil.r5 r2 = r0.c
            com.fossil.r5 r3 = com.fossil.r5.d
            if (r2 != r3) goto L_0x002a
            com.fossil.g7 r0 = r0.d
            com.fossil.f7 r0 = r0.b
            com.fossil.f7 r2 = com.fossil.f7.GATT_NULL
            if (r0 == r2) goto L_0x0023
            com.fossil.f7 r2 = com.fossil.f7.START_FAIL
            if (r0 != r2) goto L_0x002a
        L_0x0023:
            r0 = r1
        L_0x0024:
            if (r1 != r0) goto L_0x0029
            r4.C0()
        L_0x0029:
            return
        L_0x002a:
            r0 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e60.n0(com.fossil.nr):void");
    }

    @DexIgnore
    @Override // com.fossil.or1
    public qy1<tl7> p(xl1 xl1, ul1[] ul1Arr) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(ul1Arr).toString(2);
        k5 k5Var = this.d;
        cq cqVar = new cq(k5Var, this.b, yp.t, new ou(k5Var, xl1, ul1Arr));
        synchronized (this.m) {
            cl1 d0 = d0(cqVar);
            oy1 = new oy1();
            if (d0 != null) {
                cqVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new mc(this, cqVar));
                nd ndVar = new nd(this, cqVar);
                if (!cqVar.t) {
                    cqVar.d.add(ndVar);
                }
                gx gxVar = new gx(oy1, this, cqVar);
                if (!cqVar.t) {
                    cqVar.h.add(gxVar);
                }
                cqVar.a(new z7(oy1, this, cqVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                cqVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.fs1
    public oy1<ry1> q(ou1 ou1) {
        oy1<ry1> oy1;
        ky1 ky1 = ky1.DEBUG;
        ou1.toJSONString(2);
        ar arVar = new ar(this.d, this.b, ou1);
        arVar.v(new sx(this));
        synchronized (this.m) {
            cl1 d0 = d0(arVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                arVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new ge(this, arVar));
                ex exVar = new ex(this, arVar);
                if (!arVar.t) {
                    arVar.d.add(exVar);
                }
                sy syVar = new sy(oy1, this, arVar);
                if (!arVar.t) {
                    arVar.h.add(syVar);
                }
                arVar.a(new ww(oy1, this, arVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                arVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final void q0(yk1.c cVar) {
        yk1.c cVar2;
        synchronized (this.v) {
            cVar2 = this.v;
            this.v = cVar;
            tl7 tl7 = tl7.f3441a;
        }
        if (cVar2 != cVar) {
            d90.i.d(new a90(ey1.a(i00.e), v80.i, this.d.x, "", "", true, null, null, null, g80.k(g80.k(new JSONObject(), jd0.C0, ey1.a(cVar2)), jd0.B0, ey1.a(cVar)), 448));
            yk1.c cVar3 = this.v;
            ky1 ky1 = ky1.DEBUG;
            Intent intent = new Intent();
            intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED");
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE", cVar2);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE", cVar3);
            Context a2 = id0.i.a();
            if (a2 != null) {
                ct0.b(a2).d(intent);
            }
            Looper myLooper = Looper.myLooper();
            if (myLooper == null) {
                myLooper = Looper.getMainLooper();
            }
            if (myLooper != null) {
                new Handler(myLooper).post(new ox(this, cVar2, cVar3));
                int i2 = l10.c[cVar.ordinal()];
                if (i2 == 1) {
                    D0();
                    this.g.e(zq.CONNECTION_DROPPED, new yp[]{yp.g});
                    this.g.d(new nw(new Hashtable(), null));
                    this.e = false;
                } else if (i2 == 2) {
                    D0();
                } else if (i2 == 3) {
                    zk1 zk1 = this.u;
                    c90.e.d(zk1.getMacAddress(), zk1);
                    this.g.g();
                } else if (i2 == 5) {
                    D0();
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ss1
    public oy1<lw1> r(kw1 kw1) {
        ky1 ky1 = ky1.DEBUG;
        kw1.d();
        oy1<lw1> oy1 = new oy1<>();
        xw7 unused = gu7.d(id0.i.e(), null, null, new o60(this, kw1, oy1, null), 3, null);
        return oy1;
    }

    @DexIgnore
    public final void r0(yk1.d dVar, yk1.d dVar2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE", dVar);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE", dVar2);
        Context a2 = id0.i.a();
        if (a2 != null) {
            ct0.b(a2).d(intent);
        }
    }

    @DexIgnore
    @Override // com.fossil.wr1
    public oy1<tl7> s(zn1 zn1) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        zn1.toJSONString(2);
        gk gkVar = new gk(this.d, this.b, zn1, 0, null, 24);
        synchronized (this.m) {
            cl1 d0 = d0(gkVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                gkVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new ax(this, gkVar));
                wx wxVar = new wx(this, gkVar);
                if (!gkVar.t) {
                    gkVar.d.add(wxVar);
                }
                fz fzVar = new fz(oy1, this, gkVar);
                if (!gkVar.t) {
                    gkVar.h.add(fzVar);
                }
                gkVar.a(new pz(oy1, this, gkVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                gkVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final void s0(mp1 mp1) {
        yk1.b bVar = this.w;
        if (bVar != null) {
            bVar.onEventReceived(this, mp1);
        }
    }

    @DexIgnore
    @Override // com.fossil.qs1
    public qy1<tl7> t() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        kh khVar = new kh(this.d, this.b, e.a("UUID.randomUUID().toString()"));
        synchronized (this.m) {
            cl1 d0 = d0(khVar);
            oy1 = new oy1();
            if (d0 != null) {
                khVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new wz(this, khVar));
                k00 k00 = new k00(this, khVar);
                if (!khVar.t) {
                    khVar.d.add(k00);
                }
                n10 n10 = new n10(oy1, this, khVar);
                if (!khVar.t) {
                    khVar.h.add(n10);
                }
                khVar.a(new gc(oy1, this, khVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                khVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final void t0(yx1 yx1) {
    }

    @DexIgnore
    @Override // com.fossil.ur1
    public qy1<tl7> u() {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        qy1<tl7> qy1 = new qy1<>();
        mk mkVar = new mk(this.d, this.b, e.a("UUID.randomUUID().toString()"));
        synchronized (this.m) {
            cl1 d0 = d0(mkVar);
            oy1 = new oy1();
            if (d0 != null) {
                mkVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new j70(this, mkVar));
                b80 b80 = new b80(this, mkVar);
                if (!mkVar.t) {
                    mkVar.d.add(b80);
                }
                r1 r1Var = new r1(oy1, this, mkVar);
                if (!mkVar.t) {
                    mkVar.h.add(r1Var);
                }
                mkVar.a(new cd(oy1, this, mkVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                mkVar.F();
            }
        }
        oy1.s(new wd(qy1)).r(new uw(qy1));
        return qy1;
    }

    @DexIgnore
    public final void u0(WorkoutSessionManager workoutSessionManager) {
        this.t = workoutSessionManager;
    }

    @DexIgnore
    @Override // com.fossil.ms1
    public oy1<ru1[]> v(ru1[] ru1Arr) {
        oy1<ru1[]> oy1;
        ky1 ky1 = ky1.DEBUG;
        px1.a(ru1Arr).toString(2);
        il ilVar = new il(this.d, this.b, ru1Arr);
        synchronized (this.m) {
            cl1 d0 = d0(ilVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                ilVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new q10(this, ilVar));
                d20 d20 = new d20(this, ilVar);
                if (!ilVar.t) {
                    ilVar.d.add(d20);
                }
                i30 i30 = new i30(oy1, this, ilVar);
                if (!ilVar.t) {
                    ilVar.h.add(i30);
                }
                ilVar.a(new sz(oy1, this, ilVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                ilVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final void v0(boolean z2) {
        synchronized (Boolean.valueOf(this.f)) {
            this.f = z2;
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    @Override // com.fossil.ts1
    public qy1<Boolean> w(byte[] bArr) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        ix1.f1688a.b(bArr, ix1.a.CRC32);
        lg lgVar = new lg(this.d, this.b, bArr);
        synchronized (this.m) {
            cl1 d0 = d0(lgVar);
            oy1 = new oy1();
            if (d0 != null) {
                lgVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new yx(this, lgVar));
                qy qyVar = new qy(this, lgVar);
                if (!lgVar.t) {
                    lgVar.d.add(qyVar);
                }
                xz xzVar = new xz(oy1, this, lgVar);
                if (!lgVar.t) {
                    lgVar.h.add(xzVar);
                }
                lgVar.a(new l50(oy1, this, lgVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                lgVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.y, i2);
        }
        if (parcel != null) {
            parcel.writeString(this.z);
        }
    }

    @DexIgnore
    @Override // com.fossil.yr1
    public oy1<tl7> x(byte[] bArr) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        String str = "sendEncryptedData invoked: encryptedData=" + dy1.e(bArr, null, 1, null) + '.';
        on onVar = new on(this.d, this.b, bArr, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(onVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                onVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new l3(this, onVar));
                c4 c4Var = new c4(this, onVar);
                if (!onVar.t) {
                    onVar.d.add(c4Var);
                }
                kc kcVar = new kc(oy1, this, onVar);
                if (!onVar.t) {
                    onVar.h.add(kcVar);
                }
                onVar.a(new dd(oy1, this, onVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                onVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    @Override // com.fossil.xr1
    public oy1<tl7> y(mt1 mt1) {
        oy1<tl7> oy1;
        ky1 ky1 = ky1.DEBUG;
        mt1.toJSONString(2);
        if (!(mt1 instanceof bu1)) {
            return f0(mt1);
        }
        Cdo doVar = new Cdo(this.d, this.b, (bu1) mt1, null, 8);
        synchronized (this.m) {
            cl1 d0 = d0(doVar);
            oy1 = new oy1<>();
            if (d0 != null) {
                doVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new l30(this, doVar));
                b40 b40 = new b40(this, doVar);
                if (!doVar.t) {
                    doVar.d.add(b40);
                }
                k50 k50 = new k50(oy1, this, doVar);
                if (!doVar.t) {
                    doVar.h.add(k50);
                }
                doVar.a(new b70(oy1, this, doVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                doVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final oy1<byte[][]> y0(boolean z2, boolean z3, a9 a9Var) {
        lp cjVar;
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        if (z3) {
            cjVar = new pi(this.d, this.b, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.FALSE), hl7.a(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.FALSE)));
        } else {
            cjVar = new cj(this.d, this.b, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.valueOf(z2)), hl7.a(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z2))), (String) null, 8);
            cjVar.f(a9Var);
        }
        synchronized (this.m) {
            cl1 d0 = d0(cjVar);
            oy1 = new oy1();
            if (d0 != null) {
                cjVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new e80(this, cjVar));
                cy cyVar = new cy(this, cjVar);
                if (!cjVar.t) {
                    cjVar.d.add(cyVar);
                }
                d60 d60 = new d60(oy1, this, cjVar);
                if (!cjVar.t) {
                    cjVar.h.add(d60);
                }
                cjVar.a(new rw(oy1, this, cjVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                cjVar.F();
            }
        }
        return oy1.t(nx.b);
    }

    @DexIgnore
    @Override // com.fossil.pr1
    public qy1<tl7> z(ho1 ho1) {
        oy1 oy1;
        ky1 ky1 = ky1.DEBUG;
        ho1.toJSONString(2);
        sh shVar = new sh(this.d, this.b, ho1);
        synchronized (this.m) {
            cl1 d0 = d0(shVar);
            oy1 = new oy1();
            if (d0 != null) {
                shVar.k(l10.d[d0.ordinal()] != 1 ? zq.NOT_ALLOW_TO_START : zq.BLUETOOTH_OFF);
                oy1.n(new bl1(d0, null, 2));
            } else {
                oy1.q(new a10(this, shVar));
                p10 p10 = new p10(this, shVar);
                if (!shVar.t) {
                    shVar.d.add(p10);
                }
                r20 r20 = new r20(oy1, this, shVar);
                if (!shVar.t) {
                    shVar.h.add(r20);
                }
                shVar.a(new sw(oy1, this, shVar));
                if (this.v != yk1.c.CONNECTED && !this.e) {
                    z0(zm7.i(hl7.a(ym.AUTO_CONNECT, Boolean.valueOf(q3.f.c(true))), hl7.a(ym.CONNECTION_TIME_OUT, 30000L)));
                }
                shVar.F();
            }
        }
        return oy1;
    }

    @DexIgnore
    public final py1<zk1, nr> z0(HashMap<ym, Object> hashMap) {
        ky1 ky1 = ky1.DEBUG;
        dr7 dr7 = new dr7();
        dr7.element = null;
        py1<zk1, nr> py1 = new py1<>();
        py1.k(new n0(dr7));
        if (this.v == yk1.c.CONNECTED) {
            py1.o(this.u);
        } else if (!this.e) {
            this.l = e.a("UUID.randomUUID().toString()");
            c90.e.e(this.u.getMacAddress(), this.l);
            T t2 = (T) new fh(this.d, this.b, hashMap, e.a("UUID.randomUUID().toString()"));
            dr7.element = t2;
            T t3 = t2;
            h60 h60 = new h60(this);
            if (!t3.t) {
                t3.d.add(h60);
            }
            t3.v(new y60(this, py1));
            t3.s(new q70(this, py1));
            dr7.element.F();
        } else {
            ky1 ky12 = ky1.DEBUG;
            py1.n(new nr(yp.c, zq.NOT_ALLOW_TO_START, null, null, 12));
        }
        return py1;
    }
}
