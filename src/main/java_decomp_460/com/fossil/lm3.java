package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lm3 extends jj3 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public lm3(pm3 pm3) {
        super(pm3);
        this.f1780a.g(this);
    }

    @DexIgnore
    public abstract boolean A();

    @DexIgnore
    public void v() {
    }

    @DexIgnore
    public final boolean w() {
        return this.b;
    }

    @DexIgnore
    public final void x() {
        if (!w()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void y() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!A()) {
            this.f1780a.s();
            this.b = true;
        }
    }

    @DexIgnore
    public final void z() {
        if (!this.b) {
            v();
            this.f1780a.s();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
