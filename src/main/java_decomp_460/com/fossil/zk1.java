package com.fossil;

import android.bluetooth.BluetoothAdapter;
import com.fossil.ry1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk1 extends ox1 {
    @DexIgnore
    public static /* final */ c u; // = new c(null);
    @DexIgnore
    public al1 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ ry1 j;
    @DexIgnore
    public /* final */ ry1 k;
    @DexIgnore
    public /* final */ ry1 l;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, ry1> m;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, ry1> n;
    @DexIgnore
    public /* final */ a o;
    @DexIgnore
    public /* final */ zm1[] p;
    @DexIgnore
    public /* final */ ry1 q;
    @DexIgnore
    public /* final */ String r;
    @DexIgnore
    public /* final */ ry1 s;
    @DexIgnore
    public /* final */ String t;

    @DexIgnore
    public enum a {
        NO_REQUIRE((byte) 0),
        REQUIRE((byte) 1);
        
        @DexIgnore
        public static /* final */ C0307a d; // = new C0307a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zk1$a$a")
        /* renamed from: com.fossil.zk1$a$a  reason: collision with other inner class name */
        public static final class C0307a {
            @DexIgnore
            public /* synthetic */ C0307a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(byte b) {
                a[] values = a.values();
                for (a aVar : values) {
                    if (aVar.a() == b) {
                        return aVar;
                    }
                }
                return null;
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore
    public enum b {
        SERIAL_NUMBER(1),
        HARDWARE_REVISION(2),
        FIRMWARE_VERSION(3),
        MODEL_NUMBER(4),
        HEART_RATE_SERIAL_NUMBER(5),
        BOOTLOADER_VERSION(6),
        WATCH_APP_VERSION(7),
        FONT_VERSION(8),
        LUTS_VERSION(9),
        SUPPORTED_FILES_VERSION(10),
        BOND_REQUIREMENT(11),
        SUPPORTED_DEVICE_CONFIGS(12),
        DEVICE_SECURITY_VERSION(14),
        SOCKET_INFO(15),
        LOCALE(16),
        MICRO_APP_SYSTEM_VERSION(17),
        LOCALE_VERSION(18),
        CURRENT_FILES_VERSION(19),
        UNKNOWN((short) 65535);
        
        @DexIgnore
        public static /* final */ a d; // = new a(null);
        @DexIgnore
        public /* final */ short b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(kq7 kq7) {
            }

            @DexIgnore
            public final b a(short s) {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == s) {
                        break;
                    }
                    i++;
                }
                return bVar != null ? bVar : b.UNKNOWN;
            }
        }

        @DexIgnore
        public b(short s) {
            this.b = (short) s;
        }

        @DexIgnore
        public final short a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* synthetic */ c(kq7 kq7) {
        }

        @DexIgnore
        public final zk1 a(zk1 zk1, zk1 zk12) {
            return new zk1(zk12.getName().length() > 0 ? zk12.getName() : zk1.getName(), zk12.getMacAddress().length() > 0 ? zk12.getMacAddress() : zk1.getMacAddress(), d(zk12.getSerialNumber()) ? zk12.getSerialNumber() : zk1.getSerialNumber(), zk12.getHardwareRevision().length() > 0 ? zk12.getHardwareRevision() : zk1.getHardwareRevision(), zk12.getFirmwareVersion().length() > 0 ? zk12.getFirmwareVersion() : zk1.getFirmwareVersion(), zk12.getModelNumber().length() > 0 ? zk12.getModelNumber() : zk1.getModelNumber(), zk12.f().length() > 0 ? zk12.f() : zk1.f(), pq7.a(zk12.b(), new ry1(0, 0)) ^ true ? zk12.b() : zk1.b(), pq7.a(zk12.i(), new ry1(0, 0)) ^ true ? zk12.i() : zk1.i(), pq7.a(zk12.e(), new ry1(0, 0)) ^ true ? zk12.e() : zk1.e(), zk12.h().isEmpty() ^ true ? zk12.h() : zk1.h(), zk12.c().isEmpty() ^ true ? zk12.c() : zk1.c(), zk12.a(), (zk12.g().length == 0) ^ true ? zk12.g() : zk1.g(), pq7.a(zk12.d(), new ry1(0, 0)) ^ true ? zk12.d() : zk1.d(), pq7.a(zk12.getLocaleString(), "en_US") ^ true ? zk12.getLocaleString() : zk1.getLocaleString(), pq7.a(zk12.getMicroAppVersion(), new ry1(0, 0)) ^ true ? zk12.getMicroAppVersion() : zk1.getMicroAppVersion(), pq7.a(zk12.getFastPairIdInHexString(), "") ^ true ? zk12.getFastPairIdInHexString() : zk1.getFastPairIdInHexString());
        }

        @DexIgnore
        public final zk1 b(JSONObject jSONObject) {
            String optString = jSONObject.optString(ey1.a(jd0.H), "");
            String optString2 = jSONObject.optString(ey1.a(jd0.k0), "");
            pq7.b(optString, "name");
            if ((optString.length() == 0) || !BluetoothAdapter.checkBluetoothAddress(optString2)) {
                return null;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            JSONArray optJSONArray = jSONObject.optJSONArray(ey1.a(jd0.I2));
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            short parseShort = Short.parseShort(optJSONObject.get(ey1.a(jd0.A0)).toString());
                            ry1.a aVar = ry1.CREATOR;
                            String optString3 = optJSONObject.optString(ey1.a(jd0.K2));
                            pq7.b(optString3, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap.put(Short.valueOf(parseShort), aVar.c(optString3));
                        }
                    } catch (JSONException e) {
                        d90.i.i(e);
                    }
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            JSONArray optJSONArray2 = jSONObject.optJSONArray(ey1.a(jd0.A4));
            if (optJSONArray2 != null) {
                int length2 = optJSONArray2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    try {
                        JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject2 != null) {
                            short parseShort2 = Short.parseShort(optJSONObject2.get(ey1.a(jd0.A0)).toString());
                            ry1.a aVar2 = ry1.CREATOR;
                            String optString4 = optJSONObject2.optString(ey1.a(jd0.K2));
                            pq7.b(optString4, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap2.put(Short.valueOf(parseShort2), aVar2.c(optString4));
                        }
                    } catch (JSONException e2) {
                        d90.i.i(e2);
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray3 = jSONObject.optJSONArray(ey1.a(jd0.t0));
            if (optJSONArray3 != null) {
                int length3 = optJSONArray3.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    try {
                        zm1 a2 = zm1.e.a(optJSONArray3.get(i3).toString());
                        if (a2 != null) {
                            arrayList.add(a2);
                        }
                    } catch (JSONException e3) {
                        d90.i.i(e3);
                    }
                }
            }
            pq7.b(optString2, "macAddress");
            String optString5 = jSONObject.optString(ey1.a(jd0.l0), "");
            pq7.b(optString5, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString6 = jSONObject.optString(ey1.a(jd0.m0), "");
            pq7.b(optString6, "jsonObject.optString(JSO\u2026VISION.lowerCaseName, \"\")");
            String optString7 = jSONObject.optString(ey1.a(jd0.n0), "");
            pq7.b(optString7, "jsonObject.optString(JSO\u2026ERSION.lowerCaseName, \"\")");
            String optString8 = jSONObject.optString(ey1.a(jd0.o0), "");
            pq7.b(optString8, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString9 = jSONObject.optString(ey1.a(jd0.p0), "");
            pq7.b(optString9, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            ry1.a aVar3 = ry1.CREATOR;
            String optString10 = jSONObject.optString(ey1.a(jd0.q0), "");
            pq7.b(optString10, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            ry1 c = aVar3.c(optString10);
            ry1.a aVar4 = ry1.CREATOR;
            String optString11 = jSONObject.optString(ey1.a(jd0.r0), "");
            pq7.b(optString11, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            ry1 c2 = aVar4.c(optString11);
            ry1.a aVar5 = ry1.CREATOR;
            String optString12 = jSONObject.optString(ey1.a(jd0.s0), "");
            pq7.b(optString12, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            ry1 c3 = aVar5.c(optString12);
            a a3 = a.d.a((byte) jSONObject.optInt(ey1.a(jd0.s3), 0));
            if (a3 == null) {
                a3 = a.NO_REQUIRE;
            }
            Object[] array = arrayList.toArray(new zm1[0]);
            if (array != null) {
                ry1.a aVar6 = ry1.CREATOR;
                String optString13 = jSONObject.optString(ey1.a(jd0.v2), "");
                pq7.b(optString13, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                ry1 c4 = aVar6.c(optString13);
                String optString14 = jSONObject.optString(ey1.a(jd0.m3), "en_US");
                pq7.b(optString14, "jsonObject.optString(JSO\u2026nt.DEFAULT_LOCALE_STRING)");
                ry1.a aVar7 = ry1.CREATOR;
                String optString15 = jSONObject.optString(ey1.a(jd0.t3), "");
                pq7.b(optString15, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                ry1 c5 = aVar7.c(optString15);
                String optString16 = jSONObject.optString(ey1.a(jd0.u3), "");
                pq7.b(optString16, "jsonObject.optString(JSO\u2026STRING.lowerCaseName, \"\")");
                return new zk1(optString, optString2, optString5, optString6, optString7, optString8, optString9, c, c2, c3, linkedHashMap, linkedHashMap2, a3, (zm1[]) array, c4, optString14, c5, optString16);
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final zk1 c(byte[] bArr) throws IllegalArgumentException {
            a a2;
            zk1 zk1 = new zk1("", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            int i = 0;
            while (i <= bArr.length - 3) {
                short s = order.getShort(i);
                short p = hy1.p(order.get(i + 2));
                b a3 = b.d.a(s);
                int i2 = i + 3;
                int i3 = i2 + p;
                if (bArr.length < i3) {
                    return zk1;
                }
                byte[] k = dm7.k(bArr, i2, i3);
                switch (w60.f3887a[a3.ordinal()]) {
                    case 1:
                        Charset forName = Charset.forName("US-ASCII");
                        pq7.b(forName, "Charset.forName(\"US-ASCII\")");
                        String str = new String(k, forName);
                        if (d(str)) {
                            zk1 = zk1.a(zk1, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262139);
                            break;
                        } else {
                            throw new IllegalArgumentException("Invalid Serial Number: " + str);
                        }
                    case 2:
                        Charset forName2 = Charset.forName("US-ASCII");
                        pq7.b(forName2, "Charset.forName(\"US-ASCII\")");
                        zk1 = zk1.a(zk1, null, null, null, new String(k, forName2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262135);
                        break;
                    case 3:
                        Charset forName3 = Charset.forName("US-ASCII");
                        pq7.b(forName3, "Charset.forName(\"US-ASCII\")");
                        zk1 = zk1.a(zk1, null, null, null, null, new String(k, forName3), null, null, null, null, null, null, null, null, null, null, null, null, null, 262127);
                        break;
                    case 4:
                        Charset forName4 = Charset.forName("US-ASCII");
                        pq7.b(forName4, "Charset.forName(\"US-ASCII\")");
                        zk1 = zk1.a(zk1, null, null, null, null, null, new String(k, forName4), null, null, null, null, null, null, null, null, null, null, null, null, 262111);
                        break;
                    case 5:
                        Charset forName5 = Charset.forName("US-ASCII");
                        pq7.b(forName5, "Charset.forName(\"US-ASCII\")");
                        zk1 = zk1.a(zk1, null, null, null, null, null, null, new String(k, forName5), null, null, null, null, null, null, null, null, null, null, null, 262079);
                        break;
                    case 6:
                        ry1 b = ry1.CREATOR.b(k);
                        if (b == null) {
                            break;
                        } else {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, b, null, null, null, null, null, null, null, null, null, null, 262015);
                            break;
                        }
                    case 7:
                        ry1 b2 = ry1.CREATOR.b(k);
                        if (b2 == null) {
                            break;
                        } else {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, b2, null, null, null, null, null, null, null, null, null, 261887);
                            break;
                        }
                    case 8:
                        ry1 b3 = ry1.CREATOR.b(k);
                        if (b3 == null) {
                            break;
                        } else {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, null, b3, null, null, null, null, null, null, null, null, 261631);
                            break;
                        }
                    case 9:
                        ry1 b4 = ry1.CREATOR.b(k);
                        if (b4 == null) {
                            break;
                        } else {
                            zk1.c().put(Short.valueOf(ob.LUTS_FILE.b), b4);
                            break;
                        }
                    case 10:
                        ur7 l = bs7.l(em7.G(k), 3);
                        int a4 = l.a();
                        int b5 = l.b();
                        int c = l.c();
                        if (c < 0) {
                            if (a4 < b5) {
                                break;
                            }
                        } else if (a4 > b5) {
                            break;
                        }
                        while (true) {
                            ob a5 = ob.A.a(k[a4]);
                            if (a5 != null) {
                                zk1.h().put(Short.valueOf(a5.b), new ry1(k[a4 + 1], k[a4 + 2]));
                                tl7 tl7 = tl7.f3441a;
                            }
                            if (a4 == b5) {
                                break;
                            } else {
                                a4 += c;
                            }
                        }
                    case 11:
                        ur7 l2 = bs7.l(em7.G(k), 4);
                        int a6 = l2.a();
                        int b6 = l2.b();
                        int c2 = l2.c();
                        if (c2 < 0) {
                            if (a6 < b6) {
                                break;
                            }
                        } else if (a6 > b6) {
                            break;
                        }
                        while (true) {
                            ByteBuffer order2 = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN);
                            zk1.c().put(Short.valueOf(order2.getShort(a6)), new ry1(hy1.p(order2.get(a6 + 2)), hy1.p(order2.get(a6 + 3))));
                            if (a6 == b6) {
                                break;
                            } else {
                                a6 += c2;
                            }
                        }
                    case 12:
                        if ((!(k.length == 0)) && (a2 = a.d.a(k[0])) != null) {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, a2, null, null, null, null, null, 258047);
                            tl7 tl72 = tl7.f3441a;
                            break;
                        }
                    case 13:
                        ByteBuffer order3 = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN);
                        ArrayList arrayList = new ArrayList();
                        ur7 l3 = bs7.l(em7.G(k), 2);
                        int a7 = l3.a();
                        int b7 = l3.b();
                        int c3 = l3.c();
                        if (c3 < 0 ? a7 >= b7 : a7 <= b7) {
                            while (true) {
                                zm1 b8 = zm1.e.b(order3.getShort(a7));
                                if (b8 != null) {
                                    arrayList.add(b8);
                                }
                                if (a7 != b7) {
                                    a7 += c3;
                                }
                            }
                        }
                        Object[] array = arrayList.toArray(new zm1[0]);
                        if (array != null) {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, (zm1[]) array, null, null, null, null, 253951);
                            break;
                        } else {
                            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    case 14:
                        ry1 b9 = ry1.CREATOR.b(k);
                        if (b9 == null) {
                            break;
                        } else {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, b9, null, null, null, 245759);
                            break;
                        }
                    case 16:
                        Charset forName6 = Charset.forName("US-ASCII");
                        pq7.b(forName6, "Charset.forName(\"US-ASCII\")");
                        zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, new String(k, forName6), null, null, 229375);
                        break;
                    case 17:
                        ry1 b10 = ry1.CREATOR.b(k);
                        if (b10 == null) {
                            break;
                        } else {
                            zk1.c().put((short) 1794, b10);
                            break;
                        }
                    case 18:
                        ry1 b11 = ry1.CREATOR.b(k);
                        if (b11 == null) {
                            break;
                        } else {
                            zk1 = zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, b11, null, 196607);
                            break;
                        }
                }
                i = p + 3 + i;
            }
            return zk1;
        }

        @DexIgnore
        public final boolean d(String str) {
            if (str.length() != 10) {
                return false;
            }
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isLetterOrDigit(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public zk1(String str, String str2, String str3, String str4, String str5, String str6, String str7, ry1 ry1, ry1 ry12, ry1 ry13, LinkedHashMap<Short, ry1> linkedHashMap, LinkedHashMap<Short, ry1> linkedHashMap2, a aVar, zm1[] zm1Arr, ry1 ry14, String str8, ry1 ry15, String str9) {
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = str6;
        this.i = str7;
        this.j = ry1;
        this.k = ry12;
        this.l = ry13;
        this.m = linkedHashMap;
        this.n = linkedHashMap2;
        this.o = aVar;
        this.p = zm1Arr;
        this.q = ry14;
        this.r = str8;
        this.s = ry15;
        this.t = str9;
        this.b = al1.Companion.a(str6, str3);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zk1(String str, String str2, String str3, String str4, String str5, String str6, String str7, ry1 ry1, ry1 ry12, ry1 ry13, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, a aVar, zm1[] zm1Arr, ry1 ry14, String str8, ry1 ry15, String str9, int i2) {
        this(str, str2, str3, (i2 & 8) != 0 ? "" : str4, (i2 & 16) != 0 ? "" : str5, (i2 & 32) != 0 ? "" : str6, (i2 & 64) != 0 ? "" : str7, (i2 & 128) != 0 ? new ry1(0, 0) : ry1, (i2 & 256) != 0 ? new ry1(0, 0) : ry12, (i2 & 512) != 0 ? new ry1(0, 0) : ry13, (i2 & 1024) != 0 ? new LinkedHashMap() : linkedHashMap, (i2 & 2048) != 0 ? new LinkedHashMap() : linkedHashMap2, (i2 & 4096) != 0 ? a.NO_REQUIRE : aVar, (i2 & 8192) != 0 ? new zm1[0] : zm1Arr, (i2 & 16384) != 0 ? new ry1(0, 0) : ry14, (32768 & i2) != 0 ? "en_US" : str8, (65536 & i2) != 0 ? new ry1(0, 0) : ry15, (131072 & i2) != 0 ? "" : str9);
    }

    @DexIgnore
    public static /* synthetic */ zk1 a(zk1 zk1, String str, String str2, String str3, String str4, String str5, String str6, String str7, ry1 ry1, ry1 ry12, ry1 ry13, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, a aVar, zm1[] zm1Arr, ry1 ry14, String str8, ry1 ry15, String str9, int i2) {
        return zk1.a((i2 & 1) != 0 ? zk1.c : str, (i2 & 2) != 0 ? zk1.d : str2, (i2 & 4) != 0 ? zk1.e : str3, (i2 & 8) != 0 ? zk1.f : str4, (i2 & 16) != 0 ? zk1.g : str5, (i2 & 32) != 0 ? zk1.h : str6, (i2 & 64) != 0 ? zk1.i : str7, (i2 & 128) != 0 ? zk1.j : ry1, (i2 & 256) != 0 ? zk1.k : ry12, (i2 & 512) != 0 ? zk1.l : ry13, (i2 & 1024) != 0 ? zk1.m : linkedHashMap, (i2 & 2048) != 0 ? zk1.n : linkedHashMap2, (i2 & 4096) != 0 ? zk1.o : aVar, (i2 & 8192) != 0 ? zk1.p : zm1Arr, (i2 & 16384) != 0 ? zk1.q : ry14, (32768 & i2) != 0 ? zk1.r : str8, (65536 & i2) != 0 ? zk1.s : ry15, (131072 & i2) != 0 ? zk1.t : str9);
    }

    @DexIgnore
    public final a a() {
        return this.o;
    }

    @DexIgnore
    public final zk1 a(String str, String str2, String str3, String str4, String str5, String str6, String str7, ry1 ry1, ry1 ry12, ry1 ry13, LinkedHashMap<Short, ry1> linkedHashMap, LinkedHashMap<Short, ry1> linkedHashMap2, a aVar, zm1[] zm1Arr, ry1 ry14, String str8, ry1 ry15, String str9) {
        return new zk1(str, str2, str3, str4, str5, str6, str7, ry1, ry12, ry13, linkedHashMap, linkedHashMap2, aVar, zm1Arr, ry14, str8, ry15, str9);
    }

    @DexIgnore
    public final JSONArray a(HashMap<Short, ry1> hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry<Short, ry1> entry : hashMap.entrySet()) {
            jSONArray.put(g80.k(g80.k(g80.k(new JSONObject(), jd0.A0, hy1.l(entry.getKey().shortValue(), null, 1, null)), jd0.g4, kb.e.a(entry.getKey().shortValue())), jd0.K2, entry.getValue().toString()));
        }
        return jSONArray;
    }

    @DexIgnore
    public final void a(al1 al1) {
        this.b = al1;
    }

    @DexIgnore
    public final ry1 b() {
        return this.j;
    }

    @DexIgnore
    public final LinkedHashMap<Short, ry1> c() {
        return this.n;
    }

    @DexIgnore
    public final ry1 d() {
        return this.q;
    }

    @DexIgnore
    public final ry1 e() {
        return this.l;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(zk1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            zk1 zk1 = (zk1) obj;
            return !(pq7.a(this.c, zk1.c) ^ true) && !(pq7.a(this.d, zk1.d) ^ true) && !(pq7.a(this.e, zk1.e) ^ true) && !(pq7.a(this.f, zk1.f) ^ true) && !(pq7.a(this.g, zk1.g) ^ true) && !(pq7.a(this.h, zk1.h) ^ true) && !(pq7.a(this.i, zk1.i) ^ true) && !(pq7.a(this.j, zk1.j) ^ true) && !(pq7.a(this.k, zk1.k) ^ true) && !(pq7.a(this.l, zk1.l) ^ true) && !(pq7.a(this.m, zk1.m) ^ true) && !(pq7.a(this.n, zk1.n) ^ true) && this.o == zk1.o && Arrays.equals(this.p, zk1.p) && !(pq7.a(this.q, zk1.q) ^ true) && !(pq7.a(this.r, zk1.r) ^ true) && !(pq7.a(this.s, zk1.s) ^ true) && !(pq7.a(this.t, zk1.t) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.DeviceInformation");
    }

    @DexIgnore
    public final String f() {
        return this.i;
    }

    @DexIgnore
    public final zm1[] g() {
        return this.p;
    }

    @DexIgnore
    public final al1 getDeviceType() {
        return this.b;
    }

    @DexIgnore
    public final dl1 getELabelVersions() {
        ry1 ry1 = this.n.get((short) 1797);
        ry1 d2 = ry1 != null ? ry1 : hd0.y.d();
        ry1 ry12 = this.m.get((short) 1797);
        if (ry12 == null) {
            ry12 = hd0.y.d();
        }
        return new dl1(d2, ry12);
    }

    @DexIgnore
    public final String getFastPairIdInHexString() {
        return this.t;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.g;
    }

    @DexIgnore
    public final String getHardwareRevision() {
        return this.f;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.r;
    }

    @DexIgnore
    public final dl1 getLocaleVersions() {
        ry1 ry1 = this.n.get((short) 1794);
        ry1 d2 = ry1 != null ? ry1 : hd0.y.d();
        ry1 ry12 = this.m.get(Short.valueOf(ob.ASSET.b));
        if (ry12 == null) {
            ry12 = hd0.y.d();
        }
        return new dl1(d2, ry12);
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.d;
    }

    @DexIgnore
    public final ry1 getMicroAppVersion() {
        return this.s;
    }

    @DexIgnore
    public final String getModelNumber() {
        return this.h;
    }

    @DexIgnore
    public final String getName() {
        return this.c;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.e;
    }

    @DexIgnore
    public final ry1 getUiPackageOSVersion() {
        ry1 ry1 = this.m.get(Short.valueOf(ob.UI_PACKAGE_FILE.b));
        return ry1 != null ? ry1 : new ry1(0, 0);
    }

    @DexIgnore
    public final dl1 getWatchParameterVersions() {
        ry1 ry1 = this.n.get(Short.valueOf(ob.WATCH_PARAMETERS_FILE.b));
        ry1 d2 = ry1 != null ? ry1 : hd0.y.d();
        ry1 ry12 = this.m.get(Short.valueOf(ob.WATCH_PARAMETERS_FILE.b));
        if (ry12 == null) {
            ry12 = hd0.y.d();
        }
        return new dl1(d2, ry12);
    }

    @DexIgnore
    public final LinkedHashMap<Short, ry1> h() {
        return this.m;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = this.l.hashCode();
        int hashCode11 = this.m.hashCode();
        int hashCode12 = this.n.hashCode();
        int hashCode13 = this.o.hashCode();
        int hashCode14 = Arrays.hashCode(this.p);
        int hashCode15 = this.q.hashCode();
        int hashCode16 = this.r.hashCode();
        return (((((((((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + hashCode15) * 31) + hashCode16) * 31) + this.s.hashCode()) * 31) + this.t.hashCode();
    }

    @DexIgnore
    public final ry1 i() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        boolean z = true;
        JSONObject k2 = g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.H, this.c), jd0.L2, ey1.a(this.b)), jd0.k0, this.d), jd0.l0, this.e), jd0.m0, this.f), jd0.n0, this.g), jd0.o0, this.h), jd0.p0, this.i), jd0.q0, this.j.getShortDescription()), jd0.r0, this.k.getShortDescription()), jd0.s0, this.l.getShortDescription()), jd0.I2, a(this.m)), jd0.A4, a(this.n));
        jd0 jd0 = jd0.s3;
        int i2 = o70.f2639a[this.o.ordinal()];
        if (i2 == 1) {
            z = false;
        } else if (i2 != 2) {
            throw new al7();
        }
        return g80.k(g80.k(g80.k(g80.k(g80.k(g80.k(k2, jd0, Boolean.valueOf(z)), jd0.t0, g80.h(this.p)), jd0.v2, this.q.getShortDescription()), jd0.m3, this.r), jd0.t3, this.s.getShortDescription()), jd0.u3, this.t);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e2 = e.e("DeviceInformation(name=");
        e2.append(this.c);
        e2.append(", macAddress=");
        e2.append(this.d);
        e2.append(", serialNumber=");
        e2.append(this.e);
        e2.append(", hardwareRevision=");
        e2.append(this.f);
        e2.append(", firmwareVersion=");
        e2.append(this.g);
        e2.append(", modelNumber=");
        e2.append(this.h);
        e2.append(", heartRateSerialNumber=");
        e2.append(this.i);
        e2.append(", bootloaderVersion=");
        e2.append(this.j);
        e2.append(", watchAppVersion=");
        e2.append(this.k);
        e2.append(", fontVersion=");
        e2.append(this.l);
        e2.append(", supportedFilesVersion=");
        e2.append(this.m);
        e2.append(", currentFilesVersion=");
        e2.append(this.n);
        e2.append(", bondRequired=");
        e2.append(this.o);
        e2.append(", supportedDeviceConfigKeys=");
        e2.append(Arrays.toString(this.p));
        e2.append(", deviceSecurityVersion=");
        e2.append(this.q);
        e2.append(", localeString=");
        e2.append(this.r);
        e2.append(", microAppVersion=");
        e2.append(this.s);
        e2.append(", fastPairIdInHexString=");
        e2.append(this.t);
        e2.append(")");
        return e2.toString();
    }
}
