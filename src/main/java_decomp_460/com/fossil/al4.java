package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al4 extends hl4 {
    @DexIgnore
    public static volatile al4[] f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f286a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public al4() {
        c();
    }

    @DexIgnore
    public static al4[] d() {
        if (f == null) {
            synchronized (fl4.f1149a) {
                if (f == null) {
                    f = new al4[0];
                }
            }
        }
        return f;
    }

    @DexIgnore
    @Override // com.fossil.hl4
    public /* bridge */ /* synthetic */ hl4 b(el4 el4) throws IOException {
        e(el4);
        return this;
    }

    @DexIgnore
    public al4 c() {
        this.f286a = "";
        this.b = "";
        this.c = jl4.f1775a;
        this.d = "";
        this.e = "";
        return this;
    }

    @DexIgnore
    public al4 e(el4 el4) throws IOException {
        while (true) {
            int q = el4.q();
            if (q == 0) {
                break;
            } else if (q == 10) {
                this.f286a = el4.p();
            } else if (q == 18) {
                this.b = el4.p();
            } else if (q == 26) {
                int a2 = jl4.a(el4, 26);
                String[] strArr = this.c;
                int length = strArr == null ? 0 : strArr.length;
                int i = a2 + length;
                String[] strArr2 = new String[i];
                if (length != 0) {
                    System.arraycopy(this.c, 0, strArr2, 0, length);
                }
                while (length < i - 1) {
                    strArr2[length] = el4.p();
                    el4.q();
                    length++;
                }
                strArr2[length] = el4.p();
                this.c = strArr2;
            } else if (q == 34) {
                this.d = el4.p();
            } else if (q == 42) {
                this.e = el4.p();
            } else if (q == 48) {
                el4.h();
            } else if (!jl4.e(el4, q)) {
                break;
            }
        }
        return this;
    }
}
