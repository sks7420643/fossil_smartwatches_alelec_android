package com.fossil;

import android.content.Context;
import android.util.Log;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s42 extends ys0<Void> implements t72 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Semaphore f3203a; // = new Semaphore(0);
    @DexIgnore
    public Set<r62> b;

    @DexIgnore
    public s42(Context context, Set<r62> set) {
        super(context);
        this.b = set;
    }

    @DexIgnore
    /* renamed from: a */
    public final Void loadInBackground() {
        int i = 0;
        for (r62 r62 : this.b) {
            if (r62.o(this)) {
                i++;
            }
        }
        try {
            this.f3203a.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t72
    public final void onComplete() {
        this.f3203a.release();
    }

    @DexIgnore
    @Override // com.fossil.at0
    public final void onStartLoading() {
        this.f3203a.drainPermits();
        forceLoad();
    }
}
