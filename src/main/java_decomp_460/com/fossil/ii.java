package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii {
    @DexIgnore
    public /* synthetic */ ii(kq7 kq7) {
    }

    @DexIgnore
    public final JSONObject a(long j) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("push", new JSONObject().put("set", new JSONObject().put("buddyChallengeApp._.config.step_diff", j)));
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }
}
