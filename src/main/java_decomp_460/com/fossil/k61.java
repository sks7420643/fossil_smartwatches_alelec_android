package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k61 extends d61 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ k48 f1865a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ q51 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k61(k48 k48, String str, q51 q51) {
        super(null);
        pq7.c(k48, "source");
        pq7.c(q51, "dataSource");
        this.f1865a = k48;
        this.b = str;
        this.c = q51;
    }

    @DexIgnore
    public final q51 a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final k48 c() {
        return this.f1865a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof k61) {
                k61 k61 = (k61) obj;
                if (!pq7.a(this.f1865a, k61.f1865a) || !pq7.a(this.b, k61.b) || !pq7.a(this.c, k61.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        k48 k48 = this.f1865a;
        int hashCode = k48 != null ? k48.hashCode() : 0;
        String str = this.b;
        int hashCode2 = str != null ? str.hashCode() : 0;
        q51 q51 = this.c;
        if (q51 != null) {
            i = q51.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "SourceResult(source=" + this.f1865a + ", mimeType=" + this.b + ", dataSource=" + this.c + ")";
    }
}
