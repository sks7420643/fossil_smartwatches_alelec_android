package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import com.fossil.af1;
import com.fossil.wb1;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pf1<DataT> implements af1<Uri, DataT> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2823a;
    @DexIgnore
    public /* final */ af1<File, DataT> b;
    @DexIgnore
    public /* final */ af1<Uri, DataT> c;
    @DexIgnore
    public /* final */ Class<DataT> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<DataT> implements bf1<Uri, DataT> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f2824a;
        @DexIgnore
        public /* final */ Class<DataT> b;

        @DexIgnore
        public a(Context context, Class<DataT> cls) {
            this.f2824a = context;
            this.b = cls;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public final af1<Uri, DataT> b(ef1 ef1) {
            return new pf1(this.f2824a, ef1.d(File.class, this.b), ef1.d(Uri.class, this.b), this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a<ParcelFileDescriptor> {
        @DexIgnore
        public b(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends a<InputStream> {
        @DexIgnore
        public c(Context context) {
            super(context, InputStream.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<DataT> implements wb1<DataT> {
        @DexIgnore
        public static /* final */ String[] l; // = {"_data"};
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ af1<File, DataT> c;
        @DexIgnore
        public /* final */ af1<Uri, DataT> d;
        @DexIgnore
        public /* final */ Uri e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ ob1 h;
        @DexIgnore
        public /* final */ Class<DataT> i;
        @DexIgnore
        public volatile boolean j;
        @DexIgnore
        public volatile wb1<DataT> k;

        @DexIgnore
        public d(Context context, af1<File, DataT> af1, af1<Uri, DataT> af12, Uri uri, int i2, int i3, ob1 ob1, Class<DataT> cls) {
            this.b = context.getApplicationContext();
            this.c = af1;
            this.d = af12;
            this.e = uri;
            this.f = i2;
            this.g = i3;
            this.h = ob1;
            this.i = cls;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
            wb1<DataT> wb1 = this.k;
            if (wb1 != null) {
                wb1.a();
            }
        }

        @DexIgnore
        public final af1.a<DataT> b() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.c.b(g(this.e), this.f, this.g, this.h);
            }
            return this.d.b(f() ? MediaStore.setRequireOriginal(this.e) : this.e, this.f, this.g, this.h);
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
            this.j = true;
            wb1<DataT> wb1 = this.k;
            if (wb1 != null) {
                wb1.cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super DataT> aVar) {
            try {
                wb1<DataT> e2 = e();
                if (e2 == null) {
                    aVar.b(new IllegalArgumentException("Failed to build fetcher for: " + this.e));
                    return;
                }
                this.k = e2;
                if (this.j) {
                    cancel();
                } else {
                    e2.d(sa1, aVar);
                }
            } catch (FileNotFoundException e3) {
                aVar.b(e3);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: com.fossil.wb1<Data>, com.fossil.wb1<DataT> */
        public final wb1<DataT> e() throws FileNotFoundException {
            af1.a<DataT> b2 = b();
            if (b2 != null) {
                return (wb1<Data>) b2.c;
            }
            return null;
        }

        @DexIgnore
        public final boolean f() {
            return this.b.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.io.File g(android.net.Uri r8) throws java.io.FileNotFoundException {
            /*
                r7 = this;
                r6 = 0
                android.content.Context r0 = r7.b     // Catch:{ all -> 0x0069 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ all -> 0x0069 }
                java.lang.String[] r2 = com.fossil.pf1.d.l     // Catch:{ all -> 0x0069 }
                r3 = 0
                r4 = 0
                r5 = 0
                r1 = r8
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0069 }
                if (r1 == 0) goto L_0x0052
                boolean r0 = r1.moveToFirst()     // Catch:{ all -> 0x004b }
                if (r0 == 0) goto L_0x0052
                java.lang.String r0 = "_data"
                int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x004b }
                java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x004b }
                boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x004b }
                if (r2 != 0) goto L_0x0034
                java.io.File r2 = new java.io.File     // Catch:{ all -> 0x004b }
                r2.<init>(r0)     // Catch:{ all -> 0x004b }
                if (r1 == 0) goto L_0x0033
                r1.close()
            L_0x0033:
                return r2
            L_0x0034:
                java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "File path was empty in media store for: "
                r2.append(r3)
                r2.append(r8)
                java.lang.String r2 = r2.toString()
                r0.<init>(r2)
                throw r0
            L_0x004b:
                r0 = move-exception
            L_0x004c:
                if (r1 == 0) goto L_0x0051
                r1.close()
            L_0x0051:
                throw r0
            L_0x0052:
                java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Failed to media store entry for: "
                r2.append(r3)
                r2.append(r8)
                java.lang.String r2 = r2.toString()
                r0.<init>(r2)
                throw r0
            L_0x0069:
                r0 = move-exception
                r1 = r6
                goto L_0x004c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pf1.d.g(android.net.Uri):java.io.File");
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<DataT> getDataClass() {
            return this.i;
        }
    }

    @DexIgnore
    public pf1(Context context, af1<File, DataT> af1, af1<Uri, DataT> af12, Class<DataT> cls) {
        this.f2823a = context.getApplicationContext();
        this.b = af1;
        this.c = af12;
        this.d = cls;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<DataT> b(Uri uri, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(uri), new d(this.f2823a, this.b, this.c, uri, i, i2, ob1, this.d));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return Build.VERSION.SDK_INT >= 29 && jc1.b(uri);
    }
}
