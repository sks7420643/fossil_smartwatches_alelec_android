package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mr0<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f2411a;
    @DexIgnore
    public /* final */ LiveData<T> b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable e; // = new b();
    @DexIgnore
    public /* final */ Runnable f; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LiveData<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void j() {
            mr0 mr0 = mr0.this;
            mr0.f2411a.execute(mr0.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r4v4, resolved type: androidx.lifecycle.LiveData<T> */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            boolean z;
            do {
                if (mr0.this.d.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (mr0.this.c.compareAndSet(true, false)) {
                        try {
                            obj = mr0.this.a();
                            z = true;
                        } finally {
                            mr0.this.d.set(false);
                        }
                    }
                    if (z) {
                        mr0.this.b.l(obj);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (mr0.this.c.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            boolean g = mr0.this.b.g();
            if (mr0.this.c.compareAndSet(false, true) && g) {
                mr0 mr0 = mr0.this;
                mr0.f2411a.execute(mr0.e);
            }
        }
    }

    @DexIgnore
    public mr0(Executor executor) {
        this.f2411a = executor;
        this.b = new a();
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public LiveData<T> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        bi0.f().b(this.f);
    }
}
