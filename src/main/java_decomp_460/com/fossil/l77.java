package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum l77 {
    BACKGROUND_TEMPLATE(0),
    BACKGROUND_PHOTO(1),
    STICKER(2),
    RING_COMPLICATION(3);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public l77(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }
}
