package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<List<oo5>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<oo5>> {
    }

    @DexIgnore
    public final String a(List<oo5> list) {
        try {
            return new Gson().u(list, new a().getType());
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final List<oo5> b(String str) {
        List<oo5> list;
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            list = (List) new Gson().l(str, new b().getType());
        } catch (Exception e) {
            list = null;
        }
        return list;
    }
}
