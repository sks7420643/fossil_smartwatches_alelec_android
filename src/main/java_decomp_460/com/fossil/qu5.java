package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu5 implements Factory<pu5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ qu5 f3028a; // = new qu5();
    }

    @DexIgnore
    public static qu5 a() {
        return a.f3028a;
    }

    @DexIgnore
    public static pu5 c() {
        return new pu5();
    }

    @DexIgnore
    /* renamed from: b */
    public pu5 get() {
        return c();
    }
}
