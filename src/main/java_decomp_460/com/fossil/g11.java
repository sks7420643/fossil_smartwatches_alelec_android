package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"AddedAbstractMethod"})
public abstract class g11 {
    @DexIgnore
    public static g11 e(Context context) {
        return s11.l(context);
    }

    @DexIgnore
    public static void f(Context context, o01 o01) {
        s11.f(context, o01);
    }

    @DexIgnore
    public final a11 a(h11 h11) {
        return b(Collections.singletonList(h11));
    }

    @DexIgnore
    public abstract a11 b(List<? extends h11> list);

    @DexIgnore
    public a11 c(String str, s01 s01, z01 z01) {
        return d(str, s01, Collections.singletonList(z01));
    }

    @DexIgnore
    public abstract a11 d(String str, s01 s01, List<z01> list);
}
