package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct0 {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    public static ct0 g;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f647a;
    @DexIgnore
    public /* final */ HashMap<BroadcastReceiver, ArrayList<c>> b; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, ArrayList<c>> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ ArrayList<b> d; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Handler e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Handler {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message message) {
            if (message.what != 1) {
                super.handleMessage(message);
            } else {
                ct0.this.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Intent f649a;
        @DexIgnore
        public /* final */ ArrayList<c> b;

        @DexIgnore
        public b(Intent intent, ArrayList<c> arrayList) {
            this.f649a = intent;
            this.b = arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ IntentFilter f650a;
        @DexIgnore
        public /* final */ BroadcastReceiver b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public c(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.f650a = intentFilter;
            this.b = broadcastReceiver;
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.b);
            sb.append(" filter=");
            sb.append(this.f650a);
            if (this.d) {
                sb.append(" DEAD");
            }
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore
    public ct0(Context context) {
        this.f647a = context;
        this.e = new a(context.getMainLooper());
    }

    @DexIgnore
    public static ct0 b(Context context) {
        ct0 ct0;
        synchronized (f) {
            if (g == null) {
                g = new ct0(context.getApplicationContext());
            }
            ct0 = g;
        }
        return ct0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r1 >= r4) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r6 = r5[r1];
        r7 = r6.b.size();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        if (r3 >= r7) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
        r0 = r6.b.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        if (r0.d != false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        r0.b.onReceive(r10.f647a, r6.f649a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003e, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        r1 = r1 + 1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r10 = this;
            r2 = 0
        L_0x0001:
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<com.fossil.ct0$c>> r1 = r10.b
            monitor-enter(r1)
            java.util.ArrayList<com.fossil.ct0$b> r0 = r10.d     // Catch:{ all -> 0x0046 }
            int r4 = r0.size()     // Catch:{ all -> 0x0046 }
            if (r4 > 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            return
        L_0x000e:
            com.fossil.ct0$b[] r5 = new com.fossil.ct0.b[r4]     // Catch:{ all -> 0x0046 }
            java.util.ArrayList<com.fossil.ct0$b> r0 = r10.d     // Catch:{ all -> 0x0046 }
            r0.toArray(r5)     // Catch:{ all -> 0x0046 }
            java.util.ArrayList<com.fossil.ct0$b> r0 = r10.d     // Catch:{ all -> 0x0046 }
            r0.clear()     // Catch:{ all -> 0x0046 }
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            r1 = r2
        L_0x001c:
            if (r1 >= r4) goto L_0x0001
            r6 = r5[r1]
            java.util.ArrayList<com.fossil.ct0$c> r0 = r6.b
            int r7 = r0.size()
            r3 = r2
        L_0x0027:
            if (r3 >= r7) goto L_0x0042
            java.util.ArrayList<com.fossil.ct0$c> r0 = r6.b
            java.lang.Object r0 = r0.get(r3)
            com.fossil.ct0$c r0 = (com.fossil.ct0.c) r0
            boolean r8 = r0.d
            if (r8 != 0) goto L_0x003e
            android.content.BroadcastReceiver r0 = r0.b
            android.content.Context r8 = r10.f647a
            android.content.Intent r9 = r6.f649a
            r0.onReceive(r8, r9)
        L_0x003e:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0027
        L_0x0042:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001c
        L_0x0046:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ct0.a():void");
    }

    @DexIgnore
    public void c(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.b) {
            c cVar = new c(intentFilter, broadcastReceiver);
            ArrayList<c> arrayList = this.b.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList<>(1);
                this.b.put(broadcastReceiver, arrayList);
            }
            arrayList.add(cVar);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                ArrayList<c> arrayList2 = this.c.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList<>(1);
                    this.c.put(action, arrayList2);
                }
                arrayList2.add(cVar);
            }
        }
    }

    @DexIgnore
    public boolean d(Intent intent) {
        ArrayList arrayList;
        synchronized (this.b) {
            String action = intent.getAction();
            String resolveTypeIfNeeded = intent.resolveTypeIfNeeded(this.f647a.getContentResolver());
            Uri data = intent.getData();
            String scheme = intent.getScheme();
            Set<String> categories = intent.getCategories();
            boolean z = (intent.getFlags() & 8) != 0;
            if (z) {
                Log.v("LocalBroadcastManager", "Resolving type " + resolveTypeIfNeeded + " scheme " + scheme + " of intent " + intent);
            }
            ArrayList<c> arrayList2 = this.c.get(intent.getAction());
            if (arrayList2 != null) {
                if (z) {
                    Log.v("LocalBroadcastManager", "Action list: " + arrayList2);
                }
                ArrayList arrayList3 = null;
                int i = 0;
                while (i < arrayList2.size()) {
                    c cVar = arrayList2.get(i);
                    if (z) {
                        Log.v("LocalBroadcastManager", "Matching against filter " + cVar.f650a);
                    }
                    if (cVar.c) {
                        if (z) {
                            Log.v("LocalBroadcastManager", "  Filter's target already added");
                        }
                        arrayList = arrayList3;
                    } else {
                        int match = cVar.f650a.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                        if (match >= 0) {
                            if (z) {
                                Log.v("LocalBroadcastManager", "  Filter matched!  match=0x" + Integer.toHexString(match));
                            }
                            arrayList = arrayList3 == null ? new ArrayList() : arrayList3;
                            arrayList.add(cVar);
                            cVar.c = true;
                        } else {
                            if (z) {
                                Log.v("LocalBroadcastManager", "  Filter did not match: " + (match != -4 ? match != -3 ? match != -2 ? match != -1 ? "unknown reason" : "type" : "data" : "action" : "category"));
                            }
                            arrayList = arrayList3;
                        }
                    }
                    i++;
                    arrayList3 = arrayList;
                }
                if (arrayList3 != null) {
                    for (int i2 = 0; i2 < arrayList3.size(); i2++) {
                        ((c) arrayList3.get(i2)).c = false;
                    }
                    this.d.add(new b(intent, arrayList3));
                    if (!this.e.hasMessages(1)) {
                        this.e.sendEmptyMessage(1);
                    }
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void e(BroadcastReceiver broadcastReceiver) {
        synchronized (this.b) {
            ArrayList<c> remove = this.b.remove(broadcastReceiver);
            if (remove != null) {
                for (int size = remove.size() - 1; size >= 0; size--) {
                    c cVar = remove.get(size);
                    cVar.d = true;
                    for (int i = 0; i < cVar.f650a.countActions(); i++) {
                        String action = cVar.f650a.getAction(i);
                        ArrayList<c> arrayList = this.c.get(action);
                        if (arrayList != null) {
                            for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                                c cVar2 = arrayList.get(size2);
                                if (cVar2.b == broadcastReceiver) {
                                    cVar2.d = true;
                                    arrayList.remove(size2);
                                }
                            }
                            if (arrayList.size() <= 0) {
                                this.c.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }
}
