package com.fossil;

import android.content.Context;
import android.view.View;
import java.io.File;
import okhttp3.Cache;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r81 {
    @DexIgnore
    public static final void a(View view) {
        pq7.c(view, "view");
        w81.i(view).a();
    }

    @DexIgnore
    public static final Cache b(Context context) {
        pq7.c(context, "context");
        File g = y81.f4257a.g(context);
        return new Cache(g, y81.f4257a.c(g));
    }
}
