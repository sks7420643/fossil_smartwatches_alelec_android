package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sc2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sc2> CREATOR; // = new ae2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Account c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ GoogleSignInAccount e;

    @DexIgnore
    public sc2(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.b = i;
        this.c = account;
        this.d = i2;
        this.e = googleSignInAccount;
    }

    @DexIgnore
    public sc2(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }

    @DexIgnore
    public int c() {
        return this.d;
    }

    @DexIgnore
    public GoogleSignInAccount f() {
        return this.e;
    }

    @DexIgnore
    public Account m() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.n(parcel, 1, this.b);
        bd2.t(parcel, 2, m(), i, false);
        bd2.n(parcel, 3, c());
        bd2.t(parcel, 4, f(), i, false);
        bd2.b(parcel, a2);
    }
}
