package com.fossil;

import android.os.Bundle;
import com.fossil.qf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pf7 extends df7 {
    @DexIgnore
    public qf7 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public pf7(Bundle bundle) {
        b(bundle);
    }

    @DexIgnore
    @Override // com.fossil.df7
    public boolean a() {
        qf7 qf7 = this.c;
        if (qf7 == null) {
            return false;
        }
        return qf7.a();
    }

    @DexIgnore
    @Override // com.fossil.df7
    public void b(Bundle bundle) {
        super.b(bundle);
        this.d = bundle.getString("_wxapi_showmessage_req_lang");
        this.e = bundle.getString("_wxapi_showmessage_req_country");
        this.c = qf7.a.a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.df7
    public int c() {
        return 4;
    }

    @DexIgnore
    @Override // com.fossil.df7
    public void d(Bundle bundle) {
        Bundle d2 = qf7.a.d(this.c);
        super.d(d2);
        bundle.putString("_wxapi_showmessage_req_lang", this.d);
        bundle.putString("_wxapi_showmessage_req_country", this.e);
        bundle.putAll(d2);
    }
}
