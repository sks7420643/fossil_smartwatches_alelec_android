package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qd0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ HashMap<String, pd0> f2962a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ qd0 b; // = new qd0();

    @DexIgnore
    public final long a(String str) {
        long min;
        synchronized (f2962a) {
            pd0 pd0 = f2962a.get(str);
            if (pd0 == null) {
                pd0 = new pd0(0, 128);
            }
            min = Math.min(128L, lr7.c(Math.pow((double) 2, (double) (pd0.f2816a + 1))));
            if (min < pd0.b) {
                pd0.f2816a++;
            }
            f2962a.put(str, pd0);
            m80.c.a("ExponentialBackOff", "getNextRate: key=%s, newRate=%d.", str, Long.valueOf(min));
        }
        return 1000 * min;
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (f2962a) {
            pd0 pd0 = f2962a.get(str);
            if (pd0 == null) {
                pd0 = new pd0(0, 128);
            }
            f2962a.put(str, pd0.a(0, pd0.b));
            tl7 tl7 = tl7.f3441a;
        }
    }
}
