package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b37 implements Factory<a37> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<v36> f392a;
    @DexIgnore
    public /* final */ Provider<d26> b;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<on5> f;

    @DexIgnore
    public b37(Provider<v36> provider, Provider<d26> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<on5> provider6) {
        this.f392a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static b37 a(Provider<v36> provider, Provider<d26> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<on5> provider6) {
        return new b37(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static a37 c(v36 v36, d26 d26, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, on5 on5) {
        return new a37(v36, d26, notificationSettingsDatabase, notificationsRepository, deviceRepository, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public a37 get() {
        return c(this.f392a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }
}
