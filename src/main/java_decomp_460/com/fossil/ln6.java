package com.fossil;

import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ln6 implements Factory<kn6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<WorkoutSessionRepository> f2225a;

    @DexIgnore
    public ln6(Provider<WorkoutSessionRepository> provider) {
        this.f2225a = provider;
    }

    @DexIgnore
    public static ln6 a(Provider<WorkoutSessionRepository> provider) {
        return new ln6(provider);
    }

    @DexIgnore
    public static kn6 c(WorkoutSessionRepository workoutSessionRepository) {
        return new kn6(workoutSessionRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public kn6 get() {
        return c(this.f2225a.get());
    }
}
