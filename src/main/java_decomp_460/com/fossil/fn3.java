package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn3 implements Callable<List<hr3>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ or3 f1157a;
    @DexIgnore
    public /* final */ /* synthetic */ qm3 b;

    @DexIgnore
    public fn3(qm3 qm3, or3 or3) {
        this.b = qm3;
        this.f1157a = or3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<hr3> call() throws Exception {
        this.b.b.d0();
        return this.b.b.U().G(this.f1157a.b);
    }
}
