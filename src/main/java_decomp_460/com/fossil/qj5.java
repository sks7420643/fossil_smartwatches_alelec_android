package com.fossil;

import com.portfolio.platform.data.model.InstalledApp;
import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qj5 implements mb1 {
    @DexIgnore
    public /* final */ InstalledApp b;

    @DexIgnore
    public qj5(InstalledApp installedApp) {
        pq7.c(installedApp, "installedApp");
        this.b = installedApp;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        pq7.c(messageDigest, "messageDigest");
        String identifier = this.b.getIdentifier();
        pq7.b(identifier, "installedApp.identifier");
        Charset charset = mb1.f2349a;
        pq7.b(charset, "Key.CHARSET");
        if (identifier != null) {
            byte[] bytes = identifier.getBytes(charset);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final InstalledApp c() {
        return this.b;
    }
}
