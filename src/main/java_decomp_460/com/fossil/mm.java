package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ af b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mm(af afVar) {
        super(1);
        this.b = afVar;
    }

    @DexIgnore
    public final void a(fs fsVar) {
        aw awVar = (aw) fsVar;
        af afVar = this.b;
        byte[] bArr = afVar.N;
        long j = awVar.N;
        byte[] k = dm7.k(bArr, (int) j, (int) Math.min(afVar.D, j + awVar.O));
        af afVar2 = this.b;
        long j2 = awVar.N;
        short s = afVar2.P;
        k5 k5Var = afVar2.w;
        lp.i(afVar2, new dw(s, new wr(k, Math.min(k5Var.m, k5Var.s), n6.FTD), afVar2.w), new uo(afVar2, k), new hp(afVar2), new ho(afVar2, k, j2), null, null, 48, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(fs fsVar) {
        a(fsVar);
        return tl7.f3441a;
    }
}
