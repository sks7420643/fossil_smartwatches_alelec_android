package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl1 extends nu1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public vs1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public rl1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    pq7.b(createByteArray, "parcel.createByteArray()!!");
                    Parcelable readParcelable = parcel.readParcelable(vs1.class.getClassLoader());
                    if (readParcelable != null) {
                        return new rl1(readString, createByteArray, (vs1) readParcelable);
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public rl1[] newArray(int i) {
            return new rl1[i];
        }
    }

    @DexIgnore
    public rl1(String str, byte[] bArr, vs1 vs1) {
        super(str, bArr);
        this.e = vs1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ rl1(String str, byte[] bArr, vs1 vs1, int i, kq7 kq7) {
        this(str, bArr, (i & 4) != 0 ? new vs1(0, 62, 0) : vs1);
    }

    @DexIgnore
    public final void a(vs1 vs1) {
        this.e = vs1;
    }

    @DexIgnore
    public final JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("image_name", c()).put("pos", this.e.toJSONObject());
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final vs1 getPositionConfig() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.nu1
    public JSONObject toJSONObject() {
        JSONObject put = super.toJSONObject().put("pos", this.e.toJSONObject());
        pq7.b(put, "super.toJSONObject()\n   \u2026ionConfig.toJSONObject())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.nu1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
