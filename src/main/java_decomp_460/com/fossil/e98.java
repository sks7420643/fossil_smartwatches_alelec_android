package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e98 implements e88<w18, Long> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ e98 f899a; // = new e98();

    @DexIgnore
    /* renamed from: b */
    public Long a(w18 w18) throws IOException {
        return Long.valueOf(w18.string());
    }
}
