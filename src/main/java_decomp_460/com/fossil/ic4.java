package com.fossil;

import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ic4 extends k84 implements hc4 {
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public ic4(String str, String str2, kb4 kb4, ib4 ib4, String str3) {
        super(str, str2, kb4, ib4);
        this.f = str3;
    }

    @DexIgnore
    public ic4(String str, String str2, kb4 kb4, String str3) {
        this(str, str2, kb4, ib4.POST, str3);
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public boolean b(cc4 cc4, boolean z) {
        if (z) {
            jb4 c = c();
            g(c, cc4);
            h(c, cc4.c);
            x74 f2 = x74.f();
            f2.b("Sending report to: " + e());
            try {
                lb4 b = c.b();
                int b2 = b.b();
                x74 f3 = x74.f();
                f3.b("Create report request ID: " + b.d("X-REQUEST-ID"));
                x74 f4 = x74.f();
                f4.b("Result was: " + b2);
                return n94.a(b2) == 0;
            } catch (IOException e) {
                x74.f().e("Create report HTTP request failed.", e);
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public final jb4 g(jb4 jb4, cc4 cc4) {
        jb4.d("X-CRASHLYTICS-GOOGLE-APP-ID", cc4.b);
        jb4.d("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        jb4.d("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
        for (Map.Entry<String, String> entry : cc4.c.a().entrySet()) {
            jb4.e(entry);
        }
        return jb4;
    }

    @DexIgnore
    public final jb4 h(jb4 jb4, ec4 ec4) {
        jb4.g("report[identifier]", ec4.b());
        if (ec4.d().length == 1) {
            x74.f().b("Adding single file " + ec4.e() + " to report " + ec4.b());
            jb4.h("report[file]", ec4.e(), "application/octet-stream", ec4.c());
        } else {
            File[] d = ec4.d();
            int i = 0;
            for (File file : d) {
                x74.f().b("Adding file " + file.getName() + " to report " + ec4.b());
                StringBuilder sb = new StringBuilder();
                sb.append("report[file");
                sb.append(i);
                sb.append("]");
                jb4.h(sb.toString(), file.getName(), "application/octet-stream", file);
                i++;
            }
        }
        return jb4;
    }
}
