package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts5 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f3456a;
    @DexIgnore
    public List<? extends us5> b;
    @DexIgnore
    public e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public TextView f3457a;
        @DexIgnore
        public /* final */ /* synthetic */ ts5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ts5$a$a")
        /* renamed from: com.fossil.ts5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0244a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0244a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<us5> h = this.b.b.h();
                    if (h != null) {
                        us5 us5 = h.get(adapterPosition);
                        e g = this.b.b.g();
                        if (g != null) {
                            g.a(us5.a(), this.b.b.i(), adapterPosition, us5.b(), null);
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<us5> h = this.b.b.h();
                if (h != null) {
                    us5 us5 = h.get(adapterPosition);
                    e g = this.b.b.g();
                    if (g == null) {
                        return true;
                    }
                    g.b(us5.a(), this.b.b.i(), adapterPosition, us5.b(), null);
                    return true;
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ts5 ts5, View view) {
            super(view);
            pq7.c(view, "itemView");
            this.b = ts5;
            view.setOnClickListener(new View$OnClickListenerC0244a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(2131363303);
            if (findViewById != null) {
                this.f3457a = (TextView) findViewById;
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.f3457a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public List<String> f3458a; // = new ArrayList();
        @DexIgnore
        public TextView b;
        @DexIgnore
        public Spinner c;
        @DexIgnore
        public Button d;
        @DexIgnore
        public /* final */ /* synthetic */ ts5 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<us5> h = this.b.e.h();
                    if (h != null) {
                        us5 us5 = h.get(adapterPosition);
                        e g = this.b.e.g();
                        if (g != null) {
                            String a2 = us5.a();
                            int i = this.b.e.i();
                            Object b2 = us5.b();
                            Bundle bundle = new Bundle();
                            bundle.putInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS", this.b.c.getSelectedItemPosition());
                            g.a(a2, i, adapterPosition, b2, bundle);
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ts5 ts5, View view) {
            super(view);
            pq7.c(view, "itemView");
            this.e = ts5;
            View findViewById = view.findViewById(2131363411);
            if (findViewById != null) {
                this.b = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363130);
                if (findViewById2 != null) {
                    this.c = (Spinner) findViewById2;
                    View findViewById3 = view.findViewById(2131361970);
                    if (findViewById3 != null) {
                        Button button = (Button) findViewById3;
                        button.setOnClickListener(new a(this));
                        this.d = button;
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                throw new il7("null cannot be cast to non-null type android.widget.Spinner");
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final Button b() {
            return this.d;
        }

        @DexIgnore
        public final TextView c() {
            return this.b;
        }

        @DexIgnore
        public final void d(List<String> list) {
            pq7.c(list, "value");
            this.f3458a = list;
            e();
        }

        @DexIgnore
        public final void e() {
            View view = this.itemView;
            pq7.b(view, "itemView");
            ArrayAdapter arrayAdapter = new ArrayAdapter(view.getContext(), 17367048, this.f3458a);
            arrayAdapter.setDropDownViewResource(17367049);
            this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public TextView f3459a;
        @DexIgnore
        public SwitchCompat b;
        @DexIgnore
        public /* final */ /* synthetic */ ts5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ SwitchCompat b;
            @DexIgnore
            public /* final */ /* synthetic */ c c;

            @DexIgnore
            public a(SwitchCompat switchCompat, c cVar) {
                this.b = switchCompat;
                this.c = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.c.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<us5> h = this.c.c.h();
                    if (h != null) {
                        us5 us5 = h.get(adapterPosition);
                        if (us5 != null) {
                            ((ws5) us5).g(this.b.isChecked());
                            e g = this.c.c.g();
                            if (g != null) {
                                String a2 = us5.a();
                                int i = this.c.c.i();
                                Object b2 = us5.b();
                                Bundle bundle = new Bundle();
                                List<us5> h2 = this.c.c.h();
                                if (h2 != null) {
                                    us5 us52 = h2.get(adapterPosition);
                                    if (us52 != null) {
                                        bundle.putBoolean("DEBUG_BUNDLE_IS_CHECKED", ((ws5) us52).f());
                                        g.a(a2, i, adapterPosition, b2, bundle);
                                    } else {
                                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                                    }
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                            this.c.c.notifyItemChanged(adapterPosition);
                            return;
                        }
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ts5 ts5, View view) {
            super(view);
            pq7.c(view, "itemView");
            this.c = ts5;
            View findViewById = view.findViewById(2131363304);
            if (findViewById != null) {
                this.f3459a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363074);
                if (findViewById2 != null) {
                    SwitchCompat switchCompat = (SwitchCompat) findViewById2;
                    switchCompat.setOnClickListener(new a(switchCompat, this));
                    this.b = switchCompat;
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final SwitchCompat a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.f3459a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public TextView f3460a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ ts5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public a(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<us5> h = this.b.c.h();
                    if (h != null) {
                        us5 us5 = h.get(adapterPosition);
                        e g = this.b.c.g();
                        if (g != null) {
                            g.a(us5.a(), this.b.c.i(), adapterPosition, us5.b(), null);
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public b(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<us5> h = this.b.c.h();
                if (h != null) {
                    us5 us5 = h.get(adapterPosition);
                    e g = this.b.c.g();
                    if (g == null) {
                        return true;
                    }
                    g.b(us5.a(), this.b.c.i(), adapterPosition, us5.b(), null);
                    return true;
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ts5 ts5, View view) {
            super(view);
            pq7.c(view, "itemView");
            this.c = ts5;
            view.setOnClickListener(new a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(2131363305);
            if (findViewById != null) {
                this.f3460a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363306);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.f3460a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore
    public final e g() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<? extends us5> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        List<? extends us5> list = this.b;
        if ((list != null ? (us5) list.get(i) : null) instanceof ws5) {
            return zs5.CHILD_ITEM_WITH_SWITCH.ordinal();
        }
        List<? extends us5> list2 = this.b;
        if ((list2 != null ? (us5) list2.get(i) : null) instanceof xs5) {
            return zs5.CHILD_ITEM_WITH_TEXT.ordinal();
        }
        List<? extends us5> list3 = this.b;
        return (list3 != null ? (us5) list3.get(i) : null) instanceof vs5 ? zs5.CHILD_ITEM_WITH_SPINNER.ordinal() : zs5.CHILD.ordinal();
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.us5>, java.util.List<com.fossil.us5> */
    public final List<us5> h() {
        return this.b;
    }

    @DexIgnore
    public final int i() {
        return this.f3456a;
    }

    @DexIgnore
    public final void j(List<? extends us5> list) {
        this.b = list;
    }

    @DexIgnore
    public final void k(e eVar) {
        pq7.c(eVar, "itemClickListener");
        this.c = eVar;
    }

    @DexIgnore
    public final void l(int i) {
        this.f3456a = i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        if (viewHolder instanceof a) {
            TextView a2 = ((a) viewHolder).a();
            List<? extends us5> list = this.b;
            if (list != null) {
                a2.setText(((us5) list.get(i)).d());
            } else {
                pq7.i();
                throw null;
            }
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            TextView b2 = cVar.b();
            List<? extends us5> list2 = this.b;
            if (list2 != null) {
                b2.setText(((us5) list2.get(i)).d());
                SwitchCompat a3 = cVar.a();
                List<? extends us5> list3 = this.b;
                if (list3 != null) {
                    Object obj = list3.get(i);
                    if (obj != null) {
                        a3.setChecked(((ws5) obj).f());
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        } else if (viewHolder instanceof d) {
            d dVar = (d) viewHolder;
            TextView b3 = dVar.b();
            List<? extends us5> list4 = this.b;
            if (list4 != null) {
                b3.setText(((us5) list4.get(i)).d());
                TextView a4 = dVar.a();
                List<? extends us5> list5 = this.b;
                if (list5 != null) {
                    Object obj2 = list5.get(i);
                    if (obj2 != null) {
                        a4.setText(((xs5) obj2).f());
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        } else if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            TextView c2 = bVar.c();
            List<? extends us5> list6 = this.b;
            if (list6 != null) {
                c2.setText(((us5) list6.get(i)).d());
                Button b4 = bVar.b();
                List<? extends us5> list7 = this.b;
                if (list7 != null) {
                    Object obj3 = list7.get(i);
                    if (obj3 != null) {
                        b4.setText(((vs5) obj3).f());
                        List<? extends us5> list8 = this.b;
                        if (list8 != null) {
                            Object obj4 = list8.get(i);
                            if (obj4 != null) {
                                bVar.d(((vs5) obj4).g());
                                return;
                            }
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                        }
                        pq7.i();
                        throw null;
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == zs5.CHILD.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558675, viewGroup, false);
            pq7.b(inflate, "view");
            return new a(this, inflate);
        } else if (i == zs5.CHILD_ITEM_WITH_SWITCH.ordinal()) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558677, viewGroup, false);
            pq7.b(inflate2, "view");
            return new c(this, inflate2);
        } else if (i == zs5.CHILD_ITEM_WITH_TEXT.ordinal()) {
            View inflate3 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558678, viewGroup, false);
            pq7.b(inflate3, "view");
            return new d(this, inflate3);
        } else if (i == zs5.CHILD_ITEM_WITH_SPINNER.ordinal()) {
            View inflate4 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558676, viewGroup, false);
            pq7.b(inflate4, "view");
            return new b(this, inflate4);
        } else {
            throw new IllegalArgumentException("viewType is not appropriate");
        }
    }
}
