package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ eu6 f4370a;

    @DexIgnore
    public yt6(eu6 eu6) {
        pq7.c(eu6, "view");
        i14.o(eu6, "view cannot be null!", new Object[0]);
        pq7.b(eu6, "checkNotNull(view, \"view cannot be null!\")");
        this.f4370a = eu6;
    }

    @DexIgnore
    public final eu6 a() {
        return this.f4370a;
    }
}
