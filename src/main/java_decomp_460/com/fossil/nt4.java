package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt4 {
    @rj4("step")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Integer f2566a;

    @DexIgnore
    public nt4() {
        this(null, 1, null);
    }

    @DexIgnore
    public nt4(Integer num) {
        this.f2566a = num;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ nt4(Integer num, int i, kq7 kq7) {
        this((i & 1) != 0 ? null : num);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof nt4) && pq7.a(this.f2566a, ((nt4) obj).f2566a));
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.f2566a;
        if (num != null) {
            return num.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "StepResult(step=" + this.f2566a + ")";
    }
}
