package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b21 implements n11 {
    @DexIgnore
    public static /* final */ String c; // = x01.f("SystemAlarmScheduler");
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public b21(Context context) {
        this.b = context.getApplicationContext();
    }

    @DexIgnore
    @Override // com.fossil.n11
    public void a(o31... o31Arr) {
        for (o31 o31 : o31Arr) {
            b(o31);
        }
    }

    @DexIgnore
    public final void b(o31 o31) {
        x01.c().a(c, String.format("Scheduling work with workSpecId %s", o31.f2626a), new Throwable[0]);
        this.b.startService(x11.f(this.b, o31.f2626a));
    }

    @DexIgnore
    @Override // com.fossil.n11
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.n11
    public void e(String str) {
        this.b.startService(x11.g(this.b, str));
    }
}
