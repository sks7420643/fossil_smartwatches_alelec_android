package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class da5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ RecyclerView u;

    @DexIgnore
    public da5(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, ImageView imageView, ConstraintLayout constraintLayout, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = imageView;
        this.t = constraintLayout;
        this.u = recyclerView;
    }
}
