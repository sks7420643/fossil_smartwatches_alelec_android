package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f23 implements n23 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public n23[] f1040a;

    @DexIgnore
    public f23(n23... n23Arr) {
        this.f1040a = n23Arr;
    }

    @DexIgnore
    @Override // com.fossil.n23
    public final boolean zza(Class<?> cls) {
        for (n23 n23 : this.f1040a) {
            if (n23.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.n23
    public final k23 zzb(Class<?> cls) {
        n23[] n23Arr = this.f1040a;
        for (n23 n23 : n23Arr) {
            if (n23.zza(cls)) {
                return n23.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
