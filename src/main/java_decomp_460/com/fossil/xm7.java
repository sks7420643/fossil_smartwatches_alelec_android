package com.fossil;

import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xm7 {
    @DexIgnore
    public static final <K, V> V a(Map<K, ? extends V> map, K k) {
        pq7.c(map, "$this$getOrImplicitDefault");
        if (map instanceof wm7) {
            return (V) ((wm7) map).d(k);
        }
        V v = (V) map.get(k);
        if (v != null || map.containsKey(k)) {
            return v;
        }
        throw new NoSuchElementException("Key " + ((Object) k) + " is missing in the map.");
    }
}
