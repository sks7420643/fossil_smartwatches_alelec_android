package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l41 implements k41 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a41 f2145a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Executor c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            l41.this.d(runnable);
        }
    }

    @DexIgnore
    public l41(Executor executor) {
        this.f2145a = new a41(executor);
    }

    @DexIgnore
    @Override // com.fossil.k41
    public Executor a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.k41
    public void b(Runnable runnable) {
        this.f2145a.execute(runnable);
    }

    @DexIgnore
    @Override // com.fossil.k41
    public a41 c() {
        return this.f2145a;
    }

    @DexIgnore
    public void d(Runnable runnable) {
        this.b.post(runnable);
    }
}
