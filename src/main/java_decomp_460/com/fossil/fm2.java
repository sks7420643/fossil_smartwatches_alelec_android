package com.fossil;

import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fm2 {
    @DexIgnore
    void a(dm2 dm2, Throwable th, Object obj);

    @DexIgnore
    Object b(dm2 dm2, Message message);

    @DexIgnore
    void c(dm2 dm2, Message message, Object obj);

    @DexIgnore
    void d(dm2 dm2, Message message, long j);
}
