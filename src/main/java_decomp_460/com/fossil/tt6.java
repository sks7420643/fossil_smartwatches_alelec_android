package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt6 implements Factory<st6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f3467a;

    @DexIgnore
    public tt6(Provider<ThemeRepository> provider) {
        this.f3467a = provider;
    }

    @DexIgnore
    public static tt6 a(Provider<ThemeRepository> provider) {
        return new tt6(provider);
    }

    @DexIgnore
    public static st6 c(ThemeRepository themeRepository) {
        return new st6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public st6 get() {
        return c(this.f3467a.get());
    }
}
