package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xb1<T> {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        xb1<T> a(T t);

        @DexIgnore
        Class<T> getDataClass();
    }

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    T b() throws IOException;
}
