package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fo5 implements MembersInjector<NotificationReceiver> {
    @DexIgnore
    public static void a(NotificationReceiver notificationReceiver, mj5 mj5) {
        notificationReceiver.b = mj5;
    }

    @DexIgnore
    public static void b(NotificationReceiver notificationReceiver, on5 on5) {
        notificationReceiver.f4696a = on5;
    }

    @DexIgnore
    public static void c(NotificationReceiver notificationReceiver, UserRepository userRepository) {
        notificationReceiver.c = userRepository;
    }
}
