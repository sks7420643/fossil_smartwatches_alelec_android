package com.fossil;

import com.fossil.fz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(bz1 bz1);

        @DexIgnore
        public abstract a b(b bVar);

        @DexIgnore
        public abstract lz1 c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class b extends Enum<b> {
        @DexIgnore
        public static /* final */ b zza; // = new b("UNKNOWN", 0, 0);
        @DexIgnore
        public static /* final */ b zzb; // = new b("ANDROID_FIREBASE", 1, 23);

        @DexIgnore
        public b(String str, int i, int i2) {
        }
    }

    @DexIgnore
    public static a a() {
        return new fz1.b();
    }

    @DexIgnore
    public abstract bz1 b();

    @DexIgnore
    public abstract b c();
}
