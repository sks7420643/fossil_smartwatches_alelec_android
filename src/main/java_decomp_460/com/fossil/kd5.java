package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.cardview.widget.CardView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kd5 extends jd5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362546, 1);
        x.put(2131362543, 2);
        x.put(2131362515, 3);
        x.put(2131363161, 4);
    }
    */

    @DexIgnore
    public kd5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 5, w, x));
    }

    @DexIgnore
    public kd5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (CardView) objArr[0], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[1], (FlexibleSwitchCompat) objArr[4]);
        this.v = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.v != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.v = 1;
        }
        w();
    }
}
