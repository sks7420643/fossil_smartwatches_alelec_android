package com.fossil;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface i42 {
    @DexIgnore
    l42 a(Intent intent);

    @DexIgnore
    t62<Status> b(r62 r62);
}
