package com.fossil;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Hashtable<ow, LinkedHashSet<lp>> f2583a; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Hashtable<ow, Integer> b;

    @DexIgnore
    public /* synthetic */ nw(Hashtable hashtable, kq7 kq7) {
        this.b = hashtable;
        Set<ow> keySet = this.b.keySet();
        pq7.b(keySet, "resourceQuotas.keys");
        Iterator<T> it = keySet.iterator();
        while (it.hasNext()) {
            this.f2583a.put(it.next(), new LinkedHashSet<>());
        }
    }

    @DexIgnore
    public final boolean a(lp lpVar) {
        T t;
        synchronized (this.f2583a) {
            synchronized (this.b) {
                m80.c.a("ResourcePool", "Before allocateResource for %s(%s), current resourceHolders=%s.", ey1.a(lpVar.y), lpVar.z, this.f2583a);
                Iterator<ow> it = lpVar.z().iterator();
                while (it.hasNext()) {
                    ow next = it.next();
                    LinkedHashSet<lp> linkedHashSet = this.f2583a.get(next);
                    LinkedHashSet<lp> linkedHashSet2 = linkedHashSet != null ? linkedHashSet : new LinkedHashSet<>();
                    Integer num = this.b.get(next);
                    if (num == null) {
                        num = 0;
                    }
                    pq7.b(num, "resourceQuotas[requiredResource] ?: 0");
                    int intValue = num.intValue();
                    Iterator<T> it2 = linkedHashSet2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it2.next();
                        if (t.u(lpVar)) {
                            break;
                        }
                    }
                    if (t == null) {
                        if (linkedHashSet2.size() < intValue) {
                            linkedHashSet2.add(lpVar);
                            this.f2583a.put(next, linkedHashSet2);
                        } else {
                            Set<Map.Entry<ow, LinkedHashSet<lp>>> entrySet = this.f2583a.entrySet();
                            pq7.b(entrySet, "resourceHolders.entries");
                            Iterator<T> it3 = entrySet.iterator();
                            while (it3.hasNext()) {
                                ((LinkedHashSet) it3.next().getValue()).remove(lpVar);
                            }
                            m80.c.a("ResourcePool", "After allocateResource for %s(%s), current resourceHolders=%s.", ey1.a(lpVar.y), lpVar.z, this.f2583a);
                            return false;
                        }
                    }
                }
                m80.c.a("ResourcePool", "After allocateResource for %s(%s), current resourceHolders=%s.", ey1.a(lpVar.y), lpVar.z, this.f2583a);
                return true;
            }
        }
    }

    @DexIgnore
    public final void b(lp lpVar) {
        synchronized (this.f2583a) {
            synchronized (this.b) {
                m80.c.a("ResourcePool", "Before releaseResource for %s(%s), current resourceHolders=%s.", ey1.a(lpVar.y), lpVar.z, this.f2583a);
                Iterator<ow> it = lpVar.z().iterator();
                while (it.hasNext()) {
                    LinkedHashSet<lp> linkedHashSet = this.f2583a.get(it.next());
                    if (linkedHashSet != null) {
                        linkedHashSet.remove(lpVar);
                    }
                }
                m80.c.a("ResourcePool", "After releaseResource for %s(%s), current resourceHolders=%s.", ey1.a(lpVar.y), lpVar.z, this.f2583a);
                tl7 tl7 = tl7.f3441a;
            }
            tl7 tl72 = tl7.f3441a;
        }
    }
}
