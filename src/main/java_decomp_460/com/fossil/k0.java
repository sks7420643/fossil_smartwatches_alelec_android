package com.fossil;

import com.portfolio.platform.data.model.Firmware;
import java.util.Arrays;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k0 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1848a;
    @DexIgnore
    @rj4("themeClass")
    public ec0 b;
    @DexIgnore
    @rj4("firmwareOSVersion")
    public ry1 c;
    @DexIgnore
    @rj4("checksum")
    public String d;
    @DexIgnore
    @rj4(Firmware.COLUMN_DOWNLOAD_URL)
    public String e;
    @DexIgnore
    @rj4("updatedAt")
    public Date f;
    @DexIgnore
    @rj4("createdAt")
    public Date g;
    @DexIgnore
    @rj4("data")
    public byte[] h;

    @DexIgnore
    public k0(String str, ec0 ec0, ry1 ry1, String str2, String str3, Date date, Date date2, byte[] bArr) {
        this.f1848a = str;
        this.b = ec0;
        this.c = ry1;
        this.d = str2;
        this.e = str3;
        this.f = date;
        this.g = date2;
        this.h = bArr;
    }

    @DexIgnore
    public final Date a() {
        return this.g;
    }

    @DexIgnore
    public final Date b() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k0)) {
            return false;
        }
        k0 k0Var = (k0) obj;
        if (!pq7.a(this.f1848a, k0Var.f1848a)) {
            return false;
        }
        if (this.b != k0Var.b) {
            return false;
        }
        if (!pq7.a(this.c, k0Var.c)) {
            return false;
        }
        return !(pq7.a(this.d, k0Var.d) ^ true);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f1848a.hashCode();
        int hashCode2 = this.b.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        StringBuilder e2 = e.e("ThemeTemplateEntity(id=");
        e2.append(this.f1848a);
        e2.append(", classifier=");
        e2.append(this.b);
        e2.append(", packageOSVersion=");
        e2.append(this.c);
        e2.append(", checksum=");
        e2.append(this.d);
        e2.append(", downloadUrl=");
        e2.append(this.e);
        e2.append(", updatedAt=");
        e2.append(this.f);
        e2.append(", createdAt=");
        e2.append(this.g);
        e2.append(", data=");
        e2.append(Arrays.toString(this.h));
        e2.append(")");
        return e2.toString();
    }
}
