package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q04 extends c04 {
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ RectF D;
    @DexIgnore
    public int E;

    @DexIgnore
    public q04() {
        this(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q04(g04 g04) {
        super(g04 == null ? new g04() : g04);
        this.C = new Paint(1);
        r0();
        this.D = new RectF();
    }

    @DexIgnore
    @Override // com.fossil.c04
    public void draw(Canvas canvas) {
        m0(canvas);
        super.draw(canvas);
        canvas.drawRect(this.D, this.C);
        l0(canvas);
    }

    @DexIgnore
    public boolean k0() {
        return !this.D.isEmpty();
    }

    @DexIgnore
    public final void l0(Canvas canvas) {
        if (!s0(getCallback())) {
            canvas.restoreToCount(this.E);
        }
    }

    @DexIgnore
    public final void m0(Canvas canvas) {
        Drawable.Callback callback = getCallback();
        if (s0(callback)) {
            View view = (View) callback;
            if (view.getLayerType() != 2) {
                view.setLayerType(2, null);
                return;
            }
            return;
        }
        o0(canvas);
    }

    @DexIgnore
    public void n0() {
        p0(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void o0(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.E = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), null);
        } else {
            this.E = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), null, 31);
        }
    }

    @DexIgnore
    public void p0(float f, float f2, float f3, float f4) {
        RectF rectF = this.D;
        if (f != rectF.left || f2 != rectF.top || f3 != rectF.right || f4 != rectF.bottom) {
            this.D.set(f, f2, f3, f4);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void q0(RectF rectF) {
        p0(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    @DexIgnore
    public final void r0() {
        this.C.setStyle(Paint.Style.FILL_AND_STROKE);
        this.C.setColor(-1);
        this.C.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    @DexIgnore
    public final boolean s0(Drawable.Callback callback) {
        return callback instanceof View;
    }
}
