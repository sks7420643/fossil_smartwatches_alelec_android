package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum do1 {
    ACCEPT_PHONE_CALL((byte) 0),
    REJECT_PHONE_CALL((byte) 1),
    DISMISS_NOTIFICATION((byte) 2),
    REPLY_MESSAGE((byte) 3);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final do1 a(byte b) {
            do1[] values = do1.values();
            for (do1 do1 : values) {
                if (do1.a() == b) {
                    return do1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public do1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
