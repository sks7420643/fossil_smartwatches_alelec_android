package com.fossil;

import com.fossil.e13;
import com.fossil.yu2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu2 extends e13<wu2, a> implements o23 {
    @DexIgnore
    public static /* final */ wu2 zzi;
    @DexIgnore
    public static volatile z23<wu2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public m13<yu2> zzd; // = e13.B();
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public long zzg;
    @DexIgnore
    public int zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<wu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(wu2.zzi);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a B(yu2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).O((yu2) ((e13) aVar.h()));
            return this;
        }

        @DexIgnore
        public final a C(yu2 yu2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).O(yu2);
            return this;
        }

        @DexIgnore
        public final a E(Iterable<? extends yu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).P(iterable);
            return this;
        }

        @DexIgnore
        public final a G(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).Q(str);
            return this;
        }

        @DexIgnore
        public final yu2 H(int i) {
            return ((wu2) this.c).C(i);
        }

        @DexIgnore
        public final List<yu2> I() {
            return Collections.unmodifiableList(((wu2) this.c).D());
        }

        @DexIgnore
        public final int J() {
            return ((wu2) this.c).R();
        }

        @DexIgnore
        public final a K(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).S(i);
            return this;
        }

        @DexIgnore
        public final a M(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).T(j);
            return this;
        }

        @DexIgnore
        public final a N() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).f0();
            return this;
        }

        @DexIgnore
        public final String O() {
            return ((wu2) this.c).V();
        }

        @DexIgnore
        public final long P() {
            return ((wu2) this.c).X();
        }

        @DexIgnore
        public final long Q() {
            return ((wu2) this.c).Z();
        }

        @DexIgnore
        public final a x(int i, yu2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).E(i, (yu2) ((e13) aVar.h()));
            return this;
        }

        @DexIgnore
        public final a y(int i, yu2 yu2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).E(i, yu2);
            return this;
        }

        @DexIgnore
        public final a z(long j) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((wu2) this.c).G(j);
            return this;
        }
    }

    /*
    static {
        wu2 wu2 = new wu2();
        zzi = wu2;
        e13.u(wu2.class, wu2);
    }
    */

    @DexIgnore
    public static a c0() {
        return (a) zzi.w();
    }

    @DexIgnore
    public final yu2 C(int i) {
        return this.zzd.get(i);
    }

    @DexIgnore
    public final List<yu2> D() {
        return this.zzd;
    }

    @DexIgnore
    public final void E(int i, yu2 yu2) {
        yu2.getClass();
        e0();
        this.zzd.set(i, yu2);
    }

    @DexIgnore
    public final void G(long j) {
        this.zzc |= 2;
        this.zzf = j;
    }

    @DexIgnore
    public final void O(yu2 yu2) {
        yu2.getClass();
        e0();
        this.zzd.add(yu2);
    }

    @DexIgnore
    public final void P(Iterable<? extends yu2> iterable) {
        e0();
        nz2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final void Q(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zze = str;
    }

    @DexIgnore
    public final int R() {
        return this.zzd.size();
    }

    @DexIgnore
    public final void S(int i) {
        e0();
        this.zzd.remove(i);
    }

    @DexIgnore
    public final void T(long j) {
        this.zzc |= 4;
        this.zzg = j;
    }

    @DexIgnore
    public final String V() {
        return this.zze;
    }

    @DexIgnore
    public final boolean W() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long X() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean Y() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long Z() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean a0() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int b0() {
        return this.zzh;
    }

    @DexIgnore
    public final void e0() {
        m13<yu2> m13 = this.zzd;
        if (!m13.zza()) {
            this.zzd = e13.q(m13);
        }
    }

    @DexIgnore
    public final void f0() {
        this.zzd = e13.B();
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new wu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002\u1008\u0000\u0003\u1002\u0001\u0004\u1002\u0002\u0005\u1004\u0003", new Object[]{"zzc", "zzd", yu2.class, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                z23<wu2> z232 = zzj;
                if (z232 != null) {
                    return z232;
                }
                synchronized (wu2.class) {
                    try {
                        z23 = zzj;
                        if (z23 == null) {
                            z23 = new e13.c(zzi);
                            zzj = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
