package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w43 implements t43 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3880a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.sdk.collection.validate_param_names_alphabetical", true);

    @DexIgnore
    @Override // com.fossil.t43
    public final boolean zza() {
        return f3880a.o().booleanValue();
    }
}
