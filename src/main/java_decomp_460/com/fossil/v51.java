package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ g51 f3717a;

    @DexIgnore
    public v51(g51 g51) {
        pq7.c(g51, "bitmapPool");
        this.f3717a = g51;
    }

    @DexIgnore
    public final Bitmap a(Drawable drawable, f81 f81, Bitmap.Config config) {
        pq7.c(drawable, ResourceManager.DRAWABLE);
        pq7.c(f81, "size");
        pq7.c(config, "config");
        Bitmap.Config q = w81.q(config);
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            pq7.b(bitmap, "bitmap");
            if (w81.q(bitmap.getConfig()) == q) {
                return bitmap;
            }
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (intrinsicWidth <= 0) {
            intrinsicWidth = 512;
        }
        if (intrinsicHeight <= 0) {
            intrinsicHeight = 512;
        }
        if (f81 instanceof c81) {
            c81 c81 = (c81) f81;
            double c = t51.c(intrinsicWidth, intrinsicHeight, c81.d(), c81.c(), e81.FIT);
            int a2 = lr7.a(((double) intrinsicWidth) * c);
            int a3 = lr7.a(c * ((double) intrinsicHeight));
            Rect bounds = drawable.getBounds();
            int i = bounds.left;
            int i2 = bounds.top;
            int i3 = bounds.right;
            int i4 = bounds.bottom;
            Bitmap c2 = this.f3717a.c(a2, a3, q);
            drawable.setBounds(0, 0, a2, a3);
            drawable.draw(new Canvas(c2));
            drawable.setBounds(i, i2, i3, i4);
            return c2;
        }
        throw new al7();
    }
}
