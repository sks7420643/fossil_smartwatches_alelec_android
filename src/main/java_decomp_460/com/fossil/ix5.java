package com.fossil;

import android.database.Cursor;
import android.widget.Filter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix5 extends Filter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public a f1692a;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        Cursor b();

        @DexIgnore
        CharSequence d(Cursor cursor);

        @DexIgnore
        Cursor e(CharSequence charSequence);
    }

    @DexIgnore
    public ix5(a aVar) {
        pq7.c(aVar, "mClient");
        this.f1692a = aVar;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        pq7.c(obj, "resultValue");
        return this.f1692a.d((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        pq7.c(charSequence, "constraint");
        Cursor e = this.f1692a.e(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (e != null) {
            filterResults.count = e.getCount();
            filterResults.values = e;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        pq7.c(charSequence, "constraint");
        pq7.c(filterResults, "results");
        Cursor b = this.f1692a.b();
        Object obj = filterResults.values;
        if (obj != null && (!pq7.a(obj, b))) {
            a aVar = this.f1692a;
            Object obj2 = filterResults.values;
            if (obj2 != null) {
                aVar.a((Cursor) obj2);
                return;
            }
            throw new il7("null cannot be cast to non-null type android.database.Cursor");
        }
    }
}
