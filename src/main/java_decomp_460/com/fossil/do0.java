package com.fossil;

import android.view.View;
import android.view.ViewParent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class do0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ViewParent f814a;
    @DexIgnore
    public ViewParent b;
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int[] e;

    @DexIgnore
    public do0(View view) {
        this.c = view;
    }

    @DexIgnore
    public boolean a(float f, float f2, boolean z) {
        ViewParent i;
        if (!m() || (i = i(0)) == null) {
            return false;
        }
        return qo0.a(i, this.c, f, f2, z);
    }

    @DexIgnore
    public boolean b(float f, float f2) {
        ViewParent i;
        if (!m() || (i = i(0)) == null) {
            return false;
        }
        return qo0.b(i, this.c, f, f2);
    }

    @DexIgnore
    public boolean c(int i, int i2, int[] iArr, int[] iArr2) {
        return d(i, i2, iArr, iArr2, 0);
    }

    @DexIgnore
    public boolean d(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        boolean z;
        int i4;
        int i5;
        if (m()) {
            ViewParent i6 = i(i3);
            if (i6 == null) {
                return false;
            }
            if (i != 0 || i2 != 0) {
                if (iArr2 != null) {
                    this.c.getLocationInWindow(iArr2);
                    int i7 = iArr2[0];
                    i4 = iArr2[1];
                    i5 = i7;
                } else {
                    i4 = 0;
                    i5 = 0;
                }
                int[] j = iArr == null ? j() : iArr;
                j[0] = 0;
                j[1] = 0;
                qo0.c(i6, this.c, i, i2, j, i3);
                if (iArr2 != null) {
                    this.c.getLocationInWindow(iArr2);
                    iArr2[0] = iArr2[0] - i5;
                    iArr2[1] = iArr2[1] - i4;
                }
                if (!(j[0] == 0 && j[1] == 0)) {
                    z = true;
                    return z;
                }
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
                z = false;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public void e(int i, int i2, int i3, int i4, int[] iArr, int i5, int[] iArr2) {
        h(i, i2, i3, i4, iArr, i5, iArr2);
    }

    @DexIgnore
    public boolean f(int i, int i2, int i3, int i4, int[] iArr) {
        return h(i, i2, i3, i4, iArr, 0, null);
    }

    @DexIgnore
    public boolean g(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        return h(i, i2, i3, i4, iArr, i5, null);
    }

    @DexIgnore
    public final boolean h(int i, int i2, int i3, int i4, int[] iArr, int i5, int[] iArr2) {
        ViewParent i6;
        int i7;
        int i8;
        int[] iArr3;
        if (!m() || (i6 = i(i5)) == null) {
            return false;
        }
        if (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) {
            if (iArr != null) {
                iArr[0] = 0;
                iArr[1] = 0;
            }
            return false;
        }
        if (iArr != null) {
            this.c.getLocationInWindow(iArr);
            int i9 = iArr[0];
            i7 = iArr[1];
            i8 = i9;
        } else {
            i7 = 0;
            i8 = 0;
        }
        if (iArr2 == null) {
            iArr3 = j();
            iArr3[0] = 0;
            iArr3[1] = 0;
        } else {
            iArr3 = iArr2;
        }
        qo0.d(i6, this.c, i, i2, i3, i4, i5, iArr3);
        if (iArr != null) {
            this.c.getLocationInWindow(iArr);
            iArr[0] = iArr[0] - i8;
            iArr[1] = iArr[1] - i7;
        }
        return true;
    }

    @DexIgnore
    public final ViewParent i(int i) {
        if (i == 0) {
            return this.f814a;
        }
        if (i != 1) {
            return null;
        }
        return this.b;
    }

    @DexIgnore
    public final int[] j() {
        if (this.e == null) {
            this.e = new int[2];
        }
        return this.e;
    }

    @DexIgnore
    public boolean k() {
        return l(0);
    }

    @DexIgnore
    public boolean l(int i) {
        return i(i) != null;
    }

    @DexIgnore
    public boolean m() {
        return this.d;
    }

    @DexIgnore
    public void n(boolean z) {
        if (this.d) {
            mo0.F0(this.c);
        }
        this.d = z;
    }

    @DexIgnore
    public final void o(int i, ViewParent viewParent) {
        if (i == 0) {
            this.f814a = viewParent;
        } else if (i == 1) {
            this.b = viewParent;
        }
    }

    @DexIgnore
    public boolean p(int i) {
        return q(i, 0);
    }

    @DexIgnore
    public boolean q(int i, int i2) {
        if (l(i2)) {
            return true;
        }
        if (m()) {
            View view = this.c;
            for (ViewParent parent = this.c.getParent(); parent != null; parent = parent.getParent()) {
                if (qo0.f(parent, view, this.c, i, i2)) {
                    o(i2, parent);
                    qo0.e(parent, view, this.c, i, i2);
                    return true;
                }
                if (parent instanceof View) {
                    view = (View) parent;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public void r() {
        s(0);
    }

    @DexIgnore
    public void s(int i) {
        ViewParent i2 = i(i);
        if (i2 != null) {
            qo0.g(i2, this.c, i);
            o(i, null);
        }
    }
}
