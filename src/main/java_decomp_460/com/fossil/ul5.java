package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ul5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3611a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public ck5 c;
    @DexIgnore
    public Map<String, String> d; // = new HashMap();
    @DexIgnore
    public Map<String, Object> e; // = new HashMap();
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public a h;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object T4();  // void declaration

        @DexIgnore
        Object f2();  // void declaration
    }

    @DexIgnore
    public ul5(ck5 ck5, String str, String str2) {
        pq7.c(ck5, "analyticsHelper");
        pq7.c(str, "traceName");
        this.c = ck5;
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (str2 != null) {
            valueOf = str2 + LocaleConverter.LOCALE_DELIMITER + valueOf;
        }
        this.f3611a = valueOf;
        this.b = str;
    }

    @DexIgnore
    public final void a(sl5 sl5) {
        pq7.c(sl5, "analyticsEvent");
        sl5.a("trace_id", this.f3611a);
        sl5.b();
    }

    @DexIgnore
    public final ul5 b(String str, String str2) {
        pq7.c(str, "paramName");
        pq7.c(str2, "paramValue");
        Map<String, String> map = this.d;
        if (map != null) {
            map.put(str, str2);
        }
        return this;
    }

    @DexIgnore
    public final void c(String str) {
        pq7.c(str, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.g;
            Map<String, ? extends Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.f3611a);
                map.put("duration", Integer.valueOf((int) (currentTimeMillis - j)));
                map.put(Constants.RESULT, str);
                ck5 ck5 = this.c;
                if (ck5 != null) {
                    ck5.l(this.b + "_end", map);
                }
            }
        }
        a aVar = this.h;
        if (aVar != null) {
            aVar.T4();
        }
    }

    @DexIgnore
    public final void d(String str, boolean z, String str2) {
        pq7.c(str, "serial");
        pq7.c(str2, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.g;
            Map<String, ? extends Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.f3611a);
                map.put("duration", Integer.valueOf((int) (currentTimeMillis - j)));
                map.put("cache_hit", Integer.valueOf(z ? 1 : 0));
                map.put("Serial_Number", str);
                map.put(Constants.RESULT, str2);
                ck5 ck5 = this.c;
                if (ck5 != null) {
                    ck5.l(this.b + "_end", map);
                }
            }
        }
        a aVar = this.h;
        if (aVar != null) {
            aVar.T4();
        }
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public final boolean f() {
        return this.f;
    }

    @DexIgnore
    public final void g() {
        this.h = null;
    }

    @DexIgnore
    public final ul5 h(a aVar) {
        pq7.c(aVar, "tracingCallback");
        this.h = aVar;
        return this;
    }

    @DexIgnore
    public final void i() {
        if (this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is already started");
            return;
        }
        this.f = true;
        Map<String, String> map = this.d;
        if (map != null) {
            map.put("trace_id", this.f3611a);
        }
        ck5 ck5 = this.c;
        if (ck5 != null) {
            ck5.l(this.b + "_start", this.d);
        }
        Map<String, String> map2 = this.d;
        if (map2 != null) {
            map2.clear();
        }
        this.g = System.currentTimeMillis();
        a aVar = this.h;
        if (aVar != null) {
            aVar.f2();
        }
    }

    @DexIgnore
    public String toString() {
        return "Tracer name: " + this.b + ", id: " + this.f3611a + ", running: " + this.f;
    }
}
