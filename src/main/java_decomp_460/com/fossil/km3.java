package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km3 implements Thread.UncaughtExceptionHandler {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1931a;
    @DexIgnore
    public /* final */ /* synthetic */ im3 b;

    @DexIgnore
    public km3(im3 im3, String str) {
        this.b = im3;
        rc2.k(str);
        this.f1931a = str;
    }

    @DexIgnore
    public final void uncaughtException(Thread thread, Throwable th) {
        synchronized (this) {
            this.b.d().F().b(this.f1931a, th);
        }
    }
}
