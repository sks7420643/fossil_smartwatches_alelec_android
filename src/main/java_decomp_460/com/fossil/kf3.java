package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kf3 implements Parcelable.Creator<we3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ we3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        LatLngBounds latLngBounds = null;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                latLng4 = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 3) {
                latLng3 = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 4) {
                latLng2 = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 5) {
                latLng = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l != 6) {
                ad2.B(parcel, t);
            } else {
                latLngBounds = (LatLngBounds) ad2.e(parcel, t, LatLngBounds.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new we3(latLng4, latLng3, latLng2, latLng, latLngBounds);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ we3[] newArray(int i) {
        return new we3[i];
    }
}
