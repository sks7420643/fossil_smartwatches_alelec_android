package com.fossil;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import com.fossil.rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ed7 extends bd7 {
    @DexIgnore
    public ed7(Context context) {
        super(context);
    }

    @DexIgnore
    public static int k(Uri uri) throws IOException {
        int attributeInt = new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1);
        if (attributeInt == 3) {
            return 180;
        }
        if (attributeInt != 6) {
            return attributeInt != 8 ? 0 : 270;
        }
        return 90;
    }

    @DexIgnore
    @Override // com.fossil.bd7, com.fossil.rd7
    public boolean c(pd7 pd7) {
        return "file".equals(pd7.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.bd7, com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        return new rd7.a(null, j(pd7), Picasso.LoadedFrom.DISK, k(pd7.d));
    }
}
