package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi4 extends ri4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2390a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public mi4(String str, String str2) {
        if (str != null) {
            this.f2390a = str;
            if (str2 != null) {
                this.b = str2;
                return;
            }
            throw new NullPointerException("Null version");
        }
        throw new NullPointerException("Null libraryName");
    }

    @DexIgnore
    @Override // com.fossil.ri4
    public String b() {
        return this.f2390a;
    }

    @DexIgnore
    @Override // com.fossil.ri4
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ri4)) {
            return false;
        }
        ri4 ri4 = (ri4) obj;
        return this.f2390a.equals(ri4.b()) && this.b.equals(ri4.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f2390a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "LibraryVersion{libraryName=" + this.f2390a + ", version=" + this.b + "}";
    }
}
