package com.fossil;

import com.fossil.qb3;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg3 extends kc3 {
    @DexIgnore
    public /* final */ /* synthetic */ qb3.g b;

    @DexIgnore
    public bg3(qb3 qb3, qb3.g gVar) {
        this.b = gVar;
    }

    @DexIgnore
    @Override // com.fossil.jc3
    public final void onMapClick(LatLng latLng) {
        this.b.onMapClick(latLng);
    }
}
