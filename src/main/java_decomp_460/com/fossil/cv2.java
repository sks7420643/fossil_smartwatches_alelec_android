package com.fossil;

import com.fossil.e13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv2 extends e13<cv2, a> implements o23 {
    @DexIgnore
    public static /* final */ cv2 zzg;
    @DexIgnore
    public static volatile z23<cv2> zzh;
    @DexIgnore
    public j13 zzc; // = e13.z();
    @DexIgnore
    public j13 zzd; // = e13.z();
    @DexIgnore
    public m13<vu2> zze; // = e13.B();
    @DexIgnore
    public m13<dv2> zzf; // = e13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<cv2, a> implements o23 {
        @DexIgnore
        public a() {
            super(cv2.zzg);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a B() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).f0();
            return this;
        }

        @DexIgnore
        public final a C(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).X(i);
            return this;
        }

        @DexIgnore
        public final a E(Iterable<? extends Long> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).O(iterable);
            return this;
        }

        @DexIgnore
        public final a G(Iterable<? extends vu2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).R(iterable);
            return this;
        }

        @DexIgnore
        public final a H(Iterable<? extends dv2> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).V(iterable);
            return this;
        }

        @DexIgnore
        public final a x() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).e0();
            return this;
        }

        @DexIgnore
        public final a y(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).T(i);
            return this;
        }

        @DexIgnore
        public final a z(Iterable<? extends Long> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((cv2) this.c).I(iterable);
            return this;
        }
    }

    /*
    static {
        cv2 cv2 = new cv2();
        zzg = cv2;
        e13.u(cv2.class, cv2);
    }
    */

    @DexIgnore
    public static a b0() {
        return (a) zzg.w();
    }

    @DexIgnore
    public static cv2 c0() {
        return zzg;
    }

    @DexIgnore
    public final vu2 C(int i) {
        return this.zze.get(i);
    }

    @DexIgnore
    public final List<Long> D() {
        return this.zzc;
    }

    @DexIgnore
    public final void I(Iterable<? extends Long> iterable) {
        j13 j13 = this.zzc;
        if (!j13.zza()) {
            this.zzc = e13.p(j13);
        }
        nz2.a(iterable, this.zzc);
    }

    @DexIgnore
    public final int J() {
        return this.zzc.size();
    }

    @DexIgnore
    public final dv2 K(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final void O(Iterable<? extends Long> iterable) {
        j13 j13 = this.zzd;
        if (!j13.zza()) {
            this.zzd = e13.p(j13);
        }
        nz2.a(iterable, this.zzd);
    }

    @DexIgnore
    public final List<Long> P() {
        return this.zzd;
    }

    @DexIgnore
    public final void R(Iterable<? extends vu2> iterable) {
        g0();
        nz2.a(iterable, this.zze);
    }

    @DexIgnore
    public final int S() {
        return this.zzd.size();
    }

    @DexIgnore
    public final void T(int i) {
        g0();
        this.zze.remove(i);
    }

    @DexIgnore
    public final void V(Iterable<? extends dv2> iterable) {
        h0();
        nz2.a(iterable, this.zzf);
    }

    @DexIgnore
    public final List<vu2> W() {
        return this.zze;
    }

    @DexIgnore
    public final void X(int i) {
        h0();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final int Y() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<dv2> Z() {
        return this.zzf;
    }

    @DexIgnore
    public final int a0() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void e0() {
        this.zzc = e13.z();
    }

    @DexIgnore
    public final void f0() {
        this.zzd = e13.z();
    }

    @DexIgnore
    public final void g0() {
        m13<vu2> m13 = this.zze;
        if (!m13.zza()) {
            this.zze = e13.q(m13);
        }
    }

    @DexIgnore
    public final void h0() {
        m13<dv2> m13 = this.zzf;
        if (!m13.zza()) {
            this.zzf = e13.q(m13);
        }
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new cv2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzg, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zzc", "zzd", "zze", vu2.class, "zzf", dv2.class});
            case 4:
                return zzg;
            case 5:
                z23<cv2> z232 = zzh;
                if (z232 != null) {
                    return z232;
                }
                synchronized (cv2.class) {
                    try {
                        z23 = zzh;
                        if (z23 == null) {
                            z23 = new e13.c(zzg);
                            zzh = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
