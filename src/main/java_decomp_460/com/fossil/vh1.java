package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vh1<Z> implements th1<Z, Z> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ vh1<?> f3767a; // = new vh1<>();

    @DexIgnore
    public static <Z> th1<Z, Z> b() {
        return f3767a;
    }

    @DexIgnore
    @Override // com.fossil.th1
    public id1<Z> a(id1<Z> id1, ob1 ob1) {
        return id1;
    }
}
