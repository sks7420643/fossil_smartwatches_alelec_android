package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg1 implements qb1<ByteBuffer, Bitmap> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ xf1 f424a; // = new xf1();

    @DexIgnore
    /* renamed from: c */
    public id1<Bitmap> b(ByteBuffer byteBuffer, int i, int i2, ob1 ob1) throws IOException {
        return this.f424a.b(ImageDecoder.createSource(byteBuffer), i, i2, ob1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(ByteBuffer byteBuffer, ob1 ob1) throws IOException {
        return true;
    }
}
