package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru7 extends zw7<fx7> implements qu7 {
    @DexIgnore
    public /* final */ su7 f;

    @DexIgnore
    public ru7(fx7 fx7, su7 su7) {
        super(fx7);
        this.f = su7;
    }

    @DexIgnore
    @Override // com.fossil.qu7
    public boolean c(Throwable th) {
        return ((fx7) this.e).y(th);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        w(th);
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return "ChildHandle[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.zu7
    public void w(Throwable th) {
        this.f.m((nx7) this.e);
    }
}
