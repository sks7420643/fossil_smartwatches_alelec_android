package com.fossil;

import com.fossil.b88;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f88 extends b88.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f1082a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b88<Object, Call<?>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Type f1083a;
        @DexIgnore
        public /* final */ /* synthetic */ Executor b;

        @DexIgnore
        public a(f88 f88, Type type, Executor executor) {
            this.f1083a = type;
            this.b = executor;
        }

        @DexIgnore
        @Override // com.fossil.b88
        public Type a() {
            return this.f1083a;
        }

        @DexIgnore
        /* renamed from: c */
        public Call<Object> b(Call<Object> call) {
            Executor executor = this.b;
            return executor == null ? call : new b(executor, call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Call<T> {
        @DexIgnore
        public /* final */ Executor b;
        @DexIgnore
        public /* final */ Call<T> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements c88<T> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c88 f1084a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f88$b$a$a")
            /* renamed from: com.fossil.f88$b$a$a  reason: collision with other inner class name */
            public class RunnableC0080a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ q88 b;

                @DexIgnore
                public RunnableC0080a(q88 q88) {
                    this.b = q88;
                }

                @DexIgnore
                public void run() {
                    if (b.this.c.f()) {
                        a aVar = a.this;
                        aVar.f1084a.onFailure(b.this, new IOException("Canceled"));
                        return;
                    }
                    a aVar2 = a.this;
                    aVar2.f1084a.onResponse(b.this, this.b);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f88$b$a$b")
            /* renamed from: com.fossil.f88$b$a$b  reason: collision with other inner class name */
            public class RunnableC0081b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable b;

                @DexIgnore
                public RunnableC0081b(Throwable th) {
                    this.b = th;
                }

                @DexIgnore
                public void run() {
                    a aVar = a.this;
                    aVar.f1084a.onFailure(b.this, this.b);
                }
            }

            @DexIgnore
            public a(c88 c88) {
                this.f1084a = c88;
            }

            @DexIgnore
            @Override // com.fossil.c88
            public void onFailure(Call<T> call, Throwable th) {
                b.this.b.execute(new RunnableC0081b(th));
            }

            @DexIgnore
            @Override // com.fossil.c88
            public void onResponse(Call<T> call, q88<T> q88) {
                b.this.b.execute(new RunnableC0080a(q88));
            }
        }

        @DexIgnore
        public b(Executor executor, Call<T> call) {
            this.b = executor;
            this.c = call;
        }

        @DexIgnore
        @Override // retrofit2.Call
        public void D(c88<T> c88) {
            u88.b(c88, "callback == null");
            this.c.D(new a(c88));
        }

        @DexIgnore
        @Override // retrofit2.Call
        /* renamed from: L */
        public Call<T> clone() {
            return new b(this.b, this.c.L());
        }

        @DexIgnore
        @Override // retrofit2.Call
        public q88<T> a() throws IOException {
            return this.c.a();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public v18 c() {
            return this.c.c();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public void cancel() {
            this.c.cancel();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public boolean f() {
            return this.c.f();
        }
    }

    @DexIgnore
    public f88(Executor executor) {
        this.f1082a = executor;
    }

    @DexIgnore
    @Override // com.fossil.b88.a
    public b88<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        Executor executor = null;
        if (b88.a.c(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type h = u88.h(0, (ParameterizedType) type);
            if (!u88.m(annotationArr, s88.class)) {
                executor = this.f1082a;
            }
            return new a(this, h, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
