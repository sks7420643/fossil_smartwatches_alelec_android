package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es implements Parcelable.Creator<gs> {
    @DexIgnore
    public /* synthetic */ es(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public gs createFromParcel(Parcel parcel) {
        n6 n6Var = n6.values()[parcel.readInt()];
        long readLong = parcel.readLong();
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            pq7.b(createByteArray, "parcel.createByteArray()!!");
            return new gs(n6Var, readLong, createByteArray);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public gs[] newArray(int i) {
        return new gs[i];
    }
}
