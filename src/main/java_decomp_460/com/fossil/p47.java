package com.fossil;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import com.facebook.places.model.PlaceFields;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wk4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p47 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f2776a; // = "p47";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public a(View view) {
            this.b = view;
        }

        @DexIgnore
        public void run() {
            this.b.setEnabled(true);
        }
    }

    @DexIgnore
    public static float a(int i, Context context) {
        return ((float) i) * context.getResources().getDisplayMetrics().density;
    }

    @DexIgnore
    public static float b(float f) {
        return Resources.getSystem().getDisplayMetrics().density * f;
    }

    @DexIgnore
    public static String c(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str2 = str.split(",")[0];
        if (str2.length() <= 20) {
            return str2;
        }
        return str2.substring(0, 17) + "...";
    }

    @DexIgnore
    public static String d(String str) {
        TelephonyManager telephonyManager = (TelephonyManager) PortfolioApp.d0.getSystemService(PlaceFields.PHONE);
        wk4 n = wk4.n();
        if (telephonyManager == null) {
            return str;
        }
        try {
            return !TextUtils.isEmpty(str) ? n.i(n.H(str, e()), wk4.c.INTERNATIONAL) : str;
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    @DexIgnore
    public static String e() {
        TelephonyManager telephonyManager = (TelephonyManager) PortfolioApp.d0.getSystemService(PlaceFields.PHONE);
        if (telephonyManager != null) {
            try {
                if (telephonyManager.getNetworkCountryIso() != null && !TextUtils.isEmpty(telephonyManager.getNetworkCountryIso())) {
                    return telephonyManager.getNetworkCountryIso().toUpperCase();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return Locale.getDefault().getCountry().toUpperCase();
    }

    @DexIgnore
    public static q57 f(float f, float f2, float f3, float f4) {
        return f3 > f ? f4 > f2 ? q57.BottomRight : q57.TopRight : f4 > f2 ? q57.BottomLeft : q57.TopLeft;
    }

    @DexIgnore
    public static double g(float f, float f2, float f3, float f4) {
        return Math.atan((double) (Math.abs(f2 - f4) / Math.abs(f3 - f)));
    }

    @DexIgnore
    public static Point h(float f, float f2, float f3, float f4) {
        double g = g(f, f2, f3, f4);
        q57 f5 = f(f, f2, f3, f4);
        double radians = f5 == q57.TopLeft ? Math.toRadians(180.0d - Math.toDegrees(g)) : f5 == q57.BottomLeft ? Math.toRadians(Math.toDegrees(g) + 180.0d) : f5 == q57.BottomRight ? Math.toRadians(360.0d - Math.toDegrees(g)) : Math.toRadians(Math.toDegrees(g));
        double d = (double) 2000.0f;
        return new Point((int) (Math.cos(radians) * d), (int) (Math.sin(radians) * d));
    }

    @DexIgnore
    public static Boolean i(List<PhoneNumber> list, PhoneNumber phoneNumber) {
        if (!(list == null || phoneNumber == null)) {
            for (PhoneNumber phoneNumber2 : list) {
                if (phoneNumber2.getContact().getContactId() == phoneNumber.getContact().getContactId() && PhoneNumberUtils.compare(phoneNumber2.getNumber(), phoneNumber.getNumber())) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    @DexIgnore
    public static Boolean j(List<String> list, String str) {
        if (list != null) {
            for (String str2 : list) {
                if (PhoneNumberUtils.compare(str2, str)) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    @DexIgnore
    public static boolean k() {
        boolean z = true;
        if (Build.VERSION.SDK_INT >= 23) {
            NotificationManager notificationManager = (NotificationManager) PortfolioApp.d0.getSystemService("notification");
            if (notificationManager != null) {
                int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
                if (currentInterruptionFilter == 3 || currentInterruptionFilter == 4 || currentInterruptionFilter == 2) {
                    return true;
                }
                return currentInterruptionFilter == 0;
            }
        } else {
            try {
                if (Settings.Global.getInt(PortfolioApp.d0.getContentResolver(), "zen_mode") == 0) {
                    z = false;
                }
                return z;
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().d(f2776a, "isInDNDMode() - ex = " + e);
            }
        }
        return false;
    }

    @DexIgnore
    public static void l(View view) {
        if (view != null) {
            view.setEnabled(false);
            new Handler().postDelayed(new a(view), 500);
        }
    }

    @DexIgnore
    public static int m(float f) {
        return (int) TypedValue.applyDimension(2, f, Resources.getSystem().getDisplayMetrics());
    }

    @DexIgnore
    public static float n(float f) {
        return TypedValue.applyDimension(2, f, Resources.getSystem().getDisplayMetrics());
    }
}
