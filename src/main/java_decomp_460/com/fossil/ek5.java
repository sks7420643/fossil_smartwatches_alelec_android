package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek5 implements MembersInjector<dk5> {
    @DexIgnore
    public static void a(dk5 dk5, q27 q27) {
        dk5.d = q27;
    }

    @DexIgnore
    public static void b(dk5 dk5, DeviceRepository deviceRepository) {
        dk5.b = deviceRepository;
    }

    @DexIgnore
    public static void c(dk5 dk5, on5 on5) {
        dk5.f800a = on5;
    }

    @DexIgnore
    public static void d(dk5 dk5, UserRepository userRepository) {
        dk5.c = userRepository;
    }
}
