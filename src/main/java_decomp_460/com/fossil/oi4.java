package com.fossil;

import com.fossil.a74;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oi4 implements ti4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2685a;
    @DexIgnore
    public /* final */ pi4 b;

    @DexIgnore
    public oi4(Set<ri4> set, pi4 pi4) {
        this.f2685a = d(set);
        this.b = pi4;
    }

    @DexIgnore
    public static a74<ti4> b() {
        a74.b a2 = a74.a(ti4.class);
        a2.b(k74.h(ri4.class));
        a2.f(ni4.b());
        return a2.d();
    }

    @DexIgnore
    public static /* synthetic */ ti4 c(b74 b74) {
        return new oi4(b74.c(ri4.class), pi4.a());
    }

    @DexIgnore
    public static String d(Set<ri4> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<ri4> it = set.iterator();
        while (it.hasNext()) {
            ri4 next = it.next();
            sb.append(next.b());
            sb.append('/');
            sb.append(next.c());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.ti4
    public String a() {
        if (this.b.b().isEmpty()) {
            return this.f2685a;
        }
        return this.f2685a + ' ' + d(this.b.b());
    }
}
