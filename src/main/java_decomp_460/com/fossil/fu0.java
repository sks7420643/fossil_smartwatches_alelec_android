package com.fossil;

import com.fossil.zu0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fu0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends zu0.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ eu0 f1204a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ eu0 c;
        @DexIgnore
        public /* final */ /* synthetic */ zu0.d d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public a(eu0 eu0, int i, eu0 eu02, zu0.d dVar, int i2, int i3) {
            this.f1204a = eu0;
            this.b = i;
            this.c = eu02;
            this.d = dVar;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        @Override // com.fossil.zu0.b
        public boolean a(int i, int i2) {
            Object obj = this.f1204a.get(this.b + i);
            eu0 eu0 = this.c;
            Object obj2 = eu0.get(eu0.h() + i2);
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.zu0.b
        public boolean b(int i, int i2) {
            Object obj = this.f1204a.get(this.b + i);
            eu0 eu0 = this.c;
            Object obj2 = eu0.get(eu0.h() + i2);
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areItemsTheSame(obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.zu0.b
        public Object c(int i, int i2) {
            Object obj = this.f1204a.get(this.b + i);
            eu0 eu0 = this.c;
            Object obj2 = eu0.get(eu0.h() + i2);
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.zu0.b
        public int d() {
            return this.f;
        }

        @DexIgnore
        @Override // com.fossil.zu0.b
        public int e() {
            return this.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements jv0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1205a;
        @DexIgnore
        public /* final */ jv0 b;

        @DexIgnore
        public b(int i, jv0 jv0) {
            this.f1205a = i;
            this.b = jv0;
        }

        @DexIgnore
        @Override // com.fossil.jv0
        public void a(int i, int i2) {
            jv0 jv0 = this.b;
            int i3 = this.f1205a;
            jv0.a(i + i3, i3 + i2);
        }

        @DexIgnore
        @Override // com.fossil.jv0
        public void b(int i, int i2) {
            this.b.b(this.f1205a + i, i2);
        }

        @DexIgnore
        @Override // com.fossil.jv0
        public void c(int i, int i2) {
            this.b.c(this.f1205a + i, i2);
        }

        @DexIgnore
        @Override // com.fossil.jv0
        public void d(int i, int i2, Object obj) {
            this.b.d(this.f1205a + i, i2, obj);
        }
    }

    @DexIgnore
    public static <T> zu0.c a(eu0<T> eu0, eu0<T> eu02, zu0.d<T> dVar) {
        int d = eu0.d();
        return zu0.b(new a(eu0, d, eu02, dVar, (eu0.size() - d) - eu0.e(), (eu02.size() - eu02.d()) - eu02.e()), true);
    }

    @DexIgnore
    public static <T> void b(jv0 jv0, eu0<T> eu0, eu0<T> eu02, zu0.c cVar) {
        int e = eu0.e();
        int e2 = eu02.e();
        int d = eu0.d();
        int d2 = eu02.d();
        if (e == 0 && e2 == 0 && d == 0 && d2 == 0) {
            cVar.e(jv0);
            return;
        }
        if (e > e2) {
            int i = e - e2;
            jv0.c(eu0.size() - i, i);
        } else if (e < e2) {
            jv0.b(eu0.size(), e2 - e);
        }
        if (d > d2) {
            jv0.c(0, d - d2);
        } else if (d < d2) {
            jv0.b(0, d2 - d);
        }
        if (d2 != 0) {
            cVar.e(new b(d2, jv0));
        } else {
            cVar.e(jv0);
        }
    }

    @DexIgnore
    public static int c(zu0.c cVar, eu0 eu0, eu0 eu02, int i) {
        int d = eu0.d();
        int i2 = i - d;
        int size = eu0.size();
        int e = eu0.e();
        if (i2 >= 0 && i2 < (size - d) - e) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 % 2 == 1 ? -1 : 1) * (i3 / 2)) + i2;
                if (i4 >= 0 && i4 < eu0.n()) {
                    try {
                        int b2 = cVar.b(i4);
                        if (b2 != -1) {
                            return b2 + eu02.h();
                        }
                    } catch (IndexOutOfBoundsException e2) {
                    }
                }
            }
        }
        return Math.max(0, Math.min(i, eu02.size() - 1));
    }
}
