package com.fossil;

import android.content.Context;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb4 {
    @DexIgnore
    public static byte[] a(File file, Context context) throws IOException {
        BufferedReader bufferedReader;
        if (file == null || !file.exists()) {
            return new byte[0];
        }
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            try {
                byte[] a2 = new db4(context, new hb4()).a(bufferedReader);
                r84.f(bufferedReader);
                return a2;
            } catch (Throwable th) {
                th = th;
                r84.f(bufferedReader);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            r84.f(bufferedReader);
            throw th;
        }
    }
}
