package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f2542a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ kl3 d;

    @DexIgnore
    public nl3(kl3 kl3, int i, boolean z, boolean z2) {
        this.d = kl3;
        this.f2542a = i;
        this.b = z;
        this.c = z2;
    }

    @DexIgnore
    public final void a(String str) {
        this.d.A(this.f2542a, this.b, this.c, str, null, null, null);
    }

    @DexIgnore
    public final void b(String str, Object obj) {
        this.d.A(this.f2542a, this.b, this.c, str, obj, null, null);
    }

    @DexIgnore
    public final void c(String str, Object obj, Object obj2) {
        this.d.A(this.f2542a, this.b, this.c, str, obj, obj2, null);
    }

    @DexIgnore
    public final void d(String str, Object obj, Object obj2, Object obj3) {
        this.d.A(this.f2542a, this.b, this.c, str, obj, obj2, obj3);
    }
}
