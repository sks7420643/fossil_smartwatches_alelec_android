package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h4 extends qq7 implements gp7<tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ l4 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h4(l4 l4Var) {
        super(0);
        this.b = l4Var;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.gp7
    public tl7 invoke() {
        l4 l4Var = this.b;
        u5 poll = l4Var.f2144a.poll();
        if (poll != null) {
            l4Var.c(poll);
        } else {
            l4Var.b = null;
        }
        return tl7.f3441a;
    }
}
