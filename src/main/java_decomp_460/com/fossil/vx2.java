package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vx2<E> extends ax2<E> {
    @DexIgnore
    public /* final */ sx2<E> d;

    @DexIgnore
    public vx2(sx2<E> sx2, int i) {
        super(sx2.size(), i);
        this.d = sx2;
    }

    @DexIgnore
    @Override // com.fossil.ax2
    public final E a(int i) {
        return this.d.get(i);
    }
}
