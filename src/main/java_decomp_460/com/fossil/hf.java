package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hf extends lp {
    @DexIgnore
    public byte[] C;
    @DexIgnore
    public /* final */ ArrayList<ow> D; // = by1.a(this.i, hm7.c(ow.AUTHENTICATION));
    @DexIgnore
    public /* final */ rt E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public hf(k5 k5Var, i60 i60, rt rtVar, byte[] bArr) {
        super(k5Var, i60, yp.Q, null, false, 24);
        this.E = rtVar;
        this.F = bArr;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        byte[] bArr = this.F;
        if (bArr.length != 8) {
            k(zq.INVALID_PARAMETER);
        } else {
            lp.i(this, new ft(this.w, this.E, bArr), new qq(this), er.b, null, new sr(this), null, 40, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(g80.k(super.C(), jd0.u2, ey1.a(this.E)), jd0.o2, dy1.e(this.F, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        String str = null;
        JSONObject E2 = super.E();
        jd0 jd0 = jd0.q2;
        byte[] bArr = this.C;
        if (bArr != null) {
            str = dy1.e(bArr, null, 1, null);
        }
        return g80.k(E2, jd0, str);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        byte[] bArr = this.C;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.D;
    }
}
