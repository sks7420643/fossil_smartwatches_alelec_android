package com.fossil;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleCoroutineScopeImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs0 {
    @DexIgnore
    public static final yr0 a(Lifecycle lifecycle) {
        LifecycleCoroutineScopeImpl lifecycleCoroutineScopeImpl;
        pq7.c(lifecycle, "$this$coroutineScope");
        while (true) {
            lifecycleCoroutineScopeImpl = (LifecycleCoroutineScopeImpl) lifecycle.f74a.get();
            if (lifecycleCoroutineScopeImpl == null) {
                lifecycleCoroutineScopeImpl = new LifecycleCoroutineScopeImpl(lifecycle, ux7.b(null, 1, null).plus(bw7.c().S()));
                if (lifecycle.f74a.compareAndSet(null, lifecycleCoroutineScopeImpl)) {
                    lifecycleCoroutineScopeImpl.c();
                    break;
                }
            } else {
                break;
            }
        }
        return lifecycleCoroutineScopeImpl;
    }
}
