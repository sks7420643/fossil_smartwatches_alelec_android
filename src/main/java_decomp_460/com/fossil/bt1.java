package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt1 extends at1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public bt1 a(Parcel parcel) {
            return new bt1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bt1 createFromParcel(Parcel parcel) {
            return new bt1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bt1[] newArray(int i) {
            return new bt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ bt1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public bt1(boolean z) {
        super(g90.PERCENTAGE_CIRCLE, z);
    }

    @DexIgnore
    /* renamed from: isPercentageCircleEnable */
    public boolean a() {
        return super.a();
    }
}
