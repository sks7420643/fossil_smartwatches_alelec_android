package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ku1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<tt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public tt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(oq1.class.getClassLoader());
            if (readParcelable != null) {
                return new tt1((oq1) readParcelable, (nt1) parcel.readParcelable(nt1.class.getClassLoader()), ku1.values()[parcel.readInt()]);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public tt1[] newArray(int i) {
            return new tt1[i];
        }
    }

    @DexIgnore
    public tt1(ku1 ku1) {
        this(null, null, ku1);
    }

    @DexIgnore
    public tt1(oq1 oq1, ku1 ku1) {
        super(oq1, null);
        this.d = ku1;
    }

    @DexIgnore
    public tt1(oq1 oq1, nt1 nt1, ku1 ku1) {
        super(oq1, nt1);
        this.d = ku1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return g80.k(super.a(), jd0.n1, ey1.a(this.d));
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Constants.RING_MY_PHONE, new JSONObject().put(Constants.RESULT, ey1.a(this.d)));
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(tt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((tt1) obj).d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingPhoneData");
    }

    @DexIgnore
    public final ku1 getState() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
