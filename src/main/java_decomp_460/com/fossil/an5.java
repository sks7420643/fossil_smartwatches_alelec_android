package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an5 implements Factory<zm5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<PortfolioApp> f293a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> c;

    @DexIgnore
    public an5(Provider<PortfolioApp> provider, Provider<DeviceRepository> provider2, Provider<DianaAppSettingRepository> provider3) {
        this.f293a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static an5 a(Provider<PortfolioApp> provider, Provider<DeviceRepository> provider2, Provider<DianaAppSettingRepository> provider3) {
        return new an5(provider, provider2, provider3);
    }

    @DexIgnore
    public static zm5 c(PortfolioApp portfolioApp, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        return new zm5(portfolioApp, deviceRepository, dianaAppSettingRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public zm5 get() {
        return c(this.f293a.get(), this.b.get(), this.c.get());
    }
}
