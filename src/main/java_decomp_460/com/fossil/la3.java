package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<la3> CREATOR; // = new wa3();
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public la3(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.b = z;
        this.c = z2;
        this.d = z3;
        this.e = z4;
        this.f = z5;
        this.g = z6;
    }

    @DexIgnore
    public final boolean A() {
        return this.f;
    }

    @DexIgnore
    public final boolean D() {
        return this.c;
    }

    @DexIgnore
    public final boolean c() {
        return this.g;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final boolean h() {
        return this.e;
    }

    @DexIgnore
    public final boolean k() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.c(parcel, 1, k());
        bd2.c(parcel, 2, D());
        bd2.c(parcel, 3, f());
        bd2.c(parcel, 4, h());
        bd2.c(parcel, 5, A());
        bd2.c(parcel, 6, c());
        bd2.b(parcel, a2);
    }
}
