package com.fossil;

import com.fossil.k34;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l34<K, V> extends a34<K, V> {
    @DexIgnore
    @Deprecated
    public static <K, V> k34.b<K, V> builder() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <K, V> k34<K, V> of(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <K, V> k34<K, V> of(K k, V v, K k2, V v2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <K, V> k34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <K, V> k34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public static <K, V> k34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        throw new UnsupportedOperationException();
    }
}
