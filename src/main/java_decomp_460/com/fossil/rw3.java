package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw3 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131887235;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131887236;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131887237;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131887238;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131887239;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131887240;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131887241;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131887242;
    @DexIgnore
    public static /* final */ int abc_menu_alt_shortcut_label; // = 2131887243;
    @DexIgnore
    public static /* final */ int abc_menu_ctrl_shortcut_label; // = 2131887244;
    @DexIgnore
    public static /* final */ int abc_menu_delete_shortcut_label; // = 2131887245;
    @DexIgnore
    public static /* final */ int abc_menu_enter_shortcut_label; // = 2131887246;
    @DexIgnore
    public static /* final */ int abc_menu_function_shortcut_label; // = 2131887247;
    @DexIgnore
    public static /* final */ int abc_menu_meta_shortcut_label; // = 2131887248;
    @DexIgnore
    public static /* final */ int abc_menu_shift_shortcut_label; // = 2131887249;
    @DexIgnore
    public static /* final */ int abc_menu_space_shortcut_label; // = 2131887250;
    @DexIgnore
    public static /* final */ int abc_menu_sym_shortcut_label; // = 2131887251;
    @DexIgnore
    public static /* final */ int abc_prepend_shortcut_label; // = 2131887252;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131887253;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131887254;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131887255;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131887256;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131887257;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131887258;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131887259;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131887260;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131887261;
    @DexIgnore
    public static /* final */ int appbar_scrolling_view_behavior; // = 2131887275;
    @DexIgnore
    public static /* final */ int bottom_sheet_behavior; // = 2131887304;
    @DexIgnore
    public static /* final */ int character_counter_content_description; // = 2131887324;
    @DexIgnore
    public static /* final */ int character_counter_overflowed_content_description; // = 2131887325;
    @DexIgnore
    public static /* final */ int character_counter_pattern; // = 2131887326;
    @DexIgnore
    public static /* final */ int chip_text; // = 2131887332;
    @DexIgnore
    public static /* final */ int clear_text_end_icon_content_description; // = 2131887334;
    @DexIgnore
    public static /* final */ int error_icon_content_description; // = 2131887399;
    @DexIgnore
    public static /* final */ int exposed_dropdown_menu_content_description; // = 2131887400;
    @DexIgnore
    public static /* final */ int fab_transformation_scrim_behavior; // = 2131887401;
    @DexIgnore
    public static /* final */ int fab_transformation_sheet_behavior; // = 2131887402;
    @DexIgnore
    public static /* final */ int hide_bottom_view_on_scroll_behavior; // = 2131887427;
    @DexIgnore
    public static /* final */ int icon_content_description; // = 2131887430;
    @DexIgnore
    public static /* final */ int mtrl_badge_numberless_content_description; // = 2131887452;
    @DexIgnore
    public static /* final */ int mtrl_chip_close_icon_content_description; // = 2131887453;
    @DexIgnore
    public static /* final */ int mtrl_exceed_max_badge_number_suffix; // = 2131887454;
    @DexIgnore
    public static /* final */ int mtrl_picker_a11y_next_month; // = 2131887455;
    @DexIgnore
    public static /* final */ int mtrl_picker_a11y_prev_month; // = 2131887456;
    @DexIgnore
    public static /* final */ int mtrl_picker_announce_current_selection; // = 2131887457;
    @DexIgnore
    public static /* final */ int mtrl_picker_cancel; // = 2131887458;
    @DexIgnore
    public static /* final */ int mtrl_picker_confirm; // = 2131887459;
    @DexIgnore
    public static /* final */ int mtrl_picker_date_header_selected; // = 2131887460;
    @DexIgnore
    public static /* final */ int mtrl_picker_date_header_title; // = 2131887461;
    @DexIgnore
    public static /* final */ int mtrl_picker_date_header_unselected; // = 2131887462;
    @DexIgnore
    public static /* final */ int mtrl_picker_day_of_week_column_header; // = 2131887463;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_format; // = 2131887464;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_format_example; // = 2131887465;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_format_use; // = 2131887466;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_range; // = 2131887467;
    @DexIgnore
    public static /* final */ int mtrl_picker_navigate_to_year_description; // = 2131887468;
    @DexIgnore
    public static /* final */ int mtrl_picker_out_of_range; // = 2131887469;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_only_end_selected; // = 2131887470;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_only_start_selected; // = 2131887471;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_selected; // = 2131887472;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_title; // = 2131887473;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_unselected; // = 2131887474;
    @DexIgnore
    public static /* final */ int mtrl_picker_save; // = 2131887475;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_hint; // = 2131887476;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_range_end_hint; // = 2131887477;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_range_start_hint; // = 2131887478;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_day_abbr; // = 2131887479;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_month_abbr; // = 2131887480;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_year_abbr; // = 2131887481;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_calendar_input_mode; // = 2131887482;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_day_selection; // = 2131887483;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_text_input_mode; // = 2131887484;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_year_selection; // = 2131887485;
    @DexIgnore
    public static /* final */ int password_toggle_content_description; // = 2131887500;
    @DexIgnore
    public static /* final */ int path_password_eye; // = 2131887501;
    @DexIgnore
    public static /* final */ int path_password_eye_mask_strike_through; // = 2131887502;
    @DexIgnore
    public static /* final */ int path_password_eye_mask_visible; // = 2131887503;
    @DexIgnore
    public static /* final */ int path_password_strike_through; // = 2131887504;
    @DexIgnore
    public static /* final */ int search_menu_title; // = 2131887559;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131887575;
}
