package com.fossil;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq2 extends WeakReference<Throwable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f1789a;

    @DexIgnore
    public jq2(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.f1789a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == jq2.class) {
            if (this == obj) {
                return true;
            }
            jq2 jq2 = (jq2) obj;
            return this.f1789a == jq2.f1789a && get() == jq2.get();
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.f1789a;
    }
}
