package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.yb2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl3 extends yb2<cl3> {
    @DexIgnore
    public hl3(Context context, Looper looper, yb2.a aVar, yb2.b bVar) {
        super(context, looper, 93, aVar, bVar, null);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @DexIgnore
    /* Return type fixed from 'android.os.IInterface' to match base method */
    @Override // com.fossil.yb2
    public final /* synthetic */ cl3 q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        return queryLocalInterface instanceof cl3 ? (cl3) queryLocalInterface : new el3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final int s() {
        return h62.f1430a;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.measurement.START";
    }
}
