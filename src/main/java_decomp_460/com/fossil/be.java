package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.yk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        zk1 zk1;
        String str = null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null && action.hashCode() == 81199830 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED")) {
            e60 e60 = (e60) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            yk1.c cVar = (yk1.c) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE");
            yk1.c cVar2 = (yk1.c) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE");
            m80 m80 = m80.c;
            zw zwVar = zw.i;
            if (!(e60 == null || (zk1 = e60.u) == null)) {
                str = zk1.getMacAddress();
            }
            m80.a("ConnectionManager", "deviceStateChangeReceiver: device=%s, previousState=%s, newState=%s.", str, cVar, cVar2);
            if (e60 != null && cVar != null && cVar2 != null) {
                zw.i.b(e60);
            }
        }
    }
}
