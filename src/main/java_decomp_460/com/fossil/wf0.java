package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wf0 implements hm0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3928a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public CharSequence d;
    @DexIgnore
    public CharSequence e;
    @DexIgnore
    public Intent f;
    @DexIgnore
    public char g;
    @DexIgnore
    public int h; // = 4096;
    @DexIgnore
    public char i;
    @DexIgnore
    public int j; // = 4096;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public Context l;
    @DexIgnore
    public CharSequence m;
    @DexIgnore
    public CharSequence n;
    @DexIgnore
    public ColorStateList o; // = null;
    @DexIgnore
    public PorterDuff.Mode p; // = null;
    @DexIgnore
    public boolean q; // = false;
    @DexIgnore
    public boolean r; // = false;
    @DexIgnore
    public int s; // = 16;

    @DexIgnore
    public wf0(Context context, int i2, int i3, int i4, int i5, CharSequence charSequence) {
        this.l = context;
        this.f3928a = i3;
        this.b = i2;
        this.c = i5;
        this.d = charSequence;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public hm0 a(sn0 sn0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public sn0 b() {
        return null;
    }

    @DexIgnore
    public final void c() {
        if (this.k == null) {
            return;
        }
        if (this.q || this.r) {
            Drawable r2 = am0.r(this.k);
            this.k = r2;
            Drawable mutate = r2.mutate();
            this.k = mutate;
            if (this.q) {
                am0.o(mutate, this.o);
            }
            if (this.r) {
                am0.p(this.k, this.p);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public boolean collapseActionView() {
        return false;
    }

    @DexIgnore
    public hm0 d(int i2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public hm0 e(View view) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public boolean expandActionView() {
        return false;
    }

    @DexIgnore
    public hm0 f(int i2) {
        setShowAsAction(i2);
        return this;
    }

    @DexIgnore
    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public View getActionView() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public int getAlphabeticModifiers() {
        return this.j;
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public CharSequence getContentDescription() {
        return this.m;
    }

    @DexIgnore
    public int getGroupId() {
        return this.b;
    }

    @DexIgnore
    public Drawable getIcon() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public ColorStateList getIconTintList() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public PorterDuff.Mode getIconTintMode() {
        return this.p;
    }

    @DexIgnore
    public Intent getIntent() {
        return this.f;
    }

    @DexIgnore
    public int getItemId() {
        return this.f3928a;
    }

    @DexIgnore
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public int getNumericModifiers() {
        return this.h;
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.g;
    }

    @DexIgnore
    public int getOrder() {
        return this.c;
    }

    @DexIgnore
    public SubMenu getSubMenu() {
        return null;
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.d;
    }

    @DexIgnore
    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.e;
        return charSequence != null ? charSequence : this.d;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public CharSequence getTooltipText() {
        return this.n;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public boolean isActionViewExpanded() {
        return false;
    }

    @DexIgnore
    public boolean isCheckable() {
        return (this.s & 1) != 0;
    }

    @DexIgnore
    public boolean isChecked() {
        return (this.s & 2) != 0;
    }

    @DexIgnore
    public boolean isEnabled() {
        return (this.s & 16) != 0;
    }

    @DexIgnore
    public boolean isVisible() {
        return (this.s & 8) == 0;
    }

    @DexIgnore
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.hm0, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setActionView(int i2) {
        d(i2);
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hm0, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setActionView(View view) {
        e(view);
        throw null;
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2) {
        this.i = Character.toLowerCase(c2);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        this.i = Character.toLowerCase(c2);
        this.j = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    @DexIgnore
    public MenuItem setCheckable(boolean z) {
        this.s = (this.s & -2) | (z ? 1 : 0);
        return this;
    }

    @DexIgnore
    public MenuItem setChecked(boolean z) {
        this.s = (z ? 2 : 0) | (this.s & -3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public hm0 setContentDescription(CharSequence charSequence) {
        this.m = charSequence;
        return this;
    }

    @DexIgnore
    public MenuItem setEnabled(boolean z) {
        this.s = (z ? 16 : 0) | (this.s & -17);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(int i2) {
        this.k = gl0.f(this.l, i2);
        c();
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.k = drawable;
        c();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.o = colorStateList;
        this.q = true;
        c();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.p = mode;
        this.r = true;
        c();
        return this;
    }

    @DexIgnore
    public MenuItem setIntent(Intent intent) {
        this.f = intent;
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2) {
        this.g = (char) c2;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setNumericShortcut(char c2, int i2) {
        this.g = (char) c2;
        this.h = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    @DexIgnore
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3) {
        this.g = (char) c2;
        this.i = Character.toLowerCase(c3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.g = (char) c2;
        this.h = KeyEvent.normalizeMetaState(i2);
        this.i = Character.toLowerCase(c3);
        this.j = KeyEvent.normalizeMetaState(i3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public void setShowAsAction(int i2) {
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public /* bridge */ /* synthetic */ MenuItem setShowAsActionFlags(int i2) {
        f(i2);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(int i2) {
        this.d = this.l.getResources().getString(i2);
        return this;
    }

    @DexIgnore
    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.d = charSequence;
        return this;
    }

    @DexIgnore
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.e = charSequence;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hm0
    public hm0 setTooltipText(CharSequence charSequence) {
        this.n = charSequence;
        return this;
    }

    @DexIgnore
    public MenuItem setVisible(boolean z) {
        int i2 = this.s;
        int i3 = 8;
        if (z) {
            i3 = 0;
        }
        this.s = i3 | (i2 & 8);
        return this;
    }
}
