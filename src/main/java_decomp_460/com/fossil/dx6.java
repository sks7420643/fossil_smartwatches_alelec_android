package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx6 implements Factory<cx6> {
    @DexIgnore
    public static cx6 a(zw6 zw6, u27 u27, UserRepository userRepository, DeviceRepository deviceRepository, ServerSettingRepository serverSettingRepository, pr4 pr4, on5 on5) {
        return new cx6(zw6, u27, userRepository, deviceRepository, serverSettingRepository, pr4, on5);
    }
}
