package com.fossil;

import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu7 extends iu7 {
    @DexIgnore
    public /* final */ Future<?> b;

    @DexIgnore
    public hu7(Future<?> future) {
        this.b = future;
    }

    @DexIgnore
    @Override // com.fossil.ju7
    public void a(Throwable th) {
        this.b.cancel(false);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        a(th);
        return tl7.f3441a;
    }

    @DexIgnore
    public String toString() {
        return "CancelFutureOnCancel[" + this.b + ']';
    }
}
