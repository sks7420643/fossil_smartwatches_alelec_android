package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g93 implements xw2<j93> {
    @DexIgnore
    public static g93 c; // = new g93();
    @DexIgnore
    public /* final */ xw2<j93> b;

    @DexIgnore
    public g93() {
        this(ww2.b(new i93()));
    }

    @DexIgnore
    public g93(xw2<j93> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((j93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ j93 zza() {
        return this.b.zza();
    }
}
