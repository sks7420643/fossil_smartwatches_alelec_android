package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa0 implements Parcelable.Creator<qa0> {
    @DexIgnore
    public /* synthetic */ pa0(kq7 kq7) {
    }

    @DexIgnore
    public qa0 a(Parcel parcel) {
        return new qa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public qa0 createFromParcel(Parcel parcel) {
        return new qa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public qa0[] newArray(int i) {
        return new qa0[i];
    }
}
