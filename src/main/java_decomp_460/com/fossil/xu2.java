package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu2 extends e13<xu2, a> implements o23 {
    @DexIgnore
    public static /* final */ xu2 zzf;
    @DexIgnore
    public static volatile z23<xu2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public long zze;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<xu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(xu2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }
    }

    /*
    static {
        xu2 xu2 = new xu2();
        zzf = xu2;
        e13.u(xu2.class, xu2);
    }
    */

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new xu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                z23<xu2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (xu2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new e13.c(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
