package com.fossil;

import android.os.SystemClock;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez4 implements View.OnClickListener {
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ rp7<View, tl7> d;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.rp7<? super android.view.View, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    public ez4(int i, rp7<? super View, tl7> rp7) {
        pq7.c(rp7, "onSafeClick");
        this.c = i;
        this.d = rp7;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ez4(int i, rp7 rp7, int i2, kq7 kq7) {
        this((i2 & 1) != 0 ? 1000 : i, rp7);
    }

    @DexIgnore
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - this.b >= ((long) this.c)) {
            this.b = SystemClock.elapsedRealtime();
            this.d.invoke(view);
        }
    }
}
