package com.fossil;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fk4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fk4 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Method f1143a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public a(Method method, Object obj) {
            this.f1143a = method;
            this.b = obj;
        }

        @DexIgnore
        @Override // com.fossil.fk4
        public <T> T c(Class<T> cls) throws Exception {
            fk4.a(cls);
            return (T) this.f1143a.invoke(this.b, cls);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fk4 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Method f1144a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(Method method, int i) {
            this.f1144a = method;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.fk4
        public <T> T c(Class<T> cls) throws Exception {
            fk4.a(cls);
            return (T) this.f1144a.invoke(null, cls, Integer.valueOf(this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fk4 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Method f1145a;

        @DexIgnore
        public c(Method method) {
            this.f1145a = method;
        }

        @DexIgnore
        @Override // com.fossil.fk4
        public <T> T c(Class<T> cls) throws Exception {
            fk4.a(cls);
            return (T) this.f1145a.invoke(null, cls, Object.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fk4 {
        @DexIgnore
        @Override // com.fossil.fk4
        public <T> T c(Class<T> cls) {
            throw new UnsupportedOperationException("Cannot allocate " + cls);
        }
    }

    @DexIgnore
    public static void a(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            throw new UnsupportedOperationException("Interface can't be instantiated! Interface name: " + cls.getName());
        } else if (Modifier.isAbstract(modifiers)) {
            throw new UnsupportedOperationException("Abstract class can't be instantiated! Class name: " + cls.getName());
        }
    }

    @DexIgnore
    public static fk4 b() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return new a(cls.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception e) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                return new b(declaredMethod2, intValue);
            } catch (Exception e2) {
                try {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    return new c(declaredMethod3);
                } catch (Exception e3) {
                    return new d();
                }
            }
        }
    }

    @DexIgnore
    public abstract <T> T c(Class<T> cls) throws Exception;
}
