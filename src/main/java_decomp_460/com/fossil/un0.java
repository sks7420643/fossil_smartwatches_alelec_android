package com.fossil;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class un0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ View f3617a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ View.OnLongClickListener f; // = new a();
    @DexIgnore
    public /* final */ View.OnTouchListener g; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnLongClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            return un0.this.c(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnTouchListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return un0.this.d(view, motionEvent);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        boolean a(View view, un0 un0);
    }

    @DexIgnore
    public un0(View view, c cVar) {
        this.f3617a = view;
        this.b = cVar;
    }

    @DexIgnore
    public void a() {
        this.f3617a.setOnLongClickListener(this.f);
        this.f3617a.setOnTouchListener(this.g);
    }

    @DexIgnore
    public void b(Point point) {
        point.set(this.c, this.d);
    }

    @DexIgnore
    public boolean c(View view) {
        return this.b.a(view, this);
    }

    @DexIgnore
    public boolean d(View view, MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action != 2) {
                    if (action != 3) {
                        return false;
                    }
                } else if (!bo0.b(motionEvent, 8194) || (motionEvent.getButtonState() & 1) == 0 || this.e) {
                    return false;
                } else {
                    if (this.c == x && this.d == y) {
                        return false;
                    }
                    this.c = x;
                    this.d = y;
                    boolean a2 = this.b.a(view, this);
                    this.e = a2;
                    return a2;
                }
            }
            this.e = false;
            return false;
        }
        this.c = x;
        this.d = y;
        return false;
    }
}
