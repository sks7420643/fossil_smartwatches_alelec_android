package com.fossil;

import android.app.Application;
import com.google.errorprone.annotations.ForOverride;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xj7 extends Application implements dk7 {
    @DexIgnore
    public volatile ck7<Object> b;

    @DexIgnore
    @ForOverride
    public abstract vj7<? extends xj7> a();

    @DexIgnore
    public final void b() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    a().a(this);
                    if (this.b == null) {
                        throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public vj7<Object> c() {
        b();
        return this.b;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        b();
    }
}
