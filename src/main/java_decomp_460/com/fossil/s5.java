package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s5 extends ox1 {
    @DexIgnore
    public static /* final */ o5 e; // = new o5(null);
    @DexIgnore
    public /* final */ v5 b;
    @DexIgnore
    public /* final */ r5 c;
    @DexIgnore
    public /* final */ g7 d;

    @DexIgnore
    public s5(v5 v5Var, r5 r5Var, g7 g7Var) {
        this.b = v5Var;
        this.c = r5Var;
        this.d = g7Var;
    }

    @DexIgnore
    public /* synthetic */ s5(v5 v5Var, r5 r5Var, g7 g7Var, int i) {
        v5Var = (i & 1) != 0 ? v5.o : v5Var;
        g7Var = (i & 4) != 0 ? new g7(f7.SUCCESS, 0, 2) : g7Var;
        this.b = v5Var;
        this.c = r5Var;
        this.d = g7Var;
    }

    @DexIgnore
    public static /* synthetic */ s5 a(s5 s5Var, v5 v5Var, r5 r5Var, g7 g7Var, int i) {
        if ((i & 1) != 0) {
            v5Var = s5Var.b;
        }
        if ((i & 2) != 0) {
            r5Var = s5Var.c;
        }
        if ((i & 4) != 0) {
            g7Var = s5Var.d;
        }
        return s5Var.a(v5Var, r5Var, g7Var);
    }

    @DexIgnore
    public final s5 a(v5 v5Var, r5 r5Var, g7 g7Var) {
        return new s5(v5Var, r5Var, g7Var);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof s5) {
                s5 s5Var = (s5) obj;
                if (!pq7.a(this.b, s5Var.b) || !pq7.a(this.c, s5Var.c) || !pq7.a(this.d, s5Var.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        v5 v5Var = this.b;
        int hashCode = v5Var != null ? v5Var.hashCode() : 0;
        r5 r5Var = this.c;
        int hashCode2 = r5Var != null ? r5Var.hashCode() : 0;
        g7 g7Var = this.d;
        if (g7Var != null) {
            i = g7Var.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(g80.k(jSONObject, jd0.E2, ey1.a(this.b)), jd0.O0, ey1.a(this.c));
            if (this.d.b != f7.SUCCESS) {
                g80.k(jSONObject, jd0.F2, this.d.toJSONObject());
            }
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e2 = e.e("Result(commandId=");
        e2.append(this.b);
        e2.append(", resultCode=");
        e2.append(this.c);
        e2.append(", gattResult=");
        e2.append(this.d);
        e2.append(")");
        return e2.toString();
    }
}
