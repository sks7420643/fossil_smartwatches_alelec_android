package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class th4 {
    @DexIgnore
    public static Executor a(String str) {
        return new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), new sf2(str));
    }

    @DexIgnore
    public static ExecutorService b() {
        return gm2.a().a(new sf2("Firebase-Messaging-Intent-Handle"), lm2.f2217a);
    }

    @DexIgnore
    public static ExecutorService c() {
        return Executors.newSingleThreadExecutor(new sf2("Firebase-Messaging-Network-Io"));
    }

    @DexIgnore
    public static ScheduledExecutorService d() {
        return new ScheduledThreadPoolExecutor(1, new sf2("Firebase-Messaging-Topics-Io"));
    }

    @DexIgnore
    public static Executor e() {
        return a("Firebase-Messaging-Trigger-Topics-Io");
    }
}
