package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oi extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.FILE_CONFIG));
    @DexIgnore
    public long D;
    @DexIgnore
    public long E;
    @DexIgnore
    public long F;
    @DexIgnore
    public /* final */ long G; // = hy1.o(this.H.f.length);
    @DexIgnore
    public /* final */ j0 H;

    @DexIgnore
    public oi(k5 k5Var, i60 i60, j0 j0Var, String str) {
        super(k5Var, i60, yp.N, str, false, 16);
        this.H = j0Var;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        this.E = 0;
        long o = hy1.o(this.H.f.length);
        this.F = o;
        if (this.E == o) {
            l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
        } else {
            lp.j(this, hs.p, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.i2, this.H.a(false));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        return g80.k(super.E(), jd0.h2, Long.valueOf(this.D));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public fs b(hs hsVar) {
        if (nh.f2519a[hsVar.ordinal()] != 1) {
            return null;
        }
        long j = this.E;
        lv lvVar = new lv(j, this.F - j, this.H.b(), this.w, 0, 16);
        lvVar.o(new ai(this));
        return lvVar;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return Long.valueOf(this.D);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
