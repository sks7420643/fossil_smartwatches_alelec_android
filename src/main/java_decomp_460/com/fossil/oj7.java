package com.fossil;

import com.zendesk.service.ErrorResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oj7 implements ErrorResponse {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2689a;

    @DexIgnore
    public oj7(String str) {
        this.f2689a = str;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public int a() {
        return -1;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public String b() {
        return this.f2689a;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public boolean c() {
        return false;
    }
}
