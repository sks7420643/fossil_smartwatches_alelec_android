package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rf2 implements Executor {
    @DexIgnore
    public /* final */ Handler b;

    @DexIgnore
    public rf2(Looper looper) {
        this.b = new xl2(looper);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.post(runnable);
    }
}
