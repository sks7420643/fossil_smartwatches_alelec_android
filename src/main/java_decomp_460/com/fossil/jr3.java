package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr3 extends SSLSocketFactory {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SSLSocketFactory f1794a;

    @DexIgnore
    public jr3() {
        this(HttpsURLConnection.getDefaultSSLSocketFactory());
    }

    @DexIgnore
    public jr3(SSLSocketFactory sSLSocketFactory) {
        this.f1794a = sSLSocketFactory;
    }

    @DexIgnore
    public final SSLSocket a(SSLSocket sSLSocket) {
        return new lr3(this, sSLSocket);
    }

    @DexIgnore
    @Override // javax.net.SocketFactory
    public final Socket createSocket() throws IOException {
        return a((SSLSocket) this.f1794a.createSocket());
    }

    @DexIgnore
    @Override // javax.net.SocketFactory
    public final Socket createSocket(String str, int i) throws IOException {
        return a((SSLSocket) this.f1794a.createSocket(str, i));
    }

    @DexIgnore
    @Override // javax.net.SocketFactory
    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return a((SSLSocket) this.f1794a.createSocket(str, i, inetAddress, i2));
    }

    @DexIgnore
    @Override // javax.net.SocketFactory
    public final Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a((SSLSocket) this.f1794a.createSocket(inetAddress, i));
    }

    @DexIgnore
    @Override // javax.net.SocketFactory
    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a((SSLSocket) this.f1794a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    @DexIgnore
    @Override // javax.net.ssl.SSLSocketFactory
    public final Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a((SSLSocket) this.f1794a.createSocket(socket, str, i, z));
    }

    @DexIgnore
    public final String[] getDefaultCipherSuites() {
        return this.f1794a.getDefaultCipherSuites();
    }

    @DexIgnore
    public final String[] getSupportedCipherSuites() {
        return this.f1794a.getSupportedCipherSuites();
    }
}
