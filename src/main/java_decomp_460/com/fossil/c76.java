package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c76 implements MembersInjector<WatchAppEditActivity> {
    @DexIgnore
    public static void a(WatchAppEditActivity watchAppEditActivity, h76 h76) {
        watchAppEditActivity.A = h76;
    }

    @DexIgnore
    public static void b(WatchAppEditActivity watchAppEditActivity, po4 po4) {
        watchAppEditActivity.B = po4;
    }
}
