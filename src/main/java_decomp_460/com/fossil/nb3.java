package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb3 extends mq2 implements lb3 {
    @DexIgnore
    public nb3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationListener");
    }

    @DexIgnore
    @Override // com.fossil.lb3
    public final void onLocationChanged(Location location) throws RemoteException {
        Parcel d = d();
        qr2.c(d, location);
        n(1, d);
    }
}
