package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu1 extends vu1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ short d; // = hy1.c(fq7.f1179a);
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<uu1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public uu1 createFromParcel(Parcel parcel) {
            return new uu1(hy1.p(parcel.readByte()));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public uu1[] newArray(int i) {
            return new uu1[i];
        }
    }

    @DexIgnore
    public uu1(short s) throws IllegalArgumentException {
        this.c = (short) s;
        if (!(s >= 0 && d >= s)) {
            StringBuilder e = e.e("goalId(");
            e.append((int) this.c);
            e.append(") is out of range ");
            e.append("[0, ");
            throw new IllegalArgumentException(e.b(e, d, "]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.vu1
    public int b() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.vu1
    public byte[] c() {
        ByteBuffer allocate = ByteBuffer.allocate(3);
        pq7.b(allocate, "ByteBuffer.allocate(GOAL\u2026ACKING_TYPE_FRAME_LENGTH)");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put(ba0.c.b);
        allocate.put((byte) 1);
        allocate.put((byte) this.c);
        byte[] array = allocate.array();
        pq7.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final short getGoalId() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(new JSONObject(), jd0.N3, Short.valueOf(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) this.c);
    }
}
