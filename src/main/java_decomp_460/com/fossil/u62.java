package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u62 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends z62> extends BasePendingResult<R> {
        @DexIgnore
        public /* final */ R q;

        @DexIgnore
        public a(r62 r62, R r) {
            super(r62);
            this.q = r;
        }

        @DexIgnore
        @Override // com.google.android.gms.common.api.internal.BasePendingResult
        public final R f(Status status) {
            return this.q;
        }
    }

    @DexIgnore
    public static <R extends z62> t62<R> a(R r, r62 r62) {
        rc2.l(r, "Result must not be null");
        rc2.b(!r.a().D(), "Status code must not be SUCCESS");
        a aVar = new a(r62, r);
        aVar.j(r);
        return aVar;
    }

    @DexIgnore
    public static t62<Status> b(Status status, r62 r62) {
        rc2.l(status, "Result must not be null");
        v72 v72 = new v72(r62);
        v72.j(status);
        return v72;
    }
}
