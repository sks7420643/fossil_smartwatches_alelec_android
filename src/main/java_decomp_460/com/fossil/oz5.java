package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oz5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ld6 f2756a;
    @DexIgnore
    public /* final */ y66 b;
    @DexIgnore
    public /* final */ lb6 c;
    @DexIgnore
    public /* final */ ao6 d;
    @DexIgnore
    public /* final */ a06 e;
    @DexIgnore
    public /* final */ y36 f;
    @DexIgnore
    public /* final */ sn6 g;

    @DexIgnore
    public oz5(ld6 ld6, y66 y66, lb6 lb6, ao6 ao6, a06 a06, y36 y36, sn6 sn6) {
        pq7.c(ld6, "mDashboardView");
        pq7.c(y66, "mDianaCustomizeView");
        pq7.c(lb6, "mHybridCustomizeView");
        pq7.c(ao6, "mProfileView");
        pq7.c(a06, "mAlertsView");
        pq7.c(y36, "mAlertsHybridView");
        pq7.c(sn6, "mUpdateFirmwareView");
        this.f2756a = ld6;
        this.b = y66;
        this.c = lb6;
        this.d = ao6;
        this.e = a06;
        this.f = y36;
        this.g = sn6;
    }

    @DexIgnore
    public final y36 a() {
        return this.f;
    }

    @DexIgnore
    public final a06 b() {
        return this.e;
    }

    @DexIgnore
    public final ld6 c() {
        return this.f2756a;
    }

    @DexIgnore
    public final y66 d() {
        return this.b;
    }

    @DexIgnore
    public final lb6 e() {
        return this.c;
    }

    @DexIgnore
    public final WeakReference<ao6> f() {
        return new WeakReference<>(this.d);
    }

    @DexIgnore
    public final sn6 g() {
        return this.g;
    }
}
