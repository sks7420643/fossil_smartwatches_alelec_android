package com.fossil;

import java.lang.reflect.Array;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rn4[] f3000a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public qn4(int i, int i2) {
        rn4[] rn4Arr = new rn4[i];
        this.f3000a = rn4Arr;
        int length = rn4Arr.length;
        for (int i3 = 0; i3 < length; i3++) {
            this.f3000a[i3] = new rn4(((i2 + 4) * 17) + 1);
        }
        this.d = i2 * 17;
        this.c = i;
        this.b = -1;
    }

    @DexIgnore
    public rn4 a() {
        return this.f3000a[this.b];
    }

    @DexIgnore
    public byte[][] b(int i, int i2) {
        byte[][] bArr = (byte[][]) Array.newInstance(Byte.TYPE, this.c * i2, this.d * i);
        int i3 = this.c * i2;
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i3 - i4) - 1] = this.f3000a[i4 / i2].b(i);
        }
        return bArr;
    }

    @DexIgnore
    public void c() {
        this.b++;
    }
}
