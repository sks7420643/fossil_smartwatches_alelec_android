package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lb4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2173a;
    @DexIgnore
    public String b;
    @DexIgnore
    public p18 c;

    @DexIgnore
    public lb4(int i, String str, p18 p18) {
        this.f2173a = i;
        this.b = str;
        this.c = p18;
    }

    @DexIgnore
    public static lb4 c(Response response) throws IOException {
        return new lb4(response.f(), response.a() == null ? null : response.a().string(), response.l());
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.f2173a;
    }

    @DexIgnore
    public String d(String str) {
        return this.c.c(str);
    }
}
