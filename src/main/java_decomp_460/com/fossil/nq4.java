package com.fossil;

import com.fossil.zu0;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq4 extends zu0.d<GoalTrackingData> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        pq7.c(goalTrackingData, "oldItem");
        pq7.c(goalTrackingData2, "newItem");
        return pq7.a(goalTrackingData, goalTrackingData2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        pq7.c(goalTrackingData, "oldItem");
        pq7.c(goalTrackingData2, "newItem");
        return pq7.a(goalTrackingData.getId(), goalTrackingData2.getId());
    }
}
