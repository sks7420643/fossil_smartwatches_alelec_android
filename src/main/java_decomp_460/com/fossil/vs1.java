package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<vs1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public vs1 createFromParcel(Parcel parcel) {
            return new vs1(parcel.readInt(), parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public vs1[] newArray(int i) {
            return new vs1[i];
        }
    }

    @DexIgnore
    public vs1(int i, int i2, int i3) throws IllegalArgumentException {
        boolean z = true;
        this.b = i;
        this.c = i2;
        this.d = i3;
        if (i >= 0 && 359 >= i) {
            int i4 = this.c;
            if (i4 >= 0 && 120 >= i4) {
                if (!(this.d < 0 ? false : z)) {
                    throw new IllegalArgumentException(e.c(e.e("z index("), this.d, ") must be larger than ", "[0]."));
                }
                return;
            }
            throw new IllegalArgumentException(e.c(e.e("distanceFromCenter("), this.c, ") is out of ", "range [0, 120]."));
        }
        throw new IllegalArgumentException(e.c(e.e("angle("), this.b, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(vs1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            vs1 vs1 = (vs1) obj;
            if (this.b != vs1.b) {
                return false;
            }
            if (this.c != vs1.c) {
                return false;
            }
            return this.d == vs1.d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.b;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.c;
    }

    @DexIgnore
    public final int getZIndex() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("angle", this.b).put("distance", this.c).put("z_index", this.d);
        pq7.b(put, "JSONObject()\n           \u2026Constant.Z_INDEX, zIndex)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }
}
