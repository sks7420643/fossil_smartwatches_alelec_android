package com.fossil;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.util.Property;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ly0<T> extends Property<T, Float> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Property<T, PointF> f2277a;
    @DexIgnore
    public /* final */ PathMeasure b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float[] d; // = new float[2];
    @DexIgnore
    public /* final */ PointF e; // = new PointF();
    @DexIgnore
    public float f;

    @DexIgnore
    public ly0(Property<T, PointF> property, Path path) {
        super(Float.class, property.getName());
        this.f2277a = property;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        this.b = pathMeasure;
        this.c = pathMeasure.getLength();
    }

    @DexIgnore
    /* renamed from: a */
    public Float get(T t) {
        return Float.valueOf(this.f);
    }

    @DexIgnore
    /* renamed from: b */
    public void set(T t, Float f2) {
        this.f = f2.floatValue();
        this.b.getPosTan(this.c * f2.floatValue(), this.d, null);
        PointF pointF = this.e;
        float[] fArr = this.d;
        pointF.x = fArr[0];
        pointF.y = fArr[1];
        this.f2277a.set(t, pointF);
    }
}
