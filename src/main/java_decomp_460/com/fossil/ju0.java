package com.fossil;

import com.fossil.xt0;
import com.fossil.yt0;
import java.util.IdentityHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ju0<K, A, B> extends yt0<K, B> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ yt0<K, A> f1803a;
    @DexIgnore
    public /* final */ gi0<List<A>, List<B>> b;
    @DexIgnore
    public /* final */ IdentityHashMap<B, K> c; // = new IdentityHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends yt0.c<A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yt0.c f1804a;

        @DexIgnore
        public a(yt0.c cVar) {
            this.f1804a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.yt0.a
        public void a(List<A> list) {
            this.f1804a.a(ju0.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends yt0.a<A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yt0.a f1805a;

        @DexIgnore
        public b(yt0.a aVar) {
            this.f1805a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.yt0.a
        public void a(List<A> list) {
            this.f1805a.a(ju0.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends yt0.a<A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yt0.a f1806a;

        @DexIgnore
        public c(yt0.a aVar) {
            this.f1806a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.yt0.a
        public void a(List<A> list) {
            this.f1806a.a(ju0.this.a(list));
        }
    }

    @DexIgnore
    public ju0(yt0<K, A> yt0, gi0<List<A>, List<B>> gi0) {
        this.f1803a = yt0;
        this.b = gi0;
    }

    @DexIgnore
    public List<B> a(List<A> list) {
        List<B> convert = xt0.convert(this.b, list);
        synchronized (this.c) {
            for (int i = 0; i < convert.size(); i++) {
                this.c.put(convert.get(i), this.f1803a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void addInvalidatedCallback(xt0.c cVar) {
        this.f1803a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public K getKey(B b2) {
        K k;
        synchronized (this.c) {
            k = this.c.get(b2);
        }
        return k;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void invalidate() {
        this.f1803a.invalidate();
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        return this.f1803a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadAfter(yt0.f<K> fVar, yt0.a<B> aVar) {
        this.f1803a.loadAfter(fVar, new b(aVar));
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadBefore(yt0.f<K> fVar, yt0.a<B> aVar) {
        this.f1803a.loadBefore(fVar, new c(aVar));
    }

    @DexIgnore
    @Override // com.fossil.yt0
    public void loadInitial(yt0.e<K> eVar, yt0.c<B> cVar) {
        this.f1803a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void removeInvalidatedCallback(xt0.c cVar) {
        this.f1803a.removeInvalidatedCallback(cVar);
    }
}
