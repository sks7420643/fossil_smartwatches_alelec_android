package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha4 extends ta4.d.AbstractC0224d {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f1457a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ta4.d.AbstractC0224d.a c;
    @DexIgnore
    public /* final */ ta4.d.AbstractC0224d.c d;
    @DexIgnore
    public /* final */ ta4.d.AbstractC0224d.AbstractC0235d e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Long f1458a;
        @DexIgnore
        public String b;
        @DexIgnore
        public ta4.d.AbstractC0224d.a c;
        @DexIgnore
        public ta4.d.AbstractC0224d.c d;
        @DexIgnore
        public ta4.d.AbstractC0224d.AbstractC0235d e;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(ta4.d.AbstractC0224d dVar) {
            this.f1458a = Long.valueOf(dVar.e());
            this.b = dVar.f();
            this.c = dVar.b();
            this.d = dVar.c();
            this.e = dVar.d();
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.b
        public ta4.d.AbstractC0224d a() {
            String str = "";
            if (this.f1458a == null) {
                str = " timestamp";
            }
            if (this.b == null) {
                str = str + " type";
            }
            if (this.c == null) {
                str = str + " app";
            }
            if (this.d == null) {
                str = str + " device";
            }
            if (str.isEmpty()) {
                return new ha4(this.f1458a.longValue(), this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.b
        public ta4.d.AbstractC0224d.b b(ta4.d.AbstractC0224d.a aVar) {
            if (aVar != null) {
                this.c = aVar;
                return this;
            }
            throw new NullPointerException("Null app");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.b
        public ta4.d.AbstractC0224d.b c(ta4.d.AbstractC0224d.c cVar) {
            if (cVar != null) {
                this.d = cVar;
                return this;
            }
            throw new NullPointerException("Null device");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.b
        public ta4.d.AbstractC0224d.b d(ta4.d.AbstractC0224d.AbstractC0235d dVar) {
            this.e = dVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.b
        public ta4.d.AbstractC0224d.b e(long j) {
            this.f1458a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.b
        public ta4.d.AbstractC0224d.b f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null type");
        }
    }

    @DexIgnore
    public ha4(long j, String str, ta4.d.AbstractC0224d.a aVar, ta4.d.AbstractC0224d.c cVar, ta4.d.AbstractC0224d.AbstractC0235d dVar) {
        this.f1457a = j;
        this.b = str;
        this.c = aVar;
        this.d = cVar;
        this.e = dVar;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d
    public ta4.d.AbstractC0224d.a b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d
    public ta4.d.AbstractC0224d.c c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d
    public ta4.d.AbstractC0224d.AbstractC0235d d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d
    public long e() {
        return this.f1457a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d)) {
            return false;
        }
        ta4.d.AbstractC0224d dVar = (ta4.d.AbstractC0224d) obj;
        if (this.f1457a == dVar.e() && this.b.equals(dVar.f()) && this.c.equals(dVar.b()) && this.d.equals(dVar.c())) {
            ta4.d.AbstractC0224d.AbstractC0235d dVar2 = this.e;
            if (dVar2 == null) {
                if (dVar.d() == null) {
                    return true;
                }
            } else if (dVar2.equals(dVar.d())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d
    public String f() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d
    public ta4.d.AbstractC0224d.b g() {
        return new b(this);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f1457a;
        int i = (int) (j ^ (j >>> 32));
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        int hashCode3 = this.d.hashCode();
        ta4.d.AbstractC0224d.AbstractC0235d dVar = this.e;
        return (dVar == null ? 0 : dVar.hashCode()) ^ ((((((((i ^ 1000003) * 1000003) ^ hashCode) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "Event{timestamp=" + this.f1457a + ", type=" + this.b + ", app=" + this.c + ", device=" + this.d + ", log=" + this.e + "}";
    }
}
