package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi2 implements Parcelable.Creator<yi2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ yi2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        IBinder iBinder = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 1) {
                ad2.B(parcel, t);
            } else {
                iBinder = ad2.u(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new yi2(iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ yi2[] newArray(int i) {
        return new yi2[i];
    }
}
