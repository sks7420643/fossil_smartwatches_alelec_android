package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw4 extends ts0 {
    @DexIgnore
    public static /* final */ String g;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Boolean> f515a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> c; // = new MutableLiveData<>();
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public /* final */ LiveData<List<dt4>> e;
    @DexIgnore
    public /* final */ du4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$fetchNotification$1", f = "BCNotificationViewModel.kt", l = {57}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bw4 bw4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.f515a.l(ao7.a(true));
                bw4 bw4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (bw4.h(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel", f = "BCNotificationViewModel.kt", l = {68}, m = "fetchNotificationServer")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(bw4 bw4, qn7 qn7) {
            super(qn7);
            this.this$0 = bw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$fetchNotificationServer$result$1", f = "BCNotificationViewModel.kt", l = {68}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super kz4<List<dt4>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bw4 bw4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<List<dt4>>> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                du4 du4 = this.this$0.f;
                this.L$0 = iv7;
                this.label = 1;
                Object b = du4.b(this);
                return b == d ? d : b;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bw4 f516a;

        @DexIgnore
        public d(bw4 bw4) {
            this.f516a = bw4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<dt4>> apply(List<dt4> list) {
            MutableLiveData<List<dt4>> mutableLiveData = new MutableLiveData<>();
            mutableLiveData.o(list);
            if (!list.isEmpty()) {
                this.f516a.b.l(Boolean.FALSE);
                this.f516a.f515a.l(Boolean.FALSE);
            } else if (!this.f516a.d) {
                this.f516a.b.l(Boolean.TRUE);
            }
            if (this.f516a.d) {
                this.f516a.d = false;
                this.f516a.g();
            }
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$refresh$1", f = "BCNotificationViewModel.kt", l = {63}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(bw4 bw4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bw4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                bw4 bw4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (bw4.h(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = bw4.class.getSimpleName();
        pq7.b(simpleName, "BCNotificationViewModel::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public bw4(du4 du4) {
        pq7.c(du4, "notificationRepository");
        this.f = du4;
        LiveData<List<dt4>> c2 = ss0.c(cz4.a(this.f.c()), new d(this));
        pq7.b(c2, "Transformations.switchMa\u2026    }\n\n        live\n    }");
        this.e = c2;
    }

    @DexIgnore
    public final void g() {
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h(com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r9 instanceof com.fossil.bw4.b
            if (r0 == 0) goto L_0x006b
            r0 = r9
            com.fossil.bw4$b r0 = (com.fossil.bw4.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x006b
            int r1 = r1 + r3
            r0.label = r1
        L_0x0015:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0079
            if (r3 != r5) goto L_0x0071
            java.lang.Object r0 = r0.L$0
            com.fossil.bw4 r0 = (com.fossil.bw4) r0
            com.fossil.el7.b(r1)
            r8 = r0
        L_0x0029:
            r0 = r1
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.bw4.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "result: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r8.f515a
            java.lang.Boolean r2 = com.fossil.ao7.a(r6)
            r1.l(r2)
            java.lang.Object r1 = r0.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x0091
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x0068
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r0 = r8.b
            java.lang.Boolean r1 = com.fossil.ao7.a(r5)
            r0.l(r1)
        L_0x0068:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x006a:
            return r0
        L_0x006b:
            com.fossil.bw4$b r0 = new com.fossil.bw4$b
            r0.<init>(r8, r9)
            goto L_0x0015
        L_0x0071:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0079:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.fossil.bw4$c r3 = new com.fossil.bw4$c
            r3.<init>(r8, r7)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r1, r3, r0)
            if (r1 != r2) goto L_0x0029
            r0 = r2
            goto L_0x006a
        L_0x0091:
            androidx.lifecycle.LiveData<java.util.List<com.fossil.dt4>> r1 = r8.e
            java.lang.Object r1 = r1.e()
            if (r1 == 0) goto L_0x00a9
            androidx.lifecycle.LiveData<java.util.List<com.fossil.dt4>> r1 = r8.e
            java.lang.Object r1 = r1.e()
            if (r1 == 0) goto L_0x00cd
            java.util.List r1 = (java.util.List) r1
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x00bb
        L_0x00a9:
            androidx.lifecycle.MutableLiveData<com.fossil.cl7<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r8.c
            java.lang.Boolean r2 = com.fossil.ao7.a(r5)
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            com.fossil.cl7 r0 = com.fossil.hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x0068
        L_0x00bb:
            androidx.lifecycle.MutableLiveData<com.fossil.cl7<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r8.c
            java.lang.Boolean r2 = com.fossil.ao7.a(r6)
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            com.fossil.cl7 r0 = com.fossil.hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x0068
        L_0x00cd:
            com.fossil.pq7.i()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bw4.h(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<Boolean> i() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> j() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> k() {
        return this.f515a;
    }

    @DexIgnore
    public final LiveData<List<dt4>> l() {
        return this.e;
    }

    @DexIgnore
    public final void m() {
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, null), 3, null);
    }
}
