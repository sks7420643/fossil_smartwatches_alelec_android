package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class re6 implements Factory<qe6> {
    @DexIgnore
    public static qe6 a(oe6 oe6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new qe6(oe6, userRepository, summariesRepository, portfolioApp);
    }
}
