package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q82 extends ls3 {
    @DexIgnore
    public /* final */ WeakReference<h82> b;

    @DexIgnore
    public q82(h82 h82) {
        this.b = new WeakReference<>(h82);
    }

    @DexIgnore
    @Override // com.fossil.ks3
    public final void Q2(us3 us3) {
        h82 h82 = this.b.get();
        if (h82 != null) {
            h82.f1448a.p(new p82(this, h82, h82, us3));
        }
    }
}
