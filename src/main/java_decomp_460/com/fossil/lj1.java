package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lj1<T> implements qj1<T> {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public bj1 d;

    @DexIgnore
    public lj1() {
        this(RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    }

    @DexIgnore
    public lj1(int i, int i2) {
        if (jk1.s(i, i2)) {
            this.b = i;
            this.c = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public final void a(pj1 pj1) {
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public final void d(bj1 bj1) {
        this.d = bj1;
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public void f(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public void h(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public final bj1 i() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public final void k(pj1 pj1) {
        pj1.d(this.b, this.c);
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStart() {
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStop() {
    }
}
