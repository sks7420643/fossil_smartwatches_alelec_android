package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d6 extends u5 {
    @DexIgnore
    public UUID[] k; // = new UUID[0];
    @DexIgnore
    public n6[] l; // = new n6[0];

    @DexIgnore
    public d6(n4 n4Var) {
        super(v5.e, n4Var);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.A();
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        return h7Var instanceof c7;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.g;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        c7 c7Var = (c7) h7Var;
        this.k = c7Var.b;
        this.l = c7Var.c;
    }
}
