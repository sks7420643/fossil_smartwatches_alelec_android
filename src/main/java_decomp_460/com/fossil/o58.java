package com.fossil;

import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o58 extends t58 {
    @DexIgnore
    public List missingOptions;

    @DexIgnore
    public o58(String str) {
        super(str);
    }

    @DexIgnore
    public o58(List list) {
        this(a(list));
        this.missingOptions = list;
    }

    @DexIgnore
    public static String a(List list) {
        StringBuffer stringBuffer = new StringBuffer("Missing required option");
        stringBuffer.append(list.size() == 1 ? "" : "s");
        stringBuffer.append(": ");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            stringBuffer.append(it.next());
            if (it.hasNext()) {
                stringBuffer.append(", ");
            }
        }
        return stringBuffer.toString();
    }

    @DexIgnore
    public List getMissingOptions() {
        return this.missingOptions;
    }
}
