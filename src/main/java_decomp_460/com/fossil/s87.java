package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s87 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public w87 f3220a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends s87 {
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ cb7 e;
        @DexIgnore
        public /* final */ Bitmap f;
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public /* final */ o87 j;
        @DexIgnore
        public w87 k;
        @DexIgnore
        public int l;
        @DexIgnore
        public int m;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, o87 o87, w87 w87, int i2, int i3) {
            super(w87, i2, i3, null);
            pq7.c(str, "complicationId");
            pq7.c(o87, "colorSpace");
            this.d = str;
            this.e = cb7;
            this.f = bitmap;
            this.g = z;
            this.h = str2;
            this.i = str3;
            this.j = o87;
            this.k = w87;
            this.l = i2;
            this.m = i3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(String str, cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, o87 o87, w87 w87, int i2, int i3, int i4, kq7 kq7) {
            this(str, cb7, bitmap, z, (i4 & 16) != 0 ? null : str2, (i4 & 32) != 0 ? null : str3, o87, (i4 & 128) != 0 ? null : w87, (i4 & 256) != 0 ? 0 : i2, (i4 & 512) != 0 ? 0 : i3);
        }

        @DexIgnore
        public static /* synthetic */ a f(a aVar, String str, cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, o87 o87, w87 w87, int i2, int i3, int i4, Object obj) {
            return aVar.e((i4 & 1) != 0 ? aVar.d : str, (i4 & 2) != 0 ? aVar.e : cb7, (i4 & 4) != 0 ? aVar.f : bitmap, (i4 & 8) != 0 ? aVar.g : z, (i4 & 16) != 0 ? aVar.h : str2, (i4 & 32) != 0 ? aVar.i : str3, (i4 & 64) != 0 ? aVar.j : o87, (i4 & 128) != 0 ? aVar.b() : w87, (i4 & 256) != 0 ? aVar.a() : i2, (i4 & 512) != 0 ? aVar.c() : i3);
        }

        @DexIgnore
        @Override // com.fossil.s87
        public int a() {
            return this.l;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public w87 b() {
            return this.k;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public int c() {
            return this.m;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public void d(w87 w87) {
            this.k = w87;
        }

        @DexIgnore
        public final a e(String str, cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, o87 o87, w87 w87, int i2, int i3) {
            pq7.c(str, "complicationId");
            pq7.c(o87, "colorSpace");
            return new a(str, cb7, bitmap, z, str2, str3, o87, w87, i2, i3);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.d, aVar.d) || !pq7.a(this.e, aVar.e) || !pq7.a(this.f, aVar.f) || this.g != aVar.g || !pq7.a(this.h, aVar.h) || !pq7.a(this.i, aVar.i) || !pq7.a(this.j, aVar.j) || !pq7.a(b(), aVar.b()) || a() != aVar.a() || c() != aVar.c()) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Bitmap g() {
            return this.f;
        }

        @DexIgnore
        public final o87 h() {
            return this.j;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            String str = this.d;
            int hashCode = str != null ? str.hashCode() : 0;
            cb7 cb7 = this.e;
            int hashCode2 = cb7 != null ? cb7.hashCode() : 0;
            Bitmap bitmap = this.f;
            int hashCode3 = bitmap != null ? bitmap.hashCode() : 0;
            boolean z = this.g;
            if (z) {
                z = true;
            }
            String str2 = this.h;
            int hashCode4 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.i;
            int hashCode5 = str3 != null ? str3.hashCode() : 0;
            o87 o87 = this.j;
            int hashCode6 = o87 != null ? o87.hashCode() : 0;
            w87 b = b();
            if (b != null) {
                i2 = b.hashCode();
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            return (((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i2) * 31) + a()) * 31) + c();
        }

        @DexIgnore
        public final String i() {
            return this.d;
        }

        @DexIgnore
        public final String j() {
            return this.i;
        }

        @DexIgnore
        public final cb7 k() {
            return this.e;
        }

        @DexIgnore
        public final String l() {
            return this.h;
        }

        @DexIgnore
        public final boolean m() {
            return this.g;
        }

        @DexIgnore
        public String toString() {
            return "ComplicationConfig(complicationId=" + this.d + ", ring=" + this.e + ", bitmap=" + this.f + ", isGoalRingEnable=" + this.g + ", setting=" + this.h + ", data=" + this.i + ", colorSpace=" + this.j + ", metric=" + b() + ", index=" + a() + ", previousIndex=" + c() + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s87 {
        @DexIgnore
        public /* final */ x87 d;
        @DexIgnore
        public o87 e;
        @DexIgnore
        public List<x87> f;
        @DexIgnore
        public Bitmap g;
        @DexIgnore
        public w87 h;
        @DexIgnore
        public int i;
        @DexIgnore
        public int j;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(o87 o87, List<x87> list, Bitmap bitmap, w87 w87, int i2, int i3) {
            super(w87, i2, i3, null);
            pq7.c(o87, "colorSpace");
            pq7.c(list, "stickerInfoList");
            this.e = o87;
            this.f = list;
            this.g = bitmap;
            this.h = w87;
            this.i = i2;
            this.j = i3;
            x87 x87 = (x87) pm7.I(list, o87.ordinal());
            this.d = x87 == null ? (x87) pm7.H(this.f) : x87;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(o87 o87, List list, Bitmap bitmap, w87 w87, int i2, int i3, int i4, kq7 kq7) {
            this(o87, list, (i4 & 4) != 0 ? null : bitmap, (i4 & 8) == 0 ? w87 : null, (i4 & 16) != 0 ? 0 : i2, (i4 & 32) == 0 ? i3 : 0);
        }

        @DexIgnore
        public static /* synthetic */ b g(b bVar, o87 o87, List list, Bitmap bitmap, w87 w87, int i2, int i3, int i4, Object obj) {
            return bVar.f((i4 & 1) != 0 ? bVar.e : o87, (i4 & 2) != 0 ? bVar.f : list, (i4 & 4) != 0 ? bVar.g : bitmap, (i4 & 8) != 0 ? bVar.b() : w87, (i4 & 16) != 0 ? bVar.a() : i2, (i4 & 32) != 0 ? bVar.c() : i3);
        }

        @DexIgnore
        @Override // com.fossil.s87
        public int a() {
            return this.i;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public w87 b() {
            return this.h;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public int c() {
            return this.j;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public void d(w87 w87) {
            this.h = w87;
        }

        @DexIgnore
        public final boolean e() {
            boolean z;
            List<x87> list = this.f;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    if (b97.c.c(it.next().b()) != null) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (!z) {
                        return false;
                    }
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!pq7.a(this.e, bVar.e) || !pq7.a(this.f, bVar.f) || !pq7.a(this.g, bVar.g) || !pq7.a(b(), bVar.b()) || a() != bVar.a() || c() != bVar.c()) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final b f(o87 o87, List<x87> list, Bitmap bitmap, w87 w87, int i2, int i3) {
            pq7.c(o87, "colorSpace");
            pq7.c(list, "stickerInfoList");
            return new b(o87, list, bitmap, w87, i2, i3);
        }

        @DexIgnore
        public final o87 h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            o87 o87 = this.e;
            int hashCode = o87 != null ? o87.hashCode() : 0;
            List<x87> list = this.f;
            int hashCode2 = list != null ? list.hashCode() : 0;
            Bitmap bitmap = this.g;
            int hashCode3 = bitmap != null ? bitmap.hashCode() : 0;
            w87 b = b();
            if (b != null) {
                i2 = b.hashCode();
            }
            return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i2) * 31) + a()) * 31) + c();
        }

        @DexIgnore
        public final Bitmap i() {
            x87 x87 = (x87) pm7.I(this.f, this.e.ordinal());
            if (x87 != null) {
                return x87.a();
            }
            return null;
        }

        @DexIgnore
        public final x87 j() {
            return this.d;
        }

        @DexIgnore
        public final Bitmap k() {
            x87 x87 = (x87) pm7.H(this.f);
            if (x87 != null) {
                return x87.a();
            }
            return null;
        }

        @DexIgnore
        public final Bitmap l() {
            return this.g;
        }

        @DexIgnore
        public final List<x87> m() {
            return this.f;
        }

        @DexIgnore
        public final void n(o87 o87) {
            pq7.c(o87, "<set-?>");
            this.e = o87;
        }

        @DexIgnore
        public void o(int i2) {
            this.i = i2;
        }

        @DexIgnore
        public void p(int i2) {
            this.j = i2;
        }

        @DexIgnore
        public final void q(Bitmap bitmap) {
            this.g = bitmap;
        }

        @DexIgnore
        public String toString() {
            return "StickerConfig(colorSpace=" + this.e + ", stickerInfoList=" + this.f + ", rawBitmap=" + this.g + ", metric=" + b() + ", index=" + a() + ", previousIndex=" + c() + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends s87 {
        @DexIgnore
        public String d;
        @DexIgnore
        public Typeface e;
        @DexIgnore
        public float f;
        @DexIgnore
        public o87 g;
        @DexIgnore
        public w87 h;
        @DexIgnore
        public int i;
        @DexIgnore
        public int j;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, Typeface typeface, float f2, o87 o87, w87 w87, int i2, int i3) {
            super(w87, i2, i3, null);
            pq7.c(str, "text");
            pq7.c(o87, "colorSpace");
            this.d = str;
            this.e = typeface;
            this.f = f2;
            this.g = o87;
            this.h = w87;
            this.i = i2;
            this.j = i3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(String str, Typeface typeface, float f2, o87 o87, w87 w87, int i2, int i3, int i4, kq7 kq7) {
            this(str, typeface, f2, o87, (i4 & 16) != 0 ? null : w87, (i4 & 32) != 0 ? 0 : i2, (i4 & 64) != 0 ? 0 : i3);
        }

        @DexIgnore
        public static /* synthetic */ c f(c cVar, String str, Typeface typeface, float f2, o87 o87, w87 w87, int i2, int i3, int i4, Object obj) {
            return cVar.e((i4 & 1) != 0 ? cVar.d : str, (i4 & 2) != 0 ? cVar.e : typeface, (i4 & 4) != 0 ? cVar.f : f2, (i4 & 8) != 0 ? cVar.g : o87, (i4 & 16) != 0 ? cVar.b() : w87, (i4 & 32) != 0 ? cVar.a() : i2, (i4 & 64) != 0 ? cVar.c() : i3);
        }

        @DexIgnore
        @Override // com.fossil.s87
        public int a() {
            return this.i;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public w87 b() {
            return this.h;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public int c() {
            return this.j;
        }

        @DexIgnore
        @Override // com.fossil.s87
        public void d(w87 w87) {
            this.h = w87;
        }

        @DexIgnore
        public final c e(String str, Typeface typeface, float f2, o87 o87, w87 w87, int i2, int i3) {
            pq7.c(str, "text");
            pq7.c(o87, "colorSpace");
            return new c(str, typeface, f2, o87, w87, i2, i3);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (!pq7.a(this.d, cVar.d) || !pq7.a(this.e, cVar.e) || Float.compare(this.f, cVar.f) != 0 || !pq7.a(this.g, cVar.g) || !pq7.a(b(), cVar.b()) || a() != cVar.a() || c() != cVar.c()) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final o87 g() {
            return this.g;
        }

        @DexIgnore
        public final float h() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            String str = this.d;
            int hashCode = str != null ? str.hashCode() : 0;
            Typeface typeface = this.e;
            int hashCode2 = typeface != null ? typeface.hashCode() : 0;
            int floatToIntBits = Float.floatToIntBits(this.f);
            o87 o87 = this.g;
            int hashCode3 = o87 != null ? o87.hashCode() : 0;
            w87 b = b();
            if (b != null) {
                i2 = b.hashCode();
            }
            return (((((((((((hashCode * 31) + hashCode2) * 31) + floatToIntBits) * 31) + hashCode3) * 31) + i2) * 31) + a()) * 31) + c();
        }

        @DexIgnore
        public final String i() {
            return this.d;
        }

        @DexIgnore
        public final Typeface j() {
            return this.e;
        }

        @DexIgnore
        public final void k(o87 o87) {
            pq7.c(o87, "<set-?>");
            this.g = o87;
        }

        @DexIgnore
        public final void l(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public void m(int i2) {
            this.i = i2;
        }

        @DexIgnore
        public void n(int i2) {
            this.j = i2;
        }

        @DexIgnore
        public final void o(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void p(Typeface typeface) {
            this.e = typeface;
        }

        @DexIgnore
        public String toString() {
            return "TextConfig(text=" + this.d + ", typeface=" + this.e + ", fontSize=" + this.f + ", colorSpace=" + this.g + ", metric=" + b() + ", index=" + a() + ", previousIndex=" + c() + ")";
        }
    }

    @DexIgnore
    public s87(w87 w87, int i, int i2) {
        this.f3220a = w87;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public /* synthetic */ s87(w87 w87, int i, int i2, kq7 kq7) {
        this(w87, i, i2);
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public w87 b() {
        return this.f3220a;
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public void d(w87 w87) {
        this.f3220a = w87;
    }
}
