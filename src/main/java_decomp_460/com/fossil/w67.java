package com.fossil;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w67 extends FrameLayout {
    @DexIgnore
    public a b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;

    @DexIgnore
    public interface a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w67$a$a")
        /* renamed from: com.fossil.w67$a$a  reason: collision with other inner class name */
        public static final class C0267a {
            @DexIgnore
            public static /* synthetic */ boolean a(a aVar, float f, float f2, float f3, int i, Object obj) {
                if (obj == null) {
                    if ((i & 4) != 0) {
                        f3 = 1.0f;
                    }
                    return aVar.d(f, f2, f3);
                }
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: isReachMaximumSize");
            }

            @DexIgnore
            public static /* synthetic */ boolean b(a aVar, float f, float f2, float f3, int i, Object obj) {
                if (obj == null) {
                    if ((i & 4) != 0) {
                        f3 = 1.0f;
                    }
                    return aVar.f(f, f2, f3);
                }
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: isReachMinimumSize");
            }
        }

        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        int c();

        @DexIgnore
        boolean d(float f, float f2, float f3);

        @DexIgnore
        boolean e(w67 w67);

        @DexIgnore
        boolean f(float f, float f2, float f3);

        @DexIgnore
        WatchFaceEditorView.a g();

        @DexIgnore
        boolean j(MotionEvent motionEvent);

        @DexIgnore
        void k(s87 s87);

        @DexIgnore
        void l(w67 w67);

        @DexIgnore
        void m(t67 t67);

        @DexIgnore
        boolean n(w67 w67);

        @DexIgnore
        boolean q(w67 w67);
    }

    @DexIgnore
    public enum b {
        UPDATED,
        ADDED,
        REMOVED
    }

    @DexIgnore
    public enum c {
        TEXT,
        STICKER,
        COMPLICATION,
        NONE
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w67(Context context) {
        super(context);
        pq7.c(context, "context");
    }

    @DexIgnore
    public static /* synthetic */ s87 d(w67 w67, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            return w67.c(z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: extractConfig");
    }

    @DexIgnore
    public boolean a() {
        a aVar = this.b;
        if (aVar != null) {
            return aVar.n(this);
        }
        return false;
    }

    @DexIgnore
    public final void b() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.l(this);
        }
    }

    @DexIgnore
    public abstract s87 c(boolean z);

    @DexIgnore
    public final void e() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.b();
        }
    }

    @DexIgnore
    public final void f() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public void g() {
        setOnTouchListener(new o67(this));
    }

    @DexIgnore
    public final a getHandler() {
        return this.b;
    }

    @DexIgnore
    public final float getMPrevTranslateX() {
        return this.g;
    }

    @DexIgnore
    public final float getMPrevTranslateY() {
        return this.h;
    }

    @DexIgnore
    public final w87 getMetric() {
        return new w87(getTranslationX(), getTranslationY(), (float) getWidth(), (float) getHeight(), getScaleX());
    }

    @DexIgnore
    public final int getTouchCount$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public abstract c getType();

    @DexIgnore
    public final boolean h() {
        return this.e;
    }

    @DexIgnore
    public final boolean i() {
        a aVar = this.b;
        if (aVar != null) {
            return aVar.e(this);
        }
        return false;
    }

    @DexIgnore
    public final boolean j() {
        return this.c;
    }

    @DexIgnore
    public final boolean k(MotionEvent motionEvent) {
        pq7.c(motionEvent, Constants.EVENT);
        a aVar = this.b;
        if (aVar != null) {
            return aVar.j(motionEvent);
        }
        return false;
    }

    @DexIgnore
    public final boolean l() {
        return this.d;
    }

    @DexIgnore
    public final void m() {
        s87 c2 = c(true);
        a aVar = this.b;
        if (aVar != null) {
            aVar.k(c2);
        }
    }

    @DexIgnore
    public final void n() {
        this.f = 0;
    }

    @DexIgnore
    public abstract void o();

    @DexIgnore
    public final boolean p() {
        a aVar = this.b;
        if (!pq7.a(aVar != null ? Boolean.valueOf(aVar.q(this)) : null, Boolean.TRUE)) {
            return false;
        }
        n();
        return true;
    }

    @DexIgnore
    public final void q() {
        this.g = getTranslationX();
        this.h = getTranslationY();
    }

    @DexIgnore
    public final void r(float f2, float f3) {
        setTranslationX(f2);
        setTranslationY(f3);
    }

    @DexIgnore
    public final void s(float f2, float f3) {
        setTranslationX(f2);
        setTranslationY(f3);
        this.g = f2;
        this.h = f3;
    }

    @DexIgnore
    public final void setAnimating$app_fossilRelease(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void setEditing$app_fossilRelease(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final void setElementEventHandler(a aVar) {
        pq7.c(aVar, "handler");
        this.b = aVar;
    }

    @DexIgnore
    public final void setHandler(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public final void setMPrevTranslateX(float f2) {
        this.g = f2;
    }

    @DexIgnore
    public final void setMPrevTranslateY(float f2) {
        this.h = f2;
    }

    @DexIgnore
    public final void setTouchCount$app_fossilRelease(int i) {
        this.f = i;
    }

    @DexIgnore
    public final void setTranslating$app_fossilRelease(boolean z) {
        this.d = z;
    }
}
