package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x76 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ w76 f4048a;

    @DexIgnore
    public x76(w76 w76) {
        pq7.c(w76, "mCommuteTimeSettingsDefaultAddressContractView");
        this.f4048a = w76;
    }

    @DexIgnore
    public final w76 a() {
        return this.f4048a;
    }
}
