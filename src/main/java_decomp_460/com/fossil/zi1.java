package com.fossil;

import com.fossil.cj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zi1 implements cj1, bj1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f4474a;
    @DexIgnore
    public /* final */ cj1 b;
    @DexIgnore
    public volatile bj1 c;
    @DexIgnore
    public volatile bj1 d;
    @DexIgnore
    public cj1.a e;
    @DexIgnore
    public cj1.a f;

    @DexIgnore
    public zi1(Object obj, cj1 cj1) {
        cj1.a aVar = cj1.a.CLEARED;
        this.e = aVar;
        this.f = aVar;
        this.f4474a = obj;
        this.b = cj1;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public void a(bj1 bj1) {
        synchronized (this.f4474a) {
            if (!bj1.equals(this.d)) {
                this.e = cj1.a.FAILED;
                if (this.f != cj1.a.RUNNING) {
                    this.f = cj1.a.RUNNING;
                    this.d.f();
                }
                return;
            }
            this.f = cj1.a.FAILED;
            if (this.b != null) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1, com.fossil.cj1
    public boolean b() {
        boolean z;
        synchronized (this.f4474a) {
            z = this.c.b() || this.d.b();
        }
        return z;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.cj1] */
    /* JADX WARN: Type inference failed for: r2v3 */
    @Override // com.fossil.cj1
    public cj1 c() {
        Object r2;
        synchronized (this.f4474a) {
            cj1 cj1 = this.b;
            this = this;
            if (cj1 != null) {
                r2 = this.b.c();
            }
        }
        return r2;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void clear() {
        synchronized (this.f4474a) {
            this.e = cj1.a.CLEARED;
            this.c.clear();
            if (this.f != cj1.a.CLEARED) {
                this.f = cj1.a.CLEARED;
                this.d.clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public boolean d(bj1 bj1) {
        boolean z;
        synchronized (this.f4474a) {
            z = n() && l(bj1);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public boolean e(bj1 bj1) {
        boolean z;
        synchronized (this.f4474a) {
            z = o() && l(bj1);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void f() {
        synchronized (this.f4474a) {
            if (this.e != cj1.a.RUNNING) {
                this.e = cj1.a.RUNNING;
                this.c.f();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean g(bj1 bj1) {
        if (!(bj1 instanceof zi1)) {
            return false;
        }
        zi1 zi1 = (zi1) bj1;
        return this.c.g(zi1.c) && this.d.g(zi1.d);
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean h() {
        boolean z;
        synchronized (this.f4474a) {
            z = this.e == cj1.a.CLEARED && this.f == cj1.a.CLEARED;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public void i(bj1 bj1) {
        synchronized (this.f4474a) {
            if (bj1.equals(this.c)) {
                this.e = cj1.a.SUCCESS;
            } else if (bj1.equals(this.d)) {
                this.f = cj1.a.SUCCESS;
            }
            if (this.b != null) {
                this.b.i(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean isRunning() {
        boolean z;
        synchronized (this.f4474a) {
            z = this.e == cj1.a.RUNNING || this.f == cj1.a.RUNNING;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public boolean j() {
        boolean z;
        synchronized (this.f4474a) {
            z = this.e == cj1.a.SUCCESS || this.f == cj1.a.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.cj1
    public boolean k(bj1 bj1) {
        boolean z;
        synchronized (this.f4474a) {
            z = m() && l(bj1);
        }
        return z;
    }

    @DexIgnore
    public final boolean l(bj1 bj1) {
        return bj1.equals(this.c) || (this.e == cj1.a.FAILED && bj1.equals(this.d));
    }

    @DexIgnore
    public final boolean m() {
        cj1 cj1 = this.b;
        return cj1 == null || cj1.k(this);
    }

    @DexIgnore
    public final boolean n() {
        cj1 cj1 = this.b;
        return cj1 == null || cj1.d(this);
    }

    @DexIgnore
    public final boolean o() {
        cj1 cj1 = this.b;
        return cj1 == null || cj1.e(this);
    }

    @DexIgnore
    public void p(bj1 bj1, bj1 bj12) {
        this.c = bj1;
        this.d = bj12;
    }

    @DexIgnore
    @Override // com.fossil.bj1
    public void pause() {
        synchronized (this.f4474a) {
            if (this.e == cj1.a.RUNNING) {
                this.e = cj1.a.PAUSED;
                this.c.pause();
            }
            if (this.f == cj1.a.RUNNING) {
                this.f = cj1.a.PAUSED;
                this.d.pause();
            }
        }
    }
}
