package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ew0 extends uv0 {
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public vv0 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public RenderScript f998a;
        @DexIgnore
        public int b; // = 1;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public int g;
        @DexIgnore
        public vv0 h;

        @DexIgnore
        public a(RenderScript renderScript, vv0 vv0) {
            vv0.a();
            this.f998a = renderScript;
            this.h = vv0;
        }

        @DexIgnore
        public ew0 a() {
            if (this.d > 0) {
                if (this.b < 1 || this.c < 1) {
                    throw new zv0("Both X and Y dimension required when Z is present.");
                } else if (this.f) {
                    throw new zv0("Cube maps not supported with 3D types.");
                }
            }
            if (this.c > 0 && this.b < 1) {
                throw new zv0("X dimension required when Y is present.");
            } else if (this.f && this.c < 1) {
                throw new zv0("Cube maps require 2D Types.");
            } else if (this.g == 0 || (this.d == 0 && !this.f && !this.e)) {
                RenderScript renderScript = this.f998a;
                ew0 ew0 = new ew0(renderScript.F(this.h.c(renderScript), this.b, this.c, this.d, this.e, this.f, this.g), this.f998a);
                ew0.k = this.h;
                ew0.d = this.b;
                ew0.e = this.c;
                ew0.f = this.d;
                ew0.g = this.e;
                ew0.h = this.f;
                ew0.i = this.g;
                ew0.f();
                return ew0;
            } else {
                throw new zv0("YUV only supports basic 2D.");
            }
        }

        @DexIgnore
        public a b(boolean z) {
            this.e = z;
            return this;
        }

        @DexIgnore
        public a c(int i) {
            if (i >= 1) {
                this.b = i;
                return this;
            }
            throw new yv0("Values of less than 1 for Dimension X are not valid.");
        }

        @DexIgnore
        public a d(int i) {
            if (i >= 1) {
                this.c = i;
                return this;
            }
            throw new yv0("Values of less than 1 for Dimension Y are not valid.");
        }
    }

    @DexIgnore
    public enum b {
        POSITIVE_X(0),
        NEGATIVE_X(1),
        POSITIVE_Y(2),
        NEGATIVE_Y(3),
        POSITIVE_Z(4),
        NEGATIVE_Z(5);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public b(int i) {
            this.mID = i;
        }
    }

    @DexIgnore
    public ew0(long j2, RenderScript renderScript) {
        super(j2, renderScript);
    }

    @DexIgnore
    public void f() {
        boolean n = n();
        int j2 = j();
        int k2 = k();
        int l = l();
        int i2 = m() ? 6 : 1;
        if (j2 == 0) {
            j2 = 1;
        }
        if (k2 == 0) {
            k2 = 1;
        }
        if (l == 0) {
            l = 1;
        }
        int i3 = j2 * k2 * l * i2;
        while (n && (j2 > 1 || k2 > 1 || l > 1)) {
            if (j2 > 1) {
                j2 >>= 1;
            }
            if (k2 > 1) {
                k2 >>= 1;
            }
            if (l > 1) {
                l >>= 1;
            }
            i3 += j2 * k2 * l * i2;
        }
        this.j = i3;
    }

    @DexIgnore
    public int g() {
        return this.j;
    }

    @DexIgnore
    public long h(RenderScript renderScript, long j2) {
        return renderScript.z(j2, this.d, this.e, this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    public vv0 i() {
        return this.k;
    }

    @DexIgnore
    public int j() {
        return this.d;
    }

    @DexIgnore
    public int k() {
        return this.e;
    }

    @DexIgnore
    public int l() {
        return this.f;
    }

    @DexIgnore
    public boolean m() {
        return this.h;
    }

    @DexIgnore
    public boolean n() {
        return this.g;
    }
}
