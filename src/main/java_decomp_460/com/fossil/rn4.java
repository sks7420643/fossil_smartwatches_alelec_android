package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ byte[] f3133a;
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public rn4(int i) {
        this.f3133a = new byte[i];
    }

    @DexIgnore
    public void a(boolean z, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.b;
            this.b = i3 + 1;
            c(i3, z);
        }
    }

    @DexIgnore
    public byte[] b(int i) {
        int length = this.f3133a.length * i;
        byte[] bArr = new byte[length];
        for (int i2 = 0; i2 < length; i2++) {
            bArr[i2] = (byte) this.f3133a[i2 / i];
        }
        return bArr;
    }

    @DexIgnore
    public final void c(int i, boolean z) {
        this.f3133a[i] = (byte) (z ? (byte) 1 : 0);
    }
}
