package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class fu extends Enum<fu> {
    @DexIgnore
    public static /* final */ fu h;
    @DexIgnore
    public static /* final */ fu i;
    @DexIgnore
    public static /* final */ fu j;
    @DexIgnore
    public static /* final */ fu k;
    @DexIgnore
    public static /* final */ fu l;
    @DexIgnore
    public static /* final */ fu m;
    @DexIgnore
    public static /* final */ fu n;
    @DexIgnore
    public static /* final */ fu o;
    @DexIgnore
    public static /* final */ fu p;
    @DexIgnore
    public static /* final */ fu q;
    @DexIgnore
    public static /* final */ fu r;
    @DexIgnore
    public static /* final */ fu s;
    @DexIgnore
    public static /* final */ fu t;
    @DexIgnore
    public static /* final */ fu u;
    @DexIgnore
    public static /* final */ /* synthetic */ fu[] v;
    @DexIgnore
    public /* final */ yt b;
    @DexIgnore
    public /* final */ zt c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ yt e;
    @DexIgnore
    public /* final */ zt f;
    @DexIgnore
    public /* final */ byte[] g;

    /*
    static {
        fu fuVar = new fu("GET_CONNECTION_PARAMETERS", 0, yt.GET, zt.CURRENT_CONNECTION_PARAMETERS, null, null, null, null, 60);
        h = fuVar;
        fu fuVar2 = new fu("REQUEST_CONNECTION_PRIORITY", 1, yt.SET, zt.CONNECTION_PARAMETERS_REQUEST, null, null, zt.CURRENT_CONNECTION_PARAMETERS, null, 44);
        i = fuVar2;
        fu fuVar3 = new fu("PLAY_ANIMATION", 2, yt.SET, zt.DIAGNOSTIC_FUNCTIONS, au.PAIR_ANIMATION.b, null, null, null, 56);
        j = fuVar3;
        yt ytVar = yt.GET;
        zt ztVar = zt.DIAGNOSTIC_FUNCTIONS;
        byte[] bArr = au.OPTIMAL_PAYLOAD.b;
        fu fuVar4 = new fu("GET_OPTIMAL_PAYLOAD", 3, ytVar, ztVar, bArr, null, null, bArr, 24);
        k = fuVar4;
        yt ytVar2 = yt.GET;
        zt ztVar2 = zt.DIAGNOSTIC_FUNCTIONS;
        byte[] bArr2 = au.BLE_TROUBLESHOOT.b;
        fu fuVar5 = new fu("BLE_TROUBLESHOOT", 4, ytVar2, ztVar2, bArr2, null, null, bArr2, 24);
        fu fuVar6 = new fu("REQUEST_HANDS", 5, yt.SET, zt.CALIBRATION_SETTING, bu.REQUEST_HANDS.b, null, null, null, 56);
        l = fuVar6;
        fu fuVar7 = new fu("RELEASE_HANDS", 6, yt.SET, zt.CALIBRATION_SETTING, bu.RELEASE_HANDS.b, null, null, null, 56);
        m = fuVar7;
        fu fuVar8 = new fu("MOVE_HANDS", 7, yt.SET, zt.CALIBRATION_SETTING, bu.MOVE_HANDS.b, null, null, null, 56);
        n = fuVar8;
        fu fuVar9 = new fu("SET_CALIBRATION_POSITION", 8, yt.SET, zt.HARDWARE_TEST, bu.SET_CALIBRATION_POSITION.b, null, null, null, 56);
        o = fuVar9;
        yt ytVar3 = yt.GET;
        zt ztVar3 = zt.WORKOUT_SESSION_PRIMARY_ID;
        byte[] bArr3 = eu.WORKOUT_SESSION_CONTROL.b;
        fu fuVar10 = new fu("GET_CURRENT_WORKOUT_SESSION", 9, ytVar3, ztVar3, bArr3, null, null, bArr3, 24);
        p = fuVar10;
        fu fuVar11 = new fu("STOP_CURRENT_WORKOUT_SESSION", 10, yt.SET, zt.WORKOUT_SESSION_PRIMARY_ID, eu.WORKOUT_SESSION_CONTROL.b, null, null, null, 56);
        q = fuVar11;
        fu fuVar12 = new fu("GET_HEARTBEAT_STATISTIC", 11, yt.GET, zt.DIAGNOSTIC_FUNCTIONS, au.HEARTBEAT_STATISTIC.b, yt.RESPONSE, zt.DIAGNOSTIC_FUNCTIONS, au.HEARTBEAT_STATISTIC.b);
        fu fuVar13 = new fu("GET_HEARTBEAT_INTERVAL", 12, yt.GET, zt.STREAMING_CONFIG, du.HEARTBEAT_INTERVAL.b, null, null, null, 56);
        fu fuVar14 = new fu("SET_HEARTBEAT_INTERVAL", 13, yt.SET, zt.STREAMING_CONFIG, du.HEARTBEAT_INTERVAL.b, null, null, null, 56);
        r = fuVar14;
        fu fuVar15 = new fu("REQUEST_DISCOVER_SERVICE", 14, yt.SET, zt.ADVANCE_BLE_REQUEST, xt.REQUEST_DISCOVER_SERVICE.b, null, null, null, 56);
        fu fuVar16 = new fu("CLEAN_UP_DEVICE", 15, yt.SET, zt.DIAGNOSTIC_FUNCTIONS, xt.CLEAN_UP_DEVICE.b, null, null, null, 56);
        s = fuVar16;
        fu fuVar17 = new fu("LEGACY_OTA_ENTER", 16, yt.SET, zt.DIAGNOSTIC_FUNCTIONS, cu.LEGACY_OTA_ENTER.b, null, null, cu.LEGACY_OTA_ENTER_RESPONSE.b, 24);
        t = fuVar17;
        fu fuVar18 = new fu("LEGACY_OTA_RESET", 17, yt.SET, zt.DIAGNOSTIC_FUNCTIONS, cu.LEGACY_OTA_RESET.b, null, null, null, 56);
        u = fuVar18;
        v = new fu[]{fuVar, fuVar2, fuVar3, fuVar4, fuVar5, fuVar6, fuVar7, fuVar8, fuVar9, fuVar10, fuVar11, fuVar12, fuVar13, fuVar14, fuVar15, fuVar16, fuVar17, fuVar18};
    }
    */

    @DexIgnore
    public fu(String str, int i2, yt ytVar, zt ztVar, byte[] bArr, yt ytVar2, zt ztVar2, byte[] bArr2) {
        this.b = ytVar;
        this.c = ztVar;
        this.d = bArr;
        this.e = ytVar2;
        this.f = ztVar2;
        this.g = bArr2;
    }

    @DexIgnore
    public /* synthetic */ fu(String str, int i2, yt ytVar, zt ztVar, byte[] bArr, yt ytVar2, zt ztVar2, byte[] bArr2, int i3) {
        bArr = (i3 & 4) != 0 ? new byte[0] : bArr;
        ytVar2 = (i3 & 8) != 0 ? yt.RESPONSE : ytVar2;
        ztVar2 = (i3 & 16) != 0 ? ztVar : ztVar2;
        bArr2 = (i3 & 32) != 0 ? new byte[0] : bArr2;
        this.b = ytVar;
        this.c = ztVar;
        this.d = bArr;
        this.e = ytVar2;
        this.f = ztVar2;
        this.g = bArr2;
    }

    @DexIgnore
    public static fu valueOf(String str) {
        return (fu) Enum.valueOf(fu.class, str);
    }

    @DexIgnore
    public static fu[] values() {
        return (fu[]) v.clone();
    }
}
