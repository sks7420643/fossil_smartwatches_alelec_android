package com.fossil;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zf2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f4463a;

    @DexIgnore
    public zf2(Context context) {
        this.f4463a = context;
    }

    @DexIgnore
    public int a(String str) {
        return this.f4463a.checkCallingOrSelfPermission(str);
    }

    @DexIgnore
    public int b(String str, String str2) {
        return this.f4463a.getPackageManager().checkPermission(str, str2);
    }

    @DexIgnore
    public ApplicationInfo c(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f4463a.getPackageManager().getApplicationInfo(str, i);
    }

    @DexIgnore
    public CharSequence d(String str) throws PackageManager.NameNotFoundException {
        return this.f4463a.getPackageManager().getApplicationLabel(this.f4463a.getPackageManager().getApplicationInfo(str, 0));
    }

    @DexIgnore
    public PackageInfo e(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f4463a.getPackageManager().getPackageInfo(str, i);
    }

    @DexIgnore
    public final String[] f(int i) {
        return this.f4463a.getPackageManager().getPackagesForUid(i);
    }

    @DexIgnore
    public boolean g() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return yf2.a(this.f4463a);
        }
        if (!mf2.j() || (nameForUid = this.f4463a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.f4463a.getPackageManager().isInstantApp(nameForUid);
    }

    @DexIgnore
    public final PackageInfo h(String str, int i, int i2) throws PackageManager.NameNotFoundException {
        return this.f4463a.getPackageManager().getPackageInfo(str, 64);
    }

    @DexIgnore
    @TargetApi(19)
    public final boolean i(int i, String str) {
        if (mf2.f()) {
            try {
                ((AppOpsManager) this.f4463a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException e) {
                return false;
            }
        } else {
            String[] packagesForUid = this.f4463a.getPackageManager().getPackagesForUid(i);
            if (str == null || packagesForUid == null) {
                return false;
            }
            for (String str2 : packagesForUid) {
                if (str.equals(str2)) {
                    return true;
                }
            }
            return false;
        }
    }
}
