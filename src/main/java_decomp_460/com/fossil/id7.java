package com.fossil;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.fossil.rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class id7 extends bd7 {
    @DexIgnore
    public static /* final */ String[] b; // = {"orientation"};

    @DexIgnore
    public enum a {
        MICRO(3, 96, 96),
        MINI(1, 512, 384),
        FULL(2, -1, -1);
        
        @DexIgnore
        public /* final */ int androidKind;
        @DexIgnore
        public /* final */ int height;
        @DexIgnore
        public /* final */ int width;

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.androidKind = i;
            this.width = i2;
            this.height = i3;
        }
    }

    @DexIgnore
    public id7(Context context) {
        super(context);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int k(android.content.ContentResolver r8, android.net.Uri r9) {
        /*
            r6 = 0
            r7 = 0
            java.lang.String[] r2 = com.fossil.id7.b     // Catch:{ RuntimeException -> 0x002f, all -> 0x0027 }
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r8
            r1 = r9
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ RuntimeException -> 0x002f, all -> 0x0027 }
            if (r1 == 0) goto L_0x0015
            boolean r0 = r1.moveToFirst()     // Catch:{ RuntimeException -> 0x0038, all -> 0x003a }
            if (r0 != 0) goto L_0x001c
        L_0x0015:
            if (r1 == 0) goto L_0x001a
            r1.close()
        L_0x001a:
            r0 = r6
        L_0x001b:
            return r0
        L_0x001c:
            r0 = 0
            int r0 = r1.getInt(r0)
            if (r1 == 0) goto L_0x001b
            r1.close()
            goto L_0x001b
        L_0x0027:
            r0 = move-exception
            r1 = r7
        L_0x0029:
            if (r1 == 0) goto L_0x002e
            r1.close()
        L_0x002e:
            throw r0
        L_0x002f:
            r0 = move-exception
            r1 = r7
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            r0 = r6
            goto L_0x001b
        L_0x0038:
            r0 = move-exception
            goto L_0x0031
        L_0x003a:
            r0 = move-exception
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.id7.k(android.content.ContentResolver, android.net.Uri):int");
    }

    @DexIgnore
    public static a l(int i, int i2) {
        a aVar = a.MICRO;
        if (i <= aVar.width && i2 <= aVar.height) {
            return aVar;
        }
        a aVar2 = a.MINI;
        return (i > aVar2.width || i2 > aVar2.height) ? a.FULL : aVar2;
    }

    @DexIgnore
    @Override // com.fossil.bd7, com.fossil.rd7
    public boolean c(pd7 pd7) {
        Uri uri = pd7.d;
        return "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    @DexIgnore
    @Override // com.fossil.bd7, com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        Bitmap thumbnail;
        ContentResolver contentResolver = this.f417a.getContentResolver();
        int k = k(contentResolver, pd7.d);
        String type = contentResolver.getType(pd7.d);
        boolean z = type != null && type.startsWith("video/");
        if (pd7.c()) {
            a l = l(pd7.h, pd7.i);
            if (!z && l == a.FULL) {
                return new rd7.a(null, j(pd7), Picasso.LoadedFrom.DISK, k);
            }
            long parseId = ContentUris.parseId(pd7.d);
            BitmapFactory.Options d = rd7.d(pd7);
            d.inJustDecodeBounds = true;
            rd7.a(pd7.h, pd7.i, l.width, l.height, d, pd7);
            if (z) {
                thumbnail = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, parseId, l == a.FULL ? 1 : l.androidKind, d);
            } else {
                thumbnail = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, parseId, l.androidKind, d);
            }
            if (thumbnail != null) {
                return new rd7.a(thumbnail, null, Picasso.LoadedFrom.DISK, k);
            }
        }
        return new rd7.a(null, j(pd7), Picasso.LoadedFrom.DISK, k);
    }
}
