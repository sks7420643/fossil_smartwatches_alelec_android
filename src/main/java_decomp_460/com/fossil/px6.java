package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.n04;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px6 extends ey6 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public hy6 h;
    @DexIgnore
    public py6 i;
    @DexIgnore
    public g37<v95> j;
    @DexIgnore
    public yq4 k; // = new yq4();
    @DexIgnore
    public String l; // = "";
    @DexIgnore
    public List<String> m; // = new ArrayList();
    @DexIgnore
    public int s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public int u;
    @DexIgnore
    public /* final */ String v; // = qn5.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String w; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String x; // = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final px6 a(List<String> list, boolean z) {
            pq7.c(list, "instructionList");
            px6 px6 = new px6();
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            px6.setArguments(bundle);
            px6.m = list;
            px6.s = 0;
            return px6;
        }

        @DexIgnore
        public final px6 b(String str, boolean z) {
            pq7.c(str, "serial");
            px6 px6 = new px6();
            px6.l = str;
            px6.s = 1;
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            px6.setArguments(bundle);
            return px6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ px6 f2887a;

        @DexIgnore
        public b(px6 px6) {
            this.f2887a = px6;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f2887a.v) && !TextUtils.isEmpty(this.f2887a.w)) {
                int parseColor = Color.parseColor(this.f2887a.v);
                int parseColor2 = Color.parseColor(this.f2887a.w);
                gVar.o(2131230966);
                if (i == this.f2887a.u) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ v95 f2888a;
        @DexIgnore
        public /* final */ /* synthetic */ px6 b;

        @DexIgnore
        public c(v95 v95, px6 px6) {
            this.f2888a = v95;
            this.b = px6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            Drawable e;
            Drawable e2;
            if (i == 0) {
                FlexibleTextView flexibleTextView = this.f2888a.w;
                pq7.b(flexibleTextView, "binding.ftvTimeout");
                flexibleTextView.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView2 = this.f2888a.w;
                pq7.b(flexibleTextView2, "binding.ftvTimeout");
                flexibleTextView2.setVisibility(4);
            }
            if (!TextUtils.isEmpty(this.b.w)) {
                int parseColor = Color.parseColor(this.b.w);
                TabLayout.g v = this.f2888a.z.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.v) && this.b.u != i) {
                int parseColor2 = Color.parseColor(this.b.v);
                TabLayout.g v2 = this.f2888a.z.v(this.b.u);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.u = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px6 b;

        @DexIgnore
        public d(px6 px6) {
            this.b = px6;
        }

        @DexIgnore
        public final void onClick(View view) {
            px6.M6(this.b).n();
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px6 b;

        @DexIgnore
        public e(px6 px6) {
            this.b = px6;
        }

        @DexIgnore
        public final void onClick(View view) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context context = this.b.getContext();
            if (context != null) {
                pq7.b(context, "context!!");
                aVar.b(context, this.b.l, true, this.b.t);
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px6 b;

        @DexIgnore
        public f(px6 px6) {
            this.b = px6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                pq7.b(activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = this.b.getActivity();
                    if (activity2 != null) {
                        pq7.b(activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = this.b.getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ hy6 M6(px6 px6) {
        hy6 hy6 = px6.h;
        if (hy6 != null) {
            return hy6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void U6(List<String> list) {
        pq7.c(list, "instructionList");
        this.k.i(list);
        g37<v95> g37 = this.j;
        if (g37 != null) {
            v95 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                pq7.b(constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.q;
                pq7.b(constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V6(String str) {
        pq7.c(str, "serial");
        this.l = str;
        g37<v95> g37 = this.j;
        if (g37 != null) {
            v95 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                pq7.b(constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.q;
                pq7.b(constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: W6 */
    public void M5(hy6 hy6) {
        pq7.c(hy6, "presenter");
        this.h = hy6;
    }

    @DexIgnore
    public final void X6(boolean z2) {
        py6 py6 = this.i;
        if (py6 != null) {
            py6.c(z2);
        } else {
            pq7.n("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void Y6(int i2) {
        g37<v95> g37 = this.j;
        if (g37 != null) {
            v95 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "it.ftvTimeout");
                hr7 hr7 = hr7.f1520a;
                Context context = getContext();
                if (context != null) {
                    String c2 = um5.c(context, 2131886892);
                    pq7.b(c2, "LanguageHelper.getString\u2026_ExpiringInSecondSeconds)");
                    String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    return;
                }
                pq7.i();
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        g37<v95> g37 = new g37<>(this, (v95) aq0.f(layoutInflater, 2131558602, viewGroup, false, A6()));
        this.j = g37;
        if (g37 != null) {
            v95 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        py6 py6 = this.i;
        if (py6 != null) {
            py6.b();
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mSubPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        this.k.i(this.m);
        this.i = new py6(this);
        Bundle arguments = getArguments();
        this.t = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        g37<v95> g37 = this.j;
        if (g37 != null) {
            v95 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.C;
                pq7.b(viewPager2, "binding.vpAuthorizeInstruction");
                viewPager2.setAdapter(this.k);
                if (!TextUtils.isEmpty(this.x)) {
                    TabLayout tabLayout = a2.z;
                    pq7.b(tabLayout, "binding.indicator");
                    tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.x)));
                }
                g37<v95> g372 = this.j;
                if (g372 != null) {
                    v95 a3 = g372.a();
                    TabLayout tabLayout2 = a3 != null ? a3.z : null;
                    if (tabLayout2 != null) {
                        g37<v95> g373 = this.j;
                        if (g373 != null) {
                            v95 a4 = g373.a();
                            ViewPager2 viewPager22 = a4 != null ? a4.C : null;
                            if (viewPager22 != null) {
                                new n04(tabLayout2, viewPager22, new b(this)).a();
                                a2.C.g(new c(a2, this));
                                a2.s.setOnClickListener(new d(this));
                                a2.v.setOnClickListener(new e(this));
                                a2.t.setOnClickListener(new f(this));
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            }
            int i2 = this.s;
            if (i2 == 0) {
                U6(this.m);
            } else if (i2 == 1) {
                V6(this.l);
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
