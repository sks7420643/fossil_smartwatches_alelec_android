package com.fossil;

import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y28 {
    @DexIgnore
    public static String a(v18 v18, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(v18.g());
        sb.append(' ');
        if (b(v18, type)) {
            sb.append(v18.j());
        } else {
            sb.append(c(v18.j()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(v18 v18, Proxy.Type type) {
        return !v18.f() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String c(q18 q18) {
        String h = q18.h();
        String j = q18.j();
        if (j == null) {
            return h;
        }
        return h + '?' + j;
    }
}
