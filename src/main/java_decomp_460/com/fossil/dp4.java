package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiUserService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp4 implements Factory<AuthApiUserService> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f818a;
    @DexIgnore
    public /* final */ Provider<qq5> b;
    @DexIgnore
    public /* final */ Provider<uq5> c;

    @DexIgnore
    public dp4(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        this.f818a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static dp4 a(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        return new dp4(uo4, provider, provider2);
    }

    @DexIgnore
    public static AuthApiUserService c(uo4 uo4, qq5 qq5, uq5 uq5) {
        AuthApiUserService k = uo4.k(qq5, uq5);
        lk7.c(k, "Cannot return null from a non-@Nullable @Provides method");
        return k;
    }

    @DexIgnore
    /* renamed from: b */
    public AuthApiUserService get() {
        return c(this.f818a, this.b.get(), this.c.get());
    }
}
