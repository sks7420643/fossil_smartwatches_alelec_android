package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vg7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;

    @DexIgnore
    public vg7(Context context) {
        this.b = context;
    }

    @DexIgnore
    public final void run() {
        try {
            new Thread(new bh7(this.b, null, null), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            ig7.m.e(th);
            ig7.f(this.b, th);
        }
    }
}
