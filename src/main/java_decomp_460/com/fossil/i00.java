package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class i00 extends Enum<i00> {
    @DexIgnore
    public static /* final */ i00 b;
    @DexIgnore
    public static /* final */ i00 c;
    @DexIgnore
    public static /* final */ i00 d;
    @DexIgnore
    public static /* final */ i00 e;
    @DexIgnore
    public static /* final */ i00 f;
    @DexIgnore
    public static /* final */ i00 g;
    @DexIgnore
    public static /* final */ /* synthetic */ i00[] h;

    /*
    static {
        i00 i00 = new i00("ENABLE_MAINTAINING_CONNECTION", 0);
        b = i00;
        i00 i002 = new i00("ENABLE_MAINTAINING_HID_CONNECTION", 1);
        i00 i003 = new i00("APP_DISCONNECT", 2);
        c = i003;
        i00 i004 = new i00("APP_DISCONNECT_HID", 3);
        i00 i005 = new i00("SET_SECRET_KEY", 4);
        d = i005;
        i00 i006 = new i00("DEVICE_STATE_CHANGED", 5);
        e = i006;
        i00 i007 = new i00("CLEAR_CACHE", 6);
        f = i007;
        i00 i008 = new i00("ENABLE_BACKGROUND_SYNC", 7);
        g = i008;
        h = new i00[]{i00, i002, i003, i004, i005, i006, i007, i008};
    }
    */

    @DexIgnore
    public i00(String str, int i) {
    }

    @DexIgnore
    public static i00 valueOf(String str) {
        return (i00) Enum.valueOf(i00.class, str);
    }

    @DexIgnore
    public static i00[] values() {
        return (i00[]) h.clone();
    }
}
