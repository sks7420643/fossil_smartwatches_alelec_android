package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.m47;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class io6 extends pv5 implements ho6, t47.g {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public g37<o15> g;
    @DexIgnore
    public go6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final io6 a() {
            return new io6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io6 b;

        @DexIgnore
        public b(io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io6 b;

        @DexIgnore
        public c(io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = m47.a(m47.c.PRIVACY, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Privacy Policy URL = " + a2);
            io6 io6 = this.b;
            pq7.b(a2, "url");
            io6.M6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io6 b;

        @DexIgnore
        public d(io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = m47.a(m47.c.TERMS, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Term Of Use URL = " + a2);
            io6 io6 = this.b;
            pq7.b(a2, "url");
            io6.M6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io6 b;

        @DexIgnore
        public e(io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = m47.a(m47.c.SOURCE_LICENSES, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Open Source Licenses URL = " + a2);
            io6 io6 = this.b;
            pq7.b(a2, "url");
            io6.M6(a2);
        }
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(go6 go6) {
        pq7.c(go6, "presenter");
        i14.l(go6);
        pq7.b(go6, "Preconditions.checkNotNull(presenter)");
        this.h = go6;
    }

    @DexIgnore
    public final void M6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), pp6.k.a());
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AboutFragment", "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof ls5)) {
            activity = null;
        }
        ls5 ls5 = (ls5) activity;
        if (ls5 != null) {
            ls5.R5(str, i2, intent);
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        o15 o15 = (o15) aq0.f(LayoutInflater.from(getContext()), 2131558490, null, false, A6());
        this.g = new g37<>(this, o15);
        pq7.b(o15, "binding");
        return o15.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        go6 go6 = this.h;
        if (go6 != null) {
            go6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        go6 go6 = this.h;
        if (go6 != null) {
            go6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<o15> g37 = this.g;
        if (g37 != null) {
            o15 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                if (!wr4.f3989a.a().j()) {
                    RelativeLayout relativeLayout = a2.r;
                    pq7.b(relativeLayout, "binding.btOpenSourceLicense");
                    relativeLayout.setVisibility(4);
                    View view2 = a2.C;
                    pq7.b(view2, "binding.vLine2");
                    view2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
