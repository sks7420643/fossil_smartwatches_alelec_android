package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa4 extends ta4.d.AbstractC0224d.c {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Double f2808a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.c.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Double f2809a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Long f;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c a() {
            String str = "";
            if (this.b == null) {
                str = " batteryVelocity";
            }
            if (this.c == null) {
                str = str + " proximityOn";
            }
            if (this.d == null) {
                str = str + " orientation";
            }
            if (this.e == null) {
                str = str + " ramUsed";
            }
            if (this.f == null) {
                str = str + " diskUsed";
            }
            if (str.isEmpty()) {
                return new pa4(this.f2809a, this.b.intValue(), this.c.booleanValue(), this.d.intValue(), this.e.longValue(), this.f.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c.a b(Double d2) {
            this.f2809a = d2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c.a c(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c.a d(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c.a e(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c.a f(boolean z) {
            this.c = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.c.a
        public ta4.d.AbstractC0224d.c.a g(long j) {
            this.e = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public pa4(Double d2, int i, boolean z, int i2, long j, long j2) {
        this.f2808a = d2;
        this.b = i;
        this.c = z;
        this.d = i2;
        this.e = j;
        this.f = j2;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.c
    public Double b() {
        return this.f2808a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.c
    public int c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.c
    public long d() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.c
    public int e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.c)) {
            return false;
        }
        ta4.d.AbstractC0224d.c cVar = (ta4.d.AbstractC0224d.c) obj;
        Double d2 = this.f2808a;
        if (d2 != null ? d2.equals(cVar.b()) : cVar.b() == null) {
            if (this.b == cVar.c() && this.c == cVar.g() && this.d == cVar.e() && this.e == cVar.f() && this.f == cVar.d()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.c
    public long f() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.c
    public boolean g() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        Double d2 = this.f2808a;
        int hashCode = d2 == null ? 0 : d2.hashCode();
        int i = this.b;
        int i2 = this.c ? 1231 : 1237;
        int i3 = this.d;
        long j = this.e;
        long j2 = this.f;
        return ((((((((((hashCode ^ 1000003) * 1000003) ^ i) * 1000003) ^ i2) * 1000003) ^ i3) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2));
    }

    @DexIgnore
    public String toString() {
        return "Device{batteryLevel=" + this.f2808a + ", batteryVelocity=" + this.b + ", proximityOn=" + this.c + ", orientation=" + this.d + ", ramUsed=" + this.e + ", diskUsed=" + this.f + "}";
    }
}
