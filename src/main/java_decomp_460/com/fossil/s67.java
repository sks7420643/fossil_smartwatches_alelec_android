package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.fossil.s87;
import com.fossil.w67;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s67 extends r67 {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public s87.b l;
    @DexIgnore
    public ImageView m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final cl7<s67, ViewGroup.LayoutParams> a(WatchFaceEditorView watchFaceEditorView, s87.b bVar, int i) {
            pq7.c(watchFaceEditorView, "editorView");
            pq7.c(bVar, "stickerConfig");
            ImageView imageView = new ImageView(watchFaceEditorView.getContext());
            Bitmap i2 = bVar.i();
            if (i2 == null) {
                i2 = bVar.l();
            }
            imageView.setImageBitmap(i2);
            w87 b = bVar.b();
            FrameLayout.LayoutParams layoutParams = b != null ? new FrameLayout.LayoutParams((int) b.c(), (int) b.a()) : new FrameLayout.LayoutParams(-2, -2);
            Context context = watchFaceEditorView.getContext();
            pq7.b(context, "editorView.context");
            s67 s67 = new s67(context, null);
            s67.z(imageView, bVar, layoutParams, i);
            s67.setElementEventHandler(watchFaceEditorView);
            return hl7.a(s67, new FrameLayout.LayoutParams(-2, -2));
        }

        @DexIgnore
        public final cl7<s67, ViewGroup.LayoutParams> b(Context context, s87.b bVar) {
            String str;
            pq7.c(context, "context");
            pq7.c(bVar, "stickerConfig");
            ImageView imageView = new ImageView(context);
            x87 j = bVar.j();
            if (j == null || (str = j.c()) == null) {
                str = "";
            }
            Bitmap i = bVar.i();
            BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bVar.l());
            if (i != null) {
                imageView.setImageBitmap(i);
            } else {
                if (str.length() > 0) {
                    ty4.a(imageView, str, bitmapDrawable);
                } else {
                    imageView.setImageBitmap(bVar.l());
                }
            }
            w87 b = bVar.b();
            if (b != null) {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) b.c(), (int) b.a());
                s67 s67 = new s67(context, null);
                s67.A(s67, imageView, bVar, layoutParams, 0, 8, null);
                return hl7.a(s67, new FrameLayout.LayoutParams(-2, -2));
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public s67(Context context) {
        super(context);
    }

    @DexIgnore
    public /* synthetic */ s67(Context context, kq7 kq7) {
        this(context);
    }

    @DexIgnore
    public static /* synthetic */ s67 A(s67 s67, ImageView imageView, s87.b bVar, ViewGroup.LayoutParams layoutParams, int i, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            i = 0;
        }
        s67.z(imageView, bVar, layoutParams, i);
        return s67;
    }

    @DexIgnore
    @Override // com.fossil.w67
    public s87 c(boolean z) {
        int i = 0;
        s87.b bVar = this.l;
        if (bVar != null) {
            bVar.d(getMetric());
        }
        if (z) {
            w67.a handler = getHandler();
            int c = handler != null ? handler.c() : 0;
            s87.b bVar2 = this.l;
            if (bVar2 != null) {
                if (bVar2 != null) {
                    i = bVar2.a();
                }
                bVar2.p(i);
            }
            s87.b bVar3 = this.l;
            if (bVar3 != null) {
                bVar3.o(c);
            }
        }
        s87.b bVar4 = this.l;
        if (bVar4 != null) {
            return bVar4;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w67
    public w67.c getType() {
        return w67.c.STICKER;
    }

    @DexIgnore
    @Override // com.fossil.w67
    public void o() {
        if (getTouchCount$app_fossilRelease() > 1) {
            x();
        }
    }

    @DexIgnore
    public final void x() {
        List<x87> m2;
        int i = 0;
        Bitmap bitmap = null;
        s87.b bVar = this.l;
        if (!(bVar == null || (m2 = bVar.m()) == null || ((m2 instanceof Collection) && m2.isEmpty()))) {
            Iterator<T> it = m2.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                if (it.next().a() != null) {
                    int i3 = i2 + 1;
                    if (i3 >= 0) {
                        i2 = i3;
                    } else {
                        hm7.k();
                        throw null;
                    }
                }
            }
            i = i2;
        }
        if (i >= 2) {
            s87.b bVar2 = this.l;
            o87 h = bVar2 != null ? bVar2.h() : null;
            o87 o87 = o87.WHITE;
            if (h == o87) {
                s87.b bVar3 = this.l;
                if (bVar3 != null) {
                    bVar3.n(o87.BLACK);
                }
                ImageView imageView = this.m;
                if (imageView != null) {
                    s87.b bVar4 = this.l;
                    if (bVar4 != null) {
                        bitmap = bVar4.i();
                    }
                    imageView.setImageBitmap(bitmap);
                }
            } else {
                s87.b bVar5 = this.l;
                if (bVar5 != null) {
                    bVar5.n(o87);
                }
                ImageView imageView2 = this.m;
                if (imageView2 != null) {
                    s87.b bVar6 = this.l;
                    if (bVar6 != null) {
                        bitmap = bVar6.i();
                    }
                    imageView2.setImageBitmap(bitmap);
                }
            }
            m();
        }
    }

    @DexIgnore
    public void y() {
        Bitmap l2;
        s87.b bVar = this.l;
        if (bVar == null || (l2 = bVar.i()) == null) {
            s87.b bVar2 = this.l;
            l2 = bVar2 != null ? bVar2.l() : null;
        }
        ImageView imageView = this.m;
        if (imageView != null) {
            a51 b = x41.b();
            Context context = imageView.getContext();
            pq7.b(context, "context");
            u71 u71 = new u71(context, b.a());
            u71.x(l2);
            u71.z(imageView);
            b.b(u71.w());
        }
    }

    @DexIgnore
    public final s67 z(ImageView imageView, s87.b bVar, ViewGroup.LayoutParams layoutParams, int i) {
        addView(imageView, layoutParams);
        this.m = imageView;
        bVar.o(i);
        bVar.p(i);
        this.l = bVar;
        return this;
    }
}
