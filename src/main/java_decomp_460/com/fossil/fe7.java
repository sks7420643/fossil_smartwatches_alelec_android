package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fe7 extends de7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, Object> f1109a;
    @DexIgnore
    public /* final */ a b; // = new a(this);
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements je7 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Object f1110a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Object d;

        @DexIgnore
        public a(fe7 fe7) {
        }

        @DexIgnore
        @Override // com.fossil.je7
        public void error(String str, String str2, Object obj) {
            this.b = str;
            this.c = str2;
            this.d = obj;
        }

        @DexIgnore
        @Override // com.fossil.je7
        public void success(Object obj) {
            this.f1110a = obj;
        }
    }

    @DexIgnore
    public fe7(Map<String, Object> map, boolean z) {
        this.f1109a = map;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.ie7
    public <T> T a(String str) {
        return (T) this.f1109a.get(str);
    }

    @DexIgnore
    @Override // com.fossil.ee7, com.fossil.ie7
    public boolean c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.de7
    public je7 i() {
        return this.b;
    }

    @DexIgnore
    public String j() {
        return (String) this.f1109a.get("method");
    }

    @DexIgnore
    public Map<String, Object> k() {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        hashMap2.put("code", this.b.b);
        hashMap2.put("message", this.b.c);
        hashMap2.put("data", this.b.d);
        hashMap.put("error", hashMap2);
        return hashMap;
    }

    @DexIgnore
    public Map<String, Object> l() {
        HashMap hashMap = new HashMap();
        hashMap.put(Constants.RESULT, this.b.f1110a);
        return hashMap;
    }

    @DexIgnore
    public void m(MethodChannel.Result result) {
        a aVar = this.b;
        result.error(aVar.b, aVar.c, aVar.d);
    }

    @DexIgnore
    public void n(List<Map<String, Object>> list) {
        if (!c()) {
            list.add(k());
        }
    }

    @DexIgnore
    public void o(List<Map<String, Object>> list) {
        if (!c()) {
            list.add(l());
        }
    }
}
