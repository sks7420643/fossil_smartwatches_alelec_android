package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class au7<T> extends fx7 implements xw7, qn7<T>, iv7 {
    @DexIgnore
    public /* final */ tn7 c;
    @DexIgnore
    public /* final */ tn7 d;

    @DexIgnore
    public au7(tn7 tn7, boolean z) {
        super(z);
        this.d = tn7;
        this.c = tn7.plus(this);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public final void S(Throwable th) {
        fv7.a(this.c, th);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public String Z() {
        String b = cv7.b(this.c);
        if (b == null) {
            return super.Z();
        }
        return '\"' + b + "\":" + super.Z();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.fx7
    public final void e0(Object obj) {
        if (obj instanceof vu7) {
            vu7 vu7 = (vu7) obj;
            w0(vu7.f3837a, vu7.a());
            return;
        }
        x0(obj);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public final void f0() {
        y0();
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public final tn7 getContext() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.iv7
    public tn7 h() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.fx7, com.fossil.xw7
    public boolean isActive() {
        return super.isActive();
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public final void resumeWith(Object obj) {
        Object X = X(wu7.b(obj));
        if (X != gx7.b) {
            u0(X);
        }
    }

    @DexIgnore
    public void u0(Object obj) {
        p(obj);
    }

    @DexIgnore
    public final void v0() {
        T((xw7) this.d.get(xw7.r));
    }

    @DexIgnore
    public void w0(Throwable th, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public String x() {
        return ov7.a(this) + " was cancelled";
    }

    @DexIgnore
    public void x0(T t) {
    }

    @DexIgnore
    public void y0() {
    }

    @DexIgnore
    public final <R> void z0(lv7 lv7, R r, vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7) {
        v0();
        lv7.invoke(vp7, r, this);
    }
}
