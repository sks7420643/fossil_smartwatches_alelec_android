package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ cl3 b;
    @DexIgnore
    public /* final */ /* synthetic */ aq3 c;

    @DexIgnore
    public bq3(aq3 aq3, cl3 cl3) {
        this.c = aq3;
        this.b = cl3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.c) {
            aq3.c(this.c, false);
            if (!this.c.c.V()) {
                this.c.c.d().M().a("Connected to remote service");
                this.c.c.L(this.b);
            }
        }
    }
}
