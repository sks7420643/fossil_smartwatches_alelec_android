package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ox5 f3045a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ArrayList<Alarm> c;
    @DexIgnore
    public /* final */ Alarm d;

    @DexIgnore
    public qx5(ox5 ox5, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        pq7.c(ox5, "mView");
        pq7.c(str, "mDeviceId");
        pq7.c(arrayList, "mAlarms");
        this.f3045a = ox5;
        this.b = str;
        this.c = arrayList;
        this.d = alarm;
    }

    @DexIgnore
    public final Alarm a() {
        return this.d;
    }

    @DexIgnore
    public final ArrayList<Alarm> b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final ox5 d() {
        return this.f3045a;
    }
}
