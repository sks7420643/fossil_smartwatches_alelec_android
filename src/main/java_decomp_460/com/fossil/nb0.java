package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb0 implements Parcelable.Creator<ob0> {
    @DexIgnore
    public /* synthetic */ nb0(kq7 kq7) {
    }

    @DexIgnore
    public ob0 a(Parcel parcel) {
        return new ob0(parcel, (kq7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ob0 createFromParcel(Parcel parcel) {
        return new ob0(parcel, (kq7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ob0[] newArray(int i) {
        return new ob0[i];
    }
}
