package com.fossil;

import com.fossil.blesdk.database.SdkDatabase;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ u f3499a; // = new u();

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int a(String str) {
        Integer num;
        z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            num = null;
        } else {
            a2.f4398a.assertNotSuspendingTransaction();
            px0 acquire = a2.e.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            a2.f4398a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.f4398a.setTransactionSuccessful();
                a2.f4398a.endTransaction();
                a2.e.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.f4398a.endTransaction();
                a2.e.release(acquire);
                throw th;
            }
        }
        g80.k(new JSONObject(), jd0.w4, num != null ? num : -1);
        r rVar = r.DELETE_ALL;
        m80.c.a("SdkDatabaseWrapper", "deleteFileByDeviceMacAddress: deviceMacAddress=%s, numberOfDeletedRow=%d.", str, num);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int b(String str, byte b) {
        Integer num;
        z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            num = null;
        } else {
            a2.f4398a.assertNotSuspendingTransaction();
            px0 acquire = a2.d.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            acquire.bindLong(2, (long) b);
            a2.f4398a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.f4398a.setTransactionSuccessful();
                a2.f4398a.endTransaction();
                a2.d.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.f4398a.endTransaction();
                a2.d.release(acquire);
                throw th;
            }
        }
        g80.k(g80.k(new kb(b, (byte) 255).toJSONObject(), jd0.N2, JSONObject.NULL), jd0.w4, num != null ? num : -1);
        r rVar = r.DELETE;
        m80.c.a("SdkDatabaseWrapper", "deleteFileByFileType: deviceMacAddress=%s, fileType=%d, numberOfDeletedRow=%d.", str, Byte.valueOf(b), num);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int c(String str, byte b, byte b2) {
        Integer num;
        z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            num = null;
        } else {
            a2.f4398a.assertNotSuspendingTransaction();
            px0 acquire = a2.c.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            acquire.bindLong(2, (long) b);
            acquire.bindLong(3, (long) b2);
            a2.f4398a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.f4398a.setTransactionSuccessful();
                a2.f4398a.endTransaction();
                a2.c.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.f4398a.endTransaction();
                a2.c.release(acquire);
                throw th;
            }
        }
        g80.k(g80.k(new kb(b, b2).toJSONObject(), jd0.N2, Boolean.FALSE), jd0.w4, num != null ? num : -1);
        r rVar = r.DELETE;
        m80.c.a("SdkDatabaseWrapper", "deleteIncompleteFileByFileHandle: deviceMacAddress=%s, fileType=%d, fileIndex=%d, numberOfDeletedRow=%d.", str, Byte.valueOf(b), Byte.valueOf(b2), num);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final long d(j0 j0Var) {
        Long l;
        z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            l = null;
        } else {
            a2.f4398a.assertNotSuspendingTransaction();
            a2.f4398a.beginTransaction();
            try {
                long insertAndReturnId = a2.b.insertAndReturnId(j0Var);
                a2.f4398a.setTransactionSuccessful();
                a2.f4398a.endTransaction();
                l = Long.valueOf(insertAndReturnId);
            } catch (Throwable th) {
                a2.f4398a.endTransaction();
                throw th;
            }
        }
        zr7 n = bs7.n(Long.MIN_VALUE, 0);
        if (l != null) {
            n.e(l.longValue());
        }
        g80.k(g80.k(new JSONObject(), jd0.u4, j0Var.a(true)), jd0.v4, l != null ? l : -1);
        r rVar = r.INSERT;
        m80.c.a("SdkDatabaseWrapper", "addFile: macAddress=%s, fileType=%d, fileIndex=%d, fileCrc=%d, dataLength=%d, fileLength=%d, isCompleted=%b, insertedRowId=%d.", j0Var.c, Byte.valueOf(j0Var.d), Byte.valueOf(j0Var.e), Long.valueOf(j0Var.h), Integer.valueOf(j0Var.f.length), Long.valueOf(j0Var.g), Boolean.valueOf(j0Var.j), l);
        if (l != null) {
            return l.longValue();
        }
        return -1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.k0 r13, com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 447
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u.e(com.fossil.k0, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.fossil.ec0 r12, com.fossil.ry1 r13, com.fossil.qn7<? super com.fossil.hc0> r14) {
        /*
        // Method dump skipped, instructions count: 284
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u.f(com.fossil.ec0, com.fossil.ry1, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void g() {
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011c, code lost:
        if (r2 != null) goto L_0x011e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.j0> h(java.lang.String r27, byte r28) {
        /*
        // Method dump skipped, instructions count: 353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u.h(java.lang.String, byte):java.util.List");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0122, code lost:
        if (r2 != null) goto L_0x0124;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.j0> i(java.lang.String r27, byte r28, byte r29) {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u.i(java.lang.String, byte, byte):java.util.List");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011c, code lost:
        if (r2 != null) goto L_0x011e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.j0> j(java.lang.String r27, byte r28) {
        /*
        // Method dump skipped, instructions count: 353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u.j(java.lang.String, byte):java.util.List");
    }
}
