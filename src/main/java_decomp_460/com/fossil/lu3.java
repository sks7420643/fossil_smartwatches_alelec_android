package com.fossil;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lu3<TResult> extends nt3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f2251a; // = new Object();
    @DexIgnore
    public /* final */ iu3<TResult> b; // = new iu3<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public TResult e;
    @DexIgnore
    public Exception f;

    @DexIgnore
    public final void A() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    @DexIgnore
    public final void B() {
        synchronized (this.f2251a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> a(Executor executor, gt3 gt3) {
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new wt3(executor, gt3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> b(ht3<TResult> ht3) {
        c(pt3.f2867a, ht3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> c(Executor executor, ht3<TResult> ht3) {
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new au3(executor, ht3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> d(it3 it3) {
        e(pt3.f2867a, it3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> e(Executor executor, it3 it3) {
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new bu3(executor, it3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> f(jt3<? super TResult> jt3) {
        g(pt3.f2867a, jt3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final nt3<TResult> g(Executor executor, jt3<? super TResult> jt3) {
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new eu3(executor, jt3));
        B();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <TContinuationResult> nt3<TContinuationResult> h(ft3<TResult, TContinuationResult> ft3) {
        return i(pt3.f2867a, ft3);
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <TContinuationResult> nt3<TContinuationResult> i(Executor executor, ft3<TResult, TContinuationResult> ft3) {
        lu3 lu3 = new lu3();
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new tt3(executor, ft3, lu3));
        B();
        return lu3;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <TContinuationResult> nt3<TContinuationResult> j(ft3<TResult, nt3<TContinuationResult>> ft3) {
        return k(pt3.f2867a, ft3);
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <TContinuationResult> nt3<TContinuationResult> k(Executor executor, ft3<TResult, nt3<TContinuationResult>> ft3) {
        lu3 lu3 = new lu3();
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new ut3(executor, ft3, lu3));
        B();
        return lu3;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final Exception l() {
        Exception exc;
        synchronized (this.f2251a) {
            exc = this.f;
        }
        return exc;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final TResult m() {
        TResult tresult;
        synchronized (this.f2251a) {
            w();
            A();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new lt3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <X extends Throwable> TResult n(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.f2251a) {
            w();
            A();
            if (cls.isInstance(this.f)) {
                throw cls.cast(this.f);
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new lt3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final boolean o() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final boolean p() {
        boolean z;
        synchronized (this.f2251a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final boolean q() {
        boolean z;
        synchronized (this.f2251a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <TContinuationResult> nt3<TContinuationResult> r(mt3<TResult, TContinuationResult> mt3) {
        return s(pt3.f2867a, mt3);
    }

    @DexIgnore
    @Override // com.fossil.nt3
    public final <TContinuationResult> nt3<TContinuationResult> s(Executor executor, mt3<TResult, TContinuationResult> mt3) {
        lu3 lu3 = new lu3();
        iu3<TResult> iu3 = this.b;
        nu3.a(executor);
        iu3.b(new fu3(executor, mt3, lu3));
        B();
        return lu3;
    }

    @DexIgnore
    public final void t(Exception exc) {
        rc2.l(exc, "Exception must not be null");
        synchronized (this.f2251a) {
            z();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final void u(TResult tresult) {
        synchronized (this.f2251a) {
            z();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final boolean v() {
        synchronized (this.f2251a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void w() {
        rc2.o(this.c, "Task is not yet complete");
    }

    @DexIgnore
    public final boolean x(Exception exc) {
        rc2.l(exc, "Exception must not be null");
        synchronized (this.f2251a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final boolean y(TResult tresult) {
        synchronized (this.f2251a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void z() {
        rc2.o(!this.c, "Task is already complete");
    }
}
