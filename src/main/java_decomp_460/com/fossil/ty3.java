package com.fossil;

import android.os.Bundle;
import android.view.View;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ View f3491a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public int c; // = 0;

    @DexIgnore
    public ty3(sy3 sy3) {
        this.f3491a = (View) sy3;
    }

    @DexIgnore
    public final void a() {
        ViewParent parent = this.f3491a.getParent();
        if (parent instanceof CoordinatorLayout) {
            ((CoordinatorLayout) parent).p(this.f3491a);
        }
    }

    @DexIgnore
    public int b() {
        return this.c;
    }

    @DexIgnore
    public boolean c() {
        return this.b;
    }

    @DexIgnore
    public void d(Bundle bundle) {
        this.b = bundle.getBoolean("expanded", false);
        this.c = bundle.getInt("expandedComponentIdHint", 0);
        if (this.b) {
            a();
        }
    }

    @DexIgnore
    public Bundle e() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("expanded", this.b);
        bundle.putInt("expandedComponentIdHint", this.c);
        return bundle;
    }

    @DexIgnore
    public void f(int i) {
        this.c = i;
    }
}
