package com.fossil;

import com.fossil.af1;
import com.fossil.wb1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class df1<Model, Data> implements af1<Model, Data> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<af1<Model, Data>> f780a;
    @DexIgnore
    public /* final */ mn0<List<Throwable>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements wb1<Data>, wb1.a<Data> {
        @DexIgnore
        public /* final */ List<wb1<Data>> b;
        @DexIgnore
        public /* final */ mn0<List<Throwable>> c;
        @DexIgnore
        public int d; // = 0;
        @DexIgnore
        public sa1 e;
        @DexIgnore
        public wb1.a<? super Data> f;
        @DexIgnore
        public List<Throwable> g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public a(List<wb1<Data>> list, mn0<List<Throwable>> mn0) {
            this.c = mn0;
            ik1.c(list);
            this.b = list;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
            List<Throwable> list = this.g;
            if (list != null) {
                this.c.a(list);
            }
            this.g = null;
            for (wb1<Data> wb1 : this.b) {
                wb1.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1.a
        public void b(Exception exc) {
            List<Throwable> list = this.g;
            ik1.d(list);
            list.add(exc);
            f();
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return this.b.get(0).c();
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
            this.h = true;
            for (wb1<Data> wb1 : this.b) {
                wb1.cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super Data> aVar) {
            this.e = sa1;
            this.f = aVar;
            this.g = this.c.b();
            this.b.get(this.d).d(sa1, this);
            if (this.h) {
                cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1.a
        public void e(Data data) {
            if (data != null) {
                this.f.e(data);
            } else {
                f();
            }
        }

        @DexIgnore
        public final void f() {
            if (!this.h) {
                if (this.d < this.b.size() - 1) {
                    this.d++;
                    d(this.e, this.f);
                    return;
                }
                ik1.d(this.g);
                this.f.b(new dd1("Fetch failed", new ArrayList(this.g)));
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<Data> getDataClass() {
            return this.b.get(0).getDataClass();
        }
    }

    @DexIgnore
    public df1(List<af1<Model, Data>> list, mn0<List<Throwable>> mn0) {
        this.f780a = list;
        this.b = mn0;
    }

    @DexIgnore
    @Override // com.fossil.af1
    public boolean a(Model model) {
        for (af1<Model, Data> af1 : this.f780a) {
            if (af1.a(model)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.af1
    public af1.a<Data> b(Model model, int i, int i2, ob1 ob1) {
        mb1 mb1;
        af1.a<Data> b2;
        int size = this.f780a.size();
        ArrayList arrayList = new ArrayList(size);
        mb1 mb12 = null;
        int i3 = 0;
        while (i3 < size) {
            af1<Model, Data> af1 = this.f780a.get(i3);
            if (!af1.a(model) || (b2 = af1.b(model, i, i2, ob1)) == null) {
                mb1 = mb12;
            } else {
                mb1 = b2.f261a;
                arrayList.add(b2.c);
            }
            i3++;
            mb12 = mb1;
        }
        if (arrayList.isEmpty() || mb12 == null) {
            return null;
        }
        return new af1.a<>(mb12, new a(arrayList, this.b));
    }

    @DexIgnore
    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.f780a.toArray()) + '}';
    }
}
