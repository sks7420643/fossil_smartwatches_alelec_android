package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ps extends ns {
    @DexIgnore
    public /* final */ byte[] A; // = new byte[0];
    @DexIgnore
    public byte[] B; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] C; // = new byte[0];
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ boolean F; // = true;

    @DexIgnore
    public ps(hs hsVar, k5 k5Var, int i) {
        super(hsVar, k5Var, i);
    }

    @DexIgnore
    @Override // com.fossil.ns
    public final u5 D() {
        ByteBuffer put = ByteBuffer.allocate(M().length + L().length).order(ByteOrder.LITTLE_ENDIAN).put(M()).put(L());
        n6 K = K();
        byte[] array = put.array();
        pq7.b(array, "byteBuffer.array()");
        return new j6(K, array, this.y.z);
    }

    @DexIgnore
    public abstract mt E(byte b);

    @DexIgnore
    public JSONObject F(byte[] bArr) {
        this.E = true;
        return new JSONObject();
    }

    @DexIgnore
    public boolean G(o7 o7Var) {
        return false;
    }

    @DexIgnore
    public final boolean H(o7 o7Var) {
        if (o7Var.f2638a != N() || o7Var.b.length < P().length) {
            return false;
        }
        return Arrays.equals(P(), dm7.k(o7Var.b, 0, P().length));
    }

    @DexIgnore
    public void I(o7 o7Var) {
        JSONObject F2;
        byte[] bArr = o7Var.b;
        JSONObject jSONObject = new JSONObject();
        if (O()) {
            mt E2 = E(bArr[P().length]);
            mw a2 = mw.a(this.v, null, null, mw.g.b(E2).d, null, E2, 11);
            this.v = a2;
            if (a2.d == lw.b) {
                F2 = F(dm7.k(bArr, P().length + 1, bArr.length));
            } else {
                this.E = true;
                F2 = jSONObject;
            }
        } else {
            F2 = F(dm7.k(bArr, P().length, bArr.length));
        }
        this.g.add(new hw(0, o7Var.f2638a, bArr, F2, 1));
    }

    @DexIgnore
    public void J(o7 o7Var) {
    }

    @DexIgnore
    public abstract n6 K();

    @DexIgnore
    public byte[] L() {
        return this.C;
    }

    @DexIgnore
    public byte[] M() {
        return this.A;
    }

    @DexIgnore
    public abstract n6 N();

    @DexIgnore
    public boolean O() {
        return this.F;
    }

    @DexIgnore
    public byte[] P() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final void g(u5 u5Var) {
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final void s(o7 o7Var) {
        if (H(o7Var)) {
            I(o7Var);
        } else if (G(o7Var)) {
            J(o7Var);
        }
        if (this.D && this.E) {
            m(this.v);
        }
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final void v(u5 u5Var) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.D = true;
        a90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        a90 a902 = this.f;
        if (!(a902 == null || (jSONObject2 = a902.n) == null)) {
            g80.k(jSONObject2, jd0.k, ey1.a(lw.b));
        }
        if (this.v.d == lw.c) {
            mw a2 = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
            this.v = a2;
            lw lwVar = a2.d;
            lw lwVar2 = lw.b;
        }
        a90 a903 = this.f;
        if (a903 != null) {
            a903.j = true;
        }
        a90 a904 = this.f;
        if (!(a904 == null || (jSONObject = a904.n) == null)) {
            g80.k(jSONObject, jd0.k, ey1.a(lw.b));
        }
        C();
        if (this.E) {
            m(this.v);
        }
    }
}
