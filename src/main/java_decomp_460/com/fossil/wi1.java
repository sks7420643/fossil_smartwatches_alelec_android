package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wi1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<String> f3941a; // = new ArrayList();
    @DexIgnore
    public /* final */ Map<String, List<a<?, ?>>> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T, R> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<T> f3942a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ qb1<T, R> c;

        @DexIgnore
        public a(Class<T> cls, Class<R> cls2, qb1<T, R> qb1) {
            this.f3942a = cls;
            this.b = cls2;
            this.c = qb1;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.f3942a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public <T, R> void a(String str, qb1<T, R> qb1, Class<T> cls, Class<R> cls2) {
        synchronized (this) {
            c(str).add(new a<>(cls, cls2, qb1));
        }
    }

    @DexIgnore
    public <T, R> List<qb1<T, R>> b(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (String str : this.f3941a) {
                List<a<?, ?>> list = this.b.get(str);
                if (list != null) {
                    for (a<?, ?> aVar : list) {
                        if (aVar.a(cls, cls2)) {
                            arrayList.add(aVar.c);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<a<?, ?>> c(String str) {
        List<a<?, ?>> list;
        synchronized (this) {
            if (!this.f3941a.contains(str)) {
                this.f3941a.add(str);
            }
            list = this.b.get(str);
            if (list == null) {
                list = new ArrayList<>();
                this.b.put(str, list);
            }
        }
        return list;
    }

    @DexIgnore
    public <T, R> List<Class<R>> d(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (String str : this.f3941a) {
                List<a<?, ?>> list = this.b.get(str);
                if (list != null) {
                    for (a<?, ?> aVar : list) {
                        if (aVar.a(cls, cls2) && !arrayList.contains(aVar.b)) {
                            arrayList.add(aVar.b);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void e(List<String> list) {
        synchronized (this) {
            ArrayList<String> arrayList = new ArrayList(this.f3941a);
            this.f3941a.clear();
            for (String str : list) {
                this.f3941a.add(str);
            }
            for (String str2 : arrayList) {
                if (!list.contains(str2)) {
                    this.f3941a.add(str2);
                }
            }
        }
    }
}
