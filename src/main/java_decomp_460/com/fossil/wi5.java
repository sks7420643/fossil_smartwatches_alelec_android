package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wi5 extends ri5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f3946a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public wi5(String str, int i, int i2, int i3) {
        this.f3946a = i3;
        this.b = str;
    }

    @DexIgnore
    public int a() {
        return this.f3946a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }
}
