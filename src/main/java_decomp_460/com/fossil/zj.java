package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zj extends ro {
    @DexIgnore
    public /* final */ byte[] S;

    @DexIgnore
    public zj(k5 k5Var, i60 i60, yp ypVar, boolean z, short s, byte[] bArr, float f, String str) {
        super(k5Var, i60, ypVar, z, s, f, str, false, 128);
        this.S = bArr;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zj(k5 k5Var, i60 i60, yp ypVar, boolean z, short s, byte[] bArr, float f, String str, int i) {
        super(k5Var, i60, ypVar, z, s, (i & 64) != 0 ? 0.001f : f, (i & 128) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 128);
        this.S = bArr;
    }

    @DexIgnore
    @Override // com.fossil.ro
    public byte[] M() {
        return this.S;
    }
}
