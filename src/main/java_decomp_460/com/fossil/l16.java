package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l16 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j16 f2133a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public l16(j16 j16, LoaderManager loaderManager) {
        pq7.c(j16, "mView");
        pq7.c(loaderManager, "mLoaderManager");
        this.f2133a = j16;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final j16 b() {
        return this.f2133a;
    }
}
