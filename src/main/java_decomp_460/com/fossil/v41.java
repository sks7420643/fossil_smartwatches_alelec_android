package com.fossil;

import com.fossil.t41;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v41 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public t41<?> f3713a;

    @DexIgnore
    public v41(t41<?> t41) {
        this.f3713a = t41;
    }

    @DexIgnore
    public void a() {
        this.f3713a = null;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        t41.g o;
        try {
            t41<?> t41 = this.f3713a;
            if (!(t41 == null || (o = t41.o()) == null)) {
                o.a(t41, new w41(t41.m()));
            }
        } finally {
            super.finalize();
        }
    }
}
