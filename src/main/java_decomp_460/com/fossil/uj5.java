package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj5 extends fj1 implements Cloneable {
    @DexIgnore
    /* renamed from: A0 */
    public uj5 l(wc1 wc1) {
        return (uj5) super.l(wc1);
    }

    @DexIgnore
    /* renamed from: B0 */
    public uj5 n() {
        return (uj5) super.n();
    }

    @DexIgnore
    /* renamed from: C0 */
    public uj5 o(fg1 fg1) {
        return (uj5) super.o(fg1);
    }

    @DexIgnore
    /* renamed from: D0 */
    public uj5 V() {
        super.V();
        return this;
    }

    @DexIgnore
    /* renamed from: E0 */
    public uj5 W() {
        return (uj5) super.W();
    }

    @DexIgnore
    /* renamed from: F0 */
    public uj5 X() {
        return (uj5) super.X();
    }

    @DexIgnore
    /* renamed from: G0 */
    public uj5 Y() {
        return (uj5) super.Y();
    }

    @DexIgnore
    /* renamed from: H0 */
    public uj5 b0(int i, int i2) {
        return (uj5) super.b0(i, i2);
    }

    @DexIgnore
    /* renamed from: I0 */
    public uj5 c0(int i) {
        return (uj5) super.c0(i);
    }

    @DexIgnore
    /* renamed from: J0 */
    public uj5 d0(Drawable drawable) {
        return (uj5) super.d0(drawable);
    }

    @DexIgnore
    /* renamed from: K0 */
    public uj5 e0(sa1 sa1) {
        return (uj5) super.e0(sa1);
    }

    @DexIgnore
    /* renamed from: L0 */
    public <Y> uj5 j0(nb1<Y> nb1, Y y) {
        return (uj5) super.j0(nb1, y);
    }

    @DexIgnore
    /* renamed from: M0 */
    public uj5 k0(mb1 mb1) {
        return (uj5) super.k0(mb1);
    }

    @DexIgnore
    /* renamed from: N0 */
    public uj5 l0(float f) {
        return (uj5) super.l0(f);
    }

    @DexIgnore
    /* renamed from: O0 */
    public uj5 m0(boolean z) {
        return (uj5) super.m0(z);
    }

    @DexIgnore
    /* renamed from: P0 */
    public uj5 o0(sb1<Bitmap> sb1) {
        return (uj5) super.o0(sb1);
    }

    @DexIgnore
    /* renamed from: Q0 */
    public uj5 s0(boolean z) {
        return (uj5) super.s0(z);
    }

    @DexIgnore
    /* renamed from: w0 */
    public uj5 d(yi1<?> yi1) {
        return (uj5) super.d(yi1);
    }

    @DexIgnore
    /* renamed from: x0 */
    public uj5 e() {
        return (uj5) super.e();
    }

    @DexIgnore
    /* renamed from: y0 */
    public uj5 i() {
        return (uj5) super.clone();
    }

    @DexIgnore
    /* renamed from: z0 */
    public uj5 j(Class<?> cls) {
        return (uj5) super.j(cls);
    }
}
