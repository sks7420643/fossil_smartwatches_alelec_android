package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y63 implements v63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f4248a;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f4248a = hw2.d("measurement.client.sessions.check_on_reset_and_enable2", true);
        hw2.d("measurement.client.sessions.check_on_startup", true);
        hw2.d("measurement.client.sessions.start_session_before_view_screen", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.v63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final boolean zzb() {
        return f4248a.o().booleanValue();
    }
}
