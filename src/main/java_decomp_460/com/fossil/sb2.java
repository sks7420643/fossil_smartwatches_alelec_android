package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sb2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ DataHolder f3230a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public sb2(DataHolder dataHolder, int i) {
        rc2.k(dataHolder);
        this.f3230a = dataHolder;
        d(i);
    }

    @DexIgnore
    public byte[] a(String str) {
        return this.f3230a.c(str, this.b, this.c);
    }

    @DexIgnore
    public int b(String str) {
        return this.f3230a.f(str, this.b, this.c);
    }

    @DexIgnore
    public String c(String str) {
        return this.f3230a.A(str, this.b, this.c);
    }

    @DexIgnore
    public final void d(int i) {
        rc2.n(i >= 0 && i < this.f3230a.getCount());
        this.b = i;
        this.c = this.f3230a.D(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof sb2) {
            sb2 sb2 = (sb2) obj;
            return pc2.a(Integer.valueOf(sb2.b), Integer.valueOf(this.b)) && pc2.a(Integer.valueOf(sb2.c), Integer.valueOf(this.c)) && sb2.f3230a == this.f3230a;
        }
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(Integer.valueOf(this.b), Integer.valueOf(this.c), this.f3230a);
    }
}
