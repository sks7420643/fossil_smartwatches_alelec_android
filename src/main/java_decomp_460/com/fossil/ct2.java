package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.fossil.zs2;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ Context h;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle i;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ct2(zs2 zs2, String str, String str2, Context context, Bundle bundle) {
        super(zs2);
        this.j = zs2;
        this.f = str;
        this.g = str2;
        this.h = context;
        this.i = bundle;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() {
        String str;
        String str2;
        String str3;
        int i2;
        boolean z;
        try {
            this.j.e = new ArrayList();
            zs2 zs2 = this.j;
            if (zs2.K(this.f, this.g)) {
                str2 = this.g;
                str = this.f;
                str3 = this.j.f4521a;
            } else {
                str = null;
                str2 = null;
                str3 = null;
            }
            zs2.V(this.h);
            boolean z2 = zs2.j.booleanValue() || str != null;
            this.j.h = this.j.c(this.h, z2);
            if (this.j.h == null) {
                Log.w(this.j.f4521a, "Failed to connect to measurement client.");
                return;
            }
            int i3 = zs2.T(this.h);
            int i4 = zs2.R(this.h);
            if (z2) {
                i2 = Math.max(i3, i4);
                z = i4 < i3;
            } else {
                if (i3 > 0) {
                    i4 = i3;
                }
                if (i3 > 0) {
                    i2 = i4;
                    z = true;
                } else {
                    i2 = i4;
                    z = false;
                }
            }
            this.j.h.initialize(tg2.n(this.h), new xs2(31000, (long) i2, z, str3, str, str2, this.i), this.b);
        } catch (Exception e) {
            this.j.o(e, true, false);
        }
    }
}
