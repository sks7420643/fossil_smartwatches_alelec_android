package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h27 extends pv5 implements g27, View.OnClickListener {
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public f27 g;
    @DexIgnore
    public g37<jc5> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final h27 a() {
            h27 h27 = new h27();
            h27.setArguments(nm0.a(new cl7("KEY_DIANA_REQUIRE", Boolean.TRUE)));
            return h27;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jc5 f1416a;

        @DexIgnore
        public b(jc5 jc5) {
            this.f1416a = jc5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            FlexibleButton flexibleButton = this.f1416a.r;
            pq7.b(flexibleButton, "it.fbGetStarted");
            flexibleButton.setEnabled(z);
            this.f1416a.r.d(z ? "flexible_button_secondary" : "flexible_button_disabled");
        }
    }

    @DexIgnore
    public h27() {
        String d = qn5.l.a().d("nonBrandBlack");
        this.i = Color.parseColor(d == null ? "#FFFFFF" : d);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: K6 */
    public void M5(f27 f27) {
        pq7.c(f27, "presenter");
        this.g = f27;
    }

    @DexIgnore
    @Override // com.fossil.g27
    public void V5() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.F;
            pq7.b(activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.g27
    public void d2() {
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.i(childFragmentManager);
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362274) {
                f27 f27 = this.g;
                if (f27 != null) {
                    f27.n();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (id == 2131362738) {
                f27 f272 = this.g;
                if (f272 != null) {
                    f272.o();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        g37<jc5> g37 = new g37<>(this, (jc5) aq0.f(layoutInflater, 2131558641, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            jc5 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onPause();
        g37<jc5> g37 = this.h;
        if (g37 != null) {
            jc5 a2 = g37.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.stopShimmerAnimation();
            }
            f27 f27 = this.g;
            if (f27 != null) {
                f27.m();
                vl5 C6 = C6();
                if (C6 != null) {
                    C6.c("");
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onResume();
        g37<jc5> g37 = this.h;
        if (g37 != null) {
            jc5 a2 = g37.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.startShimmerAnimation();
            }
            f27 f27 = this.g;
            if (f27 != null) {
                f27.l();
                vl5 C6 = C6();
                if (C6 != null) {
                    C6.i();
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<jc5> g37 = this.h;
        if (g37 != null) {
            jc5 a2 = g37.a();
            if (a2 != null) {
                if (!wr4.f3989a.a().d()) {
                    ShimmerFrameLayout shimmerFrameLayout = a2.w;
                    pq7.b(shimmerFrameLayout, "it.shmNote");
                    shimmerFrameLayout.setVisibility(4);
                }
                a2.r.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                if (pq7.a(wr4.f3989a.a().h(), "CN")) {
                    FlexibleCheckBox flexibleCheckBox = a2.q;
                    pq7.b(flexibleCheckBox, "it.cbTermsService");
                    flexibleCheckBox.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.u;
                    pq7.b(flexibleTextView, "it.ftvTermsService");
                    flexibleTextView.setVisibility(0);
                    FlexibleButton flexibleButton = a2.r;
                    pq7.b(flexibleButton, "it.fbGetStarted");
                    flexibleButton.setEnabled(false);
                    a2.r.d("flexible_button_disabled");
                    a2.u.setLinkTextColor(this.i);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    pq7.b(flexibleTextView2, "it.ftvTermsService");
                    flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                    a2.q.setOnCheckedChangeListener(new b(a2));
                } else {
                    FlexibleCheckBox flexibleCheckBox2 = a2.q;
                    pq7.b(flexibleCheckBox2, "it.cbTermsService");
                    flexibleCheckBox2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.u;
                    pq7.b(flexibleTextView3, "it.ftvTermsService");
                    flexibleTextView3.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.r;
                    pq7.b(flexibleButton2, "it.fbGetStarted");
                    flexibleButton2.setEnabled(true);
                    a2.r.d("flexible_button_secondary");
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null && arguments.getBoolean("KEY_DIANA_REQUIRE")) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.u(childFragmentManager);
            }
            E6("tutorial_view");
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.g27
    public void w0(Spanned spanned) {
        FlexibleTextView flexibleTextView;
        pq7.c(spanned, "message");
        if (isActive()) {
            g37<jc5> g37 = this.h;
            if (g37 != null) {
                jc5 a2 = g37.a();
                if (a2 != null && (flexibleTextView = a2.u) != null) {
                    flexibleTextView.setText(spanned);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.g27
    public void y5() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.A;
        Context requireContext = requireContext();
        pq7.b(requireContext, "requireContext()");
        aVar.a(requireContext);
    }
}
