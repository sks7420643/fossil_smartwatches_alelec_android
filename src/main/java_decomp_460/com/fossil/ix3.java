package com.fossil;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import com.fossil.hx3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ix3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ boolean f1689a; // = (Build.VERSION.SDK_INT < 18);

    @DexIgnore
    public static void a(hx3 hx3, View view, FrameLayout frameLayout) {
        e(hx3, view, frameLayout);
        if (f1689a) {
            frameLayout.setForeground(hx3);
        } else {
            view.getOverlay().add(hx3);
        }
    }

    @DexIgnore
    public static SparseArray<hx3> b(Context context, fz3 fz3) {
        SparseArray<hx3> sparseArray = new SparseArray<>(fz3.size());
        for (int i = 0; i < fz3.size(); i++) {
            int keyAt = fz3.keyAt(i);
            hx3.a aVar = (hx3.a) fz3.valueAt(i);
            if (aVar != null) {
                sparseArray.put(keyAt, hx3.e(context, aVar));
            } else {
                throw new IllegalArgumentException("BadgeDrawable's savedState cannot be null");
            }
        }
        return sparseArray;
    }

    @DexIgnore
    public static fz3 c(SparseArray<hx3> sparseArray) {
        fz3 fz3 = new fz3();
        for (int i = 0; i < sparseArray.size(); i++) {
            int keyAt = sparseArray.keyAt(i);
            hx3 valueAt = sparseArray.valueAt(i);
            if (valueAt != null) {
                fz3.put(keyAt, valueAt.k());
            } else {
                throw new IllegalArgumentException("badgeDrawable cannot be null");
            }
        }
        return fz3;
    }

    @DexIgnore
    public static void d(hx3 hx3, View view, FrameLayout frameLayout) {
        if (hx3 != null) {
            if (f1689a) {
                frameLayout.setForeground(null);
            } else {
                view.getOverlay().remove(hx3);
            }
        }
    }

    @DexIgnore
    public static void e(hx3 hx3, View view, FrameLayout frameLayout) {
        Rect rect = new Rect();
        (f1689a ? frameLayout : view).getDrawingRect(rect);
        hx3.setBounds(rect);
        hx3.w(view, frameLayout);
    }

    @DexIgnore
    public static void f(Rect rect, float f, float f2, float f3, float f4) {
        rect.set((int) (f - f3), (int) (f2 - f4), (int) (f + f3), (int) (f2 + f4));
    }
}
