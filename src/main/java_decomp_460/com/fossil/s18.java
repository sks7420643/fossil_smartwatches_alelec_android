package com.fossil;

import com.facebook.stetho.dumpapp.Framer;
import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.p18;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s18 extends RequestBody {
    @DexIgnore
    public static /* final */ r18 e; // = r18.c("multipart/mixed");
    @DexIgnore
    public static /* final */ r18 f; // = r18.c("multipart/form-data");
    @DexIgnore
    public static /* final */ byte[] g; // = {58, 32};
    @DexIgnore
    public static /* final */ byte[] h; // = {13, 10};
    @DexIgnore
    public static /* final */ byte[] i; // = {Framer.STDIN_FRAME_PREFIX, Framer.STDIN_FRAME_PREFIX};

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ l48 f3194a;
    @DexIgnore
    public /* final */ r18 b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public long d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ l48 f3195a;
        @DexIgnore
        public r18 b;
        @DexIgnore
        public /* final */ List<b> c;

        @DexIgnore
        public a() {
            this(UUID.randomUUID().toString());
        }

        @DexIgnore
        public a(String str) {
            this.b = s18.e;
            this.c = new ArrayList();
            this.f3195a = l48.encodeUtf8(str);
        }

        @DexIgnore
        public a a(String str, String str2) {
            d(b.b(str, str2));
            return this;
        }

        @DexIgnore
        public a b(String str, String str2, RequestBody requestBody) {
            d(b.c(str, str2, requestBody));
            return this;
        }

        @DexIgnore
        public a c(p18 p18, RequestBody requestBody) {
            d(b.a(p18, requestBody));
            return this;
        }

        @DexIgnore
        public a d(b bVar) {
            if (bVar != null) {
                this.c.add(bVar);
                return this;
            }
            throw new NullPointerException("part == null");
        }

        @DexIgnore
        public s18 e() {
            if (!this.c.isEmpty()) {
                return new s18(this.f3195a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }

        @DexIgnore
        public a f(r18 r18) {
            if (r18 == null) {
                throw new NullPointerException("type == null");
            } else if (r18.f().equals("multipart")) {
                this.b = r18;
                return this;
            } else {
                throw new IllegalArgumentException("multipart != " + r18);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ p18 f3196a;
        @DexIgnore
        public /* final */ RequestBody b;

        @DexIgnore
        public b(p18 p18, RequestBody requestBody) {
            this.f3196a = p18;
            this.b = requestBody;
        }

        @DexIgnore
        public static b a(p18 p18, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (p18 != null && p18.c("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (p18 == null || p18.c(HttpHeaders.CONTENT_LENGTH) == null) {
                return new b(p18, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }

        @DexIgnore
        public static b b(String str, String str2) {
            return c(str, null, RequestBody.d(null, str2));
        }

        @DexIgnore
        public static b c(String str, String str2, RequestBody requestBody) {
            if (str != null) {
                StringBuilder sb = new StringBuilder("form-data; name=");
                s18.i(sb, str);
                if (str2 != null) {
                    sb.append("; filename=");
                    s18.i(sb, str2);
                }
                p18.a aVar = new p18.a();
                aVar.d("Content-Disposition", sb.toString());
                return a(aVar.e(), requestBody);
            }
            throw new NullPointerException("name == null");
        }
    }

    /*
    static {
        r18.c("multipart/alternative");
        r18.c("multipart/digest");
        r18.c("multipart/parallel");
    }
    */

    @DexIgnore
    public s18(l48 l48, r18 r18, List<b> list) {
        this.f3194a = l48;
        this.b = r18.c(r18 + "; boundary=" + l48.utf8());
        this.c = b28.t(list);
    }

    @DexIgnore
    public static StringBuilder i(StringBuilder sb, String str) {
        sb.append('\"');
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '\n') {
                sb.append("%0A");
            } else if (charAt == '\r') {
                sb.append("%0D");
            } else if (charAt != '\"') {
                sb.append(charAt);
            } else {
                sb.append("%22");
            }
        }
        sb.append('\"');
        return sb;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public long a() throws IOException {
        long j = this.d;
        if (j != -1) {
            return j;
        }
        long j2 = j(null, true);
        this.d = j2;
        return j2;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public r18 b() {
        return this.b;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public void h(j48 j48) throws IOException {
        j(j48, false);
    }

    @DexIgnore
    public final long j(j48 j48, boolean z) throws IOException {
        i48 i48;
        if (z) {
            i48 i482 = new i48();
            i48 = i482;
            j48 = i482;
        } else {
            i48 = null;
        }
        int size = this.c.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            p18 p18 = bVar.f3196a;
            RequestBody requestBody = bVar.b;
            j48.Z(i);
            j48.a0(this.f3194a);
            j48.Z(h);
            if (p18 != null) {
                int h2 = p18.h();
                for (int i3 = 0; i3 < h2; i3++) {
                    j48.E(p18.e(i3)).Z(g).E(p18.i(i3)).Z(h);
                }
            }
            r18 b2 = requestBody.b();
            if (b2 != null) {
                j48.E("Content-Type: ").E(b2.toString()).Z(h);
            }
            long a2 = requestBody.a();
            if (a2 != -1) {
                j48.E("Content-Length: ").k0(a2).Z(h);
            } else if (z) {
                i48.j();
                return -1;
            }
            j48.Z(h);
            if (z) {
                j += a2;
            } else {
                requestBody.h(j48);
            }
            j48.Z(h);
        }
        j48.Z(i);
        j48.a0(this.f3194a);
        j48.Z(i);
        j48.Z(h);
        if (!z) {
            return j;
        }
        long p0 = j + i48.p0();
        i48.j();
        return p0;
    }
}
