package com.fossil;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m68 implements Iterable<q68> {
    @DexIgnore
    public /* final */ List<q68> b; // = new LinkedList();
    @DexIgnore
    public /* final */ Map<String, List<q68>> c; // = new HashMap();

    @DexIgnore
    public void a(q68 q68) {
        if (q68 != null) {
            String lowerCase = q68.b().toLowerCase(Locale.US);
            List<q68> list = this.c.get(lowerCase);
            if (list == null) {
                list = new LinkedList<>();
                this.c.put(lowerCase, list);
            }
            list.add(q68);
            this.b.add(q68);
        }
    }

    @DexIgnore
    public q68 b(String str) {
        if (str == null) {
            return null;
        }
        List<q68> list = this.c.get(str.toLowerCase(Locale.US));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<q68> iterator() {
        return Collections.unmodifiableList(this.b).iterator();
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
