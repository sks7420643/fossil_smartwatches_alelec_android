package com.fossil;

import com.fossil.xw7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr0<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public xw7 f1793a;
    @DexIgnore
    public xw7 b;
    @DexIgnore
    public /* final */ nr0<T> c;
    @DexIgnore
    public /* final */ vp7<hs0<T>, qn7<? super tl7>, Object> d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ iv7 f;
    @DexIgnore
    public /* final */ gp7<tl7> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "androidx.lifecycle.BlockRunner$cancel$1", f = "CoroutineLiveData.kt", l = {187}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(jr0 jr0, qn7 qn7) {
            super(2, qn7);
            this.this$0 = jr0;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                long j = this.this$0.e;
                this.L$0 = iv7;
                this.label = 1;
                if (uv7.a(j, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!this.this$0.c.g()) {
                xw7 xw7 = this.this$0.f1793a;
                if (xw7 != null) {
                    xw7.a.a(xw7, null, 1, null);
                }
                this.this$0.f1793a = null;
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "androidx.lifecycle.BlockRunner$maybeRun$1", f = "CoroutineLiveData.kt", l = {176}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(jr0 jr0, qn7 qn7) {
            super(2, qn7);
            this.this$0 = jr0;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                is0 is0 = new is0(this.this$0.c, iv7.h());
                vp7 vp7 = this.this$0.d;
                this.L$0 = iv7;
                this.L$1 = is0;
                this.label = 1;
                if (vp7.invoke(is0, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                is0 is02 = (is0) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.g.invoke();
            return tl7.f3441a;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.vp7<? super com.fossil.hs0<T>, ? super com.fossil.qn7<? super com.fossil.tl7>, ? extends java.lang.Object> */
    /* JADX WARN: Multi-variable type inference failed */
    public jr0(nr0<T> nr0, vp7<? super hs0<T>, ? super qn7<? super tl7>, ? extends Object> vp7, long j, iv7 iv7, gp7<tl7> gp7) {
        pq7.c(nr0, "liveData");
        pq7.c(vp7, "block");
        pq7.c(iv7, "scope");
        pq7.c(gp7, "onDone");
        this.c = nr0;
        this.d = vp7;
        this.e = j;
        this.f = iv7;
        this.g = gp7;
    }

    @DexIgnore
    public final void g() {
        if (this.b == null) {
            this.b = gu7.d(this.f, bw7.c().S(), null, new a(this, null), 2, null);
            return;
        }
        throw new IllegalStateException("Cancel call cannot happen without a maybeRun".toString());
    }

    @DexIgnore
    public final void h() {
        xw7 xw7 = this.b;
        if (xw7 != null) {
            xw7.a.a(xw7, null, 1, null);
        }
        this.b = null;
        if (this.f1793a == null) {
            this.f1793a = gu7.d(this.f, null, null, new b(this, null), 3, null);
        }
    }
}
