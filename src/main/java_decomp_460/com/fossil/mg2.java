package com.fossil;

import android.util.Log;
import com.facebook.internal.Utility;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg2 {
    @DexIgnore
    public static /* final */ mg2 d; // = new mg2(true, null, null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f2377a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Throwable c;

    @DexIgnore
    public mg2(boolean z, String str, Throwable th) {
        this.f2377a = z;
        this.b = str;
        this.c = th;
    }

    @DexIgnore
    public static mg2 b(String str, Throwable th) {
        return new mg2(false, str, th);
    }

    @DexIgnore
    public static mg2 c(Callable<String> callable) {
        return new og2(callable);
    }

    @DexIgnore
    public static mg2 d(String str) {
        return new mg2(false, str, null);
    }

    @DexIgnore
    public static String e(String str, eg2 eg2, boolean z, boolean z2) {
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", z2 ? "debug cert rejected" : "not whitelisted", str, jf2.a(af2.b(Utility.HASH_ALGORITHM_SHA1).digest(eg2.i())), Boolean.valueOf(z), "12451009.false");
    }

    @DexIgnore
    public static mg2 f() {
        return d;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public final void g() {
        if (!this.f2377a && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (this.c != null) {
                Log.d("GoogleCertificatesRslt", a(), this.c);
            } else {
                Log.d("GoogleCertificatesRslt", a());
            }
        }
    }
}
