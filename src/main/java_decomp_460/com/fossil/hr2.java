package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr2 extends xq2 {
    @DexIgnore
    public j72<ka3> b;

    @DexIgnore
    public hr2(j72<ka3> j72) {
        rc2.b(j72 != null, "listener can't be null.");
        this.b = j72;
    }

    @DexIgnore
    @Override // com.fossil.wq2
    public final void E0(ka3 ka3) throws RemoteException {
        this.b.a(ka3);
        this.b = null;
    }
}
