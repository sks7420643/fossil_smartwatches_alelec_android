package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fm1 {
    WEATHER("weatherSSE"),
    HEART_RATE("hrSSE"),
    STEPS("stepsSSE"),
    DATE("dateSSE"),
    CHANCE_OF_RAIN("chanceOfRainSSE"),
    SECOND_TIMEZONE("timeZone2SSE"),
    ACTIVE_MINUTES("activeMinutesSSE"),
    CALORIES("caloriesSSE"),
    BATTERY("batterySSE"),
    EMPTY("empty");
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }
    }

    @DexIgnore
    public fm1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj = x8.f4072a[ordinal()] != 1 ? this.b : JSONObject.NULL;
        pq7.b(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
