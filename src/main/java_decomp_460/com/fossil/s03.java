package com.fossil;

import com.fossil.v03;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s03<T extends v03<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract t03<T> b(Object obj);

    @DexIgnore
    public abstract Object c(q03 q03, m23 m23, int i);

    @DexIgnore
    public abstract void d(r43 r43, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract boolean e(m23 m23);

    @DexIgnore
    public abstract t03<T> f(Object obj);

    @DexIgnore
    public abstract void g(Object obj);
}
