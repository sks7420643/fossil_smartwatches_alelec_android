package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo4 implements Factory<ApiServiceV2> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f4342a;
    @DexIgnore
    public /* final */ Provider<qq5> b;
    @DexIgnore
    public /* final */ Provider<uq5> c;

    @DexIgnore
    public yo4(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        this.f4342a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static yo4 a(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        return new yo4(uo4, provider, provider2);
    }

    @DexIgnore
    public static ApiServiceV2 c(uo4 uo4, qq5 qq5, uq5 uq5) {
        ApiServiceV2 e = uo4.e(qq5, uq5);
        lk7.c(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }

    @DexIgnore
    /* renamed from: b */
    public ApiServiceV2 get() {
        return c(this.f4342a, this.b.get(), this.c.get());
    }
}
