package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q7 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ k5 f2931a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public q7(k5 k5Var) {
        this.f2931a = k5Var;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == 2084501365 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED")) {
            r4 a2 = r4.f.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", 10));
            r4 a3 = r4.f.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", 10));
            if (pq7.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE"), this.f2931a.A)) {
                if (a3 == r4.BOND_NONE) {
                    this.f2931a.e = true;
                }
                k5 k5Var = this.f2931a;
                k5Var.b.post(new m4(k5Var, new g7(f7.SUCCESS, 0, 2), a2, a3));
                k5 k5Var2 = this.f2931a;
                k5Var2.b.post(new c5(k5Var2, new g7(f7.SUCCESS, 0, 2), a2, a3));
            }
        }
    }
}
