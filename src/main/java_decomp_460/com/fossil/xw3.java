package com.fossil;

import android.util.Property;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xw3 extends Property<ViewGroup, Float> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Property<ViewGroup, Float> f4192a; // = new xw3("childrenAlpha");

    @DexIgnore
    public xw3(String str) {
        super(Float.class, str);
    }

    @DexIgnore
    /* renamed from: a */
    public Float get(ViewGroup viewGroup) {
        Float f = (Float) viewGroup.getTag(nw3.mtrl_internal_children_alpha_tag);
        return f != null ? f : Float.valueOf(1.0f);
    }

    @DexIgnore
    /* renamed from: b */
    public void set(ViewGroup viewGroup, Float f) {
        float floatValue = f.floatValue();
        viewGroup.setTag(nw3.mtrl_internal_children_alpha_tag, Float.valueOf(floatValue));
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            viewGroup.getChildAt(i).setAlpha(floatValue);
        }
    }
}
