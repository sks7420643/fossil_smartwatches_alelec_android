package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wx4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4015a;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<mt4, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ tt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$initLoadRecommendedChallenge$1", f = "BCRecommendationViewModel.kt", l = {51}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wx4 wx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = wx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(ao7.a(true));
                wx4 wx4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (wx4.j(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel", f = "BCRecommendationViewModel.kt", l = {69, 86}, m = "loadRecommended")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(wx4 wx4, qn7 qn7) {
            super(qn7);
            this.this$0 = wx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$loadRecommended$data$1", f = "BCRecommendationViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super List<? extends Object>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $recommended;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(List list, qn7 qn7) {
            super(2, qn7);
            this.$recommended = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.$recommended, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super List<? extends Object>> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return py4.k(this.$recommended);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$loadRecommended$result$1", f = "BCRecommendationViewModel.kt", l = {69}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends mt4>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(wx4 wx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = wx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends mt4>>> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.h;
                this.L$0 = iv7;
                this.label = 1;
                Object u = tt4.u(this);
                return u == d ? d : u;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$refresh$1", f = "BCRecommendationViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wx4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(wx4 wx4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = wx4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                wx4 wx4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (wx4.j(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public wx4(tt4 tt4) {
        pq7.c(tt4, "challengeRepository");
        this.h = tt4;
        String simpleName = wx4.class.getSimpleName();
        pq7.b(simpleName, "BCRecommendationViewModel::class.java.simpleName");
        this.f4015a = simpleName;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> d() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Boolean> e() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Object>> f() {
        return cz4.a(this.e);
    }

    @DexIgnore
    public final LiveData<cl7<mt4, Integer>> g() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Integer> h() {
        return this.g;
    }

    @DexIgnore
    public final void i() {
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
        // Method dump skipped, instructions count: 283
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wx4.j(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void k() {
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void l(mt4 mt4, int i) {
        pq7.c(mt4, "recommended");
        this.d.l(hl7.a(mt4, Integer.valueOf(i)));
    }

    @DexIgnore
    public final void m(int i) {
        this.g.l(Integer.valueOf(i));
    }
}
