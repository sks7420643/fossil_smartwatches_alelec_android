package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lp {
    @DexIgnore
    public static /* final */ kp B; // = new kp(null);
    @DexIgnore
    public boolean A;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2227a;
    @DexIgnore
    public fs b;
    @DexIgnore
    public /* final */ d5 c;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<lp, tl7>> d;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<lp, tl7>> e;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<lp, tl7>> f;
    @DexIgnore
    public CopyOnWriteArrayList<rp7<lp, tl7>> g;
    @DexIgnore
    public CopyOnWriteArrayList<vp7<lp, Float, tl7>> h;
    @DexIgnore
    public /* final */ ArrayList<ow> i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public a9 k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public lp n;
    @DexIgnore
    public /* final */ Handler o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ md0 q;
    @DexIgnore
    public /* final */ el r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public nr v;
    @DexIgnore
    public /* final */ k5 w;
    @DexIgnore
    public /* final */ i60 x;
    @DexIgnore
    public /* final */ yp y;
    @DexIgnore
    public String z;

    @DexIgnore
    public lp(k5 k5Var, i60 i60, yp ypVar, String str, boolean z2) {
        this.w = k5Var;
        this.x = i60;
        this.y = ypVar;
        this.z = str;
        this.A = z2;
        this.f2227a = ey1.a(ypVar);
        this.c = new tj(this);
        this.d = new CopyOnWriteArrayList<>();
        this.e = new CopyOnWriteArrayList<>();
        this.f = new CopyOnWriteArrayList<>();
        this.g = new CopyOnWriteArrayList<>();
        this.h = new CopyOnWriteArrayList<>();
        this.i = hm7.c(ow.DEVICE_INFORMATION);
        xo xoVar = xo.b;
        this.k = a9.NORMAL;
        this.l = System.currentTimeMillis();
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.o = new Handler(myLooper);
            this.p = 30000;
            this.q = new md0(new rl(this));
            this.r = new el(this);
            if (this.A) {
                d90.i.d(new a90(ey1.a(this.y), v80.b, this.w.x, ey1.a(this.y), this.z, true, null, null, null, null, 960));
            }
            this.v = new nr(this.y, zq.NOT_START, null, null, 12);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ lp(k5 k5Var, i60 i60, yp ypVar, String str, boolean z2, int i2) {
        this(k5Var, i60, ypVar, (i2 & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, (i2 & 16) != 0 ? true : z2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.lp */
    /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.fossil.vp7<com.fossil.lp, java.lang.Float, com.fossil.tl7>> */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void h(lp lpVar, lp lpVar2, rp7 rp7, rp7 rp72, vp7 vp7, rp7 rp73, rp7 rp74, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 8) != 0) {
                vp7 = th.b;
            }
            if ((i2 & 16) != 0) {
                rp73 = gi.b;
            }
            if ((i2 & 32) != 0) {
                rp74 = ui.b;
            }
            if (!lpVar.t) {
                lpVar.n = lpVar2;
                if (lpVar2 != 0) {
                    lpVar2.v(rp7);
                    lpVar2.s(new hj(lpVar, rp74, rp72));
                    if (!lpVar2.t) {
                        lpVar2.h.add(vp7);
                    }
                    lpVar2.a(rp73);
                    lpVar2.F();
                    return;
                }
                return;
            }
            lpVar.l(lpVar.v);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeSubPhase");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v30, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.fossil.rp7<com.fossil.fs, com.fossil.tl7>> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v31, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.fossil.vp7<com.fossil.fs, java.lang.Float, com.fossil.tl7>> */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void i(lp lpVar, fs fsVar, rp7 rp7, rp7 rp72, vp7 vp7, rp7 rp73, rp7 rp74, int i2, Object obj) {
        boolean z2;
        if (obj == null) {
            if ((i2 & 8) != 0) {
                vp7 = rf.b;
            }
            if ((i2 & 16) != 0) {
                rp73 = fg.b;
            }
            if ((i2 & 32) != 0) {
                rp74 = tg.b;
            }
            if (lpVar.t || ((z2 = lpVar.u) && (!z2 || !lpVar.r(fsVar)))) {
                lpVar.l(lpVar.v);
                return;
            }
            fsVar.w = fsVar.w && lpVar.A;
            String str = lpVar.z;
            fsVar.d = str;
            a90 a90 = fsVar.f;
            if (a90 != null) {
                a90.i = str;
            }
            String a2 = ey1.a(lpVar.y);
            fsVar.c = a2;
            a90 a902 = fsVar.f;
            if (a902 != null) {
                a902.h = a2;
            }
            lpVar.b = fsVar;
            if (fsVar != null) {
                fsVar.o(rp7);
                fsVar.c(new gh(lpVar, rp74, rp72));
                if (!fsVar.t) {
                    fsVar.o.add(vp7);
                }
                if (!fsVar.t) {
                    fsVar.n.add(rp73);
                } else {
                    rp73.invoke(fsVar);
                }
                if (!fsVar.t) {
                    ky1 ky1 = ky1.DEBUG;
                    fsVar.z().toString(2);
                    long currentTimeMillis = System.currentTimeMillis();
                    a90 a903 = fsVar.f;
                    if (a903 != null) {
                        a903.b = currentTimeMillis;
                    }
                    a90 a904 = fsVar.f;
                    if (a904 != null) {
                        a904.n = gy1.c(a904.n, g80.k(g80.k(g80.k(fsVar.z(), jd0.C1, Double.valueOf(hy1.f(fsVar.e))), jd0.t1, Double.valueOf(hy1.f(currentTimeMillis))), jd0.v1, Long.valueOf(fsVar.x())));
                    }
                    fsVar.n(fsVar.p);
                    k5 k5Var = fsVar.y;
                    bs bsVar = fsVar.q;
                    if (!k5Var.i.contains(bsVar)) {
                        k5Var.i.add(bsVar);
                    }
                    k5 k5Var2 = fsVar.y;
                    ds dsVar = fsVar.r;
                    if (!k5Var2.j.contains(dsVar)) {
                        k5Var2.j.add(dsVar);
                    }
                    fsVar.q();
                    return;
                }
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeRequest");
    }

    @DexIgnore
    public static /* synthetic */ void j(lp lpVar, hs hsVar, hs hsVar2, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                hsVar2 = hsVar;
            }
            lpVar.m(hsVar, hsVar2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryExecuteRequest");
    }

    @DexIgnore
    public final long A() {
        return this.l;
    }

    @DexIgnore
    public abstract void B();

    @DexIgnore
    public JSONObject C() {
        return new JSONObject();
    }

    @DexIgnore
    public void D() {
    }

    @DexIgnore
    public JSONObject E() {
        return new JSONObject();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        r0 = com.fossil.ky1.DEBUG;
        z().toString();
        r0 = com.fossil.ky1.DEBUG;
        r0 = r6.p;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        if (r0 <= 0) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        r6.o.postDelayed(r6.q, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r0 = r6.x.b();
        r1 = r6.r;
        r0.f2965a.b(r6);
        r0.c.put(r6, r1);
        r0.g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void F() {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            com.fossil.id0 r0 = com.fossil.id0.i
            java.lang.Object r0 = r0.m()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            boolean r0 = com.fossil.pq7.a(r0, r2)
            if (r0 == 0) goto L_0x0058
            boolean r0 = r6.s
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            monitor-enter(r1)
            boolean r0 = r6.s     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x001f
            monitor-exit(r1)
        L_0x001e:
            return
        L_0x001f:
            r0 = 1
            r6.s = r0
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            monitor-exit(r1)
            com.fossil.ky1 r0 = com.fossil.ky1.DEBUG
            java.util.ArrayList r0 = r6.z()
            r0.toString()
            com.fossil.ky1 r0 = com.fossil.ky1.DEBUG
            long r0 = r6.p
            r2 = 0
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x003f
            android.os.Handler r2 = r6.o
            com.fossil.md0 r3 = r6.q
            r2.postDelayed(r3, r0)
        L_0x003f:
            com.fossil.i60 r0 = r6.x
            com.fossil.qe r0 = r0.b()
            com.fossil.el r1 = r6.r
            com.fossil.nd0<com.fossil.lp> r2 = r0.f2965a
            r2.b(r6)
            java.util.Hashtable<com.fossil.lp, com.fossil.el> r2 = r0.c
            r2.put(r6, r1)
            r0.g()
            goto L_0x001e
        L_0x0055:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0058:
            com.fossil.nr r0 = r6.v
            com.fossil.zq r2 = com.fossil.zq.SUCCESS
            r5 = 13
            r3 = r1
            r4 = r1
            com.fossil.nr r0 = com.fossil.nr.a(r0, r1, r2, r3, r4, r5)
            r6.l(r0)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lp.F():void");
    }

    @DexIgnore
    public final lp a(rp7<? super lp, tl7> rp7) {
        if (!this.t) {
            this.g.add(rp7);
        } else {
            rp7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public fs b(hs hsVar) {
        return null;
    }

    @DexIgnore
    public final void c() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            this.w.k.remove(this.c);
            qe b2 = this.x.b();
            b2.e.b(this);
            b2.f2965a.remove(this);
            b2.b.remove(this);
            b2.c.remove(this);
            JSONObject c2 = gy1.c(g80.k(g80.k(new JSONObject(), jd0.k, B.a(this.v)), jd0.O0, ey1.a(this.v.c)), E());
            if (this.v.d.d != lw.b) {
                g80.k(c2, jd0.P0, g80.k(g80.k(new JSONObject(), jd0.q, this.v.d.b), jd0.g2, this.v.d.c));
            }
            a90 a90 = new a90(ey1.a(this.y), v80.d, this.w.x, ey1.a(this.y), this.z, this.v.c == zq.SUCCESS, null, null, null, c2, 448);
            if (this.A) {
                d90.i.d(a90);
            }
            if (this instanceof mi) {
                y80.i.d(a90);
            } else if ((this instanceof cj) || (this instanceof pi)) {
                x80.i.d(a90);
            }
            nr nrVar = this.v;
            if (nrVar.c == zq.SUCCESS) {
                ky1 ky1 = ky1.DEBUG;
                ox1.toJSONString$default(nrVar, 0, 1, null);
                E().toString(2);
                Iterator<T> it = this.e.iterator();
                while (it.hasNext()) {
                    it.next().invoke(this);
                }
            } else {
                ky1 ky12 = ky1.ERROR;
                ox1.toJSONString$default(nrVar, 0, 1, null);
                Iterator<T> it2 = this.f.iterator();
                while (it2.hasNext()) {
                    it2.next().invoke(this);
                }
            }
            Iterator<T> it3 = this.g.iterator();
            while (it3.hasNext()) {
                it3.next().invoke(this);
            }
        }
    }

    @DexIgnore
    public final void d(float f2) {
        Iterator<T> it = this.h.iterator();
        while (it.hasNext()) {
            it.next().invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void e(f5 f5Var) {
        if (df.f778a[f5Var.ordinal()] == 1) {
            k(zq.CONNECTION_DROPPED);
        }
    }

    @DexIgnore
    public void f(a9 a9Var) {
        this.k = a9Var;
    }

    @DexIgnore
    public void k(zq zqVar) {
        if (!this.t && !this.u) {
            ky1 ky1 = ky1.DEBUG;
            ey1.a(zqVar);
            this.o.removeCallbacksAndMessages(null);
            this.u = true;
            this.v = nr.a(this.v, null, zqVar, null, null, 13);
            fs fsVar = this.b;
            if (fsVar == null || fsVar.t) {
                lp lpVar = this.n;
                if (lpVar == null || lpVar.t) {
                    l(this.v);
                } else if (lpVar != null) {
                    lpVar.k(zqVar);
                }
            } else if (fsVar != null) {
                fsVar.l(lw.t);
            }
        }
    }

    @DexIgnore
    public final void l(nr nrVar) {
        this.v = nr.a(nrVar, this.y, null, null, null, 14);
        c();
    }

    @DexIgnore
    public final void m(hs hsVar, hs hsVar2) {
        n(hsVar, new dm(this, hsVar2));
    }

    @DexIgnore
    public final void n(hs hsVar, gp7<tl7> gp7) {
        fs b2 = b(hsVar);
        if (b2 == null) {
            l(nr.a(this.v, null, zq.FLOW_BROKEN, null, null, 13));
        } else if (this.m < b2.z) {
            i(this, b2, qm.b, new zn(this, gp7), null, new lo(this), yo.b, 8, null);
        } else {
            l(this.v);
        }
    }

    @DexIgnore
    public final void o(mw mwVar) {
        this.v = nr.a(this.v, null, zq.I.a(mwVar), mwVar, null, 9);
        c();
    }

    @DexIgnore
    public final void p(md0 md0) {
        if (!md0.b) {
            md0.b = true;
            this.o.removeCallbacksAndMessages(md0);
        }
    }

    @DexIgnore
    public boolean q(lp lpVar) {
        return u(lpVar) || this.y != lpVar.y;
    }

    @DexIgnore
    public boolean r(fs fsVar) {
        return false;
    }

    @DexIgnore
    public final lp s(rp7<? super lp, tl7> rp7) {
        if (!this.t) {
            this.f.add(rp7);
        } else if (this.v.c != zq.SUCCESS) {
            rp7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public boolean t() {
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return ey1.a(this.y) + '(' + this.z + ')';
    }

    @DexIgnore
    public final boolean u(lp lpVar) {
        if (pq7.a(this.n, lpVar)) {
            return true;
        }
        lp lpVar2 = this.n;
        return lpVar2 != null && lpVar2.u(lpVar);
    }

    @DexIgnore
    public final lp v(rp7<? super lp, tl7> rp7) {
        if (!this.t) {
            this.e.add(rp7);
        } else if (this.v.c == zq.SUCCESS) {
            rp7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final boolean w() {
        return t() && this.x.a().d().compareTo(hd0.y.k()) >= 0;
    }

    @DexIgnore
    public Object x() {
        return tl7.f3441a;
    }

    @DexIgnore
    public a9 y() {
        return this.k;
    }

    @DexIgnore
    public ArrayList<ow> z() {
        return this.i;
    }
}
