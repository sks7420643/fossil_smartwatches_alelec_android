package com.fossil;

import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g97 {
    @rj4("id")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f1278a;
    @DexIgnore
    @rj4(Firmware.COLUMN_DOWNLOAD_URL)
    public String b;
    @DexIgnore
    @rj4("checksum")
    public String c;
    @DexIgnore
    @rj4("uid")
    public String d;
    @DexIgnore
    @rj4("createdAt")
    public String e;
    @DexIgnore
    @rj4("updatedAt")
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public int h;

    @DexIgnore
    public g97(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i) {
        pq7.c(str, "id");
        pq7.c(str2, Firmware.COLUMN_DOWNLOAD_URL);
        pq7.c(str3, "checkSum");
        pq7.c(str4, "uid");
        pq7.c(str5, "createdAt");
        pq7.c(str6, "updatedAt");
        this.f1278a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
        this.h = i;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final String d() {
        return this.f1278a;
    }

    @DexIgnore
    public final String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof g97) {
                g97 g97 = (g97) obj;
                if (!pq7.a(this.f1278a, g97.f1278a) || !pq7.a(this.b, g97.b) || !pq7.a(this.c, g97.c) || !pq7.a(this.d, g97.d) || !pq7.a(this.e, g97.e) || !pq7.a(this.f, g97.f) || !pq7.a(this.g, g97.g) || this.h != g97.h) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int f() {
        return this.h;
    }

    @DexIgnore
    public final String g() {
        return this.d;
    }

    @DexIgnore
    public final String h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1278a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.f;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.g;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i) * 31) + this.h;
    }

    @DexIgnore
    public final void i(int i) {
        this.h = i;
    }

    @DexIgnore
    public String toString() {
        return "WFBackgroundPhoto(id=" + this.f1278a + ", downloadUrl=" + this.b + ", checkSum=" + this.c + ", uid=" + this.d + ", createdAt=" + this.e + ", updatedAt=" + this.f + ", localPath=" + this.g + ", pinType=" + this.h + ")";
    }
}
