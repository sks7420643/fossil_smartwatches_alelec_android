package com.fossil;

import com.fossil.hz1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public a a(int i) {
            e(Integer.valueOf(i));
            return this;
        }

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract a c(lz1 lz1);

        @DexIgnore
        public abstract a d(qz1 qz1);

        @DexIgnore
        public abstract a e(Integer num);

        @DexIgnore
        public abstract a f(String str);

        @DexIgnore
        public abstract a g(List<mz1> list);

        @DexIgnore
        public abstract nz1 h();

        @DexIgnore
        public abstract a i(long j);

        @DexIgnore
        public a j(String str) {
            f(str);
            return this;
        }
    }

    @DexIgnore
    public static a a() {
        return new hz1.b();
    }

    @DexIgnore
    public abstract lz1 b();

    @DexIgnore
    public abstract List<mz1> c();

    @DexIgnore
    public abstract Integer d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract qz1 f();

    @DexIgnore
    public abstract long g();

    @DexIgnore
    public abstract long h();
}
