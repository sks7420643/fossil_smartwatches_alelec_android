package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.nk5;
import com.fossil.oi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yd6 extends pv5 implements xd6 {
    @DexIgnore
    public g37<u15> g;
    @DexIgnore
    public wd6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.C;
            Date date = new Date();
            pq7.b(view, "it");
            Context context = view.getContext();
            pq7.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ActiveTimeOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.xd6
    public void G(mv5 mv5, ArrayList<String> arrayList) {
        u15 a2;
        OverviewDayChart overviewDayChart;
        pq7.c(mv5, "baseModel");
        pq7.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayFragment", "showDayDetails - baseModel=" + mv5);
        g37<u15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public final void K6(zc5 zc5, WorkoutSession workoutSession) {
        String format;
        AppCompatImageView appCompatImageView;
        if (zc5 != null) {
            View n = zc5.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            oi5.a aVar = oi5.Companion;
            cl7<Integer, Integer> a2 = aVar.a(aVar.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = um5.c(context, a2.getSecond().intValue());
            zc5.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = zc5.r;
            pq7.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            DateTime editedEndTime = workoutSession.getEditedEndTime();
            if (editedEndTime != null) {
                long millis = editedEndTime.getMillis();
                DateTime editedStartTime = workoutSession.getEditedStartTime();
                if (editedStartTime != null) {
                    gl7<Integer, Integer, Integer> e0 = lk5.e0((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                    pq7.b(e0, "DateHelper.getTimeValues(duration.toInt())");
                    FlexibleTextView flexibleTextView2 = zc5.s;
                    pq7.b(flexibleTextView2, "it.ftvWorkoutValue");
                    if (pq7.d(e0.getFirst().intValue(), 0) > 0) {
                        hr7 hr7 = hr7.f1520a;
                        String c2 = um5.c(context, 2131886698);
                        pq7.b(c2, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
                        format = String.format(c2, Arrays.copyOf(new Object[]{e0.getFirst(), e0.getSecond(), e0.getThird()}, 3));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                    } else if (pq7.d(e0.getSecond().intValue(), 0) > 0) {
                        hr7 hr72 = hr7.f1520a;
                        String c3 = um5.c(context, 2131886699);
                        pq7.b(c3, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
                        format = String.format(c3, Arrays.copyOf(new Object[]{e0.getSecond(), e0.getThird()}, 2));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                    } else {
                        hr7 hr73 = hr7.f1520a;
                        String c4 = um5.c(context, 2131886700);
                        pq7.b(c4, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
                        format = String.format(c4, Arrays.copyOf(new Object[]{e0.getThird()}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                    }
                    flexibleTextView2.setText(format);
                    FlexibleTextView flexibleTextView3 = zc5.q;
                    pq7.b(flexibleTextView3, "it.ftvWorkoutTime");
                    flexibleTextView3.setText(lk5.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
                    nk5.a aVar2 = nk5.o;
                    wd6 wd6 = this.h;
                    String d = aVar2.w(wd6 != null ? wd6.n() : null) ? qn5.l.a().d("dianaActiveMinutesTab") : qn5.l.a().d("hybridActiveMinutesTab");
                    if (d != null && (appCompatImageView = zc5.t) != null) {
                        appCompatImageView.setColorFilter(Color.parseColor(d));
                        return;
                    }
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void L6() {
        u15 a2;
        OverviewDayChart overviewDayChart;
        g37<u15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            nk5.a aVar = nk5.o;
            wd6 wd6 = this.h;
            if (aVar.w(wd6 != null ? wd6.n() : null)) {
                overviewDayChart.D("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(wd6 wd6) {
        pq7.c(wd6, "presenter");
        this.h = wd6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        u15 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onCreateView");
        u15 u15 = (u15) aq0.f(layoutInflater, 2131558493, viewGroup, false, A6());
        u15.r.setOnClickListener(a.b);
        this.g = new g37<>(this, u15);
        L6();
        g37<u15> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onResume");
        L6();
        wd6 wd6 = this.h;
        if (wd6 != null) {
            wd6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onStop");
        wd6 wd6 = this.h;
        if (wd6 != null) {
            wd6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.xd6
    public void v(boolean z, List<WorkoutSession> list) {
        u15 a2;
        View n;
        View n2;
        pq7.c(list, "workoutSessions");
        g37<u15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                pq7.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.s, list.get(0));
                if (size == 1) {
                    zc5 zc5 = a2.t;
                    if (zc5 != null && (n2 = zc5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                zc5 zc52 = a2.t;
                if (!(zc52 == null || (n = zc52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            pq7.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
