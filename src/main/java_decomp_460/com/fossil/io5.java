package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.wearables.fsl.utils.TimeUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class io5 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        MFLogger.d("TimeTickReceiver", "onReceive - action=" + action);
        if (!TextUtils.isEmpty(action) && pq7.a(action, "android.intent.action.TIME_TICK")) {
            Calendar instance = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
            pq7.b(instance, "calendar");
            String format = simpleDateFormat.format(instance.getTime());
            on5 on5 = new on5(context);
            String U = on5.U();
            MFLogger.d("TimeTickReceiver", "onReceive - day= " + format + ", widgetsDateChanged= " + U);
            if (!vt7.k(U, format, false, 2, null) && context != null) {
                MFLogger.d("TimeTickReceiver", "onReceive - need to resetAllContentWidgetsUI");
                on5.X1(format);
            }
        }
    }
}
