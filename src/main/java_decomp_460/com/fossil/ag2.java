package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ag2 {
    @DexIgnore
    public static ag2 b; // = new ag2();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public zf2 f264a; // = null;

    @DexIgnore
    public static zf2 a(Context context) {
        return b.b(context);
    }

    @DexIgnore
    public final zf2 b(Context context) {
        zf2 zf2;
        synchronized (this) {
            if (this.f264a == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                this.f264a = new zf2(context);
            }
            zf2 = this.f264a;
        }
        return zf2;
    }
}
