package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.f57;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ f57.b f2255a;
    @DexIgnore
    public /* final */ uy4 b; // = uy4.d.b();
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2256a; // = qn5.l.a().d("nonBrandSurface");
        @DexIgnore
        public /* final */ String b; // = qn5.l.a().d("nonBrandPlaceholderBackground");
        @DexIgnore
        public /* final */ jf5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(jf5 jf5) {
            super(jf5.n());
            pq7.c(jf5, "binding");
            this.c = jf5;
        }

        @DexIgnore
        public final void a(ms4 ms4, long j, int i, f57.b bVar, uy4 uy4) {
            String str;
            Date f;
            String str2;
            pq7.c(ms4, "player");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            jf5 jf5 = this.c;
            FlexibleTextView flexibleTextView = jf5.t;
            pq7.b(flexibleTextView, "tvLastSync");
            jl5 jl5 = jl5.b;
            Date f2 = ms4.f();
            flexibleTextView.setText(jl5.n(f2 != null ? f2.getTime() : 0));
            Integer n = ms4.n();
            int intValue = n != null ? n.intValue() : 0;
            Integer m = ms4.m();
            int intValue2 = intValue - (m != null ? m.intValue() : 0);
            hr7 hr7 = hr7.f1520a;
            FlexibleTextView flexibleTextView2 = jf5.v;
            pq7.b(flexibleTextView2, "tvSteps");
            String c2 = um5.c(flexibleTextView2.getContext(), 2131886310);
            pq7.b(c2, "LanguageHelper.getString\u2026rd_FullView_Label__Steps)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue2)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            String d = qn5.l.a().d("primaryText");
            if (d == null) {
                d = "#242424";
            }
            int parseColor = Color.parseColor(d);
            String d2 = qn5.l.a().d("nonBrandNonReachGoal");
            if (d2 == null) {
                d2 = "#a7a7a7";
            }
            int parseColor2 = Color.parseColor(d2);
            if (pq7.a(ms4.p(), Boolean.TRUE)) {
                FlexibleTextView flexibleTextView3 = jf5.s;
                pq7.b(flexibleTextView3, "tvFullName");
                View n2 = jf5.n();
                pq7.b(n2, "root");
                flexibleTextView3.setText(um5.c(n2.getContext(), 2131887315));
                ImageView imageView = jf5.r;
                pq7.b(imageView, "ivAvatar");
                ty4.b(imageView, ms4.g(), "", bVar, uy4);
                FlexibleTextView flexibleTextView4 = jf5.u;
                pq7.b(flexibleTextView4, "tvRank");
                Integer h = ms4.h();
                if (h == null || (str2 = String.valueOf(h.intValue())) == null) {
                    str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                flexibleTextView4.setText(str2);
                jf5.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                jf5.s.setTextColor(parseColor);
                FlexibleTextView flexibleTextView5 = jf5.v;
                pq7.b(flexibleTextView5, "tvSteps");
                flexibleTextView5.setText(jl5.b(jl5.b, String.valueOf(intValue2), format, 0, 4, null));
            } else {
                if (pq7.a(ms4.d(), PortfolioApp.h0.c().l0())) {
                    FlexibleTextView flexibleTextView6 = jf5.s;
                    pq7.b(flexibleTextView6, "tvFullName");
                    View n3 = jf5.n();
                    pq7.b(n3, "root");
                    flexibleTextView6.setText(um5.c(n3.getContext(), 2131886250));
                    if (this.b != null) {
                        jf5.n().setBackgroundColor(Color.parseColor(this.b));
                    }
                } else {
                    FlexibleTextView flexibleTextView7 = jf5.s;
                    pq7.b(flexibleTextView7, "tvFullName");
                    flexibleTextView7.setText(hz4.f1561a.b(ms4.c(), ms4.e(), ms4.i()));
                    if (this.f2256a != null) {
                        jf5.n().setBackgroundColor(Color.parseColor(this.f2256a));
                    }
                }
                ImageView imageView2 = jf5.r;
                pq7.b(imageView2, "ivAvatar");
                ty4.b(imageView2, ms4.g(), ms4.e(), bVar, uy4);
                if (pq7.a("left_after_start", ms4.k())) {
                    FlexibleTextView flexibleTextView8 = jf5.u;
                    pq7.b(flexibleTextView8, "tvRank");
                    flexibleTextView8.setText("");
                    jf5.u.setCompoundDrawablesWithIntrinsicBounds(2131231026, 0, 0, 0);
                    jf5.s.setTextColor(parseColor2);
                    FlexibleTextView flexibleTextView9 = jf5.v;
                    pq7.b(flexibleTextView9, "tvSteps");
                    flexibleTextView9.setText(jl5.b.a(String.valueOf(intValue2), format, 0));
                } else {
                    FlexibleTextView flexibleTextView10 = jf5.u;
                    pq7.b(flexibleTextView10, "tvRank");
                    Integer h2 = ms4.h();
                    if (h2 == null || (str = String.valueOf(h2.intValue())) == null) {
                        str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                    }
                    flexibleTextView10.setText(str);
                    jf5.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    jf5.s.setTextColor(parseColor);
                    FlexibleTextView flexibleTextView11 = jf5.v;
                    pq7.b(flexibleTextView11, "tvSteps");
                    flexibleTextView11.setText(jl5.b(jl5.b, String.valueOf(intValue2), format, 0, 4, null));
                }
            }
            String k = ms4.k();
            if (k != null && k.hashCode() == -1402931637 && k.equals("completed") && i != -1 && (f = ms4.f()) != null) {
                long time = (f.getTime() - j) / ((long) 1000);
                FlexibleTextView flexibleTextView12 = jf5.t;
                pq7.b(flexibleTextView12, "tvLastSync");
                flexibleTextView12.setText(jl5.b.s((int) time));
            }
        }
    }

    @DexIgnore
    public lv4(int i, long j, int i2) {
        this.c = j;
        this.d = i2;
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.f2255a = f;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        return list.get(i) instanceof ms4;
    }

    @DexIgnore
    public void b(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        a aVar = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof ms4)) {
            obj = null;
        }
        ms4 ms4 = (ms4) obj;
        if (viewHolder instanceof a) {
            aVar = viewHolder;
        }
        a aVar2 = aVar;
        if (ms4 != null && aVar2 != null) {
            aVar2.a(ms4, this.c, this.d, this.f2255a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder c(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        jf5 z = jf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemPlayerListBinding.in\u2026(inflater, parent, false)");
        return new a(z);
    }
}
