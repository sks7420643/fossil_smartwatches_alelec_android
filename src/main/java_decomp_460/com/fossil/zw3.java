package com.fossil;

import android.graphics.Matrix;
import android.util.Property;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zw3 extends Property<ImageView, Matrix> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Matrix f4549a; // = new Matrix();

    @DexIgnore
    public zw3() {
        super(Matrix.class, "imageMatrixProperty");
    }

    @DexIgnore
    /* renamed from: a */
    public Matrix get(ImageView imageView) {
        this.f4549a.set(imageView.getImageMatrix());
        return this.f4549a;
    }

    @DexIgnore
    /* renamed from: b */
    public void set(ImageView imageView, Matrix matrix) {
        imageView.setImageMatrix(matrix);
    }
}
