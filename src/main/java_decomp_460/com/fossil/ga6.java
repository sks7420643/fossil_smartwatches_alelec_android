package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cw5;
import com.fossil.na6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga6 extends pv5 {
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public na6 g;
    @DexIgnore
    public po4 h;
    @DexIgnore
    public cw5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ga6 a(String str) {
            pq7.c(str, MicroAppSetting.SETTING);
            ga6 ga6 = new ga6();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_SETTING, str);
            ga6.setArguments(bundle);
            return ga6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements cw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ga6 f1283a;

        @DexIgnore
        public b(ga6 ga6) {
            this.f1283a = ga6;
        }

        @DexIgnore
        @Override // com.fossil.cw5.b
        public void a(AddressWrapper addressWrapper) {
            pq7.c(addressWrapper, "address");
            this.f1283a.Q6(addressWrapper, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends i67 {
        @DexIgnore
        public /* final */ /* synthetic */ ga6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, int i2, ga6 ga6) {
            super(i, i2);
            this.f = ga6;
        }

        @DexIgnore
        @Override // com.fossil.cv0.f
        public void B(RecyclerView.ViewHolder viewHolder, int i) {
            pq7.c(viewHolder, "viewHolder");
            int adapterPosition = viewHolder.getAdapterPosition();
            cw5 cw5 = this.f.i;
            ga6.M6(this.f).l(cw5 != null ? cw5.i(adapterPosition) : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ga6 b;

        @DexIgnore
        public d(ga6 ga6) {
            this.b = ga6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ga6 b;

        @DexIgnore
        public e(ga6 ga6) {
            this.b = ga6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<na6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ga6 f1284a;

        @DexIgnore
        public f(ga6 ga6) {
            this.f1284a = ga6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(na6.a aVar) {
            List<AddressWrapper> a2 = aVar.a();
            if (a2 != null) {
                this.f1284a.R6(a2);
            }
        }
    }

    /*
    static {
        pq7.b(ga6.class.getSimpleName(), "CommuteTimeWatchAppSetti\u2026nt::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ na6 M6(ga6 ga6) {
        na6 na6 = ga6.g;
        if (na6 != null) {
            return na6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        x0(true);
        return true;
    }

    @DexIgnore
    public final void P6() {
        na6 na6 = this.g;
        if (na6 == null) {
            pq7.n("mViewModel");
            throw null;
        } else if (na6.j()) {
            Q6(new AddressWrapper(), true);
        } else {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.L(childFragmentManager);
        }
    }

    @DexIgnore
    public final void Q6(AddressWrapper addressWrapper, boolean z) {
        ArrayList<String> arrayList = null;
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY_SELECTED_ADDRESS", addressWrapper);
        na6 na6 = this.g;
        if (na6 != null) {
            CommuteTimeWatchAppSetting f2 = na6.f();
            if (f2 != null) {
                arrayList = f2.getListAddressNameExceptOf(addressWrapper);
            }
            bundle.putStringArrayList("KEY_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_HAVING_MAP_RESULT", !z);
            CommuteTimeSettingsDetailActivity.A.a(this, bundle, 113);
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void R6(List<AddressWrapper> list) {
        cw5 cw5 = this.i;
        if (cw5 != null) {
            cw5.l(pm7.j0(list));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 113) {
            AddressWrapper addressWrapper = (AddressWrapper) intent.getParcelableExtra("KEY_SELECTED_ADDRESS");
            na6 na6 = this.g;
            if (na6 != null) {
                na6.k(addressWrapper);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        String str = null;
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().J1().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(na6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ngsViewModel::class.java)");
            na6 na6 = (na6) a2;
            this.g = na6;
            if (na6 != null) {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    str = arguments.getString(Constants.USER_SETTING);
                }
                na6.i(str);
                return;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        t35 t35 = (t35) aq0.f(layoutInflater, 2131558519, viewGroup, false, A6());
        t35.s.setOnClickListener(new d(this));
        t35.r.setOnClickListener(new e(this));
        cw5 cw5 = new cw5();
        cw5.m(new b(this));
        this.i = cw5;
        RecyclerView recyclerView = t35.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        new cv0(new c(0, 4, this)).g(recyclerView);
        na6 na6 = this.g;
        if (na6 != null) {
            na6.h().h(getViewLifecycleOwner(), new f(this));
            na6 na62 = this.g;
            if (na62 != null) {
                na62.m();
                new g37(this, t35);
                pq7.b(t35, "binding");
                return t35.n();
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void x0(boolean z) {
        if (z) {
            Intent intent = new Intent();
            na6 na6 = this.g;
            if (na6 != null) {
                intent.putExtra("COMMUTE_TIME_WATCH_APP_SETTING", na6.g());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }
}
