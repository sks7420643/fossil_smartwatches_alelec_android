package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jw4 implements Factory<iw4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f1821a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public jw4(Provider<tt4> provider, Provider<on5> provider2) {
        this.f1821a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static jw4 a(Provider<tt4> provider, Provider<on5> provider2) {
        return new jw4(provider, provider2);
    }

    @DexIgnore
    public static iw4 c(tt4 tt4, on5 on5) {
        return new iw4(tt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public iw4 get() {
        return c(this.f1821a.get(), this.b.get());
    }
}
