package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n extends wk1 {
    @DexIgnore
    public /* final */ String[] b;

    @DexIgnore
    public n(String[] strArr) {
        this.b = strArr;
    }

    @DexIgnore
    @Override // com.fossil.wk1
    public boolean a(e60 e60) {
        if (this.b.length == 0) {
            return true;
        }
        String serialNumber = e60.u.getSerialNumber();
        for (String str : this.b) {
            if (vt7.s(serialNumber, str, false, 2, null)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.H1, "serial_number_prefixes"), jd0.I1, ay1.b(this.b));
    }
}
