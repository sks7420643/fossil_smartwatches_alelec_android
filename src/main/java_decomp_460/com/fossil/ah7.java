package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ah7 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        if (fg7.M() && ig7.r != null) {
            if (fg7.J()) {
                gh7.b(ig7.r).g(new mg7(ig7.r, ig7.a(ig7.r, false, null), 2, th, thread, null), null, false, true);
                ig7.m.c("MTA has caught the following uncaught exception:");
                ig7.m.g(th);
            }
            ig7.u(ig7.r);
            if (ig7.n != null) {
                ig7.m.b("Call the original uncaught exception handler.");
                if (!(ig7.n instanceof ah7)) {
                    ig7.n.uncaughtException(thread, th);
                }
            }
        }
    }
}
