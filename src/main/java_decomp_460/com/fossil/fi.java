package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fi extends cq {
    @DexIgnore
    public /* final */ ArrayList<ow> E; // = by1.a(this.C, hm7.c(ow.ASYNC));
    @DexIgnore
    public /* final */ xn1 F;

    @DexIgnore
    public fi(k5 k5Var, i60 i60, xn1 xn1) {
        super(k5Var, i60, yp.B, new ws(xn1, k5Var));
        this.F = xn1;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.cq
    public JSONObject C() {
        return g80.k(super.C(), jd0.u0, this.F.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.cq
    public ArrayList<ow> z() {
        return this.E;
    }
}
