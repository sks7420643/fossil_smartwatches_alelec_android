package com.fossil;

import android.content.Context;
import android.content.res.Resources;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g62 extends h62 {
    @DexIgnore
    @Deprecated
    public static /* final */ int f; // = h62.f1430a;

    @DexIgnore
    public static Context d(Context context) {
        return h62.d(context);
    }

    @DexIgnore
    public static Resources e(Context context) {
        return h62.e(context);
    }

    @DexIgnore
    @Deprecated
    public static int g(Context context) {
        return h62.g(context);
    }

    @DexIgnore
    @Deprecated
    public static int h(Context context, int i) {
        return h62.h(context, i);
    }
}
