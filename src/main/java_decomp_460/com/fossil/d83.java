package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d83 implements e83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f745a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.validation.internal_limits_internal_event_params", false);

    @DexIgnore
    @Override // com.fossil.e83
    public final boolean zza() {
        return f745a.o().booleanValue();
    }
}
