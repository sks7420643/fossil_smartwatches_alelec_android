package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.g04;
import com.fossil.h04;
import com.fossil.i04;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c04 extends Drawable implements bm0, j04 {
    @DexIgnore
    public static /* final */ Paint B; // = new Paint(1);
    @DexIgnore
    public /* final */ RectF A;
    @DexIgnore
    public c b;
    @DexIgnore
    public /* final */ i04.g[] c;
    @DexIgnore
    public /* final */ i04.g[] d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ Matrix f;
    @DexIgnore
    public /* final */ Path g;
    @DexIgnore
    public /* final */ Path h;
    @DexIgnore
    public /* final */ RectF i;
    @DexIgnore
    public /* final */ RectF j;
    @DexIgnore
    public /* final */ Region k;
    @DexIgnore
    public /* final */ Region l;
    @DexIgnore
    public g04 m;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ uz3 u;
    @DexIgnore
    public /* final */ h04.a v;
    @DexIgnore
    public /* final */ h04 w;
    @DexIgnore
    public PorterDuffColorFilter x;
    @DexIgnore
    public PorterDuffColorFilter y;
    @DexIgnore
    public Rect z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements h04.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.h04.a
        public void a(i04 i04, Matrix matrix, int i) {
            c04.this.c[i] = i04.e(matrix);
        }

        @DexIgnore
        @Override // com.fossil.h04.a
        public void b(i04 i04, Matrix matrix, int i) {
            c04.this.d[i] = i04.e(matrix);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements g04.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ float f539a;

        @DexIgnore
        public b(c04 c04, float f) {
            this.f539a = f;
        }

        @DexIgnore
        @Override // com.fossil.g04.c
        public yz3 a(yz3 yz3) {
            return yz3 instanceof e04 ? yz3 : new xz3(this.f539a, yz3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public g04 f540a;
        @DexIgnore
        public ry3 b;
        @DexIgnore
        public ColorFilter c;
        @DexIgnore
        public ColorStateList d; // = null;
        @DexIgnore
        public ColorStateList e; // = null;
        @DexIgnore
        public ColorStateList f; // = null;
        @DexIgnore
        public ColorStateList g; // = null;
        @DexIgnore
        public PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
        @DexIgnore
        public Rect i; // = null;
        @DexIgnore
        public float j; // = 1.0f;
        @DexIgnore
        public float k; // = 1.0f;
        @DexIgnore
        public float l;
        @DexIgnore
        public int m; // = 255;
        @DexIgnore
        public float n; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float o; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float p; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public int q; // = 0;
        @DexIgnore
        public int r; // = 0;
        @DexIgnore
        public int s; // = 0;
        @DexIgnore
        public int t; // = 0;
        @DexIgnore
        public boolean u; // = false;
        @DexIgnore
        public Paint.Style v; // = Paint.Style.FILL_AND_STROKE;

        @DexIgnore
        public c(c cVar) {
            this.f540a = cVar.f540a;
            this.b = cVar.b;
            this.l = cVar.l;
            this.c = cVar.c;
            this.d = cVar.d;
            this.e = cVar.e;
            this.h = cVar.h;
            this.g = cVar.g;
            this.m = cVar.m;
            this.j = cVar.j;
            this.s = cVar.s;
            this.q = cVar.q;
            this.u = cVar.u;
            this.k = cVar.k;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
            this.r = cVar.r;
            this.t = cVar.t;
            this.f = cVar.f;
            this.v = cVar.v;
            if (cVar.i != null) {
                this.i = new Rect(cVar.i);
            }
        }

        @DexIgnore
        public c(g04 g04, ry3 ry3) {
            this.f540a = g04;
            this.b = ry3;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            c04 c04 = new c04(this, null);
            c04.e = true;
            return c04;
        }
    }

    @DexIgnore
    public c04() {
        this(new g04());
    }

    @DexIgnore
    public c04(Context context, AttributeSet attributeSet, int i2, int i3) {
        this(g04.e(context, attributeSet, i2, i3).m());
    }

    @DexIgnore
    public c04(c cVar) {
        this.c = new i04.g[4];
        this.d = new i04.g[4];
        this.f = new Matrix();
        this.g = new Path();
        this.h = new Path();
        this.i = new RectF();
        this.j = new RectF();
        this.k = new Region();
        this.l = new Region();
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new uz3();
        this.w = new h04();
        this.A = new RectF();
        this.b = cVar;
        this.t.setStyle(Paint.Style.STROKE);
        this.s.setStyle(Paint.Style.FILL);
        B.setColor(-1);
        B.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        i0();
        h0(getState());
        this.v = new a();
    }

    @DexIgnore
    public /* synthetic */ c04(c cVar, a aVar) {
        this(cVar);
    }

    @DexIgnore
    public c04(g04 g04) {
        this(new c(g04, null));
    }

    @DexIgnore
    public static int Q(int i2, int i3) {
        return (((i3 >>> 7) + i3) * i2) >>> 8;
    }

    @DexIgnore
    public static c04 l(Context context, float f2) {
        int b2 = vx3.b(context, jw3.colorSurface, c04.class.getSimpleName());
        c04 c04 = new c04();
        c04.M(context);
        c04.V(ColorStateList.valueOf(b2));
        c04.U(f2);
        return c04;
    }

    @DexIgnore
    public int A() {
        c cVar = this.b;
        return (int) (Math.cos(Math.toRadians((double) cVar.t)) * ((double) cVar.s));
    }

    @DexIgnore
    public int B() {
        return this.b.r;
    }

    @DexIgnore
    public g04 C() {
        return this.b.f540a;
    }

    @DexIgnore
    public final float D() {
        return L() ? this.t.getStrokeWidth() / 2.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public ColorStateList E() {
        return this.b.g;
    }

    @DexIgnore
    public float F() {
        return this.b.f540a.r().a(t());
    }

    @DexIgnore
    public float G() {
        return this.b.f540a.t().a(t());
    }

    @DexIgnore
    public float H() {
        return this.b.p;
    }

    @DexIgnore
    public float I() {
        return v() + H();
    }

    @DexIgnore
    public final boolean J() {
        c cVar = this.b;
        int i2 = cVar.q;
        return i2 != 1 && cVar.r > 0 && (i2 == 2 || S());
    }

    @DexIgnore
    public final boolean K() {
        Paint.Style style = this.b.v;
        return style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.FILL;
    }

    @DexIgnore
    public final boolean L() {
        Paint.Style style = this.b.v;
        return (style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.STROKE) && this.t.getStrokeWidth() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void M(Context context) {
        this.b.b = new ry3(context);
        j0();
    }

    @DexIgnore
    public final void N() {
        super.invalidateSelf();
    }

    @DexIgnore
    public boolean O() {
        ry3 ry3 = this.b.b;
        return ry3 != null && ry3.d();
    }

    @DexIgnore
    public boolean P() {
        return this.b.f540a.u(t());
    }

    @DexIgnore
    public final void R(Canvas canvas) {
        int z2 = z();
        int A2 = A();
        if (Build.VERSION.SDK_INT < 21) {
            Rect clipBounds = canvas.getClipBounds();
            int i2 = this.b.r;
            clipBounds.inset(-i2, -i2);
            clipBounds.offset(z2, A2);
            canvas.clipRect(clipBounds, Region.Op.REPLACE);
        }
        canvas.translate((float) z2, (float) A2);
    }

    @DexIgnore
    public final boolean S() {
        return Build.VERSION.SDK_INT < 21 || (!P() && !this.g.isConvex());
    }

    @DexIgnore
    public void T(float f2) {
        setShapeAppearanceModel(this.b.f540a.w(f2));
    }

    @DexIgnore
    public void U(float f2) {
        c cVar = this.b;
        if (cVar.o != f2) {
            cVar.o = f2;
            j0();
        }
    }

    @DexIgnore
    public void V(ColorStateList colorStateList) {
        c cVar = this.b;
        if (cVar.d != colorStateList) {
            cVar.d = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void W(float f2) {
        c cVar = this.b;
        if (cVar.k != f2) {
            cVar.k = f2;
            this.e = true;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void X(int i2, int i3, int i4, int i5) {
        c cVar = this.b;
        if (cVar.i == null) {
            cVar.i = new Rect();
        }
        this.b.i.set(i2, i3, i4, i5);
        this.z = this.b.i;
        invalidateSelf();
    }

    @DexIgnore
    public void Y(Paint.Style style) {
        this.b.v = style;
        N();
    }

    @DexIgnore
    public void Z(float f2) {
        c cVar = this.b;
        if (cVar.n != f2) {
            cVar.n = f2;
            j0();
        }
    }

    @DexIgnore
    public void a0(int i2) {
        this.u.d(i2);
        this.b.u = false;
        N();
    }

    @DexIgnore
    public void b0(int i2) {
        c cVar = this.b;
        if (cVar.t != i2) {
            cVar.t = i2;
            N();
        }
    }

    @DexIgnore
    public void c0(int i2) {
        c cVar = this.b;
        if (cVar.q != i2) {
            cVar.q = i2;
            N();
        }
    }

    @DexIgnore
    public void d0(float f2, int i2) {
        g0(f2);
        f0(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.s.setColorFilter(this.x);
        int alpha = this.s.getAlpha();
        this.s.setAlpha(Q(alpha, this.b.m));
        this.t.setColorFilter(this.y);
        this.t.setStrokeWidth(this.b.l);
        int alpha2 = this.t.getAlpha();
        this.t.setAlpha(Q(alpha2, this.b.m));
        if (this.e) {
            h();
            f(t(), this.g);
            this.e = false;
        }
        if (J()) {
            canvas.save();
            R(canvas);
            int width = (int) (this.A.width() - ((float) getBounds().width()));
            int height = (int) (this.A.height() - ((float) getBounds().height()));
            Bitmap createBitmap = Bitmap.createBitmap(((int) this.A.width()) + (this.b.r * 2) + width, ((int) this.A.height()) + (this.b.r * 2) + height, Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap);
            float f2 = (float) ((getBounds().left - this.b.r) - width);
            float f3 = (float) ((getBounds().top - this.b.r) - height);
            canvas2.translate(-f2, -f3);
            m(canvas2);
            canvas.drawBitmap(createBitmap, f2, f3, (Paint) null);
            createBitmap.recycle();
            canvas.restore();
        }
        if (K()) {
            n(canvas);
        }
        if (L()) {
            q(canvas);
        }
        this.s.setAlpha(alpha);
        this.t.setAlpha(alpha2);
    }

    @DexIgnore
    public final PorterDuffColorFilter e(Paint paint, boolean z2) {
        int color;
        int k2;
        if (!z2 || (k2 = k((color = paint.getColor()))) == color) {
            return null;
        }
        return new PorterDuffColorFilter(k2, PorterDuff.Mode.SRC_IN);
    }

    @DexIgnore
    public void e0(float f2, ColorStateList colorStateList) {
        g0(f2);
        f0(colorStateList);
    }

    @DexIgnore
    public final void f(RectF rectF, Path path) {
        g(rectF, path);
        if (this.b.j != 1.0f) {
            this.f.reset();
            Matrix matrix = this.f;
            float f2 = this.b.j;
            matrix.setScale(f2, f2, rectF.width() / 2.0f, rectF.height() / 2.0f);
            path.transform(this.f);
        }
        path.computeBounds(this.A, true);
    }

    @DexIgnore
    public void f0(ColorStateList colorStateList) {
        c cVar = this.b;
        if (cVar.e != colorStateList) {
            cVar.e = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void g(RectF rectF, Path path) {
        h04 h04 = this.w;
        c cVar = this.b;
        h04.e(cVar.f540a, cVar.k, rectF, this.v, path);
    }

    @DexIgnore
    public void g0(float f2) {
        this.b.l = f2;
        invalidateSelf();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.b.q != 2) {
            if (P()) {
                outline.setRoundRect(getBounds(), F());
                return;
            }
            f(t(), this.g);
            if (this.g.isConvex()) {
                outline.setConvexPath(this.g);
            }
        }
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        Rect rect2 = this.z;
        if (rect2 == null) {
            return super.getPadding(rect);
        }
        rect.set(rect2);
        return true;
    }

    @DexIgnore
    public Region getTransparentRegion() {
        this.k.set(getBounds());
        f(t(), this.g);
        this.l.setPath(this.g, this.k);
        this.k.op(this.l, Region.Op.DIFFERENCE);
        return this.k;
    }

    @DexIgnore
    public final void h() {
        g04 x2 = C().x(new b(this, -D()));
        this.m = x2;
        this.w.d(x2, this.b.k, u(), this.h);
    }

    @DexIgnore
    public final boolean h0(int[] iArr) {
        boolean z2;
        int color;
        int colorForState;
        int color2;
        int colorForState2;
        if (this.b.d == null || color2 == (colorForState2 = this.b.d.getColorForState(iArr, (color2 = this.s.getColor())))) {
            z2 = false;
        } else {
            this.s.setColor(colorForState2);
            z2 = true;
        }
        if (this.b.e == null || color == (colorForState = this.b.e.getColorForState(iArr, (color = this.t.getColor())))) {
            return z2;
        }
        this.t.setColor(colorForState);
        return true;
    }

    @DexIgnore
    public final PorterDuffColorFilter i(ColorStateList colorStateList, PorterDuff.Mode mode, boolean z2) {
        int colorForState = colorStateList.getColorForState(getState(), 0);
        if (z2) {
            colorForState = k(colorForState);
        }
        return new PorterDuffColorFilter(colorForState, mode);
    }

    @DexIgnore
    public final boolean i0() {
        PorterDuffColorFilter porterDuffColorFilter = this.x;
        PorterDuffColorFilter porterDuffColorFilter2 = this.y;
        c cVar = this.b;
        this.x = j(cVar.g, cVar.h, this.s, true);
        c cVar2 = this.b;
        this.y = j(cVar2.f, cVar2.h, this.t, false);
        c cVar3 = this.b;
        if (cVar3.u) {
            this.u.d(cVar3.g.getColorForState(getState(), 0));
        }
        return !kn0.a(porterDuffColorFilter, this.x) || !kn0.a(porterDuffColorFilter2, this.y);
    }

    @DexIgnore
    public void invalidateSelf() {
        this.e = true;
        super.invalidateSelf();
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3;
        ColorStateList colorStateList4;
        return super.isStateful() || ((colorStateList = this.b.g) != null && colorStateList.isStateful()) || (((colorStateList2 = this.b.f) != null && colorStateList2.isStateful()) || (((colorStateList3 = this.b.e) != null && colorStateList3.isStateful()) || ((colorStateList4 = this.b.d) != null && colorStateList4.isStateful())));
    }

    @DexIgnore
    public final PorterDuffColorFilter j(ColorStateList colorStateList, PorterDuff.Mode mode, Paint paint, boolean z2) {
        return (colorStateList == null || mode == null) ? e(paint, z2) : i(colorStateList, mode, z2);
    }

    @DexIgnore
    public final void j0() {
        float I = I();
        this.b.r = (int) Math.ceil((double) (0.75f * I));
        this.b.s = (int) Math.ceil((double) (I * 0.25f));
        i0();
        N();
    }

    @DexIgnore
    public final int k(int i2) {
        float I = I();
        float y2 = y();
        ry3 ry3 = this.b.b;
        return ry3 != null ? ry3.c(i2, I + y2) : i2;
    }

    @DexIgnore
    public final void m(Canvas canvas) {
        if (this.b.s != 0) {
            canvas.drawPath(this.g, this.u.c());
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.c[i2].b(this.u, this.b.r, canvas);
            this.d[i2].b(this.u, this.b.r, canvas);
        }
        int z2 = z();
        int A2 = A();
        canvas.translate((float) (-z2), (float) (-A2));
        canvas.drawPath(this.g, B);
        canvas.translate((float) z2, (float) A2);
    }

    @DexIgnore
    public Drawable mutate() {
        this.b = new c(this.b);
        return this;
    }

    @DexIgnore
    public final void n(Canvas canvas) {
        p(canvas, this.s, this.g, this.b.f540a, t());
    }

    @DexIgnore
    public void o(Canvas canvas, Paint paint, Path path, RectF rectF) {
        p(canvas, paint, path, this.b.f540a, rectF);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.e = true;
        super.onBoundsChange(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean z2 = h0(iArr) || i0();
        if (z2) {
            invalidateSelf();
        }
        return z2;
    }

    @DexIgnore
    public final void p(Canvas canvas, Paint paint, Path path, g04 g04, RectF rectF) {
        if (g04.u(rectF)) {
            float a2 = g04.t().a(rectF);
            canvas.drawRoundRect(rectF, a2, a2, paint);
            return;
        }
        canvas.drawPath(path, paint);
    }

    @DexIgnore
    public final void q(Canvas canvas) {
        p(canvas, this.t, this.h, this.m, u());
    }

    @DexIgnore
    public float r() {
        return this.b.f540a.j().a(t());
    }

    @DexIgnore
    public float s() {
        return this.b.f540a.l().a(t());
    }

    @DexIgnore
    public void setAlpha(int i2) {
        c cVar = this.b;
        if (cVar.m != i2) {
            cVar.m = i2;
            N();
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.c = colorFilter;
        N();
    }

    @DexIgnore
    @Override // com.fossil.j04
    public void setShapeAppearanceModel(g04 g04) {
        this.b.f540a = g04;
        invalidateSelf();
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintList(ColorStateList colorStateList) {
        this.b.g = colorStateList;
        i0();
        N();
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.b;
        if (cVar.h != mode) {
            cVar.h = mode;
            i0();
            N();
        }
    }

    @DexIgnore
    public RectF t() {
        Rect bounds = getBounds();
        this.i.set((float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        return this.i;
    }

    @DexIgnore
    public final RectF u() {
        RectF t2 = t();
        float D = D();
        this.j.set(t2.left + D, t2.top + D, t2.right - D, t2.bottom - D);
        return this.j;
    }

    @DexIgnore
    public float v() {
        return this.b.o;
    }

    @DexIgnore
    public ColorStateList w() {
        return this.b.d;
    }

    @DexIgnore
    public float x() {
        return this.b.k;
    }

    @DexIgnore
    public float y() {
        return this.b.n;
    }

    @DexIgnore
    public int z() {
        c cVar = this.b;
        return (int) (Math.sin(Math.toRadians((double) cVar.t)) * ((double) cVar.s));
    }
}
