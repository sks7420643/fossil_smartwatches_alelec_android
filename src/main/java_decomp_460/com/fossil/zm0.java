package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Handler;
import androidx.collection.SimpleArrayMap;
import com.fossil.an0;
import com.fossil.nl0;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zm0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ej0<String, Typeface> f4492a; // = new ej0<>(16);
    @DexIgnore
    public static /* final */ an0 b; // = new an0("fonts", 10, 10000);
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static /* final */ SimpleArrayMap<String, ArrayList<an0.d<g>>> d; // = new SimpleArrayMap<>();
    @DexIgnore
    public static /* final */ Comparator<byte[]> e; // = new d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<g> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Context f4493a;
        @DexIgnore
        public /* final */ /* synthetic */ ym0 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;

        @DexIgnore
        public a(Context context, ym0 ym0, int i, String str) {
            this.f4493a = context;
            this.b = ym0;
            this.c = i;
            this.d = str;
        }

        @DexIgnore
        /* renamed from: a */
        public g call() throws Exception {
            g f = zm0.f(this.f4493a, this.b, this.c);
            Typeface typeface = f.f4498a;
            if (typeface != null) {
                zm0.f4492a.f(this.d, typeface);
            }
            return f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements an0.d<g> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nl0.a f4494a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;

        @DexIgnore
        public b(nl0.a aVar, Handler handler) {
            this.f4494a = aVar;
            this.b = handler;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(g gVar) {
            if (gVar == null) {
                this.f4494a.a(1, this.b);
                return;
            }
            int i = gVar.b;
            if (i == 0) {
                this.f4494a.b(gVar.f4498a, this.b);
            } else {
                this.f4494a.a(i, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements an0.d<g> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f4495a;

        @DexIgnore
        public c(String str) {
            this.f4495a = str;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
            if (r1 >= r0.size()) goto L_0x0010;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
            r0.get(r1).a(r5);
            r1 = r1 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
            r1 = 0;
         */
        @DexIgnore
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.fossil.zm0.g r5) {
            /*
                r4 = this;
                java.lang.Object r1 = com.fossil.zm0.c
                monitor-enter(r1)
                androidx.collection.SimpleArrayMap<java.lang.String, java.util.ArrayList<com.fossil.an0$d<com.fossil.zm0$g>>> r0 = com.fossil.zm0.d     // Catch:{ all -> 0x002e }
                java.lang.String r2 = r4.f4495a     // Catch:{ all -> 0x002e }
                java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x002e }
                java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x002e }
                if (r0 != 0) goto L_0x0011
                monitor-exit(r1)     // Catch:{ all -> 0x002e }
            L_0x0010:
                return
            L_0x0011:
                androidx.collection.SimpleArrayMap<java.lang.String, java.util.ArrayList<com.fossil.an0$d<com.fossil.zm0$g>>> r2 = com.fossil.zm0.d     // Catch:{ all -> 0x002e }
                java.lang.String r3 = r4.f4495a     // Catch:{ all -> 0x002e }
                r2.remove(r3)     // Catch:{ all -> 0x002e }
                monitor-exit(r1)     // Catch:{ all -> 0x002e }
                r1 = 0
                r2 = r1
            L_0x001b:
                int r1 = r0.size()
                if (r2 >= r1) goto L_0x0010
                java.lang.Object r1 = r0.get(r2)
                com.fossil.an0$d r1 = (com.fossil.an0.d) r1
                r1.a(r5)
                int r1 = r2 + 1
                r2 = r1
                goto L_0x001b
            L_0x002e:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.zm0.c.a(com.fossil.zm0$g):void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Comparator<byte[]> {
        @DexIgnore
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            int i;
            int i2;
            if (bArr.length != bArr2.length) {
                int length = bArr.length;
                i2 = bArr2.length;
                i = length;
            } else {
                for (int i3 = 0; i3 < bArr.length; i3++) {
                    if (bArr[i3] != bArr2[i3]) {
                        byte b = bArr[i3];
                        i2 = bArr2[i3];
                        i = b;
                    }
                }
                return 0;
            }
            return (i == 1 ? 1 : 0) - (i2 == 1 ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4496a;
        @DexIgnore
        public /* final */ f[] b;

        @DexIgnore
        public e(int i, f[] fVarArr) {
            this.f4496a = i;
            this.b = fVarArr;
        }

        @DexIgnore
        public f[] a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.f4496a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Uri f4497a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore
        public f(Uri uri, int i, int i2, boolean z, int i3) {
            pn0.d(uri);
            this.f4497a = uri;
            this.b = i;
            this.c = i2;
            this.d = z;
            this.e = i3;
        }

        @DexIgnore
        public int a() {
            return this.e;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }

        @DexIgnore
        public Uri c() {
            return this.f4497a;
        }

        @DexIgnore
        public int d() {
            return this.c;
        }

        @DexIgnore
        public boolean e() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Typeface f4498a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public g(Typeface typeface, int i) {
            this.f4498a = typeface;
            this.b = i;
        }
    }

    @DexIgnore
    public static List<byte[]> a(Signature[] signatureArr) {
        ArrayList arrayList = new ArrayList();
        for (Signature signature : signatureArr) {
            arrayList.add(signature.toByteArray());
        }
        return arrayList;
    }

    @DexIgnore
    public static boolean b(List<byte[]> list, List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!Arrays.equals(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static e c(Context context, CancellationSignal cancellationSignal, ym0 ym0) throws PackageManager.NameNotFoundException {
        ProviderInfo h = h(context.getPackageManager(), ym0, context.getResources());
        return h == null ? new e(1, null) : new e(0, e(context, ym0, h.authority, cancellationSignal));
    }

    @DexIgnore
    public static List<List<byte[]>> d(ym0 ym0, Resources resources) {
        return ym0.a() != null ? ym0.a() : kl0.c(resources, ym0.b());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0146  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.zm0.f[] e(android.content.Context r18, com.fossil.ym0 r19, java.lang.String r20, android.os.CancellationSignal r21) {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zm0.e(android.content.Context, com.fossil.ym0, java.lang.String, android.os.CancellationSignal):com.fossil.zm0$f[]");
    }

    @DexIgnore
    public static g f(Context context, ym0 ym0, int i) {
        try {
            e c2 = c(context, null, ym0);
            int i2 = -3;
            if (c2.b() == 0) {
                Typeface b2 = sl0.b(context, null, c2.a(), i);
                if (b2 != null) {
                    i2 = 0;
                }
                return new g(b2, i2);
            }
            if (c2.b() == 1) {
                i2 = -2;
            }
            return new g(null, i2);
        } catch (PackageManager.NameNotFoundException e2) {
            return new g(null, -1);
        }
    }

    @DexIgnore
    public static Typeface g(Context context, ym0 ym0, nl0.a aVar, Handler handler, boolean z, int i, int i2) {
        String str = ym0.c() + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
        Typeface d2 = f4492a.d(str);
        if (d2 != null) {
            if (aVar != null) {
                aVar.d(d2);
            }
            return d2;
        } else if (!z || i != -1) {
            a aVar2 = new a(context, ym0, i2, str);
            if (z) {
                try {
                    return ((g) b.e(aVar2, i)).f4498a;
                } catch (InterruptedException e2) {
                    return null;
                }
            } else {
                b bVar = aVar == null ? null : new b(aVar, handler);
                synchronized (c) {
                    ArrayList<an0.d<g>> arrayList = d.get(str);
                    if (arrayList != null) {
                        if (bVar != null) {
                            arrayList.add(bVar);
                        }
                        return null;
                    }
                    if (bVar != null) {
                        ArrayList<an0.d<g>> arrayList2 = new ArrayList<>();
                        arrayList2.add(bVar);
                        d.put(str, arrayList2);
                    }
                    b.d(aVar2, new c(str));
                    return null;
                }
            }
        } else {
            g f2 = f(context, ym0, i2);
            if (aVar != null) {
                int i3 = f2.b;
                if (i3 == 0) {
                    aVar.b(f2.f4498a, handler);
                } else {
                    aVar.a(i3, handler);
                }
            }
            return f2.f4498a;
        }
    }

    @DexIgnore
    public static ProviderInfo h(PackageManager packageManager, ym0 ym0, Resources resources) throws PackageManager.NameNotFoundException {
        String d2 = ym0.d();
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(d2, 0);
        if (resolveContentProvider == null) {
            throw new PackageManager.NameNotFoundException("No package found for authority: " + d2);
        } else if (resolveContentProvider.packageName.equals(ym0.e())) {
            List<byte[]> a2 = a(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort(a2, e);
            List<List<byte[]>> d3 = d(ym0, resources);
            for (int i = 0; i < d3.size(); i++) {
                ArrayList arrayList = new ArrayList(d3.get(i));
                Collections.sort(arrayList, e);
                if (b(a2, arrayList)) {
                    return resolveContentProvider;
                }
            }
            return null;
        } else {
            throw new PackageManager.NameNotFoundException("Found content provider " + d2 + ", but package was not " + ym0.e());
        }
    }

    @DexIgnore
    public static Map<Uri, ByteBuffer> i(Context context, f[] fVarArr, CancellationSignal cancellationSignal) {
        HashMap hashMap = new HashMap();
        for (f fVar : fVarArr) {
            if (fVar.a() == 0) {
                Uri c2 = fVar.c();
                if (!hashMap.containsKey(c2)) {
                    hashMap.put(c2, zl0.f(context, cancellationSignal, c2));
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }
}
