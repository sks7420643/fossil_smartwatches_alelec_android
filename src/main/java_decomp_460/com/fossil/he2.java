package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.gc2;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he2 extends gc2 implements Handler.Callback {
    @DexIgnore
    public /* final */ HashMap<gc2.a, ge2> d; // = new HashMap<>();
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Handler f;
    @DexIgnore
    public /* final */ ve2 g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ long i;

    @DexIgnore
    public he2(Context context) {
        this.e = context.getApplicationContext();
        this.f = new xl2(context.getMainLooper(), this);
        this.g = ve2.b();
        this.h = 5000;
        this.i = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
    }

    @DexIgnore
    @Override // com.fossil.gc2
    public final boolean d(gc2.a aVar, ServiceConnection serviceConnection, String str) {
        boolean d2;
        rc2.l(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            ge2 ge2 = this.d.get(aVar);
            if (ge2 == null) {
                ge2 = new ge2(this, aVar);
                ge2.e(serviceConnection, serviceConnection, str);
                ge2.h(str);
                this.d.put(aVar, ge2);
            } else {
                this.f.removeMessages(0, aVar);
                if (!ge2.g(serviceConnection)) {
                    ge2.e(serviceConnection, serviceConnection, str);
                    int c = ge2.c();
                    if (c == 1) {
                        serviceConnection.onServiceConnected(ge2.b(), ge2.a());
                    } else if (c == 2) {
                        ge2.h(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = ge2.d();
        }
        return d2;
    }

    @DexIgnore
    @Override // com.fossil.gc2
    public final void e(gc2.a aVar, ServiceConnection serviceConnection, String str) {
        rc2.l(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            ge2 ge2 = this.d.get(aVar);
            if (ge2 == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (ge2.g(serviceConnection)) {
                ge2.f(serviceConnection, str);
                if (ge2.j()) {
                    this.f.sendMessageDelayed(this.f.obtainMessage(0, aVar), this.h);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 0) {
            synchronized (this.d) {
                gc2.a aVar = (gc2.a) message.obj;
                ge2 ge2 = this.d.get(aVar);
                if (ge2 != null && ge2.j()) {
                    if (ge2.d()) {
                        ge2.i("GmsClientSupervisor");
                    }
                    this.d.remove(aVar);
                }
            }
            return true;
        } else if (i2 != 1) {
            return false;
        } else {
            synchronized (this.d) {
                gc2.a aVar2 = (gc2.a) message.obj;
                ge2 ge22 = this.d.get(aVar2);
                if (ge22 != null && ge22.c() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = ge22.b();
                    if (b == null) {
                        b = aVar2.a();
                    }
                    ge22.onServiceDisconnected(b == null ? new ComponentName(aVar2.b(), "unknown") : b);
                }
            }
            return true;
        }
    }
}
