package com.fossil;

import android.os.Handler;
import android.os.Looper;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya8 {
    @DexIgnore
    public static /* final */ Handler b; // = new Handler(Looper.getMainLooper());

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MethodChannel.Result f4289a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result b;

        @DexIgnore
        public a(MethodChannel.Result result) {
            this.b = result;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.b;
            if (result != null) {
                result.notImplemented();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public b(MethodChannel.Result result, Object obj) {
            this.b = result;
            this.c = obj;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.b;
            if (result != null) {
                result.success(this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        public c(MethodChannel.Result result, String str, String str2, Object obj) {
            this.b = result;
            this.c = str;
            this.d = str2;
            this.e = obj;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.b;
            if (result != null) {
                result.error(this.c, this.d, this.e);
            }
        }
    }

    @DexIgnore
    public ya8(MethodChannel.Result result) {
        this.f4289a = result;
    }

    @DexIgnore
    public static /* synthetic */ void e(ya8 ya8, String str, String str2, Object obj, int i, Object obj2) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            obj = null;
        }
        ya8.d(str, str2, obj);
    }

    @DexIgnore
    public final MethodChannel.Result a() {
        return this.f4289a;
    }

    @DexIgnore
    public final void b() {
        MethodChannel.Result result = this.f4289a;
        this.f4289a = null;
        b.post(new a(result));
    }

    @DexIgnore
    public final void c(Object obj) {
        MethodChannel.Result result = this.f4289a;
        this.f4289a = null;
        b.post(new b(result, obj));
    }

    @DexIgnore
    public final void d(String str, String str2, Object obj) {
        pq7.c(str, "code");
        MethodChannel.Result result = this.f4289a;
        this.f4289a = null;
        b.post(new c(result, str, str2, obj));
    }
}
