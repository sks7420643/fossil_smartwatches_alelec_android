package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class t22 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ t22 f3351a; // = new t22();

    @DexIgnore
    public static j32.b a() {
        return f3351a;
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.P((SQLiteDatabase) obj);
    }
}
