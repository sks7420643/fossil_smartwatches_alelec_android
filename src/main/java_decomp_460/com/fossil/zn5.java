package com.fossil;

import android.net.ConnectivityManager;
import android.net.Network;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zn5 extends ConnectivityManager.NetworkCallback {
    @DexIgnore
    public static /* final */ String b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public th5 f4502a; // = th5.UNKNOWN;

    /*
    static {
        String simpleName = zn5.class.getSimpleName();
        pq7.b(simpleName, "NetworkStateChangeCallback::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public abstract void a(th5 th5);

    @DexIgnore
    public final void onAvailable(Network network) {
        pq7.c(network, "network");
        super.onAvailable(network);
        th5 th5 = this.f4502a == th5.DISCONNECTED ? th5.RECONNECTED : th5.CONNECTED;
        this.f4502a = th5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onAvailable new state " + this.f4502a);
        a(th5);
    }

    @DexIgnore
    public final void onLost(Network network) {
        pq7.c(network, "network");
        super.onLost(network);
        this.f4502a = th5.DISCONNECTED;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onLost new state " + this.f4502a);
        a(th5.DISCONNECTED);
    }

    @DexIgnore
    public final void onUnavailable() {
        super.onUnavailable();
        this.f4502a = th5.DISCONNECTED;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onUnavailable new state " + this.f4502a);
        a(th5.DISCONNECTED);
    }
}
