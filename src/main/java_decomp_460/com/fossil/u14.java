package com.fossil;

import com.fossil.b44;
import com.fossil.x34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u14<K, V> implements y34<K, V> {
    @DexIgnore
    public transient Collection<Map.Entry<K, V>> b;
    @DexIgnore
    public transient Set<K> c;
    @DexIgnore
    public transient c44<K> d;
    @DexIgnore
    public transient Collection<V> e;
    @DexIgnore
    public transient Map<K, Collection<V>> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends b44.c<K, V> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.b44.c
        public y34<K, V> a() {
            return u14.this;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return u14.this.entryIterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends u14<K, V>.b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public c(u14 u14) {
            super();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return x44.a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            return x44.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AbstractCollection<V> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void clear() {
            u14.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return u14.this.containsValue(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return u14.this.valueIterator();
        }

        @DexIgnore
        public int size() {
            return u14.this.size();
        }
    }

    @DexIgnore
    @Override // com.fossil.y34
    public Map<K, Collection<V>> asMap() {
        Map<K, Collection<V>> map = this.f;
        if (map != null) {
            return map;
        }
        Map<K, Collection<V>> createAsMap = createAsMap();
        this.f = createAsMap;
        return createAsMap;
    }

    @DexIgnore
    @Override // com.fossil.y34
    public boolean containsEntry(Object obj, Object obj2) {
        Collection<V> collection = asMap().get(obj);
        return collection != null && collection.contains(obj2);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        for (Collection<V> collection : asMap().values()) {
            if (collection.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Map<K, Collection<V>> createAsMap();

    @DexIgnore
    public Collection<Map.Entry<K, V>> createEntries() {
        return this instanceof w44 ? new c() : new b();
    }

    @DexIgnore
    public Set<K> createKeySet() {
        return new x34.e(asMap());
    }

    @DexIgnore
    public c44<K> createKeys() {
        return new b44.d(this);
    }

    @DexIgnore
    public Collection<V> createValues() {
        return new d();
    }

    @DexIgnore
    @Override // com.fossil.y34
    public Collection<Map.Entry<K, V>> entries() {
        Collection<Map.Entry<K, V>> collection = this.b;
        if (collection != null) {
            return collection;
        }
        Collection<Map.Entry<K, V>> createEntries = createEntries();
        this.b = createEntries;
        return createEntries;
    }

    @DexIgnore
    public abstract Iterator<Map.Entry<K, V>> entryIterator();

    @DexIgnore
    public boolean equals(Object obj) {
        return b44.a(this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return asMap().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.y34
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    @Override // com.fossil.y34
    public Set<K> keySet() {
        Set<K> set = this.c;
        if (set != null) {
            return set;
        }
        Set<K> createKeySet = createKeySet();
        this.c = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    public c44<K> keys() {
        c44<K> c44 = this.d;
        if (c44 != null) {
            return c44;
        }
        c44<K> createKeys = createKeys();
        this.d = createKeys;
        return createKeys;
    }

    @DexIgnore
    @Override // com.fossil.y34
    @CanIgnoreReturnValue
    public abstract boolean put(K k, V v);

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.u14<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public boolean putAll(y34<? extends K, ? extends V> y34) {
        boolean z = false;
        for (Map.Entry<? extends K, ? extends V> entry : y34.entries()) {
            z = put(entry.getKey(), entry.getValue()) | z;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.y34
    @CanIgnoreReturnValue
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        i14.l(iterable);
        if (iterable instanceof Collection) {
            Collection<? extends V> collection = (Collection) iterable;
            return !collection.isEmpty() && get(k).addAll(collection);
        }
        Iterator<? extends V> it = iterable.iterator();
        return it.hasNext() && p34.a(get(k), it);
    }

    @DexIgnore
    @Override // com.fossil.y34
    @CanIgnoreReturnValue
    public boolean remove(Object obj, Object obj2) {
        Collection<V> collection = asMap().get(obj);
        return collection != null && collection.remove(obj2);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public abstract Collection<V> replaceValues(K k, Iterable<? extends V> iterable);

    @DexIgnore
    public String toString() {
        return asMap().toString();
    }

    @DexIgnore
    public abstract Iterator<V> valueIterator();

    @DexIgnore
    public Collection<V> values() {
        Collection<V> collection = this.e;
        if (collection != null) {
            return collection;
        }
        Collection<V> createValues = createValues();
        this.e = createValues;
        return createValues;
    }
}
