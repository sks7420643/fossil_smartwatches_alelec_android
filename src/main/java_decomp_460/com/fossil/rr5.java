package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3147a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public rr5(String str, String str2, String str3, String str4, String str5) {
        pq7.c(str, "packageName");
        pq7.c(str2, "appName");
        pq7.c(str3, "title");
        pq7.c(str4, "artist");
        pq7.c(str5, "album");
        this.f3147a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    @DexIgnore
    public final String a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.f3147a;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof rr5) {
                rr5 rr5 = (rr5) obj;
                if (!pq7.a(this.f3147a, rr5.f3147a) || !pq7.a(this.b, rr5.b) || !pq7.a(this.c, rr5.c) || !pq7.a(this.d, rr5.d) || !pq7.a(this.e, rr5.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f3147a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "MusicMetadata[packageName=" + this.f3147a + ", appName=" + this.b + ", title=" + this.c + ", artist=" + this.d + ", album=" + this.e + ']';
    }
}
