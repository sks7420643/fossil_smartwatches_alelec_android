package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t38 extends w38 {
    @DexIgnore
    public /* final */ Method c;
    @DexIgnore
    public /* final */ Method d;

    @DexIgnore
    public t38(Method method, Method method2) {
        this.c = method;
        this.d = method2;
    }

    @DexIgnore
    public static t38 s() {
        try {
            return new t38(SSLParameters.class.getMethod("setApplicationProtocols", String[].class), SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]));
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void g(SSLSocket sSLSocket, String str, List<t18> list) {
        try {
            SSLParameters sSLParameters = sSLSocket.getSSLParameters();
            List<String> b = w38.b(list);
            this.c.invoke(sSLParameters, b.toArray(new String[b.size()]));
            sSLSocket.setSSLParameters(sSLParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw b28.b("unable to set ssl parameters", e);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public String m(SSLSocket sSLSocket) {
        try {
            String str = (String) this.d.invoke(sSLSocket, new Object[0]);
            if (str == null || str.equals("")) {
                return null;
            }
            return str;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw b28.b("unable to get selected protocols", e);
        }
    }
}
