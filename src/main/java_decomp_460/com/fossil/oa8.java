package com.fossil;

import com.fossil.la8;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ oa8 f2663a; // = new oa8();

    @DexIgnore
    public final la8 a(Map<?, ?> map) {
        pq7.c(map, Constants.MAP);
        la8 la8 = new la8();
        Object obj = map.get("title");
        if (obj != null) {
            la8.d(((Boolean) obj).booleanValue());
            la8.b bVar = new la8.b();
            la8.e(bVar);
            Object obj2 = map.get("size");
            if (obj2 != null) {
                Map map2 = (Map) obj2;
                Object obj3 = map2.get("minWidth");
                if (obj3 != null) {
                    bVar.h(((Integer) obj3).intValue());
                    Object obj4 = map2.get("maxWidth");
                    if (obj4 != null) {
                        bVar.f(((Integer) obj4).intValue());
                        Object obj5 = map2.get("minHeight");
                        if (obj5 != null) {
                            bVar.g(((Integer) obj5).intValue());
                            Object obj6 = map2.get("maxHeight");
                            if (obj6 != null) {
                                bVar.e(((Integer) obj6).intValue());
                                la8.a aVar = new la8.a();
                                la8.c(aVar);
                                Object obj7 = map.get("duration");
                                if (obj7 != null) {
                                    Map map3 = (Map) obj7;
                                    Object obj8 = map3.get("min");
                                    if (obj8 != null) {
                                        aVar.d((long) ((Integer) obj8).intValue());
                                        Object obj9 = map3.get("max");
                                        if (obj9 != null) {
                                            aVar.c((long) ((Integer) obj9).intValue());
                                            return la8;
                                        }
                                        throw new il7("null cannot be cast to non-null type kotlin.Int");
                                    }
                                    throw new il7("null cannot be cast to non-null type kotlin.Int");
                                }
                                throw new il7("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                            }
                            throw new il7("null cannot be cast to non-null type kotlin.Int");
                        }
                        throw new il7("null cannot be cast to non-null type kotlin.Int");
                    }
                    throw new il7("null cannot be cast to non-null type kotlin.Int");
                }
                throw new il7("null cannot be cast to non-null type kotlin.Int");
            }
            throw new il7("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Boolean");
    }

    @DexIgnore
    public final Map<String, Object> b(List<ka8> list) {
        pq7.c(list, "list");
        ArrayList arrayList = new ArrayList();
        for (ka8 ka8 : list) {
            long j = (long) 1000;
            arrayList.add(zm7.j(hl7.a("id", ka8.e()), hl7.a("duration", Long.valueOf(ka8.c() / j)), hl7.a("type", Integer.valueOf(ka8.j())), hl7.a("createDt", Long.valueOf(ka8.a() / j)), hl7.a("width", Integer.valueOf(ka8.k())), hl7.a("height", Integer.valueOf(ka8.d())), hl7.a("modifiedDt", Long.valueOf(ka8.h())), hl7.a(Constants.LAT, ka8.f()), hl7.a("lng", ka8.g()), hl7.a("title", ka8.b())));
        }
        return ym7.c(hl7.a("data", arrayList));
    }

    @DexIgnore
    public final Map<String, Object> c(ka8 ka8) {
        pq7.c(ka8, "entity");
        return ym7.c(hl7.a("data", zm7.j(hl7.a("id", ka8.e()), hl7.a("duration", Long.valueOf(ka8.c())), hl7.a("type", Integer.valueOf(ka8.j())), hl7.a("createDt", Long.valueOf(ka8.a() / ((long) 1000))), hl7.a("width", Integer.valueOf(ka8.k())), hl7.a("height", Integer.valueOf(ka8.d())), hl7.a("modifiedDt", Long.valueOf(ka8.h())), hl7.a(Constants.LAT, ka8.f()), hl7.a("lng", ka8.g()), hl7.a("title", ka8.b()))));
    }

    @DexIgnore
    public final Map<String, Object> d(List<ma8> list) {
        pq7.c(list, "list");
        ArrayList arrayList = new ArrayList();
        for (ma8 ma8 : list) {
            Map j = zm7.j(hl7.a("id", ma8.a()), hl7.a("name", ma8.c()), hl7.a("length", Integer.valueOf(ma8.b())), hl7.a("isAll", Boolean.valueOf(ma8.d())));
            if (ma8.b() > 0) {
                arrayList.add(j);
            }
        }
        return ym7.c(hl7.a("data", arrayList));
    }
}
