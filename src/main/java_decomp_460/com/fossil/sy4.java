package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3332a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public sy4(String str, String str2) {
        this.f3332a = str;
        this.b = str2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof sy4) {
                sy4 sy4 = (sy4) obj;
                if (!pq7.a(this.f3332a, sy4.f3332a) || !pq7.a(this.b, sy4.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f3332a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "CachedRequest(url=" + this.f3332a + ", name=" + this.b + ")";
    }
}
