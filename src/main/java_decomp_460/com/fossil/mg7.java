package com.fossil;

import android.content.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg7 extends ng7 {
    @DexIgnore
    public String m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Thread o; // = null;

    @DexIgnore
    public mg7(Context context, int i, int i2, Throwable th, jg7 jg7) {
        super(context, i, jg7);
        i(i2, th);
    }

    @DexIgnore
    public mg7(Context context, int i, int i2, Throwable th, Thread thread, jg7 jg7) {
        super(context, i, jg7);
        i(i2, th);
        this.o = thread;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public og7 a() {
        return og7.c;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public boolean b(JSONObject jSONObject) {
        ji7.d(jSONObject, "er", this.m);
        jSONObject.put("ea", this.n);
        int i = this.n;
        if (i != 2 && i != 3) {
            return true;
        }
        new vh7(this.j).b(jSONObject, this.o);
        return true;
    }

    @DexIgnore
    public final void i(int i, Throwable th) {
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            this.m = stringWriter.toString();
            this.n = i;
            printWriter.close();
        }
    }
}
