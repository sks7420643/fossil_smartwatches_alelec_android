package com.fossil;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d67 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static String o; // = "CalendarAdapter";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Calendar f743a;
    @DexIgnore
    public Calendar b;
    @DexIgnore
    public Calendar c;
    @DexIgnore
    public Calendar d; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ Map<Long, Float> e; // = new TreeMap();
    @DexIgnore
    public /* final */ int[] f; // = new int[49];
    @DexIgnore
    public int g;
    @DexIgnore
    public /* final */ Context h;
    @DexIgnore
    public RecyclerViewCalendar.a i;
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public int l; // = 0;
    @DexIgnore
    public int m; // = 0;
    @DexIgnore
    public int n; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ ProgressBar c;

        @DexIgnore
        public a(View view) {
            super(view);
            this.b = (FlexibleTextView) view.findViewById(2131362342);
            ProgressBar progressBar = (ProgressBar) view.findViewById(2131362922);
            this.c = progressBar;
            progressBar.setMax(100);
            b(this.c, d67.this.n);
            if (Build.VERSION.SDK_INT > 21) {
                this.c.setBackgroundTintList(ColorStateList.valueOf(d67.this.n));
            } else {
                Drawable f = gl0.f(view.getContext(), 2131230861);
                f.setColorFilter(d67.this.n, PorterDuff.Mode.SRC_IN);
                this.c.setBackground(f);
            }
            view.setOnClickListener(this);
        }

        @DexIgnore
        public void a(int i) {
            if (i != -1) {
                Calendar calendar = (Calendar) d67.this.d.clone();
                int i2 = calendar.get(7) - 1;
                int i3 = d67.this.f[i % 49];
                if (i3 < i2 || i3 >= calendar.getActualMaximum(5) + i2) {
                    this.b.setVisibility(4);
                    this.c.setVisibility(4);
                    return;
                }
                calendar.add(5, i3 - i2);
                boolean m0 = lk5.m0(d67.this.c.getTime(), calendar.getTime());
                this.b.setVisibility(0);
                this.c.setVisibility(0);
                this.b.setText(String.valueOf(calendar.get(5)));
                Map<Long, Float> map = d67.this.e;
                lk5.T(calendar);
                Float f = map.get(Long.valueOf(calendar.getTimeInMillis()));
                FLogger.INSTANCE.getLocal().d(d67.o, "build - position=" + i + ", progress=" + f + ", date=" + calendar.getTime().toString());
                if (f == null) {
                    FlexibleTextView flexibleTextView = this.b;
                    d67 d67 = d67.this;
                    flexibleTextView.setTextColor(m0 ? d67.m : d67.l);
                    this.c.setVisibility(4);
                } else if (f.floatValue() < 1.0f) {
                    if (f.floatValue() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        FlexibleTextView flexibleTextView2 = this.b;
                        d67 d672 = d67.this;
                        flexibleTextView2.setTextColor(m0 ? d672.m : d672.k);
                        this.c.setVisibility(0);
                        this.c.setMax(100);
                        ObjectAnimator ofInt = ObjectAnimator.ofInt(this.c, "progress", 0, (int) (f.floatValue() * 100.0f));
                        ofInt.setDuration(300L);
                        ofInt.setAutoCancel(true);
                        ofInt.start();
                    } else if (f.floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        FlexibleTextView flexibleTextView3 = this.b;
                        d67 d673 = d67.this;
                        flexibleTextView3.setTextColor(m0 ? d673.m : d673.k);
                        this.c.setVisibility(0);
                        this.c.setMax(100);
                        ObjectAnimator ofInt2 = ObjectAnimator.ofInt(this.c, "progress", 0, 0);
                        ofInt2.setDuration(300L);
                        ofInt2.setAutoCancel(true);
                        ofInt2.start();
                    } else {
                        FlexibleTextView flexibleTextView4 = this.b;
                        d67 d674 = d67.this;
                        flexibleTextView4.setTextColor(m0 ? d674.m : d674.l);
                        this.c.setVisibility(4);
                    }
                    this.c.setSelected(false);
                } else {
                    this.c.setSelected(true);
                    this.c.setProgressTintList(ColorStateList.valueOf(0));
                    this.b.setTextColor(d67.this.j);
                }
                FlexibleTextView flexibleTextView5 = this.b;
                flexibleTextView5.setTypeface(flexibleTextView5.getTypeface(), m0 ? 1 : 0);
            }
        }

        @DexIgnore
        public final void b(ProgressBar progressBar, int i) {
            Drawable f = gl0.f(this.itemView.getContext(), 2131230860);
            try {
                if (f instanceof LayerDrawable) {
                    LayerDrawable layerDrawable = (LayerDrawable) f;
                    if (layerDrawable.getNumberOfLayers() > 1) {
                        ((GradientDrawable) layerDrawable.getDrawable(1)).setColor(i);
                        progressBar.setProgressDrawable(f);
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = d67.o;
                local.d(str, "DayViewHolder - e=" + e);
            }
        }

        @DexIgnore
        public void onClick(View view) {
            Calendar g;
            int adapterPosition = getAdapterPosition();
            d67 d67 = d67.this;
            if (d67.i != null && adapterPosition != -1 && (g = d67.g(adapterPosition)) != null && !g.before(d67.this.f743a) && !g.after(d67.this.b)) {
                d67.this.i.k0(adapterPosition, g);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f744a;

        @DexIgnore
        public b(View view) {
            super(view);
            this.f744a = (FlexibleTextView) view;
        }

        @DexIgnore
        public void a(int i) {
            String c;
            switch ((i / 7) % 7) {
                case 0:
                    c = um5.c(d67.this.h, 2131886771);
                    break;
                case 1:
                    c = um5.c(d67.this.h, 2131886768);
                    break;
                case 2:
                    c = um5.c(d67.this.h, 2131886773);
                    break;
                case 3:
                    c = um5.c(d67.this.h, 2131886774);
                    break;
                case 4:
                    c = um5.c(d67.this.h, 2131886772);
                    break;
                case 5:
                    c = um5.c(d67.this.h, 2131886769);
                    break;
                case 6:
                    c = um5.c(d67.this.h, 2131886770);
                    break;
                default:
                    c = "";
                    break;
            }
            this.f744a.setText(c);
        }
    }

    @DexIgnore
    public d67(Context context) {
        this.h = context;
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 7, 7);
        for (int i2 = 0; i2 < 7; i2++) {
            for (int i3 = 0; i3 < 7; i3++) {
                iArr[i2][i3] = (i2 * 7) + i3;
            }
        }
        int[][] iArr2 = (int[][]) Array.newInstance(Integer.TYPE, 7, 7);
        for (int i4 = 0; i4 < 7; i4++) {
            for (int i5 = 0; i5 < 7; i5++) {
                iArr2[i4][i5] = iArr[i5][6 - i4];
            }
        }
        for (int i6 = 0; i6 < 7; i6++) {
            System.arraycopy(iArr2[i6], 0, this.f, (i6 * 7) + 1, 6);
        }
        this.d = lk5.w(0, this.d);
    }

    @DexIgnore
    public Calendar g(int i2) {
        Calendar calendar = (Calendar) this.d.clone();
        int i3 = this.f[i2 % 49];
        int i4 = calendar.get(7) - 1;
        if (i3 < i4 || i3 >= calendar.getActualMaximum(5) + i4) {
            return null;
        }
        calendar.add(5, i3 - i4);
        FLogger.INSTANCE.getLocal().d(o, "getCalendarItem day=" + calendar.get(5) + " month=" + calendar.get(2));
        return calendar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        Calendar calendar;
        if (this.f743a == null || (calendar = this.b) == null) {
            return 0;
        }
        return (((((calendar.get(1) - this.f743a.get(1)) * 12) + this.b.get(2)) - this.f743a.get(2)) + 1) * 49;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        return i2 % 7 == 0 ? 0 : 1;
    }

    @DexIgnore
    public Calendar h() {
        return this.b;
    }

    @DexIgnore
    public Calendar i() {
        return this.f743a;
    }

    @DexIgnore
    public void j(int i2, int i3, int i4, int i5, int i6) {
        this.j = i2;
        this.k = i3;
        this.l = i4;
        this.m = i5;
        this.n = i6;
    }

    @DexIgnore
    public void k(Map<Long, Float> map, Calendar calendar) {
        FLogger.INSTANCE.getLocal().d(o, "setData");
        this.e.putAll(map);
        this.d = calendar;
    }

    @DexIgnore
    public void l(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void m(Calendar calendar) {
        this.b = calendar;
    }

    @DexIgnore
    public void n(RecyclerViewCalendar.a aVar) {
        this.i = aVar;
    }

    @DexIgnore
    public void o(Calendar calendar) {
        this.c = calendar;
        if (this.f743a == null) {
            this.f743a = calendar;
        }
        if (this.b == null) {
            this.b = this.c;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == 0) {
            ((b) viewHolder).a(i2);
        } else if (itemViewType == 1) {
            ((a) viewHolder).a(i2);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        if (i2 != 0) {
            View inflate = LayoutInflater.from(this.h).inflate(2131558662, viewGroup, false);
            inflate.getLayoutParams().width = this.g;
            return new a(inflate);
        }
        View inflate2 = LayoutInflater.from(this.h).inflate(2131558663, viewGroup, false);
        inflate2.getLayoutParams().width = this.g;
        return new b(inflate2);
    }

    @DexIgnore
    public void p(Calendar calendar) {
        this.f743a = calendar;
    }
}
