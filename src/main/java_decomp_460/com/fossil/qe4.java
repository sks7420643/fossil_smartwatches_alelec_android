package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import java.util.concurrent.ExecutorService;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qe4 implements le4 {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static kg4 d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2967a;
    @DexIgnore
    public /* final */ ExecutorService b;

    @DexIgnore
    public qe4(Context context, ExecutorService executorService) {
        this.f2967a = context;
        this.b = executorService;
    }

    @DexIgnore
    public static nt3<Integer> b(Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Binding to service");
        }
        return c(context, "com.google.firebase.MESSAGING_EVENT").c(intent).i(se4.a(), oe4.f2672a);
    }

    @DexIgnore
    public static kg4 c(Context context, String str) {
        kg4 kg4;
        synchronized (c) {
            if (d == null) {
                d = new kg4(context, str);
            }
            kg4 = d;
        }
        return kg4;
    }

    @DexIgnore
    public static final /* synthetic */ Integer d(nt3 nt3) throws Exception {
        return -1;
    }

    @DexIgnore
    public static final /* synthetic */ nt3 g(Context context, Intent intent, nt3 nt3) throws Exception {
        return (!mf2.j() || ((Integer) nt3.m()).intValue() != 402) ? nt3 : b(context, intent).i(se4.a(), pe4.f2822a);
    }

    @DexIgnore
    @Override // com.fossil.le4
    public nt3<Integer> a(Intent intent) {
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        return h(this.f2967a, intent);
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public nt3<Integer> h(Context context, Intent intent) {
        boolean z = true;
        boolean z2 = mf2.j() && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & SQLiteDatabase.CREATE_IF_NECESSARY) == 0) {
            z = false;
        }
        return (!z2 || z) ? qt3.c(this.b, new me4(context, intent)).k(this.b, new ne4(context, intent)) : b(context, intent);
    }
}
