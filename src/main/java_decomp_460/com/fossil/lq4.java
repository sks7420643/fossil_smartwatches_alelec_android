package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.common.enums.Action;
import com.zendesk.sdk.R;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lq4 extends xp0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ SparseIntArray f2232a;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray(201);
        f2232a = sparseIntArray;
        sparseIntArray.put(2131558434, 1);
        f2232a.put(2131558438, 2);
        f2232a.put(2131558442, 3);
        f2232a.put(2131558443, 4);
        f2232a.put(2131558487, 5);
        f2232a.put(2131558490, 6);
        f2232a.put(2131558491, 7);
        f2232a.put(2131558492, 8);
        f2232a.put(2131558493, 9);
        f2232a.put(2131558494, 10);
        f2232a.put(2131558495, 11);
        f2232a.put(2131558496, 12);
        f2232a.put(2131558497, 13);
        f2232a.put(2131558498, 14);
        f2232a.put(2131558499, 15);
        f2232a.put(2131558500, 16);
        f2232a.put(2131558501, 17);
        f2232a.put(2131558503, 18);
        f2232a.put(2131558504, 19);
        f2232a.put(2131558505, 20);
        f2232a.put(2131558506, 21);
        f2232a.put(2131558507, 22);
        f2232a.put(2131558508, 23);
        f2232a.put(2131558509, 24);
        f2232a.put(2131558510, 25);
        f2232a.put(2131558511, 26);
        f2232a.put(2131558512, 27);
        f2232a.put(2131558513, 28);
        f2232a.put(2131558514, 29);
        f2232a.put(2131558515, 30);
        f2232a.put(2131558516, 31);
        f2232a.put(2131558517, 32);
        f2232a.put(2131558518, 33);
        f2232a.put(2131558519, 34);
        f2232a.put(2131558521, 35);
        f2232a.put(2131558522, 36);
        f2232a.put(2131558523, 37);
        f2232a.put(2131558525, 38);
        f2232a.put(2131558526, 39);
        f2232a.put(2131558527, 40);
        f2232a.put(2131558528, 41);
        f2232a.put(2131558529, 42);
        f2232a.put(2131558530, 43);
        f2232a.put(2131558531, 44);
        f2232a.put(2131558532, 45);
        f2232a.put(2131558533, 46);
        f2232a.put(2131558534, 47);
        f2232a.put(2131558535, 48);
        f2232a.put(2131558536, 49);
        f2232a.put(2131558537, 50);
        f2232a.put(2131558538, 51);
        f2232a.put(2131558539, 52);
        f2232a.put(2131558540, 53);
        f2232a.put(2131558541, 54);
        f2232a.put(2131558542, 55);
        f2232a.put(2131558543, 56);
        f2232a.put(2131558544, 57);
        f2232a.put(2131558545, 58);
        f2232a.put(2131558546, 59);
        f2232a.put(2131558547, 60);
        f2232a.put(2131558548, 61);
        f2232a.put(2131558549, 62);
        f2232a.put(2131558550, 63);
        f2232a.put(2131558551, 64);
        f2232a.put(2131558552, 65);
        f2232a.put(2131558553, 66);
        f2232a.put(2131558554, 67);
        f2232a.put(2131558555, 68);
        f2232a.put(2131558556, 69);
        f2232a.put(2131558557, 70);
        f2232a.put(2131558558, 71);
        f2232a.put(2131558559, 72);
        f2232a.put(2131558560, 73);
        f2232a.put(2131558561, 74);
        f2232a.put(2131558562, 75);
        f2232a.put(2131558563, 76);
        f2232a.put(2131558564, 77);
        f2232a.put(2131558565, 78);
        f2232a.put(2131558566, 79);
        f2232a.put(2131558567, 80);
        f2232a.put(R.layout.fragment_help, 81);
        f2232a.put(2131558569, 82);
        f2232a.put(2131558570, 83);
        f2232a.put(2131558571, 84);
        f2232a.put(2131558572, 85);
        f2232a.put(2131558573, 86);
        f2232a.put(2131558574, 87);
        f2232a.put(2131558575, 88);
        f2232a.put(2131558576, 89);
        f2232a.put(2131558577, 90);
        f2232a.put(2131558578, 91);
        f2232a.put(2131558579, 92);
        f2232a.put(2131558580, 93);
        f2232a.put(2131558581, 94);
        f2232a.put(2131558583, 95);
        f2232a.put(2131558584, 96);
        f2232a.put(2131558585, 97);
        f2232a.put(2131558586, 98);
        f2232a.put(2131558587, 99);
        f2232a.put(2131558588, 100);
        f2232a.put(2131558589, 101);
        f2232a.put(2131558590, 102);
        f2232a.put(2131558591, 103);
        f2232a.put(2131558592, 104);
        f2232a.put(2131558593, 105);
        f2232a.put(2131558594, 106);
        f2232a.put(2131558595, 107);
        f2232a.put(2131558596, 108);
        f2232a.put(2131558597, 109);
        f2232a.put(2131558598, 110);
        f2232a.put(2131558599, 111);
        f2232a.put(2131558600, 112);
        f2232a.put(2131558602, 113);
        f2232a.put(2131558603, 114);
        f2232a.put(2131558604, 115);
        f2232a.put(2131558605, 116);
        f2232a.put(2131558606, 117);
        f2232a.put(2131558607, 118);
        f2232a.put(2131558608, 119);
        f2232a.put(2131558609, 120);
        f2232a.put(2131558610, 121);
        f2232a.put(2131558611, 122);
        f2232a.put(2131558612, 123);
        f2232a.put(2131558613, 124);
        f2232a.put(2131558614, 125);
        f2232a.put(2131558615, 126);
        f2232a.put(2131558618, 127);
        f2232a.put(2131558620, 128);
        f2232a.put(2131558621, 129);
        f2232a.put(2131558622, 130);
        f2232a.put(2131558623, 131);
        f2232a.put(2131558624, 132);
        f2232a.put(2131558625, 133);
        f2232a.put(2131558626, 134);
        f2232a.put(2131558627, 135);
        f2232a.put(2131558628, 136);
        f2232a.put(2131558629, 137);
        f2232a.put(2131558632, 138);
        f2232a.put(2131558633, 139);
        f2232a.put(2131558635, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        f2232a.put(2131558636, 141);
        f2232a.put(2131558637, 142);
        f2232a.put(2131558639, 143);
        f2232a.put(2131558640, 144);
        f2232a.put(2131558641, 145);
        f2232a.put(2131558642, 146);
        f2232a.put(2131558643, 147);
        f2232a.put(2131558644, 148);
        f2232a.put(2131558645, 149);
        f2232a.put(2131558647, 150);
        f2232a.put(2131558650, 151);
        f2232a.put(2131558651, 152);
        f2232a.put(2131558652, 153);
        f2232a.put(2131558653, 154);
        f2232a.put(2131558654, 155);
        f2232a.put(2131558655, 156);
        f2232a.put(2131558656, 157);
        f2232a.put(2131558658, 158);
        f2232a.put(2131558659, 159);
        f2232a.put(2131558660, 160);
        f2232a.put(2131558661, 161);
        f2232a.put(2131558664, 162);
        f2232a.put(2131558665, 163);
        f2232a.put(2131558666, 164);
        f2232a.put(2131558668, 165);
        f2232a.put(2131558672, 166);
        f2232a.put(2131558673, 167);
        f2232a.put(2131558680, DateTimeConstants.HOURS_PER_WEEK);
        f2232a.put(2131558681, 169);
        f2232a.put(2131558684, 170);
        f2232a.put(2131558686, 171);
        f2232a.put(2131558688, 172);
        f2232a.put(2131558689, 173);
        f2232a.put(2131558690, 174);
        f2232a.put(2131558691, 175);
        f2232a.put(2131558692, 176);
        f2232a.put(2131558693, 177);
        f2232a.put(2131558695, 178);
        f2232a.put(2131558697, 179);
        f2232a.put(2131558698, 180);
        f2232a.put(2131558704, 181);
        f2232a.put(2131558706, 182);
        f2232a.put(2131558707, 183);
        f2232a.put(2131558708, 184);
        f2232a.put(2131558709, 185);
        f2232a.put(2131558710, 186);
        f2232a.put(2131558716, 187);
        f2232a.put(2131558717, 188);
        f2232a.put(2131558718, 189);
        f2232a.put(2131558720, FacebookRequestErrorClassification.EC_INVALID_TOKEN);
        f2232a.put(2131558722, 191);
        f2232a.put(2131558728, 192);
        f2232a.put(2131558729, 193);
        f2232a.put(2131558743, 194);
        f2232a.put(2131558843, 195);
        f2232a.put(2131558844, 196);
        f2232a.put(2131558851, 197);
        f2232a.put(2131558859, 198);
        f2232a.put(2131558862, Action.Music.MUSIC_END_ACTION);
        f2232a.put(2131558863, 200);
        f2232a.put(2131558864, 201);
    }
    */

    @DexIgnore
    @Override // com.fossil.xp0
    public List<xp0> a() {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new dq0());
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public ViewDataBinding b(zp0 zp0, View view, int i) {
        int i2 = f2232a.get(i);
        if (i2 > 0) {
            Object tag = view.getTag();
            if (tag != null) {
                int i3 = (i2 - 1) / 50;
                if (i3 == 0) {
                    return d(zp0, view, i2, tag);
                }
                if (i3 == 1) {
                    return e(zp0, view, i2, tag);
                }
                if (i3 == 2) {
                    return f(zp0, view, i2, tag);
                }
                if (i3 == 3) {
                    return g(zp0, view, i2, tag);
                }
                if (i3 == 4) {
                    return h(zp0, view, i2, tag);
                }
            } else {
                throw new RuntimeException("view must have a tag");
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public ViewDataBinding c(zp0 zp0, View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || f2232a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    public final ViewDataBinding d(zp0 zp0, View view, int i, Object obj) {
        switch (i) {
            case 1:
                if ("layout/activity_search_ringphone_0".equals(obj)) {
                    return new f15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for activity_search_ringphone is invalid. Received: " + obj);
            case 2:
                if ("layout/activity_webview_0".equals(obj)) {
                    return new h15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for activity_webview is invalid. Received: " + obj);
            case 3:
                if ("layout/bottom_date_time_dialog_0".equals(obj)) {
                    return new j15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for bottom_date_time_dialog is invalid. Received: " + obj);
            case 4:
                if ("layout/bottom_dialog_layout_0".equals(obj)) {
                    return new l15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for bottom_dialog_layout is invalid. Received: " + obj);
            case 5:
                if ("layout/dialog_item_privacy_layout_0".equals(obj)) {
                    return new n15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for dialog_item_privacy_layout is invalid. Received: " + obj);
            case 6:
                if ("layout/fragment_about_0".equals(obj)) {
                    return new p15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_about is invalid. Received: " + obj);
            case 7:
                if ("layout/fragment_active_time_detail_0".equals(obj)) {
                    return new r15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_detail is invalid. Received: " + obj);
            case 8:
                if ("layout/fragment_active_time_overview_0".equals(obj)) {
                    return new t15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview is invalid. Received: " + obj);
            case 9:
                if ("layout/fragment_active_time_overview_day_0".equals(obj)) {
                    return new v15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_day is invalid. Received: " + obj);
            case 10:
                if ("layout/fragment_active_time_overview_month_0".equals(obj)) {
                    return new x15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_month is invalid. Received: " + obj);
            case 11:
                if ("layout/fragment_active_time_overview_week_0".equals(obj)) {
                    return new z15(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_week is invalid. Received: " + obj);
            case 12:
                if ("layout/fragment_activity_detail_0".equals(obj)) {
                    return new b25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_detail is invalid. Received: " + obj);
            case 13:
                if ("layout/fragment_activity_overview_0".equals(obj)) {
                    return new d25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview is invalid. Received: " + obj);
            case 14:
                if ("layout/fragment_activity_overview_day_0".equals(obj)) {
                    return new f25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_day is invalid. Received: " + obj);
            case 15:
                if ("layout/fragment_activity_overview_month_0".equals(obj)) {
                    return new h25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_month is invalid. Received: " + obj);
            case 16:
                if ("layout/fragment_activity_overview_week_0".equals(obj)) {
                    return new j25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_week is invalid. Received: " + obj);
            case 17:
                if ("layout/fragment_add_photo_menu_0".equals(obj)) {
                    return new l25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_add_photo_menu is invalid. Received: " + obj);
            case 18:
                if ("layout/fragment_alarm_0".equals(obj)) {
                    return new o25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_alarm is invalid. Received: " + obj);
            case 19:
                if ("layout/fragment_birthday_0".equals(obj)) {
                    return new q25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_birthday is invalid. Received: " + obj);
            case 20:
                if ("layout/fragment_buddy_challenge_0".equals(obj)) {
                    return new s25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_buddy_challenge is invalid. Received: " + obj);
            case 21:
                if ("layout/fragment_calibration_0".equals(obj)) {
                    return new u25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calibration is invalid. Received: " + obj);
            case 22:
                if ("layout/fragment_calories_detail_0".equals(obj)) {
                    return new w25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_detail is invalid. Received: " + obj);
            case 23:
                if ("layout/fragment_calories_overview_0".equals(obj)) {
                    return new y25(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview is invalid. Received: " + obj);
            case 24:
                if ("layout/fragment_calories_overview_day_0".equals(obj)) {
                    return new a35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_day is invalid. Received: " + obj);
            case 25:
                if ("layout/fragment_calories_overview_month_0".equals(obj)) {
                    return new c35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_month is invalid. Received: " + obj);
            case 26:
                if ("layout/fragment_calories_overview_week_0".equals(obj)) {
                    return new e35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_week is invalid. Received: " + obj);
            case 27:
                if ("layout/fragment_challenge_find_friends_0".equals(obj)) {
                    return new g35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_challenge_find_friends is invalid. Received: " + obj);
            case 28:
                if ("layout/fragment_challenge_friends_0".equals(obj)) {
                    return new i35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_challenge_friends is invalid. Received: " + obj);
            case 29:
                if ("layout/fragment_challenge_invite_0".equals(obj)) {
                    return new k35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_challenge_invite is invalid. Received: " + obj);
            case 30:
                if ("layout/fragment_challenge_notification_0".equals(obj)) {
                    return new m35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_challenge_notification is invalid. Received: " + obj);
            case 31:
                if ("layout/fragment_commute_time_settings_0".equals(obj)) {
                    return new o35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings is invalid. Received: " + obj);
            case 32:
                if ("layout/fragment_commute_time_settings_default_address_0".equals(obj)) {
                    return new q35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings_default_address is invalid. Received: " + obj);
            case 33:
                if ("layout/fragment_commute_time_settings_detail_0".equals(obj)) {
                    return new s35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings_detail is invalid. Received: " + obj);
            case 34:
                if ("layout/fragment_commute_time_watch_app_settings_0".equals(obj)) {
                    return new u35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_watch_app_settings is invalid. Received: " + obj);
            case 35:
                if ("layout/fragment_complication_search_0".equals(obj)) {
                    return new w35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_complication_search is invalid. Received: " + obj);
            case 36:
                if ("layout/fragment_complications_0".equals(obj)) {
                    return new y35(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_complications is invalid. Received: " + obj);
            case 37:
                if ("layout/fragment_connected_apps_0".equals(obj)) {
                    return new a45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_connected_apps is invalid. Received: " + obj);
            case 38:
                if ("layout/fragment_create_challenge_input_layout_0".equals(obj)) {
                    return new c45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_create_challenge_input_layout is invalid. Received: " + obj);
            case 39:
                if ("layout/fragment_create_challenge_intro_layout_0".equals(obj)) {
                    return new e45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_create_challenge_intro_layout is invalid. Received: " + obj);
            case 40:
                if ("layout/fragment_create_subtab_layout_0".equals(obj)) {
                    return new g45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_create_subtab_layout is invalid. Received: " + obj);
            case 41:
                if ("layout/fragment_current_challenge_subtab_layout_0".equals(obj)) {
                    return new i45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_current_challenge_subtab_layout is invalid. Received: " + obj);
            case 42:
                if ("layout/fragment_customize_active_calories_chart_0".equals(obj)) {
                    return new k45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_active_calories_chart is invalid. Received: " + obj);
            case 43:
                if ("layout/fragment_customize_active_minute_chart_0".equals(obj)) {
                    return new m45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_active_minute_chart is invalid. Received: " + obj);
            case 44:
                if ("layout/fragment_customize_activity_chart_0".equals(obj)) {
                    return new o45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_activity_chart is invalid. Received: " + obj);
            case 45:
                if ("layout/fragment_customize_back_ground_0".equals(obj)) {
                    return new q45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_back_ground is invalid. Received: " + obj);
            case 46:
                if ("layout/fragment_customize_button_0".equals(obj)) {
                    return new s45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_button is invalid. Received: " + obj);
            case 47:
                if ("layout/fragment_customize_font_0".equals(obj)) {
                    return new u45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_font is invalid. Received: " + obj);
            case 48:
                if ("layout/fragment_customize_goal_tracking_chart_0".equals(obj)) {
                    return new w45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_goal_tracking_chart is invalid. Received: " + obj);
            case 49:
                if ("layout/fragment_customize_heart_rate_chart_0".equals(obj)) {
                    return new y45(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_heart_rate_chart is invalid. Received: " + obj);
            case 50:
                if ("layout/fragment_customize_ring_chart_0".equals(obj)) {
                    return new a55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_ring_chart is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding e(zp0 zp0, View view, int i, Object obj) {
        switch (i) {
            case 51:
                if ("layout/fragment_customize_sleep_chart_0".equals(obj)) {
                    return new c55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_sleep_chart is invalid. Received: " + obj);
            case 52:
                if ("layout/fragment_customize_text_0".equals(obj)) {
                    return new e55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_text is invalid. Received: " + obj);
            case 53:
                if ("layout/fragment_customize_theme_0".equals(obj)) {
                    return new g55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_theme is invalid. Received: " + obj);
            case 54:
                if ("layout/fragment_customize_tutorial_0".equals(obj)) {
                    return new i55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_tutorial is invalid. Received: " + obj);
            case 55:
                if ("layout/fragment_dashboard_active_time_0".equals(obj)) {
                    return new k55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_active_time is invalid. Received: " + obj);
            case 56:
                if ("layout/fragment_dashboard_activity_0".equals(obj)) {
                    return new m55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_activity is invalid. Received: " + obj);
            case 57:
                if ("layout/fragment_dashboard_calories_0".equals(obj)) {
                    return new o55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_calories is invalid. Received: " + obj);
            case 58:
                if ("layout/fragment_dashboard_goal_tracking_0".equals(obj)) {
                    return new q55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_goal_tracking is invalid. Received: " + obj);
            case 59:
                if ("layout/fragment_dashboard_heartrate_0".equals(obj)) {
                    return new s55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_heartrate is invalid. Received: " + obj);
            case 60:
                if ("layout/fragment_dashboard_sleep_0".equals(obj)) {
                    return new u55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_sleep is invalid. Received: " + obj);
            case 61:
                if ("layout/fragment_delete_account_0".equals(obj)) {
                    return new w55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_delete_account is invalid. Received: " + obj);
            case 62:
                if ("layout/fragment_diana_customize_edit_0".equals(obj)) {
                    return new y55(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_diana_customize_edit is invalid. Received: " + obj);
            case 63:
                if ("layout/fragment_do_not_disturb_scheduled_time_0".equals(obj)) {
                    return new a65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_do_not_disturb_scheduled_time is invalid. Received: " + obj);
            case 64:
                if ("layout/fragment_edit_photo_0".equals(obj)) {
                    return new c65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_edit_photo is invalid. Received: " + obj);
            case 65:
                if ("layout/fragment_edit_response_0".equals(obj)) {
                    return new e65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_edit_response is invalid. Received: " + obj);
            case 66:
                if ("layout/fragment_email_verification_0".equals(obj)) {
                    return new g65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_email_verification is invalid. Received: " + obj);
            case 67:
                if ("layout/fragment_explore_watch_0".equals(obj)) {
                    return new i65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_explore_watch is invalid. Received: " + obj);
            case 68:
                if ("layout/fragment_find_device_0".equals(obj)) {
                    return new k65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_find_device is invalid. Received: " + obj);
            case 69:
                if ("layout/fragment_forgot_password_0".equals(obj)) {
                    return new m65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_forgot_password is invalid. Received: " + obj);
            case 70:
                if ("layout/fragment_getting_started_0".equals(obj)) {
                    return new o65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_getting_started is invalid. Received: " + obj);
            case 71:
                if ("layout/fragment_goal_tracking_detail_0".equals(obj)) {
                    return new q65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_detail is invalid. Received: " + obj);
            case 72:
                if ("layout/fragment_goal_tracking_overview_0".equals(obj)) {
                    return new s65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview is invalid. Received: " + obj);
            case 73:
                if ("layout/fragment_goal_tracking_overview_day_0".equals(obj)) {
                    return new u65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_day is invalid. Received: " + obj);
            case 74:
                if ("layout/fragment_goal_tracking_overview_month_0".equals(obj)) {
                    return new w65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_month is invalid. Received: " + obj);
            case 75:
                if ("layout/fragment_goal_tracking_overview_week_0".equals(obj)) {
                    return new y65(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_week is invalid. Received: " + obj);
            case 76:
                if ("layout/fragment_heartrate_detail_0".equals(obj)) {
                    return new a75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_detail is invalid. Received: " + obj);
            case 77:
                if ("layout/fragment_heartrate_overview_0".equals(obj)) {
                    return new c75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview is invalid. Received: " + obj);
            case 78:
                if ("layout/fragment_heartrate_overview_day_0".equals(obj)) {
                    return new e75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_day is invalid. Received: " + obj);
            case 79:
                if ("layout/fragment_heartrate_overview_month_0".equals(obj)) {
                    return new g75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_month is invalid. Received: " + obj);
            case 80:
                if ("layout/fragment_heartrate_overview_week_0".equals(obj)) {
                    return new i75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_week is invalid. Received: " + obj);
            case 81:
                if ("layout/fragment_help_0".equals(obj)) {
                    return new k75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_help is invalid. Received: " + obj);
            case 82:
                if ("layout/fragment_history_layout_0".equals(obj)) {
                    return new m75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_history_layout is invalid. Received: " + obj);
            case 83:
                if ("layout/fragment_home_0".equals(obj)) {
                    return new s75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + obj);
            case 84:
                if ("layout/fragment_home_alerts_0".equals(obj)) {
                    return new o75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_alerts is invalid. Received: " + obj);
            case 85:
                if ("layout/fragment_home_alerts_hybrid_0".equals(obj)) {
                    return new q75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_alerts_hybrid is invalid. Received: " + obj);
            case 86:
                if ("layout/fragment_home_buddy_challenge_0".equals(obj)) {
                    return new u75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_buddy_challenge is invalid. Received: " + obj);
            case 87:
                if ("layout/fragment_home_dashboard_0".equals(obj)) {
                    return new w75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_dashboard is invalid. Received: " + obj);
            case 88:
                if ("layout/fragment_home_diana_customize_0".equals(obj)) {
                    return new y75(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_diana_customize is invalid. Received: " + obj);
            case 89:
                if ("layout/fragment_home_hybrid_customize_0".equals(obj)) {
                    return new a85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_hybrid_customize is invalid. Received: " + obj);
            case 90:
                if ("layout/fragment_home_profile_0".equals(obj)) {
                    return new c85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_profile is invalid. Received: " + obj);
            case 91:
                if ("layout/fragment_home_social_0".equals(obj)) {
                    return new e85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_social is invalid. Received: " + obj);
            case 92:
                if ("layout/fragment_home_update_firmware_0".equals(obj)) {
                    return new g85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_update_firmware is invalid. Received: " + obj);
            case 93:
                if ("layout/fragment_hybrid_customize_edit_0".equals(obj)) {
                    return new i85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_hybrid_customize_edit is invalid. Received: " + obj);
            case 94:
                if ("layout/fragment_leader_board_layout_0".equals(obj)) {
                    return new k85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_leader_board_layout is invalid. Received: " + obj);
            case 95:
                if ("layout/fragment_map_picker_0".equals(obj)) {
                    return new m85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_map_picker is invalid. Received: " + obj);
            case 96:
                if ("layout/fragment_member_in_challenge_layout_0".equals(obj)) {
                    return new o85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_member_in_challenge_layout is invalid. Received: " + obj);
            case 97:
                if ("layout/fragment_micro_app_0".equals(obj)) {
                    return new q85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_micro_app is invalid. Received: " + obj);
            case 98:
                if ("layout/fragment_micro_app_search_0".equals(obj)) {
                    return new s85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_micro_app_search is invalid. Received: " + obj);
            case 99:
                if ("layout/fragment_notification_allow_calls_and_messages_0".equals(obj)) {
                    return new u85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_allow_calls_and_messages is invalid. Received: " + obj);
            case 100:
                if ("layout/fragment_notification_apps_0".equals(obj)) {
                    return new w85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_apps is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding f(zp0 zp0, View view, int i, Object obj) {
        switch (i) {
            case 101:
                if ("layout/fragment_notification_calls_and_messages_0".equals(obj)) {
                    return new y85(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_calls_and_messages is invalid. Received: " + obj);
            case 102:
                if ("layout/fragment_notification_contacts_0".equals(obj)) {
                    return new c95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_contacts is invalid. Received: " + obj);
            case 103:
                if ("layout/fragment_notification_contacts_and_apps_assigned_0".equals(obj)) {
                    return new a95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_contacts_and_apps_assigned is invalid. Received: " + obj);
            case 104:
                if ("layout/fragment_notification_dial_landing_0".equals(obj)) {
                    return new e95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_dial_landing is invalid. Received: " + obj);
            case 105:
                if ("layout/fragment_notification_hybrid_app_0".equals(obj)) {
                    return new g95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_app is invalid. Received: " + obj);
            case 106:
                if ("layout/fragment_notification_hybrid_contact_0".equals(obj)) {
                    return new i95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_contact is invalid. Received: " + obj);
            case 107:
                if ("layout/fragment_notification_hybrid_everyone_0".equals(obj)) {
                    return new k95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_everyone is invalid. Received: " + obj);
            case 108:
                if ("layout/fragment_notification_settings_type_0".equals(obj)) {
                    return new m95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_settings_type is invalid. Received: " + obj);
            case 109:
                if ("layout/fragment_notification_watch_reminders_0".equals(obj)) {
                    return new o95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_watch_reminders is invalid. Received: " + obj);
            case 110:
                if ("layout/fragment_onboarding_height_weight_0".equals(obj)) {
                    return new q95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_onboarding_height_weight is invalid. Received: " + obj);
            case 111:
                if ("layout/fragment_opt_in_0".equals(obj)) {
                    return new s95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_opt_in is invalid. Received: " + obj);
            case 112:
                if ("layout/fragment_overview_leader_board_layout_0".equals(obj)) {
                    return new u95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_overview_leader_board_layout is invalid. Received: " + obj);
            case 113:
                if ("layout/fragment_pairing_authorize_0".equals(obj)) {
                    return new w95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_authorize is invalid. Received: " + obj);
            case 114:
                if ("layout/fragment_pairing_device_found_0".equals(obj)) {
                    return new y95(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_device_found is invalid. Received: " + obj);
            case 115:
                if ("layout/fragment_pairing_instructions_0".equals(obj)) {
                    return new aa5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_instructions is invalid. Received: " + obj);
            case 116:
                if ("layout/fragment_pairing_look_for_device_0".equals(obj)) {
                    return new ca5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_look_for_device is invalid. Received: " + obj);
            case 117:
                if ("layout/fragment_permission_0".equals(obj)) {
                    return new ea5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_permission is invalid. Received: " + obj);
            case 118:
                if ("layout/fragment_preferred_unit_0".equals(obj)) {
                    return new ga5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_preferred_unit is invalid. Received: " + obj);
            case 119:
                if ("layout/fragment_preview_0".equals(obj)) {
                    return new ia5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_preview is invalid. Received: " + obj);
            case 120:
                if ("layout/fragment_profile_change_pass_0".equals(obj)) {
                    return new ka5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_change_pass is invalid. Received: " + obj);
            case 121:
                if ("layout/fragment_profile_edit_0".equals(obj)) {
                    return new ma5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_edit is invalid. Received: " + obj);
            case 122:
                if ("layout/fragment_profile_goals_edit_0".equals(obj)) {
                    return new oa5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_goals_edit is invalid. Received: " + obj);
            case 123:
                if ("layout/fragment_profile_setup_0".equals(obj)) {
                    return new qa5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_setup is invalid. Received: " + obj);
            case 124:
                if ("layout/fragment_quick_response_0".equals(obj)) {
                    return new sa5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_quick_response is invalid. Received: " + obj);
            case 125:
                if ("layout/fragment_recommendation_subtab_layout_0".equals(obj)) {
                    return new ua5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_recommendation_subtab_layout is invalid. Received: " + obj);
            case 126:
                if ("layout/fragment_remind_time_0".equals(obj)) {
                    return new wa5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_remind_time is invalid. Received: " + obj);
            case 127:
                if ("layout/fragment_replace_battery_0".equals(obj)) {
                    return new ya5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_replace_battery is invalid. Received: " + obj);
            case 128:
                if ("layout/fragment_search_second_timezone_0".equals(obj)) {
                    return new ab5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_search_second_timezone is invalid. Received: " + obj);
            case 129:
                if ("layout/fragment_signin_0".equals(obj)) {
                    return new cb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_signin is invalid. Received: " + obj);
            case 130:
                if ("layout/fragment_signup_0".equals(obj)) {
                    return new eb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_signup is invalid. Received: " + obj);
            case 131:
                if ("layout/fragment_sleep_detail_0".equals(obj)) {
                    return new gb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_detail is invalid. Received: " + obj);
            case 132:
                if ("layout/fragment_sleep_overview_0".equals(obj)) {
                    return new ib5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview is invalid. Received: " + obj);
            case 133:
                if ("layout/fragment_sleep_overview_day_0".equals(obj)) {
                    return new kb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_day is invalid. Received: " + obj);
            case 134:
                if ("layout/fragment_sleep_overview_month_0".equals(obj)) {
                    return new mb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_month is invalid. Received: " + obj);
            case 135:
                if ("layout/fragment_sleep_overview_week_0".equals(obj)) {
                    return new ob5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_week is invalid. Received: " + obj);
            case 136:
                if ("layout/fragment_splash_screen_0".equals(obj)) {
                    return new qb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_splash_screen is invalid. Received: " + obj);
            case 137:
                if ("layout/fragment_themes_0".equals(obj)) {
                    return new sb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_themes is invalid. Received: " + obj);
            case 138:
                if ("layout/fragment_update_firmware_0".equals(obj)) {
                    return new vb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_update_firmware is invalid. Received: " + obj);
            case 139:
                if ("layout/fragment_user_customize_theme_0".equals(obj)) {
                    return new xb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_user_customize_theme is invalid. Received: " + obj);
            case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL /* 140 */:
                if ("layout/fragment_waiting_challenge_detail_layout_0".equals(obj)) {
                    return new zb5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_waiting_challenge_detail_layout is invalid. Received: " + obj);
            case 141:
                if ("layout/fragment_watch_app_search_0".equals(obj)) {
                    return new bc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_app_search is invalid. Received: " + obj);
            case 142:
                if ("layout/fragment_watch_apps_0".equals(obj)) {
                    return new dc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_apps is invalid. Received: " + obj);
            case 143:
                if ("layout/fragment_watch_setting_0".equals(obj)) {
                    return new gc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_setting is invalid. Received: " + obj);
            case 144:
                if ("layout/fragment_weather_setting_0".equals(obj)) {
                    return new ic5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_weather_setting is invalid. Received: " + obj);
            case 145:
                if ("layout/fragment_welcome_0".equals(obj)) {
                    return new kc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_welcome is invalid. Received: " + obj);
            case 146:
                if ("layout/fragment_workout_edit_0".equals(obj)) {
                    return new mc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_workout_edit is invalid. Received: " + obj);
            case 147:
                if ("layout/fragment_workout_edit_picker_0".equals(obj)) {
                    return new oc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_workout_edit_picker is invalid. Received: " + obj);
            case 148:
                if ("layout/fragment_workout_menu_0".equals(obj)) {
                    return new qc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_workout_menu is invalid. Received: " + obj);
            case 149:
                if ("layout/fragment_workout_setting_0".equals(obj)) {
                    return new sc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for fragment_workout_setting is invalid. Received: " + obj);
            case 150:
                if ("layout/home_buddy_challenge_fragment_layout_0".equals(obj)) {
                    return new uc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for home_buddy_challenge_fragment_layout is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding g(zp0 zp0, View view, int i, Object obj) {
        switch (i) {
            case 151:
                if ("layout/item_active_time_day_0".equals(obj)) {
                    return new wc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_day is invalid. Received: " + obj);
            case 152:
                if ("layout/item_active_time_week_0".equals(obj)) {
                    return new yc5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_week is invalid. Received: " + obj);
            case 153:
                if ("layout/item_active_time_workout_day_0".equals(obj)) {
                    return new ad5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_workout_day is invalid. Received: " + obj);
            case 154:
                if ("layout/item_activity_day_0".equals(obj)) {
                    return new cd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_day is invalid. Received: " + obj);
            case 155:
                if ("layout/item_activity_week_0".equals(obj)) {
                    return new ed5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_week is invalid. Received: " + obj);
            case 156:
                if ("layout/item_activity_workout_day_0".equals(obj)) {
                    return new gd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_workout_day is invalid. Received: " + obj);
            case 157:
                if ("layout/item_address_commute_time_0".equals(obj)) {
                    return new id5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_address_commute_time is invalid. Received: " + obj);
            case 158:
                if ("layout/item_alarm_0".equals(obj)) {
                    return new kd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_alarm is invalid. Received: " + obj);
            case 159:
                if ("layout/item_app_hybrid_notification_0".equals(obj)) {
                    return new md5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_app_hybrid_notification is invalid. Received: " + obj);
            case 160:
                if ("layout/item_app_notification_0".equals(obj)) {
                    return new od5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_app_notification is invalid. Received: " + obj);
            case 161:
                if ("layout/item_bc_notification_challenge_start_0".equals(obj)) {
                    return new qd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_bc_notification_challenge_start is invalid. Received: " + obj);
            case 162:
                if ("layout/item_calories_day_0".equals(obj)) {
                    return new sd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_day is invalid. Received: " + obj);
            case 163:
                if ("layout/item_calories_week_0".equals(obj)) {
                    return new ud5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_week is invalid. Received: " + obj);
            case 164:
                if ("layout/item_calories_workout_day_0".equals(obj)) {
                    return new wd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_workout_day is invalid. Received: " + obj);
            case 165:
                if ("layout/item_challenge_template_list_0".equals(obj)) {
                    return new yd5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_challenge_template_list is invalid. Received: " + obj);
            case 166:
                if ("layout/item_contact_0".equals(obj)) {
                    return new be5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_contact is invalid. Received: " + obj);
            case 167:
                if ("layout/item_contact_hybrid_0".equals(obj)) {
                    return new de5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_contact_hybrid is invalid. Received: " + obj);
            case DateTimeConstants.HOURS_PER_WEEK /* 168 */:
                if ("layout/item_default_place_commute_time_0".equals(obj)) {
                    return new fe5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_default_place_commute_time is invalid. Received: " + obj);
            case 169:
                if ("layout/item_diana_background_0".equals(obj)) {
                    return new he5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_diana_background is invalid. Received: " + obj);
            case 170:
                if ("layout/item_diana_preset_layout_0".equals(obj)) {
                    return new je5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_diana_preset_layout is invalid. Received: " + obj);
            case 171:
                if ("layout/item_favorite_contact_notification_0".equals(obj)) {
                    return new me5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_favorite_contact_notification is invalid. Received: " + obj);
            case 172:
                if ("layout/item_friend_list_0".equals(obj)) {
                    return new oe5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_friend_list is invalid. Received: " + obj);
            case 173:
                if ("layout/item_friend_search_0".equals(obj)) {
                    return new qe5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_friend_search is invalid. Received: " + obj);
            case 174:
                if ("layout/item_goal_tracking_day_0".equals(obj)) {
                    return new se5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_goal_tracking_day is invalid. Received: " + obj);
            case 175:
                if ("layout/item_goal_tracking_week_0".equals(obj)) {
                    return new ue5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_goal_tracking_week is invalid. Received: " + obj);
            case 176:
                if ("layout/item_header_member_0".equals(obj)) {
                    return new we5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_header_member is invalid. Received: " + obj);
            case 177:
                if ("layout/item_heart_rate_day_0".equals(obj)) {
                    return new ye5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_heart_rate_day is invalid. Received: " + obj);
            case 178:
                if ("layout/item_heart_rate_week_0".equals(obj)) {
                    return new af5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_heart_rate_week is invalid. Received: " + obj);
            case 179:
                if ("layout/item_heartrate_workout_day_0".equals(obj)) {
                    return new cf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_heartrate_workout_day is invalid. Received: " + obj);
            case 180:
                if ("layout/item_history_challenge_layout_0".equals(obj)) {
                    return new ef5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_history_challenge_layout is invalid. Received: " + obj);
            case 181:
                if ("layout/item_notification_hybrid_0".equals(obj)) {
                    return new gf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_notification_hybrid is invalid. Received: " + obj);
            case 182:
                if ("layout/item_permission_0".equals(obj)) {
                    return new if5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_permission is invalid. Received: " + obj);
            case 183:
                if ("layout/item_player_list_0".equals(obj)) {
                    return new kf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_player_list is invalid. Received: " + obj);
            case 184:
                if ("layout/item_recommendation_challenge_layout_0".equals(obj)) {
                    return new mf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_recommendation_challenge_layout is invalid. Received: " + obj);
            case 185:
                if ("layout/item_recommended_challenge_header_layout_0".equals(obj)) {
                    return new of5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_recommended_challenge_header_layout is invalid. Received: " + obj);
            case 186:
                if ("layout/item_recorded_goal_tracking_0".equals(obj)) {
                    return new qf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_recorded_goal_tracking is invalid. Received: " + obj);
            case 187:
                if ("layout/item_sent_request_friend_0".equals(obj)) {
                    return new tf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_sent_request_friend is invalid. Received: " + obj);
            case 188:
                if ("layout/item_sleep_day_0".equals(obj)) {
                    return new vf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_sleep_day is invalid. Received: " + obj);
            case 189:
                if ("layout/item_sleep_week_0".equals(obj)) {
                    return new xf5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_sleep_week is invalid. Received: " + obj);
            case FacebookRequestErrorClassification.EC_INVALID_TOKEN /* 190 */:
                if ("layout/item_suggesting_friend_layout_0".equals(obj)) {
                    return new ag5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_suggesting_friend_layout is invalid. Received: " + obj);
            case 191:
                if ("layout/item_theme_background_add_0".equals(obj)) {
                    return new cg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_theme_background_add is invalid. Received: " + obj);
            case 192:
                if ("layout/item_workout_0".equals(obj)) {
                    return new fg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_workout is invalid. Received: " + obj);
            case 193:
                if ("layout/item_workout_setting_0".equals(obj)) {
                    return new hg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for item_workout_setting is invalid. Received: " + obj);
            case 194:
                if ("layout/migration_fragment_0".equals(obj)) {
                    return new kg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for migration_fragment is invalid. Received: " + obj);
            case 195:
                if ("layout/view_no_device_0".equals(obj)) {
                    return new mg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for view_no_device is invalid. Received: " + obj);
            case 196:
                if ("layout/view_tab_custom_0".equals(obj)) {
                    return new og5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for view_tab_custom is invalid. Received: " + obj);
            case 197:
                if ("layout/watch_face_customize_fragment_0".equals(obj)) {
                    return new sg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for watch_face_customize_fragment is invalid. Received: " + obj);
            case 198:
                if ("layout/watch_face_photo_background_fragment_0".equals(obj)) {
                    return new xg5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for watch_face_photo_background_fragment is invalid. Received: " + obj);
            case Action.Music.MUSIC_END_ACTION /* 199 */:
                if ("layout/watch_face_sticker_fragment_0".equals(obj)) {
                    return new ah5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for watch_face_sticker_fragment is invalid. Received: " + obj);
            case 200:
                if ("layout/watch_face_template_fragment_0".equals(obj)) {
                    return new ch5(zp0, view);
                }
                throw new IllegalArgumentException("The tag for watch_face_template_fragment is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding h(zp0 zp0, View view, int i, Object obj) {
        if (i != 201) {
            return null;
        }
        if ("layout/watch_face_text_fragment_0".equals(obj)) {
            return new eh5(zp0, view);
        }
        throw new IllegalArgumentException("The tag for watch_face_text_fragment is invalid. Received: " + obj);
    }
}
