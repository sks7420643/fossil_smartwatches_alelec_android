package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gc2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static int f1288a; // = 129;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static gc2 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Uri f; // = new Uri.Builder().scheme("content").authority("com.google.android.gms.chimera").build();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1289a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ComponentName c; // = null;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public a(String str, String str2, int i, boolean z) {
            rc2.g(str);
            this.f1289a = str;
            rc2.g(str2);
            this.b = str2;
            this.d = i;
            this.e = z;
        }

        @DexIgnore
        public final ComponentName a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Intent c(Context context) {
            if (this.f1289a == null) {
                return new Intent().setComponent(this.c);
            }
            Intent d2 = this.e ? d(context) : null;
            return d2 == null ? new Intent(this.f1289a).setPackage(this.b) : d2;
        }

        @DexIgnore
        public final Intent d(Context context) {
            Bundle bundle;
            Intent intent = null;
            Bundle bundle2 = new Bundle();
            bundle2.putString("serviceActionBundleKey", this.f1289a);
            try {
                bundle = context.getContentResolver().call(f, "serviceIntentCall", (String) null, bundle2);
            } catch (IllegalArgumentException e2) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                sb.append("Dynamic intent resolution failed: ");
                sb.append(valueOf);
                Log.w("ConnectionStatusConfig", sb.toString());
                bundle = null;
            }
            if (bundle != null) {
                intent = (Intent) bundle.getParcelable("serviceResponseIntentKey");
            }
            if (intent == null) {
                String valueOf2 = String.valueOf(this.f1289a);
                Log.w("ConnectionStatusConfig", valueOf2.length() != 0 ? "Dynamic lookup for intent failed for action: ".concat(valueOf2) : new String("Dynamic lookup for intent failed for action: "));
            }
            return intent;
        }

        @DexIgnore
        public final int e() {
            return this.d;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return pc2.a(this.f1289a, aVar.f1289a) && pc2.a(this.b, aVar.b) && pc2.a(this.c, aVar.c) && this.d == aVar.d && this.e == aVar.e;
        }

        @DexIgnore
        public final int hashCode() {
            return pc2.b(this.f1289a, this.b, this.c, Integer.valueOf(this.d), Boolean.valueOf(this.e));
        }

        @DexIgnore
        public final String toString() {
            String str = this.f1289a;
            if (str != null) {
                return str;
            }
            rc2.k(this.c);
            return this.c.flattenToString();
        }
    }

    @DexIgnore
    public static int a() {
        return f1288a;
    }

    @DexIgnore
    public static gc2 b(Context context) {
        synchronized (b) {
            if (c == null) {
                c = new he2(context.getApplicationContext());
            }
        }
        return c;
    }

    @DexIgnore
    public final void c(String str, String str2, int i, ServiceConnection serviceConnection, String str3, boolean z) {
        e(new a(str, str2, i, z), serviceConnection, str3);
    }

    @DexIgnore
    public abstract boolean d(a aVar, ServiceConnection serviceConnection, String str);

    @DexIgnore
    public abstract void e(a aVar, ServiceConnection serviceConnection, String str);
}
