package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum uh5 {
    BLUETOOTH_OFF(0),
    LOCATION_PERMISSION_OFF(1),
    LOCATION_SERVICE_OFF(2),
    LOCATION_PERMISSION_FEATURE_OFF(3),
    LOCATION_SERVICE_FEATURE_OFF(4),
    BACKGROUND_LOCATION_PERMISSION_OFF(5);
    
    @DexIgnore
    public int mCode;

    @DexIgnore
    public uh5(int i) {
        this.mCode = i;
    }

    @DexIgnore
    public static List<uh5> convertBLEPermissionErrorCode(ArrayList<Integer> arrayList) {
        i14.l(arrayList);
        ArrayList arrayList2 = new ArrayList();
        Iterator<Integer> it = arrayList.iterator();
        while (it.hasNext()) {
            int intValue = it.next().intValue();
            if (intValue == 1101) {
                arrayList2.add(BLUETOOTH_OFF);
            } else if (intValue == 1112) {
                arrayList2.add(LOCATION_SERVICE_OFF);
            } else if (intValue == 1113) {
                arrayList2.add(LOCATION_PERMISSION_OFF);
            }
        }
        return arrayList2;
    }

    @DexIgnore
    public int getCode() {
        return this.mCode;
    }
}
