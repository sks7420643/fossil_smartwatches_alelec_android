package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class z64 implements d74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f4422a;

    @DexIgnore
    public z64(Object obj) {
        this.f4422a = obj;
    }

    @DexIgnore
    public static d74 b(Object obj) {
        return new z64(obj);
    }

    @DexIgnore
    @Override // com.fossil.d74
    public Object a(b74 b74) {
        Object obj = this.f4422a;
        a74.l(obj, b74);
        return obj;
    }
}
