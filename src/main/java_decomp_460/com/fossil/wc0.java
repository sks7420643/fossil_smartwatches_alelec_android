package com.fossil;

import com.fossil.v18;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wc0 extends rb0 implements Interceptor {
    @DexIgnore
    public wc0(ft1 ft1) {
        super(ft1);
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        boolean z = true;
        v18.a h = chain.c().h();
        String str = (String) fu7.b(null, new vc0(this, null), 1, null);
        if (str != null && !vt7.l(str)) {
            z = false;
        }
        if (!z) {
            h.a("Authorization", "Bearer " + str);
        }
        Response d = chain.d(h.b());
        pq7.b(d, "chain.proceed(requestBuilder.build())");
        return d;
    }
}
