package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y53 implements xw2<x53> {
    @DexIgnore
    public static y53 c; // = new y53();
    @DexIgnore
    public /* final */ xw2<x53> b;

    @DexIgnore
    public y53() {
        this(ww2.b(new a63()));
    }

    @DexIgnore
    public y53(xw2<x53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((x53) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ x53 zza() {
        return this.b.zza();
    }
}
