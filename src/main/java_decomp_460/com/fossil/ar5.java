package com.fossil;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ar5 implements SensorEventListener {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ d f310a; // = new d();
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public SensorManager c;
    @DexIgnore
    public Sensor d;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public long f311a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public b c;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public b f312a;

        @DexIgnore
        public b a() {
            b bVar = this.f312a;
            if (bVar == null) {
                return new b();
            }
            this.f312a = bVar.c;
            return bVar;
        }

        @DexIgnore
        public void b(b bVar) {
            bVar.c = this.f312a;
            this.f312a = bVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ c f313a; // = new c();
        @DexIgnore
        public b b;
        @DexIgnore
        public b c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a(long j, boolean z) {
            d(j - 500000000);
            b a2 = this.f313a.a();
            a2.f311a = j;
            a2.b = z;
            a2.c = null;
            b bVar = this.c;
            if (bVar != null) {
                bVar.c = a2;
            }
            this.c = a2;
            if (this.b == null) {
                this.b = a2;
            }
            this.d++;
            if (z) {
                this.e++;
            }
        }

        @DexIgnore
        public void b() {
            while (true) {
                b bVar = this.b;
                if (bVar != null) {
                    this.b = bVar.c;
                    this.f313a.b(bVar);
                } else {
                    this.c = null;
                    this.d = 0;
                    this.e = 0;
                    return;
                }
            }
        }

        @DexIgnore
        public boolean c() {
            b bVar;
            b bVar2 = this.c;
            if (!(bVar2 == null || (bVar = this.b) == null || bVar2.f311a - bVar.f311a < 250000000)) {
                int i = this.e;
                int i2 = this.d;
                if (i >= (i2 >> 2) + (i2 >> 1)) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public void d(long j) {
            b bVar;
            while (this.d >= 4 && (bVar = this.b) != null && j - bVar.f311a > 0) {
                if (bVar.b) {
                    this.e--;
                }
                this.d--;
                b bVar2 = bVar.c;
                this.b = bVar2;
                if (bVar2 == null) {
                    this.c = null;
                }
                this.f313a.b(bVar);
            }
        }
    }

    @DexIgnore
    public ar5(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public final boolean a(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        float f = fArr[0];
        float f2 = fArr[1];
        float f3 = fArr[2];
        return Math.sqrt((double) ((f3 * f3) + ((f * f) + (f2 * f2)))) > 13.0d;
    }

    @DexIgnore
    public boolean b(SensorManager sensorManager) {
        if (this.d != null) {
            return true;
        }
        Sensor defaultSensor = sensorManager.getDefaultSensor(1);
        this.d = defaultSensor;
        if (defaultSensor != null) {
            this.c = sensorManager;
            sensorManager.registerListener(this, defaultSensor, 0);
        }
        return this.d != null;
    }

    @DexIgnore
    public void c() {
        Sensor sensor = this.d;
        if (sensor != null) {
            this.c.unregisterListener(this, sensor);
            this.c = null;
            this.d = null;
        }
    }

    @DexIgnore
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @DexIgnore
    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean a2 = a(sensorEvent);
        this.f310a.a(sensorEvent.timestamp, a2);
        if (this.f310a.c()) {
            this.f310a.b();
            this.b.a();
        }
    }
}
