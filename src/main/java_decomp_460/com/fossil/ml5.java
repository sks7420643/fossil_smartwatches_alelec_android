package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ml5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ml5 f2399a; // = new ml5();

    @DexIgnore
    public final String a(Integer num) {
        String c = dl5.c(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        pq7.b(c, "NumberHelper.decimalForm\u2026Time?.toFloat() ?: 0F, 0)");
        return c;
    }

    @DexIgnore
    public final String b(Float f) {
        String c = dl5.c(f != null ? f.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        pq7.b(c, "NumberHelper.decimalForm\u2026Number(calories ?: 0F, 0)");
        return c;
    }

    @DexIgnore
    public final String c(Float f, ai5 ai5) {
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        if (ai5 == ai5.IMPERIAL) {
            if (f != null) {
                f2 = f.floatValue();
            }
            String c = dl5.c(jk5.l(f2), 1);
            pq7.b(c, "NumberHelper.decimalForm\u2026Miles(distance ?: 0F), 1)");
            return c;
        }
        if (f != null) {
            f2 = f.floatValue();
        }
        String c2 = dl5.c(jk5.k(f2), 1);
        pq7.b(c2, "NumberHelper.decimalForm\u2026eters(distance ?: 0F), 1)");
        return c2;
    }

    @DexIgnore
    public final String d(Integer num) {
        String c = dl5.c(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2);
        pq7.b(c, "NumberHelper.decimalForm\u2026teps?.toFloat() ?: 0F, 2)");
        return c;
    }
}
