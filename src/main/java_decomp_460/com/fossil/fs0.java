package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fs0 extends Service implements LifecycleOwner {
    @DexIgnore
    public /* final */ rs0 b; // = new rs0(this);

    @DexIgnore
    @Override // androidx.lifecycle.LifecycleOwner
    public Lifecycle getLifecycle() {
        return this.b.a();
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        this.b.b();
        return null;
    }

    @DexIgnore
    public void onCreate() {
        this.b.c();
        super.onCreate();
    }

    @DexIgnore
    public void onDestroy() {
        this.b.d();
        super.onDestroy();
    }

    @DexIgnore
    public void onStart(Intent intent, int i) {
        this.b.e();
        super.onStart(intent, i);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        return super.onStartCommand(intent, i, i2);
    }
}
