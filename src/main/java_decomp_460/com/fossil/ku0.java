package com.fossil;

import com.fossil.au0;
import com.fossil.xt0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ku0<K, A, B> extends au0<K, B> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ au0<K, A> f2079a;
    @DexIgnore
    public /* final */ gi0<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends au0.c<K, A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au0.c f2080a;

        @DexIgnore
        public a(au0.c cVar) {
            this.f2080a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.au0.c
        public void a(List<A> list, K k, K k2) {
            this.f2080a.a(xt0.convert(ku0.this.b, list), k, k2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends au0.a<K, A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au0.a f2081a;

        @DexIgnore
        public b(au0.a aVar) {
            this.f2081a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.au0.a
        public void a(List<A> list, K k) {
            this.f2081a.a(xt0.convert(ku0.this.b, list), k);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends au0.a<K, A> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au0.a f2082a;

        @DexIgnore
        public c(au0.a aVar) {
            this.f2082a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.au0.a
        public void a(List<A> list, K k) {
            this.f2082a.a(xt0.convert(ku0.this.b, list), k);
        }
    }

    @DexIgnore
    public ku0(au0<K, A> au0, gi0<List<A>, List<B>> gi0) {
        this.f2079a = au0;
        this.b = gi0;
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void addInvalidatedCallback(xt0.c cVar) {
        this.f2079a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void invalidate() {
        this.f2079a.invalidate();
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public boolean isInvalid() {
        return this.f2079a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadAfter(au0.f<K> fVar, au0.a<K, B> aVar) {
        this.f2079a.loadAfter(fVar, new c(aVar));
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadBefore(au0.f<K> fVar, au0.a<K, B> aVar) {
        this.f2079a.loadBefore(fVar, new b(aVar));
    }

    @DexIgnore
    @Override // com.fossil.au0
    public void loadInitial(au0.e<K> eVar, au0.c<K, B> cVar) {
        this.f2079a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public void removeInvalidatedCallback(xt0.c cVar) {
        this.f2079a.removeInvalidatedCallback(cVar);
    }
}
