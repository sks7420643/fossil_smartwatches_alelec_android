package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.t47;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d66 extends pv5 implements c66, t47.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public b66 g;
    @DexIgnore
    public g37<j95> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return d66.j;
        }

        @DexIgnore
        public final d66 b() {
            return new d66();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d66 b;

        @DexIgnore
        public b(d66 d66) {
            this.b = d66;
        }

        @DexIgnore
        public final void onClick(View view) {
            d66.K6(this.b).q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d66 b;

        @DexIgnore
        public c(d66 d66) {
            this.b = d66;
        }

        @DexIgnore
        public final void onClick(View view) {
            d66.K6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d66 b;

        @DexIgnore
        public d(d66 d66) {
            this.b = d66;
        }

        @DexIgnore
        public final void onClick(View view) {
            d66.K6(this.b).p();
        }
    }

    /*
    static {
        String simpleName = d66.class.getSimpleName();
        pq7.b(simpleName, "NotificationHybridEveryo\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ b66 K6(d66 d66) {
        b66 b66 = d66.g;
        if (b66 != null) {
            return b66;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.v78.a
    public void C4(int i2, List<String> list) {
        pq7.c(list, "perms");
        FLogger.INSTANCE.getLocal().d(j, ".Inside onPermissionsGranted");
        b66 b66 = this.g;
        if (b66 != null) {
            b66.s();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        b66 b66 = this.g;
        if (b66 != null) {
            b66.q();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c66
    public void I(ArrayList<j06> arrayList) {
        pq7.c(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(b66 b66) {
        pq7.c(b66, "presenter");
        this.g = b66;
    }

    @DexIgnore
    @Override // com.fossil.c66
    public void N0(j06 j06) {
        pq7.c(j06, "contactWrapper");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            int currentHandGroup = j06.getCurrentHandGroup();
            b66 b66 = this.g;
            if (b66 != null) {
                s37.b0(childFragmentManager, j06, currentHandGroup, b66.n());
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        j06 j06;
        pq7.c(str, "tag");
        if (str.hashCode() == 1018078562 && str.equals("CONFIRM_REASSIGN_CONTACT") && i2 == 2131363373) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (j06 = (j06) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                b66 b66 = this.g;
                if (b66 != null) {
                    b66.r(j06);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.c66
    public void V3(List<j06> list, int i2) {
        FlexibleCheckBox flexibleCheckBox;
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        FlexibleCheckBox flexibleCheckBox2;
        FlexibleTextView flexibleTextView3;
        FlexibleTextView flexibleTextView4;
        FlexibleCheckBox flexibleCheckBox3;
        FlexibleTextView flexibleTextView5;
        FlexibleTextView flexibleTextView6;
        FlexibleCheckBox flexibleCheckBox4;
        FlexibleTextView flexibleTextView7;
        FlexibleTextView flexibleTextView8;
        FlexibleCheckBox flexibleCheckBox5;
        FlexibleTextView flexibleTextView9;
        FlexibleTextView flexibleTextView10;
        FlexibleCheckBox flexibleCheckBox6;
        FlexibleTextView flexibleTextView11;
        FlexibleTextView flexibleTextView12;
        pq7.c(list, "listContactWrapper");
        boolean z = false;
        boolean z2 = false;
        for (T t : list) {
            Contact contact = t.getContact();
            if (contact == null || contact.getContactId() != -100) {
                Contact contact2 = t.getContact();
                if (contact2 != null && contact2.getContactId() == -200) {
                    if (t.getCurrentHandGroup() != i2) {
                        g37<j95> g37 = this.h;
                        if (g37 != null) {
                            j95 a2 = g37.a();
                            if (!(a2 == null || (flexibleTextView8 = a2.v) == null)) {
                                hr7 hr7 = hr7.f1520a;
                                String c2 = um5.c(PortfolioApp.h0.c(), 2131886157);
                                pq7.b(c2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(t.getCurrentHandGroup())}, 1));
                                pq7.b(format, "java.lang.String.format(format, *args)");
                                flexibleTextView8.setText(format);
                            }
                            g37<j95> g372 = this.h;
                            if (g372 != null) {
                                j95 a3 = g372.a();
                                if (!(a3 == null || (flexibleTextView7 = a3.v) == null)) {
                                    flexibleTextView7.setVisibility(0);
                                }
                                g37<j95> g373 = this.h;
                                if (g373 != null) {
                                    j95 a4 = g373.a();
                                    if (!(a4 == null || (flexibleCheckBox4 = a4.r) == null)) {
                                        flexibleCheckBox4.setChecked(false);
                                    }
                                } else {
                                    pq7.n("mBinding");
                                    throw null;
                                }
                            } else {
                                pq7.n("mBinding");
                                throw null;
                            }
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    } else {
                        g37<j95> g374 = this.h;
                        if (g374 != null) {
                            j95 a5 = g374.a();
                            if (!(a5 == null || (flexibleTextView6 = a5.v) == null)) {
                                flexibleTextView6.setText("");
                            }
                            g37<j95> g375 = this.h;
                            if (g375 != null) {
                                j95 a6 = g375.a();
                                if (!(a6 == null || (flexibleTextView5 = a6.v) == null)) {
                                    flexibleTextView5.setVisibility(8);
                                }
                                g37<j95> g376 = this.h;
                                if (g376 != null) {
                                    j95 a7 = g376.a();
                                    if (!(a7 == null || (flexibleCheckBox3 = a7.r) == null)) {
                                        flexibleCheckBox3.setChecked(true);
                                    }
                                } else {
                                    pq7.n("mBinding");
                                    throw null;
                                }
                            } else {
                                pq7.n("mBinding");
                                throw null;
                            }
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    }
                    z = true;
                }
            } else {
                if (t.getCurrentHandGroup() != i2) {
                    g37<j95> g377 = this.h;
                    if (g377 != null) {
                        j95 a8 = g377.a();
                        if (!(a8 == null || (flexibleTextView12 = a8.u) == null)) {
                            hr7 hr72 = hr7.f1520a;
                            String c3 = um5.c(PortfolioApp.h0.c(), 2131886157);
                            pq7.b(c3, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                            String format2 = String.format(c3, Arrays.copyOf(new Object[]{Integer.valueOf(t.getCurrentHandGroup())}, 1));
                            pq7.b(format2, "java.lang.String.format(format, *args)");
                            flexibleTextView12.setText(format2);
                        }
                        g37<j95> g378 = this.h;
                        if (g378 != null) {
                            j95 a9 = g378.a();
                            if (!(a9 == null || (flexibleTextView11 = a9.u) == null)) {
                                flexibleTextView11.setVisibility(0);
                            }
                            g37<j95> g379 = this.h;
                            if (g379 != null) {
                                j95 a10 = g379.a();
                                if (!(a10 == null || (flexibleCheckBox6 = a10.q) == null)) {
                                    flexibleCheckBox6.setChecked(false);
                                }
                            } else {
                                pq7.n("mBinding");
                                throw null;
                            }
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                } else {
                    g37<j95> g3710 = this.h;
                    if (g3710 != null) {
                        j95 a11 = g3710.a();
                        if (!(a11 == null || (flexibleTextView10 = a11.u) == null)) {
                            flexibleTextView10.setText("");
                        }
                        g37<j95> g3711 = this.h;
                        if (g3711 != null) {
                            j95 a12 = g3711.a();
                            if (!(a12 == null || (flexibleTextView9 = a12.u) == null)) {
                                flexibleTextView9.setVisibility(8);
                            }
                            g37<j95> g3712 = this.h;
                            if (g3712 != null) {
                                j95 a13 = g3712.a();
                                if (!(a13 == null || (flexibleCheckBox5 = a13.q) == null)) {
                                    flexibleCheckBox5.setChecked(true);
                                }
                            } else {
                                pq7.n("mBinding");
                                throw null;
                            }
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                }
                z2 = true;
            }
        }
        if (!z2) {
            g37<j95> g3713 = this.h;
            if (g3713 != null) {
                j95 a14 = g3713.a();
                if (!(a14 == null || (flexibleTextView4 = a14.u) == null)) {
                    flexibleTextView4.setText("");
                }
                g37<j95> g3714 = this.h;
                if (g3714 != null) {
                    j95 a15 = g3714.a();
                    if (!(a15 == null || (flexibleTextView3 = a15.u) == null)) {
                        flexibleTextView3.setVisibility(8);
                    }
                    g37<j95> g3715 = this.h;
                    if (g3715 != null) {
                        j95 a16 = g3715.a();
                        if (!(a16 == null || (flexibleCheckBox2 = a16.q) == null)) {
                            flexibleCheckBox2.setChecked(false);
                        }
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        if (!z) {
            g37<j95> g3716 = this.h;
            if (g3716 != null) {
                j95 a17 = g3716.a();
                if (!(a17 == null || (flexibleTextView2 = a17.v) == null)) {
                    flexibleTextView2.setText("");
                }
                g37<j95> g3717 = this.h;
                if (g3717 != null) {
                    j95 a18 = g3717.a();
                    if (!(a18 == null || (flexibleTextView = a18.v) == null)) {
                        flexibleTextView.setVisibility(8);
                    }
                    g37<j95> g3718 = this.h;
                    if (g3718 != null) {
                        j95 a19 = g3718.a();
                        if (!(a19 == null || (flexibleCheckBox = a19.r) == null)) {
                            flexibleCheckBox.setChecked(false);
                            return;
                        }
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.v78.a
    public void k1(int i2, List<String> list) {
        pq7.c(list, "perms");
        FLogger.INSTANCE.getLocal().d(j, ".Inside onPermissionsDenied");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "Permission Denied : " + it.next());
        }
        if (v78.f(this, list) && isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.h0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        j95 j95 = (j95) aq0.f(layoutInflater, 2131558595, viewGroup, false, A6());
        j95.x.setOnClickListener(new b(this));
        j95.q.setOnClickListener(new c(this));
        j95.r.setOnClickListener(new d(this));
        String d2 = qn5.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d2)) {
            j95.y.setBackgroundColor(Color.parseColor(d2));
        }
        this.h = new g37<>(this, j95);
        pq7.b(j95, "binding");
        return j95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        b66 b66 = this.g;
        if (b66 != null) {
            b66.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        b66 b66 = this.g;
        if (b66 != null) {
            b66.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
