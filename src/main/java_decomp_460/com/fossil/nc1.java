package com.fossil;

import android.os.Process;
import com.fossil.cd1;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nc1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f2499a;
    @DexIgnore
    public /* final */ Map<mb1, d> b;
    @DexIgnore
    public /* final */ ReferenceQueue<cd1<?>> c;
    @DexIgnore
    public cd1.a d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public volatile c f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nc1$a$a")
        /* renamed from: com.fossil.nc1$a$a  reason: collision with other inner class name */
        public class RunnableC0168a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable b;

            @DexIgnore
            public RunnableC0168a(a aVar, Runnable runnable) {
                this.b = runnable;
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(10);
                this.b.run();
            }
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(new RunnableC0168a(this, runnable), "glide-active-resources");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            nc1.this.b();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WeakReference<cd1<?>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ mb1 f2500a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public id1<?> c;

        @DexIgnore
        public d(mb1 mb1, cd1<?> cd1, ReferenceQueue<? super cd1<?>> referenceQueue, boolean z) {
            super(cd1, referenceQueue);
            id1<?> id1;
            ik1.d(mb1);
            this.f2500a = mb1;
            if (!cd1.f() || !z) {
                id1 = null;
            } else {
                id1<?> e = cd1.e();
                ik1.d(e);
                id1 = e;
            }
            this.c = id1;
            this.b = cd1.f();
        }

        @DexIgnore
        public void a() {
            this.c = null;
            clear();
        }
    }

    @DexIgnore
    public nc1(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new a()));
    }

    @DexIgnore
    public nc1(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.f2499a = z;
        executor.execute(new b());
    }

    @DexIgnore
    public void a(mb1 mb1, cd1<?> cd1) {
        synchronized (this) {
            d put = this.b.put(mb1, new d(mb1, cd1, this.c, this.f2499a));
            if (put != null) {
                put.a();
            }
        }
    }

    @DexIgnore
    public void b() {
        while (!this.e) {
            try {
                c((d) this.c.remove());
                c cVar = this.f;
                if (cVar != null) {
                    cVar.a();
                }
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexIgnore
    public void c(d dVar) {
        synchronized (this) {
            this.b.remove(dVar.f2500a);
            if (dVar.b && dVar.c != null) {
                this.d.d(dVar.f2500a, new cd1<>(dVar.c, true, false, dVar.f2500a, this.d));
            }
        }
    }

    @DexIgnore
    public void d(mb1 mb1) {
        synchronized (this) {
            d remove = this.b.remove(mb1);
            if (remove != null) {
                remove.a();
            }
        }
    }

    @DexIgnore
    public cd1<?> e(mb1 mb1) {
        synchronized (this) {
            d dVar = this.b.get(mb1);
            if (dVar == null) {
                return null;
            }
            cd1<?> cd1 = (cd1) dVar.get();
            if (cd1 == null) {
                c(dVar);
            }
            return cd1;
        }
    }

    @DexIgnore
    public void f(cd1.a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.d = aVar;
            }
        }
    }
}
