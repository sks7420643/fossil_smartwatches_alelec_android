package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m25 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ScrollView f2291a;
    @DexIgnore
    public /* final */ FlexibleEditText b;
    @DexIgnore
    public /* final */ FlexibleTextView c;
    @DexIgnore
    public /* final */ FlexibleTextView d;
    @DexIgnore
    public /* final */ FlexibleTextView e;
    @DexIgnore
    public /* final */ ImageView f;
    @DexIgnore
    public /* final */ ConstraintLayout g;
    @DexIgnore
    public /* final */ ConstraintLayout h;

    @DexIgnore
    public m25(ScrollView scrollView, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2) {
        this.f2291a = scrollView;
        this.b = flexibleEditText;
        this.c = flexibleTextView;
        this.d = flexibleTextView2;
        this.e = flexibleTextView3;
        this.f = imageView;
        this.g = constraintLayout;
        this.h = constraintLayout2;
    }

    @DexIgnore
    public static m25 a(View view) {
        int i = 2131362684;
        FlexibleEditText flexibleEditText = (FlexibleEditText) view.findViewById(2131362308);
        if (flexibleEditText != null) {
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362358);
            if (flexibleTextView != null) {
                FlexibleTextView flexibleTextView2 = (FlexibleTextView) view.findViewById(2131362378);
                if (flexibleTextView2 != null) {
                    FlexibleTextView flexibleTextView3 = (FlexibleTextView) view.findViewById(2131362386);
                    if (flexibleTextView3 != null) {
                        ImageView imageView = (ImageView) view.findViewById(2131362684);
                        if (imageView != null) {
                            i = 2131363002;
                            ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(2131363002);
                            if (constraintLayout != null) {
                                i = 2131363010;
                                ConstraintLayout constraintLayout2 = (ConstraintLayout) view.findViewById(2131363010);
                                if (constraintLayout2 != null) {
                                    return new m25((ScrollView) view, flexibleEditText, flexibleTextView, flexibleTextView2, flexibleTextView3, imageView, constraintLayout, constraintLayout2);
                                }
                            }
                        }
                    } else {
                        i = 2131362386;
                    }
                } else {
                    i = 2131362378;
                }
            } else {
                i = 2131362358;
            }
        } else {
            i = 2131362308;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static m25 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558502, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ScrollView b() {
        return this.f2291a;
    }
}
