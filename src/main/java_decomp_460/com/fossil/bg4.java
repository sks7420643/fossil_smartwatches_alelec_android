package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import com.facebook.internal.FetchedAppSettings;
import com.fossil.te4;
import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bg4 {
    @DexIgnore
    public static int h;
    @DexIgnore
    public static PendingIntent i;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SimpleArrayMap<String, ot3<Bundle>> f425a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ rf4 c;
    @DexIgnore
    public /* final */ ScheduledExecutorService d;
    @DexIgnore
    public Messenger e;
    @DexIgnore
    public Messenger f;
    @DexIgnore
    public te4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends dm2 {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message message) {
            bg4.this.d(message);
        }
    }

    @DexIgnore
    public bg4(Context context, rf4 rf4) {
        this.b = context;
        this.c = rf4;
        this.e = new Messenger(new a(Looper.getMainLooper()));
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        scheduledThreadPoolExecutor.setKeepAliveTime(60, TimeUnit.SECONDS);
        scheduledThreadPoolExecutor.allowCoreThreadTimeOut(true);
        this.d = scheduledThreadPoolExecutor;
    }

    @DexIgnore
    public static boolean b(Bundle bundle) {
        return bundle != null && bundle.containsKey("google.messenger");
    }

    @DexIgnore
    public static final /* synthetic */ Bundle e(nt3 nt3) throws Exception {
        if (nt3.q()) {
            return (Bundle) nt3.m();
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(nt3.l());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
            sb.append("Error making request: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        throw new IOException("SERVICE_NOT_AVAILABLE");
    }

    @DexIgnore
    public static final /* synthetic */ void f(ot3 ot3) {
        if (ot3.d(new IOException("TIMEOUT"))) {
            Log.w("FirebaseInstanceId", "No response");
        }
    }

    @DexIgnore
    public static final /* synthetic */ nt3 h(Bundle bundle) throws Exception {
        return b(bundle) ? qt3.f(null) : qt3.f(bundle);
    }

    @DexIgnore
    public static String j() {
        String num;
        synchronized (bg4.class) {
            try {
                int i2 = h;
                h = i2 + 1;
                num = Integer.toString(i2);
            } catch (Throwable th) {
                throw th;
            }
        }
        return num;
    }

    @DexIgnore
    public static void p(Context context, Intent intent) {
        synchronized (bg4.class) {
            try {
                if (i == null) {
                    Intent intent2 = new Intent();
                    intent2.setPackage("com.google.example.invalidpackage");
                    i = PendingIntent.getBroadcast(context, 0, intent2, 0);
                }
                intent.putExtra("app", i);
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public final void c(Intent intent) {
        String action = intent.getAction();
        if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
            String stringExtra = intent.getStringExtra("registration_id");
            if (stringExtra == null) {
                stringExtra = intent.getStringExtra("unregistered");
            }
            if (stringExtra == null) {
                k(intent);
                return;
            }
            Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
            if (matcher.matches()) {
                String group = matcher.group(1);
                String group2 = matcher.group(2);
                Bundle extras = intent.getExtras();
                extras.putString("registration_id", group2);
                q(group, extras);
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(stringExtra);
                Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Unexpected response string: ".concat(valueOf) : new String("Unexpected response string: "));
            }
        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(action);
            Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Unexpected response action: ".concat(valueOf2) : new String("Unexpected response action: "));
        }
    }

    @DexIgnore
    public final void d(Message message) {
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new te4.b());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof te4) {
                        this.g = (te4) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        this.f = (Messenger) parcelableExtra;
                    }
                }
                c((Intent) message.obj);
                return;
            }
        }
        Log.w("FirebaseInstanceId", "Dropping invalid message");
    }

    @DexIgnore
    public final /* synthetic */ void g(String str, ScheduledFuture scheduledFuture, nt3 nt3) {
        synchronized (this.f425a) {
            this.f425a.remove(str);
        }
        scheduledFuture.cancel(false);
    }

    @DexIgnore
    public final /* synthetic */ nt3 i(Bundle bundle, nt3 nt3) throws Exception {
        return (nt3.q() && b((Bundle) nt3.m())) ? m(bundle).s(se4.a(), ag4.f265a) : nt3;
    }

    @DexIgnore
    public void k(Intent intent) {
        String stringExtra = intent.getStringExtra("error");
        if (stringExtra == null) {
            String valueOf = String.valueOf(intent.getExtras());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 49);
            sb.append("Unexpected response, no error or registration id ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(stringExtra);
            Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Received InstanceID error ".concat(valueOf2) : new String("Received InstanceID error "));
        }
        if (stringExtra.startsWith("|")) {
            String[] split = stringExtra.split(FetchedAppSettings.DialogFeatureConfig.DIALOG_CONFIG_DIALOG_NAME_FEATURE_NAME_SEPARATOR);
            if (split.length <= 2 || !"ID".equals(split[1])) {
                String valueOf3 = String.valueOf(stringExtra);
                Log.w("FirebaseInstanceId", valueOf3.length() != 0 ? "Unexpected structured response ".concat(valueOf3) : new String("Unexpected structured response "));
                return;
            }
            String str = split[2];
            String str2 = split[3];
            if (str2.startsWith(":")) {
                str2 = str2.substring(1);
            }
            q(str, intent.putExtra("error", str2).getExtras());
            return;
        }
        synchronized (this.f425a) {
            for (int i2 = 0; i2 < this.f425a.size(); i2++) {
                q(this.f425a.j(i2), intent.getExtras());
            }
        }
    }

    @DexIgnore
    public nt3<Bundle> l(Bundle bundle) {
        return this.c.d() >= 12000000 ? kf4.c(this.b).g(1, bundle).i(se4.a(), wf4.f3930a) : n(bundle);
    }

    @DexIgnore
    public final nt3<Bundle> m(Bundle bundle) {
        String j = j();
        ot3<Bundle> ot3 = new ot3<>();
        synchronized (this.f425a) {
            this.f425a.put(j, ot3);
        }
        r(bundle, j);
        ot3.a().c(se4.a(), new zf4(this, j, this.d.schedule(new yf4(ot3), 30, TimeUnit.SECONDS)));
        return ot3.a();
    }

    @DexIgnore
    public final nt3<Bundle> n(Bundle bundle) {
        return !this.c.g() ? qt3.e(new IOException("MISSING_INSTANCEID_SERVICE")) : m(bundle).k(se4.a(), new xf4(this, bundle));
    }

    @DexIgnore
    public void o(Intent intent, String str) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 5);
        sb.append("|ID|");
        sb.append(str);
        sb.append("|");
        intent.putExtra("kid", sb.toString());
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(intent.getExtras());
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 8);
            sb2.append("Sending ");
            sb2.append(valueOf);
            Log.d("FirebaseInstanceId", sb2.toString());
        }
        intent.putExtra("google.messenger", this.e);
        if (!(this.f == null && this.g == null)) {
            Message obtain = Message.obtain();
            obtain.obj = intent;
            try {
                if (this.f != null) {
                    this.f.send(obtain);
                    return;
                } else {
                    this.g.b(obtain);
                    return;
                }
            } catch (RemoteException e2) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "Messenger failed, fallback to startService");
                }
            }
        }
        if (this.c.e() == 2) {
            this.b.sendBroadcast(intent);
        } else {
            this.b.startService(intent);
        }
    }

    @DexIgnore
    public final void q(String str, Bundle bundle) {
        synchronized (this.f425a) {
            ot3<Bundle> remove = this.f425a.remove(str);
            if (remove == null) {
                String valueOf = String.valueOf(str);
                Log.w("FirebaseInstanceId", valueOf.length() != 0 ? "Missing callback for ".concat(valueOf) : new String("Missing callback for "));
                return;
            }
            remove.c(bundle);
        }
    }

    @DexIgnore
    public final void r(Bundle bundle, String str) {
        Intent intent = new Intent();
        intent.setPackage("com.google.android.gms");
        if (this.c.e() == 2) {
            intent.setAction("com.google.iid.TOKEN_REQUEST");
        } else {
            intent.setAction("com.google.android.c2dm.intent.REGISTER");
        }
        intent.putExtras(bundle);
        p(this.b, intent);
        o(intent, str);
    }
}
