package com.fossil;

import com.fossil.tv7;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iw7 extends jw7 implements tv7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater f; // = AtomicReferenceFieldUpdater.newUpdater(iw7.class, Object.class, "_queue");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater g; // = AtomicReferenceFieldUpdater.newUpdater(iw7.class, Object.class, "_delayed");
    @DexIgnore
    public volatile Object _delayed; // = null;
    @DexIgnore
    public volatile int _isCompleted; // = 0;
    @DexIgnore
    public volatile Object _queue; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends c {
        @DexIgnore
        public /* final */ ku7<tl7> e;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.ku7<? super com.fossil.tl7> */
        /* JADX WARN: Multi-variable type inference failed */
        public a(long j, ku7<? super tl7> ku7) {
            super(j);
            this.e = ku7;
        }

        @DexIgnore
        public void run() {
            this.e.f(iw7.this, tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.iw7.c
        public String toString() {
            return super.toString() + this.e.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c {
        @DexIgnore
        public /* final */ Runnable e;

        @DexIgnore
        public b(long j, Runnable runnable) {
            super(j);
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            this.e.run();
        }

        @DexIgnore
        @Override // com.fossil.iw7.c
        public String toString() {
            return super.toString() + this.e.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c implements Runnable, Comparable<c>, dw7, b08 {
        @DexIgnore
        public Object b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public long d;

        @DexIgnore
        public c(long j) {
            this.d = j;
        }

        @DexIgnore
        @Override // com.fossil.b08
        public int a() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.b08
        public void b(a08<?> a08) {
            if (this.b != lw7.b()) {
                this.b = a08;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @DexIgnore
        @Override // com.fossil.dw7
        public final void dispose() {
            synchronized (this) {
                Object obj = this.b;
                if (obj != lw7.b()) {
                    if (!(obj instanceof d)) {
                        obj = null;
                    }
                    d dVar = (d) obj;
                    if (dVar != null) {
                        dVar.g(this);
                    }
                    this.b = lw7.b();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.b08
        public a08<?> e() {
            Object obj = this.b;
            if (!(obj instanceof a08)) {
                obj = null;
            }
            return (a08) obj;
        }

        @DexIgnore
        @Override // com.fossil.b08
        public void f(int i) {
            this.c = i;
        }

        @DexIgnore
        /* renamed from: h */
        public int compareTo(c cVar) {
            int i = ((this.d - cVar.d) > 0 ? 1 : ((this.d - cVar.d) == 0 ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @DexIgnore
        public final int i(long j, d dVar, iw7 iw7) {
            synchronized (this) {
                if (this.b == lw7.b()) {
                    return 2;
                }
                synchronized (dVar) {
                    c cVar = (c) dVar.b();
                    if (iw7.B0()) {
                        return 1;
                    }
                    if (cVar == null) {
                        dVar.b = j;
                    } else {
                        long j2 = cVar.d;
                        if (j2 - j < 0) {
                            j = j2;
                        }
                        if (j - dVar.b > 0) {
                            dVar.b = j;
                        }
                    }
                    if (this.d - dVar.b < 0) {
                        this.d = dVar.b;
                    }
                    dVar.a(this);
                    return 0;
                }
            }
        }

        @DexIgnore
        public final boolean j(long j) {
            return j - this.d >= 0;
        }

        @DexIgnore
        public String toString() {
            return "Delayed[nanos=" + this.d + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends a08<c> {
        @DexIgnore
        public long b;

        @DexIgnore
        public d(long j) {
            this.b = j;
        }
    }

    @DexIgnore
    public final boolean A0(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (B0()) {
                return false;
            }
            if (obj == null) {
                if (f.compareAndSet(this, null, runnable)) {
                    return true;
                }
            } else if (obj instanceof nz7) {
                if (obj != null) {
                    nz7 nz7 = (nz7) obj;
                    int a2 = nz7.a(runnable);
                    if (a2 == 0) {
                        return true;
                    }
                    if (a2 == 1) {
                        f.compareAndSet(this, obj, nz7.i());
                    } else if (a2 == 2) {
                        return false;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == lw7.a()) {
                return false;
            } else {
                nz7 nz72 = new nz7(8, true);
                if (obj != null) {
                    nz72.a((Runnable) obj);
                    nz72.a(runnable);
                    if (f.compareAndSet(this, obj, nz72)) {
                        return true;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean B0() {
        /*
            r1 = this;
            int r0 = r1._isCompleted
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw7.B0():boolean");
    }

    @DexIgnore
    public boolean C0() {
        boolean z;
        if (!p0()) {
            return false;
        }
        d dVar = (d) this._delayed;
        if (dVar != null && !dVar.d()) {
            return false;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof nz7) {
                z = ((nz7) obj).g();
            } else if (obj != lw7.a()) {
                z = false;
            }
            return z;
        }
        z = true;
        return z;
    }

    @DexIgnore
    public final void D0() {
        c cVar;
        xx7 a2 = yx7.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        while (true) {
            d dVar = (d) this._delayed;
            if (dVar != null && (cVar = (c) dVar.i()) != null) {
                u0(a3, cVar);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void E0() {
        this._queue = null;
        this._delayed = null;
    }

    @DexIgnore
    public final void F0(long j, c cVar) {
        int G0 = G0(j, cVar);
        if (G0 != 0) {
            if (G0 == 1) {
                u0(j, cVar);
            } else if (G0 != 2) {
                throw new IllegalStateException("unexpected result".toString());
            }
        } else if (J0(cVar)) {
            v0();
        }
    }

    @DexIgnore
    @Override // com.fossil.tv7
    public dw7 G(long j, Runnable runnable) {
        return tv7.a.a(this, j, runnable);
    }

    @DexIgnore
    public final int G0(long j, c cVar) {
        if (B0()) {
            return 1;
        }
        d dVar = (d) this._delayed;
        if (dVar == null) {
            g.compareAndSet(this, null, new d(j));
            Object obj = this._delayed;
            if (obj != null) {
                dVar = (d) obj;
            } else {
                pq7.i();
                throw null;
            }
        }
        return cVar.i(j, dVar, this);
    }

    @DexIgnore
    public final dw7 H0(long j, Runnable runnable) {
        long c2 = lw7.c(j);
        if (c2 >= 4611686018427387903L) {
            return lx7.b;
        }
        xx7 a2 = yx7.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        b bVar = new b(c2 + a3, runnable);
        F0(a3, bVar);
        return bVar;
    }

    @DexIgnore
    public final void I0(boolean z) {
        this._isCompleted = z ? 1 : 0;
    }

    @DexIgnore
    public final boolean J0(c cVar) {
        d dVar = (d) this._delayed;
        return (dVar != null ? (c) dVar.e() : null) == cVar;
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public final void M(tn7 tn7, Runnable runnable) {
        z0(runnable);
    }

    @DexIgnore
    @Override // com.fossil.hw7
    public long b0() {
        c cVar;
        if (super.b0() == 0) {
            return 0;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (!(obj instanceof nz7)) {
                return obj == lw7.a() ? Long.MAX_VALUE : 0;
            }
            if (!((nz7) obj).g()) {
                return 0;
            }
        }
        d dVar = (d) this._delayed;
        if (dVar == null || (cVar = (c) dVar.e()) == null) {
            return Long.MAX_VALUE;
        }
        long j = cVar.d;
        xx7 a2 = yx7.a();
        return bs7.e(j - (a2 != null ? a2.a() : System.nanoTime()), 0);
    }

    @DexIgnore
    @Override // com.fossil.tv7
    public void f(long j, ku7<? super tl7> ku7) {
        long c2 = lw7.c(j);
        if (c2 < 4611686018427387903L) {
            xx7 a2 = yx7.a();
            long a3 = a2 != null ? a2.a() : System.nanoTime();
            a aVar = new a(c2 + a3, ku7);
            nu7.a(ku7, aVar);
            F0(a3, aVar);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    @Override // com.fossil.hw7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long q0() {
        /*
            r7 = this;
            r2 = 0
            r3 = 0
            boolean r0 = r7.r0()
            if (r0 == 0) goto L_0x000d
            long r0 = r7.b0()
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.Object r0 = r7._delayed
            com.fossil.iw7$d r0 = (com.fossil.iw7.d) r0
            if (r0 == 0) goto L_0x0042
            boolean r1 = r0.d()
            if (r1 != 0) goto L_0x0042
            com.fossil.xx7 r1 = com.fossil.yx7.a()
            if (r1 == 0) goto L_0x0050
            long r4 = r1.a()
        L_0x0023:
            monitor-enter(r0)
            com.fossil.b08 r1 = r0.b()     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x0057
            com.fossil.iw7$c r1 = (com.fossil.iw7.c) r1     // Catch:{ all -> 0x005a }
            boolean r6 = r1.j(r4)     // Catch:{ all -> 0x005a }
            if (r6 == 0) goto L_0x0055
            boolean r1 = r7.A0(r1)     // Catch:{ all -> 0x005a }
        L_0x0036:
            if (r1 == 0) goto L_0x005d
            r1 = 0
            com.fossil.b08 r1 = r0.h(r1)     // Catch:{ all -> 0x005a }
        L_0x003d:
            monitor-exit(r0)
        L_0x003e:
            com.fossil.iw7$c r1 = (com.fossil.iw7.c) r1
            if (r1 != 0) goto L_0x0023
        L_0x0042:
            java.lang.Runnable r0 = r7.y0()
            if (r0 == 0) goto L_0x004b
            r0.run()
        L_0x004b:
            long r0 = r7.b0()
            goto L_0x000c
        L_0x0050:
            long r4 = java.lang.System.nanoTime()
            goto L_0x0023
        L_0x0055:
            r1 = r3
            goto L_0x0036
        L_0x0057:
            monitor-exit(r0)
            r1 = r2
            goto L_0x003e
        L_0x005a:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x005d:
            r1 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw7.q0():long");
    }

    @DexIgnore
    @Override // com.fossil.hw7
    public void shutdown() {
        wx7.b.c();
        I0(true);
        x0();
        do {
        } while (q0() <= 0);
        D0();
    }

    @DexIgnore
    public final void x0() {
        if (!nv7.a() || B0()) {
            while (true) {
                Object obj = this._queue;
                if (obj == null) {
                    if (f.compareAndSet(this, null, lw7.a())) {
                        return;
                    }
                } else if (obj instanceof nz7) {
                    ((nz7) obj).d();
                    return;
                } else if (obj != lw7.a()) {
                    nz7 nz7 = new nz7(8, true);
                    if (obj != null) {
                        nz7.a((Runnable) obj);
                        if (f.compareAndSet(this, obj, nz7)) {
                            return;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Runnable y0() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof nz7) {
                if (obj != null) {
                    nz7 nz7 = (nz7) obj;
                    Object j = nz7.j();
                    if (j != nz7.g) {
                        return (Runnable) j;
                    }
                    f.compareAndSet(this, obj, nz7.i());
                } else {
                    throw new il7("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == lw7.a()) {
                return null;
            } else {
                if (f.compareAndSet(this, obj, null)) {
                    if (obj != null) {
                        return (Runnable) obj;
                    }
                    throw new il7("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public final void z0(Runnable runnable) {
        if (A0(runnable)) {
            v0();
        } else {
            pv7.i.z0(runnable);
        }
    }
}
