package com.fossil;

import com.facebook.LegacyTokenHelper;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v48 implements j48 {
    @DexIgnore
    public /* final */ i48 b; // = new i48();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ a58 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends OutputStream {
        @DexIgnore
        public /* final */ /* synthetic */ v48 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(v48 v48) {
            this.b = v48;
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.b.close();
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() {
            v48 v48 = this.b;
            if (!v48.c) {
                v48.flush();
            }
        }

        @DexIgnore
        public String toString() {
            return this.b + ".outputStream()";
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(int i) {
            v48 v48 = this.b;
            if (!v48.c) {
                v48.b.w0((byte) i);
                this.b.x();
                return;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) {
            pq7.c(bArr, "data");
            v48 v48 = this.b;
            if (!v48.c) {
                v48.b.v0(bArr, i, i2);
                this.b.x();
                return;
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public v48(a58 a58) {
        pq7.c(a58, "sink");
        this.d = a58;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 E(String str) {
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        if (!this.c) {
            this.b.D0(str);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 J(byte[] bArr, int i, int i2) {
        pq7.c(bArr, "source");
        if (!this.c) {
            this.b.v0(bArr, i, i2);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.a58
    public void K(i48 i48, long j) {
        pq7.c(i48, "source");
        if (!this.c) {
            this.b.K(i48, j);
            x();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public long N(c58 c58) {
        pq7.c(c58, "source");
        long j = 0;
        while (true) {
            long d0 = c58.d0(this.b, (long) 8192);
            if (d0 == -1) {
                return j;
            }
            j += d0;
            x();
        }
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 O(long j) {
        if (!this.c) {
            this.b.y0(j);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 Z(byte[] bArr) {
        pq7.c(bArr, "source");
        if (!this.c) {
            this.b.u0(bArr);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 a0(l48 l48) {
        pq7.c(l48, "byteString");
        if (!this.c) {
            this.b.t0(l48);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.a58, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (!this.c) {
            Throwable th = null;
            try {
                if (this.b.p0() > 0) {
                    this.d.K(this.b, this.b.p0());
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.d.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.c = true;
            if (th != null) {
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.j48
    public i48 d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.a58
    public d58 e() {
        return this.d.e();
    }

    @DexIgnore
    @Override // com.fossil.j48, com.fossil.a58, java.io.Flushable
    public void flush() {
        if (!this.c) {
            if (this.b.p0() > 0) {
                a58 a58 = this.d;
                i48 i48 = this.b;
                a58.K(i48, i48.p0());
            }
            this.d.flush();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.c;
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 k0(long j) {
        if (!this.c) {
            this.b.x0(j);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public OutputStream l0() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 n(int i) {
        if (!this.c) {
            this.b.A0(i);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 p(int i) {
        if (!this.c) {
            this.b.z0(i);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.d + ')';
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 v(int i) {
        if (!this.c) {
            this.b.w0(i);
            x();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) {
        pq7.c(byteBuffer, "source");
        if (!this.c) {
            int write = this.b.write(byteBuffer);
            x();
            return write;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.j48
    public j48 x() {
        if (!this.c) {
            long o = this.b.o();
            if (o > 0) {
                this.d.K(this.b, o);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }
}
