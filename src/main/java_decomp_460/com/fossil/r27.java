package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r27 implements Factory<q27> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f3074a;

    @DexIgnore
    public r27(Provider<on5> provider) {
        this.f3074a = provider;
    }

    @DexIgnore
    public static r27 a(Provider<on5> provider) {
        return new r27(provider);
    }

    @DexIgnore
    public static q27 c(on5 on5) {
        return new q27(on5);
    }

    @DexIgnore
    /* renamed from: b */
    public q27 get() {
        return c(this.f3074a.get());
    }
}
