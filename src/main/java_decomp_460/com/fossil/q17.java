package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q17 implements Factory<o17> {
    @DexIgnore
    public static o17 a(PortfolioApp portfolioApp, l17 l17, UserRepository userRepository, ct0 ct0, pl5 pl5) {
        return new o17(portfolioApp, l17, userRepository, ct0, pl5);
    }
}
