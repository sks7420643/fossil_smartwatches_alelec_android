package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i93 implements j93 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1595a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true);

    @DexIgnore
    @Override // com.fossil.j93
    public final boolean zza() {
        return f1595a.o().booleanValue();
    }
}
