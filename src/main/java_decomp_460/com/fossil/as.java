package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class as extends ss {
    @DexIgnore
    public long A;
    @DexIgnore
    public f5 B; // = f5.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean C;

    @DexIgnore
    public as(k5 k5Var, boolean z, long j) {
        super(hs.c, k5Var);
        this.C = z;
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.B0, ey1.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new y5(this.C, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void g(u5 u5Var) {
        this.B = ((y5) u5Var).l;
        this.g.add(new hw(0, null, null, g80.k(new JSONObject(), jd0.B0, ey1.a(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void i(p7 p7Var) {
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(g80.k(g80.k(g80.k(super.z(), jd0.o1, ey1.a(this.y.H())), jd0.f5, Integer.valueOf(this.y.A.getType())), jd0.f1, Boolean.valueOf(this.C)), jd0.s1, ey1.a(this.y.w)), jd0.k0, this.y.x);
    }
}
