package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j23 {
    @DexIgnore
    Object a(Object obj);

    @DexIgnore
    Object b(Object obj);

    @DexIgnore
    int zza(int i, Object obj, Object obj2);

    @DexIgnore
    Object zza(Object obj, Object obj2);

    @DexIgnore
    Map<?, ?> zza(Object obj);

    @DexIgnore
    h23<?, ?> zzb(Object obj);

    @DexIgnore
    Map<?, ?> zzc(Object obj);

    @DexIgnore
    boolean zzd(Object obj);
}
