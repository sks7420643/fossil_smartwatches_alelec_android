package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.FitnessData;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rq extends lp {
    @DexIgnore
    public /* final */ boolean C; // = true;
    @DexIgnore
    public FitnessData[] D; // = new FitnessData[0];
    @DexIgnore
    public byte[][] E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public float I; // = 0.5f;
    @DexIgnore
    public float J; // = 0.5f;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ int N;
    @DexIgnore
    public /* final */ ArrayList<ow> O;
    @DexIgnore
    public /* final */ nm1 P;
    @DexIgnore
    public /* final */ HashMap<hu1, Object> Q;

    @DexIgnore
    public rq(k5 k5Var, i60 i60, nm1 nm1, HashMap<hu1, Object> hashMap) {
        super(k5Var, i60, yp.o0, null, false, 24);
        this.P = nm1;
        this.Q = hashMap;
        Boolean bool = (Boolean) hashMap.get(hu1.SKIP_LIST);
        this.K = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) this.Q.get(hu1.SKIP_ERASE);
        this.L = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) this.Q.get(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.M = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) this.Q.get(hu1.NUMBER_OF_FILE_REQUIRED);
        this.N = num != null ? num.intValue() : 0;
        this.O = by1.a(this.i, hm7.c(ow.FILE_CONFIG, ow.TRANSFER_DATA));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        if (q3.f.f(this.x.a())) {
            this.I = 1.0f;
            this.J = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            J();
            return;
        }
        lp.i(this, new iv(ke.b.a(this.w.x, ob.ACTIVITY_FILE), this.w, 0, 4), new jl(this), new vl(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        JSONObject put = super.C().put(zm1.BIOMETRIC_PROFILE.b(), this.P.c()).put(ey1.a(hu1.SKIP_LIST), this.K).put(ey1.a(hu1.SKIP_ERASE), this.L).put(ey1.a(hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.M).put(ey1.a(hu1.NUMBER_OF_FILE_REQUIRED), this.N);
        pq7.b(put, "super.optionDescription(\u2026me, numberOfFileRequired)");
        return put;
    }

    @DexIgnore
    public final void I() {
        lp.i(this, new iv(ke.b.a(this.w.x, ob.HARDWARE_LOG), this.w, 0, 4), new im(this), new um(this), null, new gn(this), null, 40, null);
    }

    @DexIgnore
    public final void J() {
        lp.h(this, new mi(this.w, this.x, this.P, this.Q, this.z, null, 32), new dp(this), new qp(this), new dq(this), null, null, 48, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public boolean t() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.O;
    }
}
