package com.fossil;

import com.fossil.a34;
import com.fossil.y24;
import java.io.Serializable;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.security.AccessControlException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f64 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ b14<Type, String> f1057a; // = new a();
    @DexIgnore
    public static /* final */ d14 b; // = d14.i(", ").k("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b14<Type, String> {
        @DexIgnore
        /* renamed from: a */
        public String apply(Type type) {
            return e.CURRENT.typeName(type);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends e64 {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicReference b;

        @DexIgnore
        public b(AtomicReference atomicReference) {
            this.b = atomicReference;
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void b(Class<?> cls) {
            this.b.set(cls.getComponentType());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void c(GenericArrayType genericArrayType) {
            this.b.set(genericArrayType.getGenericComponentType());
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void e(TypeVariable<?> typeVariable) {
            this.b.set(f64.q(typeVariable.getBounds()));
        }

        @DexIgnore
        @Override // com.fossil.e64
        public void f(WildcardType wildcardType) {
            this.b.set(f64.q(wildcardType.getUpperBounds()));
        }
    }

    @DexIgnore
    public enum c {
        OWNED_BY_ENCLOSING_CLASS {
            @DexIgnore
            @Override // com.fossil.f64.c
            public Class<?> getOwnerType(Class<?> cls) {
                return cls.getEnclosingClass();
            }
        },
        LOCAL_CLASS_HAS_NO_OWNER {
            @DexIgnore
            @Override // com.fossil.f64.c
            public Class<?> getOwnerType(Class<?> cls) {
                if (cls.isLocalClass()) {
                    return null;
                }
                return cls.getEnclosingClass();
            }
        };
        
        @DexIgnore
        public static /* final */ c JVM_BEHAVIOR; // = a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b<T> {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends b<String> {
        }

        @DexIgnore
        public /* synthetic */ c(a aVar) {
            this();
        }

        @DexIgnore
        public static c a() {
            ParameterizedType parameterizedType = (ParameterizedType) d.class.getGenericSuperclass();
            c[] values = values();
            for (c cVar : values) {
                if (cVar.getOwnerType(b.class) == parameterizedType.getOwnerType()) {
                    return cVar;
                }
            }
            throw new AssertionError();
        }

        @DexIgnore
        public abstract Class<?> getOwnerType(Class<?> cls);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements GenericArrayType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Type componentType;

        @DexIgnore
        public d(Type type) {
            this.componentType = e.CURRENT.usedInGenericType(type);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof GenericArrayType) {
                return f14.a(getGenericComponentType(), ((GenericArrayType) obj).getGenericComponentType());
            }
            return false;
        }

        @DexIgnore
        public Type getGenericComponentType() {
            return this.componentType;
        }

        @DexIgnore
        public int hashCode() {
            return this.componentType.hashCode();
        }

        @DexIgnore
        public String toString() {
            return f64.t(this.componentType) + "[]";
        }
    }

    @DexIgnore
    public enum e {
        JAVA6 {
            @DexIgnore
            @Override // com.fossil.f64.e
            public GenericArrayType newArrayType(Type type) {
                return new d(type);
            }

            @DexIgnore
            @Override // com.fossil.f64.e
            public Type usedInGenericType(Type type) {
                i14.l(type);
                if (!(type instanceof Class)) {
                    return type;
                }
                Class cls = (Class) type;
                return cls.isArray() ? new d(cls.getComponentType()) : type;
            }
        },
        JAVA7 {
            @DexIgnore
            @Override // com.fossil.f64.e
            public Type newArrayType(Type type) {
                return type instanceof Class ? f64.i((Class) type) : new d(type);
            }

            @DexIgnore
            @Override // com.fossil.f64.e
            public Type usedInGenericType(Type type) {
                i14.l(type);
                return type;
            }
        },
        JAVA8 {
            @DexIgnore
            @Override // com.fossil.f64.e
            public Type newArrayType(Type type) {
                return e.JAVA7.newArrayType(type);
            }

            @DexIgnore
            @Override // com.fossil.f64.e
            public String typeName(Type type) {
                try {
                    return (String) Type.class.getMethod("getTypeName", new Class[0]).invoke(type, new Object[0]);
                } catch (NoSuchMethodException e) {
                    throw new AssertionError("Type.getTypeName should be available in Java 8");
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                } catch (IllegalAccessException e3) {
                    throw new RuntimeException(e3);
                }
            }

            @DexIgnore
            @Override // com.fossil.f64.e
            public Type usedInGenericType(Type type) {
                return e.JAVA7.usedInGenericType(type);
            }
        };
        
        @DexIgnore
        public static /* final */ e CURRENT;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends a64<int[]> {
        }

        /*
        static {
            if (AnnotatedElement.class.isAssignableFrom(TypeVariable.class)) {
                CURRENT = JAVA8;
            } else if (new d().capture() instanceof Class) {
                CURRENT = JAVA7;
            } else {
                CURRENT = JAVA6;
            }
        }
        */

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }

        @DexIgnore
        public abstract Type newArrayType(Type type);

        @DexIgnore
        public String typeName(Type type) {
            return f64.t(type);
        }

        @DexIgnore
        public final y24<Type> usedInGenericType(Type[] typeArr) {
            y24.b builder = y24.builder();
            for (Type type : typeArr) {
                builder.g(usedInGenericType(type));
            }
            return builder.i();
        }

        @DexIgnore
        public abstract Type usedInGenericType(Type type);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<X> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ boolean f1058a; // = (!f.class.getTypeParameters()[0].equals(f64.l(f.class, "X", new Type[0])));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ParameterizedType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ y24<Type> argumentsList;
        @DexIgnore
        public /* final */ Type ownerType;
        @DexIgnore
        public /* final */ Class<?> rawType;

        @DexIgnore
        public g(Type type, Class<?> cls, Type[] typeArr) {
            i14.l(cls);
            i14.d(typeArr.length == cls.getTypeParameters().length);
            f64.g(typeArr, "type parameter");
            this.ownerType = type;
            this.rawType = cls;
            this.argumentsList = e.CURRENT.usedInGenericType(typeArr);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) obj;
            return getRawType().equals(parameterizedType.getRawType()) && f14.a(getOwnerType(), parameterizedType.getOwnerType()) && Arrays.equals(getActualTypeArguments(), parameterizedType.getActualTypeArguments());
        }

        @DexIgnore
        public Type[] getActualTypeArguments() {
            return f64.s(this.argumentsList);
        }

        @DexIgnore
        public Type getOwnerType() {
            return this.ownerType;
        }

        @DexIgnore
        public Type getRawType() {
            return this.rawType;
        }

        @DexIgnore
        public int hashCode() {
            Type type = this.ownerType;
            return ((type == null ? 0 : type.hashCode()) ^ this.argumentsList.hashCode()) ^ this.rawType.hashCode();
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Type type = this.ownerType;
            if (type != null) {
                sb.append(e.CURRENT.typeName(type));
                sb.append('.');
            }
            sb.append(this.rawType.getName());
            sb.append('<');
            sb.append(f64.b.e(o34.k(this.argumentsList, f64.f1057a)));
            sb.append('>');
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<D extends GenericDeclaration> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ D f1059a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ y24<Type> c;

        @DexIgnore
        public h(D d, String str, Type[] typeArr) {
            f64.g(typeArr, "bound for type variable");
            i14.l(d);
            this.f1059a = d;
            i14.l(str);
            this.b = str;
            this.c = y24.copyOf(typeArr);
        }

        @DexIgnore
        public D a() {
            return this.f1059a;
        }

        @DexIgnore
        public String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            boolean z = true;
            if (f.f1058a) {
                if (obj == null || !Proxy.isProxyClass(obj.getClass()) || !(Proxy.getInvocationHandler(obj) instanceof i)) {
                    return false;
                }
                h hVar = ((i) Proxy.getInvocationHandler(obj)).f1060a;
                return this.b.equals(hVar.b()) && this.f1059a.equals(hVar.a()) && this.c.equals(hVar.c);
            } else if (!(obj instanceof TypeVariable)) {
                return false;
            } else {
                TypeVariable typeVariable = (TypeVariable) obj;
                if (!this.b.equals(typeVariable.getName()) || !this.f1059a.equals(typeVariable.getGenericDeclaration())) {
                    z = false;
                }
                return z;
            }
        }

        @DexIgnore
        public int hashCode() {
            return this.f1059a.hashCode() ^ this.b.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements InvocationHandler {
        @DexIgnore
        public static /* final */ a34<String, Method> b;

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ h<?> f1060a;

        /*
        static {
            a34.b builder = a34.builder();
            Method[] methods = h.class.getMethods();
            for (Method method : methods) {
                if (method.getDeclaringClass().equals(h.class)) {
                    try {
                        method.setAccessible(true);
                    } catch (AccessControlException e) {
                    }
                    builder.c(method.getName(), method);
                }
            }
            b = builder.a();
        }
        */

        @DexIgnore
        public i(h<?> hVar) {
            this.f1060a = hVar;
        }

        @DexIgnore
        @Override // java.lang.reflect.InvocationHandler
        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Method method2 = b.get(name);
            if (method2 != null) {
                try {
                    return method2.invoke(this.f1060a, objArr);
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            } else {
                throw new UnsupportedOperationException(name);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements WildcardType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ y24<Type> lowerBounds;
        @DexIgnore
        public /* final */ y24<Type> upperBounds;

        @DexIgnore
        public j(Type[] typeArr, Type[] typeArr2) {
            f64.g(typeArr, "lower bound for wildcard");
            f64.g(typeArr2, "upper bound for wildcard");
            this.lowerBounds = e.CURRENT.usedInGenericType(typeArr);
            this.upperBounds = e.CURRENT.usedInGenericType(typeArr2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) obj;
            return this.lowerBounds.equals(Arrays.asList(wildcardType.getLowerBounds())) && this.upperBounds.equals(Arrays.asList(wildcardType.getUpperBounds()));
        }

        @DexIgnore
        public Type[] getLowerBounds() {
            return f64.s(this.lowerBounds);
        }

        @DexIgnore
        public Type[] getUpperBounds() {
            return f64.s(this.upperBounds);
        }

        @DexIgnore
        public int hashCode() {
            return this.lowerBounds.hashCode() ^ this.upperBounds.hashCode();
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder("?");
            Iterator it = this.lowerBounds.iterator();
            while (it.hasNext()) {
                sb.append(" super ");
                sb.append(e.CURRENT.typeName((Type) it.next()));
            }
            for (Type type : f64.h(this.upperBounds)) {
                sb.append(" extends ");
                sb.append(e.CURRENT.typeName(type));
            }
            return sb.toString();
        }
    }

    @DexIgnore
    public static void g(Type[] typeArr, String str) {
        for (Type type : typeArr) {
            if (type instanceof Class) {
                Class cls = (Class) type;
                i14.i(!cls.isPrimitive(), "Primitive type '%s' used as %s", cls, str);
            }
        }
    }

    @DexIgnore
    public static Iterable<Type> h(Iterable<Type> iterable) {
        return o34.d(iterable, k14.d(k14.a(Object.class)));
    }

    @DexIgnore
    public static Class<?> i(Class<?> cls) {
        return Array.newInstance(cls, 0).getClass();
    }

    @DexIgnore
    public static Type j(Type type) {
        i14.l(type);
        AtomicReference atomicReference = new AtomicReference();
        new b(atomicReference).a(type);
        return (Type) atomicReference.get();
    }

    @DexIgnore
    public static Type k(Type type) {
        boolean z = true;
        if (!(type instanceof WildcardType)) {
            return e.CURRENT.newArrayType(type);
        }
        WildcardType wildcardType = (WildcardType) type;
        Type[] lowerBounds = wildcardType.getLowerBounds();
        i14.e(lowerBounds.length <= 1, "Wildcard cannot have more than one lower bounds.");
        if (lowerBounds.length == 1) {
            return r(k(lowerBounds[0]));
        }
        Type[] upperBounds = wildcardType.getUpperBounds();
        if (upperBounds.length != 1) {
            z = false;
        }
        i14.e(z, "Wildcard should have only one upper bound.");
        return p(k(upperBounds[0]));
    }

    @DexIgnore
    public static <D extends GenericDeclaration> TypeVariable<D> l(D d2, String str, Type... typeArr) {
        if (typeArr.length == 0) {
            typeArr = new Type[]{Object.class};
        }
        return o(d2, str, typeArr);
    }

    @DexIgnore
    public static ParameterizedType m(Class<?> cls, Type... typeArr) {
        return new g(c.JVM_BEHAVIOR.getOwnerType(cls), cls, typeArr);
    }

    @DexIgnore
    public static ParameterizedType n(Type type, Class<?> cls, Type... typeArr) {
        if (type == null) {
            return m(cls, typeArr);
        }
        i14.l(typeArr);
        i14.h(cls.getEnclosingClass() != null, "Owner type for unenclosed %s", cls);
        return new g(type, cls, typeArr);
    }

    @DexIgnore
    public static <D extends GenericDeclaration> TypeVariable<D> o(D d2, String str, Type[] typeArr) {
        return (TypeVariable) z54.a(TypeVariable.class, new i(new h(d2, str, typeArr)));
    }

    @DexIgnore
    public static WildcardType p(Type type) {
        return new j(new Type[0], new Type[]{type});
    }

    @DexIgnore
    public static Type q(Type[] typeArr) {
        for (Type type : typeArr) {
            Type j2 = j(type);
            if (j2 != null) {
                if (j2 instanceof Class) {
                    Class cls = (Class) j2;
                    if (cls.isPrimitive()) {
                        return cls;
                    }
                }
                return p(j2);
            }
        }
        return null;
    }

    @DexIgnore
    public static WildcardType r(Type type) {
        return new j(new Type[]{type}, new Type[]{Object.class});
    }

    @DexIgnore
    public static Type[] s(Collection<Type> collection) {
        return (Type[]) collection.toArray(new Type[collection.size()]);
    }

    @DexIgnore
    public static String t(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }
}
