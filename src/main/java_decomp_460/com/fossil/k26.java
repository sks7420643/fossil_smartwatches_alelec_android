package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k26 implements Factory<j26> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<QuickResponseRepository> f1856a;

    @DexIgnore
    public k26(Provider<QuickResponseRepository> provider) {
        this.f1856a = provider;
    }

    @DexIgnore
    public static k26 a(Provider<QuickResponseRepository> provider) {
        return new k26(provider);
    }

    @DexIgnore
    public static j26 c(QuickResponseRepository quickResponseRepository) {
        return new j26(quickResponseRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public j26 get() {
        return c(this.f1856a.get());
    }
}
