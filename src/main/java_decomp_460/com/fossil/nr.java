package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr extends ox1 {
    @DexIgnore
    public /* final */ yp b;
    @DexIgnore
    public /* final */ zq c;
    @DexIgnore
    public /* final */ mw d;
    @DexIgnore
    public /* final */ ax1 e;

    @DexIgnore
    public nr(yp ypVar, zq zqVar, mw mwVar, ax1 ax1) {
        this.b = ypVar;
        this.c = zqVar;
        this.d = mwVar;
        this.e = ax1;
    }

    @DexIgnore
    public /* synthetic */ nr(yp ypVar, zq zqVar, mw mwVar, ax1 ax1, int i) {
        ax1 ax12 = null;
        ypVar = (i & 1) != 0 ? yp.b : ypVar;
        mw mwVar2 = (i & 4) != 0 ? new mw(null, null, lw.b, null, null, 27) : mwVar;
        ax12 = (i & 8) == 0 ? ax1 : ax12;
        this.b = ypVar;
        this.c = zqVar;
        this.d = mwVar2;
        this.e = ax12;
    }

    @DexIgnore
    public static /* synthetic */ nr a(nr nrVar, yp ypVar, zq zqVar, mw mwVar, ax1 ax1, int i) {
        if ((i & 1) != 0) {
            ypVar = nrVar.b;
        }
        if ((i & 2) != 0) {
            zqVar = nrVar.c;
        }
        if ((i & 4) != 0) {
            mwVar = nrVar.d;
        }
        if ((i & 8) != 0) {
            ax1 = nrVar.e;
        }
        return nrVar.a(ypVar, zqVar, mwVar, ax1);
    }

    @DexIgnore
    public final nr a(yp ypVar, zq zqVar, mw mwVar, ax1 ax1) {
        return new nr(ypVar, zqVar, mwVar, ax1);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof nr) {
                nr nrVar = (nr) obj;
                if (!pq7.a(this.b, nrVar.b) || !pq7.a(this.c, nrVar.c) || !pq7.a(this.d, nrVar.d) || !pq7.a(this.e, nrVar.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        yp ypVar = this.b;
        int hashCode = ypVar != null ? ypVar.hashCode() : 0;
        zq zqVar = this.c;
        int hashCode2 = zqVar != null ? zqVar.hashCode() : 0;
        mw mwVar = this.d;
        int hashCode3 = mwVar != null ? mwVar.hashCode() : 0;
        ax1 ax1 = this.e;
        if (ax1 != null) {
            i = ax1.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            g80.k(g80.k(jSONObject, jd0.i4, ey1.a(this.b)), jd0.O0, ey1.a(this.c));
            if (this.d.d != lw.b) {
                g80.k(jSONObject, jd0.j4, this.d.toJSONObject());
            }
            jd0 jd0 = jd0.e6;
            ax1 ax1 = this.e;
            g80.k(jSONObject, jd0, ax1 != null ? ax1.toJSONObject() : null);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e2 = e.e("Result(phaseId=");
        e2.append(this.b);
        e2.append(", resultCode=");
        e2.append(this.c);
        e2.append(", requestResult=");
        e2.append(this.d);
        e2.append(", networkError=");
        e2.append(this.e);
        e2.append(")");
        return e2.toString();
    }
}
