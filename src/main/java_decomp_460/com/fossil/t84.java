package com.fossil;

import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class t84 implements FilenameFilter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ t84 f3376a; // = new t84();

    @DexIgnore
    public static FilenameFilter a() {
        return f3376a;
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return str.startsWith(".ae");
    }
}
