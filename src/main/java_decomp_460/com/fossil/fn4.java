package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn4 extends mn4 {
    @DexIgnore
    @Override // com.fossil.jn4, com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) throws rl4 {
        if (kl4 == kl4.EAN_8) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_8, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.jn4
    public boolean[] c(String str) {
        if (str.length() == 8) {
            boolean[] zArr = new boolean[67];
            int i = 0;
            int b = jn4.b(zArr, 0, ln4.f2223a, true) + 0;
            while (i <= 3) {
                int i2 = i + 1;
                b += jn4.b(zArr, b, ln4.d[Integer.parseInt(str.substring(i, i2))], false);
                i = i2;
            }
            int i3 = 4;
            int b2 = b + jn4.b(zArr, b, ln4.b, false);
            while (i3 <= 7) {
                int i4 = i3 + 1;
                b2 += jn4.b(zArr, b2, ln4.d[Integer.parseInt(str.substring(i3, i4))], true);
                i3 = i4;
            }
            jn4.b(zArr, b2, ln4.f2223a, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
