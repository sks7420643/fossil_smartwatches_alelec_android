package com.fossil;

import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vu1 extends ox1 implements Parcelable {
    @DexIgnore
    public o90 b;

    @DexIgnore
    public final void a(o90 o90) {
        this.b = o90;
    }

    @DexIgnore
    public final byte[] a() {
        o90 o90 = this.b;
        if (o90 != null) {
            int length = o90.f2650a.length + 3 + c().length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            pq7.b(allocate, "ByteBuffer.allocate(size)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            o90 o902 = this.b;
            if (o902 != null) {
                allocate.put(o902.f2650a);
                allocate.putShort((short) length);
                allocate.put((byte) b());
                allocate.put(c());
                byte[] array = allocate.array();
                pq7.b(array, "byteBuffer.array()");
                return array;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract byte[] c();
}
