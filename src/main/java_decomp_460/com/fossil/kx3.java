package com.fossil;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx3 extends cg0 {
    @DexIgnore
    public kx3(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.cg0
    public MenuItem a(int i, int i2, int i3, CharSequence charSequence) {
        if (size() + 1 <= 5) {
            h0();
            MenuItem a2 = super.a(i, i2, i3, charSequence);
            if (a2 instanceof eg0) {
                ((eg0) a2).t(true);
            }
            g0();
            return a2;
        }
        throw new IllegalArgumentException("Maximum number of items supported by BottomNavigationView is 5. Limit can be checked with BottomNavigationView#getMaxItemCount()");
    }

    @DexIgnore
    @Override // android.view.Menu, com.fossil.cg0
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        throw new UnsupportedOperationException("BottomNavigationView does not support submenus");
    }
}
