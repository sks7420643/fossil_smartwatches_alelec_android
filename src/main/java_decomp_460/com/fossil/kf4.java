package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kf4 {
    @DexIgnore
    public static kf4 e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f1910a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public b c; // = new b();
    @DexIgnore
    public int d; // = 1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f1911a;
        @DexIgnore
        public /* final */ Messenger b;
        @DexIgnore
        public c c;
        @DexIgnore
        public /* final */ Queue<e<?>> d;
        @DexIgnore
        public /* final */ SparseArray<e<?>> e;

        @DexIgnore
        public b() {
            this.f1911a = 0;
            this.b = new Messenger(new dm2(Looper.getMainLooper(), new lf4(this)));
            this.d = new ArrayDeque();
            this.e = new SparseArray<>();
        }

        @DexIgnore
        public boolean a(e<?> eVar) {
            synchronized (this) {
                int i = this.f1911a;
                if (i == 0) {
                    this.d.add(eVar);
                    k();
                    return true;
                } else if (i == 1) {
                    this.d.add(eVar);
                    return true;
                } else if (i == 2) {
                    this.d.add(eVar);
                    i();
                    return true;
                } else if (i == 3 || i == 4) {
                    return false;
                } else {
                    int i2 = this.f1911a;
                    StringBuilder sb = new StringBuilder(26);
                    sb.append("Unknown state: ");
                    sb.append(i2);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }

        @DexIgnore
        public void b(f fVar) {
            for (e<?> eVar : this.d) {
                eVar.b(fVar);
            }
            this.d.clear();
            for (int i = 0; i < this.e.size(); i++) {
                this.e.valueAt(i).b(fVar);
            }
            this.e.clear();
        }

        @DexIgnore
        public void c(int i, String str) {
            synchronized (this) {
                if (Log.isLoggable("MessengerIpcClient", 3)) {
                    String valueOf = String.valueOf(str);
                    Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
                }
                int i2 = this.f1911a;
                if (i2 == 0) {
                    throw new IllegalStateException();
                } else if (i2 == 1 || i2 == 2) {
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Unbinding service");
                    }
                    this.f1911a = 4;
                    ve2.b().c(kf4.this.f1910a, this);
                    b(new f(i, str));
                } else if (i2 == 3) {
                    this.f1911a = 4;
                } else if (i2 != 4) {
                    int i3 = this.f1911a;
                    StringBuilder sb = new StringBuilder(26);
                    sb.append("Unknown state: ");
                    sb.append(i3);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }

        @DexIgnore
        public final /* synthetic */ void d(IBinder iBinder) {
            synchronized (this) {
                if (iBinder == null) {
                    c(0, "Null service connection");
                    return;
                }
                try {
                    this.c = new c(iBinder);
                    this.f1911a = 2;
                    i();
                } catch (RemoteException e2) {
                    c(0, e2.getMessage());
                }
            }
        }

        @DexIgnore
        public final /* synthetic */ void e() {
            c(2, "Service disconnected");
        }

        @DexIgnore
        public final /* synthetic */ void f(e eVar) {
            m(eVar.f1913a);
        }

        @DexIgnore
        public final /* synthetic */ void g() {
            e<?> poll;
            while (true) {
                synchronized (this) {
                    if (this.f1911a == 2) {
                        if (this.d.isEmpty()) {
                            n();
                            return;
                        }
                        poll = this.d.poll();
                        this.e.put(poll.f1913a, poll);
                        kf4.this.b.schedule(new qf4(this, poll), 30, TimeUnit.SECONDS);
                    } else {
                        return;
                    }
                }
                j(poll);
            }
        }

        @DexIgnore
        public boolean h(Message message) {
            int i = message.arg1;
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                StringBuilder sb = new StringBuilder(41);
                sb.append("Received response to request: ");
                sb.append(i);
                Log.d("MessengerIpcClient", sb.toString());
            }
            synchronized (this) {
                e<?> eVar = this.e.get(i);
                if (eVar == null) {
                    StringBuilder sb2 = new StringBuilder(50);
                    sb2.append("Received response for unknown request: ");
                    sb2.append(i);
                    Log.w("MessengerIpcClient", sb2.toString());
                } else {
                    this.e.remove(i);
                    n();
                    eVar.e(message.getData());
                }
            }
            return true;
        }

        @DexIgnore
        public void i() {
            kf4.this.b.execute(new of4(this));
        }

        @DexIgnore
        public void j(e<?> eVar) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(eVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
                sb.append("Sending ");
                sb.append(valueOf);
                Log.d("MessengerIpcClient", sb.toString());
            }
            try {
                this.c.a(eVar.a(kf4.this.f1910a, this.b));
            } catch (RemoteException e2) {
                c(2, e2.getMessage());
            }
        }

        @DexIgnore
        public void k() {
            rc2.n(this.f1911a == 0);
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Starting bind to GmsCore");
            }
            this.f1911a = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!ve2.b().a(kf4.this.f1910a, intent, this, 1)) {
                c(0, "Unable to bind to service");
            } else {
                kf4.this.b.schedule(new mf4(this), 30, TimeUnit.SECONDS);
            }
        }

        @DexIgnore
        public void l() {
            synchronized (this) {
                if (this.f1911a == 1) {
                    c(1, "Timed out while binding");
                }
            }
        }

        @DexIgnore
        public void m(int i) {
            synchronized (this) {
                e<?> eVar = this.e.get(i);
                if (eVar != null) {
                    StringBuilder sb = new StringBuilder(31);
                    sb.append("Timing out request: ");
                    sb.append(i);
                    Log.w("MessengerIpcClient", sb.toString());
                    this.e.remove(i);
                    eVar.b(new f(3, "Timed out waiting for response"));
                    n();
                }
            }
        }

        @DexIgnore
        public void n() {
            synchronized (this) {
                if (this.f1911a == 2 && this.d.isEmpty() && this.e.size() == 0) {
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
                    }
                    this.f1911a = 3;
                    ve2.b().c(kf4.this.f1910a, this);
                }
            }
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Service connected");
            }
            kf4.this.b.execute(new nf4(this, iBinder));
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Service disconnected");
            }
            kf4.this.b.execute(new pf4(this));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Messenger f1912a;
        @DexIgnore
        public /* final */ te4 b;

        @DexIgnore
        public c(IBinder iBinder) throws RemoteException {
            String interfaceDescriptor = iBinder.getInterfaceDescriptor();
            if ("android.os.IMessenger".equals(interfaceDescriptor)) {
                this.f1912a = new Messenger(iBinder);
                this.b = null;
            } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
                this.b = new te4(iBinder);
                this.f1912a = null;
            } else {
                String valueOf = String.valueOf(interfaceDescriptor);
                Log.w("MessengerIpcClient", valueOf.length() != 0 ? "Invalid interface descriptor: ".concat(valueOf) : new String("Invalid interface descriptor: "));
                throw new RemoteException();
            }
        }

        @DexIgnore
        public void a(Message message) throws RemoteException {
            Messenger messenger = this.f1912a;
            if (messenger != null) {
                messenger.send(message);
                return;
            }
            te4 te4 = this.b;
            if (te4 != null) {
                te4.b(message);
                return;
            }
            throw new IllegalStateException("Both messengers are null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends e<Void> {
        @DexIgnore
        public d(int i, int i2, Bundle bundle) {
            super(i, i2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.kf4.e
        public void f(Bundle bundle) {
            if (bundle.getBoolean("ack", false)) {
                c(null);
            } else {
                b(new f(4, "Invalid response to one way request"));
            }
        }

        @DexIgnore
        @Override // com.fossil.kf4.e
        public boolean g() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1913a;
        @DexIgnore
        public /* final */ ot3<T> b; // = new ot3<>();
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ Bundle d;

        @DexIgnore
        public e(int i, int i2, Bundle bundle) {
            this.f1913a = i;
            this.c = i2;
            this.d = bundle;
        }

        @DexIgnore
        public Message a(Context context, Messenger messenger) {
            Message obtain = Message.obtain();
            obtain.what = this.c;
            obtain.arg1 = this.f1913a;
            obtain.replyTo = messenger;
            Bundle bundle = new Bundle();
            bundle.putBoolean("oneWay", g());
            bundle.putString("pkg", context.getPackageName());
            bundle.putBundle("data", this.d);
            obtain.setData(bundle);
            return obtain;
        }

        @DexIgnore
        public void b(f fVar) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(this);
                String valueOf2 = String.valueOf(fVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14 + String.valueOf(valueOf2).length());
                sb.append("Failing ");
                sb.append(valueOf);
                sb.append(" with ");
                sb.append(valueOf2);
                Log.d("MessengerIpcClient", sb.toString());
            }
            this.b.b(fVar);
        }

        @DexIgnore
        public void c(T t) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(this);
                String valueOf2 = String.valueOf(t);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 16 + String.valueOf(valueOf2).length());
                sb.append("Finishing ");
                sb.append(valueOf);
                sb.append(" with ");
                sb.append(valueOf2);
                Log.d("MessengerIpcClient", sb.toString());
            }
            this.b.c(t);
        }

        @DexIgnore
        public nt3<T> d() {
            return this.b.a();
        }

        @DexIgnore
        public void e(Bundle bundle) {
            if (bundle.getBoolean("unsupported", false)) {
                b(new f(4, "Not supported by GmsCore"));
            } else {
                f(bundle);
            }
        }

        @DexIgnore
        public abstract void f(Bundle bundle);

        @DexIgnore
        public abstract boolean g();

        @DexIgnore
        public String toString() {
            int i = this.c;
            int i2 = this.f1913a;
            boolean g = g();
            StringBuilder sb = new StringBuilder(55);
            sb.append("Request { what=");
            sb.append(i);
            sb.append(" id=");
            sb.append(i2);
            sb.append(" oneWay=");
            sb.append(g);
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends Exception {
        @DexIgnore
        public /* final */ int errorCode;

        @DexIgnore
        public f(int i, String str) {
            super(str);
            this.errorCode = i;
        }

        @DexIgnore
        public int getErrorCode() {
            return this.errorCode;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends e<Bundle> {
        @DexIgnore
        public g(int i, int i2, Bundle bundle) {
            super(i, i2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.kf4.e
        public void f(Bundle bundle) {
            Bundle bundle2 = bundle.getBundle("data");
            if (bundle2 == null) {
                bundle2 = Bundle.EMPTY;
            }
            c(bundle2);
        }

        @DexIgnore
        @Override // com.fossil.kf4.e
        public boolean g() {
            return false;
        }
    }

    @DexIgnore
    public kf4(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.f1910a = context.getApplicationContext();
    }

    @DexIgnore
    public static kf4 c(Context context) {
        kf4 kf4;
        synchronized (kf4.class) {
            try {
                if (e == null) {
                    e = new kf4(context, zl2.a().a(1, new sf2("MessengerIpcClient"), em2.f958a));
                }
                kf4 = e;
            } catch (Throwable th) {
                throw th;
            }
        }
        return kf4;
    }

    @DexIgnore
    public final int d() {
        int i;
        synchronized (this) {
            i = this.d;
            this.d = i + 1;
        }
        return i;
    }

    @DexIgnore
    public nt3<Void> e(int i, Bundle bundle) {
        return f(new d(d(), i, bundle));
    }

    @DexIgnore
    public final <T> nt3<T> f(e<T> eVar) {
        nt3<T> d2;
        synchronized (this) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(eVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
                sb.append("Queueing ");
                sb.append(valueOf);
                Log.d("MessengerIpcClient", sb.toString());
            }
            if (!this.c.a(eVar)) {
                b bVar = new b();
                this.c = bVar;
                bVar.a(eVar);
            }
            d2 = eVar.d();
        }
        return d2;
    }

    @DexIgnore
    public nt3<Bundle> g(int i, Bundle bundle) {
        return f(new g(d(), i, bundle));
    }
}
