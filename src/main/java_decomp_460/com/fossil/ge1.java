package com.fossil;

import android.content.Context;
import com.fossil.ee1;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ge1 extends ee1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ee1.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Context f1295a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(Context context, String str) {
            this.f1295a = context;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.ee1.a
        public File a() {
            File cacheDir = this.f1295a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            return this.b != null ? new File(cacheDir, this.b) : cacheDir;
        }
    }

    @DexIgnore
    public ge1(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    @DexIgnore
    public ge1(Context context, String str, long j) {
        super(new a(context, str), j);
    }
}
