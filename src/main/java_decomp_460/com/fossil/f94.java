package com.fossil;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f94 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ String f1088a;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicLong b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f94$a$a")
        /* renamed from: com.fossil.f94$a$a  reason: collision with other inner class name */
        public class C0082a extends n84 {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable b;

            @DexIgnore
            public C0082a(a aVar, Runnable runnable) {
                this.b = runnable;
            }

            @DexIgnore
            @Override // com.fossil.n84
            public void a() {
                this.b.run();
            }
        }

        @DexIgnore
        public a(String str, AtomicLong atomicLong) {
            this.f1088a = str;
            this.b = atomicLong;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new C0082a(this, runnable));
            newThread.setName(this.f1088a + this.b.getAndIncrement());
            return newThread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends n84 {
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ TimeUnit e;

        @DexIgnore
        public b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.b = str;
            this.c = executorService;
            this.d = j;
            this.e = timeUnit;
        }

        @DexIgnore
        @Override // com.fossil.n84
        public void a() {
            try {
                x74 f = x74.f();
                f.b("Executing shutdown hook for " + this.b);
                this.c.shutdown();
                if (!this.c.awaitTermination(this.d, this.e)) {
                    x74 f2 = x74.f();
                    f2.b(this.b + " did not shut down in the allocated time. Requesting immediate shutdown.");
                    this.c.shutdownNow();
                }
            } catch (InterruptedException e2) {
                x74.f().b(String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", this.b));
                this.c.shutdownNow();
            }
        }
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService) {
        b(str, executorService, 2, TimeUnit.SECONDS);
    }

    @DexIgnore
    public static final void b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        b bVar = new b(str, executorService, j, timeUnit);
        runtime.addShutdownHook(new Thread(bVar, "Crashlytics Shutdown Hook for " + str));
    }

    @DexIgnore
    public static ExecutorService c(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(d(str));
        a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    @DexIgnore
    public static final ThreadFactory d(String str) {
        return new a(str, new AtomicLong(1));
    }
}
