package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr5 implements Factory<yr5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ zr5 f4519a; // = new zr5();
    }

    @DexIgnore
    public static zr5 a() {
        return a.f4519a;
    }

    @DexIgnore
    public static yr5 c() {
        return new yr5();
    }

    @DexIgnore
    /* renamed from: b */
    public yr5 get() {
        return c();
    }
}
