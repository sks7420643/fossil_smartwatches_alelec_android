package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.uj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sj0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public uj0 f3267a;
    @DexIgnore
    public uj0 b;
    @DexIgnore
    public uj0 c;
    @DexIgnore
    public uj0 d;
    @DexIgnore
    public uj0 e;
    @DexIgnore
    public uj0 f;
    @DexIgnore
    public uj0 g;
    @DexIgnore
    public ArrayList<uj0> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;

    @DexIgnore
    public sj0(uj0 uj0, int i2, boolean z) {
        this.f3267a = uj0;
        this.l = i2;
        this.m = z;
    }

    @DexIgnore
    public static boolean c(uj0 uj0, int i2) {
        if (uj0.C() != 8 && uj0.C[i2] == uj0.b.MATCH_CONSTRAINT) {
            int[] iArr = uj0.g;
            if (iArr[i2] == 0 || iArr[i2] == 3) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a() {
        if (!this.q) {
            b();
        }
        this.q = true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00b7 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
        // Method dump skipped, instructions count: 225
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sj0.b():void");
    }
}
