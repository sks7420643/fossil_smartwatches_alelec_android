package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gu7 {
    @DexIgnore
    public static final <T> rv7<T> a(iv7 iv7, tn7 tn7, lv7 lv7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7) {
        tn7 c = cv7.c(iv7, tn7);
        sv7 hx7 = lv7.isLazy() ? new hx7(c, vp7) : new sv7(c, true);
        ((au7) hx7).z0(lv7, hx7, vp7);
        return hx7;
    }

    @DexIgnore
    public static /* synthetic */ rv7 b(iv7 iv7, tn7 tn7, lv7 lv7, vp7 vp7, int i, Object obj) {
        if ((i & 1) != 0) {
            tn7 = un7.INSTANCE;
        }
        if ((i & 2) != 0) {
            lv7 = lv7.DEFAULT;
        }
        return eu7.a(iv7, tn7, lv7, vp7);
    }

    @DexIgnore
    public static final xw7 c(iv7 iv7, tn7 tn7, lv7 lv7, vp7<? super iv7, ? super qn7<? super tl7>, ? extends Object> vp7) {
        tn7 c = cv7.c(iv7, tn7);
        au7 ix7 = lv7.isLazy() ? new ix7(c, vp7) : new rx7(c, true);
        ix7.z0(lv7, ix7, vp7);
        return ix7;
    }

    @DexIgnore
    public static /* synthetic */ xw7 d(iv7 iv7, tn7 tn7, lv7 lv7, vp7 vp7, int i, Object obj) {
        if ((i & 1) != 0) {
            tn7 = un7.INSTANCE;
        }
        if ((i & 2) != 0) {
            lv7 = lv7.DEFAULT;
        }
        return eu7.c(iv7, tn7, lv7, vp7);
    }

    @DexIgnore
    public static final <T> Object e(tn7 tn7, vp7<? super iv7, ? super qn7<? super T>, ? extends Object> vp7, qn7<? super T> qn7) {
        Object A0;
        tn7 context = qn7.getContext();
        tn7 plus = context.plus(tn7);
        fy7.a(plus);
        if (plus == context) {
            tz7 tz7 = new tz7(plus, qn7);
            A0 = e08.c(tz7, tz7, vp7);
        } else if (pq7.a((rn7) plus.get(rn7.p), (rn7) context.get(rn7.p))) {
            dy7 dy7 = new dy7(plus, qn7);
            Object c = zz7.c(plus, null);
            try {
                A0 = e08.c(dy7, dy7, vp7);
            } finally {
                zz7.a(plus, c);
            }
        } else {
            xv7 xv7 = new xv7(plus, qn7);
            xv7.v0();
            d08.c(vp7, xv7, xv7);
            A0 = xv7.A0();
        }
        if (A0 == yn7.d()) {
            go7.c(qn7);
        }
        return A0;
    }
}
