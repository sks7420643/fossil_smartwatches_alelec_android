package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ap1 {
    DIAGNOSTICS("diagnosticsApp"),
    WELLNESS("wellnessApp"),
    WORKOUT("workoutApp"),
    MUSIC("musicApp"),
    NOTIFICATIONS_PANEL("notificationsPanelApp"),
    EMPTY("empty"),
    STOP_WATCH("stopwatchApp"),
    ASSISTANT("assistantApp"),
    TIMER("timerApp"),
    WEATHER("weatherApp"),
    COMMUTE("commuteApp"),
    BUDDY_CHALLENGE("buddyChallengeApp");
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final ap1 a(Object obj) {
            ap1 ap1;
            ap1[] values = ap1.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ap1 = null;
                    break;
                }
                ap1 = values[i];
                if (pq7.a(ap1.a(), obj)) {
                    break;
                }
                i++;
            }
            return ap1 != null ? ap1 : ap1.EMPTY;
        }
    }

    @DexIgnore
    public ap1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj = vb.f3743a[ordinal()] != 1 ? this.b : JSONObject.NULL;
        pq7.b(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
