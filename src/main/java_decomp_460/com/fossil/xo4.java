package com.fossil;

import com.portfolio.platform.data.source.remote.ApiService2Dot1;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo4 implements Factory<ApiService2Dot1> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f4150a;
    @DexIgnore
    public /* final */ Provider<qq5> b;
    @DexIgnore
    public /* final */ Provider<uq5> c;

    @DexIgnore
    public xo4(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        this.f4150a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static xo4 a(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        return new xo4(uo4, provider, provider2);
    }

    @DexIgnore
    public static ApiService2Dot1 c(uo4 uo4, qq5 qq5, uq5 uq5) {
        ApiService2Dot1 f = uo4.f(qq5, uq5);
        lk7.c(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }

    @DexIgnore
    /* renamed from: b */
    public ApiService2Dot1 get() {
        return c(this.f4150a, this.b.get(), this.c.get());
    }
}
