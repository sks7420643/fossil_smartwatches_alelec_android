package com.fossil;

import android.content.Context;
import android.util.Log;
import com.fossil.yh1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bi1 implements zh1 {
    @DexIgnore
    @Override // com.fossil.zh1
    public yh1 a(Context context, yh1.a aVar) {
        boolean z = gl0.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        if (Log.isLoggable("ConnectivityMonitor", 3)) {
            Log.d("ConnectivityMonitor", z ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor" : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
        }
        return z ? new ai1(context, aVar) : new fi1();
    }
}
