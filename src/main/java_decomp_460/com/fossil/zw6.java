package com.fossil;

import android.os.Bundle;
import android.text.Spanned;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zw6 extends gq4<yw6> {
    @DexIgnore
    void H4(Spanned spanned);

    @DexIgnore
    void I3(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    void K4(boolean z, String str);

    @DexIgnore
    void L3(boolean z);

    @DexIgnore
    Object Q1();  // void declaration

    @DexIgnore
    void Q3(Spanned spanned);

    @DexIgnore
    void T5(int i, String str);

    @DexIgnore
    void Y2(Spanned spanned);

    @DexIgnore
    void Z5(String str);

    @DexIgnore
    Object c6();  // void declaration

    @DexIgnore
    void e3(SignUpSocialAuth signUpSocialAuth);

    @DexIgnore
    Object f();  // void declaration

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    Object i();  // void declaration

    @DexIgnore
    Object k3();  // void declaration

    @DexIgnore
    void n0(boolean z, boolean z2, String str);

    @DexIgnore
    void n2(MFUser mFUser);

    @DexIgnore
    void p3(boolean z);

    @DexIgnore
    void p4(qh5 qh5);

    @DexIgnore
    void w0(Spanned spanned);

    @DexIgnore
    void z0(Bundle bundle);
}
