package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g46 extends fq4 {
    @DexIgnore
    public boolean e;

    @DexIgnore
    public abstract void n(ArrayList<j06> arrayList);

    @DexIgnore
    public abstract void o(ArrayList<j06> arrayList);

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public final boolean q() {
        return this.e;
    }

    @DexIgnore
    public abstract void r();

    @DexIgnore
    public abstract void s();

    @DexIgnore
    public abstract void t();

    @DexIgnore
    public abstract void u(ArrayList<String> arrayList);

    @DexIgnore
    public abstract void v();

    @DexIgnore
    public final void w(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public abstract void x(int i, boolean z, boolean z2);
}
