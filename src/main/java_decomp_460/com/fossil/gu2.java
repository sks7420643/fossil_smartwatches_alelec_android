package com.fossil;

import com.fossil.e13;
import com.fossil.hu2;
import com.fossil.ku2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu2 extends e13<gu2, a> implements o23 {
    @DexIgnore
    public static /* final */ gu2 zzi;
    @DexIgnore
    public static volatile z23<gu2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public m13<ku2> zze; // = e13.B();
    @DexIgnore
    public m13<hu2> zzf; // = e13.B();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<gu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(gu2.zzi);
        }

        @DexIgnore
        public /* synthetic */ a(fu2 fu2) {
            this();
        }

        @DexIgnore
        public final ku2 B(int i) {
            return ((gu2) this.c).C(i);
        }

        @DexIgnore
        public final int C() {
            return ((gu2) this.c).O();
        }

        @DexIgnore
        public final hu2 E(int i) {
            return ((gu2) this.c).K(i);
        }

        @DexIgnore
        public final int x() {
            return ((gu2) this.c).M();
        }

        @DexIgnore
        public final a y(int i, hu2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((gu2) this.c).D(i, (hu2) ((e13) aVar.h()));
            return this;
        }

        @DexIgnore
        public final a z(int i, ku2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((gu2) this.c).E(i, (ku2) ((e13) aVar.h()));
            return this;
        }
    }

    /*
    static {
        gu2 gu2 = new gu2();
        zzi = gu2;
        e13.u(gu2.class, gu2);
    }
    */

    @DexIgnore
    public final ku2 C(int i) {
        return this.zze.get(i);
    }

    @DexIgnore
    public final void D(int i, hu2 hu2) {
        hu2.getClass();
        m13<hu2> m13 = this.zzf;
        if (!m13.zza()) {
            this.zzf = e13.q(m13);
        }
        this.zzf.set(i, hu2);
    }

    @DexIgnore
    public final void E(int i, ku2 ku2) {
        ku2.getClass();
        m13<ku2> m13 = this.zze;
        if (!m13.zza()) {
            this.zze = e13.q(m13);
        }
        this.zze.set(i, ku2);
    }

    @DexIgnore
    public final boolean I() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int J() {
        return this.zzd;
    }

    @DexIgnore
    public final hu2 K(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final List<ku2> L() {
        return this.zze;
    }

    @DexIgnore
    public final int M() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<hu2> N() {
        return this.zzf;
    }

    @DexIgnore
    public final int O() {
        return this.zzf.size();
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (fu2.f1206a[i - 1]) {
            case 1:
                return new gu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001\u1004\u0000\u0002\u001b\u0003\u001b\u0004\u1007\u0001\u0005\u1007\u0002", new Object[]{"zzc", "zzd", "zze", ku2.class, "zzf", hu2.class, "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                z23<gu2> z232 = zzj;
                if (z232 != null) {
                    return z232;
                }
                synchronized (gu2.class) {
                    try {
                        z23 = zzj;
                        if (z23 == null) {
                            z23 = new e13.c(zzi);
                            zzj = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
