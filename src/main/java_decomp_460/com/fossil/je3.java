package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<je3> CREATOR; // = new cf3();
    @DexIgnore
    public String b;

    @DexIgnore
    public je3(String str) {
        this.b = str;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 2, this.b, false);
        bd2.b(parcel, a2);
    }
}
