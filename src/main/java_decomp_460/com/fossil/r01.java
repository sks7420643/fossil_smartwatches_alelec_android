package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r01 {
    @DexIgnore
    public static /* final */ String b; // = x01.f("Data");
    @DexIgnore
    public static /* final */ r01 c; // = new a().a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Map<String, Object> f3055a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Map<String, Object> f3056a; // = new HashMap();

        @DexIgnore
        public r01 a() {
            r01 r01 = new r01(this.f3056a);
            r01.k(r01);
            return r01;
        }

        @DexIgnore
        public a b(String str, Object obj) {
            if (obj == null) {
                this.f3056a.put(str, null);
            } else {
                Class<?> cls = obj.getClass();
                if (cls == Boolean.class || cls == Byte.class || cls == Integer.class || cls == Long.class || cls == Float.class || cls == Double.class || cls == String.class || cls == Boolean[].class || cls == Byte[].class || cls == Integer[].class || cls == Long[].class || cls == Float[].class || cls == Double[].class || cls == String[].class) {
                    this.f3056a.put(str, obj);
                } else if (cls == boolean[].class) {
                    this.f3056a.put(str, r01.a((boolean[]) obj));
                } else if (cls == byte[].class) {
                    this.f3056a.put(str, r01.b((byte[]) obj));
                } else if (cls == int[].class) {
                    this.f3056a.put(str, r01.e((int[]) obj));
                } else if (cls == long[].class) {
                    this.f3056a.put(str, r01.f((long[]) obj));
                } else if (cls == float[].class) {
                    this.f3056a.put(str, r01.d((float[]) obj));
                } else if (cls == double[].class) {
                    this.f3056a.put(str, r01.c((double[]) obj));
                } else {
                    throw new IllegalArgumentException(String.format("Key %s has invalid type %s", str, cls));
                }
            }
            return this;
        }

        @DexIgnore
        public a c(r01 r01) {
            d(r01.f3055a);
            return this;
        }

        @DexIgnore
        public a d(Map<String, Object> map) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                b(entry.getKey(), entry.getValue());
            }
            return this;
        }

        @DexIgnore
        public a e(String str, String str2) {
            this.f3056a.put(str, str2);
            return this;
        }
    }

    @DexIgnore
    public r01() {
    }

    @DexIgnore
    public r01(r01 r01) {
        this.f3055a = new HashMap(r01.f3055a);
    }

    @DexIgnore
    public r01(Map<String, ?> map) {
        this.f3055a = new HashMap(map);
    }

    @DexIgnore
    public static Boolean[] a(boolean[] zArr) {
        Boolean[] boolArr = new Boolean[zArr.length];
        for (int i = 0; i < zArr.length; i++) {
            boolArr[i] = Boolean.valueOf(zArr[i]);
        }
        return boolArr;
    }

    @DexIgnore
    public static Byte[] b(byte[] bArr) {
        Byte[] bArr2 = new Byte[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            bArr2[i] = Byte.valueOf(bArr[i]);
        }
        return bArr2;
    }

    @DexIgnore
    public static Double[] c(double[] dArr) {
        Double[] dArr2 = new Double[dArr.length];
        for (int i = 0; i < dArr.length; i++) {
            dArr2[i] = Double.valueOf(dArr[i]);
        }
        return dArr2;
    }

    @DexIgnore
    public static Float[] d(float[] fArr) {
        Float[] fArr2 = new Float[fArr.length];
        for (int i = 0; i < fArr.length; i++) {
            fArr2[i] = Float.valueOf(fArr[i]);
        }
        return fArr2;
    }

    @DexIgnore
    public static Integer[] e(int[] iArr) {
        Integer[] numArr = new Integer[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            numArr[i] = Integer.valueOf(iArr[i]);
        }
        return numArr;
    }

    @DexIgnore
    public static Long[] f(long[] jArr) {
        Long[] lArr = new Long[jArr.length];
        for (int i = 0; i < jArr.length; i++) {
            lArr[i] = Long.valueOf(jArr[i]);
        }
        return lArr;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0049 A[SYNTHETIC, Splitter:B:23:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0066 A[SYNTHETIC, Splitter:B:33:0x0066] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.r01 g(byte[] r6) {
        /*
            r2 = 0
            int r0 = r6.length
            r1 = 10240(0x2800, float:1.4349E-41)
            if (r0 > r1) goto L_0x007f
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream
            r4.<init>(r6)
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x003e, ClassNotFoundException -> 0x0087, all -> 0x0089 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x003e, ClassNotFoundException -> 0x0087, all -> 0x0089 }
            int r0 = r1.readInt()     // Catch:{ IOException -> 0x008b, ClassNotFoundException -> 0x008d }
        L_0x0019:
            if (r0 <= 0) goto L_0x0029
            java.lang.String r2 = r1.readUTF()     // Catch:{ IOException -> 0x008b, ClassNotFoundException -> 0x008d }
            java.lang.Object r5 = r1.readObject()     // Catch:{ IOException -> 0x008b, ClassNotFoundException -> 0x008d }
            r3.put(r2, r5)     // Catch:{ IOException -> 0x008b, ClassNotFoundException -> 0x008d }
            int r0 = r0 + -1
            goto L_0x0019
        L_0x0029:
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x002c:
            r4.close()     // Catch:{ IOException -> 0x0050 }
        L_0x002f:
            com.fossil.r01 r0 = new com.fossil.r01
            r0.<init>(r3)
            return r0
        L_0x0035:
            r0 = move-exception
            java.lang.String r1 = com.fossil.r01.b
            java.lang.String r2 = "Error in Data#fromByteArray: "
            android.util.Log.e(r1, r2, r0)
            goto L_0x002c
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            r1 = r2
        L_0x0040:
            java.lang.String r2 = com.fossil.r01.b     // Catch:{ all -> 0x0062 }
            java.lang.String r5 = "Error in Data#fromByteArray: "
            android.util.Log.e(r2, r5, r0)     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x004c:
            r4.close()
            goto L_0x002f
        L_0x0050:
            r0 = move-exception
            java.lang.String r1 = com.fossil.r01.b
            java.lang.String r2 = "Error in Data#fromByteArray: "
            android.util.Log.e(r1, r2, r0)
            goto L_0x002f
        L_0x0059:
            r0 = move-exception
            java.lang.String r1 = com.fossil.r01.b
            java.lang.String r2 = "Error in Data#fromByteArray: "
            android.util.Log.e(r1, r2, r0)
            goto L_0x004c
        L_0x0062:
            r0 = move-exception
            r2 = r1
        L_0x0064:
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x006d }
        L_0x0069:
            r4.close()     // Catch:{ IOException -> 0x0076 }
        L_0x006c:
            throw r0
        L_0x006d:
            r1 = move-exception
            java.lang.String r2 = com.fossil.r01.b
            java.lang.String r3 = "Error in Data#fromByteArray: "
            android.util.Log.e(r2, r3, r1)
            goto L_0x0069
        L_0x0076:
            r1 = move-exception
            java.lang.String r2 = com.fossil.r01.b
            java.lang.String r3 = "Error in Data#fromByteArray: "
            android.util.Log.e(r2, r3, r1)
            goto L_0x006c
        L_0x007f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Data cannot occupy more than 10240 bytes when serialized"
            r0.<init>(r1)
            throw r0
        L_0x0087:
            r0 = move-exception
            goto L_0x003f
        L_0x0089:
            r0 = move-exception
            goto L_0x0064
        L_0x008b:
            r0 = move-exception
            goto L_0x0040
        L_0x008d:
            r0 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r01.g(byte[]):com.fossil.r01");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004a A[SYNTHETIC, Splitter:B:13:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0082 A[SYNTHETIC, Splitter:B:33:0x0082] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] k(com.fossil.r01 r6) {
        /*
            r2 = 0
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>()
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x00ad }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00ad }
            int r1 = r6.j()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            r3.writeInt(r1)     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            java.util.Map<java.lang.String, java.lang.Object> r1 = r6.f3055a     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            java.util.Set r1 = r1.entrySet()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
        L_0x001c:
            boolean r1 = r5.hasNext()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            if (r1 == 0) goto L_0x0051
            java.lang.Object r1 = r5.next()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            r0 = r1
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            r2 = r0
            java.lang.Object r1 = r2.getKey()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            r3.writeUTF(r1)     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            java.lang.Object r1 = r2.getValue()     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            r3.writeObject(r1)     // Catch:{ IOException -> 0x003b, all -> 0x00af }
            goto L_0x001c
        L_0x003b:
            r1 = move-exception
            r2 = r3
        L_0x003d:
            java.lang.String r3 = com.fossil.r01.b     // Catch:{ all -> 0x007e }
            java.lang.String r5 = "Error in Data#toByteArray: "
            android.util.Log.e(r3, r5, r1)     // Catch:{ all -> 0x007e }
            byte[] r1 = r4.toByteArray()     // Catch:{ all -> 0x007e }
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ IOException -> 0x0089 }
        L_0x004d:
            r4.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0050:
            return r1
        L_0x0051:
            r3.close()     // Catch:{ IOException -> 0x0064 }
        L_0x0054:
            r4.close()     // Catch:{ IOException -> 0x006d }
        L_0x0057:
            int r1 = r4.size()
            r2 = 10240(0x2800, float:1.4349E-41)
            if (r1 > r2) goto L_0x0076
            byte[] r1 = r4.toByteArray()
            goto L_0x0050
        L_0x0064:
            r1 = move-exception
            java.lang.String r2 = com.fossil.r01.b
            java.lang.String r3 = "Error in Data#toByteArray: "
            android.util.Log.e(r2, r3, r1)
            goto L_0x0054
        L_0x006d:
            r1 = move-exception
            java.lang.String r2 = com.fossil.r01.b
            java.lang.String r3 = "Error in Data#toByteArray: "
            android.util.Log.e(r2, r3, r1)
            goto L_0x0057
        L_0x0076:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "Data cannot occupy more than 10240 bytes when serialized"
            r1.<init>(r2)
            throw r1
        L_0x007e:
            r1 = move-exception
            r3 = r2
        L_0x0080:
            if (r3 == 0) goto L_0x0085
            r3.close()     // Catch:{ IOException -> 0x009b }
        L_0x0085:
            r4.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x0088:
            throw r1
        L_0x0089:
            r2 = move-exception
            java.lang.String r3 = com.fossil.r01.b
            java.lang.String r5 = "Error in Data#toByteArray: "
            android.util.Log.e(r3, r5, r2)
            goto L_0x004d
        L_0x0092:
            r2 = move-exception
            java.lang.String r3 = com.fossil.r01.b
            java.lang.String r4 = "Error in Data#toByteArray: "
            android.util.Log.e(r3, r4, r2)
            goto L_0x0050
        L_0x009b:
            r2 = move-exception
            java.lang.String r3 = com.fossil.r01.b
            java.lang.String r5 = "Error in Data#toByteArray: "
            android.util.Log.e(r3, r5, r2)
            goto L_0x0085
        L_0x00a4:
            r2 = move-exception
            java.lang.String r3 = com.fossil.r01.b
            java.lang.String r4 = "Error in Data#toByteArray: "
            android.util.Log.e(r3, r4, r2)
            goto L_0x0088
        L_0x00ad:
            r1 = move-exception
            goto L_0x003d
        L_0x00af:
            r1 = move-exception
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r01.k(com.fossil.r01):byte[]");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (this == obj) {
            return true;
        }
        if (obj == null || r01.class != obj.getClass()) {
            return false;
        }
        r01 r01 = (r01) obj;
        Set<String> keySet = this.f3055a.keySet();
        if (!keySet.equals(r01.f3055a.keySet())) {
            return false;
        }
        for (String str : keySet) {
            Object obj2 = this.f3055a.get(str);
            Object obj3 = r01.f3055a.get(str);
            if (obj2 == null || obj3 == null) {
                if (obj2 == obj3) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
            } else if (!(obj2 instanceof Object[]) || !(obj3 instanceof Object[])) {
                z = obj2.equals(obj3);
                continue;
            } else {
                z = Arrays.deepEquals((Object[]) obj2, (Object[]) obj3);
                continue;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public Map<String, Object> h() {
        return Collections.unmodifiableMap(this.f3055a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f3055a.hashCode() * 31;
    }

    @DexIgnore
    public String i(String str) {
        Object obj = this.f3055a.get(str);
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }

    @DexIgnore
    public int j() {
        return this.f3055a.size();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("Data {");
        if (!this.f3055a.isEmpty()) {
            for (String str : this.f3055a.keySet()) {
                sb.append(str);
                sb.append(" : ");
                Object obj = this.f3055a.get(str);
                if (obj instanceof Object[]) {
                    sb.append(Arrays.toString((Object[]) obj));
                } else {
                    sb.append(obj);
                }
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }
}
