package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj6 extends pv5 implements zi6 {
    @DexIgnore
    public g37<f75> g;
    @DexIgnore
    public yi6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements RecyclerViewHeartRateCalendar.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aj6 f277a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(aj6 aj6) {
            this.f277a = aj6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.b
        public void a(Calendar calendar) {
            pq7.c(calendar, "calendar");
            yi6 K6 = aj6.K6(this.f277a);
            Date time = calendar.getTime();
            pq7.b(time, "calendar.time");
            K6.n(time);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewHeartRateCalendar.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aj6 f278a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(aj6 aj6) {
            this.f278a = aj6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.a
        public void k0(int i, Calendar calendar) {
            pq7.c(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.f278a.getActivity();
            if (activity != null) {
                HeartRateDetailActivity.a aVar = HeartRateDetailActivity.C;
                Date time = calendar.getTime();
                pq7.b(time, "it.time");
                pq7.b(activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ yi6 K6(aj6 aj6) {
        yi6 yi6 = aj6.h;
        if (yi6 != null) {
            return yi6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HeartRateOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(yi6 yi6) {
        pq7.c(yi6, "presenter");
        this.h = yi6;
    }

    @DexIgnore
    @Override // com.fossil.zi6
    public void e(TreeMap<Long, Integer> treeMap) {
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar;
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar2;
        pq7.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        g37<f75> g37 = this.g;
        if (g37 != null) {
            f75 a2 = g37.a();
            if (!(a2 == null || (recyclerViewHeartRateCalendar2 = a2.q) == null)) {
                recyclerViewHeartRateCalendar2.setData(treeMap);
            }
            g37<f75> g372 = this.g;
            if (g372 != null) {
                f75 a3 = g372.a();
                if (a3 != null && (recyclerViewHeartRateCalendar = a3.q) != null) {
                    recyclerViewHeartRateCalendar.setEnableButtonNextAndPrevMonth(true);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zi6
    public void g(Date date, Date date2) {
        pq7.c(date, "selectDate");
        pq7.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        g37<f75> g37 = this.g;
        if (g37 != null) {
            f75 a2 = g37.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                pq7.b(instance, "selectCalendar");
                instance.setTime(date);
                pq7.b(instance2, "startCalendar");
                instance2.setTime(lk5.V(date2));
                pq7.b(instance3, "endCalendar");
                instance3.setTime(lk5.E(instance3.getTime()));
                a2.q.K(instance, instance2, instance3);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onCreateView");
        f75 f75 = (f75) aq0.f(layoutInflater, 2131558566, viewGroup, false, A6());
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = f75.q;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        recyclerViewHeartRateCalendar.setEndDate(instance);
        f75.q.setOnCalendarMonthChanged(new a(this));
        f75.q.setOnCalendarItemClickListener(new b(this));
        g37<f75> g37 = new g37<>(this, f75);
        this.g = g37;
        if (g37 != null) {
            f75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onResume");
        yi6 yi6 = this.h;
        if (yi6 != null) {
            yi6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onStop");
        yi6 yi6 = this.h;
        if (yi6 != null) {
            yi6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
