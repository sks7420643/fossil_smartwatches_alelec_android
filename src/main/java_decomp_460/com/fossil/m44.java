package com.fossil;

import com.google.j2objc.annotations.Weak;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m44<E> extends s24<E> {
    @DexIgnore
    @Weak
    public /* final */ u24<E> delegate;
    @DexIgnore
    public /* final */ y24<? extends E> delegateList;

    @DexIgnore
    public m44(u24<E> u24, y24<? extends E> y24) {
        this.delegate = u24;
        this.delegateList = y24;
    }

    @DexIgnore
    public m44(u24<E> u24, Object[] objArr) {
        this(u24, y24.asImmutableList(objArr));
    }

    @DexIgnore
    @Override // com.fossil.u24, com.fossil.y24
    public int copyIntoArray(Object[] objArr, int i) {
        return this.delegateList.copyIntoArray(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.s24
    public u24<E> delegateCollection() {
        return this.delegate;
    }

    @DexIgnore
    public y24<? extends E> delegateList() {
        return this.delegateList;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return (E) this.delegateList.get(i);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: com.fossil.i54<? extends E>, com.fossil.i54<E> */
    @Override // java.util.List, com.fossil.y24, com.fossil.y24
    public i54<E> listIterator(int i) {
        return (i54<? extends E>) this.delegateList.listIterator(i);
    }
}
