package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.n04;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn6 extends pv5 implements sn6 {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ String g; // = qn5.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String h; // = qn5.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String i; // = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public int j;
    @DexIgnore
    public rn6 k;
    @DexIgnore
    public gr4 l;
    @DexIgnore
    public g37<f85> m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final tn6 a() {
            return new tn6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f85 f3445a;
        @DexIgnore
        public /* final */ /* synthetic */ tn6 b;

        @DexIgnore
        public b(f85 f85, tn6 tn6) {
            this.f3445a = f85;
            this.b = tn6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.h)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwareFragment", "set icon color " + this.b.h);
                int parseColor = Color.parseColor(this.b.h);
                TabLayout.g v = this.f3445a.t.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.g) && this.b.j != i) {
                int parseColor2 = Color.parseColor(this.b.g);
                TabLayout.g v2 = this.f3445a.t.v(this.b.j);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tn6 f3446a;

        @DexIgnore
        public c(tn6 tn6) {
            this.f3446a = tn6;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f3446a.g) && !TextUtils.isEmpty(this.f3446a.h)) {
                int parseColor = Color.parseColor(this.f3446a.g);
                int parseColor2 = Color.parseColor(this.f3446a.h);
                gVar.o(2131230966);
                if (i == this.f3446a.j) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: O6 */
    public void M5(rn6 rn6) {
        pq7.c(rn6, "presenter");
        this.k = rn6;
    }

    @DexIgnore
    public final void P6() {
        g37<f85> g37 = this.m;
        if (g37 != null) {
            f85 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.t : null;
            if (tabLayout != null) {
                g37<f85> g372 = this.m;
                if (g372 != null) {
                    f85 a3 = g372.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.w : null;
                    if (viewPager2 != null) {
                        new n04(tabLayout, viewPager2, new c(this)).a();
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sn6
    public void X(List<? extends Explore> list) {
        pq7.c(list, "data");
        gr4 gr4 = this.l;
        if (gr4 != null) {
            gr4.h(list);
        } else {
            pq7.n("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sn6
    public void Z(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        if (isActive()) {
            g37<f85> g37 = this.m;
            if (g37 != null) {
                f85 a2 = g37.a();
                if (a2 != null && (flexibleProgressBar = a2.u) != null) {
                    flexibleProgressBar.setProgress(i2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        f85 f85 = (f85) aq0.f(layoutInflater, 2131558579, viewGroup, false, A6());
        this.m = new g37<>(this, f85);
        pq7.b(f85, "binding");
        return f85.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        rn6 rn6 = this.k;
        if (rn6 == null) {
            return;
        }
        if (rn6 != null) {
            rn6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        rn6 rn6 = this.k;
        if (rn6 == null) {
            return;
        }
        if (rn6 != null) {
            rn6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        this.l = new gr4(new ArrayList());
        g37<f85> g37 = this.m;
        if (g37 != null) {
            f85 a2 = g37.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.u;
                pq7.b(flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleTextView flexibleTextView = a2.s;
                pq7.b(flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                ViewPager2 viewPager2 = a2.w;
                pq7.b(viewPager2, "binding.rvpTutorial");
                gr4 gr4 = this.l;
                if (gr4 != null) {
                    viewPager2.setAdapter(gr4);
                    if (!TextUtils.isEmpty(this.i)) {
                        TabLayout tabLayout = a2.t;
                        pq7.b(tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.i)));
                    }
                    P6();
                    a2.w.g(new b(a2, this));
                    return;
                }
                pq7.n("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
