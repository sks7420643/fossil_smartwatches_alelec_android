package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa4 extends ta4.d.f {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3226a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.f.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3227a;

        @DexIgnore
        @Override // com.fossil.ta4.d.f.a
        public ta4.d.f a() {
            String str = "";
            if (this.f3227a == null) {
                str = " identifier";
            }
            if (str.isEmpty()) {
                return new sa4(this.f3227a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.f.a
        public ta4.d.f.a b(String str) {
            if (str != null) {
                this.f3227a = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }
    }

    @DexIgnore
    public sa4(String str) {
        this.f3226a = str;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.f
    public String b() {
        return this.f3226a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ta4.d.f) {
            return this.f3226a.equals(((ta4.d.f) obj).b());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f3226a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "User{identifier=" + this.f3226a + "}";
    }
}
