package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum y03 {
    DOUBLE(0, a13.SCALAR, n13.DOUBLE),
    FLOAT(1, a13.SCALAR, n13.FLOAT),
    INT64(2, a13.SCALAR, n13.LONG),
    UINT64(3, a13.SCALAR, n13.LONG),
    INT32(4, a13.SCALAR, n13.INT),
    FIXED64(5, a13.SCALAR, n13.LONG),
    FIXED32(6, a13.SCALAR, n13.INT),
    BOOL(7, a13.SCALAR, n13.BOOLEAN),
    STRING(8, a13.SCALAR, n13.STRING),
    MESSAGE(9, a13.SCALAR, n13.MESSAGE),
    BYTES(10, a13.SCALAR, n13.BYTE_STRING),
    UINT32(11, a13.SCALAR, n13.INT),
    ENUM(12, a13.SCALAR, n13.ENUM),
    SFIXED32(13, a13.SCALAR, n13.INT),
    SFIXED64(14, a13.SCALAR, n13.LONG),
    SINT32(15, a13.SCALAR, n13.INT),
    SINT64(16, a13.SCALAR, n13.LONG),
    GROUP(17, a13.SCALAR, n13.MESSAGE),
    DOUBLE_LIST(18, a13.VECTOR, n13.DOUBLE),
    FLOAT_LIST(19, a13.VECTOR, n13.FLOAT),
    INT64_LIST(20, a13.VECTOR, n13.LONG),
    UINT64_LIST(21, a13.VECTOR, n13.LONG),
    INT32_LIST(22, a13.VECTOR, n13.INT),
    FIXED64_LIST(23, a13.VECTOR, n13.LONG),
    FIXED32_LIST(24, a13.VECTOR, n13.INT),
    BOOL_LIST(25, a13.VECTOR, n13.BOOLEAN),
    STRING_LIST(26, a13.VECTOR, n13.STRING),
    MESSAGE_LIST(27, a13.VECTOR, n13.MESSAGE),
    BYTES_LIST(28, a13.VECTOR, n13.BYTE_STRING),
    UINT32_LIST(29, a13.VECTOR, n13.INT),
    ENUM_LIST(30, a13.VECTOR, n13.ENUM),
    SFIXED32_LIST(31, a13.VECTOR, n13.INT),
    SFIXED64_LIST(32, a13.VECTOR, n13.LONG),
    SINT32_LIST(33, a13.VECTOR, n13.INT),
    SINT64_LIST(34, a13.VECTOR, n13.LONG),
    DOUBLE_LIST_PACKED(35, a13.PACKED_VECTOR, n13.DOUBLE),
    FLOAT_LIST_PACKED(36, a13.PACKED_VECTOR, n13.FLOAT),
    INT64_LIST_PACKED(37, a13.PACKED_VECTOR, n13.LONG),
    UINT64_LIST_PACKED(38, a13.PACKED_VECTOR, n13.LONG),
    INT32_LIST_PACKED(39, a13.PACKED_VECTOR, n13.INT),
    FIXED64_LIST_PACKED(40, a13.PACKED_VECTOR, n13.LONG),
    FIXED32_LIST_PACKED(41, a13.PACKED_VECTOR, n13.INT),
    BOOL_LIST_PACKED(42, a13.PACKED_VECTOR, n13.BOOLEAN),
    UINT32_LIST_PACKED(43, a13.PACKED_VECTOR, n13.INT),
    ENUM_LIST_PACKED(44, a13.PACKED_VECTOR, n13.ENUM),
    SFIXED32_LIST_PACKED(45, a13.PACKED_VECTOR, n13.INT),
    SFIXED64_LIST_PACKED(46, a13.PACKED_VECTOR, n13.LONG),
    SINT32_LIST_PACKED(47, a13.PACKED_VECTOR, n13.INT),
    SINT64_LIST_PACKED(48, a13.PACKED_VECTOR, n13.LONG),
    GROUP_LIST(49, a13.VECTOR, n13.MESSAGE),
    MAP(50, a13.MAP, n13.VOID);
    
    @DexIgnore
    public static /* final */ y03[] d0;
    @DexIgnore
    public /* final */ n13 zzaz;
    @DexIgnore
    public /* final */ int zzba;
    @DexIgnore
    public /* final */ a13 zzbb;
    @DexIgnore
    public /* final */ Class<?> zzbc;
    @DexIgnore
    public /* final */ boolean zzbd;

    /*
    static {
        y03[] values = values();
        d0 = new y03[values.length];
        for (y03 y03 : values) {
            d0[y03.zzba] = y03;
        }
    }
    */

    @DexIgnore
    public y03(int i, a13 a13, n13 n13) {
        int i2;
        boolean z = true;
        this.zzba = i;
        this.zzbb = a13;
        this.zzaz = n13;
        int i3 = x03.f4027a[a13.ordinal()];
        if (i3 == 1) {
            this.zzbc = n13.zza();
        } else if (i3 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = n13.zza();
        }
        this.zzbd = (a13 != a13.SCALAR || (i2 = x03.b[n13.ordinal()]) == 1 || i2 == 2 || i2 == 3) ? false : z;
    }

    @DexIgnore
    public final int zza() {
        return this.zzba;
    }
}
