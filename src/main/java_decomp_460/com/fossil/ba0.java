package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ba0 extends Enum<ba0> {
    @DexIgnore
    public static /* final */ ba0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ ba0[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        ba0 ba0 = new ba0("EVENT_MAPPING", 0, (byte) 0);
        ba0 ba02 = new ba0("KEY_CODE_MAPPING", 1, (byte) 1);
        ba0 ba03 = new ba0("GOAL_TRACKING", 2, (byte) 2);
        c = ba03;
        d = new ba0[]{ba0, ba02, ba03, new ba0("UNDEFINED", 3, (byte) 255)};
    }
    */

    @DexIgnore
    public ba0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static ba0 valueOf(String str) {
        return (ba0) Enum.valueOf(ba0.class, str);
    }

    @DexIgnore
    public static ba0[] values() {
        return (ba0[]) d.clone();
    }
}
