package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ih7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ gh7 e;

    @DexIgnore
    public ih7(gh7 gh7, List list, boolean z, boolean z2) {
        this.e = gh7;
        this.b = list;
        this.c = z;
        this.d = z2;
    }

    @DexIgnore
    public void run() {
        this.e.q(this.b, this.c);
        if (this.d) {
            this.b.clear();
        }
    }
}
