package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d90 extends z80 {
    @DexIgnore
    public static long h; // = 60000;
    @DexIgnore
    public static /* final */ d90 i; // = new d90();

    @DexIgnore
    public d90() {
        super("sdk_log", 102400, 20971520, "sdk_log", "sdklog", new zw1("", "", ""), 1800, new b90(), ld0.c, true);
    }

    @DexIgnore
    @Override // com.fossil.z80
    public long e() {
        return h;
    }

    @DexIgnore
    public final void i(Exception exc) {
    }
}
