package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w56 implements Factory<v56> {
    @DexIgnore
    public static v56 a(p56 p56, int i, ArrayList<j06> arrayList, LoaderManager loaderManager, uq4 uq4, y56 y56) {
        return new v56(p56, i, arrayList, loaderManager, uq4, y56);
    }
}
