package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jo7 extends io7 implements mq7<Object> {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public jo7(int i) {
        this(i, null);
    }

    @DexIgnore
    public jo7(int i, qn7<Object> qn7) {
        super(qn7);
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public String toString() {
        if (getCompletion() != null) {
            return super.toString();
        }
        String h = er7.h(this);
        pq7.b(h, "Reflection.renderLambdaToString(this)");
        return h;
    }
}
