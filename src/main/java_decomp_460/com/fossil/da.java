package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da extends ux1<iw1> {
    @DexIgnore
    public static /* final */ da b; // = new da();

    @DexIgnore
    public da() {
        super(new ry1(3, 0));
    }

    @DexIgnore
    /* renamed from: d */
    public byte[] b(iw1 iw1) {
        ByteBuffer order = ByteBuffer.allocate(64).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(PACK\u2026(ByteOrder.LITTLE_ENDIAN)");
        cc0[] f = iw1.f();
        cc0[] b2 = iw1.b();
        cc0[] d = iw1.d();
        cc0[] e = iw1.e();
        cc0[] c = iw1.c();
        cc0[] a2 = iw1.a();
        cc0[] i = iw1.i();
        byte[] bArr = new byte[0];
        int i2 = 88;
        for (int i3 = 0; i3 < 7; i3++) {
            cc0[] cc0Arr = new cc0[][]{f, b2, d, e, c, a2, i}[i3];
            order.putInt(i2);
            for (cc0 cc0 : cc0Arr) {
                String a3 = iy1.a(cc0.b);
                Charset c2 = hd0.y.c();
                if (a3 != null) {
                    byte[] bytes = a3.getBytes(c2);
                    pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] array = ByteBuffer.allocate(bytes.length + 1 + 2 + cc0.c.length).order(ByteOrder.LITTLE_ENDIAN).put((byte) bytes.length).put(bytes).putShort((short) cc0.c.length).put(cc0.c).array();
                    pq7.b(array, "ByteBuffer.allocate(\n   \u2026\n                .array()");
                    bArr = dm7.q(bArr, array);
                    i2 += array.length;
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        byte[] a4 = iw1.g().a();
        byte[] array2 = order.array();
        pq7.b(array2, "metaData.array()");
        return dm7.q(dm7.q(a4, array2), bArr);
    }
}
