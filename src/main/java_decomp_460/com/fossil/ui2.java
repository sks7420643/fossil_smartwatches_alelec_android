package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.fitness.data.MapValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui2 implements Parcelable.Creator<MapValue> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ MapValue createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i = ad2.v(parcel, t);
            } else if (l != 2) {
                ad2.B(parcel, t);
            } else {
                f = ad2.r(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new MapValue(i, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ MapValue[] newArray(int i) {
        return new MapValue[i];
    }
}
