package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx7 implements dw7, qu7 {
    @DexIgnore
    public static /* final */ lx7 b; // = new lx7();

    @DexIgnore
    @Override // com.fossil.qu7
    public boolean c(Throwable th) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.dw7
    public void dispose() {
    }

    @DexIgnore
    public String toString() {
        return "NonDisposableHandle";
    }
}
