package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xw1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ yw1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<xw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public xw1 createFromParcel(Parcel parcel) {
            yw1 yw1 = yw1.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = rc0.f3097a[yw1.ordinal()];
            if (i == 1) {
                return tc0.CREATOR.a(parcel);
            }
            if (i == 2) {
                return ww1.CREATOR.a(parcel);
            }
            throw new al7();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public xw1[] newArray(int i) {
            return new xw1[i];
        }
    }

    @DexIgnore
    public xw1(yw1 yw1) {
        this.b = yw1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((xw1) obj).b;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
    }
}
