package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* renamed from: com.fossil.do  reason: invalid class name */
public final class Cdo extends lp {
    @DexIgnore
    public /* final */ bu1 C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Cdo(k5 k5Var, i60 i60, bu1 bu1, String str, int i) {
        super(k5Var, i60, yp.J0, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = bu1;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        byte[] bArr;
        kp1 b = this.C.b();
        if (b == null || b.d()) {
            I(this.C);
            return;
        }
        try {
            xa xaVar = xa.d;
            ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.ASSET.b));
            if (ry1 == null) {
                ry1 = hd0.y.d();
            }
            bArr = xaVar.a(1796, ry1, new kp1[]{this.C.b()});
        } catch (sx1 e) {
            bArr = new byte[0];
        }
        lp.h(this, new zj(this.w, this.x, yp.K0, true, 1796, bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new fn(this), new rn(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void I(bu1 bu1) {
        lp.h(this, new cn(this.w, this.x, bu1, ob.UI_SCRIPT, null, 16), new hm(this), new tm(this), null, null, null, 56, null);
    }
}
