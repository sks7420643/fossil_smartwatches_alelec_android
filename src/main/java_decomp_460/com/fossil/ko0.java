package com.fossil;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object f1939a;

    @DexIgnore
    public ko0(Object obj) {
        this.f1939a = obj;
    }

    @DexIgnore
    public static ko0 b(Context context, int i) {
        return Build.VERSION.SDK_INT >= 24 ? new ko0(PointerIcon.getSystemIcon(context, i)) : new ko0(null);
    }

    @DexIgnore
    public Object a() {
        return this.f1939a;
    }
}
