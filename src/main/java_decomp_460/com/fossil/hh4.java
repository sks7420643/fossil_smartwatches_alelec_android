package com.fossil;

import com.fossil.kh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh4 extends kh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1478a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ kh4.b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends kh4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f1479a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public kh4.b c;

        @DexIgnore
        @Override // com.fossil.kh4.a
        public kh4 a() {
            String str = "";
            if (this.b == null) {
                str = " tokenExpirationTimestamp";
            }
            if (str.isEmpty()) {
                return new hh4(this.f1479a, this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.kh4.a
        public kh4.a b(kh4.b bVar) {
            this.c = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.kh4.a
        public kh4.a c(String str) {
            this.f1479a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.kh4.a
        public kh4.a d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public hh4(String str, long j, kh4.b bVar) {
        this.f1478a = str;
        this.b = j;
        this.c = bVar;
    }

    @DexIgnore
    @Override // com.fossil.kh4
    public kh4.b b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.kh4
    public String c() {
        return this.f1478a;
    }

    @DexIgnore
    @Override // com.fossil.kh4
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof kh4)) {
            return false;
        }
        kh4 kh4 = (kh4) obj;
        String str = this.f1478a;
        if (str != null ? str.equals(kh4.c()) : kh4.c() == null) {
            if (this.b == kh4.d()) {
                kh4.b bVar = this.c;
                if (bVar == null) {
                    if (kh4.b() == null) {
                        return true;
                    }
                } else if (bVar.equals(kh4.b())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f1478a;
        int hashCode = str == null ? 0 : str.hashCode();
        long j = this.b;
        int i2 = (int) (j ^ (j >>> 32));
        kh4.b bVar = this.c;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return ((((hashCode ^ 1000003) * 1000003) ^ i2) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "TokenResult{token=" + this.f1478a + ", tokenExpirationTimestamp=" + this.b + ", responseCode=" + this.c + "}";
    }
}
