package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya1 implements Closeable {
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public long g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public long i; // = 0;
    @DexIgnore
    public Writer j;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> k; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int l;
    @DexIgnore
    public long m; // = 0;
    @DexIgnore
    public /* final */ ThreadPoolExecutor s; // = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new b(null));
    @DexIgnore
    public /* final */ Callable<Void> t; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            synchronized (ya1.this) {
                if (ya1.this.j != null) {
                    ya1.this.g0();
                    if (ya1.this.M()) {
                        ya1.this.V();
                        ya1.this.l = 0;
                    }
                }
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ThreadFactory {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread;
            synchronized (this) {
                thread = new Thread(runnable, "glide-disk-lru-cache-thread");
                thread.setPriority(1);
            }
            return thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d f4284a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c(d dVar) {
            this.f4284a = dVar;
            this.b = dVar.e ? null : new boolean[ya1.this.h];
        }

        @DexIgnore
        public /* synthetic */ c(ya1 ya1, d dVar, a aVar) {
            this(dVar);
        }

        @DexIgnore
        public void a() throws IOException {
            ya1.this.A(this, false);
        }

        @DexIgnore
        public void b() {
            if (!this.c) {
                try {
                    a();
                } catch (IOException e) {
                }
            }
        }

        @DexIgnore
        public void e() throws IOException {
            ya1.this.A(this, true);
            this.c = true;
        }

        @DexIgnore
        public File f(int i) throws IOException {
            File k;
            synchronized (ya1.this) {
                if (this.f4284a.f == this) {
                    if (!this.f4284a.e) {
                        this.b[i] = true;
                    }
                    k = this.f4284a.k(i);
                    if (!ya1.this.b.exists()) {
                        ya1.this.b.mkdirs();
                    }
                } else {
                    throw new IllegalStateException();
                }
            }
            return k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f4285a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public File[] c;
        @DexIgnore
        public File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public d(String str) {
            this.f4285a = str;
            this.b = new long[ya1.this.h];
            this.c = new File[ya1.this.h];
            this.d = new File[ya1.this.h];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < ya1.this.h; i++) {
                sb.append(i);
                this.c[i] = new File(ya1.this.b, sb.toString());
                sb.append(".tmp");
                this.d[i] = new File(ya1.this.b, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public /* synthetic */ d(ya1 ya1, String str, a aVar) {
            this(str);
        }

        @DexIgnore
        public File j(int i) {
            return this.c[i];
        }

        @DexIgnore
        public File k(int i) {
            return this.d[i];
        }

        @DexIgnore
        public String l() throws IOException {
            StringBuilder sb = new StringBuilder();
            long[] jArr = this.b;
            for (long j : jArr) {
                sb.append(' ');
                sb.append(j);
            }
            return sb.toString();
        }

        @DexIgnore
        public final IOException m(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public final void n(String[] strArr) throws IOException {
            if (strArr.length == ya1.this.h) {
                for (int i = 0; i < strArr.length; i++) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                    } catch (NumberFormatException e2) {
                        m(strArr);
                        throw null;
                    }
                }
                return;
            }
            m(strArr);
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ File[] f4286a;

        @DexIgnore
        public e(String str, long j, File[] fileArr, long[] jArr) {
            this.f4286a = fileArr;
        }

        @DexIgnore
        public /* synthetic */ e(ya1 ya1, String str, long j, File[] fileArr, long[] jArr, a aVar) {
            this(str, j, fileArr, jArr);
        }

        @DexIgnore
        public File a(int i) {
            return this.f4286a[i];
        }
    }

    @DexIgnore
    public ya1(File file, int i2, int i3, long j2) {
        this.b = file;
        this.f = i2;
        this.c = new File(file, "journal");
        this.d = new File(file, "journal.tmp");
        this.e = new File(file, "journal.bkp");
        this.h = i3;
        this.g = j2;
    }

    @DexIgnore
    public static void C(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void G(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.flush();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.flush();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    public static ya1 P(File file, int i2, int i3, long j2) throws IOException {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    b0(file2, file3, false);
                }
            }
            ya1 ya1 = new ya1(file, i2, i3, j2);
            if (ya1.c.exists()) {
                try {
                    ya1.S();
                    ya1.Q();
                    return ya1;
                } catch (IOException e2) {
                    PrintStream printStream = System.out;
                    printStream.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    ya1.B();
                }
            }
            file.mkdirs();
            ya1 ya12 = new ya1(file, i2, i3, j2);
            ya12.V();
            return ya12;
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public static void b0(File file, File file2, boolean z) throws IOException {
        if (z) {
            C(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void o(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.close();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.close();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    public final void A(c cVar, boolean z) throws IOException {
        synchronized (this) {
            d dVar = cVar.f4284a;
            if (dVar.f == cVar) {
                if (z && !dVar.e) {
                    for (int i2 = 0; i2 < this.h; i2++) {
                        if (!cVar.b[i2]) {
                            cVar.a();
                            throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                        } else if (!dVar.k(i2).exists()) {
                            cVar.a();
                            return;
                        }
                    }
                }
                for (int i3 = 0; i3 < this.h; i3++) {
                    File k2 = dVar.k(i3);
                    if (!z) {
                        C(k2);
                    } else if (k2.exists()) {
                        File j2 = dVar.j(i3);
                        k2.renameTo(j2);
                        long j3 = dVar.b[i3];
                        long length = j2.length();
                        dVar.b[i3] = length;
                        this.i = (this.i - j3) + length;
                    }
                }
                this.l++;
                dVar.f = null;
                if (dVar.e || z) {
                    dVar.e = true;
                    this.j.append((CharSequence) "CLEAN");
                    this.j.append(' ');
                    this.j.append((CharSequence) dVar.f4285a);
                    this.j.append((CharSequence) dVar.l());
                    this.j.append('\n');
                    if (z) {
                        long j4 = this.m;
                        this.m = 1 + j4;
                        dVar.g = j4;
                    }
                } else {
                    this.k.remove(dVar.f4285a);
                    this.j.append((CharSequence) "REMOVE");
                    this.j.append(' ');
                    this.j.append((CharSequence) dVar.f4285a);
                    this.j.append('\n');
                }
                G(this.j);
                if (this.i > this.g || M()) {
                    this.s.submit(this.t);
                }
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void B() throws IOException {
        close();
        ab1.b(this.b);
    }

    @DexIgnore
    public c D(String str) throws IOException {
        return F(str, -1);
    }

    @DexIgnore
    public final c F(String str, long j2) throws IOException {
        d dVar;
        synchronized (this) {
            m();
            d dVar2 = this.k.get(str);
            if (j2 != -1 && (dVar2 == null || dVar2.g != j2)) {
                return null;
            }
            if (dVar2 == null) {
                d dVar3 = new d(this, str, null);
                this.k.put(str, dVar3);
                dVar = dVar3;
            } else if (dVar2.f != null) {
                return null;
            } else {
                dVar = dVar2;
            }
            c cVar = new c(this, dVar, null);
            dVar.f = cVar;
            this.j.append((CharSequence) "DIRTY");
            this.j.append(' ');
            this.j.append((CharSequence) str);
            this.j.append('\n');
            G(this.j);
            return cVar;
        }
    }

    @DexIgnore
    public e L(String str) throws IOException {
        e eVar = null;
        synchronized (this) {
            m();
            d dVar = this.k.get(str);
            if (dVar != null) {
                if (dVar.e) {
                    File[] fileArr = dVar.c;
                    int length = fileArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            this.l++;
                            this.j.append((CharSequence) "READ");
                            this.j.append(' ');
                            this.j.append((CharSequence) str);
                            this.j.append('\n');
                            if (M()) {
                                this.s.submit(this.t);
                            }
                            eVar = new e(this, str, dVar.g, dVar.c, dVar.b, null);
                        } else if (!fileArr[i2].exists()) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
        }
        return eVar;
    }

    @DexIgnore
    public final boolean M() {
        int i2 = this.l;
        return i2 >= 2000 && i2 >= this.k.size();
    }

    @DexIgnore
    public final void Q() throws IOException {
        C(this.d);
        Iterator<d> it = this.k.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            if (next.f == null) {
                for (int i2 = 0; i2 < this.h; i2++) {
                    this.i += next.b[i2];
                }
            } else {
                next.f = null;
                for (int i3 = 0; i3 < this.h; i3++) {
                    C(next.j(i3));
                    C(next.k(i3));
                }
                it.remove();
            }
        }
    }

    @DexIgnore
    public final void S() throws IOException {
        za1 za1 = new za1(new FileInputStream(this.c), ab1.f240a);
        try {
            String f2 = za1.f();
            String f3 = za1.f();
            String f4 = za1.f();
            String f5 = za1.f();
            String f6 = za1.f();
            if (!"libcore.io.DiskLruCache".equals(f2) || !"1".equals(f3) || !Integer.toString(this.f).equals(f4) || !Integer.toString(this.h).equals(f5) || !"".equals(f6)) {
                throw new IOException("unexpected journal header: [" + f2 + ", " + f3 + ", " + f5 + ", " + f6 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    T(za1.f());
                    i2++;
                } catch (EOFException e2) {
                    this.l = i2 - this.k.size();
                    if (za1.c()) {
                        V();
                    } else {
                        this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c, true), ab1.f240a));
                    }
                    return;
                }
            }
        } finally {
            ab1.a(za1);
        }
    }

    @DexIgnore
    public final void T(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                String substring = str.substring(i2);
                if (indexOf != 6 || !str.startsWith("REMOVE")) {
                    str2 = substring;
                } else {
                    this.k.remove(substring);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.k.get(str2);
            if (dVar == null) {
                dVar = new d(this, str2, null);
                this.k.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                dVar.e = true;
                dVar.f = null;
                dVar.n(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f = new c(this, dVar, null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void V() throws IOException {
        synchronized (this) {
            if (this.j != null) {
                o(this.j);
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.d), ab1.f240a));
            try {
                bufferedWriter.write("libcore.io.DiskLruCache");
                bufferedWriter.write("\n");
                bufferedWriter.write("1");
                bufferedWriter.write("\n");
                bufferedWriter.write(Integer.toString(this.f));
                bufferedWriter.write("\n");
                bufferedWriter.write(Integer.toString(this.h));
                bufferedWriter.write("\n");
                bufferedWriter.write("\n");
                for (d dVar : this.k.values()) {
                    if (dVar.f != null) {
                        bufferedWriter.write("DIRTY " + dVar.f4285a + '\n');
                    } else {
                        bufferedWriter.write("CLEAN " + dVar.f4285a + dVar.l() + '\n');
                    }
                }
                o(bufferedWriter);
                if (this.c.exists()) {
                    b0(this.c, this.e, true);
                }
                b0(this.d, this.c, false);
                this.e.delete();
                this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c, true), ab1.f240a));
            } catch (Throwable th) {
                o(bufferedWriter);
                throw th;
            }
        }
    }

    @DexIgnore
    public boolean X(String str) throws IOException {
        synchronized (this) {
            m();
            d dVar = this.k.get(str);
            if (dVar == null || dVar.f != null) {
                return false;
            }
            for (int i2 = 0; i2 < this.h; i2++) {
                File j2 = dVar.j(i2);
                if (!j2.exists() || j2.delete()) {
                    this.i -= dVar.b[i2];
                    dVar.b[i2] = 0;
                } else {
                    throw new IOException("failed to delete " + j2);
                }
            }
            this.l++;
            this.j.append((CharSequence) "REMOVE");
            this.j.append(' ');
            this.j.append((CharSequence) str);
            this.j.append('\n');
            this.k.remove(str);
            if (M()) {
                this.s.submit(this.t);
            }
            return true;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            if (this.j != null) {
                Iterator it = new ArrayList(this.k.values()).iterator();
                while (it.hasNext()) {
                    d dVar = (d) it.next();
                    if (dVar.f != null) {
                        dVar.f.a();
                    }
                }
                g0();
                o(this.j);
                this.j = null;
            }
        }
    }

    @DexIgnore
    public final void g0() throws IOException {
        while (this.i > this.g) {
            X(this.k.entrySet().iterator().next().getKey());
        }
    }

    @DexIgnore
    public final void m() {
        if (this.j == null) {
            throw new IllegalStateException("cache is closed");
        }
    }
}
