package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c0 implements Callable<tl7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ k0 f537a;
    @DexIgnore
    public /* final */ /* synthetic */ g0 b;

    @DexIgnore
    public c0(g0 g0Var, k0 k0Var) {
        this.b = g0Var;
        this.f537a = k0Var;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public tl7 call() throws Exception {
        this.b.f1243a.beginTransaction();
        try {
            this.b.b.insert((jw0<k0>) this.f537a);
            this.b.f1243a.setTransactionSuccessful();
            return tl7.f3441a;
        } finally {
            this.b.f1243a.endTransaction();
        }
    }
}
