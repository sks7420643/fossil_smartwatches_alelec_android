package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.t47;
import com.fossil.zs6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws6 extends pv5 implements x47, t47.g {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static String m;
    @DexIgnore
    public static String s;
    @DexIgnore
    public static String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public zs6 h;
    @DexIgnore
    public g37<z45> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ws6.m;
        }

        @DexIgnore
        public final String b() {
            return ws6.l;
        }

        @DexIgnore
        public final String c() {
            return ws6.s;
        }

        @DexIgnore
        public final String d() {
            return ws6.t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<zs6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ws6 f3992a;

        @DexIgnore
        public b(ws6 ws6) {
            this.f3992a = ws6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(zs6.a aVar) {
            if (aVar != null) {
                Integer d = aVar.d();
                if (d != null) {
                    this.f3992a.R6(d.intValue());
                }
                Integer c = aVar.c();
                if (c != null) {
                    this.f3992a.Q6(c.intValue());
                }
                Integer b = aVar.b();
                if (b != null) {
                    this.f3992a.P6(b.intValue());
                }
                Integer a2 = aVar.a();
                if (a2 != null) {
                    this.f3992a.O6(a2.intValue());
                }
                Integer f = aVar.f();
                if (f != null) {
                    this.f3992a.T6(f.intValue());
                }
                Integer e = aVar.e();
                if (e != null) {
                    this.f3992a.S6(e.intValue());
                }
                Integer h = aVar.h();
                if (h != null) {
                    this.f3992a.V6(h.intValue());
                }
                Integer g = aVar.g();
                if (g != null) {
                    this.f3992a.U6(g.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ws6 b;

        @DexIgnore
        public c(ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 401);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ws6 b;

        @DexIgnore
        public d(ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, Action.ActivityTracker.TAG_ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ws6 b;

        @DexIgnore
        public e(ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, MFNetworkReturnCode.WRONG_PASSWORD);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ws6 b;

        @DexIgnore
        public f(ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 404);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ws6 b;

        @DexIgnore
        public g(ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = ws6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeRingChartFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.x47
    public void C3(int i2, int i3) {
        hr7 hr7 = hr7.f1520a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3 + " hexColor=" + format);
        zs6 zs6 = this.h;
        if (zs6 != null) {
            zs6.h(i2, Color.parseColor(format));
            switch (i2) {
                case 401:
                    l = format;
                    return;
                case Action.ActivityTracker.TAG_ACTIVITY /* 402 */:
                    m = format;
                    return;
                case MFNetworkReturnCode.WRONG_PASSWORD /* 403 */:
                    s = format;
                    return;
                case 404:
                    t = format;
                    return;
                default:
                    return;
            }
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.A.setProgressRingColorPreview(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.I.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.B.setProgressRingColorPreview(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            zs6 zs6 = this.h;
            if (zs6 != null) {
                zs6.f(pt6.m.a(), l, m, s, t);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.J.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void S6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.C.setProgressRingColorPreview(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.K.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void U6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.D.setProgressRingColorPreview(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(int i2) {
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.L.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        z45 z45 = (z45) aq0.f(LayoutInflater.from(getContext()), 2131558537, null, false, A6());
        PortfolioApp.h0.c().M().e(new ys6()).a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(zs6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            zs6 zs6 = (zs6) a2;
            this.h = zs6;
            if (zs6 != null) {
                zs6.e().h(getViewLifecycleOwner(), new b(this));
                zs6 zs62 = this.h;
                if (zs62 != null) {
                    zs62.g();
                    this.i = new g37<>(this, z45);
                    pq7.b(z45, "binding");
                    return z45.n();
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
        m = null;
        s = null;
        t = null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        zs6 zs6 = this.h;
        if (zs6 != null) {
            zs6.g();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<z45> g37 = this.i;
        if (g37 != null) {
            z45 a2 = g37.a();
            if (a2 != null) {
                a2.B.setProgress(1.0f);
                a2.A.setProgress(1.0f);
                a2.C.setProgress(1.0f);
                a2.D.setProgress(1.0f);
                a2.x.setOnClickListener(new c(this));
                a2.w.setOnClickListener(new d(this));
                a2.y.setOnClickListener(new e(this));
                a2.z.setOnClickListener(new f(this));
                a2.v.setOnClickListener(new g(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.x47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
