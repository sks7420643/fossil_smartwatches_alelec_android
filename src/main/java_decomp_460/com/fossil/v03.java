package com.fossil;

import com.fossil.v03;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v03<T extends v03<T>> extends Comparable<T> {
    @DexIgnore
    p23 d(p23 p23, m23 m23);

    @DexIgnore
    v23 g(v23 v23, v23 v232);

    @DexIgnore
    int zza();

    @DexIgnore
    l43 zzb();

    @DexIgnore
    s43 zzc();

    @DexIgnore
    boolean zzd();

    @DexIgnore
    boolean zze();
}
