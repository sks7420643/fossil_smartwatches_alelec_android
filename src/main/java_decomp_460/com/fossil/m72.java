package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public final class m72 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static m72 e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2311a;
    @DexIgnore
    public /* final */ Status b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public m72(Context context) {
        boolean z = false;
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(j62.common_google_play_services_unknown_issue));
        if (identifier != 0) {
            this.c = !(resources.getInteger(identifier) != 0 ? true : z);
        } else {
            this.c = false;
        }
        String a2 = qe2.a(context);
        a2 = a2 == null ? new xc2(context).a("google_app_id") : a2;
        if (TextUtils.isEmpty(a2)) {
            this.b = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.f2311a = null;
            return;
        }
        this.f2311a = a2;
        this.b = Status.f;
    }

    @DexIgnore
    public static m72 a(String str) {
        m72 m72;
        synchronized (d) {
            if (e != null) {
                m72 = e;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(CodelessMatcher.CURRENT_CLASS_NAME);
                throw new IllegalStateException(sb.toString());
            }
        }
        return m72;
    }

    @DexIgnore
    public static String b() {
        return a("getGoogleAppId").f2311a;
    }

    @DexIgnore
    public static Status c(Context context) {
        Status status;
        rc2.l(context, "Context must not be null.");
        synchronized (d) {
            if (e == null) {
                e = new m72(context);
            }
            status = e.b;
        }
        return status;
    }

    @DexIgnore
    public static boolean d() {
        return a("isMeasurementExplicitlyDisabled").c;
    }
}
