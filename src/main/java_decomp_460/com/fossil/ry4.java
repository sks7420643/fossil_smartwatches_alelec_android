package com.fossil;

import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ry4 implements MembersInjector<BCNotificationActionReceiver> {
    @DexIgnore
    public static void a(BCNotificationActionReceiver bCNotificationActionReceiver, tt4 tt4) {
        bCNotificationActionReceiver.b = tt4;
    }

    @DexIgnore
    public static void b(BCNotificationActionReceiver bCNotificationActionReceiver, zt4 zt4) {
        bCNotificationActionReceiver.f4691a = zt4;
    }

    @DexIgnore
    public static void c(BCNotificationActionReceiver bCNotificationActionReceiver, on5 on5) {
        bCNotificationActionReceiver.c = on5;
    }
}
