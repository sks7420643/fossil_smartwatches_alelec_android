package com.fossil;

import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr5 implements MembersInjector<FossilFirebaseMessagingService> {
    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, vt4 vt4) {
        fossilFirebaseMessagingService.k = vt4;
    }

    @DexIgnore
    public static void b(FossilFirebaseMessagingService fossilFirebaseMessagingService, pr4 pr4) {
        fossilFirebaseMessagingService.l = pr4;
    }

    @DexIgnore
    public static void c(FossilFirebaseMessagingService fossilFirebaseMessagingService, dr5 dr5) {
        fossilFirebaseMessagingService.i = dr5;
    }

    @DexIgnore
    public static void d(FossilFirebaseMessagingService fossilFirebaseMessagingService, pu6 pu6) {
        fossilFirebaseMessagingService.h = pu6;
    }

    @DexIgnore
    public static void e(FossilFirebaseMessagingService fossilFirebaseMessagingService, on5 on5) {
        fossilFirebaseMessagingService.j = on5;
    }
}
