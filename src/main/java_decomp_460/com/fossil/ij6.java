package com.fossil;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij6 implements Factory<hj6> {
    @DexIgnore
    public static hj6 a(fj6 fj6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new hj6(fj6, userRepository, heartRateSummaryRepository);
    }
}
