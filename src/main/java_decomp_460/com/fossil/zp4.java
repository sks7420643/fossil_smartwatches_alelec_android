package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp4 implements Factory<rl5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f4508a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;

    @DexIgnore
    public zp4(uo4 uo4, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        this.f4508a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static zp4 a(uo4 uo4, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return new zp4(uo4, provider, provider2);
    }

    @DexIgnore
    public static rl5 c(uo4 uo4, DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        rl5 G = uo4.G(deviceRepository, portfolioApp);
        lk7.c(G, "Cannot return null from a non-@Nullable @Provides method");
        return G;
    }

    @DexIgnore
    /* renamed from: b */
    public rl5 get() {
        return c(this.f4508a, this.b.get(), this.c.get());
    }
}
