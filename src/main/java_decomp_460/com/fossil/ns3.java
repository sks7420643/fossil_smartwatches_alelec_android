package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ns3 extends gl2 implements ks3 {
    @DexIgnore
    public ns3() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.gl2
    public boolean X2(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 3:
                o2((z52) il2.b(parcel, z52.CREATOR), (js3) il2.b(parcel, js3.CREATOR));
                break;
            case 4:
                f1((Status) il2.b(parcel, Status.CREATOR));
                break;
            case 5:
            default:
                return false;
            case 6:
                s1((Status) il2.b(parcel, Status.CREATOR));
                break;
            case 7:
                v((Status) il2.b(parcel, Status.CREATOR), (GoogleSignInAccount) il2.b(parcel, GoogleSignInAccount.CREATOR));
                break;
            case 8:
                Q2((us3) il2.b(parcel, us3.CREATOR));
                break;
            case 9:
                M1((os3) il2.b(parcel, os3.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
