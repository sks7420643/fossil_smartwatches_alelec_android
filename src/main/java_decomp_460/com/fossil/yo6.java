package com.fossil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.bp6;
import com.fossil.jn5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo6 extends pv5 implements t47.g {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public bp6 g;
    @DexIgnore
    public g37<la5> h;
    @DexIgnore
    public wj5 i;
    @DexIgnore
    public sv5 j;
    @DexIgnore
    public z67 k;
    @DexIgnore
    public po4 l;
    @DexIgnore
    public kx6 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final yo6 a() {
            return new yo6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public b(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6(qh5.FEMALE);
            yo6.O6(this.b).q(qh5.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public c(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6(qh5.OTHER);
            yo6.O6(this.b).q(qh5.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public d(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            yo6.O6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<bp6.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yo6 f4344a;

        @DexIgnore
        public e(yo6 yo6) {
            this.f4344a = yo6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bp6.c cVar) {
            FlexibleTextInputLayout flexibleTextInputLayout;
            FlexibleTextInputLayout flexibleTextInputLayout2;
            FossilCircleImageView fossilCircleImageView;
            FossilCircleImageView fossilCircleImageView2;
            FossilCircleImageView fossilCircleImageView3;
            FossilCircleImageView fossilCircleImageView4;
            FlexibleTextInputLayout flexibleTextInputLayout3;
            la5 la5;
            FlexibleTextInputEditText flexibleTextInputEditText;
            MFUser j = cVar.j();
            if (j != null) {
                this.f4344a.b7(j);
            }
            Boolean k = cVar.k();
            if (k != null) {
                this.f4344a.d7(k.booleanValue());
            }
            Uri e = cVar.e();
            if (e != null) {
                this.f4344a.a7(e);
            }
            if (cVar.f()) {
                this.f4344a.m();
            } else {
                this.f4344a.k();
            }
            if (cVar.i() != null) {
                this.f4344a.e0();
            }
            cl7<Integer, String> h = cVar.h();
            if (h != null) {
                this.f4344a.o(h.getFirst().intValue(), h.getSecond());
            }
            if (cVar.g()) {
                this.f4344a.E0();
            }
            Bundle d = cVar.d();
            if (d != null) {
                this.f4344a.z0(d);
            }
            String a2 = cVar.a();
            if (!(a2 == null || (la5 = (la5) yo6.M6(this.f4344a).a()) == null || (flexibleTextInputEditText = la5.L) == null)) {
                flexibleTextInputEditText.setText(a2);
            }
            if (cVar.b() == null) {
                la5 la52 = (la5) yo6.M6(this.f4344a).a();
                if (!(la52 == null || (flexibleTextInputLayout3 = la52.C) == null)) {
                    flexibleTextInputLayout3.setErrorEnabled(false);
                }
            } else {
                la5 la53 = (la5) yo6.M6(this.f4344a).a();
                if (!(la53 == null || (flexibleTextInputLayout2 = la53.C) == null)) {
                    flexibleTextInputLayout2.setErrorEnabled(true);
                }
                la5 la54 = (la5) yo6.M6(this.f4344a).a();
                if (!(la54 == null || (flexibleTextInputLayout = la54.C) == null)) {
                    flexibleTextInputLayout.setError(cVar.b());
                }
            }
            bp6.a c = cVar.c();
            if (c != null && c.a() != null) {
                String a3 = c.a();
                if (a3 != null) {
                    Bitmap e2 = i37.e(a3);
                    if (e2 != null) {
                        vj5<Drawable> T0 = yo6.N6(this.f4344a).F(e2).u0(new fj1().o0(new hk5()));
                        la5 la55 = (la5) yo6.M6(this.f4344a).a();
                        FossilCircleImageView fossilCircleImageView5 = la55 != null ? la55.q : null;
                        if (fossilCircleImageView5 != null) {
                            T0.F0(fossilCircleImageView5);
                            la5 la56 = (la5) yo6.M6(this.f4344a).a();
                            if (la56 != null && (fossilCircleImageView4 = la56.q) != null) {
                                fossilCircleImageView4.setBorderColor(gl0.d(this.f4344a.requireContext(), R.color.transparent));
                                return;
                            }
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    String str = c.b() + " " + c.c();
                    if (!(str == null || vt7.l(str))) {
                        vj5<Drawable> T02 = yo6.N6(this.f4344a).I(new sj5("", str)).u0(new fj1().o0(new hk5()));
                        la5 la57 = (la5) yo6.M6(this.f4344a).a();
                        FossilCircleImageView fossilCircleImageView6 = la57 != null ? la57.q : null;
                        if (fossilCircleImageView6 != null) {
                            T02.F0(fossilCircleImageView6);
                            la5 la58 = (la5) yo6.M6(this.f4344a).a();
                            if (!(la58 == null || (fossilCircleImageView3 = la58.q) == null)) {
                                fossilCircleImageView3.setBorderColor(gl0.d(PortfolioApp.h0.c(), 2131099830));
                            }
                            la5 la59 = (la5) yo6.M6(this.f4344a).a();
                            if (!(la59 == null || (fossilCircleImageView2 = la59.q) == null)) {
                                fossilCircleImageView2.setBorderWidth(3);
                            }
                            la5 la510 = (la5) yo6.M6(this.f4344a).a();
                            if (la510 != null && (fossilCircleImageView = la510.q) != null) {
                                fossilCircleImageView.setBackground(gl0.f(PortfolioApp.h0.c(), 2131231287));
                                return;
                            }
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    return;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<Date> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yo6 f4345a;

        @DexIgnore
        public f(yo6 yo6) {
            this.f4345a = yo6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Date date) {
            if (date != null) {
                yo6.O6(this.f4345a).n(date);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public g(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputEditText flexibleTextInputEditText;
            bp6 O6 = yo6.O6(this.b);
            la5 la5 = (la5) yo6.M6(this.b).a();
            String valueOf = String.valueOf((la5 == null || (flexibleTextInputEditText = la5.s) == null) ? null : flexibleTextInputEditText.getEditableText());
            if (valueOf != null) {
                O6.p(wt7.u0(valueOf).toString());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputEditText flexibleTextInputEditText;
            bp6 O6 = yo6.O6(this.b);
            la5 la5 = (la5) yo6.M6(this.b).a();
            String valueOf = String.valueOf((la5 == null || (flexibleTextInputEditText = la5.t) == null) ? null : flexibleTextInputEditText.getEditableText());
            if (valueOf != null) {
                O6.s(wt7.u0(valueOf).toString());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public j(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.c7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public k(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements k67 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yo6 f4346a;
        @DexIgnore
        public /* final */ /* synthetic */ la5 b;

        @DexIgnore
        public l(yo6 yo6, la5 la5) {
            this.f4346a = yo6;
            this.b = la5;
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void a(int i) {
            if (this.b.I.getUnit() == ai5.METRIC) {
                yo6.O6(this.f4346a).r(i);
            } else {
                yo6.O6(this.f4346a).r(lr7.b(jk5.d((float) (i / 12), ((float) i) % 12.0f)));
            }
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void c(boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements k67 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ yo6 f4347a;
        @DexIgnore
        public /* final */ /* synthetic */ la5 b;

        @DexIgnore
        public m(yo6 yo6, la5 la5) {
            this.f4347a = yo6;
            this.b = la5;
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void a(int i) {
            if (this.b.J.getUnit() == ai5.METRIC) {
                yo6.O6(this.f4347a).u(lr7.b((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            yo6.O6(this.f4347a).u(lr7.b(jk5.m(((float) i) / 10.0f)));
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void c(boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yo6 b;

        @DexIgnore
        public n(yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6(qh5.MALE);
            yo6.O6(this.b).q(qh5.MALE);
        }
    }

    /*
    static {
        String simpleName = yo6.class.getSimpleName();
        pq7.b(simpleName, "ProfileEditFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 M6(yo6 yo6) {
        g37<la5> g37 = yo6.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ wj5 N6(yo6 yo6) {
        wj5 wj5 = yo6.i;
        if (wj5 != null) {
            return wj5;
        }
        pq7.n("mGlideRequests");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ bp6 O6(yo6 yo6) {
        bp6 bp6 = yo6.g;
        if (bp6 != null) {
            return bp6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @t78(1222)
    public final void doCameraTask() {
        FragmentActivity activity;
        Intent f2;
        if (jn5.c(jn5.b, getActivity(), jn5.a.EDIT_AVATAR, false, false, false, null, 60, null) && (activity = getActivity()) != null && (f2 = vk5.f(activity)) != null) {
            startActivityForResult(f2, 1234);
        }
    }

    @DexIgnore
    public final void E0() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.X(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                pq7.b(requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    bp6 bp6 = this.g;
                    if (bp6 == null) {
                        pq7.n("mViewModel");
                        throw null;
                    } else if (bp6.l()) {
                        s37 s37 = s37.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        pq7.b(childFragmentManager, "childFragmentManager");
                        s37.y0(childFragmentManager);
                    } else {
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            activity.finish();
                        }
                    }
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final void H1(int i2, ai5 ai5) {
        int i3 = zo6.b[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "updateData weight=" + i2 + " metric");
            g37<la5> g37 = this.h;
            if (g37 != null) {
                la5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B;
                    pq7.b(flexibleTextView, "it.ftvWeightUnit");
                    flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131886955));
                    a2.J.setUnit(ai5.METRIC);
                    a2.J.setFormatter(new ProfileFormatter(4));
                    a2.J.l(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            g37<la5> g372 = this.h;
            if (g372 != null) {
                la5 a3 = g372.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.B;
                    pq7.b(flexibleTextView2, "it.ftvWeightUnit");
                    flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131886956));
                    float h2 = jk5.h((float) i2);
                    a3.J.setUnit(ai5.IMPERIAL);
                    a3.J.setFormatter(new ProfileFormatter(4));
                    a3.J.l(780, 4401, Math.round(h2 * ((float) 10)));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void R0(int i2, ai5 ai5) {
        int i3 = zo6.f4507a[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "updateData height=" + i2 + " metric");
            g37<la5> g37 = this.h;
            if (g37 != null) {
                la5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    pq7.b(flexibleTextView, "it.ftvHeightUnit");
                    flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131887202));
                    a2.I.setUnit(ai5.METRIC);
                    a2.I.setFormatter(new ProfileFormatter(-1));
                    a2.I.l(100, 251, i2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            g37<la5> g372 = this.h;
            if (g372 != null) {
                la5 a3 = g372.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.z;
                    pq7.b(flexibleTextView2, "it.ftvHeightUnit");
                    flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887203));
                    cl7<Integer, Integer> b2 = jk5.b((float) i2);
                    a3.I.setUnit(ai5.IMPERIAL);
                    a3.I.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.I;
                    Integer first = b2.getFirst();
                    pq7.b(first, "currentHeightInFeetAndInches.first");
                    int e2 = jk5.e(first.intValue());
                    Integer second = b2.getSecond();
                    pq7.b(second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.l(40, 99, second.intValue() + e2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        pq7.c(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).R5(str, i2, intent);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363291) {
            e0();
        } else if (i2 == 2131363373) {
            c7();
        }
    }

    @DexIgnore
    public final void Z6(qh5 qh5) {
        g37<la5> g37 = this.h;
        if (g37 != null) {
            la5 a2 = g37.a();
            if (a2 != null) {
                a2.u.d("flexible_button_secondary");
                a2.v.d("flexible_button_secondary");
                a2.w.d("flexible_button_secondary");
                int i2 = zo6.c[qh5.ordinal()];
                if (i2 == 1) {
                    g37<la5> g372 = this.h;
                    if (g372 != null) {
                        la5 a3 = g372.a();
                        if (a3 != null && a3.u != null) {
                            a2.u.d("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                } else if (i2 != 2) {
                    g37<la5> g373 = this.h;
                    if (g373 != null) {
                        la5 a4 = g373.a();
                        if (a4 != null && a4.w != null) {
                            a2.w.d("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                } else {
                    g37<la5> g374 = this.h;
                    if (g374 != null) {
                        la5 a5 = g374.a();
                        if (a5 != null && a5.v != null) {
                            a2.v.d("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    pq7.n("mBinding");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(Uri uri) {
        if (isActive()) {
            wj5 wj5 = this.i;
            if (wj5 != null) {
                vj5<Drawable> T0 = wj5.G(uri).l(wc1.f3916a).u0(new fj1().o0(new hk5()));
                g37<la5> g37 = this.h;
                if (g37 != null) {
                    la5 a2 = g37.a();
                    FossilCircleImageView fossilCircleImageView = a2 != null ? a2.q : null;
                    if (fossilCircleImageView != null) {
                        T0.F0(fossilCircleImageView);
                        d7(true);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mGlideRequests");
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"SimpleDateFormat"})
    public final void b7(MFUser mFUser) {
        FossilCircleImageView fossilCircleImageView;
        FossilCircleImageView fossilCircleImageView2;
        FossilCircleImageView fossilCircleImageView3;
        FlexibleTextInputEditText flexibleTextInputEditText;
        FlexibleTextView flexibleTextView;
        FlexibleTextInputEditText flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3;
        FossilCircleImageView fossilCircleImageView4;
        String profilePicture = mFUser.getProfilePicture();
        String str = mFUser.getFirstName() + " " + mFUser.getLastName();
        if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
            wj5 wj5 = this.i;
            if (wj5 != null) {
                vj5<Drawable> T0 = wj5.I(new sj5("", str)).u0(new fj1().o0(new hk5()));
                g37<la5> g37 = this.h;
                if (g37 != null) {
                    la5 a2 = g37.a();
                    FossilCircleImageView fossilCircleImageView5 = a2 != null ? a2.q : null;
                    if (fossilCircleImageView5 != null) {
                        T0.F0(fossilCircleImageView5);
                        g37<la5> g372 = this.h;
                        if (g372 != null) {
                            la5 a3 = g372.a();
                            if (!(a3 == null || (fossilCircleImageView3 = a3.q) == null)) {
                                fossilCircleImageView3.setBorderColor(gl0.d(PortfolioApp.h0.c(), 2131099830));
                            }
                            g37<la5> g373 = this.h;
                            if (g373 != null) {
                                la5 a4 = g373.a();
                                if (!(a4 == null || (fossilCircleImageView2 = a4.q) == null)) {
                                    fossilCircleImageView2.setBorderWidth(3);
                                }
                                g37<la5> g374 = this.h;
                                if (g374 != null) {
                                    la5 a5 = g374.a();
                                    if (!(a5 == null || (fossilCircleImageView = a5.q) == null)) {
                                        fossilCircleImageView.setBackground(gl0.f(PortfolioApp.h0.c(), 2131231287));
                                    }
                                } else {
                                    pq7.n("mBinding");
                                    throw null;
                                }
                            } else {
                                pq7.n("mBinding");
                                throw null;
                            }
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mGlideRequests");
                throw null;
            }
        } else {
            g37<la5> g375 = this.h;
            if (g375 != null) {
                la5 a6 = g375.a();
                if (!(a6 == null || (fossilCircleImageView4 = a6.q) == null)) {
                    wj5 wj52 = this.i;
                    if (wj52 != null) {
                        fossilCircleImageView4.j(wj52, profilePicture, str);
                    } else {
                        pq7.n("mGlideRequests");
                        throw null;
                    }
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        g37<la5> g376 = this.h;
        if (g376 != null) {
            la5 a7 = g376.a();
            if (!(a7 == null || (flexibleTextInputEditText3 = a7.s) == null)) {
                flexibleTextInputEditText3.setText(mFUser.getFirstName());
            }
            g37<la5> g377 = this.h;
            if (g377 != null) {
                la5 a8 = g377.a();
                if (!(a8 == null || (flexibleTextInputEditText2 = a8.t) == null)) {
                    flexibleTextInputEditText2.setText(mFUser.getLastName());
                }
                g37<la5> g378 = this.h;
                if (g378 != null) {
                    la5 a9 = g378.a();
                    if (!(a9 == null || (flexibleTextView = a9.M) == null)) {
                        flexibleTextView.setText(mFUser.getEmail());
                    }
                    gk5 gk5 = gk5.f1320a;
                    qh5 a10 = qh5.Companion.a(mFUser.getGender());
                    String birthday = mFUser.getBirthday();
                    if (birthday != null) {
                        cl7<Integer, Integer> c2 = gk5.c(a10, mFUser.getAge(birthday));
                        if (mFUser.getHeightInCentimeters() == 0) {
                            mFUser.setHeightInCentimeters(c2.getFirst().intValue());
                        }
                        if (mFUser.getHeightInCentimeters() > 0) {
                            int heightInCentimeters = mFUser.getHeightInCentimeters();
                            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                            String height = unitGroup != null ? unitGroup.getHeight() : null;
                            if (height != null) {
                                ai5 fromString = ai5.fromString(height);
                                pq7.b(fromString, "Unit.fromString(user.unitGroup?.height!!)");
                                R0(heightInCentimeters, fromString);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                        if (mFUser.getWeightInGrams() == 0) {
                            mFUser.setWeightInGrams(c2.getSecond().intValue() * 1000);
                        }
                        if (mFUser.getWeightInGrams() > 0) {
                            int weightInGrams = mFUser.getWeightInGrams();
                            MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                            String weight = unitGroup2 != null ? unitGroup2.getWeight() : null;
                            if (weight != null) {
                                ai5 fromString2 = ai5.fromString(weight);
                                pq7.b(fromString2, "Unit.fromString(user.unitGroup?.weight!!)");
                                H1(weightInGrams, fromString2);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                        String birthday2 = mFUser.getBirthday();
                        if (!TextUtils.isEmpty(birthday2)) {
                            try {
                                Date r0 = lk5.r0(birthday2);
                                g37<la5> g379 = this.h;
                                if (g379 != null) {
                                    la5 a11 = g379.a();
                                    if (!(a11 == null || (flexibleTextInputEditText = a11.L) == null)) {
                                        flexibleTextInputEditText.setText(lk5.f(r0));
                                    }
                                } else {
                                    pq7.n("mBinding");
                                    throw null;
                                }
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        Z6(qh5.Companion.a(mFUser.getGender()));
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c7() {
        bp6 bp6 = this.g;
        if (bp6 != null) {
            bp6.v();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0048, code lost:
        if ((com.fossil.wt7.u0(r3).length() > 0) != false) goto L_0x004a;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d7(boolean r6) {
        /*
            r5 = this;
            r1 = 1
            r2 = 0
            com.fossil.g37<com.fossil.la5> r0 = r5.h
            if (r0 == 0) goto L_0x006c
            java.lang.Object r0 = r0.a()
            com.fossil.la5 r0 = (com.fossil.la5) r0
            if (r0 == 0) goto L_0x005d
            if (r6 == 0) goto L_0x0062
            com.portfolio.platform.view.FlexibleTextInputEditText r3 = r0.s
            java.lang.String r4 = "it.etFirstName"
            com.fossil.pq7.b(r3, r4)
            android.text.Editable r3 = r3.getEditableText()
            java.lang.String r4 = "it.etFirstName.editableText"
            com.fossil.pq7.b(r3, r4)
            java.lang.CharSequence r3 = com.fossil.wt7.u0(r3)
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x005e
            r3 = r1
        L_0x002b:
            if (r3 == 0) goto L_0x0062
            com.portfolio.platform.view.FlexibleTextInputEditText r3 = r0.t
            java.lang.String r4 = "it.etLastName"
            com.fossil.pq7.b(r3, r4)
            android.text.Editable r3 = r3.getEditableText()
            java.lang.String r4 = "it.etLastName.editableText"
            com.fossil.pq7.b(r3, r4)
            java.lang.CharSequence r3 = com.fossil.wt7.u0(r3)
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x0060
            r3 = r1
        L_0x0048:
            if (r3 == 0) goto L_0x0062
        L_0x004a:
            com.portfolio.platform.view.ProgressButton r2 = r0.K
            java.lang.String r3 = "it.save"
            com.fossil.pq7.b(r2, r3)
            r2.setEnabled(r1)
            if (r1 == 0) goto L_0x0064
            com.portfolio.platform.view.ProgressButton r0 = r0.K
            java.lang.String r1 = "flexible_button_primary"
            r0.d(r1)
        L_0x005d:
            return
        L_0x005e:
            r3 = r2
            goto L_0x002b
        L_0x0060:
            r3 = r2
            goto L_0x0048
        L_0x0062:
            r1 = r2
            goto L_0x004a
        L_0x0064:
            com.portfolio.platform.view.ProgressButton r0 = r0.K
            java.lang.String r1 = "flexible_button_disabled"
            r0.d(r1)
            goto L_0x005d
        L_0x006c:
            java.lang.String r0 = "mBinding"
            com.fossil.pq7.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yo6.d7(boolean):void");
    }

    @DexIgnore
    public final void e0() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                pq7.b(requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    requireActivity().finish();
                }
            }
        }
    }

    @DexIgnore
    public final void k() {
        ProgressButton progressButton;
        g37<la5> g37 = this.h;
        if (g37 != null) {
            la5 a2 = g37.a();
            if (a2 != null && (progressButton = a2.K) != null) {
                progressButton.f();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void m() {
        ProgressButton progressButton;
        g37<la5> g37 = this.h;
        if (g37 != null) {
            la5 a2 = g37.a();
            if (a2 != null && (progressButton = a2.K) != null) {
                progressButton.g();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void o(int i2, String str) {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            bp6 bp6 = this.g;
            if (bp6 != null) {
                bp6.t(intent);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        la5 la5 = (la5) aq0.f(LayoutInflater.from(getContext()), 2131558610, null, false, A6());
        sv5 sv5 = (sv5) getChildFragmentManager().Z(sv5.u.a());
        this.j = sv5;
        if (sv5 == null) {
            this.j = sv5.u.b();
        }
        ro4 M = PortfolioApp.h0.c().M();
        sv5 sv52 = this.j;
        if (sv52 != null) {
            M.C1(new ix6(sv52)).a(this);
            po4 po4 = this.l;
            if (po4 != null) {
                ts0 a2 = vs0.d(this, po4).a(bp6.class);
                pq7.b(a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
                this.g = (bp6) a2;
                ts0 a3 = vs0.e(requireActivity()).a(z67.class);
                pq7.b(a3, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
                z67 z67 = (z67) a3;
                this.k = z67;
                if (z67 != null) {
                    z67.a().h(getViewLifecycleOwner(), new f(this));
                    la5.F.setOnClickListener(new g(this));
                    la5.s.addTextChangedListener(new h(this));
                    la5.t.addTextChangedListener(new i(this));
                    la5.K.setOnClickListener(new j(this));
                    FossilCircleImageView fossilCircleImageView = la5.q;
                    pq7.b(fossilCircleImageView, "binding.avatar");
                    nl5.a(fossilCircleImageView, new k(this));
                    la5.I.setValuePickerListener(new l(this, la5));
                    la5.J.setValuePickerListener(new m(this, la5));
                    la5.v.setOnClickListener(new n(this));
                    la5.u.setOnClickListener(new b(this));
                    la5.w.setOnClickListener(new c(this));
                    la5.L.setOnClickListener(new d(this));
                    bp6 bp6 = this.g;
                    if (bp6 != null) {
                        bp6.i().h(getViewLifecycleOwner(), new e(this));
                        this.h = new g37<>(this, la5);
                        pq7.b(la5, "binding");
                        return la5.n();
                    }
                    pq7.n("mViewModel");
                    throw null;
                }
                pq7.n("mUserBirthDayViewModel");
                throw null;
            }
            pq7.n("viewModelFactory");
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        bp6 bp6 = this.g;
        if (bp6 != null) {
            bp6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("edit_profile_view");
        wj5 c2 = tj5.c(this);
        pq7.b(c2, "GlideApp.with(this)");
        this.i = c2;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void z0(Bundle bundle) {
        sv5 sv5 = this.j;
        if (sv5 != null) {
            sv5.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            sv5.show(childFragmentManager, sv5.u.a());
        }
    }
}
