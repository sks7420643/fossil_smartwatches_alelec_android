package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb1<T> {
    @DexIgnore
    public static /* final */ b<Object> e; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f2495a;
    @DexIgnore
    public /* final */ b<T> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public volatile byte[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b<Object> {
        @DexIgnore
        @Override // com.fossil.nb1.b
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void a(byte[] bArr, T t, MessageDigest messageDigest);
    }

    @DexIgnore
    public nb1(String str, T t, b<T> bVar) {
        ik1.b(str);
        this.c = str;
        this.f2495a = t;
        ik1.d(bVar);
        this.b = bVar;
    }

    @DexIgnore
    public static <T> nb1<T> a(String str, T t, b<T> bVar) {
        return new nb1<>(str, t, bVar);
    }

    @DexIgnore
    public static <T> b<T> b() {
        return (b<T>) e;
    }

    @DexIgnore
    public static <T> nb1<T> e(String str) {
        return new nb1<>(str, null, b());
    }

    @DexIgnore
    public static <T> nb1<T> f(String str, T t) {
        return new nb1<>(str, t, b());
    }

    @DexIgnore
    public T c() {
        return this.f2495a;
    }

    @DexIgnore
    public final byte[] d() {
        if (this.d == null) {
            this.d = this.c.getBytes(mb1.f2349a);
        }
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof nb1) {
            return this.c.equals(((nb1) obj).c);
        }
        return false;
    }

    @DexIgnore
    public void g(T t, MessageDigest messageDigest) {
        this.b.a(d(), t, messageDigest);
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Option{key='" + this.c + "'}";
    }
}
