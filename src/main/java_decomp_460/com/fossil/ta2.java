package com.fossil;

import android.app.Dialog;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta2 implements Runnable {
    @DexIgnore
    public /* final */ qa2 b;
    @DexIgnore
    public /* final */ /* synthetic */ ra2 c;

    @DexIgnore
    public ta2(ra2 ra2, qa2 qa2) {
        this.c = ra2;
        this.b = qa2;
    }

    @DexIgnore
    public final void run() {
        if (this.c.c) {
            z52 a2 = this.b.a();
            if (a2.k()) {
                ra2 ra2 = this.c;
                ra2.b.startActivityForResult(GoogleApiActivity.b(ra2.b(), a2.h(), this.b.b(), false), 1);
            } else if (this.c.f.m(a2.c())) {
                ra2 ra22 = this.c;
                ra22.f.A(ra22.b(), this.c.b, a2.c(), 2, this.c);
            } else if (a2.c() == 18) {
                Dialog u = c62.u(this.c.b(), this.c);
                ra2 ra23 = this.c;
                ra23.f.w(ra23.b().getApplicationContext(), new sa2(this, u));
            } else {
                this.c.m(a2, this.b.b());
            }
        }
    }
}
