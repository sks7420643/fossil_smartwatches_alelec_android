package com.fossil;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kx2<T> implements Iterator<T> {
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ hx2 e;

    @DexIgnore
    public kx2(hx2 hx2) {
        this.e = hx2;
        this.b = this.e.c;
        this.c = this.e.zzd();
        this.d = -1;
    }

    @DexIgnore
    public /* synthetic */ kx2(hx2 hx2, gx2 gx2) {
        this(hx2);
    }

    @DexIgnore
    public abstract T a(int i);

    @DexIgnore
    public final void b() {
        if (this.e.c != this.b) {
            throw new ConcurrentModificationException();
        }
    }

    @DexIgnore
    public boolean hasNext() {
        return this.c >= 0;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        b();
        if (hasNext()) {
            int i = this.c;
            this.d = i;
            T a2 = a(i);
            this.c = this.e.zza(this.c);
            return a2;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        b();
        sw2.h(this.d >= 0, "no calls to next() since the last call to remove()");
        this.b += 32;
        hx2 hx2 = this.e;
        hx2.remove(hx2.zzb[this.d]);
        this.c = hx2.zzb(this.c, this.d);
        this.d = -1;
    }
}
