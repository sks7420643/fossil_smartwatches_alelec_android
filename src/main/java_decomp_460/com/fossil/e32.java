package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class e32 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j32 f874a;
    @DexIgnore
    public /* final */ h02 b;
    @DexIgnore
    public /* final */ c02 c;

    @DexIgnore
    public e32(j32 j32, h02 h02, c02 c02) {
        this.f874a = j32;
        this.b = h02;
        this.c = c02;
    }

    @DexIgnore
    public static j32.b a(j32 j32, h02 h02, c02 c02) {
        return new e32(j32, h02, c02);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.V(this.f874a, this.b, this.c, (SQLiteDatabase) obj);
    }
}
