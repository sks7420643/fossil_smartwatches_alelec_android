package com.fossil;

import android.app.Activity;
import android.app.ActivityOptions;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sk0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends sk0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ActivityOptions f3272a;

        @DexIgnore
        public a(ActivityOptions activityOptions) {
            this.f3272a = activityOptions;
        }

        @DexIgnore
        @Override // com.fossil.sk0
        public Bundle b() {
            return this.f3272a.toBundle();
        }
    }

    @DexIgnore
    public static sk0 a(Activity activity, ln0<View, String>... ln0Arr) {
        if (Build.VERSION.SDK_INT < 21) {
            return new sk0();
        }
        Pair[] pairArr = null;
        if (ln0Arr != null) {
            Pair[] pairArr2 = new Pair[ln0Arr.length];
            for (int i = 0; i < ln0Arr.length; i++) {
                pairArr2[i] = Pair.create(ln0Arr[i].f2221a, ln0Arr[i].b);
            }
            pairArr = pairArr2;
        }
        return new a(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    @DexIgnore
    public Bundle b() {
        return null;
    }
}
