package com.fossil;

import android.os.Build;
import android.util.Log;
import com.fossil.kk1;
import com.fossil.sc1;
import com.fossil.ua1;
import com.fossil.vc1;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uc1<R> implements sc1.a, Runnable, Comparable<uc1<?>>, kk1.f {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public Object B;
    @DexIgnore
    public Thread C;
    @DexIgnore
    public mb1 D;
    @DexIgnore
    public mb1 E;
    @DexIgnore
    public Object F;
    @DexIgnore
    public gb1 G;
    @DexIgnore
    public wb1<?> H;
    @DexIgnore
    public volatile sc1 I;
    @DexIgnore
    public volatile boolean J;
    @DexIgnore
    public volatile boolean K;
    @DexIgnore
    public /* final */ tc1<R> b; // = new tc1<>();
    @DexIgnore
    public /* final */ List<Throwable> c; // = new ArrayList();
    @DexIgnore
    public /* final */ mk1 d; // = mk1.a();
    @DexIgnore
    public /* final */ e e;
    @DexIgnore
    public /* final */ mn0<uc1<?>> f;
    @DexIgnore
    public /* final */ d<?> g; // = new d<>();
    @DexIgnore
    public /* final */ f h; // = new f();
    @DexIgnore
    public qa1 i;
    @DexIgnore
    public mb1 j;
    @DexIgnore
    public sa1 k;
    @DexIgnore
    public ad1 l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public wc1 t;
    @DexIgnore
    public ob1 u;
    @DexIgnore
    public b<R> v;
    @DexIgnore
    public int w;
    @DexIgnore
    public h x;
    @DexIgnore
    public g y;
    @DexIgnore
    public long z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3564a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c;

        /*
        static {
            int[] iArr = new int[ib1.values().length];
            c = iArr;
            try {
                iArr[ib1.SOURCE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                c[ib1.TRANSFORMED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            int[] iArr2 = new int[h.values().length];
            b = iArr2;
            try {
                iArr2[h.RESOURCE_CACHE.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[h.DATA_CACHE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                b[h.SOURCE.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                b[h.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                b[h.INITIALIZE.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            int[] iArr3 = new int[g.values().length];
            f3564a = iArr3;
            try {
                iArr3[g.INITIALIZE.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f3564a[g.SWITCH_TO_SOURCE_SERVICE.ordinal()] = 2;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f3564a[g.DECODE_DATA.ordinal()] = 3;
            } catch (NoSuchFieldError e10) {
            }
        }
        */
    }

    @DexIgnore
    public interface b<R> {
        @DexIgnore
        void a(dd1 dd1);

        @DexIgnore
        void c(id1<R> id1, gb1 gb1);

        @DexIgnore
        void d(uc1<?> uc1);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c<Z> implements vc1.a<Z> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ gb1 f3565a;

        @DexIgnore
        public c(gb1 gb1) {
            this.f3565a = gb1;
        }

        @DexIgnore
        @Override // com.fossil.vc1.a
        public id1<Z> a(id1<Z> id1) {
            return uc1.this.x(this.f3565a, id1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Z> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public mb1 f3566a;
        @DexIgnore
        public rb1<Z> b;
        @DexIgnore
        public hd1<Z> c;

        @DexIgnore
        public void a() {
            this.f3566a = null;
            this.b = null;
            this.c = null;
        }

        @DexIgnore
        public void b(e eVar, ob1 ob1) {
            lk1.a("DecodeJob.encode");
            try {
                eVar.a().a(this.f3566a, new rc1(this.b, this.c, ob1));
            } finally {
                this.c.h();
                lk1.d();
            }
        }

        @DexIgnore
        public boolean c() {
            return this.c != null;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.rb1<X> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.hd1<X> */
        /* JADX WARN: Multi-variable type inference failed */
        public <X> void d(mb1 mb1, rb1<X> rb1, hd1<X> hd1) {
            this.f3566a = mb1;
            this.b = rb1;
            this.c = hd1;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        be1 a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f3567a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public final boolean a(boolean z) {
            return (this.c || z || this.b) && this.f3567a;
        }

        @DexIgnore
        public boolean b() {
            boolean a2;
            synchronized (this) {
                this.b = true;
                a2 = a(false);
            }
            return a2;
        }

        @DexIgnore
        public boolean c() {
            boolean a2;
            synchronized (this) {
                this.c = true;
                a2 = a(false);
            }
            return a2;
        }

        @DexIgnore
        public boolean d(boolean z) {
            boolean a2;
            synchronized (this) {
                this.f3567a = true;
                a2 = a(z);
            }
            return a2;
        }

        @DexIgnore
        public void e() {
            synchronized (this) {
                this.b = false;
                this.f3567a = false;
                this.c = false;
            }
        }
    }

    @DexIgnore
    public enum g {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    @DexIgnore
    public enum h {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    @DexIgnore
    public uc1(e eVar, mn0<uc1<?>> mn0) {
        this.e = eVar;
        this.f = mn0;
    }

    @DexIgnore
    public final void A() {
        this.C = Thread.currentThread();
        this.z = ek1.b();
        boolean z2 = false;
        while (!this.K && this.I != null && !(z2 = this.I.c())) {
            this.x = m(this.x);
            this.I = l();
            if (this.x == h.SOURCE) {
                b();
                return;
            }
        }
        if ((this.x == h.FINISHED || this.K) && !z2) {
            u();
        }
    }

    @DexIgnore
    public final <Data, ResourceType> id1<R> B(Data data, gb1 gb1, gd1<Data, ResourceType, R> gd1) throws dd1 {
        ob1 n = n(gb1);
        xb1<Data> l2 = this.i.h().l(data);
        try {
            return gd1.a(l2, n, this.m, this.s, new c(gb1));
        } finally {
            l2.a();
        }
    }

    @DexIgnore
    public final void C() {
        int i2 = a.f3564a[this.y.ordinal()];
        if (i2 == 1) {
            this.x = m(h.INITIALIZE);
            this.I = l();
            A();
        } else if (i2 == 2) {
            A();
        } else if (i2 == 3) {
            k();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.y);
        }
    }

    @DexIgnore
    public final void D() {
        Throwable th;
        this.d.c();
        if (this.J) {
            if (this.c.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.c;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.J = true;
    }

    @DexIgnore
    public boolean E() {
        h m2 = m(h.INITIALIZE);
        return m2 == h.RESOURCE_CACHE || m2 == h.DATA_CACHE;
    }

    @DexIgnore
    @Override // com.fossil.sc1.a
    public void a(mb1 mb1, Exception exc, wb1<?> wb1, gb1 gb1) {
        wb1.a();
        dd1 dd1 = new dd1("Fetching data failed", exc);
        dd1.setLoggingDetails(mb1, gb1, wb1.getDataClass());
        this.c.add(dd1);
        if (Thread.currentThread() != this.C) {
            this.y = g.SWITCH_TO_SOURCE_SERVICE;
            this.v.d(this);
            return;
        }
        A();
    }

    @DexIgnore
    @Override // com.fossil.sc1.a
    public void b() {
        this.y = g.SWITCH_TO_SOURCE_SERVICE;
        this.v.d(this);
    }

    @DexIgnore
    public void c() {
        this.K = true;
        sc1 sc1 = this.I;
        if (sc1 != null) {
            sc1.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.sc1.a
    public void e(mb1 mb1, Object obj, wb1<?> wb1, gb1 gb1, mb1 mb12) {
        this.D = mb1;
        this.F = obj;
        this.H = wb1;
        this.G = gb1;
        this.E = mb12;
        if (Thread.currentThread() != this.C) {
            this.y = g.DECODE_DATA;
            this.v.d(this);
            return;
        }
        lk1.a("DecodeJob.decodeFromRetrievedData");
        try {
            k();
        } finally {
            lk1.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.kk1.f
    public mk1 f() {
        return this.d;
    }

    @DexIgnore
    /* renamed from: h */
    public int compareTo(uc1<?> uc1) {
        int o = o() - uc1.o();
        return o == 0 ? this.w - uc1.w : o;
    }

    @DexIgnore
    public final <Data> id1<R> i(wb1<?> wb1, Data data, gb1 gb1) throws dd1 {
        if (data == null) {
            wb1.a();
            return null;
        }
        try {
            long b2 = ek1.b();
            id1<R> j2 = j(data, gb1);
            if (Log.isLoggable("DecodeJob", 2)) {
                q("Decoded result " + j2, b2);
            }
            return j2;
        } finally {
            wb1.a();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: com.fossil.gd1<Data, ?, R>, com.fossil.gd1<Data, ResourceType, R> */
    public final <Data> id1<R> j(Data data, gb1 gb1) throws dd1 {
        return B(data, gb1, (gd1<Data, ?, R>) this.b.h(data.getClass()));
    }

    @DexIgnore
    public final void k() {
        id1<R> id1;
        if (Log.isLoggable("DecodeJob", 2)) {
            r("Retrieved data", this.z, "data: " + this.F + ", cache key: " + this.D + ", fetcher: " + this.H);
        }
        try {
            id1 = i(this.H, this.F, this.G);
        } catch (dd1 e2) {
            e2.setLoggingDetails(this.E, this.G);
            this.c.add(e2);
            id1 = null;
        }
        if (id1 != null) {
            t(id1, this.G);
        } else {
            A();
        }
    }

    @DexIgnore
    public final sc1 l() {
        int i2 = a.b[this.x.ordinal()];
        if (i2 == 1) {
            return new jd1(this.b, this);
        }
        if (i2 == 2) {
            return new pc1(this.b, this);
        }
        if (i2 == 3) {
            return new md1(this.b, this);
        }
        if (i2 == 4) {
            return null;
        }
        throw new IllegalStateException("Unrecognized stage: " + this.x);
    }

    @DexIgnore
    public final h m(h hVar) {
        int i2 = a.b[hVar.ordinal()];
        if (i2 == 1) {
            return this.t.a() ? h.DATA_CACHE : m(h.DATA_CACHE);
        }
        if (i2 == 2) {
            return this.A ? h.FINISHED : h.SOURCE;
        }
        if (i2 == 3 || i2 == 4) {
            return h.FINISHED;
        }
        if (i2 == 5) {
            return this.t.b() ? h.RESOURCE_CACHE : m(h.RESOURCE_CACHE);
        }
        throw new IllegalArgumentException("Unrecognized stage: " + hVar);
    }

    @DexIgnore
    public final ob1 n(gb1 gb1) {
        ob1 ob1 = this.u;
        if (Build.VERSION.SDK_INT < 26) {
            return ob1;
        }
        boolean z2 = gb1 == gb1.RESOURCE_DISK_CACHE || this.b.w();
        Boolean bool = (Boolean) ob1.c(gg1.i);
        if (bool != null && (!bool.booleanValue() || z2)) {
            return ob1;
        }
        ob1 ob12 = new ob1();
        ob12.d(this.u);
        ob12.e(gg1.i, Boolean.valueOf(z2));
        return ob12;
    }

    @DexIgnore
    public final int o() {
        return this.k.ordinal();
    }

    @DexIgnore
    public uc1<R> p(qa1 qa1, Object obj, ad1 ad1, mb1 mb1, int i2, int i3, Class<?> cls, Class<R> cls2, sa1 sa1, wc1 wc1, Map<Class<?>, sb1<?>> map, boolean z2, boolean z3, boolean z4, ob1 ob1, b<R> bVar, int i4) {
        this.b.u(qa1, obj, mb1, i2, i3, wc1, cls, cls2, sa1, ob1, map, z2, z3, this.e);
        this.i = qa1;
        this.j = mb1;
        this.k = sa1;
        this.l = ad1;
        this.m = i2;
        this.s = i3;
        this.t = wc1;
        this.A = z4;
        this.u = ob1;
        this.v = bVar;
        this.w = i4;
        this.y = g.INITIALIZE;
        this.B = obj;
        return this;
    }

    @DexIgnore
    public final void q(String str, long j2) {
        r(str, j2, null);
    }

    @DexIgnore
    public final void r(String str, long j2, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(ek1.a(j2));
        sb.append(", load key: ");
        sb.append(this.l);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }

    @DexIgnore
    public void run() {
        lk1.b("DecodeJob#run(model=%s)", this.B);
        wb1<?> wb1 = this.H;
        try {
            if (this.K) {
                u();
                if (wb1 != null) {
                    wb1.a();
                }
                lk1.d();
                return;
            }
            C();
            if (wb1 != null) {
                wb1.a();
            }
            lk1.d();
        } catch (oc1 e2) {
            throw e2;
        } catch (Throwable th) {
            if (wb1 != null) {
                wb1.a();
            }
            lk1.d();
            throw th;
        }
    }

    @DexIgnore
    public final void s(id1<R> id1, gb1 gb1) {
        D();
        this.v.c(id1, gb1);
    }

    @DexIgnore
    public final void t(id1<R> id1, gb1 gb1) {
        hd1 hd1;
        if (id1 instanceof ed1) {
            ((ed1) id1).a();
        }
        if (this.g.c()) {
            hd1 e2 = hd1.e(id1);
            id1 = e2;
            hd1 = e2;
        } else {
            hd1 = null;
        }
        s(id1, gb1);
        this.x = h.ENCODE;
        try {
            if (this.g.c()) {
                this.g.b(this.e, this.u);
            }
            v();
        } finally {
            if (hd1 != null) {
                hd1.h();
            }
        }
    }

    @DexIgnore
    public final void u() {
        D();
        this.v.a(new dd1("Failed to load resource", new ArrayList(this.c)));
        w();
    }

    @DexIgnore
    public final void v() {
        if (this.h.b()) {
            z();
        }
    }

    @DexIgnore
    public final void w() {
        if (this.h.c()) {
            z();
        }
    }

    @DexIgnore
    public <Z> id1<Z> x(gb1 gb1, id1<Z> id1) {
        sb1<Z> sb1;
        id1<Z> id12;
        ib1 ib1;
        rb1<X> rb1;
        mb1 qc1;
        Class<?> cls = id1.get().getClass();
        if (gb1 != gb1.RESOURCE_DISK_CACHE) {
            sb1 = this.b.r(cls);
            id12 = sb1.b(this.i, id1, this.m, this.s);
        } else {
            sb1 = null;
            id12 = id1;
        }
        if (!id1.equals(id12)) {
            id1.b();
        }
        if (this.b.v(id12)) {
            rb1<Z> n = this.b.n(id12);
            ib1 = n.b(this.u);
            rb1 = (rb1<X>) n;
        } else {
            ib1 = ib1.NONE;
            rb1 = null;
        }
        if (!this.t.d(!this.b.x(this.D), gb1, ib1)) {
            return id12;
        }
        if (rb1 != null) {
            int i2 = a.c[ib1.ordinal()];
            if (i2 == 1) {
                qc1 = new qc1(this.D, this.j);
            } else if (i2 == 2) {
                qc1 = new kd1(this.b.b(), this.D, this.j, this.m, this.s, sb1, cls, this.u);
            } else {
                throw new IllegalArgumentException("Unknown strategy: " + ib1);
            }
            hd1 e2 = hd1.e(id12);
            this.g.d(qc1, rb1, e2);
            return e2;
        }
        throw new ua1.d(id12.get().getClass());
    }

    @DexIgnore
    public void y(boolean z2) {
        if (this.h.d(z2)) {
            z();
        }
    }

    @DexIgnore
    public final void z() {
        this.h.e();
        this.g.a();
        this.b.a();
        this.J = false;
        this.i = null;
        this.j = null;
        this.u = null;
        this.k = null;
        this.l = null;
        this.v = null;
        this.x = null;
        this.I = null;
        this.C = null;
        this.D = null;
        this.F = null;
        this.G = null;
        this.H = null;
        this.z = 0;
        this.K = false;
        this.B = null;
        this.c.clear();
        this.f.a(this);
    }
}
