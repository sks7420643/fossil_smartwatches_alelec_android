package com.fossil;

import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class yk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f4329a;

    /*
    static {
        int[] iArr = new int[Gesture.values().length];
        f4329a = iArr;
        iArr[Gesture.SAM_BT1_SINGLE_PRESS.ordinal()] = 1;
        f4329a[Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD.ordinal()] = 2;
        f4329a[Gesture.SAM_BT1_DOUBLE_PRESS.ordinal()] = 3;
        f4329a[Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD.ordinal()] = 4;
        f4329a[Gesture.SAM_BT1_TRIPLE_PRESS.ordinal()] = 5;
        f4329a[Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD.ordinal()] = 6;
        f4329a[Gesture.SAM_BT1_PRESSED.ordinal()] = 7;
        f4329a[Gesture.SAM_BT2_SINGLE_PRESS.ordinal()] = 8;
        f4329a[Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD.ordinal()] = 9;
        f4329a[Gesture.SAM_BT2_DOUBLE_PRESS.ordinal()] = 10;
        f4329a[Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD.ordinal()] = 11;
        f4329a[Gesture.SAM_BT2_TRIPLE_PRESS.ordinal()] = 12;
        f4329a[Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD.ordinal()] = 13;
        f4329a[Gesture.SAM_BT2_PRESSED.ordinal()] = 14;
        f4329a[Gesture.SAM_BT3_SINGLE_PRESS.ordinal()] = 15;
        f4329a[Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD.ordinal()] = 16;
        f4329a[Gesture.SAM_BT3_DOUBLE_PRESS.ordinal()] = 17;
        f4329a[Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD.ordinal()] = 18;
        f4329a[Gesture.SAM_BT3_TRIPLE_PRESS.ordinal()] = 19;
        f4329a[Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD.ordinal()] = 20;
        f4329a[Gesture.SAM_BT3_PRESSED.ordinal()] = 21;
    }
    */
}
