package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.l72;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v92<T> extends ja2 {
    @DexIgnore
    public /* final */ ot3<T> b;

    @DexIgnore
    public v92(int i, ot3<T> ot3) {
        super(i);
        this.b = ot3;
    }

    @DexIgnore
    @Override // com.fossil.z82
    public void b(Status status) {
        this.b.d(new n62(status));
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void c(l72.a<?> aVar) throws DeadObjectException {
        try {
            i(aVar);
        } catch (DeadObjectException e) {
            b(z82.a(e));
            throw e;
        } catch (RemoteException e2) {
            b(z82.a(e2));
        } catch (RuntimeException e3) {
            e(e3);
        }
    }

    @DexIgnore
    @Override // com.fossil.z82
    public void e(Exception exc) {
        this.b.d(exc);
    }

    @DexIgnore
    public abstract void i(l72.a<?> aVar) throws RemoteException;
}
