package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a83 implements xw2<z73> {
    @DexIgnore
    public static a83 c; // = new a83();
    @DexIgnore
    public /* final */ xw2<z73> b;

    @DexIgnore
    public a83() {
        this(ww2.b(new c83()));
    }

    @DexIgnore
    public a83(xw2<z73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static long a() {
        return ((z73) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ z73 zza() {
        return this.b.zza();
    }
}
