package com.fossil;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.tj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nj1<Z> extends rj1<ImageView, Z> implements tj1.a {
    @DexIgnore
    public Animatable h;

    @DexIgnore
    public nj1(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    @Override // com.fossil.qj1
    public void b(Z z, tj1<? super Z> tj1) {
        if (tj1 == null || !tj1.a(z, this)) {
            p(z);
        } else {
            m(z);
        }
    }

    @DexIgnore
    @Override // com.fossil.jj1, com.fossil.qj1
    public void f(Drawable drawable) {
        super.f(drawable);
        p(null);
        n(drawable);
    }

    @DexIgnore
    @Override // com.fossil.jj1, com.fossil.qj1, com.fossil.rj1
    public void h(Drawable drawable) {
        super.h(drawable);
        p(null);
        n(drawable);
    }

    @DexIgnore
    @Override // com.fossil.jj1, com.fossil.qj1, com.fossil.rj1
    public void j(Drawable drawable) {
        super.j(drawable);
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.stop();
        }
        p(null);
        n(drawable);
    }

    @DexIgnore
    public final void m(Z z) {
        if (z instanceof Animatable) {
            Z z2 = z;
            this.h = z2;
            z2.start();
            return;
        }
        this.h = null;
    }

    @DexIgnore
    public void n(Drawable drawable) {
        ((ImageView) this.b).setImageDrawable(drawable);
    }

    @DexIgnore
    public abstract void o(Z z);

    @DexIgnore
    @Override // com.fossil.ei1, com.fossil.jj1
    public void onStart() {
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.start();
        }
    }

    @DexIgnore
    @Override // com.fossil.ei1, com.fossil.jj1
    public void onStop() {
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @DexIgnore
    public final void p(Z z) {
        o(z);
        m(z);
    }
}
