package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bu0<T> {
    @DexIgnore
    public static /* final */ bu0 e; // = new bu0(Collections.emptyList(), 0);
    @DexIgnore
    public static /* final */ bu0 f; // = new bu0(Collections.emptyList(), 0);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<T> f508a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<T> {
        @DexIgnore
        public abstract void a(int i, bu0<T> bu0);
    }

    @DexIgnore
    public bu0(List<T> list, int i) {
        this.f508a = list;
        this.b = 0;
        this.c = 0;
        this.d = i;
    }

    @DexIgnore
    public bu0(List<T> list, int i, int i2, int i3) {
        this.f508a = list;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public static <T> bu0<T> a() {
        return e;
    }

    @DexIgnore
    public static <T> bu0<T> b() {
        return f;
    }

    @DexIgnore
    public boolean c() {
        return this == f;
    }

    @DexIgnore
    public String toString() {
        return "Result " + this.b + ", " + this.f508a + ", " + this.c + ", offset " + this.d;
    }
}
