package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kv7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2104a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[lv7.values().length];
        f2104a = iArr;
        iArr[lv7.DEFAULT.ordinal()] = 1;
        f2104a[lv7.ATOMIC.ordinal()] = 2;
        f2104a[lv7.UNDISPATCHED.ordinal()] = 3;
        f2104a[lv7.LAZY.ordinal()] = 4;
        int[] iArr2 = new int[lv7.values().length];
        b = iArr2;
        iArr2[lv7.DEFAULT.ordinal()] = 1;
        b[lv7.ATOMIC.ordinal()] = 2;
        b[lv7.UNDISPATCHED.ordinal()] = 3;
        b[lv7.LAZY.ordinal()] = 4;
    }
    */
}
