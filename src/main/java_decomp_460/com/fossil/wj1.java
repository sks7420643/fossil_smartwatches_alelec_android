package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wj1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ConcurrentMap<String, mb1> f3948a; // = new ConcurrentHashMap();

    @DexIgnore
    public static PackageInfo a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("AppVersionSignature", "Cannot resolve info for" + context.getPackageName(), e);
            return null;
        }
    }

    @DexIgnore
    public static String b(PackageInfo packageInfo) {
        return packageInfo != null ? String.valueOf(packageInfo.versionCode) : UUID.randomUUID().toString();
    }

    @DexIgnore
    public static mb1 c(Context context) {
        String packageName = context.getPackageName();
        mb1 mb1 = f3948a.get(packageName);
        if (mb1 != null) {
            return mb1;
        }
        mb1 d = d(context);
        mb1 putIfAbsent = f3948a.putIfAbsent(packageName, d);
        return putIfAbsent == null ? d : putIfAbsent;
    }

    @DexIgnore
    public static mb1 d(Context context) {
        return new yj1(b(a(context)));
    }
}
