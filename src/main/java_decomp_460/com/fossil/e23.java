package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e23<K, V> {
    @DexIgnore
    public static <K, V> int a(h23<K, V> h23, K k, V v) {
        return t03.b(h23.f1414a, 1, k) + t03.b(h23.c, 2, v);
    }

    @DexIgnore
    public static <K, V> void b(l03 l03, h23<K, V> h23, K k, V v) throws IOException {
        t03.f(l03, h23.f1414a, 1, k);
        t03.f(l03, h23.c, 2, v);
    }
}
