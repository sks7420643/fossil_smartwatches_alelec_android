package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k64 extends Exception {
    @DexIgnore
    @Deprecated
    public k64() {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k64(String str) {
        super(str);
        rc2.h(str, "Detail message must not be empty");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k64(String str, Throwable th) {
        super(str, th);
        rc2.h(str, "Detail message must not be empty");
    }
}
