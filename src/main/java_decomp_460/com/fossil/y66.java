package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y66 extends rv5<x66> {
    @DexIgnore
    void C0(String str);

    @DexIgnore
    void E5(int i, String str);

    @DexIgnore
    void F5(List<dp5> list, Map<String, ? extends List<? extends s87>> map);

    @DexIgnore
    Object K1();  // void declaration

    @DexIgnore
    Object M4();  // void declaration

    @DexIgnore
    void O(int i);

    @DexIgnore
    void W3(String str);

    @DexIgnore
    void W4(dp5 dp5, List<? extends s87> list);

    @DexIgnore
    void c0(int i);

    @DexIgnore
    void g2(String str, int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    Object h1();  // void declaration

    @DexIgnore
    void m0(int i);

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    void r0(boolean z, String str, String str2, String str3);

    @DexIgnore
    Object u();  // void declaration

    @DexIgnore
    Object w();  // void declaration
}
