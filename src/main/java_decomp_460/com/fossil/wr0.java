package com.fossil;

import androidx.lifecycle.ViewModelProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wr0 {
    @DexIgnore
    ViewModelProvider.Factory getDefaultViewModelProviderFactory();
}
