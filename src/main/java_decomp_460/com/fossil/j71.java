package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j71 {
    @DexIgnore
    public static final Bitmap b(Drawable drawable) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) (!(drawable instanceof BitmapDrawable) ? null : drawable);
        if (bitmapDrawable != null) {
            return bitmapDrawable.getBitmap();
        }
        return null;
    }
}
