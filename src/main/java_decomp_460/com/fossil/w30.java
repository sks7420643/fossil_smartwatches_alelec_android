package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w30 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ n40 b;
    @DexIgnore
    public /* final */ /* synthetic */ lp c;
    @DexIgnore
    public /* final */ /* synthetic */ float d;

    @DexIgnore
    public w30(n40 n40, lp lpVar, float f) {
        this.b = n40;
        this.c = lpVar;
        this.d = f;
    }

    @DexIgnore
    public final void run() {
        e60.p0(this.b.c, ky1.DEBUG, ey1.a(this.c.y), "Progress: %.4f.", Float.valueOf(this.d));
        this.b.b.y(this.d);
    }
}
