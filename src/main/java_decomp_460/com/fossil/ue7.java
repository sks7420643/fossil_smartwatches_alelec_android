package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3575a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public Bundle e;

        @DexIgnore
        public final String toString() {
            return "targetPkgName:" + this.f3575a + ", targetClassName:" + this.b + ", content:" + this.c + ", flags:" + this.d + ", bundle:" + this.e;
        }
    }

    @DexIgnore
    public static boolean a(Context context, a aVar) {
        if (context == null) {
            ye7.b("MicroMsg.SDK.MMessageAct", "send fail, invalid argument");
            return false;
        } else if (af7.a(aVar.f3575a)) {
            ye7.b("MicroMsg.SDK.MMessageAct", "send fail, invalid targetPkgName, targetPkgName = " + aVar.f3575a);
            return false;
        } else {
            if (af7.a(aVar.b)) {
                aVar.b = aVar.f3575a + ".wxapi.WXEntryActivity";
            }
            ye7.e("MicroMsg.SDK.MMessageAct", "send, targetPkgName = " + aVar.f3575a + ", targetClassName = " + aVar.b);
            Intent intent = new Intent();
            intent.setClassName(aVar.f3575a, aVar.b);
            Bundle bundle = aVar.e;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 587268097);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", aVar.c);
            intent.putExtra("_mmessage_checksum", we7.a(aVar.c, 587268097, packageName));
            int i = aVar.d;
            if (i == -1) {
                intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY).addFlags(134217728);
            } else {
                intent.setFlags(i);
            }
            try {
                context.startActivity(intent);
                ye7.e("MicroMsg.SDK.MMessageAct", "send mm message, intent=" + intent);
                return true;
            } catch (Exception e) {
                ye7.a("MicroMsg.SDK.MMessageAct", "send fail, ex = %s", e.getMessage());
                return false;
            }
        }
    }
}
