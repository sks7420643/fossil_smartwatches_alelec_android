package com.fossil;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cd7 implements ViewTreeObserver.OnPreDrawListener {
    @DexIgnore
    public /* final */ qd7 b;
    @DexIgnore
    public /* final */ WeakReference<ImageView> c;
    @DexIgnore
    public zc7 d;

    @DexIgnore
    public cd7(qd7 qd7, ImageView imageView, zc7 zc7) {
        this.b = qd7;
        this.c = new WeakReference<>(imageView);
        this.d = zc7;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @DexIgnore
    public void a() {
        this.d = null;
        ImageView imageView = this.c.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        ImageView imageView = this.c.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width > 0 && height > 0) {
                    viewTreeObserver.removeOnPreDrawListener(this);
                    qd7 qd7 = this.b;
                    qd7.k();
                    qd7.i(width, height);
                    qd7.e(imageView, this.d);
                }
            }
        }
        return true;
    }
}
