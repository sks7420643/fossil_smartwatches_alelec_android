package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface o71 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final a f2640a = a.f2641a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ a f2641a; // = new a();

        @DexIgnore
        public final o71 a(Context context, b bVar) {
            pq7.c(context, "context");
            pq7.c(bVar, "listener");
            ConnectivityManager connectivityManager = (ConnectivityManager) gl0.j(context, ConnectivityManager.class);
            if (connectivityManager != null) {
                if (gl0.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                    try {
                        return Build.VERSION.SDK_INT >= 21 ? new q71(connectivityManager, bVar) : new p71(context, connectivityManager, bVar);
                    } catch (Exception e) {
                        x81.a("NetworkObserverStrategy", new RuntimeException("Failed to register network observer.", e));
                        return l71.b;
                    }
                }
            }
            if (q81.c.a() && q81.c.b() <= 5) {
                Log.println(5, "NetworkObserverStrategy", "Unable to register network observer.");
            }
            return l71.b;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    Object start();  // void declaration

    @DexIgnore
    Object stop();  // void declaration
}
