package com.fossil;

import com.zendesk.sdk.network.Constants;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ed4 extends k84 implements fd4 {
    @DexIgnore
    public x74 f;

    @DexIgnore
    public ed4(String str, String str2, kb4 kb4) {
        this(str, str2, kb4, ib4.GET, x74.f());
    }

    @DexIgnore
    public ed4(String str, String str2, kb4 kb4, ib4 ib4, x74 x74) {
        super(str, str2, kb4, ib4);
        this.f = x74;
    }

    @DexIgnore
    @Override // com.fossil.fd4
    public JSONObject a(bd4 bd4, boolean z) {
        if (z) {
            try {
                Map<String, String> j = j(bd4);
                jb4 d = d(j);
                g(d, bd4);
                x74 x74 = this.f;
                x74.b("Requesting settings from " + e());
                x74 x742 = this.f;
                x742.b("Settings query params were: " + j);
                lb4 b = d.b();
                x74 x743 = this.f;
                x743.b("Settings request ID: " + b.d("X-REQUEST-ID"));
                return k(b);
            } catch (IOException e) {
                this.f.e("Settings request failed.", e);
                return null;
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public final jb4 g(jb4 jb4, bd4 bd4) {
        h(jb4, "X-CRASHLYTICS-GOOGLE-APP-ID", bd4.f415a);
        h(jb4, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        h(jb4, "X-CRASHLYTICS-API-CLIENT-VERSION", w84.i());
        h(jb4, Constants.ACCEPT_HEADER, Constants.APPLICATION_JSON);
        h(jb4, "X-CRASHLYTICS-DEVICE-MODEL", bd4.b);
        h(jb4, "X-CRASHLYTICS-OS-BUILD-VERSION", bd4.c);
        h(jb4, "X-CRASHLYTICS-OS-DISPLAY-VERSION", bd4.d);
        h(jb4, "X-CRASHLYTICS-INSTALLATION-ID", bd4.e.a());
        return jb4;
    }

    @DexIgnore
    public final void h(jb4 jb4, String str, String str2) {
        if (str2 != null) {
            jb4.d(str, str2);
        }
    }

    @DexIgnore
    public final JSONObject i(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            x74 x74 = this.f;
            x74.c("Failed to parse settings JSON from " + e(), e);
            x74 x742 = this.f;
            x742.b("Settings response " + str);
            return null;
        }
    }

    @DexIgnore
    public final Map<String, String> j(bd4 bd4) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", bd4.h);
        hashMap.put("display_version", bd4.g);
        hashMap.put("source", Integer.toString(bd4.i));
        String str = bd4.f;
        if (!r84.D(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    @DexIgnore
    public JSONObject k(lb4 lb4) {
        int b = lb4.b();
        x74 x74 = this.f;
        x74.b("Settings result was: " + b);
        if (l(b)) {
            return i(lb4.a());
        }
        x74 x742 = this.f;
        x742.d("Failed to retrieve settings from " + e());
        return null;
    }

    @DexIgnore
    public boolean l(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }
}
