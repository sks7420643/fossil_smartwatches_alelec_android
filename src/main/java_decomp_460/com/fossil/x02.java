package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f4026a;
    @DexIgnore
    public /* final */ t32 b;
    @DexIgnore
    public /* final */ t32 c;

    @DexIgnore
    public x02(Context context, t32 t32, t32 t322) {
        this.f4026a = context;
        this.b = t32;
        this.c = t322;
    }

    @DexIgnore
    public w02 a(String str) {
        return w02.a(this.f4026a, this.b, this.c, str);
    }
}
