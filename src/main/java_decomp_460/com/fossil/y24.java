package com.fossil;

import com.fossil.u24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y24<E> extends u24<E> implements List<E>, RandomAccess {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends o14<E> {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.o14
        public E a(int i) {
            return (E) y24.this.get(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<E> extends u24.a<E> {
        @DexIgnore
        public b() {
            this(4);
        }

        @DexIgnore
        public b(int i) {
            super(i);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.u24.b
        public /* bridge */ /* synthetic */ u24.b a(Object obj) {
            g(obj);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<E> g(E e) {
            super.e(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<E> h(Iterator<? extends E> it) {
            super.c(it);
            return this;
        }

        @DexIgnore
        public y24<E> i() {
            return y24.asImmutableList(this.f3507a, this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<E> extends y24<E> {
        @DexIgnore
        public /* final */ transient y24<E> b;

        @DexIgnore
        public c(y24<E> y24) {
            this.b = y24;
        }

        @DexIgnore
        public final int b(int i) {
            return (size() - 1) - i;
        }

        @DexIgnore
        public final int c(int i) {
            return size() - i;
        }

        @DexIgnore
        @Override // com.fossil.u24, com.fossil.y24
        public boolean contains(Object obj) {
            return this.b.contains(obj);
        }

        @DexIgnore
        @Override // java.util.List
        public E get(int i) {
            i14.j(i, size());
            return this.b.get(b(i));
        }

        @DexIgnore
        @Override // com.fossil.y24
        public int indexOf(Object obj) {
            int lastIndexOf = this.b.lastIndexOf(obj);
            if (lastIndexOf >= 0) {
                return b(lastIndexOf);
            }
            return -1;
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return this.b.isPartialView();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.u24, com.fossil.u24, com.fossil.y24, com.fossil.y24, java.lang.Iterable
        public /* bridge */ /* synthetic */ Iterator iterator() {
            return y24.super.iterator();
        }

        @DexIgnore
        @Override // com.fossil.y24
        public int lastIndexOf(Object obj) {
            int indexOf = this.b.indexOf(obj);
            if (indexOf >= 0) {
                return b(indexOf);
            }
            return -1;
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.y24, com.fossil.y24
        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return y24.super.listIterator();
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.y24, com.fossil.y24
        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return y24.super.listIterator(i);
        }

        @DexIgnore
        @Override // com.fossil.y24
        public y24<E> reverse() {
            return this.b;
        }

        @DexIgnore
        public int size() {
            return this.b.size();
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.y24, com.fossil.y24
        public y24<E> subList(int i, int i2) {
            i14.r(i, i2, size());
            return this.b.subList(c(i2), c(i)).reverse();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public d(Object[] objArr) {
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            return y24.copyOf(this.elements);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends y24<E> {
        @DexIgnore
        public /* final */ transient int length;
        @DexIgnore
        public /* final */ transient int offset;

        @DexIgnore
        public e(int i, int i2) {
            this.offset = i;
            this.length = i2;
        }

        @DexIgnore
        @Override // java.util.List
        public E get(int i) {
            i14.j(i, this.length);
            return (E) y24.this.get(this.offset + i);
        }

        @DexIgnore
        @Override // com.fossil.u24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.u24, com.fossil.u24, com.fossil.y24, com.fossil.y24, java.lang.Iterable
        public /* bridge */ /* synthetic */ Iterator iterator() {
            return y24.super.iterator();
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.y24, com.fossil.y24
        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return y24.super.listIterator();
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.y24, com.fossil.y24
        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return y24.super.listIterator(i);
        }

        @DexIgnore
        public int size() {
            return this.length;
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.y24, com.fossil.y24
        public y24<E> subList(int i, int i2) {
            i14.r(i, i2, this.length);
            y24 y24 = y24.this;
            int i3 = this.offset;
            return y24.subList(i + i3, i3 + i2);
        }
    }

    @DexIgnore
    public static <E> y24<E> a(Object... objArr) {
        h44.c(objArr);
        return asImmutableList(objArr);
    }

    @DexIgnore
    public static <E> y24<E> asImmutableList(Object[] objArr) {
        return asImmutableList(objArr, objArr.length);
    }

    @DexIgnore
    public static <E> y24<E> asImmutableList(Object[] objArr, int i) {
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return new z44(objArr[0]);
        }
        if (i < objArr.length) {
            objArr = h44.a(objArr, i);
        }
        return new o44(objArr);
    }

    @DexIgnore
    public static <E> b<E> builder() {
        return new b<>();
    }

    @DexIgnore
    public static <E> y24<E> copyOf(Iterable<? extends E> iterable) {
        i14.l(iterable);
        return iterable instanceof Collection ? copyOf((Collection) iterable) : copyOf(iterable.iterator());
    }

    @DexIgnore
    public static <E> y24<E> copyOf(Collection<? extends E> collection) {
        if (!(collection instanceof u24)) {
            return a(collection.toArray());
        }
        y24<E> asList = ((u24) collection).asList();
        return asList.isPartialView() ? asImmutableList(asList.toArray()) : asList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.y24$b */
    /* JADX WARN: Multi-variable type inference failed */
    public static <E> y24<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        b bVar = new b();
        bVar.g(next);
        bVar.h(it);
        return bVar.i();
    }

    @DexIgnore
    public static <E> y24<E> copyOf(E[] eArr) {
        int length = eArr.length;
        if (length == 0) {
            return of();
        }
        if (length == 1) {
            return new z44(eArr[0]);
        }
        Object[] objArr = (Object[]) eArr.clone();
        h44.c(objArr);
        return new o44(objArr);
    }

    @DexIgnore
    public static <E> y24<E> of() {
        return (y24<E>) o44.EMPTY;
    }

    @DexIgnore
    public static <E> y24<E> of(E e2) {
        return new z44(e2);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3) {
        return a(e2, e3);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4) {
        return a(e2, e3, e4);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5) {
        return a(e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6) {
        return a(e2, e3, e4, e5, e6);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7) {
        return a(e2, e3, e4, e5, e6, e7);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        return a(e2, e3, e4, e5, e6, e7, e8);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9, e10);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9, e10, e11);
    }

    @DexIgnore
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11, E e12) {
        return a(e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12);
    }

    @DexIgnore
    @SafeVarargs
    public static <E> y24<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E e11, E e12, E e13, E... eArr) {
        Object[] objArr = new Object[(eArr.length + 12)];
        objArr[0] = e2;
        objArr[1] = e3;
        objArr[2] = e4;
        objArr[3] = e5;
        objArr[4] = e6;
        objArr[5] = e7;
        objArr[6] = e8;
        objArr[7] = e9;
        objArr[8] = e10;
        objArr[9] = e11;
        objArr[10] = e12;
        objArr[11] = e13;
        System.arraycopy(eArr, 0, objArr, 12, eArr.length);
        return a(objArr);
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final void add(int i, E e2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.u24
    public final y24<E> asList() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public int copyIntoArray(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return t34.b(this, obj);
    }

    @DexIgnore
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    @DexIgnore
    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return t34.c(this, obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.u24, com.fossil.u24, java.lang.Iterable
    public h54<E> iterator() {
        return listIterator();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return t34.e(this, obj);
    }

    @DexIgnore
    @Override // java.util.List
    public i54<E> listIterator() {
        return listIterator(0);
    }

    @DexIgnore
    @Override // java.util.List
    public i54<E> listIterator(int i) {
        return new a(size(), i);
    }

    @DexIgnore
    @Override // java.util.List
    @CanIgnoreReturnValue
    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public y24<E> reverse() {
        return size() <= 1 ? this : new c(this);
    }

    @DexIgnore
    @Override // java.util.List
    @CanIgnoreReturnValue
    @Deprecated
    public final E set(int i, E e2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    public y24<E> subList(int i, int i2) {
        i14.r(i, i2, size());
        int i3 = i2 - i;
        return i3 == size() ? this : i3 != 0 ? i3 != 1 ? subListUnchecked(i, i2) : of((Object) get(i)) : of();
    }

    @DexIgnore
    public y24<E> subListUnchecked(int i, int i2) {
        return new e(i, i2 - i);
    }

    @DexIgnore
    @Override // com.fossil.u24
    public Object writeReplace() {
        return new d(toArray());
    }
}
