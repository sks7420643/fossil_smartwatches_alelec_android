package com.fossil;

import java.nio.ByteBuffer;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vr {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public short f3809a; // = ((short) -1);
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ n6 f;

    @DexIgnore
    public vr(byte[] bArr, int i, n6 n6Var) {
        this.d = bArr;
        this.e = i;
        this.f = n6Var;
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        pq7.b(wrap, "ByteBuffer.wrap(data)");
        this.b = wrap;
        byte[] bArr2 = this.d;
        this.c = bArr2.length;
        ByteBuffer wrap2 = ByteBuffer.wrap(bArr2);
        pq7.b(wrap2, "ByteBuffer.wrap(data)");
        this.b = wrap2;
    }

    @DexIgnore
    public final byte[] a() {
        int remaining = this.b.remaining();
        if (remaining <= 0) {
            return new byte[0];
        }
        this.f3809a = (short) ((short) (this.f3809a + 1));
        int min = Math.min(remaining + 1, this.e);
        byte[] copyOfRange = Arrays.copyOfRange(new byte[]{(byte) (((wr) this).f3809a % 256)}, 0, min);
        this.b.get(copyOfRange, 1, min - 1);
        pq7.b(copyOfRange, "bytes");
        return copyOfRange;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public final int c() {
        return Math.min((this.f3809a + 1) * (this.e - b()), this.c);
    }
}
