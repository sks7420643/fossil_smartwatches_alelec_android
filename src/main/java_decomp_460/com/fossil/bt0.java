package com.fossil;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bt0<Params, Progress, Result> {
    @DexIgnore
    public static /* final */ ThreadFactory g; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> h; // = new LinkedBlockingQueue(10);
    @DexIgnore
    public static /* final */ Executor i; // = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, h, g);
    @DexIgnore
    public static f j;
    @DexIgnore
    public /* final */ h<Params, Result> b; // = new b();
    @DexIgnore
    public /* final */ FutureTask<Result> c; // = new c(this.b);
    @DexIgnore
    public volatile g d; // = g.PENDING;
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AtomicInteger f500a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "ModernAsyncTask #" + this.f500a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends h<Params, Result> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Result call() throws Exception {
            bt0.this.f.set(true);
            Result result = null;
            try {
                Process.setThreadPriority(10);
                result = (Result) bt0.this.b(this.f503a);
                Binder.flushPendingCommands();
                bt0.this.l(result);
                return result;
            } catch (Throwable th) {
                bt0.this.l(result);
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends FutureTask<Result> {
        @DexIgnore
        public c(Callable callable) {
            super(callable);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.fossil.bt0 */
        /* JADX WARN: Multi-variable type inference failed */
        public void done() {
            try {
                bt0.this.m(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occurred while executing doInBackground()", e2.getCause());
            } catch (CancellationException e3) {
                bt0.this.m(null);
            } catch (Throwable th) {
                throw new RuntimeException("An error occurred while executing doInBackground()", th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f501a;

        /*
        static {
            int[] iArr = new int[g.values().length];
            f501a = iArr;
            try {
                iArr[g.RUNNING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f501a[g.FINISHED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Data> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ bt0 f502a;
        @DexIgnore
        public /* final */ Data[] b;

        @DexIgnore
        public e(bt0 bt0, Data... dataArr) {
            this.f502a = bt0;
            this.b = dataArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends Handler {
        @DexIgnore
        public f() {
            super(Looper.getMainLooper());
        }

        @DexIgnore
        public void handleMessage(Message message) {
            e eVar = (e) message.obj;
            int i = message.what;
            if (i == 1) {
                eVar.f502a.d(eVar.b[0]);
            } else if (i == 2) {
                eVar.f502a.k(eVar.b);
            }
        }
    }

    @DexIgnore
    public enum g {
        PENDING,
        RUNNING,
        FINISHED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h<Params, Result> implements Callable<Result> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Params[] f503a;
    }

    @DexIgnore
    public static Handler e() {
        f fVar;
        synchronized (bt0.class) {
            try {
                if (j == null) {
                    j = new f();
                }
                fVar = j;
            } catch (Throwable th) {
                throw th;
            }
        }
        return fVar;
    }

    @DexIgnore
    public final boolean a(boolean z) {
        this.e.set(true);
        return this.c.cancel(z);
    }

    @DexIgnore
    public abstract Result b(Params... paramsArr);

    @DexIgnore
    public final bt0<Params, Progress, Result> c(Executor executor, Params... paramsArr) {
        if (this.d != g.PENDING) {
            int i2 = d.f501a[this.d.ordinal()];
            if (i2 == 1) {
                throw new IllegalStateException("Cannot execute task: the task is already running.");
            } else if (i2 != 2) {
                throw new IllegalStateException("We should never reach this state");
            } else {
                throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        } else {
            this.d = g.RUNNING;
            j();
            this.b.f503a = paramsArr;
            executor.execute(this.c);
            return this;
        }
    }

    @DexIgnore
    public void d(Result result) {
        if (f()) {
            h(result);
        } else {
            i(result);
        }
        this.d = g.FINISHED;
    }

    @DexIgnore
    public final boolean f() {
        return this.e.get();
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h(Result result) {
        g();
    }

    @DexIgnore
    public void i(Result result) {
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public void k(Progress... progressArr) {
    }

    @DexIgnore
    public Result l(Result result) {
        e().obtainMessage(1, new e(this, result)).sendToTarget();
        return result;
    }

    @DexIgnore
    public void m(Result result) {
        if (!this.f.get()) {
            l(result);
        }
    }
}
