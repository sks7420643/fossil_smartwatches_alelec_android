package com.fossil;

import android.media.RemoteController;
import android.view.KeyEvent;
import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tr5 extends qr5 {
    @DexIgnore
    public /* final */ String c; // = "OldMusicController";
    @DexIgnore
    public /* final */ RemoteController.OnClientUpdateListener d;
    @DexIgnore
    public /* final */ RemoteController e;
    @DexIgnore
    public int f;
    @DexIgnore
    public rr5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements RemoteController.OnClientUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tr5 f3455a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(tr5 tr5, String str) {
            this.f3455a = tr5;
            this.b = str;
        }

        @DexIgnore
        public void onClientChange(boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.f3455a.c;
            local.d(str, "SystemCallback of " + this.b + " - onClientChange clearing=" + z);
        }

        @DexIgnore
        public void onClientMetadataUpdate(RemoteController.MetadataEditor metadataEditor) {
            if (metadataEditor != null) {
                String b2 = ij5.b(metadataEditor.getString(7, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String b3 = ij5.b(metadataEditor.getString(2, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String b4 = ij5.b(metadataEditor.getString(1, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.f3455a.c;
                local.d(str, "SystemCallback of " + this.b + " - onMetadataChanged, title=" + b2 + ", artist=" + b3 + ", album=" + b4);
                rr5 rr5 = new rr5(this.f3455a.d(), this.b, b2, b3, b4);
                if (!pq7.a(rr5, this.f3455a.h())) {
                    rr5 h = this.f3455a.h();
                    this.f3455a.l(rr5);
                    this.f3455a.j(h, rr5);
                }
            }
        }

        @DexIgnore
        public void onClientPlaybackStateUpdate(int i) {
            int g = this.f3455a.g(i);
            if (g != this.f3455a.i()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.f3455a.c;
                local.d(str, "SystemCallback of " + this.b + " - onClientPlaybackStateUpdate(), state=" + i);
                int i2 = this.f3455a.i();
                this.f3455a.m(g);
                tr5 tr5 = this.f3455a;
                tr5.k(i2, g, tr5);
            }
        }

        @DexIgnore
        public void onClientPlaybackStateUpdate(int i, long j, long j2, float f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.f3455a.c;
            local.d(str, "SystemCallback of " + this.b + " - onClientPlaybackStateUpdate(), state=" + i + ", stateChangeTimeMs=" + j + ", currentPosMs=" + j2 + ", speed=" + f);
        }

        @DexIgnore
        public void onClientTransportControlUpdate(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.f3455a.c;
            local.d(str, "SystemCallback of " + this.b + " - onClientTransportControlUpdate(), transportControlFlags=" + i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tr5(String str) {
        super(str, "All apps");
        pq7.c(str, "appName");
        this.d = new a(this, str);
        this.g = new rr5(d(), str, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
        this.e = new RemoteController(PortfolioApp.h0.c(), this.d);
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public boolean a(KeyEvent keyEvent) {
        pq7.c(keyEvent, "keyEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, ".dispatchMediaButtonEvent of " + d() + " keyEvent " + keyEvent);
        return this.e.sendMediaKeyEvent(keyEvent);
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public rr5 c() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public int e() {
        return this.f;
    }

    @DexIgnore
    public final int g(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 10;
            case 7:
                return 9;
            case 8:
                return 6;
            case 9:
                return 7;
            default:
                return 0;
        }
    }

    @DexIgnore
    public final rr5 h() {
        return this.g;
    }

    @DexIgnore
    public final int i() {
        return this.f;
    }

    @DexIgnore
    public void j(rr5 rr5, rr5 rr52) {
        pq7.c(rr5, "oldMetadata");
        pq7.c(rr52, "newMetadata");
    }

    @DexIgnore
    public void k(int i, int i2, qr5 qr5) {
        pq7.c(qr5, "controller");
    }

    @DexIgnore
    public final void l(rr5 rr5) {
        pq7.c(rr5, "<set-?>");
        this.g = rr5;
    }

    @DexIgnore
    public final void m(int i) {
        this.f = i;
    }
}
