package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ int f4222a; // = Runtime.getRuntime().availableProcessors();

    @DexIgnore
    public static final int a() {
        return f4222a;
    }

    @DexIgnore
    public static final String b(String str) {
        try {
            return System.getProperty(str);
        } catch (SecurityException e) {
            return null;
        }
    }
}
