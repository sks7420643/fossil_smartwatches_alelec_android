package com.fossil;

import android.net.Uri;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n41 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Uri f2463a;
    @DexIgnore
    public Uri b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Uri f2464a;

        @DexIgnore
        public a(String str, String str2, Uri uri, String str3) {
            this.f2464a = uri;
        }
    }

    @DexIgnore
    public n41(Uri uri, List<a> list, Uri uri2) {
        this.f2463a = uri;
        if (list == null) {
            Collections.emptyList();
        }
        this.b = uri2;
    }
}
