package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp4 implements Factory<PortfolioApp> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f474a;

    @DexIgnore
    public bp4(uo4 uo4) {
        this.f474a = uo4;
    }

    @DexIgnore
    public static bp4 a(uo4 uo4) {
        return new bp4(uo4);
    }

    @DexIgnore
    public static PortfolioApp c(uo4 uo4) {
        PortfolioApp g = uo4.g();
        lk7.c(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }

    @DexIgnore
    /* renamed from: b */
    public PortfolioApp get() {
        return c(this.f474a);
    }
}
