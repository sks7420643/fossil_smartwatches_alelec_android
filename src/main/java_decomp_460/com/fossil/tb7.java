package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb7 implements qb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public mb7 f3392a;

    @DexIgnore
    public tb7(mb7 mb7) {
        pq7.c(mb7, "textData");
        this.f3392a = mb7;
    }

    @DexIgnore
    public final mb7 a() {
        return this.f3392a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof tb7) && pq7.a(this.f3392a, ((tb7) obj).f3392a));
    }

    @DexIgnore
    public int hashCode() {
        mb7 mb7 = this.f3392a;
        if (mb7 != null) {
            return mb7.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "UIText(textData=" + this.f3392a + ")";
    }
}
