package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ fn0 f1332a; // = new e(null, false);
    @DexIgnore
    public static /* final */ fn0 b; // = new e(null, true);
    @DexIgnore
    public static /* final */ fn0 c; // = new e(b.f1334a, false);
    @DexIgnore
    public static /* final */ fn0 d; // = new e(b.f1334a, true);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c {
        @DexIgnore
        public static /* final */ a b; // = new a(true);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f1333a;

        @DexIgnore
        public a(boolean z) {
            this.f1333a = z;
        }

        @DexIgnore
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:20:0x0018 */
        @Override // com.fossil.gn0.c
        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i;
            boolean z = false;
            while (i3 < i2 + i) {
                int a2 = gn0.a(Character.getDirectionality(charSequence.charAt(i3)));
                if (a2 == 0) {
                    if (this.f1333a) {
                        return 0;
                    }
                    z = true;
                } else if (a2 == 1) {
                    if (!this.f1333a) {
                        return 1;
                    }
                    z = true;
                } else {
                    continue;
                }
                i3++;
                z = z;
            }
            if (z) {
                return this.f1333a ? 1 : 0;
            }
            return 2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ b f1334a; // = new b();

        @DexIgnore
        @Override // com.fossil.gn0.c
        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = 2;
            for (int i4 = i; i4 < i2 + i && i3 == 2; i4++) {
                i3 = gn0.b(Character.getDirectionality(charSequence.charAt(i4)));
            }
            return i3;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        int a(CharSequence charSequence, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d implements fn0 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ c f1335a;

        @DexIgnore
        public d(c cVar) {
            this.f1335a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.fn0
        public boolean a(CharSequence charSequence, int i, int i2) {
            if (charSequence != null && i >= 0 && i2 >= 0 && charSequence.length() - i2 >= i) {
                return this.f1335a == null ? b() : c(charSequence, i, i2);
            }
            throw new IllegalArgumentException();
        }

        @DexIgnore
        public abstract boolean b();

        @DexIgnore
        public final boolean c(CharSequence charSequence, int i, int i2) {
            int a2 = this.f1335a.a(charSequence, i, i2);
            if (a2 == 0) {
                return true;
            }
            if (a2 != 1) {
                return b();
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends d {
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(c cVar, boolean z) {
            super(cVar);
            this.b = z;
        }

        @DexIgnore
        @Override // com.fossil.gn0.d
        public boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends d {
        @DexIgnore
        public static /* final */ f b; // = new f();

        @DexIgnore
        public f() {
            super(null);
        }

        @DexIgnore
        @Override // com.fossil.gn0.d
        public boolean b() {
            return hn0.b(Locale.getDefault()) == 1;
        }
    }

    /*
    static {
        a aVar = a.b;
        f fVar = f.b;
    }
    */

    @DexIgnore
    public static int a(int i) {
        if (i != 0) {
            return (i == 1 || i == 2) ? 0 : 2;
        }
        return 1;
    }

    @DexIgnore
    public static int b(int i) {
        if (i != 0) {
            if (!(i == 1 || i == 2)) {
                switch (i) {
                    case 14:
                    case 15:
                        break;
                    case 16:
                    case 17:
                        break;
                    default:
                        return 2;
                }
            }
            return 0;
        }
        return 1;
    }
}
