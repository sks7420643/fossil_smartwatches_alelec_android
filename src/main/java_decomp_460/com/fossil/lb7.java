package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum lb7 {
    STEP,
    ACTIVE,
    BATTERY,
    CALORIES,
    RAIN,
    DATE,
    SECOND_TIME,
    WEATHER,
    HEART,
    TICKER,
    TEXT;
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final lb7 a(String str) {
            pq7.c(str, "id");
            switch (str.hashCode()) {
                case -331239923:
                    if (str.equals(Constants.BATTERY)) {
                        return lb7.BATTERY;
                    }
                    break;
                case -168965370:
                    if (str.equals("calories")) {
                        return lb7.CALORIES;
                    }
                    break;
                case -85386984:
                    if (str.equals("active-minutes")) {
                        return lb7.ACTIVE;
                    }
                    break;
                case -48173007:
                    if (str.equals("chance-of-rain")) {
                        return lb7.RAIN;
                    }
                    break;
                case 3076014:
                    if (str.equals("date")) {
                        return lb7.DATE;
                    }
                    break;
                case 109761319:
                    if (str.equals("steps")) {
                        return lb7.STEP;
                    }
                    break;
                case 134170930:
                    if (str.equals("second-timezone")) {
                        return lb7.SECOND_TIME;
                    }
                    break;
                case 1223440372:
                    if (str.equals("weather")) {
                        return lb7.WEATHER;
                    }
                    break;
                case 1884273159:
                    if (str.equals("heart-rate")) {
                        return lb7.HEART;
                    }
                    break;
            }
            return lb7.STEP;
        }

        @DexIgnore
        public final String b(lb7 lb7) {
            pq7.c(lb7, "type");
            switch (kb7.f1896a[lb7.ordinal()]) {
                case 1:
                    return "steps";
                case 2:
                    return "active-minutes";
                case 3:
                    return Constants.BATTERY;
                case 4:
                    return "calories";
                case 5:
                    return "chance-of-rain";
                case 6:
                    return "date";
                case 7:
                    return "second-timezone";
                case 8:
                    return "weather";
                case 9:
                    return "heart-rate";
                default:
                    return "empty";
            }
        }
    }
}
