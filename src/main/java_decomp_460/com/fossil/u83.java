package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u83 implements r83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3524a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.scheduler.task_thread.cleanup_on_exit", false);

    @DexIgnore
    @Override // com.fossil.r83
    public final boolean zza() {
        return f3524a.o().booleanValue();
    }
}
