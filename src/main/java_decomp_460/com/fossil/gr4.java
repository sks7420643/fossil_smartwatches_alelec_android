package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr4 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ArrayList<Explore> f1347a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public FlexibleTextView f1348a;
        @DexIgnore
        public ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gr4 gr4, View view) {
            super(view);
            pq7.c(view, "view");
            this.f1348a = (FlexibleTextView) view.findViewById(2131362386);
            this.b = (ImageView) view.findViewById(2131362726);
        }

        @DexIgnore
        public final void a(Explore explore, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareTutorialAdapter", "build - position=" + i);
            if (explore != null) {
                FlexibleTextView flexibleTextView = this.f1348a;
                pq7.b(flexibleTextView, "ftvContent");
                flexibleTextView.setText(explore.getDescription());
                ImageView imageView = this.b;
                pq7.b(imageView, "ivIcon");
                imageView.setBackground(gl0.f(PortfolioApp.h0.c(), explore.getBackground()));
            }
        }
    }

    @DexIgnore
    public gr4(ArrayList<Explore> arrayList) {
        pq7.c(arrayList, "mData");
        this.f1347a = arrayList;
    }

    @DexIgnore
    /* renamed from: g */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558687, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026_tutorial, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f1347a.size();
    }

    @DexIgnore
    public final void h(List<? extends Explore> list) {
        pq7.c(list, "data");
        this.f1347a.clear();
        this.f1347a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        if (getItemCount() > i && i != -1) {
            ((a) viewHolder).a(this.f1347a.get(i), i);
        }
    }
}
