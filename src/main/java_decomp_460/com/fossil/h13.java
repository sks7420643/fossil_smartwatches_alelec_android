package com.fossil;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h13 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Charset f1410a; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ byte[] b;

    /*
    static {
        Charset.forName("ISO-8859-1");
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        byte[] bArr2 = b;
        j03.b(bArr2, 0, bArr2.length, false);
    }
    */

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    @DexIgnore
    public static int b(long j) {
        return (int) ((j >>> 32) ^ j);
    }

    @DexIgnore
    public static int c(boolean z) {
        return z ? 1231 : 1237;
    }

    @DexIgnore
    public static <T> T d(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    public static Object e(Object obj, Object obj2) {
        p23 e = ((m23) obj).e();
        e.F((m23) obj2);
        return e.b();
    }

    @DexIgnore
    public static <T> T f(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static boolean g(m23 m23) {
        if (!(m23 instanceof pz2)) {
            return false;
        }
        pz2 pz2 = (pz2) m23;
        return false;
    }

    @DexIgnore
    public static boolean h(byte[] bArr) {
        return g43.f(bArr);
    }

    @DexIgnore
    public static String i(byte[] bArr) {
        return new String(bArr, f1410a);
    }

    @DexIgnore
    public static int j(byte[] bArr) {
        int length = bArr.length;
        int a2 = a(length, bArr, 0, length);
        if (a2 == 0) {
            return 1;
        }
        return a2;
    }
}
