package com.fossil;

import com.fossil.tq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vq4 {
    @DexIgnore
    <R extends tq4.c, E extends tq4.a> void a(R r, tq4.d<R, E> dVar);

    @DexIgnore
    <R extends tq4.c, E extends tq4.a> void b(E e, tq4.d<R, E> dVar);

    @DexIgnore
    void execute(Runnable runnable);
}
