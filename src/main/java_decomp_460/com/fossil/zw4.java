package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationActivity;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw4 extends pv5 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public g37<t75> g;
    @DexIgnore
    public /* final */ List<Fragment> h; // = new ArrayList();
    @DexIgnore
    public int i;
    @DexIgnore
    public String j; // = "";
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return zw4.l;
        }

        @DexIgnore
        public final zw4 b() {
            return new zw4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements TabLayout.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zw4 f4550a;

        @DexIgnore
        public b(zw4 zw4) {
            this.f4550a = zw4;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            if (gVar != null) {
                this.f4550a.O6(gVar.f());
            }
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ zw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zw4 zw4) {
            super(1);
            this.this$0 = zw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            BCFindFriendsActivity.A.a(this.this$0, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ zw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(zw4 zw4) {
            super(1);
            this.this$0 = zw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            BCNotificationActivity.A.a(this.this$0);
        }
    }

    /*
    static {
        String simpleName = zw4.class.getSimpleName();
        pq7.b(simpleName, "BCTabsFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public final void M6() {
        g37<t75> g37 = this.g;
        if (g37 != null) {
            t75 a2 = g37.a();
            if (a2 != null) {
                a2.u.c(new b(this));
                RTLImageView rTLImageView = a2.r;
                pq7.b(rTLImageView, "imgSearch");
                fz4.a(rTLImageView, new c(this));
                RTLImageView rTLImageView2 = a2.q;
                pq7.b(rTLImageView2, "imgNotification");
                fz4.a(rTLImageView2, new d(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void N6() {
        FLogger.INSTANCE.getLocal().d(l, "Inside .initTabs");
        Fragment Z = getChildFragmentManager().Z(dx4.s.a());
        Fragment Z2 = getChildFragmentManager().Z(by4.m.a());
        Fragment Z3 = getChildFragmentManager().Z(ky4.s.a());
        if (Z == null) {
            Z = dx4.s.b();
        }
        if (Z2 == null) {
            Z2 = by4.m.b();
        }
        if (Z3 == null) {
            Z3 = ky4.s.b();
        }
        this.h.clear();
        this.h.add(Z);
        this.h.add(Z2);
        this.h.add(Z3);
        yw4 yw4 = new yw4(this.h, this);
        g37<t75> g37 = this.g;
        if (g37 != null) {
            t75 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.t;
                pq7.b(viewPager2, "this");
                viewPager2.setAdapter(yw4);
                viewPager2.setUserInputEnabled(false);
                viewPager2.setOffscreenPageLimit(this.h.size());
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.e(str, "scroll to position=" + i2 + " fragment " + this.h.get(i2));
        this.i = i2;
        g37<t75> g37 = this.g;
        if (g37 != null) {
            t75 a2 = g37.a();
            if (a2 != null) {
                a2.t.j(this.i, true);
                Fragment fragment = this.h.get(i2);
                if (fragment instanceof by4) {
                    RTLImageView rTLImageView = a2.r;
                    pq7.b(rTLImageView, "imgSearch");
                    rTLImageView.setVisibility(0);
                    this.j = "bc_friend_tab";
                } else if (fragment instanceof ky4) {
                    RTLImageView rTLImageView2 = a2.r;
                    pq7.b(rTLImageView2, "imgSearch");
                    rTLImageView2.setVisibility(4);
                    this.j = "bc_history_tab";
                } else {
                    this.j = "";
                    RTLImageView rTLImageView3 = a2.r;
                    pq7.b(rTLImageView3, "imgSearch");
                    rTLImageView3.setVisibility(4);
                }
                ck5 g2 = ck5.f.g();
                String str2 = this.j;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    g2.m(str2, activity);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.app.Activity");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        t75 t75 = (t75) aq0.f(layoutInflater, 2131558573, viewGroup, false, A6());
        this.g = new g37<>(this, t75);
        pq7.b(t75, "binding");
        return t75.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ck5 g2 = ck5.f.g();
        String str = this.j;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m(str, activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        M6();
        N6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
