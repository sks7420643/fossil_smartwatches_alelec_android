package com.fossil;

import android.util.Log;
import com.fossil.be1;
import com.fossil.cd1;
import com.fossil.ie1;
import com.fossil.kk1;
import com.fossil.uc1;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xc1 implements zc1, ie1.a, cd1.a {
    @DexIgnore
    public static /* final */ boolean i; // = Log.isLoggable("Engine", 2);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ fd1 f4089a;
    @DexIgnore
    public /* final */ bd1 b;
    @DexIgnore
    public /* final */ ie1 c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ ld1 e;
    @DexIgnore
    public /* final */ c f;
    @DexIgnore
    public /* final */ a g;
    @DexIgnore
    public /* final */ nc1 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ uc1.e f4090a;
        @DexIgnore
        public /* final */ mn0<uc1<?>> b; // = kk1.d(150, new C0281a());
        @DexIgnore
        public int c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xc1$a$a")
        /* renamed from: com.fossil.xc1$a$a  reason: collision with other inner class name */
        public class C0281a implements kk1.d<uc1<?>> {
            @DexIgnore
            public C0281a() {
            }

            @DexIgnore
            /* renamed from: a */
            public uc1<?> create() {
                a aVar = a.this;
                return new uc1<>(aVar.f4090a, aVar.b);
            }
        }

        @DexIgnore
        public a(uc1.e eVar) {
            this.f4090a = eVar;
        }

        @DexIgnore
        public <R> uc1<R> a(qa1 qa1, Object obj, ad1 ad1, mb1 mb1, int i, int i2, Class<?> cls, Class<R> cls2, sa1 sa1, wc1 wc1, Map<Class<?>, sb1<?>> map, boolean z, boolean z2, boolean z3, ob1 ob1, uc1.b<R> bVar) {
            uc1<?> b2 = this.b.b();
            ik1.d(b2);
            uc1<R> uc1 = (uc1<R>) b2;
            int i3 = this.c;
            this.c = i3 + 1;
            uc1.p(qa1, obj, ad1, mb1, i, i2, cls, cls2, sa1, wc1, map, z, z2, z3, ob1, bVar, i3);
            return uc1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ le1 f4092a;
        @DexIgnore
        public /* final */ le1 b;
        @DexIgnore
        public /* final */ le1 c;
        @DexIgnore
        public /* final */ le1 d;
        @DexIgnore
        public /* final */ zc1 e;
        @DexIgnore
        public /* final */ cd1.a f;
        @DexIgnore
        public /* final */ mn0<yc1<?>> g; // = kk1.d(150, new a());

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements kk1.d<yc1<?>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            /* renamed from: a */
            public yc1<?> create() {
                b bVar = b.this;
                return new yc1<>(bVar.f4092a, bVar.b, bVar.c, bVar.d, bVar.e, bVar.f, bVar.g);
            }
        }

        @DexIgnore
        public b(le1 le1, le1 le12, le1 le13, le1 le14, zc1 zc1, cd1.a aVar) {
            this.f4092a = le1;
            this.b = le12;
            this.c = le13;
            this.d = le14;
            this.e = zc1;
            this.f = aVar;
        }

        @DexIgnore
        public <R> yc1<R> a(mb1 mb1, boolean z, boolean z2, boolean z3, boolean z4) {
            yc1<?> b2 = this.g.b();
            ik1.d(b2);
            yc1<R> yc1 = (yc1<R>) b2;
            yc1.l(mb1, z, z2, z3, z4);
            return yc1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements uc1.e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ be1.a f4094a;
        @DexIgnore
        public volatile be1 b;

        @DexIgnore
        public c(be1.a aVar) {
            this.f4094a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.uc1.e
        public be1 a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.f4094a.build();
                    }
                    if (this.b == null) {
                        this.b = new ce1();
                    }
                }
            }
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ yc1<?> f4095a;
        @DexIgnore
        public /* final */ gj1 b;

        @DexIgnore
        public d(gj1 gj1, yc1<?> yc1) {
            this.b = gj1;
            this.f4095a = yc1;
        }

        @DexIgnore
        public void a() {
            synchronized (xc1.this) {
                this.f4095a.r(this.b);
            }
        }
    }

    @DexIgnore
    public xc1(ie1 ie1, be1.a aVar, le1 le1, le1 le12, le1 le13, le1 le14, fd1 fd1, bd1 bd1, nc1 nc1, b bVar, a aVar2, ld1 ld1, boolean z) {
        this.c = ie1;
        this.f = new c(aVar);
        nc1 = nc1 == null ? new nc1(z) : nc1;
        this.h = nc1;
        nc1.f(this);
        this.b = bd1 == null ? new bd1() : bd1;
        this.f4089a = fd1 == null ? new fd1() : fd1;
        this.d = bVar == null ? new b(le1, le12, le13, le14, this, this) : bVar;
        this.g = aVar2 == null ? new a(this.f) : aVar2;
        this.e = ld1 == null ? new ld1() : ld1;
        ie1.e(this);
    }

    @DexIgnore
    public xc1(ie1 ie1, be1.a aVar, le1 le1, le1 le12, le1 le13, le1 le14, boolean z) {
        this(ie1, aVar, le1, le12, le13, le14, null, null, null, null, null, null, z);
    }

    @DexIgnore
    public static void j(String str, long j, mb1 mb1) {
        Log.v("Engine", str + " in " + ek1.a(j) + "ms, key: " + mb1);
    }

    @DexIgnore
    @Override // com.fossil.ie1.a
    public void a(id1<?> id1) {
        this.e.a(id1, true);
    }

    @DexIgnore
    @Override // com.fossil.zc1
    public void b(yc1<?> yc1, mb1 mb1, cd1<?> cd1) {
        synchronized (this) {
            if (cd1 != null) {
                if (cd1.f()) {
                    this.h.a(mb1, cd1);
                }
            }
            this.f4089a.d(mb1, yc1);
        }
    }

    @DexIgnore
    @Override // com.fossil.zc1
    public void c(yc1<?> yc1, mb1 mb1) {
        synchronized (this) {
            this.f4089a.d(mb1, yc1);
        }
    }

    @DexIgnore
    @Override // com.fossil.cd1.a
    public void d(mb1 mb1, cd1<?> cd1) {
        this.h.d(mb1);
        if (cd1.f()) {
            this.c.b(mb1, cd1);
        } else {
            this.e.a(cd1, false);
        }
    }

    @DexIgnore
    public final cd1<?> e(mb1 mb1) {
        id1<?> c2 = this.c.c(mb1);
        if (c2 == null) {
            return null;
        }
        return c2 instanceof cd1 ? (cd1) c2 : new cd1<>(c2, true, true, mb1, this);
    }

    @DexIgnore
    public <R> d f(qa1 qa1, Object obj, mb1 mb1, int i2, int i3, Class<?> cls, Class<R> cls2, sa1 sa1, wc1 wc1, Map<Class<?>, sb1<?>> map, boolean z, boolean z2, ob1 ob1, boolean z3, boolean z4, boolean z5, boolean z6, gj1 gj1, Executor executor) {
        long b2 = i ? ek1.b() : 0;
        ad1 a2 = this.b.a(obj, mb1, i2, i3, map, cls, cls2, ob1);
        synchronized (this) {
            cd1<?> i4 = i(a2, z3, b2);
            if (i4 == null) {
                return l(qa1, obj, mb1, i2, i3, cls, cls2, sa1, wc1, map, z, z2, ob1, z3, z4, z5, z6, gj1, executor, a2, b2);
            }
            gj1.c(i4, gb1.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    public final cd1<?> g(mb1 mb1) {
        cd1<?> e2 = this.h.e(mb1);
        if (e2 != null) {
            e2.a();
        }
        return e2;
    }

    @DexIgnore
    public final cd1<?> h(mb1 mb1) {
        cd1<?> e2 = e(mb1);
        if (e2 != null) {
            e2.a();
            this.h.a(mb1, e2);
        }
        return e2;
    }

    @DexIgnore
    public final cd1<?> i(ad1 ad1, boolean z, long j) {
        if (!z) {
            return null;
        }
        cd1<?> g2 = g(ad1);
        if (g2 != null) {
            if (i) {
                j("Loaded resource from active resources", j, ad1);
            }
            return g2;
        }
        cd1<?> h2 = h(ad1);
        if (h2 == null) {
            return null;
        }
        if (i) {
            j("Loaded resource from cache", j, ad1);
        }
        return h2;
    }

    @DexIgnore
    public void k(id1<?> id1) {
        if (id1 instanceof cd1) {
            ((cd1) id1).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    public final <R> d l(qa1 qa1, Object obj, mb1 mb1, int i2, int i3, Class<?> cls, Class<R> cls2, sa1 sa1, wc1 wc1, Map<Class<?>, sb1<?>> map, boolean z, boolean z2, ob1 ob1, boolean z3, boolean z4, boolean z5, boolean z6, gj1 gj1, Executor executor, ad1 ad1, long j) {
        yc1<?> a2 = this.f4089a.a(ad1, z6);
        if (a2 != null) {
            a2.b(gj1, executor);
            if (i) {
                j("Added to existing load", j, ad1);
            }
            return new d(gj1, a2);
        }
        yc1<R> a3 = this.d.a(ad1, z3, z4, z5, z6);
        uc1<R> a4 = this.g.a(qa1, obj, ad1, mb1, i2, i3, cls, cls2, sa1, wc1, map, z, z2, z6, ob1, a3);
        this.f4089a.c(ad1, a3);
        a3.b(gj1, executor);
        a3.s(a4);
        if (i) {
            j("Started new load", j, ad1);
        }
        return new d(gj1, a3);
    }
}
