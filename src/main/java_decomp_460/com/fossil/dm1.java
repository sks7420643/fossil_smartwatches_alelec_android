package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dm1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ fm1 b;
    @DexIgnore
    public /* final */ at1 c;
    @DexIgnore
    public dt1 d;
    @DexIgnore
    public et1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<dm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public dm1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                fm1 valueOf = fm1.valueOf(readString);
                parcel.setDataPosition(0);
                switch (w8.f3894a[valueOf.ordinal()]) {
                    case 1:
                        return lm1.CREATOR.a(parcel);
                    case 2:
                        return im1.CREATOR.a(parcel);
                    case 3:
                        return jm1.CREATOR.a(parcel);
                    case 4:
                        return gm1.CREATOR.a(parcel);
                    case 5:
                        return cm1.CREATOR.a(parcel);
                    case 6:
                        return km1.CREATOR.a(parcel);
                    case 7:
                        return zl1.CREATOR.a(parcel);
                    case 8:
                        return bm1.CREATOR.a(parcel);
                    case 9:
                        return am1.CREATOR.a(parcel);
                    case 10:
                        return hm1.CREATOR.a(parcel);
                    default:
                        throw new al7();
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public dm1[] newArray(int i) {
            return new dm1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public dm1(android.os.Parcel r6) {
        /*
            r5 = this;
            r4 = 0
            java.lang.String r0 = r6.readString()
            if (r0 == 0) goto L_0x004a
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.fm1 r3 = com.fossil.fm1.valueOf(r0)
            java.lang.Class<com.fossil.at1> r0 = com.fossil.at1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r6.readParcelable(r0)
            if (r0 == 0) goto L_0x0046
            com.fossil.at1 r0 = (com.fossil.at1) r0
            java.lang.Class<com.fossil.dt1> r1 = com.fossil.dt1.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r6.readParcelable(r1)
            if (r1 == 0) goto L_0x0042
            com.fossil.dt1 r1 = (com.fossil.dt1) r1
            java.lang.Class<com.fossil.et1> r2 = com.fossil.et1.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            android.os.Parcelable r2 = r6.readParcelable(r2)
            if (r2 == 0) goto L_0x003e
            com.fossil.et1 r2 = (com.fossil.et1) r2
            r5.<init>(r3, r0, r1, r2)
            return
        L_0x003e:
            com.fossil.pq7.i()
            throw r4
        L_0x0042:
            com.fossil.pq7.i()
            throw r4
        L_0x0046:
            com.fossil.pq7.i()
            throw r4
        L_0x004a:
            com.fossil.pq7.i()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dm1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public dm1(fm1 fm1, at1 at1, dt1 dt1, et1 et1) {
        this.b = fm1;
        this.c = at1;
        this.d = dt1;
        this.e = et1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ dm1(fm1 fm1, at1 at1, dt1 dt1, et1 et1, int i) {
        this(fm1, (i & 2) != 0 ? new i90() : at1, (i & 4) != 0 ? new dt1(0, 62) : dt1, (i & 8) != 0 ? new et1(et1.CREATOR.a()) : et1);
    }

    @DexIgnore
    public final at1 a() {
        return this.c;
    }

    @DexIgnore
    public final void a(dt1 dt1) {
        this.d = dt1;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            dm1 dm1 = (dm1) obj;
            return this.b == dm1.b && !(pq7.a(this.c, dm1.c) ^ true) && !(pq7.a(this.d, dm1.d) ^ true) && !(pq7.a(this.e, dm1.e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.Complication");
    }

    @DexIgnore
    public final fm1 getId() {
        return this.b;
    }

    @DexIgnore
    public final dt1 getPositionConfig() {
        return this.d;
    }

    @DexIgnore
    public final et1 getThemeConfig() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public final JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.b.a()).put("pos", this.d.toJSONObject()).put("data", this.c.toJSONObject()).put("theme", this.e.toJSONObject());
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
