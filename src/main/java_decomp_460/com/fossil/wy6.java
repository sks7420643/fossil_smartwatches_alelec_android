package com.fossil;

import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nk5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wy6 extends iq4<ty6, uy6, sy6> {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public String d;
    @DexIgnore
    public ry6 e; // = ry6.LOGIN;
    @DexIgnore
    public /* final */ WatchAppRepository f;
    @DexIgnore
    public /* final */ ComplicationRepository g;
    @DexIgnore
    public /* final */ DianaAppSettingRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository j;
    @DexIgnore
    public /* final */ RingStyleRepository k;
    @DexIgnore
    public /* final */ v36 l;
    @DexIgnore
    public /* final */ d26 m;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase n;
    @DexIgnore
    public /* final */ on5 o;
    @DexIgnore
    public /* final */ WatchLocalizationRepository p;
    @DexIgnore
    public /* final */ AlarmsRepository q;
    @DexIgnore
    public /* final */ s77 r;
    @DexIgnore
    public /* final */ FileRepository s;
    @DexIgnore
    public /* final */ uo5 t;
    @DexIgnore
    public /* final */ zo5 u;
    @DexIgnore
    public /* final */ UserRepository v;
    @DexIgnore
    public /* final */ k97 w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase", f = "GetDianaDeviceSettingUseCase.kt", l = {125, 130, 160, 166}, m = "initialisePresets")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wy6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wy6 wy6, qn7 qn7) {
            super(qn7);
            this.this$0 = wy6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.G(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase", f = "GetDianaDeviceSettingUseCase.kt", l = {175, 177, 186}, m = "signInUpPresets")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wy6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(wy6 wy6, qn7 qn7) {
            super(qn7);
            this.this$0 = wy6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.J(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1", f = "GetDianaDeviceSettingUseCase.kt", l = {82, 83, 84, 85, 86, 87, 88, 89, 90, 94, 98, 99, 103, 104, 113}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wy6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(wy6 wy6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = wy6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x02a7  */
        /* JADX WARNING: Removed duplicated region for block: B:102:0x02ab  */
        /* JADX WARNING: Removed duplicated region for block: B:104:0x02af  */
        /* JADX WARNING: Removed duplicated region for block: B:108:0x02b9  */
        /* JADX WARNING: Removed duplicated region for block: B:110:0x02bf  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x007e  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00f7  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0127  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0142  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0187  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01a3  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01be  */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x01d6  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x01fc  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0214  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x0236  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0037  */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x025b  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x0276  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x028e  */
        /* JADX WARNING: Removed duplicated region for block: B:98:0x02a3  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 742
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wy6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = wy6.class.getSimpleName();
        pq7.b(simpleName, "GetDianaDeviceSettingUse\u2026se::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public wy6(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository, CategoryRepository categoryRepository, DianaWatchFaceRepository dianaWatchFaceRepository, RingStyleRepository ringStyleRepository, v36 v36, d26 d26, NotificationSettingsDatabase notificationSettingsDatabase, on5 on5, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository, s77 s77, FileRepository fileRepository, uo5 uo5, zo5 zo5, UserRepository userRepository, k97 k97) {
        pq7.c(watchAppRepository, "mWatchAppRepository");
        pq7.c(complicationRepository, "mComplicationRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        pq7.c(categoryRepository, "mCategoryRepository");
        pq7.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        pq7.c(ringStyleRepository, "mRingStyleRepository");
        pq7.c(v36, "mGetApp");
        pq7.c(d26, "mGetAllContactGroups");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        pq7.c(on5, "mSharePref");
        pq7.c(watchLocalizationRepository, "mWatchLocalizationRepository");
        pq7.c(alarmsRepository, "mAlarmsRepository");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(fileRepository, "fileRepository");
        pq7.c(uo5, "dianaPresetRepository");
        pq7.c(zo5, "dianaRecommendedPresetRepository");
        pq7.c(userRepository, "userRepository");
        pq7.c(k97, "photoRepository");
        this.f = watchAppRepository;
        this.g = complicationRepository;
        this.h = dianaAppSettingRepository;
        this.i = categoryRepository;
        this.j = dianaWatchFaceRepository;
        this.k = ringStyleRepository;
        this.l = v36;
        this.m = d26;
        this.n = notificationSettingsDatabase;
        this.o = on5;
        this.p = watchLocalizationRepository;
        this.q = alarmsRepository;
        this.r = s77;
        this.s = fileRepository;
        this.t = uo5;
        this.u = zo5;
        this.v = userRepository;
        this.w = k97;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v43, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x02e3  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02dd A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x025c  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0289  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object G(com.fossil.qn7<? super com.fossil.tl7> r19) {
        /*
        // Method dump skipped, instructions count: 742
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wy6.G(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* renamed from: H */
    public Object k(ty6 ty6, qn7<? super tl7> qn7) {
        if (ty6 == null) {
            i(new sy6(600, ""));
            return tl7.f3441a;
        }
        this.d = ty6.a();
        this.e = ty6.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "download device setting of " + this.d);
        if (!o37.b(PortfolioApp.h0.c())) {
            i(new sy6(601, ""));
            return tl7.f3441a;
        }
        if (!TextUtils.isEmpty(this.d)) {
            nk5.a aVar = nk5.o;
            String str2 = this.d;
            if (str2 == null) {
                pq7.i();
                throw null;
            } else if (aVar.v(str2)) {
                K();
                return tl7.f3441a;
            }
        }
        i(new sy6(600, ""));
        return tl7.f3441a;
    }

    @DexIgnore
    public final void I(mo5 mo5) {
        String str;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        StringBuilder sb = new StringBuilder();
        sb.append("setUpPresetDataForWatch - id: ");
        sb.append(mo5 != null ? mo5.e() : null);
        sb.append(" - url: ");
        sb.append(mo5 != null ? mo5.d() : null);
        local.e(str2, sb.toString());
        if (mo5 != null) {
            List<oo5> a2 = mo5.a();
            if (a2 != null) {
                WatchAppMappingSettings l2 = kj5.l(a2, new Gson(), new ArrayList());
                PortfolioApp c2 = PortfolioApp.h0.c();
                String str3 = this.d;
                if (str3 != null) {
                    c2.v1(l2, str3);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            PortfolioApp c3 = PortfolioApp.h0.c();
            File fileByFileName = this.s.getFileByFileName(mo5.e());
            if (fileByFileName == null || (str = fileByFileName.getAbsolutePath()) == null) {
                str = "";
            }
            String str4 = this.d;
            if (str4 != null) {
                PortfolioApp.W1(c3, str, null, str4, 2, null);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object J(com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 285
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wy6.J(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final xw7 K() {
        return gu7.d(g(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return x;
    }
}
