package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ml7 implements Collection<ll7>, jr7 {
    @DexIgnore
    public /* final */ int[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends hn7 {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ int[] c;

        @DexIgnore
        public a(int[] iArr) {
            pq7.c(iArr, "array");
            this.c = iArr;
        }

        @DexIgnore
        @Override // com.fossil.hn7
        public int b() {
            int i = this.b;
            int[] iArr = this.c;
            if (i < iArr.length) {
                this.b = i + 1;
                int i2 = iArr[i];
                ll7.e(i2);
                return i2;
            }
            throw new NoSuchElementException(String.valueOf(this.b));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.length;
        }
    }

    @DexIgnore
    public static boolean b(int[] iArr, int i) {
        return em7.z(iArr, i);
    }

    @DexIgnore
    public static boolean c(int[] iArr, Collection<ll7> collection) {
        boolean z;
        pq7.c(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof ll7) || !em7.z(iArr, t.j())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean e(int[] iArr, Object obj) {
        return (obj instanceof ml7) && pq7.a(iArr, ((ml7) obj).m());
    }

    @DexIgnore
    public static int g(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public static int h(int[] iArr) {
        if (iArr != null) {
            return Arrays.hashCode(iArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean i(int[] iArr) {
        return iArr.length == 0;
    }

    @DexIgnore
    public static hn7 k(int[] iArr) {
        return new a(iArr);
    }

    @DexIgnore
    public static String l(int[] iArr) {
        return "UIntArray(storage=" + Arrays.toString(iArr) + ")";
    }

    @DexIgnore
    public boolean a(int i) {
        return b(this.b, i);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(ll7 ll7) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends ll7> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof ll7) {
            return a(((ll7) obj).j());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return c(this.b, collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return e(this.b, obj);
    }

    @DexIgnore
    public int f() {
        return g(this.b);
    }

    @DexIgnore
    public int hashCode() {
        return h(this.b);
    }

    @DexIgnore
    public boolean isEmpty() {
        return i(this.b);
    }

    @DexIgnore
    /* renamed from: j */
    public hn7 iterator() {
        return k(this.b);
    }

    @DexIgnore
    public final /* synthetic */ int[] m() {
        return this.b;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return f();
    }

    @DexIgnore
    public Object[] toArray() {
        return jq7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) jq7.b(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return l(this.b);
    }
}
