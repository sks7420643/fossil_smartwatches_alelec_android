package com.fossil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lm7 extends km7 {
    @DexIgnore
    public static final <T extends Comparable<? super T>> void q(List<T> list) {
        pq7.c(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }

    @DexIgnore
    public static final <T> void r(List<T> list, Comparator<? super T> comparator) {
        pq7.c(list, "$this$sortWith");
        pq7.c(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }
}
