package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il1 extends gl1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<il1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final il1 a(byte[] bArr) {
            return new il1(new String(dm7.k(bArr, 0, bArr.length - 1), et7.f986a));
        }

        @DexIgnore
        /* renamed from: b */
        public il1 createFromParcel(Parcel parcel) {
            parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                return new il1(readString);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public il1[] newArray(int i) {
            return new il1[i];
        }
    }

    @DexIgnore
    public il1(String str) {
        super(r8.MESSAGE);
        this.c = str;
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public byte[] b() {
        String str = this.c;
        Charset c2 = hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return dm7.p(bytes, (byte) 0);
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(il1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.c, ((il1) obj).c) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.MessageEntry");
    }

    @DexIgnore
    public final String getMessage() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.gl1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.k, this.c);
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c);
        }
    }
}
