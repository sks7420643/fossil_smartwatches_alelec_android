package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k63 implements xw2<j63> {
    @DexIgnore
    public static k63 c; // = new k63();
    @DexIgnore
    public /* final */ xw2<j63> b;

    @DexIgnore
    public k63() {
        this(ww2.b(new m63()));
    }

    @DexIgnore
    public k63(xw2<j63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((j63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((j63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ j63 zza() {
        return this.b.zza();
    }
}
