package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr2 implements Parcelable.Creator<kr2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ kr2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 1;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder3 = null;
        ir2 ir2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    i = ad2.v(parcel, t);
                    break;
                case 2:
                    ir2 = (ir2) ad2.e(parcel, t, ir2.CREATOR);
                    break;
                case 3:
                    iBinder3 = ad2.u(parcel, t);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) ad2.e(parcel, t, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = ad2.u(parcel, t);
                    break;
                case 6:
                    iBinder = ad2.u(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new kr2(i, ir2, iBinder3, pendingIntent, iBinder2, iBinder);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ kr2[] newArray(int i) {
        return new kr2[i];
    }
}
