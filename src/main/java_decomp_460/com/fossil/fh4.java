package com.fossil;

import com.fossil.ch4;
import com.fossil.eh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fh4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract fh4 a();

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(long j);

        @DexIgnore
        public abstract a d(String str);

        @DexIgnore
        public abstract a e(String str);

        @DexIgnore
        public abstract a f(String str);

        @DexIgnore
        public abstract a g(eh4.a aVar);

        @DexIgnore
        public abstract a h(long j);
    }

    /*
    static {
        a().a();
    }
    */

    @DexIgnore
    public static a a() {
        ch4.b bVar = new ch4.b();
        bVar.h(0);
        bVar.g(eh4.a.ATTEMPT_MIGRATION);
        bVar.c(0);
        return bVar;
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract eh4.a g();

    @DexIgnore
    public abstract long h();

    @DexIgnore
    public boolean i() {
        return g() == eh4.a.REGISTER_ERROR;
    }

    @DexIgnore
    public boolean j() {
        return g() == eh4.a.NOT_GENERATED || g() == eh4.a.ATTEMPT_MIGRATION;
    }

    @DexIgnore
    public boolean k() {
        return g() == eh4.a.REGISTERED;
    }

    @DexIgnore
    public boolean l() {
        return g() == eh4.a.UNREGISTERED;
    }

    @DexIgnore
    public boolean m() {
        return g() == eh4.a.ATTEMPT_MIGRATION;
    }

    @DexIgnore
    public abstract a n();

    @DexIgnore
    public fh4 o(String str, long j, long j2) {
        a n = n();
        n.b(str);
        n.c(j);
        n.h(j2);
        return n.a();
    }

    @DexIgnore
    public fh4 p() {
        a n = n();
        n.b(null);
        return n.a();
    }

    @DexIgnore
    public fh4 q(String str) {
        a n = n();
        n.e(str);
        n.g(eh4.a.REGISTER_ERROR);
        return n.a();
    }

    @DexIgnore
    public fh4 r() {
        a n = n();
        n.g(eh4.a.NOT_GENERATED);
        return n.a();
    }

    @DexIgnore
    public fh4 s(String str, String str2, long j, String str3, long j2) {
        a n = n();
        n.d(str);
        n.g(eh4.a.REGISTERED);
        n.b(str3);
        n.f(str2);
        n.c(j2);
        n.h(j);
        return n.a();
    }

    @DexIgnore
    public fh4 t(String str) {
        a n = n();
        n.d(str);
        n.g(eh4.a.UNREGISTERED);
        return n.a();
    }
}
