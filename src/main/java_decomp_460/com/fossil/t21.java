package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t21<T> {
    @DexIgnore
    public static /* final */ String f; // = x01.f("ConstraintTracker");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ k41 f3350a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public /* final */ Set<e21<T>> d; // = new LinkedHashSet();
    @DexIgnore
    public T e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List b;

        @DexIgnore
        public a(List list) {
            this.b = list;
        }

        @DexIgnore
        public void run() {
            for (e21 e21 : this.b) {
                e21.a(t21.this.e);
            }
        }
    }

    @DexIgnore
    public t21(Context context, k41 k41) {
        this.b = context.getApplicationContext();
        this.f3350a = k41;
    }

    @DexIgnore
    public void a(e21<T> e21) {
        synchronized (this.c) {
            if (this.d.add(e21)) {
                if (this.d.size() == 1) {
                    this.e = b();
                    x01.c().a(f, String.format("%s: initial state = %s", getClass().getSimpleName(), this.e), new Throwable[0]);
                    e();
                }
                e21.a(this.e);
            }
        }
    }

    @DexIgnore
    public abstract T b();

    @DexIgnore
    public void c(e21<T> e21) {
        synchronized (this.c) {
            if (this.d.remove(e21) && this.d.isEmpty()) {
                f();
            }
        }
    }

    @DexIgnore
    public void d(T t) {
        synchronized (this.c) {
            if (this.e != t && (this.e == null || !this.e.equals(t))) {
                this.e = t;
                this.f3350a.a().execute(new a(new ArrayList(this.d)));
            }
        }
    }

    @DexIgnore
    public abstract void e();

    @DexIgnore
    public abstract void f();
}
