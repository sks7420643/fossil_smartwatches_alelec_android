package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class lw extends Enum<lw> {
    @DexIgnore
    public static /* final */ kw A; // = new kw(null);
    @DexIgnore
    public static /* final */ lw b;
    @DexIgnore
    public static /* final */ lw c;
    @DexIgnore
    public static /* final */ lw d;
    @DexIgnore
    public static /* final */ lw e;
    @DexIgnore
    public static /* final */ lw f;
    @DexIgnore
    public static /* final */ lw g;
    @DexIgnore
    public static /* final */ lw h;
    @DexIgnore
    public static /* final */ lw i;
    @DexIgnore
    public static /* final */ lw j;
    @DexIgnore
    public static /* final */ lw k;
    @DexIgnore
    public static /* final */ lw l;
    @DexIgnore
    public static /* final */ lw m;
    @DexIgnore
    public static /* final */ lw n;
    @DexIgnore
    public static /* final */ lw o;
    @DexIgnore
    public static /* final */ lw p;
    @DexIgnore
    public static /* final */ lw q;
    @DexIgnore
    public static /* final */ lw r;
    @DexIgnore
    public static /* final */ lw s;
    @DexIgnore
    public static /* final */ lw t;
    @DexIgnore
    public static /* final */ lw u;
    @DexIgnore
    public static /* final */ lw v;
    @DexIgnore
    public static /* final */ lw w;
    @DexIgnore
    public static /* final */ lw x;
    @DexIgnore
    public static /* final */ lw y;
    @DexIgnore
    public static /* final */ /* synthetic */ lw[] z;

    /*
    static {
        lw lwVar = new lw("SUCCESS", 0, 0);
        b = lwVar;
        lw lwVar2 = new lw("NOT_START", 1, 1);
        c = lwVar2;
        lw lwVar3 = new lw("COMMAND_ERROR", 2, 2);
        d = lwVar3;
        lw lwVar4 = new lw("RESPONSE_ERROR", 3, 3);
        e = lwVar4;
        lw lwVar5 = new lw("TIMEOUT", 4, 4);
        lw lwVar6 = new lw("EOF_TIME_OUT", 5, 5);
        f = lwVar6;
        lw lwVar7 = new lw("CONNECTION_DROPPED", 6, 6);
        g = lwVar7;
        lw lwVar8 = new lw("MISS_PACKAGE", 7, 7);
        h = lwVar8;
        lw lwVar9 = new lw("INVALID_DATA_LENGTH", 8, 8);
        i = lwVar9;
        lw lwVar10 = new lw("RECEIVED_DATA_CRC_MISS_MATCH", 9, 9);
        j = lwVar10;
        lw lwVar11 = new lw("INVALID_RESPONSE_LENGTH", 10, 10);
        k = lwVar11;
        lw lwVar12 = new lw("INVALID_RESPONSE_DATA", 11, 11);
        l = lwVar12;
        lw lwVar13 = new lw("REQUEST_UNSUPPORTED", 12, 12);
        m = lwVar13;
        lw lwVar14 = new lw("UNSUPPORTED_FILE_HANDLE", 13, 13);
        n = lwVar14;
        lw lwVar15 = new lw("BLUETOOTH_OFF", 14, 14);
        o = lwVar15;
        lw lwVar16 = new lw("WRONG_AUTHENTICATION_KEY_TYPE", 15, 15);
        p = lwVar16;
        lw lwVar17 = new lw("REQUEST_TIMEOUT", 16, 16);
        q = lwVar17;
        lw lwVar18 = new lw("RESPONSE_TIMEOUT", 17, 17);
        r = lwVar18;
        lw lwVar19 = new lw("EMPTY_SERVICES", 18, 18);
        s = lwVar19;
        lw lwVar20 = new lw("INTERRUPTED", 19, 254);
        t = lwVar20;
        lw lwVar21 = new lw("UNKNOWN_ERROR", 20, 255);
        u = lwVar21;
        lw lwVar22 = new lw("HID_PROXY_NOT_CONNECTED", 21, 256);
        v = lwVar22;
        lw lwVar23 = new lw("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 22, 257);
        w = lwVar23;
        lw lwVar24 = new lw("HID_INPUT_DEVICE_DISABLED", 23, 258);
        x = lwVar24;
        lw lwVar25 = new lw("HID_UNKNOWN_ERROR", 24, 511);
        y = lwVar25;
        z = new lw[]{lwVar, lwVar2, lwVar3, lwVar4, lwVar5, lwVar6, lwVar7, lwVar8, lwVar9, lwVar10, lwVar11, lwVar12, lwVar13, lwVar14, lwVar15, lwVar16, lwVar17, lwVar18, lwVar19, lwVar20, lwVar21, lwVar22, lwVar23, lwVar24, lwVar25};
    }
    */

    @DexIgnore
    public lw(String str, int i2, int i3) {
    }

    @DexIgnore
    public static lw valueOf(String str) {
        return (lw) Enum.valueOf(lw.class, str);
    }

    @DexIgnore
    public static lw[] values() {
        return (lw[]) z.clone();
    }
}
