package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public zn4 f1338a;
    @DexIgnore
    public yn4 b;
    @DexIgnore
    public ao4 c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public co4 e;

    @DexIgnore
    public static boolean b(int i) {
        return i >= 0 && i < 8;
    }

    @DexIgnore
    public co4 a() {
        return this.e;
    }

    @DexIgnore
    public void c(yn4 yn4) {
        this.b = yn4;
    }

    @DexIgnore
    public void d(int i) {
        this.d = i;
    }

    @DexIgnore
    public void e(co4 co4) {
        this.e = co4;
    }

    @DexIgnore
    public void f(zn4 zn4) {
        this.f1338a = zn4;
    }

    @DexIgnore
    public void g(ao4 ao4) {
        this.c = ao4;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.f1338a);
        sb.append("\n ecLevel: ");
        sb.append(this.b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }
}
