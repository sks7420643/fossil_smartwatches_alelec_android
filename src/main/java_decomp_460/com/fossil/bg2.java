package com.fossil;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg2 implements Parcelable.Creator<z52> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ z52 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        String str = null;
        PendingIntent pendingIntent = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i2 = ad2.v(parcel, t);
            } else if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l == 3) {
                pendingIntent = (PendingIntent) ad2.e(parcel, t, PendingIntent.CREATOR);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                str = ad2.f(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new z52(i2, i, pendingIntent, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ z52[] newArray(int i) {
        return new z52[i];
    }
}
