package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rf5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f3107a;
    @DexIgnore
    public /* final */ ImageView b;
    @DexIgnore
    public /* final */ FlexibleTextView c;
    @DexIgnore
    public /* final */ View d;
    @DexIgnore
    public /* final */ View e;

    @DexIgnore
    public rf5(ConstraintLayout constraintLayout, ImageView imageView, FlexibleTextView flexibleTextView, View view, View view2) {
        this.f3107a = constraintLayout;
        this.b = imageView;
        this.c = flexibleTextView;
        this.d = view;
        this.e = view2;
    }

    @DexIgnore
    public static rf5 a(View view) {
        int i = 2131363454;
        ImageView imageView = (ImageView) view.findViewById(2131362671);
        if (imageView != null) {
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363370);
            if (flexibleTextView != null) {
                View findViewById = view.findViewById(2131363449);
                if (findViewById != null) {
                    View findViewById2 = view.findViewById(2131363454);
                    if (findViewById2 != null) {
                        return new rf5((ConstraintLayout) view, imageView, flexibleTextView, findViewById, findViewById2);
                    }
                } else {
                    i = 2131363449;
                }
            } else {
                i = 2131363370;
            }
        } else {
            i = 2131362671;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static rf5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558711, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f3107a;
    }
}
