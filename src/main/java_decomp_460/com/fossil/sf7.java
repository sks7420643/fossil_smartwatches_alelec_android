package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sf7 {
    @DexIgnore
    boolean a(Intent intent, tf7 tf7);

    @DexIgnore
    boolean b();

    @DexIgnore
    boolean c(String str);

    @DexIgnore
    boolean d(df7 df7);
}
