package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mp1 extends ox1 implements Parcelable {
    @DexIgnore
    public /* final */ np1 b;
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public mp1(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0017
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.np1 r0 = com.fossil.np1.valueOf(r0)
            byte r1 = r3.readByte()
            r2.<init>(r0, r1)
            return
        L_0x0017:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mp1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public mp1(np1 np1, byte b2) {
        this.b = np1;
        this.c = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            mp1 mp1 = (mp1) obj;
            if (this.b != mp1.b) {
                return false;
            }
            return this.c == mp1.c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.DeviceEvent");
    }

    @DexIgnore
    public final np1 getDeviceEventId() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.O2, ey1.a(this.b)), jd0.z1, Short.valueOf(hy1.p(this.c)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
    }
}
