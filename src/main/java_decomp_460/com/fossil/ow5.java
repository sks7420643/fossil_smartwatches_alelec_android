package com.fossil;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow5 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<cl7<MicroApp, String>> f2733a;
    @DexIgnore
    public List<cl7<MicroApp, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public d d;
    @DexIgnore
    public c e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ow5 ow5, View view) {
            super(view);
            pq7.c(view, "itemView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f2734a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ow5 ow5, View view) {
            super(ow5, view);
            pq7.c(view, "itemView");
            View findViewById = view.findViewById(2131363383);
            if (findViewById != null) {
                this.f2734a = (FlexibleTextView) findViewById;
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.f2734a;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(MicroApp microApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ CustomizeWidget f2735a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public MicroApp d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ /* synthetic */ ow5 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e b;

            @DexIgnore
            public a(e eVar) {
                this.b = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                MicroApp a2 = this.b.a();
                if (a2 != null) {
                    d dVar = this.b.f.d;
                    if (dVar != null) {
                        dVar.a(a2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(ow5.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(ow5.f, "itemClick(), MicroApp tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ow5 ow5, View view) {
            super(ow5, view);
            pq7.c(view, "itemView");
            this.f = ow5;
            View findViewById = view.findViewById(2131363547);
            pq7.b(findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.f2735a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363370);
            pq7.b(findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            this.c = (FlexibleTextView) view.findViewById(2131363263);
            View findViewById3 = view.findViewById(2131363010);
            pq7.b(findViewById3, "itemView.findViewById(R.id.root_background)");
            this.e = (ConstraintLayout) findViewById3;
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                this.e.setBackgroundColor(Color.parseColor(d2));
            }
            view.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final MicroApp a() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.f2735a;
        }

        @DexIgnore
        public final void e(MicroApp microApp) {
            this.d = microApp;
        }
    }

    /*
    static {
        String name = ow5.class.getName();
        pq7.b(name, "SearchMicroAppAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<cl7<MicroApp, String>> list = this.f2733a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return (this.f2733a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final SpannableString i(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                jt7 jt7 = new jt7(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    ts7<ht7> findAll$default = jt7.findAll$default(jt7, lowerCase, 0, 2, null);
                    SpannableString spannableString = new SpannableString(str);
                    for (ht7 ht7 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), ht7.a().h().intValue(), ht7.a().h().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    /* renamed from: j */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (!(aVar instanceof b)) {
            e eVar = (e) aVar;
            List<cl7<MicroApp, String>> list = this.f2733a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty(this.b.get(i2).getSecond())) {
                        FlexibleTextView b2 = eVar.b();
                        pq7.b(b2, "resultSearchViewHolder.tvAssignedTo");
                        b2.setVisibility(0);
                        FlexibleTextView b3 = eVar.b();
                        pq7.b(b3, "resultSearchViewHolder.tvAssignedTo");
                        b3.setText(um5.c(PortfolioApp.h0.c(), 2131886516));
                    } else {
                        FlexibleTextView b4 = eVar.b();
                        pq7.b(b4, "resultSearchViewHolder.tvAssignedTo");
                        b4.setVisibility(8);
                    }
                    eVar.d().S(this.b.get(i2).getFirst().getId());
                    eVar.c().setText(um5.d(PortfolioApp.h0.c(), this.b.get(i2).getFirst().getNameKey(), this.b.get(i2).getFirst().getName()));
                    eVar.e(this.b.get(i2).getFirst());
                    return;
                }
                eVar.e(null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty(list.get(i).getSecond())) {
                    FlexibleTextView b5 = eVar.b();
                    pq7.b(b5, "resultSearchViewHolder.tvAssignedTo");
                    b5.setVisibility(0);
                    FlexibleTextView b6 = eVar.b();
                    pq7.b(b6, "resultSearchViewHolder.tvAssignedTo");
                    b6.setText(um5.c(PortfolioApp.h0.c(), 2131886516));
                } else {
                    FlexibleTextView b7 = eVar.b();
                    pq7.b(b7, "resultSearchViewHolder.tvAssignedTo");
                    b7.setVisibility(8);
                }
                eVar.d().S(list.get(i).getFirst().getId());
                String d2 = um5.d(PortfolioApp.h0.c(), list.get(i).getFirst().getNameKey(), list.get(i).getFirst().getName());
                FlexibleTextView c2 = eVar.c();
                pq7.b(d2, "name");
                c2.setText(i(d2, this.c));
                eVar.e(list.get(i).getFirst());
            } else {
                eVar.e(null);
            }
        } else if (this.b.isEmpty()) {
            ((b) aVar).a().setVisibility(4);
        } else {
            ((b) aVar).a().setVisibility(0);
        }
    }

    @DexIgnore
    /* renamed from: k */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558671, viewGroup, false);
            pq7.b(inflate, "view");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558670, viewGroup, false);
        pq7.b(inflate2, "view");
        return new e(this, inflate2);
    }

    @DexIgnore
    public final void l(List<cl7<MicroApp, String>> list) {
        pq7.c(list, "value");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(List<cl7<MicroApp, String>> list) {
        c cVar;
        this.f2733a = list;
        if (!(list == null || !list.isEmpty() || (cVar = this.e) == null)) {
            cVar.a(this.c);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(String str) {
        pq7.c(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final void o(c cVar) {
        pq7.c(cVar, "listener");
        this.e = cVar;
    }

    @DexIgnore
    public final void p(d dVar) {
        pq7.c(dVar, "listener");
        this.d = dVar;
    }
}
