package com.fossil;

import android.os.Bundle;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa2 implements r62.b, r62.c {
    @DexIgnore
    public /* final */ m62<?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public va2 d;

    @DexIgnore
    public wa2(m62<?> m62, boolean z) {
        this.b = m62;
        this.c = z;
    }

    @DexIgnore
    public final void a(va2 va2) {
        this.d = va2;
    }

    @DexIgnore
    public final void b() {
        rc2.l(this.d, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void d(int i) {
        b();
        this.d.d(i);
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void e(Bundle bundle) {
        b();
        this.d.e(bundle);
    }

    @DexIgnore
    @Override // com.fossil.r72
    public final void n(z52 z52) {
        b();
        this.d.i(z52, this.b, this.c);
    }
}
