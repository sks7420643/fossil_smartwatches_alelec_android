package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq5 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1345a;

    /*
    static {
        String simpleName = gq5.class.getSimpleName();
        pq7.b(simpleName, "TimeZoneReceiver::class.java.simpleName");
        f1345a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        pq7.c(context, "context");
        pq7.c(intent, "intent");
        String action = intent.getAction();
        if (pq7.a("android.intent.action.TIME_SET", action) || pq7.a("android.intent.action.TIMEZONE_CHANGED", action)) {
            FLogger.INSTANCE.getLocal().d(f1345a, "Inside .timeZoneChangeReceiver");
            lk5.u0();
        } else if (pq7.a("android.intent.action.TIME_TICK", action)) {
            TimeZone timeZone = TimeZone.getDefault();
            try {
                Calendar instance = Calendar.getInstance();
                pq7.b(instance, "calendar");
                Calendar instance2 = Calendar.getInstance();
                pq7.b(instance2, "Calendar.getInstance()");
                instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                Calendar instance3 = Calendar.getInstance();
                pq7.b(instance3, "Calendar.getInstance()");
                if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                    FLogger.INSTANCE.getLocal().d(f1345a, "Inside .timeZoneChangeReceiver - DST change");
                    lk5.u0();
                    PortfolioApp.h0.c().b1();
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f1345a;
                local.e(str, ".timeZoneChangeReceiver - ex=" + e);
            }
        }
    }
}
