package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class za0 extends va0 {
    @DexIgnore
    public static /* final */ ya0 CREATOR; // = new ya0(null);

    @DexIgnore
    public za0() {
        super(aa0.START_CRITICAL);
    }

    @DexIgnore
    public /* synthetic */ za0(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    @Override // com.fossil.va0
    public byte[] a() {
        return new byte[0];
    }
}
