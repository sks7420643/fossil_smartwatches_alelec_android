package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u44<T> extends i44<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ i44<? super T> forwardOrder;

    @DexIgnore
    public u44(i44<? super T> i44) {
        i14.l(i44);
        this.forwardOrder = i44;
    }

    @DexIgnore
    @Override // com.fossil.i44, java.util.Comparator
    public int compare(T t, T t2) {
        return this.forwardOrder.compare(t2, t);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof u44) {
            return this.forwardOrder.equals(((u44) obj).forwardOrder);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return -this.forwardOrder.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E max(Iterable<E> iterable) {
        return (E) this.forwardOrder.min(iterable);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E max(E e, E e2) {
        return (E) this.forwardOrder.min(e, e2);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E max(E e, E e2, E e3, E... eArr) {
        return (E) this.forwardOrder.min(e, e2, e3, eArr);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E max(Iterator<E> it) {
        return (E) this.forwardOrder.min(it);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E min(Iterable<E> iterable) {
        return (E) this.forwardOrder.max(iterable);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E min(E e, E e2) {
        return (E) this.forwardOrder.max(e, e2);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E min(E e, E e2, E e3, E... eArr) {
        return (E) this.forwardOrder.max(e, e2, e3, eArr);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <E extends T> E min(Iterator<E> it) {
        return (E) this.forwardOrder.max(it);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.i44<? super T>, com.fossil.i44<S extends T> */
    @Override // com.fossil.i44
    public <S extends T> i44<S> reverse() {
        return (i44<? super T>) this.forwardOrder;
    }

    @DexIgnore
    public String toString() {
        return this.forwardOrder + ".reverse()";
    }
}
