package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gi2 implements Parcelable.Creator<ai2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ai2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        int i = 0;
        byte[] bArr = null;
        float[] fArr = null;
        int[] iArr = null;
        Bundle bundle = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    i = ad2.v(parcel, t);
                    break;
                case 2:
                    z = ad2.m(parcel, t);
                    break;
                case 3:
                    f = ad2.r(parcel, t);
                    break;
                case 4:
                    str = ad2.f(parcel, t);
                    break;
                case 5:
                    bundle = ad2.a(parcel, t);
                    break;
                case 6:
                    iArr = ad2.d(parcel, t);
                    break;
                case 7:
                    fArr = ad2.c(parcel, t);
                    break;
                case 8:
                    bArr = ad2.b(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new ai2(i, z, f, str, bundle, iArr, fArr, bArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ai2[] newArray(int i) {
        return new ai2[i];
    }
}
