package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px7<T> extends ex7<fx7> {
    @DexIgnore
    public /* final */ lu7<T> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.lu7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public px7(fx7 fx7, lu7<? super T> lu7) {
        super(fx7);
        this.f = lu7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        w(th);
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return "ResumeAwaitOnCompletion[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.zu7
    public void w(Throwable th) {
        Object Q = ((fx7) this.e).Q();
        if (nv7.a() && !(!(Q instanceof sw7))) {
            throw new AssertionError();
        } else if (Q instanceof vu7) {
            lu7<T> lu7 = this.f;
            Throwable th2 = ((vu7) Q).f3837a;
            dl7.a aVar = dl7.Companion;
            lu7.resumeWith(dl7.m1constructorimpl(el7.a(th2)));
        } else {
            lu7<T> lu72 = this.f;
            Object h = gx7.h(Q);
            dl7.a aVar2 = dl7.Companion;
            lu72.resumeWith(dl7.m1constructorimpl(h));
        }
    }
}
