package com.fossil;

import android.content.Context;
import com.facebook.share.internal.VideoUploader;
import com.fossil.jn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.PermissionData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz6 extends cz6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ dz6 e;
    @DexIgnore
    public /* final */ List<PermissionData> f;
    @DexIgnore
    public /* final */ on5 g;

    /*
    static {
        String simpleName = jz6.class.getSimpleName();
        pq7.b(simpleName, "PermissionPresenter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public jz6(dz6 dz6, List<PermissionData> list, on5 on5) {
        pq7.c(dz6, "mView");
        pq7.c(list, "mListPerms");
        pq7.c(on5, "mSharedPreferencesManager");
        this.e = dz6;
        this.f = list;
        this.g = on5;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        boolean z;
        FLogger.INSTANCE.getLocal().d(h, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        dz6 dz6 = this.e;
        if (dz6 != null) {
            Context context = ((ez6) dz6).getContext();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (T t : this.f) {
                ArrayList<String> androidPermissionSet = t.getAndroidPermissionSet();
                ArrayList arrayList3 = new ArrayList();
                for (T t2 : androidPermissionSet) {
                    T t3 = t2;
                    if (context == null) {
                        pq7.i();
                        throw null;
                    } else if (!v78.a(context, t3)) {
                        arrayList3.add(t2);
                    }
                }
                if (t.getSettingPermissionId() != null) {
                    jn5 jn5 = jn5.b;
                    jn5.c settingPermissionId = t.getSettingPermissionId();
                    if (settingPermissionId != null) {
                        z = jn5.n(settingPermissionId);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    z = true;
                }
                FLogger.INSTANCE.getLocal().d(h, "check " + ((Object) t) + " androidPermissionsNotGranted " + arrayList3 + " settingPermissionGranted " + z);
                t.setGranted(arrayList3.isEmpty() && z);
                if (pq7.a(t.getType(), "PERMISSION_SETTING_TYPE")) {
                    arrayList.add(t);
                }
                if (!t.isGranted()) {
                    arrayList2.add(t);
                }
            }
            if (!arrayList2.isEmpty() || !arrayList.isEmpty()) {
                this.e.d5(this.f);
            } else {
                this.e.close();
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    @Override // com.fossil.cz6
    public boolean n(String str) {
        pq7.c(str, "permission");
        Boolean e0 = this.g.e0(str);
        pq7.b(e0, "mSharedPreferencesManage\u2026rstTimeAsking(permission)");
        return e0.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.cz6
    public void o(String str, boolean z) {
        pq7.c(str, "permission");
        this.g.f1(str, Boolean.valueOf(z));
    }

    @DexIgnore
    public void p() {
        this.e.M5(this);
    }
}
