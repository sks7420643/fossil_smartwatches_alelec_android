package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.LimitSlopeSwipeRefresh;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ky4 extends pv5 implements ny5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<l75> g;
    @DexIgnore
    public my4 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public oy4 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ky4.m;
        }

        @DexIgnore
        public final ky4 b() {
            return new ky4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l75 f2110a;
        @DexIgnore
        public /* final */ /* synthetic */ ky4 b;

        @DexIgnore
        public b(l75 l75, ky4 ky4) {
            this.f2110a = l75;
            this.b = ky4;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            super.c(i);
            hr7 hr7 = hr7.f1520a;
            String c = um5.c(PortfolioApp.h0.c(), 2131886239);
            pq7.b(c, "LanguageHelper.getString\u2026story_Text__OfChallenges)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i + 1), Integer.valueOf(this.b.P6().getItemCount())}, 2));
            pq7.b(format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView = this.f2110a.s;
            pq7.b(flexibleTextView, "ftvIndicator");
            flexibleTextView.setText(format);
            ky4.N6(this.b).q(i, this.b.P6().getItemCount());
            this.b.k = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ l75 f2111a;
        @DexIgnore
        public /* final */ /* synthetic */ ky4 b;

        @DexIgnore
        public c(l75 l75, ky4 ky4) {
            this.f2111a = l75;
            this.b = ky4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f2111a.r;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            ky4.N6(this.b).r(this.b.k, this.b.P6().getItemCount());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<List<? extends qt4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ky4 f2112a;

        @DexIgnore
        public d(ky4 ky4) {
            this.f2112a = ky4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<qt4> list) {
            oy4 P6 = this.f2112a.P6();
            pq7.b(list, "it");
            P6.i(list);
            hr7 hr7 = hr7.f1520a;
            String c = um5.c(PortfolioApp.h0.c(), 2131886239);
            pq7.b(c, "LanguageHelper.getString\u2026story_Text__OfChallenges)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(this.f2112a.k + 1), Integer.valueOf(list.size())}, 2));
            pq7.b(format, "java.lang.String.format(format, *args)");
            l75 l75 = (l75) ky4.K6(this.f2112a).a();
            if (l75 != null) {
                FlexibleTextView flexibleTextView = l75.s;
                pq7.b(flexibleTextView, "ftvIndicator");
                if (format != null) {
                    String upperCase = format.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    if (!list.isEmpty()) {
                        FlexibleTextView flexibleTextView2 = l75.r;
                        pq7.b(flexibleTextView2, "ftvError");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = l75.s;
                        pq7.b(flexibleTextView3, "ftvIndicator");
                        flexibleTextView3.setVisibility(0);
                        FlexibleTextView flexibleTextView4 = l75.t;
                        pq7.b(flexibleTextView4, "ftvNotice");
                        flexibleTextView4.setVisibility(0);
                        return;
                    }
                    return;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ky4 f2113a;

        @DexIgnore
        public e(ky4 ky4) {
            this.f2113a = ky4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            l75 l75 = (l75) ky4.K6(this.f2113a).a();
            if (l75 != null) {
                pq7.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = l75.q;
                    pq7.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    ViewPager2 viewPager2 = l75.v;
                    pq7.b(viewPager2, "subTabs");
                    viewPager2.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = l75.s;
                    pq7.b(flexibleTextView2, "ftvIndicator");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = l75.t;
                    pq7.b(flexibleTextView3, "ftvNotice");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = l75.q;
                pq7.b(flexibleTextView4, "ftvEmpty");
                flexibleTextView4.setVisibility(8);
                ViewPager2 viewPager22 = l75.v;
                pq7.b(viewPager22, "subTabs");
                viewPager22.setVisibility(0);
                FlexibleTextView flexibleTextView5 = l75.s;
                pq7.b(flexibleTextView5, "ftvIndicator");
                flexibleTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView6 = l75.t;
                pq7.b(flexibleTextView6, "ftvNotice");
                flexibleTextView6.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ky4 f2114a;

        @DexIgnore
        public f(ky4 ky4) {
            this.f2114a = ky4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            l75 l75 = (l75) ky4.K6(this.f2114a).a();
            if (l75 != null) {
                LimitSlopeSwipeRefresh limitSlopeSwipeRefresh = l75.w;
                pq7.b(limitSlopeSwipeRefresh, "swipe");
                pq7.b(bool, "it");
                limitSlopeSwipeRefresh.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ky4 f2115a;

        @DexIgnore
        public g(ky4 ky4) {
            this.f2115a = ky4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            if (cl7 != null) {
                boolean booleanValue = cl7.getFirst().booleanValue();
                ServerError serverError = (ServerError) cl7.getSecond();
                l75 l75 = (l75) ky4.K6(this.f2115a).a();
                if (l75 != null) {
                    FlexibleTextView flexibleTextView = l75.q;
                    pq7.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(8);
                    if (booleanValue) {
                        FlexibleTextView flexibleTextView2 = l75.t;
                        pq7.b(flexibleTextView2, "ftvNotice");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = l75.s;
                        pq7.b(flexibleTextView3, "ftvIndicator");
                        flexibleTextView3.setVisibility(8);
                        FlexibleTextView flexibleTextView4 = l75.r;
                        pq7.b(flexibleTextView4, "ftvError");
                        flexibleTextView4.setVisibility(0);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = l75.r;
                    pq7.b(flexibleTextView5, "ftvError");
                    flexibleTextView5.setVisibility(8);
                    if (this.f2115a.isResumed()) {
                        FlexibleTextView flexibleTextView6 = l75.r;
                        pq7.b(flexibleTextView6, "ftvError");
                        String c = um5.c(flexibleTextView6.getContext(), 2131886231);
                        FragmentActivity activity = this.f2115a.getActivity();
                        if (activity != null) {
                            Toast.makeText(activity, c, 1).show();
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        String simpleName = ky4.class.getSimpleName();
        pq7.b(simpleName, "BCHistoryFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(ky4 ky4) {
        g37<l75> g37 = ky4.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ my4 N6(ky4 ky4) {
        my4 my4 = ky4.h;
        if (my4 != null) {
            return my4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final oy4 P6() {
        oy4 oy4 = this.j;
        if (oy4 != null) {
            return oy4;
        }
        pq7.n("historyAdapter");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        g37<l75> g37 = this.g;
        if (g37 != null) {
            l75 a2 = g37.a();
            if (a2 != null) {
                this.j = new oy4(this);
                ViewPager2 viewPager2 = a2.v;
                pq7.b(viewPager2, "this");
                oy4 oy4 = this.j;
                if (oy4 != null) {
                    viewPager2.setAdapter(oy4);
                    viewPager2.setOffscreenPageLimit(5);
                    a2.v.g(new b(a2, this));
                    a2.w.setOnRefreshListener(new c(a2, this));
                    return;
                }
                pq7.n("historyAdapter");
                throw null;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        my4 my4 = this.h;
        if (my4 != null) {
            my4.m().h(getViewLifecycleOwner(), new d(this));
            my4 my42 = this.h;
            if (my42 != null) {
                my42.k().h(getViewLifecycleOwner(), new e(this));
                my4 my43 = this.h;
                if (my43 != null) {
                    my43.n().h(getViewLifecycleOwner(), new f(this));
                    my4 my44 = this.h;
                    if (my44 != null) {
                        my44.l().h(getViewLifecycleOwner(), new g(this));
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ny5
    public void e6(ps4 ps4) {
        pq7.c(ps4, "challenge");
        BCOverviewLeaderBoardActivity.A.a(this, ps4, ps4.f());
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().N1().a(this);
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(my4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026oryViewModel::class.java)");
            this.h = (my4) a2;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        l75 l75 = (l75) aq0.f(layoutInflater, 2131558569, viewGroup, false, A6());
        this.g = new g37<>(this, l75);
        pq7.b(l75, "binding");
        return l75.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Q6();
        R6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
