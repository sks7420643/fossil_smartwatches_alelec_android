package com.fossil;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pe7 extends qe7 {
    @DexIgnore
    public pe7(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.qe7
    public final void b(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to Settings.System");
            Settings.System.putString(this.f2970a.getContentResolver(), se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
        }
    }

    @DexIgnore
    @Override // com.fossil.qe7
    public final boolean c() {
        return se7.d(this.f2970a, "android.permission.WRITE_SETTINGS");
    }

    @DexIgnore
    @Override // com.fossil.qe7
    public final String d() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from Settings.System");
            string = Settings.System.getString(this.f2970a.getContentResolver(), se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="));
        }
        return string;
    }
}
