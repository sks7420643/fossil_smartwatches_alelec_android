package com.fossil;

import com.fossil.xd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xd4<T extends xd4<T>> {
    @DexIgnore
    <U> T a(Class<U> cls, sd4<? super U> sd4);
}
