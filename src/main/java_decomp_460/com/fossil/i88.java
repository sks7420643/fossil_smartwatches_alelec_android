package com.fossil;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i88 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Method f1594a;
    @DexIgnore
    public /* final */ List<?> b;

    @DexIgnore
    public i88(Method method, List<?> list) {
        this.f1594a = method;
        this.b = Collections.unmodifiableList(list);
    }

    @DexIgnore
    public Method a() {
        return this.f1594a;
    }

    @DexIgnore
    public String toString() {
        return String.format("%s.%s() %s", this.f1594a.getDeclaringClass().getName(), this.f1594a.getName(), this.b);
    }
}
