package com.fossil;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ RecyclerView.m f2253a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ Rect c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends lv0 {
        @DexIgnore
        public a(RecyclerView.m mVar) {
            super(mVar, null);
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int d(View view) {
            return ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).rightMargin + this.f2253a.U(view);
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int e(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + this.f2253a.T(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int f(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + this.f2253a.S(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int g(View view) {
            return this.f2253a.R(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int h() {
            return this.f2253a.p0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int i() {
            return this.f2253a.p0() - this.f2253a.g0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int j() {
            return this.f2253a.g0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int k() {
            return this.f2253a.q0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int l() {
            return this.f2253a.Y();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int m() {
            return this.f2253a.f0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int n() {
            return (this.f2253a.p0() - this.f2253a.f0()) - this.f2253a.g0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int p(View view) {
            this.f2253a.o0(view, true, this.c);
            return this.c.right;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int q(View view) {
            this.f2253a.o0(view, true, this.c);
            return this.c.left;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public void r(int i) {
            this.f2253a.D0(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends lv0 {
        @DexIgnore
        public b(RecyclerView.m mVar) {
            super(mVar, null);
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int d(View view) {
            return ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).bottomMargin + this.f2253a.P(view);
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int e(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + this.f2253a.S(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int f(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + this.f2253a.T(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int g(View view) {
            return this.f2253a.V(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int h() {
            return this.f2253a.X();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int i() {
            return this.f2253a.X() - this.f2253a.e0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int j() {
            return this.f2253a.e0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int k() {
            return this.f2253a.Y();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int l() {
            return this.f2253a.q0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int m() {
            return this.f2253a.h0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int n() {
            return (this.f2253a.X() - this.f2253a.h0()) - this.f2253a.e0();
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int p(View view) {
            this.f2253a.o0(view, true, this.c);
            return this.c.bottom;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public int q(View view) {
            this.f2253a.o0(view, true, this.c);
            return this.c.top;
        }

        @DexIgnore
        @Override // com.fossil.lv0
        public void r(int i) {
            this.f2253a.E0(i);
        }
    }

    @DexIgnore
    public lv0(RecyclerView.m mVar) {
        this.b = RecyclerView.UNDEFINED_DURATION;
        this.c = new Rect();
        this.f2253a = mVar;
    }

    @DexIgnore
    public /* synthetic */ lv0(RecyclerView.m mVar, a aVar) {
        this(mVar);
    }

    @DexIgnore
    public static lv0 a(RecyclerView.m mVar) {
        return new a(mVar);
    }

    @DexIgnore
    public static lv0 b(RecyclerView.m mVar, int i) {
        if (i == 0) {
            return a(mVar);
        }
        if (i == 1) {
            return c(mVar);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    @DexIgnore
    public static lv0 c(RecyclerView.m mVar) {
        return new b(mVar);
    }

    @DexIgnore
    public abstract int d(View view);

    @DexIgnore
    public abstract int e(View view);

    @DexIgnore
    public abstract int f(View view);

    @DexIgnore
    public abstract int g(View view);

    @DexIgnore
    public abstract int h();

    @DexIgnore
    public abstract int i();

    @DexIgnore
    public abstract int j();

    @DexIgnore
    public abstract int k();

    @DexIgnore
    public abstract int l();

    @DexIgnore
    public abstract int m();

    @DexIgnore
    public abstract int n();

    @DexIgnore
    public int o() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return n() - this.b;
    }

    @DexIgnore
    public abstract int p(View view);

    @DexIgnore
    public abstract int q(View view);

    @DexIgnore
    public abstract void r(int i);

    @DexIgnore
    public void s() {
        this.b = n();
    }
}
