package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jq1 extends mp1 {
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public jq1(Parcel parcel) {
        super(parcel);
        this.d = parcel.readInt();
    }

    @DexIgnore
    public jq1(np1 np1, byte b, int i) {
        super(np1, b);
        this.d = i;
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((jq1) obj).d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.DeviceRequest");
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.q, Integer.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }
}
