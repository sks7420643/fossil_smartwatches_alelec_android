package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mm7 extends lm7 {
    @DexIgnore
    public static final <T> boolean s(Collection<? super T> collection, Iterable<? extends T> iterable) {
        pq7.c(collection, "$this$addAll");
        pq7.c(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        Iterator<? extends T> it = iterable.iterator();
        while (it.hasNext()) {
            if (collection.add((Object) it.next())) {
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static final <T> boolean t(Collection<? super T> collection, T[] tArr) {
        pq7.c(collection, "$this$addAll");
        pq7.c(tArr, MessengerShareContentUtility.ELEMENTS);
        return collection.addAll(dm7.d(tArr));
    }

    @DexIgnore
    public static final <T> boolean u(Iterable<? extends T> iterable, rp7<? super T, Boolean> rp7, boolean z) {
        Iterator<? extends T> it = iterable.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (rp7.invoke((Object) it.next()).booleanValue() == z) {
                it.remove();
                z2 = true;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final <T> boolean v(List<T> list, rp7<? super T, Boolean> rp7, boolean z) {
        int i;
        if (list instanceof RandomAccess) {
            int g = hm7.g(list);
            if (g >= 0) {
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    T t = list.get(i3);
                    if (rp7.invoke(t).booleanValue() == z) {
                        i = i2;
                    } else {
                        if (i2 != i3) {
                            list.set(i2, t);
                        }
                        i = i2 + 1;
                    }
                    if (i3 == g) {
                        break;
                    }
                    i3++;
                    i2 = i;
                }
            } else {
                i = 0;
            }
            if (i >= list.size()) {
                return false;
            }
            int g2 = hm7.g(list);
            if (g2 >= i) {
                while (true) {
                    list.remove(g2);
                    if (g2 == i) {
                        break;
                    }
                    g2--;
                }
            }
            return true;
        } else if (list != null) {
            return u(ir7.a(list), rp7, z);
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }

    @DexIgnore
    public static final <T> boolean w(List<T> list, rp7<? super T, Boolean> rp7) {
        pq7.c(list, "$this$removeAll");
        pq7.c(rp7, "predicate");
        return v(list, rp7, true);
    }

    @DexIgnore
    public static final <T> boolean x(Iterable<? extends T> iterable, rp7<? super T, Boolean> rp7) {
        pq7.c(iterable, "$this$retainAll");
        pq7.c(rp7, "predicate");
        return u(iterable, rp7, false);
    }
}
