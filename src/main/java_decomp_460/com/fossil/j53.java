package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j53 implements k53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1713a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f1713a = hw2.d("measurement.service.configurable_service_limits", true);
        b = hw2.d("measurement.client.configurable_service_limits", true);
        hw2.b("measurement.id.service.configurable_service_limits", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.k53
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.k53
    public final boolean zzb() {
        return f1713a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.k53
    public final boolean zzc() {
        return b.o().booleanValue();
    }
}
