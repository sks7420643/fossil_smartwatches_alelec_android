package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fs4;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es4 extends u47 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public List<gs4> k; // = hm7.e();
    @DexIgnore
    public fs4 l;
    @DexIgnore
    public /* final */ zp0 m; // = new sr4(this);
    @DexIgnore
    public g37<k15> s;
    @DexIgnore
    public my5 t;
    @DexIgnore
    public gs4 u;
    @DexIgnore
    public String v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return es4.z;
        }

        @DexIgnore
        public final es4 b() {
            return new es4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es4 b;

        @DexIgnore
        public b(es4 es4) {
            this.b = es4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es4 b;

        @DexIgnore
        public c(es4 es4) {
            this.b = es4;
        }

        @DexIgnore
        public final void onClick(View view) {
            my5 my5;
            gs4 gs4 = this.b.u;
            if (!(gs4 == null || (my5 = this.b.t) == null)) {
                my5.a(gs4);
            }
            this.b.dismiss();
        }
    }

    /*
    static {
        String name = es4.class.getName();
        pq7.b(name, "BottomDialog::class.java.name");
        z = name;
    }
    */

    @DexIgnore
    public final void D6() {
        this.l = new fs4(this.k, this);
        g37<k15> g37 = this.s;
        if (g37 != null) {
            k15 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                pq7.b(flexibleTextView, "ftvTitle");
                flexibleTextView.setText(this.v);
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                pq7.b(recyclerView, "this");
                fs4 fs4 = this.l;
                if (fs4 != null) {
                    recyclerView.setAdapter(fs4);
                    recyclerView.setHasFixedSize(true);
                    a2.t.setOnClickListener(new b(this));
                    if (this.w) {
                        FlexibleButton flexibleButton = a2.q;
                        pq7.b(flexibleButton, "btnOk");
                        flexibleButton.setVisibility(0);
                        RTLImageView rTLImageView = a2.t;
                        pq7.b(rTLImageView, "ivClose");
                        rTLImageView.setVisibility(8);
                    } else {
                        FlexibleButton flexibleButton2 = a2.q;
                        pq7.b(flexibleButton2, "btnOk");
                        flexibleButton2.setVisibility(8);
                        RTLImageView rTLImageView2 = a2.t;
                        pq7.b(rTLImageView2, "ivClose");
                        rTLImageView2.setVisibility(0);
                    }
                    a2.q.setOnClickListener(new c(this));
                    return;
                }
                pq7.n("bottomDialogAdapter");
                throw null;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final es4 E6(List<gs4> list) {
        int i;
        pq7.c(list, "newData");
        Iterator<gs4> it = list.iterator();
        int i2 = 0;
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (it.next().c()) {
                break;
            } else {
                i2 = i + 1;
            }
        }
        if (i != -1) {
            this.u = list.get(i);
        }
        this.k = list;
        return this;
    }

    @DexIgnore
    public final void F6(boolean z2) {
        this.w = z2;
    }

    @DexIgnore
    public final void G6(my5 my5) {
        pq7.c(my5, "listener");
        this.t = my5;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        D6();
    }

    @DexIgnore
    public void onClick(View view) {
        Object tag = view != null ? view.getTag() : null;
        if (tag != null) {
            fs4.a aVar = (fs4.a) tag;
            gs4 gs4 = this.k.get(aVar.getAdapterPosition());
            this.u = gs4;
            if (!this.x) {
                Iterator<gs4> it = this.k.iterator();
                int i = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i = -1;
                        break;
                    } else if (it.next().c()) {
                        break;
                    } else {
                        i++;
                    }
                }
                int adapterPosition = aVar.getAdapterPosition();
                if (!(i == -1 || i == adapterPosition)) {
                    this.k.get(i).d(false);
                    this.k.get(adapterPosition).d(true);
                    fs4 fs4 = this.l;
                    if (fs4 != null) {
                        fs4.notifyDataSetChanged();
                    } else {
                        pq7.n("bottomDialogAdapter");
                        throw null;
                    }
                }
            }
            if (!this.w) {
                my5 my5 = this.t;
                if (my5 != null) {
                    my5.a(gs4);
                }
                dismiss();
                return;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.buddy_challenge.customview.BottomDialogAdapter.PrivacyHolder");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        k15 k15 = (k15) aq0.f(layoutInflater, 2131558443, viewGroup, false, this.m);
        this.s = new g37<>(this, k15);
        pq7.b(k15, "binding");
        return k15.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    public final void setTitle(String str) {
        pq7.c(str, "title");
        this.v = str;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
