package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o48 extends d58 {
    @DexIgnore
    public d58 e;

    @DexIgnore
    public o48(d58 d58) {
        pq7.c(d58, "delegate");
        this.e = d58;
    }

    @DexIgnore
    @Override // com.fossil.d58
    public d58 a() {
        return this.e.a();
    }

    @DexIgnore
    @Override // com.fossil.d58
    public d58 b() {
        return this.e.b();
    }

    @DexIgnore
    @Override // com.fossil.d58
    public long c() {
        return this.e.c();
    }

    @DexIgnore
    @Override // com.fossil.d58
    public d58 d(long j) {
        return this.e.d(j);
    }

    @DexIgnore
    @Override // com.fossil.d58
    public boolean e() {
        return this.e.e();
    }

    @DexIgnore
    @Override // com.fossil.d58
    public void f() throws IOException {
        this.e.f();
    }

    @DexIgnore
    @Override // com.fossil.d58
    public d58 g(long j, TimeUnit timeUnit) {
        pq7.c(timeUnit, Constants.PROFILE_KEY_UNIT);
        return this.e.g(j, timeUnit);
    }

    @DexIgnore
    @Override // com.fossil.d58
    public long h() {
        return this.e.h();
    }

    @DexIgnore
    public final d58 i() {
        return this.e;
    }

    @DexIgnore
    public final o48 j(d58 d58) {
        pq7.c(d58, "delegate");
        this.e = d58;
        return this;
    }
}
