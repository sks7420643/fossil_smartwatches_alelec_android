package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jb3 extends gr2 implements ib3 {
    @DexIgnore
    public jb3() {
        super("com.google.android.gms.location.ILocationCallback");
    }

    @DexIgnore
    public static ib3 e(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationCallback");
        return queryLocalInterface instanceof ib3 ? (ib3) queryLocalInterface : new kb3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            S1((LocationResult) qr2.a(parcel, LocationResult.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            j1((LocationAvailability) qr2.a(parcel, LocationAvailability.CREATOR));
        }
        return true;
    }
}
