package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y46 implements Factory<x46> {
    @DexIgnore
    public static x46 a(s46 s46, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        return new x46(s46, notificationsLoader, loaderManager);
    }
}
