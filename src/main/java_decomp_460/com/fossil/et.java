package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et extends bt {
    @DexIgnore
    public /* final */ rt K;
    @DexIgnore
    public /* final */ byte[] L;

    @DexIgnore
    public et(k5 k5Var, rt rtVar, byte[] bArr) {
        super(k5Var, ut.SEND_BOTH_SIDES_RANDOM_NUMBERS, hs.I, 0, 8);
        this.K = rtVar;
        this.L = bArr;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(this.L.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.K.b).put(this.L).array();
        pq7.b(array, "ByteBuffer.allocate(KEY_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.u2, ey1.a(this.K)), jd0.q2, dy1.e(this.L, null, 1, null));
    }
}
