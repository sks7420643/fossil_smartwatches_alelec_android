package com.fossil;

import com.fossil.a34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k34<K, V> extends l34<K, V> implements NavigableMap<K, V> {
    @DexIgnore
    public static /* final */ Comparator<Comparable> i; // = i44.natural();
    @DexIgnore
    public static /* final */ k34<Comparable, Object> j; // = new k34<>(m34.emptySet(i44.natural()), y24.of());
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient s44<K> f;
    @DexIgnore
    public /* final */ transient y24<V> g;
    @DexIgnore
    public transient k34<K, V> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends c34<K, V> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.k34$a$a")
        /* renamed from: com.fossil.k34$a$a  reason: collision with other inner class name */
        public class C0134a extends s24<Map.Entry<K, V>> {
            @DexIgnore
            public C0134a() {
            }

            @DexIgnore
            @Override // com.fossil.s24
            public u24<Map.Entry<K, V>> delegateCollection() {
                return a.this;
            }

            @DexIgnore
            @Override // java.util.List
            public Map.Entry<K, V> get(int i) {
                return x34.e(k34.this.f.asList().get(i), k34.this.g.get(i));
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.h34
        public y24<Map.Entry<K, V>> createAsList() {
            return new C0134a();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, com.fossil.h34, com.fossil.h34, java.lang.Iterable
        public h54<Map.Entry<K, V>> iterator() {
            return asList().iterator();
        }

        @DexIgnore
        @Override // com.fossil.c34
        public a34<K, V> map() {
            return k34.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends a34.b<K, V> {
        @DexIgnore
        public /* final */ Comparator<? super K> e;

        @DexIgnore
        public b(Comparator<? super K> comparator) {
            i14.l(comparator);
            this.e = comparator;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b c(Object obj, Object obj2) {
            h(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b d(Map.Entry entry) {
            i(entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b e(Iterable iterable) {
            j(iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.a34.b
        public /* bridge */ /* synthetic */ a34.b f(Map map) {
            k(map);
            return this;
        }

        @DexIgnore
        /* renamed from: g */
        public k34<K, V> a() {
            int i = this.c;
            return i != 0 ? i != 1 ? k34.e(this.e, false, this.b, i) : k34.g(this.e, this.b[0].getKey(), this.b[0].getValue()) : k34.emptyMap(this.e);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> h(K k, V v) {
            super.c(k, v);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> i(Map.Entry<? extends K, ? extends V> entry) {
            super.d(entry);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> j(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.e(iterable);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> k(Map<? extends K, ? extends V> map) {
            super.f(map);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends a34.e {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<Object> comparator;

        @DexIgnore
        public c(k34<?, ?> k34) {
            super(k34);
            this.comparator = k34.comparator();
        }

        @DexIgnore
        @Override // com.fossil.a34.e
        public Object readResolve() {
            return createMap(new b(this.comparator));
        }
    }

    @DexIgnore
    public k34(s44<K> s44, y24<V> y24) {
        this(s44, y24, null);
    }

    @DexIgnore
    public k34(s44<K> s44, y24<V> y24, k34<K, V> k34) {
        this.f = s44;
        this.g = y24;
        this.h = k34;
    }

    @DexIgnore
    public static <K, V> k34<K, V> b(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        boolean z = false;
        if (map instanceof SortedMap) {
            Comparator<? super K> comparator2 = ((SortedMap) map).comparator();
            if (comparator2 != null) {
                z = comparator.equals(comparator2);
            } else if (comparator == i) {
                z = true;
            }
        }
        if (z && (map instanceof k34)) {
            k34<K, V> k34 = (k34) map;
            if (!k34.isPartialView()) {
                return k34;
            }
        }
        return c(comparator, z, map.entrySet());
    }

    @DexIgnore
    public static <K, V> k34<K, V> c(Comparator<? super K> comparator, boolean z, Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) o34.i(iterable, a34.EMPTY_ENTRY_ARRAY);
        return e(comparator, z, entryArr, entryArr.length);
    }

    @DexIgnore
    public static <K, V> k34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return copyOf(iterable, (i44) i);
    }

    @DexIgnore
    public static <K, V> k34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable, Comparator<? super K> comparator) {
        i14.l(comparator);
        return c(comparator, false, iterable);
    }

    @DexIgnore
    public static <K, V> k34<K, V> copyOf(Map<? extends K, ? extends V> map) {
        return b(map, (i44) i);
    }

    @DexIgnore
    public static <K, V> k34<K, V> copyOf(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        i14.l(comparator);
        return b(map, comparator);
    }

    @DexIgnore
    public static <K, V> k34<K, V> copyOfSorted(SortedMap<K, ? extends V> sortedMap) {
        Comparator<? super K> comparator = sortedMap.comparator();
        Comparator<Comparable> comparator2 = comparator == null ? i : comparator;
        if (sortedMap instanceof k34) {
            k34<K, V> k34 = (k34) sortedMap;
            if (!k34.isPartialView()) {
                return k34;
            }
        }
        return c(comparator2, true, sortedMap.entrySet());
    }

    @DexIgnore
    public static <K, V> k34<K, V> e(Comparator<? super K> comparator, boolean z, Map.Entry<K, V>[] entryArr, int i2) {
        if (i2 == 0) {
            return emptyMap(comparator);
        }
        if (i2 == 1) {
            return g(comparator, entryArr[0].getKey(), entryArr[0].getValue());
        }
        Object[] objArr = new Object[i2];
        Object[] objArr2 = new Object[i2];
        if (z) {
            for (int i3 = 0; i3 < i2; i3++) {
                K key = entryArr[i3].getKey();
                V value = entryArr[i3].getValue();
                a24.a(key, value);
                objArr[i3] = key;
                objArr2[i3] = value;
            }
        } else {
            Arrays.sort(entryArr, 0, i2, i44.from(comparator).onKeys());
            K key2 = entryArr[0].getKey();
            objArr[0] = key2;
            objArr2[0] = entryArr[0].getValue();
            int i4 = 1;
            while (i4 < i2) {
                K key3 = entryArr[i4].getKey();
                V value2 = entryArr[i4].getValue();
                a24.a(key3, value2);
                objArr[i4] = key3;
                objArr2[i4] = value2;
                a34.checkNoConflict(comparator.compare(key2, key3) != 0, "key", entryArr[i4 - 1], entryArr[i4]);
                i4++;
                key2 = key3;
            }
        }
        return new k34<>(new s44(new o44(objArr), comparator), new o44(objArr2));
    }

    @DexIgnore
    public static <K, V> k34<K, V> emptyMap(Comparator<? super K> comparator) {
        return i44.natural().equals(comparator) ? of() : new k34<>(m34.emptySet(comparator), y24.of());
    }

    @DexIgnore
    public static <K, V> k34<K, V> g(Comparator<? super K> comparator, K k, V v) {
        y24 of = y24.of(k);
        i14.l(comparator);
        return new k34<>(new s44(of, comparator), y24.of(v));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> k34<K, V> h(b34<K, V>... b34Arr) {
        return e(i44.natural(), false, b34Arr, b34Arr.length);
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> b<K, V> naturalOrder() {
        return new b<>(i44.natural());
    }

    @DexIgnore
    public static <K, V> k34<K, V> of() {
        return (k34<K, V>) j;
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> k34<K, V> of(K k, V v) {
        return g(i44.natural(), k, v);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> k34<K, V> of(K k, V v, K k2, V v2) {
        return h(a34.entryOf(k, v), a34.entryOf(k2, v2));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> k34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return h(a34.entryOf(k, v), a34.entryOf(k2, v2), a34.entryOf(k3, v3));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> k34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return h(a34.entryOf(k, v), a34.entryOf(k2, v2), a34.entryOf(k3, v3), a34.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> k34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return h(a34.entryOf(k, v), a34.entryOf(k2, v2), a34.entryOf(k3, v3), a34.entryOf(k4, v4), a34.entryOf(k5, v5));
    }

    @DexIgnore
    public static <K, V> b<K, V> orderedBy(Comparator<K> comparator) {
        return new b<>(comparator);
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> b<K, V> reverseOrder() {
        return new b<>(i44.natural().reverse());
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> ceilingEntry(K k) {
        return tailMap((k34<K, V>) k, true).firstEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K ceilingKey(K k) {
        return (K) x34.i(ceilingEntry(k));
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public Comparator<? super K> comparator() {
        return keySet().comparator();
    }

    @DexIgnore
    @Override // com.fossil.a34
    public h34<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? h34.of() : new a();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public m34<K> descendingKeySet() {
        return this.f.descendingSet();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public k34<K, V> descendingMap() {
        k34<K, V> k34 = this.h;
        return k34 == null ? isEmpty() ? emptyMap(i44.from(comparator()).reverse()) : new k34<>((s44) this.f.descendingSet(), this.g.reverse(), this) : k34;
    }

    @DexIgnore
    @Override // com.fossil.a34, com.fossil.a34, java.util.Map, java.util.SortedMap
    public h34<Map.Entry<K, V>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    public final k34<K, V> f(int i2, int i3) {
        return (i2 == 0 && i3 == size()) ? this : i2 == i3 ? emptyMap(comparator()) : new k34<>(this.f.getSubSet(i2, i3), this.g.subList(i2, i3));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> firstEntry() {
        if (isEmpty()) {
            return null;
        }
        return entrySet().asList().get(0);
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public K firstKey() {
        return keySet().first();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> floorEntry(K k) {
        return headMap((k34<K, V>) k, true).lastEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K floorKey(K k) {
        return (K) x34.i(floorEntry(k));
    }

    @DexIgnore
    @Override // com.fossil.a34, java.util.Map
    public V get(Object obj) {
        int indexOf = this.f.indexOf(obj);
        if (indexOf == -1) {
            return null;
        }
        return this.g.get(indexOf);
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public k34<K, V> headMap(K k) {
        return headMap((k34<K, V>) k, false);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public k34<K, V> headMap(K k, boolean z) {
        s44<K> s44 = this.f;
        i14.l(k);
        return f(0, s44.headIndex(k, z));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> higherEntry(K k) {
        return tailMap((k34<K, V>) k, false).firstEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K higherKey(K k) {
        return (K) x34.i(higherEntry(k));
    }

    @DexIgnore
    @Override // com.fossil.a34
    public boolean isPartialView() {
        return this.f.isPartialView() || this.g.isPartialView();
    }

    @DexIgnore
    @Override // com.fossil.a34, com.fossil.a34, java.util.Map, java.util.SortedMap
    public m34<K> keySet() {
        return this.f;
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> lastEntry() {
        if (isEmpty()) {
            return null;
        }
        return entrySet().asList().get(size() - 1);
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public K lastKey() {
        return keySet().last();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> lowerEntry(K k) {
        return headMap((k34<K, V>) k, false).lastEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K lowerKey(K k) {
        return (K) x34.i(lowerEntry(k));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public m34<K> navigableKeySet() {
        return this.f;
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    @CanIgnoreReturnValue
    @Deprecated
    public final Map.Entry<K, V> pollFirstEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    @CanIgnoreReturnValue
    @Deprecated
    public final Map.Entry<K, V> pollLastEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int size() {
        return this.g.size();
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public k34<K, V> subMap(K k, K k2) {
        return subMap((boolean) k, true, (boolean) k2, false);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public k34<K, V> subMap(K k, boolean z, K k2, boolean z2) {
        i14.l(k);
        i14.l(k2);
        i14.i(comparator().compare(k, k2) <= 0, "expected fromKey <= toKey but %s > %s", k, k2);
        return headMap((k34<K, V>) k2, z2).tailMap((k34<K, V>) k, z);
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public k34<K, V> tailMap(K k) {
        return tailMap((k34<K, V>) k, true);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public k34<K, V> tailMap(K k, boolean z) {
        s44<K> s44 = this.f;
        i14.l(k);
        return f(s44.tailIndex(k, z), size());
    }

    @DexIgnore
    @Override // com.fossil.a34, com.fossil.a34, java.util.Map, java.util.SortedMap
    public u24<V> values() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.a34
    public Object writeReplace() {
        return new c(this);
    }
}
