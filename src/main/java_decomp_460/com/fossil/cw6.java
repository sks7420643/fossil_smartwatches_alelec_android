package com.fossil;

import android.os.Bundle;
import com.fossil.iq4;
import com.fossil.iv5;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cw6 extends yv6 {
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public /* final */ zv6 f;
    @DexIgnore
    public /* final */ iv5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<iv5.c, iv5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ cw6 f676a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(cw6 cw6) {
            this.f676a = cw6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(iv5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f676a.q().h();
            int a2 = bVar.a();
            if (a2 == 400005) {
                zv6 q = this.f676a.q();
                String c = um5.c(PortfolioApp.h0.c(), 2131886985);
                pq7.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
                q.r5(c);
                this.f676a.q().P4(false);
            } else if (a2 != 404001) {
                zv6 q2 = this.f676a.q();
                int a3 = bVar.a();
                String b = bVar.b();
                if (b == null) {
                    b = "";
                }
                q2.o(a3, b);
                this.f676a.q().P4(false);
            } else {
                zv6 q3 = this.f676a.q();
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886980);
                pq7.b(c2, "LanguageHelper.getString\u2026xt__EmailIsNotRegistered)");
                q3.r5(c2);
                this.f676a.q().P4(true);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(iv5.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f676a.q().h();
            this.f676a.q().m3();
        }
    }

    @DexIgnore
    public cw6(zv6 zv6, iv5 iv5) {
        pq7.c(zv6, "mView");
        pq7.c(iv5, "mResetPasswordUseCase");
        this.f = zv6;
        this.g = iv5;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        this.f.n6(this.e);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.yv6
    public void n(String str) {
        pq7.c(str, Constants.EMAIL);
        this.e = str;
        if (str.length() == 0) {
            this.f.H2();
        } else {
            this.f.S0();
        }
    }

    @DexIgnore
    @Override // com.fossil.yv6
    public void o(String str) {
        pq7.c(str, Constants.EMAIL);
        if (r()) {
            this.f.i();
            this.g.e(new iv5.a(str), new a(this));
        }
    }

    @DexIgnore
    public final void p(String str) {
        pq7.c(str, Constants.EMAIL);
        this.e = str;
    }

    @DexIgnore
    public final zv6 q() {
        return this.f;
    }

    @DexIgnore
    public final boolean r() {
        if (this.e.length() == 0) {
            this.f.r5("");
            return false;
        } else if (!b47.a(this.e)) {
            zv6 zv6 = this.f;
            String c = um5.c(PortfolioApp.h0.c(), 2131886985);
            pq7.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            zv6.r5(c);
            return false;
        } else {
            this.f.r5("");
            return true;
        }
    }

    @DexIgnore
    public final void s(String str, Bundle bundle) {
        pq7.c(str, "key");
        pq7.c(bundle, "outState");
        bundle.putString(str, this.e);
    }

    @DexIgnore
    public void t() {
        this.f.M5(this);
    }
}
