package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class td1 implements nd1<byte[]> {
    @DexIgnore
    @Override // com.fossil.nd1
    public int a() {
        return 1;
    }

    @DexIgnore
    /* renamed from: c */
    public int b(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    /* renamed from: d */
    public byte[] newArray(int i) {
        return new byte[i];
    }

    @DexIgnore
    @Override // com.fossil.nd1
    public String getTag() {
        return "ByteArrayPool";
    }
}
