package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ym7 extends xm7 {
    @DexIgnore
    public static final int b(int i) {
        if (i < 0) {
            return i;
        }
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> c(cl7<? extends K, ? extends V> cl7) {
        pq7.c(cl7, "pair");
        Map<K, V> singletonMap = Collections.singletonMap(cl7.getFirst(), cl7.getSecond());
        pq7.b(singletonMap, "java.util.Collections.si\u2026(pair.first, pair.second)");
        return singletonMap;
    }

    @DexIgnore
    public static final <K extends Comparable<? super K>, V> SortedMap<K, V> d(cl7<? extends K, ? extends V>... cl7Arr) {
        pq7.c(cl7Arr, "pairs");
        TreeMap treeMap = new TreeMap();
        zm7.m(treeMap, cl7Arr);
        return treeMap;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> e(Map<? extends K, ? extends V> map) {
        pq7.c(map, "$this$toSingletonMap");
        Map.Entry<? extends K, ? extends V> next = map.entrySet().iterator().next();
        Map<K, V> singletonMap = Collections.singletonMap(next.getKey(), next.getValue());
        pq7.b(singletonMap, "java.util.Collections.singletonMap(key, value)");
        pq7.b(singletonMap, "with(entries.iterator().\u2026ingletonMap(key, value) }");
        return singletonMap;
    }

    @DexIgnore
    public static final <K extends Comparable<? super K>, V> SortedMap<K, V> f(Map<? extends K, ? extends V> map) {
        pq7.c(map, "$this$toSortedMap");
        return new TreeMap(map);
    }
}
