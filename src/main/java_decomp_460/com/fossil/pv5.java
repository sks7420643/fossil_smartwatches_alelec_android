package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ul5;
import com.fossil.v78;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pv5 extends Fragment implements v78.a, View.OnKeyListener, ul5.a {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public ck5 b;
    @DexIgnore
    public /* final */ zp0 c; // = new sr4(this);
    @DexIgnore
    public vl5 d;
    @DexIgnore
    public HashMap e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Window f2876a;

        @DexIgnore
        public a(Window window) {
            this.f2876a = window;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            Window window = this.f2876a;
            pq7.b(window, "window");
            pq7.b(valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                window.setStatusBarColor(((Integer) animatedValue).intValue());
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AnimatorListenerAdapter {
        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            pq7.c(animator, "animation");
            super.onAnimationEnd(animator);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ View f2877a;

        @DexIgnore
        public c(View view) {
            this.f2877a = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            pq7.c(animator, "animation");
            this.f2877a.setVisibility(4);
            super.onAnimationEnd(animator);
        }
    }

    /*
    static {
        String simpleName = pv5.class.getSimpleName();
        pq7.b(simpleName, "BaseFragment::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public final zp0 A6() {
        return this.c;
    }

    @DexIgnore
    public final ck5 B6() {
        ck5 ck5 = this.b;
        if (ck5 != null) {
            return ck5;
        }
        pq7.n("mAnalyticHelper");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v78.a
    public void C4(int i, List<String> list) {
        pq7.c(list, "perms");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "onPermissionsGranted: perm = " + it.next());
        }
    }

    @DexIgnore
    public final vl5 C6() {
        return this.d;
    }

    @DexIgnore
    public String D6() {
        return f;
    }

    @DexIgnore
    public final void E6(String str) {
        pq7.c(str, "view");
        vl5 e2 = ck5.f.e();
        e2.l(str);
        this.d = e2;
        if (e2 != null) {
            e2.h(this);
        }
    }

    @DexIgnore
    public boolean F6() {
        if (!isActive() || getParentFragment() == null || !(getParentFragment() instanceof pv5)) {
            return false;
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            return ((pv5) parentFragment).F6();
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final void G6(Fragment fragment, String str, int i) {
        pq7.c(fragment, "fragment");
        pq7.c(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        xq0 j = childFragmentManager.j();
        pq7.b(j, "fragmentManager.beginTransaction()");
        Fragment Y = childFragmentManager.Y(i);
        if (Y != null) {
            j.q(Y);
        }
        j.f(null);
        j.s(i, fragment, str);
        j.i();
    }

    @DexIgnore
    public final void H6(String str) {
        pq7.c(str, "title");
        FLogger.INSTANCE.getLocal().d(f, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).G(str);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void I6(String str) {
        pq7.c(str, "content");
        LayoutInflater layoutInflater = getLayoutInflater();
        pq7.b(layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558834, activity != null ? (ViewGroup) activity.findViewById(2131362178) : null);
        View findViewById = inflate.findViewById(2131362386);
        pq7.b(findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public final void J6(Intent intent, String str) {
        pq7.c(intent, "browserIntent");
        pq7.c(str, "tag");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            FLogger.INSTANCE.getLocal().e(str, "Exception when start url with no browser app");
        }
    }

    @DexIgnore
    @Override // com.fossil.ul5.a
    public void T4() {
        FLogger.INSTANCE.getLocal().d(f, "Tracer ended");
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(f, "hideProgressDialog");
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).t();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(f, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).F();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    @Override // com.fossil.ul5.a
    public void f2() {
        FLogger.INSTANCE.getLocal().d(f, "Tracer started");
        PortfolioApp.h0.c().f2(this.d);
    }

    @DexIgnore
    public final boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    @Override // com.fossil.v78.a
    public void k1(int i, List<String> list) {
        pq7.c(list, "perms");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "onPermissionsDenied: perm = " + it.next());
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = ck5.f.g();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        vl5 vl5 = this.d;
        if (vl5 != null) {
            vl5.g();
        }
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        pq7.c(view, "view");
        pq7.c(keyEvent, "keyEvent");
        if (keyEvent.getAction() != 1 || i != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(f, "onKey KEYCODE_BACK");
        return F6();
    }

    @DexIgnore
    @Override // com.fossil.rk0.b, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        pq7.c(strArr, "permissions");
        pq7.c(iArr, "grantResults");
        super.onRequestPermissionsResult(i, strArr, iArr);
        v78.c(i, strArr, iArr, this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131363010);
        if (viewGroup != null) {
            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                viewGroup.setBackgroundColor(Color.parseColor(d2));
            }
        }
        view.setOnKeyListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void w6(String str, Map<String, String> map) {
        pq7.c(str, Constants.EVENT);
        pq7.c(map, "values");
        ck5 ck5 = this.b;
        if (ck5 != null) {
            ck5.l(str, map);
        } else {
            pq7.n("mAnalyticHelper");
            throw null;
        }
    }

    @DexIgnore
    public void x6(String str, String str2, long j) {
        pq7.c(str, "startColor");
        pq7.c(str2, "endColor");
        if (Build.VERSION.SDK_INT >= 23) {
            FragmentActivity requireActivity = requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            Window window = requireActivity.getWindow();
            window.clearFlags(67108864);
            window.addFlags(RecyclerView.UNDEFINED_DURATION);
            int parseColor = Color.parseColor(str);
            int parseColor2 = Color.parseColor(str2);
            ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(parseColor), Integer.valueOf(parseColor2));
            ofObject.addUpdateListener(new a(window));
            ofObject.setDuration(j).start();
        }
    }

    @DexIgnore
    public final void y6(View view, long j, int[] iArr) {
        pq7.c(view, "v");
        pq7.c(iArr, "target");
        view.setVisibility(0);
        view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setX((float) iArr[0]);
        view.setY((float) iArr[1]);
        view.animate().setDuration(j).translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).translationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new b()).setInterpolator(new OvershootInterpolator(0.9f)).alpha(1.0f).start();
    }

    @DexIgnore
    public final void z6(View view, long j, int[] iArr) {
        pq7.c(view, "v");
        pq7.c(iArr, "target");
        view.setVisibility(0);
        view.setAlpha(1.0f);
        view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.animate().setDuration(j).y((float) iArr[1]).x((float) iArr[0]).setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new c(view)).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).start();
    }
}
