package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s33 implements v33 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ xz2 f3201a;

    @DexIgnore
    public s33(xz2 xz2) {
        this.f3201a = xz2;
    }

    @DexIgnore
    @Override // com.fossil.v33
    public final byte zza(int i) {
        return this.f3201a.zza(i);
    }

    @DexIgnore
    @Override // com.fossil.v33
    public final int zza() {
        return this.f3201a.zza();
    }
}
