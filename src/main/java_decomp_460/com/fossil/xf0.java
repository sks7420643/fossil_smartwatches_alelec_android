package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ig0;
import com.fossil.jg0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xf0 implements ig0 {
    @DexIgnore
    public Context b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public cg0 d;
    @DexIgnore
    public LayoutInflater e;
    @DexIgnore
    public ig0.a f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public jg0 i;
    @DexIgnore
    public int j;

    @DexIgnore
    public xf0(Context context, int i2, int i3) {
        this.b = context;
        this.e = LayoutInflater.from(context);
        this.g = i2;
        this.h = i3;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void b(cg0 cg0, boolean z) {
        ig0.a aVar = this.f;
        if (aVar != null) {
            aVar.b(cg0, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void c(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.i;
        if (viewGroup != null) {
            cg0 cg0 = this.d;
            if (cg0 != null) {
                cg0.t();
                ArrayList<eg0> G = this.d.G();
                int size = G.size();
                int i4 = 0;
                i2 = 0;
                while (i4 < size) {
                    eg0 eg0 = G.get(i4);
                    if (t(i2, eg0)) {
                        View childAt = viewGroup.getChildAt(i2);
                        eg0 itemData = childAt instanceof jg0.a ? ((jg0.a) childAt).getItemData() : null;
                        View q = q(eg0, childAt, viewGroup);
                        if (eg0 != itemData) {
                            q.setPressed(false);
                            q.jumpDrawablesToCurrentState();
                        }
                        if (q != childAt) {
                            j(q, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i4++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!o(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean e(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean f(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void g(ig0.a aVar) {
        this.f = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public int getId() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void h(Context context, cg0 cg0) {
        this.c = context;
        LayoutInflater.from(context);
        this.d = cg0;
    }

    @DexIgnore
    public void j(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.i).addView(view, i2);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.cg0] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.ig0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean k(com.fossil.ng0 r2) {
        /*
            r1 = this;
            com.fossil.ig0$a r0 = r1.f
            if (r0 == 0) goto L_0x000e
            if (r2 == 0) goto L_0x000b
        L_0x0006:
            boolean r0 = r0.c(r2)
        L_0x000a:
            return r0
        L_0x000b:
            com.fossil.cg0 r2 = r1.d
            goto L_0x0006
        L_0x000e:
            r0 = 0
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xf0.k(com.fossil.ng0):boolean");
    }

    @DexIgnore
    public abstract void m(eg0 eg0, jg0.a aVar);

    @DexIgnore
    public jg0.a n(ViewGroup viewGroup) {
        return (jg0.a) this.e.inflate(this.h, viewGroup, false);
    }

    @DexIgnore
    public boolean o(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    @DexIgnore
    public ig0.a p() {
        return this.f;
    }

    @DexIgnore
    public View q(eg0 eg0, View view, ViewGroup viewGroup) {
        jg0.a n = view instanceof jg0.a ? (jg0.a) view : n(viewGroup);
        m(eg0, n);
        return (View) n;
    }

    @DexIgnore
    public jg0 r(ViewGroup viewGroup) {
        if (this.i == null) {
            jg0 jg0 = (jg0) this.e.inflate(this.g, viewGroup, false);
            this.i = jg0;
            jg0.b(this.d);
            c(true);
        }
        return this.i;
    }

    @DexIgnore
    public void s(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public abstract boolean t(int i2, eg0 eg0);
}
