package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nz4 {
    @DexIgnore
    public final String a(int[] iArr) {
        if (iArr == null) {
            return "[]";
        }
        String arrays = Arrays.toString(iArr);
        pq7.b(arrays, "Arrays.toString(array)");
        return arrays;
    }

    @DexIgnore
    public final int[] b(String str) {
        if (str == null) {
            return new int[0];
        }
        if (wt7.v(str, "null", false, 2, null)) {
            return new int[0];
        }
        try {
            List<String> Y = wt7.Y(wt7.V(str, "[", "]"), new String[]{","}, false, 0, 6, null);
            ArrayList arrayList = new ArrayList(im7.m(Y, 10));
            for (String str2 : Y) {
                int length = str2.length() - 1;
                boolean z = false;
                int i = 0;
                while (i <= length) {
                    boolean z2 = str2.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                arrayList.add(Integer.valueOf(Integer.parseInt(str2.subSequence(i, length + 1).toString())));
            }
            return pm7.g0(arrayList);
        } catch (Exception e) {
            return new int[0];
        }
    }
}
