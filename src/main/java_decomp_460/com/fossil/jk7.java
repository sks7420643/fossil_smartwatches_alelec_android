package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jk7<T> implements Factory<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f1773a;

    @DexIgnore
    public jk7(T t) {
        this.f1773a = t;
    }

    @DexIgnore
    public static <T> Factory<T> a(T t) {
        lk7.c(t, "instance cannot be null");
        return new jk7(t);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public T get() {
        return this.f1773a;
    }
}
