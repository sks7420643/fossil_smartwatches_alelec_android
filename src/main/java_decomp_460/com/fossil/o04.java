package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o04 extends s04 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new b();
    @DexIgnore
    public AnimatorSet f;
    @DexIgnore
    public ValueAnimator g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (!o04.j(editable)) {
                o04.this.f.cancel();
                o04.this.g.start();
            } else if (!o04.this.f3188a.E()) {
                o04.this.g.cancel();
                o04.this.f.start();
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements TextInputLayout.f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(o04.j(editText.getText()));
            textInputLayout.setEndIconCheckable(false);
            editText.removeTextChangedListener(o04.this.d);
            editText.addTextChangedListener(o04.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(View view) {
            o04.this.f3188a.getEditText().setText((CharSequence) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AnimatorListenerAdapter {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            o04.this.f3188a.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AnimatorListenerAdapter {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            o04.this.f3188a.setEndIconVisible(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            o04.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            o04.this.c.setScaleX(floatValue);
            o04.this.c.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public o04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public static boolean j(Editable editable) {
        return editable.length() > 0;
    }

    @DexIgnore
    @Override // com.fossil.s04
    public void a() {
        this.f3188a.setEndIconDrawable(gf0.d(this.b, mw3.mtrl_ic_cancel));
        TextInputLayout textInputLayout = this.f3188a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(rw3.clear_text_end_icon_content_description));
        this.f3188a.setEndIconOnClickListener(new c());
        this.f3188a.c(this.e);
        k();
    }

    @DexIgnore
    public final ValueAnimator h(float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(uw3.f3651a);
        ofFloat.setDuration(100L);
        ofFloat.addUpdateListener(new f());
        return ofFloat;
    }

    @DexIgnore
    public final ValueAnimator i() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.8f, 1.0f);
        ofFloat.setInterpolator(uw3.d);
        ofFloat.setDuration(150L);
        ofFloat.addUpdateListener(new g());
        return ofFloat;
    }

    @DexIgnore
    public final void k() {
        ValueAnimator i = i();
        ValueAnimator h = h(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        this.f = animatorSet;
        animatorSet.playTogether(i, h);
        this.f.addListener(new d());
        ValueAnimator h2 = h(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = h2;
        h2.addListener(new e());
    }
}
