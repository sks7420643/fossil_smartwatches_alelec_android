package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p54 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements r54<List<String>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<String> f2782a; // = t34.g();

        @DexIgnore
        @Override // com.fossil.r54
        public boolean a(String str) {
            this.f2782a.add(str);
            return true;
        }

        @DexIgnore
        /* renamed from: b */
        public List<String> getResult() {
            return this.f2782a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends k54 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ File f2783a;

        @DexIgnore
        public b(File file) {
            i14.l(file);
            this.f2783a = file;
        }

        @DexIgnore
        public /* synthetic */ b(File file, a aVar) {
            this(file);
        }

        @DexIgnore
        /* renamed from: c */
        public FileInputStream b() throws IOException {
            return new FileInputStream(this.f2783a);
        }

        @DexIgnore
        public String toString() {
            return "Files.asByteSource(" + this.f2783a + ")";
        }
    }

    @DexIgnore
    public static k54 a(File file) {
        return new b(file, null);
    }

    @DexIgnore
    public static l54 b(File file, Charset charset) {
        return a(file).a(charset);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T c(File file, Charset charset, r54<T> r54) throws IOException {
        return (T) b(file, charset).b(r54);
    }

    @DexIgnore
    public static List<String> d(File file, Charset charset) throws IOException {
        return (List) c(file, charset, new a());
    }
}
