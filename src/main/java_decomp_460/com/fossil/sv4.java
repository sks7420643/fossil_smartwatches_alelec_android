package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv4 extends pv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<n85> h;
    @DexIgnore
    public uv4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public xv4 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return sv4.s;
        }

        @DexIgnore
        public final sv4 b(String str, boolean z) {
            sv4 sv4 = new sv4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id_extra", str);
            bundle.putBoolean("challenge_status_extra", z);
            sv4.setArguments(bundle);
            return sv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sv4 b;

        @DexIgnore
        public b(sv4 sv4) {
            this.b = sv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv4 f3313a;

        @DexIgnore
        public c(sv4 sv4) {
            this.f3313a = sv4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            n85 n85 = (n85) sv4.K6(this.f3313a).a();
            if (!(n85 == null || (flexibleTextView = n85.q) == null)) {
                flexibleTextView.setVisibility(8);
            }
            sv4.O6(this.f3313a).j(this.f3313a.j, this.f3313a.k);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv4 f3314a;

        @DexIgnore
        public d(sv4 sv4) {
            this.f3314a = sv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            n85 n85 = (n85) sv4.K6(this.f3314a).a();
            if (n85 != null) {
                SwipeRefreshLayout swipeRefreshLayout = n85.u;
                pq7.b(swipeRefreshLayout, "swipe");
                pq7.b(bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<List<? extends Object>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv4 f3315a;

        @DexIgnore
        public e(sv4 sv4) {
            this.f3315a = sv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            xv4 M6 = sv4.M6(this.f3315a);
            pq7.b(list, "it");
            M6.g(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sv4 f3316a;

        @DexIgnore
        public f(sv4 sv4) {
            this.f3316a = sv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            if (cl7.getFirst().booleanValue()) {
                n85 n85 = (n85) sv4.K6(this.f3316a).a();
                if (n85 != null) {
                    FlexibleTextView flexibleTextView = n85.q;
                    pq7.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            FragmentActivity activity = this.f3316a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, um5.c(activity, 2131886231), 1).show();
            }
        }
    }

    /*
    static {
        String simpleName = sv4.class.getSimpleName();
        pq7.b(simpleName, "BCMemberInChallengeFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(sv4 sv4) {
        g37<n85> g37 = sv4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ xv4 M6(sv4 sv4) {
        xv4 xv4 = sv4.l;
        if (xv4 != null) {
            return xv4;
        }
        pq7.n("memberAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ uv4 O6(sv4 sv4) {
        uv4 uv4 = sv4.i;
        if (uv4 != null) {
            return uv4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        xv4 xv4 = new xv4();
        this.l = xv4;
        if (xv4 != null) {
            xv4.setHasStableIds(true);
            g37<n85> g37 = this.h;
            if (g37 != null) {
                n85 a2 = g37.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.t;
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    recyclerView.setHasFixedSize(true);
                    xv4 xv42 = this.l;
                    if (xv42 != null) {
                        recyclerView.setAdapter(xv42);
                        a2.r.setOnClickListener(new b(this));
                        a2.u.setOnRefreshListener(new c(this));
                        return;
                    }
                    pq7.n("memberAdapter");
                    throw null;
                }
                return;
            }
            pq7.n("binding");
            throw null;
        }
        pq7.n("memberAdapter");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        uv4 uv4 = this.i;
        if (uv4 != null) {
            uv4.g().h(getViewLifecycleOwner(), new d(this));
            uv4 uv42 = this.i;
            if (uv42 != null) {
                uv42.h().h(getViewLifecycleOwner(), new e(this));
                uv4 uv43 = this.i;
                if (uv43 != null) {
                    uv43.f().h(getViewLifecycleOwner(), new f(this));
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().D1().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(uv4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ngeViewModel::class.java)");
            this.i = (uv4) a2;
            Bundle arguments = getArguments();
            this.j = arguments != null ? arguments.getString("challenge_id_extra") : null;
            Bundle arguments2 = getArguments();
            this.k = arguments2 != null ? arguments2.getBoolean("challenge_status_extra") : true;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        n85 n85 = (n85) aq0.f(layoutInflater, 2131558584, viewGroup, false, A6());
        this.h = new g37<>(this, n85);
        pq7.b(n85, "binding");
        return n85.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        uv4 uv4 = this.i;
        if (uv4 != null) {
            uv4.j(this.j, this.k);
            Q6();
            R6();
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
