package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy2<E> extends sx2<E> {
    @DexIgnore
    public static /* final */ sx2<Object> zza; // = new qy2(new Object[0], 0);
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient int e;

    @DexIgnore
    public qy2(Object[] objArr, int i) {
        this.d = objArr;
        this.e = i;
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        sw2.a(i, this.e);
        return (E) this.d[i];
    }

    @DexIgnore
    public final int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.sx2, com.fossil.tx2
    public final int zzb(Object[] objArr, int i) {
        System.arraycopy(this.d, 0, objArr, i, this.e);
        return this.e + i;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final Object[] zze() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzf() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzg() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return false;
    }
}
