package com.fossil;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v11 implements n11, g21, k11 {
    @DexIgnore
    public static /* final */ String j; // = x01.f("GreedyScheduler");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ s11 c;
    @DexIgnore
    public /* final */ h21 d;
    @DexIgnore
    public /* final */ Set<o31> e; // = new HashSet();
    @DexIgnore
    public u11 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ Object h;
    @DexIgnore
    public Boolean i;

    @DexIgnore
    public v11(Context context, o01 o01, k41 k41, s11 s11) {
        this.b = context;
        this.c = s11;
        this.d = new h21(context, k41, this);
        this.f = new u11(this, o01.h());
        this.h = new Object();
    }

    @DexIgnore
    @Override // com.fossil.n11
    public void a(o31... o31Arr) {
        if (this.i == null) {
            this.i = Boolean.valueOf(TextUtils.equals(this.b.getPackageName(), g()));
        }
        if (!this.i.booleanValue()) {
            x01.c().d(j, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        h();
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (o31 o31 : o31Arr) {
            long a2 = o31.a();
            long currentTimeMillis = System.currentTimeMillis();
            if (o31.b == f11.ENQUEUED) {
                if (currentTimeMillis < a2) {
                    u11 u11 = this.f;
                    if (u11 != null) {
                        u11.a(o31);
                    }
                } else if (!o31.b()) {
                    x01.c().a(j, String.format("Starting work for %s", o31.f2626a), new Throwable[0]);
                    this.c.v(o31.f2626a);
                } else if (Build.VERSION.SDK_INT >= 23 && o31.j.h()) {
                    x01.c().a(j, String.format("Ignoring WorkSpec %s, Requires device idle.", o31), new Throwable[0]);
                } else if (Build.VERSION.SDK_INT < 24 || !o31.j.e()) {
                    hashSet.add(o31);
                    hashSet2.add(o31.f2626a);
                } else {
                    x01.c().a(j, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", o31), new Throwable[0]);
                }
            }
        }
        synchronized (this.h) {
            if (!hashSet.isEmpty()) {
                x01.c().a(j, String.format("Starting tracking for [%s]", TextUtils.join(",", hashSet2)), new Throwable[0]);
                this.e.addAll(hashSet);
                this.d.d(this.e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.g21
    public void b(List<String> list) {
        for (String str : list) {
            x01.c().a(j, String.format("Constraints not met: Cancelling work ID %s", str), new Throwable[0]);
            this.c.y(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.n11
    public boolean c() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.k11
    public void d(String str, boolean z) {
        i(str);
    }

    @DexIgnore
    @Override // com.fossil.n11
    public void e(String str) {
        if (this.i == null) {
            this.i = Boolean.valueOf(TextUtils.equals(this.b.getPackageName(), g()));
        }
        if (!this.i.booleanValue()) {
            x01.c().d(j, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        h();
        x01.c().a(j, String.format("Cancelling work ID %s", str), new Throwable[0]);
        u11 u11 = this.f;
        if (u11 != null) {
            u11.b(str);
        }
        this.c.y(str);
    }

    @DexIgnore
    @Override // com.fossil.g21
    public void f(List<String> list) {
        for (String str : list) {
            x01.c().a(j, String.format("Constraints met: Scheduling work ID %s", str), new Throwable[0]);
            this.c.v(str);
        }
    }

    @DexIgnore
    public final String g() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        Object invoke;
        if (Build.VERSION.SDK_INT >= 28) {
            return Application.getProcessName();
        }
        try {
            Class<?> cls = Class.forName("android.app.ActivityThread", false, v11.class.getClassLoader());
            if (Build.VERSION.SDK_INT >= 18) {
                Method declaredMethod = cls.getDeclaredMethod("currentProcessName", new Class[0]);
                declaredMethod.setAccessible(true);
                invoke = declaredMethod.invoke(null, new Object[0]);
            } else {
                Method declaredMethod2 = cls.getDeclaredMethod("currentActivityThread", new Class[0]);
                declaredMethod2.setAccessible(true);
                Method declaredMethod3 = cls.getDeclaredMethod("getProcessName", new Class[0]);
                declaredMethod3.setAccessible(true);
                invoke = declaredMethod3.invoke(declaredMethod2.invoke(null, new Object[0]), new Object[0]);
            }
            if (invoke instanceof String) {
                return (String) invoke;
            }
        } catch (Throwable th) {
            x01.c().a(j, "Unable to check ActivityThread for processName", th);
        }
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) this.b.getSystemService(Constants.ACTIVITY);
        if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.isEmpty())) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.pid == myPid) {
                    return runningAppProcessInfo.processName;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public final void h() {
        if (!this.g) {
            this.c.n().b(this);
            this.g = true;
        }
    }

    @DexIgnore
    public final void i(String str) {
        synchronized (this.h) {
            Iterator<o31> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                o31 next = it.next();
                if (next.f2626a.equals(str)) {
                    x01.c().a(j, String.format("Stopping tracking for %s", str), new Throwable[0]);
                    this.e.remove(next);
                    this.d.d(this.e);
                    break;
                }
            }
        }
    }
}
