package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yx1 extends ox1 {
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public static /* final */ String JSON_ERROR_CODE_KEY; // = "error_code";
    @DexIgnore
    public /* final */ zx1 errorCode;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public yx1(zx1 zx1) {
        pq7.c(zx1, "errorCode");
        this.errorCode = zx1;
    }

    @DexIgnore
    public zx1 getErrorCode() {
        return this.errorCode;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("error_code", getErrorCode().getLogName());
        } catch (JSONException e) {
        }
        return jSONObject;
    }
}
