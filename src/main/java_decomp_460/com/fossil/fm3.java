package com.fossil;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm3 implements ServiceConnection {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1155a;
    @DexIgnore
    public /* final */ /* synthetic */ cm3 b;

    @DexIgnore
    public fm3(cm3 cm3, String str) {
        this.b = cm3;
        this.f1155a = str;
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.b.f629a.d().I().a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            bw2 e = dz2.e(iBinder);
            if (e == null) {
                this.b.f629a.d().I().a("Install Referrer Service implementation was not found");
                return;
            }
            this.b.f629a.d().N().a("Install Referrer Service connected");
            this.b.f629a.c().y(new em3(this, e, this));
        } catch (Exception e2) {
            this.b.f629a.d().I().b("Exception occurred while calling Install Referrer API", e2);
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        this.b.f629a.d().N().a("Install Referrer Service disconnected");
    }
}
