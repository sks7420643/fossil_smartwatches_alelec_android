package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu5 implements Factory<au5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<GoogleApiService> f511a;

    @DexIgnore
    public bu5(Provider<GoogleApiService> provider) {
        this.f511a = provider;
    }

    @DexIgnore
    public static bu5 a(Provider<GoogleApiService> provider) {
        return new bu5(provider);
    }

    @DexIgnore
    public static au5 c(GoogleApiService googleApiService) {
        return new au5(googleApiService);
    }

    @DexIgnore
    /* renamed from: b */
    public au5 get() {
        return c(this.f511a.get());
    }
}
