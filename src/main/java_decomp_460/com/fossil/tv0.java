package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.Surface;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ew0;
import com.fossil.vv0;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tv0 extends uv0 {
    @DexIgnore
    public static BitmapFactory.Options n;
    @DexIgnore
    public ew0 d;
    @DexIgnore
    public Bitmap e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public long l;
    @DexIgnore
    public boolean m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3474a;

        /*
        static {
            int[] iArr = new int[Bitmap.Config.values().length];
            f3474a = iArr;
            try {
                iArr[Bitmap.Config.ALPHA_8.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f3474a[Bitmap.Config.ARGB_8888.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f3474a[Bitmap.Config.RGB_565.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f3474a[Bitmap.Config.ARGB_4444.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        MIPMAP_NONE(0),
        MIPMAP_FULL(1),
        MIPMAP_ON_SYNC_TO_TEXTURE(2);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public b(int i) {
            this.mID = i;
        }
    }

    /*
    static {
        BitmapFactory.Options options = new BitmapFactory.Options();
        n = options;
        options.inScaled = false;
    }
    */

    @DexIgnore
    public tv0(long j2, RenderScript renderScript, ew0 ew0, int i2) {
        super(j2, renderScript);
        ew0.b bVar = ew0.b.POSITIVE_X;
        if ((i2 & -228) != 0) {
            throw new yv0("Unknown usage specified.");
        } else if ((i2 & 32) == 0 || (i2 & -36) == 0) {
            this.d = ew0;
            this.f = i2;
            this.l = 0;
            this.m = false;
            if (ew0 != null) {
                this.g = ew0.g() * this.d.i().o();
                q(ew0);
            }
            if (RenderScript.x) {
                try {
                    RenderScript.z.invoke(RenderScript.y, Integer.valueOf(this.g));
                } catch (Exception e2) {
                    Log.e("RenderScript_jni", "Couldn't invoke registerNativeAllocation:" + e2);
                    throw new aw0("Couldn't invoke registerNativeAllocation:" + e2);
                }
            }
        } else {
            throw new yv0("Invalid usage combination.");
        }
    }

    @DexIgnore
    public static tv0 h(RenderScript renderScript, Bitmap bitmap, b bVar, int i2) {
        renderScript.I();
        if (bitmap.getConfig() != null) {
            ew0 p = p(renderScript, bitmap, bVar);
            if (bVar == b.MIPMAP_NONE && p.i().q(vv0.h(renderScript)) && i2 == 131) {
                long l2 = renderScript.l(p.c(renderScript), bVar.mID, bitmap, i2);
                if (l2 != 0) {
                    tv0 tv0 = new tv0(l2, renderScript, p, i2);
                    tv0.m(bitmap);
                    return tv0;
                }
                throw new aw0("Load failed.");
            }
            long m2 = renderScript.m(p.c(renderScript), bVar.mID, bitmap, i2);
            if (m2 != 0) {
                return new tv0(m2, renderScript, p, i2);
            }
            throw new aw0("Load failed.");
        } else if ((i2 & 128) == 0) {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            return h(renderScript, createBitmap, bVar, i2);
        } else {
            throw new yv0("USAGE_SHARED cannot be used with a Bitmap that has a null config.");
        }
    }

    @DexIgnore
    public static tv0 i(RenderScript renderScript, ew0 ew0) {
        return j(renderScript, ew0, b.MIPMAP_NONE, 1);
    }

    @DexIgnore
    public static tv0 j(RenderScript renderScript, ew0 ew0, b bVar, int i2) {
        renderScript.I();
        if (ew0.c(renderScript) == 0) {
            throw new zv0("Bad Type");
        } else if (renderScript.H() || (i2 & 32) == 0) {
            long n2 = renderScript.n(ew0.c(renderScript), bVar.mID, i2, 0);
            if (n2 != 0) {
                return new tv0(n2, renderScript, ew0, i2);
            }
            throw new aw0("Allocation creation failed.");
        } else {
            throw new aw0("USAGE_IO not supported, Allocation creation failed.");
        }
    }

    @DexIgnore
    public static vv0 k(RenderScript renderScript, Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == Bitmap.Config.ALPHA_8) {
            return vv0.f(renderScript);
        }
        if (config == Bitmap.Config.ARGB_4444) {
            return vv0.g(renderScript);
        }
        if (config == Bitmap.Config.ARGB_8888) {
            return vv0.h(renderScript);
        }
        if (config == Bitmap.Config.RGB_565) {
            return vv0.i(renderScript);
        }
        throw new zv0("Bad bitmap type: " + config);
    }

    @DexIgnore
    public static ew0 p(RenderScript renderScript, Bitmap bitmap, b bVar) {
        ew0.a aVar = new ew0.a(renderScript, k(renderScript, bitmap));
        aVar.c(bitmap.getWidth());
        aVar.d(bitmap.getHeight());
        aVar.b(bVar == b.MIPMAP_FULL);
        return aVar.a();
    }

    @DexIgnore
    @Override // com.fossil.uv0
    public void b() {
        boolean z = true;
        if (this.l != 0) {
            synchronized (this) {
                if (!this.m) {
                    this.m = true;
                } else {
                    z = false;
                }
            }
            if (z) {
                ReentrantReadWriteLock.ReadLock readLock = this.c.k.readLock();
                readLock.lock();
                if (this.c.h()) {
                    this.c.y(this.l);
                }
                readLock.unlock();
                this.l = 0;
            }
        }
        if ((this.f & 96) != 0) {
            o(null);
        }
        super.b();
    }

    @DexIgnore
    public void f(Bitmap bitmap) {
        this.c.I();
        if (bitmap.getConfig() == null) {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            f(createBitmap);
            return;
        }
        s(bitmap);
        r(bitmap);
        RenderScript renderScript = this.c;
        renderScript.j(c(renderScript), bitmap);
    }

    @DexIgnore
    @Override // com.fossil.uv0
    public void finalize() throws Throwable {
        if (RenderScript.x) {
            RenderScript.A.invoke(RenderScript.y, Integer.valueOf(this.g));
        }
        super.finalize();
    }

    @DexIgnore
    public void g(Bitmap bitmap) {
        this.c.I();
        r(bitmap);
        s(bitmap);
        RenderScript renderScript = this.c;
        renderScript.k(c(renderScript), bitmap);
    }

    @DexIgnore
    public ew0 l() {
        return this.d;
    }

    @DexIgnore
    public final void m(Bitmap bitmap) {
        this.e = bitmap;
    }

    @DexIgnore
    public void n(long j2) {
        this.l = j2;
    }

    @DexIgnore
    public void o(Surface surface) {
        this.c.I();
        if ((this.f & 64) != 0) {
            RenderScript renderScript = this.c;
            renderScript.o(c(renderScript), surface);
            return;
        }
        throw new zv0("Allocation is not USAGE_IO_OUTPUT.");
    }

    @DexIgnore
    public final void q(ew0 ew0) {
        this.h = ew0.j();
        this.i = ew0.k();
        this.j = ew0.l();
        int i2 = this.h;
        this.k = i2;
        int i3 = this.i;
        if (i3 > 1) {
            this.k = i2 * i3;
        }
        int i4 = this.j;
        if (i4 > 1) {
            this.k = i4 * this.k;
        }
    }

    @DexIgnore
    public final void r(Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config != null) {
            int i2 = a.f3474a[config.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            if (this.d.i().f != vv0.b.PIXEL_RGBA || this.d.i().o() != 2) {
                                throw new yv0("Allocation kind is " + this.d.i().f + ", type " + this.d.i().e + " of " + this.d.i().o() + " bytes, passed bitmap was " + config);
                            }
                        }
                    } else if (this.d.i().f != vv0.b.PIXEL_RGB || this.d.i().o() != 2) {
                        throw new yv0("Allocation kind is " + this.d.i().f + ", type " + this.d.i().e + " of " + this.d.i().o() + " bytes, passed bitmap was " + config);
                    }
                } else if (this.d.i().f != vv0.b.PIXEL_RGBA || this.d.i().o() != 4) {
                    throw new yv0("Allocation kind is " + this.d.i().f + ", type " + this.d.i().e + " of " + this.d.i().o() + " bytes, passed bitmap was " + config);
                }
            } else if (this.d.i().f != vv0.b.PIXEL_A) {
                throw new yv0("Allocation kind is " + this.d.i().f + ", type " + this.d.i().e + " of " + this.d.i().o() + " bytes, passed bitmap was " + config);
            }
        } else {
            throw new yv0("Bitmap has an unsupported format for this operation");
        }
    }

    @DexIgnore
    public final void s(Bitmap bitmap) {
        if (this.h != bitmap.getWidth() || this.i != bitmap.getHeight()) {
            throw new yv0("Cannot update allocation from bitmap, sizes mismatch");
        }
    }
}
