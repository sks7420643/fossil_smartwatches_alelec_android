package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv1 extends ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<lv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public lv1 createFromParcel(Parcel parcel) {
            return new lv1(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public lv1[] newArray(int i) {
            return new lv1[i];
        }
    }

    @DexIgnore
    public lv1(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("w", this.b).put("h", this.c);
        pq7.b(put, "JSONObject().put(UIScrip\u2026ScriptConstant.H, height)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof lv1) {
                lv1 lv1 = (lv1) obj;
                if (!(this.b == lv1.b && this.c == lv1.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getHeight() {
        return this.c;
    }

    @DexIgnore
    public final int getWidth() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b * 31) + this.c;
    }

    @DexIgnore
    public final void setHeight(int i) {
        this.c = i;
    }

    @DexIgnore
    public final void setWidth(int i) {
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public final kv1 toScaledSize(int i, int i2) {
        try {
            return new kv1((((float) this.b) * 1.0f) / ((float) i), (((float) this.c) * 1.0f) / ((float) i2));
        } catch (IllegalArgumentException e) {
            return new kv1(1.0f, 1.0f);
        }
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e = e.e("Size(width=");
        e.append(this.b);
        e.append(", height=");
        return e.b(e, this.c, ")");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
    }
}
