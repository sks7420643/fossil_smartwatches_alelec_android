package com.fossil;

import com.fossil.oj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tj0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ak0 f3421a; // = new ak0(this);
    @DexIgnore
    public /* final */ uj0 b;
    @DexIgnore
    public /* final */ d c;
    @DexIgnore
    public tj0 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public c g; // = c.NONE;
    @DexIgnore
    public int h;
    @DexIgnore
    public oj0 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3422a;

        /*
        static {
            int[] iArr = new int[d.values().length];
            f3422a = iArr;
            try {
                iArr[d.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f3422a[d.LEFT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f3422a[d.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f3422a[d.TOP.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f3422a[d.BOTTOM.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f3422a[d.BASELINE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f3422a[d.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f3422a[d.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f3422a[d.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        RELAXED,
        STRICT
    }

    @DexIgnore
    public enum c {
        NONE,
        STRONG,
        WEAK
    }

    @DexIgnore
    public enum d {
        NONE,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        BASELINE,
        CENTER,
        CENTER_X,
        CENTER_Y
    }

    @DexIgnore
    public tj0(uj0 uj0, d dVar) {
        b bVar = b.RELAXED;
        this.h = 0;
        this.b = uj0;
        this.c = dVar;
    }

    @DexIgnore
    public boolean a(tj0 tj0, int i2, int i3, c cVar, int i4, boolean z) {
        if (tj0 == null) {
            this.d = null;
            this.e = 0;
            this.f = -1;
            this.g = c.NONE;
            this.h = 2;
            return true;
        } else if (!z && !l(tj0)) {
            return false;
        } else {
            this.d = tj0;
            if (i2 > 0) {
                this.e = i2;
            } else {
                this.e = 0;
            }
            this.f = i3;
            this.g = cVar;
            this.h = i4;
            return true;
        }
    }

    @DexIgnore
    public boolean b(tj0 tj0, int i2, c cVar, int i3) {
        return a(tj0, i2, -1, cVar, i3, false);
    }

    @DexIgnore
    public int c() {
        return this.h;
    }

    @DexIgnore
    public int d() {
        tj0 tj0;
        if (this.b.C() == 8) {
            return 0;
        }
        return (this.f <= -1 || (tj0 = this.d) == null || tj0.b.C() != 8) ? this.e : this.f;
    }

    @DexIgnore
    public uj0 e() {
        return this.b;
    }

    @DexIgnore
    public ak0 f() {
        return this.f3421a;
    }

    @DexIgnore
    public oj0 g() {
        return this.i;
    }

    @DexIgnore
    public c h() {
        return this.g;
    }

    @DexIgnore
    public tj0 i() {
        return this.d;
    }

    @DexIgnore
    public d j() {
        return this.c;
    }

    @DexIgnore
    public boolean k() {
        return this.d != null;
    }

    @DexIgnore
    public boolean l(tj0 tj0) {
        boolean z;
        if (tj0 == null) {
            return false;
        }
        d j = tj0.j();
        d dVar = this.c;
        if (j == dVar) {
            return dVar != d.BASELINE || (tj0.e().I() && e().I());
        }
        switch (a.f3422a[dVar.ordinal()]) {
            case 1:
                return (j == d.BASELINE || j == d.CENTER_X || j == d.CENTER_Y) ? false : true;
            case 2:
            case 3:
                z = j == d.LEFT || j == d.RIGHT;
                if (tj0.e() instanceof xj0) {
                    return z || j == d.CENTER_X;
                }
                break;
            case 4:
            case 5:
                z = j == d.TOP || j == d.BOTTOM;
                if (tj0.e() instanceof xj0) {
                    return z || j == d.CENTER_Y;
                }
                break;
            case 6:
            case 7:
            case 8:
            case 9:
                return false;
            default:
                throw new AssertionError(this.c.name());
        }
        return z;
    }

    @DexIgnore
    public void m() {
        this.d = null;
        this.e = 0;
        this.f = -1;
        this.g = c.STRONG;
        this.h = 0;
        b bVar = b.RELAXED;
        this.f3421a.e();
    }

    @DexIgnore
    public void n(ij0 ij0) {
        oj0 oj0 = this.i;
        if (oj0 == null) {
            this.i = new oj0(oj0.a.UNRESTRICTED, null);
        } else {
            oj0.d();
        }
    }

    @DexIgnore
    public String toString() {
        return this.b.n() + ":" + this.c.toString();
    }
}
