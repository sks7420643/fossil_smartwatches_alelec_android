package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.t47;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt6 extends pv5 implements eu6, View.OnClickListener, t47.g {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public g37<fa5> g;
    @DexIgnore
    public du6 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public GradientDrawable t; // = new GradientDrawable();
    @DexIgnore
    public GradientDrawable u; // = new GradientDrawable();
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return wt6.w;
        }

        @DexIgnore
        public final wt6 b() {
            return new wt6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public b(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public c(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).o(ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public d(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).o(ai5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public e(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).q(ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public f(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).q(ai5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public g(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).n(ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public h(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).n(ai5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public i(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).p(ai5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wt6 b;

        @DexIgnore
        public j(wt6 wt6) {
            this.b = wt6;
        }

        @DexIgnore
        public final void onClick(View view) {
            wt6.K6(this.b).p(ai5.IMPERIAL);
        }
    }

    /*
    static {
        String simpleName = wt6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "PreferredUnitFragment::class.java.simpleName!!");
            w = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ du6 K6(wt6 wt6) {
        du6 du6 = wt6.h;
        if (du6 != null) {
            return du6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.eu6
    public void A2(ai5 ai5) {
        g37<fa5> g37 = this.g;
        if (g37 != null) {
            fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.J.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = xt6.f4175a[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.J.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            pq7.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            pq7.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.J.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            pq7.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            pq7.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.J.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    pq7.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    pq7.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.eu6
    public void F3(ai5 ai5) {
        g37<fa5> g37 = this.g;
        if (g37 != null) {
            fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.I.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = xt6.c[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.I.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            pq7.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            pq7.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.I.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            pq7.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            pq7.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.I.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    pq7.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    pq7.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(du6 du6) {
        pq7.c(du6, "presenter");
        i14.l(du6);
        pq7.b(du6, "checkNotNull(presenter)");
        this.h = du6;
    }

    @DexIgnore
    @Override // com.fossil.eu6
    public void R2(ai5 ai5) {
        g37<fa5> g37 = this.g;
        if (g37 != null) {
            fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.L.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = xt6.b[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.L.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            pq7.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            pq7.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.L.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            pq7.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            pq7.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.L.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    pq7.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    pq7.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0) && getActivity() != null && str.hashCode() == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).y();
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.eu6
    public void m1(ai5 ai5) {
        g37<fa5> g37 = this.g;
        if (g37 != null) {
            fa5 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.K.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ai5 != null) {
                        int i2 = xt6.d[ai5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g v2 = a2.K.v(1);
                            if (v2 != null) {
                                v2.k();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            pq7.b(childAt2, "selectedTab");
                            childAt2.setBackground(this.t);
                            View childAt3 = linearLayout.getChildAt(0);
                            pq7.b(childAt3, "unSelectedTab");
                            childAt3.setBackground(this.u);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g v3 = a2.K.v(0);
                            if (v3 != null) {
                                v3.k();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            pq7.b(childAt4, "selectedTab");
                            childAt4.setBackground(this.t);
                            View childAt5 = linearLayout.getChildAt(1);
                            pq7.b(childAt5, "unSelectedTab");
                            childAt5.setBackground(this.u);
                            return;
                        }
                    }
                    TabLayout.g v4 = a2.K.v(0);
                    if (v4 != null) {
                        v4.k();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    pq7.b(childAt6, "selectedTab");
                    childAt6.setBackground(this.t);
                    View childAt7 = linearLayout.getChildAt(1);
                    pq7.b(childAt7, "unSelectedTab");
                    childAt7.setBackground(this.u);
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.eu6
    public void o(int i2, String str) {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        pq7.c(view, "v");
        if (view.getId() == 2131361851) {
            e0();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        gl0.d(requireContext(), 2131100360);
        gl0.d(requireContext(), 2131099829);
        this.i = Color.parseColor(qn5.l.a().d("primaryButton"));
        this.j = Color.parseColor(qn5.l.a().d("onPrimaryButton"));
        this.k = Color.parseColor(qn5.l.a().d("primaryButtonBorder"));
        this.l = Color.parseColor(qn5.l.a().d("secondaryButton"));
        this.m = Color.parseColor(qn5.l.a().d("onSecondaryButton"));
        this.s = Color.parseColor(qn5.l.a().d("secondaryButtonBorder"));
        fa5 fa5 = (fa5) aq0.f(LayoutInflater.from(getContext()), 2131558607, null, false, A6());
        fa5.q.setOnClickListener(new b(this));
        View childAt = fa5.J.getChildAt(0);
        if (childAt != null) {
            View childAt2 = ((LinearLayout) childAt).getChildAt(0);
            if (childAt2 != null) {
                childAt2.setOnClickListener(new c(this));
            }
            View childAt3 = fa5.J.getChildAt(0);
            if (childAt3 != null) {
                View childAt4 = ((LinearLayout) childAt3).getChildAt(1);
                if (childAt4 != null) {
                    childAt4.setOnClickListener(new d(this));
                }
                View childAt5 = fa5.L.getChildAt(0);
                if (childAt5 != null) {
                    View childAt6 = ((LinearLayout) childAt5).getChildAt(0);
                    if (childAt6 != null) {
                        childAt6.setOnClickListener(new e(this));
                    }
                    View childAt7 = fa5.L.getChildAt(0);
                    if (childAt7 != null) {
                        View childAt8 = ((LinearLayout) childAt7).getChildAt(1);
                        if (childAt8 != null) {
                            childAt8.setOnClickListener(new f(this));
                        }
                        View childAt9 = fa5.I.getChildAt(0);
                        if (childAt9 != null) {
                            View childAt10 = ((LinearLayout) childAt9).getChildAt(0);
                            if (childAt10 != null) {
                                childAt10.setOnClickListener(new g(this));
                            }
                            View childAt11 = fa5.I.getChildAt(0);
                            if (childAt11 != null) {
                                View childAt12 = ((LinearLayout) childAt11).getChildAt(1);
                                if (childAt12 != null) {
                                    childAt12.setOnClickListener(new h(this));
                                }
                                View childAt13 = fa5.K.getChildAt(0);
                                if (childAt13 != null) {
                                    View childAt14 = ((LinearLayout) childAt13).getChildAt(0);
                                    if (childAt14 != null) {
                                        childAt14.setOnClickListener(new i(this));
                                    }
                                    View childAt15 = fa5.K.getChildAt(0);
                                    if (childAt15 != null) {
                                        View childAt16 = ((LinearLayout) childAt15).getChildAt(1);
                                        if (childAt16 != null) {
                                            childAt16.setOnClickListener(new j(this));
                                        }
                                        fa5.I.H(this.m, this.j);
                                        fa5.K.H(this.m, this.j);
                                        fa5.J.H(this.m, this.j);
                                        fa5.L.H(this.m, this.j);
                                        this.g = new g37<>(this, fa5);
                                        this.t.setShape(0);
                                        this.t.setColor(this.i);
                                        this.t.setStroke(4, this.k);
                                        this.u.setShape(0);
                                        this.u.setColor(this.l);
                                        this.u.setStroke(4, this.s);
                                        pq7.b(fa5, "binding");
                                        return fa5.n();
                                    }
                                    throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
                                }
                                throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
                            }
                            throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
                        }
                        throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
                    }
                    throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
                }
                throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
        }
        throw new il7("null cannot be cast to non-null type android.widget.LinearLayout");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        du6 du6 = this.h;
        if (du6 != null) {
            du6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        du6 du6 = this.h;
        if (du6 != null) {
            du6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
