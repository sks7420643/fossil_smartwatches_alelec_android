package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t7 extends qq7 implements rp7<u5, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ k5 b;
    @DexIgnore
    public /* final */ /* synthetic */ u5 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t7(k5 k5Var, u5 u5Var) {
        super(1);
        this.b = k5Var;
        this.c = u5Var;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(u5 u5Var) {
        k5 k5Var = this.b;
        u5 u5Var2 = this.c;
        Iterator<bs> it = k5Var.i.iterator();
        while (it.hasNext()) {
            try {
                k5Var.b.post(new w7(it.next(), u5Var2));
            } catch (Exception e) {
                d90.i.i(e);
            }
        }
        return tl7.f3441a;
    }
}
