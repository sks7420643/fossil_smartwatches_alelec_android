package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv extends fv {
    @DexIgnore
    public byte[] X;
    @DexIgnore
    public long Y;
    @DexIgnore
    public long Z;
    @DexIgnore
    public /* final */ long a0;
    @DexIgnore
    public /* final */ long b0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ gv(short s, long j, long j2, k5 k5Var, int i, int i2) {
        super(iu.c, s, hs.w, k5Var, (i2 & 16) != 0 ? 3 : i);
        this.a0 = j;
        this.b0 = j2;
        this.X = new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a0).putInt((int) this.b0).array();
        pq7.b(array, "ByteBuffer.allocate(8).o\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fv
    public void Q(long j) {
        this.Y = j;
    }

    @DexIgnore
    @Override // com.fossil.fv
    public void R(byte[] bArr) {
        this.X = bArr;
    }

    @DexIgnore
    @Override // com.fossil.fv
    public void S(long j) {
        this.Z = j;
    }

    @DexIgnore
    @Override // com.fossil.fv
    public long T() {
        return this.Y;
    }

    @DexIgnore
    @Override // com.fossil.fv
    public long U() {
        return this.Z;
    }

    @DexIgnore
    @Override // com.fossil.fv
    public byte[] V() {
        return this.X;
    }

    @DexIgnore
    @Override // com.fossil.dv, com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.c1, Long.valueOf(this.a0)), jd0.d1, Long.valueOf(this.b0));
    }
}
