package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ir7 {
    @DexIgnore
    public static Iterable a(Object obj) {
        if (!(obj instanceof jr7)) {
            return e(obj);
        }
        l(obj, "kotlin.collections.MutableIterable");
        throw null;
    }

    @DexIgnore
    public static List b(Object obj) {
        if (!(obj instanceof jr7)) {
            return f(obj);
        }
        l(obj, "kotlin.collections.MutableList");
        throw null;
    }

    @DexIgnore
    public static Map c(Object obj) {
        if (!(obj instanceof jr7)) {
            return g(obj);
        }
        l(obj, "kotlin.collections.MutableMap");
        throw null;
    }

    @DexIgnore
    public static Object d(Object obj, int i) {
        if (obj == null || i(obj, i)) {
            return obj;
        }
        l(obj, "kotlin.jvm.functions.Function" + i);
        throw null;
    }

    @DexIgnore
    public static Iterable e(Object obj) {
        try {
            return (Iterable) obj;
        } catch (ClassCastException e) {
            k(e);
            throw null;
        }
    }

    @DexIgnore
    public static List f(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            k(e);
            throw null;
        }
    }

    @DexIgnore
    public static Map g(Object obj) {
        try {
            return (Map) obj;
        } catch (ClassCastException e) {
            k(e);
            throw null;
        }
    }

    @DexIgnore
    public static int h(Object obj) {
        if (obj instanceof mq7) {
            return ((mq7) obj).getArity();
        }
        if (obj instanceof gp7) {
            return 0;
        }
        if (obj instanceof rp7) {
            return 1;
        }
        if (obj instanceof vp7) {
            return 2;
        }
        if (obj instanceof wp7) {
            return 3;
        }
        if (obj instanceof xp7) {
            return 4;
        }
        if (obj instanceof yp7) {
            return 5;
        }
        if (obj instanceof zp7) {
            return 6;
        }
        if (obj instanceof aq7) {
            return 7;
        }
        if (obj instanceof bq7) {
            return 8;
        }
        if (obj instanceof cq7) {
            return 9;
        }
        if (obj instanceof hp7) {
            return 10;
        }
        if (obj instanceof ip7) {
            return 11;
        }
        if (obj instanceof jp7) {
            return 12;
        }
        if (obj instanceof kp7) {
            return 13;
        }
        if (obj instanceof lp7) {
            return 14;
        }
        if (obj instanceof mp7) {
            return 15;
        }
        if (obj instanceof np7) {
            return 16;
        }
        if (obj instanceof op7) {
            return 17;
        }
        if (obj instanceof pp7) {
            return 18;
        }
        if (obj instanceof qp7) {
            return 19;
        }
        if (obj instanceof sp7) {
            return 20;
        }
        if (obj instanceof tp7) {
            return 21;
        }
        return obj instanceof up7 ? 22 : -1;
    }

    @DexIgnore
    public static boolean i(Object obj, int i) {
        return (obj instanceof uk7) && h(obj) == i;
    }

    @DexIgnore
    public static <T extends Throwable> T j(T t) {
        pq7.g(t, ir7.class.getName());
        return t;
    }

    @DexIgnore
    public static ClassCastException k(ClassCastException classCastException) {
        j(classCastException);
        throw classCastException;
    }

    @DexIgnore
    public static void l(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        m(name + " cannot be cast to " + str);
        throw null;
    }

    @DexIgnore
    public static void m(String str) {
        k(new ClassCastException(str));
        throw null;
    }
}
