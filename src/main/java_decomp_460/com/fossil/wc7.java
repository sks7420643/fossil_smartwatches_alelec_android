package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.fossil.rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wc7 extends rd7 {
    @DexIgnore
    public static /* final */ int b; // = 22;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AssetManager f3918a;

    @DexIgnore
    public wc7(Context context) {
        this.f3918a = context.getAssets();
    }

    @DexIgnore
    public static String j(pd7 pd7) {
        return pd7.d.toString().substring(b);
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public boolean c(pd7 pd7) {
        Uri uri = pd7.d;
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }

    @DexIgnore
    @Override // com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        return new rd7.a(this.f3918a.open(j(pd7)), Picasso.LoadedFrom.DISK);
    }
}
