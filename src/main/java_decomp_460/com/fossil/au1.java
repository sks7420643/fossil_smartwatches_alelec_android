package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<au1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public au1 createFromParcel(Parcel parcel) {
            return new au1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public au1[] newArray(int i) {
            return new au1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ au1(android.os.Parcel r4, com.fossil.kq7 r5) {
        /*
            r3 = this;
            r2 = 0
            java.lang.Class<com.fossil.tq1> r0 = com.fossil.tq1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r4.readParcelable(r0)
            if (r0 == 0) goto L_0x002a
            java.lang.String r1 = "parcel.readParcelable<Wo\u2026class.java.classLoader)!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.tq1 r0 = (com.fossil.tq1) r0
            java.lang.Class<com.fossil.nt1> r1 = com.fossil.nt1.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r4.readParcelable(r1)
            if (r1 == 0) goto L_0x0026
            com.fossil.nt1 r1 = (com.fossil.nt1) r1
            r3.<init>(r0, r1)
            return
        L_0x0026:
            com.fossil.pq7.i()
            throw r2
        L_0x002a:
            com.fossil.pq7.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.au1.<init>(android.os.Parcel, com.fossil.kq7):void");
    }

    @DexIgnore
    public au1(tq1 tq1, nt1 nt1) {
        super(tq1, nt1);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            nt1 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.toJSONObject());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    d90.i.i(e);
                }
                String jSONObject4 = jSONObject2.toString();
                pq7.b(jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = hd0.y.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            pq7.i();
            throw null;
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
    }
}
