package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge.BCChallengeInfoWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge.BCListChallengeWatchAppInfo;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr5 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public iv7 f825a;
    @DexIgnore
    public u08 b; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ tt4 d;
    @DexIgnore
    public /* final */ zt4 e;
    @DexIgnore
    public /* final */ on5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return dr5.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$getChallengeInfo$1", f = "BuddyChallengeManager.kt", l = {192}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dr5 dr5, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$challengeId = str;
            this.$serial = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$challengeId, this.$serial, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Long f;
            Long f2;
            Long f3;
            Long f4;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.d;
                List<String> i2 = hm7.i(this.$challengeId);
                this.L$0 = iv7;
                this.label = 1;
                p = tt4.p(i2, 1, 3, false, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) ((kz4) p).c();
            if (list != null && (!list.isEmpty())) {
                ks4 ks4 = (ks4) pm7.F(list);
                ArrayList arrayList = new ArrayList();
                List<ms4> d2 = ks4.d();
                List<ms4> j0 = d2 != null ? pm7.j0(d2) : null;
                List<ms4> b = ks4.b();
                if (!(b == null || j0 == null)) {
                    ao7.a(j0.addAll(b));
                }
                if (j0 != null) {
                    for (ms4 ms4 : j0) {
                        Integer n = ms4.n();
                        long j = 0;
                        long longValue = (n == null || (f4 = ao7.f((long) n.intValue())) == null) ? 0 : f4.longValue();
                        Integer m = ms4.m();
                        long longValue2 = (m == null || (f3 = ao7.f((long) m.intValue())) == null) ? 0 : f3.longValue();
                        Integer b2 = ms4.b();
                        long longValue3 = (b2 == null || (f2 = ao7.f((long) b2.intValue())) == null) ? 0 : f2.longValue();
                        Integer a2 = ms4.a();
                        if (!(a2 == null || (f = ao7.f((long) a2.intValue())) == null)) {
                            j = f.longValue();
                        }
                        String c = pq7.a(PortfolioApp.h0.c().l0(), ms4.d()) ? um5.c(PortfolioApp.h0.c(), 2131886250) : hz4.f1561a.a(ms4.c(), ms4.e(), ms4.i());
                        pq7.b(c, "name");
                        Integer h = ms4.h();
                        arrayList.add(new zs1(c, h != null ? h.intValue() : -1, new ys1(longValue - longValue2, longValue3 - j)));
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = dr5.h.a();
                local.e(a3, "getChallengeInfo - watchPlayers: " + arrayList);
                String a4 = ks4.a();
                Object[] array = arrayList.toArray(new zs1[0]);
                if (array != null) {
                    PortfolioApp.h0.c().g1(new BCChallengeInfoWatchAppInfo(new ws1(a4, "BC", (zs1[]) array)), this.$serial);
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$getListChallenge$1", f = "BuddyChallengeManager.kt", l = {133}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(dr5 dr5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$serial, qn7);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object w;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.d;
                this.L$0 = iv7;
                this.label = 1;
                w = tt4.w(this);
                if (w == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                w = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<ps4> list = (List) w;
            if (list != null) {
                ArrayList arrayList = new ArrayList(im7.m(list, 10));
                for (ps4 ps4 : list) {
                    String f = ps4.f();
                    String g = ps4.g();
                    if (g == null) {
                        g = "";
                    }
                    arrayList.add(new xs1(f, g, ""));
                }
                PortfolioApp.h0.c().g1(new BCListChallengeWatchAppInfo(arrayList), this.$serial);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onChallengeCompleted$1", f = "BuddyChallengeManager.kt", l = {66, 71, 72, 74}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(dr5 dr5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$challengeId, qn7);
            dVar.p$ = (iv7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0086  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a1  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0110  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 276
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onChallengeResponse$1", f = "BuddyChallengeManager.kt", l = {177}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(dr5 dr5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$challengeId, qn7);
            eVar.p$ = (iv7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object t;
            ps4 a2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.d;
                String str = this.$challengeId;
                this.L$0 = iv7;
                this.label = 1;
                t = tt4.t(str, new String[]{"waiting", "running"}, this);
                if (t == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                t = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) t;
            if (!(kz4.c() == null || (a2 = this.this$0.d.a(this.$challengeId)) == null)) {
                a2.u(ao7.e(((List) kz4.c()).size()));
                this.this$0.d.B(a2);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onChallengeStarted$1", f = "BuddyChallengeManager.kt", l = {48, 55}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(dr5 dr5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$challengeId, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0063  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                r2 = 3
                r11 = 2
                r4 = 0
                r10 = 1
                java.lang.Object r9 = com.fossil.yn7.d()
                int r0 = r12.label
                if (r0 == 0) goto L_0x009e
                if (r0 == r10) goto L_0x0026
                if (r0 != r11) goto L_0x001e
                java.lang.Object r0 = r12.L$1
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r12.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r13)
            L_0x001b:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001d:
                return r0
            L_0x001e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0026:
                java.lang.Object r0 = r12.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r13)
            L_0x002d:
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                boolean r1 = r1.L()
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                java.lang.String r2 = r2.J()
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.dr5$a r5 = com.fossil.dr5.h
                java.lang.String r5 = r5.a()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "onChallengeStarted - consider retry sync data for buddy challenge - activeStatus: "
                r6.append(r7)
                r6.append(r1)
                java.lang.String r6 = r6.toString()
                r3.e(r5, r6)
                if (r1 == 0) goto L_0x001b
                int r3 = r2.length()
                if (r3 <= 0) goto L_0x006a
                r4 = r10
            L_0x006a:
                if (r4 == 0) goto L_0x001b
                com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r3 = r3.c()
                boolean r3 = r3.A0(r2)
                if (r3 != 0) goto L_0x001b
                com.fossil.dr5 r3 = r12.this$0
                com.fossil.on5 r3 = com.fossil.dr5.d(r3)
                java.lang.Long r3 = r3.d()
                long r4 = r3.longValue()
                r6 = 0
                int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r3 <= 0) goto L_0x001b
                com.fossil.dr5 r3 = r12.this$0
                r12.L$0 = r0
                r12.Z$0 = r1
                r12.L$1 = r2
                r12.label = r11
                java.lang.Object r0 = r3.u(r12)
                if (r0 != r9) goto L_0x001b
                r0 = r9
                goto L_0x001d
            L_0x009e:
                com.fossil.el7.b(r13)
                com.fossil.iv7 r8 = r12.p$
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.dr5$a r1 = com.fossil.dr5.h
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = "onChallengeStarted - challengeId: "
                r3.append(r5)
                java.lang.String r5 = r12.$challengeId
                r3.append(r5)
                java.lang.String r3 = r3.toString()
                r0.e(r1, r3)
                com.fossil.dr5 r0 = r12.this$0
                com.fossil.tt4 r0 = com.fossil.dr5.c(r0)
                java.lang.String[] r1 = new java.lang.String[r10]
                java.lang.String r3 = r12.$challengeId
                r1[r4] = r3
                java.util.List r1 = com.fossil.hm7.i(r1)
                r12.L$0 = r8
                r12.label = r10
                r6 = 8
                r7 = 0
                r3 = r2
                r5 = r12
                java.lang.Object r0 = com.fossil.tt4.q(r0, r1, r2, r3, r4, r5, r6, r7)
                if (r0 != r9) goto L_0x00e7
                r0 = r9
                goto L_0x001d
            L_0x00e7:
                r0 = r8
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ws4 $this_run;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ws4 ws4, qn7 qn7, dr5 dr5) {
            super(2, qn7);
            this.$this_run = ws4;
            this.this$0 = dr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.$this_run, qn7, this.this$0);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                zt4 zt4 = this.this$0.e;
                lt4 d = this.$this_run.d();
                String c = d != null ? d.c() : null;
                if (c != null) {
                    xs4 j = zt4.j(c);
                    if (j != null && j.c() == 1) {
                        j.k(0);
                        long n = this.this$0.e.n(j);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = dr5.h.a();
                        local.e(a2, "onFriendResponse - friend: " + j + " - rowid: " + n);
                    }
                    return tl7.f3441a;
                }
                pq7.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onReachGoal$1", f = "BuddyChallengeManager.kt", l = {82, 83}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(dr5 dr5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$challengeId, qn7);
            hVar.p$ = (iv7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0055  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                r7 = 0
                r2 = 3
                r4 = 0
                r11 = 2
                r10 = 1
                java.lang.Object r9 = com.fossil.yn7.d()
                int r0 = r12.label
                if (r0 == 0) goto L_0x0057
                if (r0 == r10) goto L_0x0034
                if (r0 != r11) goto L_0x002c
                java.lang.Object r0 = r12.L$1
                com.fossil.kz4 r0 = (com.fossil.kz4) r0
                java.lang.Object r1 = r12.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r13)
            L_0x001c:
                com.fossil.dr5 r1 = r12.this$0
                java.lang.Object r0 = r0.c()
                java.util.List r0 = (java.util.List) r0
                java.lang.String r2 = r12.$challengeId
                com.fossil.dr5.a(r1, r0, r2, r10)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002b:
                return r0
            L_0x002c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0034:
                java.lang.Object r0 = r12.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r13)
                r3 = r0
                r1 = r13
            L_0x003d:
                r0 = r1
                com.fossil.kz4 r0 = (com.fossil.kz4) r0
                com.fossil.dr5 r1 = r12.this$0
                com.fossil.tt4 r2 = com.fossil.dr5.c(r1)
                r12.L$0 = r3
                r12.L$1 = r0
                r12.label = r11
                r3 = 5
                r5 = r12
                r6 = r11
                java.lang.Object r1 = com.fossil.tt4.s(r2, r3, r4, r5, r6, r7)
                if (r1 != r9) goto L_0x001c
                r0 = r9
                goto L_0x002b
            L_0x0057:
                com.fossil.el7.b(r13)
                com.fossil.iv7 r8 = r12.p$
                com.fossil.dr5 r0 = r12.this$0
                com.fossil.tt4 r0 = com.fossil.dr5.c(r0)
                java.lang.String r1 = r12.$challengeId
                java.util.List r1 = com.fossil.gm7.b(r1)
                r12.L$0 = r8
                r12.label = r10
                r6 = 8
                r3 = r2
                r5 = r12
                java.lang.Object r1 = com.fossil.tt4.q(r0, r1, r2, r3, r4, r5, r6, r7)
                if (r1 != r9) goto L_0x0078
                r0 = r9
                goto L_0x002b
            L_0x0078:
                r3 = r8
                goto L_0x003d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ lt4 $this_run;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(lt4 lt4, qn7 qn7, dr5 dr5) {
            super(2, qn7);
            this.$this_run = lt4;
            this.this$0 = dr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.$this_run, qn7, this.this$0);
            iVar.p$ = (iv7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                String c = this.$this_run.c();
                String e = this.$this_run.e();
                if (e == null) {
                    e = "";
                }
                String b = this.$this_run.b();
                if (b == null) {
                    b = "";
                }
                String d = this.$this_run.d();
                if (d == null) {
                    d = "";
                }
                String a2 = this.$this_run.a();
                if (a2 == null) {
                    a2 = "";
                }
                xs4 xs4 = new xs4(c, e, b, d, null, a2, false, 0, 2);
                long n = this.this$0.e.n(xs4);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = dr5.h.a();
                local.e(a3, "onReceivedRequest - friend: " + xs4 + " - rowId: " + n);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$processEncryptedData$1", f = "BuddyChallengeManager.kt", l = {125}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedData $encryptedData;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(dr5 dr5, EncryptedData encryptedData, qn7 qn7) {
            super(2, qn7);
            this.this$0 = dr5;
            this.$encryptedData = encryptedData;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, this.$encryptedData, qn7);
            jVar.p$ = (iv7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object F;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.$encryptedData != null) {
                    tt4 tt4 = this.this$0.d;
                    EncryptedData encryptedData = this.$encryptedData;
                    this.L$0 = iv7;
                    this.label = 1;
                    F = tt4.F(encryptedData, this);
                    if (F == d) {
                        return d;
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                F = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = dr5.h.a();
            local.e(a2, "processEncryptedData - result - " + ((kz4) F));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager", f = "BuddyChallengeManager.kt", l = {304, 234, 240}, m = "requestSyncData")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(dr5 dr5, qn7 qn7) {
            super(qn7);
            this.this$0 = dr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager", f = "BuddyChallengeManager.kt", l = {252}, m = "setSyncData")
    public static final class l extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ dr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(dr5 dr5, qn7 qn7) {
            super(qn7);
            this.this$0 = dr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    /*
    static {
        String simpleName = dr5.class.getSimpleName();
        pq7.b(simpleName, "BuddyChallengeManager::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public dr5(PortfolioApp portfolioApp, tt4 tt4, zt4 zt4, on5 on5) {
        pq7.c(portfolioApp, "mPortfolioApp");
        pq7.c(tt4, "mChallengeRepository");
        pq7.c(zt4, "friendRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = tt4;
        this.e = zt4;
        this.f = on5;
    }

    @DexIgnore
    public static /* synthetic */ Object t(dr5 dr5, String str, boolean z, qn7 qn7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        return dr5.s(str, z, qn7);
    }

    @DexIgnore
    public final void f() {
        iv7 iv7 = this.f825a;
        if (iv7 != null) {
            jv7.d(iv7, null, 1, null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g(java.util.List<com.fossil.ks4> r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            r2 = 0
            r5 = 0
            r6 = 1
            if (r9 == 0) goto L_0x000b
            boolean r0 = r9.isEmpty()
            if (r0 == 0) goto L_0x0075
        L_0x000b:
            r0 = r6
        L_0x000c:
            if (r0 != 0) goto L_0x0074
            java.lang.Object r0 = com.fossil.pm7.F(r9)
            com.fossil.ks4 r0 = (com.fossil.ks4) r0
            java.util.List r1 = r0.d()
            if (r1 == 0) goto L_0x0077
            java.util.List r1 = com.fossil.pm7.j0(r1)
        L_0x001e:
            java.util.List r3 = r0.b()
            if (r1 == 0) goto L_0x007b
            java.util.Iterator r4 = r1.iterator()
        L_0x0028:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r1 = r4.next()
            r0 = r1
            com.fossil.ms4 r0 = (com.fossil.ms4) r0
            java.lang.String r0 = r0.d()
            com.portfolio.platform.PortfolioApp r7 = r8.c
            java.lang.String r7 = r7.l0()
            boolean r0 = com.fossil.pq7.a(r0, r7)
            if (r0 == 0) goto L_0x0028
            r0 = r1
        L_0x0046:
            com.fossil.ms4 r0 = (com.fossil.ms4) r0
            if (r0 == 0) goto L_0x007b
            r2 = r0
        L_0x004b:
            if (r2 == 0) goto L_0x00ac
            java.lang.Integer r0 = r2.h()
            if (r0 == 0) goto L_0x00a3
            int r0 = r0.intValue()
        L_0x0057:
            java.lang.Integer r1 = r2.n()
            if (r1 == 0) goto L_0x00a5
            int r1 = r1.intValue()
        L_0x0061:
            if (r11 == 0) goto L_0x00a7
        L_0x0063:
            r4 = r1
            r3 = r0
            r5 = r6
        L_0x0066:
            com.fossil.xr4 r0 = com.fossil.xr4.f4164a
            com.portfolio.platform.PortfolioApp r1 = r8.c
            java.lang.String r2 = r1.l0()
            com.portfolio.platform.PortfolioApp r6 = r8.c
            r1 = r10
            r0.c(r1, r2, r3, r4, r5, r6)
        L_0x0074:
            return
        L_0x0075:
            r0 = r5
            goto L_0x000c
        L_0x0077:
            r1 = r2
            goto L_0x001e
        L_0x0079:
            r0 = r2
            goto L_0x0046
        L_0x007b:
            if (r3 == 0) goto L_0x004b
            java.util.Iterator r3 = r3.iterator()
        L_0x0081:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00af
            java.lang.Object r1 = r3.next()
            r0 = r1
            com.fossil.ms4 r0 = (com.fossil.ms4) r0
            java.lang.String r0 = r0.d()
            com.portfolio.platform.PortfolioApp r4 = r8.c
            java.lang.String r4 = r4.l0()
            boolean r0 = com.fossil.pq7.a(r0, r4)
            if (r0 == 0) goto L_0x0081
            r0 = r1
        L_0x009f:
            com.fossil.ms4 r0 = (com.fossil.ms4) r0
            r2 = r0
            goto L_0x004b
        L_0x00a3:
            r0 = r5
            goto L_0x0057
        L_0x00a5:
            r1 = r5
            goto L_0x0061
        L_0x00a7:
            if (r0 == r6) goto L_0x0063
        L_0x00a9:
            r4 = r1
            r3 = r0
            goto L_0x0066
        L_0x00ac:
            r1 = r6
            r0 = r6
            goto L_0x00a9
        L_0x00af:
            r0 = r2
            goto L_0x009f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.g(java.util.List, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003d, code lost:
        if (com.fossil.jv7.g(r0) == false) goto L_0x003f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.iv7 h() {
        /*
            r5 = this;
            r1 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r0.getLocal()
            java.lang.String r3 = com.fossil.dr5.g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = "coroutine - "
            r4.append(r0)
            com.fossil.iv7 r0 = r5.f825a
            r4.append(r0)
            java.lang.String r0 = " - isActive: "
            r4.append(r0)
            com.fossil.iv7 r0 = r5.f825a
            if (r0 == 0) goto L_0x0057
            boolean r0 = com.fossil.jv7.g(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
        L_0x0029:
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.e(r3, r0)
            com.fossil.iv7 r0 = r5.f825a
            if (r0 == 0) goto L_0x003f
            if (r0 == 0) goto L_0x0059
            boolean r0 = com.fossil.jv7.g(r0)
            if (r0 != 0) goto L_0x0052
        L_0x003f:
            com.fossil.jx7 r0 = com.fossil.bw7.c()
            com.fossil.iv7 r0 = com.fossil.jv7.a(r0)
            r2 = 1
            com.fossil.uu7 r2 = com.fossil.ux7.b(r1, r2, r1)
            com.fossil.iv7 r0 = com.fossil.jv7.h(r0, r2)
            r5.f825a = r0
        L_0x0052:
            com.fossil.iv7 r0 = r5.f825a
            if (r0 == 0) goto L_0x005d
            return r0
        L_0x0057:
            r0 = r1
            goto L_0x0029
        L_0x0059:
            com.fossil.pq7.i()
            throw r1
        L_0x005d:
            com.fossil.pq7.i()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.h():com.fossil.iv7");
    }

    @DexIgnore
    public final void i(String str, String str2) {
        pq7.c(str, "serial");
        pq7.c(str2, "challengeId");
        xw7 unused = gu7.d(h(), bw7.b(), null, new b(this, str2, str, null), 2, null);
    }

    @DexIgnore
    public final void j(String str) {
        pq7.c(str, "serial");
        xw7 unused = gu7.d(h(), bw7.b(), null, new c(this, str, null), 2, null);
    }

    @DexIgnore
    public final void k(ws4 ws4) {
        ss4 b2;
        ss4 b3;
        String a2;
        ss4 b4;
        String a3;
        ss4 b5;
        String a4;
        pq7.c(ws4, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.e(str, "handleNewNotification() - notification = " + ws4);
        String e2 = ws4.e();
        switch (e2.hashCode()) {
            case -1559094286:
                if (e2.equals("Title_Notification_End_Challenge") && (b2 = ws4.b()) != null) {
                    l(b2.a(), b2.d());
                    return;
                }
                return;
            case -764698119:
                if (e2.equals("Title_Notification_Start_Challenge") && (b3 = ws4.b()) != null && (a2 = b3.a()) != null) {
                    n(a2);
                    return;
                }
                return;
            case -473527954:
                if (e2.equals("Title_Respond_Invitation_Challenge") && (b4 = ws4.b()) != null && (a3 = b4.a()) != null) {
                    m(a3);
                    return;
                }
                return;
            case 238453777:
                if (e2.equals("Title_Notification_Reached_Goal_Challenge") && (b5 = ws4.b()) != null && (a4 = b5.a()) != null) {
                    p(a4);
                    return;
                }
                return;
            case 634854495:
                if (e2.equals("Title_Accepted_Friend_Request")) {
                    o(ws4);
                    return;
                }
                return;
            case 1433363166:
                if (e2.equals("Title_Send_Friend_Request")) {
                    q(ws4);
                    return;
                }
                return;
            case 1669546642:
                e2.equals("Title_Notification_Remind_End_Challenge");
                return;
            case 1898363949:
                e2.equals("Title_Send_Invitation_Challenge");
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void l(String str, String str2) {
        xw7 unused = gu7.d(h(), bw7.b(), null, new d(this, str, null), 2, null);
    }

    @DexIgnore
    public final void m(String str) {
        xw7 unused = gu7.d(h(), bw7.b(), null, new e(this, str, null), 2, null);
    }

    @DexIgnore
    public final void n(String str) {
        xw7 unused = gu7.d(h(), bw7.b(), null, new f(this, str, null), 2, null);
    }

    @DexIgnore
    public final void o(ws4 ws4) {
        lt4 d2 = ws4.d();
        if ((d2 != null ? d2.c() : null) != null) {
            xw7 unused = gu7.d(h(), bw7.b(), null, new g(ws4, null, this), 2, null);
        }
    }

    @DexIgnore
    public final void p(String str) {
        xw7 unused = gu7.d(h(), bw7.b(), null, new h(this, str, null), 2, null);
    }

    @DexIgnore
    public final void q(ws4 ws4) {
        lt4 d2 = ws4.d();
        if (d2 != null) {
            if (d2.c().length() > 0) {
                xw7 unused = gu7.d(h(), bw7.b(), null, new i(d2, null, this), 2, null);
            }
        }
    }

    @DexIgnore
    public final void r(String str, EncryptedData encryptedData) {
        pq7.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local.e(str2, "processEncryptedData - serial: " + str + " - encryptedData: " + encryptedData);
        xw7 unused = gu7.d(h(), bw7.b(), null, new j(this, encryptedData, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b0 A[Catch:{ all -> 0x015a }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(java.lang.String r13, boolean r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 354
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.s(java.lang.String, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.dr5.l
            if (r0 == 0) goto L_0x003e
            r0 = r7
            com.fossil.dr5$l r0 = (com.fossil.dr5.l) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003e
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x004c
            if (r3 != r5) goto L_0x0044
            java.lang.Object r0 = r0.L$0
            com.fossil.dr5 r0 = (com.fossil.dr5) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            java.lang.Object r0 = r0.c()
            com.fossil.pt4 r0 = (com.fossil.pt4) r0
            if (r0 == 0) goto L_0x003b
            com.portfolio.platform.PortfolioApp r1 = r6.c
            java.lang.String r0 = r0.a()
            r1.m1(r0)
        L_0x003b:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x003d:
            return r0
        L_0x003e:
            com.fossil.dr5$l r0 = new com.fossil.dr5$l
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.el7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.fossil.dr5.g
            java.lang.String r4 = "setSyncData"
            r1.e(r3, r4)
            com.fossil.tt4 r1 = r6.d
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = r1.v(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dr5.u(com.fossil.qn7):java.lang.Object");
    }
}
