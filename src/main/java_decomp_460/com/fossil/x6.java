package com.fossil;

import com.facebook.internal.NativeProtocol;
import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class x6 extends Enum<x6> {
    @DexIgnore
    public static /* final */ x6 c;
    @DexIgnore
    public static /* final */ x6 d;
    @DexIgnore
    public static /* final */ x6 e;
    @DexIgnore
    public static /* final */ x6 f;
    @DexIgnore
    public static /* final */ x6 g;
    @DexIgnore
    public static /* final */ x6 h;
    @DexIgnore
    public static /* final */ x6 i;
    @DexIgnore
    public static /* final */ x6 j;
    @DexIgnore
    public static /* final */ x6 k;
    @DexIgnore
    public static /* final */ x6 l;
    @DexIgnore
    public static /* final */ /* synthetic */ x6[] m;
    @DexIgnore
    public static /* final */ w6 n; // = new w6(null);
    @DexIgnore
    public /* final */ int b;

    /*
    static {
        x6 x6Var = new x6("SUCCESS", 0, 0);
        c = x6Var;
        x6 x6Var2 = new x6("UNSUPPORTED_FILE_HANDLE", 1, 1);
        d = x6Var2;
        x6 x6Var3 = new x6("READ_NOT_PERMITTED", 2, 2);
        x6 x6Var4 = new x6("WRITE_NOT_PERMITTED", 3, 3);
        x6 x6Var5 = new x6("INVALID_PDU", 4, 4);
        x6 x6Var6 = new x6("INSUFFICIENT_AUTHENTICATION", 5, 5);
        x6 x6Var7 = new x6("REQUEST_NOT_SUPPORTED", 6, 6);
        e = x6Var7;
        x6 x6Var8 = new x6("INVALID_OFFSET", 7, 7);
        x6 x6Var9 = new x6("INSUFFICIENT_AUTHORIZATION", 8, 8);
        x6 x6Var10 = new x6("PREPARE_QUEUE_FULL", 9, 9);
        x6 x6Var11 = new x6("ATTRIBUTE_NOT_FOUND", 10, 10);
        x6 x6Var12 = new x6("ATTRIBUTE_NOT_LONG", 11, 11);
        x6 x6Var13 = new x6("INSUFFICIENT_ENCRYPTION_KEY_SIZE", 12, 12);
        x6 x6Var14 = new x6("INVALID_ATTRIBUTE_VALUE_LEN", 13, 13);
        x6 x6Var15 = new x6("OTHER_UNLIKELY_ERROR", 14, 14);
        x6 x6Var16 = new x6("INSUFFICIENT_ENCRYPTION", 15, 15);
        x6 x6Var17 = new x6("UNSUPPORTED_GROUP_TYPE", 16, 16);
        x6 x6Var18 = new x6("INSUFFICIENT_RESOURCES", 17, 17);
        x6 x6Var19 = new x6("OUT_OF_MEMORY", 18, 112);
        x6 x6Var20 = new x6("TRANSACTION_TIMEOUT", 19, 113);
        x6 x6Var21 = new x6("TRANSACTION_OVERFLOW", 20, 114);
        x6 x6Var22 = new x6("INVALID_RESPONSE_PDU", 21, 115);
        x6 x6Var23 = new x6("REQUEST_CANCELLED", 22, 116);
        x6 x6Var24 = new x6("OTHER_UNDEFINED_ERROR", 23, 117);
        x6 x6Var25 = new x6("REQUIRED_CHARACTERISTIC_NOT_FOUND", 24, 118);
        x6 x6Var26 = new x6("ATTRIBUTE_PDU_LENGTH_EXCEEDED_MTU_SIZE", 25, 119);
        x6 x6Var27 = new x6("PROCEDURE_CONTINUING", 26, 120);
        x6 x6Var28 = new x6("NO_RESOURCES", 27, 128);
        x6 x6Var29 = new x6("INTERNAL_ERROR", 28, 129);
        x6 x6Var30 = new x6("WRONG_STATE", 29, 130);
        x6 x6Var31 = new x6("DB_FULL", 30, 131);
        x6 x6Var32 = new x6("BUSY", 31, 132);
        x6 x6Var33 = new x6("ERROR", 32, 133);
        x6 x6Var34 = new x6("CMD_STARTED", 33, 134);
        x6 x6Var35 = new x6("ILLEGAL_PARAMETER", 34, 135);
        x6 x6Var36 = new x6("PENDING", 35, 136);
        x6 x6Var37 = new x6("AUTH_FAIL", 36, 137);
        x6 x6Var38 = new x6("MORE", 37, 138);
        x6 x6Var39 = new x6("INVALID_CFG", 38, 139);
        x6 x6Var40 = new x6("SERVICE_STARTED", 39, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        x6 x6Var41 = new x6("ENCRYPED_NO_MITM", 40, 141);
        x6 x6Var42 = new x6("NOT_ENCRYPTED", 41, 142);
        x6 x6Var43 = new x6("CONGESTED", 42, 143);
        x6 x6Var44 = new x6("CCCD_IMPROPERLY_CONFIGURED", 43, 253);
        x6 x6Var45 = new x6("PROCEDURE_ALREADY_IN_PROGRESS", 44, 254);
        x6 x6Var46 = new x6("VALUE_OUT_OF_RANGE", 45, 255);
        x6 x6Var47 = new x6("GATT_FAILURE", 46, 257);
        x6 x6Var48 = new x6("START_FAIL", 47, 65536);
        f = x6Var48;
        x6 x6Var49 = new x6("HID_PROXY_NOT_CONNECTED", 48, NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY);
        g = x6Var49;
        x6 x6Var50 = new x6("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 49, NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REQUEST);
        h = x6Var50;
        x6 x6Var51 = new x6("HID_INPUT_DEVICE_DISABLED", 50, NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REPLY);
        i = x6Var51;
        x6 x6Var52 = new x6("HID_UNKNOWN_ERROR", 51, NativeProtocol.MESSAGE_GET_INSTALL_DATA_REQUEST);
        j = x6Var52;
        x6 x6Var53 = new x6("BLUETOOTH_OFF", 52, 16777215);
        k = x6Var53;
        x6 x6Var54 = new x6("UNKNOWN", 53, 16777215);
        l = x6Var54;
        m = new x6[]{x6Var, x6Var2, x6Var3, x6Var4, x6Var5, x6Var6, x6Var7, x6Var8, x6Var9, x6Var10, x6Var11, x6Var12, x6Var13, x6Var14, x6Var15, x6Var16, x6Var17, x6Var18, x6Var19, x6Var20, x6Var21, x6Var22, x6Var23, x6Var24, x6Var25, x6Var26, x6Var27, x6Var28, x6Var29, x6Var30, x6Var31, x6Var32, x6Var33, x6Var34, x6Var35, x6Var36, x6Var37, x6Var38, x6Var39, x6Var40, x6Var41, x6Var42, x6Var43, x6Var44, x6Var45, x6Var46, x6Var47, x6Var48, x6Var49, x6Var50, x6Var51, x6Var52, x6Var53, x6Var54};
    }
    */

    @DexIgnore
    public x6(String str, int i2, int i3) {
        this.b = i3;
    }

    @DexIgnore
    public static x6 valueOf(String str) {
        return (x6) Enum.valueOf(x6.class, str);
    }

    @DexIgnore
    public static x6[] values() {
        return (x6[]) m.clone();
    }
}
