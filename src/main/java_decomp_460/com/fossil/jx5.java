package com.fossil;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.ix5;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jx5<VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> implements Filterable, ix5.a {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Cursor c;
    @DexIgnore
    public jx5<VH>.a d;
    @DexIgnore
    public DataSetObserver e;
    @DexIgnore
    public ix5 f;
    @DexIgnore
    public FilterQueryProvider g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            jx5.this.j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends DataSetObserver {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public void onChanged() {
            jx5.this.b = true;
            jx5.this.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            jx5.this.b = false;
            jx5 jx5 = jx5.this;
            jx5.notifyItemRangeRemoved(0, jx5.getItemCount());
        }
    }

    /*
    static {
        String simpleName = jx5.class.getSimpleName();
        pq7.b(simpleName, "CursorRecyclerViewAdapter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public jx5(Cursor cursor) {
        boolean z = cursor != null;
        this.c = cursor;
        this.b = z;
        this.d = new a();
        this.e = new b();
        if (z) {
            jx5<VH>.a aVar = this.d;
            if (aVar != null) {
                if (cursor != null) {
                    cursor.registerContentObserver(aVar);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            DataSetObserver dataSetObserver = this.e;
            if (dataSetObserver == null) {
                return;
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ix5.a
    public void a(Cursor cursor) {
        pq7.c(cursor, "cursor");
        Cursor l = l(cursor);
        if (l != null) {
            l.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.ix5.a
    public Cursor b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ix5.a
    public CharSequence d(Cursor cursor) {
        String obj;
        return (cursor == null || (obj = cursor.toString()) == null) ? "" : obj;
    }

    @DexIgnore
    @Override // com.fossil.ix5.a
    public Cursor e(CharSequence charSequence) {
        pq7.c(charSequence, "constraint");
        FilterQueryProvider filterQueryProvider = this.g;
        if (filterQueryProvider == null) {
            return this.c;
        }
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.f == null) {
            this.f = new ix5(this);
        }
        ix5 ix5 = this.f;
        if (ix5 != null) {
            return ix5;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        Cursor cursor;
        if (!this.b || (cursor = this.c) == null) {
            return 0;
        }
        if (cursor != null) {
            return cursor.getCount();
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public abstract void i(VH vh, Cursor cursor, int i);

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(h, ".Inside onContentChanged");
    }

    @DexIgnore
    public final void k(FilterQueryProvider filterQueryProvider) {
        pq7.c(filterQueryProvider, "filterQueryProvider");
        this.g = filterQueryProvider;
    }

    @DexIgnore
    public Cursor l(Cursor cursor) {
        if (pq7.a(cursor, this.c)) {
            return null;
        }
        Cursor cursor2 = this.c;
        if (cursor2 != null) {
            jx5<VH>.a aVar = this.d;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.e;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.c = cursor;
        if (cursor != null) {
            jx5<VH>.a aVar2 = this.d;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.e;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.b = true;
            notifyDataSetChanged();
            return cursor2;
        }
        this.b = false;
        notifyItemRangeRemoved(0, getItemCount());
        return cursor2;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(VH vh, int i) {
        pq7.c(vh, "holder");
        if (!this.b) {
            FLogger.INSTANCE.getLocal().d(h, ".Inside onBindViewHolder the cursor is invalid");
        }
        i(vh, this.c, i);
    }
}
