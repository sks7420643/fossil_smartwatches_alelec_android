package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv7 {
    @DexIgnore
    public static final <T> void a(yv7<? super T> yv7, int i) {
        qn7<? super T> c = yv7.c();
        if (!c(i) || !(c instanceof vv7) || b(i) != b(yv7.d)) {
            d(yv7, c, i);
            return;
        }
        dv7 dv7 = ((vv7) c).h;
        tn7 context = c.getContext();
        if (dv7.Q(context)) {
            dv7.M(context, yv7);
        } else {
            e(yv7);
        }
    }

    @DexIgnore
    public static final boolean b(int i) {
        return i == 1;
    }

    @DexIgnore
    public static final boolean c(int i) {
        return i == 0 || i == 1;
    }

    @DexIgnore
    public static final <T> void d(yv7<? super T> yv7, qn7<? super T> qn7, int i) {
        Object r0;
        Object j = yv7.j();
        Throwable d = yv7.d(j);
        Throwable th = d != null ? nv7.d() ? !(qn7 instanceof do7) ? d : uz7.j(d, (do7) qn7) : d : null;
        if (th != null) {
            dl7.a aVar = dl7.Companion;
            r0 = dl7.m1constructorimpl(el7.a(th));
        } else {
            dl7.a aVar2 = dl7.Companion;
            r0 = dl7.m1constructorimpl(j);
        }
        if (i == 0) {
            qn7.resumeWith(r0);
        } else if (i == 1) {
            wv7.b(qn7, r0);
        } else if (i != 2) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        } else if (qn7 != null) {
            vv7 vv7 = (vv7) qn7;
            tn7 context = vv7.getContext();
            Object c = zz7.c(context, vv7.g);
            try {
                vv7.i.resumeWith(r0);
                tl7 tl7 = tl7.f3441a;
            } finally {
                zz7.a(context, c);
            }
        } else {
            throw new il7("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        }
    }

    @DexIgnore
    public static final void e(yv7<?> yv7) {
        hw7 b = wx7.b.b();
        if (b.o0()) {
            b.X(yv7);
            return;
        }
        b.g0(true);
        try {
            d(yv7, yv7.c(), 2);
            do {
            } while (b.r0());
        } catch (Throwable th) {
            b.S(true);
            throw th;
        }
        b.S(true);
    }
}
