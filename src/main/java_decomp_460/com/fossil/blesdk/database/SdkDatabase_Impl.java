package com.fossil.blesdk.database;

import com.fossil.ex0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.fossil.z;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SdkDatabase_Impl extends SdkDatabase {
    @DexIgnore
    public volatile z h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sw0.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `DeviceFile` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `deviceMacAddress` TEXT NOT NULL, `fileType` INTEGER NOT NULL, `fileIndex` INTEGER NOT NULL, `rawData` BLOB NOT NULL, `fileLength` INTEGER NOT NULL, `fileCrc` INTEGER NOT NULL, `createdTimeStamp` INTEGER NOT NULL, `isCompleted` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'f0501e661379ccab31d0b3858cee8e83')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `DeviceFile`");
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) SdkDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) SdkDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            SdkDatabase_Impl.this.mDatabase = lx0;
            SdkDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) SdkDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(9);
            hashMap.put("id", new ix0.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("deviceMacAddress", new ix0.a("deviceMacAddress", "TEXT", true, 0, null, 1));
            hashMap.put("fileType", new ix0.a("fileType", "INTEGER", true, 0, null, 1));
            hashMap.put("fileIndex", new ix0.a("fileIndex", "INTEGER", true, 0, null, 1));
            hashMap.put("rawData", new ix0.a("rawData", "BLOB", true, 0, null, 1));
            hashMap.put("fileLength", new ix0.a("fileLength", "INTEGER", true, 0, null, 1));
            hashMap.put("fileCrc", new ix0.a("fileCrc", "INTEGER", true, 0, null, 1));
            hashMap.put("createdTimeStamp", new ix0.a("createdTimeStamp", "INTEGER", true, 0, null, 1));
            hashMap.put("isCompleted", new ix0.a("isCompleted", "INTEGER", true, 0, null, 1));
            ix0 ix0 = new ix0("DeviceFile", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "DeviceFile");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "DeviceFile(com.fossil.blesdk.database.entity.DeviceFile).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.blesdk.database.SdkDatabase
    public z a() {
        z zVar;
        if (this.h != null) {
            return this.h;
        }
        synchronized (this) {
            if (this.h == null) {
                this.h = new z(this);
            }
            zVar = this.h;
        }
        return zVar;
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `DeviceFile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "DeviceFile");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new a(5), "f0501e661379ccab31d0b3858cee8e83", "05778a001d8750bf160c2cf361f4e16f");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }
}
