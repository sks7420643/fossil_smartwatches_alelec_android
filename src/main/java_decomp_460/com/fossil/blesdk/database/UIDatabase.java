package com.fossil.blesdk.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import com.fossil.cl7;
import com.fossil.e;
import com.fossil.et7;
import com.fossil.g0;
import com.fossil.id0;
import com.fossil.kq7;
import com.fossil.m80;
import com.fossil.mx1;
import com.fossil.pq7;
import com.fossil.pw0;
import com.fossil.q;
import com.fossil.qw0;
import com.fossil.vt7;
import java.io.File;
import java.security.SecureRandom;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UIDatabase extends qw0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f448a;
    @DexIgnore
    public static String b;
    @DexIgnore
    public static UIDatabase c;
    @DexIgnore
    public static /* final */ a d; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final UIDatabase a(Context context, String str, byte[] bArr) {
            qw0.a a2 = pw0.a(context, UIDatabase.class, str);
            a2.g(new SupportFactory(bArr));
            qw0 d = a2.d();
            pq7.b(d, "Room.databaseBuilder(con\u2026                 .build()");
            return (UIDatabase) d;
        }

        @DexIgnore
        public final cl7<byte[], String> b() {
            boolean z = false;
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr = new byte[128];
            for (int i = 0; i < 128; i++) {
                bArr[i] = (byte) ((byte) "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(secureRandom.nextInt(64)));
            }
            mx1 mx1 = mx1.j;
            String encodeToString = Base64.encodeToString(bArr, 2);
            pq7.b(encodeToString, "Base64.encodeToString(passPhrase, Base64.NO_WRAP)");
            String o = mx1.o(encodeToString);
            if (o == null || vt7.l(o)) {
                z = true;
            }
            if (!z) {
                return new cl7<>(bArr, o);
            }
            return null;
        }

        @DexIgnore
        public final byte[] c(String str) {
            byte[] bArr = null;
            Context a2 = id0.i.a();
            if (a2 == null) {
                return null;
            }
            SharedPreferences sharedPreferences = a2.getSharedPreferences(str, 0);
            String string = sharedPreferences.getString("ppa", null);
            if (string == null) {
                m80.c.a(UIDatabase.f448a, "Encrypted passphrase does not existed.", new Object[0]);
                cl7<byte[], String> b = UIDatabase.d.b();
                if (b != null) {
                    sharedPreferences.edit().putString("ppa", b.getSecond()).apply();
                    return b.getFirst();
                }
                m80.c.a(UIDatabase.f448a, "Fail to generate new passphrase.", new Object[0]);
                return null;
            }
            String n = mx1.j.n(string);
            if (n != null) {
                bArr = n.getBytes(et7.f986a);
                pq7.b(bArr, "(this as java.lang.String).getBytes(charset)");
            }
            if (bArr != null) {
                return bArr;
            }
            m80.c.a(UIDatabase.f448a, "Fail to decrypt existing passphrase.", new Object[0]);
            sharedPreferences.edit().remove("ppa").apply();
            cl7<byte[], String> b2 = UIDatabase.d.b();
            if (b2 != null) {
                sharedPreferences.edit().putString("ppa", b2.getSecond()).apply();
                return b2.getFirst();
            }
            m80.c.a(UIDatabase.f448a, "Fail to generate new passphrase.", new Object[0]);
            return bArr;
        }

        @DexIgnore
        public final String d() {
            String str;
            synchronized (this) {
                if (UIDatabase.b == null) {
                    String i = id0.i.i();
                    if (!(i == null || vt7.l(i))) {
                        UIDatabase.b = i + "_ui.sdbk";
                    }
                }
                str = UIDatabase.b;
            }
            return str;
        }

        @DexIgnore
        public final UIDatabase e() {
            UIDatabase uIDatabase;
            boolean z = false;
            synchronized (this) {
                if (UIDatabase.c == null) {
                    Context a2 = id0.i.a();
                    if (a2 != null) {
                        String d = UIDatabase.d.d();
                        if (d == null || vt7.l(d)) {
                            z = true;
                        }
                        if (!z) {
                            m80 m80 = m80.c;
                            String str = UIDatabase.f448a;
                            m80.a(str, "databaseName=" + d, new Object[0]);
                            byte[] c = UIDatabase.d.c(d);
                            if (c != null) {
                                m80 m802 = m80.c;
                                String str2 = UIDatabase.f448a;
                                StringBuilder e = e.e("passPhrase=");
                                e.append(new String(c, et7.f986a));
                                m802.a(str2, e.toString(), new Object[0]);
                                q qVar = q.f2900a;
                                SQLiteDatabase.loadLibs(a2);
                                File databasePath = a2.getDatabasePath(d);
                                pq7.b(databasePath, "ctxt.getDatabasePath(dbName)");
                                if (!qVar.a(databasePath, c)) {
                                    m80.c.a(UIDatabase.f448a, "Fail to open database, drop database and create new one now.", new Object[0]);
                                    File databasePath2 = a2.getDatabasePath(d);
                                    if (databasePath2 != null) {
                                        databasePath2.delete();
                                    }
                                }
                                UIDatabase.c = UIDatabase.d.a(a2, d, c);
                            } else {
                                m80.c.a(UIDatabase.f448a, "Fail to get passphrase, skip opening database.", new Object[0]);
                            }
                        } else {
                            m80.c.a(UIDatabase.f448a, "Fail to get database name, skip opening database.", new Object[0]);
                        }
                    } else {
                        m80.c.a(UIDatabase.f448a, "Application context is not set, skip opening database.", new Object[0]);
                    }
                }
                uIDatabase = UIDatabase.c;
            }
            return uIDatabase;
        }
    }

    /*
    static {
        String simpleName = UIDatabase.class.getSimpleName();
        pq7.b(simpleName, "UIDatabase::class.java.simpleName");
        f448a = simpleName;
    }
    */

    @DexIgnore
    public abstract g0 a();
}
