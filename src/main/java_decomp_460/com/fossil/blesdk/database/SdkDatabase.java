package com.fossil.blesdk.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Base64;
import com.fossil.ax0;
import com.fossil.cl7;
import com.fossil.dy1;
import com.fossil.et7;
import com.fossil.id0;
import com.fossil.ix1;
import com.fossil.kq7;
import com.fossil.lx0;
import com.fossil.m80;
import com.fossil.mx1;
import com.fossil.pq7;
import com.fossil.pw0;
import com.fossil.qw0;
import com.fossil.vt7;
import com.fossil.z;
import java.security.SecureRandom;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SdkDatabase extends qw0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f446a;
    @DexIgnore
    public static SdkDatabase b;
    @DexIgnore
    public static /* final */ ax0 c; // = new a(1, 2);
    @DexIgnore
    public static /* final */ ax0 d; // = new b(2, 3);
    @DexIgnore
    public static /* final */ ax0 e; // = new c(3, 4);
    @DexIgnore
    public static /* final */ ax0 f; // = new d(4, 5);
    @DexIgnore
    public static /* final */ e g; // = new e(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ax0 {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("DROP TABLE ActivityFile");
            lx0.execSQL("CREATE TABLE DeviceFile(deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, PRIMARY KEY(deviceMacAddress, fileType, fileIndex))");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ax0 {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
            lx0.execSQL("CREATE TABLE DeviceFile_New(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, isCompleted INTEGER NOT NULL)");
            Cursor query = lx0.query("SELECT * from DeviceFile");
            pq7.b(query, "database.query(\"SELECT * from DeviceFile\")");
            int columnIndex = query.getColumnIndex("deviceMacAddress");
            int columnIndex2 = query.getColumnIndex("fileType");
            int columnIndex3 = query.getColumnIndex("fileIndex");
            int columnIndex4 = query.getColumnIndex("rawData");
            int columnIndex5 = query.getColumnIndex("fileLength");
            int columnIndex6 = query.getColumnIndex("fileCrc");
            int columnIndex7 = query.getColumnIndex("createdTimeStamp");
            while (query.moveToNext()) {
                String string = query.getString(columnIndex);
                pq7.b(string, "oldDataCursor.getString(\u2026iceMacAddressColumnIndex)");
                byte b = (byte) query.getShort(columnIndex2);
                byte b2 = (byte) query.getShort(columnIndex3);
                byte[] blob = query.getBlob(columnIndex4);
                pq7.b(blob, "oldDataCursor.getBlob(rawDataColumnIndex)");
                long j = query.getLong(columnIndex5);
                long j2 = query.getLong(columnIndex6);
                long j3 = query.getLong(columnIndex7);
                int i = j2 == ix1.f1688a.b(blob, ix1.a.CRC32) ? 1 : 0;
                lx0.execSQL("Insert into DeviceFile_New(deviceMacAddress, fileType, fileIndex, rawData, fileLength, fileCrc, createdTimeStamp, isCompleted) values ('" + string + "', " + ((int) b) + ", " + ((int) b2) + ", X'" + dy1.e(blob, null, 1, null) + "', " + j + ", " + j2 + ", " + j3 + ", " + i + ')');
            }
            lx0.execSQL("DROP TABLE DeviceFile");
            lx0.execSQL("ALTER TABLE DeviceFile_New RENAME TO DeviceFile");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ax0 {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ax0 {
        @DexIgnore
        public d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.ax0
        public void migrate(lx0 lx0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* synthetic */ e(kq7 kq7) {
        }

        @DexIgnore
        public final SdkDatabase a(Context context, byte[] bArr) {
            qw0.a a2 = pw0.a(context, SdkDatabase.class, "device.sdbk");
            a2.b(SdkDatabase.c, SdkDatabase.d, SdkDatabase.e, SdkDatabase.f);
            a2.g(new SupportFactory(bArr));
            a2.c();
            qw0 d = a2.d();
            pq7.b(d, "Room.databaseBuilder(con\u2026                 .build()");
            return (SdkDatabase) d;
        }

        @DexIgnore
        public final cl7<byte[], String> b() {
            boolean z = false;
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr = new byte[128];
            for (int i = 0; i < 128; i++) {
                bArr[i] = (byte) ((byte) "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(secureRandom.nextInt(64)));
            }
            mx1 mx1 = mx1.j;
            String encodeToString = Base64.encodeToString(bArr, 2);
            pq7.b(encodeToString, "Base64.encodeToString(passPhrase, Base64.NO_WRAP)");
            String o = mx1.o(encodeToString);
            if (o == null || vt7.l(o)) {
                z = true;
            }
            if (!z) {
                return new cl7<>(bArr, o);
            }
            return null;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0100  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0131  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0175  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.fossil.blesdk.database.SdkDatabase c() {
            /*
            // Method dump skipped, instructions count: 376
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.database.SdkDatabase.e.c():com.fossil.blesdk.database.SdkDatabase");
        }

        @DexIgnore
        public final byte[] d() {
            byte[] bArr = null;
            Context a2 = id0.i.a();
            if (a2 == null) {
                return null;
            }
            SharedPreferences sharedPreferences = a2.getSharedPreferences("device.sdbk", 0);
            String string = sharedPreferences.getString("ppa", null);
            if (string == null) {
                m80.c.a(SdkDatabase.f446a, "Encrypted passphrase does not existed.", new Object[0]);
                cl7<byte[], String> b = SdkDatabase.g.b();
                if (b != null) {
                    sharedPreferences.edit().putString("ppa", b.getSecond()).apply();
                    return b.getFirst();
                }
                m80.c.a(SdkDatabase.f446a, "Fail to generate new passphrase.", new Object[0]);
                return null;
            }
            String n = mx1.j.n(string);
            if (n != null) {
                bArr = n.getBytes(et7.f986a);
                pq7.b(bArr, "(this as java.lang.String).getBytes(charset)");
            }
            if (bArr != null) {
                return bArr;
            }
            m80.c.a(SdkDatabase.f446a, "Fail to decrypt existing passphrase.", new Object[0]);
            sharedPreferences.edit().remove("ppa").apply();
            cl7<byte[], String> b2 = SdkDatabase.g.b();
            if (b2 != null) {
                sharedPreferences.edit().putString("ppa", b2.getSecond()).apply();
                return b2.getFirst();
            }
            m80.c.a(SdkDatabase.f446a, "Fail to generate new passphrase.", new Object[0]);
            return bArr;
        }
    }

    /*
    static {
        String simpleName = SdkDatabase.class.getSimpleName();
        pq7.b(simpleName, "SdkDatabase::class.java.simpleName");
        f446a = simpleName;
    }
    */

    @DexIgnore
    public abstract z a();
}
