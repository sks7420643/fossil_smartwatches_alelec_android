package com.fossil.blesdk.database;

import com.fossil.ex0;
import com.fossil.g0;
import com.fossil.hw0;
import com.fossil.ix0;
import com.fossil.lx0;
import com.fossil.mx0;
import com.fossil.nw0;
import com.fossil.qw0;
import com.fossil.sw0;
import com.portfolio.platform.data.model.Firmware;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UIDatabase_Impl extends UIDatabase {
    @DexIgnore
    public volatile g0 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sw0.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void createAllTables(lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `ThemeTemplateEntity` (`id` TEXT NOT NULL, `classifier` INTEGER NOT NULL, `packageOSVersion` TEXT NOT NULL, `checksum` TEXT NOT NULL, `downloadUrl` TEXT NOT NULL, `updatedAt` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `data` BLOB, PRIMARY KEY(`classifier`, `packageOSVersion`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '61814d7411cfec8e2060487dde0de32b')");
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void dropAllTables(lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `ThemeTemplateEntity`");
            if (UIDatabase_Impl.this.mCallbacks != null) {
                int size = UIDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UIDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onCreate(lx0 lx0) {
            if (UIDatabase_Impl.this.mCallbacks != null) {
                int size = UIDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UIDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onOpen(lx0 lx0) {
            UIDatabase_Impl.this.mDatabase = lx0;
            UIDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (UIDatabase_Impl.this.mCallbacks != null) {
                int size = UIDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((qw0.b) UIDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPostMigrate(lx0 lx0) {
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public void onPreMigrate(lx0 lx0) {
            ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.fossil.sw0.a
        public sw0.b onValidateSchema(lx0 lx0) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new ix0.a("id", "TEXT", true, 0, null, 1));
            hashMap.put("classifier", new ix0.a("classifier", "INTEGER", true, 1, null, 1));
            hashMap.put("packageOSVersion", new ix0.a("packageOSVersion", "TEXT", true, 2, null, 1));
            hashMap.put("checksum", new ix0.a("checksum", "TEXT", true, 0, null, 1));
            hashMap.put(Firmware.COLUMN_DOWNLOAD_URL, new ix0.a(Firmware.COLUMN_DOWNLOAD_URL, "TEXT", true, 0, null, 1));
            hashMap.put("updatedAt", new ix0.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ix0.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("data", new ix0.a("data", "BLOB", false, 0, null, 1));
            ix0 ix0 = new ix0("ThemeTemplateEntity", hashMap, new HashSet(0), new HashSet(0));
            ix0 a2 = ix0.a(lx0, "ThemeTemplateEntity");
            if (ix0.equals(a2)) {
                return new sw0.b(true, null);
            }
            return new sw0.b(false, "ThemeTemplateEntity(com.fossil.blesdk.database.entity.ThemeTemplateEntity).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.blesdk.database.UIDatabase
    public g0 a() {
        g0 g0Var;
        if (this.e != null) {
            return this.e;
        }
        synchronized (this) {
            if (this.e == null) {
                this.e = new g0(this);
            }
            g0Var = this.e;
        }
        return g0Var;
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public void clearAllTables() {
        super.assertNotMainThread();
        lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `ThemeTemplateEntity`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public nw0 createInvalidationTracker() {
        return new nw0(this, new HashMap(0), new HashMap(0), "ThemeTemplateEntity");
    }

    @DexIgnore
    @Override // com.fossil.qw0
    public mx0 createOpenHelper(hw0 hw0) {
        sw0 sw0 = new sw0(hw0, new a(1), "61814d7411cfec8e2060487dde0de32b", "52e083e2630cbc37ce444cf873a985b3");
        mx0.b.a a2 = mx0.b.a(hw0.b);
        a2.c(hw0.c);
        a2.b(sw0);
        return hw0.f1544a.create(a2.a());
    }
}
