package com.fossil.blesdk.model.network;

import com.fossil.rj4;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApiResponse<T> {
    @rj4(CloudLogWriter.ITEMS_PARAM)

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<T> f450a; // = new ArrayList();

    @DexIgnore
    public final List<T> a() {
        return this.f450a;
    }
}
