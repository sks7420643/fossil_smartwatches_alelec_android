package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at2 implements Parcelable.Creator<xs2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ xs2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        Bundle bundle = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j = 0;
        long j2 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    j2 = ad2.y(parcel, t);
                    break;
                case 2:
                    j = ad2.y(parcel, t);
                    break;
                case 3:
                    z = ad2.m(parcel, t);
                    break;
                case 4:
                    str3 = ad2.f(parcel, t);
                    break;
                case 5:
                    str2 = ad2.f(parcel, t);
                    break;
                case 6:
                    str = ad2.f(parcel, t);
                    break;
                case 7:
                    bundle = ad2.a(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new xs2(j2, j, z, str3, str2, str, bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ xs2[] newArray(int i) {
        return new xs2[i];
    }
}
