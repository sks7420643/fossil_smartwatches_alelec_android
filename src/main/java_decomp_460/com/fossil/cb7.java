package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f596a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public cb7(String str, String str2, String str3) {
        pq7.c(str, "ringId");
        pq7.c(str2, "name");
        this.f596a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.f596a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof cb7) {
                cb7 cb7 = (cb7) obj;
                if (!pq7.a(this.f596a, cb7.f596a) || !pq7.a(this.b, cb7.b) || !pq7.a(this.c, cb7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f596a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "RingWrapper(ringId=" + this.f596a + ", name=" + this.b + ", url=" + this.c + ")";
    }
}
