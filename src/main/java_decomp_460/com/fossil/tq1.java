package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutRunningHistory;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ int[] g;
    @DexIgnore
    public /* final */ ArrayList<WorkoutRunningHistory> h; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<tq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public tq1 createFromParcel(Parcel parcel) {
            return new tq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public tq1[] newArray(int i) {
            return new tq1[i];
        }
    }

    @DexIgnore
    public tq1(byte b, int i, long j, long j2, int[] iArr) {
        super(np1.WORKOUT_PAUSE_RUN_SEQUENCE, b, i);
        this.e = j;
        this.g = iArr;
        this.f = j2;
        ur7 l = bs7.l(em7.H(iArr), 2);
        int a2 = l.a();
        int b2 = l.b();
        int c = l.c();
        if (c >= 0) {
            if (a2 > b2) {
                return;
            }
        } else if (a2 < b2) {
            return;
        }
        while (true) {
            this.h.add(new WorkoutRunningHistory(iArr[a2], iArr[a2 + 1]));
            if (a2 != b2) {
                a2 += c;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public /* synthetic */ tq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = parcel.readLong();
        int[] createIntArray = parcel.createIntArray();
        if (createIntArray != null) {
            this.g = createIntArray;
            this.f = parcel.readLong();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final long c() {
        return this.f;
    }

    @DexIgnore
    public final ArrayList<WorkoutRunningHistory> d() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(tq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            tq1 tq1 = (tq1) obj;
            if (this.e != tq1.e) {
                return false;
            }
            if (!Arrays.equals(this.g, tq1.g)) {
                return false;
            }
            return this.f == tq1.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutPauseRunSequenceRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = Long.valueOf(this.e).hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + this.g.hashCode()) * 31) + Long.valueOf(this.f).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1, com.fossil.jq1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(super.toJSONObject(), jd0.H5, Long.valueOf(this.e)), jd0.R5, ay1.a(this.g)), jd0.M5, Long.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeIntArray(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
    }
}
