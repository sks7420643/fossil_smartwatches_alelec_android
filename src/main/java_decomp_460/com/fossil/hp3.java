package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ fr3 c;
    @DexIgnore
    public /* final */ /* synthetic */ or3 d;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 e;

    @DexIgnore
    public hp3(fp3 fp3, boolean z, fr3 fr3, or3 or3) {
        this.e = fp3;
        this.b = z;
        this.c = fr3;
        this.d = or3;
    }

    @DexIgnore
    public final void run() {
        cl3 cl3 = this.e.d;
        if (cl3 == null) {
            this.e.d().F().a("Discarding data. Failed to set user property");
            return;
        }
        this.e.M(cl3, this.b ? null : this.c, this.d);
        this.e.e0();
    }
}
