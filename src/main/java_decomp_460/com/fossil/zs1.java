package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs1 extends ox1 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ ys1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zs1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zs1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                Parcelable readParcelable = parcel.readParcelable(ys1.class.getClassLoader());
                if (readParcelable != null) {
                    pq7.b(readParcelable, "parcel.readParcelable<Fi\u2026class.java.classLoader)!!");
                    return new zs1(readString, readInt, (ys1) readParcelable);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zs1[] newArray(int i) {
            return new zs1[i];
        }
    }

    @DexIgnore
    public zs1(String str, int i, ys1 ys1) {
        this.b = str;
        this.c = i;
        this.d = ys1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof zs1) {
                zs1 zs1 = (zs1) obj;
                if (!pq7.a(this.b, zs1.b) || this.c != zs1.c || !pq7.a(this.d, zs1.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ys1 getFitnessData() {
        return this.d;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final int getRank() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        int i2 = this.c;
        ys1 ys1 = this.d;
        if (ys1 != null) {
            i = ys1.hashCode();
        }
        return (((hashCode * 31) + i2) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return gy1.c(g80.k(g80.k(new JSONObject(), jd0.H, this.b), jd0.G4, Integer.valueOf(this.c)), this.d.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e = e.e("Player(name=");
        e.append(this.b);
        e.append(", rank=");
        e.append(this.c);
        e.append(", fitnessData=");
        e.append(this.d);
        e.append(")");
        return e.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeParcelable(this.d, i);
    }
}
