package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import com.fossil.v18;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.remote.ApiService2Dot1;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.SecureApiService;
import com.portfolio.platform.data.source.remote.SecureApiService2Dot1;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.io.File;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ PortfolioApp f3623a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Interceptor {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uo4 f3624a;
        @DexIgnore
        public /* final */ /* synthetic */ on5 b;

        @DexIgnore
        public a(uo4 uo4, on5 on5) {
            this.f3624a = uo4;
            this.b = on5;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            v18.a h = chain.c().h();
            h.a("Content-Type", Constants.CONTENT_TYPE);
            h.a("User-Agent", lh5.b.a());
            h.a("locale", this.f3624a.f3623a.X());
            String g = this.b.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Interceptor {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uo4 f3625a;
        @DexIgnore
        public /* final */ /* synthetic */ on5 b;

        @DexIgnore
        public b(uo4 uo4, on5 on5) {
            this.f3625a = uo4;
            this.b = on5;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            v18.a h = chain.c().h();
            h.a("Content-Type", Constants.CONTENT_TYPE);
            h.a("User-Agent", lh5.b.a());
            String g = this.b.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            h.a("locale", this.f3625a.f3623a.X());
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Interceptor {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ on5 f3626a;

        @DexIgnore
        public c(on5 on5) {
            this.f3626a = on5;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            UserDao userDao;
            MFUser currentUser;
            MFUser.Auth auth;
            v18.a h = chain.c().h();
            String j = lk5.j(DateTime.now());
            UserDatabase B = bn5.j.B();
            String accessToken = (B == null || (userDao = B.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
            h.a(ConfigItem.KEY_DATE, j);
            String g = this.f3626a.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            h.a("Authorization", "Bearer " + accessToken);
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Interceptor {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ on5 f3627a;

        @DexIgnore
        public d(on5 on5) {
            this.f3627a = on5;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            UserDao userDao;
            MFUser currentUser;
            MFUser.Auth auth;
            v18.a h = chain.c().h();
            String j = lk5.j(DateTime.now());
            UserDatabase B = bn5.j.B();
            String accessToken = (B == null || (userDao = B.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
            h.a(ConfigItem.KEY_DATE, j);
            String g = this.f3627a.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            h.a("Authorization", "Bearer " + accessToken);
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Interceptor {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ e f3628a; // = new e();

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            return chain.d(chain.c().h().b());
        }
    }

    @DexIgnore
    public uo4(PortfolioApp portfolioApp) {
        pq7.c(portfolioApp, "mApplication");
        this.f3623a = portfolioApp;
    }

    @DexIgnore
    public final SecureApiService2Dot1 A(on5 on5) {
        pq7.c(on5, "sharedPreferencesManager");
        d dVar = new d(on5);
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.f(dVar);
        return (SecureApiService2Dot1) tq5.g.b(SecureApiService2Dot1.class);
    }

    @DexIgnore
    public final on5 B(Context context) {
        pq7.c(context, "context");
        return new on5(context);
    }

    @DexIgnore
    public final ShortcutApiService C(qq5 qq5, uq5 uq5) {
        pq7.c(qq5, "interceptor");
        pq7.c(uq5, "authenticator");
        return (ShortcutApiService) d(qq5, uq5, ShortcutApiService.class);
    }

    @DexIgnore
    public final uq4 D() {
        return new uq4(new wq4());
    }

    @DexIgnore
    public final n47 E() {
        return n47.d.a();
    }

    @DexIgnore
    public final pl5 F() {
        pl5 h = pl5.h();
        pq7.b(h, "WatchHelper.getInstance()");
        return h;
    }

    @DexIgnore
    public final rl5 G(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        pq7.c(deviceRepository, "deviceRepository");
        pq7.c(portfolioApp, "app");
        return new rl5(deviceRepository, portfolioApp);
    }

    @DexIgnore
    public final wn5 H() {
        return new wn5();
    }

    @DexIgnore
    public final xn5 I() {
        return new xn5();
    }

    @DexIgnore
    public final DownloadServiceApi J() {
        e eVar = e.f3628a;
        tq5 tq5 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq5.e(cacheDir);
        tq5.g.g(3);
        tq5.g.f(eVar);
        return (DownloadServiceApi) tq5.g.b(DownloadServiceApi.class);
    }

    @DexIgnore
    public final bk5 b(on5 on5, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        pq7.c(on5, "sharedPreferencesManager");
        pq7.c(userRepository, "userRepository");
        pq7.c(alarmsRepository, "alarmsRepository");
        return new bk5(on5, userRepository, alarmsRepository);
    }

    @DexIgnore
    public final ck5 c() {
        return ck5.f.g();
    }

    @DexIgnore
    public final <S> S d(qq5 qq5, uq5 uq5, Class<S> cls) {
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.b(this.f3623a, 0) + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.c(uq5);
        tq5.g.f(qq5);
        return (S) tq5.g.b(cls);
    }

    @DexIgnore
    public final ApiServiceV2 e(qq5 qq5, uq5 uq5) {
        pq7.c(qq5, "interceptor");
        pq7.c(uq5, "authenticator");
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.c(uq5);
        tq5.g.f(qq5);
        return (ApiServiceV2) tq5.g.b(ApiServiceV2.class);
    }

    @DexIgnore
    public final ApiService2Dot1 f(qq5 qq5, uq5 uq5) {
        pq7.c(qq5, "interceptor");
        pq7.c(uq5, "authenticator");
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2.1") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.c(uq5);
        tq5.g.f(qq5);
        return (ApiService2Dot1) tq5.g.b(ApiService2Dot1.class);
    }

    @DexIgnore
    public final PortfolioApp g() {
        return this.f3623a;
    }

    @DexIgnore
    public final Context h() {
        Context applicationContext = this.f3623a.getApplicationContext();
        pq7.b(applicationContext, "mApplication.applicationContext");
        return applicationContext;
    }

    @DexIgnore
    public final ApplicationEventListener i(PortfolioApp portfolioApp, on5 on5, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, mj5 mj5, DianaWatchFaceRepository dianaWatchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, RingStyleRepository ringStyleRepository, hu4 hu4, zt4 zt4, tt4 tt4, FileRepository fileRepository, vt4 vt4, WorkoutSettingRepository workoutSettingRepository, pr4 pr4, dr5 dr5, ThemeRepository themeRepository, s77 s77, uo5 uo5, zo5 zo5, k97 k97, DianaAppSettingRepository dianaAppSettingRepository) {
        pq7.c(portfolioApp, "app");
        pq7.c(on5, "sharedPreferencesManager");
        pq7.c(hybridPresetRepository, "hybridPresetRepository");
        pq7.c(categoryRepository, "categoryRepository");
        pq7.c(watchAppRepository, "watchAppRepository");
        pq7.c(complicationRepository, "complicationRepository");
        pq7.c(microAppRepository, "microAppRepository");
        pq7.c(dianaPresetRepository, "dianaPresetRepository");
        pq7.c(deviceRepository, "deviceRepository");
        pq7.c(userRepository, "userRepository");
        pq7.c(alarmsRepository, "alarmsRepository");
        pq7.c(mj5, "deviceSettingFactory");
        pq7.c(dianaWatchFaceRepository, "watchFaceRepository");
        pq7.c(watchLocalizationRepository, "watchLocalizationRepository");
        pq7.c(ringStyleRepository, "ringStyleRepository");
        pq7.c(hu4, "socialProfileRepository");
        pq7.c(zt4, "socialFriendRepository");
        pq7.c(tt4, "challengeRepository");
        pq7.c(fileRepository, "fileRepository");
        pq7.c(vt4, "fcmRepository");
        pq7.c(workoutSettingRepository, "workoutSettingRepository");
        pq7.c(pr4, "flagRepository");
        pq7.c(dr5, "buddyChallengeManager");
        pq7.c(themeRepository, "themeRepository");
        pq7.c(s77, "assetRepository");
        pq7.c(uo5, "dianaWatchFaceRepository");
        pq7.c(zo5, "dianaRecommendedPresetRepository");
        pq7.c(k97, "wfBackgroundPhotoRepository");
        pq7.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        return new ApplicationEventListener(portfolioApp, on5, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, ringStyleRepository, deviceRepository, userRepository, mj5, alarmsRepository, dianaWatchFaceRepository, watchLocalizationRepository, hu4, zt4, tt4, fileRepository, vt4, workoutSettingRepository, pr4, dr5, themeRepository, s77, uo5, zo5, k97, dianaAppSettingRepository);
    }

    @DexIgnore
    public final AuthApiGuestService j(on5 on5) {
        pq7.c(on5, "sharedPreferencesManager");
        a aVar = new a(this, on5);
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2.1") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.f(aVar);
        return (AuthApiGuestService) tq5.g.b(AuthApiGuestService.class);
    }

    @DexIgnore
    public final AuthApiUserService k(qq5 qq5, uq5 uq5) {
        pq7.c(qq5, "interceptor");
        pq7.c(uq5, "authenticator");
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2.1") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.c(uq5);
        tq5.g.f(qq5);
        return (AuthApiUserService) tq5.g.b(AuthApiUserService.class);
    }

    @DexIgnore
    public final ContentResolver l() {
        ContentResolver contentResolver = this.f3623a.getContentResolver();
        pq7.b(contentResolver, "mApplication.contentResolver");
        return contentResolver;
    }

    @DexIgnore
    public final un5 m() {
        return new un5();
    }

    @DexIgnore
    public final cn5 n() {
        return new cn5();
    }

    @DexIgnore
    public final FirmwareFileRepository o() {
        Context applicationContext = this.f3623a.getApplicationContext();
        pq7.b(applicationContext, "mApplication.applicationContext");
        return new FirmwareFileRepository(applicationContext, new FirmwareFileLocalSource());
    }

    @DexIgnore
    public final GoogleApiService p(qq5 qq5, uq5 uq5) {
        pq7.c(qq5, "interceptor");
        pq7.c(uq5, "authenticator");
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.b(this.f3623a, 4) + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.c(uq5);
        tq5.g.f(qq5);
        return (GoogleApiService) tq5.g.b(GoogleApiService.class);
    }

    @DexIgnore
    public final uk5 q(Context context, no4 no4, on5 on5) {
        pq7.c(context, "context");
        pq7.c(no4, "appExecutors");
        pq7.c(on5, "sharedPreferencesManager");
        return new uk5(context, no4.b(), on5);
    }

    @DexIgnore
    public final GuestApiService r(on5 on5) {
        pq7.c(on5, "sharedPreferencesManager");
        b bVar = new b(this, on5);
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.f(bVar);
        return (GuestApiService) tq5.g.b(GuestApiService.class);
    }

    @DexIgnore
    public final pu6 s(InAppNotificationRepository inAppNotificationRepository) {
        pq7.c(inAppNotificationRepository, "repository");
        return new pu6(inAppNotificationRepository);
    }

    @DexIgnore
    public final hn5 t(HybridPresetRepository hybridPresetRepository, dr5 dr5, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, on5 on5, a37 a37, or5 or5, fs5 fs5) {
        pq7.c(hybridPresetRepository, "hybridPresetRepository");
        pq7.c(dr5, "buddyChallengeManager");
        pq7.c(quickResponseRepository, "quickResponseRepository");
        pq7.c(alarmsRepository, "alarmsRepository");
        pq7.c(on5, "sharedPreferencesManager");
        pq7.c(a37, "setNotificationUseCase");
        pq7.c(or5, "musicControlComponent");
        pq7.c(fs5, "mWorkoutTetherGpsManager");
        return new hn5(hybridPresetRepository, alarmsRepository, dr5, fs5, quickResponseRepository, or5, a37, on5);
    }

    @DexIgnore
    public final ct0 u() {
        ct0 b2 = ct0.b(this.f3623a);
        pq7.b(b2, "LocalBroadcastManager.getInstance(mApplication)");
        return b2;
    }

    @DexIgnore
    public final vn5 v() {
        return new vn5();
    }

    @DexIgnore
    public final lo4 w() {
        lo4 h = lo4.h(this.f3623a);
        pq7.b(h, "MFLocationService.getInstance(mApplication)");
        return h;
    }

    @DexIgnore
    public final do5 x(on5 on5, DeviceRepository deviceRepository, DianaPresetRepository dianaPresetRepository, oq4 oq4) {
        pq7.c(on5, "mSharedPrefs");
        pq7.c(deviceRepository, "deviceRepository");
        pq7.c(dianaPresetRepository, "oldPresetRepository");
        pq7.c(oq4, "migrationManager");
        return new do5(on5, deviceRepository, dianaPresetRepository, oq4);
    }

    @DexIgnore
    public final oq4 y(on5 on5, UserRepository userRepository, yy6 yy6, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, mj5 mj5, pr4 pr4, DianaPresetRepository dianaPresetRepository, WatchFaceRepository watchFaceRepository, FileRepository fileRepository, k97 k97, uo5 uo5, s77 s77, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        pq7.c(on5, "mSharedPrefs");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(yy6, "getHybridUseCase");
        pq7.c(notificationsRepository, "mNotificationRepository");
        pq7.c(portfolioApp, "mApp");
        pq7.c(goalTrackingRepository, "goalTrackingRepo");
        pq7.c(deviceDao, "deviceDao");
        pq7.c(hybridCustomizeDatabase, "mHybridCustomizeDatabase");
        pq7.c(microAppLastSettingRepository, "microAppLastSettingRepository");
        pq7.c(mj5, "deviceSettingFactory");
        pq7.c(pr4, "flagRepository");
        pq7.c(dianaPresetRepository, "oldPresetRepository");
        pq7.c(watchFaceRepository, "oldWatchFaceRepository");
        pq7.c(fileRepository, "fileRepository");
        pq7.c(k97, "newPhotoWFBackgroundPhotoRepository");
        pq7.c(uo5, "newPresetRepository");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(deviceRepository, "deviceRepository");
        pq7.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        pq7.c(dianaWatchFaceRepository, "dianaWatchFaceRepository");
        return new oq4(on5, userRepository, notificationsRepository, yy6, portfolioApp, goalTrackingRepository, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, mj5, pr4, dianaPresetRepository, watchFaceRepository, fileRepository, k97, uo5, s77, deviceRepository, dianaAppSettingRepository, dianaWatchFaceRepository);
    }

    @DexIgnore
    public final SecureApiService z(on5 on5) {
        pq7.c(on5, "sharedPreferencesManager");
        c cVar = new c(on5);
        tq5 tq5 = tq5.g;
        tq5.d(h37.b.c(this.f3623a, 0, "2") + "/");
        tq5 tq52 = tq5.g;
        File cacheDir = this.f3623a.getCacheDir();
        pq7.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        tq5.g.f(cVar);
        return (SecureApiService) tq5.g.b(SecureApiService.class);
    }
}
