package com.fossil;

import com.fossil.dl7;
import com.fossil.lz7;
import com.fossil.yy7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jy7<E> extends ly7<E> implements my7<E> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<E> extends uy7<E> {
        @DexIgnore
        public /* final */ ku7<Object> e;
        @DexIgnore
        public /* final */ int f;

        @DexIgnore
        public a(ku7<Object> ku7, int i) {
            this.e = ku7;
            this.f = i;
        }

        @DexIgnore
        @Override // com.fossil.wy7
        public void d(E e2) {
            this.e.g(mu7.f2424a);
        }

        @DexIgnore
        @Override // com.fossil.wy7
        public vz7 e(E e2, lz7.c cVar) {
            Object b = this.e.b(x(e2), cVar != null ? cVar.f2283a : null);
            if (b == null) {
                return null;
            }
            if (nv7.a()) {
                if (!(b == mu7.f2424a)) {
                    throw new AssertionError();
                }
            }
            if (cVar == null) {
                return mu7.f2424a;
            }
            cVar.d();
            throw null;
        }

        @DexIgnore
        @Override // com.fossil.lz7
        public String toString() {
            return "ReceiveElement@" + ov7.b(this) + "[receiveMode=" + this.f + ']';
        }

        @DexIgnore
        @Override // com.fossil.uy7
        public void w(py7<?> py7) {
            if (this.f == 1 && py7.e == null) {
                ku7<Object> ku7 = this.e;
                dl7.a aVar = dl7.Companion;
                ku7.resumeWith(dl7.m1constructorimpl(null));
            } else if (this.f == 2) {
                ku7<Object> ku72 = this.e;
                yy7.b bVar = yy7.b;
                yy7.a aVar2 = new yy7.a(py7.e);
                yy7.b(aVar2);
                yy7 a2 = yy7.a(aVar2);
                dl7.a aVar3 = dl7.Companion;
                ku72.resumeWith(dl7.m1constructorimpl(a2));
            } else {
                ku7<Object> ku73 = this.e;
                Throwable x = py7.x();
                dl7.a aVar4 = dl7.Companion;
                ku73.resumeWith(dl7.m1constructorimpl(el7.a(x)));
            }
        }

        @DexIgnore
        public final Object x(E e2) {
            if (this.f != 2) {
                return e2;
            }
            yy7.b bVar = yy7.b;
            yy7.b(e2);
            return yy7.a(e2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends iu7 {
        @DexIgnore
        public /* final */ uy7<?> b;

        @DexIgnore
        public b(uy7<?> uy7) {
            this.b = uy7;
        }

        @DexIgnore
        @Override // com.fossil.ju7
        public void a(Throwable th) {
            if (this.b.r()) {
                jy7.this.s();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
            a(th);
            return tl7.f3441a;
        }

        @DexIgnore
        public String toString() {
            return "RemoveReceiveOnCancel[" + this.b + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends lz7.b {
        @DexIgnore
        public /* final */ /* synthetic */ jy7 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(lz7 lz7, lz7 lz72, jy7 jy7) {
            super(lz72);
            this.d = jy7;
        }

        @DexIgnore
        /* renamed from: i */
        public Object g(lz7 lz7) {
            if (this.d.r()) {
                return null;
            }
            return kz7.a();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.qn7<? super E> */
    /* JADX WARN: Multi-variable type inference failed */
    public final Object a(qn7<? super E> qn7) {
        Object u = u();
        return (u == ky7.b || (u instanceof py7)) ? v(1, qn7) : u;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.qn7<? super E> */
    /* JADX WARN: Multi-variable type inference failed */
    public final Object b(qn7<? super E> qn7) {
        Object u = u();
        return (u == ky7.b || (u instanceof py7)) ? v(0, qn7) : u;
    }

    @DexIgnore
    @Override // com.fossil.ly7
    public wy7<E> l() {
        wy7<E> l = super.l();
        if (l != null && !(l instanceof py7)) {
            s();
        }
        return l;
    }

    @DexIgnore
    public final boolean o(uy7<? super E> uy7) {
        boolean p = p(uy7);
        if (p) {
            t();
        }
        return p;
    }

    @DexIgnore
    public boolean p(uy7<? super E> uy7) {
        int v;
        lz7 n;
        if (q()) {
            lz7 g = g();
            do {
                n = g.n();
                if (!(!(n instanceof xy7))) {
                    return false;
                }
            } while (!n.g(uy7, g));
        } else {
            lz7 g2 = g();
            c cVar = new c(uy7, uy7, this);
            do {
                lz7 n2 = g2.n();
                if (!(!(n2 instanceof xy7))) {
                    return false;
                }
                v = n2.v(uy7, g2, cVar);
                if (v != 1) {
                }
            } while (v != 2);
            return false;
        }
        return true;
    }

    @DexIgnore
    public abstract boolean q();

    @DexIgnore
    public abstract boolean r();

    @DexIgnore
    public void s() {
    }

    @DexIgnore
    public void t() {
    }

    @DexIgnore
    public abstract Object u();

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.jy7$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final /* synthetic */ <R> Object v(int i, qn7<? super R> qn7) {
        lu7 b2 = nu7.b(xn7.c(qn7));
        if (b2 != null) {
            a aVar = new a(b2, i);
            while (true) {
                if (!o(aVar)) {
                    Object u = u();
                    if (!(u instanceof py7)) {
                        if (u != ky7.b) {
                            Object x = aVar.x(u);
                            dl7.a aVar2 = dl7.Companion;
                            b2.resumeWith(dl7.m1constructorimpl(x));
                            break;
                        }
                    } else {
                        aVar.w((py7) u);
                        break;
                    }
                } else {
                    w(b2, aVar);
                    break;
                }
            }
            Object t = b2.t();
            if (t == yn7.d()) {
                go7.c(qn7);
            }
            return t;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.CancellableContinuation<kotlin.Any?>");
    }

    @DexIgnore
    public final void w(ku7<?> ku7, uy7<?> uy7) {
        ku7.e(new b(uy7));
    }
}
