package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uh1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<a<?, ?>> f3587a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<Z, R> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<Z> f3588a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ th1<Z, R> c;

        @DexIgnore
        public a(Class<Z> cls, Class<R> cls2, th1<Z, R> th1) {
            this.f3588a = cls;
            this.b = cls2;
            this.c = th1;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.f3588a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public <Z, R> th1<Z, R> a(Class<Z> cls, Class<R> cls2) {
        th1<Z, R> th1;
        synchronized (this) {
            if (cls2.isAssignableFrom(cls)) {
                th1 = vh1.b();
            } else {
                for (a<?, ?> aVar : this.f3587a) {
                    if (aVar.a(cls, cls2)) {
                        th1 = aVar.c;
                    }
                }
                throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
            }
        }
        return th1;
    }

    @DexIgnore
    public <Z, R> List<Class<R>> b(Class<Z> cls, Class<R> cls2) {
        synchronized (this) {
            ArrayList arrayList = new ArrayList();
            if (cls2.isAssignableFrom(cls)) {
                arrayList.add(cls2);
                return arrayList;
            }
            for (a<?, ?> aVar : this.f3587a) {
                if (aVar.a(cls, cls2)) {
                    arrayList.add(cls2);
                }
            }
            return arrayList;
        }
    }

    @DexIgnore
    public <Z, R> void c(Class<Z> cls, Class<R> cls2, th1<Z, R> th1) {
        synchronized (this) {
            this.f3587a.add(new a<>(cls, cls2, th1));
        }
    }
}
