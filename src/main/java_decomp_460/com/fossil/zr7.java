package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr7 extends xr7 {
    /*
    static {
        new zr7(1, 0);
    }
    */

    @DexIgnore
    public zr7(long j, long j2) {
        super(j, j2, 1);
    }

    @DexIgnore
    public boolean e(long j) {
        return a() <= j && j <= b();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof zr7) {
            if (!isEmpty() || !((zr7) obj).isEmpty()) {
                zr7 zr7 = (zr7) obj;
                if (!(a() == zr7.a() && b() == zr7.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * (a() ^ (a() >>> 32))) + (b() ^ (b() >>> 32)));
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
