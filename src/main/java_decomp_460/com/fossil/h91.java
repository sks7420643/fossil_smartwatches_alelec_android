package com.fossil;

import android.annotation.TargetApi;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h91 extends Thread {
    @DexIgnore
    public /* final */ BlockingQueue<m91<?>> b;
    @DexIgnore
    public /* final */ g91 c;
    @DexIgnore
    public /* final */ a91 d;
    @DexIgnore
    public /* final */ p91 e;
    @DexIgnore
    public volatile boolean f; // = false;

    @DexIgnore
    public h91(BlockingQueue<m91<?>> blockingQueue, g91 g91, a91 a91, p91 p91) {
        this.b = blockingQueue;
        this.c = g91;
        this.d = a91;
        this.e = p91;
    }

    @DexIgnore
    @TargetApi(14)
    public final void a(m91<?> m91) {
        if (Build.VERSION.SDK_INT >= 14) {
            TrafficStats.setThreadStatsTag(m91.getTrafficStatsTag());
        }
    }

    @DexIgnore
    public final void b(m91<?> m91, t91 t91) {
        this.e.c(m91, m91.parseNetworkError(t91));
    }

    @DexIgnore
    public final void c() throws InterruptedException {
        d(this.b.take());
    }

    @DexIgnore
    public void d(m91<?> m91) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            m91.addMarker("network-queue-take");
            if (m91.isCanceled()) {
                m91.finish("network-discard-cancelled");
                m91.notifyListenerResponseNotUsable();
                return;
            }
            a(m91);
            j91 a2 = this.c.a(m91);
            m91.addMarker("network-http-complete");
            if (!a2.e || !m91.hasHadResponseDelivered()) {
                o91<?> parseNetworkResponse = m91.parseNetworkResponse(a2);
                m91.addMarker("network-parse-complete");
                if (m91.shouldCache() && parseNetworkResponse.b != null) {
                    this.d.c(m91.getCacheKey(), parseNetworkResponse.b);
                    m91.addMarker("network-cache-written");
                }
                m91.markDelivered();
                this.e.a(m91, parseNetworkResponse);
                m91.notifyListenerResponseReceived(parseNetworkResponse);
                return;
            }
            m91.finish("not-modified");
            m91.notifyListenerResponseNotUsable();
        } catch (t91 e2) {
            e2.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            b(m91, e2);
            m91.notifyListenerResponseNotUsable();
        } catch (Exception e3) {
            u91.d(e3, "Unhandled exception %s", e3.toString());
            t91 t91 = new t91(e3);
            t91.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.e.c(m91, t91);
            m91.notifyListenerResponseNotUsable();
        }
    }

    @DexIgnore
    public void e() {
        this.f = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                c();
            } catch (InterruptedException e2) {
                if (this.f) {
                    Thread.currentThread().interrupt();
                    return;
                }
                u91.c("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }
}
