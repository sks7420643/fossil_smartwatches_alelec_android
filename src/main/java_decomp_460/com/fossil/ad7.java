package com.fossil;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import com.fossil.rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;

public class ad7 extends rd7 {
    public static final UriMatcher b;

    /* renamed from: a  reason: collision with root package name */
    public final Context f254a;

    @TargetApi(14)
    public static class a {
        public static InputStream a(ContentResolver contentResolver, Uri uri) {
            return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
        }
    }

    /*
    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        b = uriMatcher;
        uriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        b.addURI("com.android.contacts", "contacts/lookup/*", 1);
        b.addURI("com.android.contacts", "contacts/#/photo", 2);
        b.addURI("com.android.contacts", "contacts/#", 3);
        b.addURI("com.android.contacts", "display_photo/#", 4);
    }
    */

    public ad7(Context context) {
        this.f254a = context;
    }

    @Override // com.fossil.rd7
    public boolean c(pd7 pd7) {
        Uri uri = pd7.d;
        return "content".equals(uri.getScheme()) && ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) && b.match(pd7.d) != -1;
    }

    @Override // com.fossil.rd7
    public rd7.a f(pd7 pd7, int i) throws IOException {
        InputStream j = j(pd7);
        if (j != null) {
            return new rd7.a(j, Picasso.LoadedFrom.DISK);
        }
        return null;
    }

    public final InputStream j(pd7 pd7) throws IOException {
        ContentResolver contentResolver = this.f254a.getContentResolver();
        Uri uri = pd7.d;
        int match = b.match(uri);
        if (match != 1) {
            if (match != 2) {
                if (match != 3) {
                    if (match != 4) {
                        throw new IllegalStateException("Invalid uri: " + uri);
                    }
                }
            }
            return contentResolver.openInputStream(uri);
        }
        uri = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (uri == null) {
            return null;
        }
        return Build.VERSION.SDK_INT < 14 ? ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri) : a.a(contentResolver, uri);
    }
}
