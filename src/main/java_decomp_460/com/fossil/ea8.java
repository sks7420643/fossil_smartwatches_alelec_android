package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import com.fossil.qa8;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ea8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f908a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<byte[], tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 $resultHandler;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ya8 ya8, Bitmap bitmap) {
            super(1);
            this.$resultHandler = ya8;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(byte[] bArr) {
            invoke(bArr);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(byte[] bArr) {
            this.$resultHandler.c(bArr);
            Bitmap bitmap = this.$bitmap;
            if (bitmap != null) {
                bitmap.recycle();
            }
        }
    }

    @DexIgnore
    public ea8(Context context) {
        pq7.c(context, "context");
        this.b = context;
    }

    @DexIgnore
    public final void a(String str, ya8 ya8) {
        pq7.c(str, "id");
        pq7.c(ya8, "resultHandler");
        ya8.c(Boolean.valueOf(g().b(this.b, str)));
    }

    @DexIgnore
    public final void b() {
        g().l();
    }

    @DexIgnore
    public final List<String> c(List<String> list) {
        pq7.c(list, "ids");
        return g().d(this.b, list);
    }

    @DexIgnore
    public final boolean d() {
        return Build.VERSION.SDK_INT >= 29;
    }

    @DexIgnore
    public final List<ka8> e(String str, int i, int i2, int i3, long j, la8 la8) {
        pq7.c(str, "galleryId");
        pq7.c(la8, "option");
        return qa8.b.d(g(), this.b, pq7.a(str, "isAll") ? "" : str, i, i2, i3, j, la8, null, 128, null);
    }

    @DexIgnore
    public final List<ka8> f(String str, int i, int i2, int i3, long j, la8 la8) {
        pq7.c(str, "galleryId");
        pq7.c(la8, "option");
        return g().e(this.b, pq7.a(str, "isAll") ? "" : str, i2, i3, i, j, la8);
    }

    @DexIgnore
    public final qa8 g() {
        return (this.f908a || Build.VERSION.SDK_INT < 29) ? pa8.f : na8.e;
    }

    @DexIgnore
    public final void h(String str, boolean z, ya8 ya8) {
        pq7.c(str, "id");
        pq7.c(ya8, "resultHandler");
        ya8.c(g().h(this.b, str, z));
    }

    @DexIgnore
    public final List<ma8> i(int i, long j, boolean z, la8 la8) {
        pq7.c(la8, "option");
        List<ma8> c = g().c(this.b, i, j, la8);
        if (!z) {
            return c;
        }
        int i2 = 0;
        for (ma8 ma8 : c) {
            i2 += ma8.b();
        }
        return pm7.V(gm7.b(new ma8("isAll", "Recent", i2, i, true)), c);
    }

    @DexIgnore
    public final Map<String, Double> j(String str) {
        pq7.c(str, "id");
        eq0 m = g().m(this.b, str);
        double[] k = m != null ? m.k() : null;
        if (k == null) {
            return zm7.j(hl7.a(Constants.LAT, Double.valueOf(0.0d)), hl7.a("lng", Double.valueOf(0.0d)));
        }
        return zm7.j(hl7.a(Constants.LAT, Double.valueOf(k[0])), hl7.a("lng", Double.valueOf(k[1])));
    }

    @DexIgnore
    public final void k(String str, boolean z, boolean z2, ya8 ya8) {
        pq7.c(str, "id");
        pq7.c(ya8, "resultHandler");
        ka8 j = g().j(this.b, str);
        if (j == null) {
            ya8.e(ya8, "The asset not found", null, null, 6, null);
        } else if (!qa8.f2953a.e()) {
            ya8.c(ap7.a(new File(j.i())));
        } else {
            byte[] f = g().f(this.b, j, z2);
            ya8.c(f);
            if (z) {
                g().a(this.b, j, f);
            }
        }
    }

    @DexIgnore
    public final ma8 l(String str, int i, long j, la8 la8) {
        pq7.c(str, "id");
        pq7.c(la8, "option");
        if (!pq7.a(str, "isAll")) {
            return g().o(this.b, str, i, j, la8);
        }
        List<ma8> c = g().c(this.b, i, j, la8);
        if (c.isEmpty()) {
            return null;
        }
        int i2 = 0;
        for (ma8 ma8 : c) {
            i2 += ma8.b();
        }
        return new ma8("isAll", "Recent", i2, i, true);
    }

    @DexIgnore
    public final void m(String str, int i, int i2, int i3, ya8 ya8) {
        Integer num = null;
        pq7.c(str, "id");
        pq7.c(ya8, "resultHandler");
        try {
            if (!d()) {
                ka8 j = g().j(this.b, str);
                if (j == null) {
                    ya8.e(ya8, "The asset not found!", null, null, 6, null);
                } else {
                    wa8.f3910a.b(this.b, j.i(), i, i2, i3, ya8.a());
                }
            } else {
                ka8 j2 = g().j(this.b, str);
                if (j2 != null) {
                    num = Integer.valueOf(j2.j());
                }
                Bitmap g = g().g(this.b, str, i, i2, num);
                wa8.f3910a.a(this.b, g, i, i2, i3, new a(ya8, g));
            }
        } catch (Exception e) {
            Log.e("PhotoManagerPlugin", "get thumb error", e);
            ya8.d("201", "get thumb error", e);
        }
    }

    @DexIgnore
    public final ka8 n(byte[] bArr, String str, String str2) {
        pq7.c(bArr, "image");
        pq7.c(str, "title");
        pq7.c(str2, "description");
        return g().i(this.b, bArr, str, str2);
    }

    @DexIgnore
    public final ka8 o(String str, String str2, String str3) {
        pq7.c(str, "path");
        pq7.c(str2, "title");
        pq7.c(str3, Constants.DESC);
        if (!new File(str).exists()) {
            return null;
        }
        return g().k(this.b, new FileInputStream(str), str2, str3);
    }

    @DexIgnore
    public final void p(boolean z) {
        this.f908a = z;
    }
}
