package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ci2 implements Parcelable.Creator<zh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zh2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        Long l = null;
        ii2 ii2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j = 0;
        long j2 = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    j2 = ad2.y(parcel, t);
                    break;
                case 2:
                    j = ad2.y(parcel, t);
                    break;
                case 3:
                    str3 = ad2.f(parcel, t);
                    break;
                case 4:
                    str2 = ad2.f(parcel, t);
                    break;
                case 5:
                    str = ad2.f(parcel, t);
                    break;
                case 6:
                default:
                    ad2.B(parcel, t);
                    break;
                case 7:
                    i = ad2.v(parcel, t);
                    break;
                case 8:
                    ii2 = (ii2) ad2.e(parcel, t, ii2.CREATOR);
                    break;
                case 9:
                    l = ad2.z(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new zh2(j2, j, str3, str2, str, i, ii2, l);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zh2[] newArray(int i) {
        return new zh2[i];
    }
}
