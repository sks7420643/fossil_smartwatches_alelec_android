package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class se4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Executor f3248a; // = re4.b;

    @DexIgnore
    public static Executor a() {
        return f3248a;
    }

    @DexIgnore
    public static ExecutorService b() {
        return new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), new sf2("firebase-iid-executor"));
    }
}
