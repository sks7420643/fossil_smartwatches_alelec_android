package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy3<S> extends ly3<S> {
    @DexIgnore
    public static /* final */ Object m; // = "MONTHS_VIEW_GROUP_TAG";
    @DexIgnore
    public static /* final */ Object s; // = "NAVIGATION_PREV_TAG";
    @DexIgnore
    public static /* final */ Object t; // = "NAVIGATION_NEXT_TAG";
    @DexIgnore
    public static /* final */ Object u; // = "SELECTOR_TOGGLE_TAG";
    @DexIgnore
    public int c;
    @DexIgnore
    public zx3<S> d;
    @DexIgnore
    public wx3 e;
    @DexIgnore
    public hy3 f;
    @DexIgnore
    public k g;
    @DexIgnore
    public yx3 h;
    @DexIgnore
    public RecyclerView i;
    @DexIgnore
    public RecyclerView j;
    @DexIgnore
    public View k;
    @DexIgnore
    public View l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public a(int i) {
            this.b = i;
        }

        @DexIgnore
        public void run() {
            dy3.this.j.smoothScrollToPosition(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends rn0 {
        @DexIgnore
        public b(dy3 dy3) {
        }

        @DexIgnore
        @Override // com.fossil.rn0
        public void g(View view, yo0 yo0) {
            super.g(view, yo0);
            yo0.e0(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends my3 {
        @DexIgnore
        public /* final */ /* synthetic */ int I;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Context context, int i, boolean z, int i2) {
            super(context, i, z);
            this.I = i2;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.LinearLayoutManager
        public void N1(RecyclerView.State state, int[] iArr) {
            if (this.I == 0) {
                iArr[0] = dy3.this.j.getWidth();
                iArr[1] = dy3.this.j.getWidth();
                return;
            }
            iArr[0] = dy3.this.j.getHeight();
            iArr[1] = dy3.this.j.getHeight();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements l {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.dy3.l
        public void a(long j) {
            if (dy3.this.e.e().S(j)) {
                dy3.this.d.i0(j);
                Iterator<ky3<S>> it = dy3.this.b.iterator();
                while (it.hasNext()) {
                    it.next().a((S) dy3.this.d.b0());
                }
                dy3.this.j.getAdapter().notifyDataSetChanged();
                if (dy3.this.i != null) {
                    dy3.this.i.getAdapter().notifyDataSetChanged();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends RecyclerView.l {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Calendar f849a; // = ny3.k();
        @DexIgnore
        public /* final */ Calendar b; // = ny3.k();

        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.l
        public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
            if ((recyclerView.getAdapter() instanceof oy3) && (recyclerView.getLayoutManager() instanceof GridLayoutManager)) {
                oy3 oy3 = (oy3) recyclerView.getAdapter();
                GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                for (ln0<Long, Long> ln0 : dy3.this.d.o()) {
                    F f = ln0.f2221a;
                    if (!(f == null || ln0.b == null)) {
                        this.f849a.setTimeInMillis(f.longValue());
                        this.b.setTimeInMillis(ln0.b.longValue());
                        int i = oy3.i(this.f849a.get(1));
                        int i2 = oy3.i(this.b.get(1));
                        View D = gridLayoutManager.D(i);
                        View D2 = gridLayoutManager.D(i2);
                        int Z2 = i / gridLayoutManager.Z2();
                        int Z22 = i2 / gridLayoutManager.Z2();
                        int i3 = Z2;
                        while (i3 <= Z22) {
                            View D3 = gridLayoutManager.D(gridLayoutManager.Z2() * i3);
                            if (D3 != null) {
                                canvas.drawRect((float) (i3 == Z2 ? D.getLeft() + (D.getWidth() / 2) : 0), (float) (D3.getTop() + dy3.this.h.d.c()), (float) (i3 == Z22 ? D2.getLeft() + (D2.getWidth() / 2) : recyclerView.getWidth()), (float) (D3.getBottom() - dy3.this.h.d.b()), dy3.this.h.h);
                            }
                            i3++;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends rn0 {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        @Override // com.fossil.rn0
        public void g(View view, yo0 yo0) {
            super.g(view, yo0);
            yo0.o0(dy3.this.l.getVisibility() == 0 ? dy3.this.getString(rw3.mtrl_picker_toggle_to_year_selection) : dy3.this.getString(rw3.mtrl_picker_toggle_to_day_selection));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends RecyclerView.q {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jy3 f850a;
        @DexIgnore
        public /* final */ /* synthetic */ MaterialButton b;

        @DexIgnore
        public g(jy3 jy3, MaterialButton materialButton) {
            this.f850a = jy3;
            this.b = materialButton;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            if (i == 0) {
                CharSequence text = this.b.getText();
                if (Build.VERSION.SDK_INT >= 16) {
                    recyclerView.announceForAccessibility(text);
                } else {
                    recyclerView.sendAccessibilityEvent(2048);
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            int a2 = i < 0 ? dy3.this.L6().a2() : dy3.this.L6().d2();
            dy3.this.f = this.f850a.h(a2);
            this.b.setText(this.f850a.i(a2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements View.OnClickListener {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void onClick(View view) {
            dy3.this.Q6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jy3 b;

        @DexIgnore
        public i(jy3 jy3) {
            this.b = jy3;
        }

        @DexIgnore
        public void onClick(View view) {
            int a2 = dy3.this.L6().a2() + 1;
            if (a2 < dy3.this.j.getAdapter().getItemCount()) {
                dy3.this.O6(this.b.h(a2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jy3 b;

        @DexIgnore
        public j(jy3 jy3) {
            this.b = jy3;
        }

        @DexIgnore
        public void onClick(View view) {
            int d2 = dy3.this.L6().d2() - 1;
            if (d2 >= 0) {
                dy3.this.O6(this.b.h(d2));
            }
        }
    }

    @DexIgnore
    public enum k {
        DAY,
        YEAR
    }

    @DexIgnore
    public interface l {
        @DexIgnore
        void a(long j);
    }

    @DexIgnore
    public static int K6(Context context) {
        return context.getResources().getDimensionPixelSize(lw3.mtrl_calendar_day_height);
    }

    @DexIgnore
    public static <T> dy3<T> M6(zx3<T> zx3, int i2, wx3 wx3) {
        dy3<T> dy3 = new dy3<>();
        Bundle bundle = new Bundle();
        bundle.putInt("THEME_RES_ID_KEY", i2);
        bundle.putParcelable("GRID_SELECTOR_KEY", zx3);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", wx3);
        bundle.putParcelable("CURRENT_MONTH_KEY", wx3.h());
        dy3.setArguments(bundle);
        return dy3;
    }

    @DexIgnore
    public final void E6(View view, jy3 jy3) {
        MaterialButton materialButton = (MaterialButton) view.findViewById(nw3.month_navigation_fragment_toggle);
        materialButton.setTag(u);
        mo0.l0(materialButton, new f());
        MaterialButton materialButton2 = (MaterialButton) view.findViewById(nw3.month_navigation_previous);
        materialButton2.setTag(s);
        MaterialButton materialButton3 = (MaterialButton) view.findViewById(nw3.month_navigation_next);
        materialButton3.setTag(t);
        this.k = view.findViewById(nw3.mtrl_calendar_year_selector_frame);
        this.l = view.findViewById(nw3.mtrl_calendar_day_selector_frame);
        P6(k.DAY);
        materialButton.setText(this.f.h());
        this.j.addOnScrollListener(new g(jy3, materialButton));
        materialButton.setOnClickListener(new h());
        materialButton3.setOnClickListener(new i(jy3));
        materialButton2.setOnClickListener(new j(jy3));
    }

    @DexIgnore
    public final RecyclerView.l F6() {
        return new e();
    }

    @DexIgnore
    public wx3 G6() {
        return this.e;
    }

    @DexIgnore
    public yx3 H6() {
        return this.h;
    }

    @DexIgnore
    public hy3 I6() {
        return this.f;
    }

    @DexIgnore
    public zx3<S> J6() {
        return this.d;
    }

    @DexIgnore
    public LinearLayoutManager L6() {
        return (LinearLayoutManager) this.j.getLayoutManager();
    }

    @DexIgnore
    public final void N6(int i2) {
        this.j.post(new a(i2));
    }

    @DexIgnore
    public void O6(hy3 hy3) {
        jy3 jy3 = (jy3) this.j.getAdapter();
        int j2 = jy3.j(hy3);
        int j3 = j2 - jy3.j(this.f);
        boolean z = Math.abs(j3) > 3;
        boolean z2 = j3 > 0;
        this.f = hy3;
        if (z && z2) {
            this.j.scrollToPosition(j2 - 3);
            N6(j2);
        } else if (z) {
            this.j.scrollToPosition(j2 + 3);
            N6(j2);
        } else {
            N6(j2);
        }
    }

    @DexIgnore
    public void P6(k kVar) {
        this.g = kVar;
        if (kVar == k.YEAR) {
            this.i.getLayoutManager().y1(((oy3) this.i.getAdapter()).i(this.f.e));
            this.k.setVisibility(0);
            this.l.setVisibility(8);
        } else if (kVar == k.DAY) {
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            O6(this.f);
        }
    }

    @DexIgnore
    public void Q6() {
        k kVar = this.g;
        if (kVar == k.YEAR) {
            P6(k.DAY);
        } else if (kVar == k.DAY) {
            P6(k.YEAR);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.c = bundle.getInt("THEME_RES_ID_KEY");
        this.d = (zx3) bundle.getParcelable("GRID_SELECTOR_KEY");
        this.e = (wx3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.f = (hy3) bundle.getParcelable("CURRENT_MONTH_KEY");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i2;
        int i3;
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), this.c);
        this.h = new yx3(contextThemeWrapper);
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(contextThemeWrapper);
        hy3 i4 = this.e.i();
        if (ey3.K6(contextThemeWrapper)) {
            i2 = pw3.mtrl_calendar_vertical;
            i3 = 1;
        } else {
            i2 = pw3.mtrl_calendar_horizontal;
            i3 = 0;
        }
        View inflate = cloneInContext.inflate(i2, viewGroup, false);
        GridView gridView = (GridView) inflate.findViewById(nw3.mtrl_calendar_days_of_week);
        mo0.l0(gridView, new b(this));
        gridView.setAdapter((ListAdapter) new cy3());
        gridView.setNumColumns(i4.f);
        gridView.setEnabled(false);
        this.j = (RecyclerView) inflate.findViewById(nw3.mtrl_calendar_months);
        this.j.setLayoutManager(new c(getContext(), i3, false, i3));
        this.j.setTag(m);
        jy3 jy3 = new jy3(contextThemeWrapper, this.d, this.e, new d());
        this.j.setAdapter(jy3);
        int integer = contextThemeWrapper.getResources().getInteger(ow3.mtrl_calendar_year_selector_span);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(nw3.mtrl_calendar_year_selector_frame);
        this.i = recyclerView;
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            this.i.setLayoutManager(new GridLayoutManager((Context) contextThemeWrapper, integer, 1, false));
            this.i.setAdapter(new oy3(this));
            this.i.addItemDecoration(F6());
        }
        if (inflate.findViewById(nw3.month_navigation_fragment_toggle) != null) {
            E6(inflate, jy3);
        }
        if (!ey3.K6(contextThemeWrapper)) {
            new hv0().b(this.j);
        }
        this.j.scrollToPosition(jy3.j(this.f));
        return inflate;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("THEME_RES_ID_KEY", this.c);
        bundle.putParcelable("GRID_SELECTOR_KEY", this.d);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.e);
        bundle.putParcelable("CURRENT_MONTH_KEY", this.f);
    }
}
