package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he3 extends me3 {
    @DexIgnore
    public he3() {
        super(1, null);
    }

    @DexIgnore
    @Override // com.fossil.me3
    public final String toString() {
        return "[Dot]";
    }
}
