package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc1 {
    @DexIgnore
    public static boolean a(Uri uri) {
        return b(uri) && !e(uri);
    }

    @DexIgnore
    public static boolean b(Uri uri) {
        return uri != null && "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    @DexIgnore
    public static boolean c(Uri uri) {
        return b(uri) && e(uri);
    }

    @DexIgnore
    public static boolean d(int i, int i2) {
        return i != Integer.MIN_VALUE && i2 != Integer.MIN_VALUE && i <= 512 && i2 <= 384;
    }

    @DexIgnore
    public static boolean e(Uri uri) {
        return uri.getPathSegments().contains("video");
    }
}
