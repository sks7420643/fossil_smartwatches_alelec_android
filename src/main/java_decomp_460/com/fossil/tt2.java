package com.fossil;

import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ r93 g;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tt2(zs2 zs2, String str, r93 r93) {
        super(zs2);
        this.h = zs2;
        this.f = str;
        this.g = r93;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.h.h.getMaxUserProperties(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void b() {
        this.g.c(null);
    }
}
