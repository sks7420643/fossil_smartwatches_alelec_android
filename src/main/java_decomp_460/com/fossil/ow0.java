package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import com.fossil.kw0;
import com.fossil.lw0;
import com.fossil.nw0;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ow0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2729a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ nw0 d;
    @DexIgnore
    public /* final */ nw0.c e;
    @DexIgnore
    public lw0 f;
    @DexIgnore
    public /* final */ Executor g;
    @DexIgnore
    public /* final */ kw0 h; // = new a();
    @DexIgnore
    public /* final */ AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ServiceConnection j; // = new b();
    @DexIgnore
    public /* final */ Runnable k; // = new c();
    @DexIgnore
    public /* final */ Runnable l; // = new d();
    @DexIgnore
    public /* final */ Runnable m; // = new e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends kw0.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ow0$a$a")
        /* renamed from: com.fossil.ow0$a$a  reason: collision with other inner class name */
        public class RunnableC0182a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String[] b;

            @DexIgnore
            public RunnableC0182a(String[] strArr) {
                this.b = strArr;
            }

            @DexIgnore
            public void run() {
                ow0.this.d.g(this.b);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.kw0
        public void T(String[] strArr) {
            ow0.this.g.execute(new RunnableC0182a(strArr));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ow0.this.f = lw0.a.d(iBinder);
            ow0 ow0 = ow0.this;
            ow0.g.execute(ow0.k);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            ow0 ow0 = ow0.this;
            ow0.g.execute(ow0.l);
            ow0.this.f = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                lw0 lw0 = ow0.this.f;
                if (lw0 != null) {
                    ow0.this.c = lw0.l0(ow0.this.h, ow0.this.b);
                    ow0.this.d.a(ow0.this.e);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            ow0 ow0 = ow0.this;
            ow0.d.j(ow0.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            ow0 ow0 = ow0.this;
            ow0.d.j(ow0.e);
            try {
                lw0 lw0 = ow0.this.f;
                if (lw0 != null) {
                    lw0.M2(ow0.this.h, ow0.this.c);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e);
            }
            ow0 ow02 = ow0.this;
            ow02.f2729a.unbindService(ow02.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends nw0.c {
        @DexIgnore
        public f(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.nw0.c
        public void onInvalidated(Set<String> set) {
            if (!ow0.this.i.get()) {
                try {
                    lw0 lw0 = ow0.this.f;
                    if (lw0 != null) {
                        lw0.E2(ow0.this.c, (String[]) set.toArray(new String[0]));
                    }
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public ow0(Context context, String str, nw0 nw0, Executor executor) {
        this.f2729a = context.getApplicationContext();
        this.b = str;
        this.d = nw0;
        this.g = executor;
        this.e = new f((String[]) nw0.f2584a.keySet().toArray(new String[0]));
        this.f2729a.bindService(new Intent(this.f2729a, MultiInstanceInvalidationService.class), this.j, 1);
    }

    @DexIgnore
    public void a() {
        if (this.i.compareAndSet(false, true)) {
            this.g.execute(this.m);
        }
    }
}
