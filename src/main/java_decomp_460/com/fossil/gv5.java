package com.fossil;

import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ wn5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return gv5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeakReference<ls5> f1376a;

        @DexIgnore
        public b(WeakReference<ls5> weakReference) {
            pq7.c(weakReference, "activityContext");
            this.f1376a = weakReference;
        }

        @DexIgnore
        public final WeakReference<ls5> a() {
            return this.f1376a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1377a;
        @DexIgnore
        public /* final */ z52 b;

        @DexIgnore
        public c(int i, z52 z52) {
            this.f1377a = i;
            this.b = z52;
        }

        @DexIgnore
        public final int a() {
            return this.f1377a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ SignUpSocialAuth f1378a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            this.f1378a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.f1378a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements yn5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv5 f1379a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(gv5 gv5) {
            this.f1379a = gv5;
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void a(SignUpSocialAuth signUpSocialAuth) {
            pq7.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = gv5.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.f1379a.j(new d(signUpSocialAuth));
        }

        @DexIgnore
        @Override // com.fossil.yn5
        public void b(int i, z52 z52, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = gv5.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + z52);
            this.f1379a.i(new c(i, z52));
        }
    }

    /*
    static {
        String simpleName = gv5.class.getSimpleName();
        pq7.b(simpleName, "LoginWechatUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public gv5(wn5 wn5) {
        pq7.c(wn5, "mLoginWechatManager");
        this.d = wn5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return e;
    }

    @DexIgnore
    /* renamed from: n */
    public Object k(b bVar, qn7<Object> qn7) {
        String str;
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            wn5 wn5 = this.d;
            Access c2 = SoLibraryLoader.f().c(PortfolioApp.h0.c());
            if (c2 == null || (str = c2.getD()) == null) {
                str = "";
            }
            wn5.j(str);
            wn5 wn52 = this.d;
            WeakReference<ls5> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                ls5 ls5 = a2.get();
                if (ls5 != null) {
                    pq7.b(ls5, "requestValues?.activityContext!!.get()!!");
                    wn52.f(ls5, new e(this));
                    return tl7.f3441a;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "Inside .run failed with exception=" + e2);
            return new c(600, null);
        }
    }
}
