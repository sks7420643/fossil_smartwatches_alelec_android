package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ff1<Data> implements af1<Integer, Data> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ af1<Uri, Data> f1115a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements bf1<Integer, AssetFileDescriptor> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Resources f1116a;

        @DexIgnore
        public a(Resources resources) {
            this.f1116a = resources;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Integer, AssetFileDescriptor> b(ef1 ef1) {
            return new ff1(this.f1116a, ef1.d(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements bf1<Integer, ParcelFileDescriptor> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Resources f1117a;

        @DexIgnore
        public b(Resources resources) {
            this.f1117a = resources;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Integer, ParcelFileDescriptor> b(ef1 ef1) {
            return new ff1(this.f1117a, ef1.d(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements bf1<Integer, InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Resources f1118a;

        @DexIgnore
        public c(Resources resources) {
            this.f1118a = resources;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Integer, InputStream> b(ef1 ef1) {
            return new ff1(this.f1118a, ef1.d(Uri.class, InputStream.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements bf1<Integer, Uri> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Resources f1119a;

        @DexIgnore
        public d(Resources resources) {
            this.f1119a = resources;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Integer, Uri> b(ef1 ef1) {
            return new ff1(this.f1119a, if1.c());
        }
    }

    @DexIgnore
    public ff1(Resources resources, af1<Uri, Data> af1) {
        this.b = resources;
        this.f1115a = af1;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<Data> b(Integer num, int i, int i2, ob1 ob1) {
        Uri d2 = d(num);
        if (d2 == null) {
            return null;
        }
        return this.f1115a.b(d2, i, i2, ob1);
    }

    @DexIgnore
    public final Uri d(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            }
            return null;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public boolean a(Integer num) {
        return true;
    }
}
