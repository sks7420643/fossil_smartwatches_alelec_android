package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yd3 extends ds2 implements xd3 {
    @DexIgnore
    public yd3() {
        super("com.google.android.gms.maps.internal.IOnCircleClickListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        C(js2.e(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
