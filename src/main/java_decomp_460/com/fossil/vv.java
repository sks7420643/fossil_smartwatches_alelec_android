package com.fossil;

import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv extends uv {
    @DexIgnore
    public byte[] Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public long S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public /* final */ boolean W;
    @DexIgnore
    public /* final */ byte[] X;
    @DexIgnore
    public byte[] Y;
    @DexIgnore
    public /* final */ long Z;
    @DexIgnore
    public /* final */ float a0;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ vv(short r7, long r8, com.fossil.k5 r10, int r11, float r12, int r13) {
        /*
            r6 = this;
            r0 = r13 & 8
            if (r0 == 0) goto L_0x0074
            r5 = 3
        L_0x0005:
            r0 = r13 & 16
            if (r0 == 0) goto L_0x000c
            r12 = 981668463(0x3a83126f, float:0.001)
        L_0x000c:
            com.fossil.sv r1 = com.fossil.sv.LEGACY_GET_FILE
            com.fossil.hs r3 = com.fossil.hs.W
            r0 = r6
            r2 = r7
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            r6.Z = r8
            r6.a0 = r12
            r0 = 0
            byte[] r0 = new byte[r0]
            r6.Q = r0
            r0 = 65535(0xffff, float:9.1834E-41)
            r6.U = r0
            r0 = 11
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.allocate(r0)
            java.nio.ByteOrder r1 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r0 = r0.order(r1)
            com.fossil.sv r1 = com.fossil.sv.LEGACY_GET_FILE
            byte r1 = r1.b
            java.nio.ByteBuffer r0 = r0.put(r1)
            java.nio.ByteBuffer r0 = r0.putShort(r7)
            int r1 = r6.T
            java.nio.ByteBuffer r0 = r0.putInt(r1)
            int r1 = r6.U
            java.nio.ByteBuffer r0 = r0.putInt(r1)
            byte[] r0 = r0.array()
            java.lang.String r1 = "ByteBuffer.allocate(11)\n\u2026gth)\n            .array()"
            com.fossil.pq7.b(r0, r1)
            r6.X = r0
            r0 = 1
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.allocate(r0)
            java.nio.ByteOrder r1 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r0 = r0.order(r1)
            com.fossil.sv r1 = com.fossil.sv.LEGACY_GET_FILE
            byte r1 = r1.a()
            java.nio.ByteBuffer r0 = r0.put(r1)
            byte[] r0 = r0.array()
            java.lang.String r1 = "ByteBuffer.allocate(1)\n \u2026e())\n            .array()"
            com.fossil.pq7.b(r0, r1)
            r6.Y = r0
            return
        L_0x0074:
            r5 = r11
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vv.<init>(short, long, com.fossil.k5, int, float, int):void");
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(g80.k(super.A(), jd0.J, Long.valueOf(ix1.f1688a.b(this.Q, ix1.a.CRC32))), jd0.d1, Integer.valueOf(this.Q.length));
    }

    @DexIgnore
    @Override // com.fossil.ps, com.fossil.tv
    public byte[] M() {
        return this.X;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.W;
    }

    @DexIgnore
    @Override // com.fossil.ps, com.fossil.tv
    public byte[] P() {
        return this.Y;
    }

    @DexIgnore
    @Override // com.fossil.tv
    public void Q(byte[] bArr) {
        this.Y = bArr;
    }

    @DexIgnore
    @Override // com.fossil.uv
    public void S(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.R = order.getShort(0);
        this.S = hy1.o(order.getInt(bArr.length - 4));
        if (!(this.R == R() || this.S == ix1.f1688a.a(bArr, 0, bArr.length - 4, ix1.a.CRC32))) {
            this.v = mw.a(this.v, null, null, lw.l, null, null, 27);
        }
        this.Q = bArr;
    }

    @DexIgnore
    @Override // com.fossil.uv
    public void T() {
        float length = (((float) this.Q.length) * 1.0f) / ((float) this.Z);
        if (length - this.V > this.a0 || length == 1.0f) {
            this.V = length;
            e(length);
        }
    }

    @DexIgnore
    @Override // com.fossil.tv, com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(g80.k(super.z(), jd0.i3, Long.valueOf(this.Z)), jd0.c1, Integer.valueOf(this.T)), jd0.d1, Integer.valueOf(this.U));
    }
}
