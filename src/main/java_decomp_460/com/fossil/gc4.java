package com.fossil;

import com.fossil.ec4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gc4 implements hc4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ic4 f1290a;
    @DexIgnore
    public /* final */ jc4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f1291a;

        /*
        static {
            int[] iArr = new int[ec4.a.values().length];
            f1291a = iArr;
            try {
                iArr[ec4.a.JAVA.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1291a[ec4.a.NATIVE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public gc4(ic4 ic4, jc4 jc4) {
        this.f1290a = ic4;
        this.b = jc4;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public boolean b(cc4 cc4, boolean z) {
        int i = a.f1291a[cc4.c.getType().ordinal()];
        if (i == 1) {
            this.f1290a.b(cc4, z);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.b.b(cc4, z);
            return true;
        }
    }
}
