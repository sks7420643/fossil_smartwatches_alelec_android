package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.o71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"MissingPermission"})
public final class p71 implements o71 {
    @DexIgnore
    public static /* final */ IntentFilter e; // = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ ConnectivityManager d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ p71 f2791a;
        @DexIgnore
        public /* final */ /* synthetic */ o71.b b;

        @DexIgnore
        public a(p71 p71, o71.b bVar) {
            this.f2791a = p71;
            this.b = bVar;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            if (pq7.a(intent != null ? intent.getAction() : null, "android.net.conn.CONNECTIVITY_CHANGE")) {
                this.b.a(this.f2791a.a());
            }
        }
    }

    @DexIgnore
    public p71(Context context, ConnectivityManager connectivityManager, o71.b bVar) {
        pq7.c(context, "context");
        pq7.c(connectivityManager, "connectivityManager");
        pq7.c(bVar, "listener");
        this.c = context;
        this.d = connectivityManager;
        this.b = new a(this, bVar);
    }

    @DexIgnore
    @Override // com.fossil.o71
    public boolean a() {
        NetworkInfo activeNetworkInfo = this.d.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @DexIgnore
    @Override // com.fossil.o71
    public void start() {
        this.c.registerReceiver(this.b, e);
    }

    @DexIgnore
    @Override // com.fossil.o71
    public void stop() {
        this.c.unregisterReceiver(this.b);
    }
}
