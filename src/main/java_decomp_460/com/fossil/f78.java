package com.fossil;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f78 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static volatile int f1070a;
    @DexIgnore
    public static /* final */ q78 b; // = new q78();
    @DexIgnore
    public static /* final */ n78 c; // = new n78();
    @DexIgnore
    public static /* final */ String[] d; // = {"1.6", "1.7"};
    @DexIgnore
    public static String e; // = "org/slf4j/impl/StaticLoggerBinder.class";

    /*
    static {
        r78.c("slf4j.detectLoggerNameMismatch");
    }
    */

    @DexIgnore
    public static final void a() {
        Set<URL> set = null;
        try {
            if (!k()) {
                set = f();
                q(set);
            }
            s78.c();
            f1070a = 3;
            p(set);
            g();
            n();
            b.b();
        } catch (NoClassDefFoundError e2) {
            if (l(e2.getMessage())) {
                f1070a = 4;
                r78.a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                r78.a("Defaulting to no-operation (NOP) logger implementation");
                r78.a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            e(e2);
            throw e2;
        } catch (NoSuchMethodError e3) {
            String message = e3.getMessage();
            if (message != null && message.contains("org.slf4j.impl.StaticLoggerBinder.getSingleton()")) {
                f1070a = 2;
                r78.a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                r78.a("Your binding is version 1.5.5 or earlier.");
                r78.a("Upgrade your binding to version 1.6.x.");
            }
            throw e3;
        } catch (Exception e4) {
            e(e4);
            throw new IllegalStateException("Unexpected initialization failure", e4);
        }
    }

    @DexIgnore
    public static void b(k78 k78, int i) {
        if (k78.a().d()) {
            c(i);
        } else if (!k78.a().e()) {
            d();
        }
    }

    @DexIgnore
    public static void c(int i) {
        r78.a("A number (" + i + ") of logging calls during the initialization phase have been intercepted and are");
        r78.a("now being replayed. These are subject to the filtering rules of the underlying logging system.");
        r78.a("See also http://www.slf4j.org/codes.html#replay");
    }

    @DexIgnore
    public static void d() {
        r78.a("The following set of substitute loggers may have been accessed");
        r78.a("during the initialization phase. Logging calls during this");
        r78.a("phase were not honored. However, subsequent logging calls to these");
        r78.a("loggers will work as normally expected.");
        r78.a("See also http://www.slf4j.org/codes.html#substituteLogger");
    }

    @DexIgnore
    public static void e(Throwable th) {
        f1070a = 2;
        r78.b("Failed to instantiate SLF4J LoggerFactory", th);
    }

    @DexIgnore
    public static Set<URL> f() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            ClassLoader classLoader = f78.class.getClassLoader();
            Enumeration<URL> systemResources = classLoader == null ? ClassLoader.getSystemResources(e) : classLoader.getResources(e);
            while (systemResources.hasMoreElements()) {
                linkedHashSet.add(systemResources.nextElement());
            }
        } catch (IOException e2) {
            r78.b("Error getting resources from path", e2);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static void g() {
        synchronized (b) {
            b.e();
            for (p78 p78 : b.d()) {
                p78.h(i(p78.c()));
            }
        }
    }

    @DexIgnore
    public static d78 h() {
        if (f1070a == 0) {
            synchronized (f78.class) {
                try {
                    if (f1070a == 0) {
                        f1070a = 1;
                        m();
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        int i = f1070a;
        if (i == 1) {
            return b;
        }
        if (i == 2) {
            throw new IllegalStateException("org.slf4j.LoggerFactory in failed state. Original exception was thrown EARLIER. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
        } else if (i == 3) {
            return s78.c().a();
        } else {
            if (i == 4) {
                return c;
            }
            throw new IllegalStateException("Unreachable code");
        }
    }

    @DexIgnore
    public static e78 i(String str) {
        return h().a(str);
    }

    @DexIgnore
    public static boolean j(Set<URL> set) {
        return set.size() > 1;
    }

    @DexIgnore
    public static boolean k() {
        String d2 = r78.d("java.vendor.url");
        if (d2 == null) {
            return false;
        }
        return d2.toLowerCase().contains("android");
    }

    @DexIgnore
    public static boolean l(String str) {
        if (str == null) {
            return false;
        }
        if (str.contains("org/slf4j/impl/StaticLoggerBinder")) {
            return true;
        }
        return str.contains("org.slf4j.impl.StaticLoggerBinder");
    }

    @DexIgnore
    public static final void m() {
        a();
        if (f1070a == 3) {
            r();
        }
    }

    @DexIgnore
    public static void n() {
        LinkedBlockingQueue<k78> c2 = b.c();
        int size = c2.size();
        ArrayList<k78> arrayList = new ArrayList(128);
        int i = 0;
        while (c2.drainTo(arrayList, 128) != 0) {
            int i2 = i;
            for (k78 k78 : arrayList) {
                o(k78);
                if (i2 == 0) {
                    b(k78, size);
                }
                i2++;
            }
            arrayList.clear();
            i = i2;
        }
    }

    @DexIgnore
    public static void o(k78 k78) {
        if (k78 != null) {
            p78 a2 = k78.a();
            String c2 = a2.c();
            if (a2.f()) {
                throw new IllegalStateException("Delegate logger cannot be null at this state.");
            } else if (a2.e()) {
            } else {
                if (a2.d()) {
                    a2.g(k78);
                } else {
                    r78.a(c2);
                }
            }
        }
    }

    @DexIgnore
    public static void p(Set<URL> set) {
        if (set != null && j(set)) {
            r78.a("Actual binding is of type [" + s78.c().b() + "]");
        }
    }

    @DexIgnore
    public static void q(Set<URL> set) {
        if (j(set)) {
            r78.a("Class path contains multiple SLF4J bindings.");
            Iterator<URL> it = set.iterator();
            while (it.hasNext()) {
                r78.a("Found binding in [" + it.next() + "]");
            }
            r78.a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }

    @DexIgnore
    public static final void r() {
        try {
            String str = s78.c;
            boolean z = false;
            for (String str2 : d) {
                if (str.startsWith(str2)) {
                    z = true;
                }
            }
            if (!z) {
                r78.a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(d).toString());
                r78.a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError e2) {
        } catch (Throwable th) {
            r78.b("Unexpected problem occured during version sanity check", th);
        }
    }
}
