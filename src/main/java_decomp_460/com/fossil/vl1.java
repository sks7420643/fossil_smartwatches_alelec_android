package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vl1 {
    CLOCKWISE((byte) 1),
    COUNTER_CLOCKWISE((byte) 2),
    SHORTEST_PATH((byte) 3);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public vl1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
