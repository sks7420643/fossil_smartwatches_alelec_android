package com.fossil;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cm3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ pm3 f629a;

    @DexIgnore
    public cm3(pm3 pm3) {
        this.f629a = pm3;
    }

    @DexIgnore
    public final Bundle a(String str, bw2 bw2) {
        this.f629a.c().h();
        if (bw2 == null) {
            this.f629a.d().I().a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle c = bw2.c(bundle);
            if (c != null) {
                return c;
            }
            this.f629a.d().F().a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.f629a.d().F().b("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        if (str == null || str.isEmpty()) {
            this.f629a.d().J().a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.f629a.c().h();
        if (!c()) {
            this.f629a.d().L().a("Install Referrer Reporter is not available");
            return;
        }
        fm3 fm3 = new fm3(this, str);
        this.f629a.c().h();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.f629a.e().getPackageManager();
        if (packageManager == null) {
            this.f629a.d().J().a("Failed to obtain Package Manager to verify binding conditions for Install Referrer");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.f629a.d().L().a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (serviceInfo.name == null || !"com.android.vending".equals(str2) || !c()) {
                this.f629a.d().I().a("Play Store version 8.3.73 or higher required for Install Referrer");
                return;
            }
            try {
                this.f629a.d().N().b("Install Referrer Service is", ve2.b().a(this.f629a.e(), new Intent(intent), fm3, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.f629a.d().F().b("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    @DexIgnore
    public final boolean c() {
        try {
            zf2 a2 = ag2.a(this.f629a.e());
            if (a2 != null) {
                return a2.e("com.android.vending", 128).versionCode >= 80837300;
            }
            this.f629a.d().N().a("Failed to get PackageManager for Install Referrer Play Store compatibility check");
            return false;
        } catch (Exception e) {
            this.f629a.d().N().b("Failed to retrieve Play Store version for Install Referrer", e);
            return false;
        }
    }
}
