package com.fossil;

import com.portfolio.platform.data.source.remote.SecureApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sp4 implements Factory<SecureApiService> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f3284a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public sp4(uo4 uo4, Provider<on5> provider) {
        this.f3284a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static sp4 a(uo4 uo4, Provider<on5> provider) {
        return new sp4(uo4, provider);
    }

    @DexIgnore
    public static SecureApiService c(uo4 uo4, on5 on5) {
        SecureApiService z = uo4.z(on5);
        lk7.c(z, "Cannot return null from a non-@Nullable @Provides method");
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public SecureApiService get() {
        return c(this.f3284a, this.b.get());
    }
}
