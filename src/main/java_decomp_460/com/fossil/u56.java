package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u56 implements Factory<p56> {
    @DexIgnore
    public static p56 a(r56 r56) {
        p56 d = r56.d();
        lk7.c(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
