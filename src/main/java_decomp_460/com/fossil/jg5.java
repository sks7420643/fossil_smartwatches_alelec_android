package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;

    @DexIgnore
    public jg5(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
    }

    @DexIgnore
    @Deprecated
    public static jg5 A(LayoutInflater layoutInflater, Object obj) {
        return (jg5) ViewDataBinding.p(layoutInflater, 2131558743, null, false, obj);
    }

    @DexIgnore
    public static jg5 z(LayoutInflater layoutInflater) {
        return A(layoutInflater, aq0.d());
    }
}
