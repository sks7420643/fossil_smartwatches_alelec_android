package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public byte[] f1460a;
    @DexIgnore
    public String b;

    @DexIgnore
    public hb7(byte[] bArr, String str) {
        pq7.c(str, "idOfBackground");
        this.f1460a = bArr;
        this.b = str;
    }

    @DexIgnore
    public final byte[] a() {
        return this.f1460a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(hb7.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.b, ((hb7) obj).b) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFBackground");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "WFBackground(byteData=" + Arrays.toString(this.f1460a) + ", idOfBackground=" + this.b + ")";
    }
}
