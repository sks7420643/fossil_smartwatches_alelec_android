package com.fossil;

import android.content.Context;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cz3 extends cg0 {
    @DexIgnore
    public cz3(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // android.view.Menu, com.fossil.cg0
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        eg0 eg0 = (eg0) a(i, i2, i3, charSequence);
        ez3 ez3 = new ez3(w(), this, eg0);
        eg0.x(ez3);
        return ez3;
    }
}
