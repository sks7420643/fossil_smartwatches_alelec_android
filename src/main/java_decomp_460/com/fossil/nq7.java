package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nq7 extends gq7 implements mq7, gs7 {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public nq7(int i) {
        this.arity = i;
    }

    @DexIgnore
    public nq7(int i, Object obj) {
        super(obj);
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public ds7 computeReflected() {
        er7.a(this);
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof nq7) {
            nq7 nq7 = (nq7) obj;
            if (getOwner() != null ? getOwner().equals(nq7.getOwner()) : nq7.getOwner() == null) {
                if (getName().equals(nq7.getName()) && getSignature().equals(nq7.getSignature()) && pq7.a(getBoundReceiver(), nq7.getBoundReceiver())) {
                    return true;
                }
            }
            return false;
        } else if (obj instanceof gs7) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public gs7 getReflected() {
        return (gs7) super.getReflected();
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner() == null ? 0 : getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.gs7
    public boolean isExternal() {
        return getReflected().isExternal();
    }

    @DexIgnore
    @Override // com.fossil.gs7
    public boolean isInfix() {
        return getReflected().isInfix();
    }

    @DexIgnore
    @Override // com.fossil.gs7
    public boolean isInline() {
        return getReflected().isInline();
    }

    @DexIgnore
    @Override // com.fossil.gs7
    public boolean isOperator() {
        return getReflected().isOperator();
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7, com.fossil.gs7
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    @DexIgnore
    public String toString() {
        ds7 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        if ("<init>".equals(getName())) {
            return "constructor (Kotlin reflection is not available)";
        }
        return "function " + getName() + " (Kotlin reflection is not available)";
    }
}
