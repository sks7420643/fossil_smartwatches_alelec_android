package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.jn5;
import com.fossil.xq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z36 extends qv5 implements y36 {
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<p75> h;
    @DexIgnore
    public x36 i;
    @DexIgnore
    public xq4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final z36 a() {
            return new z36();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements xq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ z36 f4414a;

        @DexIgnore
        public b(z36 z36) {
            this.f4414a = z36;
        }

        @DexIgnore
        @Override // com.fossil.xq4.a
        public void a(Alarm alarm) {
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            z36.K6(this.f4414a).o(alarm, !alarm.isActive());
        }

        @DexIgnore
        @Override // com.fossil.xq4.a
        public void b(Alarm alarm) {
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            z36.K6(this.f4414a).q(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 b;

        @DexIgnore
        public c(z36 z36) {
            this.b = z36;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                pq7.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 b;

        @DexIgnore
        public d(z36 z36) {
            this.b = z36;
        }

        @DexIgnore
        public final void onClick(View view) {
            z36.K6(this.b).q(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 b;

        @DexIgnore
        public e(z36 z36) {
            this.b = z36;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (jn5.c(jn5.b, this.b.getContext(), jn5.a.NOTIFICATION_GET_CONTACTS, false, false, false, null, 60, null)) {
                NotificationDialLandingActivity.B.a(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 b;

        @DexIgnore
        public f(z36 z36) {
            this.b = z36;
        }

        @DexIgnore
        public final void onClick(View view) {
            z36.K6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 b;

        @DexIgnore
        public g(z36 z36) {
            this.b = z36;
        }

        @DexIgnore
        public final void onClick(View view) {
            z36.K6(this.b).n();
        }
    }

    @DexIgnore
    public static final /* synthetic */ x36 K6(z36 z36) {
        x36 x36 = z36.i;
        if (x36 != null) {
            return x36;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HomeAlertsHybridFragment";
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void F(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            g37<p75> g37 = this.h;
            if (g37 != null) {
                p75 a2 = g37.a();
                if (a2 != null && (rTLImageView2 = a2.I) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<p75> g372 = this.h;
        if (g372 != null) {
            p75 a3 = g372.a();
            if (a3 != null && (rTLImageView = a3.I) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(x36 x36) {
        pq7.c(x36, "presenter");
        this.i = x36;
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void N1(boolean z) {
        Context requireContext;
        int i2;
        g37<p75> g37 = this.h;
        if (g37 != null) {
            p75 a2 = g37.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.P;
                pq7.b(flexibleSwitchCompat, "it.swScheduled");
                flexibleSwitchCompat.setChecked(z);
                FlexibleTextView flexibleTextView = a2.C;
                if (z) {
                    requireContext = requireContext();
                    i2 = 2131099968;
                } else {
                    requireContext = requireContext();
                    i2 = 2131100358;
                }
                flexibleTextView.setTextColor(gl0.d(requireContext, i2));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void U() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.Z(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void W() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsHybridFragment", "notifyListAlarm()");
        xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.notifyDataSetChanged();
        } else {
            pq7.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void j4(boolean z) {
        g37<p75> g37 = this.h;
        if (g37 != null) {
            p75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerView recyclerView = a2.O;
                pq7.b(recyclerView, "binding.rvAlarms");
                recyclerView.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "binding.ftvAlarmsSection");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = a2.s;
                pq7.b(flexibleButton, "binding.btnAdd");
                flexibleButton.setVisibility(0);
                return;
            }
            RecyclerView recyclerView2 = a2.O;
            pq7.b(recyclerView2, "binding.rvAlarms");
            recyclerView2.setVisibility(8);
            FlexibleTextView flexibleTextView2 = a2.v;
            pq7.b(flexibleTextView2, "binding.ftvAlarmsSection");
            flexibleTextView2.setVisibility(8);
            FlexibleButton flexibleButton2 = a2.s;
            pq7.b(flexibleButton2, "binding.btnAdd");
            flexibleButton2.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void l0(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        pq7.c(str, "deviceId");
        pq7.c(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.B;
        Context requireContext = requireContext();
        pq7.b(requireContext, "requireContext()");
        aVar.a(requireContext, str, arrayList, alarm);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        p75 p75 = (p75) aq0.f(layoutInflater, 2131558572, viewGroup, false, A6());
        E6("alert_view");
        String d2 = qn5.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d2)) {
            int parseColor = Color.parseColor(d2);
            p75.Q.setBackgroundColor(parseColor);
            p75.R.setBackgroundColor(parseColor);
            p75.q.setBackgroundColor(parseColor);
            p75.r.setBackgroundColor(parseColor);
        }
        String d3 = qn5.l.a().d("onPrimaryButton");
        if (!TextUtils.isEmpty(d3)) {
            int parseColor2 = Color.parseColor(d3);
            Drawable drawable = PortfolioApp.h0.c().getDrawable(2131230986);
            if (drawable != null) {
                drawable.setTint(parseColor2);
                p75.s.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        p75.s.setOnClickListener(new d(this));
        p75.K.setOnClickListener(new e(this));
        p75.P.setOnClickListener(new f(this));
        lg5 lg5 = p75.H;
        if (lg5 != null) {
            ConstraintLayout constraintLayout = lg5.q;
            pq7.b(constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            lg5.t.setImageResource(2131230818);
            FlexibleTextView flexibleTextView = lg5.r;
            pq7.b(flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(um5.c(getContext(), 2131887050));
            lg5.s.setOnClickListener(new c(this));
        }
        p75.I.setOnClickListener(new g(this));
        xq4 xq4 = new xq4();
        xq4.o(new b(this));
        this.j = xq4;
        RecyclerView recyclerView = p75.O;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        xq4 xq42 = this.j;
        if (xq42 != null) {
            recyclerView.setAdapter(xq42);
            g37<p75> g37 = new g37<>(this, p75);
            this.h = g37;
            p75 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        x36 x36 = this.i;
        if (x36 != null) {
            if (x36 != null) {
                x36.m();
                vl5 C6 = C6();
                if (C6 != null) {
                    C6.c("");
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        super.onPause();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        x36 x36 = this.i;
        if (x36 == null) {
            return;
        }
        if (x36 != null) {
            x36.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void p0(List<Alarm> list) {
        pq7.c(list, "alarms");
        xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.n(list);
        } else {
            pq7.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void r(boolean z) {
        ConstraintLayout constraintLayout;
        g37<p75> g37 = this.h;
        if (g37 != null) {
            p75 a2 = g37.a();
            if (a2 != null && (constraintLayout = a2.t) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void u3(boolean z) {
        ConstraintLayout constraintLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeAlertsHybridFragment", "updateAssignNotificationState() - isEnable = " + z);
        g37<p75> g37 = this.h;
        if (g37 != null) {
            p75 a2 = g37.a();
            if (a2 != null && (constraintLayout = a2.K) != null) {
                pq7.b(constraintLayout, "it");
                constraintLayout.setAlpha(z ? 0.5f : 1.0f);
                constraintLayout.setClickable(!z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y36
    public void v0() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.r0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
