package com.fossil;

import com.fossil.b88;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@IgnoreJRERequirement
public final class d88 extends b88.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ b88.a f748a; // = new d88();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class a<R> implements b88<R, CompletableFuture<R>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Type f749a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d88$a$a")
        /* renamed from: com.fossil.d88$a$a  reason: collision with other inner class name */
        public class C0048a extends CompletableFuture<R> {
            @DexIgnore
            public /* final */ /* synthetic */ Call b;

            @DexIgnore
            public C0048a(a aVar, Call call) {
                this.b = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.b.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements c88<R> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ CompletableFuture f750a;

            @DexIgnore
            public b(a aVar, CompletableFuture completableFuture) {
                this.f750a = completableFuture;
            }

            @DexIgnore
            @Override // com.fossil.c88
            public void onFailure(Call<R> call, Throwable th) {
                this.f750a.completeExceptionally(th);
            }

            @DexIgnore
            @Override // com.fossil.c88
            public void onResponse(Call<R> call, q88<R> q88) {
                if (q88.e()) {
                    this.f750a.complete(q88.a());
                } else {
                    this.f750a.completeExceptionally(new g88(q88));
                }
            }
        }

        @DexIgnore
        public a(Type type) {
            this.f749a = type;
        }

        @DexIgnore
        @Override // com.fossil.b88
        public Type a() {
            return this.f749a;
        }

        @DexIgnore
        /* renamed from: c */
        public CompletableFuture<R> b(Call<R> call) {
            C0048a aVar = new C0048a(this, call);
            call.D(new b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class b<R> implements b88<R, CompletableFuture<q88<R>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Type f751a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends CompletableFuture<q88<R>> {
            @DexIgnore
            public /* final */ /* synthetic */ Call b;

            @DexIgnore
            public a(b bVar, Call call) {
                this.b = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.b.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d88$b$b")
        /* renamed from: com.fossil.d88$b$b  reason: collision with other inner class name */
        public class C0049b implements c88<R> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ CompletableFuture f752a;

            @DexIgnore
            public C0049b(b bVar, CompletableFuture completableFuture) {
                this.f752a = completableFuture;
            }

            @DexIgnore
            @Override // com.fossil.c88
            public void onFailure(Call<R> call, Throwable th) {
                this.f752a.completeExceptionally(th);
            }

            @DexIgnore
            @Override // com.fossil.c88
            public void onResponse(Call<R> call, q88<R> q88) {
                this.f752a.complete(q88);
            }
        }

        @DexIgnore
        public b(Type type) {
            this.f751a = type;
        }

        @DexIgnore
        @Override // com.fossil.b88
        public Type a() {
            return this.f751a;
        }

        @DexIgnore
        /* renamed from: c */
        public CompletableFuture<q88<R>> b(Call<R> call) {
            a aVar = new a(this, call);
            call.D(new C0049b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore
    @Override // com.fossil.b88.a
    public b88<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (b88.a.c(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b2 = b88.a.b(0, (ParameterizedType) type);
            if (b88.a.c(b2) != q88.class) {
                return new a(b2);
            }
            if (b2 instanceof ParameterizedType) {
                return new b(b88.a.b(0, (ParameterizedType) b2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
