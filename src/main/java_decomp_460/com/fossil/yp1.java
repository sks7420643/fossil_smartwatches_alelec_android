package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ op1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public yp1 createFromParcel(Parcel parcel) {
            return new yp1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public yp1[] newArray(int i) {
            return new yp1[i];
        }
    }

    @DexIgnore
    public yp1(byte b, t8 t8Var) {
        super(np1.NOTIFICATION_FILTER_SYNC, b);
        this.d = op1.c.a(t8Var);
    }

    @DexIgnore
    public /* synthetic */ yp1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.d = op1.values()[parcel.readInt()];
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(yp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((yp1) obj).d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.NotificationFilterSyncNotification");
    }

    @DexIgnore
    public final op1 getAction() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
