package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v27 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ s27 d;
    @DexIgnore
    public /* final */ DeviceRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3700a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(String str, String str2) {
            pq7.c(str, "deviceId");
            pq7.c(str2, ButtonService.USER_ID);
            this.f3700a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.f3700a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMesagge");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.GetSecretKeyUseCase", f = "GetSecretKeyUseCase.kt", l = {23, 29}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ v27 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(v27 v27, qn7 qn7) {
            super(qn7);
            this.this$0 = v27;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public v27(s27 s27, DeviceRepository deviceRepository) {
        pq7.c(s27, "mEncryptValueKeyStoreUseCase");
        pq7.c(deviceRepository, "mDeviceRepository");
        this.d = s27;
        this.e = deviceRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GetSecretKeyUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.v27.a r10, com.fossil.qn7<java.lang.Object> r11) {
        /*
        // Method dump skipped, instructions count: 337
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.v27.k(com.fossil.v27$a, com.fossil.qn7):java.lang.Object");
    }
}
