package com.fossil;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m52 implements Parcelable.Creator<GoogleSignInAccount> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInAccount createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        long j = 0;
        int i = 0;
        String str = null;
        String str2 = null;
        ArrayList arrayList = null;
        String str3 = null;
        String str4 = null;
        Uri uri = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    i = ad2.v(parcel, t);
                    break;
                case 2:
                    str8 = ad2.f(parcel, t);
                    break;
                case 3:
                    str7 = ad2.f(parcel, t);
                    break;
                case 4:
                    str6 = ad2.f(parcel, t);
                    break;
                case 5:
                    str5 = ad2.f(parcel, t);
                    break;
                case 6:
                    uri = (Uri) ad2.e(parcel, t, Uri.CREATOR);
                    break;
                case 7:
                    str4 = ad2.f(parcel, t);
                    break;
                case 8:
                    j = ad2.y(parcel, t);
                    break;
                case 9:
                    str3 = ad2.f(parcel, t);
                    break;
                case 10:
                    arrayList = ad2.j(parcel, t, Scope.CREATOR);
                    break;
                case 11:
                    str2 = ad2.f(parcel, t);
                    break;
                case 12:
                    str = ad2.f(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new GoogleSignInAccount(i, str8, str7, str6, str5, uri, str4, j, str3, arrayList, str2, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInAccount[] newArray(int i) {
        return new GoogleSignInAccount[i];
    }
}
