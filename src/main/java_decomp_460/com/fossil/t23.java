package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class t23 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3352a;

    /*
    static {
        int[] iArr = new int[l43.values().length];
        f3352a = iArr;
        try {
            iArr[l43.BOOL.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3352a[l43.BYTES.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3352a[l43.DOUBLE.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3352a[l43.FIXED32.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3352a[l43.SFIXED32.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3352a[l43.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3352a[l43.SFIXED64.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f3352a[l43.FLOAT.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f3352a[l43.ENUM.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f3352a[l43.INT32.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f3352a[l43.UINT32.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f3352a[l43.INT64.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f3352a[l43.UINT64.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f3352a[l43.MESSAGE.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f3352a[l43.SINT32.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f3352a[l43.SINT64.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f3352a[l43.STRING.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
    }
    */
}
