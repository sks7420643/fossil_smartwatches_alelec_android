package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class c1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f542a;

    /*
    static {
        int[] iArr = new int[zq.values().length];
        f542a = iArr;
        iArr[zq.INTERRUPTED.ordinal()] = 1;
        f542a[zq.CONNECTION_DROPPED.ordinal()] = 2;
        f542a[zq.INCOMPATIBLE_FIRMWARE.ordinal()] = 3;
        f542a[zq.REQUEST_ERROR.ordinal()] = 4;
        f542a[zq.LACK_OF_SERVICE.ordinal()] = 5;
        f542a[zq.LACK_OF_CHARACTERISTIC.ordinal()] = 6;
        f542a[zq.INVALID_SERIAL_NUMBER.ordinal()] = 7;
        f542a[zq.DATA_TRANSFER_RETRY_REACH_THRESHOLD.ordinal()] = 8;
        f542a[zq.EXCHANGED_VALUE_NOT_SATISFIED.ordinal()] = 9;
        f542a[zq.INVALID_FILE_LENGTH.ordinal()] = 10;
        f542a[zq.MISMATCH_VERSION.ordinal()] = 11;
        f542a[zq.INVALID_FILE_CRC.ordinal()] = 12;
        f542a[zq.INVALID_RESPONSE.ordinal()] = 13;
        f542a[zq.INVALID_DATA_LENGTH.ordinal()] = 14;
        f542a[zq.NOT_ENOUGH_FILE_TO_PROCESS.ordinal()] = 15;
        f542a[zq.WAITING_FOR_EXECUTION_TIMEOUT.ordinal()] = 16;
        f542a[zq.REQUEST_UNSUPPORTED.ordinal()] = 17;
        f542a[zq.UNSUPPORTED_FILE_HANDLE.ordinal()] = 18;
        f542a[zq.BLUETOOTH_OFF.ordinal()] = 19;
        f542a[zq.AUTHENTICATION_FAILED.ordinal()] = 20;
        f542a[zq.WRONG_RANDOM_NUMBER.ordinal()] = 21;
        f542a[zq.SECRET_KEY_IS_REQUIRED.ordinal()] = 22;
        f542a[zq.INVALID_PARAMETER.ordinal()] = 23;
        f542a[zq.HID_INPUT_DEVICE_DISABLED.ordinal()] = 24;
        f542a[zq.NOT_ALLOW_TO_START.ordinal()] = 25;
        f542a[zq.UNSUPPORTED_FORMAT.ordinal()] = 26;
        f542a[zq.TARGET_FIRMWARE_NOT_MATCHED.ordinal()] = 27;
        f542a[zq.DATABASE_ERROR.ordinal()] = 28;
        f542a[zq.FLOW_BROKEN.ordinal()] = 29;
        f542a[zq.SUCCESS.ordinal()] = 30;
        f542a[zq.NOT_START.ordinal()] = 31;
        f542a[zq.NETWORK_ERROR.ordinal()] = 32;
    }
    */
}
