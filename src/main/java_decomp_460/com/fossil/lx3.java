package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ig0;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lx3 implements ig0 {
    @DexIgnore
    public cg0 b;
    @DexIgnore
    public BottomNavigationMenuView c;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0146a();
        @DexIgnore
        public int b;
        @DexIgnore
        public fz3 c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx3$a$a")
        /* renamed from: com.fossil.lx3$a$a  reason: collision with other inner class name */
        public static final class C0146a implements Parcelable.Creator<a> {
            @DexIgnore
            /* renamed from: a */
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            /* renamed from: b */
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public a(Parcel parcel) {
            this.b = parcel.readInt();
            this.c = (fz3) parcel.readParcelable(a.class.getClassLoader());
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.b);
            parcel.writeParcelable(this.c, 0);
        }
    }

    @DexIgnore
    public void a(BottomNavigationMenuView bottomNavigationMenuView) {
        this.c = bottomNavigationMenuView;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void b(cg0 cg0, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void c(boolean z) {
        if (!this.d) {
            if (z) {
                this.c.d();
            } else {
                this.c.k();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean e(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean f(cg0 cg0, eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void g(ig0.a aVar) {
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public int getId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void h(Context context, cg0 cg0) {
        this.b = cg0;
        this.c.b(cg0);
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public void i(Parcelable parcelable) {
        if (parcelable instanceof a) {
            a aVar = (a) parcelable;
            this.c.j(aVar.b);
            this.c.setBadgeDrawables(ix3.b(this.c.getContext(), aVar.c));
        }
    }

    @DexIgnore
    public void j(int i) {
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public boolean k(ng0 ng0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ig0
    public Parcelable l() {
        a aVar = new a();
        aVar.b = this.c.getSelectedItemId();
        aVar.c = ix3.c(this.c.getBadgeDrawables());
        return aVar;
    }

    @DexIgnore
    public void m(boolean z) {
        this.d = z;
    }
}
