package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in6 extends u47 {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public static /* final */ a G; // = new a(null);
    @DexIgnore
    public Long A;
    @DexIgnore
    public Long B;
    @DexIgnore
    public Long C;
    @DexIgnore
    public g37<nc5> D;
    @DexIgnore
    public HashMap E;
    @DexIgnore
    public Integer k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public Integer m;
    @DexIgnore
    public Float s;
    @DexIgnore
    public Integer t;
    @DexIgnore
    public Integer u;
    @DexIgnore
    public Integer v;
    @DexIgnore
    public Float w;
    @DexIgnore
    public b x;
    @DexIgnore
    public hi5 y;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return in6.F;
        }

        @DexIgnore
        public final in6 b() {
            return new in6();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void z4(hi5 hi5, int i, Integer num, float f, Float f2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ in6 f1645a;

        @DexIgnore
        public c(in6 in6) {
            this.f1645a = in6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = in6.G.a();
            local.d(a2, "second was changed from " + i + " to " + i2);
            this.f1645a.v = Integer.valueOf(i2);
            this.f1645a.T6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ in6 b;

        @DexIgnore
        public d(in6 in6) {
            this.b = in6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Integer num;
            b bVar = this.b.x;
            if (!(bVar == null || (num = this.b.m) == null)) {
                int intValue = num.intValue();
                hi5 hi5 = this.b.y;
                if (hi5 != null) {
                    Integer num2 = this.b.v;
                    Float f = this.b.s;
                    if (f != null) {
                        bVar.z4(hi5, intValue, num2, f.floatValue(), this.b.w);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.b.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ in6 b;

        @DexIgnore
        public e(in6 in6) {
            this.b = in6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ in6 f1646a;

        @DexIgnore
        public f(in6 in6) {
            this.f1646a = in6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = in6.G.a();
            local.d(a2, "first was changed from " + i + " to " + i2);
            this.f1646a.m = Integer.valueOf(i2);
            this.f1646a.T6();
        }
    }

    /*
    static {
        String simpleName = in6.class.getSimpleName();
        pq7.b(simpleName, "WorkoutEditPickerFragment::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    public final boolean K6(int i, int i2, int i3, int i4, long j, long j2) {
        if (DateUtils.isToday(j)) {
            hi5 hi5 = this.y;
            if (hi5 == null) {
                return true;
            }
            int i5 = jn6.f1782a[hi5.ordinal()];
            if (i5 == 1) {
                Long l2 = this.C;
                if (l2 != null) {
                    long longValue = l2.longValue();
                    int i6 = ((i2 * 3600) + (i4 * 60)) * 1000;
                    long j3 = ((long) (((i * 3600) + (i3 * 60)) * 1000)) + longValue;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = F;
                    local.d(str, "checkIfValidStartTime duration " + longValue + " editedTime " + j3 + " curTime " + i6);
                    return j3 <= ((long) i6);
                }
            } else if (i5 != 2) {
                return true;
            } else {
                if (i == 0 && i3 == 0) {
                    return false;
                }
                Long l3 = this.B;
                if (l3 != null) {
                    long longValue2 = l3.longValue();
                    long j4 = ((long) (((i * 3600) + (i3 * 60)) * 1000)) + longValue2;
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = F;
                    local2.d(str2, "checkIfValidStartTime startTime " + longValue2 + " durationEdited " + j4 + " now " + j2);
                    return j4 <= j2;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final nc5 L6() {
        g37<nc5> g37 = this.D;
        if (g37 != null) {
            return g37.a();
        }
        return null;
    }

    @DexIgnore
    public final void M6(hi5 hi5) {
        pq7.c(hi5, "mode");
        this.y = hi5;
    }

    @DexIgnore
    public final void N6(ii5 ii5) {
        Float f2 = null;
        this.m = ii5 != null ? Integer.valueOf(ii5.d()) : null;
        this.l = ii5 != null ? Integer.valueOf(ii5.b()) : null;
        this.k = ii5 != null ? Integer.valueOf(ii5.a()) : null;
        if (ii5 != null) {
            f2 = Float.valueOf(ii5.c());
        }
        this.s = f2;
    }

    @DexIgnore
    public final void O6(b bVar) {
        pq7.c(bVar, "listener");
        this.x = bVar;
    }

    @DexIgnore
    public final void P6(ii5 ii5) {
        Float f2 = null;
        this.v = ii5 != null ? Integer.valueOf(ii5.d()) : null;
        this.u = ii5 != null ? Integer.valueOf(ii5.b()) : null;
        this.t = ii5 != null ? Integer.valueOf(ii5.a()) : null;
        if (ii5 != null) {
            f2 = Float.valueOf(ii5.c());
        }
        this.w = f2;
    }

    @DexIgnore
    public final void Q6(Long l2) {
        this.A = l2;
    }

    @DexIgnore
    public final void R6(Long l2) {
        if (l2 != null) {
            this.C = Long.valueOf(l2.longValue());
        }
    }

    @DexIgnore
    public final void S6(Long l2) {
        this.B = l2;
    }

    @DexIgnore
    public final void T6() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        Long l2 = this.A;
        if (l2 != null) {
            long longValue = l2.longValue();
            Calendar instance = Calendar.getInstance();
            Integer num = this.m;
            if (num != null) {
                int intValue = num.intValue();
                int i = instance.get(11);
                Integer num2 = this.v;
                if (num2 != null) {
                    int intValue2 = num2.intValue();
                    int i2 = instance.get(12);
                    pq7.b(instance, "calendar");
                    boolean K6 = K6(intValue, i, intValue2, i2, longValue, instance.getTimeInMillis());
                    String str = K6 ? "flexible_button_primary" : "flexible_button_disabled";
                    nc5 L6 = L6();
                    if (!(L6 == null || (flexibleButton2 = L6.s) == null)) {
                        flexibleButton2.setEnabled(K6);
                    }
                    nc5 L62 = L6();
                    if (L62 != null && (flexibleButton = L62.s) != null) {
                        flexibleButton.d(str);
                        return;
                    }
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 2131951897);
    }

    @DexIgnore
    @Override // com.fossil.ze0, com.fossil.nx3, com.fossil.kq0
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(F, "onCreateDialog");
        View inflate = View.inflate(getContext(), 2131558643, null);
        pq7.b(inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mx3 mx3 = new mx3(requireContext(), getTheme());
        mx3.setContentView(inflate);
        mx3.setCanceledOnTouchOutside(true);
        return mx3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ViewDataBinding e2 = aq0.e(layoutInflater, 2131558643, viewGroup, false);
        pq7.b(e2, "DataBindingUtil.inflate(\u2026picker, container, false)");
        nc5 nc5 = (nc5) e2;
        this.D = new g37<>(this, nc5);
        return nc5.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        nc5 L6;
        NumberPicker numberPicker3;
        NumberPicker numberPicker4;
        NumberPicker numberPicker5;
        NumberPicker numberPicker6;
        NumberPicker numberPicker7;
        NumberPicker numberPicker8;
        NumberPicker numberPicker9;
        NumberPicker numberPicker10;
        NumberPicker numberPicker11;
        FlexibleTextView flexibleTextView;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        nc5 L62 = L6();
        if (!(L62 == null || (flexibleTextView = L62.t) == null)) {
            flexibleTextView.setText(this.z);
        }
        Integer num = this.k;
        if (num != null) {
            int intValue = num.intValue();
            nc5 L63 = L6();
            if (!(L63 == null || (numberPicker11 = L63.u) == null)) {
                numberPicker11.setMaxValue(intValue);
            }
        }
        Integer num2 = this.l;
        if (num2 != null) {
            int intValue2 = num2.intValue();
            nc5 L64 = L6();
            if (!(L64 == null || (numberPicker10 = L64.u) == null)) {
                numberPicker10.setMinValue(intValue2);
            }
        }
        Integer num3 = this.m;
        if (num3 != null) {
            int intValue3 = num3.intValue();
            nc5 L65 = L6();
            if (!(L65 == null || (numberPicker9 = L65.u) == null)) {
                numberPicker9.setValue(intValue3);
            }
        }
        Float f2 = this.s;
        if (f2 != null) {
            float floatValue = f2.floatValue();
            ArrayList arrayList = new ArrayList();
            Integer num4 = this.l;
            if (num4 != null) {
                int intValue4 = num4.intValue();
                Integer num5 = this.k;
                if (num5 != null) {
                    int intValue5 = num5.intValue();
                    if (intValue4 <= intValue5) {
                        while (true) {
                            float f3 = ((float) intValue4) * floatValue;
                            if (f3 % ((float) 1) == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                arrayList.add(String.valueOf((int) f3));
                            } else {
                                arrayList.add(String.valueOf(dl5.h(f3, 1)));
                            }
                            if (intValue4 == intValue5) {
                                break;
                            }
                            intValue4++;
                        }
                    }
                    nc5 L66 = L6();
                    if (!(L66 == null || (numberPicker8 = L66.u) == null)) {
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            numberPicker8.setDisplayedValues((String[]) array);
                        } else {
                            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        Integer num6 = this.t;
        if (num6 != null) {
            int intValue6 = num6.intValue();
            nc5 L67 = L6();
            if (!(L67 == null || (numberPicker7 = L67.v) == null)) {
                numberPicker7.setMaxValue(intValue6);
            }
        }
        Integer num7 = this.u;
        if (num7 != null) {
            int intValue7 = num7.intValue();
            nc5 L68 = L6();
            if (!(L68 == null || (numberPicker6 = L68.v) == null)) {
                numberPicker6.setMinValue(intValue7);
            }
        }
        Integer num8 = this.v;
        if (num8 != null) {
            int intValue8 = num8.intValue();
            nc5 L69 = L6();
            if (!(L69 == null || (numberPicker5 = L69.v) == null)) {
                numberPicker5.setValue(intValue8);
            }
        }
        Float f4 = this.w;
        if (f4 != null) {
            float floatValue2 = f4.floatValue();
            ArrayList arrayList2 = new ArrayList();
            Integer num9 = this.u;
            if (num9 != null) {
                int intValue9 = num9.intValue();
                Integer num10 = this.t;
                if (num10 != null) {
                    int intValue10 = num10.intValue();
                    if (intValue9 <= intValue10) {
                        while (true) {
                            float f5 = ((float) intValue9) * floatValue2;
                            if (f5 % ((float) 1) == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                arrayList2.add(String.valueOf((int) f5));
                            } else {
                                arrayList2.add(String.valueOf(dl5.h(f5, 1)));
                            }
                            if (intValue9 == intValue10) {
                                break;
                            }
                            intValue9++;
                        }
                    }
                    nc5 L610 = L6();
                    if (!(L610 == null || (numberPicker4 = L610.v) == null)) {
                        Object[] array2 = arrayList2.toArray(new String[0]);
                        if (array2 != null) {
                            numberPicker4.setDisplayedValues((String[]) array2);
                        } else {
                            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        if (!(this.v != null || (L6 = L6()) == null || (numberPicker3 = L6.v) == null)) {
            numberPicker3.setVisibility(8);
        }
        nc5 L611 = L6();
        if (!(L611 == null || (numberPicker2 = L611.u) == null)) {
            numberPicker2.setOnValueChangedListener(new f(this));
        }
        nc5 L612 = L6();
        if (!(L612 == null || (numberPicker = L612.v) == null)) {
            numberPicker.setOnValueChangedListener(new c(this));
        }
        nc5 L613 = L6();
        if (!(L613 == null || (flexibleButton2 = L613.s) == null)) {
            flexibleButton2.setOnClickListener(new d(this));
        }
        nc5 L614 = L6();
        if (L614 != null && (flexibleButton = L614.r) != null) {
            flexibleButton.setOnClickListener(new e(this));
        }
    }

    @DexIgnore
    public final void setTitle(String str) {
        pq7.c(str, "title");
        this.z = str;
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.E;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
