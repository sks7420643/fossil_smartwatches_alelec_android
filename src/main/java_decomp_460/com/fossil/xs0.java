package com.fossil;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import com.fossil.at0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xs0 extends LoaderManager {
    @DexIgnore
    public static boolean c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LifecycleOwner f4167a;
    @DexIgnore
    public /* final */ c b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<D> extends MutableLiveData<D> implements at0.c<D> {
        @DexIgnore
        public /* final */ int k;
        @DexIgnore
        public /* final */ Bundle l;
        @DexIgnore
        public /* final */ at0<D> m;
        @DexIgnore
        public LifecycleOwner n;
        @DexIgnore
        public b<D> o;
        @DexIgnore
        public at0<D> p;

        @DexIgnore
        public a(int i, Bundle bundle, at0<D> at0, at0<D> at02) {
            this.k = i;
            this.l = bundle;
            this.m = at0;
            this.p = at02;
            at0.registerListener(i, this);
        }

        @DexIgnore
        @Override // com.fossil.at0.c
        public void a(at0<D> at0, D d) {
            if (xs0.c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                o(d);
                return;
            }
            if (xs0.c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            l(d);
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void j() {
            if (xs0.c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.startLoading();
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void k() {
            if (xs0.c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.stopLoading();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.ls0<? super D> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // androidx.lifecycle.LiveData
        public void m(ls0<? super D> ls0) {
            super.m(ls0);
            this.n = null;
            this.o = null;
        }

        @DexIgnore
        @Override // androidx.lifecycle.MutableLiveData, androidx.lifecycle.LiveData
        public void o(D d) {
            super.o(d);
            at0<D> at0 = this.p;
            if (at0 != null) {
                at0.reset();
                this.p = null;
            }
        }

        @DexIgnore
        public at0<D> p(boolean z) {
            if (xs0.c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.cancelLoad();
            this.m.abandon();
            b<D> bVar = this.o;
            if (bVar != null) {
                m(bVar);
                if (z) {
                    bVar.c();
                }
            }
            this.m.unregisterListener(this);
            if ((bVar == null || bVar.b()) && !z) {
                return this.m;
            }
            this.m.reset();
            return this.p;
        }

        @DexIgnore
        public void q(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            at0<D> at0 = this.m;
            at0.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                b<D> bVar = this.o;
                bVar.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(r().dataToString((D) e()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(g());
        }

        @DexIgnore
        public at0<D> r() {
            return this.m;
        }

        @DexIgnore
        public void s() {
            LifecycleOwner lifecycleOwner = this.n;
            b<D> bVar = this.o;
            if (lifecycleOwner != null && bVar != null) {
                super.m(bVar);
                h(lifecycleOwner, bVar);
            }
        }

        @DexIgnore
        public at0<D> t(LifecycleOwner lifecycleOwner, LoaderManager.a<D> aVar) {
            b<D> bVar = new b<>(this.m, aVar);
            h(lifecycleOwner, bVar);
            b<D> bVar2 = this.o;
            if (bVar2 != null) {
                m(bVar2);
            }
            this.n = lifecycleOwner;
            this.o = bVar;
            return this.m;
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.k);
            sb.append(" : ");
            in0.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<D> implements ls0<D> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ at0<D> f4168a;
        @DexIgnore
        public /* final */ LoaderManager.a<D> b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public b(at0<D> at0, LoaderManager.a<D> aVar) {
            this.f4168a = at0;
            this.b = aVar;
        }

        @DexIgnore
        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }

        @DexIgnore
        public boolean b() {
            return this.c;
        }

        @DexIgnore
        public void c() {
            if (this.c) {
                if (xs0.c) {
                    Log.v("LoaderManager", "  Resetting: " + this.f4168a);
                }
                this.b.g(this.f4168a);
            }
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public void onChanged(D d) {
            if (xs0.c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.f4168a + ": " + this.f4168a.dataToString(d));
            }
            this.b.a(this.f4168a, d);
            this.c = true;
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ts0 {
        @DexIgnore
        public static /* final */ ViewModelProvider.Factory c; // = new a();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public SparseArrayCompat<a> f4169a; // = new SparseArrayCompat<>();
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ViewModelProvider.Factory {
            @DexIgnore
            @Override // androidx.lifecycle.ViewModelProvider.Factory
            public <T extends ts0> T create(Class<T> cls) {
                return new c();
            }
        }

        @DexIgnore
        public static c c(ViewModelStore viewModelStore) {
            return (c) new ViewModelProvider(viewModelStore, c).a(c.class);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.f4169a.s() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.f4169a.s(); i++) {
                    a t = this.f4169a.t(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.f4169a.p(i));
                    printWriter.print(": ");
                    printWriter.println(t.toString());
                    t.q(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }

        @DexIgnore
        public void b() {
            this.b = false;
        }

        @DexIgnore
        public <D> a<D> d(int i) {
            return this.f4169a.j(i);
        }

        @DexIgnore
        public boolean e() {
            return this.b;
        }

        @DexIgnore
        public void f() {
            int s = this.f4169a.s();
            for (int i = 0; i < s; i++) {
                this.f4169a.t(i).s();
            }
        }

        @DexIgnore
        public void g(int i, a aVar) {
            this.f4169a.q(i, aVar);
        }

        @DexIgnore
        public void h(int i) {
            this.f4169a.r(i);
        }

        @DexIgnore
        public void i() {
            this.b = true;
        }

        @DexIgnore
        @Override // com.fossil.ts0
        public void onCleared() {
            super.onCleared();
            int s = this.f4169a.s();
            for (int i = 0; i < s; i++) {
                this.f4169a.t(i).p(true);
            }
            this.f4169a.e();
        }
    }

    @DexIgnore
    public xs0(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        this.f4167a = lifecycleOwner;
        this.b = c.c(viewModelStore);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public void a(int i) {
        if (this.b.e()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            a d = this.b.d(i);
            if (d != null) {
                d.p(true);
                this.b.h(i);
            }
        } else {
            throw new IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    @Deprecated
    public void b(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public <D> at0<D> d(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.e()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            a<D> d = this.b.d(i);
            if (c) {
                Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (d == null) {
                return g(i, bundle, aVar, null);
            }
            if (c) {
                Log.v("LoaderManager", "  Re-using existing loader " + d);
            }
            return d.t(this.f4167a, aVar);
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public void e() {
        this.b.f();
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public <D> at0<D> f(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.e()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            a<D> d = this.b.d(i);
            at0<D> at0 = null;
            if (d != null) {
                at0 = d.p(false);
            }
            return g(i, bundle, aVar, at0);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final <D> at0<D> g(int i, Bundle bundle, LoaderManager.a<D> aVar, at0<D> at0) {
        try {
            this.b.i();
            at0<D> d = aVar.d(i, bundle);
            if (d == null) {
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
            } else if (!d.getClass().isMemberClass() || Modifier.isStatic(d.getClass().getModifiers())) {
                a aVar2 = new a(i, bundle, d, at0);
                if (c) {
                    Log.v("LoaderManager", "  Created new loader " + aVar2);
                }
                this.b.g(i, aVar2);
                this.b.b();
                return aVar2.t(this.f4167a, aVar);
            } else {
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + d);
            }
        } catch (Throwable th) {
            this.b.b();
            throw th;
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        in0.a(this.f4167a, sb);
        sb.append("}}");
        return sb.toString();
    }
}
