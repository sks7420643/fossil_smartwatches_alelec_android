package com.fossil;

import android.util.Log;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fl5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f1150a; // = new Object();
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ c[] c; // = {new c(d.INITIAL), new c(d.BEFORE), new c(d.AFTER)};
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<a> d; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public interface a {
        @DexIgnore
        void e(g gVar);
    }

    @DexIgnore
    @FunctionalInterface
    public interface b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ AtomicBoolean f1151a; // = new AtomicBoolean();
            @DexIgnore
            public /* final */ e b;
            @DexIgnore
            public /* final */ fl5 c;

            @DexIgnore
            public a(e eVar, fl5 fl5) {
                this.b = eVar;
                this.c = fl5;
            }

            @DexIgnore
            public final void a(Throwable th) {
                Log.d("PagingRequestHelper", "recordFailure");
                if (th == null) {
                    throw new IllegalArgumentException("You must provide a throwable describing the error to record the failure");
                } else if (this.f1151a.compareAndSet(false, true)) {
                    this.c.e(this.b, th);
                } else {
                    throw new IllegalStateException("already called recordSuccess or recordFailure");
                }
            }

            @DexIgnore
            public final void b() {
                Log.d("PagingRequestHelper", "recordSuccess");
                if (this.f1151a.compareAndSet(false, true)) {
                    this.c.e(this.b, null);
                    return;
                }
                throw new IllegalStateException("already called recordSuccess or recordFailure");
            }
        }

        @DexIgnore
        void run(a aVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public e f1152a;
        @DexIgnore
        public b b;
        @DexIgnore
        public Throwable c;
        @DexIgnore
        public f d; // = f.SUCCESS;

        @DexIgnore
        public c(d dVar) {
        }
    }

    @DexIgnore
    public enum d {
        INITIAL,
        BEFORE,
        AFTER
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ b b;
        @DexIgnore
        public /* final */ fl5 c;
        @DexIgnore
        public /* final */ d d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                e eVar = e.this;
                eVar.c.h(eVar.d, eVar.b);
            }
        }

        @DexIgnore
        public e(b bVar, fl5 fl5, d dVar) {
            this.b = bVar;
            this.c = fl5;
            this.d = dVar;
        }

        @DexIgnore
        public void a(Executor executor) {
            executor.execute(new a());
        }

        @DexIgnore
        public void run() {
            this.b.run(new b.a(this, this.c));
        }
    }

    @DexIgnore
    public enum f {
        RUNNING,
        SUCCESS,
        FAILED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ f f1153a;
        @DexIgnore
        public /* final */ f b;
        @DexIgnore
        public /* final */ f c;
        @DexIgnore
        public /* final */ Throwable[] d;

        @DexIgnore
        public g(f fVar, f fVar2, f fVar3, Throwable[] thArr) {
            this.f1153a = fVar;
            this.b = fVar2;
            this.c = fVar3;
            this.d = thArr;
        }

        @DexIgnore
        public Throwable a(d dVar) {
            return this.d[dVar.ordinal()];
        }

        @DexIgnore
        public boolean b() {
            f fVar = this.f1153a;
            f fVar2 = f.FAILED;
            return fVar == fVar2 || this.b == fVar2 || this.c == fVar2;
        }

        @DexIgnore
        public boolean c() {
            f fVar = this.f1153a;
            f fVar2 = f.RUNNING;
            return fVar == fVar2 || this.b == fVar2 || this.c == fVar2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || g.class != obj.getClass()) {
                return false;
            }
            g gVar = (g) obj;
            if (this.f1153a == gVar.f1153a && this.b == gVar.b && this.c == gVar.c) {
                return Arrays.equals(this.d, gVar.d);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.f1153a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + Arrays.hashCode(this.d);
        }

        @DexIgnore
        public String toString() {
            return "StatusReport{initial=" + this.f1153a + ", before=" + this.b + ", after=" + this.c + ", mErrors=" + Arrays.toString(this.d) + '}';
        }
    }

    @DexIgnore
    public fl5(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public boolean a(a aVar) {
        return this.d.add(aVar);
    }

    @DexIgnore
    public final void b(g gVar) {
        Iterator<a> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().e(gVar);
        }
    }

    @DexIgnore
    public final f c(d dVar) {
        return this.c[dVar.ordinal()].d;
    }

    @DexIgnore
    public final g d() {
        c[] cVarArr = this.c;
        Throwable th = cVarArr[0].c;
        Throwable th2 = cVarArr[1].c;
        Throwable th3 = cVarArr[2].c;
        return new g(c(d.INITIAL), c(d.BEFORE), c(d.AFTER), new Throwable[]{th, th2, th3});
    }

    @DexIgnore
    public void e(e eVar, Throwable th) {
        g d2;
        boolean z = th == null;
        boolean isEmpty = this.d.isEmpty();
        synchronized (this.f1150a) {
            c cVar = this.c[eVar.d.ordinal()];
            cVar.b = null;
            cVar.c = th;
            if (z) {
                cVar.f1152a = null;
                cVar.d = f.SUCCESS;
            } else {
                cVar.f1152a = eVar;
                cVar.d = f.FAILED;
            }
            d2 = isEmpty ^ true ? d() : null;
        }
        if (d2 != null) {
            b(d2);
        }
    }

    @DexIgnore
    public boolean f(a aVar) {
        return this.d.remove(aVar);
    }

    @DexIgnore
    public boolean g() {
        FLogger.INSTANCE.getLocal().d("PagingRequestHelper", "retryAllFailed");
        int length = d.values().length;
        e[] eVarArr = new e[length];
        synchronized (this.f1150a) {
            for (int i = 0; i < d.values().length; i++) {
                eVarArr[i] = this.c[i].f1152a;
                this.c[i].f1152a = null;
            }
        }
        boolean z = false;
        for (int i2 = 0; i2 < length; i2++) {
            e eVar = eVarArr[i2];
            if (eVar != null) {
                eVar.a(this.b);
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        if (r0 == null) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        new com.fossil.fl5.e(r7, r5, r6).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean h(com.fossil.fl5.d r6, com.fossil.fl5.b r7) {
        /*
            r5 = this;
            r0 = 0
            java.util.concurrent.CopyOnWriteArrayList<com.fossil.fl5$a> r1 = r5.d
            boolean r1 = r1.isEmpty()
            java.lang.Object r2 = r5.f1150a
            monitor-enter(r2)
            com.fossil.fl5$c[] r3 = r5.c     // Catch:{ all -> 0x003d }
            int r4 = r6.ordinal()     // Catch:{ all -> 0x003d }
            r3 = r3[r4]     // Catch:{ all -> 0x003d }
            com.fossil.fl5$b r4 = r3.b     // Catch:{ all -> 0x003d }
            if (r4 == 0) goto L_0x0019
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            r0 = 0
        L_0x0018:
            return r0
        L_0x0019:
            r3.b = r7     // Catch:{ all -> 0x003d }
            com.fossil.fl5$f r4 = com.fossil.fl5.f.RUNNING     // Catch:{ all -> 0x003d }
            r3.d = r4     // Catch:{ all -> 0x003d }
            r4 = 0
            r3.f1152a = r4     // Catch:{ all -> 0x003d }
            r4 = 0
            r3.c = r4     // Catch:{ all -> 0x003d }
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x002d
            com.fossil.fl5$g r0 = r5.d()     // Catch:{ all -> 0x003d }
        L_0x002d:
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0033
            r5.b(r0)
        L_0x0033:
            com.fossil.fl5$e r0 = new com.fossil.fl5$e
            r0.<init>(r7, r5, r6)
            r0.run()
            r0 = 1
            goto L_0x0018
        L_0x003d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fl5.h(com.fossil.fl5$d, com.fossil.fl5$b):boolean");
    }
}
