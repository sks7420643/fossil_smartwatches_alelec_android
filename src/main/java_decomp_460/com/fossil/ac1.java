package com.fossil;

import android.content.res.AssetManager;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ac1 extends ub1<ParcelFileDescriptor> {
    @DexIgnore
    public ac1(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    /* renamed from: f */
    public void b(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        parcelFileDescriptor.close();
    }

    @DexIgnore
    /* renamed from: g */
    public ParcelFileDescriptor e(AssetManager assetManager, String str) throws IOException {
        return assetManager.openFd(str).getParcelFileDescriptor();
    }

    @DexIgnore
    @Override // com.fossil.wb1
    public Class<ParcelFileDescriptor> getDataClass() {
        return ParcelFileDescriptor.class;
    }
}
