package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface af1<Model, Data> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ mb1 f261a;
        @DexIgnore
        public /* final */ List<mb1> b;
        @DexIgnore
        public /* final */ wb1<Data> c;

        @DexIgnore
        public a(mb1 mb1, wb1<Data> wb1) {
            this(mb1, Collections.emptyList(), wb1);
        }

        @DexIgnore
        public a(mb1 mb1, List<mb1> list, wb1<Data> wb1) {
            ik1.d(mb1);
            this.f261a = mb1;
            ik1.d(list);
            this.b = list;
            ik1.d(wb1);
            this.c = wb1;
        }
    }

    @DexIgnore
    boolean a(Model model);

    @DexIgnore
    a<Data> b(Model model, int i, int i2, ob1 ob1);
}
