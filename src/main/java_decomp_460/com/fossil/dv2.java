package com.fossil;

import com.fossil.e13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv2 extends e13<dv2, a> implements o23 {
    @DexIgnore
    public static /* final */ dv2 zzf;
    @DexIgnore
    public static volatile z23<dv2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public j13 zze; // = e13.z();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<dv2, a> implements o23 {
        @DexIgnore
        public a() {
            super(dv2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(tu2 tu2) {
            this();
        }

        @DexIgnore
        public final a x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((dv2) this.c).J(i);
            return this;
        }

        @DexIgnore
        public final a y(Iterable<? extends Long> iterable) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((dv2) this.c).G(iterable);
            return this;
        }
    }

    /*
    static {
        dv2 dv2 = new dv2();
        zzf = dv2;
        e13.u(dv2.class, dv2);
    }
    */

    @DexIgnore
    public static a M() {
        return (a) zzf.w();
    }

    @DexIgnore
    public final long C(int i) {
        return this.zze.zzb(i);
    }

    @DexIgnore
    public final void G(Iterable<? extends Long> iterable) {
        j13 j13 = this.zze;
        if (!j13.zza()) {
            this.zze = e13.p(j13);
        }
        nz2.a(iterable, this.zze);
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int I() {
        return this.zzd;
    }

    @DexIgnore
    public final void J(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final List<Long> K() {
        return this.zze;
    }

    @DexIgnore
    public final int L() {
        return this.zze.size();
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (tu2.f3469a[i - 1]) {
            case 1:
                return new dv2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u1004\u0000\u0002\u0014", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                z23<dv2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (dv2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new e13.c(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
