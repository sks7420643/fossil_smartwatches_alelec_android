package com.fossil;

import com.fossil.e13;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s23<T> implements f33<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ m23 f3199a;
    @DexIgnore
    public /* final */ x33<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ s03<?> d;

    @DexIgnore
    public s23(x33<?, ?> x33, s03<?> s03, m23 m23) {
        this.b = x33;
        this.c = s03.e(m23);
        this.d = s03;
        this.f3199a = m23;
    }

    @DexIgnore
    public static <T> s23<T> c(x33<?, ?> x33, s03<?> s03, m23 m23) {
        return new s23<>(x33, s03, m23);
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final void a(T t, byte[] bArr, int i, int i2, sz2 sz2) throws IOException {
        T t2 = t;
        w33 w33 = t2.zzb;
        if (w33 == w33.a()) {
            w33 = w33.g();
            t2.zzb = w33;
        }
        t.C();
        e13.d dVar = null;
        while (i < i2) {
            int i3 = tz2.i(bArr, i, sz2);
            int i4 = sz2.f3339a;
            if (i4 == 11) {
                int i5 = 0;
                xz2 xz2 = null;
                i = i3;
                while (i < i2) {
                    i = tz2.i(bArr, i, sz2);
                    int i6 = sz2.f3339a;
                    int i7 = i6 >>> 3;
                    int i8 = i6 & 7;
                    if (i7 != 2) {
                        if (i7 == 3) {
                            if (dVar != null) {
                                b33.a();
                                throw new NoSuchMethodError();
                            } else if (i8 == 2) {
                                i = tz2.q(bArr, i, sz2);
                                xz2 = (xz2) sz2.c;
                            }
                        }
                    } else if (i8 == 0) {
                        i = tz2.i(bArr, i, sz2);
                        i5 = sz2.f3339a;
                        dVar = (e13.d) this.d.c(sz2.d, this.f3199a, i5);
                    }
                    if (i6 == 12) {
                        break;
                    }
                    i = tz2.a(i6, bArr, i, i2, sz2);
                }
                if (xz2 != null) {
                    w33.c((i5 << 3) | 2, xz2);
                }
            } else if ((i4 & 7) == 2) {
                dVar = (e13.d) this.d.c(sz2.d, this.f3199a, i4 >>> 3);
                if (dVar == null) {
                    i = tz2.c(i4, bArr, i3, i2, w33, sz2);
                } else {
                    b33.a();
                    throw new NoSuchMethodError();
                }
            } else {
                i = tz2.a(i4, bArr, i3, i2, sz2);
            }
        }
        if (i != i2) {
            throw l13.zzg();
        }
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final void b(T t, r43 r43) throws IOException {
        Iterator<Map.Entry<?, Object>> p = this.d.b(t).p();
        while (p.hasNext()) {
            Map.Entry<?, Object> next = p.next();
            v03 v03 = (v03) next.getKey();
            if (v03.zzc() != s43.MESSAGE || v03.zzd() || v03.zze()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof s13) {
                r43.zza(v03.zza(), ((s13) next).a().d());
            } else {
                r43.zza(v03.zza(), next.getValue());
            }
        }
        x33<?, ?> x33 = this.b;
        x33.g(x33.f(t), r43);
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final int zza(T t) {
        int hashCode = this.b.f(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.b(t).hashCode() : hashCode;
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final T zza() {
        return (T) this.f3199a.i().b();
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final boolean zza(T t, T t2) {
        if (!this.b.f(t).equals(this.b.f(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.b(t).equals(this.d.b(t2));
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final int zzb(T t) {
        x33<?, ?> x33 = this.b;
        int k = x33.k(x33.f(t)) + 0;
        return this.c ? k + this.d.b(t).s() : k;
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final void zzb(T t, T t2) {
        h33.o(this.b, t, t2);
        if (this.c) {
            h33.m(this.d, t, t2);
        }
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final void zzc(T t) {
        this.b.j(t);
        this.d.g(t);
    }

    @DexIgnore
    @Override // com.fossil.f33
    public final boolean zzd(T t) {
        return this.d.b(t).r();
    }
}
