package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.a11;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l11 implements a11 {
    @DexIgnore
    public /* final */ MutableLiveData<a11.b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ j41<a11.b.c> d; // = j41.t();

    @DexIgnore
    public l11() {
        a(a11.b);
    }

    @DexIgnore
    public void a(a11.b bVar) {
        this.c.l(bVar);
        if (bVar instanceof a11.b.c) {
            this.d.p((a11.b.c) bVar);
        } else if (bVar instanceof a11.b.a) {
            this.d.q(((a11.b.a) bVar).a());
        }
    }
}
