package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o86 extends j86 {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> e; // = new ArrayList<>();
    @DexIgnore
    public Ringtone f;
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ k86 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1", f = "SearchRingPhonePresenter.kt", l = {29}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o86 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.o86$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1$1", f = "SearchRingPhonePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.o86$a$a  reason: collision with other inner class name */
        public static final class C0178a extends ko7 implements vp7<iv7, qn7<? super List<? extends Ringtone>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public C0178a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0178a aVar = new C0178a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends Ringtone>> qn7) {
                return ((C0178a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return dk5.g.e();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(o86 o86, qn7 qn7) {
            super(2, qn7);
            this.this$0 = o86;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ArrayList arrayList;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.this$0.e.isEmpty()) {
                    ArrayList arrayList2 = this.this$0.e;
                    dv7 h = this.this$0.h();
                    C0178a aVar = new C0178a(null);
                    this.L$0 = iv7;
                    this.L$1 = arrayList2;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    arrayList = arrayList2;
                }
                this.this$0.u().J3(this.this$0.e);
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                arrayList = (ArrayList) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList.addAll((Collection) g);
            if (this.this$0.f == null) {
                o86 o86 = this.this$0;
                o86.f = (Ringtone) o86.e.get(0);
            }
            this.this$0.u().J3(this.this$0.e);
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public o86(k86 k86) {
        pq7.c(k86, "mView");
        this.h = k86;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.j86
    public Ringtone n() {
        Ringtone ringtone = this.f;
        if (ringtone != null) {
            return ringtone;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j86
    public void o(Ringtone ringtone) {
        pq7.c(ringtone, Constants.RINGTONE);
        pn5.o.a().j(ringtone);
        this.f = ringtone;
    }

    @DexIgnore
    @Override // com.fossil.j86
    public void p() {
        pn5.o.a().p();
    }

    @DexIgnore
    public final k86 u() {
        return this.h;
    }

    @DexIgnore
    public void v(Ringtone ringtone) {
        pq7.c(ringtone, Constants.RINGTONE);
        this.f = ringtone;
    }

    @DexIgnore
    public void w(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        try {
            this.f = (Ringtone) this.g.k(str, Ringtone.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SearchRingPhonePresenter", "exception when parse ringtone setting " + e2);
        }
    }

    @DexIgnore
    public void x() {
        this.h.M5(this);
    }
}
