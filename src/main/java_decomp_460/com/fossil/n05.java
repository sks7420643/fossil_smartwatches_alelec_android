package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Style;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<Style>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<Style>> {
    }

    @DexIgnore
    public final ArrayList<Style> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        try {
            Object l = new Gson().l(str, new a().getType());
            pq7.b(l, "Gson().fromJson(json, type)");
            return (ArrayList) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ThemeConverter", "error when convert fromJsonToStyle ex=" + e);
            return new ArrayList<>();
        }
    }

    @DexIgnore
    public final String b(ArrayList<Style> arrayList) {
        if (ff2.a(arrayList)) {
            return "";
        }
        try {
            String u = new Gson().u(arrayList, new b().getType());
            pq7.b(u, "Gson().toJson(styles, type)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ThemeConverter", "error when convert fromStyleToJson ex=" + e);
            return "";
        }
    }
}
