package com.fossil;

import android.annotation.SuppressLint;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kz0 extends jz0 {
    @DexIgnore
    public static boolean j; // = true;

    @DexIgnore
    @Override // com.fossil.nz0
    @SuppressLint({"NewApi"})
    public void f(View view, int i, int i2, int i3, int i4) {
        if (j) {
            try {
                view.setLeftTopRightBottom(i, i2, i3, i4);
            } catch (NoSuchMethodError e) {
                j = false;
            }
        }
    }
}
