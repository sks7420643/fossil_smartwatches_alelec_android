package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h03 extends e03 {
    @DexIgnore
    public /* final */ byte[] zzb;

    @DexIgnore
    public h03(byte[] bArr) {
        if (bArr != null) {
            this.zzb = bArr;
            return;
        }
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof xz2)) {
            return false;
        }
        if (zza() != ((xz2) obj).zza()) {
            return false;
        }
        if (zza() == 0) {
            return true;
        }
        if (!(obj instanceof h03)) {
            return obj.equals(this);
        }
        h03 h03 = (h03) obj;
        int zzd = zzd();
        int zzd2 = h03.zzd();
        if (zzd == 0 || zzd2 == 0 || zzd == zzd2) {
            return zza(h03, 0, zza());
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public byte zza(int i) {
        return this.zzb[i];
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public int zza() {
        return this.zzb.length;
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public final int zza(int i, int i2, int i3) {
        return h13.a(i, this.zzb, zze(), i3);
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public final xz2 zza(int i, int i2) {
        int zzb2 = xz2.zzb(0, i2, zza());
        return zzb2 == 0 ? xz2.zza : new zz2(this.zzb, zze(), zzb2);
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public final String zza(Charset charset) {
        return new String(this.zzb, zze(), zza(), charset);
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public final void zza(uz2 uz2) throws IOException {
        uz2.a(this.zzb, zze(), zza());
    }

    @DexIgnore
    @Override // com.fossil.e03
    public final boolean zza(xz2 xz2, int i, int i2) {
        if (i2 > xz2.zza()) {
            int zza = zza();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(zza);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 > xz2.zza()) {
            int zza2 = xz2.zza();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(zza2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(xz2 instanceof h03)) {
            return xz2.zza(0, i2).equals(zza(0, i2));
        } else {
            h03 h03 = (h03) xz2;
            byte[] bArr = this.zzb;
            byte[] bArr2 = h03.zzb;
            int zze = zze();
            int zze2 = zze();
            int zze3 = h03.zze();
            while (zze2 < zze + i2) {
                if (bArr[zze2] != bArr2[zze3]) {
                    return false;
                }
                zze2++;
                zze3++;
            }
            return true;
        }
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public byte zzb(int i) {
        return this.zzb[i];
    }

    @DexIgnore
    @Override // com.fossil.xz2
    public final boolean zzc() {
        int zze = zze();
        return g43.g(this.zzb, zze, zza() + zze);
    }

    @DexIgnore
    public int zze() {
        return 0;
    }
}
