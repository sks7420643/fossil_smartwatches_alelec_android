package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cw4 implements Factory<bw4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<du4> f673a;

    @DexIgnore
    public cw4(Provider<du4> provider) {
        this.f673a = provider;
    }

    @DexIgnore
    public static cw4 a(Provider<du4> provider) {
        return new cw4(provider);
    }

    @DexIgnore
    public static bw4 c(du4 du4) {
        return new bw4(du4);
    }

    @DexIgnore
    /* renamed from: b */
    public bw4 get() {
        return c(this.f673a.get());
    }
}
