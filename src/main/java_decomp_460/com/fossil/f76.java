package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.jn5;
import com.fossil.r66;
import com.fossil.sm5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f76 extends qv5 implements e76, View.OnClickListener, t47.g, sm5.b {
    @DexIgnore
    public d76 h;
    @DexIgnore
    public g37<x55> i;
    @DexIgnore
    public /* final */ ArrayList<Fragment> j; // = new ArrayList<>();
    @DexIgnore
    public x96 k;
    @DexIgnore
    public aa6 l;
    @DexIgnore
    public po4 m;
    @DexIgnore
    public k76 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements r66.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f76 f1065a;

        @DexIgnore
        public a(f76 f76) {
            this.f1065a = f76;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void a(String str) {
            pq7.c(str, "label");
            this.f1065a.R6(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void b() {
            this.f1065a.P6(ViewHierarchy.DIMENSION_TOP_KEY);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void c(String str) {
            pq7.c(str, "label");
            this.f1065a.Q6(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean d(String str) {
            pq7.c(str, "fromPos");
            this.f1065a.Y6(str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean e(View view, String str) {
            pq7.c(view, "view");
            pq7.c(str, "id");
            return this.f1065a.S6(ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements r66.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f76 f1066a;

        @DexIgnore
        public b(f76 f76) {
            this.f1066a = f76;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void a(String str) {
            pq7.c(str, "label");
            this.f1066a.R6("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void b() {
            this.f1066a.P6("middle");
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void c(String str) {
            pq7.c(str, "label");
            this.f1066a.Q6("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean d(String str) {
            pq7.c(str, "fromPos");
            this.f1066a.Y6(str, "middle");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean e(View view, String str) {
            pq7.c(view, "view");
            pq7.c(str, "id");
            return this.f1066a.S6("middle", view, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements r66.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f76 f1067a;

        @DexIgnore
        public c(f76 f76) {
            this.f1067a = f76;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void a(String str) {
            pq7.c(str, "label");
            this.f1067a.R6("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void b() {
            this.f1067a.P6("bottom");
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void c(String str) {
            pq7.c(str, "label");
            this.f1067a.Q6("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean d(String str) {
            pq7.c(str, "fromPos");
            this.f1067a.Y6(str, "bottom");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean e(View view, String str) {
            pq7.c(view, "view");
            pq7.c(str, "id");
            return this.f1067a.S6("bottom", view, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void A0(View view) {
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchAppEditFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        d76 d76 = this.h;
        if (d76 != null) {
            d76.o();
            return false;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void L() {
        t47.f fVar = new t47.f(2131558482);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886536));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886534));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886535));
        fVar.b(2131363291);
        fVar.b(2131363373);
        fVar.k(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public /* bridge */ /* synthetic */ void N5(View view, Boolean bool) {
        V6(view, bool.booleanValue());
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void P() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            pq7.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.h0.c().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    public final void P6(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditFragment", "cancelDrag - position=" + str);
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.J();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.J();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.J();
                }
                a2.N.setDragMode(false);
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                x96 x96 = this.k;
                if (x96 != null) {
                    x96.L6();
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.I();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.I();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.I();
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        boolean z = true;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode == -1395717072) {
            str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION");
        } else if (hashCode != -523101473) {
            if (hashCode == 291193711 && str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING") && i2 == 2131363373 && intent != null) {
                String stringExtra = intent.getStringExtra("TO_POS");
                if (stringExtra == null) {
                    stringExtra = "";
                }
                String stringExtra2 = intent.getStringExtra("TO_ID");
                boolean booleanExtra = intent.getBooleanExtra("TO_COMPLICATION", true);
                FLogger.INSTANCE.getLocal().d("WatchAppEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
                if (!booleanExtra) {
                    if (stringExtra.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        d76 d76 = this.h;
                        if (d76 != null) {
                            d76.r(stringExtra);
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    }
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
        } else {
            if (i2 == 2131363291) {
                t0(false);
            } else if (i2 == 2131363373) {
                d76 d762 = this.h;
                if (d762 != null) {
                    d762.p(true);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void R6(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditFragment", "dragExit - position=" + str + ", label=" + str2);
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.J();
                        }
                    } else if (str.equals("middle")) {
                        a2.M.J();
                    }
                } else if (str.equals("bottom")) {
                    a2.L.J();
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final boolean S6(String str, View view, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.N.J();
                    }
                } else if (str.equals("middle")) {
                    a2.M.J();
                }
            } else if (str.equals("bottom")) {
                a2.L.J();
            }
            a2.N.setDragMode(false);
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            x96 x96 = this.k;
            if (x96 != null) {
                x96.L6();
            }
            d76 d76 = this.h;
            if (d76 != null) {
                d76.n(str2, str);
                return true;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void T6() {
        FLogger.INSTANCE.getLocal().d("WatchAppEditFragment", "Inside .showNoActiveDeviceFlow");
        x96 x96 = (x96) getChildFragmentManager().Z("WatchAppsFragment");
        this.k = x96;
        if (x96 == null) {
            this.k = new x96();
        }
        x96 x962 = this.k;
        if (x962 != null) {
            this.j.add(x962);
        }
        ro4 M = PortfolioApp.h0.c().M();
        x96 x963 = this.k;
        if (x963 != null) {
            M.L(new p66(x963)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsContract.View");
    }

    @DexIgnore
    public final void U6(String str) {
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.setSelectedWc(true);
                            a2.M.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.N.setSelectedWc(false);
                        a2.M.setSelectedWc(true);
                        a2.L.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.N.setSelectedWc(false);
                    a2.M.setSelectedWc(false);
                    a2.L.setSelectedWc(true);
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void V0(View view) {
    }

    @DexIgnore
    public void V6(View view, boolean z) {
        FLogger.INSTANCE.getLocal().d("WatchAppEditFragment", "onHorizontalFling");
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void W5(String str) {
        pq7.c(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditFragment", "updateSelectedWatchApp position=" + str);
        U6(str);
    }

    @DexIgnore
    /* renamed from: W6 */
    public void M5(d76 d76) {
        pq7.c(d76, "presenter");
        this.h = d76;
    }

    @DexIgnore
    public final void X6() {
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.N;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                pq7.b(putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.X(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new r66(new a(this)), null, 8, null);
                CustomizeWidget customizeWidget2 = a2.M;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                pq7.b(putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.X(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new r66(new b(this)), null, 8, null);
                CustomizeWidget customizeWidget3 = a2.L;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                pq7.b(putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.X(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new r66(new c(this)), null, 8, null);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Y6(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.N.J();
                        }
                    } else if (str2.equals("middle")) {
                        a2.M.J();
                    }
                } else if (str2.equals("bottom")) {
                    a2.L.J();
                }
                a2.N.setDragMode(false);
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                d76 d76 = this.h;
                if (d76 != null) {
                    d76.s(str, str2);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Z6(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void a2(l66 l66) {
        Object obj;
        Object obj2;
        Object obj3;
        pq7.c(l66, "data");
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(l66.b());
                FlexibleTextView flexibleTextView = a2.G;
                pq7.b(flexibleTextView, "it.tvPresetName");
                String a3 = l66.a();
                if (a3 != null) {
                    String upperCase = a3.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        Object next = it.next();
                        if (pq7.a(((n66) next).c(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                            obj = next;
                            break;
                        }
                    }
                    n66 n66 = (n66) obj;
                    Iterator it2 = arrayList.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        Object next2 = it2.next();
                        if (pq7.a(((n66) next2).c(), "middle")) {
                            obj2 = next2;
                            break;
                        }
                    }
                    n66 n662 = (n66) obj2;
                    Iterator it3 = arrayList.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            obj3 = null;
                            break;
                        }
                        Object next3 = it3.next();
                        if (pq7.a(((n66) next3).c(), "bottom")) {
                            obj3 = next3;
                            break;
                        }
                    }
                    CustomizeWidget customizeWidget = a2.N;
                    pq7.b(customizeWidget, "it.waTop");
                    FlexibleTextView flexibleTextView2 = a2.J;
                    pq7.b(flexibleTextView2, "it.tvWaTop");
                    a7(customizeWidget, flexibleTextView2, n66);
                    CustomizeWidget customizeWidget2 = a2.M;
                    pq7.b(customizeWidget2, "it.waMiddle");
                    FlexibleTextView flexibleTextView3 = a2.I;
                    pq7.b(flexibleTextView3, "it.tvWaMiddle");
                    a7(customizeWidget2, flexibleTextView3, n662);
                    CustomizeWidget customizeWidget3 = a2.L;
                    pq7.b(customizeWidget3, "it.waBottom");
                    FlexibleTextView flexibleTextView4 = a2.H;
                    pq7.b(flexibleTextView4, "it.tvWaBottom");
                    a7(customizeWidget3, flexibleTextView4, (n66) obj3);
                    return;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a7(CustomizeWidget customizeWidget, FlexibleTextView flexibleTextView, n66 n66) {
        if (n66 != null) {
            flexibleTextView.setText(n66.b());
            customizeWidget.b0(n66.a());
            customizeWidget.T();
            Z6(customizeWidget, n66.a());
            return;
        }
        flexibleTextView.setText("");
        customizeWidget.b0("empty");
        customizeWidget.T();
        Z6(customizeWidget, "empty");
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void h5(View view) {
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void i0(String str) {
        pq7.c(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.A;
            pq7.b(activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchAppEditActivity watchAppEditActivity = (WatchAppEditActivity) activity;
            po4 po4 = this.m;
            if (po4 != null) {
                ts0 a2 = vs0.f(watchAppEditActivity, po4).a(k76.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026ditViewModel::class.java)");
                k76 k76 = (k76) a2;
                this.s = k76;
                d76 d76 = this.h;
                if (d76 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (k76 != null) {
                    d76.q(k76);
                } else {
                    pq7.n("mShareViewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        FLogger.INSTANCE.getLocal().d("WatchAppEditFragment", "");
        if (i2 == 111 && i3 == 1 && jn5.b.m(getContext(), jn5.c.BLUETOOTH_CONNECTION)) {
            d76 d76 = this.h;
            if (d76 != null) {
                d76.p(false);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362528) {
                d76 d76 = this.h;
                if (d76 != null) {
                    d76.p(true);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (id != 2131363291) {
                switch (id) {
                    case 2131363529:
                        d76 d762 = this.h;
                        if (d762 != null) {
                            d762.r("bottom");
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    case 2131363530:
                        d76 d763 = this.h;
                        if (d763 != null) {
                            d763.r("middle");
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    case 2131363531:
                        d76 d764 = this.h;
                        if (d764 != null) {
                            d764.r(ViewHierarchy.DIMENSION_TOP_KEY);
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                d76 d765 = this.h;
                if (d765 != null) {
                    d765.o();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        x55 x55 = (x55) aq0.f(layoutInflater, 2131558549, viewGroup, false, A6());
        T6();
        this.i = new g37<>(this, x55);
        pq7.b(x55, "binding");
        return x55.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        d76 d76 = this.h;
        if (d76 != null) {
            d76.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        d76 d76 = this.h;
        if (d76 != null) {
            d76.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 != null) {
                String d = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    a2.u.setBackgroundColor(Color.parseColor(d));
                    a2.K.setBackgroundColor(Color.parseColor(d));
                }
                a2.N.setOnClickListener(this);
                a2.M.setOnClickListener(this);
                a2.L.setOnClickListener(this);
                a2.F.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.E;
                pq7.b(viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new g67(getChildFragmentManager(), this.j));
                if (a2.E.getChildAt(0) != null) {
                    View childAt = a2.E.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(2);
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.E;
                pq7.b(viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                sm5 sm5 = new sm5();
                pq7.b(a2, "it");
                sm5.a(a2.n(), this);
            }
            X6();
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void s0(boolean z) {
        g37<x55> g37 = this.i;
        if (g37 != null) {
            x55 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "it.ftvSetToWatch");
                flexibleTextView.setEnabled(true);
                FlexibleTextView flexibleTextView2 = a2.v;
                pq7.b(flexibleTextView2, "it.ftvSetToWatch");
                flexibleTextView2.setClickable(true);
                a2.v.setBackgroundResource(2131231291);
                return;
            }
            FlexibleTextView flexibleTextView3 = a2.v;
            pq7.b(flexibleTextView3, "it.ftvSetToWatch");
            flexibleTextView3.setClickable(false);
            FlexibleTextView flexibleTextView4 = a2.v;
            pq7.b(flexibleTextView4, "it.ftvSetToWatch");
            flexibleTextView4.setEnabled(false);
            a2.v.setBackgroundResource(2131231292);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void t0(boolean z) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (z) {
                activity.setResult(-1);
            } else {
                activity.setResult(0);
            }
            activity.finishAfterTransition();
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void v4(View view) {
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void w() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.e76
    public void y() {
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886814);
        pq7.b(c2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        H6(c2);
    }
}
