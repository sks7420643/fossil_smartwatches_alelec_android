package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl1 extends ml1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<pl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public pl1 createFromParcel(Parcel parcel) {
            ml1 b = ml1.CREATOR.createFromParcel(parcel);
            if (b != null) {
                return (pl1) b;
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedReminder");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public pl1[] newArray(int i) {
            return new pl1[i];
        }
    }

    @DexIgnore
    public pl1(ol1 ol1, ql1 ql1, il1 il1) {
        super(ol1, ql1, il1);
    }

    @DexIgnore
    @Override // com.fossil.el1
    public ol1 getFireTime() {
        gl1[] b = b();
        for (gl1 gl1 : b) {
            if (gl1 instanceof ol1) {
                if (gl1 != null) {
                    return (ol1) gl1;
                } else {
                    throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedFireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
