package com.fossil;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.fossil.mx0;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rx0 implements mx0 {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ mx0.a d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public a g;
    @DexIgnore
    public boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends SQLiteOpenHelper {
        @DexIgnore
        public /* final */ qx0[] b;
        @DexIgnore
        public /* final */ mx0.a c;
        @DexIgnore
        public boolean d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rx0$a$a")
        /* renamed from: com.fossil.rx0$a$a  reason: collision with other inner class name */
        public class C0211a implements DatabaseErrorHandler {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ mx0.a f3178a;
            @DexIgnore
            public /* final */ /* synthetic */ qx0[] b;

            @DexIgnore
            public C0211a(mx0.a aVar, qx0[] qx0Arr) {
                this.f3178a = aVar;
                this.b = qx0Arr;
            }

            @DexIgnore
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.f3178a.c(a.b(this.b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public a(Context context, String str, qx0[] qx0Arr, mx0.a aVar) {
            super(context, str, null, aVar.f2431a, new C0211a(aVar, qx0Arr));
            this.c = aVar;
            this.b = qx0Arr;
        }

        @DexIgnore
        public static qx0 b(qx0[] qx0Arr, SQLiteDatabase sQLiteDatabase) {
            qx0 qx0 = qx0Arr[0];
            if (qx0 == null || !qx0.a(sQLiteDatabase)) {
                qx0Arr[0] = new qx0(sQLiteDatabase);
            }
            return qx0Arr[0];
        }

        @DexIgnore
        public qx0 a(SQLiteDatabase sQLiteDatabase) {
            return b(this.b, sQLiteDatabase);
        }

        @DexIgnore
        public lx0 c() {
            lx0 a2;
            synchronized (this) {
                this.d = false;
                SQLiteDatabase writableDatabase = super.getWritableDatabase();
                if (this.d) {
                    close();
                    a2 = c();
                } else {
                    a2 = a(writableDatabase);
                }
            }
            return a2;
        }

        @DexIgnore
        @Override // java.lang.AutoCloseable
        public void close() {
            synchronized (this) {
                super.close();
                this.b[0] = null;
            }
        }

        @DexIgnore
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.c.b(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.c.d(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.d = true;
            this.c.e(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.d) {
                this.c.f(a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.d = true;
            this.c.g(a(sQLiteDatabase), i, i2);
        }
    }

    @DexIgnore
    public rx0(Context context, String str, mx0.a aVar, boolean z) {
        this.b = context;
        this.c = str;
        this.d = aVar;
        this.e = z;
    }

    @DexIgnore
    public final a a() {
        a aVar;
        synchronized (this.f) {
            if (this.g == null) {
                qx0[] qx0Arr = new qx0[1];
                if (Build.VERSION.SDK_INT < 23 || this.c == null || !this.e) {
                    this.g = new a(this.b, this.c, qx0Arr, this.d);
                } else {
                    this.g = new a(this.b, new File(this.b.getNoBackupFilesDir(), this.c).getAbsolutePath(), qx0Arr, this.d);
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    this.g.setWriteAheadLoggingEnabled(this.h);
                }
            }
            aVar = this.g;
        }
        return aVar;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.mx0, java.lang.AutoCloseable
    public void close() {
        a().close();
    }

    @DexIgnore
    @Override // com.fossil.mx0
    public String getDatabaseName() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.mx0
    public lx0 getWritableDatabase() {
        return a().c();
    }

    @DexIgnore
    @Override // com.fossil.mx0
    public void setWriteAheadLoggingEnabled(boolean z) {
        synchronized (this.f) {
            if (this.g != null) {
                this.g.setWriteAheadLoggingEnabled(z);
            }
            this.h = z;
        }
    }
}
