package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k6 extends BluetoothGattCallback {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ k5 f1864a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public k6(k5 k5Var) {
        this.f1864a = k5Var;
    }

    @DexIgnore
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        k5 k5Var = this.f1864a;
        o6 o6Var = p6.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            pq7.b(uuid, "characteristic!!.uuid");
            n6 a2 = o6Var.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            k5Var.q(a2, value);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        k5 k5Var = this.f1864a;
        g7 a2 = g7.d.a(i);
        o6 o6Var = p6.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            pq7.b(uuid, "characteristic!!.uuid");
            n6 a3 = o6Var.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            k5Var.b.post(new y4(k5Var, a2, a3, value));
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        k5 k5Var = this.f1864a;
        g7 a2 = g7.d.a(i);
        o6 o6Var = p6.b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        pq7.b(uuid, "characteristic.uuid");
        n6 a3 = o6Var.a(uuid);
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value == null) {
            value = new byte[0];
        }
        k5Var.b.post(new g5(k5Var, a2, a3, value));
    }

    @DexIgnore
    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        LinkedHashSet<Long> linkedHashSet;
        LinkedHashSet<Long> linkedHashSet2;
        BluetoothDevice device;
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        BluetoothDevice device2 = bluetoothGatt.getDevice();
        if ((device2 != null ? device2.getAddress() : null) != null) {
            BluetoothDevice device3 = bluetoothGatt.getDevice();
            String address = device3 != null ? device3.getAddress() : null;
            BluetoothGatt bluetoothGatt2 = this.f1864a.c;
            if (pq7.a(address, (bluetoothGatt2 == null || (device = bluetoothGatt2.getDevice()) == null) ? null : device.getAddress())) {
                ky1 ky1 = ky1.DEBUG;
                if (i == 0) {
                    k5 k5Var = this.f1864a;
                    v4 v4Var = k5Var.f;
                    if (v4Var != null) {
                        k5Var.e(v4Var);
                    }
                    k5 k5Var2 = this.f1864a;
                    k5Var2.f = null;
                    k5Var2.e(new v4(i, i2, dn7.c(Long.valueOf(System.currentTimeMillis()))));
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    k5 k5Var3 = this.f1864a;
                    if (currentTimeMillis - k5Var3.d < ((long) 3000)) {
                        k5Var3.e = true;
                    }
                    v4 v4Var2 = this.f1864a.f;
                    if (v4Var2 != null && v4Var2.f3712a == i && v4Var2.b == i2) {
                        if (((v4Var2 == null || (linkedHashSet2 = v4Var2.c) == null) ? 0 : linkedHashSet2.size()) < 1000) {
                            v4 v4Var3 = this.f1864a.f;
                            if (!(v4Var3 == null || (linkedHashSet = v4Var3.c) == null)) {
                                linkedHashSet.add(Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    }
                    k5 k5Var4 = this.f1864a;
                    v4 v4Var4 = k5Var4.f;
                    if (v4Var4 != null) {
                        k5Var4.e(v4Var4);
                    }
                    this.f1864a.f = new v4(i, i2, dn7.c(Long.valueOf(System.currentTimeMillis())));
                }
                this.f1864a.c(i2, i);
            }
        }
    }

    @DexIgnore
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        ky1 ky1 = ky1.DEBUG;
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        pq7.b(characteristic, "descriptor.characteristic");
        characteristic.getUuid();
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        UUID uuid = characteristic2 != null ? characteristic2.getUuid() : null;
        UUID uuid2 = bluetoothGattDescriptor.getUuid();
        byte[] value = bluetoothGattDescriptor.getValue();
        if (value == null) {
            value = new byte[0];
        }
        if (uuid == null || uuid2 == null) {
            ky1 ky12 = ky1.DEBUG;
            return;
        }
        k5 k5Var = this.f1864a;
        k5Var.b.post(new i5(k5Var, g7.d.a(i), p6.b.a(uuid), v6.d.a(uuid2), value));
    }

    @DexIgnore
    public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onMtuChanged(bluetoothGatt, i, i2);
        if (i2 == 0) {
            this.f1864a.m = i - 3;
        }
        k5 k5Var = this.f1864a;
        k5Var.b.post(new e5(k5Var, g7.d.a(i2), i));
    }

    @DexIgnore
    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        k5 k5Var = this.f1864a;
        k5Var.b.post(new a5(k5Var, g7.d.a(i2), i));
    }

    @DexIgnore
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        super.onServicesDiscovered(bluetoothGatt, i);
        List<BluetoothGattService> services = bluetoothGatt.getServices();
        k5 k5Var = this.f1864a;
        pq7.b(services, "services");
        k5Var.s(services);
        ky1 ky1 = ky1.DEBUG;
        pm7.N(services, "\n", null, null, 0, null, l5.b, 30, null);
        k5 k5Var2 = this.f1864a;
        g7 a2 = g7.d.a(i);
        ArrayList arrayList = new ArrayList(im7.m(services, 10));
        for (T t : services) {
            pq7.b(t, "it");
            arrayList.add(t.getUuid());
        }
        Object[] array = arrayList.toArray(new UUID[0]);
        if (array != null) {
            UUID[] uuidArr = (UUID[]) array;
            Set<n6> keySet = this.f1864a.l.keySet();
            pq7.b(keySet, "gattCharacteristicMap.keys");
            Object[] array2 = keySet.toArray(new n6[0]);
            if (array2 != null) {
                k5Var2.b.post(new s4(k5Var2, a2, uuidArr, (n6[]) array2));
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
