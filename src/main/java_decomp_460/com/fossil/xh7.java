package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xh7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f4127a;
    @DexIgnore
    public String b;
    @DexIgnore
    public DisplayMetrics c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public int l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public Context o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;

    @DexIgnore
    public xh7(Context context) {
        this.b = "2.0.3";
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        Context applicationContext = context.getApplicationContext();
        this.o = applicationContext;
        this.c = ei7.u(applicationContext);
        this.f4127a = ei7.F(this.o);
        this.h = fg7.v(this.o);
        this.i = ei7.E(this.o);
        this.j = TimeZone.getDefault().getID();
        this.l = ei7.K(this.o);
        this.k = ei7.L(this.o);
        this.m = this.o.getPackageName();
        if (this.d >= 14) {
            this.p = ei7.R(this.o);
        }
        this.q = ei7.Q(this.o).toString();
        this.r = ei7.P(this.o);
        this.s = ei7.v();
        this.n = ei7.b(this.o);
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String w;
        String str;
        if (thread == null) {
            if (this.c != null) {
                jSONObject.put("sr", this.c.widthPixels + g78.ANY_MARKER + this.c.heightPixels);
                jSONObject.put("dpi", this.c.xdpi + g78.ANY_MARKER + this.c.ydpi);
            }
            if (tg7.a(this.o).i()) {
                JSONObject jSONObject2 = new JSONObject();
                ji7.d(jSONObject2, "bs", ji7.i(this.o));
                ji7.d(jSONObject2, "ss", ji7.j(this.o));
                if (jSONObject2.length() > 0) {
                    ji7.d(jSONObject, "wf", jSONObject2.toString());
                }
            }
            JSONArray c2 = ji7.c(this.o, 10);
            if (c2 != null && c2.length() > 0) {
                ji7.d(jSONObject, "wflist", c2.toString());
            }
            w = this.p;
            str = "sen";
        } else {
            ji7.d(jSONObject, "thn", thread.getName());
            ji7.d(jSONObject, "qq", fg7.E(this.o));
            ji7.d(jSONObject, "cui", fg7.u(this.o));
            if (ei7.t(this.r) && this.r.split("/").length == 2) {
                ji7.d(jSONObject, "fram", this.r.split("/")[0]);
            }
            if (ei7.t(this.s) && this.s.split("/").length == 2) {
                ji7.d(jSONObject, "from", this.s.split("/")[0]);
            }
            if (gh7.b(this.o).v(this.o) != null) {
                jSONObject.put("ui", gh7.b(this.o).v(this.o).c());
            }
            w = fg7.w(this.o);
            str = "mid";
        }
        ji7.d(jSONObject, str, w);
        ji7.d(jSONObject, "pcn", ei7.M(this.o));
        ji7.d(jSONObject, "osn", Build.VERSION.RELEASE);
        ji7.d(jSONObject, "av", this.f4127a);
        ji7.d(jSONObject, "ch", this.h);
        ji7.d(jSONObject, "mf", this.f);
        ji7.d(jSONObject, "sv", this.b);
        ji7.d(jSONObject, "osd", Build.DISPLAY);
        ji7.d(jSONObject, "prod", Build.PRODUCT);
        ji7.d(jSONObject, "tags", Build.TAGS);
        ji7.d(jSONObject, "id", Build.ID);
        ji7.d(jSONObject, "fng", Build.FINGERPRINT);
        ji7.d(jSONObject, "lch", this.n);
        ji7.d(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        ji7.d(jSONObject, "op", this.i);
        ji7.d(jSONObject, "lg", this.g);
        ji7.d(jSONObject, "md", this.e);
        ji7.d(jSONObject, "tz", this.j);
        int i2 = this.l;
        if (i2 != 0) {
            jSONObject.put("jb", i2);
        }
        ji7.d(jSONObject, "sd", this.k);
        ji7.d(jSONObject, "apn", this.m);
        ji7.d(jSONObject, "cpu", this.q);
        ji7.d(jSONObject, "abi", Build.CPU_ABI);
        ji7.d(jSONObject, "abi2", Build.CPU_ABI2);
        ji7.d(jSONObject, "ram", this.r);
        ji7.d(jSONObject, "rom", this.s);
    }
}
