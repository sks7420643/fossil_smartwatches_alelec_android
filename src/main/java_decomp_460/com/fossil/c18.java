package com.fossil;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c18 {
    @DexIgnore
    public static /* final */ c18 c; // = new a().a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<b> f544a;
    @DexIgnore
    public /* final */ a48 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<b> f545a; // = new ArrayList();

        @DexIgnore
        public c18 a() {
            return new c18(new LinkedHashSet(this.f545a), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f546a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ l48 d;

        @DexIgnore
        public boolean a(String str) {
            if (!this.f546a.startsWith("*.")) {
                return str.equals(this.b);
            }
            int indexOf = str.indexOf(46);
            if ((str.length() - indexOf) - 1 != this.b.length()) {
                return false;
            }
            String str2 = this.b;
            return str.regionMatches(false, indexOf + 1, str2, 0, str2.length());
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                b bVar = (b) obj;
                return this.f546a.equals(bVar.f546a) && this.c.equals(bVar.c) && this.d.equals(bVar.d);
            }
        }

        @DexIgnore
        public int hashCode() {
            return ((((this.f546a.hashCode() + 527) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.c + this.d.base64();
        }
    }

    @DexIgnore
    public c18(Set<b> set, a48 a48) {
        this.f544a = set;
        this.b = a48;
    }

    @DexIgnore
    public static String c(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + e((X509Certificate) certificate).base64();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    @DexIgnore
    public static l48 d(X509Certificate x509Certificate) {
        return l48.of(x509Certificate.getPublicKey().getEncoded()).sha1();
    }

    @DexIgnore
    public static l48 e(X509Certificate x509Certificate) {
        return l48.of(x509Certificate.getPublicKey().getEncoded()).sha256();
    }

    @DexIgnore
    public void a(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List<b> b2 = b(str);
        if (!b2.isEmpty()) {
            a48 a48 = this.b;
            if (a48 != null) {
                list = a48.a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = b2.size();
                int i2 = 0;
                l48 l48 = null;
                l48 l482 = null;
                while (i2 < size2) {
                    b bVar = b2.get(i2);
                    if (bVar.c.equals("sha256/")) {
                        if (l48 == null) {
                            l48 = e(x509Certificate);
                        }
                        if (bVar.d.equals(l48)) {
                            return;
                        }
                    } else if (bVar.c.equals("sha1/")) {
                        if (l482 == null) {
                            l482 = d(x509Certificate);
                        }
                        if (bVar.d.equals(l482)) {
                            return;
                        }
                    } else {
                        throw new AssertionError("unsupported hashAlgorithm: " + bVar.c);
                    }
                    i2++;
                    l48 = l48;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Certificate pinning failure!");
            sb.append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                sb.append("\n    ");
                sb.append(c(x509Certificate2));
                sb.append(": ");
                sb.append(x509Certificate2.getSubjectDN().getName());
            }
            sb.append("\n  Pinned certificates for ");
            sb.append(str);
            sb.append(":");
            int size4 = b2.size();
            for (int i4 = 0; i4 < size4; i4++) {
                sb.append("\n    ");
                sb.append(b2.get(i4));
            }
            throw new SSLPeerUnverifiedException(sb.toString());
        }
    }

    @DexIgnore
    public List<b> b(String str) {
        List<b> emptyList = Collections.emptyList();
        List<b> list = emptyList;
        for (b bVar : this.f544a) {
            if (bVar.a(str)) {
                if (list.isEmpty()) {
                    list = new ArrayList<>();
                }
                list.add(bVar);
            }
        }
        return list;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof c18) {
            c18 c18 = (c18) obj;
            return b28.q(this.b, c18.b) && this.f544a.equals(c18.f544a);
        }
    }

    @DexIgnore
    public c18 f(a48 a48) {
        return b28.q(this.b, a48) ? this : new c18(this.f544a, a48);
    }

    @DexIgnore
    public int hashCode() {
        a48 a48 = this.b;
        return ((a48 != null ? a48.hashCode() : 0) * 31) + this.f544a.hashCode();
    }
}
