package com.fossil;

import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i01 {
    @DexIgnore
    public static /* final */ ViewGroup.MarginLayoutParams b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public LinearLayoutManager f1565a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<int[]> {
        @DexIgnore
        public a(i01 i01) {
        }

        @DexIgnore
        /* renamed from: a */
        public int compare(int[] iArr, int[] iArr2) {
            return iArr[0] - iArr2[0];
        }
    }

    /*
    static {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -1);
        b = marginLayoutParams;
        marginLayoutParams.setMargins(0, 0, 0, 0);
    }
    */

    @DexIgnore
    public i01(LinearLayoutManager linearLayoutManager) {
        this.f1565a = linearLayoutManager;
    }

    @DexIgnore
    public static boolean c(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            LayoutTransition layoutTransition = viewGroup.getLayoutTransition();
            if (layoutTransition != null && layoutTransition.isChangingLayout()) {
                return true;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (c(viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean a() {
        int top;
        int i;
        int bottom;
        int i2;
        int K = this.f1565a.K();
        if (K == 0) {
            return true;
        }
        boolean z = this.f1565a.q2() == 0;
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, K, 2);
        for (int i3 = 0; i3 < K; i3++) {
            View J = this.f1565a.J(i3);
            if (J != null) {
                ViewGroup.LayoutParams layoutParams = J.getLayoutParams();
                ViewGroup.MarginLayoutParams marginLayoutParams = layoutParams instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams : b;
                int[] iArr2 = iArr[i3];
                if (z) {
                    top = J.getLeft();
                    i = marginLayoutParams.leftMargin;
                } else {
                    top = J.getTop();
                    i = marginLayoutParams.topMargin;
                }
                iArr2[0] = top - i;
                int[] iArr3 = iArr[i3];
                if (z) {
                    bottom = J.getRight();
                    i2 = marginLayoutParams.rightMargin;
                } else {
                    bottom = J.getBottom();
                    i2 = marginLayoutParams.bottomMargin;
                }
                iArr3[1] = bottom + i2;
            } else {
                throw new IllegalStateException("null view contained in the view hierarchy");
            }
        }
        Arrays.sort(iArr, new a(this));
        for (int i4 = 1; i4 < K; i4++) {
            if (iArr[i4 - 1][1] != iArr[i4][0]) {
                return false;
            }
        }
        return iArr[0][0] <= 0 && iArr[K + -1][1] >= iArr[0][1] - iArr[0][0];
    }

    @DexIgnore
    public final boolean b() {
        int K = this.f1565a.K();
        for (int i = 0; i < K; i++) {
            if (c(this.f1565a.J(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean d() {
        return (!a() || this.f1565a.K() <= 1) && b();
    }
}
