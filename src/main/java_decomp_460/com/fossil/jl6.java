package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl6 extends du0<WorkoutSession, b> {
    @DexIgnore
    public /* final */ String c; // = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public a d;
    @DexIgnore
    public c e;
    @DexIgnore
    public ai5 f;
    @DexIgnore
    public String g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void B2(WorkoutSession workoutSession);

        @DexIgnore
        void t4(WorkoutSession workoutSession);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public WorkoutSession f1777a;
        @DexIgnore
        public /* final */ eg5 b;
        @DexIgnore
        public /* final */ /* synthetic */ jl6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession c;

            @DexIgnore
            public a(b bVar, WorkoutSession workoutSession) {
                this.b = bVar;
                this.c = workoutSession;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.c.d != null) {
                    a aVar = this.b.c.d;
                    if (aVar != null) {
                        aVar.B2(this.c);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jl6$b$b")
        /* renamed from: com.fossil.jl6$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0131b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession c;

            @DexIgnore
            public View$OnClickListenerC0131b(b bVar, WorkoutSession workoutSession) {
                this.b = bVar;
                this.c = workoutSession;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.c.d != null) {
                    a aVar = this.b.c.d;
                    if (aVar != null) {
                        aVar.t4(this.c);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(jl6 jl6, eg5 eg5) {
            super(eg5.n());
            pq7.c(eg5, "mBinding");
            this.c = jl6;
            this.b = eg5;
            String str = jl6.c;
            if (str != null) {
                this.b.q.setBackgroundColor(Color.parseColor(str));
                ImageView imageView = this.b.H;
                pq7.b(imageView, "mBinding.ivWorkout");
                Drawable background = imageView.getBackground();
                if (background instanceof ShapeDrawable) {
                    Paint paint = ((ShapeDrawable) background).getPaint();
                    pq7.b(paint, "drawable.paint");
                    paint.setColor(Color.parseColor(str));
                } else if (background instanceof GradientDrawable) {
                    ((GradientDrawable) background).setColor(Color.parseColor(str));
                } else if (background instanceof ColorDrawable) {
                    ((ColorDrawable) background).setColor(Color.parseColor(str));
                }
            }
        }

        @DexIgnore
        public final void a(WorkoutSession workoutSession) {
            pq7.c(workoutSession, "value");
            this.f1777a = workoutSession;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutAdapter", "build - value=" + workoutSession);
            jl6 jl6 = this.c;
            View n = this.b.n();
            pq7.b(n, "mBinding.root");
            Context context = n.getContext();
            pq7.b(context, "mBinding.root.context");
            WorkoutSession workoutSession2 = this.f1777a;
            if (workoutSession2 != null) {
                d o = jl6.o(context, workoutSession2);
                mi5 editedType = workoutSession.getEditedType();
                if (editedType == null) {
                    editedType = workoutSession.getWorkoutType();
                }
                gi5 editedMode = workoutSession.getEditedMode();
                if (editedMode == null) {
                    editedMode = workoutSession.getMode();
                }
                this.b.E.setImageResource(0);
                this.b.H.setImageResource(o.i());
                FlexibleTextView flexibleTextView = this.b.y;
                pq7.b(flexibleTextView, "mBinding.ftvWorkoutTitle");
                flexibleTextView.setText(o.n());
                FlexibleTextView flexibleTextView2 = this.b.y;
                pq7.b(flexibleTextView2, "mBinding.ftvWorkoutTitle");
                flexibleTextView2.setSelected(true);
                FlexibleTextView flexibleTextView3 = this.b.x;
                pq7.b(flexibleTextView3, "mBinding.ftvWorkoutTime");
                flexibleTextView3.setText(o.m());
                FlexibleTextView flexibleTextView4 = this.b.r;
                pq7.b(flexibleTextView4, "mBinding.ftvActiveTime");
                flexibleTextView4.setText(o.a());
                FlexibleTextView flexibleTextView5 = this.b.t;
                pq7.b(flexibleTextView5, "mBinding.ftvDistance");
                flexibleTextView5.setText(o.d());
                FlexibleTextView flexibleTextView6 = this.b.w;
                pq7.b(flexibleTextView6, "mBinding.ftvSteps");
                flexibleTextView6.setText(o.l());
                FlexibleTextView flexibleTextView7 = this.b.s;
                pq7.b(flexibleTextView7, "mBinding.ftvCalories");
                flexibleTextView7.setText(o.c());
                FlexibleTextView flexibleTextView8 = this.b.u;
                pq7.b(flexibleTextView8, "mBinding.ftvHeartRateAvg");
                flexibleTextView8.setText(o.e());
                FlexibleTextView flexibleTextView9 = this.b.v;
                pq7.b(flexibleTextView9, "mBinding.ftvHeartRateMax");
                flexibleTextView9.setText(o.f());
                ImageView imageView = this.b.E;
                pq7.b(imageView, "mBinding.ivMap");
                nl5.a(imageView, new a(this, workoutSession));
                if (!TextUtils.isEmpty(workoutSession.getScreenShotUri())) {
                    ImageView imageView2 = this.b.E;
                    pq7.b(imageView2, "mBinding.ivMap");
                    imageView2.setVisibility(0);
                    FlexibleProgressBar flexibleProgressBar = this.b.I;
                    pq7.b(flexibleProgressBar, "mBinding.progressBar");
                    flexibleProgressBar.setVisibility(8);
                    ImageView imageView3 = this.b.E;
                    pq7.b(imageView3, "mBinding.ivMap");
                    wj5 a2 = tj5.a(imageView3.getContext());
                    String screenShotUri = workoutSession.getScreenShotUri();
                    if (screenShotUri != null) {
                        pq7.b(a2.t(screenShotUri).F0(this.b.E), "GlideApp.with(mBinding.i\u2026    .into(mBinding.ivMap)");
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
                    if (workoutGpsPoints == null || !(!workoutGpsPoints.isEmpty())) {
                        ImageView imageView4 = this.b.E;
                        pq7.b(imageView4, "mBinding.ivMap");
                        imageView4.setVisibility(8);
                        FlexibleProgressBar flexibleProgressBar2 = this.b.I;
                        pq7.b(flexibleProgressBar2, "mBinding.progressBar");
                        flexibleProgressBar2.setVisibility(8);
                    } else {
                        ImageView imageView5 = this.b.E;
                        pq7.b(imageView5, "mBinding.ivMap");
                        imageView5.setVisibility(0);
                        FlexibleProgressBar flexibleProgressBar3 = this.b.I;
                        pq7.b(flexibleProgressBar3, "mBinding.progressBar");
                        flexibleProgressBar3.setVisibility(0);
                    }
                }
                this.b.F.setOnClickListener(new View$OnClickListenerC0131b(this, workoutSession));
                int d = TextUtils.isEmpty(this.c.g) ? gl0.d(PortfolioApp.h0.c(), 2131099812) : Color.parseColor(this.c.g);
                ColorStateList valueOf = ColorStateList.valueOf(d);
                pq7.b(valueOf, "ColorStateList.valueOf(color)");
                int i = ll6.b[this.c.e.ordinal()];
                if (i == 1) {
                    this.b.z.setColorFilter(d);
                    this.b.r.setTextColor(d);
                    ImageView imageView6 = this.b.F;
                    pq7.b(imageView6, "mBinding.ivMore");
                    imageView6.setVisibility(8);
                } else if (i == 2) {
                    Drawable f = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231069);
                    Drawable f2 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231086);
                    Drawable f3 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231067);
                    this.b.G.setImageDrawable(f);
                    this.b.D.setImageDrawable(f2);
                    this.b.C.setImageDrawable(f3);
                    if (editedType != null) {
                        int i2 = ll6.f2216a[editedType.ordinal()];
                        if (i2 == 1 || i2 == 2 || i2 == 3) {
                            Drawable f4 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231119);
                            Drawable f5 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231100);
                            this.b.C.setImageDrawable(f4);
                            FlexibleTextView flexibleTextView10 = this.b.u;
                            pq7.b(flexibleTextView10, "mBinding.ftvHeartRateAvg");
                            flexibleTextView10.setText(o.g());
                            this.b.D.setImageDrawable(f5);
                            FlexibleTextView flexibleTextView11 = this.b.v;
                            pq7.b(flexibleTextView11, "mBinding.ftvHeartRateMax");
                            flexibleTextView11.setText(o.h());
                        } else if (i2 == 4 && editedMode == gi5.OUTDOOR) {
                            Drawable f6 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231002);
                            Drawable f7 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231101);
                            Drawable f8 = gl0.f(PortfolioApp.h0.c().getApplicationContext(), 2131231044);
                            this.b.C.setImageDrawable(f6);
                            FlexibleTextView flexibleTextView12 = this.b.u;
                            pq7.b(flexibleTextView12, "mBinding.ftvHeartRateAvg");
                            flexibleTextView12.setText(o.j());
                            this.b.D.setImageDrawable(f7);
                            FlexibleTextView flexibleTextView13 = this.b.v;
                            pq7.b(flexibleTextView13, "mBinding.ftvHeartRateMax");
                            flexibleTextView13.setText(o.k());
                            this.b.G.setImageDrawable(f8);
                            FlexibleTextView flexibleTextView14 = this.b.w;
                            pq7.b(flexibleTextView14, "mBinding.ftvSteps");
                            flexibleTextView14.setText(o.b());
                        }
                    }
                    this.b.G.setColorFilter(d);
                    this.b.w.setTextColor(d);
                } else if (i == 3) {
                    this.b.A.setColorFilter(d);
                    this.b.s.setTextColor(d);
                    ImageView imageView7 = this.b.F;
                    pq7.b(imageView7, "mBinding.ivMore");
                    imageView7.setVisibility(8);
                } else if (i == 4) {
                    this.b.C.setColorFilter(d);
                    this.b.D.setColorFilter(d);
                    this.b.u.setTextColor(d);
                    this.b.v.setTextColor(d);
                    ImageView imageView8 = this.b.F;
                    pq7.b(imageView8, "mBinding.ivMore");
                    imageView8.setVisibility(8);
                }
                ImageView imageView9 = this.b.H;
                pq7.b(imageView9, "mBinding.ivWorkout");
                imageView9.setImageTintList(valueOf);
                return;
            }
            pq7.n("mValue");
            throw null;
        }
    }

    @DexIgnore
    public enum c {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        HEART_RATE
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f1778a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public String l;
        @DexIgnore
        public String m;
        @DexIgnore
        public String n;

        @DexIgnore
        public d(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13) {
            pq7.c(str, "mTitle");
            pq7.c(str2, "mTime");
            pq7.c(str3, "mActiveMinute");
            pq7.c(str4, "mDistance");
            pq7.c(str5, "mSteps");
            pq7.c(str6, "mCalories");
            pq7.c(str7, "mHeartRateAvg");
            pq7.c(str8, "mHeartRateMax");
            pq7.c(str9, "mPaceAvg");
            pq7.c(str10, "mPaceMax");
            pq7.c(str11, "mSpeedAvg");
            pq7.c(str12, "mSpeedMax");
            pq7.c(str13, "mCadence");
            this.f1778a = i2;
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = str5;
            this.g = str6;
            this.h = str7;
            this.i = str8;
            this.j = str9;
            this.k = str10;
            this.l = str11;
            this.m = str12;
            this.n = str13;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final String b() {
            return this.n;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final String e() {
            return this.h;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof d) {
                    d dVar = (d) obj;
                    if (this.f1778a != dVar.f1778a || !pq7.a(this.b, dVar.b) || !pq7.a(this.c, dVar.c) || !pq7.a(this.d, dVar.d) || !pq7.a(this.e, dVar.e) || !pq7.a(this.f, dVar.f) || !pq7.a(this.g, dVar.g) || !pq7.a(this.h, dVar.h) || !pq7.a(this.i, dVar.i) || !pq7.a(this.j, dVar.j) || !pq7.a(this.k, dVar.k) || !pq7.a(this.l, dVar.l) || !pq7.a(this.m, dVar.m) || !pq7.a(this.n, dVar.n)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String f() {
            return this.i;
        }

        @DexIgnore
        public final String g() {
            return this.j;
        }

        @DexIgnore
        public final String h() {
            return this.k;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            int i3 = this.f1778a;
            String str = this.b;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.c;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.d;
            int hashCode3 = str3 != null ? str3.hashCode() : 0;
            String str4 = this.e;
            int hashCode4 = str4 != null ? str4.hashCode() : 0;
            String str5 = this.f;
            int hashCode5 = str5 != null ? str5.hashCode() : 0;
            String str6 = this.g;
            int hashCode6 = str6 != null ? str6.hashCode() : 0;
            String str7 = this.h;
            int hashCode7 = str7 != null ? str7.hashCode() : 0;
            String str8 = this.i;
            int hashCode8 = str8 != null ? str8.hashCode() : 0;
            String str9 = this.j;
            int hashCode9 = str9 != null ? str9.hashCode() : 0;
            String str10 = this.k;
            int hashCode10 = str10 != null ? str10.hashCode() : 0;
            String str11 = this.l;
            int hashCode11 = str11 != null ? str11.hashCode() : 0;
            String str12 = this.m;
            int hashCode12 = str12 != null ? str12.hashCode() : 0;
            String str13 = this.n;
            if (str13 != null) {
                i2 = str13.hashCode();
            }
            return ((((((((((((((((((((((((hashCode + (i3 * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + i2;
        }

        @DexIgnore
        public final int i() {
            return this.f1778a;
        }

        @DexIgnore
        public final String j() {
            return this.l;
        }

        @DexIgnore
        public final String k() {
            return this.m;
        }

        @DexIgnore
        public final String l() {
            return this.f;
        }

        @DexIgnore
        public final String m() {
            return this.c;
        }

        @DexIgnore
        public final String n() {
            return this.b;
        }

        @DexIgnore
        public String toString() {
            return "WorkoutModel(mResIconId=" + this.f1778a + ", mTitle=" + this.b + ", mTime=" + this.c + ", mActiveMinute=" + this.d + ", mDistance=" + this.e + ", mSteps=" + this.f + ", mCalories=" + this.g + ", mHeartRateAvg=" + this.h + ", mHeartRateMax=" + this.i + ", mPaceAvg=" + this.j + ", mPaceMax=" + this.k + ", mSpeedAvg=" + this.l + ", mSpeedMax=" + this.m + ", mCadence=" + this.n + ")";
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jl6(c cVar, ai5 ai5, WorkoutSessionDifference workoutSessionDifference, String str) {
        super(workoutSessionDifference);
        pq7.c(cVar, "mWorkoutItem");
        pq7.c(ai5, "mDistanceUnit");
        pq7.c(workoutSessionDifference, "workoutSessionDiff");
        this.e = cVar;
        this.f = ai5;
        this.g = str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0608  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x038d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.jl6.d o(android.content.Context r29, com.portfolio.platform.data.model.diana.workout.WorkoutSession r30) {
        /*
        // Method dump skipped, instructions count: 1551
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jl6.o(android.content.Context, com.portfolio.platform.data.model.diana.workout.WorkoutSession):com.fossil.jl6$d");
    }

    @DexIgnore
    public final String p(float f2) {
        if (f2 <= ((float) 0)) {
            return "--";
        }
        cl7<Integer, Integer> s = s(f2);
        hr7 hr7 = hr7.f1520a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{s.getFirst(), s.getSecond()}, 2));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    /* renamed from: q */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        WorkoutSession workoutSession = (WorkoutSession) getItem(i);
        if (workoutSession != null) {
            bVar.a(workoutSession);
        }
    }

    @DexIgnore
    /* renamed from: r */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        eg5 z = eg5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemWorkoutBinding.infla\u2026.context), parent, false)");
        return new b(this, z);
    }

    @DexIgnore
    public final cl7<Integer, Integer> s(float f2) {
        int i = (int) (f2 / ((float) 60));
        return new cl7<>(Integer.valueOf(i), Integer.valueOf((int) (f2 - ((float) (i * 60)))));
    }

    @DexIgnore
    public final void t(ai5 ai5, cu0<WorkoutSession> cu0) {
        pq7.c(ai5, "distanceUnit");
        pq7.c(cu0, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutAdapter", "setData - distanceUnit=" + ai5 + ", data=" + cu0.size());
        this.f = ai5;
        i(cu0);
    }

    @DexIgnore
    public final void u(a aVar) {
        this.d = aVar;
    }
}
