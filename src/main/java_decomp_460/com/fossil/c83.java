package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c83 implements z73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Long> f581a;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        hw2.b("measurement.id.max_bundles_per_iteration", 0);
        f581a = hw2.b("measurement.max_bundles_per_iteration", 2);
    }
    */

    @DexIgnore
    @Override // com.fossil.z73
    public final long zza() {
        return f581a.o().longValue();
    }
}
