package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h93 implements d93 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1453a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.integration.disable_firebase_instance_id", false);

    @DexIgnore
    @Override // com.fossil.d93
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.d93
    public final boolean zzb() {
        return f1453a.o().booleanValue();
    }
}
