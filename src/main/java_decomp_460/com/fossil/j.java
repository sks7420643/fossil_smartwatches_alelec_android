package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j extends qq7 implements rp7<nr, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(BluetoothDevice bluetoothDevice) {
        super(1);
        this.b = bluetoothDevice;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(nr nrVar) {
        tk1 tk1 = tk1.k;
        tk1.d.remove(this.b.getAddress());
        return tl7.f3441a;
    }
}
