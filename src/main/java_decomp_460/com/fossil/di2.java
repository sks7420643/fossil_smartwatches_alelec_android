package com.fossil;

import com.facebook.places.PlaceManager;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class di2 {
    @DexIgnore
    public static /* final */ double c; // = (10.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ double d; // = (1000.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ double e; // = (2000.0d / ((double) TimeUnit.HOURS.toNanos(1)));
    @DexIgnore
    public static /* final */ double f; // = (100.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ Set<String> g; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(PlaceManager.PARAM_ALTITUDE, "duration", "food_item", "meal_type", "repetitions", "resistance", "resistance_type", "debug_session", "google.android.fitness.SessionV2")));
    @DexIgnore
    public static /* final */ di2 h; // = new di2();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<String, Map<String, ei2>> f790a;
    @DexIgnore
    public /* final */ Map<String, ei2> b;

    @DexIgnore
    public di2() {
        HashMap hashMap = new HashMap();
        hashMap.put("latitude", new ei2(-90.0d, 90.0d));
        hashMap.put("longitude", new ei2(-180.0d, 180.0d));
        hashMap.put(PlaceManager.PARAM_ACCURACY, new ei2(0.0d, 10000.0d));
        hashMap.put("bpm", new ei2(0.0d, 1000.0d));
        hashMap.put(PlaceManager.PARAM_ALTITUDE, new ei2(-100000.0d, 100000.0d));
        hashMap.put("percentage", new ei2(0.0d, 100.0d));
        hashMap.put("confidence", new ei2(0.0d, 100.0d));
        hashMap.put("duration", new ei2(0.0d, 9.223372036854776E18d));
        hashMap.put("height", new ei2(0.0d, 3.0d));
        hashMap.put(Constants.PROFILE_KEY_UNITS_WEIGHT, new ei2(0.0d, 1000.0d));
        hashMap.put(PlaceManager.PARAM_SPEED, new ei2(0.0d, 11000.0d));
        this.b = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("com.google.step_count.delta", b("steps", new ei2(0.0d, c)));
        hashMap2.put("com.google.calories.consumed", b("calories", new ei2(0.0d, d)));
        hashMap2.put("com.google.calories.expended", b("calories", new ei2(0.0d, e)));
        hashMap2.put("com.google.distance.delta", b("distance", new ei2(0.0d, f)));
        this.f790a = Collections.unmodifiableMap(hashMap2);
    }

    @DexIgnore
    public static <K, V> Map<K, V> b(K k, V v) {
        HashMap hashMap = new HashMap();
        hashMap.put(k, v);
        return hashMap;
    }

    @DexIgnore
    public static di2 d() {
        return h;
    }

    @DexIgnore
    public final ei2 a(String str, String str2) {
        Map<String, ei2> map = this.f790a.get(str);
        if (map != null) {
            return map.get(str2);
        }
        return null;
    }

    @DexIgnore
    public final ei2 c(String str) {
        return this.b.get(str);
    }
}
