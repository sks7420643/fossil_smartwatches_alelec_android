package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ic3 extends ds2 implements hc3 {
    @DexIgnore
    public ic3() {
        super("com.google.android.gms.maps.internal.IOnInfoWindowClickListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        K2(ms2.e(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
