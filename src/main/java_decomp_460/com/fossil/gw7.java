package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gw7 implements sw7 {
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public gw7(boolean z) {
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public kx7 b() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.sw7
    public boolean isActive() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty{");
        sb.append(isActive() ? "Active" : "New");
        sb.append('}');
        return sb.toString();
    }
}
