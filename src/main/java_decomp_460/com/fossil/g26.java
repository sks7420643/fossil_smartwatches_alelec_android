package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g26 extends tq4<a, b, tq4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<j06> f1250a;
        @DexIgnore
        public /* final */ List<j06> b;

        @DexIgnore
        public a(List<j06> list, List<j06> list2) {
            pq7.c(list, "contactWrapperListRemoved");
            pq7.c(list2, "contactWrapperListAdded");
            this.f1250a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<j06> a() {
            return this.b;
        }

        @DexIgnore
        public final List<j06> b() {
            return this.f1250a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.c {
        @DexIgnore
        public b(boolean z) {
        }
    }

    /*
    static {
        String simpleName = g26.class.getSimpleName();
        pq7.b(simpleName, "SaveContactGroupsNotific\u2026on::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public g26(NotificationsRepository notificationsRepository) {
        pq7.c(notificationsRepository, "notificationsRepository");
        i14.o(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        pq7.b(notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    /* renamed from: f */
    public void a(a aVar) {
        pq7.c(aVar, "requestValues");
        g(aVar.b());
        i(aVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveContactGroupsNotification done");
        b().onSuccess(new b(true));
    }

    @DexIgnore
    public final void g(List<j06> list) {
        if (!list.isEmpty()) {
            ContactProvider d2 = mn5.p.a().d();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (j06 j06 : list) {
                Contact contact = j06.getContact();
                if (contact != null) {
                    arrayList2.add(contact);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    local.d(str, "Removed contact = " + contact.getFirstName() + " row id = " + contact.getDbRowId());
                    for (ContactGroup contactGroup : d2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue())) {
                        pq7.b(contactGroup, "contactGroupItem");
                        Iterator<Contact> it = contactGroup.getContacts().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Contact next = it.next();
                            int contactId = contact.getContactId();
                            pq7.b(next, "contactItem");
                            if (contactId == next.getContactId()) {
                                contact.setDbRowId(next.getDbRowId());
                                arrayList.add(contactGroup);
                                break;
                            }
                        }
                    }
                    for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
                        pq7.b(phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                        arrayList3.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            h(arrayList3);
        }
    }

    @DexIgnore
    public final void h(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(phoneFavoritesContact);
        }
    }

    @DexIgnore
    public final void i(List<j06> list) {
        Iterator<j06> it;
        if (!list.isEmpty()) {
            List<ContactGroup> allContactGroups = this.d.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ContactProvider d2 = mn5.p.a().d();
            Iterator<j06> it2 = list.iterator();
            while (it2.hasNext()) {
                j06 next = it2.next();
                if (allContactGroups != null) {
                    Iterator<ContactGroup> it3 = allContactGroups.iterator();
                    boolean z = false;
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        ContactGroup next2 = it3.next();
                        pq7.b(next2, "contactGroup");
                        Iterator<Contact> it4 = next2.getContacts().iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                z = z;
                                break;
                            }
                            Contact next3 = it4.next();
                            if (!(next.getContact() == null || next3 == null)) {
                                int contactId = next3.getContactId();
                                Contact contact = next.getContact();
                                if (contact != null && contactId == contact.getContactId()) {
                                    Contact contact2 = next.getContact();
                                    Boolean valueOf = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                                    if (valueOf != null) {
                                        next3.setUseCall(valueOf.booleanValue());
                                        Contact contact3 = next.getContact();
                                        Boolean valueOf2 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                                        if (valueOf2 != null) {
                                            next3.setUseSms(valueOf2.booleanValue());
                                            Contact contact4 = next.getContact();
                                            next3.setFirstName(contact4 != null ? contact4.getFirstName() : null);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String str = e;
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("Contact Id = ");
                                            Contact contact5 = next.getContact();
                                            sb.append(contact5 != null ? Integer.valueOf(contact5.getContactId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact name = ");
                                            Contact contact6 = next.getContact();
                                            sb.append(contact6 != null ? contact6.getFirstName() : null);
                                            sb.append(',');
                                            sb.append("Contact db row = ");
                                            Contact contact7 = next.getContact();
                                            sb.append(contact7 != null ? Integer.valueOf(contact7.getDbRowId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact phone = ");
                                            sb.append(next.getPhoneNumber());
                                            local.d(str, sb.toString());
                                            next.setContact(next3);
                                            d2.removeContactGroup(next2);
                                            allContactGroups.remove(next2);
                                            z = true;
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    } else {
                                        pq7.i();
                                        throw null;
                                    }
                                }
                            }
                        }
                        if (z) {
                            it = it2;
                            break;
                        }
                    }
                }
                it = it2;
                ContactGroup contactGroup = new ContactGroup();
                contactGroup.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                contactGroup.setHour(next.getCurrentHandGroup());
                arrayList2.add(contactGroup);
                Contact contact8 = next.getContact();
                if (contact8 != null) {
                    contact8.setContactGroup(contactGroup);
                }
                if (contact8 != null) {
                    Contact contact9 = next.getContact();
                    Boolean valueOf3 = contact9 != null ? Boolean.valueOf(contact9.isUseCall()) : null;
                    if (valueOf3 != null) {
                        contact8.setUseCall(valueOf3.booleanValue());
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (contact8 != null) {
                    Contact contact10 = next.getContact();
                    Boolean valueOf4 = contact10 != null ? Boolean.valueOf(contact10.isUseSms()) : null;
                    if (valueOf4 != null) {
                        contact8.setUseSms(valueOf4.booleanValue());
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                if (contact8 != null) {
                    contact8.setUseEmail(false);
                }
                if (contact8 != null) {
                    arrayList.add(contact8);
                    ContentResolver contentResolver = PortfolioApp.h0.c().getContentResolver();
                    if (next.hasPhoneNumber()) {
                        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                        Cursor query = contentResolver.query(uri, new String[]{"data1"}, "contact_id=" + contact8.getContactId(), null, null);
                        if (query != null) {
                            while (query.moveToNext()) {
                                try {
                                    PhoneNumber phoneNumber = new PhoneNumber();
                                    phoneNumber.setNumber(query.getString(query.getColumnIndex("data1")));
                                    phoneNumber.setContact(contact8);
                                    if (!p47.i(arrayList3, phoneNumber).booleanValue()) {
                                        arrayList3.add(phoneNumber);
                                    }
                                    if (next.isFavorites()) {
                                        arrayList4.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                    }
                                } catch (Exception e2) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String str2 = e;
                                    local2.e(str2, "Error Inside " + e + ".saveContactToFSL - ex=" + e2);
                                } catch (Throwable th) {
                                    query.close();
                                    throw th;
                                }
                            }
                            query.close();
                        }
                    }
                    it2 = it;
                } else {
                    pq7.i();
                    throw null;
                }
            }
            this.d.saveContactGroupList(arrayList2);
            this.d.saveListContact(arrayList);
            this.d.saveListPhoneNumber(arrayList3);
            j(arrayList4);
        }
    }

    @DexIgnore
    public final void j(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(phoneFavoritesContact);
        }
    }
}
