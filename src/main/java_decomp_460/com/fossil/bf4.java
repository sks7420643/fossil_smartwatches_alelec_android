package com.fossil;

import android.content.BroadcastReceiver;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bf4 implements ht3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f423a;
    @DexIgnore
    public /* final */ BroadcastReceiver.PendingResult b;

    @DexIgnore
    public bf4(boolean z, BroadcastReceiver.PendingResult pendingResult) {
        this.f423a = z;
        this.b = pendingResult;
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3 nt3) {
        FirebaseInstanceIdReceiver.d(this.f423a, this.b, nt3);
    }
}
