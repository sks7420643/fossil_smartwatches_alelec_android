package com.fossil;

import android.graphics.Rect;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n67 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ q67 f2474a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public n67(float f, float f2, float f3) {
        this.b = f3;
        this.f2474a = new q67(f, f2);
    }

    @DexIgnore
    public final boolean a(Rect rect) {
        pq7.c(rect, "rect");
        return !(b(new q67((float) rect.left, (float) rect.top)) || b(new q67((float) rect.right, (float) rect.top)) || b(new q67((float) rect.left, (float) rect.bottom)) || b(new q67((float) rect.right, (float) rect.bottom)));
    }

    @DexIgnore
    public final boolean b(q67 q67) {
        pq7.c(q67, "point");
        return this.f2474a.a(q67) <= this.b;
    }

    @DexIgnore
    public String toString() {
        return "Circle(center=" + this.f2474a + ", radius=" + this.b + ')';
    }
}
