package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aw extends tv {
    @DexIgnore
    public n6 L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ aw(long j, long j2, long j3, short s, k5 k5Var, int i, int i2) {
        super(sv.LEGACY_PUT_FILE, s, hs.O, k5Var, (i2 & 32) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.L = n6.UNKNOWN;
        this.M = true;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.G0, this.L.b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        n6 n6Var = n6.FTD;
        this.L = n6Var;
        return g80.k(jSONObject, jd0.G0, n6Var.b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        pq7.b(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public boolean O() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.tv, com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(g80.k(super.z(), jd0.c1, Long.valueOf(this.N)), jd0.d1, Long.valueOf(this.O)), jd0.e1, Long.valueOf(this.P));
    }
}
