package com.fossil;

import com.fossil.xb1;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dc1 implements xb1<InputStream> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qg1 f765a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements xb1.a<InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ od1 f766a;

        @DexIgnore
        public a(od1 od1) {
            this.f766a = od1;
        }

        @DexIgnore
        /* renamed from: b */
        public xb1<InputStream> a(InputStream inputStream) {
            return new dc1(inputStream, this.f766a);
        }

        @DexIgnore
        @Override // com.fossil.xb1.a
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore
    public dc1(InputStream inputStream, od1 od1) {
        qg1 qg1 = new qg1(inputStream, od1);
        this.f765a = qg1;
        qg1.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    }

    @DexIgnore
    @Override // com.fossil.xb1
    public void a() {
        this.f765a.c();
    }

    @DexIgnore
    public void c() {
        this.f765a.b();
    }

    @DexIgnore
    /* renamed from: d */
    public InputStream b() throws IOException {
        this.f765a.reset();
        return this.f765a;
    }
}
