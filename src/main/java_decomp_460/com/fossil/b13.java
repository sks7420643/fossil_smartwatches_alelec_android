package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b13 implements n23 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ b13 f379a; // = new b13();

    @DexIgnore
    public static b13 a() {
        return f379a;
    }

    @DexIgnore
    @Override // com.fossil.n23
    public final boolean zza(Class<?> cls) {
        return e13.class.isAssignableFrom(cls);
    }

    @DexIgnore
    @Override // com.fossil.n23
    public final k23 zzb(Class<?> cls) {
        if (!e13.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (k23) e13.o(cls.asSubclass(e13.class)).r(e13.f.c, null, null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
