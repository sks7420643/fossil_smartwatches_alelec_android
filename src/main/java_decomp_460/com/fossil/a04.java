package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a04 extends zz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float f175a; // = -1.0f;

    @DexIgnore
    @Override // com.fossil.zz3
    public void a(i04 i04, float f, float f2, float f3) {
        i04.n(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3 * f2, 180.0f, 180.0f - f);
        double d = (double) f3;
        double d2 = (double) f2;
        i04.l((float) (Math.sin(Math.toRadians((double) f)) * d * d2), (float) (d * Math.sin(Math.toRadians((double) (90.0f - f))) * d2));
    }
}
