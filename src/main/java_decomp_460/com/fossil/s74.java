package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s74 extends x64 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<Class<?>> f3213a;
    @DexIgnore
    public /* final */ Set<Class<?>> b;
    @DexIgnore
    public /* final */ Set<Class<?>> c;
    @DexIgnore
    public /* final */ Set<Class<?>> d;
    @DexIgnore
    public /* final */ Set<Class<?>> e;
    @DexIgnore
    public /* final */ b74 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements fe4 {
        @DexIgnore
        public a(Set<Class<?>> set, fe4 fe4) {
        }
    }

    @DexIgnore
    public s74(a74<?> a74, b74 b74) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (k74 k74 : a74.c()) {
            if (k74.b()) {
                if (k74.d()) {
                    hashSet3.add(k74.a());
                } else {
                    hashSet.add(k74.a());
                }
            } else if (k74.d()) {
                hashSet4.add(k74.a());
            } else {
                hashSet2.add(k74.a());
            }
        }
        if (!a74.f().isEmpty()) {
            hashSet.add(fe4.class);
        }
        this.f3213a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = Collections.unmodifiableSet(hashSet3);
        this.d = Collections.unmodifiableSet(hashSet4);
        this.e = a74.f();
        this.f = b74;
    }

    @DexIgnore
    @Override // com.fossil.b74
    public <T> mg4<T> a(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.f.a(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.b74
    public <T> mg4<Set<T>> b(Class<T> cls) {
        if (this.d.contains(cls)) {
            return this.f.b(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.b74, com.fossil.x64
    public <T> Set<T> c(Class<T> cls) {
        if (this.c.contains(cls)) {
            return this.f.c(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.b74, com.fossil.x64
    public <T> T get(Class<T> cls) {
        if (this.f3213a.contains(cls)) {
            T t = (T) this.f.get(cls);
            return !cls.equals(fe4.class) ? t : (T) new a(this.e, t);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }
}
