package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xv7<T> extends tz7<T> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater f; // = AtomicIntegerFieldUpdater.newUpdater(xv7.class, "_decision");
    @DexIgnore
    public volatile int _decision; // = 0;

    @DexIgnore
    public xv7(tn7 tn7, qn7<? super T> qn7) {
        super(tn7, qn7);
    }

    @DexIgnore
    public final Object A0() {
        if (C0()) {
            return yn7.d();
        }
        Object h = gx7.h(Q());
        if (!(h instanceof vu7)) {
            return h;
        }
        throw ((vu7) h).f3837a;
    }

    @DexIgnore
    public final boolean B0() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!f.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean C0() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!f.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tz7, com.fossil.fx7
    public void p(Object obj) {
        u0(obj);
    }

    @DexIgnore
    @Override // com.fossil.tz7, com.fossil.au7
    public void u0(Object obj) {
        if (!B0()) {
            wv7.b(xn7.c(this.e), wu7.a(obj, this.e));
        }
    }
}
