package com.fossil;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b33 {
    @DexIgnore
    public static /* final */ b33 c; // = new b33();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ e33 f390a; // = new d23();
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, f33<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static b33 a() {
        return c;
    }

    @DexIgnore
    public final <T> f33<T> b(Class<T> cls) {
        h13.f(cls, "messageType");
        f33<T> f33 = (f33<T>) this.b.get(cls);
        if (f33 != null) {
            return f33;
        }
        f33<T> zza = this.f390a.zza(cls);
        h13.f(cls, "messageType");
        h13.f(zza, "schema");
        f33<T> f332 = (f33<T>) this.b.putIfAbsent(cls, zza);
        return f332 != null ? f332 : zza;
    }

    @DexIgnore
    public final <T> f33<T> c(T t) {
        return b(t.getClass());
    }
}
