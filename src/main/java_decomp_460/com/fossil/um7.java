package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class um7 implements Iterator<Integer>, jr7 {
    @DexIgnore
    /* renamed from: a */
    public final Integer next() {
        return Integer.valueOf(b());
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
