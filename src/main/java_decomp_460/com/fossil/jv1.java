package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv1 extends ox1 implements Parcelable, Serializable, nx1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jv1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jv1 createFromParcel(Parcel parcel) {
            return new jv1(parcel.readFloat(), parcel.readFloat());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jv1[] newArray(int i) {
            return new jv1[i];
        }
    }

    @DexIgnore
    public jv1(float f, float f2) {
        this.b = f;
        this.c = f2;
    }

    @DexIgnore
    public final void a(float f) {
        this.b = f;
    }

    @DexIgnore
    public final void b(float f) {
        this.c = f;
    }

    @DexIgnore
    @Override // java.lang.Object
    public jv1 clone() {
        return new jv1(this.b, this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof jv1) {
                jv1 jv1 = (jv1) obj;
                if (!(Float.compare(this.b, jv1.b) == 0 && Float.compare(this.c, jv1.c) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getScaledX() {
        return this.b;
    }

    @DexIgnore
    public final float getScaledY() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (Float.floatToIntBits(this.b) * 31) + Float.floatToIntBits(this.c);
    }

    @DexIgnore
    public final iv1 toActualPosition(int i, int i2) {
        return new iv1(lr7.b(this.b * ((float) i)), lr7.b(this.c * ((float) i2)));
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return gy1.d(gy1.d(new JSONObject(), jd0.X5, Float.valueOf(this.b)), jd0.Y5, Float.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder e = e.e("ScaledPosition(scaledX=");
        e.append(this.b);
        e.append(", scaledY=");
        e.append(this.c);
        e.append(")");
        return e.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
    }
}
