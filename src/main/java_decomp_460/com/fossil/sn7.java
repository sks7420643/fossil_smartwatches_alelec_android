package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn7 {
    @DexIgnore
    public static final <T> void a(rp7<? super qn7<? super T>, ? extends Object> rp7, qn7<? super T> qn7) {
        pq7.c(rp7, "$this$startCoroutine");
        pq7.c(qn7, "completion");
        qn7 c = xn7.c(xn7.a(rp7, qn7));
        tl7 tl7 = tl7.f3441a;
        dl7.a aVar = dl7.Companion;
        c.resumeWith(dl7.m1constructorimpl(tl7));
    }

    @DexIgnore
    public static final <R, T> void b(vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7, R r, qn7<? super T> qn7) {
        pq7.c(vp7, "$this$startCoroutine");
        pq7.c(qn7, "completion");
        qn7 c = xn7.c(xn7.b(vp7, r, qn7));
        tl7 tl7 = tl7.f3441a;
        dl7.a aVar = dl7.Companion;
        c.resumeWith(dl7.m1constructorimpl(tl7));
    }
}
