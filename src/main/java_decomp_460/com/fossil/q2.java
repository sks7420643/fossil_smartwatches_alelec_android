package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q2 extends c2 {
    @DexIgnore
    public static /* final */ p2 CREATOR; // = new p2(null);
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ JSONObject f;

    @DexIgnore
    public q2(byte b, int i, JSONObject jSONObject) {
        super(lt.JSON_FILE_EVENT, b, false, 4);
        this.e = i;
        this.f = jSONObject;
    }

    @DexIgnore
    public /* synthetic */ q2(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.e = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            this.f = new JSONObject(readString);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(q2.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            q2 q2Var = (q2) obj;
            if (this.b != q2Var.b) {
                return false;
            }
            if (this.c != q2Var.c) {
                return false;
            }
            return this.e == q2Var.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.JSONRequestEvent");
    }

    @DexIgnore
    public int hashCode() {
        return (((this.b.hashCode() * 31) + this.c) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.q, Integer.valueOf(this.e)), jd0.d3, this.f);
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f.toString());
        }
    }
}
