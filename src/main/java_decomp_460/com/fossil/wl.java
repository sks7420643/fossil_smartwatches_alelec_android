package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl extends qq7 implements vp7<fs, Float, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ ro b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wl(ro roVar, long j) {
        super(2);
        this.b = roVar;
        this.c = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public tl7 invoke(fs fsVar, Float f) {
        ro roVar = this.b;
        long floatValue = ((long) (f.floatValue() * ((float) this.c))) + roVar.I;
        roVar.F = floatValue;
        float N = (((float) floatValue) * 1.0f) / ((float) roVar.N());
        if (Math.abs(N - this.b.G) > this.b.R || N == 1.0f) {
            ro roVar2 = this.b;
            roVar2.G = N;
            roVar2.d(N);
        }
        return tl7.f3441a;
    }
}
