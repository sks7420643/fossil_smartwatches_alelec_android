package com.fossil;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds0 {
    @DexIgnore
    public static final yr0 a(LifecycleOwner lifecycleOwner) {
        pq7.c(lifecycleOwner, "$this$lifecycleScope");
        Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        pq7.b(lifecycle, "lifecycle");
        return bs0.a(lifecycle);
    }
}
