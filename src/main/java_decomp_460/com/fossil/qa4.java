package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa4 extends ta4.d.AbstractC0224d.AbstractC0235d {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2949a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.AbstractC0235d.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2950a;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.AbstractC0235d.a
        public ta4.d.AbstractC0224d.AbstractC0235d a() {
            String str = "";
            if (this.f2950a == null) {
                str = " content";
            }
            if (str.isEmpty()) {
                return new qa4(this.f2950a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.AbstractC0235d.a
        public ta4.d.AbstractC0224d.AbstractC0235d.a b(String str) {
            if (str != null) {
                this.f2950a = str;
                return this;
            }
            throw new NullPointerException("Null content");
        }
    }

    @DexIgnore
    public qa4(String str) {
        this.f2949a = str;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.AbstractC0235d
    public String b() {
        return this.f2949a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ta4.d.AbstractC0224d.AbstractC0235d) {
            return this.f2949a.equals(((ta4.d.AbstractC0224d.AbstractC0235d) obj).b());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f2949a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Log{content=" + this.f2949a + "}";
    }
}
