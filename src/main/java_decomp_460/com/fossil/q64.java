package com.fossil;

import android.os.Bundle;
import com.fossil.fg3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q64 implements fg3.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ r64 f2928a;

    @DexIgnore
    public q64(r64 r64) {
        this.f2928a = r64;
    }

    @DexIgnore
    @Override // com.fossil.sn3
    public final void a(String str, String str2, Bundle bundle, long j) {
        if (this.f2928a.f3085a.contains(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("events", o64.g(str2));
            this.f2928a.b.a(2, bundle2);
        }
    }
}
