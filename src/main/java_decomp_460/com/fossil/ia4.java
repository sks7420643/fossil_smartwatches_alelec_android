package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia4 extends ta4.d.AbstractC0224d.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ta4.d.AbstractC0224d.a.b f1598a;
    @DexIgnore
    public /* final */ ua4<ta4.b> b;
    @DexIgnore
    public /* final */ Boolean c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.a.AbstractC0225a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ta4.d.AbstractC0224d.a.b f1599a;
        @DexIgnore
        public ua4<ta4.b> b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Integer d;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(ta4.d.AbstractC0224d.a aVar) {
            this.f1599a = aVar.d();
            this.b = aVar.c();
            this.c = aVar.b();
            this.d = Integer.valueOf(aVar.e());
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.AbstractC0225a
        public ta4.d.AbstractC0224d.a a() {
            String str = "";
            if (this.f1599a == null) {
                str = " execution";
            }
            if (this.d == null) {
                str = str + " uiOrientation";
            }
            if (str.isEmpty()) {
                return new ia4(this.f1599a, this.b, this.c, this.d.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.AbstractC0225a
        public ta4.d.AbstractC0224d.a.AbstractC0225a b(Boolean bool) {
            this.c = bool;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.AbstractC0225a
        public ta4.d.AbstractC0224d.a.AbstractC0225a c(ua4<ta4.b> ua4) {
            this.b = ua4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.AbstractC0225a
        public ta4.d.AbstractC0224d.a.AbstractC0225a d(ta4.d.AbstractC0224d.a.b bVar) {
            if (bVar != null) {
                this.f1599a = bVar;
                return this;
            }
            throw new NullPointerException("Null execution");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.AbstractC0225a
        public ta4.d.AbstractC0224d.a.AbstractC0225a e(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }
    }

    @DexIgnore
    public ia4(ta4.d.AbstractC0224d.a.b bVar, ua4<ta4.b> ua4, Boolean bool, int i) {
        this.f1598a = bVar;
        this.b = ua4;
        this.c = bool;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a
    public Boolean b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a
    public ua4<ta4.b> c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a
    public ta4.d.AbstractC0224d.a.b d() {
        return this.f1598a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a
    public int e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        ua4<ta4.b> ua4;
        Boolean bool;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.a)) {
            return false;
        }
        ta4.d.AbstractC0224d.a aVar = (ta4.d.AbstractC0224d.a) obj;
        return this.f1598a.equals(aVar.d()) && ((ua4 = this.b) != null ? ua4.equals(aVar.c()) : aVar.c() == null) && ((bool = this.c) != null ? bool.equals(aVar.b()) : aVar.b() == null) && this.d == aVar.e();
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a
    public ta4.d.AbstractC0224d.a.AbstractC0225a f() {
        return new b(this);
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int hashCode = this.f1598a.hashCode();
        ua4<ta4.b> ua4 = this.b;
        int hashCode2 = ua4 == null ? 0 : ua4.hashCode();
        Boolean bool = this.c;
        if (bool != null) {
            i = bool.hashCode();
        }
        return ((((hashCode2 ^ ((hashCode ^ 1000003) * 1000003)) * 1000003) ^ i) * 1000003) ^ this.d;
    }

    @DexIgnore
    public String toString() {
        return "Application{execution=" + this.f1598a + ", customAttributes=" + this.b + ", background=" + this.c + ", uiOrientation=" + this.d + "}";
    }
}
