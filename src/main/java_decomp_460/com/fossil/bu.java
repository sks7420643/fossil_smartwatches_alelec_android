package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bu {
    REQUEST_HANDS(new byte[]{1}),
    RELEASE_HANDS(new byte[]{2}),
    MOVE_HANDS(new byte[]{3}),
    SET_CALIBRATION_POSITION(new byte[]{DateTimeFieldType.HOUR_OF_HALFDAY});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public bu(byte[] bArr) {
        this.b = bArr;
    }
}
