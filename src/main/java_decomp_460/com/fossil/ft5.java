package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ct5;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.EmptyFirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingAuthorizeResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.LabelRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft5 extends iq4<h, j, i> {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ c t; // = new c(null);
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public Device f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public boolean h;
    @DexIgnore
    public k i;
    @DexIgnore
    public /* final */ g j; // = new g();
    @DexIgnore
    public /* final */ DeviceRepository k;
    @DexIgnore
    public /* final */ ct5 l;
    @DexIgnore
    public /* final */ on5 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ q27 o;
    @DexIgnore
    public /* final */ mj5 p;
    @DexIgnore
    public /* final */ LabelRepository q;
    @DexIgnore
    public /* final */ FileRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1191a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, "serial");
            this.f1191a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f1191a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f1192a;

        @DexIgnore
        public b(String str, boolean z) {
            pq7.c(str, "serial");
            this.f1192a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.f1192a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ft5.s;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(int i, String str, String str2) {
            super(i, str, str2);
            pq7.c(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, String str2) {
            super(-1, str, str2);
            pq7.c(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1193a;

        @DexIgnore
        public f(String str) {
            pq7.c(str, "serial");
            this.f1193a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f1193a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ft5.t.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + ft5.this.B() + ", isSuccess=" + intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1));
            if (!TextUtils.isEmpty(stringExtra) && vt7.j(stringExtra, ft5.this.z(), true) && communicateMode == CommunicateMode.LINK && ft5.this.B()) {
                switch (gt5.f1358a[serviceActionResult.ordinal()]) {
                    case 1:
                        ft5 ft5 = ft5.this;
                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            String string = extras.getString("device_model", "");
                            pq7.b(string, "intent.extras!!.getStrin\u2026nstants.DEVICE_MODEL, \"\")");
                            ft5.K(string);
                            ft5 ft52 = ft5.this;
                            if (stringExtra != null) {
                                ft52.j(new a(stringExtra));
                                ft5.this.H();
                                return;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    case 2:
                        ft5 ft53 = ft5.this;
                        if (stringExtra != null) {
                            ft53.j(new f(stringExtra));
                            return;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 3:
                        ft5 ft54 = ft5.this;
                        if (stringExtra != null) {
                            ft54.j(new m(stringExtra, true));
                            return;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 4:
                        ft5 ft55 = ft5.this;
                        if (stringExtra != null) {
                            ft55.j(new m(stringExtra, false));
                            return;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 5:
                        ft5 ft56 = ft5.this;
                        if (stringExtra != null) {
                            ft56.j(new b(stringExtra, true));
                            return;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 6:
                        ft5 ft57 = ft5.this;
                        if (stringExtra != null) {
                            ft57.j(new b(stringExtra, false));
                            return;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 7:
                        FLogger.INSTANCE.getLocal().d(ft5.t.a(), "onReceive(), ASK_FOR_LINK_SERVER");
                        Bundle extras2 = intent.getExtras();
                        if (extras2 != null) {
                            MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                            if (misfitDeviceProfile != null) {
                                ft5.this.M(misfitDeviceProfile.getDeviceSerial());
                                ft5.this.L(misfitDeviceProfile.getAddress());
                                ft5.this.J(new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), misfitDeviceProfile.getBatteryLevel(), 50, false, 64, null));
                                Device w = ft5.this.w();
                                if (w != null) {
                                    w.appendAdditionalInfo(misfitDeviceProfile);
                                }
                                ft5.this.u();
                                return;
                            }
                            ft5 ft58 = ft5.this;
                            String z = ft58.z();
                            if (z != null) {
                                ft58.i(new d(FailureCode.UNKNOWN_ERROR, z, "No device profile"));
                                PortfolioApp c = PortfolioApp.h0.c();
                                String z2 = ft5.this.z();
                                if (z2 != null) {
                                    c.s(z2);
                                    return;
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 8:
                        FLogger.INSTANCE.getLocal().d(ft5.t.a(), "onReceive(), ASK_FOR_LABEL_FILE");
                        ft5 ft59 = ft5.this;
                        pq7.b(stringExtra, "serial");
                        ft59.v(stringExtra);
                        return;
                    case 9:
                        ft5.this.I(false);
                        PortfolioApp c2 = PortfolioApp.h0.c();
                        if (stringExtra != null) {
                            c2.n1(stringExtra, ft5.this.y());
                            ft5 ft510 = ft5.this;
                            Device w2 = ft510.w();
                            if (w2 != null) {
                                ft510.j(new l(w2));
                                return;
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    case 10:
                        ft5.this.I(false);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = ft5.t.a();
                        local2.d(a3, "Pair device failed due to " + intExtra + ", remove this device on button service");
                        try {
                            PortfolioApp c3 = PortfolioApp.h0.c();
                            String z3 = ft5.this.z();
                            if (z3 != null) {
                                c3.a1(z3);
                                PortfolioApp c4 = PortfolioApp.h0.c();
                                String z4 = ft5.this.z();
                                if (z4 != null) {
                                    c4.Z0(z4);
                                    if (intExtra > 1000) {
                                        ft5 ft511 = ft5.this;
                                        String z5 = ft511.z();
                                        if (z5 != null) {
                                            ft511.i(new d(intExtra, z5, ""));
                                            return;
                                        } else {
                                            pq7.i();
                                            throw null;
                                        }
                                    } else {
                                        ft5 ft512 = ft5.this;
                                        String z6 = ft512.z();
                                        if (z6 != null) {
                                            ft512.N(new k(intExtra, z6, ""));
                                            ft5 ft513 = ft5.this;
                                            k A = ft513.A();
                                            if (A != null) {
                                                ft513.i(A);
                                                ft5.this.N(null);
                                                return;
                                            }
                                            pq7.i();
                                            throw null;
                                        }
                                        pq7.i();
                                        throw null;
                                    }
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } catch (Exception e) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = ft5.t.a();
                            local3.d(a4, "Pair device failed, remove this device on button service exception=" + e.getMessage());
                        }
                    default:
                        return;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1195a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public h(String str, String str2) {
            pq7.c(str, "device");
            pq7.c(str2, "macAddress");
            this.f1195a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.f1195a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1196a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public i(int i, String str, String str2) {
            pq7.c(str, "deviceId");
            this.f1196a = i;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f1196a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class j implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(int i, String str, String str2) {
            super(i, str, str2);
            pq7.c(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Device f1197a;

        @DexIgnore
        public l(Device device) {
            pq7.c(device, "device");
            this.f1197a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.f1197a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1198a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public m(String str, boolean z) {
            pq7.c(str, "serial");
            this.f1198a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.f1198a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements iq4.e<uy6, sy6> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ft5 f1199a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public n(ft5 ft5) {
            this.f1199a = ft5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(sy6 sy6) {
            pq7.c(sy6, "errorValue");
            FLogger.INSTANCE.getLocal().d(ft5.t.a(), " get device setting fail!!");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String z = this.f1199a.z();
            if (z != null) {
                String a2 = ft5.t.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Get device setting of ");
                String z2 = this.f1199a.z();
                if (z2 != null) {
                    sb.append(z2);
                    sb.append(", server error=");
                    sb.append(sy6.a());
                    sb.append(", error = ");
                    sb.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
                    remote.e(component, session, z, a2, sb.toString());
                    ft5 ft5 = this.f1199a;
                    int a3 = sy6.a();
                    String z3 = this.f1199a.z();
                    if (z3 != null) {
                        ft5.N(new k(a3, z3, ""));
                        PortfolioApp c = PortfolioApp.h0.c();
                        String z4 = this.f1199a.z();
                        if (z4 != null) {
                            c.M0(z4, PairingResponse.CREATOR.buildPairingLinkServerResponse(false, sy6.a()));
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(uy6 uy6) {
            pq7.c(uy6, "responseValue");
            FLogger.INSTANCE.getLocal().d(ft5.t.a(), " get device setting success");
            this.f1199a.F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$downloadLabelFile$1", f = "LinkDeviceUseCase.kt", l = {285, 294}, m = "invokeSuspend")
    public static final class o extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ft5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(ft5 ft5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ft5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            o oVar = new o(this.this$0, this.$serial, qn7);
            oVar.p$ = (iv7) obj;
            return oVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((o) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0129  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 372
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ft5.o.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $deviceModel;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ft5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(Device device, qn7 qn7, ft5 ft5) {
            super(2, qn7);
            this.$deviceModel = device;
            this.this$0 = ft5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            p pVar = new p(this.$deviceModel, qn7, this.this$0);
            pVar.p$ = (iv7) obj;
            return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((p) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00c4  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0118  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x021d  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0249  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 760
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ft5.p.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase", f = "LinkDeviceUseCase.kt", l = {174}, m = "run")
    public static final class q extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ft5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(ft5 ft5, qn7 qn7) {
            super(qn7);
            this.this$0 = ft5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements iq4.e<ct5.c, ct5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ft5 f1200a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ct5.c $responseValue;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ r this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(r rVar, ct5.c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = rVar;
                this.$responseValue = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$responseValue, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    String a2 = this.$responseValue.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = ft5.t.a();
                    local.d(a3, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a2);
                    FirmwareData a4 = yt5.g.a(this.this$0.f1200a.m, this.this$0.f1200a.x());
                    if (a4 == null) {
                        a4 = new EmptyFirmwareData();
                    }
                    PortfolioApp c = PortfolioApp.h0.c();
                    String z = this.this$0.f1200a.z();
                    if (z != null) {
                        c.M0(z, PairingResponse.CREATOR.buildPairingUpdateFWResponse(a4));
                        return tl7.f3441a;
                    }
                    pq7.i();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public r(ft5 ft5) {
            this.f1200a = ft5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ct5.b bVar) {
            pq7.c(bVar, "errorValue");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String z = this.f1200a.z();
            if (z != null) {
                String a2 = ft5.t.a();
                StringBuilder sb = new StringBuilder();
                sb.append(" downloadFw FAILED!!!, latestFwVersion=");
                String z2 = this.f1200a.z();
                if (z2 != null) {
                    sb.append(z2);
                    sb.append(" but device is DianaEV1!!!");
                    remote.e(component, session, z, a2, sb.toString());
                    FLogger.INSTANCE.getLocal().e(ft5.t.a(), "checkFirmware - downloadFw FAILED!!!");
                    ft5 ft5 = this.f1200a;
                    String z3 = ft5.z();
                    if (z3 != null) {
                        ft5.i(new e(z3, ""));
                        PortfolioApp c = PortfolioApp.h0.c();
                        String z4 = this.f1200a.z();
                        if (z4 != null) {
                            c.s(z4);
                            this.f1200a.I(false);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ct5.c cVar) {
            pq7.c(cVar, "responseValue");
            xw7 unused = gu7.d(this.f1200a.g(), null, null, new a(this, cVar, null), 3, null);
        }
    }

    /*
    static {
        String simpleName = ft5.class.getSimpleName();
        pq7.b(simpleName, "LinkDeviceUseCase::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public ft5(DeviceRepository deviceRepository, ct5 ct5, on5 on5, UserRepository userRepository, q27 q27, mj5 mj5, LabelRepository labelRepository, FileRepository fileRepository) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(ct5, "mDownloadFwByDeviceModel");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(q27, "mDecryptValueKeyStoreUseCase");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(labelRepository, "mLabelRepository");
        pq7.c(fileRepository, "mFileRepository");
        this.k = deviceRepository;
        this.l = ct5;
        this.m = on5;
        this.n = userRepository;
        this.o = q27;
        this.p = mj5;
        this.q = labelRepository;
        this.r = fileRepository;
    }

    @DexIgnore
    public final k A() {
        return this.i;
    }

    @DexIgnore
    public final boolean B() {
        return this.h;
    }

    @DexIgnore
    public final void C(ShineDevice shineDevice, iq4.e<? super j, ? super i> eVar) {
        pq7.c(shineDevice, "closestDevice");
        pq7.c(eVar, "caseCallback");
        FLogger.INSTANCE.getLocal().d(s, "onRetrieveLinkAction");
        this.h = true;
        this.d = shineDevice.getSerial();
        this.e = shineDevice.getMacAddress();
        l(eVar);
    }

    @DexIgnore
    public final void D() {
        wq5.d.e(this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    public final void E(long j2) {
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(s, "requestAuthorizeDevice()");
            PortfolioApp c2 = PortfolioApp.h0.c();
            String str = this.d;
            if (str != null) {
                c2.M0(str, new PairingAuthorizeResponse(j2));
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void F() {
        synchronized (this) {
            Device device = this.f;
            if (device != null) {
                xw7 unused = gu7.d(g(), null, null, new p(device, null, this), 3, null);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: G */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ft5.h r8, com.fossil.qn7<java.lang.Object> r9) {
        /*
            r7 = this;
            r6 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r9 instanceof com.fossil.ft5.q
            if (r0 == 0) goto L_0x0032
            r0 = r9
            com.fossil.ft5$q r0 = (com.fossil.ft5.q) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r5) goto L_0x0039
            java.lang.Object r0 = r1.L$1
            com.fossil.ft5$h r0 = (com.fossil.ft5.h) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.ft5 r0 = (com.fossil.ft5) r0
            com.fossil.el7.b(r2)     // Catch:{ Exception -> 0x0098 }
        L_0x002c:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
        L_0x0031:
            return r0
        L_0x0032:
            com.fossil.ft5$q r0 = new com.fossil.ft5$q
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.ft5.s
            java.lang.String r4 = "running UseCase"
            r2.d(r3, r4)
            if (r8 != 0) goto L_0x005f
            com.fossil.ft5$i r0 = new com.fossil.ft5$i
            r1 = 600(0x258, float:8.41E-43)
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            r0.<init>(r1, r2, r3)
            goto L_0x0031
        L_0x005f:
            r7.h = r5
            java.lang.String r2 = r8.a()
            r7.d = r2
            java.lang.String r2 = ""
            r7.e = r2
            r8.b()
            java.lang.String r2 = r8.b()
            r7.e = r2
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r3 = r7.d
            if (r3 == 0) goto L_0x0094
            java.lang.String r4 = r7.e
            if (r4 == 0) goto L_0x0090
            r1.L$0 = r7
            r1.L$1 = r8
            r5 = 1
            r1.label = r5
            java.lang.Object r1 = r2.L0(r3, r4, r1)
            if (r1 != r0) goto L_0x002c
            goto L_0x0031
        L_0x0090:
            com.fossil.pq7.i()
            throw r6
        L_0x0094:
            com.fossil.pq7.i()
            throw r6
        L_0x0098:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.ft5.s
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error inside "
            r3.append(r4)
            java.lang.String r4 = com.fossil.ft5.s
            r3.append(r4)
            java.lang.String r4 = ".connectDevice - e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ft5.k(com.fossil.ft5$h, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void H() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "sendLatestFWResponseToDevice(), deviceModel=" + this.g + ", isSkipOTA=" + this.m.y0());
        if (PortfolioApp.h0.c().z0() || !this.m.y0()) {
            this.l.e(new ct5.a(this.g), new r(this));
            return;
        }
        PortfolioApp c2 = PortfolioApp.h0.c();
        String str2 = this.d;
        if (str2 != null) {
            c2.M0(str2, PairingResponse.CREATOR.buildPairingUpdateFWResponse(new SkipFirmwareData()));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void I(boolean z) {
        this.h = z;
    }

    @DexIgnore
    public final void J(Device device) {
        this.f = device;
    }

    @DexIgnore
    public final void K(String str) {
        pq7.c(str, "<set-?>");
        this.g = str;
    }

    @DexIgnore
    public final void L(String str) {
        this.e = str;
    }

    @DexIgnore
    public final void M(String str) {
        this.d = str;
    }

    @DexIgnore
    public final void N(k kVar) {
        this.i = kVar;
    }

    @DexIgnore
    public final void O() {
        wq5.d.j(this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return s;
    }

    @DexIgnore
    public final void u() {
        synchronized (this) {
            mj5 mj5 = this.p;
            String str = this.d;
            if (str != null) {
                iq4<ty6, uy6, sy6> a2 = mj5.a(str);
                String str2 = this.d;
                if (str2 != null) {
                    a2.e(new ty6(str2, ry6.PAIR), new n(this));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void v(String str) {
        synchronized (this) {
            pq7.c(str, "serial");
            this.h = true;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local.d(str2, "downloadLabelFile, serial = " + str);
            xw7 unused = gu7.d(g(), null, null, new o(this, str, null), 3, null);
        }
    }

    @DexIgnore
    public final Device w() {
        return this.f;
    }

    @DexIgnore
    public final String x() {
        return this.g;
    }

    @DexIgnore
    public final String y() {
        return this.e;
    }

    @DexIgnore
    public final String z() {
        return this.d;
    }
}
