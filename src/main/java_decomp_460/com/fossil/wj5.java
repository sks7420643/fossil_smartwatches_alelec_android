package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wj5 extends wa1 {
    @DexIgnore
    public wj5(oa1 oa1, di1 di1, ii1 ii1, Context context) {
        super(oa1, di1, ii1, context);
    }

    @DexIgnore
    /* renamed from: C */
    public <ResourceType> vj5<ResourceType> c(Class<ResourceType> cls) {
        return new vj5<>(this.b, this, cls, this.c);
    }

    @DexIgnore
    /* renamed from: D */
    public vj5<Bitmap> e() {
        return (vj5) super.e();
    }

    @DexIgnore
    /* renamed from: E */
    public vj5<Drawable> g() {
        return (vj5) super.g();
    }

    @DexIgnore
    public vj5<Drawable> F(Bitmap bitmap) {
        return (vj5) super.p(bitmap);
    }

    @DexIgnore
    public vj5<Drawable> G(Uri uri) {
        return (vj5) super.q(uri);
    }

    @DexIgnore
    /* renamed from: H */
    public vj5<Drawable> r(Integer num) {
        return (vj5) super.r(num);
    }

    @DexIgnore
    public vj5<Drawable> I(Object obj) {
        return (vj5) super.s(obj);
    }

    @DexIgnore
    /* renamed from: J */
    public vj5<Drawable> t(String str) {
        return (vj5) super.t(str);
    }

    @DexIgnore
    @Override // com.fossil.wa1
    public void y(fj1 fj1) {
        if (fj1 instanceof uj5) {
            super.y(fj1);
        } else {
            super.y(new uj5().d(fj1));
        }
    }
}
