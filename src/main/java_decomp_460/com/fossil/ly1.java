package com.fossil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ly1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ CopyOnWriteArraySet<my1> f2278a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public ky1[] b; // = new ky1[0];

    @DexIgnore
    public final void a(String str, String str2, Object... objArr) {
        pq7.c(str, "tag");
        pq7.c(str2, "logContent");
        pq7.c(objArr, "logParams");
        c(3, str, str2, Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public final void b(String str, String str2, Object... objArr) {
        pq7.c(str, "tag");
        pq7.c(str2, "logContent");
        pq7.c(objArr, "logParams");
        c(6, str, str2, Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public final void c(int i, String str, String str2, Object... objArr) {
        pq7.c(str, "tag");
        pq7.c(str2, "logContent");
        pq7.c(objArr, "logParams");
        long currentTimeMillis = System.currentTimeMillis();
        ky1 a2 = ky1.Companion.a(i);
        if (a2 != null && em7.B(this.b, a2)) {
            Iterator<T> it = this.f2278a.iterator();
            while (it.hasNext()) {
                it.next().a(currentTimeMillis, a2, str, str2, Arrays.copyOf(objArr, objArr.length));
            }
        }
    }
}
