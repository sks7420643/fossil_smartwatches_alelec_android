package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dq7<T> implements Iterator<T>, jr7 {
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ T[] c;

    @DexIgnore
    public dq7(T[] tArr) {
        pq7.c(tArr, "array");
        this.c = tArr;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.b < this.c.length;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        try {
            T[] tArr = this.c;
            int i = this.b;
            this.b = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.b--;
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
