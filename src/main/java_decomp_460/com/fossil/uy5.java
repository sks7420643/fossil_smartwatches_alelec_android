package com.fossil;

import android.graphics.RectF;
import com.fossil.vy5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uy5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ RectF f3674a; // = new RectF();
    @DexIgnore
    public /* final */ RectF b; // = new RectF();
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k; // = 1.0f;
    @DexIgnore
    public float l; // = 1.0f;

    @DexIgnore
    public static boolean l(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f6 && f3 > f5 && f3 < f7;
    }

    @DexIgnore
    public static boolean m(float f2, float f3, float f4, float f5, float f6) {
        return Math.abs(f2 - f4) <= f6 && Math.abs(f3 - f5) <= f6;
    }

    @DexIgnore
    public static boolean n(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f5 && Math.abs(f3 - f6) <= f7;
    }

    @DexIgnore
    public static boolean o(float f2, float f3, float f4, float f5, float f6, float f7) {
        return Math.abs(f2 - f4) <= f7 && f3 > f5 && f3 < f6;
    }

    @DexIgnore
    public final boolean a() {
        return !u();
    }

    @DexIgnore
    public float b() {
        return Math.min(this.f, this.j / this.l);
    }

    @DexIgnore
    public float c() {
        return Math.min(this.e, this.i / this.k);
    }

    @DexIgnore
    public float d() {
        return Math.max(this.d, this.h / this.l);
    }

    @DexIgnore
    public float e() {
        return Math.max(this.c, this.g / this.k);
    }

    @DexIgnore
    public vy5 f(float f2, float f3, float f4, CropImageView.c cVar) {
        vy5.b g2 = cVar == CropImageView.c.OVAL ? g(f2, f3) : i(f2, f3, f4);
        if (g2 != null) {
            return new vy5(g2, this, f2, f3);
        }
        return null;
    }

    @DexIgnore
    public final vy5.b g(float f2, float f3) {
        float width = this.f3674a.width() / 6.0f;
        RectF rectF = this.f3674a;
        float f4 = rectF.left;
        float height = rectF.height() / 6.0f;
        float f5 = this.f3674a.top;
        float f6 = f5 + height;
        float f7 = (height * 5.0f) + f5;
        return f2 < f4 + width ? f3 < f6 ? vy5.b.TOP_LEFT : f3 < f7 ? vy5.b.LEFT : vy5.b.BOTTOM_LEFT : f2 < (width * 5.0f) + f4 ? f3 < f6 ? vy5.b.TOP : f3 < f7 ? vy5.b.CENTER : vy5.b.BOTTOM : f3 < f6 ? vy5.b.TOP_RIGHT : f3 < f7 ? vy5.b.RIGHT : vy5.b.BOTTOM_RIGHT;
    }

    @DexIgnore
    public RectF h() {
        this.b.set(this.f3674a);
        return this.b;
    }

    @DexIgnore
    public final vy5.b i(float f2, float f3, float f4) {
        RectF rectF = this.f3674a;
        if (m(f2, f3, rectF.left, rectF.top, f4)) {
            return vy5.b.TOP_LEFT;
        }
        RectF rectF2 = this.f3674a;
        if (m(f2, f3, rectF2.right, rectF2.top, f4)) {
            return vy5.b.TOP_RIGHT;
        }
        RectF rectF3 = this.f3674a;
        if (m(f2, f3, rectF3.left, rectF3.bottom, f4)) {
            return vy5.b.BOTTOM_LEFT;
        }
        RectF rectF4 = this.f3674a;
        if (m(f2, f3, rectF4.right, rectF4.bottom, f4)) {
            return vy5.b.BOTTOM_RIGHT;
        }
        RectF rectF5 = this.f3674a;
        if (l(f2, f3, rectF5.left, rectF5.top, rectF5.right, rectF5.bottom) && a()) {
            return vy5.b.CENTER;
        }
        RectF rectF6 = this.f3674a;
        if (n(f2, f3, rectF6.left, rectF6.right, rectF6.top, f4)) {
            return vy5.b.TOP;
        }
        RectF rectF7 = this.f3674a;
        if (n(f2, f3, rectF7.left, rectF7.right, rectF7.bottom, f4)) {
            return vy5.b.BOTTOM;
        }
        RectF rectF8 = this.f3674a;
        if (o(f2, f3, rectF8.left, rectF8.top, rectF8.bottom, f4)) {
            return vy5.b.LEFT;
        }
        RectF rectF9 = this.f3674a;
        if (o(f2, f3, rectF9.right, rectF9.top, rectF9.bottom, f4)) {
            return vy5.b.RIGHT;
        }
        RectF rectF10 = this.f3674a;
        if (!l(f2, f3, rectF10.left, rectF10.top, rectF10.right, rectF10.bottom) || a()) {
            return null;
        }
        return vy5.b.CENTER;
    }

    @DexIgnore
    public float j() {
        return this.l;
    }

    @DexIgnore
    public float k() {
        return this.k;
    }

    @DexIgnore
    public void p(float f2, float f3, float f4, float f5) {
        this.e = f2;
        this.f = f3;
        this.k = f4;
        this.l = f5;
    }

    @DexIgnore
    public void q(ty5 ty5) {
        this.c = (float) ty5.D;
        this.d = (float) ty5.E;
        this.g = (float) ty5.F;
        this.h = (float) ty5.G;
        this.i = (float) ty5.H;
        this.j = (float) ty5.I;
    }

    @DexIgnore
    public void r(int i2, int i3) {
        this.i = (float) i2;
        this.j = (float) i3;
    }

    @DexIgnore
    public void s(int i2, int i3) {
        this.g = (float) i2;
        this.h = (float) i3;
    }

    @DexIgnore
    public void t(RectF rectF) {
        this.f3674a.set(rectF);
    }

    @DexIgnore
    public boolean u() {
        return this.f3674a.width() >= 100.0f && this.f3674a.height() >= 100.0f;
    }
}
