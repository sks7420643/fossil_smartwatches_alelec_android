package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl4 extends hl4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public bl4[] f627a;

    @DexIgnore
    public cl4() {
        c();
    }

    @DexIgnore
    public cl4 c() {
        this.f627a = bl4.d();
        return this;
    }

    @DexIgnore
    /* renamed from: d */
    public cl4 b(el4 el4) throws IOException {
        while (true) {
            int q = el4.q();
            if (q == 0) {
                break;
            } else if (q == 10) {
                int a2 = jl4.a(el4, 10);
                bl4[] bl4Arr = this.f627a;
                int length = bl4Arr == null ? 0 : bl4Arr.length;
                int i = a2 + length;
                bl4[] bl4Arr2 = new bl4[i];
                if (length != 0) {
                    System.arraycopy(this.f627a, 0, bl4Arr2, 0, length);
                }
                while (length < i - 1) {
                    bl4Arr2[length] = new bl4();
                    el4.j(bl4Arr2[length]);
                    el4.q();
                    length++;
                }
                bl4Arr2[length] = new bl4();
                el4.j(bl4Arr2[length]);
                this.f627a = bl4Arr2;
            } else if (!jl4.e(el4, q)) {
                break;
            }
        }
        return this;
    }
}
