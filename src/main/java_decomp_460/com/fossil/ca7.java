package com.fossil;

import com.portfolio.platform.data.source.FileRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca7 implements Factory<ba7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<s77> f590a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;

    @DexIgnore
    public ca7(Provider<s77> provider, Provider<FileRepository> provider2) {
        this.f590a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ca7 a(Provider<s77> provider, Provider<FileRepository> provider2) {
        return new ca7(provider, provider2);
    }

    @DexIgnore
    public static ba7 c(s77 s77, FileRepository fileRepository) {
        return new ba7(s77, fileRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public ba7 get() {
        return c(this.f590a.get(), this.b.get());
    }
}
