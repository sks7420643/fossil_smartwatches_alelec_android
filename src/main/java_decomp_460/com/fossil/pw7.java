package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw7 {
    @DexIgnore
    public static final dv7 a(Executor executor) {
        dv7 dv7;
        aw7 aw7 = (aw7) (!(executor instanceof aw7) ? null : executor);
        return (aw7 == null || (dv7 = aw7.b) == null) ? new ow7(executor) : dv7;
    }

    @DexIgnore
    public static final mw7 b(ExecutorService executorService) {
        return new ow7(executorService);
    }
}
