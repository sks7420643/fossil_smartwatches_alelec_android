package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tq7 extends sq7 {
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ fs7 owner;
    @DexIgnore
    public /* final */ String signature;

    @DexIgnore
    public tq7(fs7 fs7, String str, String str2) {
        this.owner = fs7;
        this.name = str;
        this.signature = str2;
    }

    @DexIgnore
    @Override // com.fossil.sq7
    public Object get(Object obj) {
        return getGetter().call(obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7, com.fossil.ds7
    public String getName() {
        return this.name;
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public fs7 getOwner() {
        return this.owner;
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public String getSignature() {
        return this.signature;
    }

    @DexIgnore
    @Override // com.fossil.sq7
    public void set(Object obj, Object obj2) {
        getSetter().call(obj, obj2);
    }
}
