package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class i32 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j32 f1575a;
    @DexIgnore
    public /* final */ h02 b;

    @DexIgnore
    public i32(j32 j32, h02 h02) {
        this.f1575a = j32;
        this.b = h02;
    }

    @DexIgnore
    public static j32.b a(j32 j32, h02 h02) {
        return new i32(j32, h02);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.L(this.f1575a, this.b, (SQLiteDatabase) obj);
    }
}
