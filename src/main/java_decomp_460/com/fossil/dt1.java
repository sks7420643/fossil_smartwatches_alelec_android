package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<dt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public dt1 createFromParcel(Parcel parcel) {
            return new dt1(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public dt1[] newArray(int i) {
            return new dt1[i];
        }
    }

    @DexIgnore
    public dt1(int i, int i2) throws IllegalArgumentException {
        boolean z = true;
        this.b = i;
        this.c = i2;
        if (i >= 0 && 359 >= i) {
            int i3 = this.c;
            if (!((i3 < 0 || 120 < i3) ? false : z)) {
                throw new IllegalArgumentException(e.c(e.e("distanceFromCenter("), this.c, ") is out of ", "range [0, 120]."));
            }
            return;
        }
        throw new IllegalArgumentException(e.c(e.e("angle("), this.b, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(dt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            dt1 dt1 = (dt1) obj;
            if (this.b != dt1.b) {
                return false;
            }
            return this.c == dt1.c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.b;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("angle", this.b).put("distance", this.c);
        pq7.b(put, "JSONObject()\n           \u2026ANCE, distanceFromCenter)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
