package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn5 implements MembersInjector<sn5> {
    @DexIgnore
    public static void a(sn5 sn5, ApiServiceV2 apiServiceV2) {
        sn5.b = apiServiceV2;
    }

    @DexIgnore
    public static void b(sn5 sn5, CustomizeRealDataRepository customizeRealDataRepository) {
        sn5.e = customizeRealDataRepository;
    }

    @DexIgnore
    public static void c(sn5 sn5, DianaAppSettingRepository dianaAppSettingRepository) {
        sn5.f = dianaAppSettingRepository;
    }

    @DexIgnore
    public static void d(sn5 sn5, GoogleApiService googleApiService) {
        sn5.g = googleApiService;
    }

    @DexIgnore
    public static void e(sn5 sn5, LocationSource locationSource) {
        sn5.c = locationSource;
    }

    @DexIgnore
    public static void f(sn5 sn5, PortfolioApp portfolioApp) {
        sn5.f3282a = portfolioApp;
    }

    @DexIgnore
    public static void g(sn5 sn5, UserRepository userRepository) {
        sn5.d = userRepository;
    }
}
