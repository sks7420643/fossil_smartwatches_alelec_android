package com.fossil;

import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.fossil.m64;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t74 implements m64.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public c84 f3374a;
    @DexIgnore
    public c84 b;

    @DexIgnore
    public static void b(c84 c84, String str, Bundle bundle) {
        if (c84 != null) {
            c84.q(str, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.m64.b
    public void a(int i, Bundle bundle) {
        String string;
        x74 f = x74.f();
        f.b("Received Analytics message: " + i + " " + bundle);
        if (bundle != null && (string = bundle.getString("name")) != null) {
            Bundle bundle2 = bundle.getBundle(NativeProtocol.WEB_DIALOG_PARAMS);
            if (bundle2 == null) {
                bundle2 = new Bundle();
            }
            c(string, bundle2);
        }
    }

    @DexIgnore
    public final void c(String str, Bundle bundle) {
        b("clx".equals(bundle.getString("_o")) ? this.f3374a : this.b, str, bundle);
    }

    @DexIgnore
    public void d(c84 c84) {
        this.b = c84;
    }

    @DexIgnore
    public void e(c84 c84) {
        this.f3374a = c84;
    }
}
