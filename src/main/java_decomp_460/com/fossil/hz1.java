package com.fossil;

import com.fossil.nz1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz1 extends nz1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f1556a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ lz1 c;
    @DexIgnore
    public /* final */ Integer d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ List<mz1> f;
    @DexIgnore
    public /* final */ qz1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends nz1.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Long f1557a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public lz1 c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public String e;
        @DexIgnore
        public List<mz1> f;
        @DexIgnore
        public qz1 g;

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a b(long j) {
            this.f1557a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a c(lz1 lz1) {
            this.c = lz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a d(qz1 qz1) {
            this.g = qz1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a e(Integer num) {
            this.d = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a f(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a g(List<mz1> list) {
            this.f = list;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1 h() {
            String str = "";
            if (this.f1557a == null) {
                str = " requestTimeMs";
            }
            if (this.b == null) {
                str = str + " requestUptimeMs";
            }
            if (str.isEmpty()) {
                return new hz1(this.f1557a.longValue(), this.b.longValue(), this.c, this.d, this.e, this.f, this.g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.nz1.a
        public nz1.a i(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public /* synthetic */ hz1(long j, long j2, lz1 lz1, Integer num, String str, List list, qz1 qz1, a aVar) {
        this.f1556a = j;
        this.b = j2;
        this.c = lz1;
        this.d = num;
        this.e = str;
        this.f = list;
        this.g = qz1;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public lz1 b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public List<mz1> c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public Integer d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public String e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        lz1 lz1;
        Integer num;
        String str;
        List<mz1> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof nz1)) {
            return false;
        }
        nz1 nz1 = (nz1) obj;
        if (this.f1556a == nz1.g() && this.b == nz1.h() && ((lz1 = this.c) != null ? lz1.equals(((hz1) nz1).c) : ((hz1) nz1).c == null) && ((num = this.d) != null ? num.equals(((hz1) nz1).d) : ((hz1) nz1).d == null) && ((str = this.e) != null ? str.equals(((hz1) nz1).e) : ((hz1) nz1).e == null) && ((list = this.f) != null ? list.equals(((hz1) nz1).f) : ((hz1) nz1).f == null)) {
            qz1 qz1 = this.g;
            if (qz1 == null) {
                if (((hz1) nz1).g == null) {
                    z = true;
                    return z;
                }
            } else if (qz1.equals(((hz1) nz1).g)) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public qz1 f() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public long g() {
        return this.f1556a;
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public long h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        long j = this.f1556a;
        int i2 = (int) (j ^ (j >>> 32));
        long j2 = this.b;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        lz1 lz1 = this.c;
        int hashCode = lz1 == null ? 0 : lz1.hashCode();
        Integer num = this.d;
        int hashCode2 = num == null ? 0 : num.hashCode();
        String str = this.e;
        int hashCode3 = str == null ? 0 : str.hashCode();
        List<mz1> list = this.f;
        int hashCode4 = list == null ? 0 : list.hashCode();
        qz1 qz1 = this.g;
        if (qz1 != null) {
            i = qz1.hashCode();
        }
        return ((((((((hashCode ^ ((((i2 ^ 1000003) * 1000003) ^ i3) * 1000003)) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "LogRequest{requestTimeMs=" + this.f1556a + ", requestUptimeMs=" + this.b + ", clientInfo=" + this.c + ", logSource=" + this.d + ", logSourceName=" + this.e + ", logEvents=" + this.f + ", qosTier=" + this.g + "}";
    }
}
