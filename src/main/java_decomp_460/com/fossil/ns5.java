package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ns5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2562a;

    /*
    static {
        int[] iArr = new int[uh5.values().length];
        f2562a = iArr;
        iArr[uh5.BLUETOOTH_OFF.ordinal()] = 1;
        f2562a[uh5.LOCATION_PERMISSION_OFF.ordinal()] = 2;
        f2562a[uh5.LOCATION_PERMISSION_FEATURE_OFF.ordinal()] = 3;
        f2562a[uh5.LOCATION_SERVICE_OFF.ordinal()] = 4;
        f2562a[uh5.LOCATION_SERVICE_FEATURE_OFF.ordinal()] = 5;
        f2562a[uh5.BACKGROUND_LOCATION_PERMISSION_OFF.ordinal()] = 6;
    }
    */
}
