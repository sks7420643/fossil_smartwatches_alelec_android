package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ei6 implements Factory<di6> {
    @DexIgnore
    public static di6 a(bi6 bi6, UserRepository userRepository, on5 on5, GoalTrackingRepository goalTrackingRepository) {
        return new di6(bi6, userRepository, on5, goalTrackingRepository);
    }
}
