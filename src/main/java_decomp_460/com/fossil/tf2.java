package com.fossil;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tf2 implements ThreadFactory {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3403a;
    @DexIgnore
    public /* final */ AtomicInteger b;
    @DexIgnore
    public /* final */ ThreadFactory c;

    @DexIgnore
    public tf2(String str) {
        this(str, 0);
    }

    @DexIgnore
    public tf2(String str, int i) {
        this.b = new AtomicInteger();
        this.c = Executors.defaultThreadFactory();
        rc2.l(str, "Name must not be null");
        this.f3403a = str;
    }

    @DexIgnore
    public Thread newThread(Runnable runnable) {
        Thread newThread = this.c.newThread(new uf2(runnable, 0));
        String str = this.f3403a;
        int andIncrement = this.b.getAndIncrement();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13);
        sb.append(str);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        newThread.setName(sb.toString());
        return newThread;
    }
}
