package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public int b;
    @DexIgnore
    @rj4("id")
    public int c;
    @DexIgnore
    @rj4("encryptMethod")
    public int d;
    @DexIgnore
    @rj4("encryptedData")
    public String e;
    @DexIgnore
    @rj4("keyType")
    public int f;
    @DexIgnore
    @rj4("sequence")
    public String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ls4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        /* renamed from: a */
        public ls4 createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ls4(parcel);
        }

        @DexIgnore
        /* renamed from: b */
        public ls4[] newArray(int i) {
            return new ls4[i];
        }
    }

    @DexIgnore
    public ls4(int i, int i2, String str, int i3, String str2) {
        pq7.c(str, "encryptedData");
        pq7.c(str2, "sequence");
        this.c = i;
        this.d = i2;
        this.e = str;
        this.f = i3;
        this.g = str2;
        this.b = 1;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ls4(android.os.Parcel r8) {
        /*
            r7 = this;
            r6 = 0
            java.lang.String r0 = "parcel"
            com.fossil.pq7.c(r8, r0)
            int r1 = r8.readInt()
            int r2 = r8.readInt()
            java.lang.String r3 = r8.readString()
            if (r3 == 0) goto L_0x0031
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r3, r0)
            int r4 = r8.readInt()
            java.lang.String r5 = r8.readString()
            if (r5 == 0) goto L_0x002d
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.pq7.b(r5, r0)
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x002d:
            com.fossil.pq7.i()
            throw r6
        L_0x0031:
            com.fossil.pq7.i()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ls4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public final int a() {
        return this.d;
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final int d() {
        return this.f;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ls4) {
                ls4 ls4 = (ls4) obj;
                if (!(this.c == ls4.c && this.d == ls4.d && pq7.a(this.e, ls4.e) && this.f == ls4.f && pq7.a(this.g, ls4.g))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.g;
    }

    @DexIgnore
    public final void g(int i) {
        this.b = i;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.c;
        int i3 = this.d;
        String str = this.e;
        int hashCode = str != null ? str.hashCode() : 0;
        int i4 = this.f;
        String str2 = this.g;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((((hashCode + (((i2 * 31) + i3) * 31)) * 31) + i4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BCFitnessData(id=" + this.c + ", encryptMethod=" + this.d + ", encryptedData=" + this.e + ", keyType=" + this.f + ", sequence=" + this.g + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        pq7.c(parcel, "parcel");
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
    }
}
