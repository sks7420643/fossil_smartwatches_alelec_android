package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cd3 extends as2 implements cc3 {
    @DexIgnore
    public cd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IProjectionDelegate");
    }

    @DexIgnore
    @Override // com.fossil.cc3
    public final we3 H0() throws RemoteException {
        Parcel e = e(3, d());
        we3 we3 = (we3) es2.b(e, we3.CREATOR);
        e.recycle();
        return we3;
    }

    @DexIgnore
    @Override // com.fossil.cc3
    public final rg2 k0(LatLng latLng) throws RemoteException {
        Parcel d = d();
        es2.d(d, latLng);
        Parcel e = e(2, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.cc3
    public final LatLng m2(rg2 rg2) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        Parcel e = e(1, d);
        LatLng latLng = (LatLng) es2.b(e, LatLng.CREATOR);
        e.recycle();
        return latLng;
    }
}
