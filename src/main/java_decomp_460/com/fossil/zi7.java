package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.zendesk.belvedere.Belvedere;
import java.util.Arrays;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zi7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f4477a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public String f;
    @DexIgnore
    public dj7 g;
    @DexIgnore
    public TreeSet<fj7> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f4478a;
        @DexIgnore
        public String b; // = "belvedere-data";
        @DexIgnore
        public int c; // = FailureCode.USER_CANCELLED_BUT_USER_DID_NOT_SELECT_ANY_DEVICE;
        @DexIgnore
        public int d; // = FailureCode.USER_CANCELLED;
        @DexIgnore
        public int e; // = 1653;
        @DexIgnore
        public boolean f; // = true;
        @DexIgnore
        public String g; // = "*/*";
        @DexIgnore
        public dj7 h; // = new hj7();
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public TreeSet<fj7> j; // = new TreeSet<>(Arrays.asList(fj7.Camera, fj7.Gallery));

        @DexIgnore
        public a(Context context) {
            this.f4478a = context;
        }

        @DexIgnore
        public Belvedere i() {
            this.h.setLoggable(this.i);
            return new Belvedere(this.f4478a, new zi7(this));
        }

        @DexIgnore
        public a j(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public a k(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public a l(dj7 dj7) {
            if (dj7 != null) {
                this.h = dj7;
                return this;
            }
            throw new IllegalArgumentException("Invalid logger provided");
        }

        @DexIgnore
        public a m(boolean z) {
            this.i = z;
            return this;
        }
    }

    @DexIgnore
    public zi7(a aVar) {
        this.f4477a = aVar.b;
        this.b = aVar.c;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.j;
    }

    @DexIgnore
    public boolean a() {
        return this.e;
    }

    @DexIgnore
    public dj7 b() {
        return this.g;
    }

    @DexIgnore
    public TreeSet<fj7> c() {
        return this.h;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public String f() {
        return this.f;
    }

    @DexIgnore
    public String g() {
        return this.f4477a;
    }

    @DexIgnore
    public int h() {
        return this.b;
    }
}
