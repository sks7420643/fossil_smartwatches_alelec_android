package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl5 extends ul5 {
    @DexIgnore
    public String i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vl5(ck5 ck5, String str, String str2) {
        super(ck5, str, str2);
        pq7.c(ck5, "analyticsHelper");
        pq7.c(str, "traceName");
        j68.b(pq7.a(str, "view_appearance"), "traceName should be view_appearance", new Object[0]);
    }

    @DexIgnore
    public final String j() {
        return this.i;
    }

    @DexIgnore
    public final boolean k(vl5 vl5) {
        if (vl5 == null) {
            return false;
        }
        return pq7.a(vl5.i, this.i);
    }

    @DexIgnore
    public final vl5 l(String str) {
        pq7.c(str, "viewName");
        this.i = str;
        b("view_name", str);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ul5
    public String toString() {
        return "View name: " + this.i + ", running: " + f();
    }
}
