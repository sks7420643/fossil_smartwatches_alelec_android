package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b34<K, V> extends v24<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends b<K, V> {
        @DexIgnore
        public /* final */ transient b34<K, V> c;

        @DexIgnore
        public a(K k, V v, b34<K, V> b34, b34<K, V> b342) {
            super(k, v, b34);
            this.c = b342;
        }

        @DexIgnore
        @Override // com.fossil.b34
        public b34<K, V> getNextInValueBucket() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends b34<K, V> {
        @DexIgnore
        public /* final */ transient b34<K, V> b;

        @DexIgnore
        public b(K k, V v, b34<K, V> b34) {
            super(k, v);
            this.b = b34;
        }

        @DexIgnore
        @Override // com.fossil.b34
        public final b34<K, V> getNextInKeyBucket() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.b34
        public final boolean isReusable() {
            return false;
        }
    }

    @DexIgnore
    public b34(b34<K, V> b34) {
        super(b34.getKey(), b34.getValue());
    }

    @DexIgnore
    public b34(K k, V v) {
        super(k, v);
        a24.a(k, v);
    }

    @DexIgnore
    public static <K, V> b34<K, V>[] createEntryArray(int i) {
        return new b34[i];
    }

    @DexIgnore
    public b34<K, V> getNextInKeyBucket() {
        return null;
    }

    @DexIgnore
    public b34<K, V> getNextInValueBucket() {
        return null;
    }

    @DexIgnore
    public boolean isReusable() {
        return true;
    }
}
