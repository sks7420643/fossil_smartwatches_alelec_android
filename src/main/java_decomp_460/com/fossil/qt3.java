package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ CountDownLatch f3021a;

        @DexIgnore
        public a() {
            this.f3021a = new CountDownLatch(1);
        }

        @DexIgnore
        public /* synthetic */ a(pu3 pu3) {
            this();
        }

        @DexIgnore
        public final void a() throws InterruptedException {
            this.f3021a.await();
        }

        @DexIgnore
        public final boolean b(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.f3021a.await(j, timeUnit);
        }

        @DexIgnore
        @Override // com.fossil.gt3
        public final void onCanceled() {
            this.f3021a.countDown();
        }

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            this.f3021a.countDown();
        }

        @DexIgnore
        @Override // com.fossil.jt3
        public final void onSuccess(Object obj) {
            this.f3021a.countDown();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f3022a; // = new Object();
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ lu3<Void> c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public Exception g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public b(int i, lu3<Void> lu3) {
            this.b = i;
            this.c = lu3;
        }

        @DexIgnore
        public final void a() {
            if (this.d + this.e + this.f != this.b) {
                return;
            }
            if (this.g != null) {
                lu3<Void> lu3 = this.c;
                int i = this.e;
                int i2 = this.b;
                StringBuilder sb = new StringBuilder(54);
                sb.append(i);
                sb.append(" out of ");
                sb.append(i2);
                sb.append(" underlying tasks failed");
                lu3.t(new ExecutionException(sb.toString(), this.g));
            } else if (this.h) {
                this.c.v();
            } else {
                this.c.u(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.gt3
        public final void onCanceled() {
            synchronized (this.f3022a) {
                this.f++;
                this.h = true;
                a();
            }
        }

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            synchronized (this.f3022a) {
                this.e++;
                this.g = exc;
                a();
            }
        }

        @DexIgnore
        @Override // com.fossil.jt3
        public final void onSuccess(Object obj) {
            synchronized (this.f3022a) {
                this.d++;
                a();
            }
        }
    }

    @DexIgnore
    public interface c extends gt3, it3, jt3<Object> {
    }

    @DexIgnore
    public static <TResult> TResult a(nt3<TResult> nt3) throws ExecutionException, InterruptedException {
        rc2.i();
        rc2.l(nt3, "Task must not be null");
        if (nt3.p()) {
            return (TResult) i(nt3);
        }
        a aVar = new a(null);
        j(nt3, aVar);
        aVar.a();
        return (TResult) i(nt3);
    }

    @DexIgnore
    public static <TResult> TResult b(nt3<TResult> nt3, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        rc2.i();
        rc2.l(nt3, "Task must not be null");
        rc2.l(timeUnit, "TimeUnit must not be null");
        if (nt3.p()) {
            return (TResult) i(nt3);
        }
        a aVar = new a(null);
        j(nt3, aVar);
        if (aVar.b(j, timeUnit)) {
            return (TResult) i(nt3);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @DexIgnore
    public static <TResult> nt3<TResult> c(Executor executor, Callable<TResult> callable) {
        rc2.l(executor, "Executor must not be null");
        rc2.l(callable, "Callback must not be null");
        lu3 lu3 = new lu3();
        executor.execute(new pu3(lu3, callable));
        return lu3;
    }

    @DexIgnore
    public static <TResult> nt3<TResult> d() {
        lu3 lu3 = new lu3();
        lu3.v();
        return lu3;
    }

    @DexIgnore
    public static <TResult> nt3<TResult> e(Exception exc) {
        lu3 lu3 = new lu3();
        lu3.t(exc);
        return lu3;
    }

    @DexIgnore
    public static <TResult> nt3<TResult> f(TResult tresult) {
        lu3 lu3 = new lu3();
        lu3.u(tresult);
        return lu3;
    }

    @DexIgnore
    public static nt3<Void> g(Collection<? extends nt3<?>> collection) {
        if (collection == null || collection.isEmpty()) {
            return f(null);
        }
        Iterator<? extends nt3<?>> it = collection.iterator();
        while (it.hasNext()) {
            if (((nt3) it.next()) == null) {
                throw new NullPointerException("null tasks are not accepted");
            }
        }
        lu3 lu3 = new lu3();
        b bVar = new b(collection.size(), lu3);
        Iterator<? extends nt3<?>> it2 = collection.iterator();
        while (it2.hasNext()) {
            j((nt3) it2.next(), bVar);
        }
        return lu3;
    }

    @DexIgnore
    public static nt3<Void> h(nt3<?>... nt3Arr) {
        return (nt3Arr == null || nt3Arr.length == 0) ? f(null) : g(Arrays.asList(nt3Arr));
    }

    @DexIgnore
    public static <TResult> TResult i(nt3<TResult> nt3) throws ExecutionException {
        if (nt3.q()) {
            return nt3.m();
        }
        if (nt3.o()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(nt3.l());
    }

    @DexIgnore
    public static void j(nt3<?> nt3, c cVar) {
        nt3.g(pt3.b, cVar);
        nt3.e(pt3.b, cVar);
        nt3.a(pt3.b, cVar);
    }
}
