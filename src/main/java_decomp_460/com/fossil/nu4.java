package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu4 implements Factory<mu4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f2573a;
    @DexIgnore
    public /* final */ Provider<tt4> b;

    @DexIgnore
    public nu4(Provider<on5> provider, Provider<tt4> provider2) {
        this.f2573a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static nu4 a(Provider<on5> provider, Provider<tt4> provider2) {
        return new nu4(provider, provider2);
    }

    @DexIgnore
    public static mu4 c(on5 on5, tt4 tt4) {
        return new mu4(on5, tt4);
    }

    @DexIgnore
    /* renamed from: b */
    public mu4 get() {
        return c(this.f2573a.get(), this.b.get());
    }
}
