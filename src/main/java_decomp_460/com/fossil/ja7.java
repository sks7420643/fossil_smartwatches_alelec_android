package com.fossil;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja7 extends kq0 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ String g; // = er7.b(ja7.class).a();
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public b b;
    @DexIgnore
    public m25 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public HashMap f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ja7.g;
        }

        @DexIgnore
        public final ja7 b(String str, b bVar) {
            pq7.c(bVar, "listener");
            ja7 ja7 = new ja7(str);
            ja7.b = bVar;
            return ja7;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);

        @DexIgnore
        Object onCancel();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ja7 b;

        @DexIgnore
        public c(ja7 ja7) {
            this.b = ja7;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ja7 ja7 = this.b;
            String valueOf = String.valueOf(editable);
            int length = valueOf.length() - 1;
            boolean z = false;
            int i = 0;
            while (i <= length) {
                boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            ja7.e = valueOf.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.b.e);
            ImageView imageView = ja7.x6(this.b).f;
            pq7.b(imageView, "binding.ivClearName");
            imageView.setVisibility(z3 ? 0 : 8);
            if (!z3 || !(!pq7.a(this.b.d, this.b.e))) {
                this.b.E6(false);
            } else {
                this.b.E6(true);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore
    public ja7(String str) {
        this.e = str;
        this.d = str;
    }

    @DexIgnore
    public static final /* synthetic */ m25 x6(ja7 ja7) {
        m25 m25 = ja7.c;
        if (m25 != null) {
            return m25;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void D6(boolean z) {
        b bVar = this.b;
        if (bVar != null) {
            if (z) {
                bVar.onCancel();
            } else {
                String str = this.e;
                if (str != null) {
                    bVar.a(str);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public final void E6(boolean z) {
        m25 m25 = this.c;
        if (m25 != null) {
            FlexibleTextView flexibleTextView = m25.c;
            pq7.b(flexibleTextView, "binding.ftvAdd");
            flexibleTextView.setEnabled(z);
            if (z) {
                m25 m252 = this.c;
                if (m252 != null) {
                    FlexibleTextView flexibleTextView2 = m252.c;
                    if (m252 != null) {
                        ScrollView b2 = m252.b();
                        pq7.b(b2, "binding.root");
                        flexibleTextView2.setTextColor(gl0.d(b2.getContext(), 2131099968));
                        m25 m253 = this.c;
                        if (m253 != null) {
                            m253.c.setBackgroundResource(2131230824);
                        } else {
                            pq7.n("binding");
                            throw null;
                        }
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                } else {
                    pq7.n("binding");
                    throw null;
                }
            } else {
                m25 m254 = this.c;
                if (m254 != null) {
                    FlexibleTextView flexibleTextView3 = m254.c;
                    if (m254 != null) {
                        ScrollView b3 = m254.b();
                        pq7.b(b3, "binding.root");
                        flexibleTextView3.setTextColor(gl0.d(b3.getContext(), 2131099844));
                        m25 m255 = this.c;
                        if (m255 != null) {
                            m255.c.setBackgroundResource(2131230825);
                        } else {
                            pq7.n("binding");
                            throw null;
                        }
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                } else {
                    pq7.n("binding");
                    throw null;
                }
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id != 2131362358) {
                if (id == 2131362378) {
                    D6(true);
                } else if (id == 2131362684) {
                    m25 m25 = this.c;
                    if (m25 != null) {
                        m25.b.setText("");
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                }
            } else if (!TextUtils.isEmpty(this.e)) {
                D6(false);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, 2131951890);
    }

    @DexIgnore
    @Override // com.fossil.kq0
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        pq7.b(onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        boolean z = true;
        pq7.c(layoutInflater, "inflater");
        m25 c2 = m25.c(layoutInflater, viewGroup, false);
        pq7.b(c2, "FragmentAddTextElementBi\u2026flater, container, false)");
        this.c = c2;
        if (c2 != null) {
            FlexibleTextView flexibleTextView = c2.e;
            pq7.b(flexibleTextView, "binding.ftvContent");
            flexibleTextView.setText(um5.c(requireContext(), 2131886596));
            m25 m25 = this.c;
            if (m25 != null) {
                FlexibleEditText flexibleEditText = m25.b;
                pq7.b(flexibleEditText, "binding.fetText");
                flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(23)});
                String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                if (d2 != null) {
                    m25 m252 = this.c;
                    if (m252 != null) {
                        m252.h.setBackgroundColor(Color.parseColor(d2));
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                }
                String str = this.e;
                if (str != null) {
                    m25 m253 = this.c;
                    if (m253 != null) {
                        FlexibleEditText flexibleEditText2 = m253.b;
                        flexibleEditText2.setText(str);
                        flexibleEditText2.setSelection(str.length());
                        m25 m254 = this.c;
                        if (m254 != null) {
                            ImageView imageView = m254.f;
                            pq7.b(imageView, "binding.ivClearName");
                            if (str.length() <= 0) {
                                z = false;
                            }
                            imageView.setVisibility(z ? 0 : 8);
                        } else {
                            pq7.n("binding");
                            throw null;
                        }
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                }
                m25 m255 = this.c;
                if (m255 != null) {
                    FlexibleEditText flexibleEditText3 = m255.b;
                    pq7.b(flexibleEditText3, "binding.fetText");
                    flexibleEditText3.addTextChangedListener(new c(this));
                    m25 m256 = this.c;
                    if (m256 != null) {
                        m256.f.setOnClickListener(this);
                        m25 m257 = this.c;
                        if (m257 != null) {
                            m257.c.setOnClickListener(this);
                            m25 m258 = this.c;
                            if (m258 != null) {
                                m258.d.setOnClickListener(this);
                                E6(false);
                                m25 m259 = this.c;
                                if (m259 != null) {
                                    return m259.b();
                                }
                                pq7.n("binding");
                                throw null;
                            }
                            pq7.n("binding");
                            throw null;
                        }
                        pq7.n("binding");
                        throw null;
                    }
                    pq7.n("binding");
                    throw null;
                }
                pq7.n("binding");
                throw null;
            }
            pq7.n("binding");
            throw null;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Object systemService = requireActivity().getSystemService("input_method");
        if (systemService != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) systemService;
            m25 m25 = this.c;
            if (m25 != null) {
                FlexibleEditText flexibleEditText = m25.b;
                pq7.b(flexibleEditText, "binding.fetText");
                gj5.b(flexibleEditText, inputMethodManager);
                return;
            }
            pq7.n("binding");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
