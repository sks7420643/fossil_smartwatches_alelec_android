package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f2635a; // = 3;
    @DexIgnore
    public float b; // = 0.75f;
    @DexIgnore
    public float c; // = 12.0f;
    @DexIgnore
    public float d; // = 0.02f;
    @DexIgnore
    public r57 e; // = r57.Top;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public List<s57> l; // = s57.FREEDOM;
}
