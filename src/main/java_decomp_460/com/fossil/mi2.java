package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi2 implements Parcelable.Creator<uh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uh2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        int[] iArr = null;
        String str = null;
        ii2 ii2 = null;
        vh2 vh2 = null;
        String str2 = null;
        DataType dataType = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    dataType = (DataType) ad2.e(parcel, t, DataType.CREATOR);
                    break;
                case 2:
                    str2 = ad2.f(parcel, t);
                    break;
                case 3:
                    i = ad2.v(parcel, t);
                    break;
                case 4:
                    vh2 = (vh2) ad2.e(parcel, t, vh2.CREATOR);
                    break;
                case 5:
                    ii2 = (ii2) ad2.e(parcel, t, ii2.CREATOR);
                    break;
                case 6:
                    str = ad2.f(parcel, t);
                    break;
                case 7:
                default:
                    ad2.B(parcel, t);
                    break;
                case 8:
                    iArr = ad2.d(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new uh2(dataType, str2, i, vh2, ii2, str, iArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uh2[] newArray(int i) {
        return new uh2[i];
    }
}
