package com.fossil;

import android.os.IBinder;
import com.fossil.kf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nf4 implements Runnable {
    @DexIgnore
    public /* final */ kf4.b b;
    @DexIgnore
    public /* final */ IBinder c;

    @DexIgnore
    public nf4(kf4.b bVar, IBinder iBinder) {
        this.b = bVar;
        this.c = iBinder;
    }

    @DexIgnore
    public final void run() {
        this.b.d(this.c);
    }
}
