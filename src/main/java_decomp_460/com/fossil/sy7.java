package com.fossil;

import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy7<E> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ AtomicReferenceFieldUpdater f3334a; // = AtomicReferenceFieldUpdater.newUpdater(sy7.class, Object.class, "_state");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(sy7.class, "_updating");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater c; // = AtomicReferenceFieldUpdater.newUpdater(sy7.class, Object.class, "onCloseHandler");
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public static /* final */ vz7 e;
    @DexIgnore
    public static /* final */ b<Object> f;
    @DexIgnore
    public volatile Object _state; // = f;
    @DexIgnore
    public volatile int _updating; // = 0;
    @DexIgnore
    public volatile Object onCloseHandler; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Throwable f3335a;

        @DexIgnore
        public a(Throwable th) {
            this.f3335a = th;
        }

        @DexIgnore
        public final Throwable a() {
            Throwable th = this.f3335a;
            return th != null ? th : new ry7("Channel was closed");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<E> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f3336a;
        @DexIgnore
        public /* final */ c<E>[] b;

        @DexIgnore
        public b(Object obj, c<E>[] cVarArr) {
            this.f3336a = obj;
            this.b = cVarArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<E> extends ty7<E> implements vy7<E> {
        @DexIgnore
        public c(sy7<E> sy7) {
        }

        @DexIgnore
        @Override // com.fossil.ty7
        public Object x(E e) {
            return super.x(e);
        }
    }

    /*
    static {
        vz7 vz7 = new vz7("UNDEFINED");
        e = vz7;
        f = new b<>(vz7, null);
    }
    */

    @DexIgnore
    public final c<E>[] a(c<E>[] cVarArr, c<E> cVar) {
        if (cVarArr != null) {
            return (c[]) dm7.r(cVarArr, cVar);
        }
        c<E>[] cVarArr2 = new c[1];
        for (int i = 0; i < 1; i++) {
            cVarArr2[i] = cVar;
        }
        return cVarArr2;
    }

    @DexIgnore
    public void b(CancellationException cancellationException) {
        c(cancellationException);
    }

    @DexIgnore
    public boolean c(Throwable th) {
        Object obj;
        do {
            obj = this._state;
            if (obj instanceof a) {
                return false;
            }
            if (!(obj instanceof b)) {
                throw new IllegalStateException(("Invalid state " + obj).toString());
            }
        } while (!f3334a.compareAndSet(this, obj, th == null ? d : new a(th)));
        if (obj != null) {
            c<E>[] cVarArr = ((b) obj).b;
            if (cVarArr != null) {
                for (c<E> cVar : cVarArr) {
                    cVar.c(th);
                }
            }
            d(th);
            return true;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
    }

    @DexIgnore
    public final void d(Throwable th) {
        Object obj;
        Object obj2 = this.onCloseHandler;
        if (obj2 != null && obj2 != (obj = ky7.c) && c.compareAndSet(this, obj2, obj)) {
            ir7.d(obj2, 1);
            ((rp7) obj2).invoke(th);
        }
    }

    @DexIgnore
    public final a e(E e2) {
        Object obj;
        if (!b.compareAndSet(this, 0, 1)) {
            return null;
        }
        do {
            try {
                obj = this._state;
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    this._updating = 0;
                    return aVar;
                } else if (!(obj instanceof b)) {
                    throw new IllegalStateException(("Invalid state " + obj).toString());
                } else if (obj == null) {
                    throw new il7("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
                }
            } finally {
                this._updating = 0;
            }
        } while (!f3334a.compareAndSet(this, obj, new b(e2, ((b) obj).b)));
        c<E>[] cVarArr = ((b) obj).b;
        if (cVarArr != null) {
            for (c<E> cVar : cVarArr) {
                cVar.x(e2);
            }
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.sy7<E> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.sy7$c */
    /* JADX WARN: Multi-variable type inference failed */
    public vy7<E> f() {
        Object obj;
        b bVar;
        Object obj2;
        c cVar = new c(this);
        do {
            obj = this._state;
            if (obj instanceof a) {
                cVar.c(((a) obj).f3335a);
                return cVar;
            } else if (obj instanceof b) {
                bVar = (b) obj;
                Object obj3 = bVar.f3336a;
                if (obj3 != e) {
                    cVar.x(obj3);
                }
                obj2 = bVar.f3336a;
                if (obj != null) {
                } else {
                    throw new il7("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
                }
            } else {
                throw new IllegalStateException(("Invalid state " + obj).toString());
            }
        } while (!f3334a.compareAndSet(this, obj, new b(obj2, a(bVar.b, cVar))));
        return cVar;
    }

    @DexIgnore
    public Object g(E e2, qn7<? super tl7> qn7) {
        a e3 = e(e2);
        if (e3 == null) {
            return e3 == yn7.d() ? e3 : tl7.f3441a;
        }
        throw e3.a();
    }
}
