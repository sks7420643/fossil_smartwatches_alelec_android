package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu3<TResult> implements hu3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f987a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public jt3<? super TResult> c;

    @DexIgnore
    public eu3(Executor executor, jt3<? super TResult> jt3) {
        this.f987a = executor;
        this.c = jt3;
    }

    @DexIgnore
    @Override // com.fossil.hu3
    public final void a(nt3<TResult> nt3) {
        if (nt3.q()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.f987a.execute(new du3(this, nt3));
                }
            }
        }
    }
}
