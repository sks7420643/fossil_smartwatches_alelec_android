package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qj1<R> extends ei1 {
    @DexIgnore
    void a(pj1 pj1);

    @DexIgnore
    void b(R r, tj1<? super R> tj1);

    @DexIgnore
    void d(bj1 bj1);

    @DexIgnore
    void f(Drawable drawable);

    @DexIgnore
    void h(Drawable drawable);

    @DexIgnore
    bj1 i();

    @DexIgnore
    void j(Drawable drawable);

    @DexIgnore
    void k(pj1 pj1);
}
