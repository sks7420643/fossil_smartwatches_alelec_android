package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ry1 d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public kt1 createFromParcel(Parcel parcel) {
            boolean z = true;
            Parcelable readParcelable = parcel.readParcelable(hq1.class.getClassLoader());
            if (readParcelable != null) {
                pq7.b(readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                hq1 hq1 = (hq1) readParcelable;
                nt1 nt1 = (nt1) parcel.readParcelable(nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    ry1 ry1 = (ry1) readParcelable2;
                    int readInt = parcel.readInt();
                    if (parcel.readInt() != 1) {
                        z = false;
                    }
                    return new kt1(hq1, nt1, ry1, readInt, z);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public kt1[] newArray(int i) {
            return new kt1[i];
        }
    }

    @DexIgnore
    public kt1(hq1 hq1, nt1 nt1, ry1 ry1, int i, boolean z) {
        super(hq1, nt1);
        this.e = z;
        this.f = i;
        this.d = ry1;
    }

    @DexIgnore
    public kt1(hq1 hq1, ry1 ry1, int i) {
        super(hq1, null);
        this.e = true;
        this.f = i;
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        return g80.k(g80.k(g80.k(super.a(), jd0.t3, this.d.toString()), jd0.w3, Boolean.valueOf(this.e)), jd0.x3, Integer.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        try {
            i9 i9Var = i9.d;
            jq1 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9Var.a(s, ry1, new ib0(((lq1) deviceRequest).c(), new ry1(this.d.getMajor(), this.d.getMinor()), this.e, this.f).a());
            }
            throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sx1 e2) {
            d90.i.i(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(kt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            kt1 kt1 = (kt1) obj;
            if (!pq7.a(this.d, kt1.d)) {
                return false;
            }
            if (this.e != kt1.e) {
                return false;
            }
            return this.f == kt1.f;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeTravelMicroAppData");
    }

    @DexIgnore
    public final ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    public final boolean getShipHandsToTwelve() {
        return this.e;
    }

    @DexIgnore
    public final int getTravelTimeInMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.d.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Boolean.valueOf(this.e).hashCode()) * 31) + Integer.valueOf(this.f).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(this.e ? 1 : 0);
        }
    }
}
