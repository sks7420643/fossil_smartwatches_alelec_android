package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qg7 extends ng7 {
    @DexIgnore
    public static String o;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;

    @DexIgnore
    public qg7(Context context, int i, jg7 jg7) {
        super(context, i, jg7);
        this.m = tg7.a(context).e();
        if (o == null) {
            o = ei7.E(context);
        }
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public og7 a() {
        return og7.h;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public boolean b(JSONObject jSONObject) {
        ji7.d(jSONObject, "op", o);
        ji7.d(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }

    @DexIgnore
    public void i(String str) {
        this.n = str;
    }
}
