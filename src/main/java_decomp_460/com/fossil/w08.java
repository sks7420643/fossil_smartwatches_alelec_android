package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w08 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ vz7 f3864a; // = new vz7("UNLOCK_FAIL");
    @DexIgnore
    public static /* final */ vz7 b; // = new vz7("LOCKED");
    @DexIgnore
    public static /* final */ vz7 c; // = new vz7("UNLOCKED");
    @DexIgnore
    public static /* final */ t08 d; // = new t08(b);
    @DexIgnore
    public static /* final */ t08 e; // = new t08(c);

    @DexIgnore
    public static final u08 a(boolean z) {
        return new v08(z);
    }

    @DexIgnore
    public static /* synthetic */ u08 b(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }
}
