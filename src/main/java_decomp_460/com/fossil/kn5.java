package com.fossil;

import com.fossil.jn5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kn5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1936a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] g;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] h;

    /*
    static {
        int[] iArr = new int[uh5.values().length];
        f1936a = iArr;
        iArr[uh5.BLUETOOTH_OFF.ordinal()] = 1;
        f1936a[uh5.BACKGROUND_LOCATION_PERMISSION_OFF.ordinal()] = 2;
        f1936a[uh5.LOCATION_PERMISSION_OFF.ordinal()] = 3;
        f1936a[uh5.LOCATION_SERVICE_OFF.ordinal()] = 4;
        int[] iArr2 = new int[jn5.c.values().length];
        b = iArr2;
        iArr2[jn5.c.LOCATION_BACKGROUND.ordinal()] = 1;
        b[jn5.c.LOCATION_FINE.ordinal()] = 2;
        b[jn5.c.READ_CALL_LOG.ordinal()] = 3;
        b[jn5.c.READ_PHONE_STATE.ordinal()] = 4;
        b[jn5.c.ANSWER_PHONE_CALL.ordinal()] = 5;
        b[jn5.c.CALL_PHONE.ordinal()] = 6;
        b[jn5.c.READ_SMS.ordinal()] = 7;
        b[jn5.c.RECEIVE_MMS.ordinal()] = 8;
        b[jn5.c.RECEIVE_SMS.ordinal()] = 9;
        int[] iArr3 = new int[jn5.c.values().length];
        c = iArr3;
        iArr3[jn5.c.BLUETOOTH_CONNECTION.ordinal()] = 1;
        c[jn5.c.LOCATION_FINE.ordinal()] = 2;
        c[jn5.c.LOCATION_BACKGROUND.ordinal()] = 3;
        c[jn5.c.LOCATION_SERVICE.ordinal()] = 4;
        c[jn5.c.READ_CONTACTS.ordinal()] = 5;
        c[jn5.c.READ_PHONE_STATE.ordinal()] = 6;
        c[jn5.c.READ_CALL_LOG.ordinal()] = 7;
        c[jn5.c.CALL_PHONE.ordinal()] = 8;
        c[jn5.c.ANSWER_PHONE_CALL.ordinal()] = 9;
        c[jn5.c.READ_SMS.ordinal()] = 10;
        c[jn5.c.RECEIVE_SMS.ordinal()] = 11;
        c[jn5.c.RECEIVE_MMS.ordinal()] = 12;
        c[jn5.c.SEND_SMS.ordinal()] = 13;
        c[jn5.c.NOTIFICATION_ACCESS.ordinal()] = 14;
        c[jn5.c.CAMERA.ordinal()] = 15;
        c[jn5.c.READ_EXTERNAL_STORAGE.ordinal()] = 16;
        int[] iArr4 = new int[jn5.c.values().length];
        d = iArr4;
        iArr4[jn5.c.LOCATION_FINE.ordinal()] = 1;
        d[jn5.c.LOCATION_BACKGROUND.ordinal()] = 2;
        d[jn5.c.READ_CONTACTS.ordinal()] = 3;
        d[jn5.c.READ_PHONE_STATE.ordinal()] = 4;
        d[jn5.c.READ_CALL_LOG.ordinal()] = 5;
        d[jn5.c.CALL_PHONE.ordinal()] = 6;
        d[jn5.c.ANSWER_PHONE_CALL.ordinal()] = 7;
        d[jn5.c.READ_SMS.ordinal()] = 8;
        d[jn5.c.RECEIVE_SMS.ordinal()] = 9;
        d[jn5.c.RECEIVE_MMS.ordinal()] = 10;
        d[jn5.c.SEND_SMS.ordinal()] = 11;
        d[jn5.c.CAMERA.ordinal()] = 12;
        d[jn5.c.READ_EXTERNAL_STORAGE.ordinal()] = 13;
        int[] iArr5 = new int[jn5.a.values().length];
        e = iArr5;
        iArr5[jn5.a.SET_COMPLICATION_WATCH_APP_WEATHER.ordinal()] = 1;
        e[jn5.a.SET_MICRO_APP_COMMUTE_TIME.ordinal()] = 2;
        e[jn5.a.SET_WATCH_APP_COMMUTE_TIME.ordinal()] = 3;
        e[jn5.a.SET_COMPLICATION_CHANCE_OF_RAIN.ordinal()] = 4;
        e[jn5.a.FIND_DEVICE.ordinal()] = 5;
        e[jn5.a.PAIR_DEVICE.ordinal()] = 6;
        e[jn5.a.SET_WATCH_APP_MUSIC.ordinal()] = 7;
        e[jn5.a.SET_MICRO_APP_MUSIC.ordinal()] = 8;
        int[] iArr6 = new int[jn5.c.values().length];
        f = iArr6;
        iArr6[jn5.c.BLUETOOTH_CONNECTION.ordinal()] = 1;
        f[jn5.c.LOCATION_FINE.ordinal()] = 2;
        f[jn5.c.LOCATION_BACKGROUND.ordinal()] = 3;
        f[jn5.c.LOCATION_SERVICE.ordinal()] = 4;
        f[jn5.c.READ_CONTACTS.ordinal()] = 5;
        f[jn5.c.READ_PHONE_STATE.ordinal()] = 6;
        f[jn5.c.READ_CALL_LOG.ordinal()] = 7;
        f[jn5.c.CALL_PHONE.ordinal()] = 8;
        f[jn5.c.ANSWER_PHONE_CALL.ordinal()] = 9;
        f[jn5.c.READ_SMS.ordinal()] = 10;
        f[jn5.c.RECEIVE_MMS.ordinal()] = 11;
        f[jn5.c.RECEIVE_SMS.ordinal()] = 12;
        f[jn5.c.SEND_SMS.ordinal()] = 13;
        f[jn5.c.NOTIFICATION_ACCESS.ordinal()] = 14;
        f[jn5.c.CAMERA.ordinal()] = 15;
        f[jn5.c.READ_EXTERNAL_STORAGE.ordinal()] = 16;
        int[] iArr7 = new int[jn5.c.values().length];
        g = iArr7;
        iArr7[jn5.c.NOTIFICATION_ACCESS.ordinal()] = 1;
        g[jn5.c.BLUETOOTH_CONNECTION.ordinal()] = 2;
        g[jn5.c.LOCATION_SERVICE.ordinal()] = 3;
        int[] iArr8 = new int[jn5.c.values().length];
        h = iArr8;
        iArr8[jn5.c.BLUETOOTH_CONNECTION.ordinal()] = 1;
        h[jn5.c.LOCATION_FINE.ordinal()] = 2;
        h[jn5.c.LOCATION_SERVICE.ordinal()] = 3;
        h[jn5.c.READ_CONTACTS.ordinal()] = 4;
        h[jn5.c.READ_PHONE_STATE.ordinal()] = 5;
        h[jn5.c.READ_CALL_LOG.ordinal()] = 6;
        h[jn5.c.CALL_PHONE.ordinal()] = 7;
        h[jn5.c.ANSWER_PHONE_CALL.ordinal()] = 8;
        h[jn5.c.READ_SMS.ordinal()] = 9;
        h[jn5.c.RECEIVE_SMS.ordinal()] = 10;
        h[jn5.c.RECEIVE_MMS.ordinal()] = 11;
        h[jn5.c.CAMERA.ordinal()] = 12;
        h[jn5.c.READ_EXTERNAL_STORAGE.ordinal()] = 13;
        h[jn5.c.NOTIFICATION_ACCESS.ordinal()] = 14;
        h[jn5.c.LOCATION_BACKGROUND.ordinal()] = 15;
        h[jn5.c.SEND_SMS.ordinal()] = 16;
    }
    */
}
