package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pq2 extends zc2 implements z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<pq2> CREATOR; // = new qq2();
    @DexIgnore
    public /* final */ Status b;

    /*
    static {
        Status status = Status.f;
    }
    */

    @DexIgnore
    public pq2(Status status) {
        this.b = status;
    }

    @DexIgnore
    @Override // com.fossil.z62
    public final Status a() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, a(), i, false);
        bd2.b(parcel, a2);
    }
}
