package com.fossil;

import com.portfolio.platform.ui.view.chart.base.BarChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nv5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2581a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;

    /*
    static {
        int[] iArr = new int[BarChart.e.values().length];
        f2581a = iArr;
        iArr[BarChart.e.LOWEST.ordinal()] = 1;
        f2581a[BarChart.e.DEFAULT.ordinal()] = 2;
        f2581a[BarChart.e.HIGHEST.ordinal()] = 3;
        int[] iArr2 = new int[BarChart.e.values().length];
        b = iArr2;
        iArr2[BarChart.e.LOWEST.ordinal()] = 1;
        b[BarChart.e.DEFAULT.ordinal()] = 2;
        b[BarChart.e.HIGHEST.ordinal()] = 3;
        int[] iArr3 = new int[BarChart.e.values().length];
        c = iArr3;
        iArr3[BarChart.e.LOWEST.ordinal()] = 1;
        c[BarChart.e.DEFAULT.ordinal()] = 2;
        c[BarChart.e.HIGHEST.ordinal()] = 3;
        int[] iArr4 = new int[BarChart.e.values().length];
        d = iArr4;
        iArr4[BarChart.e.LOWEST.ordinal()] = 1;
        d[BarChart.e.DEFAULT.ordinal()] = 2;
        d[BarChart.e.HIGHEST.ordinal()] = 3;
        int[] iArr5 = new int[BarChart.e.values().length];
        e = iArr5;
        iArr5[BarChart.e.LOWEST.ordinal()] = 1;
        e[BarChart.e.DEFAULT.ordinal()] = 2;
        e[BarChart.e.HIGHEST.ordinal()] = 3;
    }
    */
}
