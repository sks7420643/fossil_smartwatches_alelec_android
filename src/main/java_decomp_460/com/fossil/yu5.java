package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu5 implements Factory<xu5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<UserRepository> f4379a;

    @DexIgnore
    public yu5(Provider<UserRepository> provider) {
        this.f4379a = provider;
    }

    @DexIgnore
    public static yu5 a(Provider<UserRepository> provider) {
        return new yu5(provider);
    }

    @DexIgnore
    public static xu5 c(UserRepository userRepository) {
        return new xu5(userRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public xu5 get() {
        return c(this.f4379a.get());
    }
}
