package com.fossil;

import android.content.Context;
import android.content.IntentFilter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tg7 {
    @DexIgnore
    public static tg7 i;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<String> f3413a; // = null;
    @DexIgnore
    public volatile int b; // = 2;
    @DexIgnore
    public volatile String c; // = "";
    @DexIgnore
    public volatile HttpHost d; // = null;
    @DexIgnore
    public yh7 e; // = null;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public Context g; // = null;
    @DexIgnore
    public th7 h; // = null;

    @DexIgnore
    public tg7(Context context) {
        this.g = context.getApplicationContext();
        this.e = new yh7();
        qi7.b(context);
        this.h = ei7.p();
        p();
        m();
        k();
    }

    @DexIgnore
    public static tg7 a(Context context) {
        if (i == null) {
            synchronized (tg7.class) {
                try {
                    if (i == null) {
                        i = new tg7(context);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return i;
    }

    @DexIgnore
    public HttpHost c() {
        return this.d;
    }

    @DexIgnore
    public void d(String str) {
        if (fg7.K()) {
            this.h.h("updateIpList " + str);
        }
        try {
            if (ei7.t(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.length() > 0) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String string = jSONObject.getString(keys.next());
                        if (ei7.t(string)) {
                            String[] split = string.split(";");
                            for (String str2 : split) {
                                if (ei7.t(str2)) {
                                    String[] split2 = str2.split(":");
                                    if (split2.length > 1) {
                                        String str3 = split2[0];
                                        if (f(str3) && !this.f3413a.contains(str3)) {
                                            if (fg7.K()) {
                                                this.h.h("add new ip:" + str3);
                                            }
                                            this.f3413a.add(str3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            this.h.e(e2);
        }
        this.f = new Random().nextInt(this.f3413a.size());
    }

    @DexIgnore
    public String e() {
        return this.c;
    }

    @DexIgnore
    public final boolean f(String str) {
        return Pattern.compile("(2[5][0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})").matcher(str).matches();
    }

    @DexIgnore
    public int g() {
        return this.b;
    }

    @DexIgnore
    public void h() {
        this.f = (this.f + 1) % this.f3413a.size();
    }

    @DexIgnore
    public boolean i() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean j() {
        return this.b != 0;
    }

    @DexIgnore
    public void k() {
        if (ji7.k(this.g)) {
            if (fg7.v) {
                o();
            }
            this.c = ei7.H(this.g);
            if (fg7.K()) {
                th7 th7 = this.h;
                th7.h("NETWORK name:" + this.c);
            }
            if (ei7.t(this.c)) {
                this.b = "WIFI".equalsIgnoreCase(this.c) ? 1 : 2;
                this.d = ei7.k(this.g);
            }
            if (ig7.g()) {
                ig7.q(this.g);
                return;
            }
            return;
        }
        if (fg7.K()) {
            this.h.h("NETWORK TYPE: network is close.");
        }
        p();
    }

    @DexIgnore
    public void l() {
        this.g.getApplicationContext().registerReceiver(new mh7(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @DexIgnore
    public final void m() {
        ArrayList arrayList = new ArrayList(10);
        this.f3413a = arrayList;
        arrayList.add("117.135.169.101");
        this.f3413a.add("140.207.54.125");
        this.f3413a.add("180.153.8.53");
        this.f3413a.add("120.198.203.175");
        this.f3413a.add("14.17.43.18");
        this.f3413a.add("163.177.71.186");
        this.f3413a.add("111.30.131.31");
        this.f3413a.add("123.126.121.167");
        this.f3413a.add("123.151.152.111");
        this.f3413a.add("113.142.45.79");
        this.f3413a.add("123.138.162.90");
        this.f3413a.add("103.7.30.94");
    }

    @DexIgnore
    public final String n() {
        try {
            if (!f("pingma.qq.com")) {
                return InetAddress.getByName("pingma.qq.com").getHostAddress();
            }
        } catch (Exception e2) {
            this.h.e(e2);
        }
        return "";
    }

    @DexIgnore
    public final void o() {
        String str;
        String n = n();
        if (fg7.K()) {
            th7 th7 = this.h;
            th7.h("remoteIp ip is " + n);
        }
        if (ei7.t(n)) {
            if (this.f3413a.contains(n)) {
                str = n;
            } else {
                str = this.f3413a.get(this.f);
                if (fg7.K()) {
                    th7 th72 = this.h;
                    th72.l(n + " not in ip list, change to:" + str);
                }
            }
            fg7.T("http://" + str + ":80/mstat/report");
        }
    }

    @DexIgnore
    public final void p() {
        this.b = 0;
        this.d = null;
        this.c = null;
    }
}
