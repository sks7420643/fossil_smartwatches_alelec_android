package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp4 implements Factory<AuthApiGuestService> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f636a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public cp4(uo4 uo4, Provider<on5> provider) {
        this.f636a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static cp4 a(uo4 uo4, Provider<on5> provider) {
        return new cp4(uo4, provider);
    }

    @DexIgnore
    public static AuthApiGuestService c(uo4 uo4, on5 on5) {
        AuthApiGuestService j = uo4.j(on5);
        lk7.c(j, "Cannot return null from a non-@Nullable @Provides method");
        return j;
    }

    @DexIgnore
    /* renamed from: b */
    public AuthApiGuestService get() {
        return c(this.f636a, this.b.get());
    }
}
