package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import com.fossil.n57;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1714a; // = "j57";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f1715a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ m57 c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ k57 e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.j57$a$a")
        /* renamed from: com.fossil.j57$a$a  reason: collision with other inner class name */
        public class C0127a implements n57.b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ ImageView f1716a;

            @DexIgnore
            public C0127a(ImageView imageView) {
                this.f1716a = imageView;
            }

            @DexIgnore
            @Override // com.fossil.n57.b
            public void a(BitmapDrawable bitmapDrawable) {
                k57 k57 = a.this.e;
                if (k57 == null) {
                    this.f1716a.setImageDrawable(bitmapDrawable);
                } else {
                    k57.a(bitmapDrawable);
                }
            }
        }

        @DexIgnore
        public a(Context context, Bitmap bitmap, m57 m57, boolean z, k57 k57) {
            this.f1715a = context;
            this.b = bitmap;
            this.c = m57;
            this.d = z;
            this.e = k57;
        }

        @DexIgnore
        public void a(ImageView imageView) {
            this.c.f2304a = this.b.getWidth();
            this.c.b = this.b.getHeight();
            if (this.d) {
                new n57(imageView.getContext(), this.b, this.c, new C0127a(imageView)).a();
            } else {
                imageView.setImageDrawable(new BitmapDrawable(this.f1715a.getResources(), i57.a(imageView.getContext(), this.b, this.c)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ View f1717a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ m57 c; // = new m57();
        @DexIgnore
        public boolean d;
        @DexIgnore
        public k57 e;

        @DexIgnore
        public b(Context context) {
            this.b = context;
            View view = new View(context);
            this.f1717a = view;
            view.setTag(j57.f1714a);
        }

        @DexIgnore
        public a a(Bitmap bitmap) {
            return new a(this.b, bitmap, this.c, this.d, this.e);
        }

        @DexIgnore
        public b b(int i) {
            this.c.c = i;
            return this;
        }

        @DexIgnore
        public b c(int i) {
            this.c.d = i;
            return this;
        }
    }

    @DexIgnore
    public static b a(Context context) {
        return new b(context);
    }
}
