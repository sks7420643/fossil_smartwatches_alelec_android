package com.fossil;

import android.view.View;
import coil.memory.ViewTargetRequestDelegate;
import com.fossil.xw7;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k71 implements View.OnAttachStateChangeListener {
    @DexIgnore
    public ViewTargetRequestDelegate b;
    @DexIgnore
    public volatile UUID c;
    @DexIgnore
    public volatile xw7 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "coil.memory.ViewTargetRequestManager$clearCurrentRequest$1", f = "ViewTargetRequestManager.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ k71 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(k71 k71, qn7 qn7) {
            super(2, qn7);
            this.this$0 = k71;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.c(null);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
        xw7 xw7 = this.d;
        if (xw7 != null) {
            xw7.a.a(xw7, null, 1, null);
        }
        this.d = gu7.d(jv7.a(bw7.c().S()), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final UUID b() {
        UUID uuid = this.c;
        if (uuid != null && w81.n() && this.e) {
            return uuid;
        }
        UUID randomUUID = UUID.randomUUID();
        pq7.b(randomUUID, "UUID.randomUUID()");
        return randomUUID;
    }

    @DexIgnore
    public final void c(ViewTargetRequestDelegate viewTargetRequestDelegate) {
        if (this.e) {
            this.e = false;
        } else {
            xw7 xw7 = this.d;
            if (xw7 != null) {
                xw7.a.a(xw7, null, 1, null);
            }
            this.d = null;
        }
        ViewTargetRequestDelegate viewTargetRequestDelegate2 = this.b;
        if (viewTargetRequestDelegate2 != null) {
            viewTargetRequestDelegate2.c();
        }
        this.b = viewTargetRequestDelegate;
        this.f = true;
    }

    @DexIgnore
    public final UUID d(xw7 xw7) {
        pq7.c(xw7, "job");
        UUID b2 = b();
        this.c = b2;
        return b2;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
        pq7.c(view, "v");
        if (this.f) {
            this.f = false;
            return;
        }
        ViewTargetRequestDelegate viewTargetRequestDelegate = this.b;
        if (viewTargetRequestDelegate != null) {
            this.e = true;
            viewTargetRequestDelegate.d();
        }
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        pq7.c(view, "v");
        this.f = false;
        ViewTargetRequestDelegate viewTargetRequestDelegate = this.b;
        if (viewTargetRequestDelegate != null) {
            viewTargetRequestDelegate.c();
        }
    }
}
