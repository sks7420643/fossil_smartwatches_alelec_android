package com.fossil;

import android.os.Build;
import android.util.Log;
import com.zendesk.service.ErrorResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import retrofit.android.AndroidLog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mj7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ TimeZone f2393a; // = TimeZone.getTimeZone("UTC");
    @DexIgnore
    public static /* final */ List<c> b; // = new ArrayList();
    @DexIgnore
    public static c c;
    @DexIgnore
    public static boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c {
        @DexIgnore
        @Override // com.fossil.mj7.c
        public void a(d dVar, String str, String str2, Throwable th) {
            String a2 = nj7.a(str);
            if (b(str) && d.ERROR == dVar) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
                simpleDateFormat.setTimeZone(mj7.f2393a);
                Log.println(d.ERROR.priority, a2, "Time in UTC: " + simpleDateFormat.format(new Date()));
            }
            if (th != null) {
                str2 = str2 + tj7.b + Log.getStackTraceString(th);
            }
            for (String str3 : nj7.c(str2, AndroidLog.LOG_CHUNK_SIZE)) {
                Log.println(dVar == null ? d.INFO.priority : dVar.priority, a2, str3);
            }
        }

        @DexIgnore
        public final boolean b(String str) {
            return tj7.b(str) && (str.endsWith("Provider") || str.endsWith("Service"));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {
        @DexIgnore
        @Override // com.fossil.mj7.c
        public void a(d dVar, String str, String str2, Throwable th) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("[");
            sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).format(new Date()));
            sb.append("]");
            sb.append(" ");
            if (dVar == null) {
                dVar = d.INFO;
            }
            sb.append(nj7.b(dVar.priority));
            sb.append("/");
            if (!tj7.b(str)) {
                str = "UNKNOWN";
            }
            sb.append(str);
            sb.append(": ");
            sb.append(str2);
            System.out.println(sb.toString());
            if (th != null) {
                th.printStackTrace(System.out);
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(d dVar, String str, String str2, Throwable th);
    }

    @DexIgnore
    public enum d {
        VERBOSE(2),
        DEBUG(3),
        INFO(4),
        WARN(5),
        ERROR(6);
        
        @DexIgnore
        public /* final */ int priority;

        @DexIgnore
        public d(int i) {
            this.priority = i;
        }
    }

    /*
    static {
        b bVar;
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                c = new a();
            }
            if (c == null) {
                bVar = new b();
                c = bVar;
            }
        } catch (ClassNotFoundException e) {
            if (c == null) {
                bVar = new b();
            }
        } catch (Throwable th) {
            if (c == null) {
                c = new b();
            }
            throw th;
        }
    }
    */

    @DexIgnore
    public static void b(String str, String str2, Throwable th, Object... objArr) {
        i(d.DEBUG, str, str2, th, objArr);
    }

    @DexIgnore
    public static void c(String str, String str2, Object... objArr) {
        i(d.DEBUG, str, str2, null, objArr);
    }

    @DexIgnore
    public static void d(String str, ErrorResponse errorResponse) {
        StringBuilder sb = new StringBuilder();
        if (errorResponse != null) {
            sb.append("Network Error: ");
            sb.append(errorResponse.c());
            sb.append(", Status Code: ");
            sb.append(errorResponse.a());
            if (tj7.b(errorResponse.b())) {
                sb.append(", Reason: ");
                sb.append(errorResponse.b());
            }
        }
        String sb2 = sb.toString();
        d dVar = d.ERROR;
        if (!tj7.b(sb2)) {
            sb2 = "Unknown error";
        }
        i(dVar, str, sb2, null, new Object[0]);
    }

    @DexIgnore
    public static void e(String str, String str2, Throwable th, Object... objArr) {
        i(d.ERROR, str, str2, th, objArr);
    }

    @DexIgnore
    public static void f(String str, String str2, Object... objArr) {
        i(d.ERROR, str, str2, null, objArr);
    }

    @DexIgnore
    public static void g(String str, String str2, Object... objArr) {
        i(d.INFO, str, str2, null, objArr);
    }

    @DexIgnore
    public static boolean h() {
        return d;
    }

    @DexIgnore
    public static void i(d dVar, String str, String str2, Throwable th, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str2 = String.format(Locale.US, str2, objArr);
        }
        if (d) {
            c.a(dVar, str, str2, th);
            for (c cVar : b) {
                cVar.a(dVar, str, str2, th);
            }
        }
    }

    @DexIgnore
    public static void j(boolean z) {
        d = z;
    }

    @DexIgnore
    public static void k(String str, String str2, Throwable th, Object... objArr) {
        i(d.WARN, str, str2, th, objArr);
    }

    @DexIgnore
    public static void l(String str, String str2, Object... objArr) {
        i(d.WARN, str, str2, null, objArr);
    }
}
