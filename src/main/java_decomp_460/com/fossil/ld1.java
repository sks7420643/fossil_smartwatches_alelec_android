package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ld1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public boolean f2181a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Handler.Callback {
        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((id1) message.obj).b();
            return true;
        }
    }

    @DexIgnore
    public void a(id1<?> id1, boolean z) {
        synchronized (this) {
            if (this.f2181a || z) {
                this.b.obtainMessage(1, id1).sendToTarget();
            } else {
                this.f2181a = true;
                id1.b();
                this.f2181a = false;
            }
        }
    }
}
