package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o44<E> extends y24<E> {
    @DexIgnore
    public static /* final */ y24<Object> EMPTY; // = new o44(h44.f1427a);
    @DexIgnore
    public /* final */ transient Object[] b;

    @DexIgnore
    public o44(Object[] objArr) {
        this.b = objArr;
    }

    @DexIgnore
    @Override // com.fossil.u24, com.fossil.y24
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.b;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return this.b.length + i;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return (E) this.b[i];
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.y24, com.fossil.y24
    public i54<E> listIterator(int i) {
        Object[] objArr = this.b;
        return p34.m(objArr, 0, objArr.length, i);
    }

    @DexIgnore
    public int size() {
        return this.b.length;
    }
}
