package com.fossil;

import com.fossil.dl7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e08 {
    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> void a(rp7<? super qn7<? super T>, ? extends Object> rp7, qn7<? super T> qn7) {
        go7.a(qn7);
        try {
            tn7 context = qn7.getContext();
            Object c = zz7.c(context, null);
            if (rp7 != null) {
                try {
                    ir7.d(rp7, 1);
                    Object invoke = rp7.invoke(qn7);
                    zz7.a(context, c);
                    if (invoke != yn7.d()) {
                        dl7.a aVar = dl7.Companion;
                        qn7.resumeWith(dl7.m1constructorimpl(invoke));
                    }
                } catch (Throwable th) {
                    zz7.a(context, c);
                    throw th;
                }
            } else {
                throw new il7("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            }
        } catch (Throwable th2) {
            dl7.a aVar2 = dl7.Companion;
            qn7.resumeWith(dl7.m1constructorimpl(el7.a(th2)));
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <R, T> void b(vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7, R r, qn7<? super T> qn7) {
        go7.a(qn7);
        try {
            tn7 context = qn7.getContext();
            Object c = zz7.c(context, null);
            if (vp7 != null) {
                try {
                    ir7.d(vp7, 2);
                    Object invoke = vp7.invoke(r, qn7);
                    zz7.a(context, c);
                    if (invoke != yn7.d()) {
                        dl7.a aVar = dl7.Companion;
                        qn7.resumeWith(dl7.m1constructorimpl(invoke));
                    }
                } catch (Throwable th) {
                    zz7.a(context, c);
                    throw th;
                }
            } else {
                throw new il7("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            }
        } catch (Throwable th2) {
            dl7.a aVar2 = dl7.Companion;
            qn7.resumeWith(dl7.m1constructorimpl(el7.a(th2)));
        }
    }

    @DexIgnore
    public static final <T, R> Object c(tz7<? super T> tz7, R r, vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7) {
        Object vu7;
        tz7.v0();
        if (vp7 != null) {
            try {
                ir7.d(vp7, 2);
                vu7 = vp7.invoke(r, tz7);
            } catch (Throwable th) {
                vu7 = new vu7(th, false, 2, null);
            }
            if (vu7 == yn7.d()) {
                return yn7.d();
            }
            Object X = tz7.X(vu7);
            if (X == gx7.b) {
                return yn7.d();
            }
            if (!(X instanceof vu7)) {
                return gx7.h(X);
            }
            Throwable th2 = ((vu7) X).f3837a;
            qn7<T> qn7 = tz7.e;
            if (!nv7.d()) {
                throw th2;
            } else if (!(qn7 instanceof do7)) {
                throw th2;
            } else {
                throw uz7.a(th2, (do7) qn7);
            }
        } else {
            throw new il7("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        }
    }

    @DexIgnore
    public static final <T, R> Object d(tz7<? super T> tz7, R r, vp7<? super R, ? super qn7<? super T>, ? extends Object> vp7) {
        Object vu7;
        tz7.v0();
        if (vp7 != null) {
            try {
                ir7.d(vp7, 2);
                vu7 = vp7.invoke(r, tz7);
            } catch (Throwable th) {
                vu7 = new vu7(th, false, 2, null);
            }
            if (vu7 == yn7.d()) {
                return yn7.d();
            }
            Object X = tz7.X(vu7);
            if (X == gx7.b) {
                return yn7.d();
            }
            if (!(X instanceof vu7)) {
                return gx7.h(X);
            }
            vu7 vu72 = (vu7) X;
            Throwable th2 = vu72.f3837a;
            if (!(th2 instanceof zx7) || ((zx7) th2).coroutine != tz7) {
                Throwable th3 = vu72.f3837a;
                qn7<T> qn7 = tz7.e;
                if (!nv7.d()) {
                    throw th3;
                } else if (!(qn7 instanceof do7)) {
                    throw th3;
                } else {
                    throw uz7.a(th3, (do7) qn7);
                }
            } else if (!(vu7 instanceof vu7)) {
                return vu7;
            } else {
                Throwable th4 = ((vu7) vu7).f3837a;
                qn7<T> qn72 = tz7.e;
                if (!nv7.d()) {
                    throw th4;
                } else if (!(qn72 instanceof do7)) {
                    throw th4;
                } else {
                    throw uz7.a(th4, (do7) qn72);
                }
            }
        } else {
            throw new il7("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        }
    }
}
