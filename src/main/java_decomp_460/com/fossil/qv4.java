package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qv4 implements Factory<pv4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<hu4> f3031a;

    @DexIgnore
    public qv4(Provider<hu4> provider) {
        this.f3031a = provider;
    }

    @DexIgnore
    public static qv4 a(Provider<hu4> provider) {
        return new qv4(provider);
    }

    @DexIgnore
    public static pv4 c(hu4 hu4) {
        return new pv4(hu4);
    }

    @DexIgnore
    /* renamed from: b */
    public pv4 get() {
        return c(this.f3031a.get());
    }
}
