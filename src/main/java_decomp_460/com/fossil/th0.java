package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.fossil.nl0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class th0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3414a;
    @DexIgnore
    public /* final */ TypedArray b;
    @DexIgnore
    public TypedValue c;

    @DexIgnore
    public th0(Context context, TypedArray typedArray) {
        this.f3414a = context;
        this.b = typedArray;
    }

    @DexIgnore
    public static th0 t(Context context, int i, int[] iArr) {
        return new th0(context, context.obtainStyledAttributes(i, iArr));
    }

    @DexIgnore
    public static th0 u(Context context, AttributeSet attributeSet, int[] iArr) {
        return new th0(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    @DexIgnore
    public static th0 v(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new th0(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    @DexIgnore
    public boolean a(int i, boolean z) {
        return this.b.getBoolean(i, z);
    }

    @DexIgnore
    public int b(int i, int i2) {
        return this.b.getColor(i, i2);
    }

    @DexIgnore
    public ColorStateList c(int i) {
        int resourceId;
        ColorStateList c2;
        return (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0 || (c2 = gf0.c(this.f3414a, resourceId)) == null) ? this.b.getColorStateList(i) : c2;
    }

    @DexIgnore
    public float d(int i, float f) {
        return this.b.getDimension(i, f);
    }

    @DexIgnore
    public int e(int i, int i2) {
        return this.b.getDimensionPixelOffset(i, i2);
    }

    @DexIgnore
    public int f(int i, int i2) {
        return this.b.getDimensionPixelSize(i, i2);
    }

    @DexIgnore
    public Drawable g(int i) {
        int resourceId;
        return (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) ? this.b.getDrawable(i) : gf0.d(this.f3414a, resourceId);
    }

    @DexIgnore
    public Drawable h(int i) {
        int resourceId;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) {
            return null;
        }
        return ug0.b().d(this.f3414a, resourceId, true);
    }

    @DexIgnore
    public float i(int i, float f) {
        return this.b.getFloat(i, f);
    }

    @DexIgnore
    public Typeface j(int i, int i2, nl0.a aVar) {
        int resourceId = this.b.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.c == null) {
            this.c = new TypedValue();
        }
        return nl0.c(this.f3414a, resourceId, this.c, i2, aVar);
    }

    @DexIgnore
    public int k(int i, int i2) {
        return this.b.getInt(i, i2);
    }

    @DexIgnore
    public int l(int i, int i2) {
        return this.b.getInteger(i, i2);
    }

    @DexIgnore
    public int m(int i, int i2) {
        return this.b.getLayoutDimension(i, i2);
    }

    @DexIgnore
    public int n(int i, int i2) {
        return this.b.getResourceId(i, i2);
    }

    @DexIgnore
    public String o(int i) {
        return this.b.getString(i);
    }

    @DexIgnore
    public CharSequence p(int i) {
        return this.b.getText(i);
    }

    @DexIgnore
    public CharSequence[] q(int i) {
        return this.b.getTextArray(i);
    }

    @DexIgnore
    public TypedArray r() {
        return this.b;
    }

    @DexIgnore
    public boolean s(int i) {
        return this.b.hasValue(i);
    }

    @DexIgnore
    public void w() {
        this.b.recycle();
    }
}
