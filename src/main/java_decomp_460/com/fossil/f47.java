package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f47 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static f47 f1051a;

    @DexIgnore
    public static f47 d() {
        if (f1051a == null) {
            f1051a = new f47();
        }
        return f1051a;
    }

    @DexIgnore
    public List<ContactGroup> a(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingIncomingCall = mn5.p.a().d().getContactGroupsMatchingIncomingCall(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingIncomingCall == null ? new ArrayList() : contactGroupsMatchingIncomingCall;
    }

    @DexIgnore
    public List<ContactGroup> b(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingSms = mn5.p.a().d().getContactGroupsMatchingSms(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingSms == null ? new ArrayList() : contactGroupsMatchingSms;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0014 A[LOOP:0: B:3:0x0014->B:16:0x0014, LOOP_END, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.wearables.fsl.contact.Contact c(java.lang.String r5, int r6) {
        /*
            r4 = this;
            com.fossil.mn5$a r0 = com.fossil.mn5.p
            com.fossil.mn5 r0 = r0.a()
            com.fossil.wearables.fsl.contact.ContactProvider r0 = r0.d()
            java.util.List r0 = r0.getAllContactGroups(r6)
            if (r0 == 0) goto L_0x0045
            java.util.Iterator r1 = r0.iterator()
        L_0x0014:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0045
            java.lang.Object r0 = r1.next()
            com.fossil.wearables.fsl.contact.ContactGroup r0 = (com.fossil.wearables.fsl.contact.ContactGroup) r0
            java.util.List r0 = r0.getContacts()
            java.util.Iterator r2 = r0.iterator()
        L_0x0028:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0014
            java.lang.Object r0 = r2.next()
            com.fossil.wearables.fsl.contact.Contact r0 = (com.fossil.wearables.fsl.contact.Contact) r0
            java.lang.String r3 = r0.getFirstName()
            boolean r3 = android.text.TextUtils.equals(r5, r3)
            if (r3 != 0) goto L_0x0044
            boolean r3 = r0.containsPhoneNumber(r5)
            if (r3 == 0) goto L_0x0028
        L_0x0044:
            return r0
        L_0x0045:
            r0 = 0
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.f47.c(java.lang.String, int):com.fossil.wearables.fsl.contact.Contact");
    }
}
