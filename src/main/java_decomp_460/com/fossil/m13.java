package com.fossil;

import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m13<E> extends List<E>, RandomAccess {
    @DexIgnore
    m13<E> zza(int i);

    @DexIgnore
    boolean zza();

    @DexIgnore
    Object zzb();  // void declaration
}
