package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rf1<T> implements qb1<ImageDecoder.Source, T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ lg1 f3104a; // = lg1.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ImageDecoder.OnHeaderDecodedListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ int f3105a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ hb1 d;
        @DexIgnore
        public /* final */ /* synthetic */ fg1 e;
        @DexIgnore
        public /* final */ /* synthetic */ pb1 f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rf1$a$a")
        /* renamed from: com.fossil.rf1$a$a  reason: collision with other inner class name */
        public class C0206a implements ImageDecoder.OnPartialImageListener {
            @DexIgnore
            public C0206a(a aVar) {
            }

            @DexIgnore
            public boolean onPartialImage(ImageDecoder.DecodeException decodeException) {
                return false;
            }
        }

        @DexIgnore
        public a(int i, int i2, boolean z, hb1 hb1, fg1 fg1, pb1 pb1) {
            this.f3105a = i;
            this.b = i2;
            this.c = z;
            this.d = hb1;
            this.e = fg1;
            this.f = pb1;
        }

        @DexIgnore
        @SuppressLint({"Override"})
        public void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
            if (rf1.this.f3104a.c(this.f3105a, this.b, this.c, false)) {
                imageDecoder.setAllocator(3);
            } else {
                imageDecoder.setAllocator(1);
            }
            if (this.d == hb1.PREFER_RGB_565) {
                imageDecoder.setMemorySizePolicy(0);
            }
            imageDecoder.setOnPartialImageListener(new C0206a(this));
            Size size = imageInfo.getSize();
            int i = this.f3105a;
            if (i == Integer.MIN_VALUE) {
                i = size.getWidth();
            }
            int i2 = this.b;
            if (i2 == Integer.MIN_VALUE) {
                i2 = size.getHeight();
            }
            float b2 = this.e.b(size.getWidth(), size.getHeight(), i, i2);
            int round = Math.round(((float) size.getWidth()) * b2);
            int round2 = Math.round(((float) size.getHeight()) * b2);
            if (Log.isLoggable("ImageDecoder", 2)) {
                Log.v("ImageDecoder", "Resizing from [" + size.getWidth() + "x" + size.getHeight() + "] to [" + round + "x" + round2 + "] scaleFactor: " + b2);
            }
            imageDecoder.setTargetSize(round, round2);
            int i3 = Build.VERSION.SDK_INT;
            if (i3 >= 28) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(this.f == pb1.DISPLAY_P3 && imageInfo.getColorSpace() != null && imageInfo.getColorSpace().isWideGamut() ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB));
            } else if (i3 >= 26) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
            }
        }
    }

    @DexIgnore
    public abstract id1<T> c(ImageDecoder.Source source, int i, int i2, ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws IOException;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: com.fossil.ob1 */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: d */
    public final id1<T> b(ImageDecoder.Source source, int i, int i2, ob1 ob1) throws IOException {
        return c(source, i, i2, new a(i, i2, ob1.c(gg1.i) != null && ((Boolean) ob1.c(gg1.i)).booleanValue(), (hb1) ob1.c(gg1.f), (fg1) ob1.c(fg1.f), (pb1) ob1.c(gg1.g)));
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean a(ImageDecoder.Source source, ob1 ob1) {
        return true;
    }
}
