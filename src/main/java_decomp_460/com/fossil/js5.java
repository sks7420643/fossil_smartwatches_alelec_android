package com.fossil;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js5 extends Transition {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ String[] c; // = {"transition:textResize"};
    @DexIgnore
    public static /* final */ a d; // = new a(Float.TYPE, "textSize");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Property<TextView, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(TextView textView) {
            pq7.c(textView, "textView");
            return Float.valueOf(textView.getTextSize());
        }

        @DexIgnore
        /* renamed from: b */
        public void set(TextView textView, Float f) {
            pq7.c(textView, "textView");
            if (f != null) {
                textView.setTextSize(0, f.floatValue());
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = js5.class.getSimpleName();
        pq7.b(simpleName, "TextResizeTransition::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public void captureEndValues(TransitionValues transitionValues) {
        pq7.c(transitionValues, "transitionValues");
        FLogger.INSTANCE.getLocal().d(b, "captureEndValues()");
        d(transitionValues);
    }

    @DexIgnore
    public void captureStartValues(TransitionValues transitionValues) {
        pq7.c(transitionValues, "transitionValues");
        FLogger.INSTANCE.getLocal().d(b, "captureStartValues()");
        d(transitionValues);
    }

    @DexIgnore
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        pq7.c(viewGroup, "sceneRoot");
        if (transitionValues == null || transitionValues2 == null || transitionValues.values.get("transition:textResize") == null || transitionValues2.values.get("transition:textResize") == null) {
            return null;
        }
        Object obj = transitionValues.values.get("transition:textResize");
        if (obj != null) {
            float floatValue = ((Float) obj).floatValue();
            Object obj2 = transitionValues2.values.get("transition:textResize");
            if (obj2 != null) {
                float floatValue2 = ((Float) obj2).floatValue();
                if (floatValue == floatValue2) {
                    return null;
                }
                View view = transitionValues2.view;
                if (view != null) {
                    TextView textView = (TextView) view;
                    textView.setTextSize(0, floatValue2);
                    return ObjectAnimator.ofFloat(textView, d, floatValue, floatValue2);
                }
                throw new il7("null cannot be cast to non-null type android.widget.TextView");
            }
            throw new il7("null cannot be cast to non-null type kotlin.Float");
        }
        throw new il7("null cannot be cast to non-null type kotlin.Float");
    }

    @DexIgnore
    public final void d(TransitionValues transitionValues) {
        View view = transitionValues.view;
        if (!(view instanceof TextView)) {
            return;
        }
        if (view != null) {
            TextView textView = (TextView) view;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            local.d(str, "captureValues textView.textSize = " + textView.getTextSize());
            Map map = transitionValues.values;
            pq7.b(map, "transitionValues.values");
            map.put("transition:textResize", Float.valueOf(textView.getTextSize()));
            return;
        }
        throw new il7("null cannot be cast to non-null type android.widget.TextView");
    }

    @DexIgnore
    public String[] getTransitionProperties() {
        return c;
    }
}
