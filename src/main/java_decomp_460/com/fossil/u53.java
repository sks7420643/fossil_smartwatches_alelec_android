package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u53 implements r53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f3516a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;
    @DexIgnore
    public static /* final */ xv2<Boolean> c;
    @DexIgnore
    public static /* final */ xv2<Boolean> d;
    @DexIgnore
    public static /* final */ xv2<Boolean> e;
    @DexIgnore
    public static /* final */ xv2<Boolean> f;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f3516a = hw2.d("measurement.gold.enhanced_ecommerce.format_logs", true);
        b = hw2.d("measurement.gold.enhanced_ecommerce.log_nested_complex_events", true);
        c = hw2.d("measurement.gold.enhanced_ecommerce.nested_param_daily_event_count", true);
        d = hw2.d("measurement.gold.enhanced_ecommerce.updated_schema.client", true);
        e = hw2.d("measurement.gold.enhanced_ecommerce.updated_schema.service", true);
        f = hw2.d("measurement.gold.enhanced_ecommerce.upload_nested_complex_events", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zzb() {
        return f3516a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zzc() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zzd() {
        return c.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zze() {
        return d.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zzf() {
        return e.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.r53
    public final boolean zzg() {
        return f.o().booleanValue();
    }
}
