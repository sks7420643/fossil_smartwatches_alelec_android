package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import com.fossil.ld7;
import com.fossil.rd7;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xc7 implements Runnable {
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> A; // = new a();
    @DexIgnore
    public static /* final */ AtomicInteger B; // = new AtomicInteger();
    @DexIgnore
    public static /* final */ rd7 C; // = new b();
    @DexIgnore
    public static /* final */ Object z; // = new Object();
    @DexIgnore
    public /* final */ int b; // = B.incrementAndGet();
    @DexIgnore
    public /* final */ Picasso c;
    @DexIgnore
    public /* final */ dd7 d;
    @DexIgnore
    public /* final */ yc7 e;
    @DexIgnore
    public /* final */ td7 f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ pd7 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ rd7 k;
    @DexIgnore
    public vc7 l;
    @DexIgnore
    public List<vc7> m;
    @DexIgnore
    public Bitmap s;
    @DexIgnore
    public Future<?> t;
    @DexIgnore
    public Picasso.LoadedFrom u;
    @DexIgnore
    public Exception v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public Picasso.e y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        /* renamed from: a */
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends rd7 {
        @DexIgnore
        @Override // com.fossil.rd7
        public boolean c(pd7 pd7) {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.rd7
        public rd7.a f(pd7 pd7, int i) throws IOException {
            throw new IllegalStateException("Unrecognized type of request: " + pd7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation b;
        @DexIgnore
        public /* final */ /* synthetic */ RuntimeException c;

        @DexIgnore
        public c(Transformation transformation, RuntimeException runtimeException) {
            this.b = transformation;
            this.c = runtimeException;
        }

        @DexIgnore
        public void run() {
            throw new RuntimeException("Transformation " + this.b.key() + " crashed with exception.", this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder b;

        @DexIgnore
        public d(StringBuilder sb) {
            this.b = sb;
        }

        @DexIgnore
        public void run() {
            throw new NullPointerException(this.b.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation b;

        @DexIgnore
        public e(Transformation transformation) {
            this.b = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.b.key() + " returned input Bitmap but recycled it.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation b;

        @DexIgnore
        public f(Transformation transformation) {
            this.b = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.b.key() + " mutated input Bitmap but failed to recycle the original.");
        }
    }

    @DexIgnore
    public xc7(Picasso picasso, dd7 dd7, yc7 yc7, td7 td7, vc7 vc7, rd7 rd7) {
        this.c = picasso;
        this.d = dd7;
        this.e = yc7;
        this.f = td7;
        this.l = vc7;
        this.g = vc7.d();
        this.h = vc7.i();
        this.y = vc7.h();
        this.i = vc7.e();
        this.j = vc7.f();
        this.k = rd7;
        this.x = rd7.e();
    }

    @DexIgnore
    public static Bitmap a(List<Transformation> list, Bitmap bitmap) {
        int size = list.size();
        int i2 = 0;
        Bitmap bitmap2 = bitmap;
        while (i2 < size) {
            Transformation transformation = list.get(i2);
            try {
                Bitmap transform = transformation.transform(bitmap2);
                if (transform == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Transformation ");
                    sb.append(transformation.key());
                    sb.append(" returned null after ");
                    sb.append(i2);
                    sb.append(" previous transformation(s).\n\nTransformation list:\n");
                    for (Transformation transformation2 : list) {
                        sb.append(transformation2.key());
                        sb.append('\n');
                    }
                    Picasso.p.post(new d(sb));
                    return null;
                } else if (transform == bitmap2 && bitmap2.isRecycled()) {
                    Picasso.p.post(new e(transformation));
                    return null;
                } else if (transform == bitmap2 || bitmap2.isRecycled()) {
                    i2++;
                    bitmap2 = transform;
                } else {
                    Picasso.p.post(new f(transformation));
                    return null;
                }
            } catch (RuntimeException e2) {
                Picasso.p.post(new c(transformation, e2));
                return null;
            }
        }
        return bitmap2;
    }

    @DexIgnore
    public static Bitmap e(InputStream inputStream, pd7 pd7) throws IOException {
        hd7 hd7 = new hd7(inputStream);
        long b2 = hd7.b(65536);
        BitmapFactory.Options d2 = rd7.d(pd7);
        boolean g2 = rd7.g(d2);
        boolean t2 = xd7.t(hd7);
        hd7.a(b2);
        if (t2) {
            byte[] x2 = xd7.x(hd7);
            if (g2) {
                BitmapFactory.decodeByteArray(x2, 0, x2.length, d2);
                rd7.b(pd7.h, pd7.i, d2, pd7);
            }
            return BitmapFactory.decodeByteArray(x2, 0, x2.length, d2);
        }
        if (g2) {
            BitmapFactory.decodeStream(hd7, null, d2);
            rd7.b(pd7.h, pd7.i, d2, pd7);
            hd7.a(b2);
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(hd7, null, d2);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    @DexIgnore
    public static xc7 g(Picasso picasso, dd7 dd7, yc7 yc7, td7 td7, vc7 vc7) {
        pd7 i2 = vc7.i();
        List<rd7> i3 = picasso.i();
        int size = i3.size();
        for (int i4 = 0; i4 < size; i4++) {
            rd7 rd7 = i3.get(i4);
            if (rd7.c(i2)) {
                return new xc7(picasso, dd7, yc7, td7, vc7, rd7);
            }
        }
        return new xc7(picasso, dd7, yc7, td7, vc7, C);
    }

    @DexIgnore
    public static boolean t(boolean z2, int i2, int i3, int i4, int i5) {
        return !z2 || i2 > i4 || i3 > i5;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap w(com.fossil.pd7 r16, android.graphics.Bitmap r17, int r18) {
        /*
        // Method dump skipped, instructions count: 223
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xc7.w(com.fossil.pd7, android.graphics.Bitmap, int):android.graphics.Bitmap");
    }

    @DexIgnore
    public static void x(pd7 pd7) {
        String a2 = pd7.a();
        StringBuilder sb = A.get();
        sb.ensureCapacity(a2.length() + 8);
        sb.replace(8, sb.length(), a2);
        Thread.currentThread().setName(sb.toString());
    }

    @DexIgnore
    public void b(vc7 vc7) {
        boolean z2 = this.c.n;
        pd7 pd7 = vc7.b;
        if (this.l == null) {
            this.l = vc7;
            if (z2) {
                List<vc7> list = this.m;
                if (list == null || list.isEmpty()) {
                    xd7.v("Hunter", "joined", pd7.d(), "to empty hunter");
                } else {
                    xd7.v("Hunter", "joined", pd7.d(), xd7.m(this, "to "));
                }
            }
        } else {
            if (this.m == null) {
                this.m = new ArrayList(3);
            }
            this.m.add(vc7);
            if (z2) {
                xd7.v("Hunter", "joined", pd7.d(), xd7.m(this, "to "));
            }
            Picasso.e h2 = vc7.h();
            if (h2.ordinal() > this.y.ordinal()) {
                this.y = h2;
            }
        }
    }

    @DexIgnore
    public boolean c() {
        Future<?> future;
        if (this.l != null) {
            return false;
        }
        List<vc7> list = this.m;
        return (list == null || list.isEmpty()) && (future = this.t) != null && future.cancel(false);
    }

    @DexIgnore
    public final Picasso.e d() {
        boolean z2 = true;
        int i2 = 0;
        Picasso.e eVar = Picasso.e.LOW;
        List<vc7> list = this.m;
        boolean z3 = list != null && !list.isEmpty();
        if (this.l == null && !z3) {
            z2 = false;
        }
        if (!z2) {
            return eVar;
        }
        vc7 vc7 = this.l;
        Picasso.e h2 = vc7 != null ? vc7.h() : eVar;
        if (!z3) {
            return h2;
        }
        int size = this.m.size();
        while (i2 < size) {
            Picasso.e h3 = this.m.get(i2).h();
            if (h3.ordinal() <= h2.ordinal()) {
                h3 = h2;
            }
            i2++;
            h2 = h3;
        }
        return h2;
    }

    @DexIgnore
    public void f(vc7 vc7) {
        boolean remove;
        if (this.l == vc7) {
            this.l = null;
            remove = true;
        } else {
            List<vc7> list = this.m;
            remove = list != null ? list.remove(vc7) : false;
        }
        if (remove && vc7.h() == this.y) {
            this.y = d();
        }
        if (this.c.n) {
            xd7.v("Hunter", "removed", vc7.b.d(), xd7.m(this, "from "));
        }
    }

    @DexIgnore
    public vc7 h() {
        return this.l;
    }

    @DexIgnore
    public List<vc7> i() {
        return this.m;
    }

    @DexIgnore
    public pd7 j() {
        return this.h;
    }

    @DexIgnore
    public Exception k() {
        return this.v;
    }

    @DexIgnore
    public String l() {
        return this.g;
    }

    @DexIgnore
    public Picasso.LoadedFrom m() {
        return this.u;
    }

    @DexIgnore
    public int n() {
        return this.i;
    }

    @DexIgnore
    public Picasso o() {
        return this.c;
    }

    @DexIgnore
    public Picasso.e p() {
        return this.y;
    }

    @DexIgnore
    public Bitmap q() {
        return this.s;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap r() throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xc7.r():android.graphics.Bitmap");
    }

    @DexIgnore
    public void run() {
        try {
            x(this.h);
            if (this.c.n) {
                xd7.u("Hunter", "executing", xd7.l(this));
            }
            Bitmap r = r();
            this.s = r;
            if (r == null) {
                this.d.e(this);
            } else {
                this.d.d(this);
            }
        } catch (Downloader.a e2) {
            if (!e2.localCacheOnly || e2.responseCode != 504) {
                this.v = e2;
            }
            this.d.e(this);
        } catch (ld7.a e3) {
            this.v = e3;
            this.d.g(this);
        } catch (IOException e4) {
            this.v = e4;
            this.d.g(this);
        } catch (OutOfMemoryError e5) {
            StringWriter stringWriter = new StringWriter();
            this.f.a().a(new PrintWriter(stringWriter));
            this.v = new RuntimeException(stringWriter.toString(), e5);
            this.d.e(this);
        } catch (Exception e6) {
            this.v = e6;
            this.d.e(this);
        } catch (Throwable th) {
            Thread.currentThread().setName("Picasso-Idle");
            throw th;
        }
        Thread.currentThread().setName("Picasso-Idle");
    }

    @DexIgnore
    public boolean s() {
        Future<?> future = this.t;
        return future != null && future.isCancelled();
    }

    @DexIgnore
    public boolean u(boolean z2, NetworkInfo networkInfo) {
        if (!(this.x > 0)) {
            return false;
        }
        this.x--;
        return this.k.h(z2, networkInfo);
    }

    @DexIgnore
    public boolean v() {
        return this.k.i();
    }
}
