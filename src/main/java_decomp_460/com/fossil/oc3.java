package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oc3 extends ds2 implements nc3 {
    @DexIgnore
    public oc3() {
        super("com.google.android.gms.maps.internal.IOnMapLongClickListener");
    }

    @DexIgnore
    @Override // com.fossil.ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onMapLongClick((LatLng) es2.b(parcel, LatLng.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
