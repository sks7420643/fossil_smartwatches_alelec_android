package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n31 implements m31 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f2458a;
    @DexIgnore
    public /* final */ xw0 b;
    @DexIgnore
    public /* final */ xw0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<l31> {
        @DexIgnore
        public a(n31 n31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, l31 l31) {
            String str = l31.f2141a;
            if (str == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, str);
            }
            byte[] k = r01.k(l31.b);
            if (k == null) {
                px0.bindNull(2);
            } else {
                px0.bindBlob(2, k);
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xw0 {
        @DexIgnore
        public b(n31 n31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE from WorkProgress where work_spec_id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xw0 {
        @DexIgnore
        public c(n31 n31, qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "DELETE FROM WorkProgress";
        }
    }

    @DexIgnore
    public n31(qw0 qw0) {
        this.f2458a = qw0;
        new a(this, qw0);
        this.b = new b(this, qw0);
        this.c = new c(this, qw0);
    }

    @DexIgnore
    @Override // com.fossil.m31
    public void b(String str) {
        this.f2458a.assertNotSuspendingTransaction();
        px0 acquire = this.b.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.f2458a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f2458a.setTransactionSuccessful();
        } finally {
            this.f2458a.endTransaction();
            this.b.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.m31
    public void deleteAll() {
        this.f2458a.assertNotSuspendingTransaction();
        px0 acquire = this.c.acquire();
        this.f2458a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.f2458a.setTransactionSuccessful();
        } finally {
            this.f2458a.endTransaction();
            this.c.release(acquire);
        }
    }
}
