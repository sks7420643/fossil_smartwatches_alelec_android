package com.fossil;

import android.annotation.SuppressLint;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import com.fossil.o71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"MissingPermission"})
public final class q71 implements o71 {
    @DexIgnore
    public static /* final */ NetworkRequest e; // = new NetworkRequest.Builder().addCapability(12).build();
    @DexIgnore
    public /* final */ a b; // = new a(this);
    @DexIgnore
    public /* final */ ConnectivityManager c;
    @DexIgnore
    public /* final */ o71.b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ConnectivityManager.NetworkCallback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ q71 f2932a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(q71 q71) {
            this.f2932a = q71;
        }

        @DexIgnore
        public void onAvailable(Network network) {
            pq7.c(network, "network");
            this.f2932a.d(network, true);
        }

        @DexIgnore
        public void onLost(Network network) {
            pq7.c(network, "network");
            this.f2932a.d(network, false);
        }
    }

    @DexIgnore
    public q71(ConnectivityManager connectivityManager, o71.b bVar) {
        pq7.c(connectivityManager, "connectivityManager");
        pq7.c(bVar, "listener");
        this.c = connectivityManager;
        this.d = bVar;
    }

    @DexIgnore
    @Override // com.fossil.o71
    public boolean a() {
        Network[] allNetworks = this.c.getAllNetworks();
        pq7.b(allNetworks, "connectivityManager.allNetworks");
        for (Network network : allNetworks) {
            pq7.b(network, "it");
            if (c(network)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean c(Network network) {
        NetworkCapabilities networkCapabilities = this.c.getNetworkCapabilities(network);
        return networkCapabilities != null && networkCapabilities.hasCapability(12);
    }

    @DexIgnore
    public final void d(Network network, boolean z) {
        boolean c2;
        boolean z2 = false;
        Network[] allNetworks = this.c.getAllNetworks();
        pq7.b(allNetworks, "connectivityManager.allNetworks");
        int length = allNetworks.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Network network2 = allNetworks[i];
            if (pq7.a(network2, network)) {
                c2 = z;
            } else {
                pq7.b(network2, "it");
                c2 = c(network2);
            }
            if (c2) {
                z2 = true;
                break;
            }
            i++;
        }
        this.d.a(z2);
    }

    @DexIgnore
    @Override // com.fossil.o71
    public void start() {
        this.c.registerNetworkCallback(e, this.b);
    }

    @DexIgnore
    @Override // com.fossil.o71
    public void stop() {
        this.c.unregisterNetworkCallback(this.b);
    }
}
