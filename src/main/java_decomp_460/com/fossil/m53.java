package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m53 implements xw2<l53> {
    @DexIgnore
    public static m53 c; // = new m53();
    @DexIgnore
    public /* final */ xw2<l53> b;

    @DexIgnore
    public m53() {
        this(ww2.b(new o53()));
    }

    @DexIgnore
    public m53(xw2<l53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((l53) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((l53) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ l53 zza() {
        return this.b.zza();
    }
}
