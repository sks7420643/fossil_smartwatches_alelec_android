package com.fossil;

import com.fossil.lz7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wy7<E> {
    @DexIgnore
    Object a();

    @DexIgnore
    void d(E e);

    @DexIgnore
    vz7 e(E e, lz7.c cVar);
}
