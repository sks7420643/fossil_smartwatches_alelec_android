package com.fossil;

import android.graphics.Bitmap;
import com.fossil.bb1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh1 implements bb1.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rd1 f1305a;
    @DexIgnore
    public /* final */ od1 b;

    @DexIgnore
    public gh1(rd1 rd1, od1 od1) {
        this.f1305a = rd1;
        this.b = od1;
    }

    @DexIgnore
    @Override // com.fossil.bb1.a
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.f1305a.e(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.bb1.a
    public int[] b(int i) {
        od1 od1 = this.b;
        return od1 == null ? new int[i] : (int[]) od1.g(i, int[].class);
    }

    @DexIgnore
    @Override // com.fossil.bb1.a
    public void c(Bitmap bitmap) {
        this.f1305a.b(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.bb1.a
    public void d(byte[] bArr) {
        od1 od1 = this.b;
        if (od1 != null) {
            od1.f(bArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.bb1.a
    public byte[] e(int i) {
        od1 od1 = this.b;
        return od1 == null ? new byte[i] : (byte[]) od1.g(i, byte[].class);
    }

    @DexIgnore
    @Override // com.fossil.bb1.a
    public void f(int[] iArr) {
        od1 od1 = this.b;
        if (od1 != null) {
            od1.f(iArr);
        }
    }
}
