package com.fossil;

import android.animation.TypeEvaluator;
import android.graphics.Rect;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oy0 implements TypeEvaluator<Rect> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Rect f2749a;

    @DexIgnore
    public oy0() {
    }

    @DexIgnore
    public oy0(Rect rect) {
        this.f2749a = rect;
    }

    @DexIgnore
    /* renamed from: a */
    public Rect evaluate(float f, Rect rect, Rect rect2) {
        int i = rect.left;
        int i2 = ((int) (((float) (rect2.left - i)) * f)) + i;
        int i3 = rect.top;
        int i4 = ((int) (((float) (rect2.top - i3)) * f)) + i3;
        int i5 = rect.right;
        int i6 = ((int) (((float) (rect2.right - i5)) * f)) + i5;
        int i7 = rect.bottom;
        int i8 = ((int) (((float) (rect2.bottom - i7)) * f)) + i7;
        Rect rect3 = this.f2749a;
        if (rect3 == null) {
            return new Rect(i2, i4, i6, i8);
        }
        rect3.set(i2, i4, i6, i8);
        return this.f2749a;
    }
}
