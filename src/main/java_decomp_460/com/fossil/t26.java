package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t26 implements Factory<s26> {
    @DexIgnore
    public static s26 a(q26 q26, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new s26(q26, remindersSettingsDatabase);
    }
}
