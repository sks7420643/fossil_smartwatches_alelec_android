package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f93 implements xw2<d93> {
    @DexIgnore
    public static f93 c; // = new f93();
    @DexIgnore
    public /* final */ xw2<d93> b;

    @DexIgnore
    public f93() {
        this(ww2.b(new h93()));
    }

    @DexIgnore
    public f93(xw2<d93> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((d93) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((d93) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ d93 zza() {
        return this.b.zza();
    }
}
