package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt4 {
    @rj4("challenge")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ps4 f2414a;
    @DexIgnore
    @rj4("category")
    public String b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final ps4 b() {
        return this.f2414a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public final void d(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof mt4) {
                mt4 mt4 = (mt4) obj;
                if (!pq7.a(this.f2414a, mt4.f2414a) || !pq7.a(this.b, mt4.b) || this.c != mt4.c) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        ps4 ps4 = this.f2414a;
        int hashCode = ps4 != null ? ps4.hashCode() : 0;
        String str = this.b;
        if (str != null) {
            i = str.hashCode();
        }
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "Recommended(challenge=" + this.f2414a + ", category=" + this.b + ", isNew=" + this.c + ")";
    }
}
