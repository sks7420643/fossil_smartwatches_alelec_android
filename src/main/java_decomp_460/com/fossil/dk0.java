package com.fossil;

import com.fossil.tj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dk0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f795a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public ArrayList<a> e; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public tj0 f796a;
        @DexIgnore
        public tj0 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public tj0.c d;
        @DexIgnore
        public int e;

        @DexIgnore
        public a(tj0 tj0) {
            this.f796a = tj0;
            this.b = tj0.i();
            this.c = tj0.d();
            this.d = tj0.h();
            this.e = tj0.c();
        }

        @DexIgnore
        public void a(uj0 uj0) {
            uj0.h(this.f796a.j()).b(this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        public void b(uj0 uj0) {
            tj0 h = uj0.h(this.f796a.j());
            this.f796a = h;
            if (h != null) {
                this.b = h.i();
                this.c = this.f796a.d();
                this.d = this.f796a.h();
                this.e = this.f796a.c();
                return;
            }
            this.b = null;
            this.c = 0;
            this.d = tj0.c.STRONG;
            this.e = 0;
        }
    }

    @DexIgnore
    public dk0(uj0 uj0) {
        this.f795a = uj0.G();
        this.b = uj0.H();
        this.c = uj0.D();
        this.d = uj0.r();
        ArrayList<tj0> i = uj0.i();
        int size = i.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.e.add(new a(i.get(i2)));
        }
    }

    @DexIgnore
    public void a(uj0 uj0) {
        uj0.C0(this.f795a);
        uj0.D0(this.b);
        uj0.y0(this.c);
        uj0.b0(this.d);
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(uj0);
        }
    }

    @DexIgnore
    public void b(uj0 uj0) {
        this.f795a = uj0.G();
        this.b = uj0.H();
        this.c = uj0.D();
        this.d = uj0.r();
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).b(uj0);
        }
    }
}
