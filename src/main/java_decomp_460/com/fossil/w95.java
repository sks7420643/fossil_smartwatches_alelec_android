package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w95 extends v95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362090, 1);
        F.put(2131362263, 2);
        F.put(2131362546, 3);
        F.put(2131362632, 4);
        F.put(2131363525, 5);
        F.put(2131362545, 6);
        F.put(2131362036, 7);
        F.put(2131362715, 8);
        F.put(2131362290, 9);
        F.put(2131362433, 10);
        F.put(2131362451, 11);
        F.put(2131362548, 12);
    }
    */

    @DexIgnore
    public w95(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 13, E, F));
    }

    @DexIgnore
    public w95(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[1], (FlexibleButton) objArr[2], (FlexibleButton) objArr[9], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[12], (TabLayout) objArr[4], (ImageView) objArr[8], (ConstraintLayout) objArr[0], (ViewPager2) objArr[5]);
        this.D = -1;
        this.B.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.D != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.D = 1;
        }
        w();
    }
}
