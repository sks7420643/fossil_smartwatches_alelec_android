package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3861a;

    @DexIgnore
    public vz7(String str) {
        this.f3861a = str;
    }

    @DexIgnore
    public String toString() {
        return this.f3861a;
    }
}
