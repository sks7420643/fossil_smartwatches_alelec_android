package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wu0 implements jv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jv0 f3997a;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public Object e; // = null;

    @DexIgnore
    public wu0(jv0 jv0) {
        this.f3997a = jv0;
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void a(int i, int i2) {
        e();
        this.f3997a.a(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void b(int i, int i2) {
        int i3;
        if (this.b == 1 && i >= (i3 = this.c)) {
            int i4 = this.d;
            if (i <= i3 + i4) {
                this.d = i4 + i2;
                this.c = Math.min(i, i3);
                return;
            }
        }
        e();
        this.c = i;
        this.d = i2;
        this.b = 1;
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void c(int i, int i2) {
        int i3;
        if (this.b != 2 || (i3 = this.c) < i || i3 > i + i2) {
            e();
            this.c = i;
            this.d = i2;
            this.b = 2;
            return;
        }
        this.d += i2;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.jv0
    public void d(int i, int i2, Object obj) {
        int i3;
        if (this.b == 3) {
            int i4 = this.c;
            int i5 = this.d;
            if (i <= i4 + i5 && (i3 = i + i2) >= i4 && this.e == obj) {
                this.c = Math.min(i, i4);
                this.d = Math.max(i4 + i5, i3) - this.c;
                return;
            }
        }
        e();
        this.c = i;
        this.d = i2;
        this.e = obj;
        this.b = 3;
    }

    @DexIgnore
    public void e() {
        int i = this.b;
        if (i != 0) {
            if (i == 1) {
                this.f3997a.b(this.c, this.d);
            } else if (i == 2) {
                this.f3997a.c(this.c, this.d);
            } else if (i == 3) {
                this.f3997a.d(this.c, this.d, this.e);
            }
            this.e = null;
            this.b = 0;
        }
    }
}
