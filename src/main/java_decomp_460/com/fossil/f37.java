package com.fossil;

import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class f37 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AppNotificationFilterSettings b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;

    @DexIgnore
    public /* synthetic */ f37(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        this.b = appNotificationFilterSettings;
        this.c = str;
    }

    @DexIgnore
    public final void run() {
        PortfolioApp.d0.s1(this.b, this.c);
    }
}
