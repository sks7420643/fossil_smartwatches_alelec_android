package com.fossil;

import android.annotation.SuppressLint;
import android.view.View;
import io.flutter.plugin.platform.PlatformView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class qk7 {
    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void $default$onFlutterViewAttached(PlatformView platformView, View view) {
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void $default$onFlutterViewDetached(PlatformView platformView) {
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void $default$onInputConnectionLocked(PlatformView platformView) {
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void $default$onInputConnectionUnlocked(PlatformView platformView) {
    }
}
