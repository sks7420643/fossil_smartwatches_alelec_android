package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vg4 extends k64 {
    @DexIgnore
    public /* final */ a status;

    @DexIgnore
    public enum a {
        BAD_CONFIG
    }

    @DexIgnore
    public vg4(a aVar) {
        this.status = aVar;
    }

    @DexIgnore
    public vg4(String str, a aVar) {
        super(str);
        this.status = aVar;
    }

    @DexIgnore
    public vg4(String str, a aVar, Throwable th) {
        super(str, th);
        this.status = aVar;
    }

    @DexIgnore
    public a getStatus() {
        return this.status;
    }
}
