package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface da3 {
    @DexIgnore
    t62<Status> a(r62 r62, ga3 ga3);

    @DexIgnore
    t62<Status> b(r62 r62, LocationRequest locationRequest, ga3 ga3);
}
