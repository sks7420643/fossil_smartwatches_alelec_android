package com.fossil;

import com.fossil.ks7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yq7 extends gq7 implements ks7 {
    @DexIgnore
    public yq7() {
    }

    @DexIgnore
    public yq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof yq7) {
            yq7 yq7 = (yq7) obj;
            return getOwner().equals(yq7.getOwner()) && getName().equals(yq7.getName()) && getSignature().equals(yq7.getSignature()) && pq7.a(getBoundReceiver(), yq7.getBoundReceiver());
        } else if (obj instanceof ks7) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    public abstract /* synthetic */ ks7.a<R> getGetter();

    @DexIgnore
    @Override // com.fossil.gq7
    public ks7 getReflected() {
        return (ks7) super.getReflected();
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ks7
    public boolean isConst() {
        return getReflected().isConst();
    }

    @DexIgnore
    @Override // com.fossil.ks7
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    @DexIgnore
    public String toString() {
        ds7 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }
}
