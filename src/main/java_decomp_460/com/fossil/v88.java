package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v88<T> implements e88<T, RequestBody> {
    @DexIgnore
    public static /* final */ r18 c; // = r18.c("application/json; charset=UTF-8");
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f3735a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public v88(Gson gson, TypeAdapter<T> typeAdapter) {
        this.f3735a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    /* renamed from: b */
    public RequestBody a(T t) throws IOException {
        i48 i48 = new i48();
        JsonWriter r = this.f3735a.r(new OutputStreamWriter(i48.l0(), d));
        this.b.write(r, t);
        r.close();
        return RequestBody.e(c, i48.S());
    }
}
