package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lg6 implements MembersInjector<CaloriesOverviewFragment> {
    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, ig6 ig6) {
        caloriesOverviewFragment.h = ig6;
    }

    @DexIgnore
    public static void b(CaloriesOverviewFragment caloriesOverviewFragment, tg6 tg6) {
        caloriesOverviewFragment.j = tg6;
    }

    @DexIgnore
    public static void c(CaloriesOverviewFragment caloriesOverviewFragment, zg6 zg6) {
        caloriesOverviewFragment.i = zg6;
    }
}
