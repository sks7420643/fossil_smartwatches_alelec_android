package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl7 implements Collection<ql7>, jr7 {
    @DexIgnore
    public /* final */ short[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends jn7 {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ short[] c;

        @DexIgnore
        public a(short[] sArr) {
            pq7.c(sArr, "array");
            this.c = sArr;
        }

        @DexIgnore
        @Override // com.fossil.jn7
        public short b() {
            int i = this.b;
            short[] sArr = this.c;
            if (i < sArr.length) {
                this.b = i + 1;
                short s = sArr[i];
                ql7.e(s);
                return s;
            }
            throw new NoSuchElementException(String.valueOf(this.b));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.length;
        }
    }

    @DexIgnore
    public static boolean b(short[] sArr, short s) {
        return em7.C(sArr, s);
    }

    @DexIgnore
    public static boolean c(short[] sArr, Collection<ql7> collection) {
        boolean z;
        pq7.c(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof ql7) || !em7.C(sArr, t.j())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean e(short[] sArr, Object obj) {
        return (obj instanceof rl7) && pq7.a(sArr, ((rl7) obj).m());
    }

    @DexIgnore
    public static int g(short[] sArr) {
        return sArr.length;
    }

    @DexIgnore
    public static int h(short[] sArr) {
        if (sArr != null) {
            return Arrays.hashCode(sArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean i(short[] sArr) {
        return sArr.length == 0;
    }

    @DexIgnore
    public static jn7 k(short[] sArr) {
        return new a(sArr);
    }

    @DexIgnore
    public static String l(short[] sArr) {
        return "UShortArray(storage=" + Arrays.toString(sArr) + ")";
    }

    @DexIgnore
    public boolean a(short s) {
        return b(this.b, s);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(ql7 ql7) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends ql7> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof ql7) {
            return a(((ql7) obj).j());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return c(this.b, collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return e(this.b, obj);
    }

    @DexIgnore
    public int f() {
        return g(this.b);
    }

    @DexIgnore
    public int hashCode() {
        return h(this.b);
    }

    @DexIgnore
    public boolean isEmpty() {
        return i(this.b);
    }

    @DexIgnore
    /* renamed from: j */
    public jn7 iterator() {
        return k(this.b);
    }

    @DexIgnore
    public final /* synthetic */ short[] m() {
        return this.b;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return f();
    }

    @DexIgnore
    public Object[] toArray() {
        return jq7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) jq7.b(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return l(this.b);
    }
}
