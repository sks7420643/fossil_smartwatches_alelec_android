package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t2 implements Parcelable.Creator<u2> {
    @DexIgnore
    public /* synthetic */ t2(kq7 kq7) {
    }

    @DexIgnore
    public u2 a(Parcel parcel) {
        return new u2(parcel, (kq7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public u2 createFromParcel(Parcel parcel) {
        return new u2(parcel, (kq7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public u2[] newArray(int i) {
        return new u2[i];
    }
}
