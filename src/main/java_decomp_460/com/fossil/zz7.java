package com.fossil;

import com.fossil.tn7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ vz7 f4560a; // = new vz7("ZERO");
    @DexIgnore
    public static /* final */ vp7<Object, tn7.b, Object> b; // = a.INSTANCE;
    @DexIgnore
    public static /* final */ vp7<vx7<?>, tn7.b, vx7<?>> c; // = b.INSTANCE;
    @DexIgnore
    public static /* final */ vp7<c08, tn7.b, c08> d; // = d.INSTANCE;
    @DexIgnore
    public static /* final */ vp7<c08, tn7.b, c08> e; // = c.INSTANCE;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements vp7<Object, tn7.b, Object> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(2);
        }

        @DexIgnore
        public final Object invoke(Object obj, tn7.b bVar) {
            if (!(bVar instanceof vx7)) {
                return obj;
            }
            Integer num = (Integer) (!(obj instanceof Integer) ? null : obj);
            int intValue = num != null ? num.intValue() : 1;
            return intValue == 0 ? bVar : Integer.valueOf(intValue + 1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements vp7<vx7<?>, tn7.b, vx7<?>> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(2);
        }

        @DexIgnore
        public final vx7<?> invoke(vx7<?> vx7, tn7.b bVar) {
            if (vx7 != null) {
                return vx7;
            }
            return (vx7) (!(bVar instanceof vx7) ? null : bVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements vp7<c08, tn7.b, c08> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(2);
        }

        @DexIgnore
        public final c08 invoke(c08 c08, tn7.b bVar) {
            if (bVar instanceof vx7) {
                ((vx7) bVar).B(c08.b(), c08.d());
            }
            return c08;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements vp7<c08, tn7.b, c08> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(2);
        }

        @DexIgnore
        public final c08 invoke(c08 c08, tn7.b bVar) {
            if (bVar instanceof vx7) {
                c08.a(((vx7) bVar).F(c08.b()));
            }
            return c08;
        }
    }

    @DexIgnore
    public static final void a(tn7 tn7, Object obj) {
        if (obj != f4560a) {
            if (obj instanceof c08) {
                ((c08) obj).c();
                tn7.fold(obj, e);
                return;
            }
            Object fold = tn7.fold(null, c);
            if (fold != null) {
                ((vx7) fold).B(tn7, obj);
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final Object b(tn7 tn7) {
        Object fold = tn7.fold(0, b);
        if (fold != null) {
            return fold;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public static final Object c(tn7 tn7, Object obj) {
        Object b2 = obj != null ? obj : b(tn7);
        if (b2 == 0) {
            return f4560a;
        }
        if (b2 instanceof Integer) {
            return tn7.fold(new c08(tn7, ((Number) b2).intValue()), d);
        }
        if (b2 != null) {
            return ((vx7) b2).F(tn7);
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
    }
}
