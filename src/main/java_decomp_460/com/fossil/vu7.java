package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vu7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(vu7.class, "_handled");
    @DexIgnore
    public volatile int _handled;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Throwable f3837a;

    @DexIgnore
    public vu7(Throwable th, boolean z) {
        this.f3837a = th;
        this._handled = z ? 1 : 0;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ vu7(Throwable th, boolean z, int i, kq7 kq7) {
        this(th, (i & 2) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r1 = this;
            int r0 = r1._handled
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vu7.a():boolean");
    }

    @DexIgnore
    public final boolean b() {
        return b.compareAndSet(this, 0, 1);
    }

    @DexIgnore
    public String toString() {
        return ov7.a(this) + '[' + this.f3837a + ']';
    }
}
