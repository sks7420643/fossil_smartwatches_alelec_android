package com.fossil;

import android.content.Context;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rb3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static boolean f3094a;

    @DexIgnore
    public static int a(Context context) {
        int i = 0;
        synchronized (rb3.class) {
            try {
                rc2.l(context, "Context is null");
                if (!f3094a) {
                    try {
                        md3 a2 = jd3.a(context);
                        try {
                            pb3.k(a2.zze());
                            ae3.e(a2.zzf());
                            f3094a = true;
                        } catch (RemoteException e) {
                            throw new se3(e);
                        }
                    } catch (e62 e2) {
                        i = e2.errorCode;
                    }
                }
            } finally {
            }
        }
        return i;
    }
}
