package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ jg7 d;

    @DexIgnore
    public xg7(Context context, String str, jg7 jg7) {
        this.b = context;
        this.c = str;
        this.d = jg7;
    }

    @DexIgnore
    public final void run() {
        Long l;
        try {
            ig7.u(this.b);
            synchronized (ig7.k) {
                l = (Long) ig7.k.remove(this.c);
            }
            if (l != null) {
                Long valueOf = Long.valueOf((System.currentTimeMillis() - l.longValue()) / 1000);
                if (valueOf.longValue() <= 0) {
                    valueOf = 1L;
                }
                String str = ig7.j;
                if (str != null && str.equals(this.c)) {
                    str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                rg7 rg7 = new rg7(this.b, str, this.c, ig7.a(this.b, false, this.d), valueOf, this.d);
                if (!this.c.equals(ig7.i)) {
                    ig7.m.m("Invalid invocation since previous onResume on diff page.");
                }
                new ch7(rg7).b();
                String unused = ig7.j = this.c;
                return;
            }
            th7 th7 = ig7.m;
            th7.d("Starttime for PageID:" + this.c + " not found, lost onResume()?");
        } catch (Throwable th) {
            ig7.m.e(th);
            ig7.f(this.b, th);
        }
    }
}
