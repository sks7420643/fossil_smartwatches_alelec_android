package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.util.Log;
import com.google.android.datatransport.runtime.backends.TransportBackendDiscovery;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z02 implements t02 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a f4399a;
    @DexIgnore
    public /* final */ x02 b;
    @DexIgnore
    public /* final */ Map<String, b12> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f4400a;
        @DexIgnore
        public Map<String, String> b; // = null;

        @DexIgnore
        public a(Context context) {
            this.f4400a = context;
        }

        @DexIgnore
        public static Bundle d(Context context) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("BackendRegistry", "Context has no PackageManager.");
                    return null;
                }
                ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, TransportBackendDiscovery.class), 128);
                if (serviceInfo != null) {
                    return serviceInfo.metaData;
                }
                Log.w("BackendRegistry", "TransportBackendDiscovery has no service info.");
                return null;
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("BackendRegistry", "Application info not found.");
                return null;
            }
        }

        @DexIgnore
        public final Map<String, String> a(Context context) {
            Bundle d = d(context);
            if (d == null) {
                Log.w("BackendRegistry", "Could not retrieve metadata, returning empty list of transport backends.");
                return Collections.emptyMap();
            }
            HashMap hashMap = new HashMap();
            for (String str : d.keySet()) {
                Object obj = d.get(str);
                if ((obj instanceof String) && str.startsWith("backend:")) {
                    String[] split = ((String) obj).split(",", -1);
                    for (String str2 : split) {
                        String trim = str2.trim();
                        if (!trim.isEmpty()) {
                            hashMap.put(trim, str.substring(8));
                        }
                    }
                }
            }
            return hashMap;
        }

        @DexIgnore
        public s02 b(String str) {
            String str2 = c().get(str);
            if (str2 == null) {
                return null;
            }
            try {
                return (s02) Class.forName(str2).asSubclass(s02.class).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassNotFoundException e) {
                Log.w("BackendRegistry", String.format("Class %s is not found.", str2), e);
            } catch (IllegalAccessException e2) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e2);
            } catch (InstantiationException e3) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e3);
            } catch (NoSuchMethodException e4) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e4);
            } catch (InvocationTargetException e5) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e5);
            }
            return null;
        }

        @DexIgnore
        public final Map<String, String> c() {
            if (this.b == null) {
                this.b = a(this.f4400a);
            }
            return this.b;
        }
    }

    @DexIgnore
    public z02(Context context, x02 x02) {
        this(new a(context), x02);
    }

    @DexIgnore
    public z02(a aVar, x02 x02) {
        this.c = new HashMap();
        this.f4399a = aVar;
        this.b = x02;
    }

    @DexIgnore
    @Override // com.fossil.t02
    public b12 b(String str) {
        synchronized (this) {
            if (this.c.containsKey(str)) {
                return this.c.get(str);
            }
            s02 b2 = this.f4399a.b(str);
            if (b2 == null) {
                return null;
            }
            b12 create = b2.create(this.b.a(str));
            this.c.put(str, create);
            return create;
        }
    }
}
