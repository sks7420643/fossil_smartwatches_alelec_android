package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ae3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static fs2 f258a;

    @DexIgnore
    public static zd3 a() {
        try {
            return new zd3(f().g());
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static zd3 b(float f) {
        try {
            return new zd3(f().O1(f));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static zd3 c(String str) {
        try {
            return new zd3(f().zza(str));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static zd3 d(Bitmap bitmap) {
        try {
            return new zd3(f().zza(bitmap));
        } catch (RemoteException e) {
            throw new se3(e);
        }
    }

    @DexIgnore
    public static void e(fs2 fs2) {
        if (f258a == null) {
            rc2.k(fs2);
            f258a = fs2;
        }
    }

    @DexIgnore
    public static fs2 f() {
        fs2 fs2 = f258a;
        rc2.l(fs2, "IBitmapDescriptorFactory is not initialized");
        return fs2;
    }
}
