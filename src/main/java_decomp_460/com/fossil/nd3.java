package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nd3 extends as2 implements md3 {
    @DexIgnore
    public nd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    @DexIgnore
    @Override // com.fossil.md3
    public final ac3 F2(rg2 rg2) throws RemoteException {
        ac3 pd3;
        Parcel d = d();
        es2.c(d, rg2);
        Parcel e = e(2, d);
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            pd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapFragmentDelegate");
            pd3 = queryLocalInterface instanceof ac3 ? (ac3) queryLocalInterface : new pd3(readStrongBinder);
        }
        e.recycle();
        return pd3;
    }

    @DexIgnore
    @Override // com.fossil.md3
    public final void p2(rg2 rg2, int i) throws RemoteException {
        Parcel d = d();
        es2.c(d, rg2);
        d.writeInt(i);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.md3
    public final bc3 q0(rg2 rg2, GoogleMapOptions googleMapOptions) throws RemoteException {
        bc3 qd3;
        Parcel d = d();
        es2.c(d, rg2);
        es2.d(d, googleMapOptions);
        Parcel e = e(3, d);
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            qd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
            qd3 = queryLocalInterface instanceof bc3 ? (bc3) queryLocalInterface : new qd3(readStrongBinder);
        }
        e.recycle();
        return qd3;
    }

    @DexIgnore
    @Override // com.fossil.md3
    public final ec3 t1(rg2 rg2, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException {
        ec3 gd3;
        Parcel d = d();
        es2.c(d, rg2);
        es2.d(d, streetViewPanoramaOptions);
        Parcel e = e(7, d);
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            gd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
            gd3 = queryLocalInterface instanceof ec3 ? (ec3) queryLocalInterface : new gd3(readStrongBinder);
        }
        e.recycle();
        return gd3;
    }

    @DexIgnore
    @Override // com.fossil.md3
    public final yb3 zze() throws RemoteException {
        yb3 vc3;
        Parcel e = e(4, d());
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            vc3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
            vc3 = queryLocalInterface instanceof yb3 ? (yb3) queryLocalInterface : new vc3(readStrongBinder);
        }
        e.recycle();
        return vc3;
    }

    @DexIgnore
    @Override // com.fossil.md3
    public final fs2 zzf() throws RemoteException {
        Parcel e = e(5, d());
        fs2 e2 = gs2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
