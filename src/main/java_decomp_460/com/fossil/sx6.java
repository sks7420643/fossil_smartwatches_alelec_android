package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.x37;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sx6 extends qv5 implements yx6 {
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public g37<z95> h;
    @DexIgnore
    public xx6 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final sx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            sx6 sx6 = new sx6();
            sx6.setArguments(bundle);
            return sx6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sx6 b;

        @DexIgnore
        public b(sx6 sx6, String str) {
            this.b = sx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            sx6.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sx6 b;

        @DexIgnore
        public c(sx6 sx6, String str) {
            this.b = sx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                if (this.b.j) {
                    HomeActivity.a aVar = HomeActivity.B;
                    pq7.b(activity, "fragmentActivity");
                    HomeActivity.a.b(aVar, activity, null, 2, null);
                }
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sx6 b;

        @DexIgnore
        public d(sx6 sx6, String str) {
            this.b = sx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore
    public static final /* synthetic */ xx6 L6(sx6 sx6) {
        xx6 xx6 = sx6.i;
        if (xx6 != null) {
            return xx6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FragmentActivity activity;
        xx6 xx6 = this.i;
        if (xx6 == null) {
            pq7.n("mPresenter");
            throw null;
        } else if (xx6.o() || (activity = getActivity()) == null) {
            return false;
        } else {
            activity.finish();
            return false;
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(xx6 xx6) {
        pq7.c(xx6, "presenter");
        this.i = xx6;
    }

    @DexIgnore
    @Override // com.fossil.yx6
    public void f() {
        DashBar dashBar;
        g37<z95> g37 = this.h;
        if (g37 != null) {
            z95 a2 = g37.a();
            if (a2 != null && (dashBar = a2.z) != null) {
                x37.a aVar = x37.f4036a;
                pq7.b(dashBar, "this");
                aVar.g(dashBar, this.j, 500);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yx6
    public void l1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingActivity.a aVar = PairingActivity.C;
            pq7.b(activity, "fragmentActivity");
            xx6 xx6 = this.i;
            if (xx6 != null) {
                aVar.b(activity, xx6.o());
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        z95 z95 = (z95) aq0.f(layoutInflater, 2131558604, viewGroup, false, A6());
        this.h = new g37<>(this, z95);
        pq7.b(z95, "binding");
        return z95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        xx6 xx6 = this.i;
        if (xx6 != null) {
            xx6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        xx6 xx6 = this.i;
        if (xx6 != null) {
            xx6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886948);
        g37<z95> g37 = this.h;
        if (g37 != null) {
            z95 a2 = g37.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new b(this, c2));
                a2.s.setOnClickListener(new c(this, c2));
                a2.w.setOnClickListener(new d(this, c2));
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "binding.ftvWearOs");
                flexibleTextView.setText(c2);
                FlexibleTextView flexibleTextView2 = a2.v;
                pq7.b(flexibleTextView2, "binding.ftvWearOs");
                flexibleTextView2.setTypeface(flexibleTextView2.getTypeface(), 1);
                if (!wr4.f3989a.a().d()) {
                    RelativeLayout relativeLayout = a2.A;
                    pq7.b(relativeLayout, "binding.rlWearOsGroup");
                    relativeLayout.setVisibility(8);
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                this.j = z;
                xx6 xx6 = this.i;
                if (xx6 != null) {
                    xx6.p(z);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.yx6
    public void y0(boolean z) {
        ImageView imageView;
        if (isActive()) {
            g37<z95> g37 = this.h;
            if (g37 != null) {
                z95 a2 = g37.a();
                if (a2 != null && (imageView = a2.w) != null) {
                    pq7.b(imageView, "it");
                    imageView.setVisibility(!z ? 4 : 0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }
}
