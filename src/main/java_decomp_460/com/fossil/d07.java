package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d07 implements MembersInjector<b07> {
    @DexIgnore
    public static void a(b07 b07, z27 z27) {
        b07.e = z27;
    }

    @DexIgnore
    public static void b(b07 b07, c37 c37) {
        b07.f = c37;
    }

    @DexIgnore
    public static void c(b07 b07) {
        b07.w();
    }
}
