package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fossil.sg2;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qg2<T extends sg2> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public T f2976a;
    @DexIgnore
    public Bundle b;
    @DexIgnore
    public LinkedList<a> c;
    @DexIgnore
    public /* final */ ug2<T> d; // = new xg2(this);

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(sg2 sg2);

        @DexIgnore
        int getState();
    }

    @DexIgnore
    public static void o(FrameLayout frameLayout) {
        c62 q = c62.q();
        Context context = frameLayout.getContext();
        int i = q.i(context);
        String d2 = bc2.d(context, i);
        String c2 = bc2.c(context, i);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        textView.setText(d2);
        linearLayout.addView(textView);
        Intent d3 = q.d(context, i, null);
        if (d3 != null) {
            Button button = new Button(context);
            button.setId(16908313);
            button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            button.setText(c2);
            linearLayout.addView(button);
            button.setOnClickListener(new bh2(context, d3));
        }
    }

    @DexIgnore
    public abstract void a(ug2<T> ug2);

    @DexIgnore
    public T b() {
        return this.f2976a;
    }

    @DexIgnore
    public void c(FrameLayout frameLayout) {
        o(frameLayout);
    }

    @DexIgnore
    public void d(Bundle bundle) {
        s(bundle, new zg2(this, bundle));
    }

    @DexIgnore
    public View e(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FrameLayout frameLayout = new FrameLayout(layoutInflater.getContext());
        s(bundle, new yg2(this, frameLayout, layoutInflater, viewGroup, bundle));
        if (this.f2976a == null) {
            c(frameLayout);
        }
        return frameLayout;
    }

    @DexIgnore
    public void f() {
        T t = this.f2976a;
        if (t != null) {
            t.onDestroy();
        } else {
            u(1);
        }
    }

    @DexIgnore
    public void g() {
        T t = this.f2976a;
        if (t != null) {
            t.o();
        } else {
            u(2);
        }
    }

    @DexIgnore
    public void h(Activity activity, Bundle bundle, Bundle bundle2) {
        s(bundle2, new wg2(this, activity, bundle, bundle2));
    }

    @DexIgnore
    public void i() {
        T t = this.f2976a;
        if (t != null) {
            t.onLowMemory();
        }
    }

    @DexIgnore
    public void j() {
        T t = this.f2976a;
        if (t != null) {
            t.onPause();
        } else {
            u(5);
        }
    }

    @DexIgnore
    public void k() {
        s(null, new ch2(this));
    }

    @DexIgnore
    public void l(Bundle bundle) {
        T t = this.f2976a;
        if (t != null) {
            t.onSaveInstanceState(bundle);
            return;
        }
        Bundle bundle2 = this.b;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
    }

    @DexIgnore
    public void m() {
        s(null, new ah2(this));
    }

    @DexIgnore
    public void n() {
        T t = this.f2976a;
        if (t != null) {
            t.onStop();
        } else {
            u(4);
        }
    }

    @DexIgnore
    public final void s(Bundle bundle, a aVar) {
        T t = this.f2976a;
        if (t != null) {
            aVar.a(t);
            return;
        }
        if (this.c == null) {
            this.c = new LinkedList<>();
        }
        this.c.add(aVar);
        if (bundle != null) {
            Bundle bundle2 = this.b;
            if (bundle2 == null) {
                this.b = (Bundle) bundle.clone();
            } else {
                bundle2.putAll(bundle);
            }
        }
        a(this.d);
    }

    @DexIgnore
    public final void u(int i) {
        while (!this.c.isEmpty() && this.c.getLast().getState() >= i) {
            this.c.removeLast();
        }
    }
}
