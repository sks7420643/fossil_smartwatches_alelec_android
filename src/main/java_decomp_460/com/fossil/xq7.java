package com.fossil;

import com.fossil.ms7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xq7 extends yq7 implements ms7 {
    @DexIgnore
    public xq7() {
    }

    @DexIgnore
    public xq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.gq7
    public ds7 computeReflected() {
        er7.g(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get(T t);

    @DexIgnore
    @Override // com.fossil.ms7
    public Object getDelegate(Object obj) {
        return ((ms7) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    @Override // com.fossil.yq7, com.fossil.ms7
    public ms7.a getGetter() {
        return ((ms7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.rp7
    public Object invoke(Object obj) {
        return get(obj);
    }
}
