package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import androidx.fragment.app.Fragment;
import com.fossil.om0;
import com.fossil.yq0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lq0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements om0.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Fragment f2229a;

        @DexIgnore
        public a(Fragment fragment) {
            this.f2229a = fragment;
        }

        @DexIgnore
        @Override // com.fossil.om0.a
        public void onCancel() {
            if (this.f2229a.getAnimatingAway() != null) {
                View animatingAway = this.f2229a.getAnimatingAway();
                this.f2229a.setAnimatingAway(null);
                animatingAway.clearAnimation();
            }
            this.f2229a.setAnimator(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ yq0.g d;
        @DexIgnore
        public /* final */ /* synthetic */ om0 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                if (b.this.c.getAnimatingAway() != null) {
                    b.this.c.setAnimatingAway(null);
                    b bVar = b.this;
                    bVar.d.a(bVar.c, bVar.e);
                }
            }
        }

        @DexIgnore
        public b(ViewGroup viewGroup, Fragment fragment, yq0.g gVar, om0 om0) {
            this.b = viewGroup;
            this.c = fragment;
            this.d = gVar;
            this.e = om0;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.b.post(new a());
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ViewGroup f2230a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ yq0.g d;
        @DexIgnore
        public /* final */ /* synthetic */ om0 e;

        @DexIgnore
        public c(ViewGroup viewGroup, View view, Fragment fragment, yq0.g gVar, om0 om0) {
            this.f2230a = viewGroup;
            this.b = view;
            this.c = fragment;
            this.d = gVar;
            this.e = om0;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.f2230a.endViewTransition(this.b);
            Animator animator2 = this.c.getAnimator();
            this.c.setAnimator(null);
            if (animator2 != null && this.f2230a.indexOfChild(this.b) < 0) {
                this.d.a(this.c, this.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Animation f2231a;
        @DexIgnore
        public /* final */ Animator b;

        @DexIgnore
        public d(Animator animator) {
            this.f2231a = null;
            this.b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }

        @DexIgnore
        public d(Animation animation) {
            this.f2231a = animation;
            this.b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends AnimationSet implements Runnable {
        @DexIgnore
        public /* final */ ViewGroup b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f; // = true;

        @DexIgnore
        public e(Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.b = viewGroup;
            this.c = view;
            addAnimation(animation);
            this.b.post(this);
        }

        @DexIgnore
        public boolean getTransformation(long j, Transformation transformation) {
            this.f = true;
            if (this.d) {
                return !this.e;
            }
            if (super.getTransformation(j, transformation)) {
                return true;
            }
            this.d = true;
            jo0.a(this.b, this);
            return true;
        }

        @DexIgnore
        public boolean getTransformation(long j, Transformation transformation, float f2) {
            this.f = true;
            if (this.d) {
                return !this.e;
            }
            if (super.getTransformation(j, transformation, f2)) {
                return true;
            }
            this.d = true;
            jo0.a(this.b, this);
            return true;
        }

        @DexIgnore
        public void run() {
            if (this.d || !this.f) {
                this.b.endViewTransition(this.c);
                this.e = true;
                return;
            }
            this.f = false;
            this.b.post(this);
        }
    }

    @DexIgnore
    public static void a(Fragment fragment, d dVar, yq0.g gVar) {
        View view = fragment.mView;
        ViewGroup viewGroup = fragment.mContainer;
        viewGroup.startViewTransition(view);
        om0 om0 = new om0();
        om0.d(new a(fragment));
        gVar.b(fragment, om0);
        if (dVar.f2231a != null) {
            e eVar = new e(dVar.f2231a, viewGroup, view);
            fragment.setAnimatingAway(fragment.mView);
            eVar.setAnimationListener(new b(viewGroup, fragment, gVar, om0));
            fragment.mView.startAnimation(eVar);
            return;
        }
        Animator animator = dVar.b;
        fragment.setAnimator(animator);
        animator.addListener(new c(viewGroup, view, fragment, gVar, om0));
        animator.setTarget(fragment.mView);
        animator.start();
    }

    @DexIgnore
    public static d b(Context context, mq0 mq0, Fragment fragment, boolean z) {
        int c2;
        boolean z2;
        int nextTransition = fragment.getNextTransition();
        int nextAnim = fragment.getNextAnim();
        fragment.setNextAnim(0);
        View b2 = mq0.b(fragment.mContainerId);
        if (!(b2 == null || b2.getTag(gq0.visible_removing_fragment_view_tag) == null)) {
            b2.setTag(gq0.visible_removing_fragment_view_tag, null);
        }
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null && viewGroup.getLayoutTransition() != null) {
            return null;
        }
        Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, z, nextAnim);
        if (onCreateAnimation != null) {
            return new d(onCreateAnimation);
        }
        Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, z, nextAnim);
        if (onCreateAnimator != null) {
            return new d(onCreateAnimator);
        }
        if (nextAnim != 0) {
            boolean equals = "anim".equals(context.getResources().getResourceTypeName(nextAnim));
            if (equals) {
                try {
                    Animation loadAnimation = AnimationUtils.loadAnimation(context, nextAnim);
                    if (loadAnimation != null) {
                        return new d(loadAnimation);
                    }
                    z2 = true;
                } catch (Resources.NotFoundException e2) {
                    throw e2;
                } catch (RuntimeException e3) {
                    z2 = false;
                }
            } else {
                z2 = false;
            }
            if (!z2) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(context, nextAnim);
                    if (loadAnimator != null) {
                        return new d(loadAnimator);
                    }
                } catch (RuntimeException e4) {
                    if (!equals) {
                        Animation loadAnimation2 = AnimationUtils.loadAnimation(context, nextAnim);
                        if (loadAnimation2 != null) {
                            return new d(loadAnimation2);
                        }
                    } else {
                        throw e4;
                    }
                }
            }
        }
        if (nextTransition == 0 || (c2 = c(nextTransition, z)) < 0) {
            return null;
        }
        return new d(AnimationUtils.loadAnimation(context, c2));
    }

    @DexIgnore
    public static int c(int i, boolean z) {
        if (i == 4097) {
            return z ? fq0.fragment_open_enter : fq0.fragment_open_exit;
        }
        if (i == 4099) {
            return z ? fq0.fragment_fade_enter : fq0.fragment_fade_exit;
        }
        if (i != 8194) {
            return -1;
        }
        return z ? fq0.fragment_close_enter : fq0.fragment_close_exit;
    }
}
