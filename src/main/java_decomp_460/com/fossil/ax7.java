package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ax7 extends fx7 implements uu7 {
    @DexIgnore
    public /* final */ boolean c; // = u0();

    @DexIgnore
    public ax7(xw7 xw7) {
        super(true);
        T(xw7);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public boolean M() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public boolean N() {
        return true;
    }

    @DexIgnore
    public final boolean u0() {
        fx7 fx7;
        qu7 P = P();
        if (!(P instanceof ru7)) {
            P = null;
        }
        ru7 ru7 = (ru7) P;
        if (!(ru7 == null || (fx7 = (fx7) ru7.e) == null)) {
            while (!fx7.M()) {
                qu7 P2 = fx7.P();
                if (!(P2 instanceof ru7)) {
                    P2 = null;
                }
                ru7 ru72 = (ru7) P2;
                if (ru72 != null) {
                    fx7 = (fx7) ru72.e;
                    if (fx7 == null) {
                    }
                }
            }
            return true;
        }
        return false;
    }
}
