package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qe7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Context f2970a; // = null;

    @DexIgnore
    public qe7(Context context) {
        this.f2970a = context;
    }

    @DexIgnore
    public final void a(ne7 ne7) {
        if (ne7 != null) {
            String ne72 = ne7.toString();
            if (c()) {
                b(se7.i(ne72));
            }
        }
    }

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public final ne7 e() {
        String h = c() ? se7.h(d()) : null;
        if (h != null) {
            return ne7.b(h);
        }
        return null;
    }
}
