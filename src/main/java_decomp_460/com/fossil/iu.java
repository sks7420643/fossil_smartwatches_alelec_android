package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class iu extends Enum<iu> {
    @DexIgnore
    public static /* final */ iu c;
    @DexIgnore
    public static /* final */ iu d;
    @DexIgnore
    public static /* final */ iu e;
    @DexIgnore
    public static /* final */ iu f;
    @DexIgnore
    public static /* final */ iu g;
    @DexIgnore
    public static /* final */ iu h;
    @DexIgnore
    public static /* final */ iu i;
    @DexIgnore
    public static /* final */ iu j;
    @DexIgnore
    public static /* final */ iu k;
    @DexIgnore
    public static /* final */ iu l;
    @DexIgnore
    public static /* final */ /* synthetic */ iu[] m;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        iu iuVar = new iu("GET_FILE", 0, (byte) 1);
        c = iuVar;
        iu iuVar2 = new iu("LIST_FILE", 1, (byte) 2);
        d = iuVar2;
        iu iuVar3 = new iu("PUT_FILE", 2, (byte) 3);
        e = iuVar3;
        iu iuVar4 = new iu("VERIFY_FILE", 3, (byte) 4);
        f = iuVar4;
        iu iuVar5 = new iu("GET_SIZE_WRITTEN", 4, (byte) 5);
        g = iuVar5;
        iu iuVar6 = new iu("VERIFY_DATA", 5, (byte) 6);
        h = iuVar6;
        iu iuVar7 = new iu("ERASE_DATA", 6, (byte) 7);
        iu iuVar8 = new iu("EOF_REACH", 7, (byte) 8);
        i = iuVar8;
        iu iuVar9 = new iu("ABORT_FILE", 8, (byte) 9);
        j = iuVar9;
        iu iuVar10 = new iu("WAITING_REQUEST", 9, (byte) 10);
        k = iuVar10;
        iu iuVar11 = new iu("DELETE_FILE", 10, (byte) 11);
        l = iuVar11;
        m = new iu[]{iuVar, iuVar2, iuVar3, iuVar4, iuVar5, iuVar6, iuVar7, iuVar8, iuVar9, iuVar10, iuVar11};
    }
    */

    @DexIgnore
    public iu(String str, int i2, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static iu valueOf(String str) {
        return (iu) Enum.valueOf(iu.class, str);
    }

    @DexIgnore
    public static iu[] values() {
        return (iu[]) m.clone();
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.b | Byte.MIN_VALUE);
    }
}
