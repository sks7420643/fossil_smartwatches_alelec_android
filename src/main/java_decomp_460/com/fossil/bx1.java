package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bx1 implements zx1 {
    NETWORK_UNAVAILABLE(100),
    HTTP_ERROR(101);
    
    @DexIgnore
    public /* final */ String b; // = ey1.a(this);
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public bx1(int i) {
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.zx1
    public int getCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.zx1
    public String getLogName() {
        return this.b;
    }
}
