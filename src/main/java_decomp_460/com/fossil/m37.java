package com.fossil;

import android.content.ContentResolver;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m37 implements Factory<l37> {
    @DexIgnore
    public static l37 a(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, no4 no4, ContentResolver contentResolver, lu5 lu5, on5 on5) {
        return new l37(portfolioApp, notificationsRepository, deviceRepository, notificationSettingsDatabase, no4, contentResolver, lu5, on5);
    }
}
