package com.fossil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u38 extends w38 {
    @DexIgnore
    public /* final */ Method c;
    @DexIgnore
    public /* final */ Method d;
    @DexIgnore
    public /* final */ Method e;
    @DexIgnore
    public /* final */ Class<?> f;
    @DexIgnore
    public /* final */ Class<?> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements InvocationHandler {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<String> f3512a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;

        @DexIgnore
        public a(List<String> list) {
            this.f3512a = list;
        }

        @DexIgnore
        @Override // java.lang.reflect.InvocationHandler
        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = b28.b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return Boolean.TRUE;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.b = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f3512a;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f3512a.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.c = str;
                            return str;
                        }
                    }
                    String str2 = this.f3512a.get(0);
                    this.c = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.c = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    @DexIgnore
    public u38(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.c = method;
        this.d = method2;
        this.e = method3;
        this.f = cls;
        this.g = cls2;
    }

    @DexIgnore
    public static w38 s() {
        try {
            Class<?> cls = Class.forName("org.eclipse.jetty.alpn.ALPN");
            Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN$Provider");
            Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN$ClientProvider");
            Class<?> cls4 = Class.forName("org.eclipse.jetty.alpn.ALPN$ServerProvider");
            return new u38(cls.getMethod("put", SSLSocket.class, cls2), cls.getMethod("get", SSLSocket.class), cls.getMethod("remove", SSLSocket.class), cls3, cls4);
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void a(SSLSocket sSLSocket) {
        try {
            this.e.invoke(null, sSLSocket);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw b28.b("unable to remove alpn", e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public void g(SSLSocket sSLSocket, String str, List<t18> list) {
        List<String> b = w38.b(list);
        try {
            ClassLoader classLoader = w38.class.getClassLoader();
            Class<?> cls = this.f;
            Class<?> cls2 = this.g;
            a aVar = new a(b);
            Object newProxyInstance = Proxy.newProxyInstance(classLoader, new Class[]{cls, cls2}, aVar);
            this.c.invoke(null, sSLSocket, newProxyInstance);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw b28.b("unable to set alpn", e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.w38
    public String m(SSLSocket sSLSocket) {
        try {
            a aVar = (a) Proxy.getInvocationHandler(this.d.invoke(null, sSLSocket));
            if (aVar.b || aVar.c != null) {
                return aVar.b ? null : aVar.c;
            }
            w38.j().q(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", null);
            return null;
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw b28.b("unable to get selected protocol", e2);
        }
    }
}
