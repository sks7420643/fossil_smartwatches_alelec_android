package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y67 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Integer> f4250a; // = new MutableLiveData<>();

    @DexIgnore
    public final MutableLiveData<Integer> a() {
        return this.f4250a;
    }
}
