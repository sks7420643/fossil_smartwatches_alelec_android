package com.fossil;

import com.fossil.lx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ lx1.b f1826a; // = lx1.b.CTR_NO_PADDING;
    @DexIgnore
    public static /* final */ jx b; // = new jx();

    @DexIgnore
    public final byte[] a(String str, n6 n6Var, lx1.a aVar, byte[] bArr) {
        boolean z = true;
        hx a2 = ix.b.a(str);
        byte[] c = a2.c();
        byte[] a3 = a2.a(n6Var).a();
        if (c == null) {
            return bArr;
        }
        if (true != (!(c.length == 0))) {
            return bArr;
        }
        if (a3.length != 0) {
            z = false;
        }
        if (!(!z)) {
            return bArr;
        }
        byte[] d = lx1.f2275a.d(aVar, f1826a, c, a3, bArr);
        int ceil = (int) ((float) Math.ceil((double) (((float) bArr.length) / ((float) 16))));
        kx kxVar = a2.d.get(n6Var);
        if (kxVar != null) {
            kxVar.e = ceil + kxVar.e;
        }
        return d;
    }

    @DexIgnore
    public final byte[] b(String str, n6 n6Var, byte[] bArr) {
        return a(str, n6Var, lx1.a.DECRYPT, bArr);
    }

    @DexIgnore
    public final byte[] c(String str, n6 n6Var, byte[] bArr) {
        return a(str, n6Var, lx1.a.ENCRYPT, bArr);
    }
}
