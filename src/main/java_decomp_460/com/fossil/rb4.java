package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rb4 implements FilenameFilter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ rb4 f3095a; // = new rb4();

    @DexIgnore
    public static FilenameFilter a() {
        return f3095a;
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return str.startsWith(Constants.EVENT);
    }
}
