package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr0 implements dw7 {
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ LiveData<?> c;
    @DexIgnore
    public /* final */ js0<?> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "androidx.lifecycle.EmittedSource$dispose$1", f = "CoroutineLiveData.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sr0 sr0, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sr0;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.d();
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "androidx.lifecycle.EmittedSource$disposeNow$2", f = "CoroutineLiveData.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sr0 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sr0 sr0, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sr0;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.d();
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public sr0(LiveData<?> liveData, js0<?> js0) {
        pq7.c(liveData, "source");
        pq7.c(js0, "mediator");
        this.c = liveData;
        this.d = js0;
    }

    @DexIgnore
    public final Object b(qn7<? super tl7> qn7) {
        return eu7.g(bw7.c().S(), new b(this, null), qn7);
    }

    @DexIgnore
    public final void d() {
        if (!this.b) {
            this.d.q((LiveData<S>) this.c);
            this.b = true;
        }
    }

    @DexIgnore
    @Override // com.fossil.dw7
    public void dispose() {
        xw7 unused = gu7.d(jv7.a(bw7.c().S()), null, null, new a(this, null), 3, null);
    }
}
