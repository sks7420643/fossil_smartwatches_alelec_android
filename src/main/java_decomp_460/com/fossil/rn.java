package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn extends qq7 implements rp7<lp, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ Cdo b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rn(Cdo doVar) {
        super(1);
        this.b = doVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(lp lpVar) {
        Cdo doVar = this.b;
        jq1 deviceRequest = Cdo.G(doVar).getDeviceRequest();
        if (deviceRequest != null) {
            Cdo.H(doVar, new bu1((uq1) deviceRequest, new nt1("", eu1.ERROR)));
            return tl7.f3441a;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
    }
}
