package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q90 implements Parcelable.Creator<r90> {
    @DexIgnore
    public /* synthetic */ q90(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public r90 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            ca0 valueOf = ca0.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                pq7.b(readString2, "parcel.readString()!!");
                w90 valueOf2 = w90.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    pq7.b(readString3, "parcel.readString()!!");
                    da0 valueOf3 = da0.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        pq7.b(readString4, "parcel.readString()!!");
                        return new r90(valueOf, valueOf2, valueOf3, ea0.valueOf(readString4), (short) parcel.readInt());
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public r90[] newArray(int i) {
        return new r90[i];
    }
}
