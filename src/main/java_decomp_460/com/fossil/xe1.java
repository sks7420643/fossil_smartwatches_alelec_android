package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.af1;
import com.fossil.wb1;
import java.io.File;
import java.io.FileNotFoundException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe1 implements af1<Uri, File> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f4111a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements bf1<Uri, File> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Context f4112a;

        @DexIgnore
        public a(Context context) {
            this.f4112a = context;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, File> b(ef1 ef1) {
            return new xe1(this.f4112a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements wb1<File> {
        @DexIgnore
        public static /* final */ String[] d; // = {"_data"};
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ Uri c;

        @DexIgnore
        public b(Context context, Uri uri) {
            this.b = context;
            this.c = uri;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super File> aVar) {
            String str = null;
            Cursor query = this.b.getContentResolver().query(this.c, d, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                aVar.b(new FileNotFoundException("Failed to find file path for: " + this.c));
                return;
            }
            aVar.e(new File(str));
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<File> getDataClass() {
            return File.class;
        }
    }

    @DexIgnore
    public xe1(Context context) {
        this.f4111a = context;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<File> b(Uri uri, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(uri), new b(this.f4111a, uri));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return jc1.b(uri);
    }
}
