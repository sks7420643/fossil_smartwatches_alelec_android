package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class do5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ on5 f817a;
    @DexIgnore
    public /* final */ DeviceRepository b;
    @DexIgnore
    public /* final */ DianaPresetRepository c;
    @DexIgnore
    public /* final */ oq4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.migration.MigrationHelper$isNeedToBlockingMigratePresetToWatchFace$2", f = "MigrationHelper.kt", l = {37}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ do5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(do5 do5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = do5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            T t;
            Object allPresets;
            Device device;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                Boolean j0 = this.this$0.f817a.j0();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MigrationHelper", "isNeedToBlockingMigratePresetToWatchFace isMigrated " + j0);
                if (!j0.booleanValue()) {
                    Iterator<T> it = this.this$0.b.getAllDevice().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (ao7.a(nk5.o.x(next.getDeviceId())).booleanValue()) {
                            t = next;
                            break;
                        }
                    }
                    T t2 = t;
                    DianaPresetRepository dianaPresetRepository = this.this$0.c;
                    this.L$0 = iv7;
                    this.L$1 = j0;
                    this.L$2 = t2;
                    this.label = 1;
                    allPresets = dianaPresetRepository.getAllPresets(this);
                    if (allPresets == d) {
                        return d;
                    }
                    device = t2;
                }
                this.this$0.f817a.G1(ao7.a(true));
                return ao7.a(false);
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                device = (Device) this.L$2;
                allPresets = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) allPresets;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MigrationHelper", "isNeedToBlockingMigratePresetToWatchFace dianaDevice " + device + " oldPresetSize " + list.size());
            if (device != null && (!list.isEmpty())) {
                return ao7.a(true);
            }
            this.this$0.f817a.G1(ao7.a(true));
            return ao7.a(false);
        }
    }

    @DexIgnore
    public do5(on5 on5, DeviceRepository deviceRepository, DianaPresetRepository dianaPresetRepository, oq4 oq4) {
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(dianaPresetRepository, "oldPresetRepository");
        pq7.c(oq4, "mMigrationManager");
        this.f817a = on5;
        this.b = deviceRepository;
        this.c = dianaPresetRepository;
        this.d = oq4;
    }

    @DexIgnore
    public final boolean d(String str) {
        pq7.c(str, "version");
        return this.f817a.k0(str);
    }

    @DexIgnore
    public final Object e(qn7<? super Boolean> qn7) {
        return eu7.g(bw7.b(), new a(this, null), qn7);
    }

    @DexIgnore
    public final Object f(String str, qn7<? super Boolean> qn7) {
        String x = this.f817a.x();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MigrationHelper", "start migration for " + str + " lastVersion " + x);
        return this.d.x(qn7);
    }

    @DexIgnore
    public final Object g(qn7<? super Integer> qn7) {
        return this.d.s(qn7);
    }
}
