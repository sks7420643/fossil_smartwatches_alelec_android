package com.fossil;

import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ne4 implements ft3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2509a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public ne4(Context context, Intent intent) {
        this.f2509a = context;
        this.b = intent;
    }

    @DexIgnore
    @Override // com.fossil.ft3
    public final Object then(nt3 nt3) {
        return qe4.g(this.f2509a, this.b, nt3);
    }
}
