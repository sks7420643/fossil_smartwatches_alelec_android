package com.fossil;

import android.util.Log;
import com.fossil.ag7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zf7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ag7.a b;

    @DexIgnore
    public zf7(ag7.a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public void run() {
        if (ag7.e() != null && !ag7.a.a(this.b)) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onForeground");
            hg7.d(ag7.a.c(this.b), "onForeground_WX", null);
            ag7.a.b(this.b, true);
        }
    }
}
