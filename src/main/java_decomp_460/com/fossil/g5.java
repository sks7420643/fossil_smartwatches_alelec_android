package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g5 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ k5 b;
    @DexIgnore
    public /* final */ /* synthetic */ g7 c;
    @DexIgnore
    public /* final */ /* synthetic */ n6 d;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] e;

    @DexIgnore
    public g5(k5 k5Var, g7 g7Var, n6 n6Var, byte[] bArr) {
        this.b = k5Var;
        this.c = g7Var;
        this.d = n6Var;
        this.e = bArr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.f2462a.f();
        this.b.z.f2462a.c(new l7(this.c, this.d, this.e));
    }
}
