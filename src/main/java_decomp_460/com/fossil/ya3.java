package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya3 implements Parcelable.Creator<xa3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ xa3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 1;
        int i2 = 1;
        long j = -1;
        long j2 = -1;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i2 = ad2.v(parcel, t);
            } else if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l == 3) {
                j2 = ad2.y(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                j = ad2.y(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new xa3(i2, i, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ xa3[] newArray(int i) {
        return new xa3[i];
    }
}
