package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl1 extends hl1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ un1 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final LinkedHashSet<un1> a(un1 un1) {
            LinkedHashSet<un1> linkedHashSet = new LinkedHashSet<>();
            if (un1 == null) {
                mm7.t(linkedHashSet, un1.values());
            } else {
                linkedHashSet.add(un1);
            }
            return linkedHashSet;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public kl1 createFromParcel(Parcel parcel) {
            return (kl1) hl1.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public kl1[] newArray(int i) {
            return new kl1[i];
        }
    }

    @DexIgnore
    public kl1(byte b, byte b2, un1 un1) throws IllegalArgumentException {
        super(b, b2, CREATOR.a(un1), false, false, true);
        this.i = un1;
    }

    @DexIgnore
    public final un1 getDayOfWeek() {
        return this.i;
    }
}
