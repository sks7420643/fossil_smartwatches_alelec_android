package com.fossil;

import com.fossil.sx1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma extends xx1<iw1> {
    @DexIgnore
    public static /* final */ ma f; // = new ma();

    @DexIgnore
    public ma() {
        super(DateTimeFieldType.SECOND_OF_MINUTE, (byte) 254, new ry1(3, 0));
    }

    @DexIgnore
    public final cc0[] f(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < bArr.length) {
            short p = hy1.p(bArr[i]);
            int i2 = i + p;
            int i3 = i2 + 1;
            int i4 = i3 + 2;
            ByteBuffer order = ByteBuffer.wrap(dm7.k(bArr, i3, i4)).order(ByteOrder.LITTLE_ENDIAN);
            pq7.b(order, "ByteBuffer.wrap(\n       \u2026(ByteOrder.LITTLE_ENDIAN)");
            int n = hy1.n(order.getShort());
            arrayList.add(new cc0(new String(dm7.k(bArr, i + 1, i2), hd0.y.c()), dm7.k(bArr, i4, i4 + n)));
            i += p + 1 + 2 + n;
        }
        Object[] array = arrayList.toArray(new cc0[0]);
        if (array != null) {
            return (cc0[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* renamed from: g */
    public iw1 d(byte[] bArr) {
        ec0 ec0;
        cc0 cc0;
        byte[] bArr2;
        ry1 ry1 = new ry1(bArr[2], bArr[3]);
        yb0 yb0 = new yb0(dm7.k(bArr, 12, 24));
        ByteBuffer order = ByteBuffer.wrap(dm7.k(bArr, 24, 28)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i = order.getInt();
        ByteBuffer order2 = ByteBuffer.wrap(dm7.k(bArr, 28, 32)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order2, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i2 = order2.getInt();
        ByteBuffer order3 = ByteBuffer.wrap(dm7.k(bArr, 32, 36)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order3, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i3 = order3.getInt();
        ByteBuffer order4 = ByteBuffer.wrap(dm7.k(bArr, 36, 40)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order4, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i4 = order4.getInt();
        ByteBuffer order5 = ByteBuffer.wrap(dm7.k(bArr, 40, 44)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order5, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i5 = order5.getInt();
        ByteBuffer order6 = ByteBuffer.wrap(dm7.k(bArr, 44, 48)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order6, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i6 = order6.getInt();
        ByteBuffer order7 = ByteBuffer.wrap(dm7.k(bArr, 48, 52)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order7, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i7 = order7.getInt();
        if (i7 == 0) {
            i7 = bArr.length - 4;
        }
        ByteBuffer order8 = ByteBuffer.wrap(dm7.k(bArr, 52, 56)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order8, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        int i8 = order8.getInt();
        int length = i8 == 0 ? bArr.length - 4 : i8;
        cc0[] f2 = f(dm7.k(bArr, i, i2));
        cc0[] f3 = f(dm7.k(bArr, i2, i3));
        cc0[] f4 = f(dm7.k(bArr, i3, i4));
        cc0[] f5 = f(dm7.k(bArr, i4, i5));
        cc0[] f6 = f(dm7.k(bArr, i5, i6));
        cc0[] f7 = f(dm7.k(bArr, i6, i7));
        cc0[] f8 = f(dm7.k(bArr, i7, length));
        int i9 = ja.b[yb0.b.ordinal()];
        if (i9 == 1) {
            throw new sx1(sx1.a.INVALID_FILE_DATA, null, null, 6, null);
        } else if (i9 == 2) {
            int length2 = f6.length;
            int i10 = 0;
            while (true) {
                ec0 = null;
                if (i10 >= length2) {
                    cc0 = null;
                    break;
                }
                cc0 = f6[i10];
                if (pq7.a(cc0.b, "theme_class")) {
                    break;
                }
                i10++;
            }
            if (!(cc0 == null || (bArr2 = cc0.c) == null)) {
                try {
                    ec0 = ec0.g.a(new String(dm7.k(bArr2, 0, bArr2.length - 1), hd0.y.c()));
                } catch (Exception e) {
                }
            }
            if (ec0 != null) {
                int i11 = ja.f1732a[ec0.ordinal()];
                if (i11 == 1) {
                    return new qw1(ry1, yb0, f2, f3, f4, f5, f6, f7, f8);
                }
                if (i11 == 2) {
                    return new ow1(ry1, yb0, f2, f3, f4, f5, f6, f7, f8);
                }
                if (i11 == 3) {
                    return new tw1(ry1, yb0, f2, f3, f4, f5, f6, f7, f8);
                }
                throw new al7();
            }
            throw new sx1(sx1.a.INVALID_FILE_DATA, null, null, 6, null);
        } else if (i9 == 3) {
            return new ru1(ry1, yb0, f2, f3, f4, f5, f6, f7, f8);
        } else {
            throw new al7();
        }
    }
}
