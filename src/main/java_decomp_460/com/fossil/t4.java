package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t4 implements Parcelable.Creator<k5> {
    @DexIgnore
    public /* synthetic */ t4(kq7 kq7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public k5 createFromParcel(Parcel parcel) {
        x4 x4Var = x4.b;
        Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
        if (readParcelable != null) {
            return x4Var.a((BluetoothDevice) readParcelable);
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public k5[] newArray(int i) {
        return new k5[i];
    }
}
