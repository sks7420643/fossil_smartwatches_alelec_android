package com.fossil;

import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LinkedList<u5> f2144a; // = new LinkedList<>();
    @DexIgnore
    public u5 b;
    @DexIgnore
    public /* final */ k5 c;

    @DexIgnore
    public l4(k5 k5Var) {
        this.c = k5Var;
    }

    @DexIgnore
    public final void a(r5 r5Var) {
        LinkedList linkedList = new LinkedList(this.f2144a);
        this.f2144a.clear();
        u5 u5Var = this.b;
        if (u5Var != null) {
            u5Var.e(r5Var);
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            ((u5) it.next()).e(r5Var);
        }
    }

    @DexIgnore
    public final void b(u5 u5Var) {
        u5 poll;
        ky1 ky1 = ky1.DEBUG;
        ey1.a(u5Var.i);
        try {
            synchronized (this.f2144a) {
                this.f2144a.add(d(u5Var), u5Var);
                tl7 tl7 = tl7.f3441a;
            }
        } catch (IndexOutOfBoundsException e) {
            this.f2144a.add(u5Var);
        }
        if (this.b == null && (poll = this.f2144a.poll()) != null) {
            c(poll);
        }
    }

    @DexIgnore
    public final void c(u5 u5Var) {
        this.b = u5Var;
        if (u5Var != null) {
            u5Var.h = new h4(this);
            k5 k5Var = this.c;
            if (!u5Var.d) {
                ky1 ky1 = ky1.DEBUG;
                fd0<h7> j = u5Var.j();
                if (j != null) {
                    j.b(u5Var);
                }
                u5Var.d(k5Var);
            }
        }
    }

    @DexIgnore
    public final int d(u5 u5Var) {
        synchronized (this.f2144a) {
            int size = this.f2144a.size();
            for (int i = 0; i < size; i++) {
                if (u5Var.h().compareTo(this.f2144a.get(i).h()) > 0) {
                    return i;
                }
            }
            return this.f2144a.size();
        }
    }
}
