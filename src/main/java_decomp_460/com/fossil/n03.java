package com.fossil;

import com.fossil.e13;
import java.io.IOException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n03 implements r43 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ l03 f2444a;

    @DexIgnore
    public n03(l03 l03) {
        h13.f(l03, "output");
        l03 l032 = l03;
        this.f2444a = l032;
        l032.f2126a = this;
    }

    @DexIgnore
    public static n03 g(l03 l03) {
        n03 n03 = l03.f2126a;
        return n03 != null ? n03 : new n03(l03);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void a(int i, List<?> list, f33 f33) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            d(i, list.get(i2), f33);
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void b(int i, Object obj, f33 f33) throws IOException {
        this.f2444a.q(i, (m23) obj, f33);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final <K, V> void c(int i, h23<K, V> h23, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.f2444a.m(i, 2);
            this.f2444a.O(e23.a(h23, entry.getKey(), entry.getValue()));
            e23.b(this.f2444a, h23, entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void d(int i, Object obj, f33 f33) throws IOException {
        l03 l03 = this.f2444a;
        l03.m(i, 3);
        f33.b((m23) obj, l03.f2126a);
        l03.m(i, 4);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void e(int i, List<?> list, f33 f33) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, list.get(i2), f33);
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void f(int i, xz2 xz2) throws IOException {
        this.f2444a.o(i, xz2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final int zza() {
        return e13.f.k;
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i) throws IOException {
        this.f2444a.m(i, 3);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, double d) throws IOException {
        this.f2444a.k(i, d);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, float f) throws IOException {
        this.f2444a.l(i, f);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, int i2) throws IOException {
        this.f2444a.k0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, long j) throws IOException {
        this.f2444a.n(i, j);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof xz2) {
            this.f2444a.R(i, (xz2) obj);
        } else {
            this.f2444a.p(i, (m23) obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, String str) throws IOException {
        this.f2444a.r(i, str);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof w13) {
            w13 w13 = (w13) list;
            while (i2 < list.size()) {
                Object zzb = w13.zzb(i2);
                if (zzb instanceof String) {
                    this.f2444a.r(i, (String) zzb);
                } else {
                    this.f2444a.o(i, (xz2) zzb);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.r(i, list.get(i2));
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.l0(list.get(i4).intValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.j(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.P(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zza(int i, boolean z) throws IOException {
        this.f2444a.s(i, z);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzb(int i) throws IOException {
        this.f2444a.m(i, 4);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzb(int i, int i2) throws IOException {
        this.f2444a.P(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzb(int i, long j) throws IOException {
        this.f2444a.Z(i, j);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzb(int i, List<xz2> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.f2444a.o(i, list.get(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.x0(list.get(i4).intValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.f0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.k0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzc(int i, int i2) throws IOException {
        this.f2444a.P(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzc(int i, long j) throws IOException {
        this.f2444a.n(i, j);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.e0(list.get(i4).longValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.n(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzd(int i, int i2) throws IOException {
        this.f2444a.k0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzd(int i, long j) throws IOException {
        this.f2444a.Z(i, j);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.j0(list.get(i4).longValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.n(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zze(int i, int i2) throws IOException {
        this.f2444a.Y(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zze(int i, long j) throws IOException {
        this.f2444a.Q(i, j);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.s0(list.get(i4).longValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.a0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.Z(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzf(int i, int i2) throws IOException {
        this.f2444a.g0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.A(list.get(i4).floatValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.i(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.l(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.z(list.get(i4).doubleValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.h(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.k(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.C0(list.get(i4).intValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.j(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.P(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.L(list.get(i4).booleanValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.y(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.s(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += l03.p0(list.get(i4).intValue());
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.O(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.Y(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.A0(list.get(i4).intValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.f0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.k0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.w0(list.get(i4).longValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.a0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.Z(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.t0(list.get(i4).intValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.X(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.g0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.r43
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2444a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = l03.o0(list.get(i4).longValue()) + i3;
            }
            this.f2444a.O(i3);
            while (i2 < list.size()) {
                this.f2444a.S(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2444a.Q(i, list.get(i2).longValue());
            i2++;
        }
    }
}
