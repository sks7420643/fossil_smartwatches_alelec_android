package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentActivity;
import com.fossil.x37;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tx6 extends ey6 implements gq4 {
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public g37<ba5> h;
    @DexIgnore
    public hy6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final tx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            tx6 tx6 = new tx6();
            tx6.setArguments(bundle);
            return tx6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity b;

        @DexIgnore
        public b(FragmentActivity fragmentActivity) {
            this.b = fragmentActivity;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b, 2130772009);
            FlexibleTextView flexibleTextView = (FlexibleTextView) this.b.findViewById(2131362546);
            if (flexibleTextView != null) {
                flexibleTextView.startAnimation(loadAnimation);
            }
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) this.b.findViewById(2131362405);
            if (flexibleTextView2 != null) {
                flexibleTextView2.startAnimation(loadAnimation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tx6 b;

        @DexIgnore
        public c(tx6 tx6) {
            this.b = tx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            tx6.K6(this.b).r(1);
        }
    }

    @DexIgnore
    public static final /* synthetic */ hy6 K6(tx6 tx6) {
        hy6 hy6 = tx6.i;
        if (hy6 != null) {
            return hy6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(hy6 hy6) {
        pq7.c(hy6, "presenter");
        this.i = hy6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        FragmentActivity activity;
        if (z || (activity = getActivity()) == null) {
            return null;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(activity, 2130772005);
        if (loadAnimation == null) {
            return loadAnimation;
        }
        loadAnimation.setAnimationListener(new b(activity));
        return loadAnimation;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ba5 ba5 = (ba5) aq0.f(layoutInflater, 2131558605, viewGroup, false, A6());
        this.h = new g37<>(this, ba5);
        pq7.b(ba5, "binding");
        return ba5.n();
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        RTLImageView rTLImageView;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<ba5> g37 = this.h;
        if (g37 != null) {
            ba5 a2 = g37.a();
            if (!(a2 == null || (rTLImageView = a2.s) == null)) {
                rTLImageView.setOnClickListener(new c(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                g37<ba5> g372 = this.h;
                if (g372 != null) {
                    ba5 a3 = g372.a();
                    if (a3 != null && (dashBar = a3.u) != null) {
                        x37.a aVar = x37.f4036a;
                        pq7.b(dashBar, "this");
                        aVar.d(dashBar, z, 500);
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ey6, com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
