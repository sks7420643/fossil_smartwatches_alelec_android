package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jk5 {
    @DexIgnore
    public static float a(float f) {
        return ((float) Math.round(((((double) f) * 1.8d) + 32.0d) * 100.0d)) / 100.0f;
    }

    @DexIgnore
    public static cl7<Integer, Integer> b(float f) {
        int round = (int) Math.round(((double) f) / 2.54d);
        return new cl7<>(Integer.valueOf(round / 12), Integer.valueOf(round % 12));
    }

    @DexIgnore
    public static float c(float f) {
        return ((float) Math.round((((double) (f - 32.0f)) * 0.5555555555555556d) * 100.0d)) / 100.0f;
    }

    @DexIgnore
    public static float d(float f, float f2) {
        return i((12.0f * f) + f2);
    }

    @DexIgnore
    public static int e(int i) {
        return i * 12;
    }

    @DexIgnore
    public static float f(float f) {
        return f / 100.0f;
    }

    @DexIgnore
    public static float g(float f) {
        return 0.001f * f;
    }

    @DexIgnore
    public static float h(float f) {
        return 0.00220462f * f;
    }

    @DexIgnore
    public static float i(float f) {
        return 2.54f * f;
    }

    @DexIgnore
    public static float j(float f) {
        return 1000.0f * f;
    }

    @DexIgnore
    public static float k(float f) {
        return 0.001f * f;
    }

    @DexIgnore
    public static float l(float f) {
        return 6.21371E-4f * f;
    }

    @DexIgnore
    public static float m(float f) {
        return 453.592f * f;
    }
}
