package com.fossil;

import android.util.Base64;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt4 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ String f3465a;
    @DexIgnore
    public /* final */ rt4 b;
    @DexIgnore
    public /* final */ st4 c;
    @DexIgnore
    public /* final */ on5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(t.h(), t2.h());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, 0, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, 0, 0, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(null, 0, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.E(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.G(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.H(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(tt4 tt4, qn7 qn7) {
            super(qn7);
            this.this$0 = tt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.I(null, null, this);
        }
    }

    @DexIgnore
    public tt4(rt4 rt4, st4 st4, on5 on5) {
        pq7.c(rt4, "local");
        pq7.c(st4, "remote");
        pq7.c(on5, "shared");
        this.b = rt4;
        this.c = st4;
        this.d = on5;
        String simpleName = tt4.class.getSimpleName();
        pq7.b(simpleName, "ChallengeRepository::class.java.simpleName");
        this.f3465a = simpleName;
    }

    @DexIgnore
    public static /* synthetic */ Object l(tt4 tt4, String str, String str2, String str3, Integer num, qn7 qn7, int i2, Object obj) {
        Integer num2 = null;
        String str4 = (i2 & 2) != 0 ? null : str2;
        String str5 = (i2 & 4) != 0 ? null : str3;
        if ((i2 & 8) == 0) {
            num2 = num;
        }
        return tt4.k(str, str4, str5, num2, qn7);
    }

    @DexIgnore
    public static /* synthetic */ Object n(tt4 tt4, String str, String str2, qn7 qn7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            str2 = "joined_challenge";
        }
        return tt4.m(str, str2, qn7);
    }

    @DexIgnore
    public static /* synthetic */ Object q(tt4 tt4, List list, int i2, int i3, boolean z, qn7 qn7, int i4, Object obj) {
        return tt4.p(list, i2, i3, (i4 & 8) != 0 ? true : z, qn7);
    }

    @DexIgnore
    public static /* synthetic */ Object s(tt4 tt4, int i2, int i3, qn7 qn7, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i2 = 5;
        }
        if ((i4 & 2) != 0) {
            i3 = 0;
        }
        return tt4.r(i2, i3, qn7);
    }

    @DexIgnore
    public final LiveData<List<bt4>> A() {
        return this.b.p();
    }

    @DexIgnore
    public final long B(ps4 ps4) {
        pq7.c(ps4, "challenge");
        return this.b.r(ps4);
    }

    @DexIgnore
    public final void C(List<ms4> list) {
        pq7.c(list, "players");
        this.b.f();
        this.b.v(list);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(java.lang.String r7, int r8, java.lang.String r9, com.fossil.qn7<? super com.fossil.kz4<com.fossil.ps4>> r10) {
        /*
        // Method dump skipped, instructions count: 269
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.D(java.lang.String, int, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object E(java.lang.String r23, com.fossil.qn7<? super com.fossil.kz4<com.fossil.ps4>> r24) {
        /*
        // Method dump skipped, instructions count: 285
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.E(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object F(EncryptedData encryptedData, qn7<? super kz4<nt4>> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.f3465a;
        local.d(str, "processStepData - encryptedData=" + encryptedData);
        int ordinal = encryptedData.getEncryptMethod().ordinal();
        String encodeToString = Base64.encodeToString(encryptedData.getEncryptedData(), 2);
        pq7.b(encodeToString, "Base64.encodeToString(en\u2026ptedData, Base64.NO_WRAP)");
        return H(new ls4(0, ordinal, encodeToString, encryptedData.getKeyType().ordinal(), String.valueOf((int) encryptedData.getSequence())), qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object G(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.tt4.n
            if (r0 == 0) goto L_0x002e
            r0 = r7
            com.fossil.tt4$n r0 = (com.fossil.tt4.n) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r3 = r1.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x003d
            if (r0 != r5) goto L_0x0035
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.tt4 r0 = (com.fossil.tt4) r0
            com.fossil.el7.b(r3)
        L_0x002b:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x002d:
            return r0
        L_0x002e:
            com.fossil.tt4$n r0 = new com.fossil.tt4$n
            r0.<init>(r6, r7)
            r1 = r0
            goto L_0x0014
        L_0x0035:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003d:
            com.fossil.el7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = r6.f3465a
            java.lang.String r4 = "pushPendingStepData"
            r0.e(r3, r4)
            com.fossil.rt4 r0 = r6.b
            java.util.List r3 = r0.l()
            boolean r0 = r3.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x002b
            r0 = 0
            java.lang.Object r0 = r3.get(r0)
            com.fossil.ls4 r0 = (com.fossil.ls4) r0
            r1.L$0 = r6
            r1.L$1 = r3
            r1.label = r5
            java.lang.Object r0 = r6.H(r0, r1)
            if (r0 != r2) goto L_0x002b
            r0 = r2
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.G(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object H(com.fossil.ls4 r9, com.fossil.qn7<? super com.fossil.kz4<com.fossil.nt4>> r10) {
        /*
        // Method dump skipped, instructions count: 311
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.H(com.fossil.ls4, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object I(java.lang.String r7, java.lang.String[] r8, com.fossil.qn7<? super com.fossil.kz4<com.fossil.ps4>> r9) {
        /*
        // Method dump skipped, instructions count: 260
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.I(java.lang.String, java.lang.String[], com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void J(String str, boolean z) {
        pq7.c(str, "challengeId");
        this.d.I1(str, Boolean.valueOf(z));
    }

    @DexIgnore
    public final int K(bt4 bt4) {
        pq7.c(bt4, "historyChallenge");
        return this.b.w(bt4);
    }

    @DexIgnore
    public final ps4 a(String str) {
        pq7.c(str, "id");
        return this.b.a(str);
    }

    @DexIgnore
    public final LiveData<ps4> b(String str) {
        pq7.c(str, "id");
        return this.b.b(str);
    }

    @DexIgnore
    public final int c(String str) {
        pq7.c(str, "id");
        return this.b.c(str);
    }

    @DexIgnore
    public final void d() {
        this.b.d();
    }

    @DexIgnore
    public final void e() {
        this.b.e();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.fossil.ts4 r11, int r12, java.lang.String r13, com.fossil.qn7<? super com.fossil.kz4<com.fossil.ps4>> r14) {
        /*
        // Method dump skipped, instructions count: 295
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.f(com.fossil.ts4, int, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final ps4 g(String[] strArr, Date date) {
        pq7.c(strArr, "array");
        pq7.c(date, "date");
        return this.b.g(strArr, date);
    }

    @DexIgnore
    public final LiveData<ps4> h(String[] strArr, Date date) {
        pq7.c(strArr, "array");
        pq7.c(date, "date");
        return this.b.h(strArr, date);
    }

    @DexIgnore
    public final int i(String str) {
        pq7.c(str, "id");
        return this.b.i(str);
    }

    @DexIgnore
    public final ks4 j(String str) {
        pq7.c(str, "id");
        return this.b.k(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object k(java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.Integer r12, com.fossil.qn7<? super com.fossil.kz4<com.fossil.ps4>> r13) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.k(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m(java.lang.String r9, java.lang.String r10, com.fossil.qn7<? super com.fossil.kz4<com.fossil.ps4>> r11) {
        /*
        // Method dump skipped, instructions count: 297
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.m(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object o(java.lang.String[] r8, int r9, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.ps4>>> r10) {
        /*
        // Method dump skipped, instructions count: 340
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.o(java.lang.String[], int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object p(java.util.List<java.lang.String> r8, int r9, int r10, boolean r11, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.ks4>>> r12) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.p(java.util.List, int, int, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(int r8, int r9, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.bt4>>> r10) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.r(int, int, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object t(java.lang.String r7, java.lang.String[] r8, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.ms4>>> r9) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.t(java.lang.String, java.lang.String[], com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object u(com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.mt4>>> r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            boolean r0 = r7 instanceof com.fossil.tt4.i
            if (r0 == 0) goto L_0x004c
            r0 = r7
            com.fossil.tt4$i r0 = (com.fossil.tt4.i) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x004c
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x005a
            if (r4 != r5) goto L_0x0052
            java.lang.Object r0 = r0.L$0
            com.fossil.tt4 r0 = (com.fossil.tt4) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0098
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x006b
            java.util.List r1 = r0.get_items()
        L_0x003d:
            if (r1 != 0) goto L_0x006d
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x0044:
            com.fossil.kz4 r1 = new com.fossil.kz4
            r3 = 2
            r1.<init>(r0, r2, r3, r2)
            r0 = r1
        L_0x004b:
            return r0
        L_0x004c:
            com.fossil.tt4$i r0 = new com.fossil.tt4$i
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0052:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005a:
            com.fossil.el7.b(r1)
            com.fossil.st4 r1 = r6.c
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = r1.g(r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x004b
        L_0x006b:
            r1 = r2
            goto L_0x003d
        L_0x006d:
            java.util.Iterator r3 = r1.iterator()
        L_0x0071:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00c0
            java.lang.Object r0 = r3.next()
            com.fossil.mt4 r0 = (com.fossil.mt4) r0
            com.fossil.on5 r4 = r6.d
            com.fossil.ps4 r5 = r0.b()
            java.lang.String r5 = r5.f()
            java.lang.Boolean r4 = r4.p0(r5)
            java.lang.String r5 = "shared.isNewChallenge(it.challenge.id)"
            com.fossil.pq7.b(r4, r5)
            boolean r4 = r4.booleanValue()
            r0.d(r4)
            goto L_0x0071
        L_0x0098:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00b8
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r3 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00be
            java.lang.String r0 = r0.getMessage()
        L_0x00ac:
            com.fossil.kz4 r1 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r2.<init>(r3, r0)
            r1.<init>(r2)
            r0 = r1
            goto L_0x004b
        L_0x00b8:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        L_0x00be:
            r0 = r2
            goto L_0x00ac
        L_0x00c0:
            r0 = r1
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.u(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object v(com.fossil.qn7<? super com.fossil.kz4<com.fossil.pt4>> r8) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.v(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object w(com.fossil.qn7<? super java.util.List<com.fossil.ps4>> r9) {
        /*
            r8 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            r7 = 1
            boolean r0 = r9 instanceof com.fossil.tt4.k
            if (r0 == 0) goto L_0x004d
            r0 = r9
            com.fossil.tt4$k r0 = (com.fossil.tt4.k) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x004d
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x005b
            if (r4 != r7) goto L_0x0053
            java.lang.Object r0 = r0.L$0
            com.fossil.tt4 r0 = (com.fossil.tt4) r0
            com.fossil.el7.b(r1)
            r8 = r0
        L_0x0028:
            r0 = r1
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x0079
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x003d
            java.util.List r2 = r0.get_items()
        L_0x003d:
            if (r2 != 0) goto L_0x004c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r8.f3465a
            java.lang.String r3 = "getWatchDisplayChallenges - Success with null response"
            r0.e(r1, r3)
        L_0x004c:
            return r2
        L_0x004d:
            com.fossil.tt4$k r0 = new com.fossil.tt4$k
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0053:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005b:
            com.fossil.el7.b(r1)
            com.fossil.st4 r1 = r8.c
            r0.L$0 = r8
            r0.label = r7
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]
            r5 = 0
            java.lang.String r6 = "running"
            r4[r5] = r6
            java.lang.String r5 = "completed"
            r4[r7] = r5
            r5 = 5
            java.lang.Object r1 = r1.d(r4, r5, r0)
            if (r1 != r3) goto L_0x0028
            r2 = r3
            goto L_0x004c
        L_0x0079:
            boolean r1 = r0 instanceof com.fossil.hq5
            if (r1 == 0) goto L_0x00b4
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r8.f3465a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getWatchDisplayChallenges - errorCode: "
            r4.append(r5)
            com.fossil.hq5 r0 = (com.fossil.hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r5 = "message: "
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00b2
            java.lang.String r0 = r0.getMessage()
        L_0x00a7:
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r1.e(r3, r0)
            goto L_0x004c
        L_0x00b2:
            r0 = r2
            goto L_0x00a7
        L_0x00b4:
            com.fossil.al7 r0 = new com.fossil.al7
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tt4.w(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final List<ms4> x() {
        return this.b.m();
    }

    @DexIgnore
    public final LiveData<bt4> y(String str) {
        pq7.c(str, "challengeId");
        return this.b.n(str);
    }

    @DexIgnore
    public final bt4 z(String str) {
        pq7.c(str, "id");
        return this.b.o(str);
    }
}
