package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zr0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static AtomicBoolean f4517a; // = new AtomicBoolean(false);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends tr0 {
        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            os0.g(activity);
        }

        @DexIgnore
        @Override // com.fossil.tr0
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static void a(Context context) {
        if (!f4517a.getAndSet(true)) {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new a());
        }
    }
}
