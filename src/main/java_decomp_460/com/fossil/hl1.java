package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.un1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hl1 extends gl1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ Set<un1> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final hl1 a(byte[] bArr) throws IllegalArgumentException {
            boolean z = false;
            if (bArr.length == 3) {
                byte b = bArr[0];
                Set<un1> a2 = un1.e.a(bArr[0]);
                if (((byte) (((byte) 128) & bArr[1])) != 0) {
                    z = true;
                }
                byte b2 = bArr[1];
                byte b3 = (byte) (bArr[1] & 63);
                byte b4 = (byte) (bArr[2] & 31);
                return z ? new ol1(b4, b3, a2) : (a2.isEmpty() || a2.size() == un1.values().length) ? new kl1(b4, b3, null) : new kl1(b4, b3, (un1) pm7.E(a2));
            }
            throw new IllegalArgumentException(e.b(e.e("Size("), bArr.length, " not equal to 3."));
        }

        @DexIgnore
        /* renamed from: b */
        public hl1 createFromParcel(Parcel parcel) {
            boolean z = true;
            parcel.readInt();
            byte readByte = parcel.readByte();
            byte readByte2 = parcel.readByte();
            un1.a aVar = un1.e;
            String[] createStringArray = parcel.createStringArray();
            if (createStringArray != null) {
                pq7.b(createStringArray, "parcel.createStringArray()!!");
                LinkedHashSet linkedHashSet = new LinkedHashSet(em7.d0(aVar.b(createStringArray)));
                boolean z2 = parcel.readInt() != 0;
                boolean z3 = parcel.readInt() != 0;
                if (parcel.readInt() == 0) {
                    z = false;
                }
                if (z2) {
                    return new ol1(readByte, readByte2, linkedHashSet, z3, z);
                }
                return new kl1(readByte, readByte2, !linkedHashSet.isEmpty() ? linkedHashSet.size() == un1.values().length ? null : (un1) pm7.E(linkedHashSet) : null);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hl1[] newArray(int i) {
            return new hl1[i];
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.util.Set<? extends com.fossil.un1> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hl1(byte b, byte b2, Set<? extends un1> set, boolean z, boolean z2, boolean z3) throws IllegalArgumentException {
        super(r8.FIRE_TIME);
        boolean z4 = true;
        this.c = (byte) b;
        this.d = (byte) b2;
        this.e = set;
        this.f = z;
        this.g = z2;
        this.h = z3;
        if (b >= 0 && 23 >= b) {
            byte b3 = this.d;
            if (!((b3 < 0 || 59 < b3) ? false : z4)) {
                throw new IllegalArgumentException(e.c(e.e("minute("), this.d, ") is out of range ", "[0, 59]."));
            }
            return;
        }
        throw new IllegalArgumentException(e.c(e.e("hour("), this.c, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public byte[] b() {
        return new byte[]{(byte) ((this.h ? (byte) 128 : (byte) 0) | g80.a(this.e)), (byte) (((byte) ((this.f ? (byte) 128 : (byte) 0) | (this.g ? (byte) 64 : (byte) 0))) | this.d), this.c};
    }

    @DexIgnore
    public final Set<un1> c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            hl1 hl1 = (hl1) obj;
            return this.c == hl1.c && this.d == hl1.d && g80.a(this.e) == g80.a(hl1.e) && this.f == hl1.f && this.g == hl1.g && this.h == hl1.h;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
    }

    @DexIgnore
    public final byte getHour() {
        return this.c;
    }

    @DexIgnore
    public final byte getMinute() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public int hashCode() {
        int hashCode = super.hashCode();
        byte b = this.c;
        byte b2 = this.d;
        int hashCode2 = this.e.hashCode();
        int hashCode3 = Boolean.valueOf(this.f).hashCode();
        return (((((((((((hashCode * 31) + b) * 31) + b2) * 31) + hashCode2) * 31) + hashCode3) * 31) + Boolean.valueOf(this.g).hashCode()) * 31) + Boolean.valueOf(this.h).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1, com.fossil.gl1
    public JSONObject toJSONObject() {
        JSONObject k = g80.k(g80.k(super.toJSONObject(), jd0.A, Byte.valueOf(this.c)), jd0.B, Byte.valueOf(this.d));
        jd0 jd0 = jd0.C;
        Set<un1> set = this.e;
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = set.iterator();
        while (it.hasNext()) {
            jSONArray.put(ey1.a(it.next()));
        }
        return g80.k(g80.k(g80.k(g80.k(k, jd0, jSONArray), jd0.D, Boolean.valueOf(this.f)), jd0.F, Boolean.valueOf(this.h)), jd0.E, Boolean.valueOf(this.g));
    }

    @DexIgnore
    @Override // com.fossil.gl1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            Object[] array = this.e.toArray(new un1[0]);
            if (array != null) {
                un1[] un1Arr = (un1[]) array;
                ArrayList arrayList = new ArrayList(un1Arr.length);
                for (un1 un1 : un1Arr) {
                    arrayList.add(un1.name());
                }
                Object[] array2 = arrayList.toArray(new String[0]);
                if (array2 != null) {
                    parcel.writeStringArray((String[]) array2);
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                }
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        if (parcel != null) {
            parcel.writeInt(this.f ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.g ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.h ? 1 : 0);
        }
    }
}
