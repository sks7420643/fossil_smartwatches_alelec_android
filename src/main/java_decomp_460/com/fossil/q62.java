package com.fossil;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.ac2;
import com.fossil.l72;
import com.fossil.m62;
import com.fossil.m62.d;
import com.fossil.p72;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q62<O extends m62.d> implements s62<O> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2925a;
    @DexIgnore
    public /* final */ m62<O> b;
    @DexIgnore
    public /* final */ O c;
    @DexIgnore
    public /* final */ g72<O> d;
    @DexIgnore
    public /* final */ Looper e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ r62 g;
    @DexIgnore
    public /* final */ u72 h;
    @DexIgnore
    public /* final */ l72 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a c; // = new C0194a().a();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ u72 f2926a;
        @DexIgnore
        public /* final */ Looper b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.q62$a$a")
        /* renamed from: com.fossil.q62$a$a  reason: collision with other inner class name */
        public static class C0194a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public u72 f2927a;
            @DexIgnore
            public Looper b;

            @DexIgnore
            public a a() {
                if (this.f2927a == null) {
                    this.f2927a = new f72();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new a(this.f2927a, this.b);
            }

            @DexIgnore
            public C0194a b(Looper looper) {
                rc2.l(looper, "Looper must not be null.");
                this.b = looper;
                return this;
            }

            @DexIgnore
            public C0194a c(u72 u72) {
                rc2.l(u72, "StatusExceptionMapper must not be null.");
                this.f2927a = u72;
                return this;
            }
        }

        @DexIgnore
        public a(u72 u72, Account account, Looper looper) {
            this.f2926a = u72;
            this.b = looper;
        }
    }

    @DexIgnore
    public q62(Activity activity, m62<O> m62, O o, a aVar) {
        rc2.l(activity, "Null activity is not permitted.");
        rc2.l(m62, "Api must not be null.");
        rc2.l(aVar, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.f2925a = activity.getApplicationContext();
        r(activity);
        this.b = m62;
        this.c = o;
        this.e = aVar.b;
        this.d = g72.b(m62, o);
        this.g = new p92(this);
        l72 o2 = l72.o(this.f2925a);
        this.i = o2;
        this.f = o2.r();
        this.h = aVar.f2926a;
        if (!(activity instanceof GoogleApiActivity)) {
            b82.q(activity, this.i, this.d);
        }
        this.i.i(this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q62(android.app.Activity r3, com.fossil.m62<O> r4, O r5, com.fossil.u72 r6) {
        /*
            r2 = this;
            com.fossil.q62$a$a r0 = new com.fossil.q62$a$a
            r0.<init>()
            r0.c(r6)
            android.os.Looper r1 = r3.getMainLooper()
            r0.b(r1)
            com.fossil.q62$a r0 = r0.a()
            r2.<init>(r3, r4, r5, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q62.<init>(android.app.Activity, com.fossil.m62, com.fossil.m62$d, com.fossil.u72):void");
    }

    @DexIgnore
    public q62(Context context, m62<O> m62, Looper looper) {
        rc2.l(context, "Null context is not permitted.");
        rc2.l(m62, "Api must not be null.");
        rc2.l(looper, "Looper must not be null.");
        this.f2925a = context.getApplicationContext();
        r(context);
        this.b = m62;
        this.c = null;
        this.e = looper;
        this.d = g72.c(m62);
        this.g = new p92(this);
        l72 o = l72.o(this.f2925a);
        this.i = o;
        this.f = o.r();
        this.h = new f72();
    }

    @DexIgnore
    public q62(Context context, m62<O> m62, O o, a aVar) {
        rc2.l(context, "Null context is not permitted.");
        rc2.l(m62, "Api must not be null.");
        rc2.l(aVar, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.f2925a = context.getApplicationContext();
        r(context);
        this.b = m62;
        this.c = o;
        this.e = aVar.b;
        this.d = g72.b(m62, o);
        this.g = new p92(this);
        l72 o2 = l72.o(this.f2925a);
        this.i = o2;
        this.f = o2.r();
        this.h = aVar.f2926a;
        this.i.i(this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q62(android.content.Context r2, com.fossil.m62<O> r3, O r4, com.fossil.u72 r5) {
        /*
            r1 = this;
            com.fossil.q62$a$a r0 = new com.fossil.q62$a$a
            r0.<init>()
            r0.c(r5)
            com.fossil.q62$a r0 = r0.a()
            r1.<init>(r2, r3, r4, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q62.<init>(android.content.Context, com.fossil.m62, com.fossil.m62$d, com.fossil.u72):void");
    }

    @DexIgnore
    public static String r(Object obj) {
        if (mf2.m()) {
            try {
                return (String) Context.class.getMethod("getFeatureId", new Class[0]).invoke(obj, new Object[0]);
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.s62
    public g72<O> a() {
        return this.d;
    }

    @DexIgnore
    public r62 b() {
        return this.g;
    }

    @DexIgnore
    public ac2.a c() {
        Account m;
        GoogleSignInAccount b2;
        GoogleSignInAccount b3;
        ac2.a aVar = new ac2.a();
        O o = this.c;
        if (!(o instanceof m62.d.b) || (b3 = ((m62.d.b) o).b()) == null) {
            O o2 = this.c;
            m = o2 instanceof m62.d.a ? ((m62.d.a) o2).m() : null;
        } else {
            m = b3.m();
        }
        aVar.c(m);
        O o3 = this.c;
        aVar.a((!(o3 instanceof m62.d.b) || (b2 = ((m62.d.b) o3).b()) == null) ? Collections.emptySet() : b2.o0());
        aVar.d(this.f2925a.getClass().getName());
        aVar.e(this.f2925a.getPackageName());
        return aVar;
    }

    @DexIgnore
    public <A extends m62.b, T extends i72<? extends z62, A>> T d(T t) {
        o(0, t);
        return t;
    }

    @DexIgnore
    public <TResult, A extends m62.b> nt3<TResult> e(w72<A, TResult> w72) {
        return q(0, w72);
    }

    @DexIgnore
    @Deprecated
    public <A extends m62.b, T extends s72<A, ?>, U extends y72<A, ?>> nt3<Void> f(T t, U u) {
        rc2.k(t);
        rc2.k(u);
        rc2.l(t.b(), "Listener has already been released.");
        rc2.l(u.a(), "Listener has already been released.");
        rc2.b(t.b().equals(u.a()), "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.i.f(this, t, u, lb2.b);
    }

    @DexIgnore
    public nt3<Boolean> g(p72.a<?> aVar) {
        rc2.l(aVar, "Listener key cannot be null.");
        return this.i.e(this, aVar);
    }

    @DexIgnore
    public <A extends m62.b, T extends i72<? extends z62, A>> T h(T t) {
        o(1, t);
        return t;
    }

    @DexIgnore
    public final m62<O> i() {
        return this.b;
    }

    @DexIgnore
    public O j() {
        return this.c;
    }

    @DexIgnore
    public Context k() {
        return this.f2925a;
    }

    @DexIgnore
    public final int l() {
        return this.f;
    }

    @DexIgnore
    public Looper m() {
        return this.e;
    }

    @DexIgnore
    public m62.f n(Looper looper, l72.a<O> aVar) {
        return this.b.d().c(this.f2925a, looper, c().b(), this.c, aVar, aVar);
    }

    @DexIgnore
    public final <A extends m62.b, T extends i72<? extends z62, A>> T o(int i2, T t) {
        t.t();
        this.i.j(this, i2, t);
        return t;
    }

    @DexIgnore
    public x92 p(Context context, Handler handler) {
        return new x92(context, handler, c().b());
    }

    @DexIgnore
    public final <TResult, A extends m62.b> nt3<TResult> q(int i2, w72<A, TResult> w72) {
        ot3 ot3 = new ot3();
        this.i.k(this, i2, w72, ot3, this.h);
        return ot3.a();
    }
}
