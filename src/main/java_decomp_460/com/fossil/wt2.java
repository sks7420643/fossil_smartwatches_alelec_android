package com.fossil;

import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ Object h;
    @DexIgnore
    public /* final */ /* synthetic */ boolean i;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wt2(zs2 zs2, String str, String str2, Object obj, boolean z) {
        super(zs2);
        this.j = zs2;
        this.f = str;
        this.g = str2;
        this.h = obj;
        this.i = z;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.j.h.setUserProperty(this.f, this.g, tg2.n(this.h), this.i, this.b);
    }
}
