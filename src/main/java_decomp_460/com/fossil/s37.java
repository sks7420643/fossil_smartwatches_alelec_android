package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.FragmentManager;
import com.fossil.t47;
import com.fossil.w47;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.network.utils.ReturnCodeRangeChecker;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.NumberPickerLarge;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3202a;
    @DexIgnore
    public static /* final */ String b; // = b;
    @DexIgnore
    public static /* final */ s37 c; // = new s37();

    /*
    static {
        String simpleName = s37.class.getSimpleName();
        pq7.b(simpleName, "DialogUtils::class.java.simpleName");
        f3202a = simpleName;
    }
    */

    @DexIgnore
    public final void A(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886879));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886878));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886877));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886876));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "FEEDBACK_CONFIRM");
    }

    @DexIgnore
    public final void A0(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "description");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
        fVar.e(2131363317, str);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "LIMIT_WARNING");
    }

    @DexIgnore
    public final void B(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886735));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886734));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "GOAL_TRACKING_ADD_FUTURE_ERROR");
    }

    @DexIgnore
    public final void B0(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "message");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363317, str);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886582));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886793));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "WATCH_FACE_CANNOT_OPEN");
    }

    @DexIgnore
    public final void C(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887398));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886901));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "GENERAL_ERROR");
    }

    @DexIgnore
    public final void C0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558483);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886338));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886336));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886337));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "FINDING_FRIEND");
    }

    @DexIgnore
    public final void D(FragmentManager fragmentManager) {
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886832));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886793));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void E(FragmentManager fragmentManager, String str, String str2) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "title");
        pq7.c(str2, "description");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, str);
        fVar.e(2131363317, str2);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "GENERAL_WARNING");
    }

    @DexIgnore
    public final void F(FragmentManager fragmentManager, int i, int i2, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "emailAddress");
        DashbarData dashbarData = new DashbarData(2131362968, i, i2);
        t47.f fVar = new t47.f(2131558557);
        fVar.e(2131362423, str);
        fVar.a(dashbarData);
        fVar.b(2131362686);
        fVar.b(2131361942);
        fVar.j(true);
        fVar.h(false);
        fVar.i(R.color.transparent);
        pq7.b(fVar, "AlertDialogFragment.Buil\u2026olor(R.color.transparent)");
        t47 f = fVar.f("EMAIL_OTP_VERIFICATION");
        pq7.b(f, "builder.create(EMAIL_OTP_VERIFICATION)");
        f.setStyle(0, 2131951629);
        f.show(fragmentManager, "EMAIL_OTP_VERIFICATION");
    }

    @DexIgnore
    public final void G(FragmentManager fragmentManager, int i, int i2, int i3, String[] strArr) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(strArr, "displayedValues");
        t47.f fVar = new t47.f(2131558478);
        fVar.b(2131362260);
        fVar.b(2131363009);
        fVar.c(2131362892, 1, 12, i);
        fVar.c(2131362893, 0, 59, i2);
        fVar.d(2131362896, 0, 1, i3, NumberPickerLarge.getTwoDigitFormatter(), strArr);
        fVar.k(fragmentManager, "GOAL_TRACKING_ADD");
    }

    @DexIgnore
    public final void H(FragmentManager fragmentManager, GoalTrackingData goalTrackingData) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(goalTrackingData, "goalTrackingData");
        Bundle bundle = new Bundle();
        bundle.putSerializable("GOAL_TRACKING_DELETE_BUNDLE", goalTrackingData);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886739));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886738));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886737));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886736));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, "GOAL_TRACKING_DELETE", bundle);
    }

    @DexIgnore
    public final void I(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886875));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886872));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886871));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886869));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "HAPPINESS_CONFIRM");
    }

    @DexIgnore
    public final void J(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886517));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886365));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "COMMUTE_TIME_LOAD_LOCATION_FAIL");
    }

    @DexIgnore
    public final void K(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "description");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, str);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886392));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "LOCATION_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void L(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886367));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886365));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void M(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886366));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886365));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void N(FragmentManager fragmentManager) {
        t47.f fVar = new t47.f(2131558488);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886794));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886793));
        fVar.b(2131363373);
        fVar.h(false);
        fVar.k(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void O(FragmentManager fragmentManager, int i, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(f3202a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                D(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        N(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            D(fragmentManager);
                            return;
                        } else if (str != null) {
                            x(str, fragmentManager);
                            return;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
                o0(fragmentManager);
            } else {
                p0(fragmentManager);
            }
        }
    }

    @DexIgnore
    public final void P(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887110));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887109));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887108));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "CONFIRM_LOGOUT_ACCOUNT");
    }

    @DexIgnore
    public final void Q(FragmentManager fragmentManager) {
        t47.f fVar = new t47.f(2131558484);
        fVar.b(2131363373);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886794));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886796));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886797));
        fVar.b(2131363291);
        fVar.k(fragmentManager, "NO_INTERNET_CONNECTION");
    }

    @DexIgnore
    public final void R(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c().getApplicationContext(), 2131886468));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886831));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "");
    }

    @DexIgnore
    public final void S(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886899);
        pq7.b(c2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886928);
        pq7.b(c3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        U(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void T(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886913);
        pq7.b(c2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886806);
        pq7.b(c3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        U(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void U(FragmentManager fragmentManager, String str, String str2) {
        t47.f fVar = new t47.f(2131558484);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363410, str);
        fVar.e(2131363317, str2);
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887075));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.k(fragmentManager, "REQUEST_OPEN_LOCATION_SERVICE");
    }

    @DexIgnore
    public final void V(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886885));
        fVar.b(2131363373);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.h(false);
        fVar.k(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void W(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, Constants.ACTIVITY);
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887398));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886995));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "WRONG_FORMAT_PASSWORD");
    }

    @DexIgnore
    public final void X(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887398));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886901));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "PROCESS_IMAGE_ERROR");
    }

    @DexIgnore
    public final void Y(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886126));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886880));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "QUICK_RESPONSE_WARNING");
    }

    @DexIgnore
    public final void Z(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886083));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886392));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "MAX_NUMBER_OF_ALARMS");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.b(2131363373);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886159));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886158));
        fVar.k(fragmentManager, "NOTIFICATION_WARNING");
    }

    @DexIgnore
    public final void a0(FragmentManager fragmentManager, String str, int i, int i2, i06 i06) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "name");
        pq7.c(i06, "appWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_APPWRAPPER", i06);
        t47.f fVar = new t47.f(2131558482);
        hr7 hr7 = hr7.f1520a;
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886153);
        pq7.b(c2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{str, String.valueOf(i), String.valueOf(i2)}, 3));
        pq7.b(format, "java.lang.String.format(format, *args)");
        fVar.e(2131363317, format);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886152));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886151));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, "CONFIRM_REASSIGN_APP", bundle);
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558482);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886174));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886173));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886172));
        fVar.k(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void b0(FragmentManager fragmentManager, j06 j06, int i, int i2) {
        String displayName;
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(j06, "contactWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER", j06);
        Contact contact = j06.getContact();
        if (contact == null || contact.getContactId() != -100) {
            Contact contact2 = j06.getContact();
            if (contact2 == null || contact2.getContactId() != -200) {
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    displayName = contact3.getDisplayName();
                    pq7.b(displayName, "contactWrapper.contact!!.displayName");
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                displayName = um5.c(PortfolioApp.h0.c(), 2131886155);
                pq7.b(displayName, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
            }
        } else {
            displayName = um5.c(PortfolioApp.h0.c(), 2131886154);
            pq7.b(displayName, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
        }
        t47.f fVar = new t47.f(2131558482);
        hr7 hr7 = hr7.f1520a;
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886153);
        pq7.b(c2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{displayName, Integer.valueOf(i), Integer.valueOf(i2)}, 3));
        pq7.b(format, "java.lang.String.format(format, *args)");
        fVar.e(2131363317, format);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886152));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886151));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, "CONFIRM_REASSIGN_CONTACT", bundle);
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886828));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886826));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886821));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886819));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.h(false);
        t47 k = fVar.k(fragmentManager, "ASK_TO_CANCEL_WORKOUT");
        pq7.b(k, "AlertDialogFragment.Buil\u2026r, ASK_TO_CANCEL_WORKOUT)");
        k.setCancelable(false);
    }

    @DexIgnore
    public final void c0(FragmentManager fragmentManager, ContactGroup contactGroup) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(contactGroup, "contactGroup");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE", contactGroup);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887545));
        hr7 hr7 = hr7.f1520a;
        String c2 = um5.c(PortfolioApp.h0.c(), 2131887546);
        pq7.b(c2, "LanguageHelper.getString\u2026move_contact_description)");
        Contact contact = contactGroup.getContacts().get(0);
        pq7.b(contact, "contactGroup.contacts[0]");
        String format = String.format(c2, Arrays.copyOf(new Object[]{contact.getDisplayName()}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        fVar.e(2131363317, format);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887181));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886545));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, "CONFIRM_REMOVE_CONTACT", bundle);
    }

    @DexIgnore
    public final void d(FragmentManager fragmentManager, Bundle bundle, String str, String str2) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(bundle, "bundle");
        pq7.c(str, "type");
        pq7.c(str2, "message");
        t47.f fVar = new t47.f(2131558483);
        fVar.e(2131363317, str2);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886324));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886869));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, str, bundle);
    }

    @DexIgnore
    public final void d0(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "deviceName");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887183));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887182));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887181));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887180));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "CONFIRM_REMOVE_DEVICE");
    }

    @DexIgnore
    public final void e(FragmentManager fragmentManager, Bundle bundle) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(bundle, "bundle");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886567));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886566));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886563));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886562));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, "DELETE_PHOTO_BACKGROUND", bundle);
    }

    @DexIgnore
    public final void e0(String str, FragmentManager fragmentManager) {
        pq7.c(str, "serial");
        pq7.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886824));
        hr7 hr7 = hr7.f1520a;
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886825);
        pq7.b(c2, "LanguageHelper.getString\u2026AnActiveDeviceIsDisabled)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{str}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        fVar.e(2131363317, format);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886822));
        fVar.b(2131363373);
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886820));
        fVar.b(2131363291);
        fVar.m(fragmentManager, "REMOVE_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final String f() {
        return b;
    }

    @DexIgnore
    public final void f0(String str, FragmentManager fragmentManager) {
        pq7.c(str, "serial");
        pq7.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886818));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886817));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886816));
        fVar.b(2131363373);
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886815));
        fVar.b(2131363291);
        fVar.m(fragmentManager, "REMOVE_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void g(Integer num, String str, FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        if (num == null) {
            C(fragmentManager);
        } else {
            n0(num.intValue(), str, fragmentManager);
        }
    }

    @DexIgnore
    public final void g0(FragmentManager fragmentManager, String str) {
        t47.f fVar = new t47.f(2131558484);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363410, str);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886802));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886803));
        fVar.k(fragmentManager, b);
    }

    @DexIgnore
    public final void h(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886320));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886325));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886324));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886323));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "LEAVE_CHALLENGE");
    }

    @DexIgnore
    public final void h0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887319));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887584));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887075));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.k(fragmentManager, "REQUEST_CONTACT_PHONE_SMS_PERMISSION");
    }

    @DexIgnore
    public final void i(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887398));
        fVar.e(2131363317, "Sorry, you are required to log out as you are using unauthorized app");
        fVar.h(false);
        fVar.k(fragmentManager, "AA_WARNING");
    }

    @DexIgnore
    public final void i0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886830);
        pq7.b(c2, "LanguageHelper.getString\u2026ourSmartwatchsFindDevice)");
        g0(fragmentManager, c2);
    }

    @DexIgnore
    public final void j(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886874));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886873));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886870));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886868));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "APP_RATING_CONFIRM");
    }

    @DexIgnore
    public final void j0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886927);
        pq7.b(c2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886928);
        pq7.b(c3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        l0(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void k(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, Constants.ACTIVITY);
        t47.f fVar = new t47.f(2131558484);
        fVar.b(2131363291);
        fVar.b(2131363373);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886898));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886929));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887075));
        fVar.h(false);
        fVar.k(fragmentManager, "BLUETOOTH_OFF");
    }

    @DexIgnore
    public final void k0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886927);
        pq7.b(c2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String c3 = um5.c(PortfolioApp.h0.c(), 2131886806);
        pq7.b(c3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        l0(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void l(FragmentManager fragmentManager, int i) {
        pq7.c(fragmentManager, "fragmentManager");
        w47.k D6 = w47.D6();
        D6.e(0);
        D6.b(false);
        D6.d(i);
        D6.c(-65536);
        D6.f(false);
        D6.a().show(fragmentManager, "COLOR_PICKER_DIALOG");
    }

    @DexIgnore
    public final void l0(FragmentManager fragmentManager, String str, String str2) {
        t47.f fVar = new t47.f(2131558482);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, str2);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886798));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886799));
        fVar.k(fragmentManager, "REQUEST_LOCATION_SERVICE_PERMISSION");
    }

    @DexIgnore
    public final void m(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "watchFaceId");
        Bundle bundle = new Bundle();
        bundle.putString("WATCH_FACE_ID", str);
        t47.f fVar = new t47.f(2131558484);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886565));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886563));
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886564));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886562));
        fVar.m(fragmentManager, "DELETE_MY_FACE", bundle);
    }

    @DexIgnore
    public final void m0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886981));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "RESET_PASS_SUCCESS");
    }

    @DexIgnore
    public final void n(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558482);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887392));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887499));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887320));
        fVar.k(fragmentManager, "DELETE_THEME");
    }

    @DexIgnore
    public final void n0(int i, String str, FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(f3202a, "Response is OK, no need to show error message");
            return;
        }
        switch (i) {
            case 401:
                return;
            case MFNetworkReturnCode.CLIENT_TIMEOUT /* 408 */:
                D(fragmentManager);
                return;
            case MFNetworkReturnCode.RATE_LIMIT_EXEEDED /* 429 */:
                p0(fragmentManager);
                return;
            case 500:
            case 503:
            case 504:
                o0(fragmentManager);
                return;
            case 601:
                Q(fragmentManager);
                return;
            case 400002:
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886235);
                pq7.b(c2, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c3 = um5.c(PortfolioApp.h0.c(), 2131886232);
                pq7.b(c3, "LanguageHelper.getString\u2026tTimeShouldBeGreaterThan)");
                E(fragmentManager, c2, c3);
                return;
            case 400605:
            case 400611:
                String c4 = um5.c(PortfolioApp.h0.c(), 2131886235);
                pq7.b(c4, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c5 = um5.c(PortfolioApp.h0.c(), 2131886227);
                pq7.b(c5, "LanguageHelper.getString\u2026eIsNotAvailableOrAlready)");
                E(fragmentManager, c4, c5);
                return;
            case 400609:
                String c6 = um5.c(PortfolioApp.h0.c(), 2131886235);
                pq7.b(c6, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c7 = um5.c(PortfolioApp.h0.c(), 2131886230);
                pq7.b(c7, "LanguageHelper.getString\u2026ayersPerChallengeReached)");
                E(fragmentManager, c6, c7);
                return;
            default:
                if (TextUtils.isEmpty(str)) {
                    D(fragmentManager);
                    return;
                } else if (str != null) {
                    x(str, fragmentManager);
                    return;
                } else {
                    pq7.i();
                    throw null;
                }
        }
    }

    @DexIgnore
    public final void o(FragmentManager fragmentManager, String str, String str2, ps4 ps4) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "title");
        pq7.c(str2, "description");
        Bundle bundle = new Bundle();
        bundle.putParcelable("CHALLENGE", ps4);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, str);
        fVar.e(2131363317, str2);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886324));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887108));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(fragmentManager, "LEAVE_CHALLENGE", bundle);
    }

    @DexIgnore
    public final void o0(FragmentManager fragmentManager) {
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886881));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886882));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886880));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void p(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558482);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887393));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887499));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887320));
        fVar.k(fragmentManager, "APPLY_NEW_COLOR_THEME");
    }

    @DexIgnore
    public final void p0(FragmentManager fragmentManager) {
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887398));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886987));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void q(FragmentManager fragmentManager, String str) {
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(str, "description");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, "");
        fVar.e(2131363317, str);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886114));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "DND_SCHEDULED_TIME_ERROR");
    }

    @DexIgnore
    public final void q0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragment");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887398));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887567));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "CONFIRM_SET_ALARM_FAILED");
    }

    @DexIgnore
    public final void r(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887096));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887095));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887091));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887090));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "CONFIRM_DELETE_ACCOUNT");
    }

    @DexIgnore
    public final void r0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558630);
        fVar.b(2131362686);
        fVar.b(2131362686);
        fVar.k(fragmentManager, "DEVICE_SET_DATA_FAILED");
    }

    @DexIgnore
    public final void s(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558482);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886101));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886100));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886099));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "CONFIRM_DELETE_ALARM");
    }

    @DexIgnore
    public final void s0(FragmentManager fragmentManager, rh5 rh5, int i) {
        String format;
        pq7.c(fragmentManager, "fragmentManager");
        pq7.c(rh5, "type");
        int i2 = r37.f3077a[rh5.ordinal()];
        if (i2 == 1) {
            hr7 hr7 = hr7.f1520a;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887120);
            pq7.b(c2, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 == 2) {
            hr7 hr72 = hr7.f1520a;
            String c3 = um5.c(PortfolioApp.h0.c(), 2131887111);
            pq7.b(c3, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c3, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 == 3) {
            hr7 hr73 = hr7.f1520a;
            String c4 = um5.c(PortfolioApp.h0.c(), 2131887113);
            pq7.b(c4, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c4, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 == 4) {
            hr7 hr74 = hr7.f1520a;
            String c5 = um5.c(PortfolioApp.h0.c(), 2131887115);
            pq7.b(c5, "LanguageHelper.getString\u2026t__PleaseSetASleepGoalOf)");
            format = String.format(c5, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 != 5) {
            format = "";
        } else {
            hr7 hr75 = hr7.f1520a;
            String c6 = um5.c(PortfolioApp.h0.c(), 2131887128);
            pq7.b(c6, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c6, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
        }
        t47.f fVar = new t47.f(2131558480);
        fVar.b(2131363373);
        fVar.e(2131363317, format);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887119));
        fVar.k(fragmentManager, "GOAL_EXCEED_VALUE");
    }

    @DexIgnore
    public final void t(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131887096));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131887095));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131887091));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131887090));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "DELETE_WORKOUT");
    }

    @DexIgnore
    public final void t0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886129));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886134));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886130));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886952));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.k(fragmentManager, "SET TO WATCH FAIL");
    }

    @DexIgnore
    public final void u(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886582));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886579));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886793));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "DIANA_REQUIRE");
    }

    @DexIgnore
    public final void u0(String str, FragmentManager fragmentManager) {
        pq7.c(str, "serial");
        pq7.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886818));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886817));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886816));
        fVar.b(2131363373);
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886815));
        fVar.b(2131363291);
        fVar.m(fragmentManager, "SWITCH_DEVICE_ERASE_FAIL", bundle);
    }

    @DexIgnore
    public final void v(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558482);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886835));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886834));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886833));
        fVar.k(fragmentManager, "DOWNLOAD");
    }

    @DexIgnore
    public final void v0(String str, FragmentManager fragmentManager) {
        pq7.c(str, "serial");
        pq7.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886823));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886827));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886822));
        fVar.b(2131363373);
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886820));
        fVar.b(2131363291);
        fVar.m(fragmentManager, "SWITCH_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final void w(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886849));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886848));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "SEARCH_ON_STORE");
    }

    @DexIgnore
    public final void w0(String str, FragmentManager fragmentManager) {
        pq7.c(str, "serial");
        pq7.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        t47.f fVar = new t47.f(2131558484);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886818));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886817));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886816));
        fVar.b(2131363373);
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886815));
        fVar.b(2131363291);
        fVar.m(fragmentManager, "SWITCH_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void x(String str, FragmentManager fragmentManager) {
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886795));
        fVar.e(2131363317, str);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886793));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "SERVER_ERROR");
    }

    @DexIgnore
    public final void x0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886885));
        fVar.b(2131363373);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
        fVar.k(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void y(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558480);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886884));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886883));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "FAIL_DUE_TO_DEVICE_DISCONNECTED");
    }

    @DexIgnore
    public final void y0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558482);
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886174));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886173));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886172));
        fVar.k(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void z(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragment");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886235));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886571));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886793));
        fVar.b(2131363373);
        fVar.k(fragmentManager, "FAILED_LOADING_WATCH_FACE");
    }

    @DexIgnore
    public final void z0(FragmentManager fragmentManager) {
        pq7.c(fragmentManager, "fragmentManager");
        t47.f fVar = new t47.f(2131558481);
        fVar.e(2131363410, um5.c(PortfolioApp.h0.c(), 2131886791));
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886790));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886787));
        fVar.h(false);
        fVar.b(2131363373);
        fVar.k(fragmentManager, "FIRMWARE_UPDATE_FAIL");
    }
}
