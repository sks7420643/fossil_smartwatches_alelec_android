package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lq1 extends jq1 {
    @DexIgnore
    public /* final */ bv1 e;

    @DexIgnore
    public lq1(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(bv1.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (bv1) readParcelable;
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public lq1(np1 np1, byte b, bv1 bv1) {
        super(np1, b, hy1.p(bv1.getRequestId()));
        this.e = bv1;
    }

    @DexIgnore
    public final bv1 c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.e, ((lq1) obj).e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1, com.fossil.jq1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        try {
            g80.k(jSONObject, jd0.v3, this.e);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
