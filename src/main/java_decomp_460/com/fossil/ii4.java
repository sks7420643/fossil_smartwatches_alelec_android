package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii4 {
    @DexIgnore
    public static WeakReference<ii4> d;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SharedPreferences f1632a;
    @DexIgnore
    public gi4 b;
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public ii4(SharedPreferences sharedPreferences, Executor executor) {
        this.c = executor;
        this.f1632a = sharedPreferences;
    }

    @DexIgnore
    public static ii4 a(Context context, Executor executor) {
        ii4 ii4;
        synchronized (ii4.class) {
            ii4 = null;
            try {
                if (d != null) {
                    ii4 = d.get();
                }
                if (ii4 == null) {
                    ii4 = new ii4(context.getSharedPreferences("com.google.android.gms.appid", 0), executor);
                    ii4.c();
                    d = new WeakReference<>(ii4);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return ii4;
    }

    @DexIgnore
    public final hi4 b() {
        hi4 a2;
        synchronized (this) {
            a2 = hi4.a(this.b.e());
        }
        return a2;
    }

    @DexIgnore
    public final void c() {
        synchronized (this) {
            this.b = gi4.c(this.f1632a, "topic_operation_queue", ",", this.c);
        }
    }

    @DexIgnore
    public final boolean d(hi4 hi4) {
        boolean f;
        synchronized (this) {
            f = this.b.f(hi4.e());
        }
        return f;
    }
}
