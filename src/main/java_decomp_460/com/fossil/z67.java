package com.fossil;

import androidx.lifecycle.MutableLiveData;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z67 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Date> f4428a; // = new MutableLiveData<>();

    @DexIgnore
    public final MutableLiveData<Date> a() {
        return this.f4428a;
    }
}
