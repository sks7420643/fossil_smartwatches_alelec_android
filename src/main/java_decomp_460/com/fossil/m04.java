package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m04 extends rp0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<m04> CREATOR; // = new a();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, Bundle> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.ClassLoaderCreator<m04> {
        @DexIgnore
        /* renamed from: a */
        public m04 createFromParcel(Parcel parcel) {
            return new m04(parcel, null, null);
        }

        @DexIgnore
        /* renamed from: b */
        public m04 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new m04(parcel, classLoader, null);
        }

        @DexIgnore
        /* renamed from: c */
        public m04[] newArray(int i) {
            return new m04[i];
        }
    }

    @DexIgnore
    public m04(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.d = new SimpleArrayMap<>(readInt);
        for (int i = 0; i < readInt; i++) {
            this.d.put(strArr[i], bundleArr[i]);
        }
    }

    @DexIgnore
    public /* synthetic */ m04(Parcel parcel, ClassLoader classLoader, a aVar) {
        this(parcel, classLoader);
    }

    @DexIgnore
    public m04(Parcelable parcelable) {
        super(parcelable);
        this.d = new SimpleArrayMap<>();
    }

    @DexIgnore
    public String toString() {
        return "ExtendableSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " states=" + this.d + "}";
    }

    @DexIgnore
    @Override // com.fossil.rp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        int size = this.d.size();
        parcel.writeInt(size);
        String[] strArr = new String[size];
        Bundle[] bundleArr = new Bundle[size];
        for (int i2 = 0; i2 < size; i2++) {
            strArr[i2] = this.d.j(i2);
            bundleArr[i2] = this.d.n(i2);
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }
}
