package com.fossil;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.pn6;
import com.fossil.t47;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutEditActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn6 extends u47 implements t47.g {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public pn6 k;
    @DexIgnore
    public /* final */ zp0 l; // = new sr4(this);
    @DexIgnore
    public String m;
    @DexIgnore
    public po4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return nn6.u;
        }

        @DexIgnore
        public final nn6 b(String str) {
            pq7.c(str, "workoutSessionId");
            nn6 nn6 = new nn6();
            nn6.setArguments(nm0.a(new cl7("WORKOUT_ID_KEY", str)));
            return nn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 b;

        @DexIgnore
        public b(nn6 nn6) {
            this.b = nn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.b.getDialog();
            if (dialog != null) {
                nn6 nn6 = this.b;
                pq7.b(dialog, "it");
                nn6.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 b;

        @DexIgnore
        public c(nn6 nn6) {
            this.b = nn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                WorkoutEditActivity.a aVar = WorkoutEditActivity.A;
                pq7.b(activity, "it");
                String str = this.b.m;
                if (str != null) {
                    aVar.a(activity, str);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            Dialog dialog = this.b.getDialog();
            if (dialog != null) {
                nn6 nn6 = this.b;
                pq7.b(dialog, "it");
                nn6.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 b;

        @DexIgnore
        public d(nn6 nn6) {
            this.b = nn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.t(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<pn6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ nn6 f2551a;

        @DexIgnore
        public e(nn6 nn6) {
            this.f2551a = nn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(pn6.a aVar) {
            if (aVar.a()) {
                Dialog dialog = this.f2551a.getDialog();
                if (dialog != null) {
                    nn6 nn6 = this.f2551a;
                    pq7.b(dialog, "it");
                    nn6.onDismiss(dialog);
                }
            } else if (aVar.b() > 0) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f2551a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.O(childFragmentManager, aVar.b(), aVar.c());
            }
        }
    }

    /*
    static {
        String simpleName = nn6.class.getSimpleName();
        pq7.b(simpleName, "WorkoutMenuFragment::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i, Intent intent) {
        pq7.c(str, "tag");
        if (!(str.length() == 0)) {
            if (str.hashCode() != -479587351 || !str.equals("DELETE_WORKOUT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((ls5) activity).R5(str, i, intent);
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i == 2131363373) {
                pn6 pn6 = this.k;
                if (pn6 != null) {
                    pn6.q();
                } else {
                    pq7.n("mViewModel");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().a0().a(this);
        po4 po4 = this.s;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(pn6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026enuViewModel::class.java)");
            this.k = (pn6) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                String string = arguments.getString("WORKOUT_ID_KEY");
                if (string == null) {
                    string = "";
                }
                this.m = string;
                pn6 pn6 = this.k;
                if (pn6 == null) {
                    pq7.n("mViewModel");
                    throw null;
                } else if (string != null) {
                    pn6.s(string);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        } else {
            pq7.n("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        pc5 pc5 = (pc5) aq0.f(layoutInflater, 2131558644, viewGroup, false, this.l);
        pc5.t.setOnClickListener(new b(this));
        new g37(this, pc5);
        pc5.r.setOnClickListener(new c(this));
        pc5.q.setOnClickListener(new d(this));
        pn6 pn6 = this.k;
        if (pn6 != null) {
            pn6.r().h(getViewLifecycleOwner(), new e(this));
            pq7.b(pc5, "binding");
            return pc5.n();
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
