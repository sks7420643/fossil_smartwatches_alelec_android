package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x86 extends t86 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public SecondTimezoneSetting e;
    @DexIgnore
    public ArrayList<SecondTimezoneSetting> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ u86 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1", f = "SearchSecondTimezonePresenter.kt", l = {37, 38}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ x86 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x86$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$rawDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.x86$a$a  reason: collision with other inner class name */
        public static final class C0279a extends ko7 implements vp7<iv7, qn7<? super ArrayList<SecondTimezoneSetting>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public C0279a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0279a aVar = new C0279a(qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ArrayList<SecondTimezoneSetting>> qn7) {
                return ((C0279a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return dk5.g.i();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super ArrayList<SecondTimezoneSetting>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $rawDataList;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x86$a$b$a")
            /* renamed from: com.fossil.x86$a$b$a  reason: collision with other inner class name */
            public static final class C0280a<T> implements Comparator<SecondTimezoneSetting> {
                @DexIgnore
                public static /* final */ C0280a b; // = new C0280a();

                @DexIgnore
                /* renamed from: a */
                public final int compare(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
                    return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(ArrayList arrayList, qn7 qn7) {
                super(2, qn7);
                this.$rawDataList = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.$rawDataList, qn7);
                bVar.p$ = (iv7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ArrayList<SecondTimezoneSetting>> qn7) {
                return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    lm7.r(this.$rawDataList, C0280a.b);
                    return this.$rawDataList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(x86 x86, qn7 qn7) {
            super(2, qn7);
            this.this$0 = x86;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0063  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0065
                if (r0 == r4) goto L_0x0040
                if (r0 != r5) goto L_0x0038
                java.lang.Object r0 = r7.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r0 = r8
            L_0x001b:
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                com.fossil.x86 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.x86.o(r1)
                r1.addAll(r0)
                com.fossil.x86 r0 = r7.this$0
                com.fossil.u86 r0 = com.fossil.x86.p(r0)
                com.fossil.x86 r1 = r7.this$0
                java.util.ArrayList r1 = com.fossil.x86.o(r1)
                r0.G2(r1)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0037:
                return r0
            L_0x0038:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0040:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0049:
                r0 = r1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                com.fossil.x86 r1 = r7.this$0
                com.fossil.dv7 r1 = com.fossil.x86.n(r1)
                com.fossil.x86$a$b r4 = new com.fossil.x86$a$b
                r4.<init>(r0, r6)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x0037
            L_0x0065:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.x86 r1 = r7.this$0
                com.fossil.dv7 r1 = com.fossil.x86.n(r1)
                com.fossil.x86$a$a r2 = new com.fossil.x86$a$a
                r2.<init>(r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0081
                r0 = r3
                goto L_0x0037
            L_0x0081:
                r2 = r0
                goto L_0x0049
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.x86.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = x86.class.getSimpleName();
        pq7.b(simpleName, "SearchSecondTimezonePres\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public x86(u86 u86) {
        pq7.c(u86, "mView");
        this.h = u86;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        if (this.f.isEmpty()) {
            xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
        } else {
            this.h.G2(this.f);
        }
        SecondTimezoneSetting secondTimezoneSetting = this.e;
        if (secondTimezoneSetting != null) {
            this.h.v5(secondTimezoneSetting.getTimeZoneName());
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    public void q(String str) {
        pq7.c(str, MicroAppSetting.SETTING);
        try {
            this.e = (SecondTimezoneSetting) this.g.k(str, SecondTimezoneSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse second timezone setting " + e2);
        }
    }

    @DexIgnore
    public void r() {
        this.h.M5(this);
    }
}
