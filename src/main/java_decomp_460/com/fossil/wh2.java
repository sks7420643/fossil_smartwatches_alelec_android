package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wh2 extends zc2 {
    @DexIgnore
    public static /* final */ wh2 A; // = D("circumference");
    @DexIgnore
    public static /* final */ wh2 B; // = D("percentage");
    @DexIgnore
    public static /* final */ wh2 C; // = D(PlaceManager.PARAM_SPEED);
    @DexIgnore
    public static /* final */ Parcelable.Creator<wh2> CREATOR; // = new qi2();
    @DexIgnore
    public static /* final */ wh2 D; // = D("rpm");
    @DexIgnore
    public static /* final */ wh2 E; // = o0("google.android.fitness.GoalV2");
    @DexIgnore
    public static /* final */ wh2 F; // = o0("symptom");
    @DexIgnore
    public static /* final */ wh2 G; // = o0("google.android.fitness.StrideModel");
    @DexIgnore
    public static /* final */ wh2 H; // = o0("google.android.fitness.Device");
    @DexIgnore
    public static /* final */ wh2 I; // = k("revolutions");
    @DexIgnore
    public static /* final */ wh2 J; // = D("calories");
    @DexIgnore
    public static /* final */ wh2 K; // = D("watts");
    @DexIgnore
    public static /* final */ wh2 L; // = D("volume");
    @DexIgnore
    public static /* final */ wh2 M; // = A("meal_type");
    @DexIgnore
    public static /* final */ wh2 N; // = new wh2("food_item", 3, Boolean.TRUE);
    @DexIgnore
    public static /* final */ wh2 O; // = L("nutrients");
    @DexIgnore
    public static /* final */ wh2 P; // = D("elevation.change");
    @DexIgnore
    public static /* final */ wh2 Q; // = L("elevation.gain");
    @DexIgnore
    public static /* final */ wh2 R; // = L("elevation.loss");
    @DexIgnore
    public static /* final */ wh2 S; // = D("floors");
    @DexIgnore
    public static /* final */ wh2 T; // = L("floor.gain");
    @DexIgnore
    public static /* final */ wh2 U; // = L("floor.loss");
    @DexIgnore
    public static /* final */ wh2 V; // = new wh2("exercise", 3);
    @DexIgnore
    public static /* final */ wh2 W; // = A("repetitions");
    @DexIgnore
    public static /* final */ wh2 X; // = F("resistance");
    @DexIgnore
    public static /* final */ wh2 Y; // = A("resistance_type");
    @DexIgnore
    public static /* final */ wh2 Z; // = k("num_segments");
    @DexIgnore
    public static /* final */ wh2 a0; // = D(GoalTrackingSummary.COLUMN_AVERAGE);
    @DexIgnore
    public static /* final */ wh2 b0; // = D("max");
    @DexIgnore
    public static /* final */ wh2 c0; // = D("min");
    @DexIgnore
    public static /* final */ wh2 d0; // = D("low_latitude");
    @DexIgnore
    public static /* final */ wh2 e; // = k(Constants.ACTIVITY);
    @DexIgnore
    public static /* final */ wh2 e0; // = D("low_longitude");
    @DexIgnore
    public static /* final */ wh2 f; // = D("confidence");
    @DexIgnore
    public static /* final */ wh2 f0; // = D("high_latitude");
    @DexIgnore
    @Deprecated
    public static /* final */ wh2 g; // = L("activity_confidence");
    @DexIgnore
    public static /* final */ wh2 g0; // = D("high_longitude");
    @DexIgnore
    public static /* final */ wh2 h; // = k("steps");
    @DexIgnore
    public static /* final */ wh2 h0; // = k("occurrences");
    @DexIgnore
    public static /* final */ wh2 i; // = D("step_length");
    @DexIgnore
    public static /* final */ wh2 i0; // = k("sensor_type");
    @DexIgnore
    public static /* final */ wh2 j; // = k("duration");
    @DexIgnore
    public static /* final */ wh2 j0; // = k("sensor_types");
    @DexIgnore
    public static /* final */ wh2 k; // = A("duration");
    @DexIgnore
    public static /* final */ wh2 k0; // = new wh2("timestamps", 5);
    @DexIgnore
    public static /* final */ wh2 l; // = L("activity_duration.ascending");
    @DexIgnore
    public static /* final */ wh2 l0; // = k("sample_period");
    @DexIgnore
    public static /* final */ wh2 m; // = L("activity_duration.descending");
    @DexIgnore
    public static /* final */ wh2 m0; // = k("num_samples");
    @DexIgnore
    public static /* final */ wh2 n0; // = k("num_dimensions");
    @DexIgnore
    public static /* final */ wh2 o0; // = new wh2("sensor_values", 6);
    @DexIgnore
    public static /* final */ wh2 p0; // = D("intensity");
    @DexIgnore
    public static /* final */ wh2 q0; // = D("probability");
    @DexIgnore
    public static /* final */ wh2 s; // = D("bpm");
    @DexIgnore
    public static /* final */ wh2 t; // = D("latitude");
    @DexIgnore
    public static /* final */ wh2 u; // = D("longitude");
    @DexIgnore
    public static /* final */ wh2 v; // = D(PlaceManager.PARAM_ACCURACY);
    @DexIgnore
    public static /* final */ wh2 w; // = F(PlaceManager.PARAM_ALTITUDE);
    @DexIgnore
    public static /* final */ wh2 x; // = D("distance");
    @DexIgnore
    public static /* final */ wh2 y; // = D("height");
    @DexIgnore
    public static /* final */ wh2 z; // = D(Constants.PROFILE_KEY_UNITS_WEIGHT);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ wh2 f3937a; // = wh2.D("x");
        @DexIgnore
        public static /* final */ wh2 b; // = wh2.D("y");
        @DexIgnore
        public static /* final */ wh2 c; // = wh2.D("z");
        @DexIgnore
        public static /* final */ wh2 d; // = wh2.p0("debug_session");
        @DexIgnore
        public static /* final */ wh2 e; // = wh2.p0("google.android.fitness.SessionV2");
        @DexIgnore
        public static /* final */ wh2 f; // = wh2.o0("google.android.fitness.DataPointSession");
    }

    @DexIgnore
    public wh2(String str, int i2) {
        this(str, i2, null);
    }

    @DexIgnore
    public wh2(String str, int i2, Boolean bool) {
        rc2.k(str);
        this.b = str;
        this.c = i2;
        this.d = bool;
    }

    @DexIgnore
    public static wh2 A(String str) {
        return new wh2(str, 1, Boolean.TRUE);
    }

    @DexIgnore
    public static wh2 D(String str) {
        return new wh2(str, 2);
    }

    @DexIgnore
    public static wh2 F(String str) {
        return new wh2(str, 2, Boolean.TRUE);
    }

    @DexIgnore
    public static wh2 L(String str) {
        return new wh2(str, 4);
    }

    @DexIgnore
    public static wh2 k(String str) {
        return new wh2(str, 1);
    }

    @DexIgnore
    public static wh2 o0(String str) {
        return new wh2(str, 7);
    }

    @DexIgnore
    public static wh2 p0(String str) {
        return new wh2(str, 7, Boolean.TRUE);
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof wh2)) {
            return false;
        }
        wh2 wh2 = (wh2) obj;
        return this.b.equals(wh2.b) && this.c == wh2.c;
    }

    @DexIgnore
    public final String f() {
        return this.b;
    }

    @DexIgnore
    public final Boolean h() {
        return this.d;
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        return String.format("%s(%s)", this.b, this.c == 1 ? "i" : "f");
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 1, f(), false);
        bd2.n(parcel, 2, c());
        bd2.d(parcel, 3, h(), false);
        bd2.b(parcel, a2);
    }
}
