package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h53 implements xw2<k53> {
    @DexIgnore
    public static h53 c; // = new h53();
    @DexIgnore
    public /* final */ xw2<k53> b;

    @DexIgnore
    public h53() {
        this(ww2.b(new j53()));
    }

    @DexIgnore
    public h53(xw2<k53> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((k53) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((k53) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((k53) c.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ k53 zza() {
        return this.b.zza();
    }
}
