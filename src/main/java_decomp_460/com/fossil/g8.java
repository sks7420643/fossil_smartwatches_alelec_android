package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g8 extends qq7 implements rp7<yx1, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ hc b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g8(hc hcVar) {
        super(1);
        this.b = hcVar;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(yx1 yx1) {
        yx1 yx12 = yx1;
        zw zwVar = zw.i;
        zw.d.remove(this.b.b);
        bl1 bl1 = (bl1) yx12;
        if (bl1.getErrorCode() == cl1.REQUEST_UNSUPPORTED) {
            zw.i.h(this.b.b);
            this.b.b.t0(yx12);
        } else if (bl1.getErrorCode() == cl1.REQUEST_FAILED && bl1.a().c == zq.HID_INPUT_DEVICE_DISABLED) {
            zw zwVar2 = zw.i;
            zw.c.add(this.b.b);
        } else {
            zw.i.c(this.b.b, qd0.b.a("HID_EXPONENT_BACK_OFF_TAG"));
        }
        return tl7.f3441a;
    }
}
