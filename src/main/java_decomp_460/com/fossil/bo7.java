package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo7 implements qn7<Object> {
    @DexIgnore
    public static /* final */ bo7 b; // = new bo7();

    @DexIgnore
    @Override // com.fossil.qn7
    public tn7 getContext() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void resumeWith(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public String toString() {
        return "This continuation is already complete";
    }
}
