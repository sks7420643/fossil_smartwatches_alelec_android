package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht5 extends iq4<c, e, d> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ b f; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ht5.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ht5.h.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + ht5.this.n() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.FORCE_CONNECT && ht5.this.n()) {
                ht5.this.q(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    ht5.this.j(new e());
                    return;
                }
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                ht5.this.i(new d(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1530a;

        @DexIgnore
        public c(String str) {
            pq7.c(str, "serial");
            this.f1530a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f1530a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1531a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public d(int i, int i2, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "errorCodes");
            this.f1531a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.f1531a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase", f = "ReconnectDeviceUseCase.kt", l = {58}, m = "run")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ht5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ht5 ht5, qn7 qn7) {
            super(qn7);
            this.this$0 = ht5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = ht5.class.getSimpleName();
        pq7.b(simpleName, "ReconnectDeviceUseCase::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return g;
    }

    @DexIgnore
    public final boolean n() {
        return this.e;
    }

    @DexIgnore
    public final void o() {
        wq5.d.e(this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: p */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ht5.c r7, com.fossil.qn7<java.lang.Object> r8) {
        /*
            r6 = this;
            r2 = 0
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r8 instanceof com.fossil.ht5.f
            if (r0 == 0) goto L_0x0032
            r0 = r8
            com.fossil.ht5$f r0 = (com.fossil.ht5.f) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0032
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r4 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0041
            if (r0 != r5) goto L_0x0039
            java.lang.Object r0 = r1.L$1
            com.fossil.ht5$c r0 = (com.fossil.ht5.c) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.ht5 r0 = (com.fossil.ht5) r0
            com.fossil.el7.b(r4)     // Catch:{ Exception -> 0x006d }
        L_0x002c:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
        L_0x0031:
            return r0
        L_0x0032:
            com.fossil.ht5$f r0 = new com.fossil.ht5$f
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r4)
            r6.e = r5
            if (r7 == 0) goto L_0x0067
            java.lang.String r0 = r7.a()
        L_0x004c:
            r6.d = r0
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r4 = r6.d
            if (r4 == 0) goto L_0x0069
            r1.L$0 = r6
            r1.L$1 = r7
            r2 = 1
            r1.label = r2
            java.lang.Object r0 = r0.G(r4, r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0031
        L_0x0067:
            r0 = r2
            goto L_0x004c
        L_0x0069:
            com.fossil.pq7.i()
            throw r2
        L_0x006d:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.ht5.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error inside "
            r3.append(r4)
            java.lang.String r4 = com.fossil.ht5.g
            r3.append(r4)
            java.lang.String r4 = ".connectDevice - e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ht5.k(com.fossil.ht5$c, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void q(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void r() {
        wq5.d.j(this.f, CommunicateMode.FORCE_CONNECT);
    }
}
