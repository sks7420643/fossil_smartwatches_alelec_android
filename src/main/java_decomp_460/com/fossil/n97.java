package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.jn5;
import com.fossil.k96;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n97 extends j77 implements k96.b, t47.g {
    @DexIgnore
    public po4 h;
    @DexIgnore
    public wg5 i;
    @DexIgnore
    public p97 j;
    @DexIgnore
    public m97 k;
    @DexIgnore
    public k96 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements d77 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n97 f2485a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(n97 n97) {
            this.f2485a = n97;
        }

        @DexIgnore
        @Override // com.fossil.d77
        public void a() {
            n97.L6(this.f2485a).i(true);
        }

        @DexIgnore
        @Override // com.fossil.d77
        public void b(fb7 fb7, int i) {
            pq7.c(fb7, "watchFaceWrapper");
            n97.L6(this.f2485a).g(i);
            n97.M6(this.f2485a).B(fb7, i);
            n97.K6(this.f2485a).q.smoothScrollToPosition(i);
        }

        @DexIgnore
        @Override // com.fossil.d77
        public void c() {
            this.f2485a.S6();
        }

        @DexIgnore
        @Override // com.fossil.d77
        public void d(fb7 fb7) {
            pq7.c(fb7, AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_PHOTO);
            if (pq7.a(n97.M6(this.f2485a).y(), fb7.b())) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.f2485a.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.R(childFragmentManager);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("background_photo_id_extra", fb7.b());
            s37 s372 = s37.c;
            FragmentManager childFragmentManager2 = this.f2485a.getChildFragmentManager();
            pq7.b(childFragmentManager2, "childFragmentManager");
            s372.e(childFragmentManager2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.d77
        public void onCancel() {
            n97.L6(this.f2485a).i(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<cl7<? extends List<Object>, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n97 f2486a;

        @DexIgnore
        public b(n97 n97) {
            this.f2486a = n97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<? extends List<Object>, Integer> cl7) {
            n97.L6(this.f2486a).h((List) cl7.getFirst(), cl7.getSecond().intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<fb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n97 f2487a;

        @DexIgnore
        public c(n97 n97) {
            this.f2487a = n97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fb7 fb7) {
            gc7 e = hc7.c.e(this.f2487a);
            if (e != null) {
                e.z(fb7);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Object> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n97 f2488a;

        @DexIgnore
        public d(n97 n97) {
            this.f2488a = n97;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(Object obj) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.f2488a.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.R(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<fb7> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n97 f2489a;

        @DexIgnore
        public e(n97 n97) {
            this.f2489a = n97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fb7 fb7) {
            p97.F(n97.M6(this.f2489a), fb7.b(), false, 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ n97 f2490a;

        @DexIgnore
        public f(n97 n97) {
            this.f2490a = n97;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            p97 M6 = n97.M6(this.f2490a);
            if (str == null) {
                str = "";
            }
            M6.E(str, true);
        }
    }

    @DexIgnore
    public static final /* synthetic */ wg5 K6(n97 n97) {
        wg5 wg5 = n97.i;
        if (wg5 != null) {
            return wg5;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ m97 L6(n97 n97) {
        m97 m97 = n97.k;
        if (m97 != null) {
            return m97;
        }
        pq7.n("photoAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ p97 M6(n97 n97) {
        p97 p97 = n97.j;
        if (p97 != null) {
            return p97;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFacePhotoBackgroundFragment";
    }

    @DexIgnore
    public final void O6() {
        this.k = new m97(new a(this));
        wg5 wg5 = this.i;
        if (wg5 != null) {
            RecyclerView recyclerView = wg5.q;
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(null);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
            m97 m97 = this.k;
            if (m97 != null) {
                recyclerView.setAdapter(m97);
            } else {
                pq7.n("photoAdapter");
                throw null;
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6() {
        MutableLiveData<String> r;
        LiveData<fb7> e2;
        p97 p97 = this.j;
        if (p97 != null) {
            p97.w().h(getViewLifecycleOwner(), new b(this));
            p97 p972 = this.j;
            if (p972 != null) {
                p972.x().h(getViewLifecycleOwner(), new c(this));
                p97 p973 = this.j;
                if (p973 != null) {
                    p973.u().h(getViewLifecycleOwner(), new d(this));
                    gc7 e3 = hc7.c.e(this);
                    if (!(e3 == null || (e2 = e3.e()) == null)) {
                        e2.h(getViewLifecycleOwner(), new e(this));
                    }
                    gc7 e4 = hc7.c.e(this);
                    if (e4 != null && (r = e4.r()) != null) {
                        r.h(getViewLifecycleOwner(), new f(this));
                        return;
                    }
                    return;
                }
                pq7.n("viewModel");
                throw null;
            }
            pq7.n("viewModel");
            throw null;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void Q6(Uri uri) {
        Intent intent = new Intent(getContext(), EditPhotoActivity.class);
        intent.putExtra("IMAGE_URI_EXTRA", uri);
        startActivityForResult(intent, 3);
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if (pq7.a(str, "DELETE_PHOTO_BACKGROUND") && i2 == 2131363373) {
            String stringExtra = intent != null ? intent.getStringExtra("background_photo_id_extra") : null;
            p97 p97 = this.j;
            if (p97 != null) {
                if (stringExtra == null) {
                    stringExtra = "";
                }
                p97.t(stringExtra);
                return;
            }
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void R6(String str) {
        Uri parse = Uri.parse(str);
        pq7.b(parse, "Uri.parse(uri)");
        Q6(parse);
    }

    @DexIgnore
    public final void S6() {
        k96 a2 = k96.t.a();
        this.l = a2;
        if (a2 != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            a2.show(childFragmentManager, "AddPhotoMenuFragment");
        }
        k96 k96 = this.l;
        if (k96 != null) {
            k96.B6(this);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.h0.c().M().w().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(p97.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026undViewModel::class.java)");
            p97 p97 = (p97) a2;
            this.j = p97;
            if (p97 != null) {
                p97.z();
                String d2 = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d2)) {
                    wg5 wg5 = this.i;
                    if (wg5 != null) {
                        wg5.r.setBackgroundColor(Color.parseColor(d2));
                    } else {
                        pq7.n("binding");
                        throw null;
                    }
                }
                O6();
                P6();
                return;
            }
            pq7.n("viewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        Bundle extras;
        boolean z = true;
        Uri uri = null;
        String str = null;
        if (i3 == -1) {
            if (i2 == 1) {
                if (intent != null) {
                    uri = intent.getData();
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("WatchFacePhotoBackgroundFragment", "From Gallery, uri == " + uri);
                if (uri != null) {
                    String uri2 = uri.toString();
                    pq7.b(uri2, "uri.toString()");
                    R6(uri2);
                }
            } else if (i2 == 2) {
                Uri g = vk5.g(intent, PortfolioApp.h0.c());
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("WatchFacePhotoBackgroundFragment", "From Camera, uri = " + g);
                if (g != null) {
                    str = g.getPath();
                }
                if (!(str == null || str.length() == 0)) {
                    z = false;
                }
                if (!z) {
                    R6(str);
                }
            } else if (i2 == 3) {
                String string = (intent == null || (extras = intent.getExtras()) == null) ? null : extras.getString("WATCH_FACE_ID");
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.e("WatchFacePhotoBackgroundFragment", "Image is cropped and saved with watchFaceId = " + string);
                p97 p97 = this.j;
                if (p97 != null) {
                    if (string == null) {
                        string = "";
                    }
                    p97.A(string);
                    return;
                }
                pq7.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        wg5 z = wg5.z(layoutInflater);
        pq7.b(z, "WatchFacePhotoBackground\u2026Binding.inflate(inflater)");
        this.i = z;
        if (z != null) {
            View n = z.n();
            pq7.b(n, "binding.root");
            return n;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment, com.fossil.j77
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.k96.b
    public void p5() {
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @DexIgnore
    @Override // com.fossil.k96.b
    public void u1() {
        FragmentActivity activity;
        Intent d2;
        if (jn5.c(jn5.b, getActivity(), jn5.a.CAPTURE_IMAGE, false, false, false, null, 60, null) && (activity = getActivity()) != null && (d2 = vk5.d(activity)) != null) {
            startActivityForResult(d2, 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.j77
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
