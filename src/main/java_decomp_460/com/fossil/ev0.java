package com.fossil;

import android.graphics.Canvas;
import android.os.Build;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ev0 implements dv0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ dv0 f990a; // = new ev0();

    @DexIgnore
    public static float e(RecyclerView recyclerView, View view) {
        int childCount = recyclerView.getChildCount();
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (childAt != view) {
                float u = mo0.u(childAt);
                if (u > f) {
                    f = u;
                }
            }
        }
        return f;
    }

    @DexIgnore
    @Override // com.fossil.dv0
    public void a(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            Object tag = view.getTag(qu0.item_touch_helper_previous_elevation);
            if (tag instanceof Float) {
                mo0.s0(view, ((Float) tag).floatValue());
            }
            view.setTag(qu0.item_touch_helper_previous_elevation, null);
        }
        view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    @Override // com.fossil.dv0
    public void b(View view) {
    }

    @DexIgnore
    @Override // com.fossil.dv0
    public void c(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.dv0
    public void d(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        if (Build.VERSION.SDK_INT >= 21 && z && view.getTag(qu0.item_touch_helper_previous_elevation) == null) {
            float u = mo0.u(view);
            mo0.s0(view, e(recyclerView, view) + 1.0f);
            view.setTag(qu0.item_touch_helper_previous_elevation, Float.valueOf(u));
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }
}
