package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj2 implements Parcelable.Creator<cj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ cj2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        IBinder iBinder = null;
        DataSet dataSet = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                dataSet = (DataSet) ad2.e(parcel, t, DataSet.CREATOR);
            } else if (l == 2) {
                iBinder = ad2.u(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                z = ad2.m(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new cj2(dataSet, iBinder, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ cj2[] newArray(int i) {
        return new cj2[i];
    }
}
