package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Intent f1630a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Intent f1631a;
        @DexIgnore
        public ArrayList<Bundle> b;
        @DexIgnore
        public Bundle c;
        @DexIgnore
        public ArrayList<Bundle> d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public a() {
            this(null);
        }

        @DexIgnore
        public a(ki0 ki0) {
            IBinder iBinder = null;
            Intent intent = new Intent("android.intent.action.VIEW");
            this.f1631a = intent;
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = true;
            if (ki0 != null) {
                intent.setPackage(ki0.b().getPackageName());
            }
            Bundle bundle = new Bundle();
            vk0.b(bundle, "android.support.customtabs.extra.SESSION", ki0 != null ? ki0.a() : iBinder);
            this.f1631a.putExtras(bundle);
        }

        @DexIgnore
        public ii0 a() {
            ArrayList<Bundle> arrayList = this.b;
            if (arrayList != null) {
                this.f1631a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", arrayList);
            }
            ArrayList<Bundle> arrayList2 = this.d;
            if (arrayList2 != null) {
                this.f1631a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", arrayList2);
            }
            this.f1631a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.e);
            return new ii0(this.f1631a, this.c);
        }
    }

    @DexIgnore
    public ii0(Intent intent, Bundle bundle) {
        this.f1630a = intent;
        this.b = bundle;
    }

    @DexIgnore
    public void a(Context context, Uri uri) {
        this.f1630a.setData(uri);
        gl0.n(context, this.f1630a, this.b);
    }
}
