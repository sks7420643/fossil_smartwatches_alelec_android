package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.Where;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Firmware;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kp5 extends BaseDbProvider implements jp5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f1944a; // = "kp5";

    @DexIgnore
    public kp5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.jp5
    public Firmware e(String str) {
        try {
            Where<Firmware, Integer> where = o().queryBuilder().where();
            where.eq("deviceModel", str);
            List<Firmware> query = where.query();
            if (query == null || query.isEmpty()) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = f1944a;
            local.e(str2, "Error inside " + f1944a + ".getLatestFirmware with model " + str + ") - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{Firmware.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.jp5
    public void m(Firmware firmware) {
        try {
            o().createOrUpdate(firmware);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f1944a;
            local.e(str, "Error inside " + f1944a + ".addOrUpdatePhoto - e=" + e);
        }
    }

    @DexIgnore
    public final Dao<Firmware, Integer> o() throws SQLException {
        return this.databaseHelper.getDao(Firmware.class);
    }
}
