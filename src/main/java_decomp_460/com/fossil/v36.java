package com.fossil;

import android.text.TextUtils;
import com.fossil.dk5;
import com.fossil.iq4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v36 extends iq4<iq4.b, a, iq4.a> {
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<i06> f3709a;

        @DexIgnore
        public a(List<i06> list) {
            pq7.c(list, "apps");
            i14.o(list, "apps cannot be null!", new Object[0]);
            pq7.b(list, "checkNotNull(apps, \"apps cannot be null!\")");
            this.f3709a = list;
        }

        @DexIgnore
        public final List<i06> a() {
            return this.f3709a;
        }
    }

    @DexIgnore
    public v36() {
        String simpleName = v36.class.getSimpleName();
        pq7.b(simpleName, "GetApps::class.java.simpleName");
        this.d = simpleName;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public Object k(iq4.b bVar, qn7<Object> qn7) {
        FLogger.INSTANCE.getLocal().d(this.d, "executeUseCase GetApps");
        List<AppFilter> allAppFilters = mn5.p.a().c().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<dk5.b> it = dk5.g.b().iterator();
        while (it.hasNext()) {
            dk5.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !vt7.j(next.b(), PortfolioApp.h0.c().getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), ao7.a(false));
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    pq7.b(next2, "appFilter");
                    if (pq7.a(next2.getType(), installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                i06 i06 = new i06();
                i06.setInstalledApp(installedApp);
                i06.setUri(next.c());
                i06.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(i06);
            }
        }
        lm7.q(linkedList);
        return new a(linkedList);
    }
}
