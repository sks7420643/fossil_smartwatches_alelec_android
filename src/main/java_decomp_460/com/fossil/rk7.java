package com.fossil;

import io.flutter.util.Predicate;
import io.flutter.view.AccessibilityBridge;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rk7 implements Predicate {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ AccessibilityBridge.SemanticsNode f3123a;

    @DexIgnore
    public /* synthetic */ rk7(AccessibilityBridge.SemanticsNode semanticsNode) {
        this.f3123a = semanticsNode;
    }

    @DexIgnore
    @Override // io.flutter.util.Predicate
    public final boolean test(Object obj) {
        return AccessibilityBridge.a(this.f3123a, (AccessibilityBridge.SemanticsNode) obj);
    }
}
