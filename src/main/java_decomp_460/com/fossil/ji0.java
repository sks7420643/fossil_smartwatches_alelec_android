package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.sd0;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ji0 extends Service {
    @DexIgnore
    public /* final */ Map<IBinder, IBinder.DeathRecipient> b; // = new zi0();
    @DexIgnore
    public sd0.a c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sd0.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ji0$a$a")
        /* renamed from: com.fossil.ji0$a$a  reason: collision with other inner class name */
        public class C0129a implements IBinder.DeathRecipient {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ li0 f1761a;

            @DexIgnore
            public C0129a(li0 li0) {
                this.f1761a = li0;
            }

            @DexIgnore
            public void binderDied() {
                ji0.this.a(this.f1761a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public int B1(rd0 rd0, String str, Bundle bundle) {
            return ji0.this.e(new li0(rd0), str, bundle);
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public boolean D(rd0 rd0, Uri uri, Bundle bundle, List<Bundle> list) {
            return ji0.this.c(new li0(rd0), uri, bundle, list);
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public boolean F(rd0 rd0, int i, Uri uri, Bundle bundle) {
            return ji0.this.h(new li0(rd0), i, uri, bundle);
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public boolean G1(rd0 rd0) {
            li0 li0 = new li0(rd0);
            try {
                C0129a aVar = new C0129a(li0);
                synchronized (ji0.this.b) {
                    rd0.asBinder().linkToDeath(aVar, 0);
                    ji0.this.b.put(rd0.asBinder(), aVar);
                }
                return ji0.this.d(li0);
            } catch (RemoteException e) {
                return false;
            }
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public boolean Q0(long j) {
            return ji0.this.i(j);
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public boolean Z1(rd0 rd0, Bundle bundle) {
            return ji0.this.g(new li0(rd0), bundle);
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public boolean i2(rd0 rd0, Uri uri) {
            return ji0.this.f(new li0(rd0), uri);
        }

        @DexIgnore
        @Override // com.fossil.sd0
        public Bundle r0(String str, Bundle bundle) {
            return ji0.this.b(str, bundle);
        }
    }

    @DexIgnore
    public boolean a(li0 li0) {
        try {
            synchronized (this.b) {
                IBinder a2 = li0.a();
                a2.unlinkToDeath(this.b.get(a2), 0);
                this.b.remove(a2);
            }
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @DexIgnore
    public abstract Bundle b(String str, Bundle bundle);

    @DexIgnore
    public abstract boolean c(li0 li0, Uri uri, Bundle bundle, List<Bundle> list);

    @DexIgnore
    public abstract boolean d(li0 li0);

    @DexIgnore
    public abstract int e(li0 li0, String str, Bundle bundle);

    @DexIgnore
    public abstract boolean f(li0 li0, Uri uri);

    @DexIgnore
    public abstract boolean g(li0 li0, Bundle bundle);

    @DexIgnore
    public abstract boolean h(li0 li0, int i, Uri uri, Bundle bundle);

    @DexIgnore
    public abstract boolean i(long j);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.c;
    }
}
