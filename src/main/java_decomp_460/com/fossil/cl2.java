package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl2 extends ec2<dl2> {
    @DexIgnore
    public /* final */ Bundle E;

    @DexIgnore
    public cl2(Context context, Looper looper, ac2 ac2, f42 f42, r62.b bVar, r62.c cVar) {
        super(context, looper, 16, ac2, bVar, cVar);
        if (f42 == null) {
            this.E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final Bundle F() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        return queryLocalInterface instanceof dl2 ? (dl2) queryLocalInterface : new el2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public final int s() {
        return h62.f1430a;
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.yb2
    public final boolean v() {
        ac2 o0 = o0();
        return !TextUtils.isEmpty(o0.b()) && !o0.e(e42.c).isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.auth.service.START";
    }
}
