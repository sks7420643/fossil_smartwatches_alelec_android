package com.fossil;

import com.fossil.zu0;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax5 extends zu0.d<SleepSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        pq7.c(sleepSummary, "oldItem");
        pq7.c(sleepSummary2, "newItem");
        return pq7.a(sleepSummary, sleepSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        Date date = null;
        pq7.c(sleepSummary, "oldItem");
        pq7.c(sleepSummary2, "newItem");
        MFSleepDay sleepDay = sleepSummary.getSleepDay();
        Date date2 = sleepDay != null ? sleepDay.getDate() : null;
        MFSleepDay sleepDay2 = sleepSummary2.getSleepDay();
        if (sleepDay2 != null) {
            date = sleepDay2.getDate();
        }
        return lk5.m0(date2, date);
    }
}
