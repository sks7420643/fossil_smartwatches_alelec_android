package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p15 extends o15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362116, 1);
        F.put(2131361851, 2);
        F.put(2131363410, 3);
        F.put(2131362837, 4);
        F.put(2131361960, 5);
        F.put(2131362604, 6);
        F.put(2131363462, 7);
        F.put(2131361952, 8);
        F.put(2131362602, 9);
        F.put(2131363463, 10);
        F.put(2131361950, 11);
        F.put(2131362601, 12);
    }
    */

    @DexIgnore
    public p15(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 13, E, F));
    }

    @DexIgnore
    public p15(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[2], (RelativeLayout) objArr[11], (RelativeLayout) objArr[8], (RelativeLayout) objArr[5], (ConstraintLayout) objArr[1], (RTLImageView) objArr[12], (RTLImageView) objArr[9], (RTLImageView) objArr[6], (LinearLayout) objArr[4], (RelativeLayout) objArr[0], (FlexibleTextView) objArr[3], (View) objArr[7], (View) objArr[10]);
        this.D = -1;
        this.z.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.D != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.D = 1;
        }
        w();
    }
}
