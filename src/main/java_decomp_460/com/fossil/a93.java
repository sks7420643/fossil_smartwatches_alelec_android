package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a93 implements x83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f227a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.client.string_reader", true);

    @DexIgnore
    @Override // com.fossil.x83
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.x83
    public final boolean zzb() {
        return f227a.o().booleanValue();
    }
}
