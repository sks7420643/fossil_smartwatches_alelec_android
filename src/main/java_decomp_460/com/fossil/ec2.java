package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.fc2;
import com.fossil.m62;
import com.fossil.r62;
import com.fossil.yb2;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ec2<T extends IInterface> extends yb2<T> implements m62.f, fc2.a {
    @DexIgnore
    public /* final */ ac2 B;
    @DexIgnore
    public /* final */ Set<Scope> C;
    @DexIgnore
    public /* final */ Account D;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ec2(android.content.Context r10, android.os.Looper r11, int r12, com.fossil.ac2 r13, com.fossil.k72 r14, com.fossil.r72 r15) {
        /*
            r9 = this;
            com.fossil.gc2 r3 = com.fossil.gc2.b(r10)
            com.fossil.c62 r4 = com.fossil.c62.q()
            com.fossil.rc2.k(r14)
            r7 = r14
            com.fossil.k72 r7 = (com.fossil.k72) r7
            com.fossil.rc2.k(r15)
            r8 = r15
            com.fossil.r72 r8 = (com.fossil.r72) r8
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ec2.<init>(android.content.Context, android.os.Looper, int, com.fossil.ac2, com.fossil.k72, com.fossil.r72):void");
    }

    @DexIgnore
    @Deprecated
    public ec2(Context context, Looper looper, int i, ac2 ac2, r62.b bVar, r62.c cVar) {
        this(context, looper, i, ac2, (k72) bVar, (r72) cVar);
    }

    @DexIgnore
    public ec2(Context context, Looper looper, gc2 gc2, c62 c62, int i, ac2 ac2, k72 k72, r72 r72) {
        super(context, looper, gc2, c62, i, q0(k72), r0(r72), ac2.h());
        this.B = ac2;
        this.D = ac2.a();
        this.C = s0(ac2.d());
    }

    @DexIgnore
    public static yb2.a q0(k72 k72) {
        if (k72 == null) {
            return null;
        }
        return new ud2(k72);
    }

    @DexIgnore
    public static yb2.b r0(r72 r72) {
        if (r72 == null) {
            return null;
        }
        return new td2(r72);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final Account C() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final Set<Scope> H() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.m62.f
    public Set<Scope> h() {
        return v() ? this.C : Collections.emptySet();
    }

    @DexIgnore
    public final ac2 o0() {
        return this.B;
    }

    @DexIgnore
    public Set<Scope> p0(Set<Scope> set) {
        return set;
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.yb2
    public int s() {
        return super.s();
    }

    @DexIgnore
    public final Set<Scope> s0(Set<Scope> set) {
        Set<Scope> p0 = p0(set);
        for (Scope scope : p0) {
            if (!set.contains(scope)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return p0;
    }
}
