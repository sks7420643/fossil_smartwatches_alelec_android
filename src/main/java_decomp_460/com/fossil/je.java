package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class je {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f1743a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[gb.values().length];
        f1743a = iArr;
        iArr[gb.GET.ordinal()] = 1;
        f1743a[gb.PUT.ordinal()] = 2;
        int[] iArr2 = new int[ob.values().length];
        b = iArr2;
        iArr2[ob.OTA.ordinal()] = 1;
        b[ob.ACTIVITY_FILE.ordinal()] = 2;
        b[ob.DEVICE_INFO.ordinal()] = 3;
        b[ob.HARDWARE_LOG.ordinal()] = 4;
        b[ob.FONT.ordinal()] = 5;
        b[ob.ALL_FILE.ordinal()] = 6;
        b[ob.ASSET.ordinal()] = 7;
        b[ob.DEVICE_CONFIG.ordinal()] = 8;
        b[ob.ALARM.ordinal()] = 9;
        b[ob.NOTIFICATION_FILTER.ordinal()] = 10;
        b[ob.MUSIC_CONTROL.ordinal()] = 11;
        b[ob.MICRO_APP.ordinal()] = 12;
        b[ob.WATCH_PARAMETERS_FILE.ordinal()] = 13;
        b[ob.REPLY_MESSAGES_FILE.ordinal()] = 14;
        b[ob.DEPRECATED_UI_PACKAGE_FILE.ordinal()] = 15;
        b[ob.UI_PACKAGE_FILE.ordinal()] = 16;
        b[ob.NOTIFICATION.ordinal()] = 17;
        b[ob.UI_SCRIPT.ordinal()] = 18;
        b[ob.LUTS_FILE.ordinal()] = 19;
        b[ob.RATE_FILE.ordinal()] = 20;
        b[ob.DATA_COLLECTION_FILE.ordinal()] = 21;
        b[ob.UI_ENCRYPTED_FILE.ordinal()] = 22;
    }
    */
}
