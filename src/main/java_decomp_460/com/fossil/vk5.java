package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.view.View;
import androidx.core.content.FileProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vk5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f3778a; // = "vk5";

    @DexIgnore
    public static int a(BitmapFactory.Options options, int i, int i2) {
        int i3 = 1;
        int i4 = options.outHeight;
        int i5 = options.outWidth;
        if (i4 > i2 || i5 > i) {
            int i6 = i4 / 2;
            int i7 = i5 / 2;
            while (i6 / i3 >= i2 && i7 / i3 >= i) {
                i3 *= 2;
            }
        }
        return i3;
    }

    @DexIgnore
    public static Bitmap b(InputStream inputStream, int i, int i2) {
        FLogger.INSTANCE.getLocal().d(f3778a, "fastReadingBitmapFromAssets");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    options.inSampleSize = a(options, i, i2);
                    options.inJustDecodeBounds = false;
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    try {
                        byteArrayOutputStream.close();
                        return decodeByteArray;
                    } catch (Exception e) {
                        e.printStackTrace();
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = f3778a;
                        local.e(str, "fastReadingBitmapFromAssets - os close with e=" + e);
                        return decodeByteArray;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f3778a;
            local2.e(str2, "fastReadingBitmapFromAssets - e=" + e2);
            try {
                byteArrayOutputStream.close();
            } catch (Exception e3) {
                e3.printStackTrace();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = f3778a;
                local3.e(str3, "fastReadingBitmapFromAssets - os close with e=" + e3);
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = f3778a;
                local4.e(str4, "fastReadingBitmapFromAssets - os close with e=" + e4);
            }
            throw th;
        }
    }

    @DexIgnore
    public static Bitmap c(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(str, options);
    }

    @DexIgnore
    public static Intent d(Context context) {
        Uri e = e(context);
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", e);
        return intent;
    }

    @DexIgnore
    public static Uri e(Context context) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath(), "pickerImage.jpg");
        return FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
    }

    @DexIgnore
    public static Intent f(Context context) {
        Intent intent;
        Uri e = e(context);
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
        for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent2, 0)) {
            Intent intent3 = new Intent(intent2);
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            intent3.setComponent(new ComponentName(activityInfo.packageName, activityInfo.name));
            intent3.setPackage(resolveInfo.activityInfo.packageName);
            if (e != null) {
                intent3.putExtra("output", e);
            }
            arrayList.add(intent3);
        }
        Intent intent4 = new Intent("android.intent.action.GET_CONTENT");
        intent4.setType("image/*");
        for (ResolveInfo resolveInfo2 : packageManager.queryIntentActivities(intent4, 0)) {
            Intent intent5 = new Intent(intent4);
            ActivityInfo activityInfo2 = resolveInfo2.activityInfo;
            intent5.setComponent(new ComponentName(activityInfo2.packageName, activityInfo2.name));
            intent5.setPackage(resolveInfo2.activityInfo.packageName);
            arrayList.add(intent5);
        }
        Intent intent6 = (Intent) arrayList.get(arrayList.size() - 1);
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                intent = intent6;
                break;
            }
            intent = (Intent) it.next();
            if (intent.getComponent() != null && "com.android.documentsui.DocumentsActivity".equalsIgnoreCase(intent.getComponent().getClassName())) {
                break;
            }
        }
        arrayList.remove(intent);
        Intent createChooser = Intent.createChooser(intent, context.getResources().getString(2131887333));
        createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        return createChooser;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005f, code lost:
        if (r1.equalsIgnoreCase("inline-data") == false) goto L_0x0004;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0006  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.net.Uri g(android.content.Intent r6, android.content.Context r7) {
        /*
            r0 = 0
            if (r6 != 0) goto L_0x0029
        L_0x0003:
            r0 = 1
        L_0x0004:
            if (r0 == 0) goto L_0x0064
            java.lang.String r0 = android.os.Environment.DIRECTORY_PICTURES
            java.io.File r0 = r7.getExternalFilesDir(r0)
            if (r0 == 0) goto L_0x0062
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = java.io.File.separator
            r1.append(r0)
            java.lang.String r0 = "pickerImage.jpg"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
        L_0x0028:
            return r0
        L_0x0029:
            android.net.Uri r1 = r6.getData()
            if (r1 != 0) goto L_0x0004
            java.lang.String r1 = r6.getAction()
            if (r1 == 0) goto L_0x0003
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.vk5.f3778a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Action is "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            java.lang.String r2 = "android.media.action.IMAGE_CAPTURE"
            boolean r2 = r1.equals(r2)
            if (r2 != 0) goto L_0x0003
            java.lang.String r2 = "inline-data"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0004
            goto L_0x0003
        L_0x0062:
            r0 = 0
            goto L_0x0028
        L_0x0064:
            android.net.Uri r0 = r6.getData()
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vk5.g(android.content.Intent, android.content.Context):android.net.Uri");
    }

    @DexIgnore
    public static Bitmap h(View view) {
        view.setDrawingCacheEnabled(true);
        view.destroyDrawingCache();
        view.buildDrawingCache();
        return view.getDrawingCache();
    }
}
