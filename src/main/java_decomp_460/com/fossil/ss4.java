package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss4 {
    @rj4("challengeId")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3295a;
    @DexIgnore
    @rj4("name")
    public String b;
    @DexIgnore
    @rj4("challengeType")
    public String c;
    @DexIgnore
    @rj4("target")
    public Integer d;
    @DexIgnore
    @rj4("duration")
    public Integer e;

    @DexIgnore
    public final String a() {
        return this.f3295a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final Integer c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ss4) {
                ss4 ss4 = (ss4) obj;
                if (!pq7.a(this.f3295a, ss4.f3295a) || !pq7.a(this.b, ss4.b) || !pq7.a(this.c, ss4.c) || !pq7.a(this.d, ss4.d) || !pq7.a(this.e, ss4.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f3295a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Integer num = this.d;
        int hashCode4 = num != null ? num.hashCode() : 0;
        Integer num2 = this.e;
        if (num2 != null) {
            i = num2.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeData(id=" + this.f3295a + ", name=" + this.b + ", type=" + this.c + ", target=" + this.d + ", duration=" + this.e + ")";
    }
}
