package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.m62;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kn2 extends ip2<jo2> {
    @DexIgnore
    public static /* final */ ep2 E; // = ep2.FIT_SENSORS;
    @DexIgnore
    public static /* final */ m62.g<kn2> F; // = new m62.g<>();
    @DexIgnore
    public static /* final */ m62<m62.d.C0151d> G; // = new m62<>("Fitness.SENSORS_API", new ln2(), F);

    /*
    static {
        new m62("Fitness.SENSORS_CLIENT", new nn2(), F);
    }
    */

    @DexIgnore
    public kn2(Context context, Looper looper, ac2 ac2, r62.b bVar, r62.c cVar) {
        super(context, looper, E, bVar, cVar, ac2);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.fitness.internal.IGoogleFitSensorsApi";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitSensorsApi");
        return queryLocalInterface instanceof jo2 ? (jo2) queryLocalInterface : new io2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public final int s() {
        return h62.f1430a;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.fitness.SensorsApi";
    }
}
