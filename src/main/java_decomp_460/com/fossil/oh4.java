package com.fossil;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.fossil.mh4;
import com.fossil.zk0;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oh4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f2683a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ bi4 c;

    @DexIgnore
    public oh4(Context context, bi4 bi4, Executor executor) {
        this.f2683a = executor;
        this.b = context;
        this.c = bi4;
    }

    @DexIgnore
    public boolean a() {
        if (this.c.a("gcm.n.noui")) {
            return true;
        }
        if (b()) {
            return false;
        }
        zh4 d = d();
        mh4.a d2 = mh4.d(this.b, this.c);
        e(d2.f2383a, d);
        c(d2);
        return true;
    }

    @DexIgnore
    public final boolean b() {
        if (((KeyguardManager) this.b.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            return false;
        }
        if (!mf2.h()) {
            SystemClock.sleep(10);
        }
        int myPid = Process.myPid();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.b.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    if (next.importance == 100) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void c(mh4.a aVar) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", "Showing notification");
        }
        ((NotificationManager) this.b.getSystemService("notification")).notify(aVar.b, aVar.c, aVar.f2383a.c());
    }

    @DexIgnore
    public final zh4 d() {
        zh4 c2 = zh4.c(this.c.p("gcm.n.image"));
        if (c2 != null) {
            c2.h(this.f2683a);
        }
        return c2;
    }

    @DexIgnore
    public final void e(zk0.e eVar, zh4 zh4) {
        if (zh4 != null) {
            try {
                Bitmap bitmap = (Bitmap) qt3.b(zh4.f(), 5, TimeUnit.SECONDS);
                eVar.r(bitmap);
                zk0.b bVar = new zk0.b();
                bVar.h(bitmap);
                bVar.g(null);
                eVar.A(bVar);
            } catch (ExecutionException e) {
                String valueOf = String.valueOf(e.getCause());
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 26);
                sb.append("Failed to download image: ");
                sb.append(valueOf);
                Log.w("FirebaseMessaging", sb.toString());
            } catch (InterruptedException e2) {
                Log.w("FirebaseMessaging", "Interrupted while downloading image, showing notification without it");
                zh4.close();
                Thread.currentThread().interrupt();
            } catch (TimeoutException e3) {
                Log.w("FirebaseMessaging", "Failed to download image in time, showing notification without it");
                zh4.close();
            }
        }
    }
}
