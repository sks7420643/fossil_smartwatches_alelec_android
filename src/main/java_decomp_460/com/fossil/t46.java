package com.fossil;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.view.NotificationSummaryDialView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t46 extends pv5 implements s46 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public r46 g;
    @DexIgnore
    public g37<d95> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return t46.j;
        }

        @DexIgnore
        public final t46 b() {
            return new t46();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t46 b;

        @DexIgnore
        public b(t46 t46) {
            this.b = t46;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NotificationSummaryDialView.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ t46 f3362a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(t46 t46) {
            this.f3362a = t46;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NotificationSummaryDialView.a
        public void a(int i) {
            NotificationContactsAndAppsAssignedActivity.B.a(this.f3362a, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ d95 b;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 c;

        @DexIgnore
        public d(d95 d95, dr7 dr7) {
            this.b = d95;
            this.c = dr7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            this.b.v.requestLayout();
            NotificationSummaryDialView notificationSummaryDialView = this.b.v;
            pq7.b(notificationSummaryDialView, "binding.nsdv");
            notificationSummaryDialView.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
        }
    }

    /*
    static {
        String simpleName = t46.class.getSimpleName();
        pq7.b(simpleName, "NotificationDialLandingF\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: L6 */
    public void M5(r46 r46) {
        pq7.c(r46, "presenter");
        this.g = r46;
    }

    @DexIgnore
    @Override // com.fossil.s46
    public void k5(SparseArray<List<BaseFeatureModel>> sparseArray) {
        NotificationSummaryDialView notificationSummaryDialView;
        pq7.c(sparseArray, "data");
        g37<d95> g37 = this.h;
        if (g37 != null) {
            d95 a2 = g37.a();
            if (a2 != null && (notificationSummaryDialView = a2.v) != null) {
                notificationSummaryDialView.setDataAsync(sparseArray);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        d95 d95 = (d95) aq0.f(layoutInflater, 2131558592, viewGroup, false, A6());
        d95.u.setOnClickListener(new b(this));
        d95.v.setOnItemClickListener(new c(this));
        dr7 dr7 = new dr7();
        dr7.element = null;
        dr7.element = (T) new d(d95, dr7);
        NotificationSummaryDialView notificationSummaryDialView = d95.v;
        pq7.b(notificationSummaryDialView, "binding.nsdv");
        notificationSummaryDialView.getViewTreeObserver().addOnGlobalLayoutListener(dr7.element);
        this.h = new g37<>(this, d95);
        pq7.b(d95, "binding");
        return d95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        r46 r46 = this.g;
        if (r46 != null) {
            r46.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        r46 r46 = this.g;
        if (r46 != null) {
            r46.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
