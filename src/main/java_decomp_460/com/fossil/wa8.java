package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodChannel;
import java.io.ByteArrayOutputStream;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wa8 f3910a; // = new wa8();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ua8 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ rp7 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, rp7 rp7, int i2, int i3, int i4, int i5) {
            super(i4, i5);
            this.f = i;
            this.g = rp7;
        }

        @DexIgnore
        @Override // com.fossil.ua8
        /* renamed from: c */
        public void b(Bitmap bitmap, tj1<? super Bitmap> tj1) {
            pq7.c(bitmap, "resource");
            super.c(bitmap, tj1);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(this.f == 1 ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            this.g.invoke(byteArrayOutputStream.toByteArray());
        }

        @DexIgnore
        @Override // com.fossil.qj1
        public void j(Drawable drawable) {
            this.g.invoke(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ua8 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ya8 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, ya8 ya8, int i2, int i3, int i4, int i5) {
            super(i4, i5);
            this.f = i;
            this.g = ya8;
        }

        @DexIgnore
        @Override // com.fossil.ua8
        /* renamed from: c */
        public void b(Bitmap bitmap, tj1<? super Bitmap> tj1) {
            pq7.c(bitmap, "resource");
            super.c(bitmap, tj1);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(this.f == 1 ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            this.g.c(byteArrayOutputStream.toByteArray());
        }

        @DexIgnore
        @Override // com.fossil.va8, com.fossil.qj1
        public void f(Drawable drawable) {
            this.g.c(null);
        }

        @DexIgnore
        @Override // com.fossil.qj1
        public void j(Drawable drawable) {
            this.g.c(null);
        }
    }

    @DexIgnore
    public final void a(Context context, Bitmap bitmap, int i, int i2, int i3, rp7<? super byte[], tl7> rp7) {
        pq7.c(context, "ctx");
        pq7.c(rp7, Constants.CALLBACK);
        oa1.t(context).e().I0(bitmap).C0(new a(i3, rp7, i, i2, i, i2));
    }

    @DexIgnore
    public final void b(Context context, String str, int i, int i2, int i3, MethodChannel.Result result) {
        pq7.c(context, "ctx");
        pq7.c(str, "path");
        oa1.t(context).e().K0(new File(str)).C0(new b(i3, new ya8(result), i, i2, i, i2));
    }
}
