package com.fossil;

import com.fossil.hh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kh4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract kh4 a();

        @DexIgnore
        public abstract a b(b bVar);

        @DexIgnore
        public abstract a c(String str);

        @DexIgnore
        public abstract a d(long j);
    }

    @DexIgnore
    public enum b {
        OK,
        BAD_CONFIG,
        AUTH_ERROR
    }

    @DexIgnore
    public static a a() {
        hh4.b bVar = new hh4.b();
        bVar.d(0);
        return bVar;
    }

    @DexIgnore
    public abstract b b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract long d();
}
