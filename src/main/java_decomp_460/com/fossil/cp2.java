package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp2 implements Parcelable.Creator<zo2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zo2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        uh2 uh2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            if (ad2.l(t) != 1) {
                ad2.B(parcel, t);
            } else {
                uh2 = (uh2) ad2.e(parcel, t, uh2.CREATOR);
            }
        }
        ad2.k(parcel, C);
        return new zo2(uh2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zo2[] newArray(int i) {
        return new zo2[i];
    }
}
