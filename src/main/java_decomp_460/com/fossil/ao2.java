package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao2 extends tn2 implements bo2 {
    @DexIgnore
    public ao2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
    }

    @DexIgnore
    @Override // com.fossil.bo2
    public final void Y1(yi2 yi2) throws RemoteException {
        Parcel d = d();
        qo2.b(d, yi2);
        e(22, d);
    }
}
