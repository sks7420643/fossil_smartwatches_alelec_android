package com.fossil;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.ActionBarContainer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qg0 extends Drawable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ActionBarContainer f2975a;

    @DexIgnore
    public qg0(ActionBarContainer actionBarContainer) {
        this.f2975a = actionBarContainer;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        ActionBarContainer actionBarContainer = this.f2975a;
        if (actionBarContainer.i) {
            Drawable drawable = actionBarContainer.h;
            if (drawable != null) {
                drawable.draw(canvas);
                return;
            }
            return;
        }
        Drawable drawable2 = actionBarContainer.f;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
        ActionBarContainer actionBarContainer2 = this.f2975a;
        Drawable drawable3 = actionBarContainer2.g;
        if (drawable3 != null && actionBarContainer2.j) {
            drawable3.draw(canvas);
        }
    }

    @DexIgnore
    public int getOpacity() {
        return 0;
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        ActionBarContainer actionBarContainer = this.f2975a;
        if (actionBarContainer.i) {
            Drawable drawable = actionBarContainer.h;
            if (drawable != null) {
                drawable.getOutline(outline);
                return;
            }
            return;
        }
        Drawable drawable2 = actionBarContainer.f;
        if (drawable2 != null) {
            drawable2.getOutline(outline);
        }
    }

    @DexIgnore
    public void setAlpha(int i) {
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
    }
}
