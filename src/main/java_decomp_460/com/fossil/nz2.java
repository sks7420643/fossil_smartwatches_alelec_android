package com.fossil;

import com.fossil.mz2;
import com.fossil.nz2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nz2<MessageType extends nz2<MessageType, BuilderType>, BuilderType extends mz2<MessageType, BuilderType>> implements m23 {
    @DexIgnore
    public int zza; // = 0;

    @DexIgnore
    public static <T> void a(Iterable<T> iterable, List<? super T> list) {
        h13.d(iterable);
        if (iterable instanceof w13) {
            List<?> zzd = ((w13) iterable).zzd();
            w13 w13 = (w13) list;
            int size = list.size();
            for (Object obj : zzd) {
                if (obj == null) {
                    int size2 = w13.size();
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(size2 - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size3 = w13.size() - 1; size3 >= size; size3--) {
                        w13.remove(size3);
                    }
                    throw new NullPointerException(sb2);
                } else if (obj instanceof xz2) {
                    w13.V((xz2) obj);
                } else {
                    w13.add((String) obj);
                }
            }
        } else if (iterable instanceof y23) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(((Collection) iterable).size() + list.size());
            }
            int size4 = list.size();
            for (T t : iterable) {
                if (t == null) {
                    int size5 = list.size();
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(size5 - size4);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size6 = list.size() - 1; size6 >= size4; size6--) {
                        list.remove(size6);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(t);
            }
        }
    }

    @DexIgnore
    public final byte[] c() {
        try {
            byte[] bArr = new byte[l()];
            l03 f = l03.f(bArr);
            n(f);
            f.N();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "byte array".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public int f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.m23
    public final xz2 g() {
        try {
            f03 zzc = xz2.zzc(l());
            n(zzc.b());
            return zzc.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public void m(int i) {
        throw new UnsupportedOperationException();
    }
}
