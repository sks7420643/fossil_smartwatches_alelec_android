package com.fossil;

import com.fossil.af1;
import com.fossil.ua1;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ef1 {
    @DexIgnore
    public static /* final */ c e; // = new c();
    @DexIgnore
    public static /* final */ af1<Object, Object> f; // = new a();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<b<?, ?>> f924a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public /* final */ Set<b<?, ?>> c;
    @DexIgnore
    public /* final */ mn0<List<Throwable>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements af1<Object, Object> {
        @DexIgnore
        @Override // com.fossil.af1
        public boolean a(Object obj) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.af1
        public af1.a<Object> b(Object obj, int i, int i2, ob1 ob1) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model, Data> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Class<Model> f925a;
        @DexIgnore
        public /* final */ Class<Data> b;
        @DexIgnore
        public /* final */ bf1<? extends Model, ? extends Data> c;

        @DexIgnore
        public b(Class<Model> cls, Class<Data> cls2, bf1<? extends Model, ? extends Data> bf1) {
            this.f925a = cls;
            this.b = cls2;
            this.c = bf1;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.f925a.isAssignableFrom(cls);
        }

        @DexIgnore
        public boolean b(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <Model, Data> df1<Model, Data> a(List<af1<Model, Data>> list, mn0<List<Throwable>> mn0) {
            return new df1<>(list, mn0);
        }
    }

    @DexIgnore
    public ef1(mn0<List<Throwable>> mn0) {
        this(mn0, e);
    }

    @DexIgnore
    public ef1(mn0<List<Throwable>> mn0, c cVar) {
        this.f924a = new ArrayList();
        this.c = new HashSet();
        this.d = mn0;
        this.b = cVar;
    }

    @DexIgnore
    public static <Model, Data> af1<Model, Data> f() {
        return (af1<Model, Data>) f;
    }

    @DexIgnore
    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, bf1<? extends Model, ? extends Data> bf1, boolean z) {
        b<?, ?> bVar = new b<>(cls, cls2, bf1);
        List<b<?, ?>> list = this.f924a;
        list.add(z ? list.size() : 0, bVar);
    }

    @DexIgnore
    public <Model, Data> void b(Class<Model> cls, Class<Data> cls2, bf1<? extends Model, ? extends Data> bf1) {
        synchronized (this) {
            a(cls, cls2, bf1, true);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: com.fossil.af1<? extends Model, ? extends Data>, com.fossil.af1<Model, Data> */
    public final <Model, Data> af1<Model, Data> c(b<?, ?> bVar) {
        af1 b2 = bVar.c.b(this);
        ik1.d(b2);
        return (af1<? extends Model, ? extends Data>) b2;
    }

    @DexIgnore
    public <Model, Data> af1<Model, Data> d(Class<Model> cls, Class<Data> cls2) {
        af1<Model, Data> f2;
        synchronized (this) {
            try {
                ArrayList arrayList = new ArrayList();
                boolean z = false;
                for (b<?, ?> bVar : this.f924a) {
                    if (this.c.contains(bVar)) {
                        z = true;
                    } else if (bVar.b(cls, cls2)) {
                        this.c.add(bVar);
                        arrayList.add(c(bVar));
                        this.c.remove(bVar);
                    }
                }
                if (arrayList.size() > 1) {
                    f2 = this.b.a(arrayList, this.d);
                } else if (arrayList.size() == 1) {
                    f2 = (af1) arrayList.get(0);
                } else if (z) {
                    f2 = f();
                } else {
                    throw new ua1.c((Class<?>) cls, (Class<?>) cls2);
                }
            } catch (Throwable th) {
                this.c.clear();
                throw th;
            }
        }
        return f2;
    }

    @DexIgnore
    public <Model> List<af1<Model, ?>> e(Class<Model> cls) {
        ArrayList arrayList;
        synchronized (this) {
            try {
                arrayList = new ArrayList();
                for (b<?, ?> bVar : this.f924a) {
                    if (!this.c.contains(bVar) && bVar.a(cls)) {
                        this.c.add(bVar);
                        arrayList.add(c(bVar));
                        this.c.remove(bVar);
                    }
                }
            } catch (Throwable th) {
                this.c.clear();
                throw th;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<Class<?>> g(Class<?> cls) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (b<?, ?> bVar : this.f924a) {
                if (!arrayList.contains(bVar.b) && bVar.a(cls)) {
                    arrayList.add(bVar.b);
                }
            }
        }
        return arrayList;
    }
}
