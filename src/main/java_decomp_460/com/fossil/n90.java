package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum n90 {
    ROUTE_IMAGE("req_route"),
    DISTANCE("req_distance");
    
    @DexIgnore
    public static /* final */ m90 f; // = new m90(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public n90(String str) {
        this.b = str;
    }
}
