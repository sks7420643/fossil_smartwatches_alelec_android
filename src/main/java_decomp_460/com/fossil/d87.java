package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.viewpager2.widget.ViewPager2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d87 extends pv5 {
    @DexIgnore
    public /* final */ ArrayList<Fragment> g; // = new ArrayList<>();
    @DexIgnore
    public pg5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d87 b;

        @DexIgnore
        public a(d87 d87) {
            this.b = d87;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d87 b;

        @DexIgnore
        public b(d87 d87) {
            this.b = d87;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Q6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ d87 f747a;

        @DexIgnore
        public c(d87 d87) {
            this.f747a = d87;
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationContainerFragment", "watchFaceComplicationsLive, value = " + ((Object) t2));
            if (!t2.b().b().isEmpty()) {
                String a2 = t2.b().a();
                if (!(a2 == null || vt7.l(a2))) {
                    FlexibleButton flexibleButton = d87.K6(this.f747a).c;
                    pq7.b(flexibleButton, "mBinding.fbRing");
                    flexibleButton.setVisibility(0);
                    return;
                }
            }
            this.f747a.P6();
            FlexibleButton flexibleButton2 = d87.K6(this.f747a).c;
            pq7.b(flexibleButton2, "mBinding.fbRing");
            flexibleButton2.setVisibility(4);
        }
    }

    @DexIgnore
    public static final /* synthetic */ pg5 K6(d87 d87) {
        pg5 pg5 = d87.h;
        if (pg5 != null) {
            return pg5;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "WatchFaceComplicationContainerFragment";
    }

    @DexIgnore
    public final void N6() {
        this.g.clear();
        f87 f87 = (f87) getChildFragmentManager().Z("WatchFaceComplicationFragment");
        k87 k87 = (k87) getChildFragmentManager().Z("WatchFaceRingFragment");
        if (f87 == null) {
            f87 = f87.l.a();
        }
        if (k87 == null) {
            k87 = k87.l.a();
        }
        this.g.add(f87);
        this.g.add(k87);
        pg5 pg5 = this.h;
        if (pg5 != null) {
            ViewPager2 viewPager2 = pg5.e;
            pq7.b(viewPager2, "it");
            viewPager2.setAdapter(new g67(getChildFragmentManager(), this.g));
            viewPager2.setUserInputEnabled(false);
            viewPager2.setOffscreenPageLimit(2);
            hc7 hc7 = hc7.c;
            Fragment requireParentFragment = requireParentFragment();
            pq7.b(requireParentFragment, "requireParentFragment()");
            hc7.b(requireParentFragment, f87);
            hc7 hc72 = hc7.c;
            Fragment requireParentFragment2 = requireParentFragment();
            pq7.b(requireParentFragment2, "requireParentFragment()");
            hc72.b(requireParentFragment2, k87);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        N6();
        P6();
        pg5 pg5 = this.h;
        if (pg5 != null) {
            pg5.b.setOnClickListener(new a(this));
            pg5.c.setOnClickListener(new b(this));
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        pg5 pg5 = this.h;
        if (pg5 != null) {
            pg5.c.d("flexible_button_secondary");
            pg5.b.d("flexible_button_primary");
            pg5.e.j(0, false);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        pg5 pg5 = this.h;
        if (pg5 != null) {
            pg5.b.d("flexible_button_secondary");
            pg5.c.d("flexible_button_primary");
            pg5.e.j(1, false);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        LiveData<eb7> q;
        super.onActivityCreated(bundle);
        String d = qn5.l.a().d("nonBrandSurface");
        if (!TextUtils.isEmpty(d)) {
            pg5 pg5 = this.h;
            if (pg5 != null) {
                pg5.d.setBackgroundColor(Color.parseColor(d));
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        hc7 hc7 = hc7.c;
        Fragment requireParentFragment = requireParentFragment();
        pq7.b(requireParentFragment, "requireParentFragment()");
        gc7 f = hc7.f(requireParentFragment);
        if (f != null && (q = f.q()) != null) {
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            pq7.b(viewLifecycleOwner, "viewLifecycleOwner");
            q.h(viewLifecycleOwner, new c(this));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        pg5 c2 = pg5.c(layoutInflater);
        pq7.b(c2, "WatchFaceComplicationCon\u2026Binding.inflate(inflater)");
        this.h = c2;
        O6();
        pg5 pg5 = this.h;
        if (pg5 != null) {
            ConstraintLayout b2 = pg5.b();
            pq7.b(b2, "mBinding.root");
            return b2;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
