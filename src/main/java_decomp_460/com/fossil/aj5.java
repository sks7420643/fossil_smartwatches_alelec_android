package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aj5 extends ri5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f276a;
    @DexIgnore
    public /* final */ Boolean b;

    @DexIgnore
    public aj5(int i, boolean z) {
        this.f276a = i;
        this.b = Boolean.valueOf(z);
    }

    @DexIgnore
    public int a() {
        return this.f276a;
    }

    @DexIgnore
    public boolean b() {
        return this.b.booleanValue();
    }
}
