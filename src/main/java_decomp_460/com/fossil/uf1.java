package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uf1<DataType> implements qb1<DataType, BitmapDrawable> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qb1<DataType, Bitmap> f3578a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore
    public uf1(Resources resources, qb1<DataType, Bitmap> qb1) {
        ik1.d(resources);
        this.b = resources;
        ik1.d(qb1);
        this.f3578a = qb1;
    }

    @DexIgnore
    @Override // com.fossil.qb1
    public boolean a(DataType datatype, ob1 ob1) throws IOException {
        return this.f3578a.a(datatype, ob1);
    }

    @DexIgnore
    @Override // com.fossil.qb1
    public id1<BitmapDrawable> b(DataType datatype, int i, int i2, ob1 ob1) throws IOException {
        return og1.f(this.b, this.f3578a.b(datatype, i, i2, ob1));
    }
}
