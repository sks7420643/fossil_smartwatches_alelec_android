package com.fossil;

import com.fossil.yg4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xg4 implements ah4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ bh4 f4118a;
    @DexIgnore
    public /* final */ ot3<yg4> b;

    @DexIgnore
    public xg4(bh4 bh4, ot3<yg4> ot3) {
        this.f4118a = bh4;
        this.b = ot3;
    }

    @DexIgnore
    @Override // com.fossil.ah4
    public boolean a(fh4 fh4, Exception exc) {
        if (!fh4.i() && !fh4.j() && !fh4.l()) {
            return false;
        }
        this.b.d(exc);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ah4
    public boolean b(fh4 fh4) {
        if (!fh4.k() || this.f4118a.b(fh4)) {
            return false;
        }
        ot3<yg4> ot3 = this.b;
        yg4.a a2 = yg4.a();
        a2.b(fh4.b());
        a2.d(fh4.c());
        a2.c(fh4.h());
        ot3.c(a2.a());
        return true;
    }
}
