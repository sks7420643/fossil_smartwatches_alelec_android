package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr3 extends wr3 {
    @DexIgnore
    public ku2 g;
    @DexIgnore
    public /* final */ /* synthetic */ pr3 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vr3(pr3 pr3, String str, int i, ku2 ku2) {
        super(str, i);
        this.h = pr3;
        this.g = ku2;
    }

    @DexIgnore
    @Override // com.fossil.wr3
    public final int a() {
        return this.g.G();
    }

    @DexIgnore
    @Override // com.fossil.wr3
    public final boolean i() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.wr3
    public final boolean j() {
        return false;
    }

    @DexIgnore
    public final boolean k(Long l, Long l2, ev2 ev2, boolean z) {
        Boolean bool = null;
        boolean z2 = e63.a() && this.h.m().y(this.f3988a, xg3.e0);
        boolean J = this.g.J();
        boolean K = this.g.K();
        boolean M = this.g.M();
        boolean z3 = J || K || M;
        if (!z || z3) {
            iu2 I = this.g.I();
            boolean K2 = I.K();
            if (ev2.V()) {
                if (!I.H()) {
                    this.h.d().I().b("No number filter for long property. property", this.h.j().z(ev2.R()));
                } else {
                    bool = wr3.d(wr3.c(ev2.W(), I.I()), K2);
                }
            } else if (ev2.X()) {
                if (!I.H()) {
                    this.h.d().I().b("No number filter for double property. property", this.h.j().z(ev2.R()));
                } else {
                    bool = wr3.d(wr3.b(ev2.Y(), I.I()), K2);
                }
            } else if (!ev2.T()) {
                this.h.d().I().b("User property has no value, property", this.h.j().z(ev2.R()));
            } else if (I.E()) {
                bool = wr3.d(wr3.g(ev2.U(), I.G(), this.h.d()), K2);
            } else if (!I.H()) {
                this.h.d().I().b("No string or number filter defined. property", this.h.j().z(ev2.R()));
            } else if (gr3.S(ev2.U())) {
                bool = wr3.d(wr3.e(ev2.U(), I.I()), K2);
            } else {
                this.h.d().I().c("Invalid user property value for Numeric number filter. property, value", this.h.j().z(ev2.R()), ev2.U());
            }
            this.h.d().N().b("Property filter result", bool == null ? "null" : bool);
            if (bool == null) {
                return false;
            }
            this.c = Boolean.TRUE;
            if (M && !bool.booleanValue()) {
                return true;
            }
            if (!z || this.g.J()) {
                this.d = bool;
            }
            if (!bool.booleanValue() || !z3 || !ev2.K()) {
                return true;
            }
            long L = ev2.L();
            if (l != null) {
                L = l.longValue();
            }
            if (z2 && this.g.J() && !this.g.K() && l2 != null) {
                L = l2.longValue();
            }
            if (this.g.K()) {
                this.f = Long.valueOf(L);
                return true;
            }
            this.e = Long.valueOf(L);
            return true;
        }
        this.h.d().N().c("Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID", Integer.valueOf(this.b), this.g.E() ? Integer.valueOf(this.g.G()) : null);
        return true;
    }
}
