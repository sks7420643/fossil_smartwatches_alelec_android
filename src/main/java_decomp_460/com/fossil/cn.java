package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn extends ro {
    @DexIgnore
    public /* final */ mt1 S;
    @DexIgnore
    public /* final */ ob T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ cn(k5 k5Var, i60 i60, mt1 mt1, ob obVar, String str, int i) {
        super(k5Var, i60, yp.V, true, ke.b.b(k5Var.x, obVar), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? e.a("UUID.randomUUID().toString()") : str, true, 32);
        boolean z = mt1 instanceof zt1;
        this.S = mt1;
        this.T = obVar;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.Z2, this.S.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.ro
    public byte[] M() {
        mt1 mt1 = this.S;
        short b = ke.b.b(this.w.x, this.T);
        ry1 ry1 = this.x.a().h().get(Short.valueOf(this.T.b));
        if (ry1 == null) {
            ry1 = hd0.y.d();
        }
        return mt1.a(b, ry1);
    }
}
