package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o83 implements l83 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2646a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.collection.log_event_and_bundle_v2", true);

    @DexIgnore
    @Override // com.fossil.l83
    public final boolean zza() {
        return f2646a.o().booleanValue();
    }
}
