package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sh5 {
    Jawbone("jawbone"),
    UnderArmour("underarmour"),
    HealthKit("healthkit"),
    GoogleFit("googlefit");
    
    @DexIgnore
    public /* final */ String name;

    @DexIgnore
    public sh5(String str) {
        this.name = str;
    }

    @DexIgnore
    public static sh5 fromName(String str) {
        sh5[] values = values();
        for (sh5 sh5 : values) {
            if (sh5.name.equalsIgnoreCase(str)) {
                return sh5;
            }
        }
        return null;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
