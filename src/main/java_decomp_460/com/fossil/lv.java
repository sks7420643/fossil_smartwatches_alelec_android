package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv extends dv {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public /* final */ long P;
    @DexIgnore
    public /* final */ long Q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ lv(long j, long j2, short s, k5 k5Var, int i, int i2) {
        super(iu.h, s, hs.p, k5Var, (i2 & 16) != 0 ? 3 : i);
        this.P = j;
        this.Q = j2;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(g80.k(g80.k(super.A(), jd0.L0, Long.valueOf(this.M)), jd0.M0, Long.valueOf(this.N)), jd0.N0, Long.valueOf(this.O));
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 12) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = hy1.o(order.getInt(0));
            this.N = hy1.o(order.getInt(4));
            this.O = hy1.o(order.getInt(8));
            g80.k(g80.k(g80.k(jSONObject, jd0.L0, Long.valueOf(this.M)), jd0.M0, Long.valueOf(this.N)), jd0.N0, Long.valueOf(this.O));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.P).putInt((int) this.Q).array();
        pq7.b(array, "ByteBuffer.allocate(8)\n \u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.dv, com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.c1, Long.valueOf(this.P)), jd0.d1, Long.valueOf(this.Q));
    }
}
