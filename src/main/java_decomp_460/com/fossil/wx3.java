package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wx3 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wx3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ hy3 b;
    @DexIgnore
    public /* final */ hy3 c;
    @DexIgnore
    public /* final */ hy3 d;
    @DexIgnore
    public /* final */ c e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wx3> {
        @DexIgnore
        /* renamed from: a */
        public wx3 createFromParcel(Parcel parcel) {
            return new wx3((hy3) parcel.readParcelable(hy3.class.getClassLoader()), (hy3) parcel.readParcelable(hy3.class.getClassLoader()), (hy3) parcel.readParcelable(hy3.class.getClassLoader()), (c) parcel.readParcelable(c.class.getClassLoader()), null);
        }

        @DexIgnore
        /* renamed from: b */
        public wx3[] newArray(int i) {
            return new wx3[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ long e; // = ny3.a(hy3.b(1900, 0).h);
        @DexIgnore
        public static /* final */ long f; // = ny3.a(hy3.b(2100, 11).h);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public long f4014a; // = e;
        @DexIgnore
        public long b; // = f;
        @DexIgnore
        public Long c;
        @DexIgnore
        public c d; // = by3.a(Long.MIN_VALUE);

        @DexIgnore
        public b(wx3 wx3) {
            this.f4014a = wx3.b.h;
            this.b = wx3.c.h;
            this.c = Long.valueOf(wx3.d.h);
            this.d = wx3.e;
        }

        @DexIgnore
        public wx3 a() {
            if (this.c == null) {
                long M6 = ey3.M6();
                if (this.f4014a > M6 || M6 > this.b) {
                    M6 = this.f4014a;
                }
                this.c = Long.valueOf(M6);
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("DEEP_COPY_VALIDATOR_KEY", this.d);
            return new wx3(hy3.c(this.f4014a), hy3.c(this.b), hy3.c(this.c.longValue()), (c) bundle.getParcelable("DEEP_COPY_VALIDATOR_KEY"), null);
        }

        @DexIgnore
        public b b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public interface c extends Parcelable {
        @DexIgnore
        boolean S(long j);
    }

    @DexIgnore
    public wx3(hy3 hy3, hy3 hy32, hy3 hy33, c cVar) {
        this.b = hy3;
        this.c = hy32;
        this.d = hy33;
        this.e = cVar;
        if (hy3.compareTo(hy33) > 0) {
            throw new IllegalArgumentException("start Month cannot be after current Month");
        } else if (hy33.compareTo(hy32) <= 0) {
            this.g = hy3.m(hy32) + 1;
            this.f = (hy32.e - hy3.e) + 1;
        } else {
            throw new IllegalArgumentException("current Month cannot be after end Month");
        }
    }

    @DexIgnore
    public /* synthetic */ wx3(hy3 hy3, hy3 hy32, hy3 hy33, c cVar, a aVar) {
        this(hy3, hy32, hy33, cVar);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public c e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof wx3)) {
            return false;
        }
        wx3 wx3 = (wx3) obj;
        return this.b.equals(wx3.b) && this.c.equals(wx3.c) && this.d.equals(wx3.d) && this.e.equals(wx3.e);
    }

    @DexIgnore
    public hy3 f() {
        return this.c;
    }

    @DexIgnore
    public int g() {
        return this.g;
    }

    @DexIgnore
    public hy3 h() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.b, this.c, this.d, this.e});
    }

    @DexIgnore
    public hy3 i() {
        return this.b;
    }

    @DexIgnore
    public int k() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.b, 0);
        parcel.writeParcelable(this.c, 0);
        parcel.writeParcelable(this.d, 0);
        parcel.writeParcelable(this.e, 0);
    }
}
