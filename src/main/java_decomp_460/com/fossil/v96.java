package com.fossil;

import android.os.Parcelable;
import com.portfolio.platform.data.model.Category;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v96 extends fq4 {
    @DexIgnore
    public abstract void n();

    @DexIgnore
    public abstract void o();

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public abstract void q(Category category);

    @DexIgnore
    public abstract void r(String str);

    @DexIgnore
    public abstract void s(k76 k76);

    @DexIgnore
    public abstract void t(String str, Parcelable parcelable);
}
