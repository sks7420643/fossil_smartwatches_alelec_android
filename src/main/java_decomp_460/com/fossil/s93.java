package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s93 extends pu2 implements t93 {
    @DexIgnore
    public s93() {
        super("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    public static t93 asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
        return queryLocalInterface instanceof t93 ? (t93) queryLocalInterface : new v93(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.pu2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        u93 w93;
        u93 w932;
        u93 u93 = null;
        u93 w933 = null;
        us2 ws2 = null;
        us2 ws22 = null;
        us2 ws23 = null;
        u93 w934 = null;
        u93 w935 = null;
        u93 w936 = null;
        u93 w937 = null;
        u93 w938 = null;
        u93 w939 = null;
        vs2 ys2 = null;
        u93 w9310 = null;
        u93 w9311 = null;
        u93 w9312 = null;
        u93 w9313 = null;
        switch (i) {
            case 1:
                initialize(rg2.a.e(parcel.readStrongBinder()), (xs2) qt2.a(parcel, xs2.CREATOR), parcel.readLong());
                break;
            case 2:
                logEvent(parcel.readString(), parcel.readString(), (Bundle) qt2.a(parcel, Bundle.CREATOR), qt2.e(parcel), qt2.e(parcel), parcel.readLong());
                break;
            case 3:
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                Bundle bundle = (Bundle) qt2.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    w93 = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w93 = queryLocalInterface instanceof u93 ? (u93) queryLocalInterface : new w93(readStrongBinder);
                }
                logEventAndBundle(readString, readString2, bundle, w93, parcel.readLong());
                break;
            case 4:
                setUserProperty(parcel.readString(), parcel.readString(), rg2.a.e(parcel.readStrongBinder()), qt2.e(parcel), parcel.readLong());
                break;
            case 5:
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                boolean e = qt2.e(parcel);
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    u93 = queryLocalInterface2 instanceof u93 ? (u93) queryLocalInterface2 : new w93(readStrongBinder2);
                }
                getUserProperties(readString3, readString4, e, u93);
                break;
            case 6:
                String readString5 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w9313 = queryLocalInterface3 instanceof u93 ? (u93) queryLocalInterface3 : new w93(readStrongBinder3);
                }
                getMaxUserProperties(readString5, w9313);
                break;
            case 7:
                setUserId(parcel.readString(), parcel.readLong());
                break;
            case 8:
                setConditionalUserProperty((Bundle) qt2.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 9:
                clearConditionalUserProperty(parcel.readString(), parcel.readString(), (Bundle) qt2.a(parcel, Bundle.CREATOR));
                break;
            case 10:
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w9312 = queryLocalInterface4 instanceof u93 ? (u93) queryLocalInterface4 : new w93(readStrongBinder4);
                }
                getConditionalUserProperties(readString6, readString7, w9312);
                break;
            case 11:
                setMeasurementEnabled(qt2.e(parcel), parcel.readLong());
                break;
            case 12:
                resetAnalyticsData(parcel.readLong());
                break;
            case 13:
                setMinimumSessionDuration(parcel.readLong());
                break;
            case 14:
                setSessionTimeoutDuration(parcel.readLong());
                break;
            case 15:
                setCurrentScreen(rg2.a.e(parcel.readStrongBinder()), parcel.readString(), parcel.readString(), parcel.readLong());
                break;
            case 16:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w9311 = queryLocalInterface5 instanceof u93 ? (u93) queryLocalInterface5 : new w93(readStrongBinder5);
                }
                getCurrentScreenName(w9311);
                break;
            case 17:
                IBinder readStrongBinder6 = parcel.readStrongBinder();
                if (readStrongBinder6 != null) {
                    IInterface queryLocalInterface6 = readStrongBinder6.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w9310 = queryLocalInterface6 instanceof u93 ? (u93) queryLocalInterface6 : new w93(readStrongBinder6);
                }
                getCurrentScreenClass(w9310);
                break;
            case 18:
                IBinder readStrongBinder7 = parcel.readStrongBinder();
                if (readStrongBinder7 != null) {
                    IInterface queryLocalInterface7 = readStrongBinder7.queryLocalInterface("com.google.android.gms.measurement.api.internal.IStringProvider");
                    ys2 = queryLocalInterface7 instanceof vs2 ? (vs2) queryLocalInterface7 : new ys2(readStrongBinder7);
                }
                setInstanceIdProvider(ys2);
                break;
            case 19:
                IBinder readStrongBinder8 = parcel.readStrongBinder();
                if (readStrongBinder8 != null) {
                    IInterface queryLocalInterface8 = readStrongBinder8.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w939 = queryLocalInterface8 instanceof u93 ? (u93) queryLocalInterface8 : new w93(readStrongBinder8);
                }
                getCachedAppInstanceId(w939);
                break;
            case 20:
                IBinder readStrongBinder9 = parcel.readStrongBinder();
                if (readStrongBinder9 != null) {
                    IInterface queryLocalInterface9 = readStrongBinder9.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w938 = queryLocalInterface9 instanceof u93 ? (u93) queryLocalInterface9 : new w93(readStrongBinder9);
                }
                getAppInstanceId(w938);
                break;
            case 21:
                IBinder readStrongBinder10 = parcel.readStrongBinder();
                if (readStrongBinder10 != null) {
                    IInterface queryLocalInterface10 = readStrongBinder10.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w937 = queryLocalInterface10 instanceof u93 ? (u93) queryLocalInterface10 : new w93(readStrongBinder10);
                }
                getGmpAppId(w937);
                break;
            case 22:
                IBinder readStrongBinder11 = parcel.readStrongBinder();
                if (readStrongBinder11 != null) {
                    IInterface queryLocalInterface11 = readStrongBinder11.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w936 = queryLocalInterface11 instanceof u93 ? (u93) queryLocalInterface11 : new w93(readStrongBinder11);
                }
                generateEventId(w936);
                break;
            case 23:
                beginAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 24:
                endAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 25:
                onActivityStarted(rg2.a.e(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 26:
                onActivityStopped(rg2.a.e(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 27:
                onActivityCreated(rg2.a.e(parcel.readStrongBinder()), (Bundle) qt2.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 28:
                onActivityDestroyed(rg2.a.e(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 29:
                onActivityPaused(rg2.a.e(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 30:
                onActivityResumed(rg2.a.e(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 31:
                rg2 e2 = rg2.a.e(parcel.readStrongBinder());
                IBinder readStrongBinder12 = parcel.readStrongBinder();
                if (readStrongBinder12 != null) {
                    IInterface queryLocalInterface12 = readStrongBinder12.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w935 = queryLocalInterface12 instanceof u93 ? (u93) queryLocalInterface12 : new w93(readStrongBinder12);
                }
                onActivitySaveInstanceState(e2, w935, parcel.readLong());
                break;
            case 32:
                Bundle bundle2 = (Bundle) qt2.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder13 = parcel.readStrongBinder();
                if (readStrongBinder13 != null) {
                    IInterface queryLocalInterface13 = readStrongBinder13.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w934 = queryLocalInterface13 instanceof u93 ? (u93) queryLocalInterface13 : new w93(readStrongBinder13);
                }
                performAction(bundle2, w934, parcel.readLong());
                break;
            case 33:
                logHealthData(parcel.readInt(), parcel.readString(), rg2.a.e(parcel.readStrongBinder()), rg2.a.e(parcel.readStrongBinder()), rg2.a.e(parcel.readStrongBinder()));
                break;
            case 34:
                IBinder readStrongBinder14 = parcel.readStrongBinder();
                if (readStrongBinder14 != null) {
                    IInterface queryLocalInterface14 = readStrongBinder14.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    ws23 = queryLocalInterface14 instanceof us2 ? (us2) queryLocalInterface14 : new ws2(readStrongBinder14);
                }
                setEventInterceptor(ws23);
                break;
            case 35:
                IBinder readStrongBinder15 = parcel.readStrongBinder();
                if (readStrongBinder15 != null) {
                    IInterface queryLocalInterface15 = readStrongBinder15.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    ws22 = queryLocalInterface15 instanceof us2 ? (us2) queryLocalInterface15 : new ws2(readStrongBinder15);
                }
                registerOnMeasurementEventListener(ws22);
                break;
            case 36:
                IBinder readStrongBinder16 = parcel.readStrongBinder();
                if (readStrongBinder16 != null) {
                    IInterface queryLocalInterface16 = readStrongBinder16.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    ws2 = queryLocalInterface16 instanceof us2 ? (us2) queryLocalInterface16 : new ws2(readStrongBinder16);
                }
                unregisterOnMeasurementEventListener(ws2);
                break;
            case 37:
                initForTests(qt2.f(parcel));
                break;
            case 38:
                IBinder readStrongBinder17 = parcel.readStrongBinder();
                if (readStrongBinder17 != null) {
                    IInterface queryLocalInterface17 = readStrongBinder17.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w933 = queryLocalInterface17 instanceof u93 ? (u93) queryLocalInterface17 : new w93(readStrongBinder17);
                }
                getTestFlag(w933, parcel.readInt());
                break;
            case 39:
                setDataCollectionEnabled(qt2.e(parcel));
                break;
            case 40:
                IBinder readStrongBinder18 = parcel.readStrongBinder();
                if (readStrongBinder18 == null) {
                    w932 = null;
                } else {
                    IInterface queryLocalInterface18 = readStrongBinder18.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    w932 = queryLocalInterface18 instanceof u93 ? (u93) queryLocalInterface18 : new w93(readStrongBinder18);
                }
                isDataCollectionEnabled(w932);
                break;
            case 41:
            default:
                return false;
            case 42:
                setDefaultEventParameters((Bundle) qt2.a(parcel, Bundle.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
