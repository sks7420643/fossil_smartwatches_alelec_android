package com.fossil;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yk4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public a<String, Pattern> f4327a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public LinkedHashMap<K, V> f4328a;
        @DexIgnore
        public int b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yk4$a$a")
        /* renamed from: com.fossil.yk4$a$a  reason: collision with other inner class name */
        public class C0296a extends LinkedHashMap<K, V> {
            @DexIgnore
            public C0296a(int i, float f, boolean z) {
                super(i, f, z);
            }

            @DexIgnore
            @Override // java.util.LinkedHashMap
            public boolean removeEldestEntry(Map.Entry<K, V> entry) {
                return size() > a.this.b;
            }
        }

        @DexIgnore
        public a(int i) {
            this.b = i;
            this.f4328a = new C0296a(((i * 4) / 3) + 1, 0.75f, true);
        }

        @DexIgnore
        public V b(K k) {
            V v;
            synchronized (this) {
                v = this.f4328a.get(k);
            }
            return v;
        }

        @DexIgnore
        public void c(K k, V v) {
            synchronized (this) {
                this.f4328a.put(k, v);
            }
        }
    }

    @DexIgnore
    public yk4(int i) {
        this.f4327a = new a<>(i);
    }

    @DexIgnore
    public Pattern a(String str) {
        Pattern b = this.f4327a.b(str);
        if (b != null) {
            return b;
        }
        Pattern compile = Pattern.compile(str);
        this.f4327a.c(str, compile);
        return compile;
    }
}
