package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y68 implements Comparable<y68> {
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public d d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {
        @DexIgnore
        public static /* final */ BigInteger c; // = new BigInteger("0");
        @DexIgnore
        public static /* final */ b d; // = new b();
        @DexIgnore
        public /* final */ BigInteger b;

        @DexIgnore
        public b() {
            this.b = c;
        }

        @DexIgnore
        public b(String str) {
            this.b = new BigInteger(str);
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public int compareTo(c cVar) {
            if (cVar == null) {
                return !c.equals(this.b);
            }
            int type = cVar.getType();
            if (type == 0) {
                return this.b.compareTo(((b) cVar).b);
            }
            if (type == 1 || type == 2) {
                return 1;
            }
            throw new RuntimeException("invalid item: " + cVar.getClass());
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public int getType() {
            return 0;
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public boolean isNull() {
            return c.equals(this.b);
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        int compareTo(c cVar);

        @DexIgnore
        int getType();

        @DexIgnore
        boolean isNull();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends ArrayList<c> implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public int compareTo(c cVar) {
            int compareTo;
            if (cVar != null) {
                int type = cVar.getType();
                if (type == 0) {
                    return -1;
                }
                if (type == 1) {
                    return 1;
                }
                if (type == 2) {
                    Iterator it = iterator();
                    Iterator it2 = ((d) cVar).iterator();
                    do {
                        if (!it.hasNext() && !it2.hasNext()) {
                            return 0;
                        }
                        c cVar2 = it.hasNext() ? (c) it.next() : null;
                        c cVar3 = it2.hasNext() ? (c) it2.next() : null;
                        if (cVar2 != null) {
                            compareTo = cVar2.compareTo(cVar3);
                            continue;
                        } else if (cVar3 == null) {
                            compareTo = 0;
                            continue;
                        } else {
                            compareTo = cVar3.compareTo(cVar2) * -1;
                            continue;
                        }
                    } while (compareTo == 0);
                    return compareTo;
                }
                throw new RuntimeException("invalid item: " + cVar.getClass());
            } else if (size() == 0) {
                return 0;
            } else {
                return ((c) get(0)).compareTo(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public int getType() {
            return 2;
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public boolean isNull() {
            return size() == 0;
        }

        @DexIgnore
        public void normalize() {
            for (int size = size() - 1; size >= 0; size--) {
                c cVar = (c) get(size);
                if (cVar.isNull()) {
                    remove(size);
                } else if (!(cVar instanceof d)) {
                    return;
                }
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator it = iterator();
            while (it.hasNext()) {
                c cVar = (c) it.next();
                if (sb.length() > 0) {
                    sb.append(cVar instanceof d ? '-' : '.');
                }
                sb.append(cVar);
            }
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements c {
        @DexIgnore
        public static /* final */ String[] c;
        @DexIgnore
        public static /* final */ List<String> d;
        @DexIgnore
        public static /* final */ Properties e;
        @DexIgnore
        public static /* final */ String f; // = String.valueOf(d.indexOf(""));
        @DexIgnore
        public String b;

        /*
        static {
            String[] strArr = {"alpha", "beta", "milestone", "rc", "snapshot", "", "sp"};
            c = strArr;
            d = Arrays.asList(strArr);
            Properties properties = new Properties();
            e = properties;
            properties.put("ga", "");
            e.put("final", "");
            e.put("cr", "rc");
        }
        */

        @DexIgnore
        public e(String str, boolean z) {
            if (z && str.length() == 1) {
                char charAt = str.charAt(0);
                if (charAt == 'a') {
                    str = "alpha";
                } else if (charAt == 'b') {
                    str = "beta";
                } else if (charAt == 'm') {
                    str = "milestone";
                }
            }
            this.b = e.getProperty(str, str);
        }

        @DexIgnore
        public static String a(String str) {
            int indexOf = d.indexOf(str);
            if (indexOf != -1) {
                return String.valueOf(indexOf);
            }
            return d.size() + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + str;
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public int compareTo(c cVar) {
            if (cVar == null) {
                return a(this.b).compareTo(f);
            }
            int type = cVar.getType();
            if (type == 0) {
                return -1;
            }
            if (type == 1) {
                return a(this.b).compareTo(a(((e) cVar).b));
            }
            if (type == 2) {
                return -1;
            }
            throw new RuntimeException("invalid item: " + cVar.getClass());
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public int getType() {
            return 1;
        }

        @DexIgnore
        @Override // com.fossil.y68.c
        public boolean isNull() {
            return a(this.b).compareTo(f) == 0;
        }

        @DexIgnore
        public String toString() {
            return this.b;
        }
    }

    @DexIgnore
    public y68(String str) {
        c(str);
    }

    @DexIgnore
    public static c b(boolean z, String str) {
        return z ? new b(str) : new e(str, false);
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(y68 y68) {
        return this.d.compareTo(y68.d);
    }

    @DexIgnore
    public final void c(String str) {
        this.b = str;
        this.d = new d();
        String lowerCase = str.toLowerCase(Locale.ENGLISH);
        d dVar = this.d;
        Stack stack = new Stack();
        stack.push(dVar);
        boolean z = false;
        int i = 0;
        for (int i2 = 0; i2 < lowerCase.length(); i2++) {
            char charAt = lowerCase.charAt(i2);
            if (charAt == '.') {
                if (i2 == i) {
                    dVar.add(b.d);
                } else {
                    dVar.add(b(z, lowerCase.substring(i, i2)));
                }
                i = i2 + 1;
            } else if (charAt == '-') {
                if (i2 == i) {
                    dVar.add(b.d);
                } else {
                    dVar.add(b(z, lowerCase.substring(i, i2)));
                }
                i = i2 + 1;
                d dVar2 = new d();
                dVar.add(dVar2);
                stack.push(dVar2);
                dVar = dVar2;
            } else if (Character.isDigit(charAt)) {
                if (!z && i2 > i) {
                    dVar.add(new e(lowerCase.substring(i, i2), true));
                    d dVar3 = new d();
                    dVar.add(dVar3);
                    stack.push(dVar3);
                    i = i2;
                    dVar = dVar3;
                }
                z = true;
            } else {
                if (z && i2 > i) {
                    dVar.add(b(true, lowerCase.substring(i, i2)));
                    d dVar4 = new d();
                    dVar.add(dVar4);
                    stack.push(dVar4);
                    i = i2;
                    dVar = dVar4;
                }
                z = false;
            }
        }
        if (lowerCase.length() > i) {
            dVar.add(b(z, lowerCase.substring(i)));
        }
        while (!stack.isEmpty()) {
            ((d) stack.pop()).normalize();
        }
        this.c = this.d.toString();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof y68) && this.c.equals(((y68) obj).c);
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.b;
    }
}
