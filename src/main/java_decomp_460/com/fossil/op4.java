package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class op4 implements Factory<vn5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f2706a;

    @DexIgnore
    public op4(uo4 uo4) {
        this.f2706a = uo4;
    }

    @DexIgnore
    public static op4 a(uo4 uo4) {
        return new op4(uo4);
    }

    @DexIgnore
    public static vn5 c(uo4 uo4) {
        vn5 v = uo4.v();
        lk7.c(v, "Cannot return null from a non-@Nullable @Provides method");
        return v;
    }

    @DexIgnore
    /* renamed from: b */
    public vn5 get() {
        return c(this.f2706a);
    }
}
