package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w36 implements Factory<v36> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ w36 f3876a; // = new w36();
    }

    @DexIgnore
    public static w36 a() {
        return a.f3876a;
    }

    @DexIgnore
    public static v36 c() {
        return new v36();
    }

    @DexIgnore
    /* renamed from: b */
    public v36 get() {
        return c();
    }
}
