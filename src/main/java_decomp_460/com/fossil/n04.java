package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.tabs.TabLayout;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n04 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ TabLayout f2445a;
    @DexIgnore
    public /* final */ ViewPager2 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public RecyclerView.g<?> e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public c g;
    @DexIgnore
    public TabLayout.d h;
    @DexIgnore
    public RecyclerView.AdapterDataObserver i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.AdapterDataObserver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            n04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void b(int i, int i2) {
            n04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void c(int i, int i2, Object obj) {
            n04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void d(int i, int i2) {
            n04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void e(int i, int i2, int i3) {
            n04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void f(int i, int i2) {
            n04.this.b();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(TabLayout.g gVar, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeakReference<TabLayout> f2447a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public c(TabLayout tabLayout) {
            this.f2447a = new WeakReference<>(tabLayout);
            d();
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i) {
            this.b = this.c;
            this.c = i;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            boolean z = false;
            TabLayout tabLayout = this.f2447a.get();
            if (tabLayout != null) {
                boolean z2 = this.c != 2 || this.b == 1;
                if (!(this.c == 2 && this.b == 0)) {
                    z = true;
                }
                tabLayout.G(i, f, z2, z);
            }
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            TabLayout tabLayout = this.f2447a.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i && i < tabLayout.getTabCount()) {
                int i2 = this.c;
                tabLayout.D(tabLayout.v(i), i2 == 0 || (i2 == 2 && this.b == 0));
            }
        }

        @DexIgnore
        public void d() {
            this.c = 0;
            this.b = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements TabLayout.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ViewPager2 f2448a;

        @DexIgnore
        public d(ViewPager2 viewPager2) {
            this.f2448a = viewPager2;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            this.f2448a.j(gVar.f(), true);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
        }
    }

    @DexIgnore
    public n04(TabLayout tabLayout, ViewPager2 viewPager2, b bVar) {
        this(tabLayout, viewPager2, true, bVar);
    }

    @DexIgnore
    public n04(TabLayout tabLayout, ViewPager2 viewPager2, boolean z, b bVar) {
        this.f2445a = tabLayout;
        this.b = viewPager2;
        this.c = z;
        this.d = bVar;
    }

    @DexIgnore
    public void a() {
        if (!this.f) {
            RecyclerView.g<?> adapter = this.b.getAdapter();
            this.e = adapter;
            if (adapter != null) {
                this.f = true;
                c cVar = new c(this.f2445a);
                this.g = cVar;
                this.b.g(cVar);
                d dVar = new d(this.b);
                this.h = dVar;
                this.f2445a.c(dVar);
                if (this.c) {
                    a aVar = new a();
                    this.i = aVar;
                    this.e.registerAdapterDataObserver(aVar);
                }
                b();
                this.f2445a.F(this.b.getCurrentItem(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, true);
                return;
            }
            throw new IllegalStateException("TabLayoutMediator attached before ViewPager2 has an adapter");
        }
        throw new IllegalStateException("TabLayoutMediator is already attached");
    }

    @DexIgnore
    public void b() {
        this.f2445a.z();
        RecyclerView.g<?> gVar = this.e;
        if (gVar != null) {
            int itemCount = gVar.getItemCount();
            for (int i2 = 0; i2 < itemCount; i2++) {
                TabLayout.g w = this.f2445a.w();
                this.d.a(w, i2);
                this.f2445a.f(w, false);
            }
            if (itemCount > 0) {
                int min = Math.min(this.b.getCurrentItem(), this.f2445a.getTabCount() - 1);
                if (min != this.f2445a.getSelectedTabPosition()) {
                    TabLayout tabLayout = this.f2445a;
                    tabLayout.C(tabLayout.v(min));
                }
            }
        }
    }
}
