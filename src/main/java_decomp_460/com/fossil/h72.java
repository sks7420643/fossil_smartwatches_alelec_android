package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h72 implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    @DexIgnore
    public static /* final */ h72 f; // = new h72();
    @DexIgnore
    public /* final */ AtomicBoolean b; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ ArrayList<a> d; // = new ArrayList<>();
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public static h72 b() {
        return f;
    }

    @DexIgnore
    public static void c(Application application) {
        synchronized (f) {
            if (!f.e) {
                application.registerActivityLifecycleCallbacks(f);
                application.registerComponentCallbacks(f);
                f.e = true;
            }
        }
    }

    @DexIgnore
    public final void a(a aVar) {
        synchronized (f) {
            this.d.add(aVar);
        }
    }

    @DexIgnore
    public final boolean d() {
        return this.b.get();
    }

    @DexIgnore
    public final void e(boolean z) {
        synchronized (f) {
            ArrayList<a> arrayList = this.d;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                a aVar = arrayList.get(i);
                i++;
                aVar.a(z);
            }
        }
    }

    @DexIgnore
    @TargetApi(16)
    public final boolean f(boolean z) {
        if (!this.c.get()) {
            if (!mf2.c()) {
                return z;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.c.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.b.set(true);
            }
        }
        return d();
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.b.compareAndSet(true, false);
        this.c.set(true);
        if (compareAndSet) {
            e(false);
        }
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.b.compareAndSet(true, false);
        this.c.set(true);
        if (compareAndSet) {
            e(false);
        }
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }

    @DexIgnore
    public final void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public final void onLowMemory() {
    }

    @DexIgnore
    public final void onTrimMemory(int i) {
        if (i == 20 && this.b.compareAndSet(false, true)) {
            this.c.set(true);
            e(true);
        }
    }
}
