package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mn7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7[] b;

        @DexIgnore
        public a(rp7[] rp7Arr) {
            this.b = rp7Arr;
        }

        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.d(t, t2, this.b);
        }
    }

    @DexIgnore
    public static final <T> Comparator<T> b(rp7<? super T, ? extends Comparable<?>>... rp7Arr) {
        pq7.c(rp7Arr, "selectors");
        if (rp7Arr.length > 0) {
            return new a(rp7Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    public static final <T extends Comparable<?>> int c(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    @DexIgnore
    public static final <T> int d(T t, T t2, rp7<? super T, ? extends Comparable<?>>[] rp7Arr) {
        for (rp7<? super T, ? extends Comparable<?>> rp7 : rp7Arr) {
            int c = c((Comparable) rp7.invoke(t), (Comparable) rp7.invoke(t2));
            if (c != 0) {
                return c;
            }
        }
        return 0;
    }
}
