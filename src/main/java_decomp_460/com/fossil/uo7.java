package com.fossil;

import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo7 extends ByteArrayOutputStream {
    @DexIgnore
    public uo7(int i) {
        super(i);
    }

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = ((ByteArrayOutputStream) this).buf;
        pq7.b(bArr, "buf");
        return bArr;
    }
}
