package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f56 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ d56 f1053a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<String> c;

    @DexIgnore
    public f56(d56 d56, int i, ArrayList<String> arrayList) {
        pq7.c(d56, "mView");
        this.f1053a = d56;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        ArrayList<String> arrayList = this.c;
        return arrayList == null ? new ArrayList<>() : arrayList;
    }

    @DexIgnore
    public final d56 c() {
        return this.f1053a;
    }
}
