package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sb7 implements qb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ib7 f3237a;
    @DexIgnore
    public Bitmap b;
    @DexIgnore
    public String c;

    @DexIgnore
    public sb7(ib7 ib7, Bitmap bitmap, String str) {
        pq7.c(ib7, "complicationData");
        this.f3237a = ib7;
        this.b = bitmap;
        this.c = str;
    }

    @DexIgnore
    public final ib7 a() {
        return this.f3237a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof sb7) {
                sb7 sb7 = (sb7) obj;
                if (!pq7.a(this.f3237a, sb7.f3237a) || !pq7.a(this.b, sb7.b) || !pq7.a(this.c, sb7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        ib7 ib7 = this.f3237a;
        int hashCode = ib7 != null ? ib7.hashCode() : 0;
        Bitmap bitmap = this.b;
        int hashCode2 = bitmap != null ? bitmap.hashCode() : 0;
        String str = this.c;
        if (str != null) {
            i = str.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UIComplication(complicationData=" + this.f3237a + ", ringBitmap=" + this.b + ", ringPreviewUrl=" + this.c + ")";
    }
}
