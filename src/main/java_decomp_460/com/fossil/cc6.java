package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cc6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ zb6 f598a;

    @DexIgnore
    public cc6(zb6 zb6, String str, String str2) {
        pq7.c(zb6, "mView");
        pq7.c(str, "mPresetId");
        pq7.c(str2, "mMicroAppPos");
        this.f598a = zb6;
    }

    @DexIgnore
    public final zb6 a() {
        return this.f598a;
    }
}
