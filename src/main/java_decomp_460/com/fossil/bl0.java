package com.fossil;

import android.app.Notification;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import androidx.core.graphics.drawable.IconCompat;
import com.facebook.applinks.AppLinkData;
import com.fossil.zk0;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bl0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Object f443a; // = new Object();
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;

    @DexIgnore
    public static SparseArray<Bundle> a(List<Bundle> list) {
        int size = list.size();
        SparseArray<Bundle> sparseArray = null;
        for (int i = 0; i < size; i++) {
            Bundle bundle = list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    @DexIgnore
    public static Bundle b(zk0.a aVar) {
        Bundle bundle = new Bundle();
        IconCompat e = aVar.e();
        bundle.putInt("icon", e != null ? e.c() : 0);
        bundle.putCharSequence("title", aVar.i());
        bundle.putParcelable("actionIntent", aVar.a());
        Bundle bundle2 = aVar.d() != null ? new Bundle(aVar.d()) : new Bundle();
        bundle2.putBoolean("android.support.allowGeneratedReplies", aVar.b());
        bundle.putBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY, bundle2);
        bundle.putParcelableArray("remoteInputs", e(aVar.f()));
        bundle.putBoolean("showsUserInterface", aVar.h());
        bundle.putInt("semanticAction", aVar.g());
        return bundle;
    }

    @DexIgnore
    public static Bundle c(Notification notification) {
        synchronized (f443a) {
            if (c) {
                return null;
            }
            try {
                if (b == null) {
                    Field declaredField = Notification.class.getDeclaredField(AppLinkData.ARGUMENTS_EXTRAS_KEY);
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        c = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    b = declaredField;
                }
                Bundle bundle = (Bundle) b.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    b.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e) {
                Log.e("NotificationCompat", "Unable to access notification extras", e);
                c = true;
                return null;
            } catch (NoSuchFieldException e2) {
                Log.e("NotificationCompat", "Unable to access notification extras", e2);
                c = true;
                return null;
            }
        }
    }

    @DexIgnore
    public static Bundle d(dl0 dl0) {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", dl0.i());
        bundle.putCharSequence("label", dl0.h());
        bundle.putCharSequenceArray("choices", dl0.e());
        bundle.putBoolean("allowFreeFormInput", dl0.c());
        bundle.putBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY, dl0.g());
        Set<String> d = dl0.d();
        if (d != null && !d.isEmpty()) {
            ArrayList<String> arrayList = new ArrayList<>(d.size());
            for (String str : d) {
                arrayList.add(str);
            }
            bundle.putStringArrayList("allowedDataTypes", arrayList);
        }
        return bundle;
    }

    @DexIgnore
    public static Bundle[] e(dl0[] dl0Arr) {
        if (dl0Arr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[dl0Arr.length];
        for (int i = 0; i < dl0Arr.length; i++) {
            bundleArr[i] = d(dl0Arr[i]);
        }
        return bundleArr;
    }

    @DexIgnore
    public static Bundle f(Notification.Builder builder, zk0.a aVar) {
        IconCompat e = aVar.e();
        builder.addAction(e != null ? e.c() : 0, aVar.i(), aVar.a());
        Bundle bundle = new Bundle(aVar.d());
        if (aVar.f() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", e(aVar.f()));
        }
        if (aVar.c() != null) {
            bundle.putParcelableArray("android.support.dataRemoteInputs", e(aVar.c()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.b());
        return bundle;
    }
}
