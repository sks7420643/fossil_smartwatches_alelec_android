package com.fossil;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.VideoUploader;
import com.fossil.nk5;
import com.fossil.sm5;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a17 extends os5 implements l17, t47.g, sm5.b, t47.h {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public k17 j;
    @DexIgnore
    public g37<t25> k;
    @DexIgnore
    public wj5 l;
    @DexIgnore
    public List<nk5.b> m; // = new ArrayList();
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final a17 a() {
            return new a17();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a17 b;

        @DexIgnore
        public b(a17 a17) {
            this.b = a17;
        }

        @DexIgnore
        public final void onClick(View view) {
            a17.c7(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a17 b;

        @DexIgnore
        public c(a17 a17) {
            this.b = a17;
        }

        @DexIgnore
        public final void onClick(View view) {
            a17.c7(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ t25 f181a;
        @DexIgnore
        public /* final */ /* synthetic */ a17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ej1<Drawable> {
            @DexIgnore
            /* renamed from: a */
            public boolean g(Drawable drawable, Object obj, qj1<Drawable> qj1, gb1 gb1, boolean z) {
                pq7.c(drawable, "resource");
                pq7.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                pq7.c(qj1, "target");
                pq7.c(gb1, "dataSource");
                FLogger.INSTANCE.getLocal().d(a17.t, "showHour onResourceReady");
                return false;
            }

            @DexIgnore
            @Override // com.fossil.ej1
            public boolean e(dd1 dd1, Object obj, qj1<Drawable> qj1, boolean z) {
                pq7.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                pq7.c(qj1, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a17.t;
                local.d(str, "showHour onLoadFail e=" + dd1);
                return true;
            }
        }

        @DexIgnore
        public d(t25 t25, a17 a17, String str) {
            this.f181a = t25;
            this.b = a17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            wj5 wj5;
            vj5<Drawable> J;
            vj5<Drawable> b1;
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a17.t;
            local.d(str3, "showHour onImageCallback serial=" + str + " filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.l);
            if (this.b.isActive() && (wj5 = this.b.l) != null && (J = wj5.t(str2)) != null && (b1 = J.b1(new a())) != null) {
                b1.F0(this.f181a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ t25 f182a;
        @DexIgnore
        public /* final */ /* synthetic */ a17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ej1<Drawable> {
            @DexIgnore
            /* renamed from: a */
            public boolean g(Drawable drawable, Object obj, qj1<Drawable> qj1, gb1 gb1, boolean z) {
                pq7.c(drawable, "resource");
                pq7.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                pq7.c(qj1, "target");
                pq7.c(gb1, "dataSource");
                FLogger.INSTANCE.getLocal().d(a17.t, "showMinute onResourceReady");
                return false;
            }

            @DexIgnore
            @Override // com.fossil.ej1
            public boolean e(dd1 dd1, Object obj, qj1<Drawable> qj1, boolean z) {
                pq7.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                pq7.c(qj1, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a17.t;
                local.d(str, "showMinute onLoadFail e=" + dd1);
                return true;
            }
        }

        @DexIgnore
        public e(t25 t25, a17 a17, String str) {
            this.f182a = t25;
            this.b = a17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            wj5 wj5;
            vj5<Drawable> J;
            vj5<Drawable> b1;
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a17.t;
            local.d(str3, "showMinute onImageCallback serial=" + str + " filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.l);
            if (this.b.isActive() && (wj5 = this.b.l) != null && (J = wj5.t(str2)) != null && (b1 = J.b1(new a())) != null) {
                b1.F0(this.f182a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ t25 f183a;
        @DexIgnore
        public /* final */ /* synthetic */ a17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ej1<Drawable> {
            @DexIgnore
            /* renamed from: a */
            public boolean g(Drawable drawable, Object obj, qj1<Drawable> qj1, gb1 gb1, boolean z) {
                pq7.c(drawable, "resource");
                pq7.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                pq7.c(qj1, "target");
                pq7.c(gb1, "dataSource");
                FLogger.INSTANCE.getLocal().d(a17.t, "showSubeye onResourceReady");
                return false;
            }

            @DexIgnore
            @Override // com.fossil.ej1
            public boolean e(dd1 dd1, Object obj, qj1<Drawable> qj1, boolean z) {
                pq7.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                pq7.c(qj1, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a17.t;
                local.d(str, "showSubeye onLoadFail e=" + dd1);
                return true;
            }
        }

        @DexIgnore
        public f(t25 t25, a17 a17, String str) {
            this.f183a = t25;
            this.b = a17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            wj5 wj5;
            vj5<Drawable> J;
            vj5<Drawable> b1;
            pq7.c(str, "serial");
            pq7.c(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a17.t;
            local.d(str3, "showSubeye onImageCallback serial=" + str + " filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.l);
            if (this.b.isActive() && (wj5 = this.b.l) != null && (J = wj5.t(str2)) != null && (b1 = J.b1(new a())) != null) {
                b1.F0(this.f183a.r);
            }
        }
    }

    /*
    static {
        String simpleName = a17.class.getSimpleName();
        pq7.b(simpleName, "CalibrationFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ k17 c7(a17 a17) {
        k17 k17 = a17.j;
        if (k17 != null) {
            return k17;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void A0(View view) {
        pq7.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onLongPressEvent");
        k17 k17 = this.j;
        if (k17 != null) {
            k17.s(view.getId() == 2131361855);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void F1(String str) {
        pq7.c(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "showHour: deviceId = " + str);
        if (isActive()) {
            g37<t25> g37 = this.k;
            if (g37 != null) {
                t25 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    pq7.b(flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131887143));
                    FlexibleTextView flexibleTextView2 = a2.x;
                    pq7.b(flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887142));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(nk5.o.m(str)).setType(Constants.CalibrationType.TYPE_HOUR);
                    AppCompatImageView appCompatImageView = a2.r;
                    pq7.b(appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, nk5.o.i(str, this.m.get(0))).setImageCallback(new d(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        k17 k17 = this.j;
        if (k17 != null) {
            k17.n();
            return true;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void I4(String str) {
        pq7.c(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local.d(str2, "showMinute: deviceId = " + str);
            g37<t25> g37 = this.k;
            if (g37 != null) {
                t25 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    pq7.b(flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131887146));
                    FlexibleTextView flexibleTextView2 = a2.x;
                    pq7.b(flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887145));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(nk5.o.m(str)).setType(Constants.CalibrationType.TYPE_MINUTE);
                    AppCompatImageView appCompatImageView = a2.r;
                    pq7.b(appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, nk5.o.i(str, this.m.get(1))).setImageCallback(new e(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void L5(int i, int i2) {
        if (isActive()) {
            g37<t25> g37 = this.k;
            if (g37 != null) {
                t25 a2 = g37.a();
                if (a2 != null) {
                    a2.A.setLength(i2);
                    int i3 = i + 1;
                    a2.A.setProgress((int) ((((float) i3) / ((float) i2)) * ((float) 100)));
                    String c2 = i3 < i2 ? um5.c(PortfolioApp.h0.c(), 2131887141) : um5.c(PortfolioApp.h0.c(), 2131887144);
                    FlexibleButton flexibleButton = a2.y;
                    pq7.b(flexibleButton, "it.ftvNext");
                    flexibleButton.setText(c2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void N5(View view, Boolean bool) {
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.os5
    public void R5(String str, int i, Intent intent) {
        pq7.c(str, "tag");
        super.R5(str, i, intent);
        int hashCode = str.hashCode();
        if (hashCode == -1391436191 ? !str.equals("SYNC_FAILED") : hashCode != 1178575340 || !str.equals("SERVER_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((ls5) activity).R5(str, i, intent);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
        e0();
    }

    @DexIgnore
    @Override // com.fossil.os5
    public void S6() {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.os5
    public void T6() {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.os5
    public void U6() {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void V0(View view) {
        pq7.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onLongPressEnded");
        k17 k17 = this.j;
        if (k17 != null) {
            k17.t();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.os5
    public void V6() {
    }

    @DexIgnore
    @Override // com.fossil.os5
    public void W6() {
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void e0() {
        FLogger.INSTANCE.getLocal().d(t, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    /* renamed from: e7 */
    public void M5(k17 k17) {
        pq7.c(k17, "presenter");
        this.j = k17;
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void h5(View view) {
        pq7.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onRelease");
        view.setPressed(false);
    }

    @DexIgnore
    @Override // com.fossil.t47.h
    public void i1(String str) {
        if (!TextUtils.isEmpty(str) && str != null && str.hashCode() == 1925385819 && str.equals("DEVICE_CONNECT_FAILED")) {
            k17 k17 = this.j;
            if (k17 != null) {
                k17.q(false);
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void i4() {
        FLogger.INSTANCE.getLocal().d(t, "showGeneralError");
        k17 k17 = this.j;
        if (k17 != null) {
            k17.q(true);
            if (isActive()) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.V(childFragmentManager);
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void onClick(View view) {
        pq7.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onClick");
        k17 k17 = this.j;
        if (k17 != null) {
            k17.r(view.getId() == 2131361855);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        t25 t25 = (t25) aq0.f(layoutInflater, 2131558506, viewGroup, false, A6());
        t25.q.setOnClickListener(new b(this));
        t25.y.setOnClickListener(new c(this));
        new sm5().a(t25.s, this);
        new sm5().a(t25.t, this);
        this.k = new g37<>(this, t25);
        this.l = tj5.c(this);
        k17 k17 = this.j;
        if (k17 != null) {
            this.m = k17.o();
            pq7.b(t25, "binding");
            return t25.n();
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.os5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        k17 k17 = this.j;
        if (k17 != null) {
            k17.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        k17 k17 = this.j;
        if (k17 != null) {
            k17.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void r1() {
        FLogger.INSTANCE.getLocal().d(t, "showBluetoothError");
        if (isActive()) {
            k17 k17 = this.j;
            if (k17 != null) {
                k17.q(true);
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.k(childFragmentManager);
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sm5.b
    public void v4(View view) {
        pq7.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onPress");
        view.setPressed(true);
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.os5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.l17
    public void x1(String str) {
        pq7.c(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local.d(str2, "showSubeye: deviceId = " + str);
            if (this.m.size() > 2) {
                g37<t25> g37 = this.k;
                if (g37 != null) {
                    t25 a2 = g37.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.z;
                        pq7.b(flexibleTextView, "binding.ftvTitle");
                        flexibleTextView.setText(um5.c(PortfolioApp.h0.c(), 2131887148));
                        FlexibleTextView flexibleTextView2 = a2.x;
                        pq7.b(flexibleTextView2, "binding.ftvDescription");
                        flexibleTextView2.setText(um5.c(PortfolioApp.h0.c(), 2131887147));
                        CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(nk5.o.m(str)).setType(Constants.CalibrationType.TYPE_SUB_EYE);
                        AppCompatImageView appCompatImageView = a2.r;
                        pq7.b(appCompatImageView, "binding.acivDevice");
                        type.setPlaceHolder(appCompatImageView, nk5.o.i(str, this.m.get(2))).setImageCallback(new f(a2, this, str)).downloadForCalibration();
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
        }
    }
}
