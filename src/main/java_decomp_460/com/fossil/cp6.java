package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp6 implements Factory<bp6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<xu5> f637a;
    @DexIgnore
    public /* final */ Provider<vu5> b;

    @DexIgnore
    public cp6(Provider<xu5> provider, Provider<vu5> provider2) {
        this.f637a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static cp6 a(Provider<xu5> provider, Provider<vu5> provider2) {
        return new cp6(provider, provider2);
    }

    @DexIgnore
    public static bp6 c(xu5 xu5, vu5 vu5) {
        return new bp6(xu5, vu5);
    }

    @DexIgnore
    /* renamed from: b */
    public bp6 get() {
        return c(this.f637a.get(), this.b.get());
    }
}
