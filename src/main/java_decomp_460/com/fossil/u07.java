package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u07 extends pv5 implements t07 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ String m; // = m;
    @DexIgnore
    public static /* final */ String s; // = s;
    @DexIgnore
    public static /* final */ String t; // = t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public tb5 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return u07.m;
        }

        @DexIgnore
        public final String b() {
            return u07.t;
        }

        @DexIgnore
        public final String c() {
            return u07.s;
        }

        @DexIgnore
        public final String d() {
            return u07.l;
        }

        @DexIgnore
        public final u07 e(boolean z, boolean z2, boolean z3) {
            u07 u07 = new u07();
            Bundle bundle = new Bundle();
            bundle.putBoolean(a(), z);
            bundle.putBoolean(c(), z2);
            bundle.putBoolean(b(), z3);
            u07.setArguments(bundle);
            return u07;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u07 b;

        @DexIgnore
        public b(u07 u07) {
            this.b = u07;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.i) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Context context = this.b.getContext();
                if (context != null) {
                    pq7.b(context, "context!!");
                    aVar.a(context, this.b.j, true);
                    return;
                }
                pq7.i();
                throw null;
            }
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u07 b;

        @DexIgnore
        public c(u07 u07) {
            this.b = u07;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u07 b;

        @DexIgnore
        public d(u07 u07) {
            this.b = u07;
        }

        @DexIgnore
        public final void onClick(View view) {
            wr4.f3989a.a().k(this.b);
        }
    }

    /*
    static {
        String simpleName = u07.class.getSimpleName();
        pq7.b(simpleName, "TroubleshootingFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return l;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: Q6 */
    public void M5(s07 s07) {
        pq7.c(s07, "presenter");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        tb5 c2 = tb5.c(getLayoutInflater());
        pq7.b(c2, "FragmentTroubleshootingB\u2026g.inflate(layoutInflater)");
        this.g = c2;
        if (c2 != null) {
            return c2.b();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        boolean z = false;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.h = arguments != null ? arguments.getBoolean(m) : false;
        Bundle arguments2 = getArguments();
        this.i = arguments2 != null ? arguments2.getBoolean(s) : false;
        Bundle arguments3 = getArguments();
        if (arguments3 != null) {
            z = arguments3.getBoolean(t);
        }
        this.j = z;
        tb5 tb5 = this.g;
        if (tb5 != null) {
            LinearLayout linearLayout = tb5.e.b;
            pq7.b(linearLayout, "mBinding.includeLayoutTr\u2026eshoot.llBatteryReinstall");
            tb5 tb52 = this.g;
            if (tb52 != null) {
                LinearLayout linearLayout2 = tb52.e.c;
                pq7.b(linearLayout2, "mBinding.includeLayoutTr\u2026llTouchscreenSmartwatches");
                if (!wr4.f3989a.a().b()) {
                    linearLayout2.setVisibility(8);
                }
                if (this.h) {
                    linearLayout.setVisibility(8);
                }
                tb5 tb53 = this.g;
                if (tb53 != null) {
                    tb53.c.setOnClickListener(new b(this));
                    tb5 tb54 = this.g;
                    if (tb54 != null) {
                        tb54.f.setOnClickListener(new c(this));
                        tb5 tb55 = this.g;
                        if (tb55 != null) {
                            tb55.b.setOnClickListener(new d(this));
                        } else {
                            pq7.n("mBinding");
                            throw null;
                        }
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
