package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr3 extends SSLSocket {
    @DexIgnore
    public /* final */ SSLSocket b;

    @DexIgnore
    public lr3(jr3 jr3, SSLSocket sSLSocket) {
        this.b = sSLSocket;
    }

    @DexIgnore
    public final void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.b.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void bind(SocketAddress socketAddress) throws IOException {
        this.b.bind(socketAddress);
    }

    @DexIgnore
    @Override // java.net.Socket, java.io.Closeable, java.lang.AutoCloseable
    public final void close() throws IOException {
        synchronized (this) {
            this.b.close();
        }
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void connect(SocketAddress socketAddress) throws IOException {
        this.b.connect(socketAddress);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void connect(SocketAddress socketAddress, int i) throws IOException {
        this.b.connect(socketAddress, i);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return this.b.equals(obj);
    }

    @DexIgnore
    public final SocketChannel getChannel() {
        return this.b.getChannel();
    }

    @DexIgnore
    public final boolean getEnableSessionCreation() {
        return this.b.getEnableSessionCreation();
    }

    @DexIgnore
    public final String[] getEnabledCipherSuites() {
        return this.b.getEnabledCipherSuites();
    }

    @DexIgnore
    public final String[] getEnabledProtocols() {
        return this.b.getEnabledProtocols();
    }

    @DexIgnore
    public final InetAddress getInetAddress() {
        return this.b.getInetAddress();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final InputStream getInputStream() throws IOException {
        return this.b.getInputStream();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final boolean getKeepAlive() throws SocketException {
        return this.b.getKeepAlive();
    }

    @DexIgnore
    public final InetAddress getLocalAddress() {
        return this.b.getLocalAddress();
    }

    @DexIgnore
    public final int getLocalPort() {
        return this.b.getLocalPort();
    }

    @DexIgnore
    public final SocketAddress getLocalSocketAddress() {
        return this.b.getLocalSocketAddress();
    }

    @DexIgnore
    public final boolean getNeedClientAuth() {
        return this.b.getNeedClientAuth();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final boolean getOOBInline() throws SocketException {
        return this.b.getOOBInline();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final OutputStream getOutputStream() throws IOException {
        return this.b.getOutputStream();
    }

    @DexIgnore
    public final int getPort() {
        return this.b.getPort();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final int getReceiveBufferSize() throws SocketException {
        int receiveBufferSize;
        synchronized (this) {
            receiveBufferSize = this.b.getReceiveBufferSize();
        }
        return receiveBufferSize;
    }

    @DexIgnore
    public final SocketAddress getRemoteSocketAddress() {
        return this.b.getRemoteSocketAddress();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final boolean getReuseAddress() throws SocketException {
        return this.b.getReuseAddress();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final int getSendBufferSize() throws SocketException {
        int sendBufferSize;
        synchronized (this) {
            sendBufferSize = this.b.getSendBufferSize();
        }
        return sendBufferSize;
    }

    @DexIgnore
    public final SSLSession getSession() {
        return this.b.getSession();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final int getSoLinger() throws SocketException {
        return this.b.getSoLinger();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final int getSoTimeout() throws SocketException {
        int soTimeout;
        synchronized (this) {
            soTimeout = this.b.getSoTimeout();
        }
        return soTimeout;
    }

    @DexIgnore
    public final String[] getSupportedCipherSuites() {
        return this.b.getSupportedCipherSuites();
    }

    @DexIgnore
    public final String[] getSupportedProtocols() {
        return this.b.getSupportedProtocols();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final boolean getTcpNoDelay() throws SocketException {
        return this.b.getTcpNoDelay();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final int getTrafficClass() throws SocketException {
        return this.b.getTrafficClass();
    }

    @DexIgnore
    public final boolean getUseClientMode() {
        return this.b.getUseClientMode();
    }

    @DexIgnore
    public final boolean getWantClientAuth() {
        return this.b.getWantClientAuth();
    }

    @DexIgnore
    public final boolean isBound() {
        return this.b.isBound();
    }

    @DexIgnore
    public final boolean isClosed() {
        return this.b.isClosed();
    }

    @DexIgnore
    public final boolean isConnected() {
        return this.b.isConnected();
    }

    @DexIgnore
    public final boolean isInputShutdown() {
        return this.b.isInputShutdown();
    }

    @DexIgnore
    public final boolean isOutputShutdown() {
        return this.b.isOutputShutdown();
    }

    @DexIgnore
    public final void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.b.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void sendUrgentData(int i) throws IOException {
        this.b.sendUrgentData(i);
    }

    @DexIgnore
    public final void setEnableSessionCreation(boolean z) {
        this.b.setEnableSessionCreation(z);
    }

    @DexIgnore
    public final void setEnabledCipherSuites(String[] strArr) {
        this.b.setEnabledCipherSuites(strArr);
    }

    @DexIgnore
    public final void setEnabledProtocols(String[] strArr) {
        if (strArr != null && Arrays.asList(strArr).contains("SSLv3")) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.b.getEnabledProtocols()));
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv3");
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        this.b.setEnabledProtocols(strArr);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setKeepAlive(boolean z) throws SocketException {
        this.b.setKeepAlive(z);
    }

    @DexIgnore
    public final void setNeedClientAuth(boolean z) {
        this.b.setNeedClientAuth(z);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setOOBInline(boolean z) throws SocketException {
        this.b.setOOBInline(z);
    }

    @DexIgnore
    public final void setPerformancePreferences(int i, int i2, int i3) {
        this.b.setPerformancePreferences(i, i2, i3);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setReceiveBufferSize(int i) throws SocketException {
        synchronized (this) {
            this.b.setReceiveBufferSize(i);
        }
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setReuseAddress(boolean z) throws SocketException {
        this.b.setReuseAddress(z);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setSendBufferSize(int i) throws SocketException {
        synchronized (this) {
            this.b.setSendBufferSize(i);
        }
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setSoLinger(boolean z, int i) throws SocketException {
        this.b.setSoLinger(z, i);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setSoTimeout(int i) throws SocketException {
        synchronized (this) {
            this.b.setSoTimeout(i);
        }
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setTcpNoDelay(boolean z) throws SocketException {
        this.b.setTcpNoDelay(z);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void setTrafficClass(int i) throws SocketException {
        this.b.setTrafficClass(i);
    }

    @DexIgnore
    public final void setUseClientMode(boolean z) {
        this.b.setUseClientMode(z);
    }

    @DexIgnore
    public final void setWantClientAuth(boolean z) {
        this.b.setWantClientAuth(z);
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void shutdownInput() throws IOException {
        this.b.shutdownInput();
    }

    @DexIgnore
    @Override // java.net.Socket
    public final void shutdownOutput() throws IOException {
        this.b.shutdownOutput();
    }

    @DexIgnore
    @Override // javax.net.ssl.SSLSocket
    public final void startHandshake() throws IOException {
        this.b.startHandshake();
    }

    @DexIgnore
    public final String toString() {
        return this.b.toString();
    }
}
