package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ig1 implements sb1<Drawable> {
    @DexIgnore
    public /* final */ sb1<Bitmap> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public ig1(sb1<Bitmap> sb1, boolean z) {
        this.b = sb1;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.sb1
    public id1<Drawable> b(Context context, id1<Drawable> id1, int i, int i2) {
        rd1 f = oa1.c(context).f();
        Drawable drawable = id1.get();
        id1<Bitmap> a2 = hg1.a(f, drawable, i, i2);
        if (a2 != null) {
            id1<Bitmap> b2 = this.b.b(context, a2, i, i2);
            if (!b2.equals(a2)) {
                return d(context, b2);
            }
            b2.b();
            return id1;
        } else if (!this.c) {
            return id1;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    public sb1<BitmapDrawable> c() {
        return this;
    }

    @DexIgnore
    public final id1<Drawable> d(Context context, id1<Bitmap> id1) {
        return og1.f(context.getResources(), id1);
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        if (obj instanceof ig1) {
            return this.b.equals(((ig1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        return this.b.hashCode();
    }
}
