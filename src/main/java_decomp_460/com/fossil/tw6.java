package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.ct5;
import com.fossil.iq4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wq5;
import com.fossil.yt5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tw6 extends pw6 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Device g;
    @DexIgnore
    public ul5 h;
    @DexIgnore
    public /* final */ e i; // = new e(this);
    @DexIgnore
    public /* final */ d j; // = new d(this);
    @DexIgnore
    public /* final */ qw6 k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ mj5 m;
    @DexIgnore
    public /* final */ on5 n;
    @DexIgnore
    public /* final */ yt5 o;
    @DexIgnore
    public /* final */ ct5 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return tw6.q;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<ct5.c, ct5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw6 f3483a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ Device c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1", f = "UpdateFirmwarePresenter.kt", l = {214, 217}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ct5.c $responseValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tw6$b$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.tw6$b$a$a  reason: collision with other inner class name */
            public static final class C0246a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0246a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0246a aVar = new C0246a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                    return ((C0246a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return ao7.a(p37.h.a().i(this.this$0.this$0.b));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tw6$b$a$b")
            @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1", f = "UpdateFirmwarePresenter.kt", l = {217}, m = "invokeSuspend")
            /* renamed from: com.fossil.tw6$b$a$b  reason: collision with other inner class name */
            public static final class C0247b extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0247b(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0247b bVar = new C0247b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                    return ((C0247b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        p37 a2 = p37.h.a();
                        b bVar = this.this$0.this$0;
                        String str = bVar.b;
                        Device device = bVar.c;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object j = a2.j(str, device, this);
                        return j == d ? d : j;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, ct5.c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$responseValue = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$responseValue, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00be  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                    r10 = this;
                    r9 = 0
                    r8 = 2
                    r7 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r10.label
                    if (r0 == 0) goto L_0x006c
                    if (r0 == r7) goto L_0x0039
                    if (r0 != r8) goto L_0x0031
                    java.lang.Object r0 = r10.L$1
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r10.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r11)
                    r0 = r11
                L_0x001b:
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 == 0) goto L_0x00b3
                    com.fossil.tw6$b r0 = r10.this$0
                    com.fossil.tw6 r0 = r0.f3483a
                    com.fossil.qw6 r0 = r0.L()
                    r0.t3()
                L_0x002e:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0030:
                    return r0
                L_0x0031:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0039:
                    java.lang.Object r0 = r10.L$1
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r1 = r10.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r11)
                    r3 = r0
                    r2 = r11
                L_0x0046:
                    r0 = r2
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x00be
                    com.fossil.tw6$b r2 = r10.this$0
                    com.fossil.tw6 r2 = r2.f3483a
                    com.fossil.dv7 r2 = com.fossil.tw6.w(r2)
                    com.fossil.tw6$b$a$b r5 = new com.fossil.tw6$b$a$b
                    r5.<init>(r10, r9)
                    r10.L$0 = r1
                    r10.L$1 = r3
                    r10.Z$0 = r0
                    r10.label = r8
                    java.lang.Object r0 = com.fossil.eu7.g(r2, r5, r10)
                    if (r0 != r4) goto L_0x001b
                    r0 = r4
                    goto L_0x0030
                L_0x006c:
                    com.fossil.el7.b(r11)
                    com.fossil.iv7 r1 = r10.p$
                    com.fossil.ct5$c r0 = r10.$responseValue
                    java.lang.String r0 = r0.a()
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.fossil.tw6$a r3 = com.fossil.tw6.r
                    java.lang.String r3 = r3.a()
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "checkFirmware - downloadFw SUCCESS, latestFwVersion="
                    r5.append(r6)
                    r5.append(r0)
                    java.lang.String r5 = r5.toString()
                    r2.d(r3, r5)
                    com.fossil.tw6$b r2 = r10.this$0
                    com.fossil.tw6 r2 = r2.f3483a
                    com.fossil.dv7 r2 = com.fossil.tw6.w(r2)
                    com.fossil.tw6$b$a$a r3 = new com.fossil.tw6$b$a$a
                    r3.<init>(r10, r9)
                    r10.L$0 = r1
                    r10.L$1 = r0
                    r10.label = r7
                    java.lang.Object r2 = com.fossil.eu7.g(r2, r3, r10)
                    if (r2 != r4) goto L_0x00e5
                    r0 = r4
                    goto L_0x0030
                L_0x00b3:
                    com.fossil.tw6$b r0 = r10.this$0
                    com.fossil.tw6 r1 = r0.f3483a
                    com.portfolio.platform.data.model.Device r0 = r0.c
                    r1.N(r0)
                    goto L_0x002e
                L_0x00be:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.fossil.tw6$a r1 = com.fossil.tw6.r
                    java.lang.String r1 = r1.a()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r4 = "checkFirmware - downloadFw SUCCESS, latestFwVersion="
                    r2.append(r4)
                    r2.append(r3)
                    java.lang.String r3 = " but device is DianaEV1!!!"
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    r0.e(r1, r2)
                    goto L_0x002e
                L_0x00e5:
                    r3 = r0
                    goto L_0x0046
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.tw6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(tw6 tw6, String str, Device device) {
            this.f3483a = tw6;
            this.b = str;
            this.c = device;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ct5.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().e(tw6.r.a(), "checkFirmware - downloadFw FAILED!!!");
            this.f3483a.L().O1();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ct5.c cVar) {
            pq7.c(cVar, "responseValue");
            xw7 unused = gu7.d(this.f3483a.k(), null, null, new a(this, cVar, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Device> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.K().getDeviceBySerial(this.this$0.$deviceId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, String str2, qn7 qn7, tw6 tw6, Device device) {
            super(2, qn7);
            this.$deviceId = str;
            this.$lastFwTemp = str2;
            this.this$0 = tw6;
            this.$device$inlined = device;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.$deviceId, this.$lastFwTemp, qn7, this.this$0, this.$device$inlined);
            cVar.p$ = (iv7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) g;
            FLogger.INSTANCE.getLocal().d(tw6.r.a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
            if (device != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = tw6.r.a();
                local.d(a2, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
                if (!vt7.j(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                    FLogger.INSTANCE.getLocal().d(tw6.r.a(), "Handle OTA complete on check last OTA success");
                    this.this$0.n.B0(this.$deviceId);
                    this.this$0.L().t3();
                } else {
                    this.this$0.H(this.$device$inlined);
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw6 f3484a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(tw6 tw6) {
            this.f3484a = tw6;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = tw6.r.a();
            local.d(a2, "otaCompleteReceiver - isSuccess=" + booleanExtra);
            this.f3484a.L().A(booleanExtra);
            this.f3484a.n.B0(PortfolioApp.h0.c().J());
            if (booleanExtra) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.f3484a.e, tw6.r.a(), "[Sync Start] AUTO SYNC after OTA");
                PortfolioApp.h0.c().S1(this.f3484a.m, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tw6 f3485a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(tw6 tw6) {
            this.f3485a = tw6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = tw6.r.a();
                local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && vt7.j(otaEvent.getSerial(), PortfolioApp.h0.c().J(), true)) {
                    this.f3485a.L().Z((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1", f = "UpdateFirmwarePresenter.kt", l = {106}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Device>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$activeSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$activeSerial, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Device> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.K().getDeviceBySerial(this.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(tw6 tw6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = tw6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            String J;
            Object g;
            tw6 tw6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                J = PortfolioApp.h0.c().J();
                tw6 tw62 = this.this$0;
                dv7 h = tw62.h();
                a aVar = new a(this, J, null);
                this.L$0 = iv7;
                this.L$1 = J;
                this.L$2 = tw62;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
                tw6 = tw62;
            } else if (i == 1) {
                J = (String) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                tw6 = (tw6) this.L$2;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            tw6.g = (Device) g;
            boolean v0 = PortfolioApp.h0.c().v0();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = tw6.r.a();
            local.d(a2, "start - activeSerial=" + J + ", isDeviceOtaing=" + v0);
            if (!v0) {
                tw6 tw63 = this.this$0;
                tw63.I(tw63.g);
            }
            this.this$0.J();
            ul5 c = ck5.f.c("ota_session");
            this.this$0.h = c;
            ck5.f.a("ota_session", c);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1", f = "UpdateFirmwarePresenter.kt", l = {248}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends String, ? extends String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends String, ? extends String>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return p37.h.a().f(this.this$0.$activeSerial, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements iq4.e<yt5.d, yt5.c> {
            @DexIgnore
            /* renamed from: b */
            public void a(yt5.c cVar) {
                pq7.c(cVar, "errorValue");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(yt5.d dVar) {
                pq7.c(dVar, "responseValue");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(tw6 tw6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = tw6;
            this.$activeSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$activeSerial, qn7);
            gVar.p$ = (iv7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ul5 ul5;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            cl7 cl7 = (cl7) g;
            String str = (String) cl7.component1();
            String str2 = (String) cl7.component2();
            if (!(str == null || str2 == null || (ul5 = this.this$0.h) == null)) {
                ul5.b("old_firmware", str);
                if (ul5 != null) {
                    ul5.b("new_firmware", str2);
                    if (ul5 != null) {
                        ul5.i();
                    }
                }
            }
            this.this$0.o.e(new yt5.b(this.$activeSerial, false, 2, null), new b());
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = tw6.class.getSimpleName();
        pq7.b(simpleName, "UpdateFirmwarePresenter::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public tw6(qw6 qw6, DeviceRepository deviceRepository, UserRepository userRepository, mj5 mj5, on5 on5, yt5 yt5, ct5 ct5) {
        pq7.c(qw6, "mView");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(yt5, "mUpdateFirmwareUseCase");
        pq7.c(ct5, "mDownloadFwByDeviceModel");
        this.k = qw6;
        this.l = deviceRepository;
        this.m = mj5;
        this.n = on5;
        this.o = yt5;
        this.p = ct5;
    }

    @DexIgnore
    public final void H(Device device) {
        String deviceId = device.getDeviceId();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "checkFirmware - deviceId=" + deviceId + ", sku=" + device.getSku() + ", fw=" + device.getFirmwareRevision());
        if (PortfolioApp.h0.c().z0() || !this.n.y0()) {
            ct5 ct5 = this.p;
            String sku = device.getSku();
            if (sku == null) {
                sku = "";
            }
            ct5.e(new ct5.a(sku), new b(this, deviceId, device));
            return;
        }
        this.k.t3();
    }

    @DexIgnore
    public final void I(Device device) {
        if (device != null) {
            String deviceId = device.getDeviceId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.d(str, "checkLastOTASuccess - deviceId=" + deviceId + ", sku=" + device.getSku());
            if (TextUtils.isEmpty(deviceId)) {
                FLogger.INSTANCE.getLocal().e(q, "checkLastOTASuccess - DEVICE ID IS EMPTY!!!");
                return;
            }
            String Q = this.n.Q(deviceId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = q;
            local2.d(str2, "checkLastOTASuccess - lastTempFw=" + Q);
            if (TextUtils.isEmpty(Q)) {
                H(device);
            } else {
                xw7 unused = gu7.d(k(), null, null, new c(deviceId, Q, null, this, device), 3, null);
            }
        }
    }

    @DexIgnore
    public final void J() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (nk5.o.w(deviceBySerial)) {
            explore.setDescription(um5.c(PortfolioApp.h0.c(), 2131886916));
            explore.setBackground(2131231353);
            explore2.setDescription(um5.c(PortfolioApp.h0.c(), 2131886917));
            explore2.setBackground(2131231351);
            explore3.setDescription(um5.c(PortfolioApp.h0.c(), 2131886914));
            explore3.setBackground(2131231354);
            explore4.setDescription(um5.c(PortfolioApp.h0.c(), 2131886915));
            explore4.setBackground(2131231350);
        } else {
            explore.setDescription(um5.c(PortfolioApp.h0.c(), 2131886923));
            explore.setBackground(2131231353);
            explore2.setDescription(um5.c(PortfolioApp.h0.c(), 2131886922));
            explore2.setBackground(2131231355);
            explore3.setDescription(um5.c(PortfolioApp.h0.c(), 2131886920));
            explore3.setBackground(2131231354);
            explore4.setDescription(um5.c(PortfolioApp.h0.c(), 2131886921));
            explore4.setBackground(2131231352);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.k.X(arrayList);
    }

    @DexIgnore
    public final DeviceRepository K() {
        return this.l;
    }

    @DexIgnore
    public final qw6 L() {
        return this.k;
    }

    @DexIgnore
    public void M() {
        this.k.M5(this);
    }

    @DexIgnore
    public final void N(Device device) {
        pq7.c(device, "Device");
        String deviceId = device.getDeviceId();
        boolean v0 = PortfolioApp.h0.c().v0();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "updateFirmware - activeSerial=" + deviceId + ", isDeviceOtaing=" + v0);
        if (!v0) {
            xw7 unused = gu7.d(k(), null, null, new g(this, deviceId, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "start - mIsOnBoardingFlow=" + this.f);
        if (!this.f) {
            this.k.K0();
        }
        wq5.d.e(this.j, CommunicateMode.OTA);
        PortfolioApp c2 = PortfolioApp.h0.c();
        e eVar = this.i;
        c2.registerReceiver(eVar, new IntentFilter(PortfolioApp.h0.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        this.k.f();
        wq5.d.g(CommunicateMode.OTA);
        xw7 unused = gu7.d(k(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        try {
            PortfolioApp.h0.c().unregisterReceiver(this.i);
            wq5.d.j(this.j, CommunicateMode.OTA);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.e(str, "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.pw6
    public void n() {
        wq5.d.g(CommunicateMode.OTA);
    }

    @DexIgnore
    @Override // com.fossil.pw6
    public void o() {
        FLogger.INSTANCE.getLocal().d(q, "checkFwAgain");
        I(this.g);
    }

    @DexIgnore
    @Override // com.fossil.pw6
    public void p(boolean z, String str) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        this.f = z;
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.pw6
    public boolean q() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.pw6
    public void r() {
        if (nk5.o.x(PortfolioApp.h0.c().J())) {
            this.k.d0();
        } else {
            this.k.l();
        }
    }

    @DexIgnore
    @Override // com.fossil.pw6
    public void s() {
        FLogger.INSTANCE.getLocal().d(q, "tryOtaAgain");
        Device device = this.g;
        if (device != null) {
            N(device);
        }
        this.k.V2();
    }
}
