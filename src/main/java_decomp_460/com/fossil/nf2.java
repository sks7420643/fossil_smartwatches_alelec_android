package com.fossil;

import android.os.Process;
import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nf2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static String f2513a;
    @DexIgnore
    public static int b;

    @DexIgnore
    public static String a() {
        if (f2513a == null) {
            if (b == 0) {
                b = Process.myPid();
            }
            f2513a = b(b);
        }
        return f2513a;
    }

    @DexIgnore
    public static String b(int i) {
        BufferedReader bufferedReader;
        String str = null;
        if (i > 0) {
            try {
                StringBuilder sb = new StringBuilder(25);
                sb.append("/proc/");
                sb.append(i);
                sb.append("/cmdline");
                bufferedReader = c(sb.toString());
                try {
                    str = bufferedReader.readLine().trim();
                    lf2.a(bufferedReader);
                } catch (IOException e) {
                    lf2.a(bufferedReader);
                    return str;
                } catch (Throwable th) {
                    th = th;
                    lf2.a(bufferedReader);
                    throw th;
                }
            } catch (IOException e2) {
                bufferedReader = null;
                lf2.a(bufferedReader);
                return str;
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = null;
                lf2.a(bufferedReader);
                throw th;
            }
        }
        return str;
    }

    @DexIgnore
    public static BufferedReader c(String str) throws IOException {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return new BufferedReader(new FileReader(str));
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
}
