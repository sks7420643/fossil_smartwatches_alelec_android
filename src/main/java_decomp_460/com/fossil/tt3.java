package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt3<TResult, TContinuationResult> implements hu3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f3464a;
    @DexIgnore
    public /* final */ ft3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ lu3<TContinuationResult> c;

    @DexIgnore
    public tt3(Executor executor, ft3<TResult, TContinuationResult> ft3, lu3<TContinuationResult> lu3) {
        this.f3464a = executor;
        this.b = ft3;
        this.c = lu3;
    }

    @DexIgnore
    @Override // com.fossil.hu3
    public final void a(nt3<TResult> nt3) {
        this.f3464a.execute(new vt3(this, nt3));
    }
}
