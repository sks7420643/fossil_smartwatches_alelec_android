package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xo3 b;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 c;

    @DexIgnore
    public np3(fp3 fp3, xo3 xo3) {
        this.c = fp3;
        this.b = xo3;
    }

    @DexIgnore
    public final void run() {
        cl3 cl3 = this.c.d;
        if (cl3 == null) {
            this.c.d().F().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.b == null) {
                cl3.N0(0, null, null, this.c.e().getPackageName());
            } else {
                cl3.N0(this.b.c, this.b.f4149a, this.b.b, this.c.e().getPackageName());
            }
            this.c.e0();
        } catch (RemoteException e) {
            this.c.d().F().b("Failed to send current screen to the service", e);
        }
    }
}
