package com.fossil;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.KeyEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface be0 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements be0 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.be0$a$a")
        /* renamed from: com.fossil.be0$a$a  reason: collision with other inner class name */
        public static class C0015a implements be0 {
            @DexIgnore
            public IBinder b;

            @DexIgnore
            public C0015a(IBinder iBinder) {
                this.b = iBinder;
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.b;
            }

            @DexIgnore
            @Override // com.fossil.be0
            public boolean k1(KeyEvent keyEvent) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.session.IMediaSession");
                    if (keyEvent != null) {
                        obtain.writeInt(1);
                        keyEvent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.b.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.fossil.be0
            public void w(ae0 ae0) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.session.IMediaSession");
                    obtain.writeStrongBinder(ae0 != null ? ae0.asBinder() : null);
                    this.b.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        public static be0 d(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.media.session.IMediaSession");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof be0)) ? new C0015a(iBinder) : (be0) queryLocalInterface;
        }
    }

    @DexIgnore
    boolean k1(KeyEvent keyEvent) throws RemoteException;

    @DexIgnore
    void w(ae0 ae0) throws RemoteException;
}
