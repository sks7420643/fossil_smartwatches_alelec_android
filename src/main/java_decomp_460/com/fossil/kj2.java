package com.fossil;

import android.content.Context;
import com.fossil.m62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj2 implements m62.d.b {
    @DexIgnore
    public /* final */ GoogleSignInAccount b;

    @DexIgnore
    public kj2(Context context, GoogleSignInAccount googleSignInAccount) {
        if ("<<default account>>".equals(googleSignInAccount.f())) {
            if (mf2.h() && context.getPackageManager().hasSystemFeature("cn.google")) {
                this.b = null;
                return;
            }
        }
        this.b = googleSignInAccount;
    }

    @DexIgnore
    @Override // com.fossil.m62.d.b
    public final GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return obj == this || ((obj instanceof kj2) && pc2.a(((kj2) obj).b, this.b));
    }

    @DexIgnore
    public final int hashCode() {
        GoogleSignInAccount googleSignInAccount = this.b;
        if (googleSignInAccount != null) {
            return googleSignInAccount.hashCode();
        }
        return 0;
    }
}
