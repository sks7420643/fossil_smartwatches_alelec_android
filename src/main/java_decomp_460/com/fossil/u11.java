package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u11 {
    @DexIgnore
    public static /* final */ String d; // = x01.f("DelayedWorkTracker");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ v11 f3502a;
    @DexIgnore
    public /* final */ d11 b;
    @DexIgnore
    public /* final */ Map<String, Runnable> c; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ o31 b;

        @DexIgnore
        public a(o31 o31) {
            this.b = o31;
        }

        @DexIgnore
        public void run() {
            x01.c().a(u11.d, String.format("Scheduling work %s", this.b.f2626a), new Throwable[0]);
            u11.this.f3502a.a(this.b);
        }
    }

    @DexIgnore
    public u11(v11 v11, d11 d11) {
        this.f3502a = v11;
        this.b = d11;
    }

    @DexIgnore
    public void a(o31 o31) {
        Runnable remove = this.c.remove(o31.f2626a);
        if (remove != null) {
            this.b.b(remove);
        }
        a aVar = new a(o31);
        this.c.put(o31.f2626a, aVar);
        long currentTimeMillis = System.currentTimeMillis();
        this.b.a(o31.a() - currentTimeMillis, aVar);
    }

    @DexIgnore
    public void b(String str) {
        Runnable remove = this.c.remove(str);
        if (remove != null) {
            this.b.b(remove);
        }
    }
}
