package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dv extends ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ n6 I;
    @DexIgnore
    public /* final */ n6 J;
    @DexIgnore
    public /* final */ iu K;
    @DexIgnore
    public /* final */ short L;

    @DexIgnore
    public dv(iu iuVar, short s, hs hsVar, k5 k5Var, int i) {
        super(hsVar, k5Var, i);
        this.K = iuVar;
        this.L = (short) s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.b).putShort(this.L).array();
        pq7.b(array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.a()).putShort(this.L).array();
        pq7.b(array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        n6 n6Var = n6.FTC;
        this.I = n6Var;
        this.J = n6Var;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final mt E(byte b) {
        return ku.i.a(b);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final n6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.ps
    public final byte[] P() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final long a(o7 o7Var) {
        if (o7Var.f2638a == n6.FTC) {
            byte[] bArr = o7Var.b;
            if (bArr.length >= 9) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                short s = order.getShort(1);
                byte b2 = order.get(3);
                byte b3 = order.get(4);
                long o = hy1.o(order.getInt(5));
                if (b == iu.k.a() && this.L == s && b2 == ku.d.c && b3 == this.K.b && o > 0) {
                    return o;
                }
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.A0, hy1.l(this.L, null, 1, null));
    }
}
