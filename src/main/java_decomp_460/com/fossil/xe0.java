package com.fossil;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle$Delegate;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.collection.SimpleArrayMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cg0;
import com.fossil.fh0;
import com.fossil.ig0;
import com.fossil.nl0;
import com.fossil.sf0;
import com.fossil.xn0;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.lang.Thread;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xe0 extends AppCompatDelegate implements cg0.a, LayoutInflater.Factory2 {
    @DexIgnore
    public static /* final */ SimpleArrayMap<String, Integer> g0; // = new SimpleArrayMap<>();
    @DexIgnore
    public static /* final */ boolean h0; // = (Build.VERSION.SDK_INT < 21);
    @DexIgnore
    public static /* final */ int[] i0; // = {16842836};
    @DexIgnore
    public static /* final */ boolean j0; // = (!"robolectric".equals(Build.FINGERPRINT));
    @DexIgnore
    public static /* final */ boolean k0;
    @DexIgnore
    public static boolean l0; // = true;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public ViewGroup B;
    @DexIgnore
    public TextView C;
    @DexIgnore
    public View D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public t[] M;
    @DexIgnore
    public t N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public boolean R;
    @DexIgnore
    public boolean S;
    @DexIgnore
    public int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public boolean W;
    @DexIgnore
    public m X;
    @DexIgnore
    public m Y;
    @DexIgnore
    public boolean Z;
    @DexIgnore
    public int a0;
    @DexIgnore
    public /* final */ Runnable b0;
    @DexIgnore
    public boolean c0;
    @DexIgnore
    public Rect d0;
    @DexIgnore
    public /* final */ Object e;
    @DexIgnore
    public Rect e0;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public af0 f0;
    @DexIgnore
    public Window g;
    @DexIgnore
    public k h;
    @DexIgnore
    public /* final */ we0 i;
    @DexIgnore
    public ActionBar j;
    @DexIgnore
    public MenuInflater k;
    @DexIgnore
    public CharSequence l;
    @DexIgnore
    public bh0 m;
    @DexIgnore
    public i s;
    @DexIgnore
    public u t;
    @DexIgnore
    public ActionMode u;
    @DexIgnore
    public ActionBarContextView v;
    @DexIgnore
    public PopupWindow w;
    @DexIgnore
    public Runnable x;
    @DexIgnore
    public ro0 y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Thread.UncaughtExceptionHandler {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Thread.UncaughtExceptionHandler f4100a;

        @DexIgnore
        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.f4100a = uncaughtExceptionHandler;
        }

        @DexIgnore
        public final boolean a(Throwable th) {
            String message;
            if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                return false;
            }
            return message.contains(ResourceManager.DRAWABLE) || message.contains("Drawable");
        }

        @DexIgnore
        public void uncaughtException(Thread thread, Throwable th) {
            if (a(th)) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.f4100a.uncaughtException(thread, notFoundException);
                return;
            }
            this.f4100a.uncaughtException(thread, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            xe0 xe0 = xe0.this;
            if ((xe0.a0 & 1) != 0) {
                xe0.X(0);
            }
            xe0 xe02 = xe0.this;
            if ((xe02.a0 & 4096) != 0) {
                xe02.X(108);
            }
            xe0 xe03 = xe0.this;
            xe03.Z = false;
            xe03.a0 = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements io0 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.io0
        public vo0 a(View view, vo0 vo0) {
            int g = vo0.g();
            int N0 = xe0.this.N0(vo0, null);
            if (g != N0) {
                vo0 = vo0.m(vo0.e(), N0, vo0.f(), vo0.d());
            }
            return mo0.X(view, vo0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements fh0.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.fh0.a
        public void a(Rect rect) {
            rect.top = xe0.this.N0(null, rect);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements ContentFrameLayout.a {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ContentFrameLayout.a
        public void a() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ContentFrameLayout.a
        public void onDetachedFromWindow() {
            xe0.this.V();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends to0 {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.so0
            public void b(View view) {
                xe0.this.v.setAlpha(1.0f);
                xe0.this.y.f(null);
                xe0.this.y = null;
            }

            @DexIgnore
            @Override // com.fossil.so0, com.fossil.to0
            public void c(View view) {
                xe0.this.v.setVisibility(0);
            }
        }

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void run() {
            xe0 xe0 = xe0.this;
            xe0.w.showAtLocation(xe0.v, 55, 0, 0);
            xe0.this.Y();
            if (xe0.this.G0()) {
                xe0.this.v.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                xe0 xe02 = xe0.this;
                ro0 c = mo0.c(xe02.v);
                c.a(1.0f);
                xe02.y = c;
                xe0.this.y.f(new a());
                return;
            }
            xe0.this.v.setAlpha(1.0f);
            xe0.this.v.setVisibility(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends to0 {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.so0
        public void b(View view) {
            xe0.this.v.setAlpha(1.0f);
            xe0.this.y.f(null);
            xe0.this.y = null;
        }

        @DexIgnore
        @Override // com.fossil.so0, com.fossil.to0
        public void c(View view) {
            xe0.this.v.setVisibility(0);
            xe0.this.v.sendAccessibilityEvent(32);
            if (xe0.this.v.getParent() instanceof View) {
                mo0.i0((View) xe0.this.v.getParent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements ActionBarDrawerToggle$Delegate {
        @DexIgnore
        public h(xe0 xe0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements ig0.a {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public void b(cg0 cg0, boolean z) {
            xe0.this.O(cg0);
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public boolean c(cg0 cg0) {
            Window.Callback i0 = xe0.this.i0();
            if (i0 == null) {
                return true;
            }
            i0.onMenuOpened(108, cg0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements ActionMode.Callback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ActionMode.Callback f4106a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends to0 {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.so0
            public void b(View view) {
                xe0.this.v.setVisibility(8);
                xe0 xe0 = xe0.this;
                PopupWindow popupWindow = xe0.w;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (xe0.v.getParent() instanceof View) {
                    mo0.i0((View) xe0.this.v.getParent());
                }
                xe0.this.v.removeAllViews();
                xe0.this.y.f(null);
                xe0 xe02 = xe0.this;
                xe02.y = null;
                mo0.i0(xe02.B);
            }
        }

        @DexIgnore
        public j(ActionMode.Callback callback) {
            this.f4106a = callback;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public void a(ActionMode actionMode) {
            this.f4106a.a(actionMode);
            xe0 xe0 = xe0.this;
            if (xe0.w != null) {
                xe0.g.getDecorView().removeCallbacks(xe0.this.x);
            }
            xe0 xe02 = xe0.this;
            if (xe02.v != null) {
                xe02.Y();
                xe0 xe03 = xe0.this;
                ro0 c = mo0.c(xe03.v);
                c.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                xe03.y = c;
                xe0.this.y.f(new a());
            }
            xe0 xe04 = xe0.this;
            we0 we0 = xe04.i;
            if (we0 != null) {
                we0.onSupportActionModeFinished(xe04.u);
            }
            xe0 xe05 = xe0.this;
            xe05.u = null;
            mo0.i0(xe05.B);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean b(ActionMode actionMode, Menu menu) {
            return this.f4106a.b(actionMode, menu);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean c(ActionMode actionMode, Menu menu) {
            mo0.i0(xe0.this.B);
            return this.f4106a.c(actionMode, menu);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean d(ActionMode actionMode, MenuItem menuItem) {
            return this.f4106a.d(actionMode, menuItem);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends vf0 {
        @DexIgnore
        public k(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public final android.view.ActionMode b(ActionMode.Callback callback) {
            sf0.a aVar = new sf0.a(xe0.this.f, callback);
            androidx.appcompat.view.ActionMode H = xe0.this.H(aVar);
            if (H != null) {
                return aVar.e(H);
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return xe0.this.W(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || xe0.this.u0(keyEvent.getKeyCode(), keyEvent);
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public void onContentChanged() {
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof cg0)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            xe0.this.x0(i);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            xe0.this.y0(i);
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public boolean onPreparePanel(int i, View view, Menu menu) {
            cg0 cg0 = menu instanceof cg0 ? (cg0) menu : null;
            if (i == 0 && cg0 == null) {
                return false;
            }
            if (cg0 != null) {
                cg0.e0(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (cg0 == null) {
                return onPreparePanel;
            }
            cg0.e0(false);
            return onPreparePanel;
        }

        @DexIgnore
        @Override // com.fossil.vf0, android.view.Window.Callback
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            cg0 cg0;
            t g0 = xe0.this.g0(0, true);
            if (g0 == null || (cg0 = g0.j) == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, cg0, i);
            }
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            return xe0.this.p0() ? b(callback) : super.onWindowStartingActionMode(callback);
        }

        @DexIgnore
        @Override // com.fossil.vf0
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            return (!xe0.this.p0() || i != 0) ? super.onWindowStartingActionMode(callback, i) : b(callback);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends m {
        @DexIgnore
        public /* final */ PowerManager c;

        @DexIgnore
        public l(Context context) {
            super();
            this.c = (PowerManager) context.getApplicationContext().getSystemService("power");
        }

        @DexIgnore
        @Override // com.fossil.xe0.m
        public IntentFilter b() {
            if (Build.VERSION.SDK_INT < 21) {
                return null;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }

        @DexIgnore
        @Override // com.fossil.xe0.m
        public int c() {
            return (Build.VERSION.SDK_INT < 21 || !this.c.isPowerSaveMode()) ? 1 : 2;
        }

        @DexIgnore
        @Override // com.fossil.xe0.m
        public void d() {
            xe0.this.I();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class m {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public BroadcastReceiver f4108a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends BroadcastReceiver {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                m.this.d();
            }
        }

        @DexIgnore
        public m() {
        }

        @DexIgnore
        public void a() {
            BroadcastReceiver broadcastReceiver = this.f4108a;
            if (broadcastReceiver != null) {
                try {
                    xe0.this.f.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException e) {
                }
                this.f4108a = null;
            }
        }

        @DexIgnore
        public abstract IntentFilter b();

        @DexIgnore
        public abstract int c();

        @DexIgnore
        public abstract void d();

        @DexIgnore
        public void e() {
            a();
            IntentFilter b2 = b();
            if (b2 != null && b2.countActions() != 0) {
                if (this.f4108a == null) {
                    this.f4108a = new a();
                }
                xe0.this.f.registerReceiver(this.f4108a, b2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n extends m {
        @DexIgnore
        public /* final */ ef0 c;

        @DexIgnore
        public n(ef0 ef0) {
            super();
            this.c = ef0;
        }

        @DexIgnore
        @Override // com.fossil.xe0.m
        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        @DexIgnore
        @Override // com.fossil.xe0.m
        public int c() {
            return this.c.d() ? 2 : 1;
        }

        @DexIgnore
        @Override // com.fossil.xe0.m
        public void d() {
            xe0.this.I();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class o {
        @DexIgnore
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            int i = configuration.densityDpi;
            int i2 = configuration2.densityDpi;
            if (i != i2) {
                configuration3.densityDpi = i2;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class p {
        @DexIgnore
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            LocaleList locales = configuration.getLocales();
            LocaleList locales2 = configuration2.getLocales();
            if (!locales.equals(locales2)) {
                configuration3.setLocales(locales2);
                configuration3.locale = configuration2.locale;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class q {
        @DexIgnore
        public static void a(Configuration configuration, Configuration configuration2, Configuration configuration3) {
            int i = configuration.colorMode;
            int i2 = configuration2.colorMode;
            if ((i & 3) != (i2 & 3)) {
                configuration3.colorMode |= i2 & 3;
            }
            int i3 = configuration.colorMode;
            int i4 = configuration2.colorMode;
            if ((i3 & 12) != (i4 & 12)) {
                configuration3.colorMode |= i4 & 12;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class r {
        @DexIgnore
        public static void a(ContextThemeWrapper contextThemeWrapper, Configuration configuration) {
            contextThemeWrapper.applyOverrideConfiguration(configuration);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class s extends ContentFrameLayout {
        @DexIgnore
        public s(Context context) {
            super(context);
        }

        @DexIgnore
        public final boolean c(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return xe0.this.W(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !c((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            xe0.this.Q(0);
            return true;
        }

        @DexIgnore
        public void setBackgroundResource(int i) {
            setBackgroundDrawable(gf0.d(getContext(), i));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f4110a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public ViewGroup g;
        @DexIgnore
        public View h;
        @DexIgnore
        public View i;
        @DexIgnore
        public cg0 j;
        @DexIgnore
        public ag0 k;
        @DexIgnore
        public Context l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q; // = false;
        @DexIgnore
        public boolean r;
        @DexIgnore
        public Bundle s;

        @DexIgnore
        public t(int i2) {
            this.f4110a = i2;
        }

        @DexIgnore
        public jg0 a(ig0.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                ag0 ag0 = new ag0(this.l, re0.abc_list_menu_item_layout);
                this.k = ag0;
                ag0.g(aVar);
                this.j.b(this.k);
            }
            return this.k.j(this.g);
        }

        @DexIgnore
        public boolean b() {
            if (this.h == null) {
                return false;
            }
            return this.i != null || this.k.a().getCount() > 0;
        }

        @DexIgnore
        public void c(cg0 cg0) {
            ag0 ag0;
            cg0 cg02 = this.j;
            if (cg0 != cg02) {
                if (cg02 != null) {
                    cg02.Q(this.k);
                }
                this.j = cg0;
                if (cg0 != null && (ag0 = this.k) != null) {
                    cg0.b(ag0);
                }
            }
        }

        @DexIgnore
        public void d(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(le0.actionBarPopupTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            }
            newTheme.resolveAttribute(le0.panelMenuListTheme, typedValue, true);
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                newTheme.applyStyle(i3, true);
            } else {
                newTheme.applyStyle(te0.Theme_AppCompat_CompactMenu, true);
            }
            qf0 qf0 = new qf0(context, 0);
            qf0.getTheme().setTo(newTheme);
            this.l = qf0;
            TypedArray obtainStyledAttributes = qf0.obtainStyledAttributes(ue0.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(ue0.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(ue0.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements ig0.a {
        @DexIgnore
        public u() {
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public void b(cg0 cg0, boolean z) {
            cg0 F = cg0.F();
            boolean z2 = F != cg0;
            xe0 xe0 = xe0.this;
            if (z2) {
                cg0 = F;
            }
            t b0 = xe0.b0(cg0);
            if (b0 == null) {
                return;
            }
            if (z2) {
                xe0.this.N(b0.f4110a, b0, F);
                xe0.this.R(b0, true);
                return;
            }
            xe0.this.R(b0, z);
        }

        @DexIgnore
        @Override // com.fossil.ig0.a
        public boolean c(cg0 cg0) {
            Window.Callback i0;
            if (cg0 != cg0.F()) {
                return true;
            }
            xe0 xe0 = xe0.this;
            if (!xe0.G || (i0 = xe0.i0()) == null || xe0.this.S) {
                return true;
            }
            i0.onMenuOpened(108, cg0);
            return true;
        }
    }

    /*
    static {
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 17) {
            z2 = true;
        }
        k0 = z2;
        if (h0 && !l0) {
            Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }
    */

    @DexIgnore
    public xe0(Activity activity, we0 we0) {
        this(activity, null, we0, activity);
    }

    @DexIgnore
    public xe0(Dialog dialog, we0 we0) {
        this(dialog.getContext(), dialog.getWindow(), we0, dialog);
    }

    @DexIgnore
    public xe0(Context context, Window window, we0 we0, Object obj) {
        Integer num;
        AppCompatActivity K0;
        this.y = null;
        this.z = true;
        this.T = -100;
        this.b0 = new b();
        this.f = context;
        this.i = we0;
        this.e = obj;
        if (this.T == -100 && (obj instanceof Dialog) && (K0 = K0()) != null) {
            this.T = K0.getDelegate().l();
        }
        if (this.T == -100 && (num = g0.get(this.e.getClass().getName())) != null) {
            this.T = num.intValue();
            g0.remove(this.e.getClass().getName());
        }
        if (window != null) {
            L(window);
        }
        ug0.h();
    }

    @DexIgnore
    public static Configuration c0(Configuration configuration, Configuration configuration2) {
        Configuration configuration3 = new Configuration();
        configuration3.fontScale = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (!(configuration2 == null || configuration.diff(configuration2) == 0)) {
            float f2 = configuration.fontScale;
            float f3 = configuration2.fontScale;
            if (f2 != f3) {
                configuration3.fontScale = f3;
            }
            int i2 = configuration.mcc;
            int i3 = configuration2.mcc;
            if (i2 != i3) {
                configuration3.mcc = i3;
            }
            int i4 = configuration.mnc;
            int i5 = configuration2.mnc;
            if (i4 != i5) {
                configuration3.mnc = i5;
            }
            if (Build.VERSION.SDK_INT >= 24) {
                p.a(configuration, configuration2, configuration3);
            } else if (!kn0.a(configuration.locale, configuration2.locale)) {
                configuration3.locale = configuration2.locale;
            }
            int i6 = configuration.touchscreen;
            int i7 = configuration2.touchscreen;
            if (i6 != i7) {
                configuration3.touchscreen = i7;
            }
            int i8 = configuration.keyboard;
            int i9 = configuration2.keyboard;
            if (i8 != i9) {
                configuration3.keyboard = i9;
            }
            int i10 = configuration.keyboardHidden;
            int i11 = configuration2.keyboardHidden;
            if (i10 != i11) {
                configuration3.keyboardHidden = i11;
            }
            int i12 = configuration.navigation;
            int i13 = configuration2.navigation;
            if (i12 != i13) {
                configuration3.navigation = i13;
            }
            int i14 = configuration.navigationHidden;
            int i15 = configuration2.navigationHidden;
            if (i14 != i15) {
                configuration3.navigationHidden = i15;
            }
            int i16 = configuration.orientation;
            int i17 = configuration2.orientation;
            if (i16 != i17) {
                configuration3.orientation = i17;
            }
            int i18 = configuration.screenLayout;
            int i19 = configuration2.screenLayout;
            if ((i18 & 15) != (i19 & 15)) {
                configuration3.screenLayout |= i19 & 15;
            }
            int i20 = configuration.screenLayout;
            int i21 = configuration2.screenLayout;
            if ((i20 & 192) != (i21 & 192)) {
                configuration3.screenLayout |= i21 & 192;
            }
            int i22 = configuration.screenLayout;
            int i23 = configuration2.screenLayout;
            if ((i22 & 48) != (i23 & 48)) {
                configuration3.screenLayout |= i23 & 48;
            }
            int i24 = configuration.screenLayout;
            int i25 = configuration2.screenLayout;
            if ((i24 & 768) != (i25 & 768)) {
                configuration3.screenLayout |= i25 & 768;
            }
            if (Build.VERSION.SDK_INT >= 26) {
                q.a(configuration, configuration2, configuration3);
            }
            int i26 = configuration.uiMode;
            int i27 = configuration2.uiMode;
            if ((i26 & 15) != (i27 & 15)) {
                configuration3.uiMode |= i27 & 15;
            }
            int i28 = configuration.uiMode;
            int i29 = configuration2.uiMode;
            if ((i28 & 48) != (i29 & 48)) {
                configuration3.uiMode |= i29 & 48;
            }
            int i30 = configuration.screenWidthDp;
            int i31 = configuration2.screenWidthDp;
            if (i30 != i31) {
                configuration3.screenWidthDp = i31;
            }
            int i32 = configuration.screenHeightDp;
            int i33 = configuration2.screenHeightDp;
            if (i32 != i33) {
                configuration3.screenHeightDp = i33;
            }
            int i34 = configuration.smallestScreenWidthDp;
            int i35 = configuration2.smallestScreenWidthDp;
            if (i34 != i35) {
                configuration3.smallestScreenWidthDp = i35;
            }
            if (Build.VERSION.SDK_INT >= 17) {
                o.a(configuration, configuration2, configuration3);
            }
        }
        return configuration3;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public boolean A(int i2) {
        int F0 = F0(i2);
        if (this.K && F0 == 108) {
            return false;
        }
        if (this.G && F0 == 1) {
            this.G = false;
        }
        if (F0 == 1) {
            J0();
            this.K = true;
            return true;
        } else if (F0 == 2) {
            J0();
            this.E = true;
            return true;
        } else if (F0 == 5) {
            J0();
            this.F = true;
            return true;
        } else if (F0 == 10) {
            J0();
            this.I = true;
            return true;
        } else if (F0 == 108) {
            J0();
            this.G = true;
            return true;
        } else if (F0 != 109) {
            return this.g.requestFeature(F0);
        } else {
            J0();
            this.H = true;
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007f, code lost:
        if (r0.width == -1) goto L_0x0081;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0(com.fossil.xe0.t r11, android.view.KeyEvent r12) {
        /*
        // Method dump skipped, instructions count: 240
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xe0.A0(com.fossil.xe0$t, android.view.KeyEvent):void");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void B(int i2) {
        Z();
        ViewGroup viewGroup = (ViewGroup) this.B.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f).inflate(i2, viewGroup);
        this.h.a().onContentChanged();
    }

    @DexIgnore
    public final ActionBar B0() {
        return this.j;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void C(View view) {
        Z();
        ViewGroup viewGroup = (ViewGroup) this.B.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.h.a().onContentChanged();
    }

    @DexIgnore
    public final boolean C0(t tVar, int i2, KeyEvent keyEvent, int i3) {
        cg0 cg0;
        boolean z2 = false;
        if (!keyEvent.isSystem()) {
            if ((tVar.m || D0(tVar, keyEvent)) && (cg0 = tVar.j) != null) {
                z2 = cg0.performShortcut(i2, keyEvent, i3);
            }
            if (z2 && (i3 & 1) == 0 && this.m == null) {
                R(tVar, true);
            }
        }
        return z2;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void D(View view, ViewGroup.LayoutParams layoutParams) {
        Z();
        ViewGroup viewGroup = (ViewGroup) this.B.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.h.a().onContentChanged();
    }

    @DexIgnore
    public final boolean D0(t tVar, KeyEvent keyEvent) {
        bh0 bh0;
        bh0 bh02;
        bh0 bh03;
        if (this.S) {
            return false;
        }
        if (tVar.m) {
            return true;
        }
        t tVar2 = this.N;
        if (!(tVar2 == null || tVar2 == tVar)) {
            R(tVar2, false);
        }
        Window.Callback i02 = i0();
        if (i02 != null) {
            tVar.i = i02.onCreatePanelView(tVar.f4110a);
        }
        int i2 = tVar.f4110a;
        boolean z2 = i2 == 0 || i2 == 108;
        if (z2 && (bh03 = this.m) != null) {
            bh03.c();
        }
        if (tVar.i == null && (!z2 || !(B0() instanceof cf0))) {
            if (tVar.j == null || tVar.r) {
                if (tVar.j == null && (!m0(tVar) || tVar.j == null)) {
                    return false;
                }
                if (z2 && this.m != null) {
                    if (this.s == null) {
                        this.s = new i();
                    }
                    this.m.a(tVar.j, this.s);
                }
                tVar.j.h0();
                if (!i02.onCreatePanelMenu(tVar.f4110a, tVar.j)) {
                    tVar.c(null);
                    if (!z2 || (bh02 = this.m) == null) {
                        return false;
                    }
                    bh02.a(null, this.s);
                    return false;
                }
                tVar.r = false;
            }
            tVar.j.h0();
            Bundle bundle = tVar.s;
            if (bundle != null) {
                tVar.j.R(bundle);
                tVar.s = null;
            }
            if (!i02.onPreparePanel(0, tVar.i, tVar.j)) {
                if (z2 && (bh0 = this.m) != null) {
                    bh0.a(null, this.s);
                }
                tVar.j.g0();
                return false;
            }
            boolean z3 = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            tVar.p = z3;
            tVar.j.setQwertyMode(z3);
            tVar.j.g0();
        }
        tVar.m = true;
        tVar.n = false;
        this.N = tVar;
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void E(Toolbar toolbar) {
        if (this.e instanceof Activity) {
            ActionBar n2 = n();
            if (!(n2 instanceof ff0)) {
                this.k = null;
                if (n2 != null) {
                    n2.n();
                }
                if (toolbar != null) {
                    cf0 cf0 = new cf0(toolbar, h0(), this.h);
                    this.j = cf0;
                    this.g.setCallback(cf0.z());
                } else {
                    this.j = null;
                    this.g.setCallback(this.h);
                }
                p();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    @DexIgnore
    public final void E0(boolean z2) {
        bh0 bh0 = this.m;
        if (bh0 == null || !bh0.d() || (ViewConfiguration.get(this.f).hasPermanentMenuKey() && !this.m.e())) {
            t g02 = g0(0, true);
            g02.q = true;
            R(g02, false);
            A0(g02, null);
            return;
        }
        Window.Callback i02 = i0();
        if (this.m.b() && z2) {
            this.m.f();
            if (!this.S) {
                i02.onPanelClosed(108, g0(0, true).j);
            }
        } else if (i02 != null && !this.S) {
            if (this.Z && (this.a0 & 1) != 0) {
                this.g.getDecorView().removeCallbacks(this.b0);
                this.b0.run();
            }
            t g03 = g0(0, true);
            cg0 cg0 = g03.j;
            if (cg0 != null && !g03.r && i02.onPreparePanel(0, g03.i, cg0)) {
                i02.onMenuOpened(108, g03.j);
                this.m.g();
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void F(int i2) {
        this.U = i2;
    }

    @DexIgnore
    public final int F0(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public final void G(CharSequence charSequence) {
        this.l = charSequence;
        bh0 bh0 = this.m;
        if (bh0 != null) {
            bh0.setWindowTitle(charSequence);
        } else if (B0() != null) {
            B0().w(charSequence);
        } else {
            TextView textView = this.C;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    @DexIgnore
    public final boolean G0() {
        ViewGroup viewGroup;
        return this.A && (viewGroup = this.B) != null && mo0.Q(viewGroup);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public androidx.appcompat.view.ActionMode H(ActionMode.Callback callback) {
        we0 we0;
        if (callback != null) {
            androidx.appcompat.view.ActionMode actionMode = this.u;
            if (actionMode != null) {
                actionMode.c();
            }
            j jVar = new j(callback);
            ActionBar n2 = n();
            if (n2 != null) {
                androidx.appcompat.view.ActionMode x2 = n2.x(jVar);
                this.u = x2;
                if (!(x2 == null || (we0 = this.i) == null)) {
                    we0.onSupportActionModeStarted(x2);
                }
            }
            if (this.u == null) {
                this.u = I0(jVar);
            }
            return this.u;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    @DexIgnore
    public final boolean H0(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.g.getDecorView();
        for (ViewParent viewParent2 = viewParent; viewParent2 != null; viewParent2 = viewParent2.getParent()) {
            if (viewParent2 == decorView || !(viewParent2 instanceof View) || mo0.P((View) viewParent2)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean I() {
        return J(true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public androidx.appcompat.view.ActionMode I0(androidx.appcompat.view.ActionMode.Callback r9) {
        /*
        // Method dump skipped, instructions count: 372
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xe0.I0(androidx.appcompat.view.ActionMode$Callback):androidx.appcompat.view.ActionMode");
    }

    @DexIgnore
    public final boolean J(boolean z2) {
        if (this.S) {
            return false;
        }
        int M2 = M();
        boolean L0 = L0(q0(this.f, M2), z2);
        if (M2 == 0) {
            f0(this.f).e();
        } else {
            m mVar = this.X;
            if (mVar != null) {
                mVar.a();
            }
        }
        if (M2 == 3) {
            e0(this.f).e();
            return L0;
        }
        m mVar2 = this.Y;
        if (mVar2 == null) {
            return L0;
        }
        mVar2.a();
        return L0;
    }

    @DexIgnore
    public final void J0() {
        if (this.A) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    @DexIgnore
    public final void K() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.B.findViewById(16908290);
        View decorView = this.g.getDecorView();
        contentFrameLayout.b(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f.obtainStyledAttributes(ue0.AppCompatTheme);
        obtainStyledAttributes.getValue(ue0.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(ue0.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(ue0.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(ue0.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(ue0.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(ue0.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(ue0.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(ue0.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(ue0.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(ue0.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    @DexIgnore
    public final AppCompatActivity K0() {
        for (Context context = this.f; context != null; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof AppCompatActivity) {
                return (AppCompatActivity) context;
            }
            if (!(context instanceof ContextWrapper)) {
                break;
            }
        }
        return null;
    }

    @DexIgnore
    public final void L(Window window) {
        if (this.g == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof k)) {
                k kVar = new k(callback);
                this.h = kVar;
                window.setCallback(kVar);
                th0 u2 = th0.u(this.f, null, i0);
                Drawable h2 = u2.h(0);
                if (h2 != null) {
                    window.setBackgroundDrawable(h2);
                }
                u2.w();
                this.g = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean L0(int r8, boolean r9) {
        /*
            r7 = this;
            r6 = 0
            r1 = 1
            android.content.Context r0 = r7.f
            android.content.res.Configuration r0 = r7.S(r0, r8, r6)
            boolean r2 = r7.o0()
            android.content.Context r3 = r7.f
            android.content.res.Resources r3 = r3.getResources()
            android.content.res.Configuration r3 = r3.getConfiguration()
            int r3 = r3.uiMode
            r3 = r3 & 48
            int r0 = r0.uiMode
            r4 = r0 & 48
            if (r3 == r4) goto L_0x005b
            if (r9 == 0) goto L_0x005b
            if (r2 != 0) goto L_0x005b
            boolean r0 = r7.P
            if (r0 == 0) goto L_0x005b
            boolean r0 = com.fossil.xe0.j0
            if (r0 != 0) goto L_0x0030
            boolean r0 = r7.Q
            if (r0 == 0) goto L_0x005b
        L_0x0030:
            java.lang.Object r0 = r7.e
            boolean r5 = r0 instanceof android.app.Activity
            if (r5 == 0) goto L_0x005b
            android.app.Activity r0 = (android.app.Activity) r0
            boolean r0 = r0.isChild()
            if (r0 != 0) goto L_0x005b
            java.lang.Object r0 = r7.e
            android.app.Activity r0 = (android.app.Activity) r0
            com.fossil.rk0.t(r0)
            r0 = r1
        L_0x0046:
            if (r0 != 0) goto L_0x005d
            if (r3 == r4) goto L_0x005d
            r7.M0(r4, r2, r6)
        L_0x004d:
            if (r1 == 0) goto L_0x005a
            java.lang.Object r0 = r7.e
            boolean r2 = r0 instanceof androidx.appcompat.app.AppCompatActivity
            if (r2 == 0) goto L_0x005a
            androidx.appcompat.app.AppCompatActivity r0 = (androidx.appcompat.app.AppCompatActivity) r0
            r0.onNightModeChanged(r8)
        L_0x005a:
            return r1
        L_0x005b:
            r0 = 0
            goto L_0x0046
        L_0x005d:
            r1 = r0
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xe0.L0(int, boolean):boolean");
    }

    @DexIgnore
    public final int M() {
        int i2 = this.T;
        return i2 != -100 ? i2 : AppCompatDelegate.j();
    }

    @DexIgnore
    public final void M0(int i2, boolean z2, Configuration configuration) {
        Resources resources = this.f.getResources();
        Configuration configuration2 = new Configuration(resources.getConfiguration());
        if (configuration != null) {
            configuration2.updateFrom(configuration);
        }
        configuration2.uiMode = (resources.getConfiguration().uiMode & -49) | i2;
        resources.updateConfiguration(configuration2, null);
        if (Build.VERSION.SDK_INT < 26) {
            bf0.a(resources);
        }
        int i3 = this.U;
        if (i3 != 0) {
            this.f.setTheme(i3);
            if (Build.VERSION.SDK_INT >= 23) {
                this.f.getTheme().applyStyle(this.U, true);
            }
        }
        if (z2) {
            Object obj = this.e;
            if (obj instanceof Activity) {
                Activity activity = (Activity) obj;
                if (activity instanceof LifecycleOwner) {
                    if (((LifecycleOwner) activity).getLifecycle().b().isAtLeast(Lifecycle.State.STARTED)) {
                        activity.onConfigurationChanged(configuration2);
                    }
                } else if (this.R) {
                    activity.onConfigurationChanged(configuration2);
                }
            }
        }
    }

    @DexIgnore
    public void N(int i2, t tVar, Menu menu) {
        if (menu == null) {
            if (tVar == null && i2 >= 0) {
                t[] tVarArr = this.M;
                if (i2 < tVarArr.length) {
                    tVar = tVarArr[i2];
                }
            }
            if (tVar != null) {
                menu = tVar.j;
            }
        }
        if ((tVar == null || tVar.o) && !this.S) {
            this.h.a().onPanelClosed(i2, menu);
        }
    }

    @DexIgnore
    public final int N0(vo0 vo0, Rect rect) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        int i2 = 0;
        int g2 = vo0 != null ? vo0.g() : rect != null ? rect.top : 0;
        ActionBarContextView actionBarContextView = this.v;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.v.getLayoutParams();
            if (this.v.isShown()) {
                if (this.d0 == null) {
                    this.d0 = new Rect();
                    this.e0 = new Rect();
                }
                Rect rect2 = this.d0;
                Rect rect3 = this.e0;
                if (vo0 == null) {
                    rect2.set(rect);
                } else {
                    rect2.set(vo0.e(), vo0.g(), vo0.f(), vo0.d());
                }
                zh0.a(this.B, rect2, rect3);
                int i3 = rect2.top;
                int i4 = rect2.left;
                int i5 = rect2.right;
                vo0 G2 = mo0.G(this.B);
                int e2 = G2 == null ? 0 : G2.e();
                int f2 = G2 == null ? 0 : G2.f();
                if (marginLayoutParams.topMargin == i3 && marginLayoutParams.leftMargin == i4 && marginLayoutParams.rightMargin == i5) {
                    z5 = false;
                } else {
                    marginLayoutParams.topMargin = i3;
                    marginLayoutParams.leftMargin = i4;
                    marginLayoutParams.rightMargin = i5;
                    z5 = true;
                }
                if (i3 <= 0 || this.D != null) {
                    View view = this.D;
                    if (view != null) {
                        ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                        if (!(marginLayoutParams2.height == marginLayoutParams.topMargin && marginLayoutParams2.leftMargin == e2 && marginLayoutParams2.rightMargin == f2)) {
                            marginLayoutParams2.height = marginLayoutParams.topMargin;
                            marginLayoutParams2.leftMargin = e2;
                            marginLayoutParams2.rightMargin = f2;
                            this.D.setLayoutParams(marginLayoutParams2);
                        }
                    }
                } else {
                    View view2 = new View(this.f);
                    this.D = view2;
                    view2.setVisibility(8);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, marginLayoutParams.topMargin, 51);
                    layoutParams.leftMargin = e2;
                    layoutParams.rightMargin = f2;
                    this.B.addView(this.D, -1, layoutParams);
                }
                z3 = this.D != null;
                if (z3 && this.D.getVisibility() != 0) {
                    O0(this.D);
                }
                if (!this.I && z3) {
                    g2 = 0;
                }
                z4 = z5;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = false;
                z4 = true;
            } else {
                z3 = false;
                z4 = false;
            }
            if (z4) {
                this.v.setLayoutParams(marginLayoutParams);
                z2 = z3;
            } else {
                z2 = z3;
            }
        }
        View view3 = this.D;
        if (view3 != null) {
            if (!z2) {
                i2 = 8;
            }
            view3.setVisibility(i2);
        }
        return g2;
    }

    @DexIgnore
    public void O(cg0 cg0) {
        if (!this.L) {
            this.L = true;
            this.m.i();
            Window.Callback i02 = i0();
            if (i02 != null && !this.S) {
                i02.onPanelClosed(108, cg0);
            }
            this.L = false;
        }
    }

    @DexIgnore
    public final void O0(View view) {
        view.setBackgroundColor((mo0.J(view) & 8192) != 0 ? gl0.d(this.f, ne0.abc_decor_view_status_guard_light) : gl0.d(this.f, ne0.abc_decor_view_status_guard));
    }

    @DexIgnore
    public final void P() {
        m mVar = this.X;
        if (mVar != null) {
            mVar.a();
        }
        m mVar2 = this.Y;
        if (mVar2 != null) {
            mVar2.a();
        }
    }

    @DexIgnore
    public void Q(int i2) {
        R(g0(i2, true), true);
    }

    @DexIgnore
    public void R(t tVar, boolean z2) {
        ViewGroup viewGroup;
        bh0 bh0;
        if (!z2 || tVar.f4110a != 0 || (bh0 = this.m) == null || !bh0.b()) {
            WindowManager windowManager = (WindowManager) this.f.getSystemService("window");
            if (!(windowManager == null || !tVar.o || (viewGroup = tVar.g) == null)) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    N(tVar.f4110a, tVar, null);
                }
            }
            tVar.m = false;
            tVar.n = false;
            tVar.o = false;
            tVar.h = null;
            tVar.q = true;
            if (this.N == tVar) {
                this.N = null;
                return;
            }
            return;
        }
        O(tVar.j);
    }

    @DexIgnore
    public final Configuration S(Context context, int i2, Configuration configuration) {
        int i3 = i2 != 1 ? i2 != 2 ? context.getApplicationContext().getResources().getConfiguration().uiMode & 48 : 32 : 16;
        Configuration configuration2 = new Configuration();
        configuration2.fontScale = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (configuration != null) {
            configuration2.setTo(configuration);
        }
        configuration2.uiMode = i3 | (configuration2.uiMode & -49);
        return configuration2;
    }

    @DexIgnore
    public final ViewGroup T() {
        ViewGroup viewGroup;
        TypedArray obtainStyledAttributes = this.f.obtainStyledAttributes(ue0.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(ue0.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(ue0.AppCompatTheme_windowNoTitle, false)) {
                A(1);
            } else if (obtainStyledAttributes.getBoolean(ue0.AppCompatTheme_windowActionBar, false)) {
                A(108);
            }
            if (obtainStyledAttributes.getBoolean(ue0.AppCompatTheme_windowActionBarOverlay, false)) {
                A(109);
            }
            if (obtainStyledAttributes.getBoolean(ue0.AppCompatTheme_windowActionModeOverlay, false)) {
                A(10);
            }
            this.J = obtainStyledAttributes.getBoolean(ue0.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            a0();
            this.g.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.f);
            if (this.K) {
                viewGroup = this.I ? (ViewGroup) from.inflate(re0.abc_screen_simple_overlay_action_mode, (ViewGroup) null) : (ViewGroup) from.inflate(re0.abc_screen_simple, (ViewGroup) null);
            } else if (this.J) {
                this.H = false;
                this.G = false;
                viewGroup = (ViewGroup) from.inflate(re0.abc_dialog_title_material, (ViewGroup) null);
            } else if (this.G) {
                TypedValue typedValue = new TypedValue();
                this.f.getTheme().resolveAttribute(le0.actionBarTheme, typedValue, true);
                ViewGroup viewGroup2 = (ViewGroup) LayoutInflater.from(typedValue.resourceId != 0 ? new qf0(this.f, typedValue.resourceId) : this.f).inflate(re0.abc_screen_toolbar, (ViewGroup) null);
                bh0 bh0 = (bh0) viewGroup2.findViewById(qe0.decor_content_parent);
                this.m = bh0;
                bh0.setWindowCallback(i0());
                if (this.H) {
                    this.m.h(109);
                }
                if (this.E) {
                    this.m.h(2);
                }
                if (this.F) {
                    this.m.h(5);
                    viewGroup = viewGroup2;
                } else {
                    viewGroup = viewGroup2;
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (Build.VERSION.SDK_INT >= 21) {
                    mo0.z0(viewGroup, new c());
                } else if (viewGroup instanceof fh0) {
                    ((fh0) viewGroup).setOnFitSystemWindowsListener(new d());
                }
                if (this.m == null) {
                    this.C = (TextView) viewGroup.findViewById(qe0.title);
                }
                zh0.c(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(qe0.action_bar_activity_content);
                ViewGroup viewGroup3 = (ViewGroup) this.g.findViewById(16908290);
                if (viewGroup3 != null) {
                    while (viewGroup3.getChildCount() > 0) {
                        View childAt = viewGroup3.getChildAt(0);
                        viewGroup3.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup3.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup3 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup3).setForeground(null);
                    }
                }
                this.g.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new e());
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.G + ", windowActionBarOverlay: " + this.H + ", android:windowIsFloating: " + this.J + ", windowActionModeOverlay: " + this.I + ", windowNoTitle: " + this.K + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    @DexIgnore
    public View U(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        if (this.f0 == null) {
            String string = this.f.obtainStyledAttributes(ue0.AppCompatTheme).getString(ue0.AppCompatTheme_viewInflaterClass);
            if (string == null) {
                this.f0 = new af0();
            } else {
                try {
                    this.f0 = (af0) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.f0 = new af0();
                }
            }
        }
        if (h0) {
            z2 = attributeSet instanceof XmlPullParser ? ((XmlPullParser) attributeSet).getDepth() > 1 : H0((ViewParent) view);
        } else {
            z2 = false;
        }
        return this.f0.createView(view, str, context, attributeSet, z2, h0, true, yh0.b());
    }

    @DexIgnore
    public void V() {
        cg0 cg0;
        bh0 bh0 = this.m;
        if (bh0 != null) {
            bh0.i();
        }
        if (this.w != null) {
            this.g.getDecorView().removeCallbacks(this.x);
            if (this.w.isShowing()) {
                try {
                    this.w.dismiss();
                } catch (IllegalArgumentException e2) {
                }
            }
            this.w = null;
        }
        Y();
        t g02 = g0(0, false);
        if (g02 != null && (cg0 = g02.j) != null) {
            cg0.close();
        }
    }

    @DexIgnore
    public boolean W(KeyEvent keyEvent) {
        View decorView;
        boolean z2 = true;
        Object obj = this.e;
        if (((obj instanceof xn0.a) || (obj instanceof ye0)) && (decorView = this.g.getDecorView()) != null && xn0.d(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.h.a().dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? s0(keyCode, keyEvent) : v0(keyCode, keyEvent);
    }

    @DexIgnore
    public void X(int i2) {
        t g02;
        t g03 = g0(i2, true);
        if (g03.j != null) {
            Bundle bundle = new Bundle();
            g03.j.T(bundle);
            if (bundle.size() > 0) {
                g03.s = bundle;
            }
            g03.j.h0();
            g03.j.clear();
        }
        g03.r = true;
        g03.q = true;
        if ((i2 == 108 || i2 == 0) && this.m != null && (g02 = g0(0, false)) != null) {
            g02.m = false;
            D0(g02, null);
        }
    }

    @DexIgnore
    public void Y() {
        ro0 ro0 = this.y;
        if (ro0 != null) {
            ro0.b();
        }
    }

    @DexIgnore
    public final void Z() {
        if (!this.A) {
            this.B = T();
            CharSequence h02 = h0();
            if (!TextUtils.isEmpty(h02)) {
                bh0 bh0 = this.m;
                if (bh0 != null) {
                    bh0.setWindowTitle(h02);
                } else if (B0() != null) {
                    B0().w(h02);
                } else {
                    TextView textView = this.C;
                    if (textView != null) {
                        textView.setText(h02);
                    }
                }
            }
            K();
            z0(this.B);
            this.A = true;
            t g02 = g0(0, false);
            if (this.S) {
                return;
            }
            if (g02 == null || g02.j == null) {
                n0(108);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cg0.a
    public boolean a(cg0 cg0, MenuItem menuItem) {
        t b02;
        Window.Callback i02 = i0();
        if (i02 == null || this.S || (b02 = b0(cg0.F())) == null) {
            return false;
        }
        return i02.onMenuItemSelected(b02.f4110a, menuItem);
    }

    @DexIgnore
    public final void a0() {
        if (this.g == null) {
            Object obj = this.e;
            if (obj instanceof Activity) {
                L(((Activity) obj).getWindow());
            }
        }
        if (this.g == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    @DexIgnore
    @Override // com.fossil.cg0.a
    public void b(cg0 cg0) {
        E0(true);
    }

    @DexIgnore
    public t b0(Menu menu) {
        int i2;
        int i3;
        t[] tVarArr = this.M;
        if (tVarArr != null) {
            i3 = tVarArr.length;
            i2 = 0;
        } else {
            i3 = 0;
            i2 = 0;
        }
        while (i2 < i3) {
            t tVar = tVarArr[i2];
            if (tVar != null && tVar.j == menu) {
                return tVar;
            }
            i2++;
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void d(View view, ViewGroup.LayoutParams layoutParams) {
        Z();
        ((ViewGroup) this.B.findViewById(16908290)).addView(view, layoutParams);
        this.h.a().onContentChanged();
    }

    @DexIgnore
    public final Context d0() {
        ActionBar n2 = n();
        Context k2 = n2 != null ? n2.k() : null;
        return k2 == null ? this.f : k2;
    }

    @DexIgnore
    public final m e0(Context context) {
        if (this.Y == null) {
            this.Y = new l(context);
        }
        return this.Y;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public Context f(Context context) {
        boolean z2;
        this.P = true;
        int q0 = q0(context, M());
        if (k0 && (context instanceof ContextThemeWrapper)) {
            try {
                r.a((ContextThemeWrapper) context, S(context, q0, null));
                return context;
            } catch (IllegalStateException e2) {
            }
        }
        if (context instanceof qf0) {
            try {
                ((qf0) context).a(S(context, q0, null));
                return context;
            } catch (IllegalStateException e3) {
            }
        }
        if (!j0) {
            super.f(context);
            return context;
        }
        try {
            Configuration configuration = context.getPackageManager().getResourcesForApplication(context.getApplicationInfo()).getConfiguration();
            Configuration configuration2 = context.getResources().getConfiguration();
            Configuration S2 = S(context, q0, !configuration.equals(configuration2) ? c0(configuration, configuration2) : null);
            qf0 qf0 = new qf0(context, te0.Theme_AppCompat_Empty);
            qf0.a(S2);
            try {
                z2 = context.getTheme() != null;
            } catch (NullPointerException e4) {
                z2 = false;
            }
            if (z2) {
                nl0.b.a(qf0.getTheme());
            }
            super.f(qf0);
            return qf0;
        } catch (PackageManager.NameNotFoundException e5) {
            throw new RuntimeException("Application failed to obtain resources from itself", e5);
        }
    }

    @DexIgnore
    public final m f0(Context context) {
        if (this.X == null) {
            this.X = new n(ef0.a(context));
        }
        return this.X;
    }

    @DexIgnore
    public t g0(int i2, boolean z2) {
        t[] tVarArr = this.M;
        if (tVarArr == null || tVarArr.length <= i2) {
            t[] tVarArr2 = new t[(i2 + 1)];
            if (tVarArr != null) {
                System.arraycopy(tVarArr, 0, tVarArr2, 0, tVarArr.length);
            }
            this.M = tVarArr2;
            tVarArr = tVarArr2;
        }
        t tVar = tVarArr[i2];
        if (tVar != null) {
            return tVar;
        }
        t tVar2 = new t(i2);
        tVarArr[i2] = tVar2;
        return tVar2;
    }

    @DexIgnore
    public final CharSequence h0() {
        Object obj = this.e;
        return obj instanceof Activity ? ((Activity) obj).getTitle() : this.l;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public <T extends View> T i(int i2) {
        Z();
        return (T) this.g.findViewById(i2);
    }

    @DexIgnore
    public final Window.Callback i0() {
        return this.g.getCallback();
    }

    @DexIgnore
    public final void j0() {
        Z();
        if (this.G && this.j == null) {
            Object obj = this.e;
            if (obj instanceof Activity) {
                this.j = new ff0((Activity) this.e, this.H);
            } else if (obj instanceof Dialog) {
                this.j = new ff0((Dialog) this.e);
            }
            ActionBar actionBar = this.j;
            if (actionBar != null) {
                actionBar.r(this.c0);
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public final ActionBarDrawerToggle$Delegate k() {
        return new h(this);
    }

    @DexIgnore
    public final boolean k0(t tVar) {
        View view = tVar.i;
        if (view != null) {
            tVar.h = view;
            return true;
        } else if (tVar.j == null) {
            return false;
        } else {
            if (this.t == null) {
                this.t = new u();
            }
            View view2 = (View) tVar.a(this.t);
            tVar.h = view2;
            return view2 != null;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public int l() {
        return this.T;
    }

    @DexIgnore
    public final boolean l0(t tVar) {
        tVar.d(d0());
        tVar.g = new s(tVar.l);
        tVar.c = 81;
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public MenuInflater m() {
        if (this.k == null) {
            j0();
            ActionBar actionBar = this.j;
            this.k = new tf0(actionBar != null ? actionBar.k() : this.f);
        }
        return this.k;
    }

    @DexIgnore
    public final boolean m0(t tVar) {
        qf0 qf0;
        Context context = this.f;
        int i2 = tVar.f4110a;
        if ((i2 == 0 || i2 == 108) && this.m != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(le0.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(le0.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(le0.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                qf0 qf02 = new qf0(context, 0);
                qf02.getTheme().setTo(theme2);
                qf0 = qf02;
                cg0 cg0 = new cg0(qf0);
                cg0.V(this);
                tVar.c(cg0);
                return true;
            }
        }
        qf0 = context;
        cg0 cg02 = new cg0(qf0);
        cg02.V(this);
        tVar.c(cg02);
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public ActionBar n() {
        j0();
        return this.j;
    }

    @DexIgnore
    public final void n0(int i2) {
        this.a0 = (1 << i2) | this.a0;
        if (!this.Z) {
            mo0.d0(this.g.getDecorView(), this.b0);
            this.Z = true;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void o() {
        LayoutInflater from = LayoutInflater.from(this.f);
        if (from.getFactory() == null) {
            yn0.b(from, this);
        } else if (!(from.getFactory2() instanceof xe0)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    @DexIgnore
    public final boolean o0() {
        if (!this.W && (this.e instanceof Activity)) {
            PackageManager packageManager = this.f.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            try {
                ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(this.f, this.e.getClass()), Build.VERSION.SDK_INT >= 29 ? 269221888 : Build.VERSION.SDK_INT >= 24 ? 786432 : 0);
                this.V = (activityInfo == null || (activityInfo.configChanges & 512) == 0) ? false : true;
            } catch (PackageManager.NameNotFoundException e2) {
                Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
                this.V = false;
            }
        }
        this.W = true;
        return this.V;
    }

    @DexIgnore
    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return U(view, str, context, attributeSet);
    }

    @DexIgnore
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void p() {
        ActionBar n2 = n();
        if (n2 == null || !n2.l()) {
            n0(0);
        }
    }

    @DexIgnore
    public boolean p0() {
        return this.z;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void q(Configuration configuration) {
        ActionBar n2;
        if (this.G && this.A && (n2 = n()) != null) {
            n2.m(configuration);
        }
        ug0.b().g(this.f);
        J(false);
    }

    @DexIgnore
    public int q0(Context context, int i2) {
        if (i2 == -100) {
            return -1;
        }
        if (i2 == -1) {
            return i2;
        }
        if (i2 != 0) {
            if (i2 == 1 || i2 == 2) {
                return i2;
            }
            if (i2 == 3) {
                return e0(context).c();
            }
            throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
        } else if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) context.getApplicationContext().getSystemService(UiModeManager.class)).getNightMode() != 0) {
            return f0(context).c();
        } else {
            return -1;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void r(Bundle bundle) {
        String str;
        this.P = true;
        J(false);
        a0();
        Object obj = this.e;
        if (obj instanceof Activity) {
            try {
                str = xk0.c((Activity) obj);
            } catch (IllegalArgumentException e2) {
                str = null;
            }
            if (str != null) {
                ActionBar B0 = B0();
                if (B0 == null) {
                    this.c0 = true;
                } else {
                    B0.r(true);
                }
            }
        }
        AppCompatDelegate.c(this);
        this.Q = true;
    }

    @DexIgnore
    public boolean r0() {
        androidx.appcompat.view.ActionMode actionMode = this.u;
        if (actionMode != null) {
            actionMode.c();
            return true;
        }
        ActionBar n2 = n();
        return n2 != null && n2.h();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    @Override // androidx.appcompat.app.AppCompatDelegate
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void s() {
        /*
            r3 = this;
            androidx.appcompat.app.AppCompatDelegate.y(r3)
            boolean r0 = r3.Z
            if (r0 == 0) goto L_0x0012
            android.view.Window r0 = r3.g
            android.view.View r0 = r0.getDecorView()
            java.lang.Runnable r1 = r3.b0
            r0.removeCallbacks(r1)
        L_0x0012:
            r0 = 0
            r3.R = r0
            r0 = 1
            r3.S = r0
            int r0 = r3.T
            r1 = -100
            if (r0 == r1) goto L_0x004c
            java.lang.Object r0 = r3.e
            boolean r1 = r0 instanceof android.app.Activity
            if (r1 == 0) goto L_0x004c
            android.app.Activity r0 = (android.app.Activity) r0
            boolean r0 = r0.isChangingConfigurations()
            if (r0 == 0) goto L_0x004c
            androidx.collection.SimpleArrayMap<java.lang.String, java.lang.Integer> r0 = com.fossil.xe0.g0
            java.lang.Object r1 = r3.e
            java.lang.Class r1 = r1.getClass()
            java.lang.String r1 = r1.getName()
            int r2 = r3.T
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0.put(r1, r2)
        L_0x0041:
            androidx.appcompat.app.ActionBar r0 = r3.j
            if (r0 == 0) goto L_0x0048
            r0.n()
        L_0x0048:
            r3.P()
            return
        L_0x004c:
            androidx.collection.SimpleArrayMap<java.lang.String, java.lang.Integer> r0 = com.fossil.xe0.g0
            java.lang.Object r1 = r3.e
            java.lang.Class r1 = r1.getClass()
            java.lang.String r1 = r1.getName()
            r0.remove(r1)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xe0.s():void");
    }

    @DexIgnore
    public boolean s0(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        if (i2 == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z2 = false;
            }
            this.O = z2;
        } else if (i2 == 82) {
            t0(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void t(Bundle bundle) {
        Z();
    }

    @DexIgnore
    public final boolean t0(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            t g02 = g0(i2, true);
            if (!g02.o) {
                return D0(g02, keyEvent);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void u() {
        ActionBar n2 = n();
        if (n2 != null) {
            n2.u(true);
        }
    }

    @DexIgnore
    public boolean u0(int i2, KeyEvent keyEvent) {
        ActionBar n2 = n();
        if (n2 != null && n2.o(i2, keyEvent)) {
            return true;
        }
        t tVar = this.N;
        if (tVar == null || !C0(tVar, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.N == null) {
                t g02 = g0(0, true);
                D0(g02, keyEvent);
                boolean C0 = C0(g02, keyEvent.getKeyCode(), keyEvent, 1);
                g02.m = false;
                if (C0) {
                    return true;
                }
            }
            return false;
        }
        t tVar2 = this.N;
        if (tVar2 == null) {
            return true;
        }
        tVar2.n = true;
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void v(Bundle bundle) {
    }

    @DexIgnore
    public boolean v0(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z2 = this.O;
            this.O = false;
            t g02 = g0(0, false);
            if (g02 == null || !g02.o) {
                if (r0()) {
                    return true;
                }
            } else if (z2) {
                return true;
            } else {
                R(g02, true);
                return true;
            }
        } else if (i2 == 82) {
            w0(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void w() {
        this.R = true;
        I();
    }

    @DexIgnore
    public final boolean w0(int i2, KeyEvent keyEvent) {
        boolean z2;
        bh0 bh0;
        boolean z3 = true;
        if (this.u != null) {
            return false;
        }
        t g02 = g0(i2, true);
        if (i2 != 0 || (bh0 = this.m) == null || !bh0.d() || ViewConfiguration.get(this.f).hasPermanentMenuKey()) {
            if (g02.o || g02.n) {
                boolean z4 = g02.o;
                R(g02, true);
                z3 = z4;
            } else {
                if (g02.m) {
                    if (g02.r) {
                        g02.m = false;
                        z2 = D0(g02, keyEvent);
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        A0(g02, keyEvent);
                    }
                }
                z3 = false;
            }
        } else if (!this.m.b()) {
            if (!this.S && D0(g02, keyEvent)) {
                z3 = this.m.g();
            }
            z3 = false;
        } else {
            z3 = this.m.f();
        }
        if (z3) {
            AudioManager audioManager = (AudioManager) this.f.getApplicationContext().getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z3;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatDelegate
    public void x() {
        this.R = false;
        ActionBar n2 = n();
        if (n2 != null) {
            n2.u(false);
        }
    }

    @DexIgnore
    public void x0(int i2) {
        ActionBar n2;
        if (i2 == 108 && (n2 = n()) != null) {
            n2.i(true);
        }
    }

    @DexIgnore
    public void y0(int i2) {
        if (i2 == 108) {
            ActionBar n2 = n();
            if (n2 != null) {
                n2.i(false);
            }
        } else if (i2 == 0) {
            t g02 = g0(i2, true);
            if (g02.o) {
                R(g02, false);
            }
        }
    }

    @DexIgnore
    public void z0(ViewGroup viewGroup) {
    }
}
