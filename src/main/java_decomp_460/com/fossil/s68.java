package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s68 implements t68 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3211a;

    @DexIgnore
    public s68(String str) {
        if (str != null) {
            this.f3211a = str;
            int indexOf = str.indexOf(47);
            if (indexOf != -1) {
                str.substring(0, indexOf);
                str.substring(indexOf + 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("MIME type may not be null");
    }

    @DexIgnore
    @Override // com.fossil.u68
    public String c() {
        return this.f3211a;
    }
}
