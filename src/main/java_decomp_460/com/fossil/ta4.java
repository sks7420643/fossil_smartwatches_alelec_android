package com.fossil;

import com.fossil.aa4;
import com.fossil.ba4;
import com.fossil.ca4;
import com.fossil.da4;
import com.fossil.ea4;
import com.fossil.ga4;
import com.fossil.ha4;
import com.fossil.ia4;
import com.fossil.ja4;
import com.fossil.ka4;
import com.fossil.la4;
import com.fossil.ma4;
import com.fossil.na4;
import com.fossil.oa4;
import com.fossil.pa4;
import com.fossil.qa4;
import com.fossil.ra4;
import com.fossil.sa4;
import com.fossil.z94;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ta4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Charset f3386a; // = Charset.forName("UTF-8");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract ta4 a();

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(String str);

        @DexIgnore
        public abstract a d(String str);

        @DexIgnore
        public abstract a e(String str);

        @DexIgnore
        public abstract a f(c cVar);

        @DexIgnore
        public abstract a g(int i);

        @DexIgnore
        public abstract a h(String str);

        @DexIgnore
        public abstract a i(d dVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract b a();

            @DexIgnore
            public abstract a b(String str);

            @DexIgnore
            public abstract a c(String str);
        }

        @DexIgnore
        public static a a() {
            return new aa4.b();
        }

        @DexIgnore
        public abstract String b();

        @DexIgnore
        public abstract String c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract c a();

            @DexIgnore
            public abstract a b(ua4<b> ua4);

            @DexIgnore
            public abstract a c(String str);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class b {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract b a();

                @DexIgnore
                public abstract a b(byte[] bArr);

                @DexIgnore
                public abstract a c(String str);
            }

            @DexIgnore
            public static a a() {
                return new ca4.b();
            }

            @DexIgnore
            public abstract byte[] b();

            @DexIgnore
            public abstract String c();
        }

        @DexIgnore
        public static a a() {
            return new ba4.b();
        }

        @DexIgnore
        public abstract ua4<b> b();

        @DexIgnore
        public abstract String c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$a$a")
            /* renamed from: com.fossil.ta4$d$a$a  reason: collision with other inner class name */
            public static abstract class AbstractC0223a {
                @DexIgnore
                public abstract a a();

                @DexIgnore
                public abstract AbstractC0223a b(String str);

                @DexIgnore
                public abstract AbstractC0223a c(String str);

                @DexIgnore
                public abstract AbstractC0223a d(String str);

                @DexIgnore
                public abstract AbstractC0223a e(String str);
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class b {
                @DexIgnore
                public abstract String a();
            }

            @DexIgnore
            public static AbstractC0223a a() {
                return new ea4.b();
            }

            @DexIgnore
            public abstract String b();

            @DexIgnore
            public abstract String c();

            @DexIgnore
            public abstract String d();

            @DexIgnore
            public abstract b e();

            @DexIgnore
            public abstract String f();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class b {
            @DexIgnore
            public abstract d a();

            @DexIgnore
            public abstract b b(a aVar);

            @DexIgnore
            public abstract b c(boolean z);

            @DexIgnore
            public abstract b d(c cVar);

            @DexIgnore
            public abstract b e(Long l);

            @DexIgnore
            public abstract b f(ua4<AbstractC0224d> ua4);

            @DexIgnore
            public abstract b g(String str);

            @DexIgnore
            public abstract b h(int i);

            @DexIgnore
            public abstract b i(String str);

            @DexIgnore
            public b j(byte[] bArr) {
                i(new String(bArr, ta4.f3386a));
                return this;
            }

            @DexIgnore
            public abstract b k(e eVar);

            @DexIgnore
            public abstract b l(long j);

            @DexIgnore
            public abstract b m(f fVar);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class c {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract c a();

                @DexIgnore
                public abstract a b(int i);

                @DexIgnore
                public abstract a c(int i);

                @DexIgnore
                public abstract a d(long j);

                @DexIgnore
                public abstract a e(String str);

                @DexIgnore
                public abstract a f(String str);

                @DexIgnore
                public abstract a g(String str);

                @DexIgnore
                public abstract a h(long j);

                @DexIgnore
                public abstract a i(boolean z);

                @DexIgnore
                public abstract a j(int i);
            }

            @DexIgnore
            public static a a() {
                return new ga4.b();
            }

            @DexIgnore
            public abstract int b();

            @DexIgnore
            public abstract int c();

            @DexIgnore
            public abstract long d();

            @DexIgnore
            public abstract String e();

            @DexIgnore
            public abstract String f();

            @DexIgnore
            public abstract String g();

            @DexIgnore
            public abstract long h();

            @DexIgnore
            public abstract int i();

            @DexIgnore
            public abstract boolean j();
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d")
        /* renamed from: com.fossil.ta4$d$d  reason: collision with other inner class name */
        public static abstract class AbstractC0224d {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a")
            /* renamed from: com.fossil.ta4$d$d$a */
            public static abstract class a {

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$a")
                /* renamed from: com.fossil.ta4$d$d$a$a  reason: collision with other inner class name */
                public static abstract class AbstractC0225a {
                    @DexIgnore
                    public abstract a a();

                    @DexIgnore
                    public abstract AbstractC0225a b(Boolean bool);

                    @DexIgnore
                    public abstract AbstractC0225a c(ua4<b> ua4);

                    @DexIgnore
                    public abstract AbstractC0225a d(b bVar);

                    @DexIgnore
                    public abstract AbstractC0225a e(int i);
                }

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b")
                /* renamed from: com.fossil.ta4$d$d$a$b */
                public static abstract class b {

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$a")
                    /* renamed from: com.fossil.ta4$d$d$a$b$a  reason: collision with other inner class name */
                    public static abstract class AbstractC0226a {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$a$a")
                        /* renamed from: com.fossil.ta4$d$d$a$b$a$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0227a {
                            @DexIgnore
                            public abstract AbstractC0226a a();

                            @DexIgnore
                            public abstract AbstractC0227a b(long j);

                            @DexIgnore
                            public abstract AbstractC0227a c(String str);

                            @DexIgnore
                            public abstract AbstractC0227a d(long j);

                            @DexIgnore
                            public abstract AbstractC0227a e(String str);

                            @DexIgnore
                            public AbstractC0227a f(byte[] bArr) {
                                e(new String(bArr, ta4.f3386a));
                                return this;
                            }
                        }

                        @DexIgnore
                        public static AbstractC0227a a() {
                            return new ka4.b();
                        }

                        @DexIgnore
                        public abstract long b();

                        @DexIgnore
                        public abstract String c();

                        @DexIgnore
                        public abstract long d();

                        @DexIgnore
                        public abstract String e();

                        @DexIgnore
                        public byte[] f() {
                            String e = e();
                            if (e != null) {
                                return e.getBytes(ta4.f3386a);
                            }
                            return null;
                        }
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$b")
                    /* renamed from: com.fossil.ta4$d$d$a$b$b  reason: collision with other inner class name */
                    public static abstract class AbstractC0228b {
                        @DexIgnore
                        public abstract b a();

                        @DexIgnore
                        public abstract AbstractC0228b b(ua4<AbstractC0226a> ua4);

                        @DexIgnore
                        public abstract AbstractC0228b c(c cVar);

                        @DexIgnore
                        public abstract AbstractC0228b d(AbstractC0230d dVar);

                        @DexIgnore
                        public abstract AbstractC0228b e(ua4<e> ua4);
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$c")
                    /* renamed from: com.fossil.ta4$d$d$a$b$c */
                    public static abstract class c {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$c$a")
                        /* renamed from: com.fossil.ta4$d$d$a$b$c$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0229a {
                            @DexIgnore
                            public abstract c a();

                            @DexIgnore
                            public abstract AbstractC0229a b(c cVar);

                            @DexIgnore
                            public abstract AbstractC0229a c(ua4<e.AbstractC0233b> ua4);

                            @DexIgnore
                            public abstract AbstractC0229a d(int i);

                            @DexIgnore
                            public abstract AbstractC0229a e(String str);

                            @DexIgnore
                            public abstract AbstractC0229a f(String str);
                        }

                        @DexIgnore
                        public static AbstractC0229a a() {
                            return new la4.b();
                        }

                        @DexIgnore
                        public abstract c b();

                        @DexIgnore
                        public abstract ua4<e.AbstractC0233b> c();

                        @DexIgnore
                        public abstract int d();

                        @DexIgnore
                        public abstract String e();

                        @DexIgnore
                        public abstract String f();
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$d")
                    /* renamed from: com.fossil.ta4$d$d$a$b$d  reason: collision with other inner class name */
                    public static abstract class AbstractC0230d {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$d$a")
                        /* renamed from: com.fossil.ta4$d$d$a$b$d$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0231a {
                            @DexIgnore
                            public abstract AbstractC0230d a();

                            @DexIgnore
                            public abstract AbstractC0231a b(long j);

                            @DexIgnore
                            public abstract AbstractC0231a c(String str);

                            @DexIgnore
                            public abstract AbstractC0231a d(String str);
                        }

                        @DexIgnore
                        public static AbstractC0231a a() {
                            return new ma4.b();
                        }

                        @DexIgnore
                        public abstract long b();

                        @DexIgnore
                        public abstract String c();

                        @DexIgnore
                        public abstract String d();
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$e")
                    /* renamed from: com.fossil.ta4$d$d$a$b$e */
                    public static abstract class e {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$e$a")
                        /* renamed from: com.fossil.ta4$d$d$a$b$e$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0232a {
                            @DexIgnore
                            public abstract e a();

                            @DexIgnore
                            public abstract AbstractC0232a b(ua4<AbstractC0233b> ua4);

                            @DexIgnore
                            public abstract AbstractC0232a c(int i);

                            @DexIgnore
                            public abstract AbstractC0232a d(String str);
                        }

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$e$b")
                        /* renamed from: com.fossil.ta4$d$d$a$b$e$b  reason: collision with other inner class name */
                        public static abstract class AbstractC0233b {

                            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$a$b$e$b$a")
                            /* renamed from: com.fossil.ta4$d$d$a$b$e$b$a  reason: collision with other inner class name */
                            public static abstract class AbstractC0234a {
                                @DexIgnore
                                public abstract AbstractC0233b a();

                                @DexIgnore
                                public abstract AbstractC0234a b(String str);

                                @DexIgnore
                                public abstract AbstractC0234a c(int i);

                                @DexIgnore
                                public abstract AbstractC0234a d(long j);

                                @DexIgnore
                                public abstract AbstractC0234a e(long j);

                                @DexIgnore
                                public abstract AbstractC0234a f(String str);
                            }

                            @DexIgnore
                            public static AbstractC0234a a() {
                                return new oa4.b();
                            }

                            @DexIgnore
                            public abstract String b();

                            @DexIgnore
                            public abstract int c();

                            @DexIgnore
                            public abstract long d();

                            @DexIgnore
                            public abstract long e();

                            @DexIgnore
                            public abstract String f();
                        }

                        @DexIgnore
                        public static AbstractC0232a a() {
                            return new na4.b();
                        }

                        @DexIgnore
                        public abstract ua4<AbstractC0233b> b();

                        @DexIgnore
                        public abstract int c();

                        @DexIgnore
                        public abstract String d();
                    }

                    @DexIgnore
                    public static AbstractC0228b a() {
                        return new ja4.b();
                    }

                    @DexIgnore
                    public abstract ua4<AbstractC0226a> b();

                    @DexIgnore
                    public abstract c c();

                    @DexIgnore
                    public abstract AbstractC0230d d();

                    @DexIgnore
                    public abstract ua4<e> e();
                }

                @DexIgnore
                public static AbstractC0225a a() {
                    return new ia4.b();
                }

                @DexIgnore
                public abstract Boolean b();

                @DexIgnore
                public abstract ua4<b> c();

                @DexIgnore
                public abstract b d();

                @DexIgnore
                public abstract int e();

                @DexIgnore
                public abstract AbstractC0225a f();
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$b")
            /* renamed from: com.fossil.ta4$d$d$b */
            public static abstract class b {
                @DexIgnore
                public abstract AbstractC0224d a();

                @DexIgnore
                public abstract b b(a aVar);

                @DexIgnore
                public abstract b c(c cVar);

                @DexIgnore
                public abstract b d(AbstractC0235d dVar);

                @DexIgnore
                public abstract b e(long j);

                @DexIgnore
                public abstract b f(String str);
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$c")
            /* renamed from: com.fossil.ta4$d$d$c */
            public static abstract class c {

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$c$a")
                /* renamed from: com.fossil.ta4$d$d$c$a */
                public static abstract class a {
                    @DexIgnore
                    public abstract c a();

                    @DexIgnore
                    public abstract a b(Double d);

                    @DexIgnore
                    public abstract a c(int i);

                    @DexIgnore
                    public abstract a d(long j);

                    @DexIgnore
                    public abstract a e(int i);

                    @DexIgnore
                    public abstract a f(boolean z);

                    @DexIgnore
                    public abstract a g(long j);
                }

                @DexIgnore
                public static a a() {
                    return new pa4.b();
                }

                @DexIgnore
                public abstract Double b();

                @DexIgnore
                public abstract int c();

                @DexIgnore
                public abstract long d();

                @DexIgnore
                public abstract int e();

                @DexIgnore
                public abstract long f();

                @DexIgnore
                public abstract boolean g();
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$d")
            /* renamed from: com.fossil.ta4$d$d$d  reason: collision with other inner class name */
            public static abstract class AbstractC0235d {

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ta4$d$d$d$a")
                /* renamed from: com.fossil.ta4$d$d$d$a */
                public static abstract class a {
                    @DexIgnore
                    public abstract AbstractC0235d a();

                    @DexIgnore
                    public abstract a b(String str);
                }

                @DexIgnore
                public static a a() {
                    return new qa4.b();
                }

                @DexIgnore
                public abstract String b();
            }

            @DexIgnore
            public static b a() {
                return new ha4.b();
            }

            @DexIgnore
            public abstract a b();

            @DexIgnore
            public abstract c c();

            @DexIgnore
            public abstract AbstractC0235d d();

            @DexIgnore
            public abstract long e();

            @DexIgnore
            public abstract String f();

            @DexIgnore
            public abstract b g();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class e {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract e a();

                @DexIgnore
                public abstract a b(String str);

                @DexIgnore
                public abstract a c(boolean z);

                @DexIgnore
                public abstract a d(int i);

                @DexIgnore
                public abstract a e(String str);
            }

            @DexIgnore
            public static a a() {
                return new ra4.b();
            }

            @DexIgnore
            public abstract String b();

            @DexIgnore
            public abstract int c();

            @DexIgnore
            public abstract String d();

            @DexIgnore
            public abstract boolean e();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class f {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract f a();

                @DexIgnore
                public abstract a b(String str);
            }

            @DexIgnore
            public static a a() {
                return new sa4.b();
            }

            @DexIgnore
            public abstract String b();
        }

        @DexIgnore
        public static b a() {
            da4.b bVar = new da4.b();
            bVar.c(false);
            return bVar;
        }

        @DexIgnore
        public abstract a b();

        @DexIgnore
        public abstract c c();

        @DexIgnore
        public abstract Long d();

        @DexIgnore
        public abstract ua4<AbstractC0224d> e();

        @DexIgnore
        public abstract String f();

        @DexIgnore
        public abstract int g();

        @DexIgnore
        public abstract String h();

        @DexIgnore
        public byte[] i() {
            return h().getBytes(ta4.f3386a);
        }

        @DexIgnore
        public abstract e j();

        @DexIgnore
        public abstract long k();

        @DexIgnore
        public abstract f l();

        @DexIgnore
        public abstract boolean m();

        @DexIgnore
        public abstract b n();

        @DexIgnore
        public d o(ua4<AbstractC0224d> ua4) {
            b n = n();
            n.f(ua4);
            return n.a();
        }

        @DexIgnore
        public d p(long j, boolean z, String str) {
            b n = n();
            n.e(Long.valueOf(j));
            n.c(z);
            if (str != null) {
                f.a a2 = f.a();
                a2.b(str);
                n.m(a2.a());
                n.a();
            }
            return n.a();
        }
    }

    @DexIgnore
    public enum e {
        INCOMPLETE,
        JAVA,
        NATIVE
    }

    @DexIgnore
    public static a b() {
        return new z94.b();
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract c g();

    @DexIgnore
    public abstract int h();

    @DexIgnore
    public abstract String i();

    @DexIgnore
    public abstract d j();

    @DexIgnore
    public e k() {
        return j() != null ? e.JAVA : g() != null ? e.NATIVE : e.INCOMPLETE;
    }

    @DexIgnore
    public abstract a l();

    @DexIgnore
    public ta4 m(ua4<d.AbstractC0224d> ua4) {
        if (j() != null) {
            a l = l();
            l.i(j().o(ua4));
            return l.a();
        }
        throw new IllegalStateException("Reports without sessions cannot have events added to them.");
    }

    @DexIgnore
    public ta4 n(c cVar) {
        a l = l();
        l.i(null);
        l.f(cVar);
        return l.a();
    }

    @DexIgnore
    public ta4 o(long j, boolean z, String str) {
        a l = l();
        if (j() != null) {
            l.i(j().p(j, z, str));
        }
        return l.a();
    }
}
