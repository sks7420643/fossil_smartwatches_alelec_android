package com.fossil;

import android.util.Log;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bq0 extends xp0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Set<Class<? extends xp0>> f482a; // = new HashSet();
    @DexIgnore
    public List<xp0> b; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<String> c; // = new CopyOnWriteArrayList();

    @DexIgnore
    @Override // com.fossil.xp0
    public ViewDataBinding b(zp0 zp0, View view, int i) {
        for (xp0 xp0 : this.b) {
            ViewDataBinding b2 = xp0.b(zp0, view, i);
            if (b2 != null) {
                return b2;
            }
        }
        if (e()) {
            return b(zp0, view, i);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public ViewDataBinding c(zp0 zp0, View[] viewArr, int i) {
        for (xp0 xp0 : this.b) {
            ViewDataBinding c2 = xp0.c(zp0, viewArr, i);
            if (c2 != null) {
                return c2;
            }
        }
        if (e()) {
            return c(zp0, viewArr, i);
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.Set<java.lang.Class<? extends com.fossil.xp0>> */
    /* JADX WARN: Multi-variable type inference failed */
    public void d(xp0 xp0) {
        if (this.f482a.add(xp0.getClass())) {
            this.b.add(xp0);
            for (xp0 xp02 : xp0.a()) {
                d(xp02);
            }
        }
    }

    @DexIgnore
    public final boolean e() {
        boolean z = false;
        for (String str : this.c) {
            try {
                Class<?> cls = Class.forName(str);
                if (xp0.class.isAssignableFrom(cls)) {
                    d((xp0) cls.newInstance());
                    this.c.remove(str);
                    z = true;
                }
            } catch (ClassNotFoundException e) {
            } catch (IllegalAccessException e2) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + str, e2);
            } catch (InstantiationException e3) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + str, e3);
            }
        }
        return z;
    }
}
