package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sz3 extends Drawable implements j04, bm0 {
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public c04 f3340a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public b(c04 c04) {
            this.f3340a = c04;
            this.b = false;
        }

        @DexIgnore
        public b(b bVar) {
            this.f3340a = (c04) bVar.f3340a.getConstantState().newDrawable();
            this.b = bVar.b;
        }

        @DexIgnore
        /* renamed from: a */
        public sz3 newDrawable() {
            return new sz3(new b(this));
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }
    }

    @DexIgnore
    public sz3(g04 g04) {
        this(new b(new c04(g04)));
    }

    @DexIgnore
    public sz3(b bVar) {
        this.b = bVar;
    }

    @DexIgnore
    public sz3 a() {
        this.b = new b(this.b);
        return this;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        b bVar = this.b;
        if (bVar.b) {
            bVar.f3340a.draw(canvas);
        }
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    @DexIgnore
    public int getOpacity() {
        return this.b.f3340a.getOpacity();
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Drawable mutate() {
        a();
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.b.f3340a.setBounds(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        if (this.b.f3340a.setState(iArr)) {
            onStateChange = true;
        }
        boolean e = tz3.e(iArr);
        b bVar = this.b;
        if (bVar.b == e) {
            return onStateChange;
        }
        bVar.b = e;
        return true;
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.b.f3340a.setAlpha(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.f3340a.setColorFilter(colorFilter);
    }

    @DexIgnore
    @Override // com.fossil.j04
    public void setShapeAppearanceModel(g04 g04) {
        this.b.f3340a.setShapeAppearanceModel(g04);
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTint(int i) {
        this.b.f3340a.setTint(i);
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintList(ColorStateList colorStateList) {
        this.b.f3340a.setTintList(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.bm0
    public void setTintMode(PorterDuff.Mode mode) {
        this.b.f3340a.setTintMode(mode);
    }
}
