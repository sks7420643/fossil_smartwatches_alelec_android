package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t56 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(r56 r56) {
        LoaderManager c = r56.c();
        lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
