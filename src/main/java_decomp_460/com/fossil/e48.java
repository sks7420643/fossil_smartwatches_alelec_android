package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e48 {
    @DexIgnore
    public static final byte[] a(String str) {
        pq7.c(str, "$this$asUtf8ToByteArray");
        byte[] bytes = str.getBytes(et7.f986a);
        pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }

    @DexIgnore
    public static final String b(byte[] bArr) {
        pq7.c(bArr, "$this$toUtf8String");
        return new String(bArr, et7.f986a);
    }
}
