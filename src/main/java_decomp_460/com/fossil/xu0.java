package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xu0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b f4176a;
    @DexIgnore
    public /* final */ a b; // = new a();
    @DexIgnore
    public /* final */ List<View> c; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public long f4177a; // = 0;
        @DexIgnore
        public a b;

        @DexIgnore
        public void a(int i) {
            if (i >= 64) {
                a aVar = this.b;
                if (aVar != null) {
                    aVar.a(i - 64);
                    return;
                }
                return;
            }
            this.f4177a &= 1 << i;
        }

        @DexIgnore
        public int b(int i) {
            a aVar = this.b;
            return aVar == null ? i >= 64 ? Long.bitCount(this.f4177a) : Long.bitCount(this.f4177a & ((1 << i) - 1)) : i < 64 ? Long.bitCount(this.f4177a & ((1 << i) - 1)) : aVar.b(i - 64) + Long.bitCount(this.f4177a);
        }

        @DexIgnore
        public final void c() {
            if (this.b == null) {
                this.b = new a();
            }
        }

        @DexIgnore
        public boolean d(int i) {
            if (i < 64) {
                return (this.f4177a & (1 << i)) != 0;
            }
            c();
            return this.b.d(i - 64);
        }

        @DexIgnore
        public void e(int i, boolean z) {
            if (i >= 64) {
                c();
                this.b.e(i - 64, z);
                return;
            }
            boolean z2 = (this.f4177a & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            long j2 = this.f4177a;
            this.f4177a = (j & j2) | ((j2 & j) << 1);
            if (z) {
                h(i);
            } else {
                a(i);
            }
            if (z2 || this.b != null) {
                c();
                this.b.e(0, z2);
            }
        }

        @DexIgnore
        public boolean f(int i) {
            if (i >= 64) {
                c();
                return this.b.f(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f4177a & j) != 0;
            long j2 = this.f4177a & j;
            this.f4177a = j2;
            long j3 = j - 1;
            this.f4177a = Long.rotateRight(j3 & j2, 1) | (j2 & j3);
            a aVar = this.b;
            if (aVar == null) {
                return z;
            }
            if (aVar.d(0)) {
                h(63);
            }
            this.b.f(0);
            return z;
        }

        @DexIgnore
        public void g() {
            this.f4177a = 0;
            a aVar = this.b;
            if (aVar != null) {
                aVar.g();
            }
        }

        @DexIgnore
        public void h(int i) {
            if (i >= 64) {
                c();
                this.b.h(i - 64);
                return;
            }
            this.f4177a |= 1 << i;
        }

        @DexIgnore
        public String toString() {
            if (this.b == null) {
                return Long.toBinaryString(this.f4177a);
            }
            return this.b.toString() + "xx" + Long.toBinaryString(this.f4177a);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        View a(int i);

        @DexIgnore
        void b(View view);

        @DexIgnore
        int c();

        @DexIgnore
        Object d();  // void declaration

        @DexIgnore
        int e(View view);

        @DexIgnore
        RecyclerView.ViewHolder f(View view);

        @DexIgnore
        void g(int i);

        @DexIgnore
        void h(View view);

        @DexIgnore
        void i(View view, int i);

        @DexIgnore
        void j(int i);

        @DexIgnore
        void k(View view, int i, ViewGroup.LayoutParams layoutParams);
    }

    @DexIgnore
    public xu0(b bVar) {
        this.f4176a = bVar;
    }

    @DexIgnore
    public void a(View view, int i, boolean z) {
        int c2 = i < 0 ? this.f4176a.c() : h(i);
        this.b.e(c2, z);
        if (z) {
            l(view);
        }
        this.f4176a.i(view, c2);
    }

    @DexIgnore
    public void b(View view, boolean z) {
        a(view, -1, z);
    }

    @DexIgnore
    public void c(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int c2 = i < 0 ? this.f4176a.c() : h(i);
        this.b.e(c2, z);
        if (z) {
            l(view);
        }
        this.f4176a.k(view, c2, layoutParams);
    }

    @DexIgnore
    public void d(int i) {
        int h = h(i);
        this.b.f(h);
        this.f4176a.g(h);
    }

    @DexIgnore
    public View e(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = this.c.get(i2);
            RecyclerView.ViewHolder f = this.f4176a.f(view);
            if (!(f.getLayoutPosition() != i || f.isInvalid() || f.isRemoved())) {
                return view;
            }
        }
        return null;
    }

    @DexIgnore
    public View f(int i) {
        return this.f4176a.a(h(i));
    }

    @DexIgnore
    public int g() {
        return this.f4176a.c() - this.c.size();
    }

    @DexIgnore
    public final int h(int i) {
        if (i < 0) {
            return -1;
        }
        int c2 = this.f4176a.c();
        int i2 = i;
        while (i2 < c2) {
            int b2 = i - (i2 - this.b.b(i2));
            if (b2 == 0) {
                while (this.b.d(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += b2;
        }
        return -1;
    }

    @DexIgnore
    public View i(int i) {
        return this.f4176a.a(i);
    }

    @DexIgnore
    public int j() {
        return this.f4176a.c();
    }

    @DexIgnore
    public void k(View view) {
        int e = this.f4176a.e(view);
        if (e >= 0) {
            this.b.h(e);
            l(view);
            return;
        }
        throw new IllegalArgumentException("view is not a child, cannot hide " + view);
    }

    @DexIgnore
    public final void l(View view) {
        this.c.add(view);
        this.f4176a.b(view);
    }

    @DexIgnore
    public int m(View view) {
        int e = this.f4176a.e(view);
        if (e != -1 && !this.b.d(e)) {
            return e - this.b.b(e);
        }
        return -1;
    }

    @DexIgnore
    public boolean n(View view) {
        return this.c.contains(view);
    }

    @DexIgnore
    public void o() {
        this.b.g();
        for (int size = this.c.size() - 1; size >= 0; size--) {
            this.f4176a.h(this.c.get(size));
            this.c.remove(size);
        }
        this.f4176a.d();
    }

    @DexIgnore
    public void p(View view) {
        int e = this.f4176a.e(view);
        if (e >= 0) {
            if (this.b.f(e)) {
                t(view);
            }
            this.f4176a.j(e);
        }
    }

    @DexIgnore
    public void q(int i) {
        int h = h(i);
        View a2 = this.f4176a.a(h);
        if (a2 != null) {
            if (this.b.f(h)) {
                t(a2);
            }
            this.f4176a.j(h);
        }
    }

    @DexIgnore
    public boolean r(View view) {
        int e = this.f4176a.e(view);
        if (e == -1) {
            t(view);
            return true;
        } else if (!this.b.d(e)) {
            return false;
        } else {
            this.b.f(e);
            t(view);
            this.f4176a.j(e);
            return true;
        }
    }

    @DexIgnore
    public void s(View view) {
        int e = this.f4176a.e(view);
        if (e < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (this.b.d(e)) {
            this.b.a(e);
            t(view);
        } else {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        }
    }

    @DexIgnore
    public final boolean t(View view) {
        if (!this.c.remove(view)) {
            return false;
        }
        this.f4176a.h(view);
        return true;
    }

    @DexIgnore
    public String toString() {
        return this.b.toString() + ", hidden list:" + this.c.size();
    }
}
