package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public mt0 f2241a;

    @DexIgnore
    public lt0(String str, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.f2241a = new nt0(str, i, i2);
        } else {
            this.f2241a = new ot0(str, i, i2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof lt0)) {
            return false;
        }
        return this.f2241a.equals(((lt0) obj).f2241a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f2241a.hashCode();
    }
}
