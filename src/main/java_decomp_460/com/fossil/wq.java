package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wq extends qq7 implements rp7<fs, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ af b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wq(af afVar, long j) {
        super(1);
        this.b = afVar;
        this.c = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(fs fsVar) {
        if (this.c == ((fw) fsVar).L) {
            af afVar = this.b;
            af.K(afVar, afVar.H);
            af afVar2 = this.b;
            long j = afVar2.G;
            long j2 = afVar2.I;
            if (j != j2) {
                af.J(afVar2);
            } else if (j2 == afVar2.D) {
                af.R(afVar2);
            } else {
                afVar2.T();
            }
        } else {
            af afVar3 = this.b;
            af.M(afVar3, Math.max(0L, afVar3.H - 6144));
            af afVar4 = this.b;
            if (afVar4.H == 0) {
                afVar4.T();
            } else {
                af.S(afVar4);
            }
        }
        return tl7.f3441a;
    }
}
