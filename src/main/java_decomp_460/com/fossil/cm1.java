package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cm1 extends dm1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<cm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public cm1 a(Parcel parcel) {
            return new cm1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public cm1 createFromParcel(Parcel parcel) {
            return new cm1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public cm1[] newArray(int i) {
            return new cm1[i];
        }
    }

    @DexIgnore
    public cm1() {
        super(fm1.CHANCE_OF_RAIN, null, null, null, 14);
    }

    @DexIgnore
    public /* synthetic */ cm1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    public cm1(dt1 dt1, et1 et1) {
        super(fm1.CHANCE_OF_RAIN, null, dt1, et1, 2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ cm1(dt1 dt1, et1 et1, int i, kq7 kq7) {
        this(dt1, (i & 2) != 0 ? new et1(et1.CREATOR.a()) : et1);
    }
}
