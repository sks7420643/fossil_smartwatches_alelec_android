package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz6 implements MembersInjector<uz6> {
    @DexIgnore
    public static void A(uz6 uz6, ht5 ht5) {
        uz6.u = ht5;
    }

    @DexIgnore
    public static void B(uz6 uz6, z27 z27) {
        uz6.H = z27;
    }

    @DexIgnore
    public static void C(uz6 uz6, on5 on5) {
        uz6.K = on5;
    }

    @DexIgnore
    public static void D(uz6 uz6, on5 on5) {
        uz6.x = on5;
    }

    @DexIgnore
    public static void E(uz6 uz6, SleepSummariesRepository sleepSummariesRepository) {
        uz6.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void F(uz6 uz6, SummariesRepository summariesRepository) {
        uz6.C = summariesRepository;
    }

    @DexIgnore
    public static void G(uz6 uz6, uq4 uq4) {
        uz6.m = uq4;
    }

    @DexIgnore
    public static void H(uz6 uz6, UserRepository userRepository) {
        uz6.k = userRepository;
    }

    @DexIgnore
    public static void I(uz6 uz6, WatchLocalizationRepository watchLocalizationRepository) {
        uz6.J = watchLocalizationRepository;
    }

    @DexIgnore
    public static void J(uz6 uz6, WorkoutSettingRepository workoutSettingRepository) {
        uz6.N = workoutSettingRepository;
    }

    @DexIgnore
    public static void K(uz6 uz6) {
        uz6.c0();
    }

    @DexIgnore
    public static void a(uz6 uz6, vt4 vt4) {
        uz6.L = vt4;
    }

    @DexIgnore
    public static void b(uz6 uz6, pr4 pr4) {
        uz6.M = pr4;
    }

    @DexIgnore
    public static void c(uz6 uz6, AlarmsRepository alarmsRepository) {
        uz6.t = alarmsRepository;
    }

    @DexIgnore
    public static void d(uz6 uz6, ck5 ck5) {
        uz6.A = ck5;
    }

    @DexIgnore
    public static void e(uz6 uz6, n27 n27) {
        uz6.y = n27;
    }

    @DexIgnore
    public static void f(uz6 uz6, o27 o27) {
        uz6.z = o27;
    }

    @DexIgnore
    public static void g(uz6 uz6, DeviceRepository deviceRepository) {
        uz6.l = deviceRepository;
    }

    @DexIgnore
    public static void h(uz6 uz6, mj5 mj5) {
        uz6.v = mj5;
    }

    @DexIgnore
    public static void i(uz6 uz6, av5 av5) {
        uz6.w = av5;
    }

    @DexIgnore
    public static void j(uz6 uz6, ru5 ru5) {
        uz6.p = ru5;
    }

    @DexIgnore
    public static void k(uz6 uz6, hu5 hu5) {
        uz6.F = hu5;
    }

    @DexIgnore
    public static void l(uz6 uz6, ju5 ju5) {
        uz6.s = ju5;
    }

    @DexIgnore
    public static void m(uz6 uz6, iu5 iu5) {
        uz6.G = iu5;
    }

    @DexIgnore
    public static void n(uz6 uz6, ku5 ku5) {
        uz6.r = ku5;
    }

    @DexIgnore
    public static void o(uz6 uz6, tu5 tu5) {
        uz6.n = tu5;
    }

    @DexIgnore
    public static void p(uz6 uz6, uu5 uu5) {
        uz6.o = uu5;
    }

    @DexIgnore
    public static void q(uz6 uz6, su5 su5) {
        uz6.q = su5;
    }

    @DexIgnore
    public static void r(uz6 uz6, v27 v27) {
        uz6.I = v27;
    }

    @DexIgnore
    public static void s(uz6 uz6, vu5 vu5) {
        uz6.B = vu5;
    }

    @DexIgnore
    public static void t(uz6 uz6, GoalTrackingRepository goalTrackingRepository) {
        uz6.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void u(uz6 uz6, dv5 dv5) {
        uz6.e = dv5;
    }

    @DexIgnore
    public static void v(uz6 uz6, ev5 ev5) {
        uz6.g = ev5;
    }

    @DexIgnore
    public static void w(uz6 uz6, fv5 fv5) {
        uz6.j = fv5;
    }

    @DexIgnore
    public static void x(uz6 uz6, gv5 gv5) {
        uz6.i = gv5;
    }

    @DexIgnore
    public static void y(uz6 uz6, xn5 xn5) {
        uz6.f = xn5;
    }

    @DexIgnore
    public static void z(uz6 uz6, hv5 hv5) {
        uz6.h = hv5;
    }
}
