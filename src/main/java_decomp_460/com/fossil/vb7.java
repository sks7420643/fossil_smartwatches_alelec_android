package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb7 implements qb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public pb7 f3745a;
    @DexIgnore
    public Bitmap b;
    @DexIgnore
    public String c;

    @DexIgnore
    public vb7(pb7 pb7, Bitmap bitmap, String str) {
        pq7.c(pb7, "tickerData");
        this.f3745a = pb7;
        this.b = bitmap;
        this.c = str;
    }

    @DexIgnore
    public final Bitmap a() {
        return this.b;
    }

    @DexIgnore
    public final pb7 b() {
        return this.f3745a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof vb7) {
                vb7 vb7 = (vb7) obj;
                if (!pq7.a(this.f3745a, vb7.f3745a) || !pq7.a(this.b, vb7.b) || !pq7.a(this.c, vb7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        pb7 pb7 = this.f3745a;
        int hashCode = pb7 != null ? pb7.hashCode() : 0;
        Bitmap bitmap = this.b;
        int hashCode2 = bitmap != null ? bitmap.hashCode() : 0;
        String str = this.c;
        if (str != null) {
            i = str.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UITicker(tickerData=" + this.f3745a + ", tickerBitmap=" + this.b + ", tickerPreviewUrl=" + this.c + ")";
    }
}
