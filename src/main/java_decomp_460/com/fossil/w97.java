package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.s87;
import com.portfolio.platform.view.FlexibleProgressBar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w97 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<s87.b> f3906a; // = hm7.e();
    @DexIgnore
    public /* final */ rp7<s87.b, tl7> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ yf5 f3907a;
        @DexIgnore
        public /* final */ /* synthetic */ w97 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w97$a$a")
        /* renamed from: com.fossil.w97$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0268a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;
            @DexIgnore
            public /* final */ /* synthetic */ s87.b c;

            @DexIgnore
            public View$OnClickListenerC0268a(a aVar, s87.b bVar) {
                this.b = aVar;
                this.c = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                rp7 rp7;
                if (this.c.e() && (rp7 = this.b.b.b) != null) {
                    tl7 tl7 = (tl7) rp7.invoke(s87.b.g(this.c, null, null, null, null, 0, 0, 63, null));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(w97 w97, yf5 yf5) {
            super(yf5.b());
            pq7.c(yf5, "binding");
            this.b = w97;
            this.f3907a = yf5;
        }

        @DexIgnore
        public final void a(s87.b bVar) {
            pq7.c(bVar, "stickerConfig");
            if (bVar.e()) {
                ImageView imageView = this.f3907a.c;
                pq7.b(imageView, "binding.stickerImage");
                Bitmap k = bVar.k();
                a51 b2 = x41.b();
                Context context = imageView.getContext();
                pq7.b(context, "context");
                u71 u71 = new u71(context, b2.a());
                u71.x(k);
                u71.z(imageView);
                b2.b(u71.w());
                ImageView imageView2 = this.f3907a.c;
                pq7.b(imageView2, "binding.stickerImage");
                imageView2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = this.f3907a.b;
                pq7.b(flexibleProgressBar, "binding.progressBar");
                flexibleProgressBar.setVisibility(8);
            } else {
                ImageView imageView3 = this.f3907a.c;
                pq7.b(imageView3, "binding.stickerImage");
                imageView3.setVisibility(8);
                FlexibleProgressBar flexibleProgressBar2 = this.f3907a.b;
                pq7.b(flexibleProgressBar2, "binding.progressBar");
                flexibleProgressBar2.setVisibility(0);
            }
            this.f3907a.b().setOnClickListener(new View$OnClickListenerC0268a(this, bVar));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.rp7<? super com.fossil.s87$b, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    public w97(rp7<? super s87.b, tl7> rp7) {
        this.b = rp7;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f3906a.size();
    }

    @DexIgnore
    /* renamed from: h */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        aVar.a(this.f3906a.get(i));
    }

    @DexIgnore
    /* renamed from: i */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        yf5 c = yf5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(c, "ItemStickerPreviewBindin\u2026tInflater, parent, false)");
        return new a(this, c);
    }

    @DexIgnore
    public final void j(List<s87.b> list) {
        pq7.c(list, "stickerList");
        this.f3906a = list;
        notifyDataSetChanged();
    }
}
