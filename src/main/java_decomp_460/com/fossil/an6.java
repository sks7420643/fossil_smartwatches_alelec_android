package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.chart.HeartRateSleepSessionChart;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an6 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f294a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public ArrayList<a> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ArrayList<v57> f295a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ short c;
        @DexIgnore
        public /* final */ short d;

        @DexIgnore
        public a() {
            this(null, 0, 0, 0, 15, null);
        }

        @DexIgnore
        public a(ArrayList<v57> arrayList, int i, short s, short s2) {
            pq7.c(arrayList, "heartRateSessionData");
            this.f295a = arrayList;
            this.b = i;
            this.c = (short) s;
            this.d = (short) s2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(ArrayList arrayList, int i, short s, short s2, int i2, kq7 kq7) {
            this((i2 & 1) != 0 ? new ArrayList() : arrayList, (i2 & 2) != 0 ? 1 : i, (i2 & 4) != 0 ? 0 : s, (i2 & 8) != 0 ? 0 : s2);
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final ArrayList<v57> b() {
            return this.f295a;
        }

        @DexIgnore
        public final short c() {
            return this.d;
        }

        @DexIgnore
        public final short d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(pq7.a(this.f295a, aVar.f295a) && this.b == aVar.b && this.c == aVar.c && this.d == aVar.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            ArrayList<v57> arrayList = this.f295a;
            return ((((((arrayList != null ? arrayList.hashCode() : 0) * 31) + this.b) * 31) + this.c) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return "SleepHeartRateUIData(heartRateSessionData=" + this.f295a + ", duration=" + this.b + ", minHR=" + ((int) this.c) + ", maxHR=" + ((int) this.d) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ HeartRateSleepSessionChart f296a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(View view) {
            super(view);
            pq7.c(view, "itemView");
            View findViewById = view.findViewById(2131362590);
            pq7.b(findViewById, "itemView.findViewById(R.id.hrssc)");
            this.f296a = (HeartRateSleepSessionChart) findViewById;
        }

        @DexIgnore
        public final HeartRateSleepSessionChart a() {
            return this.f296a;
        }
    }

    @DexIgnore
    public an6(ArrayList<a> arrayList) {
        pq7.c(arrayList, "data");
        this.d = arrayList;
        String d2 = qn5.l.a().d("awakeSleep");
        this.f294a = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("lightSleep");
        this.b = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("deepSleep");
        this.c = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
    }

    @DexIgnore
    /* renamed from: g */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        a aVar = this.d.get(i);
        pq7.b(aVar, "data[position]");
        a aVar2 = aVar;
        bVar.a().setMDuration(aVar2.a());
        bVar.a().setMMinHRValue(aVar2.d());
        bVar.a().setMMaxHRValue(aVar2.c());
        bVar.a().q(this.c, this.b, this.f294a);
        bVar.a().m(aVar2.b());
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.d.size();
    }

    @DexIgnore
    /* renamed from: h */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558694, viewGroup, false);
        pq7.b(inflate, "view");
        return new b(inflate);
    }

    @DexIgnore
    public final void i(ArrayList<a> arrayList) {
        pq7.c(arrayList, "data");
        this.d = arrayList;
        notifyDataSetChanged();
    }
}
