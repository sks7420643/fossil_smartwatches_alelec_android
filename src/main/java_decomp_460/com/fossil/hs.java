package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class hs extends Enum<hs> {
    @DexIgnore
    public static /* final */ hs A;
    @DexIgnore
    public static /* final */ hs B;
    @DexIgnore
    public static /* final */ hs C;
    @DexIgnore
    public static /* final */ hs D;
    @DexIgnore
    public static /* final */ hs E;
    @DexIgnore
    public static /* final */ hs F;
    @DexIgnore
    public static /* final */ hs G;
    @DexIgnore
    public static /* final */ hs H;
    @DexIgnore
    public static /* final */ hs I;
    @DexIgnore
    public static /* final */ hs J;
    @DexIgnore
    public static /* final */ hs K;
    @DexIgnore
    public static /* final */ hs L;
    @DexIgnore
    public static /* final */ hs M;
    @DexIgnore
    public static /* final */ hs N;
    @DexIgnore
    public static /* final */ hs O;
    @DexIgnore
    public static /* final */ hs P;
    @DexIgnore
    public static /* final */ hs Q;
    @DexIgnore
    public static /* final */ hs R;
    @DexIgnore
    public static /* final */ hs S;
    @DexIgnore
    public static /* final */ hs T;
    @DexIgnore
    public static /* final */ hs U;
    @DexIgnore
    public static /* final */ hs V;
    @DexIgnore
    public static /* final */ hs W;
    @DexIgnore
    public static /* final */ hs X;
    @DexIgnore
    public static /* final */ hs Y;
    @DexIgnore
    public static /* final */ hs Z;
    @DexIgnore
    public static /* final */ hs a0;
    @DexIgnore
    public static /* final */ hs b;
    @DexIgnore
    public static /* final */ hs b0;
    @DexIgnore
    public static /* final */ hs c;
    @DexIgnore
    public static /* final */ hs c0;
    @DexIgnore
    public static /* final */ hs d;
    @DexIgnore
    public static /* final */ hs d0;
    @DexIgnore
    public static /* final */ hs e;
    @DexIgnore
    public static /* final */ hs e0;
    @DexIgnore
    public static /* final */ hs f;
    @DexIgnore
    public static /* final */ hs f0;
    @DexIgnore
    public static /* final */ hs g;
    @DexIgnore
    public static /* final */ hs g0;
    @DexIgnore
    public static /* final */ hs h;
    @DexIgnore
    public static /* final */ /* synthetic */ hs[] h0;
    @DexIgnore
    public static /* final */ hs i;
    @DexIgnore
    public static /* final */ hs j;
    @DexIgnore
    public static /* final */ hs k;
    @DexIgnore
    public static /* final */ hs l;
    @DexIgnore
    public static /* final */ hs m;
    @DexIgnore
    public static /* final */ hs n;
    @DexIgnore
    public static /* final */ hs o;
    @DexIgnore
    public static /* final */ hs p;
    @DexIgnore
    public static /* final */ hs q;
    @DexIgnore
    public static /* final */ hs r;
    @DexIgnore
    public static /* final */ hs s;
    @DexIgnore
    public static /* final */ hs t;
    @DexIgnore
    public static /* final */ hs u;
    @DexIgnore
    public static /* final */ hs v;
    @DexIgnore
    public static /* final */ hs w;
    @DexIgnore
    public static /* final */ hs x;
    @DexIgnore
    public static /* final */ hs y;
    @DexIgnore
    public static /* final */ hs z;

    /*
    static {
        hs hsVar = new hs("UNKNOWN", 0);
        b = hsVar;
        hs hsVar2 = new hs("CONNECT", 1);
        c = hsVar2;
        hs hsVar3 = new hs("DISCONNECT", 2);
        d = hsVar3;
        hs hsVar4 = new hs("CLOSE", 3);
        hs hsVar5 = new hs("DISCOVER_SERVICES", 4);
        e = hsVar5;
        hs hsVar6 = new hs("SUBSCRIBE_CHARACTERISTIC", 5);
        f = hsVar6;
        hs hsVar7 = new hs("REQUEST_MTU", 6);
        g = hsVar7;
        hs hsVar8 = new hs("GET_OPTIMAL_PAYLOAD", 7);
        h = hsVar8;
        hs hsVar9 = new hs("READ_RSSI", 8);
        i = hsVar9;
        hs hsVar10 = new hs("READ_SERIAL_NUMBER", 9);
        hs hsVar11 = new hs("READ_FIRMWARE_VERSION", 10);
        j = hsVar11;
        hs hsVar12 = new hs("READ_MODEL_NUMBER", 11);
        k = hsVar12;
        hs hsVar13 = new hs("STREAMING", 12);
        l = hsVar13;
        hs hsVar14 = new hs("PUT_FILE", 13);
        m = hsVar14;
        hs hsVar15 = new hs("VERIFY_FILE", 14);
        n = hsVar15;
        hs hsVar16 = new hs("GET_FILE_SIZE_WRITTEN", 15);
        o = hsVar16;
        hs hsVar17 = new hs("VERIFY_DATA", 16);
        p = hsVar17;
        hs hsVar18 = new hs("ABORT_FILE", 17);
        q = hsVar18;
        hs hsVar19 = new hs("TRANSFER_DATA", 18);
        r = hsVar19;
        hs hsVar20 = new hs("GET_CONNECTION_PARAMS", 19);
        s = hsVar20;
        hs hsVar21 = new hs("SET_CONNECTION_PARAMS", 20);
        t = hsVar21;
        hs hsVar22 = new hs("PLAY_ANIMATION", 21);
        u = hsVar22;
        hs hsVar23 = new hs("LIST_FILE", 22);
        v = hsVar23;
        hs hsVar24 = new hs("GET_FILE", 23);
        w = hsVar24;
        hs hsVar25 = new hs("ERASE_FILE", 24);
        x = hsVar25;
        hs hsVar26 = new hs("REQUEST_HANDS", 25);
        y = hsVar26;
        hs hsVar27 = new hs("RELEASE_HANDS", 26);
        z = hsVar27;
        hs hsVar28 = new hs("MOVE_HANDS", 27);
        A = hsVar28;
        hs hsVar29 = new hs("SET_CALIBRATION_POSITION", 28);
        B = hsVar29;
        hs hsVar30 = new hs("NOTIFY_MUSIC_EVENT", 29);
        C = hsVar30;
        hs hsVar31 = new hs("GET_CURRENT_WORKOUT_SESSION", 30);
        D = hsVar31;
        hs hsVar32 = new hs("STOP_CURRENT_WORKOUT_SESSION", 31);
        E = hsVar32;
        hs hsVar33 = new hs("GET_HEARTBEAT_STATISTIC", 32);
        hs hsVar34 = new hs("GET_HEARTBEAT_INTERVAL", 33);
        hs hsVar35 = new hs("SET_HEARTBEAT_INTERVAL", 34);
        F = hsVar35;
        hs hsVar36 = new hs("SEND_ASYNC_EVENT_ACK", 35);
        G = hsVar36;
        hs hsVar37 = new hs("SEND_PHONE_RANDOM_NUMBER", 36);
        H = hsVar37;
        hs hsVar38 = new hs("SEND_BOTH_SIDES_RANDOM_NUMBERS", 37);
        I = hsVar38;
        hs hsVar39 = new hs("EXCHANGE_PUBLIC_KEYS", 38);
        J = hsVar39;
        hs hsVar40 = new hs("CONNECT_HID", 39);
        K = hsVar40;
        hs hsVar41 = new hs("DISCONNECT_HID", 40);
        L = hsVar41;
        hs hsVar42 = new hs("TROUBLESHOOT_DEVICE_BLE", 41);
        hs hsVar43 = new hs("LEGACY_OTA_ENTER", 42);
        M = hsVar43;
        hs hsVar44 = new hs("LEGACY_GET_FILE_SIZE_WRITTEN", 43);
        N = hsVar44;
        hs hsVar45 = new hs("LEGACY_PUT_FILE", 44);
        O = hsVar45;
        hs hsVar46 = new hs("LEGACY_TRANSFER_DATA", 45);
        P = hsVar46;
        hs hsVar47 = new hs("LEGACY_VERIFY_FILE", 46);
        Q = hsVar47;
        hs hsVar48 = new hs("LEGACY_VERIFY_SEGMENT", 47);
        R = hsVar48;
        hs hsVar49 = new hs("LEGACY_OTA_RESET", 48);
        S = hsVar49;
        hs hsVar50 = new hs("LEGACY_ERASE_SEGMENT", 49);
        T = hsVar50;
        hs hsVar51 = new hs("LEGACY_ABORT_FILE", 50);
        U = hsVar51;
        hs hsVar52 = new hs("LEGACY_LIST_FILE", 51);
        V = hsVar52;
        hs hsVar53 = new hs("LEGACY_GET_ACTIVITY_FILE", 52);
        W = hsVar53;
        hs hsVar54 = new hs("LEGACY_CLOSE_CURRENT_ACTIVITY_FILE", 53);
        X = hsVar54;
        hs hsVar55 = new hs("LEGACY_ERASE_ACTIVITY_FILE", 54);
        Y = hsVar55;
        hs hsVar56 = new hs("CREATE_BOND", 55);
        Z = hsVar56;
        hs hsVar57 = new hs("REMOVE_BOND", 56);
        a0 = hsVar57;
        hs hsVar58 = new hs("CUSTOM_COMMAND", 57);
        b0 = hsVar58;
        hs hsVar59 = new hs("NOTIFY_APP_NOTIFICATION_EVENT", 58);
        c0 = hsVar59;
        hs hsVar60 = new hs("READ_SOFTWARE_REVISION", 59);
        d0 = hsVar60;
        hs hsVar61 = new hs("CUSTOM_COMMAND_WITH_RESPONSE", 60);
        hs hsVar62 = new hs("REQUEST_DISCOVER_SERVICE", 61);
        hs hsVar63 = new hs("CLEAN_UP_DEVICE", 62);
        e0 = hsVar63;
        hs hsVar64 = new hs("CONFIRM_AUTHORIZATION", 63);
        f0 = hsVar64;
        hs hsVar65 = new hs("STOP_AUTHORIZATION_PROCESS", 64);
        g0 = hsVar65;
        h0 = new hs[]{hsVar, hsVar2, hsVar3, hsVar4, hsVar5, hsVar6, hsVar7, hsVar8, hsVar9, hsVar10, hsVar11, hsVar12, hsVar13, hsVar14, hsVar15, hsVar16, hsVar17, hsVar18, hsVar19, hsVar20, hsVar21, hsVar22, hsVar23, hsVar24, hsVar25, hsVar26, hsVar27, hsVar28, hsVar29, hsVar30, hsVar31, hsVar32, hsVar33, hsVar34, hsVar35, hsVar36, hsVar37, hsVar38, hsVar39, hsVar40, hsVar41, hsVar42, hsVar43, hsVar44, hsVar45, hsVar46, hsVar47, hsVar48, hsVar49, hsVar50, hsVar51, hsVar52, hsVar53, hsVar54, hsVar55, hsVar56, hsVar57, hsVar58, hsVar59, hsVar60, hsVar61, hsVar62, hsVar63, hsVar64, hsVar65};
    }
    */

    @DexIgnore
    public hs(String str, int i2) {
    }

    @DexIgnore
    public static hs valueOf(String str) {
        return (hs) Enum.valueOf(hs.class, str);
    }

    @DexIgnore
    public static hs[] values() {
        return (hs[]) h0.clone();
    }
}
