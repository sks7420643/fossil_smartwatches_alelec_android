package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd3 extends as2 implements fc3 {
    @DexIgnore
    public hd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean A0() throws RemoteException {
        Parcel e = e(10, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean D0() throws RemoteException {
        Parcel e = e(11, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean J2() throws RemoteException {
        Parcel e = e(9, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean U() throws RemoteException {
        Parcel e = e(15, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean e0() throws RemoteException {
        Parcel e = e(14, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean f0() throws RemoteException {
        Parcel e = e(13, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean l1() throws RemoteException {
        Parcel e = e(19, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final boolean r2() throws RemoteException {
        Parcel e = e(12, d());
        boolean e2 = es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setCompassEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setMapToolbarEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setMyLocationButtonEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setRotateGesturesEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setScrollGesturesEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setTiltGesturesEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setZoomControlsEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(1, d);
    }

    @DexIgnore
    @Override // com.fossil.fc3
    public final void setZoomGesturesEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        es2.a(d, z);
        i(5, d);
    }
}
