package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.BinaryFile;
import com.fossil.fitness.FitnessAlgorithm;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GpsLogFileManager;
import com.fossil.fitness.Result;
import com.fossil.fitness.StatusCode;
import com.fossil.fitness.SyncMode;
import com.fossil.fitness.UserProfile;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mi extends bi {
    @DexIgnore
    public FitnessData[] V;
    @DexIgnore
    public StatusCode W;
    @DexIgnore
    public /* final */ HashMap<String, Long> X;
    @DexIgnore
    public long Y;
    @DexIgnore
    public /* final */ nm1 Z;
    @DexIgnore
    public /* final */ SyncMode a0;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ mi(com.fossil.k5 r13, com.fossil.i60 r14, com.fossil.nm1 r15, java.util.HashMap r16, java.lang.String r17, com.fossil.fitness.SyncMode r18, int r19) {
        /*
            r12 = this;
            r2 = r19 & 8
            if (r2 == 0) goto L_0x0044
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
        L_0x0009:
            r2 = r19 & 16
            if (r2 == 0) goto L_0x0041
            java.lang.String r2 = "UUID.randomUUID().toString()"
            java.lang.String r10 = com.fossil.e.a(r2)
        L_0x0013:
            r2 = r19 & 32
            if (r2 == 0) goto L_0x0019
            com.fossil.fitness.SyncMode r18 = com.fossil.fitness.SyncMode.FULL_SYNC
        L_0x0019:
            com.fossil.yp r5 = com.fossil.yp.n
            com.fossil.ke r2 = com.fossil.ke.b
            java.lang.String r3 = r13.x
            com.fossil.ob r4 = com.fossil.ob.ACTIVITY_FILE
            short r6 = r2.a(r3, r4)
            r7 = 1
            r9 = 0
            r11 = 64
            r2 = r12
            r3 = r13
            r4 = r14
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r12.Z = r15
            r0 = r18
            r12.a0 = r0
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            r12.X = r2
            r2 = -1
            r12.Y = r2
            return
        L_0x0041:
            r10 = r17
            goto L_0x0013
        L_0x0044:
            r8 = r16
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mi.<init>(com.fossil.k5, com.fossil.i60, com.fossil.nm1, java.util.HashMap, java.lang.String, com.fossil.fitness.SyncMode, int):void");
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public void B() {
        if (q3.f.f(this.x.a())) {
            lp.h(this, new nm(this.w, this.x, this.Z, false, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 32), new kg(this), new yg(this), new lh(this), null, null, 48, null);
        } else {
            super.B();
        }
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp, com.fossil.mj
    public JSONObject C() {
        JSONObject put = super.C().put(zm1.BIOMETRIC_PROFILE.b(), this.Z.c());
        pq7.b(put, "super.optionDescription(\u2026ofile.valueDescription())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public JSONObject E() {
        Object obj;
        String name;
        JSONObject E = super.E();
        jd0 jd0 = jd0.W;
        FitnessData[] fitnessDataArr = this.V;
        JSONObject k = g80.k(E, jd0, fitnessDataArr != null ? g80.i(fitnessDataArr) : null);
        jd0 jd02 = jd0.n2;
        StatusCode statusCode = this.W;
        if (statusCode == null || (name = statusCode.name()) == null || (obj = iy1.b(name)) == null) {
            obj = JSONObject.NULL;
        }
        return g80.k(g80.k(g80.k(g80.k(k, jd02, obj), jd0.P5, Integer.valueOf(this.X.size())), jd0.Q5, Long.valueOf(this.Y)), jd0.b6, this.a0.name());
    }

    @DexIgnore
    @Override // com.fossil.bi
    public void M(ArrayList<j0> arrayList) {
        xw7 unused = gu7.d(qw7.b, null, null, new Cif(this, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void b0(ArrayList<j0> arrayList) {
        ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
        for (T t : arrayList) {
            arrayList2.add(new BinaryFile(t.f, (int) (t.i / ((long) 1000))));
        }
        ArrayList<BinaryFile> arrayList3 = new ArrayList<>(arrayList2);
        FitnessAlgorithm create = FitnessAlgorithm.create(new UserProfile((short) this.Z.getAge(), this.Z.getGender().b(), ((float) this.Z.getHeightInCentimeter()) / 100.0f, (float) this.Z.getWeightInKilogram()));
        pq7.b(create, "FitnessAlgorithm.create(userProfile)");
        ArrayList<String> list = GpsLogFileManager.defaultManager().list();
        pq7.b(list, "GpsLogFileManager.defaultManager().list()");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String str = list.get(i);
            File file = new File(str);
            HashMap<String, Long> hashMap = this.X;
            pq7.b(str, "filePath");
            hashMap.put(str, Long.valueOf(file.length()));
            this.o.post(new yh(this, file, i, list));
        }
        long currentTimeMillis = System.currentTimeMillis();
        ky1 ky1 = ky1.DEBUG;
        arrayList3.size();
        this.a0.name();
        Result parseWithBinaries = create.parseWithBinaries(arrayList3, this.a0);
        pq7.b(parseWithBinaries, "fitnessAlgorithm.parseWi\u2026BinaryFileList, syncMode)");
        this.Y = System.currentTimeMillis() - currentTimeMillis;
        this.W = parseWithBinaries.getStatus();
        if (parseWithBinaries.getStatus() == StatusCode.SUCCESS) {
            ArrayList<FitnessData> fitnessData = parseWithBinaries.getFitnessData();
            pq7.b(fitnessData, "parsedResult.fitnessData");
            Object[] array = fitnessData.toArray(new FitnessData[0]);
            if (array != null) {
                this.V = (FitnessData[]) array;
                ky1 ky12 = ky1.DEBUG;
                FitnessData[] fitnessDataArr = this.V;
                if (fitnessDataArr != null) {
                    g80.i(fitnessDataArr).toString(2);
                    return;
                }
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        ky1 ky13 = ky1.DEBUG;
        parseWithBinaries.getStatus();
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public Object x() {
        FitnessData[] fitnessDataArr = this.V;
        return fitnessDataArr != null ? fitnessDataArr : new FitnessData[0];
    }
}
