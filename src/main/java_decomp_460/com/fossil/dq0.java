package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dq0 extends xp0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ SparseIntArray f820a; // = new SparseIntArray(0);

    @DexIgnore
    @Override // com.fossil.xp0
    public List<xp0> a() {
        return new ArrayList(0);
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public ViewDataBinding b(zp0 zp0, View view, int i) {
        if (f820a.get(i) <= 0 || view.getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public ViewDataBinding c(zp0 zp0, View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || f820a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }
}
