package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr5 implements Factory<vr5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f4165a;
    @DexIgnore
    public /* final */ Provider<DNDSettingsDatabase> b;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> d;

    @DexIgnore
    public xr5(Provider<on5> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        this.f4165a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static xr5 a(Provider<on5> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        return new xr5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static vr5 c(on5 on5, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new vr5(on5, dNDSettingsDatabase, quickResponseRepository, notificationSettingsDatabase);
    }

    @DexIgnore
    /* renamed from: b */
    public vr5 get() {
        return c(this.f4165a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
