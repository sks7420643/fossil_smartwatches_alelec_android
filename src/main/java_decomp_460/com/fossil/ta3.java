package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta3 implements Parcelable.Creator<sa3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sa3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        String str = "";
        String str2 = "";
        String str3 = "";
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                str3 = ad2.f(parcel, t);
            } else if (l == 2) {
                str2 = ad2.f(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                str = ad2.f(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new sa3(str, str3, str2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sa3[] newArray(int i) {
        return new sa3[i];
    }
}
