package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu5 implements Factory<lu5> {
    @DexIgnore
    public static lu5 a(NotificationsRepository notificationsRepository) {
        return new lu5(notificationsRepository);
    }
}
