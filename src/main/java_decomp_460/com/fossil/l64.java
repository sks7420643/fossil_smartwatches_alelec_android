package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l64 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2147a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public l64(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        rc2.o(!of2.a(str), "ApplicationId must be set.");
        this.b = str;
        this.f2147a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    @DexIgnore
    public static l64 a(Context context) {
        xc2 xc2 = new xc2(context);
        String a2 = xc2.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new l64(a2, xc2.a("google_api_key"), xc2.a("firebase_database_url"), xc2.a("ga_trackingId"), xc2.a("gcm_defaultSenderId"), xc2.a("google_storage_bucket"), xc2.a("project_id"));
    }

    @DexIgnore
    public String b() {
        return this.f2147a;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public String d() {
        return this.e;
    }

    @DexIgnore
    public String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof l64)) {
            return false;
        }
        l64 l64 = (l64) obj;
        return pc2.a(this.b, l64.b) && pc2.a(this.f2147a, l64.f2147a) && pc2.a(this.c, l64.c) && pc2.a(this.d, l64.d) && pc2.a(this.e, l64.e) && pc2.a(this.f, l64.f) && pc2.a(this.g, l64.g);
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(this.b, this.f2147a, this.c, this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("applicationId", this.b);
        c2.a("apiKey", this.f2147a);
        c2.a("databaseUrl", this.c);
        c2.a("gcmSenderId", this.e);
        c2.a("storageBucket", this.f);
        c2.a("projectId", this.g);
        return c2.toString();
    }
}
