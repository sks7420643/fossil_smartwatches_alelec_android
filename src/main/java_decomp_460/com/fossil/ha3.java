package com.fossil;

import android.content.Context;
import com.fossil.m62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ha3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ m62.g<fr2> f1456a; // = new m62.g<>();
    @DexIgnore
    public static /* final */ m62.a<fr2, m62.d.C0151d> b;
    @DexIgnore
    public static /* final */ m62<m62.d.C0151d> c;
    @DexIgnore
    @Deprecated
    public static /* final */ da3 d; // = new xr2();
    @DexIgnore
    @Deprecated
    public static /* final */ ma3 e; // = new nr2();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<R extends z62> extends i72<R, fr2> {
        @DexIgnore
        public a(r62 r62) {
            super(ha3.c, r62);
        }
    }

    /*
    static {
        ra3 ra3 = new ra3();
        b = ra3;
        c = new m62<>("LocationServices.API", ra3, f1456a);
    }
    */

    @DexIgnore
    public static ea3 a(Context context) {
        return new ea3(context);
    }

    @DexIgnore
    public static na3 b(Context context) {
        return new na3(context);
    }
}
