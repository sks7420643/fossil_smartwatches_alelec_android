package com.fossil;

import android.os.Build;
import android.text.TextUtils;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Locale f1496a; // = new Locale("", "");

    @DexIgnore
    public static int a(Locale locale) {
        byte directionality = Character.getDirectionality(locale.getDisplayName(locale).charAt(0));
        return (directionality == 1 || directionality == 2) ? 1 : 0;
    }

    @DexIgnore
    public static int b(Locale locale) {
        if (Build.VERSION.SDK_INT >= 17) {
            return TextUtils.getLayoutDirectionFromLocale(locale);
        }
        if (locale != null && !locale.equals(f1496a)) {
            String c = dn0.c(locale);
            if (c == null) {
                return a(locale);
            }
            if (c.equalsIgnoreCase("Arab") || c.equalsIgnoreCase("Hebr")) {
                return 1;
            }
        }
        return 0;
    }
}
