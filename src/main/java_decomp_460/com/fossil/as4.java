package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class as4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f314a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;

    /*
    static {
        int[] iArr = new int[cs4.values().length];
        f314a = iArr;
        iArr[cs4.SET_VALUE.ordinal()] = 1;
        f314a[cs4.SET_VALUE_ANIMATED.ordinal()] = 2;
        f314a[cs4.TICK.ordinal()] = 3;
        int[] iArr2 = new int[cs4.values().length];
        b = iArr2;
        iArr2[cs4.SET_VALUE.ordinal()] = 1;
        b[cs4.SET_VALUE_ANIMATED.ordinal()] = 2;
        b[cs4.TICK.ordinal()] = 3;
        int[] iArr3 = new int[ds4.values().length];
        c = iArr3;
        iArr3[ds4.IDLE.ordinal()] = 1;
        c[ds4.ANIMATING.ordinal()] = 2;
    }
    */
}
