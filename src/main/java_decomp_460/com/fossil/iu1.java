package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum iu1 {
    DEFAULT((byte) 0),
    PRE_SHARED_KEY((byte) 1),
    SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final iu1 a(byte b) {
            iu1[] values = iu1.values();
            for (iu1 iu1 : values) {
                if (iu1.a() == b) {
                    return iu1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public iu1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
