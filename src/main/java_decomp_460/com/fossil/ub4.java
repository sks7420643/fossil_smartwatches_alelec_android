package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ub4 implements tb4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3560a;

    @DexIgnore
    public ub4(Context context) {
        this.f3560a = context;
    }

    @DexIgnore
    @Override // com.fossil.tb4
    public String a() {
        return new File(this.f3560a.getFilesDir(), ".com.google.firebase.crashlytics").getPath();
    }

    @DexIgnore
    @Override // com.fossil.tb4
    public File b() {
        return c(new File(this.f3560a.getFilesDir(), ".com.google.firebase.crashlytics"));
    }

    @DexIgnore
    public File c(File file) {
        if (file == null) {
            x74.f().b("Null File");
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            x74.f().i("Couldn't create file");
        }
        return null;
    }
}
