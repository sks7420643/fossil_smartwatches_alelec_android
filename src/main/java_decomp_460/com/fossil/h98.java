package com.fossil;

import com.fossil.e88;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h98 extends e88.a {
    @DexIgnore
    public static h98 f() {
        return new h98();
    }

    @DexIgnore
    @Override // com.fossil.e88.a
    public e88<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (type == String.class || type == Boolean.TYPE || type == Boolean.class || type == Byte.TYPE || type == Byte.class || type == Character.TYPE || type == Character.class || type == Double.TYPE || type == Double.class || type == Float.TYPE || type == Float.class || type == Integer.TYPE || type == Integer.class || type == Long.TYPE || type == Long.class || type == Short.TYPE || type == Short.class) {
            return x88.f4076a;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.e88.a
    public e88<w18, ?> d(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == String.class) {
            return g98.f1279a;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return y88.f4260a;
        }
        if (type == Byte.class || type == Byte.TYPE) {
            return z88.f4433a;
        }
        if (type == Character.class || type == Character.TYPE) {
            return a98.f230a;
        }
        if (type == Double.class || type == Double.TYPE) {
            return b98.f407a;
        }
        if (type == Float.class || type == Float.TYPE) {
            return c98.f586a;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return d98.f754a;
        }
        if (type == Long.class || type == Long.TYPE) {
            return e98.f899a;
        }
        if (type == Short.class || type == Short.TYPE) {
            return f98.f1091a;
        }
        return null;
    }
}
