package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k96 extends u47 {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ zp0 k; // = new sr4(this);
    @DexIgnore
    public g37<k25> l;
    @DexIgnore
    public b m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final k96 a() {
            return new k96();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object p5();  // void declaration

        @DexIgnore
        Object u1();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog b;
        @DexIgnore
        public /* final */ /* synthetic */ k96 c;

        @DexIgnore
        public c(Dialog dialog, k25 k25, k96 k96) {
            this.b = dialog;
            this.c = k96;
        }

        @DexIgnore
        public final void onClick(View view) {
            k96 k96 = this.c;
            Dialog dialog = this.b;
            pq7.b(dialog, "dialog");
            k96.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog b;
        @DexIgnore
        public /* final */ /* synthetic */ k96 c;

        @DexIgnore
        public d(Dialog dialog, k25 k25, k96 k96) {
            this.b = dialog;
            this.c = k96;
        }

        @DexIgnore
        public final void onClick(View view) {
            b bVar = this.c.m;
            if (bVar != null) {
                bVar.u1();
            }
            k96 k96 = this.c;
            Dialog dialog = this.b;
            pq7.b(dialog, "dialog");
            k96.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog b;
        @DexIgnore
        public /* final */ /* synthetic */ k96 c;

        @DexIgnore
        public e(Dialog dialog, k25 k25, k96 k96) {
            this.b = dialog;
            this.c = k96;
        }

        @DexIgnore
        public final void onClick(View view) {
            b bVar = this.c.m;
            if (bVar != null) {
                bVar.p5();
            }
            k96 k96 = this.c;
            Dialog dialog = this.b;
            pq7.b(dialog, "dialog");
            k96.onDismiss(dialog);
        }
    }

    @DexIgnore
    public final void B6(b bVar) {
        this.m = bVar;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Dialog dialog;
        pq7.c(layoutInflater, "inflater");
        k25 k25 = (k25) aq0.f(layoutInflater, 2131558501, viewGroup, false, this.k);
        g37<k25> g37 = new g37<>(this, k25);
        this.l = g37;
        k25 a2 = g37.a();
        if (!(a2 == null || (dialog = getDialog()) == null)) {
            a2.q.setOnClickListener(new c(dialog, a2, this));
            a2.s.setOnClickListener(new d(dialog, a2, this));
            a2.r.setOnClickListener(new e(dialog, a2, this));
        }
        pq7.b(k25, "binding");
        return k25.n();
    }

    @DexIgnore
    @Override // com.fossil.u47, androidx.fragment.app.Fragment, com.fossil.kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // com.fossil.u47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
