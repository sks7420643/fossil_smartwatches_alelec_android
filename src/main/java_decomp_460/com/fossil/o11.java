package com.fossil;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o11 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f2610a; // = x01.f("Schedulers");

    @DexIgnore
    public static n11 a(Context context, s11 s11) {
        if (Build.VERSION.SDK_INT >= 23) {
            d21 d21 = new d21(context, s11);
            y31.a(context, SystemJobService.class, true);
            x01.c().a(f2610a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return d21;
        }
        n11 c = c(context);
        if (c != null) {
            return c;
        }
        b21 b21 = new b21(context);
        y31.a(context, SystemAlarmService.class, true);
        x01.c().a(f2610a, "Created SystemAlarmScheduler", new Throwable[0]);
        return b21;
    }

    @DexIgnore
    public static void b(o01 o01, WorkDatabase workDatabase, List<n11> list) {
        if (list != null && list.size() != 0) {
            p31 j = workDatabase.j();
            workDatabase.beginTransaction();
            try {
                List<o31> g = j.g(o01.e());
                List<o31> c = j.c();
                if (g != null && g.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (o31 o31 : g) {
                        j.d(o31.f2626a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (g != null && g.size() > 0) {
                    o31[] o31Arr = (o31[]) g.toArray(new o31[g.size()]);
                    for (n11 n11 : list) {
                        if (n11.c()) {
                            n11.a(o31Arr);
                        }
                    }
                }
                if (c != null && c.size() > 0) {
                    o31[] o31Arr2 = (o31[]) c.toArray(new o31[c.size()]);
                    for (n11 n112 : list) {
                        if (!n112.c()) {
                            n112.a(o31Arr2);
                        }
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    public static n11 c(Context context) {
        try {
            n11 n11 = (n11) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            x01.c().a(f2610a, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
            return n11;
        } catch (Throwable th) {
            x01.c().a(f2610a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
