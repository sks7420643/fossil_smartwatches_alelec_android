package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks4 {
    @rj4("challengeId")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2070a;
    @DexIgnore
    @rj4("numberOfPlayers")
    public Integer b;
    @DexIgnore
    @rj4("topPlayers")
    public List<ms4> c;
    @DexIgnore
    @rj4("nearPlayers")
    public List<ms4> d;

    @DexIgnore
    public ks4(String str, Integer num, List<ms4> list, List<ms4> list2) {
        pq7.c(str, "challengeId");
        this.f2070a = str;
        this.b = num;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public final String a() {
        return this.f2070a;
    }

    @DexIgnore
    public final List<ms4> b() {
        return this.d;
    }

    @DexIgnore
    public final Integer c() {
        return this.b;
    }

    @DexIgnore
    public final List<ms4> d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ks4) {
                ks4 ks4 = (ks4) obj;
                if (!pq7.a(this.f2070a, ks4.f2070a) || !pq7.a(this.b, ks4.b) || !pq7.a(this.c, ks4.c) || !pq7.a(this.d, ks4.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2070a;
        int hashCode = str != null ? str.hashCode() : 0;
        Integer num = this.b;
        int hashCode2 = num != null ? num.hashCode() : 0;
        List<ms4> list = this.c;
        int hashCode3 = list != null ? list.hashCode() : 0;
        List<ms4> list2 = this.d;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BCDisplayPlayer(challengeId=" + this.f2070a + ", numberOfPlayers=" + this.b + ", topPlayers=" + this.c + ", nearPlayers=" + this.d + ")";
    }
}
