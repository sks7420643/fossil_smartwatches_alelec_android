package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.uirenew.customview.LimitSlopeSwipeRefresh;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m75 extends l75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131363010, 1);
        z.put(2131363152, 2);
        z.put(2131362462, 3);
        z.put(2131362498, 4);
        z.put(2131362425, 5);
        z.put(2131362428, 6);
    }
    */

    @DexIgnore
    public m75(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 7, y, z));
    }

    @DexIgnore
    public m75(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[4], (ConstraintLayout) objArr[1], (ViewPager2) objArr[2], (LimitSlopeSwipeRefresh) objArr[0]);
        this.x = -1;
        this.w.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.x != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.x = 1;
        }
        w();
    }
}
