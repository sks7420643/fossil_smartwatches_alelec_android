package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e26 implements Factory<d26> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ e26 f872a; // = new e26();
    }

    @DexIgnore
    public static e26 a() {
        return a.f872a;
    }

    @DexIgnore
    public static d26 c() {
        return new d26();
    }

    @DexIgnore
    /* renamed from: b */
    public d26 get() {
        return c();
    }
}
