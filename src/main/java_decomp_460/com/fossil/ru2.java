package com.fossil;

import com.fossil.e13;
import com.fossil.qu2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru2 extends e13<ru2, a> implements o23 {
    @DexIgnore
    public static /* final */ ru2 zzl;
    @DexIgnore
    public static volatile z23<ru2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public int zzf;
    @DexIgnore
    public m13<su2> zzg; // = e13.B();
    @DexIgnore
    public m13<qu2> zzh; // = e13.B();
    @DexIgnore
    public m13<gu2> zzi; // = e13.B();
    @DexIgnore
    public String zzj; // = "";
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<ru2, a> implements o23 {
        @DexIgnore
        public a() {
            super(ru2.zzl);
        }

        @DexIgnore
        public /* synthetic */ a(ou2 ou2) {
            this();
        }

        @DexIgnore
        public final List<gu2> B() {
            return Collections.unmodifiableList(((ru2) this.c).N());
        }

        @DexIgnore
        public final a C() {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ru2) this.c).S();
            return this;
        }

        @DexIgnore
        public final int x() {
            return ((ru2) this.c).M();
        }

        @DexIgnore
        public final qu2 y(int i) {
            return ((ru2) this.c).C(i);
        }

        @DexIgnore
        public final a z(int i, qu2.a aVar) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((ru2) this.c).D(i, (qu2) ((e13) aVar.h()));
            return this;
        }
    }

    /*
    static {
        ru2 ru2 = new ru2();
        zzl = ru2;
        e13.u(ru2.class, ru2);
    }
    */

    @DexIgnore
    public static a P() {
        return (a) zzl.w();
    }

    @DexIgnore
    public static ru2 Q() {
        return zzl;
    }

    @DexIgnore
    public final qu2 C(int i) {
        return this.zzh.get(i);
    }

    @DexIgnore
    public final void D(int i, qu2 qu2) {
        qu2.getClass();
        m13<qu2> m13 = this.zzh;
        if (!m13.zza()) {
            this.zzh = e13.q(m13);
        }
        this.zzh.set(i, qu2);
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final long I() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String K() {
        return this.zze;
    }

    @DexIgnore
    public final List<su2> L() {
        return this.zzg;
    }

    @DexIgnore
    public final int M() {
        return this.zzh.size();
    }

    @DexIgnore
    public final List<gu2> N() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean O() {
        return this.zzk;
    }

    @DexIgnore
    public final void S() {
        this.zzi = e13.B();
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (ou2.f2725a[i - 1]) {
            case 1:
                return new ru2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001\u1002\u0000\u0002\u1008\u0001\u0003\u1004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\u1008\u0003\b\u1007\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", su2.class, "zzh", qu2.class, "zzi", gu2.class, "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                z23<ru2> z232 = zzm;
                if (z232 != null) {
                    return z232;
                }
                synchronized (ru2.class) {
                    try {
                        z23 = zzm;
                        if (z23 == null) {
                            z23 = new e13.c(zzl);
                            zzm = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
