package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ir6;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr6 extends pv5 implements x47, t47.g {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public ir6 h;
    @DexIgnore
    public g37<j45> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return fr6.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<ir6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ fr6 f1182a;

        @DexIgnore
        public b(fr6 fr6) {
            this.f1182a = fr6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ir6.a aVar) {
            if (aVar != null) {
                Integer a2 = aVar.a();
                if (a2 != null) {
                    this.f1182a.N6(a2.intValue());
                }
                Integer b = aVar.b();
                if (b != null) {
                    this.f1182a.M6(b.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fr6 b;

        @DexIgnore
        public c(fr6 fr6) {
            this.b = fr6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 503);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fr6 b;

        @DexIgnore
        public d(fr6 fr6) {
            this.b = fr6;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = fr6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeActiveCaloriesC\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.x47
    public void C3(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        hr7 hr7 = hr7.f1520a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        ir6 ir6 = this.h;
        if (ir6 != null) {
            ir6.h(i2, Color.parseColor(format));
            if (i2 == 503) {
                l = format;
                return;
            }
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void L6() {
        OverviewDayChart overviewDayChart;
        g37<j45> g37 = this.i;
        if (g37 != null) {
            j45 a2 = g37.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 10, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c2 = hm7.c(arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 214, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c3 = hm7.c(arrayList2);
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 24, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c4 = hm7.c(arrayList3);
            ArrayList arrayList4 = new ArrayList();
            arrayList4.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 20, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c5 = hm7.c(arrayList4);
            ArrayList arrayList5 = new ArrayList();
            arrayList5.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 6, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c6 = hm7.c(arrayList5);
            ArrayList arrayList6 = new ArrayList();
            arrayList6.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 100, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c7 = hm7.c(arrayList6);
            ArrayList arrayList7 = new ArrayList();
            arrayList7.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 250, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c8 = hm7.c(arrayList7);
            ArrayList arrayList8 = new ArrayList();
            arrayList8.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 280, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c9 = hm7.c(arrayList8);
            ArrayList arrayList9 = new ArrayList();
            arrayList9.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 272, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c10 = hm7.c(arrayList9);
            ArrayList arrayList10 = new ArrayList();
            arrayList10.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 79, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c11 = hm7.c(arrayList10);
            ArrayList arrayList11 = new ArrayList();
            arrayList11.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 10, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c12 = hm7.c(arrayList11);
            ArrayList arrayList12 = new ArrayList();
            arrayList12.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 5, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c13 = hm7.c(arrayList12);
            ArrayList arrayList13 = new ArrayList();
            arrayList13.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 82, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c14 = hm7.c(arrayList13);
            ArrayList arrayList14 = new ArrayList();
            arrayList14.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 5, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c15 = hm7.c(arrayList14);
            ArrayList arrayList15 = new ArrayList();
            arrayList15.add(new BarChart.a(-1, c2, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c3, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c4, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c5, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c6, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c7, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c8, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c9, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c10, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c11, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c12, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c13, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c14, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c15, -1, false, 8, null));
            BarChart.c cVar = new BarChart.c(SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, 150, arrayList15);
            if (a2 != null && (overviewDayChart = a2.r) != null) {
                overviewDayChart.I(cVar);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void M6(int i2) {
        g37<j45> g37 = this.i;
        if (g37 != null) {
            j45 a2 = g37.a();
            if (a2 != null) {
                a2.r.setGraphPreviewColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void N6(int i2) {
        g37<j45> g37 = this.i;
        if (g37 != null) {
            j45 a2 = g37.a();
            if (a2 != null) {
                a2.v.setBackgroundColor(i2);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            ir6 ir6 = this.h;
            if (ir6 != null) {
                ir6.f(pt6.m.a(), l);
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        j45 j45 = (j45) aq0.f(LayoutInflater.from(getContext()), 2131558529, null, false, A6());
        PortfolioApp.h0.c().M().Y(new hr6()).a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ir6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            ir6 ir6 = (ir6) a2;
            this.h = ir6;
            if (ir6 != null) {
                ir6.e().h(getViewLifecycleOwner(), new b(this));
                ir6 ir62 = this.h;
                if (ir62 != null) {
                    ir62.g();
                    this.i = new g37<>(this, j45);
                    L6();
                    pq7.b(j45, "binding");
                    return j45.n();
                }
                pq7.n("mViewModel");
                throw null;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        ir6 ir6 = this.h;
        if (ir6 != null) {
            ir6.g();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<j45> g37 = this.i;
        if (g37 != null) {
            j45 a2 = g37.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new c(this));
                a2.s.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.x47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
