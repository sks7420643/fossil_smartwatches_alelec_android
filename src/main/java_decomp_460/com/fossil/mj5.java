package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ wy6 f2392a;
    @DexIgnore
    public /* final */ yy6 b;
    @DexIgnore
    public /* final */ dt5 c;
    @DexIgnore
    public /* final */ at5 d;

    @DexIgnore
    public mj5(wy6 wy6, yy6 yy6, dt5 dt5, at5 at5) {
        pq7.c(wy6, "mGetDianaDeviceSettingUseCase");
        pq7.c(yy6, "mGetHybridDeviceSettingUseCase");
        pq7.c(dt5, "mHybridSyncUseCase");
        pq7.c(at5, "mDianaSyncUseCase");
        this.f2392a = wy6;
        this.b = yy6;
        this.c = dt5;
        this.d = at5;
    }

    @DexIgnore
    public final iq4<ty6, uy6, sy6> a(String str) {
        pq7.c(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = lj5.f2205a[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.b;
            }
            if (i == 4 || i == 5) {
                return this.f2392a;
            }
        }
        return this.b;
    }

    @DexIgnore
    public final iq4<tt5, ut5, st5> b(String str) {
        pq7.c(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = lj5.b[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.c;
            }
            if (i == 4 || i == 5) {
                return this.d;
            }
        }
        return this.d;
    }
}
