package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface k41 {
    @DexIgnore
    Executor a();

    @DexIgnore
    void b(Runnable runnable);

    @DexIgnore
    a41 c();
}
