package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw7 extends iu7 {
    @DexIgnore
    public /* final */ rp7<Throwable, tl7> b;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.rp7<? super java.lang.Throwable, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    public uw7(rp7<? super Throwable, tl7> rp7) {
        this.b = rp7;
    }

    @DexIgnore
    @Override // com.fossil.ju7
    public void a(Throwable th) {
        this.b.invoke(th);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        a(th);
        return tl7.f3441a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancel[" + ov7.a(this.b) + '@' + ov7.b(this) + ']';
    }
}
