package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr5 implements Factory<or5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ pr5 f2861a; // = new pr5();
    }

    @DexIgnore
    public static pr5 a() {
        return a.f2861a;
    }

    @DexIgnore
    public static or5 c() {
        return new or5();
    }

    @DexIgnore
    /* renamed from: b */
    public or5 get() {
        return c();
    }
}
