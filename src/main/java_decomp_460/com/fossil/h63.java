package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h63 implements i63 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f1431a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.service.audience.invalidate_config_cache_after_app_unisntall", true);

    @DexIgnore
    @Override // com.fossil.i63
    public final boolean zza() {
        return f1431a.o().booleanValue();
    }
}
