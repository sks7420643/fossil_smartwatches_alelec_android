package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ea4 extends ta4.d.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f902a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ ta4.d.a.b d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.a.AbstractC0223a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f903a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public ta4.d.a.b d;
        @DexIgnore
        public String e;

        @DexIgnore
        @Override // com.fossil.ta4.d.a.AbstractC0223a
        public ta4.d.a a() {
            String str = "";
            if (this.f903a == null) {
                str = " identifier";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (str.isEmpty()) {
                return new ea4(this.f903a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.a.AbstractC0223a
        public ta4.d.a.AbstractC0223a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.a.AbstractC0223a
        public ta4.d.a.AbstractC0223a c(String str) {
            if (str != null) {
                this.f903a = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.a.AbstractC0223a
        public ta4.d.a.AbstractC0223a d(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.a.AbstractC0223a
        public ta4.d.a.AbstractC0223a e(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null version");
        }
    }

    @DexIgnore
    public ea4(String str, String str2, String str3, ta4.d.a.b bVar, String str4) {
        this.f902a = str;
        this.b = str2;
        this.c = str3;
        this.d = bVar;
        this.e = str4;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.a
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.a
    public String c() {
        return this.f902a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.a
    public String d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.a
    public ta4.d.a.b e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        ta4.d.a.b bVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.a)) {
            return false;
        }
        ta4.d.a aVar = (ta4.d.a) obj;
        if (this.f902a.equals(aVar.c()) && this.b.equals(aVar.f()) && ((str = this.c) != null ? str.equals(aVar.b()) : aVar.b() == null) && ((bVar = this.d) != null ? bVar.equals(aVar.e()) : aVar.e() == null)) {
            String str2 = this.e;
            if (str2 == null) {
                if (aVar.d() == null) {
                    return true;
                }
            } else if (str2.equals(aVar.d())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.a
    public String f() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int hashCode = this.f902a.hashCode();
        int hashCode2 = this.b.hashCode();
        String str = this.c;
        int hashCode3 = str == null ? 0 : str.hashCode();
        ta4.d.a.b bVar = this.d;
        int hashCode4 = bVar == null ? 0 : bVar.hashCode();
        String str2 = this.e;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((((hashCode3 ^ ((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003)) * 1000003) ^ hashCode4) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Application{identifier=" + this.f902a + ", version=" + this.b + ", displayVersion=" + this.c + ", organization=" + this.d + ", installationUuid=" + this.e + "}";
    }
}
