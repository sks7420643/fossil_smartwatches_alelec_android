package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i2 extends c2 {
    @DexIgnore
    public static /* final */ h2 CREATOR; // = new h2(null);
    @DexIgnore
    public /* final */ cu1 e;
    @DexIgnore
    public /* final */ byte f;

    @DexIgnore
    public i2(byte b, cu1 cu1, byte b2) {
        super(lt.BATTERY_EVENT, b, false, 4);
        this.e = cu1;
        this.f = (byte) b2;
    }

    @DexIgnore
    public i2(Parcel parcel) {
        super(parcel);
        cu1 a2 = cu1.d.a(parcel.readByte());
        this.e = a2 == null ? cu1.LOW : a2;
        this.f = parcel.readByte();
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.n1, ey1.a(this.e)), jd0.b5, Byte.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f);
        }
    }
}
