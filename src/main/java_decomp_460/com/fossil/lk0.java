package com.fossil;

import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk0<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ mn0<ArrayList<T>> f2207a; // = new nn0(10);
    @DexIgnore
    public /* final */ SimpleArrayMap<T, ArrayList<T>> b; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ ArrayList<T> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ HashSet<T> d; // = new HashSet<>();

    @DexIgnore
    public void a(T t, T t2) {
        if (!this.b.containsKey(t) || !this.b.containsKey(t2)) {
            throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
        }
        ArrayList<T> arrayList = this.b.get(t);
        if (arrayList == null) {
            arrayList = f();
            this.b.put(t, arrayList);
        }
        arrayList.add(t2);
    }

    @DexIgnore
    public void b(T t) {
        if (!this.b.containsKey(t)) {
            this.b.put(t, null);
        }
    }

    @DexIgnore
    public void c() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ArrayList<T> n = this.b.n(i);
            if (n != null) {
                k(n);
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public boolean d(T t) {
        return this.b.containsKey(t);
    }

    @DexIgnore
    public final void e(T t, ArrayList<T> arrayList, HashSet<T> hashSet) {
        if (!arrayList.contains(t)) {
            if (!hashSet.contains(t)) {
                hashSet.add(t);
                ArrayList<T> arrayList2 = this.b.get(t);
                if (arrayList2 != null) {
                    int size = arrayList2.size();
                    for (int i = 0; i < size; i++) {
                        e(arrayList2.get(i), arrayList, hashSet);
                    }
                }
                hashSet.remove(t);
                arrayList.add(t);
                return;
            }
            throw new RuntimeException("This graph contains cyclic dependencies");
        }
    }

    @DexIgnore
    public final ArrayList<T> f() {
        ArrayList<T> b2 = this.f2207a.b();
        return b2 == null ? new ArrayList<>() : b2;
    }

    @DexIgnore
    public List g(T t) {
        return this.b.get(t);
    }

    @DexIgnore
    public List<T> h(T t) {
        int size = this.b.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            ArrayList<T> n = this.b.n(i);
            if (n != null && n.contains(t)) {
                ArrayList arrayList2 = arrayList == null ? new ArrayList() : arrayList;
                arrayList2.add(this.b.j(i));
                arrayList = arrayList2;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public ArrayList<T> i() {
        this.c.clear();
        this.d.clear();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            e(this.b.j(i), this.c, this.d);
        }
        return this.c;
    }

    @DexIgnore
    public boolean j(T t) {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ArrayList<T> n = this.b.n(i);
            if (n != null && n.contains(t)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void k(ArrayList<T> arrayList) {
        arrayList.clear();
        this.f2207a.a(arrayList);
    }
}
