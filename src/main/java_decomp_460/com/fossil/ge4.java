package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ge4 {
    @DexIgnore
    <T> void a(Class<T> cls, ee4<? super T> ee4);

    @DexIgnore
    <T> void b(Class<T> cls, Executor executor, ee4<? super T> ee4);
}
