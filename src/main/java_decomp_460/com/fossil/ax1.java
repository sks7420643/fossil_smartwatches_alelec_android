package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax1 extends yx1 {
    @DexIgnore
    public /* final */ bx1 b;
    @DexIgnore
    public /* final */ Integer c;

    @DexIgnore
    public ax1(bx1 bx1, Integer num) {
        super(bx1);
        this.b = bx1;
        this.c = num;
    }

    @DexIgnore
    @Override // com.fossil.yx1
    public bx1 getErrorCode() {
        return this.b;
    }

    @DexIgnore
    public final Integer getHttpStatus() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.yx1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.d6, this.c);
    }
}
