package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import com.facebook.places.PlaceManager;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w00 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Hashtable<String, e60> f3862a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ w00 c;

    /*
    static {
        String string;
        w00 w00 = new w00();
        c = w00;
        SharedPreferences b2 = g80.b(ld0.b);
        if (b2 != null && (string = b2.getString("com.fossil.blesdk.device.cache", null)) != null) {
            pq7.b(string, "getSharedPreferences(Sha\u2026ENCE_KEY, null) ?: return");
            try {
                JSONArray jSONArray = new JSONArray(mx1.j.n(string));
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string2 = jSONObject.getString(PlaceManager.PARAM_MAC_ADDRESS);
                    String string3 = jSONObject.getString(Constants.SERIAL_NUMBER);
                    pq7.b(string3, "serialNumber");
                    pq7.b(string2, "macAddress");
                    w00.d(string3, string2);
                }
            } catch (Exception e) {
                d90.i.i(e);
            }
        }
    }
    */

    @DexIgnore
    public final e60 a(BluetoothDevice bluetoothDevice, String str) {
        e60 e60;
        m80.c.a("DeviceImplementation.Factory", "getDevice: MAC=%s, serial number=%s.", bluetoothDevice.getAddress(), str);
        synchronized (f3862a) {
            e60 = f3862a.get(bluetoothDevice.getAddress());
            if (e60 == null) {
                e60 = new e60(bluetoothDevice, str, null);
                f3862a.put(e60.u.getMacAddress(), e60);
                if (zk1.u.d(str)) {
                    c.d(str, e60.u.getMacAddress());
                }
            } else if (!zk1.u.d(e60.u.getSerialNumber()) && zk1.u.d(str)) {
                zk1 a2 = zk1.a(e60.u, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262139);
                e60.u = a2;
                f3862a.put(a2.getMacAddress(), e60);
                c.d(str, e60.u.getMacAddress());
            }
        }
        return e60;
    }

    @DexIgnore
    public final String b(String str) {
        String str2;
        synchronized (b) {
            str2 = b.get(str);
        }
        return str2;
    }

    @DexIgnore
    public final void c() {
        JSONArray jSONArray = new JSONArray();
        synchronized (b) {
            for (Map.Entry<String, String> entry : b.entrySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(Constants.SERIAL_NUMBER, entry.getKey());
                    jSONObject.put(PlaceManager.PARAM_MAC_ADDRESS, entry.getValue());
                } catch (Exception e) {
                    d90.i.i(e);
                }
                jSONArray.put(jSONObject);
            }
            tl7 tl7 = tl7.f3441a;
        }
        String jSONArray2 = jSONArray.toString();
        pq7.b(jSONArray2, "array.toString()");
        String o = mx1.j.o(jSONArray2);
        SharedPreferences b2 = g80.b(ld0.b);
        if (b2 != null) {
            b2.edit().putString("com.fossil.blesdk.device.cache", o).apply();
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        if (zk1.u.d(str) && BluetoothAdapter.checkBluetoothAddress(str2)) {
            synchronized (b) {
                b.put(str, str2);
                tl7 tl7 = tl7.f3441a;
            }
        }
    }

    @DexIgnore
    public final String e(String str) {
        synchronized (b) {
            for (Map.Entry<String, String> entry : b.entrySet()) {
                String key = entry.getKey();
                if (vt7.j(entry.getValue(), str, true)) {
                    return key;
                }
            }
            return null;
        }
    }
}
