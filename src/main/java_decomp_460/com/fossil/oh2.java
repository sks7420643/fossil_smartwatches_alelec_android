package com.fossil;

import android.content.Context;
import android.os.Build;
import com.fossil.m62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oh2 {
    @Deprecated

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ m62<m62.d.C0151d> f2681a; // = fn2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ m62<m62.d.C0151d> b; // = zm2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ m62<m62.d.C0151d> c; // = op2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ nh2 d; // = new po2();

    /*
    static {
        m62<m62.d.C0151d> m62 = kn2.G;
        m62<m62.d.C0151d> m622 = on2.G;
        m62<m62.d.C0151d> m623 = up2.G;
        m62<m62.d.C0151d> m624 = kp2.G;
        if (Build.VERSION.SDK_INT < 18) {
        }
        new Scope("https://www.googleapis.com/auth/fitness.activity.read");
        new Scope("https://www.googleapis.com/auth/fitness.activity.write");
        new Scope("https://www.googleapis.com/auth/fitness.location.read");
        new Scope("https://www.googleapis.com/auth/fitness.location.write");
        new Scope("https://www.googleapis.com/auth/fitness.body.read");
        new Scope("https://www.googleapis.com/auth/fitness.body.write");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
    }
    */

    @DexIgnore
    public static rh2 a(Context context, GoogleSignInAccount googleSignInAccount) {
        rc2.k(googleSignInAccount);
        return new rh2(context, new kj2(context, googleSignInAccount));
    }

    @DexIgnore
    public static th2 b(Context context, GoogleSignInAccount googleSignInAccount) {
        rc2.k(googleSignInAccount);
        return new th2(context, new kj2(context, googleSignInAccount));
    }
}
