package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx7 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ vz7 f1392a; // = new vz7("COMPLETING_ALREADY");
    @DexIgnore
    public static /* final */ vz7 b; // = new vz7("COMPLETING_WAITING_CHILDREN");
    @DexIgnore
    public static /* final */ vz7 c; // = new vz7("COMPLETING_RETRY");
    @DexIgnore
    public static /* final */ vz7 d; // = new vz7("TOO_LATE_TO_CANCEL");
    @DexIgnore
    public static /* final */ vz7 e; // = new vz7("SEALED");
    @DexIgnore
    public static /* final */ gw7 f; // = new gw7(false);
    @DexIgnore
    public static /* final */ gw7 g; // = new gw7(true);

    @DexIgnore
    public static final Object g(Object obj) {
        return obj instanceof sw7 ? new tw7((sw7) obj) : obj;
    }

    @DexIgnore
    public static final Object h(Object obj) {
        sw7 sw7;
        tw7 tw7 = (tw7) (!(obj instanceof tw7) ? null : obj);
        return (tw7 == null || (sw7 = tw7.f3486a) == null) ? obj : sw7;
    }
}
