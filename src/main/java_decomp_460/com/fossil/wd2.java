package com.fossil;

import com.fossil.qc2;
import com.fossil.t62;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wd2 implements t62.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ t62 f3921a;
    @DexIgnore
    public /* final */ /* synthetic */ ot3 b;
    @DexIgnore
    public /* final */ /* synthetic */ qc2.a c;
    @DexIgnore
    public /* final */ /* synthetic */ qc2.b d;

    @DexIgnore
    public wd2(t62 t62, ot3 ot3, qc2.a aVar, qc2.b bVar) {
        this.f3921a = t62;
        this.b = ot3;
        this.c = aVar;
        this.d = bVar;
    }

    @DexIgnore
    @Override // com.fossil.t62.a
    public final void a(Status status) {
        if (status.D()) {
            this.b.c(this.c.a(this.f3921a.c(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.b.b(this.d.a(status));
    }
}
