package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b62 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<b62> CREATOR; // = new cg2();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    @Deprecated
    public /* final */ int c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public b62(String str, int i, long j) {
        this.b = str;
        this.c = i;
        this.d = j;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof b62) {
            b62 b62 = (b62) obj;
            return ((c() != null && c().equals(b62.c())) || (c() == null && b62.c() == null)) && f() == b62.f();
        }
    }

    @DexIgnore
    public long f() {
        long j = this.d;
        return j == -1 ? (long) this.c : j;
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(c(), Long.valueOf(f()));
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a("name", c());
        c2.a("version", Long.valueOf(f()));
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 1, c(), false);
        bd2.n(parcel, 2, this.c);
        bd2.r(parcel, 3, f());
        bd2.b(parcel, a2);
    }
}
