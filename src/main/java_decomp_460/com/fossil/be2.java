package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be2 implements Parcelable.Creator<tc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tc2 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        boolean z2 = false;
        int i = 0;
        z52 z52 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                i = ad2.v(parcel, t);
            } else if (l == 2) {
                iBinder = ad2.u(parcel, t);
            } else if (l == 3) {
                z52 = (z52) ad2.e(parcel, t, z52.CREATOR);
            } else if (l == 4) {
                z2 = ad2.m(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                z = ad2.m(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new tc2(i, iBinder, z52, z2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tc2[] newArray(int i) {
        return new tc2[i];
    }
}
