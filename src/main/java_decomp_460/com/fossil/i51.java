package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i51 implements j51 {
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ n51<b, Bitmap> b; // = new n51<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1584a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Bitmap.Config c;

        @DexIgnore
        public b(int i, int i2, Bitmap.Config config) {
            pq7.c(config, "config");
            this.f1584a = i;
            this.b = i2;
            this.c = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!(this.f1584a == bVar.f1584a && this.b == bVar.b && pq7.a(this.c, bVar.c))) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f1584a;
            int i2 = this.b;
            Bitmap.Config config = this.c;
            return (config != null ? config.hashCode() : 0) + (((i * 31) + i2) * 31);
        }

        @DexIgnore
        public String toString() {
            a aVar = i51.c;
            int i = this.f1584a;
            int i2 = this.b;
            Bitmap.Config config = this.c;
            return '[' + i + " x " + i2 + "], " + config;
        }
    }

    @DexIgnore
    @Override // com.fossil.j51
    public String a(int i, int i2, Bitmap.Config config) {
        pq7.c(config, "config");
        return '[' + i + " x " + i2 + "], " + config;
    }

    @DexIgnore
    @Override // com.fossil.j51
    public void b(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        n51<b, Bitmap> n51 = this.b;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        pq7.b(config, "bitmap.config");
        n51.f(new b(width, height, config), bitmap);
    }

    @DexIgnore
    @Override // com.fossil.j51
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        pq7.c(config, "config");
        return this.b.a(new b(i, i2, config));
    }

    @DexIgnore
    @Override // com.fossil.j51
    public String d(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        pq7.b(config, "bitmap.config");
        return '[' + width + " x " + height + "], " + config;
    }

    @DexIgnore
    @Override // com.fossil.j51
    public Bitmap removeLast() {
        return this.b.e();
    }

    @DexIgnore
    public String toString() {
        return "AttributeStrategy: groupedMap=" + this.b;
    }
}
