package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.it0;
import com.fossil.jt0;
import com.fossil.kt0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ht0 extends Service {
    @DexIgnore
    public static /* final */ boolean g; // = Log.isLoggable("MBServiceCompat", 3);
    @DexIgnore
    public g b;
    @DexIgnore
    public /* final */ zi0<IBinder, f> c; // = new zi0<>();
    @DexIgnore
    public f d;
    @DexIgnore
    public /* final */ q e; // = new q();
    @DexIgnore
    public MediaSessionCompat.Token f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends m<List<MediaBrowserCompat.MediaItem>> {
        @DexIgnore
        public /* final */ /* synthetic */ f f;
        @DexIgnore
        public /* final */ /* synthetic */ String g;
        @DexIgnore
        public /* final */ /* synthetic */ Bundle h;
        @DexIgnore
        public /* final */ /* synthetic */ Bundle i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Object obj, f fVar, String str, Bundle bundle, Bundle bundle2) {
            super(obj);
            this.f = fVar;
            this.g = str;
            this.h = bundle;
            this.i = bundle2;
        }

        @DexIgnore
        /* renamed from: h */
        public void d(List<MediaBrowserCompat.MediaItem> list) {
            if (ht0.this.c.get(this.f.c.asBinder()) == this.f) {
                if ((a() & 1) != 0) {
                    list = ht0.this.b(list, this.h);
                }
                try {
                    this.f.c.a(this.g, list, this.h, this.i);
                } catch (RemoteException e) {
                    Log.w("MBServiceCompat", "Calling onLoadChildren() failed for id=" + this.g + " package=" + this.f.f1522a);
                }
            } else if (ht0.g) {
                Log.d("MBServiceCompat", "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + this.f.f1522a + " id=" + this.g);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends m<MediaBrowserCompat.MediaItem> {
        @DexIgnore
        public /* final */ /* synthetic */ he0 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ht0 ht0, Object obj, he0 he0) {
            super(obj);
            this.f = he0;
        }

        @DexIgnore
        /* renamed from: h */
        public void d(MediaBrowserCompat.MediaItem mediaItem) {
            if ((a() & 2) != 0) {
                this.f.b(-1, null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("media_item", mediaItem);
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends m<List<MediaBrowserCompat.MediaItem>> {
        @DexIgnore
        public /* final */ /* synthetic */ he0 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ht0 ht0, Object obj, he0 he0) {
            super(obj);
            this.f = he0;
        }

        @DexIgnore
        /* renamed from: h */
        public void d(List<MediaBrowserCompat.MediaItem> list) {
            if ((a() & 4) != 0 || list == null) {
                this.f.b(-1, null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArray("search_results", (Parcelable[]) list.toArray(new MediaBrowserCompat.MediaItem[0]));
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends m<Bundle> {
        @DexIgnore
        public /* final */ /* synthetic */ he0 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ht0 ht0, Object obj, he0 he0) {
            super(obj);
            this.f = he0;
        }

        @DexIgnore
        @Override // com.fossil.ht0.m
        public void c(Bundle bundle) {
            this.f.b(-1, bundle);
        }

        @DexIgnore
        /* renamed from: h */
        public void d(Bundle bundle) {
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public abstract Bundle a();

        @DexIgnore
        public abstract String b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements IBinder.DeathRecipient {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1522a;
        @DexIgnore
        public /* final */ Bundle b;
        @DexIgnore
        public /* final */ o c;
        @DexIgnore
        public /* final */ HashMap<String, List<ln0<IBinder, Bundle>>> d; // = new HashMap<>();
        @DexIgnore
        public e e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                f fVar = f.this;
                ht0.this.c.remove(fVar.c.asBinder());
            }
        }

        @DexIgnore
        public f(String str, int i, int i2, Bundle bundle, o oVar) {
            this.f1522a = str;
            new lt0(str, i, i2);
            this.b = bundle;
            this.c = oVar;
        }

        @DexIgnore
        public void binderDied() {
            ht0.this.e.post(new a());
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        IBinder d(Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements g, it0.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<Bundle> f1523a; // = new ArrayList();
        @DexIgnore
        public Object b;
        @DexIgnore
        public Messenger c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends m<List<MediaBrowserCompat.MediaItem>> {
            @DexIgnore
            public /* final */ /* synthetic */ it0.c f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Object obj, it0.c cVar) {
                super(obj);
                this.f = cVar;
            }

            @DexIgnore
            /* renamed from: h */
            public void d(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    ArrayList arrayList2 = new ArrayList();
                    for (MediaBrowserCompat.MediaItem mediaItem : list) {
                        Parcel obtain = Parcel.obtain();
                        mediaItem.writeToParcel(obtain, 0);
                        arrayList2.add(obtain);
                    }
                    arrayList = arrayList2;
                } else {
                    arrayList = null;
                }
                this.f.b(arrayList);
            }
        }

        @DexIgnore
        public h() {
        }

        @DexIgnore
        @Override // com.fossil.ht0.g
        public void a() {
            Object a2 = it0.a(ht0.this, this);
            this.b = a2;
            it0.c(a2);
        }

        @DexIgnore
        @Override // com.fossil.it0.d
        public void c(String str, it0.c<List<Parcel>> cVar) {
            ht0.this.f(str, new a(this, str, cVar));
        }

        @DexIgnore
        @Override // com.fossil.ht0.g
        public IBinder d(Intent intent) {
            return it0.b(this.b, intent);
        }

        @DexIgnore
        @Override // com.fossil.it0.d
        public it0.a f(String str, int i, Bundle bundle) {
            Bundle bundle2;
            if (bundle == null || bundle.getInt("extra_client_version", 0) == 0) {
                bundle2 = null;
            } else {
                bundle.remove("extra_client_version");
                this.c = new Messenger(ht0.this.e);
                Bundle bundle3 = new Bundle();
                bundle3.putInt("extra_service_version", 2);
                vk0.b(bundle3, "extra_messenger", this.c.getBinder());
                MediaSessionCompat.Token token = ht0.this.f;
                if (token != null) {
                    be0 c2 = token.c();
                    vk0.b(bundle3, "extra_session_binder", c2 == null ? null : c2.asBinder());
                    bundle2 = bundle3;
                } else {
                    this.f1523a.add(bundle3);
                    bundle2 = bundle3;
                }
            }
            ht0 ht0 = ht0.this;
            ht0.d = new f(str, -1, i, bundle, null);
            e e = ht0.this.e(str, i, bundle);
            ht0.this.d = null;
            if (e == null) {
                return null;
            }
            if (bundle2 == null) {
                bundle2 = e.a();
            } else if (e.a() != null) {
                bundle2.putAll(e.a());
            }
            return new it0.a(e.b(), bundle2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends h implements jt0.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends m<MediaBrowserCompat.MediaItem> {
            @DexIgnore
            public /* final */ /* synthetic */ it0.c f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, Object obj, it0.c cVar) {
                super(obj);
                this.f = cVar;
            }

            @DexIgnore
            /* renamed from: h */
            public void d(MediaBrowserCompat.MediaItem mediaItem) {
                if (mediaItem == null) {
                    this.f.b(null);
                    return;
                }
                Parcel obtain = Parcel.obtain();
                mediaItem.writeToParcel(obtain, 0);
                this.f.b(obtain);
            }
        }

        @DexIgnore
        public i() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.ht0.h, com.fossil.ht0.g
        public void a() {
            Object a2 = jt0.a(ht0.this, this);
            this.b = a2;
            it0.c(a2);
        }

        @DexIgnore
        @Override // com.fossil.jt0.b
        public void b(String str, it0.c<Parcel> cVar) {
            ht0.this.h(str, new a(this, str, cVar));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends i implements kt0.c {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends m<List<MediaBrowserCompat.MediaItem>> {
            @DexIgnore
            public /* final */ /* synthetic */ kt0.b f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, Object obj, kt0.b bVar) {
                super(obj);
                this.f = bVar;
            }

            @DexIgnore
            /* renamed from: h */
            public void d(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    ArrayList arrayList2 = new ArrayList();
                    for (MediaBrowserCompat.MediaItem mediaItem : list) {
                        Parcel obtain = Parcel.obtain();
                        mediaItem.writeToParcel(obtain, 0);
                        arrayList2.add(obtain);
                    }
                    arrayList = arrayList2;
                } else {
                    arrayList = null;
                }
                this.f.b(arrayList, a());
            }
        }

        @DexIgnore
        public j() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.ht0.i, com.fossil.ht0.h, com.fossil.ht0.g
        public void a() {
            Object a2 = kt0.a(ht0.this, this);
            this.b = a2;
            it0.c(a2);
        }

        @DexIgnore
        @Override // com.fossil.kt0.c
        public void e(String str, kt0.b bVar, Bundle bundle) {
            ht0.this.g(str, new a(this, str, bVar), bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends j {
        @DexIgnore
        public k(ht0 ht0) {
            super();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Messenger f1524a;

        @DexIgnore
        public l() {
        }

        @DexIgnore
        @Override // com.fossil.ht0.g
        public void a() {
            this.f1524a = new Messenger(ht0.this.e);
        }

        @DexIgnore
        @Override // com.fossil.ht0.g
        public IBinder d(Intent intent) {
            if ("android.media.browse.MediaBrowserService".equals(intent.getAction())) {
                return this.f1524a.getBinder();
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class m<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Object f1525a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;

        @DexIgnore
        public m(Object obj) {
            this.f1525a = obj;
        }

        @DexIgnore
        public int a() {
            return this.e;
        }

        @DexIgnore
        public boolean b() {
            return this.b || this.c || this.d;
        }

        @DexIgnore
        public void c(Bundle bundle) {
            throw new UnsupportedOperationException("It is not supported to send an error for " + this.f1525a);
        }

        @DexIgnore
        public abstract void d(T t);

        @DexIgnore
        public void e(Bundle bundle) {
            if (this.c || this.d) {
                throw new IllegalStateException("sendError() called when either sendResult() or sendError() had already been called for: " + this.f1525a);
            }
            this.d = true;
            c(bundle);
        }

        @DexIgnore
        public void f(T t) {
            if (this.c || this.d) {
                throw new IllegalStateException("sendResult() called when either sendResult() or sendError() had already been called for: " + this.f1525a);
            }
            this.c = true;
            d(t);
        }

        @DexIgnore
        public void g(int i) {
            this.e = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ int d;
            @DexIgnore
            public /* final */ /* synthetic */ int e;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle f;

            @DexIgnore
            public a(o oVar, String str, int i, int i2, Bundle bundle) {
                this.b = oVar;
                this.c = str;
                this.d = i;
                this.e = i2;
                this.f = bundle;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.b.asBinder();
                ht0.this.c.remove(asBinder);
                f fVar = new f(this.c, this.d, this.e, this.f, this.b);
                ht0 ht0 = ht0.this;
                ht0.d = fVar;
                e e2 = ht0.e(this.c, this.e, this.f);
                fVar.e = e2;
                ht0 ht02 = ht0.this;
                ht02.d = null;
                if (e2 == null) {
                    Log.i("MBServiceCompat", "No root for client " + this.c + " from service " + a.class.getName());
                    try {
                        this.b.b();
                    } catch (RemoteException e3) {
                        Log.w("MBServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + this.c);
                    }
                } else {
                    try {
                        ht02.c.put(asBinder, fVar);
                        asBinder.linkToDeath(fVar, 0);
                        if (ht0.this.f != null) {
                            this.b.c(fVar.e.b(), ht0.this.f, fVar.e.a());
                        }
                    } catch (RemoteException e4) {
                        Log.w("MBServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + this.c);
                        ht0.this.c.remove(asBinder);
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;

            @DexIgnore
            public b(o oVar) {
                this.b = oVar;
            }

            @DexIgnore
            public void run() {
                f remove = ht0.this.c.remove(this.b.asBinder());
                if (remove != null) {
                    remove.c.asBinder().unlinkToDeath(remove, 0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ IBinder d;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle e;

            @DexIgnore
            public c(o oVar, String str, IBinder iBinder, Bundle bundle) {
                this.b = oVar;
                this.c = str;
                this.d = iBinder;
                this.e = bundle;
            }

            @DexIgnore
            public void run() {
                f fVar = ht0.this.c.get(this.b.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "addSubscription for callback that isn't registered id=" + this.c);
                    return;
                }
                ht0.this.a(this.c, fVar, this.d, this.e);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class d implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ IBinder d;

            @DexIgnore
            public d(o oVar, String str, IBinder iBinder) {
                this.b = oVar;
                this.c = str;
                this.d = iBinder;
            }

            @DexIgnore
            public void run() {
                f fVar = ht0.this.c.get(this.b.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "removeSubscription for callback that isn't registered id=" + this.c);
                } else if (!ht0.this.p(this.c, fVar, this.d)) {
                    Log.w("MBServiceCompat", "removeSubscription called for " + this.c + " which is not subscribed");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class e implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ he0 d;

            @DexIgnore
            public e(o oVar, String str, he0 he0) {
                this.b = oVar;
                this.c = str;
                this.d = he0;
            }

            @DexIgnore
            public void run() {
                f fVar = ht0.this.c.get(this.b.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "getMediaItem for callback that isn't registered id=" + this.c);
                    return;
                }
                ht0.this.n(this.c, fVar, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class f implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ int d;
            @DexIgnore
            public /* final */ /* synthetic */ int e;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle f;

            @DexIgnore
            public f(o oVar, String str, int i, int i2, Bundle bundle) {
                this.b = oVar;
                this.c = str;
                this.d = i;
                this.e = i2;
                this.f = bundle;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.b.asBinder();
                ht0.this.c.remove(asBinder);
                f fVar = new f(this.c, this.d, this.e, this.f, this.b);
                ht0.this.c.put(asBinder, fVar);
                try {
                    asBinder.linkToDeath(fVar, 0);
                } catch (RemoteException e2) {
                    Log.w("MBServiceCompat", "IBinder is already dead.");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class g implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;

            @DexIgnore
            public g(o oVar) {
                this.b = oVar;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.b.asBinder();
                f remove = ht0.this.c.remove(asBinder);
                if (remove != null) {
                    asBinder.unlinkToDeath(remove, 0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class h implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle d;
            @DexIgnore
            public /* final */ /* synthetic */ he0 e;

            @DexIgnore
            public h(o oVar, String str, Bundle bundle, he0 he0) {
                this.b = oVar;
                this.c = str;
                this.d = bundle;
                this.e = he0;
            }

            @DexIgnore
            public void run() {
                f fVar = ht0.this.c.get(this.b.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "search for callback that isn't registered query=" + this.c);
                    return;
                }
                ht0.this.o(this.c, this.d, fVar, this.e);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class i implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o b;
            @DexIgnore
            public /* final */ /* synthetic */ String c;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle d;
            @DexIgnore
            public /* final */ /* synthetic */ he0 e;

            @DexIgnore
            public i(o oVar, String str, Bundle bundle, he0 he0) {
                this.b = oVar;
                this.c = str;
                this.d = bundle;
                this.e = he0;
            }

            @DexIgnore
            public void run() {
                f fVar = ht0.this.c.get(this.b.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "sendCustomAction for callback that isn't registered action=" + this.c + ", extras=" + this.d);
                    return;
                }
                ht0.this.l(this.c, this.d, fVar, this.e);
            }
        }

        @DexIgnore
        public n() {
        }

        @DexIgnore
        public void a(String str, IBinder iBinder, Bundle bundle, o oVar) {
            ht0.this.e.a(new c(oVar, str, iBinder, bundle));
        }

        @DexIgnore
        public void b(String str, int i2, int i3, Bundle bundle, o oVar) {
            if (ht0.this.c(str, i3)) {
                ht0.this.e.a(new a(oVar, str, i2, i3, bundle));
                return;
            }
            throw new IllegalArgumentException("Package/uid mismatch: uid=" + i3 + " package=" + str);
        }

        @DexIgnore
        public void c(o oVar) {
            ht0.this.e.a(new b(oVar));
        }

        @DexIgnore
        public void d(String str, he0 he0, o oVar) {
            if (!TextUtils.isEmpty(str) && he0 != null) {
                ht0.this.e.a(new e(oVar, str, he0));
            }
        }

        @DexIgnore
        public void e(o oVar, String str, int i2, int i3, Bundle bundle) {
            ht0.this.e.a(new f(oVar, str, i2, i3, bundle));
        }

        @DexIgnore
        public void f(String str, IBinder iBinder, o oVar) {
            ht0.this.e.a(new d(oVar, str, iBinder));
        }

        @DexIgnore
        public void g(String str, Bundle bundle, he0 he0, o oVar) {
            if (!TextUtils.isEmpty(str) && he0 != null) {
                ht0.this.e.a(new h(oVar, str, bundle, he0));
            }
        }

        @DexIgnore
        public void h(String str, Bundle bundle, he0 he0, o oVar) {
            if (!TextUtils.isEmpty(str) && he0 != null) {
                ht0.this.e.a(new i(oVar, str, bundle, he0));
            }
        }

        @DexIgnore
        public void i(o oVar) {
            ht0.this.e.a(new g(oVar));
        }
    }

    @DexIgnore
    public interface o {
        @DexIgnore
        void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException;

        @DexIgnore
        IBinder asBinder();

        @DexIgnore
        void b() throws RemoteException;

        @DexIgnore
        void c(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class p implements o {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Messenger f1527a;

        @DexIgnore
        public p(Messenger messenger) {
            this.f1527a = messenger;
        }

        @DexIgnore
        @Override // com.fossil.ht0.o
        public void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException {
            Bundle bundle3 = new Bundle();
            bundle3.putString("data_media_item_id", str);
            bundle3.putBundle("data_options", bundle);
            bundle3.putBundle("data_notify_children_changed_options", bundle2);
            if (list != null) {
                bundle3.putParcelableArrayList("data_media_item_list", list instanceof ArrayList ? (ArrayList) list : new ArrayList<>(list));
            }
            d(3, bundle3);
        }

        @DexIgnore
        @Override // com.fossil.ht0.o
        public IBinder asBinder() {
            return this.f1527a.getBinder();
        }

        @DexIgnore
        @Override // com.fossil.ht0.o
        public void b() throws RemoteException {
            d(2, null);
        }

        @DexIgnore
        @Override // com.fossil.ht0.o
        public void c(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putInt("extra_service_version", 2);
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putParcelable("data_media_session_token", token);
            bundle2.putBundle("data_root_hints", bundle);
            d(1, bundle2);
        }

        @DexIgnore
        public final void d(int i, Bundle bundle) throws RemoteException {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 2;
            obtain.setData(bundle);
            this.f1527a.send(obtain);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q extends Handler {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ n f1528a; // = new n();

        @DexIgnore
        public q() {
        }

        @DexIgnore
        public void a(Runnable runnable) {
            if (Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }

        @DexIgnore
        public void handleMessage(Message message) {
            Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    Bundle bundle = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle);
                    this.f1528a.b(data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle, new p(message.replyTo));
                    return;
                case 2:
                    this.f1528a.c(new p(message.replyTo));
                    return;
                case 3:
                    Bundle bundle2 = data.getBundle("data_options");
                    MediaSessionCompat.a(bundle2);
                    this.f1528a.a(data.getString("data_media_item_id"), vk0.a(data, "data_callback_token"), bundle2, new p(message.replyTo));
                    return;
                case 4:
                    this.f1528a.f(data.getString("data_media_item_id"), vk0.a(data, "data_callback_token"), new p(message.replyTo));
                    return;
                case 5:
                    this.f1528a.d(data.getString("data_media_item_id"), (he0) data.getParcelable("data_result_receiver"), new p(message.replyTo));
                    return;
                case 6:
                    Bundle bundle3 = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle3);
                    this.f1528a.e(new p(message.replyTo), data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle3);
                    return;
                case 7:
                    this.f1528a.i(new p(message.replyTo));
                    return;
                case 8:
                    Bundle bundle4 = data.getBundle("data_search_extras");
                    MediaSessionCompat.a(bundle4);
                    this.f1528a.g(data.getString("data_search_query"), bundle4, (he0) data.getParcelable("data_result_receiver"), new p(message.replyTo));
                    return;
                case 9:
                    Bundle bundle5 = data.getBundle("data_custom_action_extras");
                    MediaSessionCompat.a(bundle5);
                    this.f1528a.h(data.getString("data_custom_action"), bundle5, (he0) data.getParcelable("data_result_receiver"), new p(message.replyTo));
                    return;
                default:
                    Log.w("MBServiceCompat", "Unhandled message: " + message + "\n  Service version: 2\n  Client version: " + message.arg1);
                    return;
            }
        }

        @DexIgnore
        public boolean sendMessageAtTime(Message message, long j) {
            Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            data.putInt("data_calling_pid", Binder.getCallingPid());
            return super.sendMessageAtTime(message, j);
        }
    }

    @DexIgnore
    public void a(String str, f fVar, IBinder iBinder, Bundle bundle) {
        List<ln0<IBinder, Bundle>> list = fVar.d.get(str);
        ArrayList arrayList = list == null ? new ArrayList() : list;
        for (ln0<IBinder, Bundle> ln0 : arrayList) {
            if (iBinder == ln0.f2221a && gt0.a(bundle, ln0.b)) {
                return;
            }
        }
        arrayList.add(new ln0<>(iBinder, bundle));
        fVar.d.put(str, arrayList);
        m(str, fVar, bundle, null);
        j(str, bundle);
    }

    @DexIgnore
    public List<MediaBrowserCompat.MediaItem> b(List<MediaBrowserCompat.MediaItem> list, Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i2 = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i3 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i2 == -1 && i3 == -1) {
            return list;
        }
        int i4 = i3 * i2;
        int i5 = i4 + i3;
        if (i2 < 0 || i3 < 1 || i4 >= list.size()) {
            return Collections.emptyList();
        }
        if (i5 > list.size()) {
            i5 = list.size();
        }
        return list.subList(i4, i5);
    }

    @DexIgnore
    public boolean c(String str, int i2) {
        if (str == null) {
            return false;
        }
        for (String str2 : getPackageManager().getPackagesForUid(i2)) {
            if (str2.equals(str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void d(String str, Bundle bundle, m<Bundle> mVar) {
        mVar.e(null);
    }

    @DexIgnore
    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    public abstract e e(String str, int i2, Bundle bundle);

    @DexIgnore
    public abstract void f(String str, m<List<MediaBrowserCompat.MediaItem>> mVar);

    @DexIgnore
    public void g(String str, m<List<MediaBrowserCompat.MediaItem>> mVar, Bundle bundle) {
        mVar.g(1);
        f(str, mVar);
    }

    @DexIgnore
    public void h(String str, m<MediaBrowserCompat.MediaItem> mVar) {
        mVar.g(2);
        mVar.f(null);
    }

    @DexIgnore
    public void i(String str, Bundle bundle, m<List<MediaBrowserCompat.MediaItem>> mVar) {
        mVar.g(4);
        mVar.f(null);
    }

    @DexIgnore
    public void j(String str, Bundle bundle) {
    }

    @DexIgnore
    public void k(String str) {
    }

    @DexIgnore
    public void l(String str, Bundle bundle, f fVar, he0 he0) {
        d dVar = new d(this, str, he0);
        d(str, bundle, dVar);
        if (!dVar.b()) {
            throw new IllegalStateException("onCustomAction must call detach() or sendResult() or sendError() before returning for action=" + str + " extras=" + bundle);
        }
    }

    @DexIgnore
    public void m(String str, f fVar, Bundle bundle, Bundle bundle2) {
        a aVar = new a(str, fVar, str, bundle, bundle2);
        if (bundle == null) {
            f(str, aVar);
        } else {
            g(str, aVar, bundle);
        }
        if (!aVar.b()) {
            throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + fVar.f1522a + " id=" + str);
        }
    }

    @DexIgnore
    public void n(String str, f fVar, he0 he0) {
        b bVar = new b(this, str, he0);
        h(str, bVar);
        if (!bVar.b()) {
            throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
        }
    }

    @DexIgnore
    public void o(String str, Bundle bundle, f fVar, he0 he0) {
        c cVar = new c(this, str, he0);
        i(str, bundle, cVar);
        if (!cVar.b()) {
            throw new IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + str);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.b.d(intent);
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 28) {
            this.b = new k(this);
        } else if (i2 >= 26) {
            this.b = new j();
        } else if (i2 >= 23) {
            this.b = new i();
        } else if (i2 >= 21) {
            this.b = new h();
        } else {
            this.b = new l();
        }
        this.b.a();
    }

    @DexIgnore
    public boolean p(String str, f fVar, IBinder iBinder) {
        boolean z = false;
        if (iBinder == null) {
            try {
                return fVar.d.remove(str) != null;
            } finally {
                k(str);
            }
        } else {
            List<ln0<IBinder, Bundle>> list = fVar.d.get(str);
            if (list != null) {
                Iterator<ln0<IBinder, Bundle>> it = list.iterator();
                boolean z2 = false;
                while (it.hasNext()) {
                    if (iBinder == it.next().f2221a) {
                        it.remove();
                        z2 = true;
                    }
                }
                if (list.size() == 0) {
                    fVar.d.remove(str);
                    z = z2;
                } else {
                    z = z2;
                }
            }
            k(str);
            return z;
        }
    }
}
