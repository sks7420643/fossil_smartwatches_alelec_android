package com.fossil;

import com.fossil.fitness.SyncMode;
import com.fossil.yk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qc extends qq7 implements rp7<tl7, tl7> {
    @DexIgnore
    public /* final */ /* synthetic */ c2 b;
    @DexIgnore
    public /* final */ /* synthetic */ e60 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qc(c2 c2Var, e60 e60) {
        super(1);
        this.b = c2Var;
        this.c = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public tl7 invoke(tl7 tl7) {
        nm1 a2;
        v8[] v8VarArr = ((g2) this.b).e;
        for (v8 v8Var : v8VarArr) {
            ob b2 = ob.A.b(v8Var.c);
            if (b2 != null) {
                switch (nc.f2497a[b2.ordinal()]) {
                    case 1:
                        if (v8Var.b == t8.GET) {
                            e60.x0(this.c, false, true, null, 4);
                            break;
                        } else {
                            continue;
                        }
                    case 2:
                        e60 e60 = this.c;
                        pp1 pp1 = new pp1(this.b.c, v8Var.b);
                        yk1.b bVar = e60.w;
                        if (bVar != null) {
                            bVar.onEventReceived(e60, pp1);
                            break;
                        } else {
                            continue;
                        }
                    case 3:
                        if (v8Var.b == t8.GET && ((a2 = cx1.f.a()) == null || this.c.l0(a2, SyncMode.AUTO_SYNC).m(new pc(this)) == null)) {
                            this.c.F0();
                            break;
                        }
                    case 4:
                        e60 e602 = this.c;
                        yp1 yp1 = new yp1(this.b.c, v8Var.b);
                        yk1.b bVar2 = e602.w;
                        if (bVar2 != null) {
                            bVar2.onEventReceived(e602, yp1);
                            break;
                        } else {
                            continue;
                        }
                    case 5:
                        e60 e603 = this.c;
                        up1 up1 = new up1(this.b.c, v8Var.b);
                        yk1.b bVar3 = e603.w;
                        if (bVar3 != null) {
                            bVar3.onEventReceived(e603, up1);
                            break;
                        } else {
                            continue;
                        }
                    case 6:
                        if (v8Var.b == t8.GET) {
                            e60.e0(this.c, false, true, null, 4);
                            break;
                        } else {
                            continue;
                        }
                }
            }
        }
        return tl7.f3441a;
    }
}
