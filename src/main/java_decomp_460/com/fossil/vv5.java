package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.x37;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv5 extends pv5 implements iw6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public hw6 g;
    @DexIgnore
    public g37<p95> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return vv5.j;
        }

        @DexIgnore
        public final vv5 b() {
            return new vv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements k67 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ p95 f3841a;
        @DexIgnore
        public /* final */ /* synthetic */ vv5 b;

        @DexIgnore
        public b(p95 p95, vv5 vv5) {
            this.f3841a = p95;
            this.b = vv5;
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void a(int i) {
            if (this.f3841a.w.getUnit() == ai5.METRIC) {
                vv5.K6(this.b).o(i);
            } else {
                vv5.K6(this.b).o(Math.round(jk5.d((float) (i / 12), ((float) i) % 12.0f)));
            }
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void c(boolean z) {
            TabLayout tabLayout = this.f3841a.z;
            pq7.b(tabLayout, "it.tlHeightUnit");
            h57.c(tabLayout, !z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TabLayout.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vv5 f3842a;

        @DexIgnore
        public c(vv5 vv5) {
            this.f3842a = vv5;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
            pq7.c(gVar, "tab");
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            pq7.c(gVar, "tab");
            CharSequence h = gVar.h();
            ai5 ai5 = ai5.METRIC;
            if (pq7.a(h, PortfolioApp.h0.c().getString(2131886954))) {
                ai5 = ai5.IMPERIAL;
            }
            vv5.K6(this.f3842a).p(ai5);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
            pq7.c(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements k67 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ p95 f3843a;
        @DexIgnore
        public /* final */ /* synthetic */ vv5 b;

        @DexIgnore
        public d(p95 p95, vv5 vv5) {
            this.f3843a = p95;
            this.b = vv5;
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void a(int i) {
            if (this.f3843a.x.getUnit() == ai5.METRIC) {
                vv5.K6(this.b).r(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            vv5.K6(this.b).r(Math.round(jk5.m(((float) i) / 10.0f)));
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.k67
        public void c(boolean z) {
            TabLayout tabLayout = this.f3843a.A;
            pq7.b(tabLayout, "it.tlWeightUnit");
            h57.c(tabLayout, !z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TabLayout.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vv5 f3844a;

        @DexIgnore
        public e(vv5 vv5) {
            this.f3844a = vv5;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
            pq7.c(gVar, "tab");
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            pq7.c(gVar, "tab");
            CharSequence h = gVar.h();
            ai5 ai5 = ai5.METRIC;
            if (pq7.a(h, PortfolioApp.h0.c().getString(2131886956))) {
                ai5 = ai5.IMPERIAL;
            }
            vv5.K6(this.f3844a).q(ai5);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
            pq7.c(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 b;

        @DexIgnore
        public f(vv5 vv5) {
            this.b = vv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            vv5.K6(this.b).n(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 b;

        @DexIgnore
        public g(vv5 vv5) {
            this.b = vv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            vv5.K6(this.b).n(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 b;

        @DexIgnore
        public h(vv5 vv5) {
            this.b = vv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            vv5.K6(this.b).n(false);
        }
    }

    /*
    static {
        String simpleName = vv5.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "OnboardingHeightWeightFr\u2026::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ hw6 K6(vv5 vv5) {
        hw6 hw6 = vv5.g;
        if (hw6 != null) {
            return hw6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.iw6
    public void H1(int i2, ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        int i3 = wv5.f4002a[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "updateData weight=" + i2 + " metric");
            g37<p95> g37 = this.h;
            if (g37 != null) {
                p95 a2 = g37.a();
                if (a2 != null) {
                    TabLayout.g v = a2.A.v(0);
                    if (v != null) {
                        v.k();
                    }
                    a2.x.setUnit(ai5.METRIC);
                    a2.x.setFormatter(new ProfileFormatter(4));
                    a2.x.l(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            g37<p95> g372 = this.h;
            if (g372 != null) {
                p95 a3 = g372.a();
                if (a3 != null) {
                    TabLayout.g v2 = a3.A.v(1);
                    if (v2 != null) {
                        v2.k();
                    }
                    float h2 = jk5.h((float) i2);
                    a3.x.setUnit(ai5.IMPERIAL);
                    a3.x.setFormatter(new ProfileFormatter(4));
                    a3.x.l(780, 4401, Math.round(h2 * ((float) 10)));
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(hw6 hw6) {
        pq7.c(hw6, "presenter");
        this.g = hw6;
    }

    @DexIgnore
    @Override // com.fossil.iw6
    public void R0(int i2, ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        int i3 = wv5.b[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "updateData height=" + i2 + " metric");
            g37<p95> g37 = this.h;
            if (g37 != null) {
                p95 a2 = g37.a();
                if (a2 != null) {
                    TabLayout.g v = a2.z.v(0);
                    if (v != null) {
                        v.k();
                    }
                    a2.w.setUnit(ai5.METRIC);
                    a2.w.setFormatter(new ProfileFormatter(-1));
                    a2.w.l(100, 251, i2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            g37<p95> g372 = this.h;
            if (g372 != null) {
                p95 a3 = g372.a();
                if (a3 != null) {
                    TabLayout.g v2 = a3.z.v(1);
                    if (v2 != null) {
                        v2.k();
                    }
                    cl7<Integer, Integer> b2 = jk5.b((float) i2);
                    a3.w.setUnit(ai5.IMPERIAL);
                    a3.w.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.w;
                    Integer first = b2.getFirst();
                    pq7.b(first, "currentHeightInFeetAndInches.first");
                    int e2 = jk5.e(first.intValue());
                    Integer second = b2.getSecond();
                    pq7.b(second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.l(40, 99, second.intValue() + e2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.iw6
    public void f() {
        DashBar dashBar;
        g37<p95> g37 = this.h;
        if (g37 != null) {
            p95 a2 = g37.a();
            if (a2 != null && (dashBar = a2.u) != null) {
                x37.a aVar = x37.f4036a;
                pq7.b(dashBar, "this");
                aVar.e(dashBar, 500);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iw6
    public void h() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.iw6
    public void i() {
        if (isActive()) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886961);
            pq7.b(c2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            H6(c2);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<p95> g37 = new g37<>(this, (p95) aq0.f(layoutInflater, 2131558598, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            p95 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        hw6 hw6 = this.g;
        if (hw6 != null) {
            hw6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        hw6 hw6 = this.g;
        if (hw6 != null) {
            hw6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        g37<p95> g37 = this.h;
        if (g37 != null) {
            p95 a2 = g37.a();
            if (a2 != null) {
                a2.w.setValuePickerListener(new b(a2, this));
                a2.z.c(new c(this));
                a2.x.setValuePickerListener(new d(a2, this));
                a2.A.c(new e(this));
                a2.t.setOnClickListener(new f(this));
                a2.C.setOnClickListener(new g(this));
                a2.q.setOnClickListener(new h(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.iw6
    public void u2() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
            pq7.b(activity, "it");
            PairingInstructionsActivity.a.b(aVar, activity, true, false, 4, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
