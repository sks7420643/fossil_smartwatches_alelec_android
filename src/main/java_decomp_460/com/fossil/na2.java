package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.l72;
import com.fossil.m62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na2<ResultT> extends ja2 {
    @DexIgnore
    public /* final */ w72<m62.b, ResultT> b;
    @DexIgnore
    public /* final */ ot3<ResultT> c;
    @DexIgnore
    public /* final */ u72 d;

    @DexIgnore
    public na2(int i, w72<m62.b, ResultT> w72, ot3<ResultT> ot3, u72 u72) {
        super(i);
        this.c = ot3;
        this.b = w72;
        this.d = u72;
        if (i == 2 && w72.b()) {
            throw new IllegalArgumentException("Best-effort write calls cannot pass methods that should auto-resolve missing features.");
        }
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void b(Status status) {
        this.c.d(this.d.a(status));
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void c(l72.a<?> aVar) throws DeadObjectException {
        try {
            this.b.a(aVar.R(), this.c);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            b(z82.a(e2));
        } catch (RuntimeException e3) {
            e(e3);
        }
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void d(a82 a82, boolean z) {
        a82.c(this.c, z);
    }

    @DexIgnore
    @Override // com.fossil.z82
    public final void e(Exception exc) {
        this.c.d(exc);
    }

    @DexIgnore
    @Override // com.fossil.ja2
    public final b62[] g(l72.a<?> aVar) {
        return this.b.c();
    }

    @DexIgnore
    @Override // com.fossil.ja2
    public final boolean h(l72.a<?> aVar) {
        return this.b.b();
    }
}
