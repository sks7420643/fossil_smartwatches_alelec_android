package com.fossil;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vo7 extends IOException {
    @DexIgnore
    public /* final */ File file;
    @DexIgnore
    public /* final */ File other;
    @DexIgnore
    public /* final */ String reason;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vo7(File file2, File file3, String str) {
        super(to7.a(file2, file3, str));
        pq7.c(file2, "file");
        this.file = file2;
        this.other = file3;
        this.reason = str;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ vo7(File file2, File file3, String str, int i, kq7 kq7) {
        this(file2, (i & 2) != 0 ? null : file3, (i & 4) != 0 ? null : str);
    }

    @DexIgnore
    public final File getFile() {
        return this.file;
    }

    @DexIgnore
    public final File getOther() {
        return this.other;
    }

    @DexIgnore
    public final String getReason() {
        return this.reason;
    }
}
