package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<fv1> f836a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements rp7<fv1, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ mo1 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(mo1 mo1) {
            super(1);
            this.b = mo1;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public Boolean invoke(fv1 fv1) {
            return Boolean.valueOf(this.b == fv1.getNotificationType());
        }
    }

    @DexIgnore
    public final fv1[] a() {
        Object[] array = this.f836a.toArray(new fv1[0]);
        if (array != null) {
            return (fv1[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final dv1 b(mo1 mo1, ev1 ev1) {
        synchronized (this) {
            mm7.w(this.f836a, new a(mo1));
            if (ev1 != null) {
                if (!(ev1.getReplyMessages().length == 0)) {
                    this.f836a.add(new fv1(mo1, ev1));
                }
            }
        }
        return this;
    }
}
