package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x71 {

    @DexIgnore
    public interface a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x71$a$a")
        /* renamed from: com.fossil.x71$a$a  reason: collision with other inner class name */
        public static final class C0278a {
            @DexIgnore
            public static void a(a aVar, Object obj) {
            }

            @DexIgnore
            public static void b(a aVar, Object obj, Throwable th) {
                pq7.c(th, "throwable");
            }

            @DexIgnore
            public static void c(a aVar, Object obj) {
                pq7.c(obj, "data");
            }

            @DexIgnore
            public static void d(a aVar, Object obj, q51 q51) {
                pq7.c(obj, "data");
                pq7.c(q51, "source");
            }
        }

        @DexIgnore
        void a(Object obj);

        @DexIgnore
        void b(Object obj, Throwable th);

        @DexIgnore
        void c(Object obj, q51 q51);

        @DexIgnore
        void onCancel(Object obj);
    }

    @DexIgnore
    public x71() {
    }

    @DexIgnore
    public /* synthetic */ x71(kq7 kq7) {
        this();
    }

    @DexIgnore
    public abstract List<String> a();

    @DexIgnore
    public abstract boolean b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract Bitmap.Config d();

    @DexIgnore
    public abstract ColorSpace e();

    @DexIgnore
    public abstract u51 f();

    @DexIgnore
    public abstract s71 g();

    @DexIgnore
    public abstract dv7 h();

    @DexIgnore
    public abstract Drawable i();

    @DexIgnore
    public abstract Drawable j();

    @DexIgnore
    public abstract p18 k();

    @DexIgnore
    public abstract String l();

    @DexIgnore
    public abstract a m();

    @DexIgnore
    public abstract s71 n();

    @DexIgnore
    public abstract s71 o();

    @DexIgnore
    public abstract w71 p();

    @DexIgnore
    public abstract Drawable q();

    @DexIgnore
    public abstract d81 r();

    @DexIgnore
    public abstract e81 s();

    @DexIgnore
    public abstract g81 t();

    @DexIgnore
    public abstract j81 u();

    @DexIgnore
    public abstract List<m81> v();

    @DexIgnore
    public abstract n81 w();
}
