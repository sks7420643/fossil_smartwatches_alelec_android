package com.fossil;

import com.fossil.jn5;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m66 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2309a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<n66> c;
    @DexIgnore
    public List<? extends jn5.a> d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public m66(String str, String str2, ArrayList<n66> arrayList, List<? extends jn5.a> list, boolean z) {
        pq7.c(str, "mPresetId");
        pq7.c(str2, "mPresetName");
        pq7.c(arrayList, "mMicroApps");
        pq7.c(list, "mPermissionFeatures");
        this.f2309a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = list;
        this.e = z;
    }

    @DexIgnore
    public final ArrayList<n66> a() {
        return this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.jn5$a>, java.util.List<com.fossil.jn5$a> */
    public final List<jn5.a> b() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.f2309a;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e() {
        return this.e;
    }
}
