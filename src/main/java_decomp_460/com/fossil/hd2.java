package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd2 extends dd2 {
    @DexIgnore
    public /* final */ j72<Status> b;

    @DexIgnore
    public hd2(j72<Status> j72) {
        this.b = j72;
    }

    @DexIgnore
    @Override // com.fossil.ld2
    public final void u0(int i) throws RemoteException {
        this.b.a(new Status(i));
    }
}
