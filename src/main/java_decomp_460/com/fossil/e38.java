package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e38 {
    @DexIgnore
    public static /* final */ l48 d; // = l48.encodeUtf8(":");
    @DexIgnore
    public static /* final */ l48 e; // = l48.encodeUtf8(":status");
    @DexIgnore
    public static /* final */ l48 f; // = l48.encodeUtf8(":method");
    @DexIgnore
    public static /* final */ l48 g; // = l48.encodeUtf8(":path");
    @DexIgnore
    public static /* final */ l48 h; // = l48.encodeUtf8(":scheme");
    @DexIgnore
    public static /* final */ l48 i; // = l48.encodeUtf8(":authority");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ l48 f876a;
    @DexIgnore
    public /* final */ l48 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(p18 p18);
    }

    @DexIgnore
    public e38(l48 l48, l48 l482) {
        this.f876a = l48;
        this.b = l482;
        this.c = l48.size() + 32 + l482.size();
    }

    @DexIgnore
    public e38(l48 l48, String str) {
        this(l48, l48.encodeUtf8(str));
    }

    @DexIgnore
    public e38(String str, String str2) {
        this(l48.encodeUtf8(str), l48.encodeUtf8(str2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof e38)) {
            return false;
        }
        e38 e38 = (e38) obj;
        return this.f876a.equals(e38.f876a) && this.b.equals(e38.b);
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f876a.hashCode() + 527) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return b28.r("%s: %s", this.f876a.utf8(), this.b.utf8());
    }
}
