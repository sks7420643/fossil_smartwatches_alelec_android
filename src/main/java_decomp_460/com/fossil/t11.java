package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t11 implements Runnable {
    @DexIgnore
    public static /* final */ String z; // = x01.f("WorkerWrapper");
    @DexIgnore
    public Context b;
    @DexIgnore
    public String c;
    @DexIgnore
    public List<n11> d;
    @DexIgnore
    public WorkerParameters.a e;
    @DexIgnore
    public o31 f;
    @DexIgnore
    public ListenableWorker g;
    @DexIgnore
    public ListenableWorker.a h; // = ListenableWorker.a.a();
    @DexIgnore
    public o01 i;
    @DexIgnore
    public k41 j;
    @DexIgnore
    public x21 k;
    @DexIgnore
    public WorkDatabase l;
    @DexIgnore
    public p31 m;
    @DexIgnore
    public a31 s;
    @DexIgnore
    public s31 t;
    @DexIgnore
    public List<String> u;
    @DexIgnore
    public String v;
    @DexIgnore
    public j41<Boolean> w; // = j41.t();
    @DexIgnore
    public g64<ListenableWorker.a> x; // = null;
    @DexIgnore
    public volatile boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ j41 b;

        @DexIgnore
        public a(j41 j41) {
            this.b = j41;
        }

        @DexIgnore
        public void run() {
            try {
                x01.c().a(t11.z, String.format("Starting work for %s", t11.this.f.c), new Throwable[0]);
                t11.this.x = t11.this.g.m();
                this.b.r(t11.this.x);
            } catch (Throwable th) {
                this.b.q(th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ j41 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public b(j41 j41, String str) {
            this.b = j41;
            this.c = str;
        }

        @DexIgnore
        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                ListenableWorker.a aVar = (ListenableWorker.a) this.b.get();
                if (aVar == null) {
                    x01.c().b(t11.z, String.format("%s returned a null result. Treating it as a failure.", t11.this.f.c), new Throwable[0]);
                } else {
                    x01.c().a(t11.z, String.format("%s returned a %s result.", t11.this.f.c, aVar), new Throwable[0]);
                    t11.this.h = aVar;
                }
            } catch (CancellationException e) {
                x01.c().d(t11.z, String.format("%s was cancelled", this.c), e);
            } catch (InterruptedException | ExecutionException e2) {
                x01.c().b(t11.z, String.format("%s failed because it threw an exception/error", this.c), e2);
            } catch (Throwable th) {
                t11.this.f();
                throw th;
            }
            t11.this.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Context f3348a;
        @DexIgnore
        public ListenableWorker b;
        @DexIgnore
        public x21 c;
        @DexIgnore
        public k41 d;
        @DexIgnore
        public o01 e;
        @DexIgnore
        public WorkDatabase f;
        @DexIgnore
        public String g;
        @DexIgnore
        public List<n11> h;
        @DexIgnore
        public WorkerParameters.a i; // = new WorkerParameters.a();

        @DexIgnore
        public c(Context context, o01 o01, k41 k41, x21 x21, WorkDatabase workDatabase, String str) {
            this.f3348a = context.getApplicationContext();
            this.d = k41;
            this.c = x21;
            this.e = o01;
            this.f = workDatabase;
            this.g = str;
        }

        @DexIgnore
        public t11 a() {
            return new t11(this);
        }

        @DexIgnore
        public c b(WorkerParameters.a aVar) {
            if (aVar != null) {
                this.i = aVar;
            }
            return this;
        }

        @DexIgnore
        public c c(List<n11> list) {
            this.h = list;
            return this;
        }
    }

    @DexIgnore
    public t11(c cVar) {
        this.b = cVar.f3348a;
        this.j = cVar.d;
        this.k = cVar.c;
        this.c = cVar.g;
        this.d = cVar.h;
        this.e = cVar.i;
        this.g = cVar.b;
        this.i = cVar.e;
        WorkDatabase workDatabase = cVar.f;
        this.l = workDatabase;
        this.m = workDatabase.j();
        this.s = this.l.b();
        this.t = this.l.k();
    }

    @DexIgnore
    public final String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.c);
        sb.append(", tags={ ");
        boolean z2 = true;
        for (String str : list) {
            if (z2) {
                z2 = false;
            } else {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(" } ]");
        return sb.toString();
    }

    @DexIgnore
    public g64<Boolean> b() {
        return this.w;
    }

    @DexIgnore
    public final void c(ListenableWorker.a aVar) {
        if (aVar instanceof ListenableWorker.a.c) {
            x01.c().d(z, String.format("Worker result SUCCESS for %s", this.v), new Throwable[0]);
            if (this.f.d()) {
                h();
            } else {
                m();
            }
        } else if (aVar instanceof ListenableWorker.a.b) {
            x01.c().d(z, String.format("Worker result RETRY for %s", this.v), new Throwable[0]);
            g();
        } else {
            x01.c().d(z, String.format("Worker result FAILURE for %s", this.v), new Throwable[0]);
            if (this.f.d()) {
                h();
            } else {
                l();
            }
        }
    }

    @DexIgnore
    public void d() {
        boolean z2;
        this.y = true;
        n();
        g64<ListenableWorker.a> g64 = this.x;
        if (g64 != null) {
            z2 = g64.isDone();
            this.x.cancel(true);
        } else {
            z2 = false;
        }
        ListenableWorker listenableWorker = this.g;
        if (listenableWorker == null || z2) {
            x01.c().a(z, String.format("WorkSpec %s is already done. Not interrupting.", this.f), new Throwable[0]);
            return;
        }
        listenableWorker.n();
    }

    @DexIgnore
    public final void e(String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            if (this.m.n(str2) != f11.CANCELLED) {
                this.m.a(f11.FAILED, str2);
            }
            linkedList.addAll(this.s.b(str2));
        }
    }

    @DexIgnore
    public void f() {
        if (!n()) {
            this.l.beginTransaction();
            try {
                f11 n = this.m.n(this.c);
                this.l.i().b(this.c);
                if (n == null) {
                    i(false);
                } else if (n == f11.RUNNING) {
                    c(this.h);
                } else if (!n.isFinished()) {
                    g();
                }
                this.l.setTransactionSuccessful();
            } finally {
                this.l.endTransaction();
            }
        }
        List<n11> list = this.d;
        if (list != null) {
            for (n11 n11 : list) {
                n11.e(this.c);
            }
            o11.b(this.i, this.l, this.d);
        }
    }

    @DexIgnore
    public final void g() {
        this.l.beginTransaction();
        try {
            this.m.a(f11.ENQUEUED, this.c);
            this.m.s(this.c, System.currentTimeMillis());
            this.m.d(this.c, -1);
            this.l.setTransactionSuccessful();
        } finally {
            this.l.endTransaction();
            i(true);
        }
    }

    @DexIgnore
    public final void h() {
        this.l.beginTransaction();
        try {
            this.m.s(this.c, System.currentTimeMillis());
            this.m.a(f11.ENQUEUED, this.c);
            this.m.p(this.c);
            this.m.d(this.c, -1);
            this.l.setTransactionSuccessful();
        } finally {
            this.l.endTransaction();
            i(false);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void i(boolean z2) {
        boolean z3 = false;
        this.l.beginTransaction();
        try {
            List<String> l2 = this.l.j().l();
            if (l2 == null || l2.isEmpty()) {
                z3 = true;
            }
            if (z3) {
                y31.a(this.b, RescheduleReceiver.class, false);
            }
            if (z2) {
                this.m.d(this.c, -1);
            }
            if (!(this.f == null || this.g == null || !this.g.i())) {
                this.k.a(this.c);
            }
            this.l.setTransactionSuccessful();
            this.l.endTransaction();
            this.w.p(Boolean.valueOf(z2));
        } catch (Throwable th) {
            this.l.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public final void j() {
        f11 n = this.m.n(this.c);
        if (n == f11.RUNNING) {
            x01.c().a(z, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", this.c), new Throwable[0]);
            i(true);
            return;
        }
        x01.c().a(z, String.format("Status for %s is %s; not doing any work", this.c, n), new Throwable[0]);
        i(false);
    }

    @DexIgnore
    public final void k() {
        r01 b2;
        if (!n()) {
            this.l.beginTransaction();
            try {
                o31 o = this.m.o(this.c);
                this.f = o;
                if (o == null) {
                    x01.c().b(z, String.format("Didn't find WorkSpec for id %s", this.c), new Throwable[0]);
                    i(false);
                } else if (o.b != f11.ENQUEUED) {
                    j();
                    this.l.setTransactionSuccessful();
                    x01.c().a(z, String.format("%s is not in ENQUEUED state. Nothing more to do.", this.f.c), new Throwable[0]);
                    this.l.endTransaction();
                } else {
                    if (o.d() || this.f.c()) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (!(this.f.n == 0) && currentTimeMillis < this.f.a()) {
                            x01.c().a(z, String.format("Delaying execution for %s because it is being executed before schedule.", this.f.c), new Throwable[0]);
                            i(true);
                            this.l.endTransaction();
                            return;
                        }
                    }
                    this.l.setTransactionSuccessful();
                    this.l.endTransaction();
                    if (this.f.d()) {
                        b2 = this.f.e;
                    } else {
                        v01 b3 = this.i.c().b(this.f.d);
                        if (b3 == null) {
                            x01.c().b(z, String.format("Could not create Input Merger %s", this.f.d), new Throwable[0]);
                            l();
                            return;
                        }
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(this.f.e);
                        arrayList.addAll(this.m.q(this.c));
                        b2 = b3.b(arrayList);
                    }
                    WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.c), b2, this.u, this.e, this.f.k, this.i.b(), this.j, this.i.j(), new f41(this.l, this.j), new e41(this.l, this.k, this.j));
                    if (this.g == null) {
                        this.g = this.i.j().b(this.b, this.f.c, workerParameters);
                    }
                    ListenableWorker listenableWorker = this.g;
                    if (listenableWorker == null) {
                        x01.c().b(z, String.format("Could not create Worker %s", this.f.c), new Throwable[0]);
                        l();
                    } else if (listenableWorker.j()) {
                        x01.c().b(z, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", this.f.c), new Throwable[0]);
                        l();
                    } else {
                        this.g.l();
                        if (!o()) {
                            j();
                        } else if (!n()) {
                            j41 t2 = j41.t();
                            this.j.a().execute(new a(t2));
                            t2.c(new b(t2, this.v), this.j.c());
                        }
                    }
                }
            } finally {
                this.l.endTransaction();
            }
        }
    }

    @DexIgnore
    public void l() {
        this.l.beginTransaction();
        try {
            e(this.c);
            this.m.j(this.c, ((ListenableWorker.a.C0005a) this.h).e());
            this.l.setTransactionSuccessful();
        } finally {
            this.l.endTransaction();
            i(false);
        }
    }

    @DexIgnore
    public final void m() {
        this.l.beginTransaction();
        try {
            this.m.a(f11.SUCCEEDED, this.c);
            this.m.j(this.c, ((ListenableWorker.a.c) this.h).e());
            long currentTimeMillis = System.currentTimeMillis();
            for (String str : this.s.b(this.c)) {
                if (this.m.n(str) == f11.BLOCKED && this.s.c(str)) {
                    x01.c().d(z, String.format("Setting status to enqueued for %s", str), new Throwable[0]);
                    this.m.a(f11.ENQUEUED, str);
                    this.m.s(str, currentTimeMillis);
                }
            }
            this.l.setTransactionSuccessful();
        } finally {
            this.l.endTransaction();
            i(false);
        }
    }

    @DexIgnore
    public final boolean n() {
        if (!this.y) {
            return false;
        }
        x01.c().a(z, String.format("Work interrupted for %s", this.v), new Throwable[0]);
        f11 n = this.m.n(this.c);
        if (n == null) {
            i(false);
            return true;
        }
        i(!n.isFinished());
        return true;
    }

    @DexIgnore
    public final boolean o() {
        boolean z2 = true;
        this.l.beginTransaction();
        try {
            if (this.m.n(this.c) == f11.ENQUEUED) {
                this.m.a(f11.RUNNING, this.c);
                this.m.r(this.c);
            } else {
                z2 = false;
            }
            this.l.setTransactionSuccessful();
            return z2;
        } finally {
            this.l.endTransaction();
        }
    }

    @DexIgnore
    public void run() {
        List<String> b2 = this.t.b(this.c);
        this.u = b2;
        this.v = a(b2);
        k();
    }
}
