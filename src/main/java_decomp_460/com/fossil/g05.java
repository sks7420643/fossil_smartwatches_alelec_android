package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<DianaPresetWatchAppSetting>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<DianaPresetWatchAppSetting>> {
    }

    @DexIgnore
    public final String a(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        pq7.c(arrayList, "configurationList");
        if (arrayList.isEmpty()) {
            return "";
        }
        return new Gson().u(arrayList, new a().getType());
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> b(String str) {
        pq7.c(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object l = new Gson().l(str, new b().getType());
        pq7.b(l, "Gson().fromJson(data, type)");
        return (ArrayList) l;
    }
}
