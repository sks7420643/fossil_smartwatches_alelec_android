package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iw1 extends ox1 implements Parcelable, Serializable, nx1 {
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public /* final */ ry1 c;
    @DexIgnore
    public /* final */ yb0 d;
    @DexIgnore
    public /* final */ cc0[] e;
    @DexIgnore
    public /* final */ cc0[] f;
    @DexIgnore
    public /* final */ cc0[] g;
    @DexIgnore
    public /* final */ cc0[] h;
    @DexIgnore
    public /* final */ cc0[] i;
    @DexIgnore
    public /* final */ cc0[] j;
    @DexIgnore
    public /* final */ cc0[] k;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public iw1(android.os.Parcel r12) {
        /*
            r11 = this;
            r10 = 0
            java.lang.Class<com.fossil.ry1> r0 = com.fossil.ry1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r1 = r12.readParcelable(r0)
            if (r1 == 0) goto L_0x00ab
            com.fossil.ry1 r1 = (com.fossil.ry1) r1
            java.lang.Class<com.fossil.yb0> r0 = com.fossil.yb0.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r2 = r12.readParcelable(r0)
            if (r2 == 0) goto L_0x00a7
            com.fossil.yb0 r2 = (com.fossil.yb0) r2
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r3 = r12.createTypedArray(r0)
            if (r3 == 0) goto L_0x00a3
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r3, r0)
            com.fossil.cc0[] r3 = (com.fossil.cc0[]) r3
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r4 = r12.createTypedArray(r0)
            if (r4 == 0) goto L_0x009f
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r4, r0)
            com.fossil.cc0[] r4 = (com.fossil.cc0[]) r4
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r5 = r12.createTypedArray(r0)
            if (r5 == 0) goto L_0x009b
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r5, r0)
            com.fossil.cc0[] r5 = (com.fossil.cc0[]) r5
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r6 = r12.createTypedArray(r0)
            if (r6 == 0) goto L_0x0097
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r6, r0)
            com.fossil.cc0[] r6 = (com.fossil.cc0[]) r6
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r7 = r12.createTypedArray(r0)
            if (r7 == 0) goto L_0x0093
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r7, r0)
            com.fossil.cc0[] r7 = (com.fossil.cc0[]) r7
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r8 = r12.createTypedArray(r0)
            if (r8 == 0) goto L_0x008f
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r8, r0)
            com.fossil.cc0[] r8 = (com.fossil.cc0[]) r8
            com.fossil.bc0 r0 = com.fossil.cc0.CREATOR
            java.lang.Object[] r9 = r12.createTypedArray(r0)
            if (r9 == 0) goto L_0x008b
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.fossil.pq7.b(r9, r0)
            com.fossil.cc0[] r9 = (com.fossil.cc0[]) r9
            r0 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            return
        L_0x008b:
            com.fossil.pq7.i()
            throw r10
        L_0x008f:
            com.fossil.pq7.i()
            throw r10
        L_0x0093:
            com.fossil.pq7.i()
            throw r10
        L_0x0097:
            com.fossil.pq7.i()
            throw r10
        L_0x009b:
            com.fossil.pq7.i()
            throw r10
        L_0x009f:
            com.fossil.pq7.i()
            throw r10
        L_0x00a3:
            com.fossil.pq7.i()
            throw r10
        L_0x00a7:
            com.fossil.pq7.i()
            throw r10
        L_0x00ab:
            com.fossil.pq7.i()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iw1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public iw1(ry1 ry1, yb0 yb0, cc0[] cc0Arr, cc0[] cc0Arr2, cc0[] cc0Arr3, cc0[] cc0Arr4, cc0[] cc0Arr5, cc0[] cc0Arr6, cc0[] cc0Arr7) {
        this.c = ry1;
        this.d = yb0;
        this.e = cc0Arr;
        this.f = cc0Arr2;
        this.g = cc0Arr3;
        this.h = cc0Arr4;
        this.i = cc0Arr5;
        this.j = cc0Arr6;
        this.k = cc0Arr7;
    }

    @DexIgnore
    public final cc0[] a() {
        return this.j;
    }

    @DexIgnore
    public final cc0[] b() {
        return this.f;
    }

    @DexIgnore
    public final cc0[] c() {
        return this.i;
    }

    @DexIgnore
    @Override // java.lang.Object
    public abstract iw1 clone();

    @DexIgnore
    @Override // java.lang.Object
    public abstract /* synthetic */ nx1 clone();

    @DexIgnore
    public final cc0[] d() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final cc0[] e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof iw1)) {
            return false;
        }
        iw1 iw1 = (iw1) obj;
        if (!pq7.a(this.c, iw1.c)) {
            return false;
        }
        if (!pq7.a(this.d, iw1.d)) {
            return false;
        }
        if (!Arrays.equals(this.e, iw1.e)) {
            return false;
        }
        if (!Arrays.equals(this.f, iw1.f)) {
            return false;
        }
        if (!Arrays.equals(this.g, iw1.g)) {
            return false;
        }
        if (!Arrays.equals(this.h, iw1.h)) {
            return false;
        }
        if (!Arrays.equals(this.i, iw1.i)) {
            return false;
        }
        if (!Arrays.equals(this.j, iw1.j)) {
            return false;
        }
        return Arrays.equals(this.k, iw1.k);
    }

    @DexIgnore
    public final cc0[] f() {
        return this.e;
    }

    @DexIgnore
    public final yb0 g() {
        return this.d;
    }

    @DexIgnore
    public final String getBundleId() {
        return this.e[0].b;
    }

    @DexIgnore
    public final byte[] getData() {
        byte[] bArr;
        if (this.b == null) {
            try {
                bArr = ga.d.a(5630, this.c, this);
            } catch (sx1 e2) {
                bArr = null;
            }
            this.b = bArr;
        }
        byte[] bArr2 = this.b;
        return bArr2 != null ? bArr2 : new byte[0];
    }

    @DexIgnore
    public final String getDisplayName() {
        cc0 cc0;
        cc0[] cc0Arr = this.i;
        int length = cc0Arr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                cc0 = null;
                break;
            }
            cc0 cc02 = cc0Arr[i2];
            if (pq7.a(cc02.b, "display_name")) {
                cc0 = cc02;
                break;
            }
            i2++;
        }
        if (cc0 == null) {
            return null;
        }
        if (!(!(cc0.c.length == 0))) {
            return null;
        }
        byte[] bArr = cc0.c;
        return new String(dm7.k(bArr, 0, bArr.length - 1), hd0.y.c());
    }

    @DexIgnore
    public final long getPackageCrc() {
        if (getData().length < 4) {
            return 0;
        }
        ByteBuffer order = ByteBuffer.wrap(dm7.k(getData(), getData().length - 4, getData().length)).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        return hy1.o(order.getInt());
    }

    @DexIgnore
    public final jw1 getPackageType() {
        return this.d.b;
    }

    @DexIgnore
    public final ry1 getPackageVersion() {
        return this.d.c;
    }

    @DexIgnore
    public final ry1 h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + Arrays.hashCode(this.e)) * 31) + Arrays.hashCode(this.f)) * 31) + Arrays.hashCode(this.g)) * 31) + Arrays.hashCode(this.h)) * 31) + Arrays.hashCode(this.i)) * 31) + Arrays.hashCode(this.j)) * 31) + Arrays.hashCode(this.k);
    }

    @DexIgnore
    public final cc0[] i() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return gy1.d(gy1.d(gy1.d(gy1.d(gy1.d(gy1.d(gy1.d(gy1.d(new JSONObject(), jd0.Q4, this.d.toJSONObject()), jd0.R4, px1.a(this.e)), jd0.S4, px1.a(this.f)), jd0.T4, px1.a(this.g)), jd0.U4, px1.a(this.h)), jd0.V4, px1.a(this.e)), jd0.W4, px1.a(this.e)), jd0.X4, px1.a(this.k));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.c, i2);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.e, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.f, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.g, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.h, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.i, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.j, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.k, i2);
        }
    }
}
