package com.fossil;

import android.util.Base64;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zg4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ byte f4468a; // = Byte.parseByte("01110000", 2);
    @DexIgnore
    public static /* final */ byte b; // = Byte.parseByte("00001111", 2);

    @DexIgnore
    public static String b(byte[] bArr) {
        return new String(Base64.encode(bArr, 11), Charset.defaultCharset()).substring(0, 22);
    }

    @DexIgnore
    public static byte[] c(UUID uuid, byte[] bArr) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }

    @DexIgnore
    public String a() {
        byte[] c = c(UUID.randomUUID(), new byte[17]);
        c[16] = (byte) c[0];
        c[0] = (byte) ((byte) ((b & c[0]) | f4468a));
        return b(c);
    }
}
