package com.fossil;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ExecutorService f3217a;
    @DexIgnore
    public nt3<Void> b; // = qt3.f(null);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public ThreadLocal<Boolean> d; // = new ThreadLocal<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            s84.this.d.set(Boolean.TRUE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Runnable f3218a;

        @DexIgnore
        public b(s84 s84, Runnable runnable) {
            this.f3218a = runnable;
        }

        @DexIgnore
        /* renamed from: a */
        public Void call() throws Exception {
            this.f3218a.run();
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ft3<Void, T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ Callable f3219a;

        @DexIgnore
        public c(s84 s84, Callable callable) {
            this.f3219a = callable;
        }

        @DexIgnore
        @Override // com.fossil.ft3
        public T then(nt3<Void> nt3) throws Exception {
            return (T) this.f3219a.call();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ft3<T, Void> {
        @DexIgnore
        public d(s84 s84) {
        }

        @DexIgnore
        /* renamed from: a */
        public Void then(nt3<T> nt3) throws Exception {
            return null;
        }
    }

    @DexIgnore
    public s84(ExecutorService executorService) {
        this.f3217a = executorService;
        executorService.submit(new a());
    }

    @DexIgnore
    public void b() {
        if (!e()) {
            throw new IllegalStateException("Not running on background worker thread as intended.");
        }
    }

    @DexIgnore
    public Executor c() {
        return this.f3217a;
    }

    @DexIgnore
    public final <T> nt3<Void> d(nt3<T> nt3) {
        return nt3.i(this.f3217a, new d(this));
    }

    @DexIgnore
    public final boolean e() {
        return Boolean.TRUE.equals(this.d.get());
    }

    @DexIgnore
    public final <T> ft3<Void, T> f(Callable<T> callable) {
        return new c(this, callable);
    }

    @DexIgnore
    public nt3<Void> g(Runnable runnable) {
        return h(new b(this, runnable));
    }

    @DexIgnore
    public <T> nt3<T> h(Callable<T> callable) {
        nt3<T> i;
        synchronized (this.c) {
            i = this.b.i(this.f3217a, f(callable));
            this.b = d(i);
        }
        return i;
    }

    @DexIgnore
    public <T> nt3<T> i(Callable<nt3<T>> callable) {
        nt3<T> k;
        synchronized (this.c) {
            k = this.b.k(this.f3217a, f(callable));
            this.b = d(k);
        }
        return k;
    }
}
