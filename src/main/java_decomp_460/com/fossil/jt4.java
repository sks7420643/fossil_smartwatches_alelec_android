package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jt4 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    LiveData<it4> b();

    @DexIgnore
    long c(it4 it4);

    @DexIgnore
    it4 d();
}
