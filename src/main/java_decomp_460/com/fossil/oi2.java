package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oi2 implements Parcelable.Creator<DataType> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataType createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        String str = null;
        String str2 = null;
        ArrayList arrayList = null;
        String str3 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                str3 = ad2.f(parcel, t);
            } else if (l == 2) {
                arrayList = ad2.j(parcel, t, wh2.CREATOR);
            } else if (l == 3) {
                str2 = ad2.f(parcel, t);
            } else if (l != 4) {
                ad2.B(parcel, t);
            } else {
                str = ad2.f(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new DataType(str3, arrayList, str2, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataType[] newArray(int i) {
        return new DataType[i];
    }
}
