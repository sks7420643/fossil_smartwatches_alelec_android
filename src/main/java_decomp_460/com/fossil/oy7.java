package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class oy7 {
    @DexIgnore
    public static final <E> Object a(vy7<? extends E> vy7, qn7<? super E> qn7) {
        if (vy7 != null) {
            return vy7.a(qn7);
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.channels.ReceiveChannel<E?>");
    }
}
