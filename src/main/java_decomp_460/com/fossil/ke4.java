package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ke4 {
    @DexIgnore
    public static ke4 b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SharedPreferences f1905a;

    @DexIgnore
    public ke4(Context context) {
        this.f1905a = context.getSharedPreferences("FirebaseAppHeartBeat", 0);
    }

    @DexIgnore
    public static ke4 a(Context context) {
        ke4 ke4;
        synchronized (ke4.class) {
            try {
                if (b == null) {
                    b = new ke4(context);
                }
                ke4 = b;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ke4;
    }

    @DexIgnore
    public boolean b(long j) {
        boolean c;
        synchronized (this) {
            c = c("fire-global", j);
        }
        return c;
    }

    @DexIgnore
    public boolean c(String str, long j) {
        synchronized (this) {
            if (!this.f1905a.contains(str)) {
                this.f1905a.edit().putLong(str, j).apply();
                return true;
            } else if (j - this.f1905a.getLong(str, -1) < LogBuilder.MAX_INTERVAL) {
                return false;
            } else {
                this.f1905a.edit().putLong(str, j).apply();
                return true;
            }
        }
    }
}
