package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class el3 extends ss2 implements cl3 {
    @DexIgnore
    public el3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void D1(xr3 xr3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, xr3);
        i(13, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void E1(vg3 vg3, or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, vg3);
        qt2.c(d, or3);
        i(1, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void K1(vg3 vg3, String str, String str2) throws RemoteException {
        Parcel d = d();
        qt2.c(d, vg3);
        d.writeString(str);
        d.writeString(str2);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void N0(long j, String str, String str2, String str3) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        d.writeString(str);
        d.writeString(str2);
        d.writeString(str3);
        i(10, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void P1(or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, or3);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final List<fr3> S(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        d.writeString(str3);
        qt2.d(d, z);
        Parcel e = e(15, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(fr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void S0(or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, or3);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final List<xr3> T0(String str, String str2, String str3) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        d.writeString(str3);
        Parcel e = e(17, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(xr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final List<xr3> V0(String str, String str2, or3 or3) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.c(d, or3);
        Parcel e = e(16, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(xr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final String n0(or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, or3);
        Parcel e = e(11, d);
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void n2(Bundle bundle, or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, bundle);
        qt2.c(d, or3);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final List<fr3> o1(String str, String str2, boolean z, or3 or3) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        qt2.d(d, z);
        qt2.c(d, or3);
        Parcel e = e(14, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(fr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final List<fr3> p1(or3 or3, boolean z) throws RemoteException {
        Parcel d = d();
        qt2.c(d, or3);
        qt2.d(d, z);
        Parcel e = e(7, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(fr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void r1(or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, or3);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void s(xr3 xr3, or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, xr3);
        qt2.c(d, or3);
        i(12, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final void u2(fr3 fr3, or3 or3) throws RemoteException {
        Parcel d = d();
        qt2.c(d, fr3);
        qt2.c(d, or3);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.cl3
    public final byte[] w2(vg3 vg3, String str) throws RemoteException {
        Parcel d = d();
        qt2.c(d, vg3);
        d.writeString(str);
        Parcel e = e(9, d);
        byte[] createByteArray = e.createByteArray();
        e.recycle();
        return createByteArray;
    }
}
