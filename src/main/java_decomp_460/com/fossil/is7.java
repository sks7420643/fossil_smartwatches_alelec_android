package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface is7<R> extends ls7<R>, Object<R> {

    @DexIgnore
    public interface a<R> extends hs7<R>, rp7<R, tl7> {
    }

    @DexIgnore
    a<R> getSetter();
}
