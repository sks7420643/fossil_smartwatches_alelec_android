package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bo1 extends ox1 implements Parcelable {
    @DexIgnore
    public /* final */ do1 b;

    @DexIgnore
    public bo1(Parcel parcel) {
        this(do1.values()[parcel.readInt()]);
    }

    @DexIgnore
    public bo1(do1 do1) {
        this.b = do1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final do1 getActionType() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(new JSONObject(), jd0.e, this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
    }
}
