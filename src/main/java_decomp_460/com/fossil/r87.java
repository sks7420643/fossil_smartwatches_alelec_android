package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r87 {
    @DexIgnore
    public static final o87 a(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? o87.Companion.a() : o87.DARK_GRAY : o87.LIGHT_GRAY : o87.WHITE : o87.BLACK;
    }

    @DexIgnore
    public static final o87 b(nb7 nb7) {
        pq7.c(nb7, "$this$toColorSpace");
        int i = q87.f2939a[nb7.ordinal()];
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? o87.Companion.a() : o87.DARK_GRAY : o87.LIGHT_GRAY : o87.WHITE : o87.BLACK;
    }
}
