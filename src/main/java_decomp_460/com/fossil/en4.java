package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en4 extends mn4 {
    @DexIgnore
    @Override // com.fossil.jn4, com.fossil.ql4
    public bm4 a(String str, kl4 kl4, int i, int i2, Map<ml4, ?> map) throws rl4 {
        if (kl4 == kl4.EAN_13) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_13, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.jn4
    public boolean[] c(String str) {
        if (str.length() == 13) {
            try {
                if (ln4.a(str)) {
                    int i = dn4.f[Integer.parseInt(str.substring(0, 1))];
                    boolean[] zArr = new boolean[95];
                    int b = jn4.b(zArr, 0, ln4.f2223a, true) + 0;
                    int i2 = 1;
                    while (i2 <= 6) {
                        int i3 = i2 + 1;
                        int parseInt = Integer.parseInt(str.substring(i2, i3));
                        if (((i >> (6 - i2)) & 1) == 1) {
                            parseInt += 10;
                        }
                        b = jn4.b(zArr, b, ln4.e[parseInt], false) + b;
                        i2 = i3;
                    }
                    int b2 = b + jn4.b(zArr, b, ln4.b, false);
                    int i4 = 7;
                    int i5 = b2;
                    while (i4 <= 12) {
                        int i6 = i4 + 1;
                        i5 += jn4.b(zArr, i5, ln4.d[Integer.parseInt(str.substring(i4, i6))], true);
                        i4 = i6;
                    }
                    jn4.b(zArr, i5, ln4.f2223a, true);
                    return zArr;
                }
                throw new IllegalArgumentException("Contents do not pass checksum");
            } catch (nl4 e) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 13 digits long, but got " + str.length());
        }
    }
}
