package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fg1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ fg1 f1123a; // = new c();
    @DexIgnore
    public static /* final */ fg1 b; // = new a();
    @DexIgnore
    public static /* final */ fg1 c; // = new b();
    @DexIgnore
    public static /* final */ fg1 d; // = new d();
    @DexIgnore
    public static /* final */ fg1 e;
    @DexIgnore
    public static /* final */ nb1<fg1> f;
    @DexIgnore
    public static /* final */ boolean g; // = (Build.VERSION.SDK_INT >= 19);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends fg1 {
        @DexIgnore
        @Override // com.fossil.fg1
        public e a(int i, int i2, int i3, int i4) {
            return b(i, i2, i3, i4) == 1.0f ? e.QUALITY : fg1.f1123a.a(i, i2, i3, i4);
        }

        @DexIgnore
        @Override // com.fossil.fg1
        public float b(int i, int i2, int i3, int i4) {
            return Math.min(1.0f, fg1.f1123a.b(i, i2, i3, i4));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends fg1 {
        @DexIgnore
        @Override // com.fossil.fg1
        public e a(int i, int i2, int i3, int i4) {
            return e.QUALITY;
        }

        @DexIgnore
        @Override // com.fossil.fg1
        public float b(int i, int i2, int i3, int i4) {
            return Math.max(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends fg1 {
        @DexIgnore
        @Override // com.fossil.fg1
        public e a(int i, int i2, int i3, int i4) {
            return fg1.g ? e.QUALITY : e.MEMORY;
        }

        @DexIgnore
        @Override // com.fossil.fg1
        public float b(int i, int i2, int i3, int i4) {
            if (fg1.g) {
                return Math.min(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
            }
            int max = Math.max(i2 / i4, i / i3);
            if (max != 0) {
                return 1.0f / ((float) Integer.highestOneBit(max));
            }
            return 1.0f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends fg1 {
        @DexIgnore
        @Override // com.fossil.fg1
        public e a(int i, int i2, int i3, int i4) {
            return e.QUALITY;
        }

        @DexIgnore
        @Override // com.fossil.fg1
        public float b(int i, int i2, int i3, int i4) {
            return 1.0f;
        }
    }

    @DexIgnore
    public enum e {
        MEMORY,
        QUALITY
    }

    /*
    static {
        fg1 fg1 = c;
        e = fg1;
        f = nb1.f("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", fg1);
    }
    */

    @DexIgnore
    public abstract e a(int i, int i2, int i3, int i4);

    @DexIgnore
    public abstract float b(int i, int i2, int i3, int i4);
}
