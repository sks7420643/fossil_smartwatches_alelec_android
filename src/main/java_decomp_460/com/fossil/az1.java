package com.fossil;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class az1 implements a02 {
    @DexIgnore
    public static /* final */ String c; // = uz1.a("hts/frbslgiggolai.o/0clgbthfra=snpoo", "tp:/ieaeogn.ogepscmvc/o/ac?omtjo_rt3");
    @DexIgnore
    public static /* final */ String d; // = uz1.a("hts/frbslgigp.ogepscmv/ieo/eaybtho", "tp:/ieaeogn-agolai.o/1frlglgc/aclg");
    @DexIgnore
    public static /* final */ String e; // = uz1.a("AzSCki82AwsLzKd5O8zo", "IayckHiZRO1EFl1aGoK");
    @DexIgnore
    public static /* final */ Set<ty1> f; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(ty1.b("proto"), ty1.b("json"))));
    @DexIgnore
    public static /* final */ az1 g; // = new az1(d, e);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f361a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public az1(String str, String str2) {
        this.f361a = str;
        this.b = str2;
    }

    @DexIgnore
    public static az1 c(byte[] bArr) {
        String str = new String(bArr, Charset.forName("UTF-8"));
        if (str.startsWith("1$")) {
            String[] split = str.substring(2).split(Pattern.quote("\\"), 2);
            if (split.length == 2) {
                String str2 = split[0];
                if (!str2.isEmpty()) {
                    String str3 = split[1];
                    if (str3.isEmpty()) {
                        str3 = null;
                    }
                    return new az1(str2, str3);
                }
                throw new IllegalArgumentException("Missing endpoint in CCTDestination extras");
            }
            throw new IllegalArgumentException("Extra is not a valid encoded LegacyFlgDestination");
        }
        throw new IllegalArgumentException("Version marker missing from extras");
    }

    @DexIgnore
    @Override // com.fossil.a02
    public Set<ty1> a() {
        return f;
    }

    @DexIgnore
    public byte[] b() {
        if (this.b == null && this.f361a == null) {
            return null;
        }
        String str = this.f361a;
        String str2 = this.b;
        if (str2 == null) {
            str2 = "";
        }
        return String.format("%s%s%s%s", "1$", str, "\\", str2).getBytes(Charset.forName("UTF-8"));
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public String e() {
        return this.f361a;
    }

    @DexIgnore
    @Override // com.fossil.zz1
    public byte[] getExtras() {
        return b();
    }

    @DexIgnore
    @Override // com.fossil.zz1
    public String getName() {
        return "cct";
    }
}
