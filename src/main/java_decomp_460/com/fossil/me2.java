package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class me2 extends rl2 implements le2 {
    @DexIgnore
    public me2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    @Override // com.fossil.le2
    public final rg2 zzb() throws RemoteException {
        Parcel e = e(1, d());
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.le2
    public final int zzc() throws RemoteException {
        Parcel e = e(2, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }
}
