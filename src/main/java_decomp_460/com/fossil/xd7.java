package com.fossil;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Transformation;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xd7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ StringBuilder f4099a; // = new StringBuilder();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Handler {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message message) {
            sendMessageDelayed(obtainMessage(), 1000);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(11)
    public static class b {
        @DexIgnore
        public static int a(ActivityManager activityManager) {
            return activityManager.getLargeMemoryClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(12)
    public static class c {
        @DexIgnore
        public static int a(Bitmap bitmap) {
            return bitmap.getByteCount();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public static Downloader a(Context context) {
            return new md7(context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends Thread {
        @DexIgnore
        public e(Runnable runnable) {
            super(runnable);
        }

        @DexIgnore
        public void run() {
            Process.setThreadPriority(10);
            super.run();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements ThreadFactory {
        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new e(runnable);
        }
    }

    @DexIgnore
    public static long a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j = (((long) statFs.getBlockSize()) * ((long) statFs.getBlockCount())) / 50;
        } catch (IllegalArgumentException e2) {
            j = 5242880;
        }
        return Math.max(Math.min(j, 52428800L), 5242880L);
    }

    @DexIgnore
    public static int b(Context context) {
        ActivityManager activityManager = (ActivityManager) p(context, Constants.ACTIVITY);
        return (((!((context.getApplicationInfo().flags & 1048576) != 0) || Build.VERSION.SDK_INT < 11) ? activityManager.getMemoryClass() : b.a(activityManager)) * 1048576) / 7;
    }

    @DexIgnore
    public static void c() {
        if (!s()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    @DexIgnore
    public static <T> T d(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static void e(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
            }
        }
    }

    @DexIgnore
    public static File f(Context context) {
        File file = new File(context.getApplicationContext().getCacheDir(), "picasso-cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    @DexIgnore
    public static Downloader g(Context context) {
        try {
            Class.forName("com.squareup.okhttp.OkHttpClient");
            return d.a(context);
        } catch (ClassNotFoundException e2) {
            return new wd7(context);
        }
    }

    @DexIgnore
    public static String h(pd7 pd7) {
        String i = i(pd7, f4099a);
        f4099a.setLength(0);
        return i;
    }

    @DexIgnore
    public static String i(pd7 pd7, StringBuilder sb) {
        String str = pd7.f;
        if (str != null) {
            sb.ensureCapacity(str.length() + 50);
            sb.append(pd7.f);
        } else {
            Uri uri = pd7.d;
            if (uri != null) {
                String uri2 = uri.toString();
                sb.ensureCapacity(uri2.length() + 50);
                sb.append(uri2);
            } else {
                sb.ensureCapacity(50);
                sb.append(pd7.e);
            }
        }
        sb.append('\n');
        if (pd7.m != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            sb.append("rotation:");
            sb.append(pd7.m);
            if (pd7.p) {
                sb.append('@');
                sb.append(pd7.n);
                sb.append('x');
                sb.append(pd7.o);
            }
            sb.append('\n');
        }
        if (pd7.c()) {
            sb.append("resize:");
            sb.append(pd7.h);
            sb.append('x');
            sb.append(pd7.i);
            sb.append('\n');
        }
        if (pd7.j) {
            sb.append("centerCrop");
            sb.append('\n');
        } else if (pd7.k) {
            sb.append("centerInside");
            sb.append('\n');
        }
        List<Transformation> list = pd7.g;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                sb.append(pd7.g.get(i).key());
                sb.append('\n');
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static void j(Looper looper) {
        a aVar = new a(looper);
        aVar.sendMessageDelayed(aVar.obtainMessage(), 1000);
    }

    @DexIgnore
    public static int k(Bitmap bitmap) {
        int a2 = Build.VERSION.SDK_INT >= 12 ? c.a(bitmap) : bitmap.getRowBytes() * bitmap.getHeight();
        if (a2 >= 0) {
            return a2;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    @DexIgnore
    public static String l(xc7 xc7) {
        return m(xc7, "");
    }

    @DexIgnore
    public static String m(xc7 xc7, String str) {
        StringBuilder sb = new StringBuilder(str);
        vc7 h = xc7.h();
        if (h != null) {
            sb.append(h.b.d());
        }
        List<vc7> i = xc7.i();
        if (i != null) {
            int size = i.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (i2 > 0 || h != null) {
                    sb.append(", ");
                }
                sb.append(i.get(i2).b.d());
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static int n(Resources resources, pd7 pd7) throws FileNotFoundException {
        Uri uri;
        if (pd7.e != 0 || (uri = pd7.d) == null) {
            return pd7.e;
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            List<String> pathSegments = pd7.d.getPathSegments();
            if (pathSegments == null || pathSegments.isEmpty()) {
                throw new FileNotFoundException("No path segments: " + pd7.d);
            } else if (pathSegments.size() == 1) {
                try {
                    return Integer.parseInt(pathSegments.get(0));
                } catch (NumberFormatException e2) {
                    throw new FileNotFoundException("Last path segment is not a resource ID: " + pd7.d);
                }
            } else if (pathSegments.size() == 2) {
                return resources.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
            } else {
                throw new FileNotFoundException("More than two path segments: " + pd7.d);
            }
        } else {
            throw new FileNotFoundException("No package provided: " + pd7.d);
        }
    }

    @DexIgnore
    public static Resources o(Context context, pd7 pd7) throws FileNotFoundException {
        Uri uri;
        if (pd7.e != 0 || (uri = pd7.d) == null) {
            return context.getResources();
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            try {
                return context.getPackageManager().getResourcesForApplication(authority);
            } catch (PackageManager.NameNotFoundException e2) {
                throw new FileNotFoundException("Unable to obtain resources for package: " + pd7.d);
            }
        } else {
            throw new FileNotFoundException("No package provided: " + pd7.d);
        }
    }

    @DexIgnore
    public static <T> T p(Context context, String str) {
        return (T) context.getSystemService(str);
    }

    @DexIgnore
    public static boolean q(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    @DexIgnore
    public static boolean r(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    @DexIgnore
    public static boolean s() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    @DexIgnore
    public static boolean t(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[12];
        return inputStream.read(bArr, 0, 12) == 12 && "RIFF".equals(new String(bArr, 0, 4, "US-ASCII")) && "WEBP".equals(new String(bArr, 8, 4, "US-ASCII"));
    }

    @DexIgnore
    public static void u(String str, String str2, String str3) {
        v(str, str2, str3, "");
    }

    @DexIgnore
    public static void v(String str, String str2, String str3, String str4) {
        Log.d("Picasso", String.format("%1$-11s %2$-12s %3$s %4$s", str, str2, str3, str4));
    }

    @DexIgnore
    public static boolean w(String str) {
        boolean z = true;
        if (str == null) {
            return false;
        }
        String[] split = str.split(" ", 2);
        if ("CACHE".equals(split[0])) {
            return true;
        }
        if (split.length == 1) {
            return false;
        }
        try {
            if (!"CONDITIONAL_CACHE".equals(split[0]) || Integer.parseInt(split[1]) != 304) {
                z = false;
            }
        } catch (NumberFormatException e2) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public static byte[] x(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
