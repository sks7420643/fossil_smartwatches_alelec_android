package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.m62;
import com.fossil.m62.b;
import com.fossil.z62;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i72<R extends z62, A extends m62.b> extends BasePendingResult<R> implements j72<R> {
    @DexIgnore
    public /* final */ m62.c<A> q;
    @DexIgnore
    public /* final */ m62<?> r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i72(m62<?> m62, r62 r62) {
        super(r62);
        rc2.l(r62, "GoogleApiClient must not be null");
        rc2.l(m62, "Api must not be null");
        this.q = (m62.c<A>) m62.a();
        this.r = m62;
    }

    @DexIgnore
    public final void A(Status status) {
        rc2.b(!status.D(), "Failed result must not be success");
        R f = f(status);
        j(f);
        x(f);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.i72<R extends com.fossil.z62, A extends com.fossil.m62$b> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.j72
    public /* bridge */ /* synthetic */ void a(Object obj) {
        super.j((z62) obj);
    }

    @DexIgnore
    public abstract void u(A a2) throws RemoteException;

    @DexIgnore
    public final m62<?> v() {
        return this.r;
    }

    @DexIgnore
    public final m62.c<A> w() {
        return this.q;
    }

    @DexIgnore
    public void x(R r2) {
    }

    @DexIgnore
    public final void y(A a2) throws DeadObjectException {
        if (a2 instanceof wc2) {
            a2 = ((wc2) a2).t0();
        }
        try {
            u(a2);
        } catch (DeadObjectException e) {
            z(e);
            throw e;
        } catch (RemoteException e2) {
            z(e2);
        }
    }

    @DexIgnore
    public final void z(RemoteException remoteException) {
        A(new Status(8, remoteException.getLocalizedMessage(), null));
    }
}
