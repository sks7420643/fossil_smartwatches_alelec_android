package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u85 extends t85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362546, 1);
        F.put(2131363458, 2);
        F.put(2131362376, 3);
        F.put(2131362674, 4);
        F.put(2131363459, 5);
        F.put(2131362375, 6);
        F.put(2131362673, 7);
        F.put(2131363460, 8);
        F.put(2131362484, 9);
        F.put(2131362732, 10);
        F.put(2131363461, 11);
        F.put(2131362774, 12);
    }
    */

    @DexIgnore
    public u85(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 13, E, F));
    }

    @DexIgnore
    public u85(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[1], (RTLImageView) objArr[7], (RTLImageView) objArr[4], (RTLImageView) objArr[10], (RTLImageView) objArr[12], (View) objArr[2], (View) objArr[5], (View) objArr[8], (View) objArr[11]);
        this.D = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.C = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.D != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.D = 1;
        }
        w();
    }
}
