package com.fossil;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h41<V> implements g64<V> {
    @DexIgnore
    public static /* final */ boolean e; // = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(h41.class.getName());
    @DexIgnore
    public static /* final */ b g;
    @DexIgnore
    public static /* final */ Object h; // = new Object();
    @DexIgnore
    public volatile Object b;
    @DexIgnore
    public volatile e c;
    @DexIgnore
    public volatile i d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public abstract boolean a(h41<?> h41, e eVar, e eVar2);

        @DexIgnore
        public abstract boolean b(h41<?> h41, Object obj, Object obj2);

        @DexIgnore
        public abstract boolean c(h41<?> h41, i iVar, i iVar2);

        @DexIgnore
        public abstract void d(i iVar, i iVar2);

        @DexIgnore
        public abstract void e(i iVar, Thread thread);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ c c;
        @DexIgnore
        public static /* final */ c d;

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f1422a;
        @DexIgnore
        public /* final */ Throwable b;

        /*
        static {
            if (h41.e) {
                d = null;
                c = null;
                return;
            }
            d = new c(false, null);
            c = new c(true, null);
        }
        */

        @DexIgnore
        public c(boolean z, Throwable th) {
            this.f1422a = z;
            this.b = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public static /* final */ d b; // = new d(new a("Failure occurred while trying to finish a future."));

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Throwable f1423a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends Throwable {
            @DexIgnore
            public a(String str) {
                super(str);
            }

            @DexIgnore
            public Throwable fillInStackTrace() {
                synchronized (this) {
                }
                return this;
            }
        }

        @DexIgnore
        public d(Throwable th) {
            h41.e(th);
            this.f1423a = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public static /* final */ e d; // = new e(null, null);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Runnable f1424a;
        @DexIgnore
        public /* final */ Executor b;
        @DexIgnore
        public e c;

        @DexIgnore
        public e(Runnable runnable, Executor executor) {
            this.f1424a = runnable;
            this.b = executor;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AtomicReferenceFieldUpdater<i, Thread> f1425a;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<i, i> b;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<h41, i> c;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<h41, e> d;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<h41, Object> e;

        @DexIgnore
        public f(AtomicReferenceFieldUpdater<i, Thread> atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater<i, i> atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater<h41, i> atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater<h41, e> atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater<h41, Object> atomicReferenceFieldUpdater5) {
            super();
            this.f1425a = atomicReferenceFieldUpdater;
            this.b = atomicReferenceFieldUpdater2;
            this.c = atomicReferenceFieldUpdater3;
            this.d = atomicReferenceFieldUpdater4;
            this.e = atomicReferenceFieldUpdater5;
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public boolean a(h41<?> h41, e eVar, e eVar2) {
            return this.d.compareAndSet(h41, eVar, eVar2);
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public boolean b(h41<?> h41, Object obj, Object obj2) {
            return this.e.compareAndSet(h41, obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public boolean c(h41<?> h41, i iVar, i iVar2) {
            return this.c.compareAndSet(h41, iVar, iVar2);
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public void d(i iVar, i iVar2) {
            this.b.lazySet(iVar, iVar2);
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public void e(i iVar, Thread thread) {
            this.f1425a.lazySet(iVar, thread);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<V> implements Runnable {
        @DexIgnore
        public /* final */ h41<V> b;
        @DexIgnore
        public /* final */ g64<? extends V> c;

        @DexIgnore
        public g(h41<V> h41, g64<? extends V> g64) {
            this.b = h41;
            this.c = g64;
        }

        @DexIgnore
        public void run() {
            if (this.b.b == this) {
                if (h41.g.b(this.b, this, h41.j(this.c))) {
                    h41.g(this.b);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends b {
        @DexIgnore
        public h() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public boolean a(h41<?> h41, e eVar, e eVar2) {
            synchronized (h41) {
                if (h41.c != eVar) {
                    return false;
                }
                h41.c = eVar2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public boolean b(h41<?> h41, Object obj, Object obj2) {
            synchronized (h41) {
                if (h41.b != obj) {
                    return false;
                }
                h41.b = obj2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public boolean c(h41<?> h41, i iVar, i iVar2) {
            synchronized (h41) {
                if (h41.d != iVar) {
                    return false;
                }
                h41.d = iVar2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public void d(i iVar, i iVar2) {
            iVar.b = iVar2;
        }

        @DexIgnore
        @Override // com.fossil.h41.b
        public void e(i iVar, Thread thread) {
            iVar.f1426a = thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public static /* final */ i c; // = new i(false);

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public volatile Thread f1426a;
        @DexIgnore
        public volatile i b;

        @DexIgnore
        public i() {
            h41.g.e(this, Thread.currentThread());
        }

        @DexIgnore
        public i(boolean z) {
        }

        @DexIgnore
        public void a(i iVar) {
            h41.g.d(this, iVar);
        }

        @DexIgnore
        public void b() {
            Thread thread = this.f1426a;
            if (thread != null) {
                this.f1426a = null;
                LockSupport.unpark(thread);
            }
        }
    }

    /*
    static {
        b hVar;
        Throwable th;
        try {
            th = null;
            hVar = new f(AtomicReferenceFieldUpdater.newUpdater(i.class, Thread.class, "a"), AtomicReferenceFieldUpdater.newUpdater(i.class, i.class, "b"), AtomicReferenceFieldUpdater.newUpdater(h41.class, i.class, "d"), AtomicReferenceFieldUpdater.newUpdater(h41.class, e.class, "c"), AtomicReferenceFieldUpdater.newUpdater(h41.class, Object.class, "b"));
        } catch (Throwable th2) {
            hVar = new h();
            th = th2;
        }
        g = hVar;
        if (th != null) {
            f.log(Level.SEVERE, "SafeAtomicHelper is broken!", th);
        }
    }
    */

    @DexIgnore
    public static CancellationException d(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }

    @DexIgnore
    public static <T> T e(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r4v2. Raw type applied. Possible types: com.fossil.h41<V>, com.fossil.h41 */
    /* JADX WARN: Type inference failed for: r3v1, types: [com.fossil.h41$b] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void g(com.fossil.h41<?> r4) {
        /*
            r0 = 0
        L_0x0001:
            r4.n()
            r4.b()
            com.fossil.h41$e r1 = r4.f(r0)
            r2 = r1
        L_0x000c:
            if (r2 == 0) goto L_0x0035
            com.fossil.h41$e r1 = r2.c
            java.lang.Runnable r0 = r2.f1424a
            boolean r3 = r0 instanceof com.fossil.h41.g
            if (r3 == 0) goto L_0x002e
            com.fossil.h41$g r0 = (com.fossil.h41.g) r0
            com.fossil.h41<V> r4 = r0.b
            java.lang.Object r2 = r4.b
            if (r2 != r0) goto L_0x0033
            com.fossil.g64<? extends V> r2 = r0.c
            java.lang.Object r2 = j(r2)
            com.fossil.h41$b r3 = com.fossil.h41.g
            boolean r0 = r3.b(r4, r0, r2)
            if (r0 == 0) goto L_0x0033
            r0 = r1
            goto L_0x0001
        L_0x002e:
            java.util.concurrent.Executor r2 = r2.b
            h(r0, r2)
        L_0x0033:
            r2 = r1
            goto L_0x000c
        L_0x0035:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.h41.g(com.fossil.h41):void");
    }

    @DexIgnore
    public static void h(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e2) {
            Logger logger = f;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, (Throwable) e2);
        }
    }

    @DexIgnore
    public static Object j(g64<?> g64) {
        if (g64 instanceof h41) {
            Object obj = ((h41) g64).b;
            if (!(obj instanceof c)) {
                return obj;
            }
            c cVar = (c) obj;
            return cVar.f1422a ? cVar.b != null ? new c(false, cVar.b) : c.d : obj;
        }
        boolean isCancelled = g64.isCancelled();
        if ((!e) && isCancelled) {
            return c.d;
        }
        try {
            Object k = k(g64);
            if (k == null) {
                k = h;
            }
            return k;
        } catch (ExecutionException e2) {
            return new d(e2.getCause());
        } catch (CancellationException e3) {
            if (isCancelled) {
                return new c(false, e3);
            }
            return new d(new IllegalArgumentException("get() threw CancellationException, despite reporting isCancelled() == false: " + g64, e3));
        } catch (Throwable th) {
            return new d(th);
        }
    }

    @DexIgnore
    public static <V> V k(Future<V> future) throws ExecutionException {
        V v;
        boolean z = false;
        while (true) {
            try {
                v = future.get();
                break;
            } catch (InterruptedException e2) {
                z = true;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return v;
    }

    @DexIgnore
    public final void a(StringBuilder sb) {
        try {
            Object k = k(this);
            sb.append("SUCCESS, result=[");
            sb.append(s(k));
            sb.append("]");
        } catch (ExecutionException e2) {
            sb.append("FAILURE, cause=[");
            sb.append(e2.getCause());
            sb.append("]");
        } catch (CancellationException e3) {
            sb.append("CANCELLED");
        } catch (RuntimeException e4) {
            sb.append("UNKNOWN, cause=[");
            sb.append(e4.getClass());
            sb.append(" thrown from get()]");
        }
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.g64
    public final void c(Runnable runnable, Executor executor) {
        e(runnable);
        e(executor);
        e eVar = this.c;
        if (eVar != e.d) {
            e eVar2 = new e(runnable, executor);
            do {
                eVar2.c = eVar;
                if (!g.a(this, eVar, eVar2)) {
                    eVar = this.c;
                } else {
                    return;
                }
            } while (eVar != e.d);
        }
        h(runnable, executor);
    }

    @DexIgnore
    public final boolean cancel(boolean z) {
        Object obj = this.b;
        if (!(obj == null) && !(obj instanceof g)) {
            return false;
        }
        c cVar = e ? new c(z, new CancellationException("Future.cancel() was called.")) : z ? c.c : c.d;
        boolean z2 = false;
        Object obj2 = obj;
        while (true) {
            if (g.b(this, obj2, cVar)) {
                if (z) {
                    this.l();
                }
                g(this);
                if (!(obj2 instanceof g)) {
                    return true;
                }
                g64<? extends V> g64 = ((g) obj2).c;
                if (g64 instanceof h41) {
                    h41<V> h41 = (h41) g64;
                    obj2 = h41.b;
                    if (!(obj2 == null) && !(obj2 instanceof g)) {
                        return true;
                    }
                    this = h41;
                    z2 = true;
                } else {
                    g64.cancel(z);
                    return true;
                }
            } else {
                obj2 = this.b;
                if (!(obj2 instanceof g)) {
                    return z2;
                }
            }
        }
    }

    @DexIgnore
    public final e f(e eVar) {
        e eVar2;
        do {
            eVar2 = this.c;
        } while (!g.a(this, eVar2, e.d));
        while (eVar2 != null) {
            e eVar3 = eVar2.c;
            eVar2.c = eVar;
            eVar = eVar2;
            eVar2 = eVar3;
        }
        return eVar;
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public final V get() throws InterruptedException, ExecutionException {
        Object obj;
        if (!Thread.interrupted()) {
            Object obj2 = this.b;
            if ((obj2 != null) && (!(obj2 instanceof g))) {
                return i(obj2);
            }
            i iVar = this.d;
            if (iVar != i.c) {
                i iVar2 = new i();
                do {
                    iVar2.a(iVar);
                    if (g.c(this, iVar, iVar2)) {
                        do {
                            LockSupport.park(this);
                            if (!Thread.interrupted()) {
                                obj = this.b;
                            } else {
                                o(iVar2);
                                throw new InterruptedException();
                            }
                        } while (!((obj != null) & (!(obj instanceof g))));
                        return i(obj);
                    }
                    iVar = this.d;
                } while (iVar != i.c);
            }
            return i(this.b);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public final V get(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long nanos = timeUnit.toNanos(j);
        if (!Thread.interrupted()) {
            Object obj = this.b;
            if ((obj != null) && (!(obj instanceof g))) {
                return i(obj);
            }
            long nanoTime = nanos > 0 ? System.nanoTime() + nanos : 0;
            if (nanos >= 1000) {
                i iVar = this.d;
                if (iVar != i.c) {
                    i iVar2 = new i();
                    do {
                        iVar2.a(iVar);
                        if (g.c(this, iVar, iVar2)) {
                            long j2 = nanos;
                            do {
                                LockSupport.parkNanos(this, j2);
                                if (!Thread.interrupted()) {
                                    Object obj2 = this.b;
                                    if ((obj2 != null) && (!(obj2 instanceof g))) {
                                        return i(obj2);
                                    }
                                    j2 = nanoTime - System.nanoTime();
                                } else {
                                    o(iVar2);
                                    throw new InterruptedException();
                                }
                            } while (j2 >= 1000);
                            o(iVar2);
                            nanos = j2;
                        } else {
                            iVar = this.d;
                        }
                    } while (iVar != i.c);
                }
                return i(this.b);
            }
            while (nanos > 0) {
                Object obj3 = this.b;
                if ((obj3 != null) && (!(obj3 instanceof g))) {
                    return i(obj3);
                }
                if (!Thread.interrupted()) {
                    nanos = nanoTime - System.nanoTime();
                } else {
                    throw new InterruptedException();
                }
            }
            String h41 = toString();
            String lowerCase = timeUnit.toString().toLowerCase(Locale.ROOT);
            String str = "Waited " + j + " " + timeUnit.toString().toLowerCase(Locale.ROOT);
            if (1000 + nanos < 0) {
                String str2 = str + " (plus ";
                long j3 = -nanos;
                long convert = timeUnit.convert(j3, TimeUnit.NANOSECONDS);
                long nanos2 = j3 - timeUnit.toNanos(convert);
                int i2 = (convert > 0 ? 1 : (convert == 0 ? 0 : -1));
                boolean z = i2 == 0 || nanos2 > 1000;
                if (i2 > 0) {
                    String str3 = str2 + convert + " " + lowerCase;
                    if (z) {
                        str3 = str3 + ",";
                    }
                    str2 = str3 + " ";
                }
                if (z) {
                    str2 = str2 + nanos2 + " nanoseconds ";
                }
                str = str2 + "delay)";
            }
            if (isDone()) {
                throw new TimeoutException(str + " but future completed as timeout expired");
            }
            throw new TimeoutException(str + " for " + h41);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public final V i(Object obj) throws ExecutionException {
        if (obj instanceof c) {
            throw d("Task was cancelled.", ((c) obj).b);
        } else if (obj instanceof d) {
            throw new ExecutionException(((d) obj).f1423a);
        } else if (obj == h) {
            return null;
        } else {
            return obj;
        }
    }

    @DexIgnore
    public final boolean isCancelled() {
        return this.b instanceof c;
    }

    @DexIgnore
    public final boolean isDone() {
        Object obj = this.b;
        return (obj != null) & (!(obj instanceof g));
    }

    @DexIgnore
    public void l() {
    }

    @DexIgnore
    public String m() {
        Object obj = this.b;
        if (obj instanceof g) {
            return "setFuture=[" + s(((g) obj).c) + "]";
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            return "remaining delay=[" + ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS) + " ms]";
        }
    }

    @DexIgnore
    public final void n() {
        i iVar;
        do {
            iVar = this.d;
        } while (!g.c(this, iVar, i.c));
        while (iVar != null) {
            iVar.b();
            iVar = iVar.b;
        }
    }

    @DexIgnore
    public final void o(i iVar) {
        iVar.f1426a = null;
        while (true) {
            i iVar2 = this.d;
            if (iVar2 != i.c) {
                i iVar3 = null;
                while (iVar2 != null) {
                    i iVar4 = iVar2.b;
                    if (iVar2.f1426a != null) {
                        iVar3 = iVar2;
                    } else if (iVar3 != null) {
                        iVar3.b = iVar4;
                        if (iVar3.f1426a == null) {
                        }
                    } else if (!g.c(this, iVar2, iVar4)) {
                    }
                    iVar2 = iVar4;
                }
                return;
            }
            return;
        }
    }

    @DexIgnore
    public boolean p(V v) {
        if (v == null) {
            v = (V) h;
        }
        if (!g.b(this, null, v)) {
            return false;
        }
        g(this);
        return true;
    }

    @DexIgnore
    public boolean q(Throwable th) {
        e(th);
        if (!g.b(this, null, new d(th))) {
            return false;
        }
        g(this);
        return true;
    }

    @DexIgnore
    public boolean r(g64<? extends V> g64) {
        g gVar;
        d dVar;
        e(g64);
        Object obj = this.b;
        if (obj == null) {
            if (g64.isDone()) {
                if (!g.b(this, null, j(g64))) {
                    return false;
                }
                g(this);
                return true;
            }
            gVar = new g(this, g64);
            if (g.b(this, null, gVar)) {
                try {
                    g64.c(gVar, i41.INSTANCE);
                } catch (Throwable th) {
                    dVar = d.b;
                }
                return true;
            }
            obj = this.b;
        }
        if (obj instanceof c) {
            g64.cancel(((c) obj).f1422a);
        }
        return false;
        g.b(this, gVar, dVar);
        return true;
    }

    @DexIgnore
    public final String s(Object obj) {
        return obj == this ? "this future" : String.valueOf(obj);
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            sb.append("CANCELLED");
        } else if (isDone()) {
            a(sb);
        } else {
            try {
                str = m();
            } catch (RuntimeException e2) {
                str = "Exception thrown from implementation: " + e2.getClass();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            } else if (isDone()) {
                a(sb);
            } else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
