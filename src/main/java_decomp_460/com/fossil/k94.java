package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k94 {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ File f1883a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends JSONObject {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ s94 f1884a;

        @DexIgnore
        public a(s94 s94) throws JSONException {
            this.f1884a = s94;
            put(ButtonService.USER_ID, this.f1884a.b());
        }
    }

    @DexIgnore
    public k94(File file) {
        this.f1883a = file;
    }

    @DexIgnore
    public static s94 c(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        s94 s94 = new s94();
        s94.e(g(jSONObject, ButtonService.USER_ID));
        return s94;
    }

    @DexIgnore
    public static String d(Map<String, String> map) throws JSONException {
        return new JSONObject(map).toString();
    }

    @DexIgnore
    public static String f(s94 s94) throws JSONException {
        return new a(s94).toString();
    }

    @DexIgnore
    public static String g(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, null);
        }
        return null;
    }

    @DexIgnore
    public File a(String str) {
        File file = this.f1883a;
        return new File(file, str + "keys.meta");
    }

    @DexIgnore
    public File b(String str) {
        File file = this.f1883a;
        return new File(file, str + "user.meta");
    }

    @DexIgnore
    public s94 e(String str) {
        FileInputStream fileInputStream;
        Throwable th;
        Throwable th2;
        File b2 = b(str);
        if (!b2.exists()) {
            return new s94();
        }
        try {
            fileInputStream = new FileInputStream(b2);
            try {
                s94 c = c(r84.I(fileInputStream));
                r84.e(fileInputStream, "Failed to close user metadata file.");
                return c;
            } catch (Exception e) {
                e = e;
                try {
                    x74.f().e("Error deserializing user metadata.", e);
                    r84.e(fileInputStream, "Failed to close user metadata file.");
                    return new s94();
                } catch (Throwable th3) {
                    th2 = th3;
                    th = th2;
                    r84.e(fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                r84.e(fileInputStream, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            fileInputStream = null;
            x74.f().e("Error deserializing user metadata.", e);
            r84.e(fileInputStream, "Failed to close user metadata file.");
            return new s94();
        } catch (Throwable th5) {
            th2 = th5;
            fileInputStream = null;
            th = th2;
            r84.e(fileInputStream, "Failed to close user metadata file.");
            throw th;
        }
    }

    @DexIgnore
    public void h(String str, Map<String, String> map) {
        BufferedWriter bufferedWriter;
        Throwable th;
        Exception e;
        File a2 = a(str);
        try {
            String d = d(map);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(a2), b));
            try {
                bufferedWriter.write(d);
                bufferedWriter.flush();
                r84.e(bufferedWriter, "Failed to close key/value metadata file.");
            } catch (Exception e2) {
                e = e2;
                try {
                    x74.f().e("Error serializing key/value metadata.", e);
                    r84.e(bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th2) {
                    th = th2;
                    r84.e(bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                r84.e(bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            bufferedWriter = null;
            x74.f().e("Error serializing key/value metadata.", e);
            r84.e(bufferedWriter, "Failed to close key/value metadata file.");
        } catch (Throwable th4) {
            th = th4;
            bufferedWriter = null;
            r84.e(bufferedWriter, "Failed to close key/value metadata file.");
            throw th;
        }
    }

    @DexIgnore
    public void i(String str, s94 s94) {
        BufferedWriter bufferedWriter;
        Throwable th;
        Exception e;
        File b2 = b(str);
        try {
            String f = f(s94);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(b2), b));
            try {
                bufferedWriter.write(f);
                bufferedWriter.flush();
                r84.e(bufferedWriter, "Failed to close user metadata file.");
            } catch (Exception e2) {
                e = e2;
                try {
                    x74.f().e("Error serializing user metadata.", e);
                    r84.e(bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th2) {
                    th = th2;
                    r84.e(bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                r84.e(bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            bufferedWriter = null;
            x74.f().e("Error serializing user metadata.", e);
            r84.e(bufferedWriter, "Failed to close user metadata file.");
        } catch (Throwable th4) {
            th = th4;
            bufferedWriter = null;
            r84.e(bufferedWriter, "Failed to close user metadata file.");
            throw th;
        }
    }
}
