package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.m62.d;
import com.fossil.r62;
import com.fossil.yb2;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m62<O extends d> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a<?, O> f2307a;
    @DexIgnore
    public /* final */ g<?> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T extends f, O> extends e<T, O> {
        @DexIgnore
        @Deprecated
        public abstract T c(Context context, Looper looper, ac2 ac2, O o, r62.b bVar, r62.c cVar);
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<C extends b> {
    }

    @DexIgnore
    public interface d {

        @DexIgnore
        public interface a extends c, d {
            @DexIgnore
            Account m();
        }

        @DexIgnore
        public interface b extends c {
            @DexIgnore
            GoogleSignInAccount b();
        }

        @DexIgnore
        public interface c extends d {
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m62$d$d")
        /* renamed from: com.fossil.m62$d$d  reason: collision with other inner class name */
        public static final class C0151d implements d {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<T extends b, O> {
        @DexIgnore
        public List<Scope> a(O o) {
            return Collections.emptyList();
        }

        @DexIgnore
        public int b() {
            return Integer.MAX_VALUE;
        }
    }

    @DexIgnore
    public interface f extends b {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        boolean c();

        @DexIgnore
        void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        @DexIgnore
        boolean g();

        @DexIgnore
        Set<Scope> h();

        @DexIgnore
        void i(jc2 jc2, Set<Scope> set);

        @DexIgnore
        boolean j();

        @DexIgnore
        String k();

        @DexIgnore
        void l(yb2.c cVar);

        @DexIgnore
        void n(yb2.e eVar);

        @DexIgnore
        boolean r();

        @DexIgnore
        int s();

        @DexIgnore
        b62[] t();

        @DexIgnore
        Intent u();

        @DexIgnore
        boolean v();

        @DexIgnore
        IBinder w();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<C extends f> extends c<C> {
    }

    @DexIgnore
    public interface h<T extends IInterface> extends b {
        @DexIgnore
        void m(int i, T t);

        @DexIgnore
        String p();

        @DexIgnore
        T q(IBinder iBinder);

        @DexIgnore
        String x();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.m62$a<C extends com.fossil.m62$f, O extends com.fossil.m62$d> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.m62$g<C extends com.fossil.m62$f> */
    /* JADX WARN: Multi-variable type inference failed */
    public <C extends f> m62(String str, a<C, O> aVar, g<C> gVar) {
        rc2.l(aVar, "Cannot construct an Api with a null ClientBuilder");
        rc2.l(gVar, "Cannot construct an Api with a null ClientKey");
        this.c = str;
        this.f2307a = aVar;
        this.b = gVar;
    }

    @DexIgnore
    public final c<?> a() {
        g<?> gVar = this.b;
        if (gVar != null) {
            return gVar;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final e<?, O> c() {
        return this.f2307a;
    }

    @DexIgnore
    public final a<?, O> d() {
        rc2.o(this.f2307a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.f2307a;
    }
}
