package com.fossil;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj0<E> implements Collection<E>, Set<E> {
    @DexIgnore
    public static /* final */ int[] f; // = new int[0];
    @DexIgnore
    public static /* final */ Object[] g; // = new Object[0];
    @DexIgnore
    public static Object[] h;
    @DexIgnore
    public static int i;
    @DexIgnore
    public static Object[] j;
    @DexIgnore
    public static int k;
    @DexIgnore
    public int[] b;
    @DexIgnore
    public Object[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public fj0<E, E> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends fj0<E, E> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public void a() {
            aj0.this.clear();
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public Object b(int i, int i2) {
            return aj0.this.c[i];
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public Map<E, E> c() {
            throw new UnsupportedOperationException("not a map");
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public int d() {
            return aj0.this.d;
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public int e(Object obj) {
            return aj0.this.indexOf(obj);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public int f(Object obj) {
            return aj0.this.indexOf(obj);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public void g(E e, E e2) {
            aj0.this.add(e);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public void h(int i) {
            aj0.this.g(i);
        }

        @DexIgnore
        @Override // com.fossil.fj0
        public E i(int i, E e) {
            throw new UnsupportedOperationException("not a map");
        }
    }

    @DexIgnore
    public aj0() {
        this(0);
    }

    @DexIgnore
    public aj0(int i2) {
        if (i2 == 0) {
            this.b = f;
            this.c = g;
        } else {
            a(i2);
        }
        this.d = 0;
    }

    @DexIgnore
    public static void c(int[] iArr, Object[] objArr, int i2) {
        if (iArr.length == 8) {
            synchronized (aj0.class) {
                try {
                    if (k < 10) {
                        objArr[0] = j;
                        objArr[1] = iArr;
                        for (int i3 = i2 - 1; i3 >= 2; i3--) {
                            objArr[i3] = null;
                        }
                        j = objArr;
                        k++;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (aj0.class) {
                try {
                    if (i < 10) {
                        objArr[0] = h;
                        objArr[1] = iArr;
                        for (int i4 = i2 - 1; i4 >= 2; i4--) {
                            objArr[i4] = null;
                        }
                        h = objArr;
                        i++;
                    }
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        }
    }

    @DexIgnore
    public final void a(int i2) {
        if (i2 == 8) {
            synchronized (aj0.class) {
                try {
                    if (j != null) {
                        Object[] objArr = j;
                        this.c = objArr;
                        j = (Object[]) objArr[0];
                        this.b = (int[]) objArr[1];
                        objArr[1] = null;
                        objArr[0] = null;
                        k--;
                        return;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        } else if (i2 == 4) {
            synchronized (aj0.class) {
                try {
                    if (h != null) {
                        Object[] objArr2 = h;
                        this.c = objArr2;
                        h = (Object[]) objArr2[0];
                        this.b = (int[]) objArr2[1];
                        objArr2[1] = null;
                        objArr2[0] = null;
                        i--;
                        return;
                    }
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        }
        this.b = new int[i2];
        this.c = new Object[i2];
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean add(E e2) {
        int hashCode;
        int e3;
        int i2 = 8;
        if (e2 == null) {
            e3 = f();
            hashCode = 0;
        } else {
            hashCode = e2.hashCode();
            e3 = e(e2, hashCode);
        }
        if (e3 >= 0) {
            return false;
        }
        int i3 = this.d;
        if (i3 >= this.b.length) {
            if (i3 >= 8) {
                i2 = (i3 >> 1) + i3;
            } else if (i3 < 4) {
                i2 = 4;
            }
            int[] iArr = this.b;
            Object[] objArr = this.c;
            a(i2);
            int[] iArr2 = this.b;
            if (iArr2.length > 0) {
                System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                System.arraycopy(objArr, 0, this.c, 0, objArr.length);
            }
            c(iArr, objArr, this.d);
        }
        int i4 = this.d;
        if (e3 < i4) {
            int[] iArr3 = this.b;
            int i5 = e3 + 1;
            System.arraycopy(iArr3, e3, iArr3, i5, i4 - e3);
            Object[] objArr2 = this.c;
            System.arraycopy(objArr2, e3, objArr2, i5, this.d - e3);
        }
        this.b[e3] = hashCode;
        this.c[e3] = e2;
        this.d++;
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.aj0<E> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Collection, java.util.Set
    public boolean addAll(Collection<? extends E> collection) {
        b(this.d + collection.size());
        Iterator<? extends E> it = collection.iterator();
        boolean z = false;
        while (it.hasNext()) {
            z |= add(it.next());
        }
        return z;
    }

    @DexIgnore
    public void b(int i2) {
        int[] iArr = this.b;
        if (iArr.length < i2) {
            Object[] objArr = this.c;
            a(i2);
            int i3 = this.d;
            if (i3 > 0) {
                System.arraycopy(iArr, 0, this.b, 0, i3);
                System.arraycopy(objArr, 0, this.c, 0, this.d);
            }
            c(iArr, objArr, this.d);
        }
    }

    @DexIgnore
    public void clear() {
        int i2 = this.d;
        if (i2 != 0) {
            c(this.b, this.c, i2);
            this.b = f;
            this.c = g;
            this.d = 0;
        }
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean containsAll(Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final fj0<E, E> d() {
        if (this.e == null) {
            this.e = new a();
        }
        return this.e;
    }

    @DexIgnore
    public final int e(Object obj, int i2) {
        int i3 = this.d;
        if (i3 == 0) {
            return -1;
        }
        int a2 = cj0.a(this.b, i3, i2);
        if (a2 < 0 || obj.equals(this.c[a2])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.b[i4] == i2) {
            if (obj.equals(this.c[i4])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.b[i5] == i2) {
            if (obj.equals(this.c[i5])) {
                return i5;
            }
            i5--;
        }
        return i4;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            if (size() != set.size()) {
                return false;
            }
            for (int i2 = 0; i2 < this.d; i2++) {
                try {
                    if (!set.contains(h(i2))) {
                        return false;
                    }
                } catch (ClassCastException | NullPointerException e2) {
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int f() {
        int i2 = this.d;
        if (i2 == 0) {
            return -1;
        }
        int a2 = cj0.a(this.b, i2, 0);
        if (a2 < 0 || this.c[a2] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.b[i3] == 0) {
            if (this.c[i3] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.b[i4] == 0) {
            if (this.c[i4] == null) {
                return i4;
            }
            i4--;
        }
        return i3;
    }

    @DexIgnore
    public E g(int i2) {
        int i3 = 8;
        Object[] objArr = this.c;
        E e2 = (E) objArr[i2];
        int i4 = this.d;
        if (i4 <= 1) {
            c(this.b, objArr, i4);
            this.b = f;
            this.c = g;
            this.d = 0;
        } else {
            int[] iArr = this.b;
            if (iArr.length <= 8 || i4 >= iArr.length / 3) {
                int i5 = this.d - 1;
                this.d = i5;
                if (i2 < i5) {
                    int[] iArr2 = this.b;
                    int i6 = i2 + 1;
                    System.arraycopy(iArr2, i6, iArr2, i2, i5 - i2);
                    Object[] objArr2 = this.c;
                    System.arraycopy(objArr2, i6, objArr2, i2, this.d - i2);
                }
                this.c[this.d] = null;
            } else {
                if (i4 > 8) {
                    i3 = (i4 >> 1) + i4;
                }
                int[] iArr3 = this.b;
                Object[] objArr3 = this.c;
                a(i3);
                this.d--;
                if (i2 > 0) {
                    System.arraycopy(iArr3, 0, this.b, 0, i2);
                    System.arraycopy(objArr3, 0, this.c, 0, i2);
                }
                int i7 = this.d;
                if (i2 < i7) {
                    int i8 = i2 + 1;
                    System.arraycopy(iArr3, i8, this.b, i2, i7 - i2);
                    System.arraycopy(objArr3, i8, this.c, i2, this.d - i2);
                }
            }
        }
        return e2;
    }

    @DexIgnore
    public E h(int i2) {
        return (E) this.c[i2];
    }

    @DexIgnore
    public int hashCode() {
        int[] iArr = this.b;
        int i2 = this.d;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 += iArr[i4];
        }
        return i3;
    }

    @DexIgnore
    public int indexOf(Object obj) {
        return obj == null ? f() : e(obj, obj.hashCode());
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.d <= 0;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator<E> iterator() {
        return d().m().iterator();
    }

    @DexIgnore
    public boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf < 0) {
            return false;
        }
        g(indexOf);
        return true;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean removeAll(Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        boolean z = false;
        while (it.hasNext()) {
            z |= remove(it.next());
        }
        return z;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i2 = this.d - 1; i2 >= 0; i2--) {
            if (!collection.contains(this.c[i2])) {
                g(i2);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public int size() {
        return this.d;
    }

    @DexIgnore
    public Object[] toArray() {
        int i2 = this.d;
        Object[] objArr = new Object[i2];
        System.arraycopy(this.c, 0, objArr, 0, i2);
        return objArr;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public <T> T[] toArray(T[] tArr) {
        T[] tArr2 = tArr.length < this.d ? (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.d)) : tArr;
        System.arraycopy(this.c, 0, tArr2, 0, this.d);
        int length = tArr2.length;
        int i2 = this.d;
        if (length > i2) {
            tArr2[i2] = null;
        }
        return tArr2;
    }

    @DexIgnore
    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.d * 14);
        sb.append('{');
        for (int i2 = 0; i2 < this.d; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            E h2 = h(i2);
            if (h2 != this) {
                sb.append((Object) h2);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
