package com.fossil;

import android.content.Context;
import com.facebook.internal.Utility;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg3 {
    @DexIgnore
    public static zk3<Long> A; // = a("measurement.upload.initial_upload_delay_time", 15000L, 15000L, th3.f3415a);
    @DexIgnore
    public static zk3<Boolean> A0;
    @DexIgnore
    public static zk3<Long> B; // = a("measurement.upload.retry_time", 1800000L, 1800000L, sh3.f3261a);
    @DexIgnore
    public static zk3<Boolean> B0;
    @DexIgnore
    public static zk3<Integer> C; // = a("measurement.upload.retry_count", 6, 6, vh3.f3768a);
    @DexIgnore
    public static zk3<Boolean> C0;
    @DexIgnore
    public static zk3<Long> D; // = a("measurement.upload.max_queue_time", 2419200000L, 2419200000L, uh3.f3590a);
    @DexIgnore
    public static zk3<Boolean> D0;
    @DexIgnore
    public static zk3<Integer> E; // = a("measurement.lifetimevalue.max_currency_tracked", 4, 4, xh3.f4122a);
    @DexIgnore
    public static zk3<Boolean> E0;
    @DexIgnore
    public static zk3<Integer> F; // = a("measurement.audience.filter_result_max_count", 200, 200, zh3.f4473a);
    @DexIgnore
    public static zk3<Boolean> F0;
    @DexIgnore
    public static zk3<Integer> G; // = a("measurement.upload.max_public_user_properties", 25, 25, null);
    @DexIgnore
    public static zk3<Boolean> G0;
    @DexIgnore
    public static zk3<Integer> H; // = a("measurement.upload.max_event_name_cardinality", 500, 500, null);
    @DexIgnore
    public static zk3<Boolean> H0;
    @DexIgnore
    public static zk3<Integer> I; // = a("measurement.upload.max_public_event_params", 25, 25, null);
    @DexIgnore
    public static zk3<Boolean> I0;
    @DexIgnore
    public static zk3<Long> J; // = a("measurement.service_client.idle_disconnect_millis", 5000L, 5000L, yh3.f4317a);
    @DexIgnore
    public static zk3<Boolean> J0;
    @DexIgnore
    public static zk3<Boolean> K;
    @DexIgnore
    public static zk3<Boolean> K0;
    @DexIgnore
    public static zk3<String> L; // = a("measurement.test.string_flag", "---", "---", ai3.f271a);
    @DexIgnore
    public static zk3<Boolean> L0;
    @DexIgnore
    public static zk3<Long> M; // = a("measurement.test.long_flag", -1L, -1L, di3.f791a);
    @DexIgnore
    public static zk3<Boolean> M0;
    @DexIgnore
    public static zk3<Integer> N; // = a("measurement.test.int_flag", -2, -2, ci3.f619a);
    @DexIgnore
    public static zk3<Boolean> N0;
    @DexIgnore
    public static zk3<Double> O;
    @DexIgnore
    public static zk3<Boolean> O0;
    @DexIgnore
    public static zk3<Integer> P; // = a("measurement.experiment.max_ids", 50, 50, ei3.f945a);
    @DexIgnore
    public static zk3<Boolean> P0;
    @DexIgnore
    public static zk3<Integer> Q; // = a("measurement.max_bundles_per_iteration", 2, 2, hi3.f1482a);
    @DexIgnore
    public static zk3<Boolean> Q0;
    @DexIgnore
    public static zk3<Boolean> R;
    @DexIgnore
    public static zk3<Boolean> R0;
    @DexIgnore
    public static zk3<Boolean> S;
    @DexIgnore
    public static zk3<Long> S0; // = a("measurement.sdk.attribution.cache.ttl", 604800000L, 604800000L, rk3.f3121a);
    @DexIgnore
    public static zk3<Boolean> T;
    @DexIgnore
    public static zk3<Boolean> T0;
    @DexIgnore
    public static zk3<Boolean> U;
    @DexIgnore
    public static zk3<Boolean> V;
    @DexIgnore
    public static zk3<Boolean> W;
    @DexIgnore
    public static zk3<Boolean> X;
    @DexIgnore
    public static zk3<Boolean> Y;
    @DexIgnore
    public static zk3<Boolean> Z;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static List<zk3<?>> f4117a; // = Collections.synchronizedList(new ArrayList());
    @DexIgnore
    public static zk3<Boolean> a0;
    @DexIgnore
    public static zk3<Long> b;
    @DexIgnore
    public static zk3<Boolean> b0;
    @DexIgnore
    public static zk3<Long> c;
    @DexIgnore
    public static zk3<Boolean> c0;
    @DexIgnore
    public static zk3<Long> d;
    @DexIgnore
    public static zk3<Boolean> d0;
    @DexIgnore
    public static zk3<String> e; // = a("measurement.config.url_scheme", Utility.URL_SCHEME, Utility.URL_SCHEME, wh3.f3938a);
    @DexIgnore
    public static zk3<Boolean> e0;
    @DexIgnore
    public static zk3<String> f; // = a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com", ki3.f1919a);
    @DexIgnore
    public static zk3<Boolean> f0;
    @DexIgnore
    public static zk3<Integer> g; // = a("measurement.upload.max_bundles", 100, 100, ti3.f3419a);
    @DexIgnore
    public static zk3<Boolean> g0;
    @DexIgnore
    public static zk3<Integer> h; // = a("measurement.upload.max_batch_size", 65536, 65536, gj3.f1314a);
    @DexIgnore
    public static zk3<Boolean> h0;
    @DexIgnore
    public static zk3<Integer> i; // = a("measurement.upload.max_bundle_size", 65536, 65536, qj3.f2992a);
    @DexIgnore
    public static zk3<Boolean> i0;
    @DexIgnore
    public static zk3<Integer> j; // = a("measurement.upload.max_events_per_bundle", 1000, 1000, dk3.f798a);
    @DexIgnore
    public static zk3<Boolean> j0;
    @DexIgnore
    public static zk3<Integer> k; // = a("measurement.upload.max_events_per_day", 100000, 100000, nk3.f2538a);
    @DexIgnore
    public static zk3<Boolean> k0;
    @DexIgnore
    public static zk3<Integer> l; // = a("measurement.upload.max_error_events_per_day", 1000, 1000, ch3.f613a);
    @DexIgnore
    public static zk3<Boolean> l0;
    @DexIgnore
    public static zk3<Integer> m;
    @DexIgnore
    public static zk3<Boolean> m0;
    @DexIgnore
    public static zk3<Integer> n; // = a("measurement.upload.max_conversions_per_day", 10000, 10000, eh3.f942a);
    @DexIgnore
    public static zk3<Boolean> n0;
    @DexIgnore
    public static zk3<Integer> o; // = a("measurement.upload.max_realtime_events_per_day", 10, 10, dh3.f788a);
    @DexIgnore
    public static zk3<Boolean> o0;
    @DexIgnore
    public static zk3<Integer> p; // = a("measurement.store.max_stored_events_per_app", 100000, 100000, gh3.f1306a);
    @DexIgnore
    public static zk3<Boolean> p0;
    @DexIgnore
    public static zk3<String> q; // = a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a", fh3.f1130a);
    @DexIgnore
    public static zk3<Boolean> q0;
    @DexIgnore
    public static zk3<Long> r; // = a("measurement.upload.backoff_period", 43200000L, 43200000L, jh3.f1760a);
    @DexIgnore
    public static zk3<Boolean> r0;
    @DexIgnore
    public static zk3<Long> s; // = a("measurement.upload.window_interval", 3600000L, 3600000L, ih3.f1627a);
    @DexIgnore
    public static zk3<Boolean> s0;
    @DexIgnore
    public static zk3<Long> t; // = a("measurement.upload.interval", 3600000L, 3600000L, lh3.f2195a);
    @DexIgnore
    public static zk3<Boolean> t0;
    @DexIgnore
    public static zk3<Long> u;
    @DexIgnore
    public static zk3<Boolean> u0;
    @DexIgnore
    public static zk3<Long> v; // = a("measurement.upload.debug_upload_interval", 1000L, 1000L, mh3.f2381a);
    @DexIgnore
    public static zk3<Boolean> v0;
    @DexIgnore
    public static zk3<Long> w; // = a("measurement.upload.minimum_delay", 500L, 500L, ph3.f2833a);
    @DexIgnore
    public static zk3<Boolean> w0;
    @DexIgnore
    public static zk3<Long> x; // = a("measurement.alarm_manager.minimum_interval", 60000L, 60000L, oh3.f2682a);
    @DexIgnore
    public static zk3<Boolean> x0;
    @DexIgnore
    public static zk3<Long> y;
    @DexIgnore
    public static zk3<Boolean> y0;
    @DexIgnore
    public static zk3<Long> z; // = a("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L, qh3.f2981a);
    @DexIgnore
    public static zk3<Boolean> z0;

    /*
    static {
        Collections.synchronizedSet(new HashSet());
        Long valueOf = Long.valueOf((long) ButtonService.CONNECT_TIMEOUT);
        b = a("measurement.ad_id_cache_time", valueOf, valueOf, ah3.f269a);
        Long valueOf2 = Long.valueOf((long) LogBuilder.MAX_INTERVAL);
        c = a("measurement.monitoring.sample_period_millis", valueOf2, valueOf2, zg3.f4467a);
        d = a("measurement.config.cache_time", valueOf2, 3600000L, nh3.f2522a);
        Integer valueOf3 = Integer.valueOf((int) SQLiteDatabase.SQLITE_MAX_LIKE_PATTERN_LENGTH);
        m = a("measurement.upload.max_public_events_per_day", valueOf3, valueOf3, bh3.f429a);
        u = a("measurement.upload.realtime_upload_interval", valueOf, valueOf, kh3.f1917a);
        y = a("measurement.upload.stale_data_deletion_interval", valueOf2, valueOf2, rh3.f3113a);
        Boolean bool = Boolean.FALSE;
        K = a("measurement.test.boolean_flag", bool, bool, bi3.f432a);
        Double valueOf4 = Double.valueOf(-3.0d);
        O = a("measurement.test.double_flag", valueOf4, valueOf4, fi3.f1134a);
        Boolean bool2 = Boolean.FALSE;
        R = a("measurement.validation.internal_limits_internal_event_params", bool2, bool2, gi3.f1311a);
        Boolean bool3 = Boolean.TRUE;
        S = a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", bool3, bool3, ji3.f1763a);
        Boolean bool4 = Boolean.TRUE;
        T = a("measurement.collection.firebase_global_collection_flag_enabled", bool4, bool4, mi3.f2389a);
        Boolean bool5 = Boolean.TRUE;
        U = a("measurement.collection.efficient_engagement_reporting_enabled_2", bool5, bool5, li3.f2199a);
        Boolean bool6 = Boolean.FALSE;
        V = a("measurement.collection.redundant_engagement_removal_enabled", bool6, bool6, oi3.f2684a);
        Boolean bool7 = Boolean.TRUE;
        W = a("measurement.client.freeride_engagement_fix", bool7, bool7, ni3.f2526a);
        Boolean bool8 = Boolean.TRUE;
        X = a("measurement.experiment.enable_experiment_reporting", bool8, bool8, qi3.f2985a);
        Boolean bool9 = Boolean.TRUE;
        Y = a("measurement.collection.log_event_and_bundle_v2", bool9, bool9, pi3.f2834a);
        Boolean bool10 = Boolean.FALSE;
        Z = a("measurement.quality.checksum", bool10, bool10, null);
        Boolean bool11 = Boolean.FALSE;
        a0 = a("measurement.sdk.dynamite.allow_remote_dynamite2", bool11, bool11, si3.f3266a);
        Boolean bool12 = Boolean.TRUE;
        b0 = a("measurement.sdk.collection.validate_param_names_alphabetical", bool12, bool12, ri3.f3115a);
        Boolean bool13 = Boolean.TRUE;
        c0 = a("measurement.collection.event_safelist", bool13, bool13, ui3.f3596a);
        Boolean bool14 = Boolean.TRUE;
        a("measurement.service.audience.invalidate_config_cache_after_app_unisntall", bool14, bool14, wi3.f3944a);
        Boolean bool15 = Boolean.TRUE;
        d0 = a("measurement.service.audience.fix_skip_audience_with_failed_filters", bool15, bool15, vi3.f3772a);
        Boolean bool16 = Boolean.FALSE;
        e0 = a("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", bool16, bool16, yi3.f4321a);
        Boolean bool17 = Boolean.FALSE;
        f0 = a("measurement.audience.refresh_event_count_filters_timestamp", bool17, bool17, xi3.f4131a);
        Boolean bool18 = Boolean.FALSE;
        g0 = a("measurement.audience.use_bundle_timestamp_for_event_count_filters", bool18, bool18, aj3.f275a);
        Boolean bool19 = Boolean.TRUE;
        h0 = a("measurement.sdk.collection.retrieve_deeplink_from_bow_2", bool19, bool19, zi3.f4475a);
        Boolean bool20 = Boolean.TRUE;
        i0 = a("measurement.sdk.collection.last_deep_link_referrer2", bool20, bool20, cj3.f622a);
        Boolean bool21 = Boolean.FALSE;
        j0 = a("measurement.sdk.collection.last_deep_link_referrer_campaign2", bool21, bool21, bj3.f435a);
        Boolean bool22 = Boolean.FALSE;
        k0 = a("measurement.sdk.collection.last_gclid_from_referrer2", bool22, bool22, ej3.f948a);
        Boolean bool23 = Boolean.TRUE;
        l0 = a("measurement.sdk.collection.enable_extend_user_property_size", bool23, bool23, dj3.f794a);
        Boolean bool24 = Boolean.FALSE;
        m0 = a("measurement.upload.file_lock_state_check", bool24, bool24, fj3.f1137a);
        Boolean bool25 = Boolean.TRUE;
        n0 = a("measurement.sampling.calculate_bundle_timestamp_before_sampling", bool25, bool25, ij3.f1637a);
        Boolean bool26 = Boolean.FALSE;
        o0 = a("measurement.ga.ga_app_id", bool26, bool26, hj3.f1487a);
        Boolean bool27 = Boolean.TRUE;
        p0 = a("measurement.lifecycle.app_backgrounded_tracking", bool27, bool27, lj3.f2204a);
        Boolean bool28 = Boolean.FALSE;
        q0 = a("measurement.lifecycle.app_in_background_parameter", bool28, bool28, kj3.f1922a);
        Boolean bool29 = Boolean.FALSE;
        r0 = a("measurement.integration.disable_firebase_instance_id", bool29, bool29, nj3.f2532a);
        Boolean bool30 = Boolean.FALSE;
        s0 = a("measurement.lifecycle.app_backgrounded_engagement", bool30, bool30, mj3.f2391a);
        Boolean bool31 = Boolean.FALSE;
        t0 = a("measurement.collection.service.update_with_analytics_fix", bool31, bool31, pj3.f2837a);
        Boolean bool32 = Boolean.FALSE;
        u0 = a("measurement.service.use_appinfo_modified", bool32, bool32, oj3.f2687a);
        Boolean bool33 = Boolean.TRUE;
        v0 = a("measurement.client.firebase_feature_rollout.v1.enable", bool33, bool33, rj3.f3118a);
        Boolean bool34 = Boolean.TRUE;
        w0 = a("measurement.client.sessions.check_on_reset_and_enable2", bool34, bool34, tj3.f3424a);
        Boolean bool35 = Boolean.TRUE;
        x0 = a("measurement.config.string.always_update_disk_on_set", bool35, bool35, sj3.f3270a);
        Boolean bool36 = Boolean.FALSE;
        y0 = a("measurement.scheduler.task_thread.cleanup_on_exit", bool36, bool36, vj3.f3774a);
        Boolean bool37 = Boolean.FALSE;
        z0 = a("measurement.upload.file_truncate_fix", bool37, bool37, uj3.f3602a);
        Boolean bool38 = Boolean.TRUE;
        A0 = a("measurement.engagement_time_main_thread", bool38, bool38, xj3.f4133a);
        Boolean bool39 = Boolean.FALSE;
        B0 = a("measurement.sdk.referrer.delayed_install_referrer_api", bool39, bool39, wj3.f3949a);
        Boolean bool40 = Boolean.FALSE;
        C0 = a("measurement.sdk.screen.disabling_automatic_reporting", bool40, bool40, zj3.f4482a);
        Boolean bool41 = Boolean.FALSE;
        D0 = a("measurement.sdk.screen.manual_screen_view_logging", bool41, bool41, yj3.f4324a);
        Boolean bool42 = Boolean.TRUE;
        E0 = a("measurement.gold.enhanced_ecommerce.format_logs", bool42, bool42, bk3.f441a);
        Boolean bool43 = Boolean.TRUE;
        F0 = a("measurement.gold.enhanced_ecommerce.nested_param_daily_event_count", bool43, bool43, ak3.f283a);
        Boolean bool44 = Boolean.TRUE;
        G0 = a("measurement.gold.enhanced_ecommerce.upload_nested_complex_events", bool44, bool44, ck3.f625a);
        Boolean bool45 = Boolean.TRUE;
        H0 = a("measurement.gold.enhanced_ecommerce.log_nested_complex_events", bool45, bool45, fk3.f1142a);
        Boolean bool46 = Boolean.TRUE;
        I0 = a("measurement.gold.enhanced_ecommerce.updated_schema.client", bool46, bool46, ek3.f952a);
        Boolean bool47 = Boolean.TRUE;
        J0 = a("measurement.gold.enhanced_ecommerce.updated_schema.service", bool47, bool47, hk3.f1492a);
        Boolean bool48 = Boolean.FALSE;
        a("measurement.collection.synthetic_data_mitigation", bool48, bool48, gk3.f1319a);
        Boolean bool49 = Boolean.TRUE;
        K0 = a("measurement.service.configurable_service_limits", bool49, bool49, jk3.f1771a);
        Boolean bool50 = Boolean.FALSE;
        L0 = a("measurement.client.configurable_service_limits", bool50, bool50, ik3.f1638a);
        Boolean bool51 = Boolean.TRUE;
        M0 = a("measurement.androidId.delete_feature", bool51, bool51, mk3.f2396a);
        Boolean bool52 = Boolean.FALSE;
        N0 = a("measurement.client.global_params.dev", bool52, bool52, lk3.f2208a);
        Boolean bool53 = Boolean.FALSE;
        O0 = a("measurement.service.global_params", bool53, bool53, ok3.f2691a);
        Boolean bool54 = Boolean.TRUE;
        P0 = a("measurement.service.global_params_in_payload", bool54, bool54, qk3.f2993a);
        Boolean bool55 = Boolean.TRUE;
        Q0 = a("measurement.client.string_reader", bool55, bool55, pk3.f2840a);
        Boolean bool56 = Boolean.TRUE;
        R0 = a("measurement.sdk.attribution.cache", bool56, bool56, sk3.f3273a);
        Boolean bool57 = Boolean.TRUE;
        T0 = a("measurement.service.database_return_empty_collection", bool57, bool57, uk3.f3607a);
        Boolean bool58 = Boolean.TRUE;
        a("measurement.service.ssaid_removal", bool58, bool58, tk3.f3430a);
        Boolean bool59 = Boolean.FALSE;
        a("measurement.client.consent_state_v1.dev", bool59, bool59, wk3.f3961a);
        Boolean bool60 = Boolean.FALSE;
        a("measurement.service.consent_state_v1", bool60, bool60, vk3.f3777a);
    }
    */

    @DexIgnore
    public static <V> zk3<V> a(String str, V v2, V v3, xk3<V> xk3) {
        zk3<V> zk3 = new zk3<>(str, v2, v3, xk3);
        f4117a.add(zk3);
        return zk3;
    }

    @DexIgnore
    public static Map<String, String> c(Context context) {
        jv2 a2 = jv2.a(context.getContentResolver(), yv2.a("com.google.android.gms.measurement"));
        return a2 == null ? Collections.emptyMap() : a2.b();
    }
}
