package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kh2 extends IInterface {
    @DexIgnore
    int L1(rg2 rg2, String str, boolean z) throws RemoteException;

    @DexIgnore
    rg2 V1(rg2 rg2, String str, int i) throws RemoteException;

    @DexIgnore
    int f2() throws RemoteException;

    @DexIgnore
    int m1(rg2 rg2, String str, boolean z) throws RemoteException;

    @DexIgnore
    rg2 w0(rg2 rg2, String str, int i) throws RemoteException;
}
