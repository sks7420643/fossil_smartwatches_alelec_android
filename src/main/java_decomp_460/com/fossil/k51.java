package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k51 implements j51 {
    @DexIgnore
    public static /* final */ Bitmap.Config[] d;
    @DexIgnore
    public static /* final */ Bitmap.Config[] e;
    @DexIgnore
    public static /* final */ Bitmap.Config[] f; // = {Bitmap.Config.RGB_565};
    @DexIgnore
    public static /* final */ Bitmap.Config[] g; // = {Bitmap.Config.ARGB_4444};
    @DexIgnore
    public static /* final */ Bitmap.Config[] h; // = {Bitmap.Config.ALPHA_8};
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public /* final */ n51<b, Bitmap> b; // = new n51<>();
    @DexIgnore
    public /* final */ HashMap<Bitmap.Config, NavigableMap<Integer, Integer>> c; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1862a;
        @DexIgnore
        public /* final */ Bitmap.Config b;

        @DexIgnore
        public b(int i, Bitmap.Config config) {
            pq7.c(config, "config");
            this.f1862a = i;
            this.b = config;
        }

        @DexIgnore
        public final int a() {
            return this.f1862a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (this.f1862a != bVar.f1862a || !pq7.a(this.b, bVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f1862a;
            Bitmap.Config config = this.b;
            return (config != null ? config.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            a aVar = k51.i;
            int i = this.f1862a;
            Bitmap.Config config = this.b;
            return '[' + i + "](" + config + ')';
        }
    }

    /*
    static {
        Bitmap.Config[] configArr = Build.VERSION.SDK_INT >= 26 ? new Bitmap.Config[]{Bitmap.Config.ARGB_8888, Bitmap.Config.RGBA_F16} : new Bitmap.Config[]{Bitmap.Config.ARGB_8888};
        d = configArr;
        e = configArr;
    }
    */

    @DexIgnore
    @Override // com.fossil.j51
    public String a(int i2, int i3, Bitmap.Config config) {
        pq7.c(config, "config");
        int a2 = y81.f4257a.a(i2, i3, config);
        return '[' + a2 + "](" + config + ')';
    }

    @DexIgnore
    @Override // com.fossil.j51
    public void b(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int b2 = w81.b(bitmap);
        Bitmap.Config config = bitmap.getConfig();
        pq7.b(config, "bitmap.config");
        b bVar = new b(b2, config);
        this.b.f(bVar, bitmap);
        NavigableMap<Integer, Integer> h2 = h(bitmap.getConfig());
        Integer num = (Integer) h2.get(Integer.valueOf(bVar.a()));
        h2.put(Integer.valueOf(bVar.a()), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    @DexIgnore
    @Override // com.fossil.j51
    public Bitmap c(int i2, int i3, Bitmap.Config config) {
        pq7.c(config, "config");
        b f2 = f(y81.f4257a.a(i2, i3, config), config);
        Bitmap a2 = this.b.a(f2);
        if (a2 != null) {
            e(f2.a(), a2);
            a2.reconfigure(i2, i3, config);
        }
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.j51
    public String d(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int b2 = w81.b(bitmap);
        Bitmap.Config config = bitmap.getConfig();
        pq7.b(config, "bitmap.config");
        return '[' + b2 + "](" + config + ')';
    }

    @DexIgnore
    public final void e(int i2, Bitmap bitmap) {
        NavigableMap<Integer, Integer> h2 = h(bitmap.getConfig());
        Object obj = h2.get(Integer.valueOf(i2));
        if (obj != null) {
            int intValue = ((Number) obj).intValue();
            if (intValue == 1) {
                h2.remove(Integer.valueOf(i2));
            } else {
                h2.put(Integer.valueOf(i2), Integer.valueOf(intValue - 1));
            }
        } else {
            throw new IllegalStateException(("Tried to decrement empty size, size: " + i2 + ", removed: " + d(bitmap) + ", this: " + this).toString());
        }
    }

    @DexIgnore
    public final b f(int i2, Bitmap.Config config) {
        Bitmap.Config[] g2 = g(config);
        for (Bitmap.Config config2 : g2) {
            Integer ceilingKey = h(config2).ceilingKey(Integer.valueOf(i2));
            if (ceilingKey != null && ceilingKey.intValue() <= i2 * 8) {
                return new b(ceilingKey.intValue(), config2);
            }
        }
        return new b(i2, config);
    }

    @DexIgnore
    public final Bitmap.Config[] g(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && Bitmap.Config.RGBA_F16 == config) {
            return e;
        }
        if (config == Bitmap.Config.ARGB_8888) {
            return d;
        }
        if (config == Bitmap.Config.RGB_565) {
            return f;
        }
        if (config == Bitmap.Config.ARGB_4444) {
            return g;
        }
        if (config == Bitmap.Config.ALPHA_8) {
            return h;
        }
        return new Bitmap.Config[]{config};
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> h(Bitmap.Config config) {
        HashMap<Bitmap.Config, NavigableMap<Integer, Integer>> hashMap = this.c;
        NavigableMap<Integer, Integer> navigableMap = hashMap.get(config);
        if (navigableMap == null) {
            navigableMap = new TreeMap<>();
            hashMap.put(config, navigableMap);
        }
        return navigableMap;
    }

    @DexIgnore
    @Override // com.fossil.j51
    public Bitmap removeLast() {
        Bitmap e2 = this.b.e();
        if (e2 != null) {
            e(w81.b(e2), e2);
        }
        return e2;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SizeConfigStrategy: groupedMap=");
        sb.append(this.b);
        sb.append(", sortedSizes=(");
        for (Map.Entry<Bitmap.Config, NavigableMap<Integer, Integer>> entry : this.c.entrySet()) {
            sb.append(entry.getKey());
            sb.append("[");
            sb.append(entry.getValue());
            sb.append("], ");
        }
        if (!this.c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")");
        String sb2 = sb.toString();
        pq7.b(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
