package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ky1 {
    VERBOSE(2, 'V'),
    DEBUG(3, 'D'),
    INFO(4, 'I'),
    WARN(5, 'W'),
    ERROR(6, 'E'),
    ASSERT(7, 'A');
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ int priority;
    @DexIgnore
    public /* final */ char symbol;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ky1 a(int i) {
            ky1[] values = ky1.values();
            for (ky1 ky1 : values) {
                if (ky1.getPriority() == i) {
                    return ky1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public ky1(int i, char c) {
        this.priority = i;
        this.symbol = (char) c;
    }

    @DexIgnore
    public final int getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final char getSymbol() {
        return this.symbol;
    }
}
