package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3629a;
    @DexIgnore
    public /* final */ so5 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {138}, m = "activePreset")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "activePresetBySerial")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {DateTimeConstants.HOURS_PER_WEEK}, m = "clean")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {107, 114}, m = "deletePresets")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {155, 159, 163}, m = "deletePresetsById")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {142}, m = "deletePresetsBySerial")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {85, 92}, m = "fetchDianaPreset")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {146}, m = "getCurrentTotalPreset")
    public static final class h extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {128}, m = "insert")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {130}, m = "insert")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {144}, m = "pendingPresets")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {136}, m = "presetWithPresetId")
    public static final class l extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {132}, m = "presets")
    public static final class m extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {134}, m = "presetsLive")
    public static final class n extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {29, 38}, m = "upsertDianaPreset")
    public static final class o extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {59, 68}, m = "upsertDianaPreset")
    public static final class p extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(uo5 uo5, qn7 qn7) {
            super(qn7);
            this.this$0 = uo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore
    public uo5(so5 so5) {
        pq7.c(so5, "remote");
        this.b = so5;
        String simpleName = uo5.class.getSimpleName();
        pq7.b(simpleName, "DianaPresetRepository::class.java.simpleName");
        this.f3629a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.qn7<? super com.fossil.mo5> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.uo5.a
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.fossil.uo5$a r0 = (com.fossil.uo5.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.fossil.uo5 r0 = (com.fossil.uo5) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            com.fossil.mo5 r0 = r0.k()
        L_0x0031:
            return r0
        L_0x0032:
            com.fossil.uo5$a r0 = new com.fossil.uo5$a
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.a(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r6, com.fossil.qn7<? super com.fossil.mo5> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.b
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.fossil.uo5$b r0 = (com.fossil.uo5.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            com.fossil.mo5 r0 = r0.b(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.uo5$b r0 = new com.fossil.uo5$b
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.b(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.qn7<? super com.fossil.tl7> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.uo5.c
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.fossil.uo5$c r0 = (com.fossil.uo5.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.uo5 r0 = (com.fossil.uo5) r0
            com.fossil.el7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            r0.a()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.uo5$c r0 = new com.fossil.uo5$c
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r1)
            com.fossil.bn5 r1 = com.fossil.bn5.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.c(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v29, types: [com.fossil.mo5] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.util.List<com.fossil.mo5> r13, com.fossil.qn7<? super com.fossil.kz4<com.fossil.gj4>> r14) {
        /*
        // Method dump skipped, instructions count: 338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.d(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(java.lang.String r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.e(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(java.lang.String r6, com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.f
            if (r0 == 0) goto L_0x003a
            r0 = r7
            com.fossil.uo5$f r0 = (com.fossil.uo5.f) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r4) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            r0.g(r6)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0039:
            return r0
        L_0x003a:
            com.fossil.uo5$f r0 = new com.fossil.uo5$f
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.f(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(java.lang.String r10, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.mo5>>> r11) {
        /*
        // Method dump skipped, instructions count: 275
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.g(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object h(String str, qn7<? super iq5<gj4>> qn7) {
        gj4 gj4 = new gj4();
        bj4 bj4 = new bj4();
        bj4.l(str);
        gj4.k("id", bj4.m(0));
        return this.b.e(gj4, qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(java.lang.String r6, com.fossil.qn7<? super java.lang.Integer> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.h
            if (r0 == 0) goto L_0x003d
            r0 = r7
            com.fossil.uo5$h r0 = (com.fossil.uo5.h) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003d
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x004c
            if (r3 != r4) goto L_0x0044
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            int r0 = r0.h(r6)
            java.lang.Integer r0 = com.fossil.ao7.e(r0)
        L_0x003c:
            return r0
        L_0x003d:
            com.fossil.uo5$h r0 = new com.fossil.uo5$h
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.i(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object j(String str, String str2, qn7<? super iq5<gj4>> qn7) {
        return this.b.f(str, str2, qn7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object k(com.fossil.mo5 r6, com.fossil.qn7<? super java.lang.Long> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.i
            if (r0 == 0) goto L_0x003d
            r0 = r7
            com.fossil.uo5$i r0 = (com.fossil.uo5.i) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003d
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x004c
            if (r3 != r4) goto L_0x0044
            java.lang.Object r0 = r1.L$1
            com.fossil.mo5 r0 = (com.fossil.mo5) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            long r0 = r0.l(r6)
            java.lang.Long r0 = com.fossil.ao7.f(r0)
        L_0x003c:
            return r0
        L_0x003d:
            com.fossil.uo5$i r0 = new com.fossil.uo5$i
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.k(com.fossil.mo5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object l(java.util.List<com.fossil.mo5> r6, com.fossil.qn7<? super java.lang.Long[]> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.j
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.fossil.uo5$j r0 = (com.fossil.uo5.j) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            java.lang.Long[] r0 = r0.insert(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.uo5$j r0 = new com.fossil.uo5$j
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.l(java.util.List, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object m(java.lang.String r6, com.fossil.qn7<? super java.util.List<com.fossil.mo5>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.k
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.fossil.uo5$k r0 = (com.fossil.uo5.k) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            java.util.List r0 = r0.d(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.uo5$k r0 = new com.fossil.uo5$k
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.m(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object n(java.lang.String r6, com.fossil.qn7<? super com.fossil.mo5> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.l
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.fossil.uo5$l r0 = (com.fossil.uo5.l) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            com.fossil.mo5 r0 = r0.getPresetById(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.uo5$l r0 = new com.fossil.uo5$l
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.n(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object o(java.lang.String r6, com.fossil.qn7<? super java.util.List<com.fossil.mo5>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.m
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.fossil.uo5$m r0 = (com.fossil.uo5.m) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            java.util.List r0 = r0.f(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.uo5$m r0 = new com.fossil.uo5$m
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.o(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object p(java.lang.String r6, com.fossil.qn7<? super androidx.lifecycle.LiveData<java.util.List<com.fossil.mo5>>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.uo5.n
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.fossil.uo5$n r0 = (com.fossil.uo5.n) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.uo5 r1 = (com.fossil.uo5) r1
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.qo5 r0 = r0.getDianaPresetDao()
            androidx.lifecycle.LiveData r0 = r0.m(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.uo5$n r0 = new com.fossil.uo5$n
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.el7.b(r2)
            com.fossil.bn5 r2 = com.fossil.bn5.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.p(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object q(com.fossil.jo5 r13, com.fossil.qn7<? super com.fossil.kz4<com.fossil.mo5>> r14) {
        /*
        // Method dump skipped, instructions count: 428
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.q(com.fossil.jo5, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(java.util.List<com.fossil.jo5> r14, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.mo5>>> r15) {
        /*
        // Method dump skipped, instructions count: 430
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo5.r(java.util.List, com.fossil.qn7):java.lang.Object");
    }
}
