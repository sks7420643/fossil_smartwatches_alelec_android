package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rv2 implements pv2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ sv2 f3172a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public rv2(sv2 sv2, String str) {
        this.f3172a = sv2;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final Object zza() {
        return this.f3172a.c(this.b);
    }
}
