package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static x01 f4025a; // = null;
    @DexIgnore
    public static /* final */ int b; // = 20;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends x01 {
        @DexIgnore
        public int c;

        @DexIgnore
        public a(int i) {
            super(i);
            this.c = i;
        }

        @DexIgnore
        @Override // com.fossil.x01
        public void a(String str, String str2, Throwable... thArr) {
            if (this.c > 3) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.d(str, str2);
            } else {
                Log.d(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.x01
        public void b(String str, String str2, Throwable... thArr) {
            if (this.c > 6) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.e(str, str2);
            } else {
                Log.e(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.x01
        public void d(String str, String str2, Throwable... thArr) {
            if (this.c > 4) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.i(str, str2);
            } else {
                Log.i(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.x01
        public void g(String str, String str2, Throwable... thArr) {
            if (this.c > 2) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.v(str, str2);
            } else {
                Log.v(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        @Override // com.fossil.x01
        public void h(String str, String str2, Throwable... thArr) {
            if (this.c > 5) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.w(str, str2);
            } else {
                Log.w(str, str2, thArr[0]);
            }
        }
    }

    @DexIgnore
    public x01(int i) {
    }

    @DexIgnore
    public static x01 c() {
        x01 x01;
        synchronized (x01.class) {
            try {
                if (f4025a == null) {
                    f4025a = new a(3);
                }
                x01 = f4025a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return x01;
    }

    @DexIgnore
    public static void e(x01 x01) {
        synchronized (x01.class) {
            try {
                f4025a = x01;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static String f(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        int i = b;
        if (length >= i) {
            sb.append(str.substring(0, i));
        } else {
            sb.append(str);
        }
        return sb.toString();
    }

    @DexIgnore
    public abstract void a(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void b(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void d(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void g(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void h(String str, String str2, Throwable... thArr);
}
