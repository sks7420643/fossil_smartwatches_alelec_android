package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vm7 implements Iterator<Long>, jr7 {
    @DexIgnore
    /* renamed from: a */
    public final Long next() {
        return Long.valueOf(b());
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
