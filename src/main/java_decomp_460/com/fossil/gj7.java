package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import androidx.core.content.FileProvider;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gj7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public zi7 f1317a;
    @DexIgnore
    public dj7 b;

    @DexIgnore
    public gj7(zi7 zi7) {
        this.f1317a = zi7;
        this.b = zi7.b();
    }

    @DexIgnore
    public final File a(String str, String str2, File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        if (TextUtils.isEmpty(str2)) {
            str2 = "";
        }
        sb.append(str2);
        return new File(file, sb.toString());
    }

    @DexIgnore
    public final File b(Context context, String str) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            str2 = str + File.separator;
        } else {
            str2 = "";
        }
        File file = new File(h(context) + File.separator + this.f1317a.g() + File.separator + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        if (file.isDirectory()) {
            return file;
        }
        return null;
    }

    @DexIgnore
    public final String c(Context context, Uri uri) {
        String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(context.getContentResolver().getType(uri));
        Locale locale = Locale.US;
        if (TextUtils.isEmpty(extensionFromMimeType)) {
            extensionFromMimeType = "tmp";
        }
        return String.format(locale, ".%s", extensionFromMimeType);
    }

    @DexIgnore
    public File d(Context context) {
        File b2 = b(context, "camera");
        if (b2 == null) {
            this.b.w("BelvedereStorage", "Error creating cache directory");
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US);
        return a(String.format(Locale.US, "camera_image_%s", simpleDateFormat.format(new Date(System.currentTimeMillis()))), ".jpg", b2);
    }

    @DexIgnore
    public final String e(Context context, Uri uri) {
        Cursor query = context.getContentResolver().query(uri, new String[]{"_display_name"}, null, null, null);
        String str = "";
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    str = query.getString(0);
                }
            } finally {
                query.close();
            }
        }
        return str;
    }

    @DexIgnore
    public String f(Context context) {
        String string = context.getString(lj7.belvedere_sdk_fpa_suffix);
        return String.format(Locale.US, "%s%s", context.getPackageName(), string);
    }

    @DexIgnore
    public Uri g(Context context, File file) {
        String f = f(context);
        try {
            return FileProvider.getUriForFile(context, f, file);
        } catch (IllegalArgumentException e) {
            this.b.e("BelvedereStorage", String.format(Locale.US, "The selected file can't be shared %s", file.toString()));
            return null;
        } catch (NullPointerException e2) {
            String format = String.format(Locale.US, "=====================\nFileProvider failed to retrieve file uri. There might be an issue with the FileProvider \nPlease make sure that manifest-merger is working, and that you have defined the applicationId (package name) in the build.gradle\nManifest merger: http://tools.android.com/tech-docs/new-build-system/user-guide/manifest-merger\nIf your are not able to use gradle or the manifest merger, please add the following to your AndroidManifest.xml:\n        <provider\n            android:name=\"com.zendesk.belvedere.BelvedereFileProvider\"\n            android:authorities=\"${applicationId}${belvedereFileProviderAuthoritySuffix}\"\n            android:exported=\"false\"\n            android:grantUriPermissions=\"true\">\n            <meta-data\n                android:name=\"android.support.FILE_PROVIDER_PATHS\"\n                android:resource=\"@xml/belvedere_attachment_storage\" />\n        </provider>\n=====================", f);
            Log.e("BelvedereStorage", format, e2);
            this.b.e("BelvedereStorage", format, e2);
            return null;
        }
    }

    @DexIgnore
    public final String h(Context context) {
        return context.getCacheDir().getAbsolutePath();
    }

    @DexIgnore
    public File i(Context context, Uri uri) {
        String str = null;
        File b2 = b(context, "gallery");
        if (b2 == null) {
            this.b.w("BelvedereStorage", "Error creating cache directory");
            return null;
        }
        String e = e(context, uri);
        if (TextUtils.isEmpty(e)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US);
            e = String.format(Locale.US, "attachment_%s", simpleDateFormat.format(new Date(System.currentTimeMillis())));
            str = c(context, uri);
        }
        return a(e, str, b2);
    }

    @DexIgnore
    public File j(Context context, String str) {
        File b2 = b(context, "request");
        if (b2 != null) {
            return a(str, null, b2);
        }
        this.b.w("BelvedereStorage", "Error creating cache directory");
        return null;
    }

    @DexIgnore
    public void k(Context context, Intent intent, Uri uri, int i) {
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 65536)) {
            context.grantUriPermission(resolveInfo.activityInfo.packageName, uri, i);
        }
    }

    @DexIgnore
    public void l(Context context, Uri uri, int i) {
        context.revokeUriPermission(uri, i);
    }
}
