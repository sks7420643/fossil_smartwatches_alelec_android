package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sv3> CREATOR; // = new tv3();
    @DexIgnore
    public byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public sv3(byte b2, byte b3, String str) {
        this.b = (byte) b2;
        this.c = (byte) b3;
        this.d = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || sv3.class != obj.getClass()) {
            return false;
        }
        sv3 sv3 = (sv3) obj;
        if (this.b != sv3.b) {
            return false;
        }
        if (this.c != sv3.c) {
            return false;
        }
        return this.d.equals(sv3.d);
    }

    @DexIgnore
    public final int hashCode() {
        return ((((this.b + 31) * 31) + this.c) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public final String toString() {
        byte b2 = this.b;
        byte b3 = this.c;
        String str = this.d;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 73);
        sb.append("AmsEntityUpdateParcelable{, mEntityId=");
        sb.append((int) b2);
        sb.append(", mAttributeId=");
        sb.append((int) b3);
        sb.append(", mValue='");
        sb.append(str);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.f(parcel, 2, this.b);
        bd2.f(parcel, 3, this.c);
        bd2.u(parcel, 4, this.d, false);
        bd2.b(parcel, a2);
    }
}
