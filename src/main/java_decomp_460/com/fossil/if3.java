package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if3 implements Parcelable.Creator<ue3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ue3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        float f = 0.0f;
        float f2 = 0.0f;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                f2 = ad2.r(parcel, t);
            } else if (l != 3) {
                ad2.B(parcel, t);
            } else {
                f = ad2.r(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new ue3(f2, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ue3[] newArray(int i) {
        return new ue3[i];
    }
}
