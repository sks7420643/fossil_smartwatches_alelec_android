package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class va0 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ ua0 CREATOR; // = new ua0(null);
    @DexIgnore
    public /* final */ aa0 b;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public va0(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0013
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.aa0 r0 = com.fossil.aa0.valueOf(r0)
            r2.<init>(r0)
            return
        L_0x0013:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.va0.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public va0(aa0 aa0) {
        this.b = aa0;
    }

    @DexIgnore
    public abstract byte[] a();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((va0) obj).b;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.Instruction");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(new JSONObject(), jd0.Q3, ey1.a(this.b));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.b.b);
        }
    }
}
