package com.fossil;

import android.os.RemoteException;
import com.fossil.zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st2 extends zs2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ r93 i;
    @DexIgnore
    public /* final */ /* synthetic */ zs2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public st2(zs2 zs2, String str, String str2, boolean z, r93 r93) {
        super(zs2);
        this.j = zs2;
        this.f = str;
        this.g = str2;
        this.h = z;
        this.i = r93;
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void a() throws RemoteException {
        this.j.h.getUserProperties(this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    @Override // com.fossil.zs2.a
    public final void b() {
        this.i.c(null);
    }
}
