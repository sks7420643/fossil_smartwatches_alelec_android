package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rq3 implements Runnable {
    @DexIgnore
    public /* final */ oq3 b;

    @DexIgnore
    public rq3(oq3 oq3) {
        this.b = oq3;
    }

    @DexIgnore
    public final void run() {
        boolean z = true;
        oq3 oq3 = this.b;
        pq3 pq3 = oq3.d;
        long j = oq3.b;
        long j2 = oq3.c;
        pq3.b.h();
        pq3.b.d().M().a("Application going to the background");
        if (pq3.b.m().s(xg3.D0)) {
            pq3.b.l().w.a(true);
        }
        Bundle bundle = new Bundle();
        if (!pq3.b.m().K().booleanValue()) {
            pq3.b.e.f(j2);
            if (pq3.b.m().s(xg3.s0)) {
                bundle.putLong("_et", pq3.b.B(j2));
                ap3.L(pq3.b.s().D(true), bundle, true);
            } else {
                z = false;
            }
            pq3.b.E(false, z, j2);
        }
        pq3.b.p().N("auto", "_ab", j, bundle);
    }
}
