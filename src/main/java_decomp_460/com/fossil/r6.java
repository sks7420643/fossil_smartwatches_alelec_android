package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class r6 extends Enum<r6> {
    @DexIgnore
    public static /* final */ r6 c;
    @DexIgnore
    public static /* final */ r6 d;
    @DexIgnore
    public static /* final */ /* synthetic */ r6[] e;
    @DexIgnore
    public static /* final */ q6 f; // = new q6(null);
    @DexIgnore
    public /* final */ int b;

    /*
    static {
        r6 r6Var = new r6("PROPERTY_BROADCAST", 0, 1);
        r6 r6Var2 = new r6("PROPERTY_READ", 1, 2);
        r6 r6Var3 = new r6("PROPERTY_WRITE_NO_RESPONSE", 2, 4);
        r6 r6Var4 = new r6("PROPERTY_WRITE", 3, 8);
        r6 r6Var5 = new r6("PROPERTY_NOTIFY", 4, 16);
        c = r6Var5;
        r6 r6Var6 = new r6("PROPERTY_INDICATE", 5, 32);
        d = r6Var6;
        e = new r6[]{r6Var, r6Var2, r6Var3, r6Var4, r6Var5, r6Var6, new r6("PROPERTY_SIGNED_WRITE", 6, 64), new r6("PROPERTY_EXTENDED_PROPS", 7, 128)};
    }
    */

    @DexIgnore
    public r6(String str, int i, int i2) {
        this.b = i2;
    }

    @DexIgnore
    public static r6 valueOf(String str) {
        return (r6) Enum.valueOf(r6.class, str);
    }

    @DexIgnore
    public static r6[] values() {
        return (r6[]) e.clone();
    }
}
