package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface g51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final a f1262a = a.f1263a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ a f1263a; // = new a();

        @DexIgnore
        public final g51 a(long j) {
            return new h51(j, null, null, 6, null);
        }
    }

    @DexIgnore
    void a(int i);

    @DexIgnore
    void b(Bitmap bitmap);

    @DexIgnore
    Bitmap c(int i, int i2, Bitmap.Config config);

    @DexIgnore
    Bitmap d(int i, int i2, Bitmap.Config config);
}
