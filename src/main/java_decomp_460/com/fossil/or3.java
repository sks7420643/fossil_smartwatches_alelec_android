package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<or3> CREATOR; // = new nr3();
    @DexIgnore
    public /* final */ List<String> A;
    @DexIgnore
    public /* final */ String B;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ long k;
    @DexIgnore
    public /* final */ String l;
    @DexIgnore
    public /* final */ long m;
    @DexIgnore
    public /* final */ long s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ boolean v;
    @DexIgnore
    public /* final */ boolean w;
    @DexIgnore
    public /* final */ String x;
    @DexIgnore
    public /* final */ Boolean y;
    @DexIgnore
    public /* final */ long z;

    @DexIgnore
    public or3(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z2, boolean z3, String str6, long j5, long j6, int i2, boolean z4, boolean z5, boolean z6, String str7, Boolean bool, long j7, List<String> list, String str8) {
        rc2.g(str);
        this.b = str;
        this.c = TextUtils.isEmpty(str2) ? null : str2;
        this.d = str3;
        this.k = j2;
        this.e = str4;
        this.f = j3;
        this.g = j4;
        this.h = str5;
        this.i = z2;
        this.j = z3;
        this.l = str6;
        this.m = j5;
        this.s = j6;
        this.t = i2;
        this.u = z4;
        this.v = z5;
        this.w = z6;
        this.x = str7;
        this.y = bool;
        this.z = j7;
        this.A = list;
        this.B = str8;
    }

    @DexIgnore
    public or3(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z2, boolean z3, long j4, String str6, long j5, long j6, int i2, boolean z4, boolean z5, boolean z6, String str7, Boolean bool, long j7, List<String> list, String str8) {
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.k = j4;
        this.e = str4;
        this.f = j2;
        this.g = j3;
        this.h = str5;
        this.i = z2;
        this.j = z3;
        this.l = str6;
        this.m = j5;
        this.s = j6;
        this.t = i2;
        this.u = z4;
        this.v = z5;
        this.w = z6;
        this.x = str7;
        this.y = bool;
        this.z = j7;
        this.A = list;
        this.B = str8;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 2, this.b, false);
        bd2.u(parcel, 3, this.c, false);
        bd2.u(parcel, 4, this.d, false);
        bd2.u(parcel, 5, this.e, false);
        bd2.r(parcel, 6, this.f);
        bd2.r(parcel, 7, this.g);
        bd2.u(parcel, 8, this.h, false);
        bd2.c(parcel, 9, this.i);
        bd2.c(parcel, 10, this.j);
        bd2.r(parcel, 11, this.k);
        bd2.u(parcel, 12, this.l, false);
        bd2.r(parcel, 13, this.m);
        bd2.r(parcel, 14, this.s);
        bd2.n(parcel, 15, this.t);
        bd2.c(parcel, 16, this.u);
        bd2.c(parcel, 17, this.v);
        bd2.c(parcel, 18, this.w);
        bd2.u(parcel, 19, this.x, false);
        bd2.d(parcel, 21, this.y, false);
        bd2.r(parcel, 22, this.z);
        bd2.w(parcel, 23, this.A, false);
        bd2.u(parcel, 24, this.B, false);
        bd2.b(parcel, a2);
    }
}
