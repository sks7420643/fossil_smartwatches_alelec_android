package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2843a;
    @DexIgnore
    public String b;
    @DexIgnore
    public long c;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public pl3(String str, String str2, Bundle bundle, long j) {
        this.f2843a = str;
        this.b = str2;
        this.d = bundle == null ? new Bundle() : bundle;
        this.c = j;
    }

    @DexIgnore
    public static pl3 b(vg3 vg3) {
        return new pl3(vg3.b, vg3.d, vg3.c.k(), vg3.e);
    }

    @DexIgnore
    public final vg3 a() {
        return new vg3(this.f2843a, new ug3(new Bundle(this.d)), this.b, this.c);
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String str2 = this.f2843a;
        String valueOf = String.valueOf(this.d);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }
}
