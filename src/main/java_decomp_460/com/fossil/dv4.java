package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv4 implements Factory<cv4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<hu4> f837a;
    @DexIgnore
    public /* final */ Provider<vt4> b;
    @DexIgnore
    public /* final */ Provider<on5> c;

    @DexIgnore
    public dv4(Provider<hu4> provider, Provider<vt4> provider2, Provider<on5> provider3) {
        this.f837a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static dv4 a(Provider<hu4> provider, Provider<vt4> provider2, Provider<on5> provider3) {
        return new dv4(provider, provider2, provider3);
    }

    @DexIgnore
    public static cv4 c(hu4 hu4, vt4 vt4, on5 on5) {
        return new cv4(hu4, vt4, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public cv4 get() {
        return c(this.f837a.get(), this.b.get(), this.c.get());
    }
}
