package com.fossil;

import com.fossil.m64;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t64 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public m64.b f3372a;
    @DexIgnore
    public fg3 b;
    @DexIgnore
    public s64 c;

    @DexIgnore
    public t64(fg3 fg3, m64.b bVar) {
        this.f3372a = bVar;
        this.b = fg3;
        s64 s64 = new s64(this);
        this.c = s64;
        this.b.b(s64);
    }
}
