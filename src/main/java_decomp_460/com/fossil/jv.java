package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv extends mv {
    @DexIgnore
    public boolean M;

    @DexIgnore
    public jv(short s, k5 k5Var) {
        super(s, hs.n, k5Var, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.ps
    public JSONObject F(byte[] bArr) {
        if (this.v.d != lw.b) {
            this.E = true;
        } else {
            this.M = true;
        }
        return new JSONObject();
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void i(p7 p7Var) {
        if (p7Var.f2790a != f5.DISCONNECTED) {
            m(mw.a(this.v, null, null, lw.u, null, null, 27));
        } else if (p7Var.b == 19 || this.M) {
            m(this.v);
        } else {
            m(mw.a(this.v, null, null, lw.g, null, null, 27));
        }
    }
}
