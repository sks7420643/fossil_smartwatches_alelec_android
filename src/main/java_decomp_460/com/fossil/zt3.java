package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zt3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ au3 c;

    @DexIgnore
    public zt3(au3 au3, nt3 nt3) {
        this.c = au3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        synchronized (au3.b(this.c)) {
            if (au3.c(this.c) != null) {
                au3.c(this.c).onComplete(this.b);
            }
        }
    }
}
