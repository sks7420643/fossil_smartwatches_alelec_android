package com.fossil;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.KeyEvent;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ce0 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Object obj);

        @DexIgnore
        void b(int i, int i2, int i3, int i4, int i5);

        @DexIgnore
        void c(Object obj);

        @DexIgnore
        void d(String str, Bundle bundle);

        @DexIgnore
        void f(Bundle bundle);

        @DexIgnore
        void h(List<?> list);

        @DexIgnore
        void j(CharSequence charSequence);

        @DexIgnore
        Object k();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends a> extends MediaController.Callback {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ T f600a;

        @DexIgnore
        public b(T t) {
            this.f600a = t;
        }

        @DexIgnore
        public void onAudioInfoChanged(MediaController.PlaybackInfo playbackInfo) {
            this.f600a.b(playbackInfo.getPlaybackType(), c.b(playbackInfo), playbackInfo.getVolumeControl(), playbackInfo.getMaxVolume(), playbackInfo.getCurrentVolume());
        }

        @DexIgnore
        public void onExtrasChanged(Bundle bundle) {
            MediaSessionCompat.a(bundle);
            this.f600a.f(bundle);
        }

        @DexIgnore
        public void onMetadataChanged(MediaMetadata mediaMetadata) {
            this.f600a.a(mediaMetadata);
        }

        @DexIgnore
        public void onPlaybackStateChanged(PlaybackState playbackState) {
            this.f600a.c(playbackState);
        }

        @DexIgnore
        @Override // android.media.session.MediaController.Callback
        public void onQueueChanged(List<MediaSession.QueueItem> list) {
            this.f600a.h(list);
        }

        @DexIgnore
        public void onQueueTitleChanged(CharSequence charSequence) {
            this.f600a.j(charSequence);
        }

        @DexIgnore
        public void onSessionDestroyed() {
            this.f600a.k();
        }

        @DexIgnore
        public void onSessionEvent(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            this.f600a.d(str, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public static AudioAttributes a(Object obj) {
            return ((MediaController.PlaybackInfo) obj).getAudioAttributes();
        }

        @DexIgnore
        public static int b(Object obj) {
            return c(a(obj));
        }

        @DexIgnore
        public static int c(AudioAttributes audioAttributes) {
            if ((audioAttributes.getFlags() & 1) == 1) {
                return 7;
            }
            if ((audioAttributes.getFlags() & 4) == 4) {
                return 6;
            }
            int usage = audioAttributes.getUsage();
            if (usage == 13) {
                return 1;
            }
            switch (usage) {
                case 2:
                    return 0;
                case 3:
                    return 8;
                case 4:
                    return 4;
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                    return 5;
                case 6:
                    return 2;
                default:
                    return 3;
            }
        }
    }

    @DexIgnore
    public static Object a(a aVar) {
        return new b(aVar);
    }

    @DexIgnore
    public static boolean b(Object obj, KeyEvent keyEvent) {
        return ((MediaController) obj).dispatchMediaButtonEvent(keyEvent);
    }

    @DexIgnore
    public static Object c(Context context, Object obj) {
        return new MediaController(context, (MediaSession.Token) obj);
    }

    @DexIgnore
    public static void d(Object obj, String str, Bundle bundle, ResultReceiver resultReceiver) {
        ((MediaController) obj).sendCommand(str, bundle, resultReceiver);
    }
}
