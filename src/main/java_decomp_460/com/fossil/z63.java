package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z63 implements a73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f4421a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.client.freeride_engagement_fix", true);

    @DexIgnore
    @Override // com.fossil.a73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.a73
    public final boolean zzb() {
        return f4421a.o().booleanValue();
    }
}
