package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class in7 implements Iterator<nl7>, jr7 {
    @DexIgnore
    /* renamed from: a */
    public final nl7 next() {
        return nl7.a(b());
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
