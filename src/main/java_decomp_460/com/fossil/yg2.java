package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.qg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yg2 implements qg2.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ FrameLayout f4313a;
    @DexIgnore
    public /* final */ /* synthetic */ LayoutInflater b;
    @DexIgnore
    public /* final */ /* synthetic */ ViewGroup c;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle d;
    @DexIgnore
    public /* final */ /* synthetic */ qg2 e;

    @DexIgnore
    public yg2(qg2 qg2, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.e = qg2;
        this.f4313a = frameLayout;
        this.b = layoutInflater;
        this.c = viewGroup;
        this.d = bundle;
    }

    @DexIgnore
    @Override // com.fossil.qg2.a
    public final void a(sg2 sg2) {
        this.f4313a.removeAllViews();
        this.f4313a.addView(this.e.f2976a.q(this.b, this.c, this.d));
    }

    @DexIgnore
    @Override // com.fossil.qg2.a
    public final int getState() {
        return 2;
    }
}
