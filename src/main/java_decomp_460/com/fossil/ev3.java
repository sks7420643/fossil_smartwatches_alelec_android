package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev3 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ev3> CREATOR; // = new fv3();
    @DexIgnore
    public /* final */ gv3 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public ev3(gv3 gv3, int i, int i2, int i3) {
        this.b = gv3;
        this.c = i;
        this.d = i2;
        this.e = i3;
    }

    @DexIgnore
    public final void c(su3 su3) {
        int i = this.c;
        if (i == 1) {
            su3.a(this.b);
        } else if (i == 2) {
            su3.b(this.b, this.d, this.e);
        } else if (i == 3) {
            su3.c(this.b, this.d, this.e);
        } else if (i != 4) {
            StringBuilder sb = new StringBuilder(25);
            sb.append("Unknown type: ");
            sb.append(i);
            Log.w("ChannelEventParcelable", sb.toString());
        } else {
            su3.d(this.b, this.d, this.e);
        }
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.b);
        int i = this.c;
        String num = i != 1 ? i != 2 ? i != 3 ? i != 4 ? Integer.toString(i) : "OUTPUT_CLOSED" : "INPUT_CLOSED" : "CHANNEL_CLOSED" : "CHANNEL_OPENED";
        int i2 = this.d;
        String num2 = i2 != 0 ? i2 != 1 ? i2 != 2 ? i2 != 3 ? Integer.toString(i2) : "CLOSE_REASON_LOCAL_CLOSE" : "CLOSE_REASON_REMOTE_CLOSE" : "CLOSE_REASON_DISCONNECTED" : "CLOSE_REASON_NORMAL";
        int i3 = this.e;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81 + String.valueOf(num).length() + String.valueOf(num2).length());
        sb.append("ChannelEventParcelable[, channel=");
        sb.append(valueOf);
        sb.append(", type=");
        sb.append(num);
        sb.append(", closeReason=");
        sb.append(num2);
        sb.append(", appErrorCode=");
        sb.append(i3);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 2, this.b, i, false);
        bd2.n(parcel, 3, this.c);
        bd2.n(parcel, 4, this.d);
        bd2.n(parcel, 5, this.e);
        bd2.b(parcel, a2);
    }
}
