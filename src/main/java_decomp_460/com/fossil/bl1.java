package com.fossil;

import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl1 extends yx1 {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ cl1 b;
    @DexIgnore
    public /* final */ nr c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final bl1 a(nr nrVar, HashMap<b, Object> hashMap) {
            return new bl1(cl1.e.a(nrVar, hashMap), nrVar);
        }
    }

    @DexIgnore
    public enum b {
        HAS_SERVICE_CHANGED
    }

    @DexIgnore
    public bl1(cl1 cl1, nr nrVar) {
        super(cl1);
        this.b = cl1;
        this.c = nrVar;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bl1(cl1 cl1, nr nrVar, int i) {
        this(cl1, (i & 2) != 0 ? new nr(null, zq.SUCCESS, null, null, 13) : nrVar);
    }

    @DexIgnore
    public final nr a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.yx1
    public cl1 getErrorCode() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.yx1, com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        nr nrVar = this.c;
        if (nrVar.c != zq.SUCCESS) {
            g80.k(jSONObject, jd0.B1, nrVar.toJSONObject());
        }
        return jSONObject;
    }
}
