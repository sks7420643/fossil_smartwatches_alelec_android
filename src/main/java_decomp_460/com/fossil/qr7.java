package com.fossil;

import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr7 extends pr7 {
    @DexIgnore
    public /* final */ a c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ThreadLocal<Random> {
        @DexIgnore
        /* renamed from: a */
        public Random initialValue() {
            return new Random();
        }
    }

    @DexIgnore
    @Override // com.fossil.pr7
    public Random d() {
        Object obj = this.c.get();
        pq7.b(obj, "implStorage.get()");
        return (Random) obj;
    }
}
