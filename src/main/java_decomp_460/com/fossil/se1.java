package com.fossil;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.af1;
import com.fossil.wb1;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class se1<Data> implements af1<File, Data> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ d<Data> f3245a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements bf1<File, Data> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d<Data> f3246a;

        @DexIgnore
        public a(d<Data> dVar) {
            this.f3246a = dVar;
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public final af1<File, Data> b(ef1 ef1) {
            return new se1(this.f3246a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a<ParcelFileDescriptor> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<ParcelFileDescriptor> {
            @DexIgnore
            /* renamed from: c */
            public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                parcelFileDescriptor.close();
            }

            @DexIgnore
            /* renamed from: d */
            public ParcelFileDescriptor b(File file) throws FileNotFoundException {
                return ParcelFileDescriptor.open(file, SQLiteDatabase.CREATE_IF_NECESSARY);
            }

            @DexIgnore
            @Override // com.fossil.se1.d
            public Class<ParcelFileDescriptor> getDataClass() {
                return ParcelFileDescriptor.class;
            }
        }

        @DexIgnore
        public b() {
            super(new a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Data> implements wb1<Data> {
        @DexIgnore
        public /* final */ File b;
        @DexIgnore
        public /* final */ d<Data> c;
        @DexIgnore
        public Data d;

        @DexIgnore
        public c(File file, d<Data> dVar) {
            this.b = file;
            this.c = dVar;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
            Data data = this.d;
            if (data != null) {
                try {
                    this.c.a(data);
                } catch (IOException e) {
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super Data> aVar) {
            try {
                Data b2 = this.c.b(this.b);
                this.d = b2;
                aVar.e(b2);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                aVar.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<Data> getDataClass() {
            return this.c.getDataClass();
        }
    }

    @DexIgnore
    public interface d<Data> {
        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Data b(File file) throws FileNotFoundException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a<InputStream> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<InputStream> {
            @DexIgnore
            /* renamed from: c */
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            @DexIgnore
            /* renamed from: d */
            public InputStream b(File file) throws FileNotFoundException {
                return new FileInputStream(file);
            }

            @DexIgnore
            @Override // com.fossil.se1.d
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }
        }

        @DexIgnore
        public e() {
            super(new a());
        }
    }

    @DexIgnore
    public se1(d<Data> dVar) {
        this.f3245a = dVar;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<Data> b(File file, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(file), new c(file, this.f3245a));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(File file) {
        return true;
    }
}
