package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s36 extends q36 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public DNDScheduledTimeModel g;
    @DexIgnore
    public DNDScheduledTimeModel h;
    @DexIgnore
    public /* final */ r36 i;
    @DexIgnore
    public /* final */ DNDSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {98}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s36 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s36$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.s36$a$a  reason: collision with other inner class name */
        public static final class C0216a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s36$a$a$a")
            /* renamed from: com.fossil.s36$a$a$a  reason: collision with other inner class name */
            public static final class RunnableC0217a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ DNDScheduledTimeModel b;
                @DexIgnore
                public /* final */ /* synthetic */ C0216a c;

                @DexIgnore
                public RunnableC0217a(DNDScheduledTimeModel dNDScheduledTimeModel, C0216a aVar) {
                    this.b = dNDScheduledTimeModel;
                    this.c = aVar;
                }

                @DexIgnore
                public final void run() {
                    this.c.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.b);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s36$a$a$b")
            /* renamed from: com.fossil.s36$a$a$b */
            public static final class b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ DNDScheduledTimeModel b;
                @DexIgnore
                public /* final */ /* synthetic */ C0216a c;

                @DexIgnore
                public b(DNDScheduledTimeModel dNDScheduledTimeModel, C0216a aVar) {
                    this.b = dNDScheduledTimeModel;
                    this.c = aVar;
                }

                @DexIgnore
                public final void run() {
                    this.c.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.b);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0216a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0216a aVar = new C0216a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((C0216a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    if (this.this$0.this$0.f == 0) {
                        DNDScheduledTimeModel dNDScheduledTimeModel = this.this$0.this$0.g;
                        if (dNDScheduledTimeModel == null) {
                            return null;
                        }
                        dNDScheduledTimeModel.setMinutes(this.this$0.this$0.e);
                        this.this$0.this$0.j.runInTransaction(new RunnableC0217a(dNDScheduledTimeModel, this));
                        return tl7.f3441a;
                    }
                    DNDScheduledTimeModel dNDScheduledTimeModel2 = this.this$0.this$0.h;
                    if (dNDScheduledTimeModel2 == null) {
                        return null;
                    }
                    dNDScheduledTimeModel2.setMinutes(this.this$0.this$0.e);
                    this.this$0.this$0.j.runInTransaction(new b(dNDScheduledTimeModel2, this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(s36 s36, qn7 qn7) {
            super(2, qn7);
            this.this$0 = s36;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                C0216a aVar = new C0216a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1$listDNDScheduledTimeModel$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends DNDScheduledTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends DNDScheduledTimeModel>> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.j.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(s36 s36, qn7 qn7) {
            super(2, qn7);
            this.this$0 = s36;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (DNDScheduledTimeModel dNDScheduledTimeModel : (List) g) {
                if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$0.f) {
                    r36 r36 = this.this$0.i;
                    s36 s36 = this.this$0;
                    r36.t(s36.A(s36.f));
                    this.this$0.i.E(dNDScheduledTimeModel.getMinutes());
                }
                if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                    this.this$0.g = dNDScheduledTimeModel;
                } else {
                    this.this$0.h = dNDScheduledTimeModel;
                }
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = s36.class.getSimpleName();
        pq7.b(simpleName, "DoNotDisturbScheduledTim\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public s36(r36 r36, DNDSettingsDatabase dNDSettingsDatabase) {
        pq7.c(r36, "mView");
        pq7.c(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.i = r36;
        this.j = dNDSettingsDatabase;
    }

    @DexIgnore
    public final String A(int i2) {
        if (i2 == 0) {
            String c = um5.c(PortfolioApp.h0.c(), 2131886122);
            pq7.b(c, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return c;
        }
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886120);
        pq7.b(c2, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return c2;
    }

    @DexIgnore
    public void B() {
        this.i.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.q36
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.q36
    public void o(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x005f  */
    @Override // com.fossil.q36
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void p(java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            r1 = 0
            r3 = 12
            java.lang.String r0 = "hourValue"
            com.fossil.pq7.c(r9, r0)
            java.lang.String r0 = "minuteValue"
            com.fossil.pq7.c(r10, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.s36.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateTime: hourValue = "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r5 = ", minuteValue = "
            r4.append(r5)
            r4.append(r10)
            java.lang.String r5 = ", isPM = "
            r4.append(r5)
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r0.d(r2, r4)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "Integer.valueOf(hourValue)"
            com.fossil.pq7.b(r0, r2)     // Catch:{ Exception -> 0x006a }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x006a }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r4 = "Integer.valueOf(minuteValue)"
            com.fossil.pq7.b(r2, r4)     // Catch:{ Exception -> 0x00a4 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x00a4 }
        L_0x0053:
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            boolean r4 = android.text.format.DateFormat.is24HourFormat(r4)
            if (r4 == 0) goto L_0x0091
            if (r11 == 0) goto L_0x008d
            if (r0 != r3) goto L_0x008a
            r1 = r3
        L_0x0064:
            int r0 = r1 * 60
            int r0 = r0 + r2
            r8.e = r0
        L_0x0069:
            return
        L_0x006a:
            r2 = move-exception
            r0 = r1
        L_0x006c:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.s36.k
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Exception when parse time e="
            r6.append(r7)
            r6.append(r2)
            java.lang.String r2 = r6.toString()
            r4.e(r5, r2)
            r2 = r1
            goto L_0x0053
        L_0x008a:
            int r1 = r0 + 12
            goto L_0x0064
        L_0x008d:
            if (r0 == r3) goto L_0x0064
            r1 = r0
            goto L_0x0064
        L_0x0091:
            if (r11 != 0) goto L_0x009d
            if (r0 != r3) goto L_0x009b
        L_0x0095:
            int r0 = r1 * 60
            int r0 = r0 + r2
            r8.e = r0
            goto L_0x0069
        L_0x009b:
            r1 = r0
            goto L_0x0095
        L_0x009d:
            if (r0 != r3) goto L_0x00a1
            r1 = r3
            goto L_0x0095
        L_0x00a1:
            int r1 = r0 + 12
            goto L_0x0095
        L_0x00a4:
            r2 = move-exception
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.s36.p(java.lang.String, java.lang.String, boolean):void");
    }
}
