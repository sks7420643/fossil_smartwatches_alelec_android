package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hl4 {
    @DexIgnore
    /* renamed from: a */
    public hl4 clone() throws CloneNotSupportedException {
        return (hl4) super.clone();
    }

    @DexIgnore
    public abstract hl4 b(el4 el4) throws IOException;

    @DexIgnore
    public String toString() {
        return il4.d(this);
    }
}
