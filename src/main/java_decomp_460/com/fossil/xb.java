package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ wb CREATOR; // = new wb(null);
    @DexIgnore
    public /* final */ double b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public xb(double d, int i) {
        this.b = d;
        this.c = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(xb.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            xb xbVar = (xb) obj;
            if (this.b != xbVar.b) {
                return false;
            }
            return this.c == xbVar.c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutGPSDistance");
    }

    @DexIgnore
    public int hashCode() {
        return (Double.valueOf(this.b).hashCode() * 31) + Integer.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(new JSONObject(), jd0.V1, Double.valueOf(this.b)), jd0.L5, Integer.valueOf(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
