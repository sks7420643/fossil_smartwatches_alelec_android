package com.fossil;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xz2 implements Serializable, Iterable<Byte> {
    @DexIgnore
    public static /* final */ c03 b; // = (qz2.b() ? new g03(null) : new a03(null));
    @DexIgnore
    public static /* final */ xz2 zza; // = new h03(h13.b);
    @DexIgnore
    public int zzc; // = 0;

    @DexIgnore
    public static int a(byte b2) {
        return b2 & 255;
    }

    @DexIgnore
    public static xz2 zza(String str) {
        return new h03(str.getBytes(h13.f1410a));
    }

    @DexIgnore
    public static xz2 zza(byte[] bArr) {
        return new h03(bArr);
    }

    @DexIgnore
    public static xz2 zza(byte[] bArr, int i, int i2) {
        zzb(i, i + i2, bArr.length);
        return new h03(b.a(bArr, i, i2));
    }

    @DexIgnore
    public static int zzb(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    @DexIgnore
    public static f03 zzc(int i) {
        return new f03(i, null);
    }

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public final int hashCode() {
        int i = this.zzc;
        if (i == 0) {
            int zza2 = zza();
            i = zza(zza2, 0, zza2);
            if (i == 0) {
                i = 1;
            }
            this.zzc = i;
        }
        return i;
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator' to match base method */
    @Override // java.lang.Iterable
    public /* synthetic */ Iterator<Byte> iterator() {
        return new wz2(this);
    }

    @DexIgnore
    public final String toString() {
        return String.format(Locale.ROOT, "<ByteString@%s size=%d contents=\"%s\">", Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(zza()), zza() <= 50 ? t33.a(this) : String.valueOf(t33.a(zza(0, 47))).concat("..."));
    }

    @DexIgnore
    public abstract byte zza(int i);

    @DexIgnore
    public abstract int zza();

    @DexIgnore
    public abstract int zza(int i, int i2, int i3);

    @DexIgnore
    public abstract xz2 zza(int i, int i2);

    @DexIgnore
    public abstract String zza(Charset charset);

    @DexIgnore
    public abstract void zza(uz2 uz2) throws IOException;

    @DexIgnore
    public abstract byte zzb(int i);

    @DexIgnore
    public final String zzb() {
        return zza() == 0 ? "" : zza(h13.f1410a);
    }

    @DexIgnore
    public abstract boolean zzc();

    @DexIgnore
    public final int zzd() {
        return this.zzc;
    }
}
