package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu2 extends e13<qu2, a> implements o23 {
    @DexIgnore
    public static /* final */ qu2 zzh;
    @DexIgnore
    public static volatile z23<qu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public int zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<qu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(qu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ a(ou2 ou2) {
            this();
        }

        @DexIgnore
        public final boolean B() {
            return ((qu2) this.c).H();
        }

        @DexIgnore
        public final boolean C() {
            return ((qu2) this.c).I();
        }

        @DexIgnore
        public final int E() {
            return ((qu2) this.c).J();
        }

        @DexIgnore
        public final a x(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((qu2) this.c).E(str);
            return this;
        }

        @DexIgnore
        public final String y() {
            return ((qu2) this.c).C();
        }

        @DexIgnore
        public final boolean z() {
            return ((qu2) this.c).G();
        }
    }

    /*
    static {
        qu2 qu2 = new qu2();
        zzh = qu2;
        e13.u(qu2.class, qu2);
    }
    */

    @DexIgnore
    public final String C() {
        return this.zzd;
    }

    @DexIgnore
    public final void E(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    @DexIgnore
    public final boolean G() {
        return this.zze;
    }

    @DexIgnore
    public final boolean H() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean I() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int J() {
        return this.zzg;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (ou2.f2725a[i - 1]) {
            case 1:
                return new qu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1007\u0001\u0003\u1007\u0002\u0004\u1004\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                z23<qu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (qu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new e13.c(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
