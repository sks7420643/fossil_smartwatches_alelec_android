package com.fossil;

import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep {
    @DexIgnore
    public /* synthetic */ ep(kq7 kq7) {
    }

    @DexIgnore
    public final byte[] a(JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        pq7.b(jSONObject2, "jsonFileContent.toString()");
        Charset c = hd0.y.c();
        if (jSONObject2 != null) {
            byte[] bytes = jSONObject2.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }
}
