package com.fossil;

import android.animation.TypeEvaluator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ww3 implements TypeEvaluator<Integer> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ww3 f4005a; // = new ww3();

    @DexIgnore
    public static ww3 b() {
        return f4005a;
    }

    @DexIgnore
    /* renamed from: a */
    public Integer evaluate(float f, Integer num, Integer num2) {
        int intValue = num.intValue();
        float f2 = ((float) ((intValue >> 24) & 255)) / 255.0f;
        int intValue2 = num2.intValue();
        float pow = (float) Math.pow((double) (((float) ((intValue >> 16) & 255)) / 255.0f), 2.2d);
        float pow2 = (float) Math.pow((double) (((float) ((intValue >> 8) & 255)) / 255.0f), 2.2d);
        float pow3 = (float) Math.pow((double) (((float) (intValue & 255)) / 255.0f), 2.2d);
        float pow4 = (float) Math.pow((double) (((float) ((intValue2 >> 16) & 255)) / 255.0f), 2.2d);
        float pow5 = (float) Math.pow((double) (((float) ((intValue2 >> 8) & 255)) / 255.0f), 2.2d);
        float pow6 = (float) Math.pow((double) (((float) (intValue2 & 255)) / 255.0f), 2.2d);
        float pow7 = (float) Math.pow((double) (pow2 + ((pow5 - pow2) * f)), 0.45454545454545453d);
        int round = Math.round((f2 + (((((float) ((intValue2 >> 24) & 255)) / 255.0f) - f2) * f)) * 255.0f) << 24;
        return Integer.valueOf(Math.round(((float) Math.pow((double) (pow3 + ((pow6 - pow3) * f)), 0.45454545454545453d)) * 255.0f) | round | (Math.round(((float) Math.pow((double) (pow + ((pow4 - pow) * f)), 0.45454545454545453d)) * 255.0f) << 16) | (Math.round(255.0f * pow7) << 8));
    }
}
