package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yl1 extends ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yl1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public yl1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    return new yl1(readString, readInt, readString2);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public yl1[] newArray(int i) {
            return new yl1[i];
        }
    }

    @DexIgnore
    public yl1(String str, int i, String str2) throws IllegalArgumentException {
        this.b = str;
        this.c = i;
        this.d = str2;
        if (!(str2.length() <= 10)) {
            StringBuilder e = e.e("traffic(");
            e.append(this.d);
            e.append(") length must be less than or equal to 10");
            throw new IllegalArgumentException(e.toString().toString());
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getCommuteTimeInMinute() {
        return this.c;
    }

    @DexIgnore
    public final String getDestination() {
        return this.b;
    }

    @DexIgnore
    public final String getTraffic() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.s, this.b), jd0.x4, Integer.valueOf(this.c)), jd0.z4, this.d);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
    }
}
