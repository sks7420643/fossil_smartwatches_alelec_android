package com.fossil;

import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bh5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerView q;
    @DexIgnore
    public /* final */ ScrollView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;

    @DexIgnore
    public bh5(Object obj, View view, int i, RecyclerView recyclerView, ScrollView scrollView, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = recyclerView;
        this.r = scrollView;
        this.s = constraintLayout;
    }
}
