package com.fossil;

import java.nio.ByteBuffer;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ob {
    OTA((byte) 0),
    ACTIVITY_FILE((byte) 1),
    HARDWARE_LOG((byte) 2),
    FONT((byte) 3),
    MUSIC_CONTROL((byte) 4),
    UI_SCRIPT((byte) 5),
    MICRO_APP((byte) 6),
    ASSET((byte) 7),
    DEVICE_CONFIG((byte) 8),
    NOTIFICATION((byte) 9),
    ALARM((byte) 10),
    DEVICE_INFO((byte) 11),
    NOTIFICATION_FILTER((byte) 12),
    DEPRECATED_UI_PACKAGE_FILE((byte) 13),
    WATCH_PARAMETERS_FILE(DateTimeFieldType.HOUR_OF_HALFDAY),
    LUTS_FILE(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    RATE_FILE(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    DATA_COLLECTION_FILE(DateTimeFieldType.HOUR_OF_DAY),
    REPLY_MESSAGES_FILE(DateTimeFieldType.MINUTE_OF_HOUR),
    UI_ENCRYPTED_FILE(DateTimeFieldType.SECOND_OF_DAY),
    UI_PACKAGE_FILE(DateTimeFieldType.SECOND_OF_MINUTE),
    ALL_FILE((byte) 255);
    
    @DexIgnore
    public static /* final */ mb A; // = new mb(null);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    public ob(byte b2) {
        this.c = b2;
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{b2, (byte) 255});
        pq7.b(wrap, "ByteBuffer.wrap(byteArrayOf(id, (0xFF).toByte()))");
        this.b = wrap.getShort();
    }

    @DexIgnore
    public final short a(byte b2) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{this.c, b2});
        pq7.b(wrap, "ByteBuffer.wrap(byteArrayOf(id, index))");
        return wrap.getShort();
    }
}
