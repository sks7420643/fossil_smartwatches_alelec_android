package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt4 implements Factory<vt4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f3995a;
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> b;

    @DexIgnore
    public wt4(Provider<on5> provider, Provider<ApiServiceV2> provider2) {
        this.f3995a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static wt4 a(Provider<on5> provider, Provider<ApiServiceV2> provider2) {
        return new wt4(provider, provider2);
    }

    @DexIgnore
    public static vt4 c(on5 on5, ApiServiceV2 apiServiceV2) {
        return new vt4(on5, apiServiceV2);
    }

    @DexIgnore
    /* renamed from: b */
    public vt4 get() {
        return c(this.f3995a.get(), this.b.get());
    }
}
