package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cj extends bi {
    @DexIgnore
    public cj(k5 k5Var, i60 i60, yp ypVar, HashMap<hu1, Object> hashMap, String str) {
        super(k5Var, i60, ypVar, ke.b.a(k5Var.x, ob.HARDWARE_LOG), true, hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 64);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ cj(k5 k5Var, i60 i60, HashMap hashMap, String str, int i) {
        this(k5Var, i60, yp.o, (i & 4) != 0 ? new HashMap() : hashMap, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public final byte[][] a0() {
        ArrayList<j0> arrayList = this.I;
        ArrayList arrayList2 = new ArrayList(im7.m(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(it.next().f);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public Object x() {
        return a0();
    }
}
