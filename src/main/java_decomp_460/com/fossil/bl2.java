package com.fossil;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bl2 implements IInterface {
    @DexIgnore
    public /* final */ IBinder b;

    @DexIgnore
    public bl2(IBinder iBinder, String str) {
        this.b = iBinder;
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.b;
    }
}
