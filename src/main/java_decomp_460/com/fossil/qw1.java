package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw1 extends lw1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qw1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qw1 createFromParcel(Parcel parcel) {
            return new qw1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qw1[] newArray(int i) {
            return new qw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ qw1(Parcel parcel, kq7 kq7) {
        super(parcel);
        k();
    }

    @DexIgnore
    public qw1(ry1 ry1, yb0 yb0, cc0[] cc0Arr, cc0[] cc0Arr2, cc0[] cc0Arr3, cc0[] cc0Arr4, cc0[] cc0Arr5, cc0[] cc0Arr6, cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        k();
    }

    @DexIgnore
    @Override // com.fossil.lw1
    private final void k() {
        if (!(getThemeClassifier() == ec0.STATIC)) {
            throw new IllegalArgumentException("Incorrect theme classifier.".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.iw1, com.fossil.iw1, com.fossil.lw1, java.lang.Object
    public qw1 clone() {
        ry1 clone = h().clone();
        jw1 jw1 = g().b;
        ry1 ry1 = new ry1(g().c.getMajor(), 0);
        boolean z = g().d;
        byte[] bArr = g().e;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        qw1 qw1 = new qw1(clone, new yb0(jw1, ry1, z, copyOf), (cc0[]) f().clone(), (cc0[]) b().clone(), (cc0[]) d().clone(), (cc0[]) e().clone(), (cc0[]) c().clone(), (cc0[]) a().clone(), (cc0[]) i().clone());
        qw1.f()[0] = new cc0(g80.e(0, 1), qw1.f()[0].c);
        return qw1;
    }
}
