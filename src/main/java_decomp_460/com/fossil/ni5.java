package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ni5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2528a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;

    /*
    static {
        int[] iArr = new int[gi5.values().length];
        f2528a = iArr;
        iArr[gi5.INDOOR.ordinal()] = 1;
        f2528a[gi5.OUTDOOR.ordinal()] = 2;
        int[] iArr2 = new int[mi5.values().length];
        b = iArr2;
        iArr2[mi5.UNKNOWN.ordinal()] = 1;
        b[mi5.RUNNING.ordinal()] = 2;
        b[mi5.CYCLING.ordinal()] = 3;
        b[mi5.SPINNING.ordinal()] = 4;
        b[mi5.TREADMILL.ordinal()] = 5;
        b[mi5.ELLIPTICAL.ordinal()] = 6;
        b[mi5.WEIGHTS.ordinal()] = 7;
        b[mi5.WORKOUT.ordinal()] = 8;
        b[mi5.YOGA.ordinal()] = 9;
        b[mi5.WALKING.ordinal()] = 10;
        b[mi5.ROWING.ordinal()] = 11;
        b[mi5.SWIMMING.ordinal()] = 12;
        b[mi5.AEROBIC.ordinal()] = 13;
        b[mi5.HIKING.ordinal()] = 14;
        int[] iArr3 = new int[oi5.values().length];
        c = iArr3;
        iArr3[oi5.UNKNOWN.ordinal()] = 1;
        c[oi5.RUNNING.ordinal()] = 2;
        c[oi5.SPINNING.ordinal()] = 3;
        c[oi5.OUTDOOR_CYCLING.ordinal()] = 4;
        c[oi5.TREADMILL.ordinal()] = 5;
        c[oi5.ELLIPTICAL.ordinal()] = 6;
        c[oi5.WEIGHTS.ordinal()] = 7;
        c[oi5.WORKOUT.ordinal()] = 8;
        c[oi5.WALK.ordinal()] = 9;
        c[oi5.ROW_MACHINE.ordinal()] = 10;
        c[oi5.HIKING.ordinal()] = 11;
        int[] iArr4 = new int[oi5.values().length];
        d = iArr4;
        iArr4[oi5.UNKNOWN.ordinal()] = 1;
        d[oi5.SPINNING.ordinal()] = 2;
        d[oi5.OUTDOOR_CYCLING.ordinal()] = 3;
        d[oi5.TREADMILL.ordinal()] = 4;
        d[oi5.ELLIPTICAL.ordinal()] = 5;
        d[oi5.WEIGHTS.ordinal()] = 6;
        d[oi5.WORKOUT.ordinal()] = 7;
        d[oi5.ROW_MACHINE.ordinal()] = 8;
        int[] iArr5 = new int[oi5.values().length];
        e = iArr5;
        iArr5[oi5.RUNNING.ordinal()] = 1;
        e[oi5.SPINNING.ordinal()] = 2;
        e[oi5.OUTDOOR_CYCLING.ordinal()] = 3;
        e[oi5.TREADMILL.ordinal()] = 4;
        e[oi5.ELLIPTICAL.ordinal()] = 5;
        e[oi5.WEIGHTS.ordinal()] = 6;
        e[oi5.WORKOUT.ordinal()] = 7;
        e[oi5.WALK.ordinal()] = 8;
        e[oi5.ROW_MACHINE.ordinal()] = 9;
        e[oi5.HIKING.ordinal()] = 10;
        e[oi5.UNKNOWN.ordinal()] = 11;
    }
    */
}
