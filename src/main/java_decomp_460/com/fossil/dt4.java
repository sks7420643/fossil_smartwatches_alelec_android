package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f830a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public Date d;
    @DexIgnore
    public ss4 e;
    @DexIgnore
    public lt4 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Date i;

    @DexIgnore
    public dt4(String str, String str2, String str3, Date date, ss4 ss4, lt4 lt4, boolean z, int i2, Date date2) {
        pq7.c(str, "id");
        pq7.c(str2, "titleKey");
        pq7.c(str3, "bodyKey");
        pq7.c(date, "createdAt");
        this.f830a = str;
        this.b = str2;
        this.c = str3;
        this.d = date;
        this.e = ss4;
        this.f = lt4;
        this.g = z;
        this.h = i2;
        this.i = date2;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final ss4 b() {
        return this.e;
    }

    @DexIgnore
    public final boolean c() {
        return this.g;
    }

    @DexIgnore
    public final Date d() {
        return this.d;
    }

    @DexIgnore
    public final String e() {
        return this.f830a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof dt4) {
                dt4 dt4 = (dt4) obj;
                if (!pq7.a(this.f830a, dt4.f830a) || !pq7.a(this.b, dt4.b) || !pq7.a(this.c, dt4.c) || !pq7.a(this.d, dt4.d) || !pq7.a(this.e, dt4.e) || !pq7.a(this.f, dt4.f) || this.g != dt4.g || this.h != dt4.h || !pq7.a(this.i, dt4.i)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final lt4 f() {
        return this.f;
    }

    @DexIgnore
    public final int g() {
        return this.h;
    }

    @DexIgnore
    public final Date h() {
        return this.i;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.f830a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Date date = this.d;
        int hashCode4 = date != null ? date.hashCode() : 0;
        ss4 ss4 = this.e;
        int hashCode5 = ss4 != null ? ss4.hashCode() : 0;
        lt4 lt4 = this.f;
        int hashCode6 = lt4 != null ? lt4.hashCode() : 0;
        boolean z = this.g;
        if (z) {
            z = true;
        }
        int i3 = this.h;
        Date date2 = this.i;
        if (date2 != null) {
            i2 = date2.hashCode();
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i4) * 31) + i3) * 31) + i2;
    }

    @DexIgnore
    public final String i() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "Notification(id=" + this.f830a + ", titleKey=" + this.b + ", bodyKey=" + this.c + ", createdAt=" + this.d + ", challengeData=" + this.e + ", profileData=" + this.f + ", confirmChallenge=" + this.g + ", rank=" + this.h + ", reachGoalAt=" + this.i + ")";
    }
}
