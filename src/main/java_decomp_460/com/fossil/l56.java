package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.dk5;
import com.fossil.tq4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l56 extends tq4<tq4.b, a, tq4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ Context d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tq4.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<i06> f2146a;

        @DexIgnore
        public a(List<i06> list) {
            pq7.c(list, "apps");
            this.f2146a = list;
        }

        @DexIgnore
        public final List<i06> a() {
            return this.f2146a;
        }
    }

    /*
    static {
        String simpleName = v36.class.getSimpleName();
        pq7.b(simpleName, "GetApps::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public l56(Context context) {
        pq7.c(context, "mContext");
        this.d = context;
    }

    @DexIgnore
    @Override // com.fossil.tq4
    public void a(tq4.b bVar) {
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase GetHybridApps");
        List<AppFilter> allAppFilters = mn5.p.a().c().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<dk5.b> it = dk5.g.g().iterator();
        while (it.hasNext()) {
            dk5.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !vt7.j(next.b(), this.d.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), Boolean.FALSE);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    pq7.b(next2, "appFilter");
                    if (pq7.a(next2.getType(), installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                i06 i06 = new i06();
                i06.setInstalledApp(installedApp);
                i06.setUri(next.c());
                i06.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(i06);
            }
        }
        lm7.q(linkedList);
        b().onSuccess(new a(linkedList));
    }
}
