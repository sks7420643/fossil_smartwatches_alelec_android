package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class i64 implements mg4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ j64 f1589a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public i64(j64 j64, Context context) {
        this.f1589a = j64;
        this.b = context;
    }

    @DexIgnore
    public static mg4 a(j64 j64, Context context) {
        return new i64(j64, context);
    }

    @DexIgnore
    @Override // com.fossil.mg4
    public Object get() {
        return j64.r(this.f1589a, this.b);
    }
}
