package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h extends qq7 implements rp7<BluetoothDevice, String> {
    @DexIgnore
    public static /* final */ h b; // = new h();

    @DexIgnore
    public h() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public String invoke(BluetoothDevice bluetoothDevice) {
        String address = bluetoothDevice.getAddress();
        pq7.b(address, "it.address");
        return address;
    }
}
