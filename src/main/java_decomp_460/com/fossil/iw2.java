package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class iw2 implements SharedPreferences.OnSharedPreferenceChangeListener {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jw2 f1677a;

    @DexIgnore
    public iw2(jw2 jw2) {
        this.f1677a = jw2;
    }

    @DexIgnore
    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.f1677a.c(sharedPreferences, str);
    }
}
