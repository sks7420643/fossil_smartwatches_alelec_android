package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.ui.debug.DebugActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs5 implements MembersInjector<DebugActivity> {
    @DexIgnore
    public static void a(DebugActivity debugActivity, mj5 mj5) {
        debugActivity.G = mj5;
    }

    @DexIgnore
    public static void b(DebugActivity debugActivity, DianaPresetRepository dianaPresetRepository) {
        debugActivity.E = dianaPresetRepository;
    }

    @DexIgnore
    public static void c(DebugActivity debugActivity, FirmwareFileRepository firmwareFileRepository) {
        debugActivity.C = firmwareFileRepository;
    }

    @DexIgnore
    public static void d(DebugActivity debugActivity, GuestApiService guestApiService) {
        debugActivity.D = guestApiService;
    }

    @DexIgnore
    public static void e(DebugActivity debugActivity, xt5 xt5) {
        debugActivity.B = xt5;
    }

    @DexIgnore
    public static void f(DebugActivity debugActivity, br5 br5) {
        debugActivity.F = br5;
    }

    @DexIgnore
    public static void g(DebugActivity debugActivity, on5 on5) {
        debugActivity.A = on5;
    }
}
