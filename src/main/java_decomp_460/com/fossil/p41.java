package com.fossil;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p41 {
    @DexIgnore
    public static /* final */ p41 c; // = new p41();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ExecutorService f2775a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public ThreadLocal<Integer> b;

        @DexIgnore
        public b() {
            this.b = new ThreadLocal<>();
        }

        @DexIgnore
        public final int a() {
            Integer num = this.b.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.b.remove();
            } else {
                this.b.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        @DexIgnore
        public final int b() {
            Integer num = this.b.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.b.set(Integer.valueOf(intValue));
            return intValue;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            if (b() <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a();
                    throw th;
                }
            } else {
                p41.a().execute(runnable);
            }
            a();
        }
    }

    @DexIgnore
    public p41() {
        this.f2775a = !c() ? Executors.newCachedThreadPool() : m41.b();
        Executors.newSingleThreadScheduledExecutor();
        this.b = new b();
    }

    @DexIgnore
    public static ExecutorService a() {
        return c.f2775a;
    }

    @DexIgnore
    public static Executor b() {
        return c.b;
    }

    @DexIgnore
    public static boolean c() {
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(Locale.US).contains("android");
    }
}
