package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn5 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ HashMap<a, ArrayList<c>> f1781a;
    @DexIgnore
    public static /* final */ jn5 b; // = new jn5();

    @DexIgnore
    public enum a {
        SET_BLE_COMMAND,
        PAIR_DEVICE,
        NOTIFICATION_HYBRID,
        NOTIFICATION_DIANA,
        NOTIFICATION_GET_CONTACTS,
        NOTIFICATION_APPS,
        SET_COMPLICATION_WATCH_APP_WEATHER,
        SET_COMPLICATION_CHANCE_OF_RAIN,
        SET_WATCH_APP_MUSIC,
        SET_WATCH_APP_COMMUTE_TIME,
        FIND_DEVICE,
        EDIT_AVATAR,
        UPDATE_FIRMWARE,
        SET_MICRO_APP_MUSIC,
        SET_MICRO_APP_COMMUTE_TIME,
        GET_USER_LOCATION,
        CAPTURE_IMAGE,
        QUICK_RESPONSE
    }

    @DexIgnore
    public enum b {
        LOCATION_GROUP,
        SMS_GROUP,
        PHONE_GROUP
    }

    @DexIgnore
    public enum c {
        BLUETOOTH_CONNECTION,
        NOTIFICATION_ACCESS,
        LOCATION_FINE,
        LOCATION_SERVICE,
        LOCATION_BACKGROUND,
        READ_CONTACTS,
        READ_PHONE_STATE,
        READ_CALL_LOG,
        READ_SMS,
        RECEIVE_SMS,
        RECEIVE_MMS,
        CAMERA,
        READ_EXTERNAL_STORAGE,
        CALL_PHONE,
        ANSWER_PHONE_CALL,
        SEND_SMS
    }

    /*
    static {
        HashMap<a, ArrayList<c>> hashMap = new HashMap<>();
        hashMap.put(a.SET_BLE_COMMAND, hm7.c(c.BLUETOOTH_CONNECTION));
        hashMap.put(a.PAIR_DEVICE, hm7.c(c.BLUETOOTH_CONNECTION, c.LOCATION_FINE, c.LOCATION_SERVICE));
        hashMap.put(a.UPDATE_FIRMWARE, hm7.c(c.BLUETOOTH_CONNECTION, c.LOCATION_FINE, c.LOCATION_SERVICE));
        hashMap.put(a.FIND_DEVICE, hm7.c(c.LOCATION_FINE, c.LOCATION_SERVICE));
        hashMap.put(a.NOTIFICATION_DIANA, hm7.c(c.READ_CONTACTS, c.READ_PHONE_STATE, c.READ_CALL_LOG, c.CALL_PHONE, c.ANSWER_PHONE_CALL, c.READ_SMS, c.RECEIVE_SMS, c.RECEIVE_MMS, c.NOTIFICATION_ACCESS));
        hashMap.put(a.NOTIFICATION_HYBRID, hm7.c(c.READ_CONTACTS, c.READ_PHONE_STATE, c.READ_CALL_LOG, c.READ_SMS, c.RECEIVE_SMS, c.RECEIVE_MMS, c.NOTIFICATION_ACCESS));
        hashMap.put(a.NOTIFICATION_GET_CONTACTS, hm7.c(c.READ_CONTACTS));
        hashMap.put(a.NOTIFICATION_APPS, hm7.c(c.NOTIFICATION_ACCESS));
        hashMap.put(a.QUICK_RESPONSE, hm7.c(c.SEND_SMS));
        hashMap.put(a.SET_COMPLICATION_WATCH_APP_WEATHER, hm7.c(c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND));
        hashMap.put(a.SET_COMPLICATION_CHANCE_OF_RAIN, hm7.c(c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND));
        hashMap.put(a.SET_WATCH_APP_MUSIC, hm7.c(c.NOTIFICATION_ACCESS));
        hashMap.put(a.SET_WATCH_APP_COMMUTE_TIME, hm7.c(c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND));
        hashMap.put(a.SET_MICRO_APP_MUSIC, hm7.c(c.NOTIFICATION_ACCESS));
        hashMap.put(a.SET_MICRO_APP_COMMUTE_TIME, hm7.c(c.LOCATION_FINE, c.LOCATION_SERVICE, c.LOCATION_BACKGROUND));
        hashMap.put(a.EDIT_AVATAR, hm7.c(c.CAMERA));
        hashMap.put(a.GET_USER_LOCATION, hm7.c(c.LOCATION_FINE, c.LOCATION_SERVICE));
        hashMap.put(a.CAPTURE_IMAGE, hm7.c(c.CAMERA));
        f1781a = hashMap;
    }
    */

    @DexIgnore
    public static /* synthetic */ boolean c(jn5 jn5, Context context, a aVar, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        boolean z4 = false;
        boolean z5 = (i & 4) != 0 ? true : z;
        boolean z6 = (i & 8) != 0 ? false : z2;
        if ((i & 16) == 0) {
            z4 = z3;
        }
        return jn5.b(context, aVar, z5, z6, z4, (i & 32) != 0 ? null : num);
    }

    @DexIgnore
    public static /* synthetic */ boolean f(jn5 jn5, Context context, List list, List list2, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        return jn5.d(context, (i & 2) != 0 ? null : list, list2, (i & 8) != 0 ? true : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? false : z3, (i & 64) != 0 ? null : num);
    }

    @DexIgnore
    public static /* synthetic */ boolean g(jn5 jn5, Context context, List list, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        boolean z4 = false;
        boolean z5 = (i & 4) != 0 ? true : z;
        boolean z6 = (i & 8) != 0 ? false : z2;
        if ((i & 16) == 0) {
            z4 = z3;
        }
        return jn5.e(context, list, z5, z6, z4, (i & 32) != 0 ? null : num);
    }

    @DexIgnore
    public final void a(Context context, ArrayList<uh5> arrayList, List<a> list) {
        pq7.c(arrayList, "errorCodes");
        pq7.c(list, "features");
        ArrayList<c> arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            int i = kn5.f1936a[it.next().ordinal()];
            if (i == 1) {
                arrayList2.add(c.BLUETOOTH_CONNECTION);
            } else if (i == 2) {
                arrayList2.add(c.LOCATION_BACKGROUND);
            } else if (i == 3) {
                arrayList2.add(c.LOCATION_FINE);
            } else if (i == 4) {
                arrayList2.add(c.LOCATION_SERVICE);
            }
        }
        for (c cVar : arrayList2) {
            gl7<String, String, String> l = b.l(cVar, list);
            String component1 = l.component1();
            String component2 = l.component2();
            String component3 = l.component3();
            arrayList3.add(new PermissionData("PERMISSION_REQUEST_TYPE", b.j(cVar), cVar, b.i(cVar), component1, component2, component3 != null ? component3 : "", false));
        }
        if ((!arrayList2.isEmpty()) && context != null) {
            PermissionActivity.a.b(PermissionActivity.B, context, arrayList3, false, false, null, 28, null);
        }
    }

    @DexIgnore
    public final boolean b(Context context, a aVar, boolean z, boolean z2, boolean z3, Integer num) {
        if (aVar == null) {
            return true;
        }
        return b.h(context, null, hm7.i(aVar), z, z2, z3, num);
    }

    @DexIgnore
    public final boolean d(Context context, List<String> list, List<? extends a> list2, boolean z, boolean z2, boolean z3, Integer num) {
        pq7.c(list2, "features");
        return h(context, list, list2, z, z2, z3, num);
    }

    @DexIgnore
    public final boolean e(Context context, List<? extends a> list, boolean z, boolean z2, boolean z3, Integer num) {
        pq7.c(list, "features");
        return h(context, null, list, z, z2, z3, num);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v23, resolved type: com.portfolio.platform.uirenew.permission.PermissionActivity$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final boolean h(Context context, List<String> list, List<? extends a> list2, boolean z, boolean z2, boolean z3, Integer num) {
        WeakReference weakReference = new WeakReference(context);
        HashMap hashMap = new HashMap();
        for (T t : list2) {
            List<c> k = b.k(t);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PermissionManager", "feature " + ((Object) t) + " appPermissionIdsNotGranted " + k);
            for (T t2 : k) {
                List list3 = (List) hashMap.get(t2);
                if (list3 != null) {
                    list3.add(t);
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(t);
                    hashMap.put(t2, arrayList);
                    tl7 tl7 = tl7.f3441a;
                }
            }
        }
        if (!(!hashMap.isEmpty())) {
            if (list == null || list.isEmpty()) {
                return true;
            }
            Context context2 = (Context) weakReference.get();
            if (context2 != null) {
                ArrayList<PermissionData> arrayList2 = new ArrayList<>();
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    arrayList2.add(new PermissionData("PERMISSION_SETTING_TYPE", new ArrayList(), null, "", it.next(), "", "", false));
                }
                if (!arrayList2.isEmpty()) {
                    PermissionActivity.a aVar = PermissionActivity.B;
                    pq7.b(context2, "it");
                    aVar.a(context2, arrayList2, z3, z2, num);
                }
                tl7 tl72 = tl7.f3441a;
            }
            return false;
        } else if (!z) {
            return false;
        } else {
            ArrayList arrayList3 = new ArrayList();
            Set<c> keySet = hashMap.keySet();
            pq7.b(keySet, "permissionUsedByFeaturesMap.keys");
            for (c cVar : keySet) {
                jn5 jn5 = b;
                Object obj = hashMap.get(cVar);
                if (obj != null) {
                    gl7<String, String, String> l = jn5.l(cVar, (List) obj);
                    String component1 = l.component1();
                    String component2 = l.component2();
                    String component3 = l.component3();
                    jn5 jn52 = b;
                    pq7.b(cVar, "appPermissionId");
                    arrayList3.add(new PermissionData("PERMISSION_REQUEST_TYPE", jn52.j(cVar), cVar, b.i(cVar), component1, component2, component3 != null ? component3 : "", false));
                } else {
                    pq7.i();
                    throw null;
                }
            }
            Context context3 = (Context) weakReference.get();
            if (context3 != null) {
                ArrayList arrayList4 = new ArrayList();
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Object obj2 : arrayList3) {
                    String permissionGroup = ((PermissionData) obj2).getPermissionGroup();
                    Object obj3 = linkedHashMap.get(permissionGroup);
                    if (obj3 == null) {
                        obj3 = new ArrayList();
                        linkedHashMap.put(permissionGroup, obj3);
                    }
                    ((List) obj3).add(obj2);
                }
                Collection<List> values = linkedHashMap.values();
                pq7.b(values, "permissionDatasGroupByPermissionGroup.values");
                for (List<PermissionData> list4 : values) {
                    pq7.b(list4, "sameGroupPermissionDataList");
                    if (!list4.isEmpty()) {
                        ArrayList<String> arrayList5 = new ArrayList<>();
                        for (PermissionData permissionData : list4) {
                            arrayList5.addAll(permissionData.getAndroidPermissionSet());
                        }
                        ((PermissionData) list4.get(0)).setAndroidPermissionSet(arrayList5);
                        arrayList4.add(list4.get(0));
                    }
                }
                if (list != null) {
                    Iterator<T> it2 = list.iterator();
                    while (it2.hasNext()) {
                        arrayList4.add(new PermissionData("PERMISSION_SETTING_TYPE", new ArrayList(), null, "", it2.next(), "", "", false));
                    }
                    tl7 tl73 = tl7.f3441a;
                }
                PermissionActivity.a aVar2 = PermissionActivity.B;
                pq7.b(context3, "it");
                aVar2.a(context3, arrayList4, z3, z2, num);
                tl7 tl74 = tl7.f3441a;
            }
            return false;
        }
    }

    @DexIgnore
    public final String i(c cVar) {
        switch (kn5.b[cVar.ordinal()]) {
            case 1:
            case 2:
                return b.LOCATION_GROUP.name();
            case 3:
            case 4:
            case 5:
            case 6:
                return b.PHONE_GROUP.name();
            case 7:
            case 8:
            case 9:
                return b.SMS_GROUP.name();
            default:
                return cVar.name();
        }
    }

    @DexIgnore
    public final ArrayList<String> j(c cVar) {
        ArrayList<String> arrayList = new ArrayList<>();
        switch (kn5.d[cVar.ordinal()]) {
            case 1:
                arrayList.add("android.permission.ACCESS_FINE_LOCATION");
                break;
            case 2:
                if (nk5.o.s()) {
                    arrayList.add("android.permission.ACCESS_BACKGROUND_LOCATION");
                    break;
                }
                break;
            case 3:
                arrayList.add("android.permission.READ_CONTACTS");
                break;
            case 4:
                arrayList.add("android.permission.READ_PHONE_STATE");
                break;
            case 5:
                arrayList.add("android.permission.READ_CALL_LOG");
                break;
            case 6:
                if (nk5.o.z() && nk5.o.x(PortfolioApp.h0.c().J())) {
                    arrayList.add("android.permission.CALL_PHONE");
                    break;
                }
            case 7:
                if (nk5.o.z() && nk5.o.x(PortfolioApp.h0.c().J())) {
                    arrayList.add("android.permission.ANSWER_PHONE_CALLS");
                    break;
                }
            case 8:
                arrayList.add("android.permission.READ_SMS");
                break;
            case 9:
                arrayList.add("android.permission.RECEIVE_SMS");
                break;
            case 10:
                arrayList.add("android.permission.RECEIVE_MMS");
                break;
            case 11:
                if (nk5.o.x(PortfolioApp.h0.c().J())) {
                    arrayList.add("android.permission.SEND_SMS");
                    break;
                }
                break;
            case 12:
                arrayList.add("android.permission.CAMERA");
                break;
            case 13:
                arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                break;
        }
        return arrayList;
    }

    @DexIgnore
    public final List<c> k(a aVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "Check permission is granted or not for feature " + aVar);
        ArrayList arrayList = new ArrayList();
        PortfolioApp c2 = PortfolioApp.h0.c();
        for (Map.Entry<a, ArrayList<c>> entry : f1781a.entrySet()) {
            if (entry.getKey() == aVar) {
                Iterator<T> it = entry.getValue().iterator();
                while (it.hasNext()) {
                    switch (kn5.c[it.next().ordinal()]) {
                        case 1:
                            if (!BluetoothUtils.isBluetoothEnable()) {
                                arrayList.add(c.BLUETOOTH_CONNECTION);
                                break;
                            } else {
                                break;
                            }
                        case 2:
                            if (!LocationUtils.isLocationPermissionGranted(c2)) {
                                arrayList.add(c.LOCATION_FINE);
                                break;
                            } else {
                                break;
                            }
                        case 3:
                            if (nk5.o.s() && !LocationUtils.isBackgroundLocationPermissionGranted(c2)) {
                                arrayList.add(c.LOCATION_BACKGROUND);
                                break;
                            }
                        case 4:
                            if (!LocationUtils.isLocationEnable(c2)) {
                                arrayList.add(c.LOCATION_SERVICE);
                                break;
                            } else {
                                break;
                            }
                        case 5:
                            if (!v78.a(c2, "android.permission.READ_CONTACTS")) {
                                arrayList.add(c.READ_CONTACTS);
                                break;
                            } else {
                                break;
                            }
                        case 6:
                            if (!v78.a(c2, "android.permission.READ_PHONE_STATE")) {
                                arrayList.add(c.READ_PHONE_STATE);
                                break;
                            } else {
                                break;
                            }
                        case 7:
                            if (!v78.a(c2, "android.permission.READ_CALL_LOG")) {
                                arrayList.add(c.READ_CALL_LOG);
                                break;
                            } else {
                                break;
                            }
                        case 8:
                            if (!v78.a(c2, "android.permission.CALL_PHONE")) {
                                arrayList.add(c.CALL_PHONE);
                                break;
                            } else {
                                break;
                            }
                        case 9:
                            if (nk5.o.z()) {
                                if (!v78.a(c2, "android.permission.ANSWER_PHONE_CALLS")) {
                                    arrayList.add(c.ANSWER_PHONE_CALL);
                                    break;
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        case 10:
                            if (!v78.a(c2, "android.permission.READ_SMS")) {
                                arrayList.add(c.READ_SMS);
                                break;
                            } else {
                                break;
                            }
                        case 11:
                            if (!v78.a(c2, "android.permission.RECEIVE_SMS")) {
                                arrayList.add(c.RECEIVE_SMS);
                                break;
                            } else {
                                break;
                            }
                        case 12:
                            if (!v78.a(c2, "android.permission.RECEIVE_MMS")) {
                                arrayList.add(c.RECEIVE_MMS);
                                break;
                            } else {
                                break;
                            }
                        case 13:
                            if (!v78.a(c2, "android.permission.SEND_SMS")) {
                                arrayList.add(c.SEND_SMS);
                                break;
                            } else {
                                break;
                            }
                        case 14:
                            if (!PortfolioApp.h0.c().y0()) {
                                arrayList.add(c.NOTIFICATION_ACCESS);
                                break;
                            } else {
                                break;
                            }
                        case 15:
                            if (!v78.a(c2, "android.permission.CAMERA")) {
                                arrayList.add(c.CAMERA);
                                break;
                            } else {
                                break;
                            }
                        case 16:
                            if (!v78.a(c2, "android.permission.READ_EXTERNAL_STORAGE")) {
                                arrayList.add(c.READ_EXTERNAL_STORAGE);
                                break;
                            } else {
                                break;
                            }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final gl7<String, String, String> l(c cVar, List<a> list) {
        boolean z;
        String str;
        String str2;
        String str3;
        String str4;
        String c2;
        if (list != null) {
            int size = list.size();
            str = "";
            z = false;
            int i = 0;
            while (true) {
                if (i < size) {
                    switch (kn5.e[list.get(i).ordinal()]) {
                        case 1:
                            c2 = um5.c(PortfolioApp.h0.c(), 2131886400);
                            pq7.b(c2, "LanguageHelper.getString\u2026naWeather_Title__Weather)");
                            break;
                        case 2:
                        case 3:
                            c2 = um5.c(PortfolioApp.h0.c(), 2131886408);
                            pq7.b(c2, "LanguageHelper.getString\u2026eTime_Title__CommuteTime)");
                            break;
                        case 4:
                            c2 = um5.c(PortfolioApp.h0.c(), 2131886476);
                            pq7.b(c2, "LanguageHelper.getString\u2026Rain_Title__ChanceOfRain)");
                            break;
                        case 5:
                            c2 = um5.c(PortfolioApp.h0.c(), 2131887157);
                            pq7.b(c2, "LanguageHelper.getString\u2026Device_Title__FindDevice)");
                            break;
                        case 6:
                            c2 = um5.c(PortfolioApp.h0.c(), 2131887605);
                            pq7.b(c2, "LanguageHelper.getString\u2026sion_feature_pair_device)");
                            break;
                        case 7:
                        case 8:
                            c2 = "";
                            z = true;
                            break;
                        default:
                            c2 = "";
                            break;
                    }
                    if (!(str.length() == 0)) {
                        if (!wt7.v(str, c2, false, 2, null)) {
                            c2 = str + ym5.b + c2;
                        } else {
                            c2 = str;
                        }
                    }
                    if (i == list.size() - 1) {
                        str = c2;
                    } else {
                        i++;
                        str = c2;
                    }
                }
            }
        } else {
            z = false;
            str = "";
        }
        if (cVar != null) {
            switch (kn5.f[cVar.ordinal()]) {
                case 1:
                    str3 = um5.c(PortfolioApp.h0.c(), 2131886929);
                    pq7.b(str3, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str4 = um5.c(PortfolioApp.h0.c(), 2131886929);
                    pq7.b(str4, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str2 = null;
                    break;
                case 2:
                case 3:
                    if (list == null) {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886927);
                        pq7.b(str3, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886927);
                        pq7.b(str4, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    } else if (list.get(0) == a.PAIR_DEVICE) {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886932);
                        pq7.b(str3, "LanguageHelper.getString\u2026PermissionThisTimeToScan)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886932);
                        pq7.b(str4, "LanguageHelper.getString\u2026PermissionThisTimeToScan)");
                    } else {
                        hr7 hr7 = hr7.f1520a;
                        String c3 = um5.c(PortfolioApp.h0.c(), 2131886926);
                        pq7.b(c3, "LanguageHelper.getString\u2026wPermissionAllTheTimeFor)");
                        str3 = String.format(c3, Arrays.copyOf(new Object[]{str}, 1));
                        pq7.b(str3, "java.lang.String.format(format, *args)");
                        hr7 hr72 = hr7.f1520a;
                        String c4 = um5.c(PortfolioApp.h0.c(), 2131886926);
                        pq7.b(c4, "LanguageHelper.getString\u2026wPermissionAllTheTimeFor)");
                        str4 = String.format(c4, Arrays.copyOf(new Object[]{str}, 1));
                        pq7.b(str4, "java.lang.String.format(format, *args)");
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a2 = um5.a();
                    pq7.b(a2, "LanguageHelper.getLocale()");
                    sb.append(a2.getLanguage());
                    str2 = sb.toString();
                    break;
                case 4:
                    if (list == null) {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886808);
                        pq7.b(str3, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886808);
                        pq7.b(str4, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                    } else if (list.get(0) == a.PAIR_DEVICE) {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886930);
                        pq7.b(str3, "LanguageHelper.getString\u2026nLocationServicesInOrder)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886930);
                        pq7.b(str4, "LanguageHelper.getString\u2026nLocationServicesInOrder)");
                    } else {
                        hr7 hr73 = hr7.f1520a;
                        String c5 = um5.c(PortfolioApp.h0.c(), 2131886940);
                        pq7.b(c5, "LanguageHelper.getString\u2026rnOnLocationServiceSoThe)");
                        str3 = String.format(c5, Arrays.copyOf(new Object[]{str}, 1));
                        pq7.b(str3, "java.lang.String.format(format, *args)");
                        hr7 hr74 = hr7.f1520a;
                        String c6 = um5.c(PortfolioApp.h0.c(), 2131886940);
                        pq7.b(c6, "LanguageHelper.getString\u2026rnOnLocationServiceSoThe)");
                        str4 = String.format(c6, Arrays.copyOf(new Object[]{str}, 1));
                        pq7.b(str4, "java.lang.String.format(format, *args)");
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a3 = um5.a();
                    pq7.b(a3, "LanguageHelper.getLocale()");
                    sb2.append(a3.getLanguage());
                    str2 = sb2.toString();
                    break;
                case 5:
                    str3 = um5.c(PortfolioApp.h0.c(), 2131886936);
                    pq7.b(str3, "LanguageHelper.getString\u2026llowTheAppToReadContacts)");
                    hr7 hr75 = hr7.f1520a;
                    String c7 = um5.c(PortfolioApp.h0.c(), 2131886801);
                    pq7.b(c7, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    str4 = String.format(c7, Arrays.copyOf(new Object[]{PortfolioApp.h0.c().Q()}, 1));
                    pq7.b(str4, "java.lang.String.format(format, *args)");
                    str2 = null;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                    if (!nk5.o.x(PortfolioApp.h0.c().J())) {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886937);
                        pq7.b(str3, "LanguageHelper.getString\u2026__AllowTheAppToReadPhone)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886937);
                        pq7.b(str4, "LanguageHelper.getString\u2026__AllowTheAppToReadPhone)");
                        str2 = null;
                        break;
                    } else {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886935);
                        pq7.b(str3, "LanguageHelper.getString\u2026AllowTheAppToAnswerPhone)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886935);
                        pq7.b(str4, "LanguageHelper.getString\u2026AllowTheAppToAnswerPhone)");
                        str2 = null;
                        break;
                    }
                case 10:
                case 11:
                case 12:
                    str3 = um5.c(PortfolioApp.h0.c(), 2131886938);
                    pq7.b(str3, "LanguageHelper.getString\u2026s___AllowTheAppToReadSms)");
                    str4 = um5.c(PortfolioApp.h0.c(), 2131886938);
                    pq7.b(str4, "LanguageHelper.getString\u2026s___AllowTheAppToReadSms)");
                    str2 = null;
                    break;
                case 13:
                    str3 = um5.c(PortfolioApp.h0.c(), 2131886939);
                    pq7.b(str3, "LanguageHelper.getString\u2026s___AllowTheAppToSendSms)");
                    str4 = um5.c(PortfolioApp.h0.c(), 2131886939);
                    pq7.b(str4, "LanguageHelper.getString\u2026s___AllowTheAppToSendSms)");
                    str2 = null;
                    break;
                case 14:
                    if (!z) {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886933);
                        pq7.b(str3, "LanguageHelper.getString\u2026ns___AllowTheAppToAccess)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886933);
                        pq7.b(str4, "LanguageHelper.getString\u2026ns___AllowTheAppToAccess)");
                        str2 = null;
                        break;
                    } else {
                        str3 = um5.c(PortfolioApp.h0.c(), 2131886934);
                        pq7.b(str3, "LanguageHelper.getString\u2026___AllowTheAppToAccess_1)");
                        str4 = um5.c(PortfolioApp.h0.c(), 2131886934);
                        pq7.b(str4, "LanguageHelper.getString\u2026___AllowTheAppToAccess_1)");
                        str2 = null;
                        break;
                    }
                case 15:
                    hr7 hr76 = hr7.f1520a;
                    String c8 = um5.c(PortfolioApp.h0.c(), 2131886800);
                    pq7.b(c8, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    str3 = String.format(c8, Arrays.copyOf(new Object[]{PortfolioApp.h0.c().Q()}, 1));
                    pq7.b(str3, "java.lang.String.format(format, *args)");
                    hr7 hr77 = hr7.f1520a;
                    String c9 = um5.c(PortfolioApp.h0.c(), 2131886800);
                    pq7.b(c9, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    str4 = String.format(c9, Arrays.copyOf(new Object[]{PortfolioApp.h0.c().Q()}, 1));
                    pq7.b(str4, "java.lang.String.format(format, *args)");
                    str2 = null;
                    break;
                case 16:
                    str3 = um5.c(PortfolioApp.h0.c(), 2131886865);
                    pq7.b(str3, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str4 = um5.c(PortfolioApp.h0.c(), 2131886865);
                    pq7.b(str4, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str2 = null;
                    break;
            }
            return new gl7<>(str3, str4, str2);
        }
        str4 = "";
        str2 = null;
        str3 = "";
        return new gl7<>(str3, str4, str2);
    }

    @DexIgnore
    public final boolean m(Context context, c cVar) {
        pq7.c(cVar, "permissionId");
        WeakReference weakReference = new WeakReference(context);
        switch (kn5.h[cVar.ordinal()]) {
            case 1:
                return BluetoothUtils.isBluetoothEnable();
            case 2:
                Context context2 = (Context) weakReference.get();
                if (context2 != null) {
                    return LocationUtils.isLocationPermissionGranted(context2);
                }
                break;
            case 3:
                Context context3 = (Context) weakReference.get();
                if (context3 != null) {
                    return LocationUtils.isLocationEnable(context3);
                }
                break;
            case 4:
                Context context4 = (Context) weakReference.get();
                if (context4 != null) {
                    return v78.a(context4, "android.permission.READ_CONTACTS");
                }
                break;
            case 5:
                Context context5 = (Context) weakReference.get();
                if (context5 != null) {
                    return v78.a(context5, "android.permission.READ_PHONE_STATE");
                }
                break;
            case 6:
                Context context6 = (Context) weakReference.get();
                if (context6 != null) {
                    return v78.a(context6, "android.permission.READ_CALL_LOG");
                }
                break;
            case 7:
                Context context7 = (Context) weakReference.get();
                if (context7 != null) {
                    return v78.a(context7, "android.permission.CALL_PHONE");
                }
                break;
            case 8:
                Context context8 = (Context) weakReference.get();
                if (context8 != null) {
                    return v78.a(context8, "android.permission.ANSWER_PHONE_CALLS");
                }
                break;
            case 9:
                Context context9 = (Context) weakReference.get();
                if (context9 != null) {
                    return v78.a(context9, "android.permission.READ_SMS");
                }
                break;
            case 10:
                Context context10 = (Context) weakReference.get();
                if (context10 != null) {
                    return v78.a(context10, "android.permission.RECEIVE_SMS");
                }
                break;
            case 11:
                Context context11 = (Context) weakReference.get();
                if (context11 != null) {
                    return v78.a(context11, "android.permission.RECEIVE_MMS");
                }
                break;
            case 12:
                Context context12 = (Context) weakReference.get();
                if (context12 != null) {
                    return v78.a(context12, "android.permission.CAMERA");
                }
                break;
            case 13:
                Context context13 = (Context) weakReference.get();
                if (context13 != null) {
                    return v78.a(context13, "android.permission.READ_EXTERNAL_STORAGE");
                }
                break;
            case 14:
                return PortfolioApp.h0.c().y0();
            case 15:
                if (nk5.o.s()) {
                    return LocationUtils.isBackgroundLocationPermissionGranted(context);
                }
                return true;
            case 16:
                Context context14 = (Context) weakReference.get();
                if (context14 != null) {
                    return v78.a(context14, "android.permission.SEND_SMS");
                }
                break;
        }
        return false;
    }

    @DexIgnore
    public final boolean n(c cVar) {
        pq7.c(cVar, "permission");
        int i = kn5.g[cVar.ordinal()];
        if (i == 1) {
            return PortfolioApp.h0.c().y0();
        }
        if (i == 2) {
            return BluetoothUtils.isBluetoothEnable();
        }
        if (i != 3) {
            return true;
        }
        return LocationUtils.isLocationEnable(PortfolioApp.h0.c());
    }
}
