package com.fossil;

import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class a57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f201a;

    /*
    static {
        int[] iArr = new int[MicroAppInstruction.MicroAppID.values().length];
        f201a = iArr;
        iArr[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
        f201a[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
        f201a[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
        f201a[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
        f201a[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
        f201a[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
        f201a[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
        f201a[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
        f201a[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
        f201a[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
        f201a[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
        f201a[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
        f201a[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
        f201a[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
        f201a[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
    }
    */
}
