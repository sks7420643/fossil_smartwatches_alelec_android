package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv4 implements Factory<uv4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f3840a;
    @DexIgnore
    public /* final */ Provider<zt4> b;

    @DexIgnore
    public vv4(Provider<tt4> provider, Provider<zt4> provider2) {
        this.f3840a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static vv4 a(Provider<tt4> provider, Provider<zt4> provider2) {
        return new vv4(provider, provider2);
    }

    @DexIgnore
    public static uv4 c(tt4 tt4, zt4 zt4) {
        return new uv4(tt4, zt4);
    }

    @DexIgnore
    /* renamed from: b */
    public uv4 get() {
        return c(this.f3840a.get(), this.b.get());
    }
}
