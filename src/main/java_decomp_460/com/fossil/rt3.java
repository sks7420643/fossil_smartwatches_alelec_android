package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt3 extends dt3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ lu3<Void> f3163a; // = new lu3<>();

    @DexIgnore
    @Override // com.fossil.dt3
    public final boolean a() {
        return this.f3163a.p();
    }

    @DexIgnore
    @Override // com.fossil.dt3
    public final dt3 b(kt3 kt3) {
        this.f3163a.f(new st3(this, kt3));
        return this;
    }

    @DexIgnore
    public final void c() {
        this.f3163a.y(null);
    }
}
