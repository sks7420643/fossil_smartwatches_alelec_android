package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v extends jw0<j0> {
    @DexIgnore
    public v(z zVar, qw0 qw0) {
        super(qw0);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.px0, java.lang.Object] */
    @Override // com.fossil.jw0
    public void bind(px0 px0, j0 j0Var) {
        j0 j0Var2 = j0Var;
        px0.bindLong(1, (long) j0Var2.b);
        String str = j0Var2.c;
        if (str == null) {
            px0.bindNull(2);
        } else {
            px0.bindString(2, str);
        }
        px0.bindLong(3, (long) j0Var2.d);
        px0.bindLong(4, (long) j0Var2.e);
        byte[] bArr = j0Var2.f;
        if (bArr == null) {
            px0.bindNull(5);
        } else {
            px0.bindBlob(5, bArr);
        }
        px0.bindLong(6, j0Var2.g);
        px0.bindLong(7, j0Var2.h);
        px0.bindLong(8, j0Var2.a());
        px0.bindLong(9, j0Var2.c() ? 1 : 0);
    }

    @DexIgnore
    @Override // com.fossil.xw0
    public String createQuery() {
        return "INSERT OR REPLACE INTO `DeviceFile` (`id`,`deviceMacAddress`,`fileType`,`fileIndex`,`rawData`,`fileLength`,`fileCrc`,`createdTimeStamp`,`isCompleted`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
    }
}
