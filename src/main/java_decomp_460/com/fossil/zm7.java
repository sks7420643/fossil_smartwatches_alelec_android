package com.fossil;

import com.facebook.share.internal.ShareConstants;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zm7 extends ym7 {
    @DexIgnore
    public static final <K, V> Map<K, V> g() {
        sm7 sm7 = sm7.INSTANCE;
        if (sm7 != null) {
            return sm7;
        }
        throw new il7("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    @DexIgnore
    public static final <K, V> V h(Map<K, ? extends V> map, K k) {
        pq7.c(map, "$this$getValue");
        return (V) xm7.a(map, k);
    }

    @DexIgnore
    public static final <K, V> HashMap<K, V> i(cl7<? extends K, ? extends V>... cl7Arr) {
        pq7.c(cl7Arr, "pairs");
        HashMap<K, V> hashMap = new HashMap<>(ym7.b(cl7Arr.length));
        m(hashMap, cl7Arr);
        return hashMap;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> j(cl7<? extends K, ? extends V>... cl7Arr) {
        pq7.c(cl7Arr, "pairs");
        if (cl7Arr.length <= 0) {
            return g();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(ym7.b(cl7Arr.length));
        p(cl7Arr, linkedHashMap);
        return linkedHashMap;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Map<K, ? extends V> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <K, V> Map<K, V> k(Map<K, ? extends V> map) {
        pq7.c(map, "$this$optimizeReadOnlyMap");
        int size = map.size();
        return size != 0 ? size != 1 ? map : ym7.e(map) : g();
    }

    @DexIgnore
    public static final <K, V> void l(Map<? super K, ? super V> map, Iterable<? extends cl7<? extends K, ? extends V>> iterable) {
        pq7.c(map, "$this$putAll");
        pq7.c(iterable, "pairs");
        Iterator<? extends cl7<? extends K, ? extends V>> it = iterable.iterator();
        while (it.hasNext()) {
            cl7 cl7 = (cl7) it.next();
            map.put((Object) cl7.component1(), (Object) cl7.component2());
        }
    }

    @DexIgnore
    public static final <K, V> void m(Map<? super K, ? super V> map, cl7<? extends K, ? extends V>[] cl7Arr) {
        pq7.c(map, "$this$putAll");
        pq7.c(cl7Arr, "pairs");
        for (cl7<? extends K, ? extends V> cl7 : cl7Arr) {
            map.put((Object) cl7.component1(), (Object) cl7.component2());
        }
    }

    @DexIgnore
    public static final <K, V> Map<K, V> n(Iterable<? extends cl7<? extends K, ? extends V>> iterable) {
        pq7.c(iterable, "$this$toMap");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return g();
            }
            if (size != 1) {
                LinkedHashMap linkedHashMap = new LinkedHashMap(ym7.b(collection.size()));
                o(iterable, linkedHashMap);
                return linkedHashMap;
            }
            return ym7.c((cl7) (iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next()));
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        o(iterable, linkedHashMap2);
        return k(linkedHashMap2);
    }

    @DexIgnore
    public static final <K, V, M extends Map<? super K, ? super V>> M o(Iterable<? extends cl7<? extends K, ? extends V>> iterable, M m) {
        pq7.c(iterable, "$this$toMap");
        pq7.c(m, ShareConstants.DESTINATION);
        l(m, iterable);
        return m;
    }

    @DexIgnore
    public static final <K, V, M extends Map<? super K, ? super V>> M p(cl7<? extends K, ? extends V>[] cl7Arr, M m) {
        pq7.c(cl7Arr, "$this$toMap");
        pq7.c(m, ShareConstants.DESTINATION);
        m(m, cl7Arr);
        return m;
    }
}
