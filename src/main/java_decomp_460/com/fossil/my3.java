package com.fossil;

import android.content.Context;
import android.util.DisplayMetrics;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class my3 extends LinearLayoutManager {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gv0 {
        @DexIgnore
        public a(my3 my3, Context context) {
            super(context);
        }

        @DexIgnore
        @Override // com.fossil.gv0
        public float v(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    public my3(Context context, int i, boolean z) {
        super(context, i, z);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.m, androidx.recyclerview.widget.LinearLayoutManager
    public void J1(RecyclerView recyclerView, RecyclerView.State state, int i) {
        a aVar = new a(this, recyclerView.getContext());
        aVar.p(i);
        K1(aVar);
    }
}
