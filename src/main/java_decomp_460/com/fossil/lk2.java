package com.fossil;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk2 extends Binder {
    @DexIgnore
    public /* final */ hk2 b;

    @DexIgnore
    public lk2(hk2 hk2) {
        this.b = hk2;
    }

    @DexIgnore
    public final void b(jk2 jk2) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.b.b.execute(new mk2(this, jk2));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
