package com.fossil;

import com.fossil.x34;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r14<K, V> extends u14<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 2447537837011683357L;
    @DexIgnore
    public transient Map<K, Collection<V>> g;
    @DexIgnore
    public transient int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends r14<K, V>.d {
        @DexIgnore
        public a(r14 r14) {
            super();
        }

        @DexIgnore
        @Override // com.fossil.r14.d
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends r14<K, V>.d {
        @DexIgnore
        public b(r14 r14) {
            super();
        }

        @DexIgnore
        /* renamed from: b */
        public Map.Entry<K, V> a(K k, V v) {
            return x34.e(k, v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends x34.g<K, Collection<V>> {
        @DexIgnore
        public /* final */ transient Map<K, Collection<V>> d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends x34.d<K, Collection<V>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.x34.d
            public Map<K, Collection<V>> a() {
                return c.this;
            }

            @DexIgnore
            @Override // com.fossil.x34.d
            public boolean contains(Object obj) {
                return b24.d(c.this.d.entrySet(), obj);
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new b();
            }

            @DexIgnore
            public boolean remove(Object obj) {
                if (!contains(obj)) {
                    return false;
                }
                r14.this.c(((Map.Entry) obj).getKey());
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Iterator<Map.Entry<K, Collection<V>>> {
            @DexIgnore
            public /* final */ Iterator<Map.Entry<K, Collection<V>>> b; // = c.this.d.entrySet().iterator();
            @DexIgnore
            public Collection<V> c;

            @DexIgnore
            public b() {
            }

            @DexIgnore
            /* renamed from: a */
            public Map.Entry<K, Collection<V>> next() {
                Map.Entry<K, Collection<V>> next = this.b.next();
                this.c = next.getValue();
                return c.this.f(next);
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            public void remove() {
                this.b.remove();
                r14.access$220(r14.this, this.c.size());
                this.c.clear();
            }
        }

        @DexIgnore
        public c(Map<K, Collection<V>> map) {
            this.d = map;
        }

        @DexIgnore
        @Override // com.fossil.x34.g
        public Set<Map.Entry<K, Collection<V>>> a() {
            return new a();
        }

        @DexIgnore
        /* renamed from: c */
        public Collection<V> get(Object obj) {
            Collection<V> collection = (Collection) x34.n(this.d, obj);
            if (collection == null) {
                return null;
            }
            return r14.this.wrapCollection(obj, collection);
        }

        @DexIgnore
        public void clear() {
            if (this.d == r14.this.g) {
                r14.this.clear();
            } else {
                p34.e(new b());
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj) {
            return x34.m(this.d, obj);
        }

        @DexIgnore
        /* renamed from: e */
        public Collection<V> remove(Object obj) {
            Collection<V> remove = this.d.remove(obj);
            if (remove == null) {
                return null;
            }
            Collection<V> createCollection = r14.this.createCollection();
            createCollection.addAll(remove);
            r14.access$220(r14.this, remove.size());
            remove.clear();
            return createCollection;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || this.d.equals(obj);
        }

        @DexIgnore
        public Map.Entry<K, Collection<V>> f(Map.Entry<K, Collection<V>> entry) {
            K key = entry.getKey();
            return x34.e(key, r14.this.wrapCollection(key, entry.getValue()));
        }

        @DexIgnore
        public int hashCode() {
            return this.d.hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Set<K> keySet() {
            return r14.this.keySet();
        }

        @DexIgnore
        public int size() {
            return this.d.size();
        }

        @DexIgnore
        public String toString() {
            return this.d.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class d<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> b;
        @DexIgnore
        public K c; // = null;
        @DexIgnore
        public Collection<V> d; // = null;
        @DexIgnore
        public Iterator<V> e; // = p34.j();

        @DexIgnore
        public d() {
            this.b = r14.this.g.entrySet().iterator();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext() || this.e.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.e.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.b.next();
                this.c = next.getKey();
                Collection<V> value = next.getValue();
                this.d = value;
                this.e = value.iterator();
            }
            return a(this.c, this.e.next());
        }

        @DexIgnore
        public void remove() {
            this.e.remove();
            if (this.d.isEmpty()) {
                this.b.remove();
            }
            r14.access$210(r14.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends x34.e<K, Collection<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<K> {
            @DexIgnore
            public Map.Entry<K, Collection<V>> b;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator c;

            @DexIgnore
            public a(Iterator it) {
                this.c = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.c.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public K next() {
                Map.Entry<K, Collection<V>> entry = (Map.Entry) this.c.next();
                this.b = entry;
                return entry.getKey();
            }

            @DexIgnore
            public void remove() {
                a24.c(this.b != null);
                Collection<V> value = this.b.getValue();
                this.c.remove();
                r14.access$220(r14.this, value.size());
                value.clear();
            }
        }

        @DexIgnore
        public e(Map<K, Collection<V>> map) {
            super(map);
        }

        @DexIgnore
        @Override // com.fossil.x34.e
        public void clear() {
            p34.e(iterator());
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return a().keySet().containsAll(collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || a().keySet().equals(obj);
        }

        @DexIgnore
        public int hashCode() {
            return a().keySet().hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.x34.e, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new a(a().entrySet().iterator());
        }

        @DexIgnore
        @Override // com.fossil.x34.e
        public boolean remove(Object obj) {
            int i;
            V remove = a().remove(obj);
            if (remove != null) {
                int size = remove.size();
                remove.clear();
                r14.access$220(r14.this, size);
                i = size;
            } else {
                i = 0;
            }
            return i > 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends r14<K, V>.j implements RandomAccess {
        @DexIgnore
        public f(r14 r14, K k, List<V> list, r14<K, V>.i iVar) {
            super(k, list, iVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends r14<K, V>.c implements SortedMap<K, Collection<V>> {
        @DexIgnore
        public SortedSet<K> f;

        @DexIgnore
        public g(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public Comparator<? super K> comparator() {
            return i().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public K firstKey() {
            return i().firstKey();
        }

        @DexIgnore
        public SortedSet<K> g() {
            return new h(i());
        }

        @DexIgnore
        /* renamed from: h */
        public SortedSet<K> keySet() {
            SortedSet<K> sortedSet = this.f;
            if (sortedSet != null) {
                return sortedSet;
            }
            SortedSet<K> g2 = g();
            this.f = g2;
            return g2;
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> headMap(K k) {
            return new g(i().headMap(k));
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> i() {
            return (SortedMap) this.d;
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public K lastKey() {
            return i().lastKey();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> subMap(K k, K k2) {
            return new g(i().subMap(k, k2));
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> tailMap(K k) {
            return new g(i().tailMap(k));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends r14<K, V>.e implements SortedSet<K> {
        @DexIgnore
        public h(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> b() {
            return (SortedMap) super.a();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public Comparator<? super K> comparator() {
            return b().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public K first() {
            return b().firstKey();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> headSet(K k) {
            return new h(b().headMap(k));
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public K last() {
            return b().lastKey();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> subSet(K k, K k2) {
            return new h(b().subMap(k, k2));
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> tailSet(K k) {
            return new h(b().tailMap(k));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends AbstractCollection<V> {
        @DexIgnore
        public /* final */ K b;
        @DexIgnore
        public Collection<V> c;
        @DexIgnore
        public /* final */ r14<K, V>.i d;
        @DexIgnore
        public /* final */ Collection<V> e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<V> {
            @DexIgnore
            public /* final */ Iterator<V> b;
            @DexIgnore
            public /* final */ Collection<V> c; // = i.this.c;

            @DexIgnore
            public a() {
                this.b = r14.this.b(i.this.c);
            }

            @DexIgnore
            public a(Iterator<V> it) {
                this.b = it;
            }

            @DexIgnore
            public Iterator<V> a() {
                b();
                return this.b;
            }

            @DexIgnore
            public void b() {
                i.this.e();
                if (i.this.c != this.c) {
                    throw new ConcurrentModificationException();
                }
            }

            @DexIgnore
            public boolean hasNext() {
                b();
                return this.b.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public V next() {
                b();
                return this.b.next();
            }

            @DexIgnore
            public void remove() {
                this.b.remove();
                r14.access$210(r14.this);
                i.this.f();
            }
        }

        @DexIgnore
        public i(K k, Collection<V> collection, r14<K, V>.i iVar) {
            this.b = k;
            this.c = collection;
            this.d = iVar;
            this.e = iVar == null ? null : iVar.c();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.util.Map */
        /* JADX WARN: Multi-variable type inference failed */
        public void a() {
            r14<K, V>.i iVar = this.d;
            if (iVar != null) {
                iVar.a();
            } else {
                r14.this.g.put(this.b, this.c);
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean add(V v) {
            e();
            boolean isEmpty = this.c.isEmpty();
            boolean add = this.c.add(v);
            if (add) {
                r14.access$208(r14.this);
                if (isEmpty) {
                    a();
                }
            }
            return add;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.c.addAll(collection);
            if (!addAll) {
                return addAll;
            }
            r14.access$212(r14.this, this.c.size() - size);
            if (size != 0) {
                return addAll;
            }
            a();
            return addAll;
        }

        @DexIgnore
        public r14<K, V>.i b() {
            return this.d;
        }

        @DexIgnore
        public Collection<V> c() {
            return this.c;
        }

        @DexIgnore
        public void clear() {
            int size = size();
            if (size != 0) {
                this.c.clear();
                r14.access$220(r14.this, size);
                f();
            }
        }

        @DexIgnore
        public boolean contains(Object obj) {
            e();
            return this.c.contains(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            e();
            return this.c.containsAll(collection);
        }

        @DexIgnore
        public K d() {
            return this.b;
        }

        @DexIgnore
        public void e() {
            Collection<V> collection;
            r14<K, V>.i iVar = this.d;
            if (iVar != null) {
                iVar.e();
                if (this.d.c() != this.e) {
                    throw new ConcurrentModificationException();
                }
            } else if (this.c.isEmpty() && (collection = (Collection) r14.this.g.get(this.b)) != null) {
                this.c = collection;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            e();
            return this.c.equals(obj);
        }

        @DexIgnore
        public void f() {
            r14<K, V>.i iVar = this.d;
            if (iVar != null) {
                iVar.f();
            } else if (this.c.isEmpty()) {
                r14.this.g.remove(this.b);
            }
        }

        @DexIgnore
        public int hashCode() {
            e();
            return this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            e();
            return new a();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            e();
            boolean remove = this.c.remove(obj);
            if (remove) {
                r14.access$210(r14.this);
                f();
            }
            return remove;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean removeAll = this.c.removeAll(collection);
            if (!removeAll) {
                return removeAll;
            }
            r14.access$212(r14.this, this.c.size() - size);
            f();
            return removeAll;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            i14.l(collection);
            int size = size();
            boolean retainAll = this.c.retainAll(collection);
            if (retainAll) {
                r14.access$212(r14.this, this.c.size() - size);
                f();
            }
            return retainAll;
        }

        @DexIgnore
        public int size() {
            e();
            return this.c.size();
        }

        @DexIgnore
        public String toString() {
            e();
            return this.c.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends r14<K, V>.i implements List<V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends r14<K, V>.i.a implements ListIterator<V> {
            @DexIgnore
            public a() {
                super();
            }

            @DexIgnore
            public a(int i) {
                super(j.this.g().listIterator(i));
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public void add(V v) {
                boolean isEmpty = j.this.isEmpty();
                c().add(v);
                r14.access$208(r14.this);
                if (isEmpty) {
                    j.this.a();
                }
            }

            @DexIgnore
            public final ListIterator<V> c() {
                return (ListIterator) a();
            }

            @DexIgnore
            public boolean hasPrevious() {
                return c().hasPrevious();
            }

            @DexIgnore
            public int nextIndex() {
                return c().nextIndex();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public V previous() {
                return c().previous();
            }

            @DexIgnore
            public int previousIndex() {
                return c().previousIndex();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public void set(V v) {
                c().set(v);
            }
        }

        @DexIgnore
        public j(K k, List<V> list, r14<K, V>.i iVar) {
            super(k, list, iVar);
        }

        @DexIgnore
        @Override // java.util.List
        public void add(int i, V v) {
            e();
            boolean isEmpty = c().isEmpty();
            g().add(i, v);
            r14.access$208(r14.this);
            if (isEmpty) {
                a();
            }
        }

        @DexIgnore
        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = g().addAll(i, collection);
            if (!addAll) {
                return addAll;
            }
            r14.access$212(r14.this, c().size() - size);
            if (size != 0) {
                return addAll;
            }
            a();
            return addAll;
        }

        @DexIgnore
        public List<V> g() {
            return (List) c();
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            e();
            return g().get(i);
        }

        @DexIgnore
        public int indexOf(Object obj) {
            e();
            return g().indexOf(obj);
        }

        @DexIgnore
        public int lastIndexOf(Object obj) {
            e();
            return g().lastIndexOf(obj);
        }

        @DexIgnore
        @Override // java.util.List
        public ListIterator<V> listIterator() {
            e();
            return new a();
        }

        @DexIgnore
        @Override // java.util.List
        public ListIterator<V> listIterator(int i) {
            e();
            return new a(i);
        }

        @DexIgnore
        @Override // java.util.List
        public V remove(int i) {
            e();
            V remove = g().remove(i);
            r14.access$210(r14.this);
            f();
            return remove;
        }

        @DexIgnore
        @Override // java.util.List
        public V set(int i, V v) {
            e();
            return g().set(i, v);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v2, types: [com.fossil.r14$i] */
        /* JADX WARN: Type inference failed for: r4v3 */
        @Override // java.util.List
        public List<V> subList(int i, int i2) {
            e();
            r14 r14 = r14.this;
            Object d = d();
            List<V> subList = g().subList(i, i2);
            r14<K, V>.i b = b();
            Object r4 = this;
            if (b != null) {
                r4 = b();
            }
            return r14.d(d, subList, r4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends r14<K, V>.i implements Set<V> {
        @DexIgnore
        public k(K k, Set<V> set) {
            super(k, set, null);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.r14.i
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean f = x44.f((Set) this.c, collection);
            if (!f) {
                return f;
            }
            r14.access$212(r14.this, this.c.size() - size);
            f();
            return f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends r14<K, V>.i implements SortedSet<V> {
        @DexIgnore
        public l(K k, SortedSet<V> sortedSet, r14<K, V>.i iVar) {
            super(k, sortedSet, iVar);
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public Comparator<? super V> comparator() {
            return g().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public V first() {
            e();
            return g().first();
        }

        @DexIgnore
        public SortedSet<V> g() {
            return (SortedSet) c();
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v2, types: [com.fossil.r14$i] */
        /* JADX WARN: Type inference failed for: r4v3 */
        @Override // java.util.SortedSet
        public SortedSet<V> headSet(V v) {
            e();
            r14 r14 = r14.this;
            Object d = d();
            SortedSet<V> headSet = g().headSet(v);
            r14<K, V>.i b = b();
            Object r4 = this;
            if (b != null) {
                r4 = b();
            }
            return new l(d, headSet, r4);
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public V last() {
            e();
            return g().last();
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v2, types: [com.fossil.r14$i] */
        /* JADX WARN: Type inference failed for: r4v3 */
        @Override // java.util.SortedSet
        public SortedSet<V> subSet(V v, V v2) {
            e();
            r14 r14 = r14.this;
            Object d = d();
            SortedSet<V> subSet = g().subSet(v, v2);
            r14<K, V>.i b = b();
            Object r4 = this;
            if (b != null) {
                r4 = b();
            }
            return new l(d, subSet, r4);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v2, types: [com.fossil.r14$i] */
        /* JADX WARN: Type inference failed for: r4v3 */
        @Override // java.util.SortedSet
        public SortedSet<V> tailSet(V v) {
            e();
            r14 r14 = r14.this;
            Object d = d();
            SortedSet<V> tailSet = g().tailSet(v);
            r14<K, V>.i b = b();
            Object r4 = this;
            if (b != null) {
                r4 = b();
            }
            return new l(d, tailSet, r4);
        }
    }

    @DexIgnore
    public r14(Map<K, Collection<V>> map) {
        i14.d(map.isEmpty());
        this.g = map;
    }

    @DexIgnore
    public static /* synthetic */ int access$208(r14 r14) {
        int i2 = r14.h;
        r14.h = i2 + 1;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$210(r14 r14) {
        int i2 = r14.h;
        r14.h = i2 - 1;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$212(r14 r14, int i2) {
        int i3 = r14.h + i2;
        r14.h = i3;
        return i3;
    }

    @DexIgnore
    public static /* synthetic */ int access$220(r14 r14, int i2) {
        int i3 = r14.h - i2;
        r14.h = i3;
        return i3;
    }

    @DexIgnore
    public final Collection<V> a(K k2) {
        Collection<V> collection = this.g.get(k2);
        if (collection != null) {
            return collection;
        }
        Collection<V> createCollection = createCollection(k2);
        this.g.put(k2, createCollection);
        return createCollection;
    }

    @DexIgnore
    public final Iterator<V> b(Collection<V> collection) {
        return collection instanceof List ? ((List) collection).listIterator() : collection.iterator();
    }

    @DexIgnore
    public Map<K, Collection<V>> backingMap() {
        return this.g;
    }

    @DexIgnore
    public final void c(Object obj) {
        Collection collection = (Collection) x34.o(this.g, obj);
        if (collection != null) {
            int size = collection.size();
            collection.clear();
            this.h -= size;
        }
    }

    @DexIgnore
    @Override // com.fossil.y34
    public void clear() {
        for (Collection<V> collection : this.g.values()) {
            collection.clear();
        }
        this.g.clear();
        this.h = 0;
    }

    @DexIgnore
    @Override // com.fossil.y34
    public boolean containsKey(Object obj) {
        return this.g.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Map<K, Collection<V>> createAsMap() {
        return this.g instanceof SortedMap ? new g((SortedMap) this.g) : new c(this.g);
    }

    @DexIgnore
    public abstract Collection<V> createCollection();

    @DexIgnore
    public Collection<V> createCollection(K k2) {
        return createCollection();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Set<K> createKeySet() {
        return this.g instanceof SortedMap ? new h((SortedMap) this.g) : new e(this.g);
    }

    @DexIgnore
    public Collection<V> createUnmodifiableEmptyCollection() {
        return unmodifiableCollectionSubclass(createCollection());
    }

    @DexIgnore
    public final List<V> d(K k2, List<V> list, r14<K, V>.i iVar) {
        return list instanceof RandomAccess ? new f(this, k2, list, iVar) : new j(k2, list, iVar);
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Iterator<Map.Entry<K, V>> entryIterator() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.y34
    public Collection<V> get(K k2) {
        Collection<V> collection = this.g.get(k2);
        if (collection == null) {
            collection = createCollection(k2);
        }
        return wrapCollection(k2, collection);
    }

    @DexIgnore
    @Override // com.fossil.u14, com.fossil.y34
    public boolean put(K k2, V v) {
        Collection<V> collection = this.g.get(k2);
        if (collection == null) {
            Collection<V> createCollection = createCollection(k2);
            if (createCollection.add(v)) {
                this.h++;
                this.g.put(k2, createCollection);
                return true;
            }
            throw new AssertionError("New Collection violated the Collection spec");
        } else if (!collection.add(v)) {
            return false;
        } else {
            this.h++;
            return true;
        }
    }

    @DexIgnore
    public Collection<V> removeAll(Object obj) {
        Collection<V> remove = this.g.remove(obj);
        if (remove == null) {
            return createUnmodifiableEmptyCollection();
        }
        Collection<V> createCollection = createCollection();
        createCollection.addAll(remove);
        this.h -= remove.size();
        remove.clear();
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Collection<V> replaceValues(K k2, Iterable<? extends V> iterable) {
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext()) {
            return removeAll(k2);
        }
        Collection<? extends V> a2 = a(k2);
        Collection<V> createCollection = createCollection();
        createCollection.addAll(a2);
        this.h -= a2.size();
        a2.clear();
        while (it.hasNext()) {
            if (a2.add((Object) it.next())) {
                this.h++;
            }
        }
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    public final void setMap(Map<K, Collection<V>> map) {
        this.g = map;
        this.h = 0;
        for (Collection<V> collection : map.values()) {
            i14.d(!collection.isEmpty());
            this.h = collection.size() + this.h;
        }
    }

    @DexIgnore
    @Override // com.fossil.y34
    public int size() {
        return this.h;
    }

    @DexIgnore
    public Collection<V> unmodifiableCollectionSubclass(Collection<V> collection) {
        return collection instanceof SortedSet ? Collections.unmodifiableSortedSet((SortedSet) collection) : collection instanceof Set ? Collections.unmodifiableSet((Set) collection) : collection instanceof List ? Collections.unmodifiableList((List) collection) : Collections.unmodifiableCollection(collection);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Iterator<V> valueIterator() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.u14
    public Collection<V> values() {
        return super.values();
    }

    @DexIgnore
    public Collection<V> wrapCollection(K k2, Collection<V> collection) {
        return collection instanceof SortedSet ? new l(k2, (SortedSet) collection, null) : collection instanceof Set ? new k(k2, (Set) collection) : collection instanceof List ? d(k2, (List) collection, null) : new i(k2, collection, null);
    }
}
