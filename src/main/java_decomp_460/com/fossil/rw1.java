package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum rw1 {
    STEP("stepsBar"),
    CALORIES("caloriesBar"),
    ACTIVE_MINUTE("activeMinutesBar");
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final rw1 a(String str) {
            rw1[] values = rw1.values();
            for (rw1 rw1 : values) {
                if (pq7.a(rw1.a(), str)) {
                    return rw1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public rw1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
