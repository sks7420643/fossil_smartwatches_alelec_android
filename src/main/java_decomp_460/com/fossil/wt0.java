package com.fossil;

import com.fossil.bu0;
import com.fossil.cu0;
import com.fossil.eu0;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wt0<K, V> extends cu0<V> implements eu0.a {
    @DexIgnore
    public /* final */ boolean A;
    @DexIgnore
    public bu0.a<V> B; // = new a();
    @DexIgnore
    public /* final */ vt0<K, V> u;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public int x; // = 0;
    @DexIgnore
    public int y; // = 0;
    @DexIgnore
    public boolean z; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends bu0.a<V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.bu0.a
        public void a(int i, bu0<V> bu0) {
            boolean z = true;
            if (bu0.c()) {
                wt0.this.m();
            } else if (!wt0.this.t()) {
                List<T> list = bu0.f508a;
                if (i == 0) {
                    wt0 wt0 = wt0.this;
                    wt0.f.r(bu0.b, list, bu0.c, bu0.d, wt0);
                    wt0 wt02 = wt0.this;
                    if (wt02.g == -1) {
                        wt02.g = bu0.b + bu0.d + (list.size() / 2);
                    }
                } else {
                    wt0 wt03 = wt0.this;
                    boolean z2 = wt03.g > wt03.f.i();
                    wt0 wt04 = wt0.this;
                    boolean z3 = wt04.A && wt04.f.A(wt04.e.d, wt04.i, list.size());
                    if (i == 1) {
                        if (!z3 || z2) {
                            wt0 wt05 = wt0.this;
                            wt05.f.c(list, wt05);
                        } else {
                            wt0 wt06 = wt0.this;
                            wt06.y = 0;
                            wt06.w = 0;
                        }
                    } else if (i != 2) {
                        throw new IllegalArgumentException("unexpected resultType " + i);
                    } else if (!z3 || !z2) {
                        wt0 wt07 = wt0.this;
                        wt07.f.z(list, wt07);
                    } else {
                        wt0 wt08 = wt0.this;
                        wt08.x = 0;
                        wt08.v = 0;
                    }
                    wt0 wt09 = wt0.this;
                    if (wt09.A) {
                        if (z2) {
                            if (wt09.v != 1 && wt09.f.E(wt09.z, wt09.e.d, wt09.i, wt09)) {
                                wt0.this.v = 0;
                            }
                        } else if (wt09.w != 1 && wt09.f.D(wt09.z, wt09.e.d, wt09.i, wt09)) {
                            wt0.this.w = 0;
                        }
                    }
                }
                wt0 wt010 = wt0.this;
                if (wt010.d != null) {
                    boolean z4 = wt010.f.size() == 0;
                    boolean z5 = !z4 && i == 2 && bu0.f508a.size() == 0;
                    if (!(!z4 && i == 1 && bu0.f508a.size() == 0)) {
                        z = false;
                    }
                    wt0.this.l(z4, z5, z);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public b(int i, Object obj) {
            this.b = i;
            this.c = obj;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.fossil.vt0<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            if (!wt0.this.t()) {
                if (wt0.this.u.isInvalid()) {
                    wt0.this.m();
                    return;
                }
                wt0 wt0 = wt0.this;
                wt0.u.dispatchLoadBefore(this.b, this.c, wt0.e.f656a, wt0.b, wt0.B);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;

        @DexIgnore
        public c(int i, Object obj) {
            this.b = i;
            this.c = obj;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.fossil.vt0<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            if (!wt0.this.t()) {
                if (wt0.this.u.isInvalid()) {
                    wt0.this.m();
                    return;
                }
                wt0 wt0 = wt0.this;
                wt0.u.dispatchLoadAfter(this.b, this.c, wt0.e.f656a, wt0.b, wt0.B);
            }
        }
    }

    @DexIgnore
    public wt0(vt0<K, V> vt0, Executor executor, Executor executor2, cu0.c<V> cVar, cu0.f fVar, K k, int i) {
        super(new eu0(), executor, executor2, cVar, fVar);
        this.u = vt0;
        this.g = i;
        if (vt0.isInvalid()) {
            m();
        } else {
            vt0<K, V> vt02 = this.u;
            cu0.f fVar2 = this.e;
            vt02.dispatchLoadInitial(k, fVar2.e, fVar2.f656a, fVar2.c, this.b, this.B);
        }
        this.A = this.u.supportsPageDropping() && this.e.d != Integer.MAX_VALUE;
    }

    @DexIgnore
    public static int F(int i, int i2, int i3) {
        return ((i2 + i) + 1) - i3;
    }

    @DexIgnore
    public static int G(int i, int i2, int i3) {
        return i - (i2 - i3);
    }

    @DexIgnore
    public final void H() {
        if (this.w == 0) {
            this.w = 1;
            int h = this.f.h();
            int n = this.f.n();
            int m = this.f.m();
            this.c.execute(new c(((h + n) - 1) + m, this.f.g()));
        }
    }

    @DexIgnore
    public final void I() {
        if (this.v == 0) {
            this.v = 1;
            int h = this.f.h();
            int m = this.f.m();
            this.c.execute(new b(h + m, this.f.f()));
        }
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void a() {
        this.w = 2;
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void b(int i, int i2, int i3) {
        int i4 = (this.x - i2) - i3;
        this.x = i4;
        this.v = 0;
        if (i4 > 0) {
            I();
        }
        x(i, i2);
        y(0, i3);
        A(i3);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void c(int i) {
        boolean z2 = false;
        y(0, i);
        if (this.f.h() > 0 || this.f.o() > 0) {
            z2 = true;
        }
        this.z = z2;
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void d(int i) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void e(int i, int i2) {
        x(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void f(int i, int i2) {
        z(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void g() {
        this.v = 2;
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void h(int i, int i2) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    @Override // com.fossil.eu0.a
    public void i(int i, int i2, int i3) {
        int i4 = (this.y - i2) - i3;
        this.y = i4;
        this.w = 0;
        if (i4 > 0) {
            H();
        }
        x(i, i2);
        y(i + i2, i3);
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public void o(cu0<V> cu0, cu0.e eVar) {
        eu0<T> eu0 = cu0.f;
        int j = this.f.j() - eu0.j();
        int k = this.f.k() - eu0.k();
        int o = eu0.o();
        int h = eu0.h();
        if (eu0.isEmpty() || j < 0 || k < 0 || this.f.o() != Math.max(o - j, 0) || this.f.h() != Math.max(h - k, 0) || this.f.n() != eu0.n() + j + k) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        if (j != 0) {
            int min = Math.min(o, j);
            int i = j - min;
            int n = eu0.n() + eu0.h();
            if (min != 0) {
                eVar.a(n, min);
            }
            if (i != 0) {
                eVar.b(n + min, i);
            }
        }
        if (k != 0) {
            int min2 = Math.min(h, k);
            int i2 = k - min2;
            if (min2 != 0) {
                eVar.a(h, min2);
            }
            if (i2 != 0) {
                eVar.b(0, i2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public xt0<?, V> p() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public Object q() {
        return this.u.getKey(this.g, this.h);
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public boolean s() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.cu0
    public void w(int i) {
        int G = G(this.e.b, i, this.f.h());
        int F = F(this.e.b, i, this.f.h() + this.f.n());
        int max = Math.max(G, this.x);
        this.x = max;
        if (max > 0) {
            I();
        }
        int max2 = Math.max(F, this.y);
        this.y = max2;
        if (max2 > 0) {
            H();
        }
    }
}
