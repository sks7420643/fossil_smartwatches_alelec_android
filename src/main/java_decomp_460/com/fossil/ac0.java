package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ac0 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ zb0 CREATOR; // = new zb0(null);
    @DexIgnore
    public /* final */ jw1 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public byte[] f;

    @DexIgnore
    public ac0(byte[] bArr) throws IllegalArgumentException {
        this.f = bArr;
        try {
            jw1 a2 = jw1.d.a(bArr[0]);
            if (a2 != null) {
                this.b = a2;
                int p = hy1.p(this.f[1]) + 2;
                this.c = iy1.c(new String(dm7.k(this.f, 2, p), et7.f986a));
                this.d = (byte) this.f[p];
                int i = p + 1;
                int i2 = i + 4;
                this.e = hy1.o(ByteBuffer.wrap(dm7.k(this.f, i, i2)).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                byte b2 = this.f[i2];
                int i3 = i2 + 1;
                new ry1(b2, this.f[i3]);
                int i4 = i3 + 1;
                new ry1(this.f[i4], this.f[i4 + 1]);
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final long b() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.e, ey1.a(this.b)), jd0.Y4, this.c), jd0.Z4, Byte.valueOf(this.d)), jd0.a5, hy1.k((int) this.e, null, 1, null));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
    }
}
