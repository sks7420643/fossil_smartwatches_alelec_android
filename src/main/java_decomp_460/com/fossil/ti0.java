package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ti0 implements wi0 {
    @DexIgnore
    @Override // com.fossil.wi0
    public void a(vi0 vi0, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        vi0.c(new xi0(colorStateList, f));
        View g = vi0.g();
        g.setClipToOutline(true);
        g.setElevation(f2);
        o(vi0, f3);
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void b(vi0 vi0, float f) {
        p(vi0).h(f);
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public float c(vi0 vi0) {
        return vi0.g().getElevation();
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public float d(vi0 vi0) {
        return p(vi0).d();
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void e(vi0 vi0) {
        o(vi0, g(vi0));
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void f(vi0 vi0, float f) {
        vi0.g().setElevation(f);
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public float g(vi0 vi0) {
        return p(vi0).c();
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public ColorStateList h(vi0 vi0) {
        return p(vi0).b();
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void i(vi0 vi0) {
        if (!vi0.e()) {
            vi0.a(0, 0, 0, 0);
            return;
        }
        float g = g(vi0);
        float d = d(vi0);
        int ceil = (int) Math.ceil((double) yi0.c(g, d, vi0.d()));
        int ceil2 = (int) Math.ceil((double) yi0.d(g, d, vi0.d()));
        vi0.a(ceil, ceil2, ceil, ceil2);
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void j() {
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public float k(vi0 vi0) {
        return d(vi0) * 2.0f;
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public float l(vi0 vi0) {
        return d(vi0) * 2.0f;
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void m(vi0 vi0) {
        o(vi0, g(vi0));
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void n(vi0 vi0, ColorStateList colorStateList) {
        p(vi0).f(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.wi0
    public void o(vi0 vi0, float f) {
        p(vi0).g(f, vi0.e(), vi0.d());
        i(vi0);
    }

    @DexIgnore
    public final xi0 p(vi0 vi0) {
        return (xi0) vi0.f();
    }
}
