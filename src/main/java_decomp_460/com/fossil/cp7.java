package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.io.File;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cp7 extends bp7 {
    @DexIgnore
    public static final boolean e(File file) {
        pq7.c(file, "$this$deleteRecursively");
        Iterator it = bp7.d(file).iterator();
        while (true) {
            boolean z = true;
            while (true) {
                if (!it.hasNext()) {
                    return z;
                }
                File file2 = (File) it.next();
                if (file2.delete() || !file2.exists()) {
                    if (!z) {
                    }
                }
                z = false;
            }
        }
    }

    @DexIgnore
    public static final String f(File file) {
        pq7.c(file, "$this$extension");
        String name = file.getName();
        pq7.b(name, "name");
        return wt7.k0(name, '.', "");
    }

    @DexIgnore
    public static final String g(File file) {
        pq7.c(file, "$this$nameWithoutExtension");
        String name = file.getName();
        pq7.b(name, "name");
        return wt7.t0(name, CodelessMatcher.CURRENT_CLASS_NAME, null, 2, null);
    }
}
