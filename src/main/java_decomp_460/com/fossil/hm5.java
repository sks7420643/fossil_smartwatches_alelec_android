package com.fossil;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hm5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Vector<lm5> f1495a;
    @DexIgnore
    public Map<String, lm5> b;
    @DexIgnore
    public Map<String, lm5> c;
    @DexIgnore
    public Map<String, lm5> d;
    @DexIgnore
    public Map<String, lm5> e;

    @DexIgnore
    public hm5() {
        this.f1495a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f1495a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    @DexIgnore
    public void a(int i, lm5 lm5) {
        if (lm5 != null) {
            d(lm5);
            this.f1495a.add(i, lm5);
            return;
        }
        throw null;
    }

    @DexIgnore
    public boolean b(lm5 lm5) {
        if (lm5 != null) {
            d(lm5);
            return this.f1495a.add(lm5);
        }
        throw null;
    }

    @DexIgnore
    public lm5 c(int i) {
        return this.f1495a.get(i);
    }

    @DexIgnore
    public final void d(lm5 lm5) {
        byte[] a2 = lm5.a();
        if (a2 != null) {
            this.b.put(new String(a2), lm5);
        }
        byte[] b2 = lm5.b();
        if (b2 != null) {
            this.c.put(new String(b2), lm5);
        }
        byte[] f = lm5.f();
        if (f != null) {
            this.d.put(new String(f), lm5);
        }
        byte[] e2 = lm5.e();
        if (e2 != null) {
            this.e.put(new String(e2), lm5);
        }
    }

    @DexIgnore
    public void e() {
        this.f1495a.clear();
    }
}
