package com.fossil;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap4 implements Factory<ApplicationEventListener> {
    @DexIgnore
    public /* final */ Provider<uo5> A;
    @DexIgnore
    public /* final */ Provider<zo5> B;
    @DexIgnore
    public /* final */ Provider<k97> C;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> D;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f301a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;
    @DexIgnore
    public /* final */ Provider<on5> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> f;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> g;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> h;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> i;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> j;
    @DexIgnore
    public /* final */ Provider<UserRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<mj5> m;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> o;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> p;
    @DexIgnore
    public /* final */ Provider<hu4> q;
    @DexIgnore
    public /* final */ Provider<zt4> r;
    @DexIgnore
    public /* final */ Provider<tt4> s;
    @DexIgnore
    public /* final */ Provider<FileRepository> t;
    @DexIgnore
    public /* final */ Provider<vt4> u;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> v;
    @DexIgnore
    public /* final */ Provider<pr4> w;
    @DexIgnore
    public /* final */ Provider<dr5> x;
    @DexIgnore
    public /* final */ Provider<ThemeRepository> y;
    @DexIgnore
    public /* final */ Provider<s77> z;

    @DexIgnore
    public ap4(uo4 uo4, Provider<PortfolioApp> provider, Provider<on5> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<mj5> provider12, Provider<DianaWatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<RingStyleRepository> provider15, Provider<hu4> provider16, Provider<zt4> provider17, Provider<tt4> provider18, Provider<FileRepository> provider19, Provider<vt4> provider20, Provider<WorkoutSettingRepository> provider21, Provider<pr4> provider22, Provider<dr5> provider23, Provider<ThemeRepository> provider24, Provider<s77> provider25, Provider<uo5> provider26, Provider<zo5> provider27, Provider<k97> provider28, Provider<DianaAppSettingRepository> provider29) {
        this.f301a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
        this.p = provider15;
        this.q = provider16;
        this.r = provider17;
        this.s = provider18;
        this.t = provider19;
        this.u = provider20;
        this.v = provider21;
        this.w = provider22;
        this.x = provider23;
        this.y = provider24;
        this.z = provider25;
        this.A = provider26;
        this.B = provider27;
        this.C = provider28;
        this.D = provider29;
    }

    @DexIgnore
    public static ap4 a(uo4 uo4, Provider<PortfolioApp> provider, Provider<on5> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<mj5> provider12, Provider<DianaWatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14, Provider<RingStyleRepository> provider15, Provider<hu4> provider16, Provider<zt4> provider17, Provider<tt4> provider18, Provider<FileRepository> provider19, Provider<vt4> provider20, Provider<WorkoutSettingRepository> provider21, Provider<pr4> provider22, Provider<dr5> provider23, Provider<ThemeRepository> provider24, Provider<s77> provider25, Provider<uo5> provider26, Provider<zo5> provider27, Provider<k97> provider28, Provider<DianaAppSettingRepository> provider29) {
        return new ap4(uo4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20, provider21, provider22, provider23, provider24, provider25, provider26, provider27, provider28, provider29);
    }

    @DexIgnore
    public static ApplicationEventListener c(uo4 uo4, PortfolioApp portfolioApp, on5 on5, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, mj5 mj5, DianaWatchFaceRepository dianaWatchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, RingStyleRepository ringStyleRepository, hu4 hu4, zt4 zt4, tt4 tt4, FileRepository fileRepository, vt4 vt4, WorkoutSettingRepository workoutSettingRepository, pr4 pr4, dr5 dr5, ThemeRepository themeRepository, s77 s77, uo5 uo5, zo5 zo5, k97 k97, DianaAppSettingRepository dianaAppSettingRepository) {
        ApplicationEventListener i2 = uo4.i(portfolioApp, on5, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, deviceRepository, userRepository, alarmsRepository, mj5, dianaWatchFaceRepository, watchLocalizationRepository, ringStyleRepository, hu4, zt4, tt4, fileRepository, vt4, workoutSettingRepository, pr4, dr5, themeRepository, s77, uo5, zo5, k97, dianaAppSettingRepository);
        lk7.c(i2, "Cannot return null from a non-@Nullable @Provides method");
        return i2;
    }

    @DexIgnore
    /* renamed from: b */
    public ApplicationEventListener get() {
        return c(this.f301a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get(), this.s.get(), this.t.get(), this.u.get(), this.v.get(), this.w.get(), this.x.get(), this.y.get(), this.z.get(), this.A.get(), this.B.get(), this.C.get(), this.D.get());
    }
}
