package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx6 extends gx6 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ int l; // = (Calendar.getInstance().get(1) - 110);
    @DexIgnore
    public Calendar e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = this.e.get(2);
    @DexIgnore
    public int h; // = this.e.get(1);
    @DexIgnore
    public int i; // = this.e.getActualMaximum(5);
    @DexIgnore
    public /* final */ hx6 j;

    /*
    static {
        String simpleName = kx6.class.getSimpleName();
        pq7.b(simpleName, "BirthdayPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public kx6(hx6 hx6) {
        pq7.c(hx6, "mView");
        this.j = hx6;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        this.e = instance;
        this.f = instance.get(5);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        r();
        this.j.A4(new cl7<>(1, Integer.valueOf(this.i)), new cl7<>(1, 12), new cl7<>(Integer.valueOf(l), Integer.valueOf(this.e.get(1))));
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        r();
    }

    @DexIgnore
    @Override // com.fossil.gx6
    public void n() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "confirmBirthday " + this.f + '/' + this.g + '/' + this.h);
        Calendar instance = Calendar.getInstance();
        instance.set(this.h, this.g + -1, this.f);
        hx6 hx6 = this.j;
        pq7.b(instance, "calendar");
        Date time = instance.getTime();
        pq7.b(time, "calendar.time");
        hx6.o6(time);
    }

    @DexIgnore
    @Override // com.fossil.gx6
    public void o(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current day: " + this.f + ", new day: " + i2);
        if (i2 >= 1 && i2 <= this.i) {
            this.f = i2;
        }
    }

    @DexIgnore
    @Override // com.fossil.gx6
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current month: " + this.g + ", new month: " + i2);
        if (i2 >= 1 && i2 <= 12) {
            this.g = i2;
            t();
        }
    }

    @DexIgnore
    @Override // com.fossil.gx6
    public void q(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current year: " + this.h + ", new year: " + i2);
        if (i2 >= l) {
            this.h = i2;
        }
    }

    @DexIgnore
    public final void r() {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        this.e = instance;
        this.f = instance.get(5);
        this.g = this.e.get(2);
        this.h = this.e.get(1);
        this.i = this.e.getActualMaximum(5);
    }

    @DexIgnore
    public void s() {
        this.j.M5(this);
    }

    @DexIgnore
    public final void t() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.h, this.g - 1, 1);
        this.i = instance.getActualMaximum(5);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Day range of " + this.g + '/' + this.h + ": " + this.i);
        this.j.M2(1, this.i);
    }
}
