package com.fossil;

import com.fossil.e13;
import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju2 extends e13<ju2, a> implements o23 {
    @DexIgnore
    public static /* final */ ju2 zzi;
    @DexIgnore
    public static volatile z23<ju2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public String zzg; // = "";
    @DexIgnore
    public String zzh; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<ju2, a> implements o23 {
        @DexIgnore
        public a() {
            super(ju2.zzi);
        }

        @DexIgnore
        public /* synthetic */ a(fu2 fu2) {
            this();
        }
    }

    @DexIgnore
    public enum b implements g13 {
        UNKNOWN_COMPARISON_TYPE(0),
        LESS_THAN(1),
        GREATER_THAN(2),
        EQUAL(3),
        BETWEEN(4);
        
        @DexIgnore
        public /* final */ int zzg;

        @DexIgnore
        public b(int i) {
            this.zzg = i;
        }

        @DexIgnore
        public static b zza(int i) {
            if (i == 0) {
                return UNKNOWN_COMPARISON_TYPE;
            }
            if (i == 1) {
                return LESS_THAN;
            }
            if (i == 2) {
                return GREATER_THAN;
            }
            if (i == 3) {
                return EQUAL;
            }
            if (i != 4) {
                return null;
            }
            return BETWEEN;
        }

        @DexIgnore
        public static i13 zzb() {
            return mu2.f2418a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.g13
        public final int zza() {
            return this.zzg;
        }
    }

    /*
    static {
        ju2 ju2 = new ju2();
        zzi = ju2;
        e13.u(ju2.class, ju2);
    }
    */

    @DexIgnore
    public static ju2 N() {
        return zzi;
    }

    @DexIgnore
    public final boolean C() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final b D() {
        b zza = b.zza(this.zzd);
        return zza == null ? b.UNKNOWN_COMPARISON_TYPE : zza;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final boolean G() {
        return this.zze;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String I() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String K() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final String M() {
        return this.zzh;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (fu2.f1206a[i - 1]) {
            case 1:
                return new ju2();
            case 2:
                return new a(null);
            case 3:
                i13 zzb = b.zzb();
                return e13.s(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u100c\u0000\u0002\u1007\u0001\u0003\u1008\u0002\u0004\u1008\u0003\u0005\u1008\u0004", new Object[]{"zzc", "zzd", zzb, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                z23<ju2> z232 = zzj;
                if (z232 != null) {
                    return z232;
                }
                synchronized (ju2.class) {
                    try {
                        z23 = zzj;
                        if (z23 == null) {
                            z23 = new e13.c(zzi);
                            zzj = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
