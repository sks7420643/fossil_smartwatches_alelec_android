package com.fossil;

import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wr4 f3989a; // = new wr4();

    @DexIgnore
    public final vr4 a() {
        String Q = PortfolioApp.h0.c().Q();
        return (!pq7.a(Q, ph5.CITIZEN.getName()) && !pq7.a(Q, ph5.UNIVERSAL.getName())) ? new ur4() : new tr4();
    }
}
