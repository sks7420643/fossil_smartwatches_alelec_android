package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.database.SdkDatabaseWrapper", f = "SdkDatabaseWrapper.kt", l = {264}, m = "getThemeTemplate")
public final class t extends co7 {
    @DexIgnore
    public /* synthetic */ Object b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ u d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public Object g;
    @DexIgnore
    public Object h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(u uVar, qn7 qn7) {
        super(qn7);
        this.d = uVar;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        this.b = obj;
        this.c |= RecyclerView.UNDEFINED_DURATION;
        return this.d.f(null, null, this);
    }
}
