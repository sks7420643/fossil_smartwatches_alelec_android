package com.fossil;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p03 extends rz2<Double> implements m13<Double>, y23, RandomAccess {
    @DexIgnore
    public double[] c;
    @DexIgnore
    public int d;

    /*
    static {
        new p03(new double[0], 0).zzb();
    }
    */

    @DexIgnore
    public p03() {
        this(new double[10], 0);
    }

    @DexIgnore
    public p03(double[] dArr, int i) {
        this.c = dArr;
        this.d = i;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        double doubleValue = ((Double) obj).doubleValue();
        a();
        if (i < 0 || i > (i2 = this.d)) {
            throw new IndexOutOfBoundsException(d(i));
        }
        double[] dArr = this.c;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.c, i, dArr2, i + 1, this.d - i);
            this.c = dArr2;
        }
        this.c[i] = doubleValue;
        this.d++;
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, com.fossil.rz2
    public final /* synthetic */ boolean add(Double d2) {
        b(d2.doubleValue());
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.rz2
    public final boolean addAll(Collection<? extends Double> collection) {
        a();
        h13.d(collection);
        if (!(collection instanceof p03)) {
            return super.addAll(collection);
        }
        p03 p03 = (p03) collection;
        int i = p03.d;
        if (i == 0) {
            return false;
        }
        int i2 = this.d;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i + i2;
            double[] dArr = this.c;
            if (i3 > dArr.length) {
                this.c = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(p03.c, 0, this.c, this.d, p03.d);
            this.d = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final void b(double d2) {
        a();
        int i = this.d;
        double[] dArr = this.c;
        if (i == dArr.length) {
            double[] dArr2 = new double[(((i * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.c = dArr2;
        }
        double[] dArr3 = this.c;
        int i2 = this.d;
        this.d = i2 + 1;
        dArr3[i2] = d2;
    }

    @DexIgnore
    public final void c(int i) {
        if (i < 0 || i >= this.d) {
            throw new IndexOutOfBoundsException(d(i));
        }
    }

    @DexIgnore
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @DexIgnore
    public final String d(int i) {
        int i2 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.rz2
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof p03)) {
            return super.equals(obj);
        }
        p03 p03 = (p03) obj;
        if (this.d != p03.d) {
            return false;
        }
        double[] dArr = p03.c;
        for (int i = 0; i < this.d; i++) {
            if (Double.doubleToLongBits(this.c[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        c(i);
        return Double.valueOf(this.c[i]);
    }

    @DexIgnore
    @Override // com.fossil.rz2
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.d; i2++) {
            i = (i * 31) + h13.b(Double.doubleToLongBits(this.c[i2]));
        }
        return i;
    }

    @DexIgnore
    public final int indexOf(Object obj) {
        if (!(obj instanceof Double)) {
            return -1;
        }
        double doubleValue = ((Double) obj).doubleValue();
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.c[i] == doubleValue) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object remove(int i) {
        a();
        c(i);
        double[] dArr = this.c;
        double d2 = dArr[i];
        int i2 = this.d;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.d--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d2);
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.rz2
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.d; i++) {
            if (obj.equals(Double.valueOf(this.c[i]))) {
                double[] dArr = this.c;
                System.arraycopy(dArr, i + 1, dArr, i, (this.d - i) - 1);
                this.d--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            double[] dArr = this.c;
            System.arraycopy(dArr, i2, dArr, i, this.d - i2);
            this.d -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        a();
        c(i);
        double[] dArr = this.c;
        double d2 = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d2);
    }

    @DexIgnore
    public final int size() {
        return this.d;
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.m13' to match base method */
    @Override // com.fossil.m13
    public final /* synthetic */ m13<Double> zza(int i) {
        if (i >= this.d) {
            return new p03(Arrays.copyOf(this.c, i), this.d);
        }
        throw new IllegalArgumentException();
    }
}
