package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ft4 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    LiveData<List<dt4>> b();

    @DexIgnore
    Long[] insert(List<dt4> list);
}
