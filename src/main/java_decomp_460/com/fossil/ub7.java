package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ rb7 f3563a;
    @DexIgnore
    public /* final */ List<qb7> b;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends com.fossil.qb7> */
    /* JADX WARN: Multi-variable type inference failed */
    public ub7(rb7 rb7, List<? extends qb7> list) {
        pq7.c(list, "movingObjects");
        this.f3563a = rb7;
        this.b = list;
    }

    @DexIgnore
    public final rb7 a() {
        return this.f3563a;
    }

    @DexIgnore
    public final List<qb7> b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ub7) {
                ub7 ub7 = (ub7) obj;
                if (!pq7.a(this.f3563a, ub7.f3563a) || !pq7.a(this.b, ub7.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        rb7 rb7 = this.f3563a;
        int hashCode = rb7 != null ? rb7.hashCode() : 0;
        List<qb7> list = this.b;
        if (list != null) {
            i = list.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UIThemeData(background=" + this.f3563a + ", movingObjects=" + this.b + ")";
    }
}
