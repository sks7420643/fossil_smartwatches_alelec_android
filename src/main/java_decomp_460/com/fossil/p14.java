package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p14<T> extends h54<T> {
    @DexIgnore
    public b b; // = b.NOT_READY;
    @DexIgnore
    public T c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f2765a;

        /*
        static {
            int[] iArr = new int[b.values().length];
            f2765a = iArr;
            try {
                iArr[b.DONE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2765a[b.READY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    @CanIgnoreReturnValue
    public final T b() {
        this.b = b.DONE;
        return null;
    }

    @DexIgnore
    public final boolean c() {
        this.b = b.FAILED;
        this.c = a();
        if (this.b == b.DONE) {
            return false;
        }
        this.b = b.READY;
        return true;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final boolean hasNext() {
        i14.s(this.b != b.FAILED);
        int i = a.f2765a[this.b.ordinal()];
        if (i == 1) {
            return false;
        }
        if (i != 2) {
            return c();
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.Iterator
    @CanIgnoreReturnValue
    public final T next() {
        if (hasNext()) {
            this.b = b.NOT_READY;
            T t = this.c;
            this.c = null;
            return t;
        }
        throw new NoSuchElementException();
    }
}
