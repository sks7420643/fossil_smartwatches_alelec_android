package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj6 extends yi6 {
    @DexIgnore
    public /* final */ MutableLiveData<Date> e; // = new MutableLiveData<>();
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g;
    @DexIgnore
    public List<DailyHeartRateSummary> h; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<h47<List<DailyHeartRateSummary>>> i;
    @DexIgnore
    public /* final */ zi6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$1", f = "HeartRateOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bj6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bj6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$1$currentUser$1", f = "HeartRateOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
        /* renamed from: com.fossil.bj6$a$a  reason: collision with other inner class name */
        public static final class C0017a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0017a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0017a aVar = new C0017a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                return ((C0017a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.k;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bj6 bj6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bj6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                C0017a aVar = new C0017a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) g;
            if (mFUser != null) {
                this.this$0.g = lk5.q0(mFUser.getCreatedAt());
                zi6 zi6 = this.this$0.j;
                Date date = this.this$0.f;
                if (date != null) {
                    Date date2 = this.this$0.g;
                    if (date2 == null) {
                        date2 = new Date();
                    }
                    zi6.g(date, date2);
                    this.this$0.e.l(this.this$0.f);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bj6 f437a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$mHeartRateSummaries$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {44, 44}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<DailyHeartRateSummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<DailyHeartRateSummary>>> hs0, qn7<? super tl7> qn7) {
                return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 289
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.bj6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(bj6 bj6) {
            this.f437a = bj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<DailyHeartRateSummary>>> apply(Date date) {
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<h47<? extends List<DailyHeartRateSummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bj6 f438a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {69}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public /* final */ /* synthetic */ h47 $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bj6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.bj6$c$a$a  reason: collision with other inner class name */
            public static final class C0018a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $calendar;
                @DexIgnore
                public /* final */ /* synthetic */ TreeMap $map;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0018a(a aVar, Calendar calendar, TreeMap treeMap, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$calendar = calendar;
                    this.$map = treeMap;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0018a aVar = new C0018a(this.this$0, this.$calendar, this.$map, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    return ((C0018a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Integer e;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        for (DailyHeartRateSummary dailyHeartRateSummary : (List) this.this$0.$it.c()) {
                            Calendar calendar = this.$calendar;
                            pq7.b(calendar, "calendar");
                            calendar.setTime(dailyHeartRateSummary.getDate());
                            this.$calendar.set(14, 0);
                            TreeMap treeMap = this.$map;
                            Calendar calendar2 = this.$calendar;
                            pq7.b(calendar2, "calendar");
                            Long f = ao7.f(calendar2.getTimeInMillis());
                            Resting resting = dailyHeartRateSummary.getResting();
                            treeMap.put(f, ao7.e((resting == null || (e = ao7.e(resting.getValue())) == null) ? 0 : e.intValue()));
                        }
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, h47 h47, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$data = list;
                this.$it = h47;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$data, this.$it, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                TreeMap<Long, Integer> treeMap;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    this.this$0.f438a.h = this.$data;
                    treeMap = new TreeMap<>();
                    Calendar instance = Calendar.getInstance();
                    dv7 h = this.this$0.f438a.h();
                    C0018a aVar = new C0018a(this, instance, treeMap, null);
                    this.L$0 = iv7;
                    this.L$1 = treeMap;
                    this.L$2 = instance;
                    this.label = 1;
                    if (eu7.g(h, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Calendar calendar = (Calendar) this.L$2;
                    treeMap = (TreeMap) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.f438a.j.e(treeMap);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(bj6 bj6) {
            this.f438a = bj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<DailyHeartRateSummary>> h47) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(h47 != null ? h47.d() : null);
            sb.append(" -- data.size=");
            sb.append((h47 == null || (list = (List) h47.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("HeartRateOverviewMonthPresenter", sb.toString());
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                List list2 = h47 != null ? (List) h47.c() : null;
                if (list2 != null && (!pq7.a(this.f438a.h, list2))) {
                    xw7 unused = gu7.d(this.f438a.k(), null, null, new a(this, list2, h47, null), 3, null);
                }
            }
        }
    }

    @DexIgnore
    public bj6(zi6 zi6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        pq7.c(zi6, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.j = zi6;
        this.k = userRepository;
        this.l = heartRateSummaryRepository;
        LiveData<h47<List<DailyHeartRateSummary>>> c2 = ss0.c(this.e, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.i = c2;
    }

    @DexIgnore
    public void A() {
        Date date = this.f;
        if (date == null || !lk5.p0(date).booleanValue()) {
            this.f = new Date();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
            xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HeartRateOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
    }

    @DexIgnore
    public void B() {
        this.j.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        A();
        LiveData<h47<List<DailyHeartRateSummary>>> liveData = this.i;
        zi6 zi6 = this.j;
        if (zi6 != null) {
            liveData.h((aj6) zi6, new c(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        MFLogger.d("HeartRateOverviewMonthPresenter", "stop");
        try {
            LiveData<h47<List<DailyHeartRateSummary>>> liveData = this.i;
            zi6 zi6 = this.j;
            if (zi6 != null) {
                liveData.n((aj6) zi6);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
        } catch (Exception e2) {
            MFLogger.d("HeartRateOverviewMonthPresenter", "stop ex " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.yi6
    public void n(Date date) {
        pq7.c(date, "date");
        if (this.e.e() == null || !lk5.m0(this.e.e(), date)) {
            this.e.l(date);
        }
    }
}
