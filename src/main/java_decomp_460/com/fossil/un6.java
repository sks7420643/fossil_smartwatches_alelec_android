package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un6 extends rn6 {
    @DexIgnore
    public String e; // = PortfolioApp.h0.c().J();
    @DexIgnore
    public /* final */ a f; // = new a(this);
    @DexIgnore
    public /* final */ sn6 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ un6 f3622a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(un6 un6) {
            this.f3622a = un6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwarePresenter", "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && vt7.j(otaEvent.getSerial(), PortfolioApp.h0.c().J(), true)) {
                    this.f3622a.o().Z((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    @DexIgnore
    public un6(sn6 sn6) {
        pq7.c(sn6, "mView");
        this.g = sn6;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        PortfolioApp c = PortfolioApp.h0.c();
        a aVar = this.f;
        c.registerReceiver(aVar, new IntentFilter(PortfolioApp.h0.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        n();
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        try {
            PortfolioApp.h0.c().unregisterReceiver(this.f);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeUpdateFirmwarePresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public final void n() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeUpdateFirmwarePresenter", "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (nk5.o.w(deviceBySerial)) {
            explore.setDescription(um5.c(PortfolioApp.h0.c(), 2131886916));
            explore.setBackground(2131231353);
            explore2.setDescription(um5.c(PortfolioApp.h0.c(), 2131886917));
            explore2.setBackground(2131231351);
            explore3.setDescription(um5.c(PortfolioApp.h0.c(), 2131886914));
            explore3.setBackground(2131231354);
            explore4.setDescription(um5.c(PortfolioApp.h0.c(), 2131886915));
            explore4.setBackground(2131231350);
        } else {
            explore.setDescription(um5.c(PortfolioApp.h0.c(), 2131886923));
            explore.setBackground(2131231353);
            explore2.setDescription(um5.c(PortfolioApp.h0.c(), 2131886922));
            explore2.setBackground(2131231355);
            explore3.setDescription(um5.c(PortfolioApp.h0.c(), 2131886920));
            explore3.setBackground(2131231354);
            explore4.setDescription(um5.c(PortfolioApp.h0.c(), 2131886921));
            explore4.setBackground(2131231352);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.g.X(arrayList);
    }

    @DexIgnore
    public final sn6 o() {
        return this.g;
    }

    @DexIgnore
    public void p() {
        this.g.M5(this);
    }
}
