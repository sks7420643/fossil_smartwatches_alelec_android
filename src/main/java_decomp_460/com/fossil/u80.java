package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Handler;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import org.json.JSONObject;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u80 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public uc0 f3522a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ ArrayList<File> c; // = new ArrayList<>();
    @DexIgnore
    public q80 d;
    @DexIgnore
    public md0 e;
    @DexIgnore
    public long f; // = 60000;
    @DexIgnore
    public /* final */ p80 g;
    @DexIgnore
    public zw1 h;
    @DexIgnore
    public /* final */ b90 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public boolean k;

    @DexIgnore
    public u80(p80 p80, zw1 zw1, b90 b90, Handler handler, boolean z) {
        this.g = p80;
        this.h = zw1;
        this.i = b90;
        this.j = handler;
        this.k = z;
        c(this.h);
    }

    @DexIgnore
    public final void a() {
        Boolean bool = null;
        Context a2 = id0.i.a();
        ActivityManager activityManager = (ActivityManager) (a2 != null ? a2.getSystemService(Constants.ACTIVITY) : null);
        if (activityManager != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            bool = Boolean.valueOf(memoryInfo.lowMemory);
        }
        if (!(!pq7.a(bool, Boolean.FALSE))) {
            synchronized (Boolean.valueOf(this.b)) {
                if (!this.b) {
                    this.b = true;
                    this.c.clear();
                    mm7.t(this.c, em7.Y(this.g.e()));
                    e();
                }
            }
        }
    }

    @DexIgnore
    public final boolean c(zw1 zw1) {
        uc0 uc0;
        synchronized (this) {
            m80 m80 = m80.c;
            StringBuilder e2 = e.e("updateEndPoint: url=");
            e2.append(zw1.c());
            e2.append(", ");
            e2.append("access=");
            e2.append(zw1.a());
            e2.append(", secret=");
            e2.append(zw1.b());
            e2.append('.');
            m80.a("LogUploader", e2.toString(), new Object[0]);
            if (zw1.c().length() == 0) {
                return false;
            }
            this.h = zw1;
            String b2 = zw1.b();
            String a2 = zw1.a();
            OkHttpClient.b bVar = new OkHttpClient.b();
            bVar.b(new zc0(a2, b2));
            bVar.g(30000, TimeUnit.MILLISECONDS);
            bVar.m(60000, TimeUnit.MILLISECONDS);
            try {
                Retrofit.b bVar2 = new Retrofit.b();
                bVar2.f(bVar.d());
                bVar2.a(h98.f());
                bVar2.b("http://localhost/");
                uc0 = (uc0) bVar2.d().b(uc0.class);
            } catch (Exception e3) {
                d90.i.i(e3);
                uc0 = null;
            }
            if (uc0 != null) {
                this.f3522a = uc0;
                m80 m802 = m80.c;
                StringBuilder e4 = e.e("updateEndPoint: success, client=");
                e4.append(this.f3522a);
                m802.a("LogUploader", e4.toString(), new Object[0]);
                return true;
            }
            m80.c.a("LogUploader", "updateEndPoint: cannot build Client, cache endPoint for future retry.", new Object[0]);
            return false;
        }
    }

    @DexIgnore
    public final void d() {
        synchronized (Boolean.valueOf(this.b)) {
            this.c.clear();
            this.b = false;
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final void e() {
        Boolean bool;
        Boolean bool2;
        uc0 uc0 = this.f3522a;
        if (uc0 == null && (!vt7.l(this.h.c())) && (!vt7.l(this.h.a())) && (!vt7.l(this.h.b()))) {
            m80.c.a("LogUploader", "logClient is null but endpoint is exists, retry build logClient", new Object[0]);
            c(this.h);
        }
        String str = uc0 == null ? "Invalid end point" : !fx1.b(fx1.f1233a, id0.i.a(), false, 2, null) ? "Network is not available" : (!this.k || fx1.f1233a.c(id0.i.a())) ? new String() : "Wifi is not available";
        if ((str.length() > 0) || uc0 == null) {
            m80.c.a("LogUploader", "Stop uploading: %s.", str);
            d();
            return;
        }
        File file = (File) pm7.H(this.c);
        if (file != null) {
            try {
                Context a2 = id0.i.a();
                ActivityManager activityManager = (ActivityManager) (a2 != null ? a2.getSystemService(Constants.ACTIVITY) : null);
                if (activityManager != null) {
                    ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                    activityManager.getMemoryInfo(memoryInfo);
                    bool = Boolean.valueOf(memoryInfo.lowMemory);
                } else {
                    bool = null;
                }
                if (!pq7.a(bool, Boolean.FALSE)) {
                    d();
                    return;
                }
                JSONObject b2 = this.i.b(file);
                if (b2.length() > 0) {
                    Context a3 = id0.i.a();
                    ActivityManager activityManager2 = (ActivityManager) (a3 != null ? a3.getSystemService(Constants.ACTIVITY) : null);
                    if (activityManager2 != null) {
                        ActivityManager.MemoryInfo memoryInfo2 = new ActivityManager.MemoryInfo();
                        activityManager2.getMemoryInfo(memoryInfo2);
                        bool2 = Boolean.valueOf(memoryInfo2.lowMemory);
                    } else {
                        bool2 = null;
                    }
                    if (!pq7.a(bool2, Boolean.FALSE)) {
                        d();
                        return;
                    }
                    String jSONObject = b2.toString();
                    pq7.b(jSONObject, "logFileInJSON.toString()");
                    uc0.a(this.h.c(), jSONObject).D(new s80(this, file));
                    return;
                }
                this.c.remove(file);
                file.delete();
                this.j.post(new t80(this));
            } catch (OutOfMemoryError e2) {
                d();
            }
        } else {
            d();
        }
    }
}
