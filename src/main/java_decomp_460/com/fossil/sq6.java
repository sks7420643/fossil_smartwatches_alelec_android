package com.fossil;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq6 extends pv5 implements rq6 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public static /* final */ Pattern z;
    @DexIgnore
    public qq6 g;
    @DexIgnore
    public TextInputEditText h;
    @DexIgnore
    public TextInputEditText i;
    @DexIgnore
    public TextInputEditText j;
    @DexIgnore
    public TextInputLayout k;
    @DexIgnore
    public TextInputLayout l;
    @DexIgnore
    public TextInputLayout m;
    @DexIgnore
    public ProgressButton s;
    @DexIgnore
    public FlexibleTextView t;
    @DexIgnore
    public FlexibleTextView u;
    @DexIgnore
    public RTLImageView v;
    @DexIgnore
    public String w;
    @DexIgnore
    public boolean x; // = true;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final sq6 a() {
            return new sq6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sq6 b;

        @DexIgnore
        public b(sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            sq6.M6(this.b).clearFocus();
            sq6.L6(this.b).clearFocus();
            sq6.K6(this.b).clearFocus();
            pq7.b(view, "it");
            view.setFocusable(true);
            if (this.b.V6()) {
                sq6.N6(this.b).n(sq6.L6(this.b).getEditableText().toString(), sq6.K6(this.b).getEditableText().toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sq6 b;

        @DexIgnore
        public c(sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                FragmentActivity requireActivity = this.b.requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                if (!requireActivity.isFinishing()) {
                    FragmentActivity requireActivity2 = this.b.requireActivity();
                    pq7.b(requireActivity2, "requireActivity()");
                    if (!requireActivity2.isDestroyed()) {
                        this.b.requireActivity().finish();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ sq6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            pq7.c(editable, "s");
            sq6.O6(this.b).setEnabled(this.b.V6());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ sq6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            pq7.c(editable, "s");
            sq6.O6(this.b).setEnabled(this.b.V6());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ sq6 b;

        @DexIgnore
        public f(sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            boolean z2 = false;
            if (z) {
                sq6.P6(this.b).setVisibility(0);
                sq6.Q6(this.b).setVisibility(0);
                return;
            }
            Editable editableText = sq6.K6(this.b).getEditableText();
            pq7.b(editableText, "mEdtNew.editableText");
            if (editableText.length() == 0) {
                z2 = true;
            }
            if (z2 || this.b.x) {
                sq6.P6(this.b).setVisibility(8);
                sq6.Q6(this.b).setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ sq6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            pq7.c(editable, "s");
            sq6.O6(this.b).setEnabled(this.b.V6());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            pq7.c(charSequence, "s");
        }
    }

    /*
    static {
        pq7.b(sq6.class.getSimpleName(), "ProfileChangePasswordFra\u2026nt::class.java.simpleName");
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            z = compile;
        } else {
            pq7.i();
            throw null;
        }
    }
    */

    @DexIgnore
    public static final /* synthetic */ TextInputEditText K6(sq6 sq6) {
        TextInputEditText textInputEditText = sq6.i;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        pq7.n("mEdtNew");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText L6(sq6 sq6) {
        TextInputEditText textInputEditText = sq6.h;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        pq7.n("mEdtOld");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText M6(sq6 sq6) {
        TextInputEditText textInputEditText = sq6.j;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        pq7.n("mEdtRepeat");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ qq6 N6(sq6 sq6) {
        qq6 qq6 = sq6.g;
        if (qq6 != null) {
            return qq6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProgressButton O6(sq6 sq6) {
        ProgressButton progressButton = sq6.s;
        if (progressButton != null) {
            return progressButton;
        }
        pq7.n("mSaveButton");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView P6(sq6 sq6) {
        FlexibleTextView flexibleTextView = sq6.t;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        pq7.n("mTvCheckChar");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView Q6(sq6 sq6) {
        FlexibleTextView flexibleTextView = sq6.u;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        pq7.n("mTvCheckCombine");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                pq7.b(requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    requireActivity().finish();
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.rq6
    public void I0(int i2, String str) {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.rq6
    public void P2() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager requireFragmentManager = requireFragmentManager();
            pq7.b(requireFragmentManager, "requireFragmentManager()");
            s37.W(requireFragmentManager);
        }
    }

    @DexIgnore
    public final void T6(boolean z2) {
        TextInputLayout textInputLayout = this.k;
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(z2);
            if (z2) {
                TextInputLayout textInputLayout2 = this.k;
                if (textInputLayout2 != null) {
                    textInputLayout2.setError(um5.c(PortfolioApp.h0.c(), 2131887086));
                } else {
                    pq7.n("mTvOldError");
                    throw null;
                }
            } else {
                TextInputLayout textInputLayout3 = this.k;
                if (textInputLayout3 != null) {
                    textInputLayout3.setError("");
                } else {
                    pq7.n("mTvOldError");
                    throw null;
                }
            }
        } else {
            pq7.n("mTvOldError");
            throw null;
        }
    }

    @DexIgnore
    public final void U6(ja5 ja5) {
        FlexibleTextInputEditText flexibleTextInputEditText = ja5.s;
        pq7.b(flexibleTextInputEditText, "binding.etOldPass");
        this.h = flexibleTextInputEditText;
        FlexibleTextInputEditText flexibleTextInputEditText2 = ja5.r;
        pq7.b(flexibleTextInputEditText2, "binding.etNewPass");
        this.i = flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3 = ja5.t;
        pq7.b(flexibleTextInputEditText3, "binding.etRepeatPass");
        this.j = flexibleTextInputEditText3;
        FlexibleTextInputLayout flexibleTextInputLayout = ja5.v;
        pq7.b(flexibleTextInputLayout, "binding.inputOldPass");
        this.k = flexibleTextInputLayout;
        FlexibleTextInputLayout flexibleTextInputLayout2 = ja5.u;
        pq7.b(flexibleTextInputLayout2, "binding.inputNewPass");
        this.l = flexibleTextInputLayout2;
        FlexibleTextInputLayout flexibleTextInputLayout3 = ja5.w;
        pq7.b(flexibleTextInputLayout3, "binding.inputRepeatPass");
        this.m = flexibleTextInputLayout3;
        ProgressButton progressButton = ja5.z;
        pq7.b(progressButton, "binding.save");
        this.s = progressButton;
        FlexibleTextView flexibleTextView = ja5.A;
        pq7.b(flexibleTextView, "binding.tvErrorCheckCharacter");
        this.t = flexibleTextView;
        FlexibleTextView flexibleTextView2 = ja5.B;
        pq7.b(flexibleTextView2, "binding.tvErrorCheckCombine");
        this.u = flexibleTextView2;
        RTLImageView rTLImageView = ja5.x;
        pq7.b(rTLImageView, "binding.ivBack");
        this.v = rTLImageView;
        ProgressButton progressButton2 = this.s;
        if (progressButton2 != null) {
            progressButton2.setOnClickListener(new b(this));
            RTLImageView rTLImageView2 = this.v;
            if (rTLImageView2 != null) {
                rTLImageView2.setOnClickListener(new c(this));
                TextInputEditText textInputEditText = this.h;
                if (textInputEditText != null) {
                    textInputEditText.addTextChangedListener(new d(this));
                    TextInputEditText textInputEditText2 = this.i;
                    if (textInputEditText2 != null) {
                        textInputEditText2.addTextChangedListener(new e(this));
                        TextInputEditText textInputEditText3 = this.i;
                        if (textInputEditText3 != null) {
                            textInputEditText3.setOnFocusChangeListener(new f(this));
                            TextInputEditText textInputEditText4 = this.j;
                            if (textInputEditText4 != null) {
                                textInputEditText4.addTextChangedListener(new g(this));
                            } else {
                                pq7.n("mEdtRepeat");
                                throw null;
                            }
                        } else {
                            pq7.n("mEdtNew");
                            throw null;
                        }
                    } else {
                        pq7.n("mEdtNew");
                        throw null;
                    }
                } else {
                    pq7.n("mEdtOld");
                    throw null;
                }
            } else {
                pq7.n("mBackButton");
                throw null;
            }
        } else {
            pq7.n("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public final boolean V6() {
        boolean z2;
        boolean z3 = true;
        TextInputEditText textInputEditText = this.h;
        if (textInputEditText != null) {
            String obj = textInputEditText.getEditableText().toString();
            TextInputEditText textInputEditText2 = this.i;
            if (textInputEditText2 != null) {
                String obj2 = textInputEditText2.getEditableText().toString();
                TextInputEditText textInputEditText3 = this.j;
                if (textInputEditText3 != null) {
                    String obj3 = textInputEditText3.getEditableText().toString();
                    if (obj2.length() >= 7) {
                        FlexibleTextView flexibleTextView = this.t;
                        if (flexibleTextView != null) {
                            flexibleTextView.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131231054), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = true;
                        } else {
                            pq7.n("mTvCheckChar");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = this.t;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = false;
                        } else {
                            pq7.n("mTvCheckChar");
                            throw null;
                        }
                    }
                    if (z.matcher(obj2).matches()) {
                        FlexibleTextView flexibleTextView3 = this.u;
                        if (flexibleTextView3 != null) {
                            flexibleTextView3.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131231054), (Drawable) null, (Drawable) null, (Drawable) null);
                        } else {
                            pq7.n("mTvCheckCombine");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView4 = this.u;
                        if (flexibleTextView4 != null) {
                            flexibleTextView4.setCompoundDrawablesWithIntrinsicBounds(gl0.f(PortfolioApp.h0.c(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = false;
                        } else {
                            pq7.n("mTvCheckCombine");
                            throw null;
                        }
                    }
                    if (obj.length() == 0) {
                        TextInputLayout textInputLayout = this.k;
                        if (textInputLayout != null) {
                            textInputLayout.setErrorEnabled(false);
                            TextInputLayout textInputLayout2 = this.k;
                            if (textInputLayout2 != null) {
                                textInputLayout2.setError("");
                            } else {
                                pq7.n("mTvOldError");
                                throw null;
                            }
                        } else {
                            pq7.n("mTvOldError");
                            throw null;
                        }
                    } else {
                        if (pq7.a(this.w, obj)) {
                            TextInputLayout textInputLayout3 = this.k;
                            if (textInputLayout3 != null) {
                                textInputLayout3.setErrorEnabled(true);
                                TextInputLayout textInputLayout4 = this.k;
                                if (textInputLayout4 != null) {
                                    textInputLayout4.setError(um5.c(PortfolioApp.h0.c(), 2131887086));
                                } else {
                                    pq7.n("mTvOldError");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvOldError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout5 = this.k;
                            if (textInputLayout5 != null) {
                                textInputLayout5.setErrorEnabled(false);
                                TextInputLayout textInputLayout6 = this.k;
                                if (textInputLayout6 != null) {
                                    textInputLayout6.setError("");
                                } else {
                                    pq7.n("mTvOldError");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvOldError");
                                throw null;
                            }
                        }
                        if ((obj2.length() == 0) || !pq7.a(obj2, obj)) {
                            TextInputLayout textInputLayout7 = this.l;
                            if (textInputLayout7 != null) {
                                textInputLayout7.setErrorEnabled(false);
                                TextInputLayout textInputLayout8 = this.l;
                                if (textInputLayout8 != null) {
                                    textInputLayout8.setError("");
                                } else {
                                    pq7.n("mTvNewError");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvNewError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout9 = this.l;
                            if (textInputLayout9 != null) {
                                textInputLayout9.setErrorEnabled(true);
                                TextInputLayout textInputLayout10 = this.l;
                                if (textInputLayout10 != null) {
                                    textInputLayout10.setError(um5.c(PortfolioApp.h0.c(), 2131886992));
                                } else {
                                    pq7.n("mTvNewError");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (obj2.length() == 0) {
                        TextInputLayout textInputLayout11 = this.l;
                        if (textInputLayout11 != null) {
                            textInputLayout11.setErrorEnabled(false);
                            TextInputLayout textInputLayout12 = this.l;
                            if (textInputLayout12 != null) {
                                textInputLayout12.setError("");
                                TextInputLayout textInputLayout13 = this.m;
                                if (textInputLayout13 != null) {
                                    textInputLayout13.setErrorEnabled(false);
                                    TextInputLayout textInputLayout14 = this.m;
                                    if (textInputLayout14 != null) {
                                        textInputLayout14.setError("");
                                    } else {
                                        pq7.n("mTvRepeatError");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvNewError");
                                throw null;
                            }
                        } else {
                            pq7.n("mTvNewError");
                            throw null;
                        }
                    }
                    if ((obj3.length() == 0) || pq7.a(obj3, obj2)) {
                        TextInputLayout textInputLayout15 = this.m;
                        if (textInputLayout15 != null) {
                            textInputLayout15.setErrorEnabled(false);
                            TextInputLayout textInputLayout16 = this.m;
                            if (textInputLayout16 != null) {
                                textInputLayout16.setError("");
                            } else {
                                pq7.n("mTvRepeatError");
                                throw null;
                            }
                        } else {
                            pq7.n("mTvRepeatError");
                            throw null;
                        }
                    } else {
                        if (!(obj2.length() == 0) && (!pq7.a(obj3, obj2))) {
                            TextInputLayout textInputLayout17 = this.m;
                            if (textInputLayout17 != null) {
                                textInputLayout17.setErrorEnabled(true);
                                TextInputLayout textInputLayout18 = this.m;
                                if (textInputLayout18 != null) {
                                    textInputLayout18.setError(um5.c(PortfolioApp.h0.c(), 2131887089));
                                } else {
                                    pq7.n("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvRepeatError");
                                throw null;
                            }
                        }
                    }
                    this.x = z2;
                    if (z2) {
                        if (!(obj.length() == 0)) {
                            if (!(obj2.length() == 0) && (!pq7.a(obj, obj2)) && pq7.a(obj2, obj3) && (!pq7.a(obj, this.w))) {
                                TextInputLayout textInputLayout19 = this.l;
                                if (textInputLayout19 != null) {
                                    textInputLayout19.setErrorEnabled(false);
                                    TextInputLayout textInputLayout20 = this.l;
                                    if (textInputLayout20 != null) {
                                        textInputLayout20.setError("");
                                        TextInputLayout textInputLayout21 = this.m;
                                        if (textInputLayout21 != null) {
                                            textInputLayout21.setErrorEnabled(false);
                                            TextInputLayout textInputLayout22 = this.m;
                                            if (textInputLayout22 != null) {
                                                textInputLayout22.setError("");
                                                return true;
                                            }
                                            pq7.n("mTvRepeatError");
                                            throw null;
                                        }
                                        pq7.n("mTvRepeatError");
                                        throw null;
                                    }
                                    pq7.n("mTvNewError");
                                    throw null;
                                }
                                pq7.n("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (!this.x) {
                        if (obj2.length() != 0) {
                            z3 = false;
                        }
                        if (!z3) {
                            FlexibleTextView flexibleTextView5 = this.t;
                            if (flexibleTextView5 != null) {
                                flexibleTextView5.setVisibility(0);
                                FlexibleTextView flexibleTextView6 = this.u;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                } else {
                                    pq7.n("mTvCheckCombine");
                                    throw null;
                                }
                            } else {
                                pq7.n("mTvCheckChar");
                                throw null;
                            }
                        }
                    }
                    return false;
                }
                pq7.n("mEdtRepeat");
                throw null;
            }
            pq7.n("mEdtNew");
            throw null;
        }
        pq7.n("mEdtOld");
        throw null;
    }

    @DexIgnore
    /* renamed from: W6 */
    public void M5(qq6 qq6) {
        pq7.c(qq6, "presenter");
        i14.l(qq6);
        pq7.b(qq6, "checkNotNull(presenter)");
        this.g = qq6;
    }

    @DexIgnore
    @Override // com.fossil.rq6
    public void f4() {
        if (isActive()) {
            ProgressButton progressButton = this.s;
            if (progressButton != null) {
                progressButton.setEnabled(false);
                TextInputEditText textInputEditText = this.h;
                if (textInputEditText != null) {
                    this.w = textInputEditText.getEditableText().toString();
                    T6(true);
                    return;
                }
                pq7.n("mEdtOld");
                throw null;
            }
            pq7.n("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.rq6
    public void k() {
        if (isActive()) {
            ProgressButton progressButton = this.s;
            if (progressButton != null) {
                progressButton.f();
            } else {
                pq7.n("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.rq6
    public void l6() {
        if (isActive()) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886986);
            pq7.b(c2, "LanguageHelper.getString\u2026urPasswordHasBeenChanged)");
            I6(c2);
            requireActivity().finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.rq6
    public void m() {
        if (isActive()) {
            ProgressButton progressButton = this.s;
            if (progressButton != null) {
                progressButton.g();
            } else {
                pq7.n("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        ja5 ja5 = (ja5) aq0.f(LayoutInflater.from(getContext()), 2131558609, null, false, A6());
        pq7.b(ja5, "binding");
        U6(ja5);
        new g37(this, ja5);
        ProgressButton progressButton = this.s;
        if (progressButton != null) {
            progressButton.setEnabled(false);
            return ja5.n();
        }
        pq7.n("mSaveButton");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qq6 qq6 = this.g;
        if (qq6 != null) {
            qq6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qq6 qq6 = this.g;
        if (qq6 != null) {
            qq6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
