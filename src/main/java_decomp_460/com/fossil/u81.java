package com.fossil;

import com.fossil.dl7;
import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u81 implements b18, rp7<Throwable, tl7> {
    @DexIgnore
    public /* final */ a18 b;
    @DexIgnore
    public /* final */ ku7<Response> c;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.ku7<? super okhttp3.Response> */
    /* JADX WARN: Multi-variable type inference failed */
    public u81(a18 a18, ku7<? super Response> ku7) {
        pq7.c(a18, "call");
        pq7.c(ku7, "continuation");
        this.b = a18;
        this.c = ku7;
    }

    @DexIgnore
    public void a(Throwable th) {
        try {
            this.b.cancel();
        } catch (Throwable th2) {
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        a(th);
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.b18
    public void onFailure(a18 a18, IOException iOException) {
        pq7.c(a18, "call");
        pq7.c(iOException, "e");
        if (!a18.f()) {
            ku7<Response> ku7 = this.c;
            dl7.a aVar = dl7.Companion;
            ku7.resumeWith(dl7.m1constructorimpl(el7.a(iOException)));
        }
    }

    @DexIgnore
    @Override // com.fossil.b18
    public void onResponse(a18 a18, Response response) {
        pq7.c(a18, "call");
        pq7.c(response, "response");
        ku7<Response> ku7 = this.c;
        dl7.a aVar = dl7.Companion;
        ku7.resumeWith(dl7.m1constructorimpl(response));
    }
}
