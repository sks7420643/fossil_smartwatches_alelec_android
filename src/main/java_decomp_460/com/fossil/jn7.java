package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jn7 implements Iterator<ql7>, jr7 {
    @DexIgnore
    /* renamed from: a */
    public final ql7 next() {
        return ql7.a(b());
    }

    @DexIgnore
    public abstract short b();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
