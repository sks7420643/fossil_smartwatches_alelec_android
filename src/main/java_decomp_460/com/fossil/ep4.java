package com.fossil;

import android.content.ContentResolver;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep4 implements Factory<ContentResolver> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f965a;

    @DexIgnore
    public ep4(uo4 uo4) {
        this.f965a = uo4;
    }

    @DexIgnore
    public static ep4 a(uo4 uo4) {
        return new ep4(uo4);
    }

    @DexIgnore
    public static ContentResolver c(uo4 uo4) {
        ContentResolver l = uo4.l();
        lk7.c(l, "Cannot return null from a non-@Nullable @Provides method");
        return l;
    }

    @DexIgnore
    /* renamed from: b */
    public ContentResolver get() {
        return c(this.f965a);
    }
}
