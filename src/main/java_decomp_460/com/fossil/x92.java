package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.m62;
import com.fossil.r62;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x92 extends ls3 implements r62.b, r62.c {
    @DexIgnore
    public static m62.a<? extends ys3, gs3> i; // = vs3.c;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public /* final */ m62.a<? extends ys3, gs3> d;
    @DexIgnore
    public Set<Scope> e;
    @DexIgnore
    public ac2 f;
    @DexIgnore
    public ys3 g;
    @DexIgnore
    public aa2 h;

    @DexIgnore
    public x92(Context context, Handler handler, ac2 ac2) {
        this(context, handler, ac2, i);
    }

    @DexIgnore
    public x92(Context context, Handler handler, ac2 ac2, m62.a<? extends ys3, gs3> aVar) {
        this.b = context;
        this.c = handler;
        rc2.l(ac2, "ClientSettings must not be null");
        this.f = ac2;
        this.e = ac2.j();
        this.d = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ks3
    public final void Q2(us3 us3) {
        this.c.post(new y92(this, us3));
    }

    @DexIgnore
    public final void a3(aa2 aa2) {
        ys3 ys3 = this.g;
        if (ys3 != null) {
            ys3.a();
        }
        this.f.m(Integer.valueOf(System.identityHashCode(this)));
        m62.a<? extends ys3, gs3> aVar = this.d;
        Context context = this.b;
        Looper looper = this.c.getLooper();
        ac2 ac2 = this.f;
        this.g = (ys3) aVar.c(context, looper, ac2, ac2.k(), this, this);
        this.h = aa2;
        Set<Scope> set = this.e;
        if (set == null || set.isEmpty()) {
            this.c.post(new z92(this));
        } else {
            this.g.b();
        }
    }

    @DexIgnore
    public final ys3 b3() {
        return this.g;
    }

    @DexIgnore
    public final void c3() {
        ys3 ys3 = this.g;
        if (ys3 != null) {
            ys3.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void d(int i2) {
        this.g.a();
    }

    @DexIgnore
    public final void d3(us3 us3) {
        z52 c2 = us3.c();
        if (c2.A()) {
            tc2 f2 = us3.f();
            z52 f3 = f2.f();
            if (!f3.A()) {
                String valueOf = String.valueOf(f3);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.h.c(f3);
                this.g.a();
                return;
            }
            this.h.b(f2.c(), this.e);
        } else {
            this.h.c(c2);
        }
        this.g.a();
    }

    @DexIgnore
    @Override // com.fossil.k72
    public final void e(Bundle bundle) {
        this.g.e(this);
    }

    @DexIgnore
    @Override // com.fossil.r72
    public final void n(z52 z52) {
        this.h.c(z52);
    }
}
