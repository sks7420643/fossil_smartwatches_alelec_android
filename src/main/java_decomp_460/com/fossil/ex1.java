package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ KeyStore f1000a;
    @DexIgnore
    public static /* final */ ex1 b;

    /*
    static {
        ex1 ex1 = new ex1();
        b = ex1;
        f1000a = ex1.a();
    }
    */

    @DexIgnore
    public final KeyStore a() {
        KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
        instance.load(null);
        pq7.b(instance, "mKeyStore");
        return instance;
    }

    @DexIgnore
    public final KeyPair b(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "alias");
        KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        Calendar instance2 = Calendar.getInstance();
        Calendar instance3 = Calendar.getInstance();
        instance3.add(1, 20);
        KeyPairGeneratorSpec.Builder serialNumber = new KeyPairGeneratorSpec.Builder(context.getApplicationContext()).setAlias(str).setSerialNumber(BigInteger.ONE);
        KeyPairGeneratorSpec.Builder subject = serialNumber.setSubject(new X500Principal("CN=" + str + " CA Certificate"));
        pq7.b(instance2, GoalPhase.COLUMN_START_DATE);
        KeyPairGeneratorSpec.Builder startDate = subject.setStartDate(instance2.getTime());
        pq7.b(instance3, GoalPhase.COLUMN_END_DATE);
        KeyPairGeneratorSpec.Builder endDate = startDate.setEndDate(instance3.getTime());
        pq7.b(endDate, "KeyPairGeneratorSpec.Bui\u2026.setEndDate(endDate.time)");
        instance.initialize(endDate.build());
        KeyPair generateKeyPair = instance.generateKeyPair();
        pq7.b(generateKeyPair, "generator.generateKeyPair()");
        return generateKeyPair;
    }

    @DexIgnore
    public final boolean c(String str) {
        pq7.c(str, "alias");
        try {
            f1000a.deleteEntry(str);
            return true;
        } catch (KeyStoreException e) {
            return false;
        }
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey d(String str) {
        KeyGenerator instance = KeyGenerator.getInstance("AES", "AndroidKeyStore");
        KeyGenParameterSpec build = new KeyGenParameterSpec.Builder(str, 3).setBlockModes("GCM").setEncryptionPaddings("NoPadding").build();
        pq7.b(build, "KeyGenParameterSpec\n    \u2026ONE)\n            .build()");
        instance.init(build);
        SecretKey generateKey = instance.generateKey();
        pq7.b(generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final KeyPair e(String str) {
        Certificate certificate;
        pq7.c(str, "alias");
        PrivateKey privateKey = (PrivateKey) f1000a.getKey(str, null);
        PublicKey publicKey = (privateKey == null || (certificate = f1000a.getCertificate(str)) == null) ? null : certificate.getPublicKey();
        if (privateKey == null || publicKey == null) {
            return null;
        }
        return new KeyPair(publicKey, privateKey);
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey f(String str) {
        pq7.c(str, "aliasName");
        if (!f1000a.containsAlias(str)) {
            return d(str);
        }
        KeyStore.Entry entry = f1000a.getEntry(str, null);
        if (entry != null) {
            SecretKey secretKey = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            pq7.b(secretKey, "secretKeyEntry.secretKey");
            return secretKey;
        }
        throw new il7("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }
}
