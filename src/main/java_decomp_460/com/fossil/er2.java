package com.fossil;

import android.location.Location;
import com.fossil.p72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class er2 implements p72.b<ga3> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ Location f977a;

    @DexIgnore
    public er2(dr2 dr2, Location location) {
        this.f977a = location;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.p72.b
    public final /* synthetic */ void a(ga3 ga3) {
        ga3.onLocationChanged(this.f977a);
    }

    @DexIgnore
    @Override // com.fossil.p72.b
    public final void b() {
    }
}
