package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la4 extends ta4.d.AbstractC0224d.a.b.c {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2162a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> c;
    @DexIgnore
    public /* final */ ta4.d.AbstractC0224d.a.b.c d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.d.AbstractC0224d.a.b.c.AbstractC0229a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f2163a;
        @DexIgnore
        public String b;
        @DexIgnore
        public ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> c;
        @DexIgnore
        public ta4.d.AbstractC0224d.a.b.c d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c.AbstractC0229a
        public ta4.d.AbstractC0224d.a.b.c a() {
            String str = "";
            if (this.f2163a == null) {
                str = " type";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (this.e == null) {
                str = str + " overflowCount";
            }
            if (str.isEmpty()) {
                return new la4(this.f2163a, this.b, this.c, this.d, this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c.AbstractC0229a
        public ta4.d.AbstractC0224d.a.b.c.AbstractC0229a b(ta4.d.AbstractC0224d.a.b.c cVar) {
            this.d = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c.AbstractC0229a
        public ta4.d.AbstractC0224d.a.b.c.AbstractC0229a c(ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> ua4) {
            if (ua4 != null) {
                this.c = ua4;
                return this;
            }
            throw new NullPointerException("Null frames");
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c.AbstractC0229a
        public ta4.d.AbstractC0224d.a.b.c.AbstractC0229a d(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c.AbstractC0229a
        public ta4.d.AbstractC0224d.a.b.c.AbstractC0229a e(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c.AbstractC0229a
        public ta4.d.AbstractC0224d.a.b.c.AbstractC0229a f(String str) {
            if (str != null) {
                this.f2163a = str;
                return this;
            }
            throw new NullPointerException("Null type");
        }
    }

    @DexIgnore
    public la4(String str, String str2, ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> ua4, ta4.d.AbstractC0224d.a.b.c cVar, int i) {
        this.f2162a = str;
        this.b = str2;
        this.c = ua4;
        this.d = cVar;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c
    public ta4.d.AbstractC0224d.a.b.c b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c
    public ua4<ta4.d.AbstractC0224d.a.b.e.AbstractC0233b> c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c
    public int d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        ta4.d.AbstractC0224d.a.b.c cVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.d.AbstractC0224d.a.b.c)) {
            return false;
        }
        ta4.d.AbstractC0224d.a.b.c cVar2 = (ta4.d.AbstractC0224d.a.b.c) obj;
        return this.f2162a.equals(cVar2.f()) && ((str = this.b) != null ? str.equals(cVar2.e()) : cVar2.e() == null) && this.c.equals(cVar2.c()) && ((cVar = this.d) != null ? cVar.equals(cVar2.b()) : cVar2.b() == null) && this.e == cVar2.d();
    }

    @DexIgnore
    @Override // com.fossil.ta4.d.AbstractC0224d.a.b.c
    public String f() {
        return this.f2162a;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int hashCode = this.f2162a.hashCode();
        String str = this.b;
        int hashCode2 = str == null ? 0 : str.hashCode();
        int hashCode3 = this.c.hashCode();
        ta4.d.AbstractC0224d.a.b.c cVar = this.d;
        if (cVar != null) {
            i = cVar.hashCode();
        }
        return ((((((hashCode2 ^ ((hashCode ^ 1000003) * 1000003)) * 1000003) ^ hashCode3) * 1000003) ^ i) * 1000003) ^ this.e;
    }

    @DexIgnore
    public String toString() {
        return "Exception{type=" + this.f2162a + ", reason=" + this.b + ", frames=" + this.c + ", causedBy=" + this.d + ", overflowCount=" + this.e + "}";
    }
}
