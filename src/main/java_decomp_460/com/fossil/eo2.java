package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo2 extends tn2 implements fo2 {
    @DexIgnore
    public eo2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
    }

    @DexIgnore
    @Override // com.fossil.fo2
    public final void E(cj2 cj2) throws RemoteException {
        Parcel d = d();
        qo2.b(d, cj2);
        e(2, d);
    }
}
