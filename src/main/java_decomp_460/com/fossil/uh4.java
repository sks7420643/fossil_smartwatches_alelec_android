package com.fossil;

import com.google.firebase.messaging.FirebaseMessaging;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class uh4 implements jt3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ FirebaseMessaging f3591a;

    @DexIgnore
    public uh4(FirebaseMessaging firebaseMessaging) {
        this.f3591a = firebaseMessaging;
    }

    @DexIgnore
    @Override // com.fossil.jt3
    public final void onSuccess(Object obj) {
        this.f3591a.c((ki4) obj);
    }
}
