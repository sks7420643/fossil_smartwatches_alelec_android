package com.fossil;

import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn6 implements Factory<pn6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<WorkoutSessionRepository> f3002a;

    @DexIgnore
    public qn6(Provider<WorkoutSessionRepository> provider) {
        this.f3002a = provider;
    }

    @DexIgnore
    public static qn6 a(Provider<WorkoutSessionRepository> provider) {
        return new qn6(provider);
    }

    @DexIgnore
    public static pn6 c(WorkoutSessionRepository workoutSessionRepository) {
        return new pn6(workoutSessionRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public pn6 get() {
        return c(this.f3002a.get());
    }
}
