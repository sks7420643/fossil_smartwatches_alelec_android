package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import com.fossil.m62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wc2<T extends IInterface> extends ec2<T> {
    @DexIgnore
    public /* final */ m62.h<T> E;

    @DexIgnore
    @Override // com.fossil.yb2
    public void P(int i, T t) {
        this.E.m(i, t);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public String p() {
        return this.E.p();
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public T q(IBinder iBinder) {
        return this.E.q(iBinder);
    }

    @DexIgnore
    public m62.h<T> t0() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public String x() {
        return this.E.x();
    }
}
