package com.fossil;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class al5 {
    @DexIgnore
    public static al5 b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ int f287a;

    @DexIgnore
    public al5() {
        WindowManager windowManager = (WindowManager) PortfolioApp.d0.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            this.f287a = point.x;
            return;
        }
        this.f287a = 0;
    }

    @DexIgnore
    public static al5 a() {
        if (b == null) {
            b = new al5();
        }
        return b;
    }

    @DexIgnore
    public int b() {
        return this.f287a;
    }
}
