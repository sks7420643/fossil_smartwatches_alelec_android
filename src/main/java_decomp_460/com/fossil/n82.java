package com.fossil;

import com.fossil.m62;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n82 extends r82 {
    @DexIgnore
    public /* final */ ArrayList<m62.f> c;
    @DexIgnore
    public /* final */ /* synthetic */ h82 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n82(h82 h82, ArrayList<m62.f> arrayList) {
        super(h82, null);
        this.d = h82;
        this.c = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.r82
    public final void a() {
        this.d.f1448a.t.q = this.d.t();
        ArrayList<m62.f> arrayList = this.c;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            m62.f fVar = arrayList.get(i);
            i++;
            fVar.i(this.d.o, this.d.f1448a.t.q);
        }
    }
}
