package com.fossil;

import android.net.ConnectivityManager;
import android.net.NetworkRequest;
import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ String f300a;
    @DexIgnore
    public static /* final */ yk7<ConnectivityManager> b; // = zk7.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ ao5 c; // = new ao5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements gp7<ConnectivityManager> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final ConnectivityManager invoke() {
            if (Build.VERSION.SDK_INT >= 24) {
                return (ConnectivityManager) PortfolioApp.h0.c().getSystemService(ConnectivityManager.class);
            }
            Object systemService = PortfolioApp.h0.c().getSystemService("connectivity");
            if (systemService != null) {
                return (ConnectivityManager) systemService;
            }
            throw new il7("null cannot be cast to non-null type android.net.ConnectivityManager");
        }
    }

    /*
    static {
        String simpleName = ao5.class.getSimpleName();
        pq7.b(simpleName, "NetworkStateChangeManager::class.java.simpleName");
        f300a = simpleName;
    }
    */

    @DexIgnore
    public final void a(zn5 zn5) {
        pq7.c(zn5, "stateChangeCallback");
        FLogger.INSTANCE.getLocal().d(f300a, "registerNetworkCallback");
        ConnectivityManager value = b.getValue();
        if (value != null) {
            value.registerNetworkCallback(new NetworkRequest.Builder().build(), zn5);
        }
    }

    @DexIgnore
    public final void b(zn5 zn5) {
        pq7.c(zn5, "stateChangeCallback");
        FLogger.INSTANCE.getLocal().d(f300a, "unregisterNetworkCallback");
        ConnectivityManager value = b.getValue();
        if (value != null) {
            value.unregisterNetworkCallback(zn5);
        }
    }
}
