package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ns extends fs {
    @DexIgnore
    public ns(hs hsVar, k5 k5Var, int i) {
        super(hsVar, k5Var, i);
    }

    @DexIgnore
    public abstract u5 D();

    @DexIgnore
    @Override // com.fossil.fs
    public final u5 w() {
        return D();
    }

    @DexIgnore
    @Override // com.fossil.fs
    public final void y() {
    }
}
