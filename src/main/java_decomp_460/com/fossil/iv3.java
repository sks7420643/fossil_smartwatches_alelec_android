package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv3 extends sb2 {
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public iv3(DataHolder dataHolder, int i, int i2) {
        super(dataHolder, i);
        this.d = i2;
    }

    @DexIgnore
    public final vu3 e() {
        return new kv3(this.f3230a, this.b, this.d);
    }

    @DexIgnore
    public final int f() {
        return b("event_type");
    }

    @DexIgnore
    public final String toString() {
        String str = f() == 1 ? "changed" : f() == 2 ? "deleted" : "unknown";
        String valueOf = String.valueOf(e());
        StringBuilder sb = new StringBuilder(str.length() + 32 + String.valueOf(valueOf).length());
        sb.append("DataEventRef{ type=");
        sb.append(str);
        sb.append(", dataitem=");
        sb.append(valueOf);
        sb.append(" }");
        return sb.toString();
    }
}
