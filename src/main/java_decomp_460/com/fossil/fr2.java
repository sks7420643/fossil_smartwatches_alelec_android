package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.p72;
import com.fossil.r62;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr2 extends rr2 {
    @DexIgnore
    public /* final */ yq2 G;

    @DexIgnore
    public fr2(Context context, Looper looper, r62.b bVar, r62.c cVar, String str, ac2 ac2) {
        super(context, looper, bVar, cVar, str, ac2);
        this.G = new yq2(context, this.F);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.yb2
    public final void a() {
        synchronized (this.G) {
            if (c()) {
                try {
                    this.G.b();
                    this.G.i();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.a();
        }
    }

    @DexIgnore
    public final Location u0() throws RemoteException {
        return this.G.a();
    }

    @DexIgnore
    public final void v0(p72.a<ga3> aVar, rq2 rq2) throws RemoteException {
        this.G.d(aVar, rq2);
    }

    @DexIgnore
    public final void w0(ir2 ir2, p72<fa3> p72, rq2 rq2) throws RemoteException {
        synchronized (this.G) {
            this.G.e(ir2, p72, rq2);
        }
    }

    @DexIgnore
    public final void x0(LocationRequest locationRequest, p72<ga3> p72, rq2 rq2) throws RemoteException {
        synchronized (this.G) {
            this.G.f(locationRequest, p72, rq2);
        }
    }

    @DexIgnore
    public final void y0(ia3 ia3, j72<ka3> j72, String str) throws RemoteException {
        boolean z = true;
        A();
        rc2.b(ia3 != null, "locationSettingsRequest can't be null nor empty.");
        if (j72 == null) {
            z = false;
        }
        rc2.b(z, "listener can't be null.");
        ((uq2) I()).O2(ia3, new hr2(j72), str);
    }

    @DexIgnore
    public final void z0(p72.a<fa3> aVar, rq2 rq2) throws RemoteException {
        this.G.j(aVar, rq2);
    }
}
