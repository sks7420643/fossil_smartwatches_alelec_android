package com.fossil;

import com.fossil.zk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class w60 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3887a;

    /*
    static {
        int[] iArr = new int[zk1.b.values().length];
        f3887a = iArr;
        iArr[zk1.b.SERIAL_NUMBER.ordinal()] = 1;
        f3887a[zk1.b.HARDWARE_REVISION.ordinal()] = 2;
        f3887a[zk1.b.FIRMWARE_VERSION.ordinal()] = 3;
        f3887a[zk1.b.MODEL_NUMBER.ordinal()] = 4;
        f3887a[zk1.b.HEART_RATE_SERIAL_NUMBER.ordinal()] = 5;
        f3887a[zk1.b.BOOTLOADER_VERSION.ordinal()] = 6;
        f3887a[zk1.b.WATCH_APP_VERSION.ordinal()] = 7;
        f3887a[zk1.b.FONT_VERSION.ordinal()] = 8;
        f3887a[zk1.b.LUTS_VERSION.ordinal()] = 9;
        f3887a[zk1.b.SUPPORTED_FILES_VERSION.ordinal()] = 10;
        f3887a[zk1.b.CURRENT_FILES_VERSION.ordinal()] = 11;
        f3887a[zk1.b.BOND_REQUIREMENT.ordinal()] = 12;
        f3887a[zk1.b.SUPPORTED_DEVICE_CONFIGS.ordinal()] = 13;
        f3887a[zk1.b.DEVICE_SECURITY_VERSION.ordinal()] = 14;
        f3887a[zk1.b.SOCKET_INFO.ordinal()] = 15;
        f3887a[zk1.b.LOCALE.ordinal()] = 16;
        f3887a[zk1.b.LOCALE_VERSION.ordinal()] = 17;
        f3887a[zk1.b.MICRO_APP_SYSTEM_VERSION.ordinal()] = 18;
        f3887a[zk1.b.UNKNOWN.ordinal()] = 19;
    }
    */
}
