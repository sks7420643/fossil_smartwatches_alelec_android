package com.fossil;

import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb7 implements Factory<wb7> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DianaWatchFaceRepository> f4087a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;

    @DexIgnore
    public xb7(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2) {
        this.f4087a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static xb7 a(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2) {
        return new xb7(provider, provider2);
    }

    @DexIgnore
    public static wb7 c(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository) {
        return new wb7(dianaWatchFaceRepository, fileRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public wb7 get() {
        return c(this.f4087a.get(), this.b.get());
    }
}
