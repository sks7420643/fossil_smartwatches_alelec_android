package com.fossil;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy1 {
    @DexIgnore
    public static final String a(String str) {
        pq7.c(str, "$this$addNullCharacter");
        if ((str.length() > 0) && yt7.x0(str) == 0) {
            return str;
        }
        return str + (char) 0;
    }

    @DexIgnore
    public static final String b(String str) {
        pq7.c(str, "$this$toNonLocalizedLowerCase");
        String lowerCase = str.toLowerCase(dx1.b());
        pq7.b(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        return lowerCase;
    }

    @DexIgnore
    public static final String c(String str) {
        pq7.c(str, "$this$trimNullCharacter");
        if (!(str.length() > 0) || yt7.x0(str) != 0) {
            return str;
        }
        return wt7.v0(str, 0);
    }

    @DexIgnore
    public static final String d(String str, int i, Charset charset, CodingErrorAction codingErrorAction) {
        pq7.c(str, "$this$truncate");
        pq7.c(charset, "charset");
        pq7.c(codingErrorAction, "codingErrorAction");
        CharsetDecoder newDecoder = charset.newDecoder();
        pq7.b(newDecoder, "charset.newDecoder()");
        byte[] bytes = str.getBytes(charset);
        pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
        int min = Math.min(i, bytes.length);
        ByteBuffer wrap = ByteBuffer.wrap(bytes, 0, min);
        pq7.b(wrap, "ByteBuffer.wrap(stringByteArray, 0, lengthInByte)");
        CharBuffer allocate = CharBuffer.allocate(min);
        pq7.b(allocate, "CharBuffer.allocate(lengthInByte)");
        newDecoder.onMalformedInput(codingErrorAction);
        newDecoder.decode(wrap, allocate, true);
        newDecoder.flush(allocate);
        char[] array = allocate.array();
        pq7.b(array, "charBuffer.array()");
        return new String(array, 0, allocate.position());
    }

    @DexIgnore
    public static /* synthetic */ String e(String str, int i, Charset charset, CodingErrorAction codingErrorAction, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            charset = et7.f986a;
        }
        if ((i2 & 4) != 0) {
            codingErrorAction = CodingErrorAction.IGNORE;
            pq7.b(codingErrorAction, "CodingErrorAction.IGNORE");
        }
        return d(str, i, charset, codingErrorAction);
    }
}
