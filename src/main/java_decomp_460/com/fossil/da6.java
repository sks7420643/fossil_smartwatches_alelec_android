package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.cr4;
import com.fossil.ka6;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da6 extends pv5 implements cr4.a {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<r35> g;
    @DexIgnore
    public ka6 h;
    @DexIgnore
    public po4 i;
    @DexIgnore
    public cr4 j;
    @DexIgnore
    public Boolean k; // = Boolean.FALSE;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return da6.m;
        }

        @DexIgnore
        public final da6 b(AddressWrapper addressWrapper, ArrayList<String> arrayList, boolean z) {
            da6 da6 = new da6();
            Bundle bundle = new Bundle();
            bundle.putParcelable("KEY_BUNDLE_SETTING_ADDRESS", addressWrapper);
            bundle.putStringArrayList("KEY_BUNDLE_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_BUNDLE_IS_MAP_RESULT", z);
            da6.setArguments(bundle);
            return da6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ r35 c;

        @DexIgnore
        public b(View view, r35 r35) {
            this.b = view;
            this.c = r35;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.z.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                r35 r35 = this.c;
                pq7.b(r35, "binding");
                r35.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = da6.s.a();
                local.d(a2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                pq7.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;

        @DexIgnore
        public c(da6 da6, r35 r35) {
            this.b = da6;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.getItem(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                pq7.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = da6.s.a();
                local.i(a2, "Autocomplete item selected: " + ((Object) fullText));
                ka6 L6 = da6.L6(this.b);
                String spannableString = fullText.toString();
                pq7.b(spannableString, "primaryText.toString()");
                if (spannableString != null) {
                    String obj = wt7.u0(spannableString).toString();
                    String placeId = f.getPlaceId();
                    cr4 cr42 = this.b.j;
                    if (cr42 != null) {
                        L6.m(obj, placeId, cr42.g());
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ r35 b;

        @DexIgnore
        public d(da6 da6, r35 r35) {
            this.b = r35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.b.t;
            pq7.b(imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;

        @DexIgnore
        public e(da6 da6, r35 r35) {
            this.b = da6;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            pq7.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ka6 L6 = da6.L6(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                String obj = wt7.u0(valueOf).toString();
                if (obj != null) {
                    String upperCase = obj.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    L6.q(upperCase);
                    return;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;

        @DexIgnore
        public g(da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.x0(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;

        @DexIgnore
        public h(da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (da6.L6(this.b).k()) {
                s37 s37 = s37.c;
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                pq7.b(childFragmentManager, "childFragmentManager");
                s37.M(childFragmentManager);
            } else if (da6.L6(this.b).l()) {
                this.b.x0(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;
        @DexIgnore
        public /* final */ /* synthetic */ r35 c;

        @DexIgnore
        public i(da6 da6, r35 r35) {
            this.b = da6;
            this.c = r35;
        }

        @DexIgnore
        public final void onClick(View view) {
            ka6.n(da6.L6(this.b), null, null, null, 7, null);
            this.c.q.setText("");
            this.b.P6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ da6 f758a;

        @DexIgnore
        public j(da6 da6) {
            this.f758a = da6;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            da6.L6(this.f758a).p(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ da6 b;

        @DexIgnore
        public k(da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            double d = 0.0d;
            MapPickerActivity.a aVar = MapPickerActivity.A;
            da6 da6 = this.b;
            AddressWrapper g = da6.L6(da6).g();
            double lat = g != null ? g.getLat() : 0.0d;
            AddressWrapper g2 = da6.L6(this.b).g();
            if (g2 != null) {
                d = g2.getLng();
            }
            AddressWrapper g3 = da6.L6(this.b).g();
            if (g3 == null || (str = g3.getAddress()) == null) {
                str = "";
            }
            aVar.a(da6, lat, d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ls0<ka6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ da6 f759a;
        @DexIgnore
        public /* final */ /* synthetic */ r35 b;

        @DexIgnore
        public l(da6 da6, r35 r35) {
            this.f759a = da6;
            this.b = r35;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ka6.a aVar) {
            AddressWrapper.AddressType e;
            PlacesClient d = aVar.d();
            if (d != null) {
                this.f759a.H(d);
            }
            String c = aVar.c();
            if (!(c == null || (e = aVar.e()) == null)) {
                int i = ea6.f904a[e.ordinal()];
                if (i == 1) {
                    this.b.u.setText(um5.c(PortfolioApp.h0.c(), 2131886354));
                } else if (i == 2) {
                    this.b.u.setText(um5.c(PortfolioApp.h0.c(), 2131886356));
                } else if (i == 3) {
                    this.b.u.setText(c);
                }
            }
            String a2 = aVar.a();
            if (a2 != null) {
                this.b.q.setText(a2);
            }
            Boolean b2 = aVar.b();
            if (b2 != null) {
                boolean booleanValue = b2.booleanValue();
                FlexibleSwitchCompat flexibleSwitchCompat = this.b.D;
                pq7.b(flexibleSwitchCompat, "binding.scAvoidTolls");
                flexibleSwitchCompat.setChecked(booleanValue);
            }
            Boolean h = aVar.h();
            if (h != null) {
                boolean booleanValue2 = h.booleanValue();
                FlexibleEditText flexibleEditText = this.b.u;
                pq7.b(flexibleEditText, "binding.fetAddressName");
                flexibleEditText.setEnabled(booleanValue2);
                if (booleanValue2) {
                    this.b.u.requestFocus();
                }
            }
            Boolean i2 = aVar.i();
            if (i2 != null) {
                if (i2.booleanValue()) {
                    this.b.s.d("flexible_button_primary");
                } else {
                    this.b.s.d("flexible_button_disabled");
                }
            }
            Boolean g = aVar.g();
            if (g != null && g.booleanValue()) {
                this.f759a.E4();
            }
            Boolean f = aVar.f();
            if (f == null) {
                return;
            }
            if (f.booleanValue()) {
                this.f759a.b();
            } else {
                this.f759a.a();
            }
        }
    }

    /*
    static {
        String simpleName = da6.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeSettingsDetai\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ka6 L6(da6 da6) {
        ka6 ka6 = da6.h;
        if (ka6 != null) {
            return ka6;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cr4.a
    public void D1(boolean z) {
        g37<r35> g37 = this.g;
        if (g37 != null) {
            r35 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        Boolean bool = this.k;
                        if (bool == null) {
                            pq7.i();
                            throw null;
                        } else if (!bool.booleanValue()) {
                            Q6();
                            return;
                        }
                    }
                }
                P6();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void E4() {
        FLogger.INSTANCE.getLocal().d(m, "showLocationError");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.J(childFragmentManager);
        }
    }

    @DexIgnore
    public final void H(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            pq7.b(requireContext, "requireContext()");
            cr4 cr4 = new cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            g37<r35> g37 = this.g;
            if (g37 != null) {
                r35 a2 = g37.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.j);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    pq7.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0);
                        a2.q.setSelection(u0.length());
                        return;
                    }
                    P6();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6() {
        g37<r35> g37 = this.g;
        if (g37 != null) {
            r35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        g37<r35> g37 = this.g;
        if (g37 != null) {
            r35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                pq7.b(flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.h0.c();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(2131886360, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.v;
                pq7.b(flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            Location location = bundleExtra != null ? (Location) bundleExtra.getParcelable(PlaceFields.LOCATION) : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            StringBuilder sb = new StringBuilder();
            sb.append("addressResult: ");
            sb.append(string);
            sb.append(", lat: ");
            sb.append(location != null ? Double.valueOf(location.getLatitude()) : null);
            sb.append(", lng: ");
            sb.append(location != null ? Double.valueOf(location.getLongitude()) : null);
            local.d(str, sb.toString());
            if (string != null && location != null) {
                ka6 ka6 = this.h;
                if (ka6 != null) {
                    ka6.o(string, location);
                    this.k = Boolean.TRUE;
                    return;
                }
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        Boolean bool = null;
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().M1().a(this);
        po4 po4 = this.i;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(ka6.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            ka6 ka6 = (ka6) a2;
            this.h = ka6;
            if (ka6 != null) {
                Bundle arguments = getArguments();
                AddressWrapper addressWrapper = arguments != null ? (AddressWrapper) arguments.getParcelable("KEY_BUNDLE_SETTING_ADDRESS") : null;
                Bundle arguments2 = getArguments();
                ka6.j(addressWrapper, arguments2 != null ? arguments2.getStringArrayList("KEY_BUNDLE_LIST_ADDRESS") : null);
                Bundle arguments3 = getArguments();
                if (arguments3 != null) {
                    bool = Boolean.valueOf(arguments3.getBoolean("KEY_BUNDLE_IS_MAP_RESULT"));
                }
                this.k = bool;
                return;
            }
            pq7.n("mViewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        r35 r35 = (r35) aq0.f(layoutInflater, 2131558518, viewGroup, false, A6());
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = r35.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(gl0.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new c(this, r35));
        flexibleAutoCompleteTextView.addTextChangedListener(new d(this, r35));
        flexibleAutoCompleteTextView.setOnKeyListener(new e(this, r35));
        r35.u.addTextChangedListener(new f(this));
        r35.w.setOnClickListener(new g(this));
        r35.s.setOnClickListener(new h(this));
        r35.t.setOnClickListener(new i(this, r35));
        r35.D.setOnCheckedChangeListener(new j(this));
        pq7.b(r35, "binding");
        View n = r35.n();
        pq7.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new b(n, r35));
        r35.x.setOnClickListener(new k(this));
        ka6 ka6 = this.h;
        if (ka6 != null) {
            ka6.i().h(getViewLifecycleOwner(), new l(this, r35));
            this.g = new g37<>(this, r35);
            return r35.n();
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ka6 ka6 = this.h;
        if (ka6 != null) {
            ka6.s();
            super.onPause();
            return;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ka6 ka6 = this.h;
        if (ka6 != null) {
            ka6.r();
        } else {
            pq7.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void x0(boolean z) {
        if (z) {
            ka6 ka6 = this.h;
            if (ka6 != null) {
                AddressWrapper g2 = ka6.g();
                if (g2 != null) {
                    Intent intent = new Intent();
                    intent.putExtra("KEY_SELECTED_ADDRESS", g2);
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.setResult(-1, intent);
                    }
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }
}
