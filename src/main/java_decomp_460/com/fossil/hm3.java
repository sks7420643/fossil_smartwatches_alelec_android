package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ gm3 f1494a;

    @DexIgnore
    public hm3(gm3 gm3) {
        rc2.k(gm3);
        this.f1494a = gm3;
    }

    @DexIgnore
    public static boolean b(Context context) {
        ActivityInfo receiverInfo;
        rc2.k(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            return (packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) == null || !receiverInfo.enabled) ? false : true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @DexIgnore
    public final void a(Context context, Intent intent) {
        pm3 a2 = pm3.a(context, null, null);
        kl3 d = a2.d();
        if (intent == null) {
            d.I().a("Receiver called with null intent");
            return;
        }
        a2.b();
        String action = intent.getAction();
        d.N().b("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            d.N().a("Starting wakeful intent.");
            this.f1494a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            d.I().a("Install Referrer Broadcasts are deprecated");
        }
    }
}
