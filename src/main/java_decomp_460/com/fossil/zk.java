package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk extends cq {
    @DexIgnore
    public /* final */ ArrayList<ow> E; // = by1.a(this.C, hm7.c(ow.DEVICE_CONFIG));
    @DexIgnore
    public lp1 F;

    @DexIgnore
    public zk(k5 k5Var, i60 i60) {
        super(k5Var, i60, yp.J, new tu(k5Var));
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        jd0 jd0 = jd0.f0;
        lp1 lp1 = this.F;
        return g80.k(E2, jd0, lp1 != null ? lp1.toJSONObject() : null);
    }

    @DexIgnore
    @Override // com.fossil.cq
    public void G(fs fsVar) {
        this.F = ((tu) fsVar).M;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        lp1 lp1 = this.F;
        return lp1 != null ? lp1 : new lp1(0, WorkoutType.UNKNOWN, WorkoutState.END, 0, 0, 0, 0, 0, 0, 0, 0, false);
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.cq
    public ArrayList<ow> z() {
        return this.E;
    }
}
