package com.fossil;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import com.fossil.a18;
import com.fossil.c71;
import com.fossil.p18;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w81 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ p18 f3895a; // = new p18.a().e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements a18.a {
        @DexIgnore
        public /* final */ /* synthetic */ yk7 b;

        @DexIgnore
        public a(yk7 yk7) {
            this.b = yk7;
        }

        @DexIgnore
        @Override // com.fossil.a18.a
        public final a18 d(v18 v18) {
            return ((a18.a) this.b.getValue()).d(v18);
        }
    }

    @DexIgnore
    public static final void a(Closeable closeable) {
        pq7.c(closeable, "$this$closeQuietly");
        try {
            closeable.close();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
        }
    }

    @DexIgnore
    public static final int b(Bitmap bitmap) {
        pq7.c(bitmap, "$this$getAllocationByteCountCompat");
        if (!bitmap.isRecycled()) {
            try {
                return Build.VERSION.SDK_INT >= 19 ? bitmap.getAllocationByteCount() : bitmap.getRowBytes() * bitmap.getHeight();
            } catch (Exception e) {
                return y81.f4257a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
            }
        } else {
            throw new IllegalStateException(("Cannot obtain size for recycled Bitmap: " + bitmap + " [" + bitmap.getWidth() + " x " + bitmap.getHeight() + "] + " + bitmap.getConfig()).toString());
        }
    }

    @DexIgnore
    public static final int c(Bitmap.Config config) {
        if (config == Bitmap.Config.ALPHA_8) {
            return 1;
        }
        if (config == Bitmap.Config.RGB_565 || config == Bitmap.Config.ARGB_4444) {
            return 2;
        }
        return (Build.VERSION.SDK_INT < 26 || config != Bitmap.Config.RGBA_F16) ? 4 : 8;
    }

    @DexIgnore
    public static final Drawable d(Resources resources, int i, Resources.Theme theme) {
        pq7.c(resources, "$this$getDrawableCompat");
        Drawable a2 = nl0.a(resources, i, theme);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalStateException("Required value was null.".toString());
    }

    @DexIgnore
    public static final String e(q51 q51) {
        pq7.c(q51, "$this$emoji");
        int i = v81.f3731a[q51.ordinal()];
        if (i == 1) {
            return "\ud83e\udde0 ";
        }
        if (i == 2) {
            return "\ud83d\udcbe";
        }
        if (i == 3) {
            return "\u2601\ufe0f ";
        }
        throw new al7();
    }

    @DexIgnore
    public static final String f(Uri uri) {
        pq7.c(uri, "$this$firstPathSegment");
        List<String> pathSegments = uri.getPathSegments();
        pq7.b(pathSegments, "pathSegments");
        return (String) pm7.H(pathSegments);
    }

    @DexIgnore
    public static final String g(MimeTypeMap mimeTypeMap, String str) {
        pq7.c(mimeTypeMap, "$this$getMimeTypeFromUrl");
        if (str == null || vt7.l(str)) {
            return null;
        }
        return mimeTypeMap.getMimeTypeFromExtension(wt7.k0(wt7.l0(wt7.s0(wt7.s0(str, '#', null, 2, null), '?', null, 2, null), '/', null, 2, null), '.', ""));
    }

    @DexIgnore
    public static final int h(Configuration configuration) {
        pq7.c(configuration, "$this$nightMode");
        return configuration.uiMode & 48;
    }

    @DexIgnore
    public static final k71 i(View view) {
        pq7.c(view, "$this$requestManager");
        Object tag = view.getTag(f51.coil_request_manager);
        if (!(tag instanceof k71)) {
            tag = null;
        }
        k71 k71 = (k71) tag;
        if (k71 != null) {
            return k71;
        }
        k71 k712 = new k71();
        view.addOnAttachStateChangeListener(k712);
        view.setTag(f51.coil_request_manager, k712);
        return k712;
    }

    @DexIgnore
    public static final e81 j(ImageView imageView) {
        int i;
        pq7.c(imageView, "$this$scale");
        ImageView.ScaleType scaleType = imageView.getScaleType();
        return (scaleType != null && ((i = v81.b[scaleType.ordinal()]) == 1 || i == 2 || i == 3 || i == 4)) ? e81.FIT : e81.FILL;
    }

    @DexIgnore
    public static final c71.b k(c71 c71, String str) {
        pq7.c(c71, "$this$getValue");
        if (str != null) {
            return c71.b(str);
        }
        return null;
    }

    @DexIgnore
    public static final boolean l(x71 x71) {
        pq7.c(x71, "$this$isDiskPreload");
        return (x71 instanceof t71) && x71.u() == null && !x71.n().getWriteEnabled();
    }

    @DexIgnore
    public static final boolean m(Bitmap.Config config) {
        pq7.c(config, "$this$isHardware");
        return Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE;
    }

    @DexIgnore
    public static final boolean n() {
        return pq7.a(Looper.myLooper(), Looper.getMainLooper());
    }

    @DexIgnore
    public static final boolean o(Drawable drawable) {
        pq7.c(drawable, "$this$isVector");
        return (drawable instanceof a01) || (Build.VERSION.SDK_INT > 21 && (drawable instanceof VectorDrawable));
    }

    @DexIgnore
    public static final a18.a p(gp7<? extends a18.a> gp7) {
        pq7.c(gp7, "initializer");
        return new a(zk7.a(gp7));
    }

    @DexIgnore
    public static final Bitmap.Config q(Bitmap.Config config) {
        return (config == null || m(config)) ? Bitmap.Config.ARGB_8888 : config;
    }

    @DexIgnore
    public static final w71 r(w71 w71) {
        return w71 != null ? w71 : w71.c;
    }

    @DexIgnore
    public static final p18 s(p18 p18) {
        return p18 != null ? p18 : f3895a;
    }

    @DexIgnore
    public static final void t(c71 c71, String str, Drawable drawable, boolean z) {
        Bitmap bitmap = null;
        pq7.c(c71, "$this$putValue");
        pq7.c(drawable, "value");
        if (str != null) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) (!(drawable instanceof BitmapDrawable) ? null : drawable);
            if (bitmapDrawable != null) {
                bitmap = bitmapDrawable.getBitmap();
            }
            if (bitmap != null) {
                c71.c(str, bitmap, z);
            }
        }
    }
}
