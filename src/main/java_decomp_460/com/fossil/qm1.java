package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm1 extends ym1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ int d; // = hy1.a(gr7.f1349a);
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qm1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final qm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new qm1(hy1.n(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0)));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 2"));
        }

        @DexIgnore
        public qm1 b(Parcel parcel) {
            return new qm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qm1 createFromParcel(Parcel parcel) {
            return new qm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qm1[] newArray(int i) {
            return new qm1[i];
        }
    }

    @DexIgnore
    public qm1(int i) throws IllegalArgumentException {
        super(zm1.DAILY_ACTIVE_MINUTE_GOAL);
        this.c = i;
        d();
    }

    @DexIgnore
    public /* synthetic */ qm1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readInt();
        d();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.c).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public Integer c() {
        return Integer.valueOf(this.c);
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        int i = d;
        int i2 = this.c;
        if (!(i2 >= 0 && i >= i2)) {
            StringBuilder e = e.e("minute(");
            e.append(this.c);
            e.append(") is out of range ");
            e.append("[0, ");
            throw new IllegalArgumentException(e.b(e, d, "]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(qm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((qm1) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
    }

    @DexIgnore
    public final int getMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
