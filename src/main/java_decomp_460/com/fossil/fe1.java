package com.fossil;

import android.util.Log;
import com.fossil.be1;
import com.fossil.ya1;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fe1 implements be1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ke1 f1108a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ de1 d; // = new de1();
    @DexIgnore
    public ya1 e;

    @DexIgnore
    @Deprecated
    public fe1(File file, long j) {
        this.b = file;
        this.c = j;
        this.f1108a = new ke1();
    }

    @DexIgnore
    public static be1 c(File file, long j) {
        return new fe1(file, j);
    }

    @DexIgnore
    @Override // com.fossil.be1
    public void a(mb1 mb1, be1.b bVar) {
        String b2 = this.f1108a.b(mb1);
        this.d.a(b2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + b2 + " for for Key: " + mb1);
            }
            try {
                ya1 d2 = d();
                if (d2.L(b2) == null) {
                    ya1.c D = d2.D(b2);
                    if (D != null) {
                        try {
                            if (bVar.a(D.f(0))) {
                                D.e();
                            }
                            this.d.b(b2);
                        } finally {
                            D.b();
                        }
                    } else {
                        throw new IllegalStateException("Had two simultaneous puts for: " + b2);
                    }
                }
            } catch (IOException e2) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e2);
                }
            }
        } finally {
            this.d.b(b2);
        }
    }

    @DexIgnore
    @Override // com.fossil.be1
    public File b(mb1 mb1) {
        String b2 = this.f1108a.b(mb1);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + b2 + " for for Key: " + mb1);
        }
        try {
            ya1.e L = d().L(b2);
            if (L != null) {
                return L.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    @DexIgnore
    public final ya1 d() throws IOException {
        ya1 ya1;
        synchronized (this) {
            if (this.e == null) {
                this.e = ya1.P(this.b, 1, 1, this.c);
            }
            ya1 = this.e;
        }
        return ya1;
    }
}
