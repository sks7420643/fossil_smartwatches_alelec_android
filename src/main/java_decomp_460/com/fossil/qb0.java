package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb0 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public int f;
    @DexIgnore
    public /* final */ /* synthetic */ rb0 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qb0(qn7 qn7, rb0 rb0) {
        super(2, qn7);
        this.g = rb0;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        qb0 qb0 = new qb0(qn7, this.g);
        qb0.b = (iv7) obj;
        return qb0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        qb0 qb0 = new qb0(qn7, this.g);
        qb0.b = iv7;
        return qb0.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r8) {
        /*
        // Method dump skipped, instructions count: 202
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qb0.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
