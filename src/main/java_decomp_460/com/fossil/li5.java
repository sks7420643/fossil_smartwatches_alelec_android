package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class li5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2201a;

    /*
    static {
        int[] iArr = new int[mi5.values().length];
        f2201a = iArr;
        iArr[mi5.RUNNING.ordinal()] = 1;
        f2201a[mi5.CYCLING.ordinal()] = 2;
        f2201a[mi5.TREADMILL.ordinal()] = 3;
        f2201a[mi5.ELLIPTICAL.ordinal()] = 4;
        f2201a[mi5.WEIGHTS.ordinal()] = 5;
        f2201a[mi5.WORKOUT.ordinal()] = 6;
        f2201a[mi5.WALKING.ordinal()] = 7;
        f2201a[mi5.ROWING.ordinal()] = 8;
        f2201a[mi5.HIKING.ordinal()] = 9;
        f2201a[mi5.YOGA.ordinal()] = 10;
        f2201a[mi5.SWIMMING.ordinal()] = 11;
        f2201a[mi5.AEROBIC.ordinal()] = 12;
        f2201a[mi5.SPINNING.ordinal()] = 13;
    }
    */
}
