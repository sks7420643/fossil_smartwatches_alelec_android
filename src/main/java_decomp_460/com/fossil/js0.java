package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class js0<T> extends MutableLiveData<T> {
    @DexIgnore
    public fi0<LiveData<?>, a<?>> k; // = new fi0<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<V> implements ls0<V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ LiveData<V> f1798a;
        @DexIgnore
        public /* final */ ls0<? super V> b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public a(LiveData<V> liveData, ls0<? super V> ls0) {
            this.f1798a = liveData;
            this.b = ls0;
        }

        @DexIgnore
        public void a() {
            this.f1798a.i(this);
        }

        @DexIgnore
        public void b() {
            this.f1798a.m(this);
        }

        @DexIgnore
        @Override // com.fossil.ls0
        public void onChanged(V v) {
            if (this.c != this.f1798a.f()) {
                this.c = this.f1798a.f();
                this.b.onChanged(v);
            }
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void j() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().getValue().a();
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void k() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().getValue().b();
        }
    }

    @DexIgnore
    public <S> void p(LiveData<S> liveData, ls0<? super S> ls0) {
        a<?> aVar = new a<>(liveData, ls0);
        a<?> f = this.k.f(liveData, aVar);
        if (f != null && f.b != ls0) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        } else if (f == null && g()) {
            aVar.a();
        }
    }

    @DexIgnore
    public <S> void q(LiveData<S> liveData) {
        a<?> g = this.k.g(liveData);
        if (g != null) {
            g.b();
        }
    }
}
