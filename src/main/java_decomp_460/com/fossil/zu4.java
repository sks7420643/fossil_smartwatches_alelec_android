package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.fossil.f57;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<ot4> f4538a; // = new ArrayList();
    @DexIgnore
    public /* final */ f57.b b;
    @DexIgnore
    public /* final */ uy4 c;
    @DexIgnore
    public /* final */ a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void G5(ot4 ot4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ zf5 f4539a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ zu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;
            @DexIgnore
            public /* final */ /* synthetic */ ot4 c;

            @DexIgnore
            public a(b bVar, ot4 ot4, f57.b bVar2, uy4 uy4) {
                this.b = bVar;
                this.c = ot4;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.getAdapterPosition() != -1) {
                    ot4 ot4 = this.c;
                    if (view != null) {
                        ot4.g(((FlexibleCheckBox) view).isChecked());
                        this.b.c.d.G5(this.c);
                        return;
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleCheckBox");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zu4 zu4, zf5 zf5, View view) {
            super(view);
            pq7.c(zf5, "binding");
            pq7.c(view, "root");
            this.c = zu4;
            this.f4539a = zf5;
            this.b = view;
        }

        @DexIgnore
        public void a(ot4 ot4, f57.b bVar, uy4 uy4) {
            pq7.c(ot4, "friend");
            pq7.c(bVar, "drawableBuilder");
            pq7.c(uy4, "colorGenerator");
            zf5 zf5 = this.f4539a;
            String b2 = hz4.f1561a.b(ot4.a(), ot4.c(), ot4.e());
            FlexibleTextView flexibleTextView = zf5.u;
            pq7.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(b2);
            FlexibleCheckBox flexibleCheckBox = zf5.q;
            pq7.b(flexibleCheckBox, "cbInvite");
            flexibleCheckBox.setChecked(ot4.f());
            ImageView imageView = zf5.s;
            pq7.b(imageView, "ivAvatar");
            ty4.b(imageView, ot4.d(), b2, bVar, uy4);
            zf5.q.setOnClickListener(new a(this, ot4, bVar, uy4));
        }
    }

    @DexIgnore
    public zu4(a aVar) {
        pq7.c(aVar, "listener");
        this.d = aVar;
        f57.b f = f57.a().f();
        pq7.b(f, "TextDrawable.builder().round()");
        this.b = f;
        this.c = uy4.d.b();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f4538a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return i;
    }

    @DexIgnore
    /* renamed from: h */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        bVar.a(this.f4538a.get(i), this.b, this.c);
    }

    @DexIgnore
    /* renamed from: i */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        zf5 z = zf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemSuggestingFriendLayo\u2026tInflater, parent, false)");
        View n = z.n();
        pq7.b(n, "itemFriendInviteListBinding.root");
        return new b(this, z, n);
    }

    @DexIgnore
    public final void j(List<ot4> list) {
        pq7.c(list, NativeProtocol.AUDIENCE_FRIENDS);
        this.f4538a.clear();
        this.f4538a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void setHasStableIds(boolean z) {
        super.setHasStableIds(true);
    }
}
