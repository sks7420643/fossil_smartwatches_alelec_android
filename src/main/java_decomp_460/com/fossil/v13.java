package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v13 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ v13 f3696a; // = new x13();
    @DexIgnore
    public static /* final */ v13 b; // = new a23();

    @DexIgnore
    public v13() {
    }

    @DexIgnore
    public static v13 a() {
        return f3696a;
    }

    @DexIgnore
    public static v13 c() {
        return b;
    }

    @DexIgnore
    public abstract <L> void b(Object obj, Object obj2, long j);

    @DexIgnore
    public abstract void d(Object obj, long j);
}
