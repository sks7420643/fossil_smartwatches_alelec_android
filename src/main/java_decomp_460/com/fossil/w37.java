package com.fossil;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.FilterQueryProvider;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w37 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ w37 f3877a; // = new w37();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements FilterQueryProvider {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a f3878a; // = new a();

        @DexIgnore
        public final Cursor runQuery(CharSequence charSequence) {
            String str;
            pq7.b(charSequence, "constraint");
            if (charSequence.length() == 0) {
                str = "has_phone_number!=0 AND mimetype=?";
            } else {
                str = "(display_name LIKE '%" + charSequence + "%' OR display_name LIKE 'N%" + charSequence + "%') AND has_phone_number!=0 AND mimetype=?";
            }
            return PortfolioApp.h0.c().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, str, new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
        }
    }

    @DexIgnore
    public final FilterQueryProvider a() {
        return a.f3878a;
    }
}
