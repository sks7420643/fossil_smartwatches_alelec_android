package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.m62;
import com.fossil.r62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn2 extends ip2<ho2> {
    @DexIgnore
    public static /* final */ ep2 E; // = ep2.FIT_RECORDING;
    @DexIgnore
    public static /* final */ m62.g<fn2> F; // = new m62.g<>();
    @DexIgnore
    public static /* final */ m62<m62.d.C0151d> G; // = new m62<>("Fitness.RECORDING_API", new hn2(), F);

    /*
    static {
        new m62("Fitness.RECORDING_CLIENT", new jn2(), F);
    }
    */

    @DexIgnore
    public fn2(Context context, Looper looper, ac2 ac2, r62.b bVar, r62.c cVar) {
        super(context, looper, E, bVar, cVar, ac2);
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String p() {
        return "com.google.android.gms.fitness.internal.IGoogleFitRecordingApi";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitRecordingApi");
        return queryLocalInterface instanceof ho2 ? (ho2) queryLocalInterface : new go2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public final int s() {
        return h62.f1430a;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final String x() {
        return "com.google.android.gms.fitness.RecordingApi";
    }
}
