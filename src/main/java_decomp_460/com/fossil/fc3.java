package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fc3 extends IInterface {
    @DexIgnore
    boolean A0() throws RemoteException;

    @DexIgnore
    boolean D0() throws RemoteException;

    @DexIgnore
    boolean J2() throws RemoteException;

    @DexIgnore
    boolean U() throws RemoteException;

    @DexIgnore
    boolean e0() throws RemoteException;

    @DexIgnore
    boolean f0() throws RemoteException;

    @DexIgnore
    boolean l1() throws RemoteException;

    @DexIgnore
    boolean r2() throws RemoteException;

    @DexIgnore
    void setCompassEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMapToolbarEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMyLocationButtonEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setRotateGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setScrollGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setTiltGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setZoomControlsEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setZoomGesturesEnabled(boolean z) throws RemoteException;
}
