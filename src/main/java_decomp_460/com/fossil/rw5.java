package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleCheckBox;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<WeatherLocationWrapper> f3175a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public TextView f3176a;
        @DexIgnore
        public FlexibleCheckBox b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ rw5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rw5$a$a")
        /* renamed from: com.fossil.rw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0210a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0210a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar;
                if (this.b.d.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && !((WeatherLocationWrapper) this.b.d.f3175a.get(this.b.getAdapterPosition())).isUseCurrentLocation() && (bVar = this.b.d.b) != null) {
                    bVar.a(this.b.getAdapterPosition() - 1, !((WeatherLocationWrapper) this.b.d.f3175a.get(this.b.getAdapterPosition())).isEnableLocation());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(rw5 rw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.d = rw5;
            View findViewById = view.findViewById(2131362056);
            if (findViewById != null) {
                this.c = findViewById;
                View findViewById2 = view.findViewById(2131362005);
                if (findViewById2 != null) {
                    this.b = (FlexibleCheckBox) findViewById2;
                    View findViewById3 = view.findViewById(2131363408);
                    if (findViewById3 != null) {
                        this.f3176a = (TextView) findViewById3;
                        View view2 = this.c;
                        if (view2 != null) {
                            view2.setOnClickListener(new View$OnClickListenerC0210a(this));
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final void a(WeatherLocationWrapper weatherLocationWrapper) {
            FlexibleCheckBox flexibleCheckBox;
            pq7.c(weatherLocationWrapper, "locationWrapper");
            TextView textView = this.f3176a;
            if (textView != null) {
                textView.setText(weatherLocationWrapper.getFullName());
            }
            FlexibleCheckBox flexibleCheckBox2 = this.b;
            if (flexibleCheckBox2 != null) {
                flexibleCheckBox2.setChecked(weatherLocationWrapper.isEnableLocation());
            }
            FlexibleCheckBox flexibleCheckBox3 = this.b;
            if (flexibleCheckBox3 != null) {
                flexibleCheckBox3.setEnabled(!weatherLocationWrapper.isUseCurrentLocation());
            }
            TextView textView2 = this.f3176a;
            if (pq7.a(textView2 != null ? textView2.getText() : null, um5.c(PortfolioApp.h0.c(), 2131886395)) && (flexibleCheckBox = this.b) != null) {
                flexibleCheckBox.setEnableColor("nonBrandDisableCalendarDay");
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f3175a.size();
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            aVar.a(this.f3175a.get(i));
        }
    }

    @DexIgnore
    /* renamed from: j */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558727, viewGroup, false);
        pq7.b(inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void k(List<WeatherLocationWrapper> list) {
        pq7.c(list, "locationSearchList");
        this.f3175a.clear();
        String c = um5.c(PortfolioApp.h0.c(), 2131886395);
        pq7.b(c, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886395);
        pq7.b(c2, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        this.f3175a.add(0, new WeatherLocationWrapper("", 0.0d, 0.0d, c, c2, true, true, 6, null));
        this.f3175a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void l(b bVar) {
        pq7.c(bVar, "listener");
        this.b = bVar;
    }
}
