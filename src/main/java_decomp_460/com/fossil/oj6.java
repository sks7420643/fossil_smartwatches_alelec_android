package com.fossil;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj6 implements Factory<nj6> {
    @DexIgnore
    public static nj6 a(lj6 lj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, no4 no4) {
        return new nj6(lj6, sleepSummariesRepository, sleepSessionsRepository, fitnessDataRepository, userRepository, no4);
    }
}
