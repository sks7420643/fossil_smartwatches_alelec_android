package com.fossil;

import com.portfolio.platform.view.chart.WLHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class t57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f3368a;

    /*
    static {
        int[] iArr = new int[WLHeartRateChart.a.values().length];
        f3368a = iArr;
        iArr[WLHeartRateChart.a.RESTING.ordinal()] = 1;
        f3368a[WLHeartRateChart.a.PEAK.ordinal()] = 2;
    }
    */
}
