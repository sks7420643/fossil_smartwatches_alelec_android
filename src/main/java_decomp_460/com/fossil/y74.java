package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y74 implements w74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ z74 f4253a; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements z74 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File a() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File b() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File c() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File d() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File e() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File f() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.z74
        public File g() {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w74
    public boolean a(String str) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.w74
    public z74 b(String str) {
        return f4253a;
    }

    @DexIgnore
    @Override // com.fossil.w74
    public void c(String str, int i, String str2, int i2, long j, long j2, boolean z, int i3, String str3, String str4) {
    }

    @DexIgnore
    @Override // com.fossil.w74
    public void d(String str, String str2, long j) {
    }

    @DexIgnore
    @Override // com.fossil.w74
    public boolean e(String str) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.w74
    public void f(String str, String str2, String str3, String str4, String str5, int i, String str6) {
    }

    @DexIgnore
    @Override // com.fossil.w74
    public void g(String str, String str2, String str3, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.w74
    public boolean h(String str) {
        return true;
    }
}
