package com.fossil;

import android.content.Intent;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu6 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ d d; // = new d();
    @DexIgnore
    public List<WorkoutSetting> e;
    @DexIgnore
    public /* final */ WorkoutSettingRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<WorkoutSetting> f1211a;

        @DexIgnore
        public a(List<WorkoutSetting> list) {
            pq7.c(list, "workoutSettings");
            this.f1211a = list;
        }

        @DexIgnore
        public final List<WorkoutSetting> a() {
            return this.f1211a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1212a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, int i2, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "errorCodes");
            this.f1212a = i2;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f1212a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$SetWorkoutSettingReceiver$receive$1", f = "SetWorkoutSettingUseCase.kt", l = {35, 37}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.fu6$d$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$SetWorkoutSettingReceiver$receive$1$2", f = "SetWorkoutSettingUseCase.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.fu6$d$a$a  reason: collision with other inner class name */
            public static final class C0090a extends ko7 implements vp7<iv7, qn7<? super xw7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0090a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0090a aVar = new C0090a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super xw7> qn7) {
                    return ((C0090a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return fu6.this.j(new c());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r1 = com.fossil.yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0041
                    if (r0 == r4) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r6.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r7)
                L_0x002b:
                    com.fossil.jx7 r2 = com.fossil.bw7.c()
                    com.fossil.fu6$d$a$a r3 = new com.fossil.fu6$d$a$a
                    r4 = 0
                    r3.<init>(r6, r4)
                    r6.L$0 = r0
                    r6.label = r5
                    java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r6)
                    if (r0 != r1) goto L_0x0015
                    r0 = r1
                    goto L_0x0017
                L_0x0041:
                    com.fossil.el7.b(r7)
                    com.fossil.iv7 r0 = r6.p$
                    com.fossil.fu6$d r2 = r6.this$0
                    com.fossil.fu6 r2 = com.fossil.fu6.this
                    java.util.List r2 = com.fossil.fu6.n(r2)
                    if (r2 == 0) goto L_0x002b
                    com.fossil.fu6$d r3 = r6.this$0
                    com.fossil.fu6 r3 = com.fossil.fu6.this
                    com.portfolio.platform.data.source.WorkoutSettingRepository r3 = com.fossil.fu6.o(r3)
                    r6.L$0 = r0
                    r6.L$1 = r2
                    r6.label = r4
                    java.lang.Object r2 = r3.upsertWorkoutSettingList(r2, r6)
                    if (r2 != r1) goto L_0x002b
                    r0 = r1
                    goto L_0x0017
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.fu6.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "onReceive()");
            if (communicateMode != CommunicateMode.SET_WORKOUT_DETECTION) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "onReceive() - success");
                xw7 unused = gu7.d(fu6.this.g(), null, null, new a(this, null), 3, null);
                return;
            }
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetWorkoutSettingUseCase", "onReceive() - failed, errorCode = " + intExtra);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra);
            }
            fu6.this.i(new b(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
        }
    }

    @DexIgnore
    public fu6(WorkoutSettingRepository workoutSettingRepository) {
        pq7.c(workoutSettingRepository, "mWorkoutSettingRepository");
        this.f = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetWorkoutSettingUseCase";
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "registerBroadcastReceiver()");
        wq5.d.e(this.d, CommunicateMode.SET_WORKOUT_DETECTION);
        wq5.d.g(CommunicateMode.SET_WORKOUT_DETECTION);
    }

    @DexIgnore
    /* renamed from: q */
    public Object k(a aVar, qn7<Object> qn7) {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "executeUseCase()");
        if (aVar != null) {
            this.e = aVar.a();
            ao7.f(PortfolioApp.h0.c().O1(PortfolioApp.h0.c().J(), r47.a(aVar.a())));
        }
        return new Object();
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "unregisterBroadcastReceiver()");
        wq5.d.j(this.d, CommunicateMode.SET_WORKOUT_DETECTION);
    }
}
