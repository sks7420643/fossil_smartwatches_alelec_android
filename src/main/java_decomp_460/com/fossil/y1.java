package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y1 extends c2 {
    @DexIgnore
    public static /* final */ w1 CREATOR; // = new w1(null);
    @DexIgnore
    public /* final */ bo1 e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public y1(byte b, int i, bo1 bo1) {
        super(lt.APP_NOTIFICATION_EVENT, b, false, 4);
        this.e = bo1;
        this.f = i;
    }

    @DexIgnore
    public /* synthetic */ y1(Parcel parcel, kq7 kq7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(bo1.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (bo1) readParcelable;
            this.f = parcel.readInt();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.v0, this.e.toJSONObject()), jd0.d4, Integer.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
