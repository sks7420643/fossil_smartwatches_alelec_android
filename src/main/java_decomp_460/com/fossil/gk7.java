package com.fossil;

import dagger.internal.Factory;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gk7<K, V, V2> implements Factory<Map<K, V2>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Map<K, Provider<V>> f1321a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<K, V, V2> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ LinkedHashMap<K, Provider<V>> f1322a;

        @DexIgnore
        public a(int i) {
            this.f1322a = hk7.b(i);
        }

        @DexIgnore
        public a<K, V, V2> a(K k, Provider<V> provider) {
            LinkedHashMap<K, Provider<V>> linkedHashMap = this.f1322a;
            lk7.c(k, "key");
            lk7.c(provider, "provider");
            linkedHashMap.put(k, provider);
            return this;
        }
    }

    @DexIgnore
    public gk7(Map<K, Provider<V>> map) {
        this.f1321a = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    public final Map<K, Provider<V>> a() {
        return this.f1321a;
    }
}
