package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n51<K, V> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ a<K, V> f2468a; // = new a<>(null, 1, null);
    @DexIgnore
    public /* final */ HashMap<K, a<K, V>> b; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public List<V> f2469a;
        @DexIgnore
        public a<K, V> b;
        @DexIgnore
        public a<K, V> c;
        @DexIgnore
        public /* final */ K d;

        @DexIgnore
        public a() {
            this(null, 1, null);
        }

        @DexIgnore
        public a(K k) {
            this.d = k;
            this.b = this;
            this.c = this;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Object obj, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : obj);
        }

        @DexIgnore
        public final void a(V v) {
            List<V> list = this.f2469a;
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(v);
            this.f2469a = list;
        }

        @DexIgnore
        public final K b() {
            return this.d;
        }

        @DexIgnore
        public final a<K, V> c() {
            return this.c;
        }

        @DexIgnore
        public final a<K, V> d() {
            return this.b;
        }

        @DexIgnore
        public final V e() {
            List<V> list = this.f2469a;
            if (list == null || !(!list.isEmpty())) {
                return null;
            }
            return list.remove(hm7.g(list));
        }

        @DexIgnore
        public final void f(a<K, V> aVar) {
            pq7.c(aVar, "<set-?>");
            this.c = aVar;
        }

        @DexIgnore
        public final void g(a<K, V> aVar) {
            pq7.c(aVar, "<set-?>");
            this.b = aVar;
        }

        @DexIgnore
        public final int h() {
            List<V> list = this.f2469a;
            if (list != null) {
                return list.size();
            }
            return 0;
        }
    }

    @DexIgnore
    public final V a(K k) {
        HashMap<K, a<K, V>> hashMap = this.b;
        a<K, V> aVar = hashMap.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            hashMap.put(k, aVar);
        }
        a<K, V> aVar2 = aVar;
        b(aVar2);
        return aVar2.e();
    }

    @DexIgnore
    public final void b(a<K, V> aVar) {
        d(aVar);
        aVar.g(this.f2468a);
        aVar.f(this.f2468a.c());
        g(aVar);
    }

    @DexIgnore
    public final void c(a<K, V> aVar) {
        d(aVar);
        aVar.g(this.f2468a.d());
        aVar.f(this.f2468a);
        g(aVar);
    }

    @DexIgnore
    public final <K, V> void d(a<K, V> aVar) {
        aVar.d().f(aVar.c());
        aVar.c().g(aVar.d());
    }

    @DexIgnore
    public final V e() {
        for (a<K, V> d = this.f2468a.d(); !pq7.a(d, this.f2468a); d = d.d()) {
            V e = d.e();
            if (e != null) {
                return e;
            }
            d(d);
            HashMap<K, a<K, V>> hashMap = this.b;
            K b2 = d.b();
            if (hashMap != null) {
                ir7.c(hashMap).remove(b2);
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>");
            }
        }
        return null;
    }

    @DexIgnore
    public final void f(K k, V v) {
        HashMap<K, a<K, V>> hashMap = this.b;
        a<K, V> aVar = hashMap.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            c(aVar);
            hashMap.put(k, aVar);
        }
        aVar.a(v);
    }

    @DexIgnore
    public final <K, V> void g(a<K, V> aVar) {
        aVar.c().g(aVar);
        aVar.d().f(aVar);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GroupedLinkedMap( ");
        a<K, V> c = this.f2468a.c();
        boolean z = false;
        while (!pq7.a(c, this.f2468a)) {
            sb.append('{');
            sb.append((Object) c.b());
            sb.append(':');
            sb.append(c.h());
            sb.append("}, ");
            c = c.c();
            z = true;
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        String sb2 = sb.toString();
        pq7.b(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
