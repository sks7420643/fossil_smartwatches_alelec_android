package com.fossil;

import android.text.TextUtils;
import com.fossil.v18;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uq5 implements y08 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public AtomicInteger b; // = new AtomicInteger(0);
    @DexIgnore
    public Auth c;
    @DexIgnore
    public /* final */ AuthApiGuestService d;
    @DexIgnore
    public /* final */ on5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Auth $auth$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Response $response$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uq5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qn7 qn7, uq5 uq5, MFUser mFUser, Auth auth, Response response) {
            super(2, qn7);
            this.this$0 = uq5;
            this.$user$inlined = mFUser;
            this.$auth$inlined = auth;
            this.$response$inlined = response;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(qn7, this.this$0, this.$user$inlined, this.$auth$inlined, this.$response$inlined);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            UserDao userDao;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                UserDatabase B = bn5.j.B();
                if (!(B == null || (userDao = B.userDao()) == null)) {
                    userDao.updateUser(this.$user$inlined);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = uq5.class.getSimpleName();
        pq7.b(simpleName, "TokenAuthenticator::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public uq5(AuthApiGuestService authApiGuestService, on5 on5) {
        pq7.c(authApiGuestService, "mAuthApiGuestService");
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = authApiGuestService;
        this.e = on5;
    }

    @DexIgnore
    public final q88<Auth> a(MFUser mFUser, Response response, boolean z) {
        q88<Auth> a2;
        Exception e2;
        q88<Auth> q88 = null;
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(f, "getAuth needProcess=" + z + " mCount=" + this.b);
            if (z) {
                if (TextUtils.isEmpty(mFUser.getAuth().getRefreshToken())) {
                    gj4 gj4 = new gj4();
                    FLogger.INSTANCE.getLocal().d(f, "Exchange Legacy Token");
                    gj4.n(Constants.PROFILE_KEY_ACCESS_TOKEN, mFUser.getAuth().getAccessToken());
                    gj4.n("clientId", dk5.g.a(mFUser.getUserId()));
                    this.c = this.d.tokenExchangeLegacy(gj4).a().a();
                } else {
                    w18 a3 = response.a();
                    if (a3 != null) {
                        StringBuilder sb = new StringBuilder();
                        String readLine = new BufferedReader(new InputStreamReader(a3.byteStream())).readLine();
                        int length = readLine.length();
                        for (int i = 0; i < length; i++) {
                            sb.append(readLine.charAt(i));
                        }
                        String sb2 = sb.toString();
                        pq7.b(sb2, "sb.toString()");
                        try {
                            JsonElement c2 = new ij4().c(sb2);
                            gj4 d2 = c2 != null ? c2.d() : null;
                            if (d2 != null && d2.s("_error")) {
                                JsonElement p = d2.p("_error");
                                pq7.b(p, "parseResult.get(\"_error\")");
                                int b2 = p.b();
                                if (b2 != 401005) {
                                    if (b2 == 401011) {
                                        FLogger.INSTANCE.getLocal().d(f, "Refresh Token");
                                        gj4 gj42 = new gj4();
                                        gj42.n(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
                                        try {
                                            a2 = this.d.tokenRefresh(gj42).a();
                                            try {
                                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                String str = f;
                                                StringBuilder sb3 = new StringBuilder();
                                                sb3.append("Refresh Token error=");
                                                sb3.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                                                sb3.append(" body=");
                                                sb3.append(a2 != null ? a2.d() : null);
                                                local.d(str, sb3.toString());
                                                this.c = a2 != null ? a2.a() : null;
                                            } catch (Exception e3) {
                                                e = e3;
                                                FLogger.INSTANCE.getLocal().d(f, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e.getMessage());
                                                e.printStackTrace();
                                                return q88;
                                            }
                                        } catch (Exception e4) {
                                            e = e4;
                                            FLogger.INSTANCE.getLocal().d(f, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e.getMessage());
                                            e.printStackTrace();
                                            return q88;
                                        }
                                    } else if (b2 != 401026) {
                                        FLogger.INSTANCE.getLocal().d(f, "unauthorized: error = " + sb2);
                                        FLogger.INSTANCE.getLocal().d(f, "Refresh Token");
                                        gj4 gj43 = new gj4();
                                        gj43.n(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
                                        try {
                                            a2 = this.d.tokenRefresh(gj43).a();
                                            try {
                                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                                String str2 = f;
                                                StringBuilder sb4 = new StringBuilder();
                                                sb4.append("Refresh Token error=");
                                                sb4.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                                                sb4.append(" body=");
                                                sb4.append(a2 != null ? a2.d() : null);
                                                local2.d(str2, sb4.toString());
                                                this.c = a2 != null ? a2.a() : null;
                                            } catch (Exception e5) {
                                                e2 = e5;
                                                try {
                                                    FLogger.INSTANCE.getLocal().d(qq5.g.a(), "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e2.getMessage());
                                                    e2.printStackTrace();
                                                } catch (Exception e6) {
                                                    q88 = a2;
                                                    FLogger.INSTANCE.getLocal().d(f, "Crash when trying to parse body " + sb2);
                                                    return q88;
                                                }
                                                return q88;
                                            }
                                        } catch (Exception e7) {
                                            e2 = e7;
                                            a2 = null;
                                            FLogger.INSTANCE.getLocal().d(qq5.g.a(), "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e2.getMessage());
                                            e2.printStackTrace();
                                            return q88;
                                        }
                                    } else {
                                        FLogger.INSTANCE.getLocal().d(f, "Exchange Legacy Token failed");
                                    }
                                    q88 = a2;
                                } else {
                                    FLogger.INSTANCE.getLocal().d(f, "Token is in black list");
                                }
                            }
                        } catch (Exception e8) {
                            FLogger.INSTANCE.getLocal().d(f, "Crash when trying to parse body " + sb2);
                            return q88;
                        }
                    }
                }
            }
        }
        return q88;
    }

    @DexIgnore
    @Override // com.fossil.y08
    public v18 authenticate(x18 x18, Response response) {
        String f2;
        String f3;
        String accessToken;
        MFUser.Auth auth;
        UserDao userDao;
        int i = 0;
        pq7.c(response, "response");
        UserDatabase B = bn5.j.B();
        MFUser currentUser = (B == null || (userDao = B.userDao()) == null) ? null : userDao.getCurrentUser();
        if (TextUtils.isEmpty((currentUser == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken())) {
            FLogger.INSTANCE.getLocal().d(f, "unauthorized: userAccessToken is null or empty");
            return null;
        }
        boolean z = this.b.getAndIncrement() == 0;
        this.c = null;
        if (currentUser != null) {
            q88<Auth> a2 = a(currentUser, response, z);
            Auth auth2 = this.c;
            if (this.b.decrementAndGet() == 0) {
                this.c = null;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "authenticate decrementAndGet mCount=" + this.b + " address=" + this);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "old headers=" + response.G().e().toString());
            if (auth2 != null && (accessToken = auth2.getAccessToken()) != null) {
                currentUser.getAuth().setAccessToken(accessToken);
                MFUser.Auth auth3 = currentUser.getAuth();
                String refreshToken = auth2.getRefreshToken();
                if (refreshToken != null) {
                    auth3.setRefreshToken(refreshToken);
                    MFUser.Auth auth4 = currentUser.getAuth();
                    String w0 = lk5.w0(auth2.getAccessTokenExpiresAt());
                    pq7.b(w0, "DateHelper.toJodaTime(auth.accessTokenExpiresAt)");
                    auth4.setAccessTokenExpiresAt(w0);
                    MFUser.Auth auth5 = currentUser.getAuth();
                    Integer accessTokenExpiresIn = auth2.getAccessTokenExpiresIn();
                    if (accessTokenExpiresIn != null) {
                        i = accessTokenExpiresIn.intValue();
                    }
                    auth5.setAccessTokenExpiresIn(i);
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    local3.d(str3, "accessToken = " + accessToken + ", refreshToken = " + auth2.getRefreshToken());
                    this.e.V1(accessToken);
                    this.e.H0(System.currentTimeMillis());
                    xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(null, this, currentUser, auth2, response), 3, null);
                    v18.a h = response.G().h();
                    h.e("Authorization", "Bearer " + accessToken);
                    return h.b();
                }
                pq7.i();
                throw null;
            } else if (a2 != null && a2.b() >= 500 && a2.b() < 600) {
                ServerError serverError = new ServerError();
                serverError.setCode(Integer.valueOf(a2.b()));
                w18 d2 = a2.d();
                if (d2 == null || (f3 = d2.string()) == null) {
                    f3 = a2.f();
                }
                serverError.setMessage(f3);
                throw new ServerErrorException(serverError);
            } else if (!z || a2 == null) {
                return null;
            } else {
                try {
                    Gson gson = new Gson();
                    w18 a3 = a2.g().a();
                    if (a3 == null || (f2 = a3.string()) == null) {
                        f2 = a2.f();
                    }
                    ServerError serverError2 = (ServerError) gson.k(f2, ServerError.class);
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str4 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("forceLogout userMessage=");
                    pq7.b(serverError2, "serverError");
                    sb.append(serverError2.getUserMessage());
                    local4.d(str4, sb.toString());
                    PortfolioApp.h0.c().F(serverError2);
                    return null;
                } catch (Exception e2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str5 = f;
                    local5.d(str5, "forceLogout ex=" + e2.getMessage());
                    e2.printStackTrace();
                    PortfolioApp.h0.c().F(null);
                    return null;
                }
            }
        } else {
            pq7.i();
            throw null;
        }
    }
}
