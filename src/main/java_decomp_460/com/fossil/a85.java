package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a85 extends z75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131363049, 1);
        F.put(2131363407, 2);
        F.put(2131363173, 3);
        F.put(2131362122, 4);
        F.put(2131363410, 5);
        F.put(2131363316, 6);
        F.put(2131362926, 7);
        F.put(2131362087, 8);
        F.put(2131362396, 9);
        F.put(2131362665, 10);
        F.put(2131362501, 11);
        F.put(2131362406, 12);
    }
    */

    @DexIgnore
    public a85(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 13, E, F));
    }

    @DexIgnore
    public a85(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[8], (ConstraintLayout) objArr[4], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[12], (FlexibleButton) objArr[11], (RTLImageView) objArr[10], (ProgressBar) objArr[7], (RelativeLayout) objArr[0], (ViewPager2) objArr[1], (TabLayout) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5]);
        this.D = -1;
        this.x.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.D != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.D = 1;
        }
        w();
    }
}
