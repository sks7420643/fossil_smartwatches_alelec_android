package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface td4 {
    @DexIgnore
    td4 a(String str, boolean z) throws IOException;

    @DexIgnore
    td4 b(String str, long j) throws IOException;

    @DexIgnore
    td4 c(String str, int i) throws IOException;

    @DexIgnore
    td4 f(String str, Object obj) throws IOException;
}
