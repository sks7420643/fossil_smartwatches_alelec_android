package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bl3 extends pu2 implements cl3 {
    @DexIgnore
    public bl3() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    @Override // com.fossil.pu2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                E1((vg3) qt2.a(parcel, vg3.CREATOR), (or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                u2((fr3) qt2.a(parcel, fr3.CREATOR), (or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                r1((or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                K1((vg3) qt2.a(parcel, vg3.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                P1((or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 7:
                List<fr3> p1 = p1((or3) qt2.a(parcel, or3.CREATOR), qt2.e(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(p1);
                break;
            case 9:
                byte[] w2 = w2((vg3) qt2.a(parcel, vg3.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(w2);
                break;
            case 10:
                N0(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                String n0 = n0((or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(n0);
                break;
            case 12:
                s((xr3) qt2.a(parcel, xr3.CREATOR), (or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                D1((xr3) qt2.a(parcel, xr3.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                List<fr3> o1 = o1(parcel.readString(), parcel.readString(), qt2.e(parcel), (or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(o1);
                break;
            case 15:
                List<fr3> S = S(parcel.readString(), parcel.readString(), parcel.readString(), qt2.e(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(S);
                break;
            case 16:
                List<xr3> V0 = V0(parcel.readString(), parcel.readString(), (or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(V0);
                break;
            case 17:
                List<xr3> T0 = T0(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(T0);
                break;
            case 18:
                S0((or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 19:
                n2((Bundle) qt2.a(parcel, Bundle.CREATOR), (or3) qt2.a(parcel, or3.CREATOR));
                parcel2.writeNoException();
                break;
        }
        return true;
    }
}
