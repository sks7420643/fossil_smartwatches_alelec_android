package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w88<T> implements e88<w18, T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f3902a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public w88(Gson gson, TypeAdapter<T> typeAdapter) {
        this.f3902a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    /* renamed from: b */
    public T a(w18 w18) throws IOException {
        JsonReader q = this.f3902a.q(w18.charStream());
        try {
            T read = this.b.read(q);
            if (q.V() == nk4.END_DOCUMENT) {
                return read;
            }
            throw new ej4("JSON document was not fully consumed.");
        } finally {
            w18.close();
        }
    }
}
