package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ff4 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ff4 {
        @DexIgnore
        public /* final */ IBinder b;

        @DexIgnore
        public a(IBinder iBinder) {
            this.b = iBinder;
        }

        @DexIgnore
        @Override // com.fossil.ff4
        public void P(Message message) throws RemoteException {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
            obtain.writeInt(1);
            message.writeToParcel(obtain, 0);
            try {
                this.b.transact(1, obtain, null, 1);
            } finally {
                obtain.recycle();
            }
        }

        @DexIgnore
        public IBinder asBinder() {
            return this.b;
        }
    }

    @DexIgnore
    void P(Message message) throws RemoteException;
}
