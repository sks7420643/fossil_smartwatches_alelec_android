package com.fossil;

import com.fossil.af1;
import com.fossil.sc1;
import com.fossil.wb1;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jd1 implements sc1, wb1.a<Object> {
    @DexIgnore
    public /* final */ sc1.a b;
    @DexIgnore
    public /* final */ tc1<?> c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public mb1 f;
    @DexIgnore
    public List<af1<File, ?>> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public volatile af1.a<?> i;
    @DexIgnore
    public File j;
    @DexIgnore
    public kd1 k;

    @DexIgnore
    public jd1(tc1<?> tc1, sc1.a aVar) {
        this.c = tc1;
        this.b = aVar;
    }

    @DexIgnore
    public final boolean a() {
        return this.h < this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.wb1.a
    public void b(Exception exc) {
        this.b.a(this.k, exc, this.i.c, gb1.RESOURCE_DISK_CACHE);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 138
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    @DexIgnore
    @Override // com.fossil.sc1
    public boolean c() {
        /*
        // Method dump skipped, instructions count: 302
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jd1.c():boolean");
    }

    @DexIgnore
    @Override // com.fossil.sc1
    public void cancel() {
        af1.a<?> aVar = this.i;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.wb1.a
    public void e(Object obj) {
        this.b.e(this.f, obj, this.i.c, gb1.RESOURCE_DISK_CACHE, this.k);
    }
}
