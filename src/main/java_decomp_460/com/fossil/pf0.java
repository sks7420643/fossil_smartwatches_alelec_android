package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface pf0 {
    @DexIgnore
    Object onActionViewCollapsed();  // void declaration

    @DexIgnore
    Object onActionViewExpanded();  // void declaration
}
