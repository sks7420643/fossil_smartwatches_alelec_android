package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "coil.RealImageLoader$loadData$2", f = "RealImageLoader.kt", l = {369, 385, 511}, m = "invokeSuspend")
public final class d51 extends ko7 implements vp7<iv7, qn7<? super c61>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Object $data;
    @DexIgnore
    public /* final */ /* synthetic */ e61 $fetcher;
    @DexIgnore
    public /* final */ /* synthetic */ Object $mappedData;
    @DexIgnore
    public /* final */ /* synthetic */ x71 $request;
    @DexIgnore
    public /* final */ /* synthetic */ e81 $scale;
    @DexIgnore
    public /* final */ /* synthetic */ f81 $size;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$10;
    @DexIgnore
    public Object L$11;
    @DexIgnore
    public Object L$12;
    @DexIgnore
    public Object L$13;
    @DexIgnore
    public Object L$14;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public iv7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ c51 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d51(c51 c51, x71 x71, f81 f81, e81 e81, e61 e61, Object obj, Object obj2, qn7 qn7) {
        super(2, qn7);
        this.this$0 = c51;
        this.$request = x71;
        this.$size = f81;
        this.$scale = e81;
        this.$fetcher = e61;
        this.$mappedData = obj;
        this.$data = obj2;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        pq7.c(qn7, "completion");
        d51 d51 = new d51(this.this$0, this.$request, this.$size, this.$scale, this.$fetcher, this.$mappedData, this.$data, qn7);
        d51.p$ = (iv7) obj;
        return d51;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super c61> qn7) {
        return ((d51) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0176 A[SYNTHETIC, Splitter:B:38:0x0176] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0255  */
    @Override // com.fossil.zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r23) {
        /*
        // Method dump skipped, instructions count: 777
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.d51.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
