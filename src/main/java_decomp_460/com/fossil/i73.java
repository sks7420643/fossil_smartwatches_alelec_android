package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i73 implements xw2<h73> {
    @DexIgnore
    public static i73 c; // = new i73();
    @DexIgnore
    public /* final */ xw2<h73> b;

    @DexIgnore
    public i73() {
        this(ww2.b(new k73()));
    }

    @DexIgnore
    public i73(xw2<h73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((h73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((h73) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ h73 zza() {
        return this.b.zza();
    }
}
