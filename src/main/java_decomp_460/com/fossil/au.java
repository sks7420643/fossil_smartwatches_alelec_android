package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum au {
    PAIR_ANIMATION(new byte[]{5}),
    OPTIMAL_PAYLOAD(new byte[]{40}),
    HEARTBEAT_STATISTIC(new byte[]{41}),
    BLE_TROUBLESHOOT(new byte[]{83});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public au(byte[] bArr) {
        this.b = bArr;
    }
}
