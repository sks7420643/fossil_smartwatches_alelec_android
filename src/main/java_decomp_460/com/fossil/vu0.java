package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.zu0;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vu0<T> {
    @DexIgnore
    public static /* final */ Executor h; // = new c();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ jv0 f3822a;
    @DexIgnore
    public /* final */ uu0<T> b;
    @DexIgnore
    public Executor c;
    @DexIgnore
    public /* final */ List<b<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<T> e;
    @DexIgnore
    public List<T> f; // = Collections.emptyList();
    @DexIgnore
    public int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vu0$a$a")
        /* renamed from: com.fossil.vu0$a$a  reason: collision with other inner class name */
        public class C0265a extends zu0.b {
            @DexIgnore
            public C0265a() {
            }

            @DexIgnore
            @Override // com.fossil.zu0.b
            public boolean a(int i, int i2) {
                Object obj = a.this.b.get(i);
                Object obj2 = a.this.c.get(i2);
                if (obj != null && obj2 != null) {
                    return vu0.this.b.b().areContentsTheSame(obj, obj2);
                }
                if (obj == null && obj2 == null) {
                    return true;
                }
                throw new AssertionError();
            }

            @DexIgnore
            @Override // com.fossil.zu0.b
            public boolean b(int i, int i2) {
                Object obj = a.this.b.get(i);
                Object obj2 = a.this.c.get(i2);
                return (obj == null || obj2 == null) ? obj == null && obj2 == null : vu0.this.b.b().areItemsTheSame(obj, obj2);
            }

            @DexIgnore
            @Override // com.fossil.zu0.b
            public Object c(int i, int i2) {
                Object obj = a.this.b.get(i);
                Object obj2 = a.this.c.get(i2);
                if (obj != null && obj2 != null) {
                    return vu0.this.b.b().getChangePayload(obj, obj2);
                }
                throw new AssertionError();
            }

            @DexIgnore
            @Override // com.fossil.zu0.b
            public int d() {
                return a.this.c.size();
            }

            @DexIgnore
            @Override // com.fossil.zu0.b
            public int e() {
                return a.this.b.size();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ zu0.c b;

            @DexIgnore
            public b(zu0.c cVar) {
                this.b = cVar;
            }

            @DexIgnore
            public void run() {
                a aVar = a.this;
                vu0 vu0 = vu0.this;
                if (vu0.g == aVar.d) {
                    vu0.c(aVar.c, this.b, aVar.e);
                }
            }
        }

        @DexIgnore
        public a(List list, List list2, int i, Runnable runnable) {
            this.b = list;
            this.c = list2;
            this.d = i;
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            vu0.this.c.execute(new b(zu0.a(new C0265a())));
        }
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void a(List<T> list, List<T> list2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }

    @DexIgnore
    public vu0(jv0 jv0, uu0<T> uu0) {
        this.f3822a = jv0;
        this.b = uu0;
        if (uu0.c() != null) {
            this.c = uu0.c();
        } else {
            this.c = h;
        }
    }

    @DexIgnore
    public void a(b<T> bVar) {
        this.d.add(bVar);
    }

    @DexIgnore
    public List<T> b() {
        return this.f;
    }

    @DexIgnore
    public void c(List<T> list, zu0.c cVar, Runnable runnable) {
        List<T> list2 = this.f;
        this.e = list;
        this.f = Collections.unmodifiableList(list);
        cVar.e(this.f3822a);
        d(list2, runnable);
    }

    @DexIgnore
    public final void d(List<T> list, Runnable runnable) {
        for (b<T> bVar : this.d) {
            bVar.a(list, this.f);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void e(List<T> list) {
        f(list, null);
    }

    @DexIgnore
    public void f(List<T> list, Runnable runnable) {
        int i = this.g + 1;
        this.g = i;
        List<T> list2 = this.e;
        if (list != list2) {
            List<T> list3 = this.f;
            if (list == null) {
                int size = list2.size();
                this.e = null;
                this.f = Collections.emptyList();
                this.f3822a.c(0, size);
                d(list3, runnable);
            } else if (list2 == null) {
                this.e = list;
                this.f = Collections.unmodifiableList(list);
                this.f3822a.b(0, list.size());
                d(list3, runnable);
            } else {
                this.b.a().execute(new a(list2, list, i, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }
}
