package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr extends ss {
    @DexIgnore
    public long A;
    @DexIgnore
    public b5 B; // = b5.DISCONNECTED;

    @DexIgnore
    public yr(k5 k5Var, long j) {
        super(hs.K, k5Var);
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.q1, ey1.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new z5(this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void g(u5 u5Var) {
        this.B = ((z5) u5Var).l;
        this.g.add(new hw(0, null, null, g80.k(new JSONObject(), jd0.q1, ey1.a(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void i(p7 p7Var) {
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.p1, ey1.a(this.y.D())), jd0.k0, this.y.x);
    }
}
