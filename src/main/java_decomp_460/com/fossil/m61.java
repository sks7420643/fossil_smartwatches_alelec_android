package com.fossil;

import android.net.Uri;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m61 implements n61<Uri, File> {
    @DexIgnore
    /* renamed from: c */
    public boolean a(Uri uri) {
        pq7.c(uri, "data");
        if (pq7.a(uri.getScheme(), "file")) {
            String f = w81.f(uri);
            if (f != null && (pq7.a(f, "android_asset") ^ true)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: d */
    public File b(Uri uri) {
        pq7.c(uri, "data");
        return lm0.a(uri);
    }
}
