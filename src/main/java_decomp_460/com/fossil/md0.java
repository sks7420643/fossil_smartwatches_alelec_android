package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md0 implements Runnable {
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ gp7<tl7> c;

    @DexIgnore
    public md0(gp7<tl7> gp7) {
        this.c = gp7;
    }

    @DexIgnore
    public void run() {
        if (!this.b) {
            this.c.invoke();
        }
    }
}
