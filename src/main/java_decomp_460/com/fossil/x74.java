package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x74 {
    @DexIgnore
    public static /* final */ x74 c; // = new x74("FirebaseCrashlytics");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4047a;
    @DexIgnore
    public int b; // = 4;

    @DexIgnore
    public x74(String str) {
        this.f4047a = str;
    }

    @DexIgnore
    public static x74 f() {
        return c;
    }

    @DexIgnore
    public final boolean a(int i) {
        return this.b <= i || Log.isLoggable(this.f4047a, i);
    }

    @DexIgnore
    public void b(String str) {
        c(str, null);
    }

    @DexIgnore
    public void c(String str, Throwable th) {
        if (a(3)) {
            Log.d(this.f4047a, str, th);
        }
    }

    @DexIgnore
    public void d(String str) {
        e(str, null);
    }

    @DexIgnore
    public void e(String str, Throwable th) {
        if (a(6)) {
            Log.e(this.f4047a, str, th);
        }
    }

    @DexIgnore
    public void g(String str) {
        h(str, null);
    }

    @DexIgnore
    public void h(String str, Throwable th) {
        if (a(4)) {
            Log.i(this.f4047a, str, th);
        }
    }

    @DexIgnore
    public void i(String str) {
        j(str, null);
    }

    @DexIgnore
    public void j(String str, Throwable th) {
        if (a(5)) {
            Log.w(this.f4047a, str, th);
        }
    }
}
