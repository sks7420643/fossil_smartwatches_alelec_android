package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ip4 implements Factory<GoogleApiService> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1648a;
    @DexIgnore
    public /* final */ Provider<qq5> b;
    @DexIgnore
    public /* final */ Provider<uq5> c;

    @DexIgnore
    public ip4(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        this.f1648a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static ip4 a(uo4 uo4, Provider<qq5> provider, Provider<uq5> provider2) {
        return new ip4(uo4, provider, provider2);
    }

    @DexIgnore
    public static GoogleApiService c(uo4 uo4, qq5 qq5, uq5 uq5) {
        GoogleApiService p = uo4.p(qq5, uq5);
        lk7.c(p, "Cannot return null from a non-@Nullable @Provides method");
        return p;
    }

    @DexIgnore
    /* renamed from: b */
    public GoogleApiService get() {
        return c(this.f1648a, this.b.get(), this.c.get());
    }
}
