package com.fossil;

import com.fossil.c44;
import com.fossil.x44;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends i44<c44.a<?>> {
        @DexIgnore
        /* renamed from: a */
        public int compare(c44.a<?> aVar, c44.a<?> aVar2) {
            return v54.a(aVar2.getCount(), aVar.getCount());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> implements c44.a<E> {
        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c44.a)) {
                return false;
            }
            c44.a aVar = (c44.a) obj;
            return getCount() == aVar.getCount() && f14.a(getElement(), aVar.getElement());
        }

        @DexIgnore
        public int hashCode() {
            E element = getElement();
            return (element == null ? 0 : element.hashCode()) ^ getCount();
        }

        @DexIgnore
        public String toString() {
            String valueOf = String.valueOf(getElement());
            int count = getCount();
            if (count == 1) {
                return valueOf;
            }
            return valueOf + " x " + count;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<E> extends x44.a<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends g54<c44.a<E>, E> {
            @DexIgnore
            public a(c cVar, Iterator it) {
                super(it);
            }

            @DexIgnore
            /* renamed from: b */
            public E a(c44.a<E> aVar) {
                return aVar.getElement();
            }
        }

        @DexIgnore
        public abstract c44<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().contains(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return a().containsAll(collection);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<E> iterator() {
            return new a(this, a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return a().remove(obj, Integer.MAX_VALUE) > 0;
        }

        @DexIgnore
        public int size() {
            return a().entrySet().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<E> extends x44.a<c44.a<E>> {
        @DexIgnore
        public abstract c44<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof c44.a)) {
                return false;
            }
            c44.a aVar = (c44.a) obj;
            return aVar.getCount() > 0 && a().count(aVar.getElement()) == aVar.getCount();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!(obj instanceof c44.a)) {
                return false;
            }
            c44.a aVar = (c44.a) obj;
            E e = (E) aVar.getElement();
            int count = aVar.getCount();
            if (count != 0) {
                return a().setCount(e, count, 0);
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<E> extends b<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int count;
        @DexIgnore
        public /* final */ E element;

        @DexIgnore
        public e(E e, int i) {
            this.element = e;
            this.count = i;
            a24.b(i, "count");
        }

        @DexIgnore
        @Override // com.fossil.c44.a
        public final int getCount() {
            return this.count;
        }

        @DexIgnore
        @Override // com.fossil.c44.a
        public final E getElement() {
            return this.element;
        }

        @DexIgnore
        public e<E> nextInBucket() {
            return null;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public static <E> boolean a(c44<E> c44, Collection<? extends E> collection) {
        if (collection.isEmpty()) {
            return false;
        }
        if (collection instanceof c44) {
            for (c44.a<E> aVar : b(collection).entrySet()) {
                c44.add(aVar.getElement(), aVar.getCount());
            }
        } else {
            p34.a(c44, collection.iterator());
        }
        return true;
    }

    @DexIgnore
    public static <T> c44<T> b(Iterable<T> iterable) {
        return (c44) iterable;
    }

    @DexIgnore
    public static boolean c(c44<?> c44, Object obj) {
        if (obj == c44) {
            return true;
        }
        if (obj instanceof c44) {
            c44 c442 = (c44) obj;
            if (c44.size() == c442.size() && c44.entrySet().size() == c442.entrySet().size()) {
                for (c44.a aVar : c442.entrySet()) {
                    if (c44.count(aVar.getElement()) != aVar.getCount()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static <E> c44.a<E> d(E e2, int i) {
        return new e(e2, i);
    }

    @DexIgnore
    public static int e(Iterable<?> iterable) {
        if (iterable instanceof c44) {
            return ((c44) iterable).elementSet().size();
        }
        return 11;
    }

    @DexIgnore
    public static boolean f(c44<?> c44, Collection<?> collection) {
        if (collection instanceof c44) {
            collection = ((c44) collection).elementSet();
        }
        return c44.elementSet().removeAll(collection);
    }

    @DexIgnore
    public static boolean g(c44<?> c44, Collection<?> collection) {
        i14.l(collection);
        if (collection instanceof c44) {
            collection = ((c44) collection).elementSet();
        }
        return c44.elementSet().retainAll(collection);
    }

    @DexIgnore
    public static <E> int h(c44<E> c44, E e2, int i) {
        a24.b(i, "count");
        int count = c44.count(e2);
        int i2 = i - count;
        if (i2 > 0) {
            c44.add(e2, i2);
        } else if (i2 < 0) {
            c44.remove(e2, -i2);
        }
        return count;
    }

    @DexIgnore
    public static <E> boolean i(c44<E> c44, E e2, int i, int i2) {
        a24.b(i, "oldCount");
        a24.b(i2, "newCount");
        if (c44.count(e2) != i) {
            return false;
        }
        c44.setCount(e2, i2);
        return true;
    }

    @DexIgnore
    public static int j(c44<?> c44) {
        long j = 0;
        for (c44.a<?> aVar : c44.entrySet()) {
            j = ((long) aVar.getCount()) + j;
        }
        return v54.b(j);
    }
}
