package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.SupportMapFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pf3 extends qc3 {
    @DexIgnore
    public /* final */ /* synthetic */ sb3 b;

    @DexIgnore
    public pf3(SupportMapFragment.a aVar, sb3 sb3) {
        this.b = sb3;
    }

    @DexIgnore
    @Override // com.fossil.pc3
    public final void l2(zb3 zb3) throws RemoteException {
        this.b.onMapReady(new qb3(zb3));
    }
}
