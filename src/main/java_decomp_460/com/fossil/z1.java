package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z1 extends c2 {
    @DexIgnore
    public static /* final */ x2 CREATOR; // = new x2(null);
    @DexIgnore
    public x1 e;

    @DexIgnore
    public z1(byte b) {
        super(lt.TIME_SYNC_EVENT, b, false, 4);
    }

    @DexIgnore
    public /* synthetic */ z1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }

    @DexIgnore
    @Override // com.fossil.c2
    public byte[] a() {
        this.e = new x1(System.currentTimeMillis());
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        x1 x1Var = this.e;
        if (x1Var != null) {
            ByteBuffer putInt = order.putInt((int) x1Var.b);
            x1 x1Var2 = this.e;
            if (x1Var2 != null) {
                ByteBuffer putShort = putInt.putShort((short) ((int) x1Var2.c));
                x1 x1Var3 = this.e;
                if (x1Var3 != null) {
                    byte[] array = putShort.putShort((short) x1Var3.d).array();
                    pq7.b(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
                    return array;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        jd0 jd0 = jd0.z0;
        x1 x1Var = this.e;
        return g80.k(jSONObject, jd0, x1Var != null ? x1Var.toJSONObject() : null);
    }
}
