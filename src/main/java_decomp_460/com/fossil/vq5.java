package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq5 implements Factory<uq5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<AuthApiGuestService> f3807a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public vq5(Provider<AuthApiGuestService> provider, Provider<on5> provider2) {
        this.f3807a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static vq5 a(Provider<AuthApiGuestService> provider, Provider<on5> provider2) {
        return new vq5(provider, provider2);
    }

    @DexIgnore
    public static uq5 c(AuthApiGuestService authApiGuestService, on5 on5) {
        return new uq5(authApiGuestService, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public uq5 get() {
        return c(this.f3807a.get(), this.b.get());
    }
}
