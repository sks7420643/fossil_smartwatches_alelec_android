package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o52 implements Parcelable.Creator<GoogleSignInOptions> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInOptions createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i = 0;
        String str = null;
        ArrayList arrayList = null;
        String str2 = null;
        String str3 = null;
        Account account = null;
        ArrayList arrayList2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 1:
                    i = ad2.v(parcel, t);
                    break;
                case 2:
                    arrayList2 = ad2.j(parcel, t, Scope.CREATOR);
                    break;
                case 3:
                    account = (Account) ad2.e(parcel, t, Account.CREATOR);
                    break;
                case 4:
                    z3 = ad2.m(parcel, t);
                    break;
                case 5:
                    z2 = ad2.m(parcel, t);
                    break;
                case 6:
                    z = ad2.m(parcel, t);
                    break;
                case 7:
                    str3 = ad2.f(parcel, t);
                    break;
                case 8:
                    str2 = ad2.f(parcel, t);
                    break;
                case 9:
                    arrayList = ad2.j(parcel, t, m42.CREATOR);
                    break;
                case 10:
                    str = ad2.f(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new GoogleSignInOptions(i, arrayList2, account, z3, z2, z, str3, str2, arrayList, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInOptions[] newArray(int i) {
        return new GoogleSignInOptions[i];
    }
}
