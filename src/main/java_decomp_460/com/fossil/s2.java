package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s2 extends c2 {
    @DexIgnore
    public static /* final */ r2 CREATOR; // = new r2(null);
    @DexIgnore
    public /* final */ bv1 e;

    @DexIgnore
    public s2(byte b, bv1 bv1) {
        super(lt.MICRO_APP_EVENT, b, false, 4);
        this.e = bv1;
    }

    @DexIgnore
    public /* synthetic */ s2(Parcel parcel, kq7 kq7) {
        super(parcel);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            pq7.b(createByteArray, "parcel.createByteArray()!!");
            this.e = new bv1(createByteArray);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(s2.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            s2 s2Var = (s2) obj;
            if (this.b != s2Var.b) {
                return false;
            }
            if (this.c != s2Var.c) {
                return false;
            }
            return !(pq7.a(this.e, s2Var.e) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return (((hashCode * 31) + this.c) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.v3, this.e.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
