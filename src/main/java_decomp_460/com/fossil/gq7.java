package com.fossil;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gq7 implements ds7, Serializable {
    @DexIgnore
    public static /* final */ Object NO_RECEIVER; // = a.b;
    @DexIgnore
    public /* final */ Object receiver;
    @DexIgnore
    public transient ds7 reflected;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Serializable {
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore
        private Object readResolve() throws ObjectStreamException {
            return b;
        }
    }

    @DexIgnore
    public gq7() {
        this(NO_RECEIVER);
    }

    @DexIgnore
    public gq7(Object obj) {
        this.receiver = obj;
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public Object call(Object... objArr) {
        return getReflected().call(objArr);
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public Object callBy(Map map) {
        return getReflected().callBy(map);
    }

    @DexIgnore
    public ds7 compute() {
        ds7 ds7 = this.reflected;
        if (ds7 != null) {
            return ds7;
        }
        ds7 computeReflected = computeReflected();
        this.reflected = computeReflected;
        return computeReflected;
    }

    @DexIgnore
    public abstract ds7 computeReflected();

    @DexIgnore
    @Override // com.fossil.cs7
    public List<Annotation> getAnnotations() {
        return getReflected().getAnnotations();
    }

    @DexIgnore
    public Object getBoundReceiver() {
        return this.receiver;
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public String getName() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    public fs7 getOwner() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public List<Object> getParameters() {
        return getReflected().getParameters();
    }

    @DexIgnore
    public ds7 getReflected() {
        ds7 compute = compute();
        if (compute != this) {
            return compute;
        }
        throw new fp7();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public ns7 getReturnType() {
        return getReflected().getReturnType();
    }

    @DexIgnore
    public String getSignature() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public List<Object> getTypeParameters() {
        return getReflected().getTypeParameters();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public os7 getVisibility() {
        return getReflected().getVisibility();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public boolean isAbstract() {
        return getReflected().isAbstract();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public boolean isFinal() {
        return getReflected().isFinal();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public boolean isOpen() {
        return getReflected().isOpen();
    }

    @DexIgnore
    @Override // com.fossil.ds7
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }
}
