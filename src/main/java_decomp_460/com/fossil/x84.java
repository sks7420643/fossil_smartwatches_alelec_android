package com.fossil;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4074a;
    @DexIgnore
    public /* final */ tb4 b;

    @DexIgnore
    public x84(String str, tb4 tb4) {
        this.f4074a = str;
        this.b = tb4;
    }

    @DexIgnore
    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            x74 f = x74.f();
            f.e("Error creating marker: " + this.f4074a, e);
            return false;
        }
    }

    @DexIgnore
    public final File b() {
        return new File(this.b.b(), this.f4074a);
    }

    @DexIgnore
    public boolean c() {
        return b().exists();
    }

    @DexIgnore
    public boolean d() {
        return b().delete();
    }
}
