package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.ue7;
import com.fossil.ve7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ag7 implements sf7 {
    @DexIgnore
    public static a e;
    @DexIgnore
    public static String f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Context f266a;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public boolean b;
        @DexIgnore
        public Handler c;
        @DexIgnore
        public Context d;
        @DexIgnore
        public Runnable e;
        @DexIgnore
        public Runnable f;

        @DexIgnore
        public a(Context context) {
            this.b = false;
            this.c = new Handler(Looper.getMainLooper());
            this.e = new yf7(this);
            this.f = new zf7(this);
            this.d = context;
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityPaused");
            this.c.removeCallbacks(this.f);
            this.c.postDelayed(this.e, 800);
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityResumed");
            this.c.removeCallbacks(this.e);
            this.c.postDelayed(this.f, 800);
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public ag7(Context context, String str, boolean z) {
        ye7.e("MicroMsg.SDK.WXApiImplV10", "<init>, appId = " + str + ", checkSignature = " + z);
        this.f266a = context;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public final boolean a(Intent intent, tf7 tf7) {
        try {
            if (!wf7.a(intent, "com.tencent.mm.openapi.token")) {
                ye7.d("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, intent not from weixin msg");
                return false;
            } else if (!this.d) {
                String stringExtra = intent.getStringExtra("_mmessage_content");
                int intExtra = intent.getIntExtra("_mmessage_sdkVersion", 0);
                String stringExtra2 = intent.getStringExtra("_mmessage_appPackage");
                if (stringExtra2 == null || stringExtra2.length() == 0) {
                    ye7.b("MicroMsg.SDK.WXApiImplV10", "invalid argument");
                    return false;
                } else if (!f(intent.getByteArrayExtra("_mmessage_checksum"), we7.a(stringExtra, intExtra, stringExtra2))) {
                    ye7.b("MicroMsg.SDK.WXApiImplV10", "checksum fail");
                    return false;
                } else {
                    int intExtra2 = intent.getIntExtra("_wxapi_command_type", 0);
                    if (intExtra2 == 9) {
                        tf7.b(new ff7(intent.getExtras()));
                        return true;
                    } else if (intExtra2 == 12) {
                        tf7.b(new jf7(intent.getExtras()));
                        return true;
                    } else if (intExtra2 == 14) {
                        tf7.b(new hf7(intent.getExtras()));
                        return true;
                    } else if (intExtra2 != 15) {
                        switch (intExtra2) {
                            case 1:
                                tf7.b(new nf7(intent.getExtras()));
                                return true;
                            case 2:
                                tf7.b(new of7(intent.getExtras()));
                                return true;
                            case 3:
                                tf7.a(new kf7(intent.getExtras()));
                                return true;
                            case 4:
                                tf7.a(new pf7(intent.getExtras()));
                                return true;
                            case 5:
                                tf7.b(new rf7(intent.getExtras()));
                                return true;
                            case 6:
                                tf7.a(new lf7(intent.getExtras()));
                                return true;
                            default:
                                ye7.b("MicroMsg.SDK.WXApiImplV10", "unknown cmd = " + intExtra2);
                                return false;
                        }
                    } else {
                        tf7.b(new if7(intent.getExtras()));
                        return true;
                    }
                }
            } else {
                throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
            }
        } catch (Exception e2) {
            ye7.a("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, ex = %s", e2.getMessage());
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public final boolean b() {
        if (!this.d) {
            try {
                PackageInfo packageInfo = this.f266a.getPackageManager().getPackageInfo("com.tencent.mm", 64);
                if (packageInfo == null) {
                    return false;
                }
                return wf7.b(this.f266a, packageInfo.signatures, this.c);
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        } else {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public final boolean c(String str) {
        Application application;
        if (this.d) {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        } else if (!wf7.c(this.f266a, "com.tencent.mm", this.c)) {
            ye7.b("MicroMsg.SDK.WXApiImplV10", "register app failed for wechat app signature check failed");
            return false;
        } else {
            if (e == null && Build.VERSION.SDK_INT >= 14) {
                Context context = this.f266a;
                if (context instanceof Activity) {
                    h(context, str);
                    e = new a(this.f266a);
                    application = ((Activity) this.f266a).getApplication();
                } else if (context instanceof Service) {
                    h(context, str);
                    e = new a(this.f266a);
                    application = ((Service) this.f266a).getApplication();
                } else {
                    ye7.c("MicroMsg.SDK.WXApiImplV10", "context is not instanceof Activity or Service, disable WXStat");
                }
                application.registerActivityLifecycleCallbacks(e);
            }
            ye7.e("MicroMsg.SDK.WXApiImplV10", "registerApp, appId = " + str);
            if (str != null) {
                this.b = str;
            }
            ye7.e("MicroMsg.SDK.WXApiImplV10", "register app " + this.f266a.getPackageName());
            ve7.a aVar = new ve7.a();
            aVar.f3756a = "com.tencent.mm";
            aVar.b = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER";
            aVar.c = "weixin://registerapp?appid=" + this.b;
            return ve7.a(this.f266a, aVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public final boolean d(df7 df7) {
        String str;
        if (!this.d) {
            if (!wf7.c(this.f266a, "com.tencent.mm", this.c)) {
                str = "sendReq failed for wechat app signature check failed";
            } else if (!df7.a()) {
                str = "sendReq checkArgs fail";
            } else {
                ye7.e("MicroMsg.SDK.WXApiImplV10", "sendReq, req type = " + df7.c());
                Bundle bundle = new Bundle();
                df7.d(bundle);
                if (df7.c() == 5) {
                    return q(this.f266a, bundle);
                }
                if (df7.c() == 7) {
                    return k(this.f266a, bundle);
                }
                if (df7.c() == 8) {
                    return m(this.f266a, bundle);
                }
                if (df7.c() == 10) {
                    return l(this.f266a, bundle);
                }
                if (df7.c() == 9) {
                    return j(this.f266a, bundle);
                }
                if (df7.c() == 11) {
                    return o(this.f266a, bundle);
                }
                if (df7.c() == 12) {
                    return p(this.f266a, bundle);
                }
                if (df7.c() == 13) {
                    return n(this.f266a, bundle);
                }
                if (df7.c() == 14) {
                    return g(this.f266a, bundle);
                }
                if (df7.c() == 15) {
                    return i(this.f266a, bundle);
                }
                ue7.a aVar = new ue7.a();
                aVar.e = bundle;
                aVar.c = "weixin://sendreq?appid=" + this.b;
                aVar.f3575a = "com.tencent.mm";
                aVar.b = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
                return ue7.a(this.f266a, aVar);
            }
            ye7.b("MicroMsg.SDK.WXApiImplV10", str);
            return false;
        }
        throw new IllegalStateException("sendReq fail, WXMsgImpl has been detached");
    }

    @DexIgnore
    public final boolean f(byte[] bArr, byte[] bArr2) {
        String str;
        if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            str = "checkSumConsistent fail, invalid arguments";
        } else if (bArr.length != bArr2.length) {
            str = "checkSumConsistent fail, length is different";
        } else {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
            return true;
        }
        ye7.b("MicroMsg.SDK.WXApiImplV10", str);
        return false;
    }

    @DexIgnore
    public final boolean g(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/createChatroom"), null, null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_create_chatroom_group_id"), bundle.getString("_wxapi_create_chatroom_chatroom_name"), bundle.getString("_wxapi_create_chatroom_chatroom_nickname"), bundle.getString("_wxapi_create_chatroom_ext_msg")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final void h(Context context, String str) {
        String str2 = "AWXOP" + str;
        fg7.N(context, str2);
        fg7.O(true);
        fg7.U(gg7.PERIOD);
        fg7.S(60);
        fg7.Q(context, "Wechat_Sdk");
        try {
            hg7.c(context, str2, "2.0.3");
        } catch (dg7 e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final boolean i(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/joinChatroom"), null, null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_join_chatroom_group_id"), bundle.getString("_wxapi_join_chatroom_chatroom_nickname"), bundle.getString("_wxapi_join_chatroom_ext_msg")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean j(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), null, null, new String[]{this.b, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0021: INVOKE  (r7v1 int) = (r13v0 android.os.Bundle), ("_wxapi_jump_to_biz_profile_req_scene") type: VIRTUAL call: android.os.Bundle.getInt(java.lang.String):int)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0033: INVOKE  (r4v4 int) = (r13v0 android.os.Bundle), ("_wxapi_jump_to_biz_profile_req_profile_type") type: VIRTUAL call: android.os.Bundle.getInt(java.lang.String):int)] */
    public final boolean k(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        String str = this.b;
        String string = bundle.getString("_wxapi_jump_to_biz_profile_req_to_user_name");
        String string2 = bundle.getString("_wxapi_jump_to_biz_profile_req_ext_msg");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_scene"));
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_profile_type"));
        Cursor query = contentResolver.query(parse, null, null, new String[]{str, string, string2, sb2, sb3.toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0021: INVOKE  (r4v3 int) = (r12v0 android.os.Bundle), ("_wxapi_jump_to_biz_webview_req_show_type") type: VIRTUAL call: android.os.Bundle.getInt(java.lang.String):int)] */
    public final boolean l(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizTempSession");
        String str = this.b;
        String string = bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name");
        String string2 = bundle.getString("_wxapi_jump_to_biz_webview_req_session_from");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_show_type"));
        Cursor query = contentResolver.query(parse, null, null, new String[]{str, string, string2, sb.toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0021: INVOKE  (r4v3 int) = (r12v0 android.os.Bundle), ("_wxapi_jump_to_biz_webview_req_scene") type: VIRTUAL call: android.os.Bundle.getInt(java.lang.String):int)] */
    public final boolean m(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        String str = this.b;
        String string = bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name");
        String string2 = bundle.getString("_wxapi_jump_to_biz_webview_req_ext_msg");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_scene"));
        Cursor query = contentResolver.query(parse, null, null, new String[]{str, string, string2, sb.toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean n(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusiLuckyMoney"), null, null, new String[]{this.b, bundle.getString("_wxapi_open_busi_lucky_money_timeStamp"), bundle.getString("_wxapi_open_busi_lucky_money_nonceStr"), bundle.getString("_wxapi_open_busi_lucky_money_signType"), bundle.getString("_wxapi_open_busi_lucky_money_signature"), bundle.getString("_wxapi_open_busi_lucky_money_package")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean o(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openRankList"), null, null, new String[0], null);
        if (query == null) {
            return true;
        }
        query.close();
        return true;
    }

    @DexIgnore
    public final boolean p(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openWebview"), null, null, new String[]{this.b, bundle.getString("_wxapi_jump_to_webview_url"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean q(Context context, Bundle bundle) {
        if (f == null) {
            f = new uf7(context).getString("_wxapp_pay_entry_classname_", null);
            ye7.e("MicroMsg.SDK.WXApiImplV10", "pay, set wxappPayEntryClassname = " + f);
            if (f == null) {
                ye7.b("MicroMsg.SDK.WXApiImplV10", "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        ue7.a aVar = new ue7.a();
        aVar.e = bundle;
        aVar.f3575a = "com.tencent.mm";
        aVar.b = f;
        return ue7.a(context, aVar);
    }
}
