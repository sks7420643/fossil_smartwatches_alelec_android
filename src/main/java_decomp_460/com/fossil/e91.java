package com.fossil;

import android.os.Handler;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e91 implements p91 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f894a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;

        @DexIgnore
        public a(e91 e91, Handler handler) {
            this.b = handler;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ m91 b;
        @DexIgnore
        public /* final */ o91 c;
        @DexIgnore
        public /* final */ Runnable d;

        @DexIgnore
        public b(m91 m91, o91 o91, Runnable runnable) {
            this.b = m91;
            this.c = o91;
            this.d = runnable;
        }

        @DexIgnore
        public void run() {
            if (this.b.isCanceled()) {
                this.b.finish("canceled-at-delivery");
                return;
            }
            if (this.c.b()) {
                this.b.deliverResponse(this.c.f2651a);
            } else {
                this.b.deliverError(this.c.c);
            }
            if (this.c.d) {
                this.b.addMarker("intermediate-response");
            } else {
                this.b.finish("done");
            }
            Runnable runnable = this.d;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public e91(Handler handler) {
        this.f894a = new a(this, handler);
    }

    @DexIgnore
    @Override // com.fossil.p91
    public void a(m91<?> m91, o91<?> o91) {
        b(m91, o91, null);
    }

    @DexIgnore
    @Override // com.fossil.p91
    public void b(m91<?> m91, o91<?> o91, Runnable runnable) {
        m91.markDelivered();
        m91.addMarker("post-response");
        this.f894a.execute(new b(m91, o91, runnable));
    }

    @DexIgnore
    @Override // com.fossil.p91
    public void c(m91<?> m91, t91 t91) {
        m91.addMarker("post-error");
        this.f894a.execute(new b(m91, o91.a(t91), null));
    }
}
