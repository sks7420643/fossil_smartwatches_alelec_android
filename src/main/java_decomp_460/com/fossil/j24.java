package com.fossil;

import com.fossil.i44;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j24<T> extends i44<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ a34<T, Integer> rankMap;

    @DexIgnore
    public j24(a34<T, Integer> a34) {
        this.rankMap = a34;
    }

    @DexIgnore
    public j24(List<T> list) {
        this(x34.f(list));
    }

    @DexIgnore
    public final int a(T t) {
        Integer num = this.rankMap.get(t);
        if (num != null) {
            return num.intValue();
        }
        throw new i44.c(t);
    }

    @DexIgnore
    @Override // com.fossil.i44, java.util.Comparator
    public int compare(T t, T t2) {
        return a(t) - a(t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof j24) {
            return this.rankMap.equals(((j24) obj).rankMap);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.rankMap.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.explicit(" + this.rankMap.keySet() + ")";
    }
}
