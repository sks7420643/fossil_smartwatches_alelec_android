package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ny4 implements Factory<my4> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<tt4> f2595a;

    @DexIgnore
    public ny4(Provider<tt4> provider) {
        this.f2595a = provider;
    }

    @DexIgnore
    public static ny4 a(Provider<tt4> provider) {
        return new ny4(provider);
    }

    @DexIgnore
    public static my4 c(tt4 tt4) {
        return new my4(tt4);
    }

    @DexIgnore
    /* renamed from: b */
    public my4 get() {
        return c(this.f2595a.get());
    }
}
