package com.fossil;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import com.fossil.a21;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y11 {
    @DexIgnore
    public static /* final */ String e; // = x01.f("ConstraintsCmdHandler");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f4225a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ a21 c;
    @DexIgnore
    public /* final */ h21 d;

    @DexIgnore
    public y11(Context context, int i, a21 a21) {
        this.f4225a = context;
        this.b = i;
        this.c = a21;
        this.d = new h21(this.f4225a, a21.f(), null);
    }

    @DexIgnore
    public void a() {
        List<o31> i = this.c.g().p().j().i();
        ConstraintProxy.a(this.f4225a, i);
        this.d.d(i);
        ArrayList<o31> arrayList = new ArrayList(i.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (o31 o31 : i) {
            String str = o31.f2626a;
            if (currentTimeMillis >= o31.a() && (!o31.b() || this.d.c(str))) {
                arrayList.add(o31);
            }
        }
        for (o31 o312 : arrayList) {
            String str2 = o312.f2626a;
            Intent b2 = x11.b(this.f4225a, str2);
            x01.c().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", str2), new Throwable[0]);
            a21 a21 = this.c;
            a21.k(new a21.b(a21, b2, this.b));
        }
        this.d.e();
    }
}
