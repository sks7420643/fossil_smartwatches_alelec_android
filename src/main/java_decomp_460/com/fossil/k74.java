package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k74 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Class<?> f1870a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public k74(Class<?> cls, int i, int i2) {
        r74.c(cls, "Null dependency anInterface.");
        this.f1870a = cls;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public static k74 e(Class<?> cls) {
        return new k74(cls, 0, 0);
    }

    @DexIgnore
    public static k74 f(Class<?> cls) {
        return new k74(cls, 1, 0);
    }

    @DexIgnore
    public static k74 g(Class<?> cls) {
        return new k74(cls, 1, 1);
    }

    @DexIgnore
    public static k74 h(Class<?> cls) {
        return new k74(cls, 2, 0);
    }

    @DexIgnore
    public Class<?> a() {
        return this.f1870a;
    }

    @DexIgnore
    public boolean b() {
        return this.c == 0;
    }

    @DexIgnore
    public boolean c() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean d() {
        return this.b == 2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof k74)) {
            return false;
        }
        k74 k74 = (k74) obj;
        return this.f1870a == k74.f1870a && this.b == k74.b && this.c == k74.c;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.f1870a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.f1870a);
        sb.append(", type=");
        int i = this.b;
        sb.append(i == 1 ? "required" : i == 0 ? "optional" : "set");
        sb.append(", direct=");
        sb.append(this.c == 0);
        sb.append("}");
        return sb.toString();
    }
}
