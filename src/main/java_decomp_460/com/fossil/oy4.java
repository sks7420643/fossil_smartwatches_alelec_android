package com.fossil;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy4 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<qt4> f2752a; // = new ArrayList();
    @DexIgnore
    public /* final */ SparseArray<a> b; // = new SparseArray<>();
    @DexIgnore
    public /* final */ ny5 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ df5 f2753a;
        @DexIgnore
        public /* final */ ny5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.oy4$a$a")
        /* renamed from: com.fossil.oy4$a$a  reason: collision with other inner class name */
        public static final class C0187a extends qq7 implements rp7<View, tl7> {
            @DexIgnore
            public /* final */ /* synthetic */ ps4 $challenge;
            @DexIgnore
            public /* final */ /* synthetic */ qt4 $history$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0187a(ps4 ps4, a aVar, qt4 qt4) {
                super(1);
                this.$challenge = ps4;
                this.this$0 = aVar;
                this.$history$inlined = qt4;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.rp7
            public /* bridge */ /* synthetic */ tl7 invoke(View view) {
                invoke(view);
                return tl7.f3441a;
            }

            @DexIgnore
            public final void invoke(View view) {
                this.this$0.a().e6(this.$challenge);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(df5 df5, ny5 ny5) {
            super(df5.n());
            pq7.c(df5, "binding");
            pq7.c(ny5, "listener");
            this.f2753a = df5;
            this.b = ny5;
        }

        @DexIgnore
        public final ny5 a() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0090  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0295  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x029e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void b(com.fossil.qt4 r15) {
            /*
            // Method dump skipped, instructions count: 700
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.oy4.a.b(com.fossil.qt4):void");
        }
    }

    @DexIgnore
    public oy4(ny5 ny5) {
        pq7.c(ny5, "listener");
        this.c = ny5;
    }

    @DexIgnore
    /* renamed from: g */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        this.b.put(i, aVar);
        aVar.b(this.f2752a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f2752a.size();
    }

    @DexIgnore
    /* renamed from: h */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        df5 z = df5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemHistoryChallengeLayo\u2026e(inflate, parent, false)");
        return new a(z, this.c);
    }

    @DexIgnore
    public final void i(List<qt4> list) {
        pq7.c(list, "newData");
        this.f2752a.clear();
        this.f2752a.addAll(list);
        notifyDataSetChanged();
    }
}
