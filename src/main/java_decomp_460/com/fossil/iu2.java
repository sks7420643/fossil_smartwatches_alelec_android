package com.fossil;

import com.fossil.e13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu2 extends e13<iu2, a> implements o23 {
    @DexIgnore
    public static /* final */ iu2 zzh;
    @DexIgnore
    public static volatile z23<iu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public lu2 zzd;
    @DexIgnore
    public ju2 zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public String zzg; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends e13.a<iu2, a> implements o23 {
        @DexIgnore
        public a() {
            super(iu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ a(fu2 fu2) {
            this();
        }

        @DexIgnore
        public final a x(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((iu2) this.c).D(str);
            return this;
        }
    }

    /*
    static {
        iu2 iu2 = new iu2();
        zzh = iu2;
        e13.u(iu2.class, iu2);
    }
    */

    @DexIgnore
    public static iu2 N() {
        return zzh;
    }

    @DexIgnore
    public final void D(String str) {
        str.getClass();
        this.zzc |= 8;
        this.zzg = str;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final lu2 G() {
        lu2 lu2 = this.zzd;
        return lu2 == null ? lu2.L() : lu2;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final ju2 I() {
        ju2 ju2 = this.zze;
        return ju2 == null ? ju2.N() : ju2;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean K() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String M() {
        return this.zzg;
    }

    @DexIgnore
    @Override // com.fossil.e13
    public final Object r(int i, Object obj, Object obj2) {
        z23 z23;
        switch (fu2.f1206a[i - 1]) {
            case 1:
                return new iu2();
            case 2:
                return new a(null);
            case 3:
                return e13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1009\u0000\u0002\u1009\u0001\u0003\u1007\u0002\u0004\u1008\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                z23<iu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (iu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new e13.c(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
