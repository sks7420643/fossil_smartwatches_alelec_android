package com.fossil;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.customview.CircleProgressView;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ox4 extends pv5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public /* final */ k g; // = new k(this);
    @DexIgnore
    public po4 h;
    @DexIgnore
    public g37<h45> i;
    @DexIgnore
    public qx4 j;
    @DexIgnore
    public /* final */ TimerViewObserver k; // = new TimerViewObserver();
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ox4.m;
        }

        @DexIgnore
        public final ox4 b() {
            return new ox4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements rp7<View, tl7> {
        @DexIgnore
        public /* final */ /* synthetic */ ox4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ox4 ox4) {
            super(1);
            this.this$0 = ox4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ tl7 invoke(View view) {
            invoke(view);
            return tl7.f3441a;
        }

        @DexIgnore
        public final void invoke(View view) {
            ox4.M6(this.this$0).H();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements InterceptSwipe.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h45 f2737a;
        @DexIgnore
        public /* final */ /* synthetic */ ox4 b;

        @DexIgnore
        public c(h45 h45, ox4 ox4) {
            this.f2737a = h45;
            this.b = ox4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.InterceptSwipe.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f2737a.w;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.f2737a.v;
            pq7.b(flexibleTextView2, "ftvEmpty");
            flexibleTextView2.setVisibility(8);
            ox4.M6(this.b).C();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2738a;

        @DexIgnore
        public d(ox4 ox4) {
            this.f2738a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            h45 h45 = (h45) ox4.K6(this.f2738a).a();
            if (h45 != null) {
                InterceptSwipe interceptSwipe = h45.B;
                pq7.b(interceptSwipe, "swipeRefresh");
                pq7.b(bool, "it");
                interceptSwipe.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2739a;

        @DexIgnore
        public e(ox4 ox4) {
            this.f2739a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ox4 ox4 = this.f2739a;
            pq7.b(bool, "it");
            ox4.X6(bool.booleanValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<ps4> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2740a;

        @DexIgnore
        public f(ox4 ox4) {
            this.f2740a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ox4.s.a();
            local.e(a2, "challengeLive - challenge: " + ps4 + " - " + System.currentTimeMillis());
            if (ps4 != null) {
                this.f2740a.Y6(ps4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cl7<? extends Boolean, ? extends ps4>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2741a;

        @DexIgnore
        public g(ox4 ox4) {
            this.f2741a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ps4> cl7) {
            if (cl7.getFirst().booleanValue()) {
                this.f2741a.W6(cl7.getSecond());
            } else {
                this.f2741a.V6(cl7.getSecond());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<gl7<? extends ks4, ? extends String, ? extends Integer>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2742a;

        @DexIgnore
        public h(ox4 ox4) {
            this.f2742a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<ks4, String, Integer> gl7) {
            ox4 ox4 = this.f2742a;
            pq7.b(gl7, "triple");
            ox4.Z6(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2743a;

        @DexIgnore
        public i(ox4 ox4) {
            this.f2743a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            h45 h45 = (h45) ox4.K6(this.f2743a).a();
            if (h45 == null) {
                return;
            }
            if (cl7.getFirst().booleanValue()) {
                FlexibleTextView flexibleTextView = h45.w;
                pq7.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = h45.w;
            pq7.b(flexibleTextView2, "ftvError");
            flexibleTextView2.setVisibility(8);
            FlexibleTextView flexibleTextView3 = h45.w;
            pq7.b(flexibleTextView3, "ftvError");
            String c = um5.c(flexibleTextView3.getContext(), 2131886231);
            FragmentActivity activity = this.f2743a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, c, 1).show();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2744a;

        @DexIgnore
        public j(ox4 ox4) {
            this.f2744a = ox4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            FlexibleTextView flexibleTextView;
            h45 h45 = (h45) ox4.K6(this.f2744a).a();
            if (h45 != null && (flexibleTextView = h45.z) != null) {
                flexibleTextView.setVisibility(pq7.a(bool, Boolean.FALSE) ? 0 : 8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ox4 f2745a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(ox4 ox4) {
            this.f2745a = ox4;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Boolean valueOf = intent != null ? Boolean.valueOf(intent.getBooleanExtra("set_sync_data_extra", true)) : null;
            ox4.M6(this.f2745a).J(valueOf != null ? valueOf.booleanValue() : true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h45 f2746a;

        @DexIgnore
        public l(h45 h45) {
            this.f2746a = h45;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            TimerTextView timerTextView = this.f2746a.t;
            pq7.b(timerTextView, "ftvCountDown");
            pq7.b(valueAnimator, "it");
            timerTextView.setText(valueAnimator.getAnimatedValue().toString());
        }
    }

    /*
    static {
        String simpleName = ox4.class.getSimpleName();
        pq7.b(simpleName, "BCCurrentChallengeSubTab\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(ox4 ox4) {
        g37<h45> g37 = ox4.i;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ qx4 M6(ox4 ox4) {
        qx4 qx4 = ox4.j;
        if (qx4 != null) {
            return qx4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        g37<h45> g37 = this.i;
        if (g37 != null) {
            h45 a2 = g37.a();
            if (a2 != null) {
                InterceptSwipe interceptSwipe = a2.B;
                pq7.b(interceptSwipe, "swipeRefresh");
                interceptSwipe.setRefreshing(true);
                CircleProgressView circleProgressView = a2.q;
                pq7.b(circleProgressView, "circleProgress");
                fz4.a(circleProgressView, new b(this));
                a2.B.setOnRefreshListener(new c(a2, this));
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final IntentFilter T6() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("set_sync_data_event");
        return intentFilter;
    }

    @DexIgnore
    public final void U6() {
        qx4 qx4 = this.j;
        if (qx4 != null) {
            qx4.x().h(getViewLifecycleOwner(), new d(this));
            qx4 qx42 = this.j;
            if (qx42 != null) {
                qx42.v().h(getViewLifecycleOwner(), new e(this));
                qx4 qx43 = this.j;
                if (qx43 != null) {
                    qx43.t().h(getViewLifecycleOwner(), new f(this));
                    qx4 qx44 = this.j;
                    if (qx44 != null) {
                        qx44.z().h(getViewLifecycleOwner(), new g(this));
                        qx4 qx45 = this.j;
                        if (qx45 != null) {
                            qx45.u().h(getViewLifecycleOwner(), new h(this));
                            qx4 qx46 = this.j;
                            if (qx46 != null) {
                                qx46.w().h(getViewLifecycleOwner(), new i(this));
                                qx4 qx47 = this.j;
                                if (qx47 != null) {
                                    qx47.y().h(getViewLifecycleOwner(), new j(this));
                                } else {
                                    pq7.n("viewModel");
                                    throw null;
                                }
                            } else {
                                pq7.n("viewModel");
                                throw null;
                            }
                        } else {
                            pq7.n("viewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(ps4 ps4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "startDetailScreen - challenge: " + ps4);
        BCWaitingChallengeDetailActivity.a aVar = BCWaitingChallengeDetailActivity.B;
        Fragment requireParentFragment = requireParentFragment();
        pq7.b(requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment, ps4, "joined_challenge", -1, false);
    }

    @DexIgnore
    public final void W6(ps4 ps4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "startOverviewLeaderBoard - challenge: " + ps4);
        BCOverviewLeaderBoardActivity.A.a(this, ps4, null);
    }

    @DexIgnore
    public final void X6(boolean z) {
        g37<h45> g37 = this.i;
        if (g37 != null) {
            h45 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                pq7.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(8);
                if (z) {
                    cl5.c.e();
                    FlexibleTextView flexibleTextView2 = a2.v;
                    pq7.b(flexibleTextView2, "ftvEmpty");
                    flexibleTextView2.setVisibility(0);
                    TimerTextView timerTextView = a2.r;
                    pq7.b(timerTextView, "ftvChallengeInfo");
                    timerTextView.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.s;
                    pq7.b(flexibleTextView3, "ftvChallengeName");
                    flexibleTextView3.setVisibility(8);
                    FlexibleTextView flexibleTextView4 = a2.y;
                    pq7.b(flexibleTextView4, "ftvRanking");
                    flexibleTextView4.setVisibility(8);
                    TimerTextView timerTextView2 = a2.t;
                    pq7.b(timerTextView2, "ftvCountDown");
                    timerTextView2.setVisibility(8);
                    CircleProgressView circleProgressView = a2.q;
                    pq7.b(circleProgressView, "circleProgress");
                    circleProgressView.setVisibility(8);
                    FlexibleTextView flexibleTextView5 = a2.x;
                    pq7.b(flexibleTextView5, "ftvPlayerInfo");
                    flexibleTextView5.setVisibility(8);
                    FlexibleTextView flexibleTextView6 = a2.u;
                    pq7.b(flexibleTextView6, "ftvCountPlayers");
                    flexibleTextView6.setVisibility(8);
                    FlexibleTextView flexibleTextView7 = a2.z;
                    pq7.b(flexibleTextView7, "ftvSetSyncData");
                    flexibleTextView7.setVisibility(8);
                    TimerTextView timerTextView3 = a2.t;
                    pq7.b(timerTextView3, "ftvCountDown");
                    timerTextView3.setTag(null);
                    a2.t.r();
                    a2.r.r();
                    TimerTextView timerTextView4 = a2.r;
                    pq7.b(timerTextView4, "ftvChallengeInfo");
                    timerTextView4.setText("");
                    a2.q.e();
                    return;
                }
                FlexibleTextView flexibleTextView8 = a2.v;
                pq7.b(flexibleTextView8, "ftvEmpty");
                flexibleTextView8.setVisibility(8);
                TimerTextView timerTextView5 = a2.r;
                pq7.b(timerTextView5, "ftvChallengeInfo");
                timerTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView9 = a2.s;
                pq7.b(flexibleTextView9, "ftvChallengeName");
                flexibleTextView9.setVisibility(0);
                FlexibleTextView flexibleTextView10 = a2.y;
                pq7.b(flexibleTextView10, "ftvRanking");
                flexibleTextView10.setVisibility(0);
                TimerTextView timerTextView6 = a2.t;
                pq7.b(timerTextView6, "ftvCountDown");
                timerTextView6.setVisibility(0);
                CircleProgressView circleProgressView2 = a2.q;
                pq7.b(circleProgressView2, "circleProgress");
                circleProgressView2.setVisibility(0);
                FlexibleTextView flexibleTextView11 = a2.x;
                pq7.b(flexibleTextView11, "ftvPlayerInfo");
                flexibleTextView11.setVisibility(0);
                FlexibleTextView flexibleTextView12 = a2.u;
                pq7.b(flexibleTextView12, "ftvCountPlayers");
                flexibleTextView12.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Y6(ps4 ps4) {
        String str;
        String a2;
        long j2 = 0;
        int i2 = 0;
        g37<h45> g37 = this.i;
        if (g37 != null) {
            h45 a3 = g37.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.w;
                pq7.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a3.s;
                pq7.b(flexibleTextView2, "ftvChallengeName");
                flexibleTextView2.setText(ps4.g());
                Integer h2 = ps4.h();
                int intValue = h2 != null ? h2.intValue() : 1;
                FlexibleTextView flexibleTextView3 = a3.u;
                pq7.b(flexibleTextView3, "ftvCountPlayers");
                Object tag = flexibleTextView3.getTag();
                if (!(tag instanceof Integer)) {
                    tag = null;
                }
                Integer num = (Integer) tag;
                int intValue2 = num != null ? num.intValue() : 0;
                if (pq7.a(ps4.n(), "waiting")) {
                    FlexibleTextView flexibleTextView4 = a3.u;
                    pq7.b(flexibleTextView4, "ftvCountPlayers");
                    flexibleTextView4.setTag(Integer.valueOf(intValue));
                    FlexibleTextView flexibleTextView5 = a3.u;
                    pq7.b(flexibleTextView5, "ftvCountPlayers");
                    flexibleTextView5.setText(String.valueOf(intValue));
                } else if (intValue > intValue2) {
                    FlexibleTextView flexibleTextView6 = a3.u;
                    pq7.b(flexibleTextView6, "ftvCountPlayers");
                    flexibleTextView6.setTag(Integer.valueOf(intValue));
                    FlexibleTextView flexibleTextView7 = a3.u;
                    pq7.b(flexibleTextView7, "ftvCountPlayers");
                    flexibleTextView7.setText(String.valueOf(intValue));
                }
                String k2 = ps4.k();
                if (k2 != null && k2.hashCode() == -314497661 && k2.equals("private")) {
                    a3.u.setCompoundDrawablesWithIntrinsicBounds(2131231028, 0, 0, 0);
                } else {
                    a3.u.setCompoundDrawablesWithIntrinsicBounds(2131231029, 0, 0, 0);
                }
                String r = ps4.r();
                if (r != null) {
                    int hashCode = r.hashCode();
                    if (hashCode != -1348781656) {
                        if (hashCode == -637042289 && r.equals("activity_reach_goal")) {
                            TimerTextView timerTextView = a3.t;
                            pq7.b(timerTextView, "ftvCountDown");
                            timerTextView.setTextColor(gl0.d(timerTextView.getContext(), 2131099689));
                            a3.r.setHighlightColour(2131099689);
                        }
                    } else if (r.equals("activity_best_result")) {
                        TimerTextView timerTextView2 = a3.t;
                        pq7.b(timerTextView2, "ftvCountDown");
                        jl5 jl5 = jl5.b;
                        Integer d2 = ps4.d();
                        timerTextView2.setText(jl5.s(d2 != null ? d2.intValue() : 0));
                        TimerTextView timerTextView3 = a3.t;
                        pq7.b(timerTextView3, "ftvCountDown");
                        timerTextView3.setTextColor(gl0.d(timerTextView3.getContext(), 2131099677));
                        a3.r.setHighlightColour(2131099677);
                    }
                }
                String n = ps4.n();
                if (n != null) {
                    int hashCode2 = n.hashCode();
                    if (hashCode2 != 1116313165) {
                        if (hashCode2 == 1550783935 && n.equals("running")) {
                            a3.r.r();
                            String r2 = ps4.r();
                            if (r2 != null) {
                                int hashCode3 = r2.hashCode();
                                if (hashCode3 != -1348781656) {
                                    if (hashCode3 == -637042289 && r2.equals("activity_reach_goal")) {
                                        TimerTextView timerTextView4 = a3.t;
                                        pq7.b(timerTextView4, "ftvCountDown");
                                        Object tag2 = timerTextView4.getTag();
                                        if (!(tag2 instanceof Integer)) {
                                            tag2 = null;
                                        }
                                        if (((Integer) tag2) == null) {
                                            TimerTextView timerTextView5 = a3.t;
                                            pq7.b(timerTextView5, "ftvCountDown");
                                            Integer q = ps4.q();
                                            timerTextView5.setText(q != null ? String.valueOf(q.intValue()) : null);
                                            a3.q.m(1, 1);
                                        }
                                        CircleProgressView circleProgressView = a3.q;
                                        pq7.b(circleProgressView, "circleProgress");
                                        circleProgressView.setProgressColour(gl0.d(circleProgressView.getContext(), 2131099689));
                                    }
                                } else if (r2.equals("activity_best_result")) {
                                    TimerTextView timerTextView6 = a3.t;
                                    Date e2 = ps4.e();
                                    timerTextView6.setTime(e2 != null ? e2.getTime() : 0);
                                    a3.t.setDisplayType(wy4.HOUR_MIN_SEC);
                                    a3.t.setObserver(this.k);
                                    CircleProgressView circleProgressView2 = a3.q;
                                    pq7.b(circleProgressView2, "circleProgress");
                                    circleProgressView2.setProgressColour(gl0.d(circleProgressView2.getContext(), 2131099677));
                                    a3.q.m(1, 1);
                                    Integer d3 = ps4.d();
                                    if (d3 != null) {
                                        i2 = d3.intValue();
                                    }
                                    CircleProgressView circleProgressView3 = a3.q;
                                    long j3 = (long) i2;
                                    Date e3 = ps4.e();
                                    if (e3 != null) {
                                        j2 = e3.getTime();
                                    }
                                    circleProgressView3.j(j3 * 1000, j2);
                                    CircleProgressView circleProgressView4 = a3.q;
                                    circleProgressView4.k(this.k, circleProgressView4.getIdentity());
                                }
                            }
                            TimerTextView timerTextView7 = a3.t;
                            pq7.b(timerTextView7, "ftvCountDown");
                            Object tag3 = timerTextView7.getTag();
                            if (!(tag3 instanceof Integer)) {
                                tag3 = null;
                            }
                            if (((Integer) tag3) == null) {
                                FlexibleTextView flexibleTextView8 = a3.x;
                                pq7.b(flexibleTextView8, "ftvPlayerInfo");
                                flexibleTextView8.setText("--");
                                TimerTextView timerTextView8 = a3.r;
                                pq7.b(timerTextView8, "ftvChallengeInfo");
                                timerTextView8.setText("--");
                            }
                        }
                    } else if (n.equals("waiting")) {
                        TimerTextView timerTextView9 = a3.t;
                        pq7.b(timerTextView9, "ftvCountDown");
                        timerTextView9.setTag(null);
                        String l0 = PortfolioApp.h0.c().l0();
                        ht4 i3 = ps4.i();
                        if (pq7.a(l0, i3 != null ? i3.b() : null)) {
                            a2 = um5.c(PortfolioApp.h0.c(), 2131886250);
                        } else {
                            hz4 hz4 = hz4.f1561a;
                            ht4 i4 = ps4.i();
                            String a4 = i4 != null ? i4.a() : null;
                            ht4 i5 = ps4.i();
                            String c2 = i5 != null ? i5.c() : null;
                            ht4 i6 = ps4.i();
                            if (i6 == null || (str = i6.d()) == null) {
                                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                            }
                            a2 = hz4.a(a4, c2, str);
                        }
                        FlexibleTextView flexibleTextView9 = a3.x;
                        pq7.b(flexibleTextView9, "ftvPlayerInfo");
                        jl5 jl52 = jl5.b;
                        pq7.b(a2, "owner");
                        hr7 hr7 = hr7.f1520a;
                        FlexibleTextView flexibleTextView10 = a3.y;
                        pq7.b(flexibleTextView10, "ftvRanking");
                        String c3 = um5.c(flexibleTextView10.getContext(), 2131886255);
                        pq7.b(c3, "LanguageHelper.getString\u2026ding_Text__CreatedByName)");
                        String format = String.format(c3, Arrays.copyOf(new Object[]{a2}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        flexibleTextView9.setText(jl5.b(jl52, a2, format, 0, 4, null));
                        FlexibleTextView flexibleTextView11 = a3.y;
                        pq7.b(flexibleTextView11, "ftvRanking");
                        hr7 hr72 = hr7.f1520a;
                        FlexibleTextView flexibleTextView12 = a3.y;
                        pq7.b(flexibleTextView12, "ftvRanking");
                        String c4 = um5.c(flexibleTextView12.getContext(), 2131887312);
                        pq7.b(c4, "LanguageHelper.getString\u2026.buddy_challenge_ranking)");
                        String format2 = String.format(c4, Arrays.copyOf(new Object[]{0, Integer.valueOf(intValue)}, 2));
                        pq7.b(format2, "java.lang.String.format(format, *args)");
                        flexibleTextView11.setText(format2);
                        CircleProgressView circleProgressView5 = a3.q;
                        pq7.b(circleProgressView5, "circleProgress");
                        circleProgressView5.setProgressColour(gl0.d(circleProgressView5.getContext(), R.color.transparent));
                        a3.q.m(0, 1);
                        a3.r.setDisplayType(wy4.START_LEFT);
                        TimerTextView timerTextView10 = a3.r;
                        Date m2 = ps4.m();
                        if (m2 != null) {
                            j2 = m2.getTime();
                        }
                        timerTextView10.setTime(j2);
                        a3.r.setObserver(this.k);
                        if (pq7.a("activity_reach_goal", ps4.r())) {
                            TimerTextView timerTextView11 = a3.t;
                            pq7.b(timerTextView11, "ftvCountDown");
                            timerTextView11.setText(String.valueOf(0));
                        }
                    }
                }
            }
        } else {
            pq7.n("binding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0429  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Z6(com.fossil.gl7<com.fossil.ks4, java.lang.String, java.lang.Integer> r19) {
        /*
        // Method dump skipped, instructions count: 1078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ox4.Z6(com.fossil.gl7):void");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().F().a(this);
        po4 po4 = this.h;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(qx4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.j = (qx4) a2;
            getLifecycle().a(this.k);
            qx4 qx4 = this.j;
            if (qx4 != null) {
                qx4.r();
                ct0.b(requireContext()).c(this.g, T6());
                return;
            }
            pq7.n("viewModel");
            throw null;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        h45 h45 = (h45) aq0.f(layoutInflater, 2131558528, viewGroup, false, A6());
        this.i = new g37<>(this, h45);
        pq7.b(h45, "binding");
        return h45.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().c(this.k);
        ct0.b(requireContext()).e(this.g);
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qx4 qx4 = this.j;
        if (qx4 != null) {
            qx4.B();
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qx4 qx4 = this.j;
        if (qx4 != null) {
            qx4.D();
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        S6();
        U6();
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
