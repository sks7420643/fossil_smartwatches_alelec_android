package com.fossil;

import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw5 extends RecyclerView.g<a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public CustomizeWidget f1547a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<Complication> c;
    @DexIgnore
    public Rect d;
    @DexIgnore
    public ArrayList<Complication> e;
    @DexIgnore
    public b f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public CustomizeWidget f1548a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public Complication e;
        @DexIgnore
        public /* final */ /* synthetic */ hw5 f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hw5$a$a")
        /* renamed from: com.fossil.hw5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0116a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public View$OnClickListenerC0116a(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b m;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (m = this.b.f.m()) != null) {
                    m.a(((Complication) this.b.f.e.get(this.b.getAdapterPosition())).getComplicationId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore
            public b(a aVar) {
                this.b = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b m;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (m = this.b.f.m()) != null) {
                    m.b(((Complication) this.b.f.e.get(this.b.getAdapterPosition())).getComplicationId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements CustomizeWidget.b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1549a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public c(a aVar) {
                this.f1549a = aVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                pq7.c(customizeWidget, "view");
                this.f1549a.f.f1547a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hw5 hw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.f = hw5;
            View findViewById = view.findViewById(2131363544);
            pq7.b(findViewById, "view.findViewById(R.id.wc_complication)");
            this.f1548a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363294);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_complication_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362728);
            this.d = view.findViewById(2131362767);
        }

        @DexIgnore
        public final void a(Complication complication) {
            pq7.c(complication, "complication");
            this.f1548a.setOnClickListener(new View$OnClickListenerC0116a(this));
            this.f1548a.setWatchFaceRect(this.f.d);
            this.d.setOnClickListener(new b(this));
            this.e = complication;
            if (complication != null) {
                this.f1548a.O(complication.getComplicationId());
                this.b.setText(um5.d(PortfolioApp.h0.c(), complication.getNameKey(), complication.getName()));
            }
            this.f1548a.setSelectedWc(pq7.a(complication.getComplicationId(), this.f.b));
            if (pq7.a(complication.getComplicationId(), this.f.b)) {
                View view = this.c;
                pq7.b(view, "ivIndicator");
                view.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230956));
            } else {
                View view2 = this.c;
                pq7.b(view2, "ivIndicator");
                view2.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230957));
            }
            View view3 = this.d;
            pq7.b(view3, "ivWarning");
            view3.setVisibility(!ik5.d.f(complication.getComplicationId()) ? 0 : 8);
            this.f1548a.M("complication_list", new c(this));
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);

        @DexIgnore
        void b(String str);
    }

    @DexIgnore
    public hw5(ArrayList<Complication> arrayList, b bVar) {
        pq7.c(arrayList, "mData");
        this.e = arrayList;
        this.f = bVar;
        this.b = "empty";
        this.c = new ArrayList<>();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ hw5(ArrayList arrayList, b bVar, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d("ComplicationsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.f1547a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
        this.f1547a = null;
    }

    @DexIgnore
    public final int l(String str) {
        T t;
        pq7.c(str, "complicationId");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getComplicationId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.e.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final b m() {
        return this.f;
    }

    @DexIgnore
    public final void n(List<Complication> list) {
        pq7.c(list, "data");
        for (Complication complication : list) {
            this.c.add(Complication.copy$default(complication, null, null, null, null, null, null, null, null, null, 511, null));
        }
        q(list);
    }

    @DexIgnore
    /* renamed from: o */
    public void onBindViewHolder(a aVar, int i) {
        pq7.c(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            Complication complication = this.e.get(i);
            pq7.b(complication, "mData[position]");
            aVar.a(complication);
        }
    }

    @DexIgnore
    /* renamed from: p */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        zd5 c2 = zd5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(c2, "ItemComplicationBinding.\u2026.context), parent, false)");
        ConstraintLayout b2 = c2.b();
        pq7.b(b2, "view.root");
        return new a(this, b2);
    }

    @DexIgnore
    public final void q(List<Complication> list) {
        this.e.clear();
        this.e.addAll(list);
        v();
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void r(Rect rect) {
        pq7.c(rect, "rect");
        this.d = rect;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void s(b bVar) {
        pq7.c(bVar, "listener");
        this.f = bVar;
    }

    @DexIgnore
    public final void t(String str) {
        if (str == null) {
            str = "empty";
        }
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void u(HashSet<String> hashSet) {
        pq7.c(hashSet, "selectedComplications");
        ArrayList<Complication> arrayList = this.c;
        List<Complication> arrayList2 = new ArrayList<>();
        for (Complication complication : arrayList) {
            Complication complication2 = complication;
            if (!(hashSet.contains(complication2.getComplicationId()) && (pq7.a(complication2.getComplicationId(), this.b) ^ true))) {
                arrayList2.add(complication);
            }
        }
        q(arrayList2);
    }

    @DexIgnore
    public final void v() {
        T t;
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getComplicationId(), "empty")) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            Collections.swap(this.e, this.e.indexOf(t2), 0);
        }
    }
}
