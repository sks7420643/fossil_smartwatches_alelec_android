package com.fossil;

import android.util.SparseArray;
import java.util.EnumMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z32 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static SparseArray<vy1> f4413a; // = new SparseArray<>();
    @DexIgnore
    public static EnumMap<vy1, Integer> b;

    /*
    static {
        EnumMap<vy1, Integer> enumMap = new EnumMap<>(vy1.class);
        b = enumMap;
        enumMap.put((EnumMap<vy1, Integer>) vy1.DEFAULT, (vy1) 0);
        b.put((EnumMap<vy1, Integer>) vy1.VERY_LOW, (vy1) 1);
        b.put((EnumMap<vy1, Integer>) vy1.HIGHEST, (vy1) 2);
        for (vy1 vy1 : b.keySet()) {
            f4413a.append(b.get(vy1).intValue(), vy1);
        }
    }
    */

    @DexIgnore
    public static int a(vy1 vy1) {
        Integer num = b.get(vy1);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + vy1);
    }

    @DexIgnore
    public static vy1 b(int i) {
        vy1 vy1 = f4413a.get(i);
        if (vy1 != null) {
            return vy1;
        }
        throw new IllegalArgumentException("Unknown Priority for value " + i);
    }
}
