package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy7<T> {
    @DexIgnore
    public static /* final */ b b; // = new b(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f4393a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Throwable f4394a;

        @DexIgnore
        public a(Throwable th) {
            this.f4394a = th;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof a) && pq7.a(this.f4394a, ((a) obj).f4394a);
        }

        @DexIgnore
        public int hashCode() {
            Throwable th = this.f4394a;
            if (th != null) {
                return th.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "Closed(" + this.f4394a + ')';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ yy7(Object obj) {
        this.f4393a = obj;
    }

    @DexIgnore
    public static final /* synthetic */ yy7 a(Object obj) {
        return new yy7(obj);
    }

    @DexIgnore
    public static Object b(Object obj) {
        return obj;
    }

    @DexIgnore
    public static boolean c(Object obj, Object obj2) {
        return (obj2 instanceof yy7) && pq7.a(obj, ((yy7) obj2).f());
    }

    @DexIgnore
    public static int d(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public static String e(Object obj) {
        if (obj instanceof a) {
            return obj.toString();
        }
        return "Value(" + obj + ')';
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return c(this.f4393a, obj);
    }

    @DexIgnore
    public final /* synthetic */ Object f() {
        return this.f4393a;
    }

    @DexIgnore
    public int hashCode() {
        return d(this.f4393a);
    }

    @DexIgnore
    public String toString() {
        return e(this.f4393a);
    }
}
