package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g84 implements b84 {
    @DexIgnore
    @Override // com.fossil.b84
    public void a(String str, Bundle bundle) {
        x74.f().b("Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
    }
}
