package com.fossil;

import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c87 implements Factory<a87> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<zm5> f583a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataRepository> c;
    @DexIgnore
    public /* final */ Provider<uo5> d;
    @DexIgnore
    public /* final */ Provider<FileRepository> e;
    @DexIgnore
    public /* final */ Provider<s77> f;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> g;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> h;

    @DexIgnore
    public c87(Provider<zm5> provider, Provider<UserRepository> provider2, Provider<CustomizeRealDataRepository> provider3, Provider<uo5> provider4, Provider<FileRepository> provider5, Provider<s77> provider6, Provider<DianaAppSettingRepository> provider7, Provider<DianaWatchFaceRepository> provider8) {
        this.f583a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static c87 a(Provider<zm5> provider, Provider<UserRepository> provider2, Provider<CustomizeRealDataRepository> provider3, Provider<uo5> provider4, Provider<FileRepository> provider5, Provider<s77> provider6, Provider<DianaAppSettingRepository> provider7, Provider<DianaWatchFaceRepository> provider8) {
        return new c87(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static a87 c(zm5 zm5, UserRepository userRepository, CustomizeRealDataRepository customizeRealDataRepository, uo5 uo5, FileRepository fileRepository, s77 s77, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        return new a87(zm5, userRepository, customizeRealDataRepository, uo5, fileRepository, s77, dianaAppSettingRepository, dianaWatchFaceRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public a87 get() {
        return c(this.f583a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get());
    }
}
