package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt {
    @DexIgnore
    public /* synthetic */ vt(kq7 kq7) {
    }

    @DexIgnore
    public final wt a(byte b) {
        wt wtVar;
        wt[] values = wt.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                wtVar = null;
                break;
            }
            wtVar = values[i];
            if (wtVar.c == b) {
                break;
            }
            i++;
        }
        return wtVar != null ? wtVar : wt.e;
    }
}
