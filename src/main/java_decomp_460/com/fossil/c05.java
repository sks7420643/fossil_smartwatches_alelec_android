package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c05 {
    @DexIgnore
    public final List<Integer> a(String str) {
        pq7.c(str, "arrayValue");
        ArrayList arrayList = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            return arrayList;
        }
        JSONArray jSONArray = new JSONArray(str);
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            Object obj = jSONArray.get(i);
            if (obj != null) {
                arrayList.add((Integer) obj);
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.Int");
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final String b(List<Integer> list) {
        if (list == null) {
            return "";
        }
        String jSONArray = new JSONArray((Collection) list).toString();
        pq7.b(jSONArray, "JSONArray(value).toString()");
        return jSONArray;
    }
}
