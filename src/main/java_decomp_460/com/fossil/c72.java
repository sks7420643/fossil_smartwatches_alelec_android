package com.fossil;

import com.fossil.z62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c72<R extends z62, S extends z62> {
    @DexIgnore
    public abstract Status a(Status status);

    @DexIgnore
    public abstract t62<S> b(R r);
}
