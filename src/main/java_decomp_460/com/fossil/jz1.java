package com.fossil;

import com.fossil.pz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz1 extends pz1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ pz1.c f1837a;
    @DexIgnore
    public /* final */ pz1.b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends pz1.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public pz1.c f1838a;
        @DexIgnore
        public pz1.b b;

        @DexIgnore
        @Override // com.fossil.pz1.a
        public pz1.a a(pz1.b bVar) {
            this.b = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.pz1.a
        public pz1.a b(pz1.c cVar) {
            this.f1838a = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.pz1.a
        public pz1 c() {
            return new jz1(this.f1838a, this.b, null);
        }
    }

    @DexIgnore
    public /* synthetic */ jz1(pz1.c cVar, pz1.b bVar, a aVar) {
        this.f1837a = cVar;
        this.b = bVar;
    }

    @DexIgnore
    @Override // com.fossil.pz1
    public pz1.b b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.pz1
    public pz1.c c() {
        return this.f1837a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof pz1)) {
            return false;
        }
        pz1.c cVar = this.f1837a;
        if (cVar != null ? cVar.equals(((jz1) obj).f1837a) : ((jz1) obj).f1837a == null) {
            pz1.b bVar = this.b;
            if (bVar == null) {
                if (((jz1) obj).b == null) {
                    z = true;
                    return z;
                }
            } else if (bVar.equals(((jz1) obj).b)) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        pz1.c cVar = this.f1837a;
        int hashCode = cVar == null ? 0 : cVar.hashCode();
        pz1.b bVar = this.b;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return ((hashCode ^ 1000003) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "NetworkConnectionInfo{networkType=" + this.f1837a + ", mobileSubtype=" + this.b + "}";
    }
}
