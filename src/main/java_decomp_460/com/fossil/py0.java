package com.fossil;

import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class py0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ViewGroup f2889a;
    @DexIgnore
    public Runnable b;

    @DexIgnore
    public static py0 b(ViewGroup viewGroup) {
        return (py0) viewGroup.getTag(ny0.transition_current_scene);
    }

    @DexIgnore
    public static void c(ViewGroup viewGroup, py0 py0) {
        viewGroup.setTag(ny0.transition_current_scene, py0);
    }

    @DexIgnore
    public void a() {
        Runnable runnable;
        if (b(this.f2889a) == this && (runnable = this.b) != null) {
            runnable.run();
        }
    }
}
