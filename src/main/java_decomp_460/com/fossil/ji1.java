package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ji1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<bj1> f1762a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ List<bj1> b; // = new ArrayList();
    @DexIgnore
    public boolean c;

    @DexIgnore
    public boolean a(bj1 bj1) {
        boolean z = true;
        if (bj1 != null) {
            boolean remove = this.f1762a.remove(bj1);
            if (!this.b.remove(bj1) && !remove) {
                z = false;
            }
            if (z) {
                bj1.clear();
            }
        }
        return z;
    }

    @DexIgnore
    public void b() {
        for (bj1 bj1 : jk1.j(this.f1762a)) {
            a(bj1);
        }
        this.b.clear();
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (bj1 bj1 : jk1.j(this.f1762a)) {
            if (bj1.isRunning() || bj1.j()) {
                bj1.clear();
                this.b.add(bj1);
            }
        }
    }

    @DexIgnore
    public void d() {
        this.c = true;
        for (bj1 bj1 : jk1.j(this.f1762a)) {
            if (bj1.isRunning()) {
                bj1.pause();
                this.b.add(bj1);
            }
        }
    }

    @DexIgnore
    public void e() {
        for (bj1 bj1 : jk1.j(this.f1762a)) {
            if (!bj1.j() && !bj1.h()) {
                bj1.clear();
                if (!this.c) {
                    bj1.f();
                } else {
                    this.b.add(bj1);
                }
            }
        }
    }

    @DexIgnore
    public void f() {
        this.c = false;
        for (bj1 bj1 : jk1.j(this.f1762a)) {
            if (!bj1.j() && !bj1.isRunning()) {
                bj1.f();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public void g(bj1 bj1) {
        this.f1762a.add(bj1);
        if (!this.c) {
            bj1.f();
            return;
        }
        bj1.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(bj1);
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{numRequests=" + this.f1762a.size() + ", isPaused=" + this.c + "}";
    }
}
