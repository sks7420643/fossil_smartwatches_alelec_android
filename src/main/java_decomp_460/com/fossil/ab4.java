package com.fossil;

import android.util.JsonReader;
import com.fossil.cb4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ab4 implements cb4.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ab4 f242a; // = new ab4();

    @DexIgnore
    public static cb4.a b() {
        return f242a;
    }

    @DexIgnore
    @Override // com.fossil.cb4.a
    public Object a(JsonReader jsonReader) {
        return cb4.t(jsonReader);
    }
}
