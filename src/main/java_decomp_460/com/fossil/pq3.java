package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pq3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public oq3 f2857a;
    @DexIgnore
    public /* final */ /* synthetic */ jq3 b;

    @DexIgnore
    public pq3(jq3 jq3) {
        this.b = jq3;
    }

    @DexIgnore
    public final void a() {
        this.b.h();
        if (this.b.m().s(xg3.p0) && this.f2857a != null) {
            this.b.c.removeCallbacks(this.f2857a);
        }
        if (this.b.m().s(xg3.D0)) {
            this.b.l().w.a(false);
        }
    }

    @DexIgnore
    public final void b(long j) {
        if (this.b.m().s(xg3.p0)) {
            this.f2857a = new oq3(this, this.b.zzm().b(), j);
            this.b.c.postDelayed(this.f2857a, 2000);
        }
    }
}
