package com.fossil;

import com.fossil.iq4;
import com.fossil.ou5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sp6 extends np6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ op6 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ ou5 g;
    @DexIgnore
    public /* final */ ck5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendFeedbackSuccessfullyEvent$1", f = "HelpPresenter.kt", l = {78}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sp6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sp6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendFeedbackSuccessfullyEvent$1$skuModel$1", f = "HelpPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.sp6$a$a  reason: collision with other inner class name */
        public static final class C0221a extends ko7 implements vp7<iv7, qn7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0221a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0221a aVar = new C0221a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super SKUModel> qn7) {
                return ((C0221a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.h0.c().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sp6 sp6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sp6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                C0221a aVar = new C0221a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) g;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                this.this$0.h.l("feedback_submit", hashMap);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendOpenFeedbackEvent$1", f = "HelpPresenter.kt", l = {92}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sp6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "HelpPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super SKUModel> qn7) {
                return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.h0.c().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sp6 sp6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sp6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) g;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.h.l("feedback_open", hashMap);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.e<ou5.c, ou5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ sp6 f3285a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ ou5.c $responseValue;

            @DexIgnore
            public a(ou5.c cVar) {
                this.$responseValue = cVar;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = sp6.i;
                local.d(str, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                FLogger.INSTANCE.getLocal().d(sp6.i, "getRequestSubject");
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                FLogger.INSTANCE.getLocal().d(sp6.i, "getTags");
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(sp6 sp6) {
            this.f3285a = sp6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ou5.a aVar) {
            pq7.c(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(sp6.i, "sendFeedback onError");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ou5.c cVar) {
            pq7.c(cVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = sp6.i;
            local.d(str, "sendFeedback onSuccess zendeskUrl " + ZendeskConfig.INSTANCE.getZendeskUrl());
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(cVar.f()).withEmailIdentifier(cVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(cVar.b());
            this.f3285a.e.j0(new a(cVar));
        }
    }

    /*
    static {
        String simpleName = sp6.class.getSimpleName();
        pq7.b(simpleName, "HelpPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public sp6(op6 op6, DeviceRepository deviceRepository, ou5 ou5, ck5 ck5) {
        pq7.c(op6, "mView");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(ou5, "mGetZendeskInformation");
        pq7.c(ck5, "mAnalyticsHelper");
        this.e = op6;
        this.f = deviceRepository;
        this.g = ou5;
        this.h = ck5;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(i, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.np6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.np6
    public void o() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.np6
    public void p(String str) {
        pq7.c(str, "subject");
        this.g.e(new ou5.b(str), new c(this));
    }

    @DexIgnore
    public void u() {
        this.e.M5(this);
    }
}
