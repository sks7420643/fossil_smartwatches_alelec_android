package com.fossil;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.fossil.af1;
import com.fossil.wb1;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pj5 implements af1<qj5, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements wb1<InputStream> {
        @DexIgnore
        public volatile boolean b;
        @DexIgnore
        public /* final */ qj5 c;

        @DexIgnore
        public a(pj5 pj5, qj5 qj5) {
            pq7.c(qj5, "mAppIconModel");
            this.c = qj5;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public gb1 c() {
            return gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void cancel() {
            this.b = true;
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public void d(sa1 sa1, wb1.a<? super InputStream> aVar) {
            pq7.c(sa1, "priority");
            pq7.c(aVar, Constants.CALLBACK);
            try {
                Drawable applicationIcon = PortfolioApp.h0.c().getPackageManager().getApplicationIcon(this.c.c().getIdentifier());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap i = i37.i(applicationIcon);
                if (i != null) {
                    i.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                }
                aVar.e(this.b ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            } catch (PackageManager.NameNotFoundException e) {
                aVar.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.wb1
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements bf1<qj5, InputStream> {
        @DexIgnore
        /* renamed from: a */
        public pj5 b(ef1 ef1) {
            pq7.c(ef1, "multiFactory");
            return new pj5();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<InputStream> b(qj5 qj5, int i, int i2, ob1 ob1) {
        pq7.c(qj5, "appIconModel");
        pq7.c(ob1, "options");
        return new af1.a<>(qj5, new a(this, qj5));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(qj5 qj5) {
        pq7.c(qj5, "appIconModel");
        return true;
    }
}
