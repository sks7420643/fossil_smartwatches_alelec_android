package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fq3 implements Runnable {
    @DexIgnore
    public /* final */ gq3 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ kl3 d;
    @DexIgnore
    public /* final */ Intent e;

    @DexIgnore
    public fq3(gq3 gq3, int i, kl3 kl3, Intent intent) {
        this.b = gq3;
        this.c = i;
        this.d = kl3;
        this.e = intent;
    }

    @DexIgnore
    public final void run() {
        this.b.d(this.c, this.d, this.e);
    }
}
