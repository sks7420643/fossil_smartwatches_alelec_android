package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yi7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Context c;
    @DexIgnore
    public /* final */ /* synthetic */ jg7 d;

    @DexIgnore
    public yi7(String str, Context context, jg7 jg7) {
        this.b = str;
        this.c = context;
        this.d = jg7;
    }

    @DexIgnore
    public final void run() {
        try {
            synchronized (ig7.k) {
                if (ig7.k.size() >= fg7.A()) {
                    th7 th7 = ig7.m;
                    th7.f("The number of page events exceeds the maximum value " + Integer.toString(fg7.A()));
                    return;
                }
                String unused = ig7.i = this.b;
                if (ig7.k.containsKey(ig7.i)) {
                    th7 th72 = ig7.m;
                    th72.d("Duplicate PageID : " + ig7.i + ", onResume() repeated?");
                    return;
                }
                ig7.k.put(ig7.i, Long.valueOf(System.currentTimeMillis()));
                ig7.a(this.c, true, this.d);
            }
        } catch (Throwable th) {
            ig7.m.e(th);
            ig7.f(this.c, th);
        }
    }
}
