package com.fossil;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w54 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Map<Class<?>, Class<?>> f3885a;
    @DexIgnore
    public static /* final */ Map<Class<?>, Class<?>> b;

    /*
    static {
        HashMap hashMap = new HashMap(16);
        HashMap hashMap2 = new HashMap(16);
        a(hashMap, hashMap2, Boolean.TYPE, Boolean.class);
        a(hashMap, hashMap2, Byte.TYPE, Byte.class);
        a(hashMap, hashMap2, Character.TYPE, Character.class);
        a(hashMap, hashMap2, Double.TYPE, Double.class);
        a(hashMap, hashMap2, Float.TYPE, Float.class);
        a(hashMap, hashMap2, Integer.TYPE, Integer.class);
        a(hashMap, hashMap2, Long.TYPE, Long.class);
        a(hashMap, hashMap2, Short.TYPE, Short.class);
        a(hashMap, hashMap2, Void.TYPE, Void.class);
        f3885a = Collections.unmodifiableMap(hashMap);
        b = Collections.unmodifiableMap(hashMap2);
    }
    */

    @DexIgnore
    public static void a(Map<Class<?>, Class<?>> map, Map<Class<?>, Class<?>> map2, Class<?> cls, Class<?> cls2) {
        map.put(cls, cls2);
        map2.put(cls2, cls);
    }

    @DexIgnore
    public static Set<Class<?>> b() {
        return b.keySet();
    }

    @DexIgnore
    public static <T> Class<T> c(Class<T> cls) {
        i14.l(cls);
        Class<T> cls2 = (Class<T>) b.get(cls);
        return cls2 == null ? cls : cls2;
    }

    @DexIgnore
    public static <T> Class<T> d(Class<T> cls) {
        i14.l(cls);
        Class<T> cls2 = (Class<T>) f3885a.get(cls);
        return cls2 == null ? cls : cls2;
    }
}
