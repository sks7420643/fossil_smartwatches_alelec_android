package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv3 implements Parcelable.Creator<uv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uv3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        byte b = 0;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            switch (ad2.l(t)) {
                case 2:
                    i = ad2.v(parcel, t);
                    break;
                case 3:
                    str7 = ad2.f(parcel, t);
                    break;
                case 4:
                    str6 = ad2.f(parcel, t);
                    break;
                case 5:
                    str5 = ad2.f(parcel, t);
                    break;
                case 6:
                    str4 = ad2.f(parcel, t);
                    break;
                case 7:
                    str3 = ad2.f(parcel, t);
                    break;
                case 8:
                    str2 = ad2.f(parcel, t);
                    break;
                case 9:
                    b = ad2.o(parcel, t);
                    break;
                case 10:
                    b2 = ad2.o(parcel, t);
                    break;
                case 11:
                    b3 = ad2.o(parcel, t);
                    break;
                case 12:
                    b4 = ad2.o(parcel, t);
                    break;
                case 13:
                    str = ad2.f(parcel, t);
                    break;
                default:
                    ad2.B(parcel, t);
                    break;
            }
        }
        ad2.k(parcel, C);
        return new uv3(i, str7, str6, str5, str4, str3, str2, b, b2, b3, b4, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uv3[] newArray(int i) {
        return new uv3[i];
    }
}
