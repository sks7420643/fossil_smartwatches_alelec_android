package com.fossil;

import com.fossil.s32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class z12 implements s32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ k22 f4405a;

    @DexIgnore
    public z12(k22 k22) {
        this.f4405a = k22;
    }

    @DexIgnore
    public static s32.a b(k22 k22) {
        return new z12(k22);
    }

    @DexIgnore
    @Override // com.fossil.s32.a
    public Object a() {
        return Integer.valueOf(this.f4405a.cleanUp());
    }
}
