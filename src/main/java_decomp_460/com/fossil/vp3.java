package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ or3 d;
    @DexIgnore
    public /* final */ /* synthetic */ u93 e;
    @DexIgnore
    public /* final */ /* synthetic */ fp3 f;

    @DexIgnore
    public vp3(fp3 fp3, String str, String str2, or3 or3, u93 u93) {
        this.f = fp3;
        this.b = str;
        this.c = str2;
        this.d = or3;
        this.e = u93;
    }

    @DexIgnore
    public final void run() {
        ArrayList<Bundle> arrayList = new ArrayList<>();
        try {
            cl3 cl3 = this.f.d;
            if (cl3 == null) {
                this.f.d().F().c("Failed to get conditional properties; not connected to service", this.b, this.c);
                return;
            }
            ArrayList<Bundle> q0 = kr3.q0(cl3.V0(this.b, this.c, this.d));
            this.f.e0();
            this.f.k().R(this.e, q0);
        } catch (RemoteException e2) {
            this.f.d().F().d("Failed to get conditional properties; remote exception", this.b, this.c, e2);
        } finally {
            this.f.k().R(this.e, arrayList);
        }
    }
}
