package com.fossil;

import com.fossil.c44;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Field f3715a;

        @DexIgnore
        public b(Field field) {
            this.f3715a = field;
            field.setAccessible(true);
        }

        @DexIgnore
        public void a(T t, int i) {
            try {
                this.f3715a.set(t, Integer.valueOf(i));
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }

        @DexIgnore
        public void b(T t, Object obj) {
            try {
                this.f3715a.set(t, obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls, String str) {
        try {
            return new b<>(cls.getDeclaredField(str));
        } catch (NoSuchFieldException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.c44<E> */
    /* JADX WARN: Multi-variable type inference failed */
    public static <E> void b(c44<E> c44, ObjectInputStream objectInputStream, int i) throws IOException, ClassNotFoundException {
        for (int i2 = 0; i2 < i; i2++) {
            c44.add(objectInputStream.readObject(), objectInputStream.readInt());
        }
    }

    @DexIgnore
    public static int c(ObjectInputStream objectInputStream) throws IOException {
        return objectInputStream.readInt();
    }

    @DexIgnore
    public static <K, V> void d(y34<K, V> y34, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(y34.asMap().size());
        for (Map.Entry<K, Collection<V>> entry : y34.asMap().entrySet()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeInt(entry.getValue().size());
            for (V v : entry.getValue()) {
                objectOutputStream.writeObject(v);
            }
        }
    }

    @DexIgnore
    public static <E> void e(c44<E> c44, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(c44.entrySet().size());
        for (c44.a<E> aVar : c44.entrySet()) {
            objectOutputStream.writeObject(aVar.getElement());
            objectOutputStream.writeInt(aVar.getCount());
        }
    }
}
