package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s94 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f3221a; // = null;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, String> b; // = new ConcurrentHashMap<>();

    @DexIgnore
    public static String c(String str) {
        if (str == null) {
            return str;
        }
        String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    @DexIgnore
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.b);
    }

    @DexIgnore
    public String b() {
        return this.f3221a;
    }

    @DexIgnore
    public void d(String str, String str2) {
        if (str != null) {
            String c = c(str);
            if (this.b.size() < 64 || this.b.containsKey(c)) {
                this.b.put(c, str2 == null ? "" : c(str2));
            } else {
                x74.f().b("Exceeded maximum number of custom attributes (64)");
            }
        } else {
            throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
    }

    @DexIgnore
    public void e(String str) {
        this.f3221a = c(str);
    }
}
