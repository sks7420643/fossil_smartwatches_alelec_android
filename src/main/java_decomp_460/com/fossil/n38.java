package com.fossil;

import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n38 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final n38 f2461a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements n38 {
        @DexIgnore
        @Override // com.fossil.n38
        public boolean a(int i, List<e38> list) {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.n38
        public boolean b(int i, List<e38> list, boolean z) {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.n38
        public void c(int i, d38 d38) {
        }

        @DexIgnore
        @Override // com.fossil.n38
        public boolean d(int i, k48 k48, int i2, boolean z) throws IOException {
            k48.skip((long) i2);
            return true;
        }
    }

    @DexIgnore
    boolean a(int i, List<e38> list);

    @DexIgnore
    boolean b(int i, List<e38> list, boolean z);

    @DexIgnore
    void c(int i, d38 d38);

    @DexIgnore
    boolean d(int i, k48 k48, int i2, boolean z) throws IOException;
}
