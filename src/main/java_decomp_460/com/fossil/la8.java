package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la8 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public b f2165a;
    @DexIgnore
    public a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public long f2166a;
        @DexIgnore
        public long b;

        @DexIgnore
        public final long a() {
            return this.b;
        }

        @DexIgnore
        public final long b() {
            return this.f2166a;
        }

        @DexIgnore
        public final void c(long j) {
            this.b = j;
        }

        @DexIgnore
        public final void d(long j) {
            this.f2166a = j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public int f2167a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public final int a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.f2167a;
        }

        @DexIgnore
        public final void e(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void f(int i) {
            this.b = i;
        }

        @DexIgnore
        public final void g(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void h(int i) {
            this.f2167a = i;
        }
    }

    @DexIgnore
    public final String[] a() {
        a aVar = this.b;
        if (aVar != null) {
            long b2 = aVar.b();
            a aVar2 = this.b;
            if (aVar2 != null) {
                List<Number> d0 = em7.d0(new Long[]{Long.valueOf(b2), Long.valueOf(aVar2.a())});
                ArrayList arrayList = new ArrayList(im7.m(d0, 10));
                for (Number number : d0) {
                    arrayList.add(String.valueOf(number.longValue()));
                }
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    return (String[]) array;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
            pq7.n("durationConstraint");
            throw null;
        }
        pq7.n("durationConstraint");
        throw null;
    }

    @DexIgnore
    public final String b() {
        return "duration >=? AND duration <=?";
    }

    @DexIgnore
    public final void c(a aVar) {
        pq7.c(aVar, "<set-?>");
        this.b = aVar;
    }

    @DexIgnore
    public final void d(boolean z) {
    }

    @DexIgnore
    public final void e(b bVar) {
        pq7.c(bVar, "<set-?>");
        this.f2165a = bVar;
    }

    @DexIgnore
    public final String[] f() {
        b bVar = this.f2165a;
        if (bVar != null) {
            int d = bVar.d();
            b bVar2 = this.f2165a;
            if (bVar2 != null) {
                int b2 = bVar2.b();
                b bVar3 = this.f2165a;
                if (bVar3 != null) {
                    int c = bVar3.c();
                    b bVar4 = this.f2165a;
                    if (bVar4 != null) {
                        List<Number> d0 = em7.d0(new Integer[]{Integer.valueOf(d), Integer.valueOf(b2), Integer.valueOf(c), Integer.valueOf(bVar4.a())});
                        ArrayList arrayList = new ArrayList(im7.m(d0, 10));
                        for (Number number : d0) {
                            arrayList.add(String.valueOf(number.intValue()));
                        }
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            return (String[]) array;
                        }
                        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    pq7.n("sizeConstraint");
                    throw null;
                }
                pq7.n("sizeConstraint");
                throw null;
            }
            pq7.n("sizeConstraint");
            throw null;
        }
        pq7.n("sizeConstraint");
        throw null;
    }

    @DexIgnore
    public final String g() {
        return "width >= ? AND width <= ? AND height >= ? AND height <=?";
    }
}
