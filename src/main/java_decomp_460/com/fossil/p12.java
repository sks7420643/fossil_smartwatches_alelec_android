package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.AlarmManagerSchedulerBroadcastReceiver;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p12 implements h22 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2764a;
    @DexIgnore
    public /* final */ k22 b;
    @DexIgnore
    public AlarmManager c;
    @DexIgnore
    public /* final */ v12 d;
    @DexIgnore
    public /* final */ t32 e;

    @DexIgnore
    public p12(Context context, k22 k22, AlarmManager alarmManager, t32 t32, v12 v12) {
        this.f2764a = context;
        this.b = k22;
        this.c = alarmManager;
        this.e = t32;
        this.d = v12;
    }

    @DexIgnore
    public p12(Context context, k22 k22, t32 t32, v12 v12) {
        this(context, k22, (AlarmManager) context.getSystemService(Alarm.TABLE_NAME), t32, v12);
    }

    @DexIgnore
    @Override // com.fossil.h22
    public void a(h02 h02, int i) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("backendName", h02.b());
        builder.appendQueryParameter("priority", String.valueOf(z32.a(h02.d())));
        if (h02.c() != null) {
            builder.appendQueryParameter(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(h02.c(), 0));
        }
        Intent intent = new Intent(this.f2764a, AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(builder.build());
        intent.putExtra("attemptNumber", i);
        if (b(intent)) {
            c12.a("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", h02);
            return;
        }
        long c0 = this.b.c0(h02);
        long f = this.d.f(h02.d(), c0, i);
        c12.b("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", h02, Long.valueOf(f), Long.valueOf(c0), Integer.valueOf(i));
        this.c.set(3, this.e.a() + f, PendingIntent.getBroadcast(this.f2764a, 0, intent, 0));
    }

    @DexIgnore
    public boolean b(Intent intent) {
        return PendingIntent.getBroadcast(this.f2764a, 0, intent, 536870912) != null;
    }
}
