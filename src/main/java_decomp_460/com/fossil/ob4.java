package com.fossil;

import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ob4 implements FilenameFilter {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ob4 f2665a; // = new ob4();

    @DexIgnore
    public static FilenameFilter a() {
        return f2665a;
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return sb4.s(file, str);
    }
}
