package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class im7 extends hm7 {
    @DexIgnore
    public static final <T> int m(Iterable<? extends T> iterable, int i) {
        pq7.c(iterable, "$this$collectionSizeOrDefault");
        return iterable instanceof Collection ? ((Collection) iterable).size() : i;
    }

    @DexIgnore
    public static final <T> Collection<T> n(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        pq7.c(iterable, "$this$convertToSetForSetOperationWith");
        pq7.c(iterable2, "source");
        if (iterable instanceof Set) {
            return (Collection) iterable;
        }
        if (!(iterable instanceof Collection)) {
            return pm7.f0(iterable);
        }
        if ((iterable2 instanceof Collection) && ((Collection) iterable2).size() < 2) {
            return (Collection) iterable;
        }
        Collection<T> collection = (Collection) iterable;
        return p(collection) ? pm7.f0(iterable) : collection;
    }

    @DexIgnore
    public static final <T> List<T> o(Iterable<? extends Iterable<? extends T>> iterable) {
        pq7.c(iterable, "$this$flatten");
        ArrayList arrayList = new ArrayList();
        Iterator<? extends Iterable<? extends T>> it = iterable.iterator();
        while (it.hasNext()) {
            mm7.s(arrayList, (Iterable) it.next());
        }
        return arrayList;
    }

    @DexIgnore
    public static final <T> boolean p(Collection<? extends T> collection) {
        return collection.size() > 2 && (collection instanceof ArrayList);
    }
}
