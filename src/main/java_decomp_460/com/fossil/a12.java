package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a12 implements Factory<z02> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<Context> f180a;
    @DexIgnore
    public /* final */ Provider<x02> b;

    @DexIgnore
    public a12(Provider<Context> provider, Provider<x02> provider2) {
        this.f180a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static a12 a(Provider<Context> provider, Provider<x02> provider2) {
        return new a12(provider, provider2);
    }

    @DexIgnore
    /* renamed from: b */
    public z02 get() {
        return new z02(this.f180a.get(), this.b.get());
    }
}
