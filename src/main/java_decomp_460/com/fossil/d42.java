package com.fossil;

import android.os.Bundle;
import com.fossil.m62;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d42 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ m62.g<yk2> f735a; // = new m62.g<>();
    @DexIgnore
    public static /* final */ m62.g<u42> b; // = new m62.g<>();
    @DexIgnore
    public static /* final */ m62.a<yk2, a> c; // = new s52();
    @DexIgnore
    public static /* final */ m62.a<u42, GoogleSignInOptions> d; // = new t52();
    @DexIgnore
    public static /* final */ m62<GoogleSignInOptions> e; // = new m62<>("Auth.GOOGLE_SIGN_IN_API", d, b);
    @DexIgnore
    public static /* final */ i42 f; // = new t42();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a implements m62.d.c, m62.d {
        @DexIgnore
        public /* final */ boolean b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d42$a$a")
        @Deprecated
        /* renamed from: com.fossil.d42$a$a  reason: collision with other inner class name */
        public static class C0045a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public Boolean f736a; // = Boolean.FALSE;

            @DexIgnore
            public a a() {
                return new a(this);
            }
        }

        /*
        static {
            new C0045a().a();
        }
        */

        @DexIgnore
        public a(C0045a aVar) {
            this.b = aVar.f736a.booleanValue();
        }

        @DexIgnore
        public final Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", null);
            bundle.putBoolean("force_save_dialog", this.b);
            return bundle;
        }
    }

    /*
    static {
        m62<f42> m62 = e42.c;
        new m62("Auth.CREDENTIALS_API", c, f735a);
        g42 g42 = e42.d;
    }
    */
}
