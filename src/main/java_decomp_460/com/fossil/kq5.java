package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq5<T> extends iq5<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ T f2066a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public kq5(T t, boolean z) {
        super(null);
        this.f2066a = t;
        this.b = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ kq5(Object obj, boolean z, int i, kq7 kq7) {
        this(obj, (i & 2) != 0 ? false : z);
    }

    @DexIgnore
    public final T a() {
        return this.f2066a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof kq5) {
                kq5 kq5 = (kq5) obj;
                if (!pq7.a(this.f2066a, kq5.f2066a) || this.b != kq5.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.f2066a;
        int hashCode = t != null ? t.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + ((Object) this.f2066a) + ", isFromCache=" + this.b + ")";
    }
}
