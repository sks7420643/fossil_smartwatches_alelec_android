package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Object f2124a; // = new vz7("CONDITION_FALSE");

    @DexIgnore
    public static final Object a() {
        return f2124a;
    }

    @DexIgnore
    public static final lz7 b(Object obj) {
        lz7 lz7;
        sz7 sz7 = (sz7) (!(obj instanceof sz7) ? null : obj);
        if (sz7 != null && (lz7 = sz7.f3341a) != null) {
            return lz7;
        }
        if (obj != null) {
            return (lz7) obj;
        }
        throw new il7("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
}
