package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h26 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ArrayList<QuickResponseMessage> f1415a;
    @DexIgnore
    public a b;
    @DexIgnore
    public int c;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object T2();  // void declaration

        @DexIgnore
        void a1(QuickResponseMessage quickResponseMessage);

        @DexIgnore
        void x5(int i, String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public RTLImageView b;
        @DexIgnore
        public FlexibleEditText c;
        @DexIgnore
        public ConstraintLayout d;
        @DexIgnore
        public View e;
        @DexIgnore
        public QuickResponseMessage f;
        @DexIgnore
        public RTLImageView g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public /* final */ /* synthetic */ h26 k;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements TextWatcher {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public void afterTextChanged(Editable editable) {
                if (this.b.c.isEnabled()) {
                    int length = String.valueOf(editable).length();
                    if (length >= this.b.k.g()) {
                        this.b.h = true;
                        if (!TextUtils.isEmpty(this.b.i)) {
                            this.b.c.setBackgroundResource(2131230862);
                            this.b.c.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.b.i)));
                            return;
                        }
                        this.b.c.setBackgroundResource(2131230862);
                        this.b.c.setBackgroundTintList(ColorStateList.valueOf(-65536));
                    } else if (length != 0) {
                        this.b.g();
                    } else {
                        this.b.c.setHint(um5.c(PortfolioApp.h0.c(), 2131886126));
                    }
                }
            }

            @DexIgnore
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @DexIgnore
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (this.b.c.hasFocus()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onTextChanged id ");
                    QuickResponseMessage quickResponseMessage = this.b.f;
                    sb.append(quickResponseMessage != null ? Integer.valueOf(quickResponseMessage.getId()) : null);
                    sb.append(" message ");
                    QuickResponseMessage quickResponseMessage2 = this.b.f;
                    sb.append(quickResponseMessage2 != null ? quickResponseMessage2.getResponse() : null);
                    local.d("QuickResponseAdapter", sb.toString());
                    a h = this.b.k.h();
                    QuickResponseMessage quickResponseMessage3 = this.b.f;
                    Integer valueOf = quickResponseMessage3 != null ? Integer.valueOf(quickResponseMessage3.getId()) : null;
                    if (valueOf != null) {
                        h.x5(valueOf.intValue(), String.valueOf(charSequence));
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h26$b$b")
        /* renamed from: com.fossil.h26$b$b  reason: collision with other inner class name */
        public static final class View$OnFocusChangeListenerC0102b implements View.OnFocusChangeListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnFocusChangeListenerC0102b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onFocusChange(View view, boolean z) {
                if (!z) {
                    this.b.g();
                    this.b.c.setEnabled(false);
                    this.b.k.h().T2();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(h26 h26, View view) {
            super(view);
            pq7.c(view, "view");
            this.k = h26;
            View findViewById = view.findViewById(2131362744);
            if (findViewById != null) {
                this.b = (RTLImageView) findViewById;
                View findViewById2 = view.findViewById(2131362302);
                if (findViewById2 != null) {
                    this.c = (FlexibleEditText) findViewById2;
                    View findViewById3 = view.findViewById(nw3.container);
                    if (findViewById3 != null) {
                        this.d = (ConstraintLayout) findViewById3;
                        View findViewById4 = view.findViewById(2131362783);
                        if (findViewById4 != null) {
                            this.e = findViewById4;
                            View findViewById5 = view.findViewById(2131361943);
                            pq7.b(findViewById5, "view.findViewById(R.id.bt_edit)");
                            this.g = (RTLImageView) findViewById5;
                            this.i = qn5.l.a().d("error");
                            this.j = qn5.l.a().d("nonBrandSurface");
                            String d2 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                            String d3 = qn5.l.a().d("nonBrandSeparatorLine");
                            if (!TextUtils.isEmpty(d2)) {
                                this.d.setBackgroundColor(Color.parseColor(d2));
                            }
                            if (!TextUtils.isEmpty(d3)) {
                                this.e.setBackgroundColor(Color.parseColor(d3));
                            }
                            this.c.setEnabled(false);
                            this.b.setOnClickListener(this);
                            this.g.setOnClickListener(this);
                            this.c.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(h26.g())});
                            this.c.addTextChangedListener(new a(this));
                            this.c.setOnFocusChangeListener(new View$OnFocusChangeListenerC0102b(this));
                            return;
                        }
                        throw new il7("null cannot be cast to non-null type android.view.View");
                    }
                    throw new il7("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout");
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleEditText");
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.view.RTLImageView");
        }

        @DexIgnore
        public final void f(QuickResponseMessage quickResponseMessage) {
            pq7.c(quickResponseMessage, "response");
            this.f = quickResponseMessage;
            if (quickResponseMessage.getResponse().length() > 0) {
                this.c.setText(quickResponseMessage.getResponse());
                quickResponseMessage.getResponse();
                return;
            }
            this.c.setHint(um5.c(PortfolioApp.h0.c(), 2131886126));
        }

        @DexIgnore
        public final void g() {
            if (this.h) {
                if (!TextUtils.isEmpty(this.j)) {
                    this.c.setBackground(null);
                    this.c.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.j)));
                } else {
                    this.c.setBackground(null);
                    this.c.setBackgroundTintList(ColorStateList.valueOf(-1));
                }
                this.h = false;
            }
        }

        @DexIgnore
        public void onClick(View view) {
            Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
            if (valueOf != null && valueOf.intValue() == 2131362744) {
                QuickResponseMessage quickResponseMessage = this.f;
                if (quickResponseMessage != null) {
                    this.k.h().a1(quickResponseMessage);
                }
            } else if (valueOf != null && valueOf.intValue() == 2131361943) {
                this.c.setEnabled(true);
                this.c.requestFocus();
                this.c.e();
                FlexibleEditText flexibleEditText = this.c;
                Editable text = flexibleEditText.getText();
                Integer valueOf2 = text != null ? Integer.valueOf(text.length()) : null;
                if (valueOf2 != null) {
                    flexibleEditText.setSelection(valueOf2.intValue());
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public h26(ArrayList<QuickResponseMessage> arrayList, a aVar, int i) {
        pq7.c(arrayList, "mData");
        pq7.c(aVar, "mListener");
        this.f1415a = arrayList;
        this.b = aVar;
        this.c = i;
    }

    @DexIgnore
    public final int g() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f1415a.size();
    }

    @DexIgnore
    public final a h() {
        return this.b;
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        QuickResponseMessage quickResponseMessage = this.f1415a.get(i);
        pq7.b(quickResponseMessage, "mData[position]");
        bVar.f(quickResponseMessage);
    }

    @DexIgnore
    /* renamed from: j */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558703, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026_response, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void k(List<QuickResponseMessage> list) {
        pq7.c(list, "data");
        this.f1415a.clear();
        for (T t : list) {
            QuickResponseMessage quickResponseMessage = new QuickResponseMessage(t.getResponse());
            quickResponseMessage.setId(t.getId());
            this.f1415a.add(quickResponseMessage);
        }
        notifyDataSetChanged();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "notifyDataSetChanged " + this.f1415a);
    }
}
