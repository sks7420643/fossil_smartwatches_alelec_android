package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x42 extends q42 {
    @DexIgnore
    public /* final */ /* synthetic */ w42 b;

    @DexIgnore
    public x42(w42 w42) {
        this.b = w42;
    }

    @DexIgnore
    @Override // com.fossil.q42, com.fossil.e52
    public final void A(Status status) throws RemoteException {
        this.b.j(status);
    }
}
