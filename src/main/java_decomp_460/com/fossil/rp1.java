package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public rp1 createFromParcel(Parcel parcel) {
            return new rp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public rp1[] newArray(int i) {
            return new rp1[i];
        }
    }

    @DexIgnore
    public rp1(byte b) {
        super(np1.AUTHENTICATION_REQUEST, b);
    }

    @DexIgnore
    public /* synthetic */ rp1(Parcel parcel, kq7 kq7) {
        super(parcel);
    }
}
