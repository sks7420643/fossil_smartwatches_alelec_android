package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ki2 implements Parcelable.Creator<DataSet> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataSet createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        ArrayList arrayList2 = null;
        uh2 uh2 = null;
        int i = 0;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 1) {
                uh2 = (uh2) ad2.e(parcel, t, uh2.CREATOR);
            } else if (l == 1000) {
                i = ad2.v(parcel, t);
            } else if (l == 3) {
                ad2.x(parcel, t, arrayList, ki2.class.getClassLoader());
            } else if (l == 4) {
                arrayList2 = ad2.j(parcel, t, uh2.CREATOR);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                z = ad2.m(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new DataSet(i, uh2, arrayList, arrayList2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataSet[] newArray(int i) {
        return new DataSet[i];
    }
}
