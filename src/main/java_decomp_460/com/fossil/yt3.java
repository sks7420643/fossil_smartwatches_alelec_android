package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wt3 b;

    @DexIgnore
    public yt3(wt3 wt3) {
        this.b = wt3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onCanceled();
            }
        }
    }
}
