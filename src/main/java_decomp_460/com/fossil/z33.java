package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z33 extends x33<w33, w33> {
    @DexIgnore
    public static void m(Object obj, w33 w33) {
        ((e13) obj).zzb = w33;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.x33
    public final /* synthetic */ w33 a() {
        return w33.g();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, long] */
    @Override // com.fossil.x33
    public final /* synthetic */ void b(w33 w33, int i, long j) {
        w33.c(i << 3, Long.valueOf(j));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, com.fossil.xz2] */
    @Override // com.fossil.x33
    public final /* synthetic */ void c(w33 w33, int i, xz2 xz2) {
        w33.c((i << 3) | 2, xz2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.r43] */
    @Override // com.fossil.x33
    public final /* synthetic */ void d(w33 w33, r43 r43) throws IOException {
        w33.h(r43);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.x33
    public final /* bridge */ /* synthetic */ void e(Object obj, w33 w33) {
        m(obj, w33);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.x33
    public final /* synthetic */ w33 f(Object obj) {
        return ((e13) obj).zzb;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.r43] */
    @Override // com.fossil.x33
    public final /* synthetic */ void g(w33 w33, r43 r43) throws IOException {
        w33.e(r43);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.x33
    public final /* synthetic */ void h(Object obj, w33 w33) {
        m(obj, w33);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.x33
    public final /* synthetic */ w33 i(w33 w33, w33 w332) {
        w33 w333 = w33;
        w33 w334 = w332;
        return w334.equals(w33.a()) ? w333 : w33.b(w333, w334);
    }

    @DexIgnore
    @Override // com.fossil.x33
    public final void j(Object obj) {
        ((e13) obj).zzb.i();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.x33
    public final /* synthetic */ int k(w33 w33) {
        return w33.j();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.x33
    public final /* synthetic */ int l(w33 w33) {
        return w33.k();
    }
}
