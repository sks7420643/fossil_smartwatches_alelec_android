package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sc1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(mb1 mb1, Exception exc, wb1<?> wb1, gb1 gb1);

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        void e(mb1 mb1, Object obj, wb1<?> wb1, gb1 gb1, mb1 mb12);
    }

    @DexIgnore
    boolean c();

    @DexIgnore
    Object cancel();  // void declaration
}
