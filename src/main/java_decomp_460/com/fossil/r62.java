package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.fossil.ac2;
import com.fossil.m62;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class r62 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Set<r62> f3083a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Account f3084a;
        @DexIgnore
        public /* final */ Set<Scope> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Scope> c; // = new HashSet();
        @DexIgnore
        public int d;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public /* final */ Map<m62<?>, ac2.b> h; // = new zi0();
        @DexIgnore
        public /* final */ Context i;
        @DexIgnore
        public /* final */ Map<m62<?>, m62.d> j; // = new zi0();
        @DexIgnore
        public n72 k;
        @DexIgnore
        public int l; // = -1;
        @DexIgnore
        public c m;
        @DexIgnore
        public Looper n;
        @DexIgnore
        public c62 o; // = c62.q();
        @DexIgnore
        public m62.a<? extends ys3, gs3> p; // = vs3.c;
        @DexIgnore
        public /* final */ ArrayList<b> q; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<c> r; // = new ArrayList<>();

        @DexIgnore
        public a(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @DexIgnore
        public final a a(m62<? extends Object> m62) {
            rc2.l(m62, "Api must not be null");
            this.j.put(m62, null);
            List<Scope> a2 = m62.c().a(null);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final <O extends m62.d.c> a b(m62<O> m62, O o2) {
            rc2.l(m62, "Api must not be null");
            rc2.l(o2, "Null options are not permitted for this Api");
            this.j.put(m62, o2);
            List<Scope> a2 = m62.c().a(o2);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final a c(m62<? extends Object> m62, Scope... scopeArr) {
            rc2.l(m62, "Api must not be null");
            this.j.put(m62, null);
            j(m62, null, scopeArr);
            return this;
        }

        @DexIgnore
        public final a d(b bVar) {
            rc2.l(bVar, "Listener must not be null");
            this.q.add(bVar);
            return this;
        }

        @DexIgnore
        public final a e(c cVar) {
            rc2.l(cVar, "Listener must not be null");
            this.r.add(cVar);
            return this;
        }

        @DexIgnore
        public final a f(String[] strArr) {
            for (String str : strArr) {
                this.b.add(new Scope(str));
            }
            return this;
        }

        @DexIgnore
        public final r62 g() {
            rc2.b(!this.j.isEmpty(), "must call addApi() to add at least one API");
            ac2 h2 = h();
            Map<m62<?>, ac2.b> g2 = h2.g();
            zi0 zi0 = new zi0();
            zi0 zi02 = new zi0();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            m62<?> m62 = null;
            for (m62<?> m622 : this.j.keySet()) {
                m62.d dVar = this.j.get(m622);
                boolean z2 = g2.get(m622) != null;
                zi0.put(m622, Boolean.valueOf(z2));
                wa2 wa2 = new wa2(m622, z2);
                arrayList.add(wa2);
                m62.a<?, ?> d2 = m622.d();
                m62.f c2 = d2.c(this.i, this.n, h2, dVar, wa2, wa2);
                zi02.put(m622.a(), c2);
                boolean z3 = d2.b() == 1 ? dVar != null : z;
                if (!c2.g()) {
                    z = z3;
                } else if (m62 == null) {
                    z = z3;
                    m62 = m622;
                } else {
                    String b2 = m622.b();
                    String b3 = m62.b();
                    StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 21 + String.valueOf(b3).length());
                    sb.append(b2);
                    sb.append(" cannot be used with ");
                    sb.append(b3);
                    throw new IllegalStateException(sb.toString());
                }
            }
            if (m62 != null) {
                if (!z) {
                    rc2.p(this.f3084a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", m62.b());
                    rc2.p(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", m62.b());
                } else {
                    String b4 = m62.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b4).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b4);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            t82 t82 = new t82(this.i, new ReentrantLock(), this.n, h2, this.o, this.p, zi0, this.q, this.r, zi02, this.l, t82.x(zi02.values(), true), arrayList, false);
            synchronized (r62.f3083a) {
                r62.f3083a.add(t82);
            }
            if (this.l >= 0) {
                oa2.q(this.k).s(this.l, t82, this.m);
            }
            return t82;
        }

        @DexIgnore
        public final ac2 h() {
            gs3 gs3 = gs3.k;
            if (this.j.containsKey(vs3.e)) {
                gs3 = (gs3) this.j.get(vs3.e);
            }
            return new ac2(this.f3084a, this.b, this.h, this.d, this.e, this.f, this.g, gs3, false);
        }

        @DexIgnore
        public final a i(Handler handler) {
            rc2.l(handler, "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }

        @DexIgnore
        public final <O extends m62.d> void j(m62<O> m62, O o2, Scope... scopeArr) {
            HashSet hashSet = new HashSet(m62.c().a(o2));
            for (Scope scope : scopeArr) {
                hashSet.add(scope);
            }
            this.h.put(m62, new ac2.b(hashSet));
        }
    }

    @DexIgnore
    @Deprecated
    public interface b extends k72 {
    }

    @DexIgnore
    @Deprecated
    public interface c extends r72 {
    }

    @DexIgnore
    public static Set<r62> k() {
        Set<r62> set;
        synchronized (f3083a) {
            set = f3083a;
        }
        return set;
    }

    @DexIgnore
    public abstract z52 d();

    @DexIgnore
    public abstract t62<Status> e();

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public abstract void g();

    @DexIgnore
    public abstract void h(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    public <A extends m62.b, R extends z62, T extends i72<R, A>> T i(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public <A extends m62.b, T extends i72<? extends z62, A>> T j(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Context l() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Looper m() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean n();

    @DexIgnore
    public boolean o(t72 t72) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public void p() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void q(c cVar);

    @DexIgnore
    public abstract void r(c cVar);

    @DexIgnore
    public void s(da2 da2) {
        throw new UnsupportedOperationException();
    }
}
