package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z53 implements xw2<c63> {
    @DexIgnore
    public static z53 c; // = new z53();
    @DexIgnore
    public /* final */ xw2<c63> b;

    @DexIgnore
    public z53() {
        this(ww2.b(new b63()));
    }

    @DexIgnore
    public z53(xw2<c63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((c63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((c63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ c63 zza() {
        return this.b.zza();
    }
}
