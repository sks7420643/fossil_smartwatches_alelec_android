package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ lz7 f3341a;

    @DexIgnore
    public sz7(lz7 lz7) {
        this.f3341a = lz7;
    }

    @DexIgnore
    public String toString() {
        return "Removed[" + this.f3341a + ']';
    }
}
