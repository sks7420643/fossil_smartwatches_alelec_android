package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.fossil.yh1;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wa1 implements ComponentCallbacks2, ei1 {
    @DexIgnore
    public static /* final */ fj1 s; // = ((fj1) fj1.t0(Bitmap.class).V());
    @DexIgnore
    public /* final */ oa1 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ di1 d;
    @DexIgnore
    public /* final */ ji1 e;
    @DexIgnore
    public /* final */ ii1 f;
    @DexIgnore
    public /* final */ li1 g;
    @DexIgnore
    public /* final */ Runnable h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ yh1 j;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<ej1<Object>> k;
    @DexIgnore
    public fj1 l;
    @DexIgnore
    public boolean m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            wa1 wa1 = wa1.this;
            wa1.d.a(wa1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements yh1.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ji1 f3908a;

        @DexIgnore
        public b(ji1 ji1) {
            this.f3908a = ji1;
        }

        @DexIgnore
        @Override // com.fossil.yh1.a
        public void a(boolean z) {
            if (z) {
                synchronized (wa1.this) {
                    this.f3908a.e();
                }
            }
        }
    }

    /*
    static {
        fj1 fj1 = (fj1) fj1.t0(hh1.class).V();
        fj1 fj12 = (fj1) ((fj1) fj1.u0(wc1.b).e0(sa1.LOW)).m0(true);
    }
    */

    @DexIgnore
    public wa1(oa1 oa1, di1 di1, ii1 ii1, Context context) {
        this(oa1, di1, ii1, new ji1(), oa1.g(), context);
    }

    @DexIgnore
    public wa1(oa1 oa1, di1 di1, ii1 ii1, ji1 ji1, zh1 zh1, Context context) {
        this.g = new li1();
        this.h = new a();
        this.i = new Handler(Looper.getMainLooper());
        this.b = oa1;
        this.d = di1;
        this.f = ii1;
        this.e = ji1;
        this.c = context;
        this.j = zh1.a(context.getApplicationContext(), new b(ji1));
        if (jk1.p()) {
            this.i.post(this.h);
        } else {
            di1.a(this);
        }
        di1.a(this.j);
        this.k = new CopyOnWriteArrayList<>(oa1.i().c());
        y(oa1.i().d());
        oa1.o(this);
    }

    @DexIgnore
    public boolean A(qj1<?> qj1) {
        synchronized (this) {
            bj1 i2 = qj1.i();
            if (i2 == null) {
                return true;
            }
            if (!this.e.a(i2)) {
                return false;
            }
            this.g.l(qj1);
            qj1.d(null);
            return true;
        }
    }

    @DexIgnore
    public final void B(qj1<?> qj1) {
        boolean A = A(qj1);
        bj1 i2 = qj1.i();
        if (!A && !this.b.p(qj1) && i2 != null) {
            qj1.d(null);
            i2.clear();
        }
    }

    @DexIgnore
    public <ResourceType> va1<ResourceType> c(Class<ResourceType> cls) {
        return new va1<>(this.b, this, cls, this.c);
    }

    @DexIgnore
    public va1<Bitmap> e() {
        return c(Bitmap.class).d(s);
    }

    @DexIgnore
    public va1<Drawable> g() {
        return c(Drawable.class);
    }

    @DexIgnore
    public void l(qj1<?> qj1) {
        if (qj1 != null) {
            B(qj1);
        }
    }

    @DexIgnore
    public List<ej1<Object>> m() {
        return this.k;
    }

    @DexIgnore
    public fj1 n() {
        fj1 fj1;
        synchronized (this) {
            fj1 = this.l;
        }
        return fj1;
    }

    @DexIgnore
    public <T> xa1<?, T> o(Class<T> cls) {
        return this.b.i().e(cls);
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onDestroy() {
        synchronized (this) {
            this.g.onDestroy();
            for (qj1<?> qj1 : this.g.e()) {
                l(qj1);
            }
            this.g.c();
            this.e.b();
            this.d.b(this);
            this.d.b(this.j);
            this.i.removeCallbacks(this.h);
            this.b.s(this);
        }
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStart() {
        synchronized (this) {
            x();
            this.g.onStart();
        }
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStop() {
        synchronized (this) {
            w();
            this.g.onStop();
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.m) {
            v();
        }
    }

    @DexIgnore
    public va1<Drawable> p(Bitmap bitmap) {
        return g().I0(bitmap);
    }

    @DexIgnore
    public va1<Drawable> q(Uri uri) {
        return g().J0(uri);
    }

    @DexIgnore
    public va1<Drawable> r(Integer num) {
        return g().L0(num);
    }

    @DexIgnore
    public va1<Drawable> s(Object obj) {
        return g().M0(obj);
    }

    @DexIgnore
    public va1<Drawable> t(String str) {
        return g().N0(str);
    }

    @DexIgnore
    public String toString() {
        String str;
        synchronized (this) {
            str = super.toString() + "{tracker=" + this.e + ", treeNode=" + this.f + "}";
        }
        return str;
    }

    @DexIgnore
    public void u() {
        synchronized (this) {
            this.e.c();
        }
    }

    @DexIgnore
    public void v() {
        synchronized (this) {
            u();
            for (wa1 wa1 : this.f.a()) {
                wa1.u();
            }
        }
    }

    @DexIgnore
    public void w() {
        synchronized (this) {
            this.e.d();
        }
    }

    @DexIgnore
    public void x() {
        synchronized (this) {
            this.e.f();
        }
    }

    @DexIgnore
    public void y(fj1 fj1) {
        synchronized (this) {
            this.l = (fj1) ((fj1) fj1.clone()).e();
        }
    }

    @DexIgnore
    public void z(qj1<?> qj1, bj1 bj1) {
        synchronized (this) {
            this.g.g(qj1);
            this.e.g(bj1);
        }
    }
}
