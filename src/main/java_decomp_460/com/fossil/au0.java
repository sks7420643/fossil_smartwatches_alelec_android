package com.fossil;

import com.fossil.bu0;
import com.fossil.xt0;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class au0<Key, Value> extends vt0<Key, Value> {
    @DexIgnore
    public /* final */ Object mKeyLock; // = new Object();
    @DexIgnore
    public Key mNextKey; // = null;
    @DexIgnore
    public Key mPreviousKey; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<Key, Value> {
        @DexIgnore
        public abstract void a(List<Value> list, Key key);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Key, Value> extends a<Key, Value> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xt0.d<Value> f324a;
        @DexIgnore
        public /* final */ au0<Key, Value> b;

        @DexIgnore
        public b(au0<Key, Value> au0, int i, Executor executor, bu0.a<Value> aVar) {
            this.f324a = new xt0.d<>(au0, i, executor, aVar);
            this.b = au0;
        }

        @DexIgnore
        @Override // com.fossil.au0.a
        public void a(List<Value> list, Key key) {
            if (!this.f324a.a()) {
                if (this.f324a.f4172a == 1) {
                    this.b.setNextKey(key);
                } else {
                    this.b.setPreviousKey(key);
                }
                this.f324a.b(new bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<Key, Value> {
        @DexIgnore
        public abstract void a(List<Value> list, Key key, Key key2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Key, Value> extends c<Key, Value> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xt0.d<Value> f325a;
        @DexIgnore
        public /* final */ au0<Key, Value> b;

        @DexIgnore
        public d(au0<Key, Value> au0, boolean z, bu0.a<Value> aVar) {
            this.f325a = new xt0.d<>(au0, 0, null, aVar);
            this.b = au0;
        }

        @DexIgnore
        @Override // com.fossil.au0.c
        public void a(List<Value> list, Key key, Key key2) {
            if (!this.f325a.a()) {
                this.b.initKeys(key, key2);
                this.f325a.b(new bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Key> {
        @DexIgnore
        public e(int i, boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<Key> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Key f326a;

        @DexIgnore
        public f(Key key, int i) {
            this.f326a = key;
        }
    }

    @DexIgnore
    private Key getNextKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mNextKey;
        }
        return key;
    }

    @DexIgnore
    private Key getPreviousKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mPreviousKey;
        }
        return key;
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, bu0.a<Value> aVar) {
        Key nextKey = getNextKey();
        if (nextKey != null) {
            loadAfter(new f<>(nextKey, i2), new b(this, 1, executor, aVar));
        } else {
            aVar.a(1, bu0.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, bu0.a<Value> aVar) {
        Key previousKey = getPreviousKey();
        if (previousKey != null) {
            loadBefore(new f<>(previousKey, i2), new b(this, 2, executor, aVar));
        } else {
            aVar.a(2, bu0.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, bu0.a<Value> aVar) {
        d dVar = new d(this, z, aVar);
        loadInitial(new e<>(i, z), dVar);
        dVar.f325a.c(executor);
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final Key getKey(int i, Value value) {
        return null;
    }

    @DexIgnore
    public void initKeys(Key key, Key key2) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
            this.mNextKey = key2;
        }
    }

    @DexIgnore
    public abstract void loadAfter(f<Key> fVar, a<Key, Value> aVar);

    @DexIgnore
    public abstract void loadBefore(f<Key> fVar, a<Key, Value> aVar);

    @DexIgnore
    public abstract void loadInitial(e<Key> eVar, c<Key, Value> cVar);

    @DexIgnore
    @Override // com.fossil.xt0
    public final <ToValue> au0<Key, ToValue> map(gi0<Value, ToValue> gi0) {
        return mapByPage((gi0) xt0.createListFunction(gi0));
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public final <ToValue> au0<Key, ToValue> mapByPage(gi0<List<Value>, List<ToValue>> gi0) {
        return new ku0(this, gi0);
    }

    @DexIgnore
    public void setNextKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mNextKey = key;
        }
    }

    @DexIgnore
    public void setPreviousKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
        }
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public boolean supportsPageDropping() {
        return false;
    }
}
