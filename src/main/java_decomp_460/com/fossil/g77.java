package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g77 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public int f1272a; // = -1;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ e77 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ge5 f1273a;
        @DexIgnore
        public /* final */ e77 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.g77$a$a")
        /* renamed from: com.fossil.g77$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0096a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a b;
            @DexIgnore
            public /* final */ /* synthetic */ fb7 c;

            @DexIgnore
            public View$OnClickListenerC0096a(a aVar, fb7 fb7, int i) {
                this.b = aVar;
                this.c = fb7;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.a(this.c, this.b.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ge5 ge5, e77 e77) {
            super(ge5.n());
            pq7.c(ge5, "binding");
            pq7.c(e77, "templateListener");
            this.f1273a = ge5;
            this.b = e77;
        }

        @DexIgnore
        public final void b(fb7 fb7, int i) {
            pq7.c(fb7, "wf");
            ge5 ge5 = this.f1273a;
            ge5.q.setOnClickListener(new View$OnClickListenerC0096a(this, fb7, i));
            ImageView imageView = ge5.q;
            pq7.b(imageView, "ivBackgroundPreview");
            ty4.a(imageView, fb7.f(), fb7.e());
            if (i == getAdapterPosition()) {
                View view = ge5.t;
                pq7.b(view, "vBackgroundSelected");
                view.setVisibility(0);
                return;
            }
            View view2 = ge5.t;
            pq7.b(view2, "vBackgroundSelected");
            view2.setVisibility(8);
        }
    }

    @DexIgnore
    public g77(int i, e77 e77) {
        pq7.c(e77, "templateListener");
        this.b = i;
        this.c = e77;
    }

    @DexIgnore
    public final void a(int i) {
        this.f1272a = i;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public boolean c(List<? extends Object> list, int i) {
        pq7.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof fb7) && ((fb7) obj).a() == l77.BACKGROUND_TEMPLATE;
    }

    @DexIgnore
    public void d(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        a aVar = null;
        pq7.c(list, "items");
        pq7.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof fb7)) {
            obj = null;
        }
        fb7 fb7 = (fb7) obj;
        if (fb7 != null) {
            if (viewHolder instanceof a) {
                aVar = viewHolder;
            }
            a aVar2 = aVar;
            if (aVar2 != null) {
                aVar2.b(fb7, this.f1272a);
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder e(ViewGroup viewGroup) {
        pq7.c(viewGroup, "parent");
        ge5 z = ge5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(z, "ItemDianaBackgroundBindi\u2026(inflater, parent, false)");
        return new a(z, this.c);
    }
}
