package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ny1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ny1 f2594a; // = new ny1();

    @DexIgnore
    public final File a(Context context, String str) {
        pq7.c(context, "context");
        pq7.c(str, "filePath");
        return new File(context.getFilesDir(), str);
    }
}
