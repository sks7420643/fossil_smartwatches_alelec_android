package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g58 {
    @DexIgnore
    public static final int a(int[] iArr, int i, int i2, int i3) {
        pq7.c(iArr, "$this$binarySearch");
        int i4 = i3 - 1;
        while (i2 <= i4) {
            int i5 = (i2 + i4) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i) {
                i2 = i5 + 1;
            } else if (i6 <= i) {
                return i5;
            } else {
                i4 = i5 - 1;
            }
        }
        return (-i2) - 1;
    }

    @DexIgnore
    public static final int b(z48 z48, int i) {
        pq7.c(z48, "$this$segment");
        int a2 = a(z48.getDirectory$okio(), i + 1, 0, z48.getSegments$okio().length);
        if (a2 >= 0) {
        }
        return a2;
    }
}
