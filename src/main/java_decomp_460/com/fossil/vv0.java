package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vv0 extends uv0 {
    @DexIgnore
    public int d;
    @DexIgnore
    public c e;
    @DexIgnore
    public b f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3838a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[b.values().length];
            b = iArr;
            try {
                iArr[b.PIXEL_LA.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[b.PIXEL_RGB.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[b.PIXEL_RGBA.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            int[] iArr2 = new int[c.values().length];
            f3838a = iArr2;
            try {
                iArr2[c.FLOAT_32.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f3838a[c.FLOAT_64.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f3838a[c.SIGNED_8.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f3838a[c.SIGNED_16.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f3838a[c.SIGNED_32.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f3838a[c.SIGNED_64.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f3838a[c.UNSIGNED_8.ordinal()] = 7;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f3838a[c.UNSIGNED_16.ordinal()] = 8;
            } catch (NoSuchFieldError e11) {
            }
            try {
                f3838a[c.UNSIGNED_32.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                f3838a[c.UNSIGNED_64.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                f3838a[c.BOOLEAN.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        USER(0),
        PIXEL_L(7),
        PIXEL_A(8),
        PIXEL_LA(9),
        PIXEL_RGB(10),
        PIXEL_RGBA(11),
        PIXEL_DEPTH(12),
        PIXEL_YUV(13);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public b(int i) {
            this.mID = i;
        }
    }

    @DexIgnore
    public enum c {
        NONE(0, 0),
        FLOAT_32(2, 4),
        FLOAT_64(3, 8),
        SIGNED_8(4, 1),
        SIGNED_16(5, 2),
        SIGNED_32(6, 4),
        SIGNED_64(7, 8),
        UNSIGNED_8(8, 1),
        UNSIGNED_16(9, 2),
        UNSIGNED_32(10, 4),
        UNSIGNED_64(11, 8),
        BOOLEAN(12, 1),
        UNSIGNED_5_6_5(13, 2),
        UNSIGNED_5_5_5_1(14, 2),
        UNSIGNED_4_4_4_4(15, 2),
        MATRIX_4X4(16, 64),
        MATRIX_3X3(17, 36),
        MATRIX_2X2(18, 16),
        RS_ELEMENT(1000),
        RS_TYPE(1001),
        RS_ALLOCATION(1002),
        RS_SAMPLER(1003),
        RS_SCRIPT(1004);
        
        @DexIgnore
        public int mID;
        @DexIgnore
        public int mSize;

        @DexIgnore
        public c(int i) {
            this.mID = i;
            this.mSize = 4;
            if (RenderScript.G == 8) {
                this.mSize = 32;
            }
        }

        @DexIgnore
        public c(int i, int i2) {
            this.mID = i;
            this.mSize = i2;
        }
    }

    @DexIgnore
    public vv0(long j, RenderScript renderScript, c cVar, b bVar, boolean z, int i) {
        super(j, renderScript);
        if (cVar == c.UNSIGNED_5_6_5 || cVar == c.UNSIGNED_4_4_4_4 || cVar == c.UNSIGNED_5_5_5_1) {
            this.d = cVar.mSize;
        } else if (i == 3) {
            this.d = cVar.mSize * 4;
        } else {
            this.d = cVar.mSize * i;
        }
        this.e = cVar;
        this.f = bVar;
        this.g = z;
        this.h = i;
    }

    @DexIgnore
    public static vv0 f(RenderScript renderScript) {
        if (renderScript.n == null) {
            renderScript.n = l(renderScript, c.UNSIGNED_8, b.PIXEL_A);
        }
        return renderScript.n;
    }

    @DexIgnore
    public static vv0 g(RenderScript renderScript) {
        if (renderScript.p == null) {
            renderScript.p = l(renderScript, c.UNSIGNED_4_4_4_4, b.PIXEL_RGBA);
        }
        return renderScript.p;
    }

    @DexIgnore
    public static vv0 h(RenderScript renderScript) {
        if (renderScript.q == null) {
            renderScript.q = l(renderScript, c.UNSIGNED_8, b.PIXEL_RGBA);
        }
        return renderScript.q;
    }

    @DexIgnore
    public static vv0 i(RenderScript renderScript) {
        if (renderScript.o == null) {
            renderScript.o = l(renderScript, c.UNSIGNED_5_6_5, b.PIXEL_RGB);
        }
        return renderScript.o;
    }

    @DexIgnore
    public static vv0 j(RenderScript renderScript) {
        if (renderScript.m == null) {
            renderScript.m = m(renderScript, c.UNSIGNED_8);
        }
        return renderScript.m;
    }

    @DexIgnore
    public static vv0 k(RenderScript renderScript) {
        if (renderScript.r == null) {
            renderScript.r = n(renderScript, c.UNSIGNED_8, 4);
        }
        return renderScript.r;
    }

    @DexIgnore
    public static vv0 l(RenderScript renderScript, c cVar, b bVar) {
        if (bVar != b.PIXEL_L && bVar != b.PIXEL_A && bVar != b.PIXEL_LA && bVar != b.PIXEL_RGB && bVar != b.PIXEL_RGBA && bVar != b.PIXEL_DEPTH && bVar != b.PIXEL_YUV) {
            throw new yv0("Unsupported DataKind");
        } else if (cVar != c.UNSIGNED_8 && cVar != c.UNSIGNED_16 && cVar != c.UNSIGNED_5_6_5 && cVar != c.UNSIGNED_4_4_4_4 && cVar != c.UNSIGNED_5_5_5_1) {
            throw new yv0("Unsupported DataType");
        } else if (cVar == c.UNSIGNED_5_6_5 && bVar != b.PIXEL_RGB) {
            throw new yv0("Bad kind and type combo");
        } else if (cVar == c.UNSIGNED_5_5_5_1 && bVar != b.PIXEL_RGBA) {
            throw new yv0("Bad kind and type combo");
        } else if (cVar == c.UNSIGNED_4_4_4_4 && bVar != b.PIXEL_RGBA) {
            throw new yv0("Bad kind and type combo");
        } else if (cVar != c.UNSIGNED_16 || bVar == b.PIXEL_DEPTH) {
            int i = a.b[bVar.ordinal()];
            int i2 = i != 1 ? i != 2 ? i != 3 ? 1 : 4 : 3 : 2;
            return new vv0(renderScript.s((long) cVar.mID, bVar.mID, true, i2), renderScript, cVar, bVar, true, i2);
        } else {
            throw new yv0("Bad kind and type combo");
        }
    }

    @DexIgnore
    public static vv0 m(RenderScript renderScript, c cVar) {
        b bVar = b.USER;
        return new vv0(renderScript.s((long) cVar.mID, bVar.mID, false, 1), renderScript, cVar, bVar, false, 1);
    }

    @DexIgnore
    public static vv0 n(RenderScript renderScript, c cVar, int i) {
        if (i < 2 || i > 4) {
            throw new yv0("Vector size out of range 2-4.");
        }
        switch (a.f3838a[cVar.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                b bVar = b.USER;
                return new vv0(renderScript.s((long) cVar.mID, bVar.mID, false, i), renderScript, cVar, bVar, false, i);
            default:
                throw new yv0("Cannot create vector of non-primitive type.");
        }
    }

    @DexIgnore
    public int o() {
        return this.d;
    }

    @DexIgnore
    public long p(RenderScript renderScript) {
        return renderScript.x((long) this.e.mID, this.f.mID, this.g, this.h);
    }

    @DexIgnore
    public boolean q(vv0 vv0) {
        c cVar;
        if (equals(vv0)) {
            return true;
        }
        return this.d == vv0.d && (cVar = this.e) != c.NONE && cVar == vv0.e && this.h == vv0.h;
    }
}
