package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class v84 implements h84 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ w84 f3733a;

    @DexIgnore
    public v84(w84 w84) {
        this.f3733a = w84;
    }

    @DexIgnore
    public static h84 b(w84 w84) {
        return new v84(w84);
    }

    @DexIgnore
    @Override // com.fossil.h84
    public void a(String str) {
        this.f3733a.k(str);
    }
}
