package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum i78 {
    ERROR(40, "ERROR"),
    WARN(30, "WARN"),
    INFO(20, "INFO"),
    DEBUG(10, "DEBUG"),
    TRACE(0, "TRACE");
    
    @DexIgnore
    public int levelInt;
    @DexIgnore
    public String levelStr;

    @DexIgnore
    public i78(int i, String str) {
        this.levelInt = i;
        this.levelStr = str;
    }

    @DexIgnore
    public int toInt() {
        return this.levelInt;
    }

    @DexIgnore
    public String toString() {
        return this.levelStr;
    }
}
