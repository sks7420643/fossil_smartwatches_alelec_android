package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go1 extends bo1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<go1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public go1 createFromParcel(Parcel parcel) {
            return new go1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public go1[] newArray(int i) {
            return new go1[i];
        }
    }

    @DexIgnore
    public go1(byte b, int i) {
        super(do1.REPLY_MESSAGE);
        this.c = (byte) b;
        this.d = i;
    }

    @DexIgnore
    public /* synthetic */ go1(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readInt();
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.c;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bo1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.k, Byte.valueOf(this.c)), jd0.j, Integer.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.bo1
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeByte(this.c);
        parcel.writeInt(this.d);
    }
}
