package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ll7 implements Comparable<ll7> {
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public /* synthetic */ ll7(int i) {
        this.b = i;
    }

    @DexIgnore
    public static final /* synthetic */ ll7 a(int i) {
        return new ll7(i);
    }

    @DexIgnore
    public static int c(int i, int i2) {
        return ul7.a(i, i2);
    }

    @DexIgnore
    public static int e(int i) {
        return i;
    }

    @DexIgnore
    public static boolean f(int i, Object obj) {
        return (obj instanceof ll7) && i == ((ll7) obj).j();
    }

    @DexIgnore
    public static int h(int i) {
        return i;
    }

    @DexIgnore
    public static String i(int i) {
        return String.valueOf(((long) i) & 4294967295L);
    }

    @DexIgnore
    public final int b(int i) {
        return c(this.b, i);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(ll7 ll7) {
        return b(ll7.j());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return f(this.b, obj);
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        h(i);
        return i;
    }

    @DexIgnore
    public final /* synthetic */ int j() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return i(this.b);
    }
}
