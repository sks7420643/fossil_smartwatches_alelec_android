package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q77 {
    @rj4("url")

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f2935a;
    @DexIgnore
    @rj4("previewUrl")
    public String b;

    @DexIgnore
    public final String a() {
        return this.f2935a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof q77) {
                q77 q77 = (q77) obj;
                if (!pq7.a(this.f2935a, q77.f2935a) || !pq7.a(this.b, q77.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f2935a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WFAssetUrl(data=" + this.f2935a + ", preview=" + this.b + ")";
    }
}
