package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o73 implements xw2<n73> {
    @DexIgnore
    public static o73 c; // = new o73();
    @DexIgnore
    public /* final */ xw2<n73> b;

    @DexIgnore
    public o73() {
        this(ww2.b(new q73()));
    }

    @DexIgnore
    public o73(xw2<n73> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((n73) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ n73 zza() {
        return this.b.zza();
    }
}
