package com.fossil;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb1 extends OutputStream {
    @DexIgnore
    public /* final */ OutputStream b;
    @DexIgnore
    public byte[] c;
    @DexIgnore
    public od1 d;
    @DexIgnore
    public int e;

    @DexIgnore
    public vb1(OutputStream outputStream, od1 od1) {
        this(outputStream, od1, 65536);
    }

    @DexIgnore
    public vb1(OutputStream outputStream, od1 od1, int i) {
        this.b = outputStream;
        this.d = od1;
        this.c = (byte[]) od1.g(i, byte[].class);
    }

    @DexIgnore
    public final void a() throws IOException {
        int i = this.e;
        if (i > 0) {
            this.b.write(this.c, 0, i);
            this.e = 0;
        }
    }

    @DexIgnore
    public final void b() throws IOException {
        if (this.e == this.c.length) {
            a();
        }
    }

    @DexIgnore
    public final void c() {
        byte[] bArr = this.c;
        if (bArr != null) {
            this.d.f(bArr);
            this.c = null;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        try {
            flush();
            this.b.close();
            c();
        } catch (Throwable th) {
            this.b.close();
            throw th;
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.Flushable
    public void flush() throws IOException {
        a();
        this.b.flush();
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(int i) throws IOException {
        byte[] bArr = this.c;
        int i2 = this.e;
        this.e = i2 + 1;
        bArr[i2] = (byte) ((byte) i);
        b();
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.e != 0 || i4 < this.c.length) {
                int min = Math.min(i4, this.c.length - this.e);
                System.arraycopy(bArr, i5, this.c, this.e, min);
                this.e += min;
                i3 += min;
                b();
            } else {
                this.b.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
