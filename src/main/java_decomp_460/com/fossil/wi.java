package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi extends rp {
    @DexIgnore
    public static /* final */ ii V; // = new ii(null);
    @DexIgnore
    public /* final */ long U;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wi(k5 k5Var, i60 i60, long j, String str, int i) {
        super(k5Var, i60, yp.G0, V.a(j), false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, 48);
        this.U = j;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.y5, Long.valueOf(this.U));
    }
}
