package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us6 implements Factory<ts6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f3640a;

    @DexIgnore
    public us6(Provider<ThemeRepository> provider) {
        this.f3640a = provider;
    }

    @DexIgnore
    public static us6 a(Provider<ThemeRepository> provider) {
        return new us6(provider);
    }

    @DexIgnore
    public static ts6 c(ThemeRepository themeRepository) {
        return new ts6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public ts6 get() {
        return c(this.f3640a.get());
    }
}
