package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yl7<T> implements Iterator<T>, jr7 {
    @DexIgnore
    public fn7 b; // = fn7.NotReady;
    @DexIgnore
    public T c;

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b() {
        this.b = fn7.Done;
    }

    @DexIgnore
    public final void c(T t) {
        this.c = t;
        this.b = fn7.Ready;
    }

    @DexIgnore
    public final boolean e() {
        this.b = fn7.Failed;
        a();
        return this.b == fn7.Ready;
    }

    @DexIgnore
    public boolean hasNext() {
        if (this.b != fn7.Failed) {
            int i = xl7.f4137a[this.b.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i != 2) {
                return e();
            }
            return true;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            this.b = fn7.NotReady;
            return this.c;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
