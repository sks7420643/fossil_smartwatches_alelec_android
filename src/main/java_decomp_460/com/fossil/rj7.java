package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rj7 {
    @DexIgnore
    public static String a(Long l) {
        return b(l, true);
    }

    @DexIgnore
    public static String b(Long l, boolean z) {
        if (l == null || l.longValue() < 0) {
            return "";
        }
        int i = z ? 1000 : 1024;
        if (l.longValue() < ((long) i)) {
            return l + " B";
        }
        double d = (double) i;
        int log = (int) (Math.log((double) l.longValue()) / Math.log(d));
        StringBuilder sb = new StringBuilder();
        sb.append((z ? "kMGTPE" : "KMGTPE").charAt(log - 1));
        sb.append(z ? "" : "i");
        return String.format(Locale.US, "%.1f %sB", Double.valueOf(((double) l.longValue()) / Math.pow(d, (double) log)), sb.toString());
    }
}
