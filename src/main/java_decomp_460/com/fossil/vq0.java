package com.fossil;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentHostCallback;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vq0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ pq0 f3804a;
    @DexIgnore
    public /* final */ Fragment b;
    @DexIgnore
    public int c; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ int[] f3805a;

        /*
        static {
            int[] iArr = new int[Lifecycle.State.values().length];
            f3805a = iArr;
            try {
                iArr[Lifecycle.State.RESUMED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f3805a[Lifecycle.State.STARTED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f3805a[Lifecycle.State.CREATED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
        */
    }

    @DexIgnore
    public vq0(pq0 pq0, Fragment fragment) {
        this.f3804a = pq0;
        this.b = fragment;
    }

    @DexIgnore
    public vq0(pq0 pq0, Fragment fragment, uq0 uq0) {
        this.f3804a = pq0;
        this.b = fragment;
        fragment.mSavedViewState = null;
        fragment.mBackStackNesting = 0;
        fragment.mInLayout = false;
        fragment.mAdded = false;
        Fragment fragment2 = fragment.mTarget;
        fragment.mTargetWho = fragment2 != null ? fragment2.mWho : null;
        Fragment fragment3 = this.b;
        fragment3.mTarget = null;
        Bundle bundle = uq0.s;
        if (bundle != null) {
            fragment3.mSavedFragmentState = bundle;
        } else {
            fragment3.mSavedFragmentState = new Bundle();
        }
    }

    @DexIgnore
    public vq0(pq0 pq0, ClassLoader classLoader, nq0 nq0, uq0 uq0) {
        this.f3804a = pq0;
        this.b = nq0.instantiate(classLoader, uq0.b);
        Bundle bundle = uq0.k;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
        }
        this.b.setArguments(uq0.k);
        Fragment fragment = this.b;
        fragment.mWho = uq0.c;
        fragment.mFromLayout = uq0.d;
        fragment.mRestored = true;
        fragment.mFragmentId = uq0.e;
        fragment.mContainerId = uq0.f;
        fragment.mTag = uq0.g;
        fragment.mRetainInstance = uq0.h;
        fragment.mRemoving = uq0.i;
        fragment.mDetached = uq0.j;
        fragment.mHidden = uq0.l;
        fragment.mMaxState = Lifecycle.State.values()[uq0.m];
        Bundle bundle2 = uq0.s;
        if (bundle2 != null) {
            this.b.mSavedFragmentState = bundle2;
        } else {
            this.b.mSavedFragmentState = new Bundle();
        }
        if (FragmentManager.s0(2)) {
            Log.v("FragmentManager", "Instantiated fragment " + this.b);
        }
    }

    @DexIgnore
    public void a() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "moveto ACTIVITY_CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        fragment.performActivityCreated(fragment.mSavedFragmentState);
        pq0 pq0 = this.f3804a;
        Fragment fragment2 = this.b;
        pq0.a(fragment2, fragment2.mSavedFragmentState, false);
    }

    @DexIgnore
    public void b(FragmentHostCallback<?> fragmentHostCallback, FragmentManager fragmentManager, Fragment fragment) {
        Fragment fragment2 = this.b;
        fragment2.mHost = fragmentHostCallback;
        fragment2.mParentFragment = fragment;
        fragment2.mFragmentManager = fragmentManager;
        this.f3804a.g(fragment2, fragmentHostCallback.e(), false);
        this.b.performAttach();
        Fragment fragment3 = this.b;
        Fragment fragment4 = fragment3.mParentFragment;
        if (fragment4 == null) {
            fragmentHostCallback.g(fragment3);
        } else {
            fragment4.onAttachFragment(fragment3);
        }
        this.f3804a.b(this.b, fragmentHostCallback.e(), false);
    }

    @DexIgnore
    public int c() {
        int i = this.c;
        Fragment fragment = this.b;
        if (fragment.mFromLayout) {
            i = fragment.mInLayout ? Math.max(i, 1) : i < 2 ? Math.min(i, fragment.mState) : Math.min(i, 1);
        }
        if (!this.b.mAdded) {
            i = Math.min(i, 1);
        }
        Fragment fragment2 = this.b;
        if (fragment2.mRemoving) {
            i = fragment2.isInBackStack() ? Math.min(i, 1) : Math.min(i, -1);
        }
        Fragment fragment3 = this.b;
        if (fragment3.mDeferStart && fragment3.mState < 3) {
            i = Math.min(i, 2);
        }
        int i2 = a.f3805a[this.b.mMaxState.ordinal()];
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? Math.min(i, -1) : Math.min(i, 1) : Math.min(i, 3) : i;
    }

    @DexIgnore
    public void d() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "moveto CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        if (!fragment.mIsCreated) {
            this.f3804a.h(fragment, fragment.mSavedFragmentState, false);
            Fragment fragment2 = this.b;
            fragment2.performCreate(fragment2.mSavedFragmentState);
            pq0 pq0 = this.f3804a;
            Fragment fragment3 = this.b;
            pq0.c(fragment3, fragment3.mSavedFragmentState, false);
            return;
        }
        fragment.restoreChildFragmentState(fragment.mSavedFragmentState);
        this.b.mState = 1;
    }

    @DexIgnore
    public void e(mq0 mq0) {
        String str;
        if (!this.b.mFromLayout) {
            if (FragmentManager.s0(3)) {
                Log.d("FragmentManager", "moveto CREATE_VIEW: " + this.b);
            }
            Fragment fragment = this.b;
            ViewGroup viewGroup = fragment.mContainer;
            if (viewGroup == null) {
                int i = fragment.mContainerId;
                if (i == 0) {
                    viewGroup = null;
                } else if (i != -1) {
                    viewGroup = (ViewGroup) mq0.b(i);
                    if (viewGroup == null) {
                        Fragment fragment2 = this.b;
                        if (!fragment2.mRestored) {
                            try {
                                str = fragment2.getResources().getResourceName(this.b.mContainerId);
                            } catch (Resources.NotFoundException e) {
                                str = "unknown";
                            }
                            throw new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(this.b.mContainerId) + " (" + str + ") for fragment " + this.b);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Cannot create fragment " + this.b + " for a container view with no id");
                }
            }
            Fragment fragment3 = this.b;
            fragment3.mContainer = viewGroup;
            fragment3.performCreateView(fragment3.performGetLayoutInflater(fragment3.mSavedFragmentState), viewGroup, this.b.mSavedFragmentState);
            View view = this.b.mView;
            if (view != null) {
                view.setSaveFromParentEnabled(false);
                Fragment fragment4 = this.b;
                fragment4.mView.setTag(gq0.fragment_container_view_tag, fragment4);
                if (viewGroup != null) {
                    viewGroup.addView(this.b.mView);
                }
                Fragment fragment5 = this.b;
                if (fragment5.mHidden) {
                    fragment5.mView.setVisibility(8);
                }
                mo0.i0(this.b.mView);
                Fragment fragment6 = this.b;
                fragment6.onViewCreated(fragment6.mView, fragment6.mSavedFragmentState);
                pq0 pq0 = this.f3804a;
                Fragment fragment7 = this.b;
                pq0.m(fragment7, fragment7.mView, fragment7.mSavedFragmentState, false);
                Fragment fragment8 = this.b;
                fragment8.mIsNewlyAdded = fragment8.mView.getVisibility() == 0 && this.b.mContainer != null;
            }
        }
    }

    @DexIgnore
    public void f(FragmentHostCallback<?> fragmentHostCallback, sq0 sq0) {
        boolean z = true;
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "movefrom CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        boolean z2 = fragment.mRemoving && !fragment.isInBackStack();
        if (z2 || sq0.j(this.b)) {
            if (fragmentHostCallback instanceof ws0) {
                z = sq0.h();
            } else if (fragmentHostCallback.e() instanceof Activity) {
                z = !((Activity) fragmentHostCallback.e()).isChangingConfigurations();
            }
            if (z2 || z) {
                sq0.b(this.b);
            }
            this.b.performDestroy();
            this.f3804a.d(this.b, false);
            return;
        }
        this.b.mState = 0;
    }

    @DexIgnore
    public void g(sq0 sq0) {
        boolean z = false;
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "movefrom ATTACHED: " + this.b);
        }
        this.b.performDetach();
        this.f3804a.e(this.b, false);
        Fragment fragment = this.b;
        fragment.mState = -1;
        fragment.mHost = null;
        fragment.mParentFragment = null;
        fragment.mFragmentManager = null;
        if (fragment.mRemoving && !fragment.isInBackStack()) {
            z = true;
        }
        if (z || sq0.j(this.b)) {
            if (FragmentManager.s0(3)) {
                Log.d("FragmentManager", "initState called for fragment: " + this.b);
            }
            this.b.initState();
        }
    }

    @DexIgnore
    public void h() {
        Fragment fragment = this.b;
        if (fragment.mFromLayout && fragment.mInLayout && !fragment.mPerformedCreateView) {
            if (FragmentManager.s0(3)) {
                Log.d("FragmentManager", "moveto CREATE_VIEW: " + this.b);
            }
            Fragment fragment2 = this.b;
            fragment2.performCreateView(fragment2.performGetLayoutInflater(fragment2.mSavedFragmentState), null, this.b.mSavedFragmentState);
            View view = this.b.mView;
            if (view != null) {
                view.setSaveFromParentEnabled(false);
                Fragment fragment3 = this.b;
                fragment3.mView.setTag(gq0.fragment_container_view_tag, fragment3);
                Fragment fragment4 = this.b;
                if (fragment4.mHidden) {
                    fragment4.mView.setVisibility(8);
                }
                Fragment fragment5 = this.b;
                fragment5.onViewCreated(fragment5.mView, fragment5.mSavedFragmentState);
                pq0 pq0 = this.f3804a;
                Fragment fragment6 = this.b;
                pq0.m(fragment6, fragment6.mView, fragment6.mSavedFragmentState, false);
            }
        }
    }

    @DexIgnore
    public Fragment i() {
        return this.b;
    }

    @DexIgnore
    public void j() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "movefrom RESUMED: " + this.b);
        }
        this.b.performPause();
        this.f3804a.f(this.b, false);
    }

    @DexIgnore
    public void k(ClassLoader classLoader) {
        Bundle bundle = this.b.mSavedFragmentState;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
            Fragment fragment = this.b;
            fragment.mSavedViewState = fragment.mSavedFragmentState.getSparseParcelableArray("android:view_state");
            Fragment fragment2 = this.b;
            fragment2.mTargetWho = fragment2.mSavedFragmentState.getString("android:target_state");
            Fragment fragment3 = this.b;
            if (fragment3.mTargetWho != null) {
                fragment3.mTargetRequestCode = fragment3.mSavedFragmentState.getInt("android:target_req_state", 0);
            }
            Fragment fragment4 = this.b;
            Boolean bool = fragment4.mSavedUserVisibleHint;
            if (bool != null) {
                fragment4.mUserVisibleHint = bool.booleanValue();
                this.b.mSavedUserVisibleHint = null;
            } else {
                fragment4.mUserVisibleHint = fragment4.mSavedFragmentState.getBoolean("android:user_visible_hint", true);
            }
            Fragment fragment5 = this.b;
            if (!fragment5.mUserVisibleHint) {
                fragment5.mDeferStart = true;
            }
        }
    }

    @DexIgnore
    public void l() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "moveto RESTORE_VIEW_STATE: " + this.b);
        }
        Fragment fragment = this.b;
        if (fragment.mView != null) {
            fragment.restoreViewState(fragment.mSavedFragmentState);
        }
        this.b.mSavedFragmentState = null;
    }

    @DexIgnore
    public void m() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "moveto RESUMED: " + this.b);
        }
        this.b.performResume();
        this.f3804a.i(this.b, false);
        Fragment fragment = this.b;
        fragment.mSavedFragmentState = null;
        fragment.mSavedViewState = null;
    }

    @DexIgnore
    public final Bundle n() {
        Bundle bundle = new Bundle();
        this.b.performSaveInstanceState(bundle);
        this.f3804a.j(this.b, bundle, false);
        if (bundle.isEmpty()) {
            bundle = null;
        }
        if (this.b.mView != null) {
            q();
        }
        if (this.b.mSavedViewState != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", this.b.mSavedViewState);
        }
        if (!this.b.mUserVisibleHint) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", this.b.mUserVisibleHint);
        }
        return bundle;
    }

    @DexIgnore
    public Fragment.SavedState o() {
        Bundle n;
        if (this.b.mState <= -1 || (n = n()) == null) {
            return null;
        }
        return new Fragment.SavedState(n);
    }

    @DexIgnore
    public uq0 p() {
        uq0 uq0 = new uq0(this.b);
        if (this.b.mState <= -1 || uq0.s != null) {
            uq0.s = this.b.mSavedFragmentState;
        } else {
            Bundle n = n();
            uq0.s = n;
            if (this.b.mTargetWho != null) {
                if (n == null) {
                    uq0.s = new Bundle();
                }
                uq0.s.putString("android:target_state", this.b.mTargetWho);
                int i = this.b.mTargetRequestCode;
                if (i != 0) {
                    uq0.s.putInt("android:target_req_state", i);
                }
            }
        }
        return uq0;
    }

    @DexIgnore
    public void q() {
        if (this.b.mView != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.b.mView.saveHierarchyState(sparseArray);
            if (sparseArray.size() > 0) {
                this.b.mSavedViewState = sparseArray;
            }
        }
    }

    @DexIgnore
    public void r(int i) {
        this.c = i;
    }

    @DexIgnore
    public void s() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "moveto STARTED: " + this.b);
        }
        this.b.performStart();
        this.f3804a.k(this.b, false);
    }

    @DexIgnore
    public void t() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "movefrom STARTED: " + this.b);
        }
        this.b.performStop();
        this.f3804a.l(this.b, false);
    }
}
