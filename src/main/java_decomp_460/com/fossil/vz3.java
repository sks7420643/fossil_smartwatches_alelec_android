package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vz3 {
    @DexIgnore
    void a(int i, int i2, int i3, int i4);

    @DexIgnore
    void b(Drawable drawable);

    @DexIgnore
    boolean c();
}
