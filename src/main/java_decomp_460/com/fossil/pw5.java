package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw5 extends RecyclerView.g<RecyclerView.ViewHolder> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<SecondTimezoneSetting> f2879a; // = new ArrayList();
    @DexIgnore
    public /* final */ ArrayList<c> b; // = new ArrayList<>();
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f2880a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(pw5 pw5, View view) {
            super(view);
            pq7.c(view, "view");
            View findViewById = view.findViewById(2131362541);
            if (findViewById != null) {
                this.f2880a = (FlexibleTextView) findViewById;
                String d = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(d)) {
                    this.f2880a.setBackgroundColor(Color.parseColor(d));
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.f2880a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public SecondTimezoneSetting f2881a;
        @DexIgnore
        public String b;
        @DexIgnore
        public a c; // = a.TYPE_HEADER;

        @DexIgnore
        public enum a {
            TYPE_HEADER,
            TYPE_VALUE
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final a b() {
            return this.c;
        }

        @DexIgnore
        public final SecondTimezoneSetting c() {
            return this.f2881a;
        }

        @DexIgnore
        public final void d(String str) {
            this.b = str;
        }

        @DexIgnore
        public final void e(a aVar) {
            pq7.c(aVar, "<set-?>");
            this.c = aVar;
        }

        @DexIgnore
        public final void f(SecondTimezoneSetting secondTimezoneSetting) {
            this.f2881a = secondTimezoneSetting;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FlexibleTextView f2882a;
        @DexIgnore
        public /* final */ b b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public a(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b bVar = this.b.b;
                if (bVar != null) {
                    bVar.a(this.b.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(pw5 pw5, View view, b bVar) {
            super(view);
            pq7.c(view, "view");
            this.b = bVar;
            View findViewById = view.findViewById(2131363009);
            View findViewById2 = view.findViewById(2131362783);
            String d = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
            String d2 = qn5.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d)) {
                findViewById.setBackgroundColor(Color.parseColor(d));
            }
            if (!TextUtils.isEmpty(d2)) {
                findViewById2.setBackgroundColor(Color.parseColor(d2));
            }
            view.setOnClickListener(new a(this));
            View findViewById3 = view.findViewById(2131363388);
            if (findViewById3 != null) {
                this.f2882a = (FlexibleTextView) findViewById3;
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.f2882a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(t.getTimeZoneName(), t2.getTimeZoneName());
        }
    }

    @DexIgnore
    public final void g(String str) {
        pq7.c(str, "filterText");
        if (TextUtils.isEmpty(str)) {
            k(this.f2879a);
        } else {
            this.b.clear();
            for (SecondTimezoneSetting secondTimezoneSetting : this.f2879a) {
                String timeZoneName = secondTimezoneSetting.getTimeZoneName();
                if (timeZoneName != null) {
                    String lowerCase = timeZoneName.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    String lowerCase2 = str.toLowerCase();
                    pq7.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                    if (wt7.v(lowerCase, lowerCase2, false, 2, null)) {
                        c cVar = new c();
                        cVar.f(secondTimezoneSetting);
                        cVar.e(c.a.TYPE_VALUE);
                        this.b.add(cVar);
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return this.b.get(i).b().ordinal();
    }

    @DexIgnore
    public final List<c> h() {
        return this.b;
    }

    @DexIgnore
    public final int i(String str) {
        pq7.c(str, "letter");
        int i = 0;
        for (c cVar : this.b) {
            if (cVar.b() == c.a.TYPE_HEADER && pq7.a(cVar.a(), str)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public final void j(b bVar) {
        pq7.c(bVar, "mItemClickListener");
        this.c = bVar;
    }

    @DexIgnore
    public final void k(List<SecondTimezoneSetting> list) {
        pq7.c(list, "secondTimeZoneList");
        this.b.clear();
        this.f2879a = list;
        List b0 = pm7.b0(list, new e());
        int size = b0.size();
        String str = "";
        int i = 0;
        while (i < size) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) b0.get(i);
            String timeZoneName = secondTimezoneSetting.getTimeZoneName();
            if (timeZoneName != null) {
                char[] charArray = timeZoneName.toCharArray();
                pq7.b(charArray, "(this as java.lang.String).toCharArray()");
                String valueOf = String.valueOf(Character.toUpperCase(charArray[0]));
                if (!TextUtils.equals(str, valueOf)) {
                    c cVar = new c();
                    cVar.d(valueOf);
                    cVar.e(c.a.TYPE_HEADER);
                    this.b.add(cVar);
                } else {
                    valueOf = str;
                }
                c cVar2 = new c();
                cVar2.f(secondTimezoneSetting);
                cVar2.e(c.a.TYPE_VALUE);
                this.b.add(cVar2);
                i++;
                str = valueOf;
            } else {
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        pq7.c(viewHolder, "holder");
        if (getItemViewType(i) == c.a.TYPE_HEADER.ordinal()) {
            FlexibleTextView a2 = ((a) viewHolder).a();
            String a3 = this.b.get(i).a();
            if (a3 != null) {
                a2.setText(a3);
            } else {
                pq7.i();
                throw null;
            }
        } else {
            SecondTimezoneSetting c2 = this.b.get(i).c();
            if (c2 != null) {
                FlexibleTextView b2 = ((d) viewHolder).b();
                b2.setText(c2.getTimeZoneName() + " (" + c2.getCityCode() + ')');
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        if (i == c.a.TYPE_HEADER.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558715, viewGroup, false);
            pq7.b(inflate, "v");
            return new a(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558734, viewGroup, false);
        pq7.b(inflate2, "v");
        return new d(this, inflate2, this.c);
    }
}
