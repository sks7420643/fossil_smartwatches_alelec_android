package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cr4;
import com.fossil.rw5;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia6 extends pv5 implements sa6, cr4.a {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public g37<hc5> g;
    @DexIgnore
    public ra6 h;
    @DexIgnore
    public rw5 i;
    @DexIgnore
    public cr4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ia6 a() {
            return new ia6();
        }

        @DexIgnore
        public final String b() {
            return ia6.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ hc5 c;

        @DexIgnore
        public b(View view, hc5 hc5) {
            this.b = view;
            this.c = hc5;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                hc5 hc5 = this.c;
                pq7.b(hc5, "binding");
                hc5.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = n76.s.b();
                local.d(b2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                pq7.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ia6 b;
        @DexIgnore
        public /* final */ /* synthetic */ hc5 c;

        @DexIgnore
        public c(ia6 ia6, hc5 hc5) {
            this.b = ia6;
            this.c = hc5;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.getItem(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                pq7.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = ia6.m.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                if (this.b.i != null && !TextUtils.isEmpty(f.getPlaceId())) {
                    this.c.q.setText("");
                    ra6 M6 = ia6.M6(this.b);
                    String spannableString = fullText.toString();
                    pq7.b(spannableString, "primaryText.toString()");
                    String placeId = f.getPlaceId();
                    pq7.b(placeId, "item.placeId");
                    cr4 cr42 = this.b.j;
                    if (cr42 != null) {
                        M6.n(spannableString, placeId, cr42.g());
                        cr4 cr43 = this.b.j;
                        if (cr43 != null) {
                            cr43.d();
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ hc5 b;

        @DexIgnore
        public d(ia6 ia6, hc5 hc5) {
            this.b = hc5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.b.s;
            pq7.b(imageView, "binding.clearIv");
            imageView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ ia6 b;

        @DexIgnore
        public e(ia6 ia6, hc5 hc5) {
            this.b = ia6;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            pq7.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements rw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ia6 f1600a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(ia6 ia6) {
            this.f1600a = ia6;
        }

        @DexIgnore
        @Override // com.fossil.rw5.b
        public void a(int i, boolean z) {
            ia6.M6(this.f1600a).q(i, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ia6 b;

        @DexIgnore
        public g(ia6 ia6) {
            this.b = ia6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hc5 b;

        @DexIgnore
        public h(hc5 hc5) {
            this.b = hc5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText("");
        }
    }

    /*
    static {
        String simpleName = ia6.class.getSimpleName();
        pq7.b(simpleName, "WeatherSettingFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ra6 M6(ia6 ia6) {
        ra6 ra6 = ia6.h;
        if (ra6 != null) {
            return ra6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cr4.a
    public void D1(boolean z) {
        g37<hc5> g37 = this.g;
        if (g37 != null) {
            hc5 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        Q6();
                        return;
                    }
                }
                O6();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.sa6
    public void E4() {
        FLogger.INSTANCE.getLocal().d(l, "showLocationError");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.J(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        g37<hc5> g37 = this.g;
        if (g37 == null) {
            pq7.n("mBinding");
            throw null;
        } else if (g37.a() == null || this.i == null) {
            return true;
        } else {
            ra6 ra6 = this.h;
            if (ra6 != null) {
                ra6.p();
                return true;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.sa6
    public void G1() {
        if (isActive()) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886393);
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            pq7.b(c2, "errorDescription");
            s37.K(childFragmentManager, c2);
        }
    }

    @DexIgnore
    @Override // com.fossil.sa6
    public void H(PlacesClient placesClient) {
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView;
        if (isActive()) {
            Context requireContext = requireContext();
            pq7.b(requireContext, "requireContext()");
            cr4 cr4 = new cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            g37<hc5> g37 = this.g;
            if (g37 != null) {
                hc5 a2 = g37.a();
                if (a2 != null && (flexibleAutoCompleteTextView = a2.q) != null) {
                    flexibleAutoCompleteTextView.setAdapter(this.j);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void O6() {
        g37<hc5> g37 = this.g;
        if (g37 != null) {
            hc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                pq7.b(flexibleTextView, "it.ftvLocationError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.y;
                pq7.b(flexibleTextView2, "it.tvAdded");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = a2.w;
                pq7.b(recyclerView, "it.recyclerView");
                recyclerView.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(ra6 ra6) {
        pq7.c(ra6, "presenter");
        this.h = ra6;
    }

    @DexIgnore
    public final void Q6() {
        g37<hc5> g37 = this.g;
        if (g37 != null) {
            hc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                pq7.b(flexibleTextView, "it.ftvLocationError");
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886813);
                pq7.b(c2, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                String format = String.format(c2, Arrays.copyOf(new Object[]{flexibleAutoCompleteTextView.getText()}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                FlexibleTextView flexibleTextView2 = a2.t;
                pq7.b(flexibleTextView2, "it.ftvLocationError");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.y;
                pq7.b(flexibleTextView3, "it.tvAdded");
                flexibleTextView3.setVisibility(8);
                RecyclerView recyclerView = a2.w;
                pq7.b(recyclerView, "it.recyclerView");
                recyclerView.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.sa6
    public void h2(List<WeatherLocationWrapper> list) {
        pq7.c(list, "locations");
        rw5 rw5 = this.i;
        if (rw5 != null) {
            rw5.k(ir7.b(list));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        hc5 hc5 = (hc5) aq0.f(layoutInflater, 2131558640, viewGroup, false, A6());
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = hc5.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(gl0.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new c(this, hc5));
        flexibleAutoCompleteTextView.addTextChangedListener(new d(this, hc5));
        flexibleAutoCompleteTextView.setOnKeyListener(new e(this, hc5));
        rw5 rw5 = new rw5();
        this.i = rw5;
        rw5.l(new f(this));
        RecyclerView recyclerView = hc5.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        pq7.b(hc5, "binding");
        View n = hc5.n();
        pq7.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new b(n, hc5));
        hc5.r.setOnClickListener(new g(this));
        hc5.s.setOnClickListener(new h(hc5));
        this.g = new g37<>(this, hc5);
        return hc5.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ra6 ra6 = this.h;
        if (ra6 != null) {
            ra6.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ra6 ra6 = this.h;
        if (ra6 != null) {
            ra6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.sa6
    public void x0(boolean z) {
        if (z) {
            Intent intent = new Intent();
            ra6 ra6 = this.h;
            if (ra6 != null) {
                intent.putExtra("WEATHER_WATCH_APP_SETTING", ra6.o());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }
}
