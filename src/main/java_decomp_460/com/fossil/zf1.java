package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zf1 implements sb1<Bitmap> {
    @DexIgnore
    @Override // com.fossil.sb1
    public final id1<Bitmap> b(Context context, id1<Bitmap> id1, int i, int i2) {
        if (jk1.s(i, i2)) {
            rd1 f = oa1.c(context).f();
            Bitmap bitmap = id1.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap c = c(f, bitmap, i, i2);
            return bitmap.equals(c) ? id1 : yf1.f(c, f);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }

    @DexIgnore
    public abstract Bitmap c(rd1 rd1, Bitmap bitmap, int i, int i2);
}
