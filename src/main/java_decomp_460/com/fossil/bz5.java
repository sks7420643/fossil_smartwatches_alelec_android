package com.fossil;

import com.portfolio.platform.view.RingProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bz5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f535a;

    /*
    static {
        int[] iArr = new int[RingProgressBar.a.values().length];
        f535a = iArr;
        iArr[RingProgressBar.a.ACTIVE_TIME.ordinal()] = 1;
        f535a[RingProgressBar.a.STEPS.ordinal()] = 2;
        f535a[RingProgressBar.a.CALORIES.ordinal()] = 3;
        f535a[RingProgressBar.a.SLEEP.ordinal()] = 4;
        f535a[RingProgressBar.a.GOAL.ordinal()] = 5;
    }
    */
}
