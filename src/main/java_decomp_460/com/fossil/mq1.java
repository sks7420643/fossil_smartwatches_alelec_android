package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mq1 extends jq1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutType e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<mq1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public mq1 createFromParcel(Parcel parcel) {
            return new mq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public mq1[] newArray(int i) {
            return new mq1[i];
        }
    }

    @DexIgnore
    public mq1(byte b, int i, WorkoutType workoutType, long j, boolean z) {
        super(np1.WORKOUT_RESUME, b, i);
        this.e = workoutType;
        this.f = j;
        this.g = z;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ mq1(Parcel parcel, kq7 kq7) {
        super(parcel);
        boolean z = true;
        this.e = g80.d(parcel.readByte());
        this.f = parcel.readLong();
        this.g = parcel.readInt() != 1 ? false : z;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(mq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            mq1 mq1 = (mq1) obj;
            if (this.e != mq1.e) {
                return false;
            }
            if (this.f != mq1.f) {
                return false;
            }
            return this.g == mq1.g;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.ResumeWorkoutRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.f;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.e.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Long.valueOf(this.f).hashCode()) * 31) + Boolean.valueOf(this.g).hashCode();
    }

    @DexIgnore
    public final boolean isRequiredGPS() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1, com.fossil.jq1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(super.toJSONObject(), jd0.G5, ey1.a(this.e)), jd0.H5, Long.valueOf(this.f)), jd0.F5, Boolean.valueOf(this.g));
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.jq1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.e.getValue());
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(this.g ? 1 : 0);
        }
    }
}
