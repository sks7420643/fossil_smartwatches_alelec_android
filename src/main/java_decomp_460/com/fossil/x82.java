package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x82 implements a72<Status> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ v72 f4073a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ r62 c;
    @DexIgnore
    public /* final */ /* synthetic */ t82 d;

    @DexIgnore
    public x82(t82 t82, v72 v72, boolean z, r62 r62) {
        this.d = t82;
        this.f4073a = v72;
        this.b = z;
        this.c = r62;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.z62] */
    @Override // com.fossil.a72
    public final /* synthetic */ void a(Status status) {
        Status status2 = status;
        o42.b(this.d.g).l();
        if (status2.D() && this.d.n()) {
            this.d.v();
        }
        this.f4073a.j(status2);
        if (this.b) {
            this.c.g();
        }
    }
}
