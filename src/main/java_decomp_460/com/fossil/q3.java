package com.fossil;

import android.os.Build;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ ry1 f2918a; // = new ry1(4, 0);
    @DexIgnore
    public static /* final */ yp[] b; // = {yp.c, yp.A0, yp.e, yp.h, yp.l, yp.L, yp.n, yp.f, yp.o0, yp.n0, yp.s0, yp.t0, yp.u0, yp.v0, yp.w0, yp.r, yp.t, yp.s, yp.u};
    @DexIgnore
    public static /* final */ Type[] c; // = {br1.class, er1.class};
    @DexIgnore
    public static /* final */ ve[] d; // = {new ve(12, 12, 30, 600), new ve(18, 18, 19, 600), new ve(24, 24, 14, 600), new ve(48, 48, 6, 600), new ve(72, 72, 4, 600)};
    @DexIgnore
    public static /* final */ ve[] e; // = {new ve(12, 12, 45, 600), new ve(24, 24, 22, 600), new ve(36, 36, 15, 600), new ve(104, 112, 4, 600)};
    @DexIgnore
    public static /* final */ q3 f; // = new q3();

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT < 28;
    }

    @DexIgnore
    public final boolean b(zk1 zk1, lp lpVar) {
        if (f.f(zk1)) {
            return em7.B(b, lpVar.y);
        }
        return true;
    }

    @DexIgnore
    public final boolean c(boolean z) {
        return !z;
    }

    @DexIgnore
    public final ve[] d(zk1 zk1) {
        switch (p3.f2772a[zk1.getDeviceType().ordinal()]) {
            case 1:
            case 2:
                return d;
            case 3:
            case 4:
            case 5:
            case 6:
                return f(zk1) ? new ve[0] : e;
            case 7:
            case 8:
            case 9:
                return new ve[0];
            default:
                throw new al7();
        }
    }

    @DexIgnore
    public final long e(boolean z) {
        return z ? 0 : 30000;
    }

    @DexIgnore
    public final boolean f(zk1 zk1) {
        if (zk1.getDeviceType().b()) {
            ry1 ry1 = zk1.h().get(Short.valueOf(ob.OTA.b));
            if (ry1 == null) {
                ry1 = hd0.y.f();
            }
            if (ry1.compareTo(hd0.y.x()) < 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Type[] g() {
        Object[] array = hm7.c(bs1.class, or1.class, tr1.class, vr1.class, xq1.class, xr1.class, ds1.class, ar1.class, br1.class, cr1.class, er1.class, gs1.class, gr1.class, hr1.class, jr1.class, mr1.class, nr1.class, hs1.class, qr1.class, rr1.class, js1.class, sr1.class, ps1.class, ts1.class, ls1.class, lr1.class, dr1.class, wr1.class, zr1.class, as1.class, is1.class, cs1.class, es1.class, qs1.class, rs1.class, ns1.class, kr1.class, os1.class, pr1.class, vq1.class, ur1.class, wq1.class, ks1.class, yr1.class, ms1.class, fs1.class, fr1.class, ss1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] h() {
        Object[] array = hm7.c(bs1.class, or1.class, tr1.class, vr1.class, xq1.class, xr1.class, ds1.class, ar1.class, br1.class, cr1.class, er1.class, gs1.class, gr1.class, hr1.class, jr1.class, mr1.class, nr1.class, hs1.class, qr1.class, rr1.class, js1.class, sr1.class, ps1.class, ts1.class, ls1.class, lr1.class, dr1.class, wr1.class, zr1.class, as1.class, is1.class, cs1.class, es1.class, qs1.class, rs1.class, ns1.class, kr1.class, os1.class, pr1.class, vq1.class, ur1.class, wq1.class, ks1.class, yr1.class, fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final ry1 i() {
        return f2918a;
    }

    @DexIgnore
    public final Type[] j() {
        Object[] array = hm7.c(or1.class, tr1.class, vr1.class, xq1.class, xr1.class, ar1.class, br1.class, cr1.class, er1.class, gr1.class, hr1.class, jr1.class, nr1.class, od.class, pd.class, qd.class, ir1.class, yq1.class, qr1.class, rr1.class, sr1.class, as1.class, is1.class, cs1.class, es1.class, rs1.class, wr1.class, os1.class, vq1.class, ur1.class, wq1.class, yr1.class, fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] k() {
        Object[] array = hm7.c(or1.class, tr1.class, vr1.class, xq1.class, xr1.class, ar1.class, br1.class, cr1.class, er1.class, gr1.class, hr1.class, jr1.class, nr1.class, od.class, pd.class, qd.class, ir1.class, yq1.class, qr1.class, rr1.class, sr1.class, as1.class, is1.class, cs1.class, es1.class, rs1.class, wr1.class, os1.class, vq1.class, ur1.class, wq1.class, yr1.class, fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] l() {
        Object[] array = hm7.c(or1.class, tr1.class, vr1.class, xq1.class, xr1.class, ar1.class, br1.class, cr1.class, er1.class, hr1.class, jr1.class, nr1.class, od.class, pd.class, qd.class, ir1.class, yq1.class, qr1.class, rr1.class, sr1.class, is1.class, cs1.class, es1.class, rs1.class, wr1.class, os1.class, vq1.class, ur1.class, wq1.class, yr1.class, fr1.class).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] m() {
        return c;
    }
}
