package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class tb extends Enum<tb> {
    @DexIgnore
    public static /* final */ tb c;
    @DexIgnore
    public static /* final */ tb d;
    @DexIgnore
    public static /* final */ tb e;
    @DexIgnore
    public static /* final */ tb f;
    @DexIgnore
    public static /* final */ tb g;
    @DexIgnore
    public static /* final */ tb h;
    @DexIgnore
    public static /* final */ tb i;
    @DexIgnore
    public static /* final */ /* synthetic */ tb[] j;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        tb tbVar = new tb("APPLICATION_NAME", 0, (byte) 1);
        tb tbVar2 = new tb("SENDER_NAME", 1, (byte) 2);
        c = tbVar2;
        tb tbVar3 = new tb("APP_BUNDLE_CRC32", 2, (byte) 4);
        d = tbVar3;
        tb tbVar4 = new tb("GROUP_ID", 3, (byte) 128);
        e = tbVar4;
        tb tbVar5 = new tb("APP_DISPLAY_NAME", 4, (byte) 129);
        tb tbVar6 = new tb("ICON_IMAGE", 5, (byte) 130);
        f = tbVar6;
        tb tbVar7 = new tb("PRIORITY", 6, (byte) 193);
        g = tbVar7;
        tb tbVar8 = new tb("HAND_MOVING", 7, (byte) 194);
        h = tbVar8;
        tb tbVar9 = new tb("VIBE", 8, (byte) 195);
        i = tbVar9;
        j = new tb[]{tbVar, tbVar2, tbVar3, tbVar4, tbVar5, tbVar6, tbVar7, tbVar8, tbVar9};
    }
    */

    @DexIgnore
    public tb(String str, int i2, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static tb valueOf(String str) {
        return (tb) Enum.valueOf(tb.class, str);
    }

    @DexIgnore
    public static tb[] values() {
        return (tb[]) j.clone();
    }
}
