package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr6 implements Factory<or6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f2862a;

    @DexIgnore
    public pr6(Provider<ThemeRepository> provider) {
        this.f2862a = provider;
    }

    @DexIgnore
    public static pr6 a(Provider<ThemeRepository> provider) {
        return new pr6(provider);
    }

    @DexIgnore
    public static or6 c(ThemeRepository themeRepository) {
        return new or6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public or6 get() {
        return c(this.f2862a.get());
    }
}
