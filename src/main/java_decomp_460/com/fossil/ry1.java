package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ry1 extends ox1 implements Parcelable, Comparable<ry1>, nx1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int major;
    @DexIgnore
    public /* final */ int minor;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ry1> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        /* renamed from: a */
        public ry1 createFromParcel(Parcel parcel) {
            pq7.c(parcel, "parcel");
            return new ry1(parcel, (kq7) null);
        }

        @DexIgnore
        public final ry1 b(byte[] bArr) {
            pq7.c(bArr, "data");
            if (bArr.length < 2) {
                return null;
            }
            return new ry1(bArr[0], bArr[1]);
        }

        @DexIgnore
        public final ry1 c(String str) {
            pq7.c(str, "versionString");
            List Y = wt7.Y(str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, null);
            if (Y.size() >= 2) {
                Integer c = ut7.c((String) Y.get(0));
                Integer c2 = ut7.c((String) Y.get(1));
                if (!(c == null || c2 == null)) {
                    return new ry1(c.intValue(), c2.intValue());
                }
            }
            return new ry1(0, 0);
        }

        @DexIgnore
        /* renamed from: d */
        public ry1[] newArray(int i) {
            return new ry1[i];
        }
    }

    @DexIgnore
    public ry1(byte b, byte b2) {
        this.major = hy1.p(b);
        this.minor = hy1.p(b2);
    }

    @DexIgnore
    public ry1(int i, int i2) {
        this.major = i;
        this.minor = i2;
    }

    @DexIgnore
    public ry1(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt());
    }

    @DexIgnore
    public /* synthetic */ ry1(Parcel parcel, kq7 kq7) {
        this(parcel);
    }

    @DexIgnore
    public ry1(short s, short s2) {
        this.major = hy1.n(s);
        this.minor = hy1.n(s2);
    }

    @DexIgnore
    @Override // java.lang.Object
    public ry1 clone() {
        return new ry1(this.major, this.minor);
    }

    @DexIgnore
    public int compareTo(ry1 ry1) {
        pq7.c(ry1, FacebookRequestErrorClassification.KEY_OTHER);
        int i = this.major;
        int i2 = ry1.major;
        if (i <= i2) {
            if (i < i2) {
                return -1;
            }
            int i3 = this.minor;
            int i4 = ry1.minor;
            if (i3 <= i4) {
                return i3 >= i4 ? 0 : -1;
            }
        }
        return 1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ry1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ry1 ry1 = (ry1) obj;
            if (this.major != ry1.major) {
                return false;
            }
            return this.minor == ry1.minor;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.common.version.Version");
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: IGET  (r1v0 int) = (r2v0 'this' com.fossil.ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.ry1.major int), ('.' char), (wrap: int : 0x000f: IGET  (r1v2 int) = (r2v0 'this' com.fossil.ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.ry1.minor int)] */
    public final String getShortDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public int hashCode() {
        return (this.major * 31) + this.minor;
    }

    @DexIgnore
    public final boolean isCompatible(ry1 ry1, boolean z) {
        pq7.c(ry1, FacebookRequestErrorClassification.KEY_OTHER);
        return this.major == ry1.major && (!z || this.minor >= ry1.minor);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("major", this.major).put("minor", this.minor);
        pq7.b(put, "JSONObject()\n           \u2026     .put(\"minor\", minor)");
        return put;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: IGET  (r1v0 int) = (r2v0 'this' com.fossil.ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.ry1.major int), ('.' char), (wrap: int : 0x000f: IGET  (r1v2 int) = (r2v0 'this' com.fossil.ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.ry1.minor int)] */
    @Override // com.fossil.ox1
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.major);
        }
        if (parcel != null) {
            parcel.writeInt(this.minor);
        }
    }
}
