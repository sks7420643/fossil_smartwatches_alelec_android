package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt1 extends mt1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ yl1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<lt1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public lt1 createFromParcel(Parcel parcel) {
            return new lt1(parcel, (kq7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public lt1[] newArray(int i) {
            return new lt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ lt1(Parcel parcel, kq7 kq7) {
        super((jq1) parcel.readParcelable(iq1.class.getClassLoader()), (nt1) parcel.readParcelable(nt1.class.getClassLoader()));
        this.d = (yl1) parcel.readParcelable(yl1.class.getClassLoader());
    }

    @DexIgnore
    public lt1(iq1 iq1, nt1 nt1) throws IllegalArgumentException {
        super(iq1, nt1);
        this.d = null;
    }

    @DexIgnore
    public lt1(iq1 iq1, nt1 nt1, yl1 yl1) {
        super(iq1, nt1);
        this.d = yl1;
    }

    @DexIgnore
    public lt1(iq1 iq1, yl1 yl1) throws IllegalArgumentException {
        super(iq1, null);
        this.d = yl1;
    }

    @DexIgnore
    public lt1(nt1 nt1) throws IllegalArgumentException {
        this(null, nt1, null);
    }

    @DexIgnore
    public lt1(yl1 yl1) throws IllegalArgumentException {
        this(null, null, yl1);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public JSONObject a() {
        Object obj;
        JSONObject a2 = super.a();
        jd0 jd0 = jd0.y4;
        yl1 yl1 = this.d;
        if (yl1 == null || (obj = yl1.toJSONObject()) == null) {
            obj = JSONObject.NULL;
        }
        return g80.k(a2, jd0, obj);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public byte[] a(short s, ry1 ry1) {
        jq1 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (this.d != null) {
                jSONObject2.put("dest", this.d.getDestination()).put("commute", this.d.getCommuteTimeInMinute()).put("traffic", this.d.getTraffic());
            } else {
                nt1 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
            }
            jSONObject.put("commuteApp._.config.commute_info", jSONObject2);
        } catch (JSONException e) {
            d90.i.i(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            d90.i.i(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        pq7.b(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c = hd0.y.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c);
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(lt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.d, ((lt1) obj).d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData");
    }

    @DexIgnore
    public final yl1 getCommuteTimeInfo() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public int hashCode() {
        int hashCode = super.hashCode();
        yl1 yl1 = this.d;
        return (yl1 != null ? yl1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.fossil.mt1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            yl1 yl1 = this.d;
            if (yl1 != null) {
                parcel.writeParcelable(yl1, i);
                return;
            }
            throw new il7("null cannot be cast to non-null type android.os.Parcelable");
        }
    }
}
