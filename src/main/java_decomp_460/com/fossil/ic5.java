package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ic5 extends hc5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131361906, 1);
        B.put(2131362546, 2);
        B.put(2131361903, 3);
        B.put(2131362129, 4);
        B.put(2131362783, 5);
        B.put(2131362479, 6);
        B.put(2131363259, 7);
        B.put(2131362988, 8);
    }
    */

    @DexIgnore
    public ic5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 9, A, B));
    }

    @DexIgnore
    public ic5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleAutoCompleteTextView) objArr[3], (RTLImageView) objArr[1], (ImageView) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (View) objArr[5], (RecyclerView) objArr[8], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[7]);
        this.z = -1;
        this.x.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.z = 1;
        }
        w();
    }
}
