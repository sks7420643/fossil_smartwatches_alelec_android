package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class li1 implements ei1 {
    @DexIgnore
    public /* final */ Set<qj1<?>> b; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public void c() {
        this.b.clear();
    }

    @DexIgnore
    public List<qj1<?>> e() {
        return jk1.j(this.b);
    }

    @DexIgnore
    public void g(qj1<?> qj1) {
        this.b.add(qj1);
    }

    @DexIgnore
    public void l(qj1<?> qj1) {
        this.b.remove(qj1);
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onDestroy() {
        for (qj1 qj1 : jk1.j(this.b)) {
            qj1.onDestroy();
        }
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStart() {
        for (qj1 qj1 : jk1.j(this.b)) {
            qj1.onStart();
        }
    }

    @DexIgnore
    @Override // com.fossil.ei1
    public void onStop() {
        for (qj1 qj1 : jk1.j(this.b)) {
            qj1.onStop();
        }
    }
}
