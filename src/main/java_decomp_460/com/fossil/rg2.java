package com.fossil;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rg2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ql2 implements rg2 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rg2$a$a")
        /* renamed from: com.fossil.rg2$a$a  reason: collision with other inner class name */
        public static final class C0207a extends rl2 implements rg2 {
            @DexIgnore
            public C0207a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        @DexIgnore
        public a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        @DexIgnore
        public static rg2 e(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            return queryLocalInterface instanceof rg2 ? (rg2) queryLocalInterface : new C0207a(iBinder);
        }
    }
}
