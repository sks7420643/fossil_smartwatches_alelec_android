package com.fossil;

import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class db7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public HashSet<String> f764a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public db7() {
        this(null, null, null, false, 15, null);
    }

    @DexIgnore
    public db7(HashSet<String> hashSet, String str, String str2, boolean z) {
        pq7.c(hashSet, "list");
        pq7.c(str2, "ringId");
        this.f764a = hashSet;
        this.b = str;
        this.c = str2;
        this.d = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ db7(HashSet hashSet, String str, String str2, boolean z, int i, kq7 kq7) {
        this((i & 1) != 0 ? new HashSet() : hashSet, (i & 2) != 0 ? null : str, (i & 4) != 0 ? "" : str2, (i & 8) != 0 ? false : z);
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final HashSet<String> b() {
        return this.f764a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e(String str) {
        this.b = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof db7) {
                db7 db7 = (db7) obj;
                if (!pq7.a(this.f764a, db7.f764a) || !pq7.a(this.b, db7.b) || !pq7.a(this.c, db7.c) || this.d != db7.d) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void f(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void g(String str) {
        pq7.c(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        HashSet<String> hashSet = this.f764a;
        int hashCode = hashSet != null ? hashSet.hashCode() : 0;
        String str = this.b;
        int hashCode2 = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        if (str2 != null) {
            i = str2.hashCode();
        }
        boolean z = this.d;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((hashCode * 31) + hashCode2) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "SelectedComplications(list=" + this.f764a + ", currentId=" + this.b + ", ringId=" + this.c + ", isGoalRingEnabled=" + this.d + ")";
    }
}
