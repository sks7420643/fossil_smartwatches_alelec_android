package com.fossil;

import androidx.work.impl.WorkDatabase;
import com.fossil.a11;
import java.util.LinkedList;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v31 implements Runnable {
    @DexIgnore
    public /* final */ l11 b; // = new l11();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends v31 {
        @DexIgnore
        public /* final */ /* synthetic */ s11 c;
        @DexIgnore
        public /* final */ /* synthetic */ UUID d;

        @DexIgnore
        public a(s11 s11, UUID uuid) {
            this.c = s11;
            this.d = uuid;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.v31
        public void g() {
            WorkDatabase p = this.c.p();
            p.beginTransaction();
            try {
                a(this.c, this.d.toString());
                p.setTransactionSuccessful();
                p.endTransaction();
                f(this.c);
            } catch (Throwable th) {
                p.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends v31 {
        @DexIgnore
        public /* final */ /* synthetic */ s11 c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        public b(s11 s11, String str, boolean z) {
            this.c = s11;
            this.d = str;
            this.e = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.v31
        public void g() {
            WorkDatabase p = this.c.p();
            p.beginTransaction();
            try {
                for (String str : p.j().m(this.d)) {
                    a(this.c, str);
                }
                p.setTransactionSuccessful();
                p.endTransaction();
                if (this.e) {
                    f(this.c);
                }
            } catch (Throwable th) {
                p.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public static v31 b(UUID uuid, s11 s11) {
        return new a(s11, uuid);
    }

    @DexIgnore
    public static v31 c(String str, s11 s11, boolean z) {
        return new b(s11, str, z);
    }

    @DexIgnore
    public void a(s11 s11, String str) {
        e(s11.p(), str);
        s11.n().k(str);
        for (n11 n11 : s11.o()) {
            n11.e(str);
        }
    }

    @DexIgnore
    public a11 d() {
        return this.b;
    }

    @DexIgnore
    public final void e(WorkDatabase workDatabase, String str) {
        p31 j = workDatabase.j();
        a31 b2 = workDatabase.b();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            f11 n = j.n(str2);
            if (!(n == f11.SUCCEEDED || n == f11.FAILED)) {
                j.a(f11.CANCELLED, str2);
            }
            linkedList.addAll(b2.b(str2));
        }
    }

    @DexIgnore
    public void f(s11 s11) {
        o11.b(s11.j(), s11.p(), s11.o());
    }

    @DexIgnore
    public abstract void g();

    @DexIgnore
    public void run() {
        try {
            g();
            this.b.a(a11.f178a);
        } catch (Throwable th) {
            this.b.a(new a11.b.a(th));
        }
    }
}
