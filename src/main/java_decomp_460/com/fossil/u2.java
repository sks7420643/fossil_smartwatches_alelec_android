package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u2 extends c2 {
    @DexIgnore
    public static /* final */ t2 CREATOR; // = new t2(null);
    @DexIgnore
    public /* final */ vn1 e;

    @DexIgnore
    public u2(byte b, vn1 vn1) {
        super(lt.MUSIC_EVENT, b, false, 4);
        this.e = vn1;
    }

    @DexIgnore
    public /* synthetic */ u2(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            this.e = vn1.valueOf(readString);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
