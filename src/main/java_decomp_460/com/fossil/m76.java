package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bw5;
import com.fossil.cr4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m76 extends pv5 implements w76, cr4.a {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<p35> g;
    @DexIgnore
    public v76 h;
    @DexIgnore
    public bw5 i;
    @DexIgnore
    public cr4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final m76 a() {
            return new m76();
        }

        @DexIgnore
        public final String b() {
            return m76.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements bw5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m76 f2312a;

        @DexIgnore
        public b(m76 m76) {
            this.f2312a = m76;
        }

        @DexIgnore
        @Override // com.fossil.bw5.b
        public void a(String str) {
            pq7.c(str, "address");
            m76.N6(this.f2312a).n(wt7.u0(str).toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ p35 c;

        @DexIgnore
        public c(View view, p35 p35) {
            this.b = view;
            this.c = p35;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.x.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                p35 p35 = this.c;
                pq7.b(p35, "binding");
                p35.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = m76.s.b();
                local.d(b2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                pq7.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m76 b;

        @DexIgnore
        public d(m76 m76, p35 p35) {
            this.b = m76;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.getItem(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                pq7.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = n76.s.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                this.b.k = fullText.toString();
                if (this.b.k != null) {
                    String str = this.b.k;
                    if (str != null) {
                        if (str.length() > 0) {
                            v76 N6 = m76.N6(this.b);
                            String str2 = this.b.k;
                            if (str2 == null) {
                                pq7.i();
                                throw null;
                            } else if (str2 != null) {
                                N6.n(wt7.u0(str2).toString());
                            } else {
                                throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ p35 b;

        @DexIgnore
        public e(m76 m76, p35 p35) {
            this.b = p35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.b.r;
            pq7.b(imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ m76 b;

        @DexIgnore
        public f(m76 m76, p35 p35) {
            this.b = m76;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            pq7.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m76 b;

        @DexIgnore
        public g(m76 m76) {
            this.b = m76;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m76 b;
        @DexIgnore
        public /* final */ /* synthetic */ p35 c;

        @DexIgnore
        public h(m76 m76, p35 p35) {
            this.b = m76;
            this.c = p35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.k = "";
            this.c.q.setText("");
            this.b.Q6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m76 b;

        @DexIgnore
        public i(m76 m76) {
            this.b = m76;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            FlexibleAutoCompleteTextView flexibleAutoCompleteTextView;
            Editable text;
            MapPickerActivity.a aVar = MapPickerActivity.A;
            m76 m76 = this.b;
            p35 p35 = (p35) m76.M6(m76).a();
            if (p35 == null || (flexibleAutoCompleteTextView = p35.q) == null || (text = flexibleAutoCompleteTextView.getText()) == null || (str = text.toString()) == null) {
                str = "";
            }
            MapPickerActivity.a.b(aVar, m76, 0.0d, 0.0d, str, 6, null);
        }
    }

    /*
    static {
        String simpleName = m76.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeSettingsDefau\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 M6(m76 m76) {
        g37<p35> g37 = m76.g;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ v76 N6(m76 m76) {
        v76 v76 = m76.h;
        if (v76 != null) {
            return v76;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cr4.a
    public void D1(boolean z) {
        g37<p35> g37 = this.g;
        if (g37 != null) {
            p35 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        this.k = null;
                        T6();
                        return;
                    }
                }
                Q6();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        String str;
        g37<p35> g37 = this.g;
        if (g37 == null) {
            pq7.n("mBinding");
            throw null;
        } else if (g37.a() == null) {
            return true;
        } else {
            v76 v76 = this.h;
            if (v76 != null) {
                String str2 = this.k;
                if (str2 == null) {
                    str = null;
                } else if (str2 != null) {
                    str = wt7.u0(str2).toString();
                } else {
                    throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                }
                v76.n(str);
                return true;
            }
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w76
    public void H(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            pq7.b(requireContext, "requireContext()");
            cr4 cr4 = new cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            g37<p35> g37 = this.g;
            if (g37 != null) {
                p35 a2 = g37.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.j);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    pq7.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0);
                        a2.q.setSelection(u0.length());
                        return;
                    }
                    Q6();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void Q6() {
        g37<p35> g37 = this.g;
        if (g37 != null) {
            p35 a2 = g37.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.z;
                pq7.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                pq7.b(flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: R6 */
    public void M5(v76 v76) {
        pq7.c(v76, "presenter");
        this.h = v76;
    }

    @DexIgnore
    @Override // com.fossil.w76
    public void S3(String str) {
        if (str != null) {
            Intent intent = new Intent();
            intent.putExtra("KEY_DEFAULT_PLACE", str);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(-1, intent);
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void S6(String str) {
        pq7.c(str, "address");
        g37<p35> g37 = this.g;
        if (g37 != null) {
            p35 a2 = g37.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText((CharSequence) str, false);
                    this.k = str;
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void T6() {
        g37<p35> g37 = this.g;
        if (g37 != null) {
            p35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                pq7.b(flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.h0.c();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(2131886360, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                pq7.b(flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.z;
                pq7.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w76
    public void V1(String str) {
        boolean z = true;
        pq7.c(str, "address");
        if (isActive()) {
            g37<p35> g37 = this.g;
            if (g37 != null) {
                p35 a2 = g37.a();
                if (a2 != null) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    pq7.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    pq7.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0);
                        return;
                    }
                    if (str.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        a2.q.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w76
    public void h0(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        pq7.c(list, "recentSearchedList");
        Q6();
        if (!list.isEmpty()) {
            bw5 bw5 = this.i;
            if (bw5 != null) {
                bw5.k(pm7.j0(list));
            }
            g37<p35> g37 = this.g;
            if (g37 != null) {
                p35 a2 = g37.a();
                if (a2 != null && (linearLayout2 = a2.z) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<p35> g372 = this.g;
        if (g372 != null) {
            p35 a3 = g372.a();
            if (a3 != null && (linearLayout = a3.z) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            if (string != null) {
                S6(string);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        p35 p35 = (p35) aq0.f(layoutInflater, 2131558517, viewGroup, false, A6());
        String d2 = qn5.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d2)) {
            p35.x.setBackgroundColor(Color.parseColor(d2));
        }
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = p35.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(gl0.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new d(this, p35));
        flexibleAutoCompleteTextView.addTextChangedListener(new e(this, p35));
        flexibleAutoCompleteTextView.setOnKeyListener(new f(this, p35));
        p35.u.setOnClickListener(new g(this));
        p35.r.setOnClickListener(new h(this, p35));
        p35.v.setOnClickListener(new i(this));
        bw5 bw5 = new bw5();
        bw5.l(new b(this));
        this.i = bw5;
        RecyclerView recyclerView = p35.B;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        pq7.b(p35, "binding");
        View n = p35.n();
        pq7.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new c(n, p35));
        this.g = new g37<>(this, p35);
        return p35.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        v76 v76 = this.h;
        if (v76 != null) {
            v76.m();
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        v76 v76 = this.h;
        if (v76 != null) {
            v76.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w76
    public void setTitle(String str) {
        pq7.c(str, "title");
        g37<p35> g37 = this.g;
        if (g37 != null) {
            p35 a2 = g37.a();
            FlexibleTextView flexibleTextView = a2 != null ? a2.t : null;
            if (flexibleTextView != null) {
                pq7.b(flexibleTextView, "mBinding.get()?.ftvTitle!!");
                flexibleTextView.setText(str);
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
