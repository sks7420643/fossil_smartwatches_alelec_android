package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PinObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qp5 extends BaseDbProvider implements pp5 {
    @DexIgnore
    public qp5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.pp5
    public void f(PinObject pinObject) {
        if (pinObject != null) {
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.TAG;
                local.d(str, "Unpin new object - uuid=" + pinObject.getUuid() + ", className=" + pinObject.getClassName());
                o().delete((Dao<PinObject, Integer>) pinObject);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                local2.e(str2, "Error inside " + this.TAG + ".unpin - e=" + e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{PinObject.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.pp5
    public List<PinObject> j(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<PinObject, Integer> queryBuilder = o().queryBuilder();
            Where<PinObject, Integer> where = queryBuilder.where();
            where.eq(PinObject.COLUMN_CLASS_NAME, str);
            queryBuilder.setWhere(where);
            List<PinObject> query = o().query(queryBuilder.prepare());
            return (query == null || query.isEmpty()) ? arrayList : query;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Error inside " + this.TAG + ".find - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public final Dao<PinObject, Integer> o() throws SQLException {
        return this.databaseHelper.getDao(PinObject.class);
    }
}
