package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f24<E> extends m34<E> {
    @DexIgnore
    public /* final */ m34<E> forward;

    @DexIgnore
    public f24(m34<E> m34) {
        super(i44.from(m34.comparator()).reverse());
        this.forward = m34;
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.m34
    public E ceiling(E e) {
        return this.forward.floor(e);
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        return this.forward.contains(obj);
    }

    @DexIgnore
    @Override // com.fossil.m34
    public m34<E> createDescendingSet() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.m34, com.fossil.m34
    public h54<E> descendingIterator() {
        return this.forward.iterator();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.m34, com.fossil.m34
    public m34<E> descendingSet() {
        return this.forward;
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.m34
    public E floor(E e) {
        return this.forward.ceiling(e);
    }

    @DexIgnore
    @Override // com.fossil.m34
    public m34<E> headSetImpl(E e, boolean z) {
        return this.forward.tailSet((m34<E>) e, z).descendingSet();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.m34
    public E higher(E e) {
        return this.forward.lower(e);
    }

    @DexIgnore
    @Override // com.fossil.m34
    public int indexOf(Object obj) {
        int indexOf = this.forward.indexOf(obj);
        return indexOf == -1 ? indexOf : (size() - 1) - indexOf;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return this.forward.isPartialView();
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, com.fossil.u24, com.fossil.u24, java.util.NavigableSet, java.lang.Iterable, com.fossil.m34, com.fossil.m34, java.util.AbstractCollection, com.fossil.h34, com.fossil.h34
    public h54<E> iterator() {
        return this.forward.descendingIterator();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.m34
    public E lower(E e) {
        return this.forward.higher(e);
    }

    @DexIgnore
    public int size() {
        return this.forward.size();
    }

    @DexIgnore
    @Override // com.fossil.m34
    public m34<E> subSetImpl(E e, boolean z, E e2, boolean z2) {
        return this.forward.subSet((boolean) e2, z2, (boolean) e, z).descendingSet();
    }

    @DexIgnore
    @Override // com.fossil.m34
    public m34<E> tailSetImpl(E e, boolean z) {
        return this.forward.headSet((m34<E>) e, z).descendingSet();
    }
}
