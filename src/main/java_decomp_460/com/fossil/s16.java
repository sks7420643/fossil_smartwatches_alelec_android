package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s16 implements Factory<r16> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ s16 f3193a; // = new s16();
    }

    @DexIgnore
    public static s16 a() {
        return a.f3193a;
    }

    @DexIgnore
    public static r16 c() {
        return new r16();
    }

    @DexIgnore
    /* renamed from: b */
    public r16 get() {
        return c();
    }
}
