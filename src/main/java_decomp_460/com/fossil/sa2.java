package com.fossil;

import android.app.Dialog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa2 extends q92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ Dialog f3225a;
    @DexIgnore
    public /* final */ /* synthetic */ ta2 b;

    @DexIgnore
    public sa2(ta2 ta2, Dialog dialog) {
        this.b = ta2;
        this.f3225a = dialog;
    }

    @DexIgnore
    @Override // com.fossil.q92
    public final void a() {
        this.b.c.p();
        if (this.f3225a.isShowing()) {
            this.f3225a.dismiss();
        }
    }
}
