package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq2 extends mq2 implements rq2 {
    @DexIgnore
    public tq2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    @Override // com.fossil.rq2
    public final void M(pq2 pq2) throws RemoteException {
        Parcel d = d();
        qr2.c(d, pq2);
        n(1, d);
    }
}
