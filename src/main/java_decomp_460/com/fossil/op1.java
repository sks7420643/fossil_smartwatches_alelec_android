package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum op1 {
    GET,
    SET;
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final op1 a(t8 t8Var) {
            int i = xc.f4088a[t8Var.ordinal()];
            if (i == 1) {
                return op1.GET;
            }
            if (i == 2) {
                return op1.SET;
            }
            throw new al7();
        }
    }
}
