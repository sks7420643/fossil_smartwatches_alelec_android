package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseIntArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s61 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SparseIntArray f3207a; // = new SparseIntArray();
    @DexIgnore
    public /* final */ o51 b; // = new o51(0, 1, null);
    @DexIgnore
    public /* final */ g51 c;

    @DexIgnore
    public s61(g51 g51) {
        pq7.c(g51, "bitmapPool");
        this.c = g51;
    }

    @DexIgnore
    public final void a(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int identityHashCode = System.identityHashCode(bitmap);
        int i = this.f3207a.get(identityHashCode) - 1;
        this.f3207a.put(identityHashCode, i);
        if (q81.c.a() && q81.c.b() <= 2) {
            Log.println(2, "BitmapReferenceCounter", "DECREMENT: [" + identityHashCode + ", " + i + ']');
        }
        if (i <= 0) {
            this.f3207a.delete(identityHashCode);
            if (!this.b.b(identityHashCode)) {
                this.c.b(bitmap);
            }
        }
    }

    @DexIgnore
    public final void b(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        int identityHashCode = System.identityHashCode(bitmap);
        int i = this.f3207a.get(identityHashCode) + 1;
        this.f3207a.put(identityHashCode, i);
        if (q81.c.a() && q81.c.b() <= 2) {
            Log.println(2, "BitmapReferenceCounter", "INCREMENT: [" + identityHashCode + ", " + i + ']');
        }
    }

    @DexIgnore
    public final void c(Bitmap bitmap) {
        pq7.c(bitmap, "bitmap");
        this.b.a(System.identityHashCode(bitmap));
    }
}
