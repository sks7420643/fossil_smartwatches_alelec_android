package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fw1 extends ox1 implements Parcelable, Serializable, nx1 {
    @DexIgnore
    public String b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ Format d;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public fw1(android.os.Parcel r5) {
        /*
            r4 = this;
            r2 = 0
            java.lang.String r0 = r5.readString()
            if (r0 == 0) goto L_0x0029
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            byte[] r1 = r5.createByteArray()
            if (r1 == 0) goto L_0x0025
            java.lang.String r2 = "parcel.createByteArray()!!"
            com.fossil.pq7.b(r1, r2)
            int r2 = r5.readInt()
            com.fossil.imagefilters.Format[] r3 = com.fossil.imagefilters.Format.values()
            r2 = r3[r2]
            r4.<init>(r0, r1, r2)
            return
        L_0x0025:
            com.fossil.pq7.i()
            throw r2
        L_0x0029:
            com.fossil.pq7.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fw1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public fw1(String str, byte[] bArr, Format format) {
        this.b = str;
        this.c = bArr;
        this.d = format;
    }

    @DexIgnore
    public static /* synthetic */ cc0 a(fw1 fw1, lv1 lv1, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                str = fw1.b;
            }
            return fw1.a(lv1, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getImageNode");
    }

    @DexIgnore
    public final cc0 a(lv1 lv1, String str) {
        byte[] a2 = a(lv1);
        if (!(a2.length == 0)) {
            return new cc0(str, a2);
        }
        return null;
    }

    @DexIgnore
    public final String a() {
        return g80.f(this.d);
    }

    @DexIgnore
    public final void a(String str) {
        this.b = str;
    }

    @DexIgnore
    public final byte[] a(lv1 lv1) {
        try {
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(this.c, 0, this.c.length);
            pq7.b(decodeByteArray, "bitmap");
            if (decodeByteArray.getByteCount() <= 0) {
                return new byte[0];
            }
            byte[] encode = EInkImageFactory.encode(decodeByteArray, FilterType.DIRECT_MAPPING, new OutputSettings(lv1.getWidth(), lv1.getHeight(), this.d, false, false));
            pq7.b(encode, "EInkImageFactory.encode(\u2026 false)\n                )");
            decodeByteArray.recycle();
            return encode;
        } catch (Throwable th) {
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // java.lang.Object
    public abstract fw1 clone();

    @DexIgnore
    @Override // java.lang.Object
    public abstract /* synthetic */ nx1 clone();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getBitmapImageData() {
        return this.c;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return gy1.d(gy1.d(gy1.d(new JSONObject(), jd0.H, this.b), jd0.F0, Integer.valueOf(this.c.length)), jd0.U5, ey1.a(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
