package com.fossil;

import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rs0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ LifecycleRegistry f3150a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public a c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ LifecycleRegistry b;
        @DexIgnore
        public /* final */ Lifecycle.a c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public a(LifecycleRegistry lifecycleRegistry, Lifecycle.a aVar) {
            this.b = lifecycleRegistry;
            this.c = aVar;
        }

        @DexIgnore
        public void run() {
            if (!this.d) {
                this.b.i(this.c);
                this.d = true;
            }
        }
    }

    @DexIgnore
    public rs0(LifecycleOwner lifecycleOwner) {
        this.f3150a = new LifecycleRegistry(lifecycleOwner);
    }

    @DexIgnore
    public Lifecycle a() {
        return this.f3150a;
    }

    @DexIgnore
    public void b() {
        f(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public void c() {
        f(Lifecycle.a.ON_CREATE);
    }

    @DexIgnore
    public void d() {
        f(Lifecycle.a.ON_STOP);
        f(Lifecycle.a.ON_DESTROY);
    }

    @DexIgnore
    public void e() {
        f(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public final void f(Lifecycle.a aVar) {
        a aVar2 = this.c;
        if (aVar2 != null) {
            aVar2.run();
        }
        a aVar3 = new a(this.f3150a, aVar);
        this.c = aVar3;
        this.b.postAtFrontOfQueue(aVar3);
    }
}
