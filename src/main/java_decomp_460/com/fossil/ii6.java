package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii6 extends pv5 implements hi6, dx5, aw5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<r55> g;
    @DexIgnore
    public gi6 h;
    @DexIgnore
    public cx5 i;
    @DexIgnore
    public HeartRateOverviewFragment j;
    @DexIgnore
    public f67 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ii6.m;
        }

        @DexIgnore
        public final ii6 b() {
            return new ii6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends f67 {
        @DexIgnore
        public /* final */ /* synthetic */ ii6 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ii6 ii6, LinearLayoutManager linearLayoutManager, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager2);
            this.e = ii6;
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void b(int i) {
            ii6.K6(this.e).p();
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void c(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = ii6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "DashboardHeartRateFragme\u2026::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gi6 K6(ii6 ii6) {
        gi6 gi6 = ii6.h;
        if (gi6 != null) {
            return gi6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return m;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(gi6 gi6) {
        pq7.c(gi6, "presenter");
        this.h = gi6;
    }

    @DexIgnore
    @Override // com.fossil.dx5
    public void Q(Date date) {
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.C;
            pq7.b(context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        g37<r55> g37;
        RecyclerView.ViewHolder findViewHolderForAdapterPosition;
        View view;
        if (isVisible() && (g37 = this.g) != null) {
            if (g37 != null) {
                r55 a2 = g37.a();
                RecyclerView recyclerView = a2 != null ? a2.q : null;
                if (recyclerView == null || (findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0)) == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    if (recyclerView != null) {
                        recyclerView.smoothScrollToPosition(0);
                    }
                    f67 f67 = this.k;
                    if (f67 != null) {
                        f67.d();
                        return;
                    }
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hi6
    public void d() {
        f67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        r55 r55 = (r55) aq0.f(layoutInflater, 2131558546, viewGroup, false, A6());
        HeartRateOverviewFragment heartRateOverviewFragment = (HeartRateOverviewFragment) getChildFragmentManager().Z("HeartRateOverviewFragment");
        this.j = heartRateOverviewFragment;
        if (heartRateOverviewFragment == null) {
            this.j = new HeartRateOverviewFragment();
        }
        bx5 bx5 = new bx5();
        PortfolioApp c = PortfolioApp.h0.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        HeartRateOverviewFragment heartRateOverviewFragment2 = this.j;
        if (heartRateOverviewFragment2 != null) {
            this.i = new cx5(bx5, c, this, childFragmentManager, heartRateOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            this.k = new b(this, linearLayoutManager, linearLayoutManager);
            bd6 bd6 = new bd6(linearLayoutManager.q2());
            Drawable f = gl0.f(requireContext(), 2131230856);
            if (f != null) {
                pq7.b(f, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                bd6.h(f);
                RecyclerView recyclerView = r55.q;
                pq7.b(recyclerView, "this");
                recyclerView.setLayoutManager(linearLayoutManager);
                cx5 cx5 = this.i;
                if (cx5 != null) {
                    recyclerView.setAdapter(cx5);
                    f67 f67 = this.k;
                    if (f67 != null) {
                        recyclerView.addOnScrollListener(f67);
                        recyclerView.setItemViewCacheSize(0);
                        recyclerView.addItemDecoration(bd6);
                        if (recyclerView.getItemAnimator() instanceof pv0) {
                            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                            if (itemAnimator != null) {
                                ((pv0) itemAnimator).setSupportsChangeAnimations(false);
                            } else {
                                throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.SimpleItemAnimator");
                            }
                        }
                        gi6 gi6 = this.h;
                        if (gi6 != null) {
                            gi6.n();
                            g37<r55> g37 = new g37<>(this, r55);
                            this.g = g37;
                            if (g37 != null) {
                                r55 a2 = g37.a();
                                if (a2 != null) {
                                    pq7.b(a2, "mBinding.get()!!");
                                    return a2.n();
                                }
                                pq7.i();
                                throw null;
                            }
                            pq7.n("mBinding");
                            throw null;
                        }
                        pq7.n("mPresenter");
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mDashboardHeartRatesAdapter");
                throw null;
            }
            pq7.i();
            throw null;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(m, "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        gi6 gi6 = this.h;
        if (gi6 != null) {
            gi6.o();
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        gi6 gi6 = this.h;
        if (gi6 != null) {
            gi6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        gi6 gi6 = this.h;
        if (gi6 != null) {
            gi6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("heart_rate_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ts0 a2 = vs0.e(activity).a(y67.class);
            pq7.b(a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            y67 y67 = (y67) a2;
        }
    }

    @DexIgnore
    @Override // com.fossil.dx5
    public void q0(Date date, Date date2) {
        pq7.c(date, "startWeekDate");
        pq7.c(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.fossil.hi6
    public void r3(cu0<DailyHeartRateSummary> cu0) {
        cx5 cx5 = this.i;
        if (cx5 != null) {
            cx5.u(cu0);
        } else {
            pq7.n("mDashboardHeartRatesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
