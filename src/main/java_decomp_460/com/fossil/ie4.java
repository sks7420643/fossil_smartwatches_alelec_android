package com.fossil;

import android.content.Context;
import com.fossil.a74;
import com.fossil.je4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ie4 implements je4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ke4 f1612a;

    @DexIgnore
    public ie4(Context context) {
        this.f1612a = ke4.a(context);
    }

    @DexIgnore
    public static a74<je4> b() {
        a74.b a2 = a74.a(je4.class);
        a2.b(k74.f(Context.class));
        a2.f(he4.b());
        return a2.d();
    }

    @DexIgnore
    public static /* synthetic */ je4 c(b74 b74) {
        return new ie4((Context) b74.get(Context.class));
    }

    @DexIgnore
    @Override // com.fossil.je4
    public je4.a a(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean c = this.f1612a.c(str, currentTimeMillis);
        boolean b = this.f1612a.b(currentTimeMillis);
        return (!c || !b) ? b ? je4.a.GLOBAL : c ? je4.a.SDK : je4.a.NONE : je4.a.COMBINED;
    }
}
