package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Type[] f3775a; // = new Type[0];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements GenericArrayType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Type componentType;

        @DexIgnore
        public a(Type type) {
            this.componentType = vj4.b(type);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && vj4.f(this, (GenericArrayType) obj);
        }

        @DexIgnore
        public Type getGenericComponentType() {
            return this.componentType;
        }

        @DexIgnore
        public int hashCode() {
            return this.componentType.hashCode();
        }

        @DexIgnore
        public String toString() {
            return vj4.u(this.componentType) + "[]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ParameterizedType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Type ownerType;
        @DexIgnore
        public /* final */ Type rawType;
        @DexIgnore
        public /* final */ Type[] typeArguments;

        @DexIgnore
        public b(Type type, Type type2, Type... typeArr) {
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                uj4.a(type == null ? Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null : true);
            }
            this.ownerType = type == null ? null : vj4.b(type);
            this.rawType = vj4.b(type2);
            Type[] typeArr2 = (Type[]) typeArr.clone();
            this.typeArguments = typeArr2;
            int length = typeArr2.length;
            for (int i = 0; i < length; i++) {
                uj4.b(this.typeArguments[i]);
                vj4.c(this.typeArguments[i]);
                Type[] typeArr3 = this.typeArguments;
                typeArr3[i] = vj4.b(typeArr3[i]);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && vj4.f(this, (ParameterizedType) obj);
        }

        @DexIgnore
        public Type[] getActualTypeArguments() {
            return (Type[]) this.typeArguments.clone();
        }

        @DexIgnore
        public Type getOwnerType() {
            return this.ownerType;
        }

        @DexIgnore
        public Type getRawType() {
            return this.rawType;
        }

        @DexIgnore
        public int hashCode() {
            return (Arrays.hashCode(this.typeArguments) ^ this.rawType.hashCode()) ^ vj4.m(this.ownerType);
        }

        @DexIgnore
        public String toString() {
            int length = this.typeArguments.length;
            if (length == 0) {
                return vj4.u(this.rawType);
            }
            StringBuilder sb = new StringBuilder((length + 1) * 30);
            sb.append(vj4.u(this.rawType));
            sb.append(SimpleComparison.LESS_THAN_OPERATION);
            sb.append(vj4.u(this.typeArguments[0]));
            for (int i = 1; i < length; i++) {
                sb.append(", ");
                sb.append(vj4.u(this.typeArguments[i]));
            }
            sb.append(SimpleComparison.GREATER_THAN_OPERATION);
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WildcardType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Type lowerBound;
        @DexIgnore
        public /* final */ Type upperBound;

        @DexIgnore
        public c(Type[] typeArr, Type[] typeArr2) {
            boolean z = true;
            uj4.a(typeArr2.length <= 1);
            uj4.a(typeArr.length == 1);
            if (typeArr2.length == 1) {
                uj4.b(typeArr2[0]);
                vj4.c(typeArr2[0]);
                uj4.a(typeArr[0] != Object.class ? false : z);
                this.lowerBound = vj4.b(typeArr2[0]);
                this.upperBound = Object.class;
                return;
            }
            uj4.b(typeArr[0]);
            vj4.c(typeArr[0]);
            this.lowerBound = null;
            this.upperBound = vj4.b(typeArr[0]);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && vj4.f(this, (WildcardType) obj);
        }

        @DexIgnore
        public Type[] getLowerBounds() {
            Type type = this.lowerBound;
            if (type == null) {
                return vj4.f3775a;
            }
            return new Type[]{type};
        }

        @DexIgnore
        public Type[] getUpperBounds() {
            return new Type[]{this.upperBound};
        }

        @DexIgnore
        public int hashCode() {
            Type type = this.lowerBound;
            return (type != null ? type.hashCode() + 31 : 1) ^ (this.upperBound.hashCode() + 31);
        }

        @DexIgnore
        public String toString() {
            if (this.lowerBound != null) {
                return "? super " + vj4.u(this.lowerBound);
            } else if (this.upperBound == Object.class) {
                return "?";
            } else {
                return "? extends " + vj4.u(this.upperBound);
            }
        }
    }

    @DexIgnore
    public static GenericArrayType a(Type type) {
        return new a(type);
    }

    @DexIgnore
    public static Type b(Type type) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            return cls.isArray() ? new a(b(cls.getComponentType())) : cls;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new b(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new a(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new c(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    @DexIgnore
    public static void c(Type type) {
        uj4.a(!(type instanceof Class) || !((Class) type).isPrimitive());
    }

    @DexIgnore
    public static Class<?> d(TypeVariable<?> typeVariable) {
        GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    @DexIgnore
    public static boolean e(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public static boolean f(Type type, Type type2) {
        boolean z = true;
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            if (!e(parameterizedType.getOwnerType(), parameterizedType2.getOwnerType()) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                z = false;
            }
            return z;
        } else if (type instanceof GenericArrayType) {
            if (type2 instanceof GenericArrayType) {
                return f(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
            }
            return false;
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                z = false;
            }
            return z;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                z = false;
            }
            return z;
        }
    }

    @DexIgnore
    public static Type g(Type type) {
        return type instanceof GenericArrayType ? ((GenericArrayType) type).getGenericComponentType() : ((Class) type).getComponentType();
    }

    @DexIgnore
    public static Type h(Type type, Class<?> cls) {
        Type l = l(type, cls, Collection.class);
        if (l instanceof WildcardType) {
            l = ((WildcardType) l).getUpperBounds()[0];
        }
        return l instanceof ParameterizedType ? ((ParameterizedType) l).getActualTypeArguments()[0] : Object.class;
    }

    @DexIgnore
    public static Type i(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return i(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return i(cls.getGenericSuperclass(), superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    @DexIgnore
    public static Type[] j(Type type, Class<?> cls) {
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type l = l(type, cls, Map.class);
        if (l instanceof ParameterizedType) {
            return ((ParameterizedType) l).getActualTypeArguments();
        }
        return new Type[]{Object.class, Object.class};
    }

    @DexIgnore
    public static Class<?> k(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            uj4.a(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(k(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return k(((WildcardType) type).getUpperBounds()[0]);
            }
            String name = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + name);
        }
    }

    @DexIgnore
    public static Type l(Type type, Class<?> cls, Class<?> cls2) {
        if (type instanceof WildcardType) {
            type = ((WildcardType) type).getUpperBounds()[0];
        }
        uj4.a(cls2.isAssignableFrom(cls));
        return p(type, cls, i(type, cls, cls2));
    }

    @DexIgnore
    public static int m(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public static int n(Object[] objArr, Object obj) {
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public static ParameterizedType o(Type type, Type type2, Type... typeArr) {
        return new b(type, type2, typeArr);
    }

    @DexIgnore
    public static Type p(Type type, Class<?> cls, Type type2) {
        return q(type, cls, type2, new HashSet());
    }

    @DexIgnore
    public static Type q(Type type, Class<?> cls, Type type2, Collection<TypeVariable> collection) {
        boolean z;
        Type type3 = type2;
        while (type3 instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) type3;
            if (collection.contains(typeVariable)) {
                return type3;
            }
            collection.add(typeVariable);
            type3 = r(type, cls, typeVariable);
            if (type3 == typeVariable) {
                return type3;
            }
        }
        if (type3 instanceof Class) {
            Class cls2 = (Class) type3;
            if (cls2.isArray()) {
                Class<?> componentType = cls2.getComponentType();
                Type q = q(type, cls, componentType, collection);
                Type type4 = cls2;
                if (componentType != q) {
                    type4 = a(q);
                }
                return type4;
            }
        }
        if (type3 instanceof GenericArrayType) {
            GenericArrayType genericArrayType = (GenericArrayType) type3;
            Type genericComponentType = genericArrayType.getGenericComponentType();
            Type q2 = q(type, cls, genericComponentType, collection);
            return genericComponentType != q2 ? a(q2) : genericArrayType;
        } else if (type3 instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type3;
            Type ownerType = parameterizedType.getOwnerType();
            Type q3 = q(type, cls, ownerType, collection);
            boolean z2 = q3 != ownerType;
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            int length = actualTypeArguments.length;
            int i = 0;
            while (i < length) {
                Type q4 = q(type, cls, actualTypeArguments[i], collection);
                if (q4 != actualTypeArguments[i]) {
                    if (!z2) {
                        z = true;
                        actualTypeArguments = (Type[]) actualTypeArguments.clone();
                    } else {
                        z = z2;
                    }
                    actualTypeArguments[i] = q4;
                } else {
                    z = z2;
                }
                i++;
                z2 = z;
            }
            return z2 ? o(q3, parameterizedType.getRawType(), actualTypeArguments) : parameterizedType;
        } else if (!(type3 instanceof WildcardType)) {
            return type3;
        } else {
            WildcardType wildcardType = (WildcardType) type3;
            Type[] lowerBounds = wildcardType.getLowerBounds();
            Type[] upperBounds = wildcardType.getUpperBounds();
            if (lowerBounds.length == 1) {
                Type q5 = q(type, cls, lowerBounds[0], collection);
                return q5 != lowerBounds[0] ? t(q5) : wildcardType;
            } else if (upperBounds.length != 1) {
                return wildcardType;
            } else {
                Type q6 = q(type, cls, upperBounds[0], collection);
                return q6 != upperBounds[0] ? s(q6) : wildcardType;
            }
        }
    }

    @DexIgnore
    public static Type r(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> d = d(typeVariable);
        if (d == null) {
            return typeVariable;
        }
        Type i = i(type, cls, d);
        if (!(i instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) i).getActualTypeArguments()[n(d.getTypeParameters(), typeVariable)];
    }

    @DexIgnore
    public static WildcardType s(Type type) {
        return new c(type instanceof WildcardType ? ((WildcardType) type).getUpperBounds() : new Type[]{type}, f3775a);
    }

    @DexIgnore
    public static WildcardType t(Type type) {
        return new c(new Type[]{Object.class}, type instanceof WildcardType ? ((WildcardType) type).getLowerBounds() : new Type[]{type});
    }

    @DexIgnore
    public static String u(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }
}
