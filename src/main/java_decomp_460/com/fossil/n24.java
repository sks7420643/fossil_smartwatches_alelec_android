package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n24<K, V> extends o24 implements Map<K, V> {
    @DexIgnore
    public void clear() {
        delegate().clear();
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return delegate().containsKey(obj);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return delegate().containsValue(obj);
    }

    @DexIgnore
    @Override // com.fossil.o24
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.o24
    public abstract Map<K, V> delegate();

    @DexIgnore
    @Override // java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        return delegate().entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || delegate().equals(obj);
    }

    @DexIgnore
    @Override // java.util.Map
    public V get(Object obj) {
        return delegate().get(obj);
    }

    @DexIgnore
    public int hashCode() {
        return delegate().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return delegate().isEmpty();
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<K> keySet() {
        return delegate().keySet();
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    public V put(K k, V v) {
        return delegate().put(k, v);
    }

    @DexIgnore
    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        delegate().putAll(map);
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    public V remove(Object obj) {
        return delegate().remove(obj);
    }

    @DexIgnore
    public int size() {
        return delegate().size();
    }

    @DexIgnore
    public void standardClear() {
        p34.e(entrySet().iterator());
    }

    @DexIgnore
    public boolean standardContainsKey(Object obj) {
        return x34.b(this, obj);
    }

    @DexIgnore
    public boolean standardContainsValue(Object obj) {
        return x34.c(this, obj);
    }

    @DexIgnore
    public boolean standardEquals(Object obj) {
        return x34.d(this, obj);
    }

    @DexIgnore
    public int standardHashCode() {
        return x44.b(entrySet());
    }

    @DexIgnore
    public boolean standardIsEmpty() {
        return !entrySet().iterator().hasNext();
    }

    @DexIgnore
    public void standardPutAll(Map<? extends K, ? extends V> map) {
        x34.l(this, map);
    }

    @DexIgnore
    public V standardRemove(Object obj) {
        Iterator<Map.Entry<K, V>> it = entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (f14.a(next.getKey(), obj)) {
                V value = next.getValue();
                it.remove();
                return value;
            }
        }
        return null;
    }

    @DexIgnore
    public String standardToString() {
        return x34.p(this);
    }

    @DexIgnore
    @Override // java.util.Map
    public Collection<V> values() {
        return delegate().values();
    }
}
