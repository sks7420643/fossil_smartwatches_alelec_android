package com.fossil;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AlertController;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ve0 extends ye0 implements DialogInterface {
    @DexIgnore
    public /* final */ AlertController d; // = new AlertController(getContext(), this, getWindow());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AlertController.f f3753a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Context context) {
            this(context, ve0.g(context, 0));
        }

        @DexIgnore
        public a(Context context, int i) {
            this.f3753a = new AlertController.f(new ContextThemeWrapper(context, ve0.g(context, i)));
            this.b = i;
        }

        @DexIgnore
        public ve0 a() {
            ve0 ve0 = new ve0(this.f3753a.f25a, this.b);
            this.f3753a.a(ve0.d);
            ve0.setCancelable(this.f3753a.r);
            if (this.f3753a.r) {
                ve0.setCanceledOnTouchOutside(true);
            }
            ve0.setOnCancelListener(this.f3753a.s);
            ve0.setOnDismissListener(this.f3753a.t);
            DialogInterface.OnKeyListener onKeyListener = this.f3753a.u;
            if (onKeyListener != null) {
                ve0.setOnKeyListener(onKeyListener);
            }
            return ve0;
        }

        @DexIgnore
        public Context b() {
            return this.f3753a.f25a;
        }

        @DexIgnore
        public a c(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f3753a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            return this;
        }

        @DexIgnore
        public a d(boolean z) {
            this.f3753a.r = z;
            return this;
        }

        @DexIgnore
        public a e(View view) {
            this.f3753a.g = view;
            return this;
        }

        @DexIgnore
        public a f(Drawable drawable) {
            this.f3753a.d = drawable;
            return this;
        }

        @DexIgnore
        public a g(CharSequence charSequence) {
            this.f3753a.h = charSequence;
            return this;
        }

        @DexIgnore
        public a h(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f3753a;
            fVar.l = charSequence;
            fVar.n = onClickListener;
            return this;
        }

        @DexIgnore
        public a i(int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f3753a;
            fVar.o = fVar.f25a.getText(i);
            this.f3753a.q = onClickListener;
            return this;
        }

        @DexIgnore
        public a j(DialogInterface.OnKeyListener onKeyListener) {
            this.f3753a.u = onKeyListener;
            return this;
        }

        @DexIgnore
        public a k(int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f3753a;
            fVar.i = fVar.f25a.getText(i);
            this.f3753a.k = onClickListener;
            return this;
        }

        @DexIgnore
        public a l(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f3753a;
            fVar.i = charSequence;
            fVar.k = onClickListener;
            return this;
        }

        @DexIgnore
        public a m(ListAdapter listAdapter, int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f3753a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            fVar.I = i;
            fVar.H = true;
            return this;
        }

        @DexIgnore
        public a n(int i) {
            AlertController.f fVar = this.f3753a;
            fVar.f = fVar.f25a.getText(i);
            return this;
        }

        @DexIgnore
        public a o(CharSequence charSequence) {
            this.f3753a.f = charSequence;
            return this;
        }

        @DexIgnore
        public a p(View view) {
            AlertController.f fVar = this.f3753a;
            fVar.z = view;
            fVar.y = 0;
            fVar.E = false;
            return this;
        }

        @DexIgnore
        public ve0 q() {
            ve0 a2 = a();
            a2.show();
            return a2;
        }
    }

    @DexIgnore
    public ve0(Context context, int i) {
        super(context, g(context, i));
    }

    @DexIgnore
    public static int g(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(le0.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public Button e(int i) {
        return this.d.c(i);
    }

    @DexIgnore
    public ListView f() {
        return this.d.e();
    }

    @DexIgnore
    @Override // com.fossil.ye0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d.f();
    }

    @DexIgnore
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.d.h(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    @DexIgnore
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.d.i(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    @DexIgnore
    @Override // com.fossil.ye0, android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.d.r(charSequence);
    }
}
