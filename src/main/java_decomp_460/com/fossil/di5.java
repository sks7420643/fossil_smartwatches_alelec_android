package com.fossil;

import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum di5 {
    CLEAR_DAY(WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY, "clear-day"),
    CLEAR_NIGHT(WeatherComplicationAppInfo.WeatherCondition.CLEAR_NIGHT, "clear-night"),
    RAIN(WeatherComplicationAppInfo.WeatherCondition.RAIN, "rain"),
    SNOW(WeatherComplicationAppInfo.WeatherCondition.SNOW, "snow"),
    SLEET(WeatherComplicationAppInfo.WeatherCondition.SLEET, "sleet"),
    WIND(WeatherComplicationAppInfo.WeatherCondition.WIND, "wind"),
    FOG(WeatherComplicationAppInfo.WeatherCondition.FOG, "fog"),
    CLOUDY(WeatherComplicationAppInfo.WeatherCondition.CLOUDY, "cloudy"),
    PARTLY_CLOUDY_DAY(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_DAY, "partly-cloudy-day"),
    PARTLY_CLOUDY_NIGHT(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_NIGHT, "partly-cloudy-night");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public /* final */ String mConditionDescription;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition mConditionValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final WeatherComplicationAppInfo.WeatherCondition a(String str) {
            di5 di5;
            WeatherComplicationAppInfo.WeatherCondition weatherCondition;
            di5[] values = di5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    di5 = null;
                    break;
                }
                di5 = values[i];
                if (pq7.a(di5.mConditionDescription, str)) {
                    break;
                }
                i++;
            }
            return (di5 == null || (weatherCondition = di5.mConditionValue) == null) ? WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY : weatherCondition;
        }
    }

    @DexIgnore
    public di5(WeatherComplicationAppInfo.WeatherCondition weatherCondition, String str) {
        this.mConditionValue = weatherCondition;
        this.mConditionDescription = str;
    }

    @DexIgnore
    public static final WeatherComplicationAppInfo.WeatherCondition getConditionValue(String str) {
        return Companion.a(str);
    }
}
