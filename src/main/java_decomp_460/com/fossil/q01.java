package com.fossil;

import android.net.Uri;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q01 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Set<a> f2901a; // = new HashSet();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Uri f2902a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(Uri uri, boolean z) {
            this.f2902a = uri;
            this.b = z;
        }

        @DexIgnore
        public Uri a() {
            return this.f2902a;
        }

        @DexIgnore
        public boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.b == aVar.b && this.f2902a.equals(aVar.f2902a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.f2902a.hashCode() * 31) + (this.b ? 1 : 0);
        }
    }

    @DexIgnore
    public void a(Uri uri, boolean z) {
        this.f2901a.add(new a(uri, z));
    }

    @DexIgnore
    public Set<a> b() {
        return this.f2901a;
    }

    @DexIgnore
    public int c() {
        return this.f2901a.size();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || q01.class != obj.getClass()) {
            return false;
        }
        return this.f2901a.equals(((q01) obj).f2901a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f2901a.hashCode();
    }
}
