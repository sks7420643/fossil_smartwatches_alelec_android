package com.fossil;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.zip.Adler32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t12 implements h22 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f3349a;
    @DexIgnore
    public /* final */ k22 b;
    @DexIgnore
    public /* final */ v12 c;

    @DexIgnore
    public t12(Context context, k22 k22, v12 v12) {
        this.f3349a = context;
        this.b = k22;
        this.c = v12;
    }

    @DexIgnore
    @Override // com.fossil.h22
    public void a(h02 h02, int i) {
        ComponentName componentName = new ComponentName(this.f3349a, JobInfoSchedulerService.class);
        JobScheduler jobScheduler = (JobScheduler) this.f3349a.getSystemService("jobscheduler");
        int b2 = b(h02);
        if (c(jobScheduler, b2, i)) {
            c12.a("JobInfoScheduler", "Upload for context %s is already scheduled. Returning...", h02);
            return;
        }
        long c0 = this.b.c0(h02);
        v12 v12 = this.c;
        JobInfo.Builder builder = new JobInfo.Builder(b2, componentName);
        v12.b(builder, h02.d(), c0, i);
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putInt("attemptNumber", i);
        persistableBundle.putString("backendName", h02.b());
        persistableBundle.putInt("priority", z32.a(h02.d()));
        if (h02.c() != null) {
            persistableBundle.putString(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(h02.c(), 0));
        }
        builder.setExtras(persistableBundle);
        c12.b("JobInfoScheduler", "Scheduling upload for context %s with jobId=%d in %dms(Backend next call timestamp %d). Attempt %d", h02, Integer.valueOf(b2), Long.valueOf(this.c.f(h02.d(), c0, i)), Long.valueOf(c0), Integer.valueOf(i));
        jobScheduler.schedule(builder.build());
    }

    @DexIgnore
    public int b(h02 h02) {
        Adler32 adler32 = new Adler32();
        adler32.update(this.f3349a.getPackageName().getBytes(Charset.forName("UTF-8")));
        adler32.update(h02.b().getBytes(Charset.forName("UTF-8")));
        adler32.update(ByteBuffer.allocate(4).putInt(z32.a(h02.d())).array());
        if (h02.c() != null) {
            adler32.update(h02.c());
        }
        return (int) adler32.getValue();
    }

    @DexIgnore
    public final boolean c(JobScheduler jobScheduler, int i, int i2) {
        Iterator<JobInfo> it = jobScheduler.getAllPendingJobs().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            JobInfo next = it.next();
            int i3 = next.getExtras().getInt("attemptNumber");
            if (next.getId() == i) {
                if (i3 >= i2) {
                    return true;
                }
            }
        }
        return false;
    }
}
