package com.fossil;

import com.fossil.i22;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l22 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ l22 f2135a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract l22 a();

        @DexIgnore
        public abstract a b(int i);

        @DexIgnore
        public abstract a c(long j);

        @DexIgnore
        public abstract a d(int i);

        @DexIgnore
        public abstract a e(int i);

        @DexIgnore
        public abstract a f(long j);
    }

    /*
    static {
        a a2 = a();
        a2.f(10485760);
        a2.d(200);
        a2.b(10000);
        a2.c(604800000);
        a2.e(81920);
        f2135a = a2.a();
    }
    */

    @DexIgnore
    public static a a() {
        return new i22.b();
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public abstract long f();
}
