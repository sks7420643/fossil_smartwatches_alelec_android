package com.fossil.wearables.fossil.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.df7;
import com.fossil.ef7;
import com.fossil.q47;
import com.fossil.tf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WXEntryActivity extends Activity implements tf7 {
    @DexIgnore
    @Override // com.fossil.tf7
    public void a(df7 df7) {
        q47.i().a(df7);
        finish();
    }

    @DexIgnore
    @Override // com.fossil.tf7
    public void b(ef7 ef7) {
        q47.i().b(ef7);
        finish();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        q47.i().c(getIntent(), this);
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        q47.i().c(getIntent(), this);
    }
}
