package com.fossil.wearables.fsl.fitness;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum FitnessSourceType {
    Device("Device"),
    User("User"),
    Mock("Mock");
    
    @DexIgnore
    public /* final */ String name;

    @DexIgnore
    public FitnessSourceType(String str) {
        this.name = str;
    }

    @DexIgnore
    public static FitnessSourceType find(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (int i = 0; i < values().length; i++) {
                if (values()[i].getName().equalsIgnoreCase(str)) {
                    return values()[i];
                }
            }
        }
        return null;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
