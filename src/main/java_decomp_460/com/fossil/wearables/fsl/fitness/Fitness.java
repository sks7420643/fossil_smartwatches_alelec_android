package com.fossil.wearables.fsl.fitness;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fitness {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Values {
        @DexIgnore
        public double calories; // = 0.0d;
        @DexIgnore
        public double distance; // = 0.0d;
        @DexIgnore
        public double steps; // = 0.0d;
    }

    @DexIgnore
    public static final Values addSamples(List<SampleDay> list) {
        Values values = new Values();
        for (SampleDay sampleDay : list) {
            values.steps += sampleDay.getSteps();
            values.calories += sampleDay.getCalories();
            values.distance += sampleDay.getDistance();
        }
        return values;
    }
}
