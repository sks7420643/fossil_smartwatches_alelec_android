package com.fossil.wearables.fsl.fitness;

import android.util.Log;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.chrono.GregorianChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Interpolator {
    @DexIgnore
    public static /* final */ String TAG; // = "Interpolator";

    @DexIgnore
    public static int calculateIntensityLevel(long j) {
        if (j < 70) {
            return 0;
        }
        return j < 140 ? 1 : 2;
    }

    @DexIgnore
    public static SampleDay createSample(DateTime dateTime, double d, double d2, double d3, List<Integer> list) {
        TimeZone timeZone = dateTime.getZone().toTimeZone();
        return new SampleDay(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), timeZone.getID(), timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0, d, d2, d3, list);
    }

    @DexIgnore
    public static DateTime getCursorEndDateTime(DateTime dateTime) {
        return new DateTime(dateTime, dateTime.getChronology()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
    }

    @DexIgnore
    public static DateTime getCursorEndHourTime(DateTime dateTime) {
        return new DateTime(dateTime, dateTime.getChronology()).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
    }

    @DexIgnore
    public static List<SampleDay> interpolateDays(Date date, Date date2, TimeZone timeZone, double d, double d2, double d3) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        GregorianChronology instance = GregorianChronology.getInstance(DateTimeZone.forTimeZone(timeZone));
        DateTime dateTime = new DateTime(date, instance);
        DateTime dateTime2 = new DateTime(date2, instance);
        ArrayList arrayList = new ArrayList();
        MathContext mathContext = new MathContext(4, RoundingMode.HALF_UP);
        if (dateTime.isBefore(dateTime2)) {
            double millis = (double) new Duration(dateTime, dateTime2).getMillis();
            BigDecimal bigDecimal = new BigDecimal(d / millis);
            BigDecimal bigDecimal2 = new BigDecimal(d2 / millis);
            BigDecimal bigDecimal3 = new BigDecimal(d3 / millis);
            DateTime dateTime3 = dateTime;
            while (dateTime2.isAfter(dateTime3)) {
                DateTime cursorEndDateTime = getCursorEndDateTime(dateTime3);
                if (cursorEndDateTime.isAfter(dateTime2)) {
                    cursorEndDateTime = dateTime2;
                }
                BigDecimal bigDecimal4 = new BigDecimal((double) new Period(dateTime3, cursorEndDateTime, PeriodType.millis()).getMillis());
                double doubleValue = bigDecimal.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue();
                double doubleValue2 = bigDecimal2.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue();
                double doubleValue3 = bigDecimal3.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue();
                int calculateIntensityLevel = calculateIntensityLevel(((long) doubleValue) / new Duration(dateTime, dateTime2).getStandardMinutes());
                if (calculateIntensityLevel == 0) {
                    i = (int) (((double) 0) + doubleValue);
                    i2 = 0;
                } else if (calculateIntensityLevel != 2) {
                    i2 = (int) (((double) 0) + doubleValue);
                    i = 0;
                } else {
                    i3 = 0;
                    i5 = (int) (((double) 0) + doubleValue);
                    i4 = 0;
                    arrayList.add(createSample(dateTime3, doubleValue, doubleValue2, doubleValue3, Arrays.asList(Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5))));
                    dateTime3 = cursorEndDateTime.plusMillis(1);
                }
                i5 = 0;
                i4 = i2;
                i3 = i;
                arrayList.add(createSample(dateTime3, doubleValue, doubleValue2, doubleValue3, Arrays.asList(Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5))));
                dateTime3 = cursorEndDateTime.plusMillis(1);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<SampleRaw> interpolateSampleHalfHour(Date date, Date date2, TimeZone timeZone, String str, FitnessSourceType fitnessSourceType, FitnessMovementType fitnessMovementType, double d, double d2, double d3) {
        ArrayList arrayList = new ArrayList();
        GregorianChronology instance = GregorianChronology.getInstance(DateTimeZone.forTimeZone(timeZone));
        DateTime dateTime = new DateTime(date, instance);
        DateTime dateTime2 = new DateTime(date2, instance);
        MathContext mathContext = new MathContext(4, RoundingMode.HALF_UP);
        if (dateTime.isBefore(dateTime2)) {
            double millis = (double) new Duration(dateTime, dateTime2).getMillis();
            BigDecimal bigDecimal = new BigDecimal(d / millis);
            BigDecimal bigDecimal2 = new BigDecimal(d2 / millis);
            BigDecimal bigDecimal3 = new BigDecimal(d3 / millis);
            DateTime dateTime3 = new DateTime(dateTime, dateTime.getChronology());
            while (dateTime2.isAfter(dateTime3)) {
                DateTime dateTime4 = new DateTime(dateTime3, dateTime3.getChronology());
                DateTime withMillisOfSecond = dateTime3.getMinuteOfHour() < 30 ? dateTime4.withMinuteOfHour(29).withSecondOfMinute(59).withMillisOfSecond(999) : dateTime4.withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
                if (withMillisOfSecond.isAfter(dateTime2)) {
                    withMillisOfSecond = dateTime2;
                }
                BigDecimal bigDecimal4 = new BigDecimal((double) new Period(dateTime3, withMillisOfSecond, PeriodType.millis()).getMillis());
                try {
                    arrayList.add(new SampleRaw(dateTime3.toDate(), withMillisOfSecond.toDate(), timeZone.getID(), str, fitnessSourceType, fitnessMovementType, bigDecimal.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue(), bigDecimal2.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue(), bigDecimal3.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue()));
                } catch (Exception e) {
                    String str2 = TAG;
                    Log.e(str2, "Error inside " + TAG + ".interpolateHours - e=" + e);
                }
                dateTime3 = withMillisOfSecond.plusMillis(1);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static DateTime resetHour(DateTime dateTime) {
        return new DateTime(dateTime, dateTime.getChronology()).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }
}
