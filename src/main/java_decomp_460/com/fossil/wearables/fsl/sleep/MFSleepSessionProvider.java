package com.fossil.wearables.fsl.sleep;

import com.fossil.wearables.fsl.BaseProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface MFSleepSessionProvider extends BaseProvider {
    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    /* synthetic */ String getDbPath();

    @DexIgnore
    void updateSleepSessionPinType(MFSleepSession mFSleepSession, int i);
}
