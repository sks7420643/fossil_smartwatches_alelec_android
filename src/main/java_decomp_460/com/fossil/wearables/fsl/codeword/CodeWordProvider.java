package com.fossil.wearables.fsl.codeword;

import com.fossil.wearables.fsl.BaseProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface CodeWordProvider extends BaseProvider {
}
