package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xz4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f4218a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new a().getType();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<List<? extends GFitWOCalorie>> {
    }

    @DexIgnore
    public final List<GFitWOCalorie> a(String str) {
        pq7.c(str, "data");
        if (str.length() == 0) {
            return hm7.e();
        }
        try {
            Object l = this.f4218a.l(str, this.b);
            pq7.b(l, "mGson.fromJson(data, mType)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListGFitWOCalorie: ");
            e.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("GFitWOCaloriesConverter", sb.toString());
            return hm7.e();
        }
    }

    @DexIgnore
    public final String b(List<GFitWOCalorie> list) {
        pq7.c(list, "calories");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String u = this.f4218a.u(list, this.b);
            pq7.b(u, "mGson.toJson(calories, mType)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("GFitWOCaloriesConverter", sb.toString());
            return "";
        }
    }
}
