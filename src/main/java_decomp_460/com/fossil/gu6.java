package com.fossil;

import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu6 implements Factory<fu6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<WorkoutSettingRepository> f1369a;

    @DexIgnore
    public gu6(Provider<WorkoutSettingRepository> provider) {
        this.f1369a = provider;
    }

    @DexIgnore
    public static gu6 a(Provider<WorkoutSettingRepository> provider) {
        return new gu6(provider);
    }

    @DexIgnore
    public static fu6 c(WorkoutSettingRepository workoutSettingRepository) {
        return new fu6(workoutSettingRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public fu6 get() {
        return c(this.f1369a.get());
    }
}
