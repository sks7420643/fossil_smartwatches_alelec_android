package com.fossil;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fj2 extends Service {
    @DexIgnore
    public a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends dp2 {
        @DexIgnore
        public /* final */ fj2 b;

        @DexIgnore
        public a(fj2 fj2) {
            this.b = fj2;
        }

        @DexIgnore
        @Override // com.fossil.bp2
        public final void R1(zo2 zo2, mo2 mo2) throws RemoteException {
            this.b.d();
            if (this.b.c(zo2.c())) {
                mo2.m0(Status.f);
            } else {
                mo2.m0(new Status(13));
            }
        }

        @DexIgnore
        @Override // com.fossil.bp2
        public final void h1(gj2 gj2, mo2 mo2) throws RemoteException {
            this.b.d();
            if (this.b.b(gj2)) {
                mo2.m0(Status.f);
            } else {
                mo2.m0(new Status(13));
            }
        }

        @DexIgnore
        @Override // com.fossil.bp2
        public final void j0(yo2 yo2, vn2 vn2) throws RemoteException {
            this.b.d();
            vn2.V(new dj2(this.b.a(yo2.c()), Status.f));
        }
    }

    @DexIgnore
    public abstract List<uh2> a(List<DataType> list);

    @DexIgnore
    public abstract boolean b(gj2 gj2);

    @DexIgnore
    public abstract boolean c(uh2 uh2);

    @DexIgnore
    @TargetApi(19)
    public final void d() throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (mf2.f()) {
            ((AppOpsManager) getSystemService("appops")).checkPackage(callingUid, "com.google.android.gms");
            return;
        }
        String[] packagesForUid = getPackageManager().getPackagesForUid(callingUid);
        if (packagesForUid != null) {
            for (String str : packagesForUid) {
                if (str.equals("com.google.android.gms")) {
                    return;
                }
            }
        }
        throw new SecurityException("Unauthorized caller");
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!"com.google.android.gms.fitness.service.FitnessSensorService".equals(intent.getAction())) {
            return null;
        }
        if (Log.isLoggable("FitnessSensorService", 3)) {
            String valueOf = String.valueOf(intent);
            String name = fj2.class.getName();
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20 + String.valueOf(name).length());
            sb.append("Intent ");
            sb.append(valueOf);
            sb.append(" received by ");
            sb.append(name);
            Log.d("FitnessSensorService", sb.toString());
        }
        a aVar = this.b;
        aVar.asBinder();
        return aVar;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.b = new a();
    }
}
