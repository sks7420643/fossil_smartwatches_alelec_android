package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez1 extends kz1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<nz1> f1008a;

    @DexIgnore
    public ez1(List<nz1> list) {
        if (list != null) {
            this.f1008a = list;
            return;
        }
        throw new NullPointerException("Null logRequests");
    }

    @DexIgnore
    @Override // com.fossil.kz1
    public List<nz1> b() {
        return this.f1008a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof kz1) {
            return this.f1008a.equals(((kz1) obj).b());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f1008a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "BatchedLogRequest{logRequests=" + this.f1008a + "}";
    }
}
