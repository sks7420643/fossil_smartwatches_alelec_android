package com.fossil;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz1 implements wd4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ wd4 f695a; // = new cz1();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements sd4<bz1> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a f696a; // = new a();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.qd4
        public void a(Object obj, td4 td4) throws IOException {
            bz1 bz1 = (bz1) obj;
            td4 td42 = td4;
            td42.f("sdkVersion", bz1.i());
            td42.f(DeviceRequestsHelper.DEVICE_INFO_MODEL, bz1.f());
            td42.f("hardware", bz1.d());
            td42.f("device", bz1.b());
            td42.f("product", bz1.h());
            td42.f("osBuild", bz1.g());
            td42.f("manufacturer", bz1.e());
            td42.f("fingerprint", bz1.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements sd4<kz1> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ b f697a; // = new b();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.qd4
        public void a(Object obj, td4 td4) throws IOException {
            td4.f("logRequest", ((kz1) obj).b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sd4<lz1> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ c f698a; // = new c();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.qd4
        public void a(Object obj, td4 td4) throws IOException {
            lz1 lz1 = (lz1) obj;
            td4 td42 = td4;
            td42.f("clientType", lz1.c());
            td42.f("androidClientInfo", lz1.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements sd4<mz1> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ d f699a; // = new d();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.qd4
        public void a(Object obj, td4 td4) throws IOException {
            mz1 mz1 = (mz1) obj;
            td4 td42 = td4;
            td42.b("eventTimeMs", mz1.d());
            td42.f("eventCode", mz1.c());
            td42.b("eventUptimeMs", mz1.e());
            td42.f("sourceExtension", mz1.g());
            td42.f("sourceExtensionJsonProto3", mz1.h());
            td42.b("timezoneOffsetSeconds", mz1.i());
            td42.f("networkConnectionInfo", mz1.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements sd4<nz1> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ e f700a; // = new e();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.qd4
        public void a(Object obj, td4 td4) throws IOException {
            nz1 nz1 = (nz1) obj;
            td4 td42 = td4;
            td42.b("requestTimeMs", nz1.g());
            td42.b("requestUptimeMs", nz1.h());
            td42.f("clientInfo", nz1.b());
            td42.f("logSource", nz1.d());
            td42.f("logSourceName", nz1.e());
            td42.f("logEvent", nz1.c());
            td42.f("qosTier", nz1.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements sd4<pz1> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ f f701a; // = new f();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.qd4
        public void a(Object obj, td4 td4) throws IOException {
            pz1 pz1 = (pz1) obj;
            td4 td42 = td4;
            td42.f("networkType", pz1.c());
            td42.f("mobileSubtype", pz1.b());
        }
    }

    @DexIgnore
    @Override // com.fossil.wd4
    public void a(xd4<?> xd4) {
        xd4.a(kz1.class, b.f697a);
        xd4.a(ez1.class, b.f697a);
        xd4.a(nz1.class, e.f700a);
        xd4.a(hz1.class, e.f700a);
        xd4.a(lz1.class, c.f698a);
        xd4.a(fz1.class, c.f698a);
        xd4.a(bz1.class, a.f696a);
        xd4.a(dz1.class, a.f696a);
        xd4.a(mz1.class, d.f699a);
        xd4.a(gz1.class, d.f699a);
        xd4.a(pz1.class, f.f701a);
        xd4.a(jz1.class, f.f701a);
    }
}
