package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jw2 implements nv2 {
    @DexIgnore
    public static /* final */ Map<String, jw2> f; // = new zi0();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SharedPreferences f1820a;
    @DexIgnore
    public /* final */ SharedPreferences.OnSharedPreferenceChangeListener b; // = new iw2(this);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, ?> d;
    @DexIgnore
    public /* final */ List<ov2> e; // = new ArrayList();

    @DexIgnore
    public jw2(SharedPreferences sharedPreferences) {
        this.f1820a = sharedPreferences;
        sharedPreferences.registerOnSharedPreferenceChangeListener(this.b);
    }

    @DexIgnore
    public static jw2 a(Context context, String str) {
        jw2 jw2;
        if (!((!hv2.a() || str.startsWith("direct_boot:")) ? true : hv2.b(context))) {
            return null;
        }
        synchronized (jw2.class) {
            try {
                jw2 = f.get(str);
                if (jw2 == null) {
                    jw2 = new jw2(d(context, str));
                    f.put(str, jw2);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return jw2;
    }

    @DexIgnore
    public static void b() {
        synchronized (jw2.class) {
            try {
                for (jw2 jw2 : f.values()) {
                    jw2.f1820a.unregisterOnSharedPreferenceChangeListener(jw2.b);
                }
                f.clear();
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static SharedPreferences d(Context context, String str) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (str.startsWith("direct_boot:")) {
                if (hv2.a()) {
                    context = context.createDeviceProtectedStorageContext();
                }
                return context.getSharedPreferences(str.substring(12), 0);
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences(str, 0);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return sharedPreferences;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public final /* synthetic */ void c(SharedPreferences sharedPreferences, String str) {
        synchronized (this.c) {
            this.d = null;
            xv2.g();
        }
        synchronized (this) {
            for (ov2 ov2 : this.e) {
                ov2.zza();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nv2
    public final Object zza(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        map = this.f1820a.getAll();
                        this.d = map;
                    } finally {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                    }
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }
}
