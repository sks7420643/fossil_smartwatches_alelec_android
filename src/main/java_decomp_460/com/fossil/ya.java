package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya extends vx1<ym1[], HashMap<zm1, ym1>> {
    @DexIgnore
    public static /* final */ za[] b;
    @DexIgnore
    public static /* final */ za[] c;
    @DexIgnore
    public static /* final */ qx1<ym1[]>[] d; // = {new oa(), new ra()};
    @DexIgnore
    public static /* final */ rx1<HashMap<zm1, ym1>>[] e; // = {new ua(ob.DEVICE_CONFIG.c), new wa(ob.DEVICE_CONFIG.c)};
    @DexIgnore
    public static /* final */ ya f; // = new ya();

    /*
    static {
        za[] zaVarArr = {new za(zm1.BIOMETRIC_PROFILE, new b9(nm1.CREATOR)), new za(zm1.DAILY_STEP, new e9(vm1.CREATOR)), new za(zm1.DAILY_STEP_GOAL, new h9(wm1.CREATOR)), new za(zm1.DAILY_CALORIE, new k9(rm1.CREATOR)), new za(zm1.DAILY_CALORIE_GOAL, new n9(sm1.CREATOR)), new za(zm1.DAILY_TOTAL_ACTIVE_MINUTE, new q9(xm1.CREATOR)), new za(zm1.DAILY_ACTIVE_MINUTE_GOAL, new t9(qm1.CREATOR)), new za(zm1.DAILY_DISTANCE, new w9(tm1.CREATOR)), new za(zm1.INACTIVE_NUDGE, new z9(en1.CREATOR)), new za(zm1.VIBE_STRENGTH, new bb(hn1.CREATOR)), new za(zm1.DO_NOT_DISTURB_SCHEDULE, new db(bn1.CREATOR)), new za(zm1.TIME, new fb(gn1.CREATOR)), new za(zm1.BATTERY, new hb(mm1.CREATOR)), new za(zm1.HEART_RATE_MODE, new jb(cn1.CREATOR)), new za(zm1.DAILY_SLEEP, new lb(um1.CREATOR)), new za(zm1.DISPLAY_UNIT, new nb(an1.CREATOR)), new za(zm1.SECOND_TIMEZONE_OFFSET, new pb(fn1.CREATOR))};
        b = zaVarArr;
        c = (za[]) dm7.s(zaVarArr, new za[]{new za(zm1.CURRENT_HEART_RATE, new ca(om1.CREATOR)), new za(zm1.HELLAS_BATTERY, new fa(dn1.CREATOR)), new za(zm1.AUTO_WORKOUT_DETECTION, new ia(jn1.CREATOR)), new za(zm1.CYCLING_CADENCE, new la(pm1.CREATOR))});
    }
    */

    @DexIgnore
    @Override // com.fossil.vx1
    public qx1<ym1[]>[] b() {
        return d;
    }

    @DexIgnore
    @Override // com.fossil.vx1
    public rx1<HashMap<zm1, ym1>>[] c() {
        return e;
    }

    @DexIgnore
    public final HashMap<zm1, ym1> h(byte[] bArr, za[] zaVarArr) {
        ym1 ym1;
        za zaVar;
        int length = (bArr.length - 12) - 4;
        byte[] k = dm7.k(bArr, 12, length + 12);
        ByteBuffer order = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN);
        HashMap<zm1, ym1> hashMap = new HashMap<>();
        int i = 0;
        while (true) {
            int i2 = i + 3;
            if (i2 > length) {
                return hashMap;
            }
            short s = order.getShort(i);
            short p = hy1.p(order.get(i + 2));
            zm1 b2 = zm1.e.b(s);
            try {
                byte[] k2 = dm7.k(k, i2, i2 + p);
                int length2 = zaVarArr.length;
                int i3 = 0;
                while (true) {
                    ym1 = null;
                    if (i3 >= length2) {
                        zaVar = null;
                        break;
                    }
                    zaVar = zaVarArr[i3];
                    if (zaVar.f4442a == b2) {
                        break;
                    }
                    i3++;
                }
                if (zaVar != null) {
                    ym1 = zaVar.b.invoke(k2);
                }
                if (!(b2 == null || ym1 == null)) {
                    hashMap.put(b2, ym1);
                }
            } catch (Exception e2) {
                d90.i.i(e2);
            }
            i = p + 3 + i;
        }
    }

    @DexIgnore
    public final byte[] j(ym1[] ym1Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (ym1 ym1 : ym1Arr) {
            byteArrayOutputStream.write(ym1.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "entryData.toByteArray()");
        return byteArray;
    }
}
