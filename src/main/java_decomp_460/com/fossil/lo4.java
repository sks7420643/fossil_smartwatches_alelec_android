package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.fossil.r62;
import com.google.android.gms.location.LocationRequest;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lo4 implements r62.b, r62.c, ga3 {
    @DexIgnore
    public static /* final */ String i; // = "lo4";
    @DexIgnore
    public static lo4 j;
    @DexIgnore
    public r62 b;
    @DexIgnore
    public CopyOnWriteArrayList<b> c; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Context d;
    @DexIgnore
    public Timer e;
    @DexIgnore
    public TimerTask f;
    @DexIgnore
    public double g;
    @DexIgnore
    public double h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TimerTask {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            SecureRandom secureRandom = new SecureRandom();
            lo4.this.g += (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            lo4.this.h += (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            Location location = new Location("");
            location.setLatitude(lo4.this.g + 10.7604877d);
            location.setLongitude(lo4.this.h + 106.698541d);
            Iterator it = lo4.this.c.iterator();
            while (it.hasNext()) {
                ((b) it.next()).a(location, 1);
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Location location, int i);
    }

    @DexIgnore
    public static lo4 h(Context context) {
        lo4 lo4;
        synchronized (lo4.class) {
            try {
                if (j == null) {
                    j = new lo4();
                }
                j.d = context.getApplicationContext();
                lo4 = j;
            } catch (Throwable th) {
                throw th;
            }
        }
        return lo4;
    }

    @DexIgnore
    @Override // com.fossil.k72
    public void d(int i2) {
        String str = i;
        Log.i(str, "MFLocationService is suspended - i=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.k72
    public void e(Bundle bundle) {
        Log.i(i, "MFLocationService is connected");
        m();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0024 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int j() {
        /*
            r4 = this;
            r2 = 0
            android.content.Context r0 = r4.d
            int r0 = com.fossil.g62.g(r0)
            if (r0 == 0) goto L_0x000b
            r2 = -2
        L_0x000a:
            return r2
        L_0x000b:
            android.content.Context r0 = r4.d
            java.lang.String r1 = "location"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            java.lang.String r1 = "gps"
            boolean r1 = r0.isProviderEnabled(r1)     // Catch:{ Exception -> 0x0028 }
            java.lang.String r3 = "network"
            boolean r0 = r0.isProviderEnabled(r3)     // Catch:{ Exception -> 0x002d }
            r3 = r0
        L_0x0022:
            if (r1 != 0) goto L_0x000a
            if (r3 != 0) goto L_0x000a
            r2 = -1
            goto L_0x000a
        L_0x0028:
            r0 = move-exception
            r0 = r2
        L_0x002a:
            r3 = r2
            r1 = r0
            goto L_0x0022
        L_0x002d:
            r0 = move-exception
            r0 = r1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lo4.j():int");
    }

    @DexIgnore
    public void k(b bVar) {
        if (!ko4.a(this.d)) {
            bVar.a(null, -1);
            return;
        }
        String str = i;
        Log.i(str, "Register Location Service - callback=" + bVar + ", size=" + this.c.size());
        this.c.add(bVar);
        if (jo4.b(this.d, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            l();
            return;
        }
        if (this.b == null) {
            r62.a aVar = new r62.a(this.d);
            aVar.a(ha3.c);
            aVar.d(this);
            aVar.e(this);
            this.b = aVar.g();
        }
        int j2 = j();
        if (j2 != 0) {
            bVar.a(null, j2);
        } else {
            this.b.f();
        }
    }

    @DexIgnore
    public final void l() {
        o();
        this.e = new Timer();
        a aVar = new a();
        this.f = aVar;
        this.e.schedule(aVar, 0, 1000);
    }

    @DexIgnore
    public final void m() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.k(0);
        locationRequest.A(1000);
        locationRequest.F(100);
        ha3.d.b(this.b, locationRequest, this);
    }

    @DexIgnore
    @Override // com.fossil.r72
    public void n(z52 z52) {
        Log.e(i, "MFLocationService is failed to connect");
    }

    @DexIgnore
    public final void o() {
        Timer timer = this.e;
        if (timer != null) {
            timer.cancel();
        }
        TimerTask timerTask = this.f;
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.ga3
    public void onLocationChanged(Location location) {
        String str = i;
        Log.d(str, "Inside " + i + ".onLocationUpdated - location=" + location);
        CopyOnWriteArrayList<b> copyOnWriteArrayList = this.c;
        if (copyOnWriteArrayList != null) {
            Iterator<b> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                it.next().a(location, 1);
            }
        }
    }

    @DexIgnore
    public void p(b bVar) {
        this.c.remove(bVar);
        String str = i;
        Log.i(str, "Unregister Location Service - callback=" + bVar + ", size=" + this.c.size());
        if (jo4.b(this.d, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            o();
            return;
        }
        r62 r62 = this.b;
        if (r62 != null && r62.n()) {
            ha3.d.a(this.b, this);
            this.b.g();
        }
    }
}
