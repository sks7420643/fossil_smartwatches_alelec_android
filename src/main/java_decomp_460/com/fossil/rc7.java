package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rc7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final rc7 f3099a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements rc7 {
        @DexIgnore
        @Override // com.fossil.rc7
        public Map<Class<?>, Set<pc7>> a(Object obj) {
            return mc7.b(obj);
        }

        @DexIgnore
        @Override // com.fossil.rc7
        public Map<Class<?>, qc7> b(Object obj) {
            return mc7.a(obj);
        }
    }

    @DexIgnore
    Map<Class<?>, Set<pc7>> a(Object obj);

    @DexIgnore
    Map<Class<?>, qc7> b(Object obj);
}
