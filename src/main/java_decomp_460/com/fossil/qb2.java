package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qb2<T> extends v62, Closeable, Iterable<T> {
    @DexIgnore
    T get(int i);

    @DexIgnore
    int getCount();
}
