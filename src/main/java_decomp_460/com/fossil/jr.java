package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.logic.phase.InstallThemePackagePhase$onStart$2", f = "InstallThemePackagePhase.kt", l = {56}, m = "invokeSuspend")
public final class jr extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ bg e;
    @DexIgnore
    public /* final */ /* synthetic */ kw1 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jr(bg bgVar, kw1 kw1, qn7 qn7) {
        super(2, qn7);
        this.e = bgVar;
        this.f = kw1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        jr jrVar = new jr(this.e, this.f, qn7);
        jrVar.b = (iv7) obj;
        return jrVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        jr jrVar = new jr(this.e, this.f, qn7);
        jrVar.b = iv7;
        return jrVar.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object a2;
        Object d2 = yn7.d();
        int i = this.d;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.b;
            nc0 nc0 = nc0.f2498a;
            ec0 themeClassifier = this.e.C.getThemeClassifier();
            ry1 uiPackageOSVersion = this.e.x.a().getUiPackageOSVersion();
            this.c = iv7;
            this.d = 1;
            a2 = nc0.a(themeClassifier, uiPackageOSVersion, true, this);
            if (a2 == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.c;
            el7.b(obj);
            a2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        o oVar = (o) a2;
        V v = oVar.f2599a;
        if (v != null) {
            lw1 a3 = this.f.a(v);
            if (a3 != null) {
                bg bgVar = this.e;
                bgVar.C = a3;
                bg.G(bgVar);
            } else {
                bg bgVar2 = this.e;
                bgVar2.l(nr.a(bgVar2.v, null, zq.FLOW_BROKEN, null, null, 13));
            }
        } else {
            bg bgVar3 = this.e;
            nr nrVar = bgVar3.v;
            zq zqVar = zq.NETWORK_ERROR;
            E e2 = oVar.b;
            if (!(e2 instanceof ax1)) {
                e2 = null;
            }
            bgVar3.l(nr.a(nrVar, null, zqVar, null, (ax1) e2, 5));
        }
        return tl7.f3441a;
    }
}
