package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n22 implements Factory<String> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ n22 f2452a; // = new n22();

    @DexIgnore
    public static n22 a() {
        return f2452a;
    }

    @DexIgnore
    public static String b() {
        String a2 = m22.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    /* renamed from: c */
    public String get() {
        return b();
    }
}
