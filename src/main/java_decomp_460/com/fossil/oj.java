package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj extends bi {
    @DexIgnore
    public /* final */ ArrayList<ac0> V; // = new ArrayList<>();

    @DexIgnore
    public oj(k5 k5Var, i60 i60) {
        super(k5Var, i60, yp.x0, ke.b.a(k5Var.x, ob.UI_PACKAGE_FILE), false, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.TRUE), hl7.a(hu1.SKIP_LIST, Boolean.FALSE), hl7.a(hu1.ERASE_CACHE_FILE_BEFORE_GET, Boolean.TRUE)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public JSONObject E() {
        JSONObject E = super.E();
        jd0 jd0 = jd0.L4;
        Object[] array = this.V.toArray(new ac0[0]);
        if (array != null) {
            return g80.k(E, jd0, px1.a((ox1[]) array));
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.bi
    public void M(ArrayList<j0> arrayList) {
        byte[] bArr;
        j0 j0Var = (j0) pm7.H(arrayList);
        if (!(j0Var == null || (bArr = j0Var.f) == null)) {
            try {
                mm7.t(this.V, (Object[]) pa.d.f(bArr));
            } catch (sx1 e) {
                tl7 tl7 = tl7.f3441a;
            }
        }
        l(nr.a(this.v, null, zq.SUCCESS, null, null, 13));
    }

    @DexIgnore
    /* renamed from: a0 */
    public ac0[] x() {
        Object[] array = this.V.toArray(new ac0[0]);
        if (array != null) {
            return (ac0[]) array;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
