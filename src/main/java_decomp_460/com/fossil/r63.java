package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r63 implements xw2<u63> {
    @DexIgnore
    public static r63 c; // = new r63();
    @DexIgnore
    public /* final */ xw2<u63> b;

    @DexIgnore
    public r63() {
        this(ww2.b(new t63()));
    }

    @DexIgnore
    public r63(xw2<u63> xw2) {
        this.b = ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((u63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((u63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.xw2
    public final /* synthetic */ u63 zza() {
        return this.b.zza();
    }
}
