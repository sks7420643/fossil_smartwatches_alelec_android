package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rh4 implements ht3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ sh4 f3114a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public rh4(sh4 sh4, Intent intent) {
        this.f3114a = sh4;
        this.b = intent;
    }

    @DexIgnore
    @Override // com.fossil.ht3
    public final void onComplete(nt3 nt3) {
        this.f3114a.f(this.b, nt3);
    }
}
