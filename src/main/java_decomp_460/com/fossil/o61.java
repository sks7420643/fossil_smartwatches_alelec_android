package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface o61<T, V> {
    @DexIgnore
    boolean a(T t);

    @DexIgnore
    V b(T t, f81 f81);
}
