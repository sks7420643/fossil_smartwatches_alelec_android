package com.fossil;

import com.fossil.s32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class a22 implements s32.a {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ b22 f185a;
    @DexIgnore
    public /* final */ h02 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public a22(b22 b22, h02 h02, int i) {
        this.f185a = b22;
        this.b = h02;
        this.c = i;
    }

    @DexIgnore
    public static s32.a b(b22 b22, h02 h02, int i) {
        return new a22(b22, h02, i);
    }

    @DexIgnore
    @Override // com.fossil.s32.a
    public Object a() {
        return this.f185a.d.a(this.b, this.c + 1);
    }
}
