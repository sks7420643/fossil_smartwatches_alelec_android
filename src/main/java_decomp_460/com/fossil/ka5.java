package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ka5 extends ja5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362172, 1);
        E.put(2131362666, 2);
        E.put(2131362643, 3);
        E.put(2131362240, 4);
        E.put(2131362642, 5);
        E.put(2131362239, 6);
        E.put(2131363329, 7);
        E.put(2131363328, 8);
        E.put(2131362646, 9);
        E.put(2131362243, 10);
        E.put(2131363069, 11);
    }
    */

    @DexIgnore
    public ka5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public ka5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (FlexibleTextInputEditText) objArr[6], (FlexibleTextInputEditText) objArr[4], (FlexibleTextInputEditText) objArr[10], (FlexibleTextInputLayout) objArr[5], (FlexibleTextInputLayout) objArr[3], (FlexibleTextInputLayout) objArr[9], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (ProgressButton) objArr[11], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[7]);
        this.C = -1;
        this.y.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
