package com.fossil;

import android.util.Log;
import android.util.SparseArray;
import com.fossil.r62;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oa2 extends ra2 {
    @DexIgnore
    public /* final */ SparseArray<a> g; // = new SparseArray<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements r62.c {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ r62 c;
        @DexIgnore
        public /* final */ r62.c d;

        @DexIgnore
        public a(int i, r62 r62, r62.c cVar) {
            this.b = i;
            this.c = r62;
            this.d = cVar;
            r62.q(this);
        }

        @DexIgnore
        @Override // com.fossil.r72
        public final void n(z52 z52) {
            String valueOf = String.valueOf(z52);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            oa2.this.n(z52, this.b);
        }
    }

    @DexIgnore
    public oa2(o72 o72) {
        super(o72);
        this.b.b1("AutoManageHelper", this);
    }

    @DexIgnore
    public static oa2 q(n72 n72) {
        o72 d = LifecycleCallback.d(n72);
        oa2 oa2 = (oa2) d.S2("AutoManageHelper", oa2.class);
        return oa2 != null ? oa2 : new oa2(d);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.g.size(); i++) {
            a t = t(i);
            if (t != null) {
                printWriter.append((CharSequence) str).append("GoogleApiClient #").print(t.b);
                printWriter.println(":");
                t.c.h(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        boolean z = this.c;
        String valueOf = String.valueOf(this.g);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (this.d.get() == null) {
            for (int i = 0; i < this.g.size(); i++) {
                a t = t(i);
                if (t != null) {
                    t.c.f();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void k() {
        super.k();
        for (int i = 0; i < this.g.size(); i++) {
            a t = t(i);
            if (t != null) {
                t.c.g();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ra2
    public final void m(z52 z52, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.g.get(i);
        if (aVar != null) {
            r(i);
            r62.c cVar = aVar.d;
            if (cVar != null) {
                cVar.n(z52);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ra2
    public final void o() {
        for (int i = 0; i < this.g.size(); i++) {
            a t = t(i);
            if (t != null) {
                t.c.f();
            }
        }
    }

    @DexIgnore
    public final void r(int i) {
        a aVar = this.g.get(i);
        this.g.remove(i);
        if (aVar != null) {
            aVar.c.r(aVar);
            aVar.c.g();
        }
    }

    @DexIgnore
    public final void s(int i, r62 r62, r62.c cVar) {
        rc2.l(r62, "GoogleApiClient instance cannot be null");
        boolean z = this.g.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        rc2.o(z, sb.toString());
        qa2 qa2 = this.d.get();
        boolean z2 = this.c;
        String valueOf = String.valueOf(qa2);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.g.put(i, new a(i, r62, cVar));
        if (this.c && qa2 == null) {
            String valueOf2 = String.valueOf(r62);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            r62.f();
        }
    }

    @DexIgnore
    public final a t(int i) {
        if (this.g.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.g;
        return sparseArray.get(sparseArray.keyAt(i));
    }
}
