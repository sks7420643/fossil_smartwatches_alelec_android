package com.fossil;

import android.os.Bundle;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w64 implements so3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ zs2 f3889a;

    @DexIgnore
    public w64(zs2 zs2) {
        this.f3889a = zs2;
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final List<Bundle> a(String str, String str2) {
        return this.f3889a.B(str, str2);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final int b(String str) {
        return this.f3889a.M(str);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final void c(Bundle bundle) {
        this.f3889a.j(bundle);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final void d(String str) {
        this.f3889a.J(str);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final Map<String, Object> e(String str, String str2, boolean z) {
        return this.f3889a.g(str, str2, z);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final void f(String str, String str2, Bundle bundle) {
        this.f3889a.E(str, str2, bundle);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final void g(String str, String str2, Bundle bundle) {
        this.f3889a.s(str, str2, bundle);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final String zza() {
        return this.f3889a.S();
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final void zza(String str) {
        this.f3889a.D(str);
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final String zzb() {
        return this.f3889a.U();
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final String zzc() {
        return this.f3889a.N();
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final String zzd() {
        return this.f3889a.I();
    }

    @DexIgnore
    @Override // com.fossil.so3
    public final long zze() {
        return this.f3889a.P();
    }
}
