package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e66 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ c66 f885a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<j06> c;

    @DexIgnore
    public e66(c66 c66, int i, ArrayList<j06> arrayList) {
        pq7.c(c66, "mView");
        this.f885a = c66;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final ArrayList<j06> a() {
        ArrayList<j06> arrayList = this.c;
        return arrayList != null ? arrayList : new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final c66 c() {
        return this.f885a;
    }
}
