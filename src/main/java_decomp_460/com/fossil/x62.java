package com.fossil;

import android.app.Activity;
import android.content.IntentSender;
import android.util.Log;
import com.fossil.z62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x62<R extends z62> extends b72<R> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Activity f4044a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public x62(Activity activity, int i) {
        rc2.l(activity, "Activity must not be null");
        this.f4044a = activity;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.b72
    public final void b(Status status) {
        if (status.k()) {
            try {
                status.F(this.f4044a, this.b);
            } catch (IntentSender.SendIntentException e) {
                Log.e("ResolvingResultCallback", "Failed to start resolution", e);
                d(new Status(8));
            }
        } else {
            d(status);
        }
    }

    @DexIgnore
    public abstract void d(Status status);
}
