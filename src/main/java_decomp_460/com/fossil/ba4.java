package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba4 extends ta4.c {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ua4<ta4.c.b> f408a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.c.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public ua4<ta4.c.b> f409a;
        @DexIgnore
        public String b;

        @DexIgnore
        @Override // com.fossil.ta4.c.a
        public ta4.c a() {
            String str = "";
            if (this.f409a == null) {
                str = " files";
            }
            if (str.isEmpty()) {
                return new ba4(this.f409a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.c.a
        public ta4.c.a b(ua4<ta4.c.b> ua4) {
            if (ua4 != null) {
                this.f409a = ua4;
                return this;
            }
            throw new NullPointerException("Null files");
        }

        @DexIgnore
        @Override // com.fossil.ta4.c.a
        public ta4.c.a c(String str) {
            this.b = str;
            return this;
        }
    }

    @DexIgnore
    public ba4(ua4<ta4.c.b> ua4, String str) {
        this.f408a = ua4;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.ta4.c
    public ua4<ta4.c.b> b() {
        return this.f408a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.c
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.c)) {
            return false;
        }
        ta4.c cVar = (ta4.c) obj;
        if (this.f408a.equals(cVar.b())) {
            String str = this.b;
            if (str == null) {
                if (cVar.c() == null) {
                    return true;
                }
            } else if (str.equals(cVar.c())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f408a.hashCode();
        String str = this.b;
        return (str == null ? 0 : str.hashCode()) ^ ((hashCode ^ 1000003) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "FilesPayload{files=" + this.f408a + ", orgId=" + this.b + "}";
    }
}
