package com.fossil;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g28 implements Closeable, Flushable {
    @DexIgnore
    public static /* final */ Pattern A; // = Pattern.compile("[a-z0-9_-]{1,120}");
    @DexIgnore
    public /* final */ q38 b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ File f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public long h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public long j; // = 0;
    @DexIgnore
    public j48 k;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> l; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public long x; // = 0;
    @DexIgnore
    public /* final */ Executor y;
    @DexIgnore
    public /* final */ Runnable z; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            boolean z = true;
            synchronized (g28.this) {
                if (g28.this.t) {
                    z = false;
                }
                if (!z && !g28.this.u) {
                    try {
                        g28.this.M();
                    } catch (IOException e) {
                        g28.this.v = true;
                    }
                    try {
                        if (g28.this.o()) {
                            g28.this.F();
                            g28.this.m = 0;
                        }
                    } catch (IOException e2) {
                        g28.this.w = true;
                        g28.this.k = s48.c(s48.b());
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends h28 {
        @DexIgnore
        public b(a58 a58) {
            super(a58);
        }

        @DexIgnore
        @Override // com.fossil.h28
        public void a(IOException iOException) {
            g28.this.s = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ d f1251a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h28 {
            @DexIgnore
            public a(a58 a58) {
                super(a58);
            }

            @DexIgnore
            @Override // com.fossil.h28
            public void a(IOException iOException) {
                synchronized (g28.this) {
                    c.this.c();
                }
            }
        }

        @DexIgnore
        public c(d dVar) {
            this.f1251a = dVar;
            this.b = dVar.e ? null : new boolean[g28.this.i];
        }

        @DexIgnore
        public void a() throws IOException {
            synchronized (g28.this) {
                if (!this.c) {
                    if (this.f1251a.f == this) {
                        g28.this.b(this, false);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void b() throws IOException {
            synchronized (g28.this) {
                if (!this.c) {
                    if (this.f1251a.f == this) {
                        g28.this.b(this, true);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void c() {
            if (this.f1251a.f == this) {
                int i = 0;
                while (true) {
                    g28 g28 = g28.this;
                    if (i < g28.i) {
                        try {
                            g28.b.f(this.f1251a.d[i]);
                        } catch (IOException e) {
                        }
                        i++;
                    } else {
                        this.f1251a.f = null;
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public a58 d(int i) {
            a58 b2;
            synchronized (g28.this) {
                if (this.c) {
                    throw new IllegalStateException();
                } else if (this.f1251a.f != this) {
                    b2 = s48.b();
                } else {
                    if (!this.f1251a.e) {
                        this.b[i] = true;
                    }
                    try {
                        b2 = new a(g28.this.b.b(this.f1251a.d[i]));
                    } catch (FileNotFoundException e) {
                        b2 = s48.b();
                    }
                }
            }
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1252a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public /* final */ File[] c;
        @DexIgnore
        public /* final */ File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public d(String str) {
            this.f1252a = str;
            int i = g28.this.i;
            this.b = new long[i];
            this.c = new File[i];
            this.d = new File[i];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i2 = 0; i2 < g28.this.i; i2++) {
                sb.append(i2);
                this.c[i2] = new File(g28.this.c, sb.toString());
                sb.append(".tmp");
                this.d[i2] = new File(g28.this.c, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public void b(String[] strArr) throws IOException {
            if (strArr.length == g28.this.i) {
                for (int i = 0; i < strArr.length; i++) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                    } catch (NumberFormatException e2) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public e c() {
            int i = 0;
            if (Thread.holdsLock(g28.this)) {
                c58[] c58Arr = new c58[g28.this.i];
                long[] jArr = (long[]) this.b.clone();
                for (int i2 = 0; i2 < g28.this.i; i2++) {
                    try {
                        c58Arr[i2] = g28.this.b.a(this.c[i2]);
                    } catch (FileNotFoundException e2) {
                        while (i < g28.this.i && c58Arr[i] != null) {
                            b28.g(c58Arr[i]);
                            i++;
                        }
                        try {
                            g28.this.L(this);
                        } catch (IOException e3) {
                        }
                        return null;
                    }
                }
                return new e(this.f1252a, this.g, c58Arr, jArr);
            }
            throw new AssertionError();
        }

        @DexIgnore
        public void d(j48 j48) throws IOException {
            for (long j : this.b) {
                j48.v(32).k0(j);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Closeable {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ c58[] d;

        @DexIgnore
        public e(String str, long j, c58[] c58Arr, long[] jArr) {
            this.b = str;
            this.c = j;
            this.d = c58Arr;
        }

        @DexIgnore
        public c a() throws IOException {
            return g28.this.j(this.b, this.c);
        }

        @DexIgnore
        public c58 b(int i) {
            return this.d[i];
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            for (c58 c58 : this.d) {
                b28.g(c58);
            }
        }
    }

    @DexIgnore
    public g28(q38 q38, File file, int i2, int i3, long j2, Executor executor) {
        this.b = q38;
        this.c = file;
        this.g = i2;
        this.d = new File(file, "journal");
        this.e = new File(file, "journal.tmp");
        this.f = new File(file, "journal.bkp");
        this.i = i3;
        this.h = j2;
        this.y = executor;
    }

    @DexIgnore
    public static g28 c(q38 q38, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            return new g28(q38, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), b28.G("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public final j48 A() throws FileNotFoundException {
        return s48.c(new b(this.b.g(this.d)));
    }

    @DexIgnore
    public final void B() throws IOException {
        this.b.f(this.e);
        Iterator<d> it = this.l.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            if (next.f == null) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.j += next.b[i2];
                }
            } else {
                next.f = null;
                for (int i3 = 0; i3 < this.i; i3++) {
                    this.b.f(next.c[i3]);
                    this.b.f(next.d[i3]);
                }
                it.remove();
            }
        }
    }

    @DexIgnore
    public final void C() throws IOException {
        k48 d2 = s48.d(this.b.a(this.d));
        try {
            String U = d2.U();
            String U2 = d2.U();
            String U3 = d2.U();
            String U4 = d2.U();
            String U5 = d2.U();
            if (!"libcore.io.DiskLruCache".equals(U) || !"1".equals(U2) || !Integer.toString(this.g).equals(U3) || !Integer.toString(this.i).equals(U4) || !"".equals(U5)) {
                throw new IOException("unexpected journal header: [" + U + ", " + U2 + ", " + U4 + ", " + U5 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    D(d2.U());
                    i2++;
                } catch (EOFException e2) {
                    this.m = i2 - this.l.size();
                    if (!d2.u()) {
                        F();
                    } else {
                        this.k = A();
                    }
                    return;
                }
            }
        } finally {
            b28.g(d2);
        }
    }

    @DexIgnore
    public final void D(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                String substring = str.substring(i2);
                if (indexOf != 6 || !str.startsWith("REMOVE")) {
                    str2 = substring;
                } else {
                    this.l.remove(substring);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.l.get(str2);
            if (dVar == null) {
                dVar = new d(str2);
                this.l.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                dVar.e = true;
                dVar.f = null;
                dVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f = new c(dVar);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void F() throws IOException {
        synchronized (this) {
            if (this.k != null) {
                this.k.close();
            }
            j48 c2 = s48.c(this.b.b(this.e));
            try {
                c2.E("libcore.io.DiskLruCache").v(10);
                c2.E("1").v(10);
                c2.k0((long) this.g).v(10);
                c2.k0((long) this.i).v(10);
                c2.v(10);
                for (d dVar : this.l.values()) {
                    if (dVar.f != null) {
                        c2.E("DIRTY").v(32);
                        c2.E(dVar.f1252a);
                        c2.v(10);
                    } else {
                        c2.E("CLEAN").v(32);
                        c2.E(dVar.f1252a);
                        dVar.d(c2);
                        c2.v(10);
                    }
                }
                c2.close();
                if (this.b.d(this.d)) {
                    this.b.e(this.d, this.f);
                }
                this.b.e(this.e, this.d);
                this.b.f(this.f);
                this.k = A();
                this.s = false;
                this.w = false;
            } catch (Throwable th) {
                c2.close();
                throw th;
            }
        }
    }

    @DexIgnore
    public boolean G(String str) throws IOException {
        synchronized (this) {
            m();
            a();
            P(str);
            d dVar = this.l.get(str);
            if (dVar == null) {
                return false;
            }
            boolean L = L(dVar);
            if (L && this.j <= this.h) {
                this.v = false;
            }
            return L;
        }
    }

    @DexIgnore
    public boolean L(d dVar) throws IOException {
        c cVar = dVar.f;
        if (cVar != null) {
            cVar.c();
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.b.f(dVar.c[i2]);
            long j2 = this.j;
            long[] jArr = dVar.b;
            this.j = j2 - jArr[i2];
            jArr[i2] = 0;
        }
        this.m++;
        this.k.E("REMOVE").v(32).E(dVar.f1252a).v(10);
        this.l.remove(dVar.f1252a);
        if (!o()) {
            return true;
        }
        this.y.execute(this.z);
        return true;
    }

    @DexIgnore
    public void M() throws IOException {
        while (this.j > this.h) {
            L(this.l.values().iterator().next());
        }
        this.v = false;
    }

    @DexIgnore
    public final void P(String str) {
        if (!A.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            if (isClosed()) {
                throw new IllegalStateException("cache is closed");
            }
        }
    }

    @DexIgnore
    public void b(c cVar, boolean z2) throws IOException {
        synchronized (this) {
            d dVar = cVar.f1251a;
            if (dVar.f == cVar) {
                if (z2 && !dVar.e) {
                    for (int i2 = 0; i2 < this.i; i2++) {
                        if (!cVar.b[i2]) {
                            cVar.a();
                            throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                        } else if (!this.b.d(dVar.d[i2])) {
                            cVar.a();
                            return;
                        }
                    }
                }
                for (int i3 = 0; i3 < this.i; i3++) {
                    File file = dVar.d[i3];
                    if (!z2) {
                        this.b.f(file);
                    } else if (this.b.d(file)) {
                        File file2 = dVar.c[i3];
                        this.b.e(file, file2);
                        long j2 = dVar.b[i3];
                        long h2 = this.b.h(file2);
                        dVar.b[i3] = h2;
                        this.j = (this.j - j2) + h2;
                    }
                }
                this.m++;
                dVar.f = null;
                if (dVar.e || z2) {
                    dVar.e = true;
                    this.k.E("CLEAN").v(32);
                    this.k.E(dVar.f1252a);
                    dVar.d(this.k);
                    this.k.v(10);
                    if (z2) {
                        long j3 = this.x;
                        this.x = 1 + j3;
                        dVar.g = j3;
                    }
                } else {
                    this.l.remove(dVar.f1252a);
                    this.k.E("REMOVE").v(32);
                    this.k.E(dVar.f1252a);
                    this.k.v(10);
                }
                this.k.flush();
                if (this.j > this.h || o()) {
                    this.y.execute(this.z);
                }
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            if (!this.t || this.u) {
                this.u = true;
                return;
            }
            d[] dVarArr = (d[]) this.l.values().toArray(new d[this.l.size()]);
            for (d dVar : dVarArr) {
                if (dVar.f != null) {
                    dVar.f.a();
                }
            }
            M();
            this.k.close();
            this.k = null;
            this.u = true;
        }
    }

    @DexIgnore
    public void f() throws IOException {
        close();
        this.b.c(this.c);
    }

    @DexIgnore
    @Override // java.io.Flushable
    public void flush() throws IOException {
        synchronized (this) {
            if (this.t) {
                a();
                M();
                this.k.flush();
            }
        }
    }

    @DexIgnore
    public c h(String str) throws IOException {
        return j(str, -1);
    }

    @DexIgnore
    public boolean isClosed() {
        boolean z2;
        synchronized (this) {
            z2 = this.u;
        }
        return z2;
    }

    @DexIgnore
    public c j(String str, long j2) throws IOException {
        d dVar;
        synchronized (this) {
            m();
            a();
            P(str);
            d dVar2 = this.l.get(str);
            if (j2 != -1 && (dVar2 == null || dVar2.g != j2)) {
                return null;
            }
            if (dVar2 != null && dVar2.f != null) {
                return null;
            }
            if (this.v || this.w) {
                this.y.execute(this.z);
                return null;
            }
            this.k.E("DIRTY").v(32).E(str).v(10);
            this.k.flush();
            if (this.s) {
                return null;
            }
            if (dVar2 == null) {
                d dVar3 = new d(str);
                this.l.put(str, dVar3);
                dVar = dVar3;
            } else {
                dVar = dVar2;
            }
            c cVar = new c(dVar);
            dVar.f = cVar;
            return cVar;
        }
    }

    @DexIgnore
    public void k() throws IOException {
        synchronized (this) {
            m();
            for (d dVar : (d[]) this.l.values().toArray(new d[this.l.size()])) {
                L(dVar);
            }
            this.v = false;
        }
    }

    @DexIgnore
    public e l(String str) throws IOException {
        synchronized (this) {
            m();
            a();
            P(str);
            d dVar = this.l.get(str);
            if (dVar == null || !dVar.e) {
                return null;
            }
            e c2 = dVar.c();
            if (c2 == null) {
                return null;
            }
            this.m++;
            this.k.E("READ").v(32).E(str).v(10);
            if (o()) {
                this.y.execute(this.z);
            }
            return c2;
        }
    }

    @DexIgnore
    public void m() throws IOException {
        synchronized (this) {
            if (!this.t) {
                if (this.b.d(this.f)) {
                    if (this.b.d(this.d)) {
                        this.b.f(this.f);
                    } else {
                        this.b.e(this.f, this.d);
                    }
                }
                if (this.b.d(this.d)) {
                    try {
                        C();
                        B();
                        this.t = true;
                        return;
                    } catch (IOException e2) {
                        w38 j2 = w38.j();
                        j2.q(5, "DiskLruCache " + this.c + " is corrupt: " + e2.getMessage() + ", removing", e2);
                        f();
                        this.u = false;
                    } catch (Throwable th) {
                        this.u = false;
                        throw th;
                    }
                }
                F();
                this.t = true;
            }
        }
    }

    @DexIgnore
    public boolean o() {
        int i2 = this.m;
        return i2 >= 2000 && i2 >= this.l.size();
    }
}
