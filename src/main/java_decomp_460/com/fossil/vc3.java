package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.rg2;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc3 extends as2 implements yb3 {
    @DexIgnore
    public vc3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 B2(LatLng latLng, float f) throws RemoteException {
        Parcel d = d();
        es2.d(d, latLng);
        d.writeFloat(f);
        Parcel e = e(9, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 C2(float f, float f2) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        d.writeFloat(f2);
        Parcel e = e(3, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 J1(CameraPosition cameraPosition) throws RemoteException {
        Parcel d = d();
        es2.d(d, cameraPosition);
        Parcel e = e(7, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 Z0(float f, int i, int i2) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        d.writeInt(i);
        d.writeInt(i2);
        Parcel e = e(6, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 c2() throws RemoteException {
        Parcel e = e(2, d());
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 g0(LatLng latLng) throws RemoteException {
        Parcel d = d();
        es2.d(d, latLng);
        Parcel e = e(8, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 u(LatLngBounds latLngBounds, int i) throws RemoteException {
        Parcel d = d();
        es2.d(d, latLngBounds);
        d.writeInt(i);
        Parcel e = e(10, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 v2(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        Parcel e = e(4, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 x(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        Parcel e = e(5, d);
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.yb3
    public final rg2 z0() throws RemoteException {
        Parcel e = e(1, d());
        rg2 e2 = rg2.a.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
