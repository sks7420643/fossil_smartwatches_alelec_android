package com.fossil;

import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.ff4;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class te4 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<te4> CREATOR; // = new a();
    @DexIgnore
    public Messenger b;
    @DexIgnore
    public ff4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Parcelable.Creator<te4> {
        @DexIgnore
        /* renamed from: a */
        public te4 createFromParcel(Parcel parcel) {
            IBinder readStrongBinder = parcel.readStrongBinder();
            if (readStrongBinder != null) {
                return new te4(readStrongBinder);
            }
            return null;
        }

        @DexIgnore
        /* renamed from: b */
        public te4[] newArray(int i) {
            return new te4[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ClassLoader {
        @DexIgnore
        @Override // java.lang.ClassLoader
        public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            if (FirebaseInstanceId.u()) {
                Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
            }
            return te4.class;
        }
    }

    @DexIgnore
    public te4(IBinder iBinder) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.b = new Messenger(iBinder);
        } else {
            this.c = new ff4.a(iBinder);
        }
    }

    @DexIgnore
    public IBinder a() {
        Messenger messenger = this.b;
        return messenger != null ? messenger.getBinder() : this.c.asBinder();
    }

    @DexIgnore
    public void b(Message message) throws RemoteException {
        Messenger messenger = this.b;
        if (messenger != null) {
            messenger.send(message);
        } else {
            this.c.P(message);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a().equals(((te4) obj).a());
        } catch (ClassCastException e) {
            return false;
        }
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Messenger messenger = this.b;
        if (messenger != null) {
            parcel.writeStrongBinder(messenger.getBinder());
        } else {
            parcel.writeStrongBinder(this.c.asBinder());
        }
    }
}
