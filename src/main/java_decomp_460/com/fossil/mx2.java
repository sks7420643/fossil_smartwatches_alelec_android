package com.fossil;

import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx2 extends dx2<K, V> {
    @DexIgnore
    @NullableDecl
    public /* final */ K b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ hx2 d;

    @DexIgnore
    public mx2(hx2 hx2, int i) {
        this.d = hx2;
        this.b = (K) hx2.zzb[i];
        this.c = i;
    }

    @DexIgnore
    public final void a() {
        int i = this.c;
        if (i == -1 || i >= this.d.size() || !qw2.a(this.b, this.d.zzb[this.c])) {
            this.c = this.d.b(this.b);
        }
    }

    @DexIgnore
    @Override // com.fossil.dx2, java.util.Map.Entry
    @NullableDecl
    public final K getKey() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.dx2, java.util.Map.Entry
    @NullableDecl
    public final V getValue() {
        Map zzb = this.d.zzb();
        if (zzb != null) {
            return (V) zzb.get(this.b);
        }
        a();
        int i = this.c;
        if (i == -1) {
            return null;
        }
        return (V) this.d.zzc[i];
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        Map zzb = this.d.zzb();
        if (zzb != null) {
            return (V) zzb.put(this.b, v);
        }
        a();
        int i = this.c;
        if (i == -1) {
            this.d.put(this.b, v);
            return null;
        }
        Object[] objArr = this.d.zzc;
        V v2 = (V) objArr[i];
        objArr[i] = v;
        return v2;
    }
}
