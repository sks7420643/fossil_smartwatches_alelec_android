package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vc7<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Picasso f3749a;
    @DexIgnore
    public /* final */ pd7 b;
    @DexIgnore
    public /* final */ WeakReference<T> c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ Drawable h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ Object j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<M> extends WeakReference<M> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ vc7 f3750a;

        @DexIgnore
        public a(vc7 vc7, M m, ReferenceQueue<? super M> referenceQueue) {
            super(m, referenceQueue);
            this.f3750a = vc7;
        }
    }

    @DexIgnore
    public vc7(Picasso picasso, T t, pd7 pd7, int i2, int i3, int i4, Drawable drawable, String str, Object obj, boolean z) {
        this.f3749a = picasso;
        this.b = pd7;
        this.c = t == null ? null : new a(this, t, picasso.k);
        this.e = i2;
        this.f = i3;
        this.d = z;
        this.g = i4;
        this.h = drawable;
        this.i = str;
        this.j = obj == null ? this : obj;
    }

    @DexIgnore
    public void a() {
        this.l = true;
    }

    @DexIgnore
    public abstract void b(Bitmap bitmap, Picasso.LoadedFrom loadedFrom);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public String d() {
        return this.i;
    }

    @DexIgnore
    public int e() {
        return this.e;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public Picasso g() {
        return this.f3749a;
    }

    @DexIgnore
    public Picasso.e h() {
        return this.b.r;
    }

    @DexIgnore
    public pd7 i() {
        return this.b;
    }

    @DexIgnore
    public Object j() {
        return this.j;
    }

    @DexIgnore
    public T k() {
        WeakReference<T> weakReference = this.c;
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    @DexIgnore
    public boolean l() {
        return this.l;
    }

    @DexIgnore
    public boolean m() {
        return this.k;
    }
}
