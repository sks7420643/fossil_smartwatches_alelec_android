package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.internal.FacebookWebFallbackDialog;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l04 {
    @DexIgnore
    public static l04 e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Object f2127a; // = new Object();
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());
    @DexIgnore
    public c c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Handler.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            l04.this.d((c) message.obj);
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);

        @DexIgnore
        Object show();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ WeakReference<b> f2129a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c(int i, b bVar) {
            this.f2129a = new WeakReference<>(bVar);
            this.b = i;
        }

        @DexIgnore
        public boolean a(b bVar) {
            return bVar != null && this.f2129a.get() == bVar;
        }
    }

    @DexIgnore
    public static l04 c() {
        if (e == null) {
            e = new l04();
        }
        return e;
    }

    @DexIgnore
    public final boolean a(c cVar, int i) {
        b bVar = cVar.f2129a.get();
        if (bVar == null) {
            return false;
        }
        this.b.removeCallbacksAndMessages(cVar);
        bVar.a(i);
        return true;
    }

    @DexIgnore
    public void b(b bVar, int i) {
        synchronized (this.f2127a) {
            if (f(bVar)) {
                a(this.c, i);
            } else if (g(bVar)) {
                a(this.d, i);
            }
        }
    }

    @DexIgnore
    public void d(c cVar) {
        synchronized (this.f2127a) {
            if (this.c == cVar || this.d == cVar) {
                a(cVar, 2);
            }
        }
    }

    @DexIgnore
    public boolean e(b bVar) {
        boolean z;
        synchronized (this.f2127a) {
            z = f(bVar) || g(bVar);
        }
        return z;
    }

    @DexIgnore
    public final boolean f(b bVar) {
        c cVar = this.c;
        return cVar != null && cVar.a(bVar);
    }

    @DexIgnore
    public final boolean g(b bVar) {
        c cVar = this.d;
        return cVar != null && cVar.a(bVar);
    }

    @DexIgnore
    public void h(b bVar) {
        synchronized (this.f2127a) {
            if (f(bVar)) {
                this.c = null;
                if (this.d != null) {
                    n();
                }
            }
        }
    }

    @DexIgnore
    public void i(b bVar) {
        synchronized (this.f2127a) {
            if (f(bVar)) {
                l(this.c);
            }
        }
    }

    @DexIgnore
    public void j(b bVar) {
        synchronized (this.f2127a) {
            if (f(bVar) && !this.c.c) {
                this.c.c = true;
                this.b.removeCallbacksAndMessages(this.c);
            }
        }
    }

    @DexIgnore
    public void k(b bVar) {
        synchronized (this.f2127a) {
            if (f(bVar) && this.c.c) {
                this.c.c = false;
                l(this.c);
            }
        }
    }

    @DexIgnore
    public final void l(c cVar) {
        int i = cVar.b;
        if (i != -2) {
            if (i <= 0) {
                i = i == -1 ? FacebookWebFallbackDialog.OS_BACK_BUTTON_RESPONSE_TIMEOUT_MILLISECONDS : 2750;
            }
            this.b.removeCallbacksAndMessages(cVar);
            Handler handler = this.b;
            handler.sendMessageDelayed(Message.obtain(handler, 0, cVar), (long) i);
        }
    }

    @DexIgnore
    public void m(int i, b bVar) {
        synchronized (this.f2127a) {
            if (f(bVar)) {
                this.c.b = i;
                this.b.removeCallbacksAndMessages(this.c);
                l(this.c);
                return;
            }
            if (g(bVar)) {
                this.d.b = i;
            } else {
                this.d = new c(i, bVar);
            }
            if (this.c == null || !a(this.c, 4)) {
                this.c = null;
                n();
            }
        }
    }

    @DexIgnore
    public final void n() {
        c cVar = this.d;
        if (cVar != null) {
            this.c = cVar;
            this.d = null;
            b bVar = cVar.f2129a.get();
            if (bVar != null) {
                bVar.show();
            } else {
                this.c = null;
            }
        }
    }
}
