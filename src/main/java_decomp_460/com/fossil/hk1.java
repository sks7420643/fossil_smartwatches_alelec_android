package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hk1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Class<?> f1491a;
    @DexIgnore
    public Class<?> b;
    @DexIgnore
    public Class<?> c;

    @DexIgnore
    public hk1() {
    }

    @DexIgnore
    public hk1(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.f1491a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || hk1.class != obj.getClass()) {
            return false;
        }
        hk1 hk1 = (hk1) obj;
        if (!this.f1491a.equals(hk1.f1491a)) {
            return false;
        }
        if (!this.b.equals(hk1.b)) {
            return false;
        }
        return jk1.d(this.c, hk1.c);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.f1491a.hashCode();
        int hashCode2 = this.b.hashCode();
        Class<?> cls = this.c;
        return (cls != null ? cls.hashCode() : 0) + (((hashCode * 31) + hashCode2) * 31);
    }

    @DexIgnore
    public String toString() {
        return "MultiClassKey{first=" + this.f1491a + ", second=" + this.b + '}';
    }
}
