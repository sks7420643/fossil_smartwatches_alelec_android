package com.fossil;

import android.view.ViewGroup;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uy0 {
    @DexIgnore
    public abstract void a(wy0 wy0);

    @DexIgnore
    public abstract String[] b();

    @DexIgnore
    public abstract long c(ViewGroup viewGroup, Transition transition, wy0 wy0, wy0 wy02);
}
