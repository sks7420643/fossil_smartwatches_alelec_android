package com.fossil;

import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jc4 extends k84 implements hc4 {
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public jc4(String str, String str2, kb4 kb4, String str3) {
        super(str, str2, kb4, ib4.POST);
        this.f = str3;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public boolean b(cc4 cc4, boolean z) {
        if (z) {
            jb4 c = c();
            g(c, cc4.b);
            h(c, cc4.f597a, cc4.c);
            x74 f2 = x74.f();
            f2.b("Sending report to: " + e());
            try {
                int b = c.b().b();
                x74 f3 = x74.f();
                f3.b("Result was: " + b);
                return n94.a(b) == 0;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public final jb4 g(jb4 jb4, String str) {
        jb4.d("User-Agent", "Crashlytics Android SDK/" + w84.i());
        jb4.d("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        jb4.d("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
        jb4.d("X-CRASHLYTICS-GOOGLE-APP-ID", str);
        return jb4;
    }

    @DexIgnore
    public final jb4 h(jb4 jb4, String str, ec4 ec4) {
        if (str != null) {
            jb4.g("org_id", str);
        }
        jb4.g("report_id", ec4.b());
        File[] d = ec4.d();
        for (File file : d) {
            if (file.getName().equals("minidump")) {
                jb4.h("minidump_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("metadata")) {
                jb4.h("crash_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("binaryImages")) {
                jb4.h("binary_images_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(Constants.SESSION)) {
                jb4.h("session_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("app")) {
                jb4.h("app_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("device")) {
                jb4.h("device_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("os")) {
                jb4.h("os_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("user")) {
                jb4.h("user_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(FileLogWriter.LOG_FOLDER)) {
                jb4.h("logs_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("keys")) {
                jb4.h("keys_file", file.getName(), "application/octet-stream", file);
            }
        }
        return jb4;
    }
}
