package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha0 implements Parcelable.Creator<ia0> {
    @DexIgnore
    public /* synthetic */ ha0(kq7 kq7) {
    }

    @DexIgnore
    public ia0 a(Parcel parcel) {
        return new ia0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ia0 createFromParcel(Parcel parcel) {
        return new ia0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ia0[] newArray(int i) {
        return new ia0[i];
    }
}
