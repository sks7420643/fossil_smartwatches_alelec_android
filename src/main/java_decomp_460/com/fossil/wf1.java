package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wf1 implements rb1<Bitmap> {
    @DexIgnore
    public static /* final */ nb1<Integer> b; // = nb1.f("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
    @DexIgnore
    public static /* final */ nb1<Bitmap.CompressFormat> c; // = nb1.e("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ od1 f3929a;

    @DexIgnore
    public wf1(od1 od1) {
        this.f3929a = od1;
    }

    @DexIgnore
    @Override // com.fossil.rb1
    public ib1 b(ob1 ob1) {
        return ib1.TRANSFORMED;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac A[Catch:{ all -> 0x00b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00bc A[SYNTHETIC, Splitter:B:32:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00d5  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.fossil.id1<android.graphics.Bitmap> r10, java.io.File r11, com.fossil.ob1 r12) {
        /*
            r9 = this;
            r3 = 0
            r4 = 0
            java.lang.Object r0 = r10.get()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            android.graphics.Bitmap$CompressFormat r5 = r9.d(r0, r12)
            java.lang.String r1 = "encode: [%dx%d] %s"
            int r2 = r0.getWidth()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r6 = r0.getHeight()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            com.fossil.lk1.c(r1, r2, r6, r5)
            long r6 = com.fossil.ek1.b()     // Catch:{ all -> 0x00c0 }
            com.fossil.nb1<java.lang.Integer> r1 = com.fossil.wf1.b     // Catch:{ all -> 0x00c0 }
            java.lang.Object r1 = r12.c(r1)     // Catch:{ all -> 0x00c0 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ all -> 0x00c0 }
            int r8 = r1.intValue()     // Catch:{ all -> 0x00c0 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00c5 }
            r2.<init>(r11)     // Catch:{ IOException -> 0x00c5 }
            com.fossil.od1 r1 = r9.f3929a     // Catch:{ IOException -> 0x00a1, all -> 0x00d3 }
            if (r1 == 0) goto L_0x00d8
            com.fossil.vb1 r1 = new com.fossil.vb1     // Catch:{ IOException -> 0x00a1, all -> 0x00d3 }
            com.fossil.od1 r3 = r9.f3929a     // Catch:{ IOException -> 0x00a1, all -> 0x00d3 }
            r1.<init>(r2, r3)     // Catch:{ IOException -> 0x00a1, all -> 0x00d3 }
        L_0x0041:
            r0.compress(r5, r8, r1)     // Catch:{ IOException -> 0x00cd, all -> 0x00d0 }
            r1.close()     // Catch:{ IOException -> 0x00cd, all -> 0x00d0 }
            r2 = 1
        L_0x0048:
            r1.close()     // Catch:{ IOException -> 0x00c8 }
            r1 = r2
        L_0x004c:
            java.lang.String r2 = "BitmapEncoder"
            r3 = 2
            boolean r2 = android.util.Log.isLoggable(r2, r3)
            if (r2 == 0) goto L_0x009d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Compressed with type: "
            r2.append(r3)
            r2.append(r5)
            java.lang.String r3 = " of size "
            r2.append(r3)
            int r3 = com.fossil.jk1.h(r0)
            r2.append(r3)
            java.lang.String r3 = " in "
            r2.append(r3)
            double r4 = com.fossil.ek1.a(r6)
            r2.append(r4)
            java.lang.String r3 = ", options format: "
            r2.append(r3)
            com.fossil.nb1<android.graphics.Bitmap$CompressFormat> r3 = com.fossil.wf1.c
            java.lang.Object r3 = r12.c(r3)
            r2.append(r3)
            java.lang.String r3 = ", hasAlpha: "
            r2.append(r3)
            boolean r0 = r0.hasAlpha()
            r2.append(r0)
            java.lang.String r0 = "BitmapEncoder"
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r0, r2)
        L_0x009d:
            com.fossil.lk1.d()
            return r1
        L_0x00a1:
            r1 = move-exception
            r3 = r2
        L_0x00a3:
            java.lang.String r2 = "BitmapEncoder"
            r8 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r8)     // Catch:{ all -> 0x00b8 }
            if (r2 == 0) goto L_0x00b3
            java.lang.String r2 = "BitmapEncoder"
            java.lang.String r8 = "Failed to encode Bitmap"
            android.util.Log.d(r2, r8, r1)     // Catch:{ all -> 0x00b8 }
        L_0x00b3:
            if (r3 == 0) goto L_0x00d5
            r2 = r4
            r1 = r3
            goto L_0x0048
        L_0x00b8:
            r0 = move-exception
        L_0x00b9:
            r2 = r3
        L_0x00ba:
            if (r2 == 0) goto L_0x00bf
            r2.close()     // Catch:{ IOException -> 0x00cb }
        L_0x00bf:
            throw r0
        L_0x00c0:
            r0 = move-exception
            com.fossil.lk1.d()
            throw r0
        L_0x00c5:
            r2 = move-exception
        L_0x00c6:
            r1 = r2
            goto L_0x00a3
        L_0x00c8:
            r1 = move-exception
            r1 = r2
            goto L_0x004c
        L_0x00cb:
            r1 = move-exception
            goto L_0x00bf
        L_0x00cd:
            r2 = move-exception
            r3 = r1
            goto L_0x00c6
        L_0x00d0:
            r0 = move-exception
            r3 = r1
            goto L_0x00b9
        L_0x00d3:
            r0 = move-exception
            goto L_0x00ba
        L_0x00d5:
            r1 = r4
            goto L_0x004c
        L_0x00d8:
            r1 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wf1.a(com.fossil.id1, java.io.File, com.fossil.ob1):boolean");
    }

    @DexIgnore
    public final Bitmap.CompressFormat d(Bitmap bitmap, ob1 ob1) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) ob1.c(c);
        return compressFormat != null ? compressFormat : bitmap.hasAlpha() ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG;
    }
}
