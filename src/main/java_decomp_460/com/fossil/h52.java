package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h52 extends vk2 implements g52 {
    @DexIgnore
    public h52(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    @DexIgnore
    @Override // com.fossil.g52
    public final void M0(e52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel d = d();
        xk2.b(d, e52);
        xk2.c(d, googleSignInOptions);
        e(102, d);
    }

    @DexIgnore
    @Override // com.fossil.g52
    public final void d1(e52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel d = d();
        xk2.b(d, e52);
        xk2.c(d, googleSignInOptions);
        e(103, d);
    }
}
