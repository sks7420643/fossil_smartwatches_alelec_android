package com.fossil;

import com.fossil.ur7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bs7 extends as7 {
    @DexIgnore
    public static final double b(double d, double d2) {
        return d < d2 ? d2 : d;
    }

    @DexIgnore
    public static final float c(float f, float f2) {
        return f < f2 ? f2 : f;
    }

    @DexIgnore
    public static final int d(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    @DexIgnore
    public static final long e(long j, long j2) {
        return j < j2 ? j2 : j;
    }

    @DexIgnore
    public static final double f(double d, double d2) {
        return d > d2 ? d2 : d;
    }

    @DexIgnore
    public static final int g(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    @DexIgnore
    public static final long h(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    @DexIgnore
    public static final int i(int i, int i2, int i3) {
        if (i2 <= i3) {
            return i < i2 ? i2 : i > i3 ? i3 : i;
        }
        throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
    }

    @DexIgnore
    public static final long j(long j, long j2, long j3) {
        if (j2 <= j3) {
            return j < j2 ? j2 : j > j3 ? j3 : j;
        }
        throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + j3 + " is less than minimum " + j2 + '.');
    }

    @DexIgnore
    public static final ur7 k(int i, int i2) {
        return ur7.e.a(i, i2, -1);
    }

    @DexIgnore
    public static final ur7 l(ur7 ur7, int i) {
        pq7.c(ur7, "$this$step");
        as7.a(i > 0, Integer.valueOf(i));
        ur7.a aVar = ur7.e;
        int a2 = ur7.a();
        int b = ur7.b();
        if (ur7.c() <= 0) {
            i = -i;
        }
        return aVar.a(a2, b, i);
    }

    @DexIgnore
    public static final wr7 m(int i, int i2) {
        return i2 <= Integer.MIN_VALUE ? wr7.g.a() : new wr7(i, i2 - 1);
    }

    @DexIgnore
    public static final zr7 n(long j, int i) {
        return new zr7(j, ((long) i) - 1);
    }
}
