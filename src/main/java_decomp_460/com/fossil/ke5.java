package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ConstraintLayout f1906a;
    @DexIgnore
    public /* final */ FlexibleButton b;
    @DexIgnore
    public /* final */ RTLImageView c;
    @DexIgnore
    public /* final */ ImageView d;
    @DexIgnore
    public /* final */ FlexibleTextView e;
    @DexIgnore
    public /* final */ Guideline f;
    @DexIgnore
    public /* final */ WatchFacePreviewView g;

    @DexIgnore
    public ke5(ConstraintLayout constraintLayout, FlexibleButton flexibleButton, RTLImageView rTLImageView, ImageView imageView, FlexibleTextView flexibleTextView, Guideline guideline, WatchFacePreviewView watchFacePreviewView) {
        this.f1906a = constraintLayout;
        this.b = flexibleButton;
        this.c = rTLImageView;
        this.d = imageView;
        this.e = flexibleTextView;
        this.f = guideline;
        this.g = watchFacePreviewView;
    }

    @DexIgnore
    public static ke5 a(View view) {
        int i = 2131363473;
        FlexibleButton flexibleButton = (FlexibleButton) view.findViewById(2131361934);
        if (flexibleButton != null) {
            RTLImageView rTLImageView = (RTLImageView) view.findViewById(2131362690);
            if (rTLImageView != null) {
                ImageView imageView = (ImageView) view.findViewById(2131362768);
                if (imageView != null) {
                    FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363410);
                    if (flexibleTextView != null) {
                        Guideline guideline = (Guideline) view.findViewById(2131363473);
                        if (guideline != null) {
                            i = 2131363538;
                            WatchFacePreviewView watchFacePreviewView = (WatchFacePreviewView) view.findViewById(2131363538);
                            if (watchFacePreviewView != null) {
                                return new ke5((ConstraintLayout) view, flexibleButton, rTLImageView, imageView, flexibleTextView, guideline, watchFacePreviewView);
                            }
                        }
                    } else {
                        i = 2131363410;
                    }
                } else {
                    i = 2131362768;
                }
            } else {
                i = 2131362690;
            }
        } else {
            i = 2131361934;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static ke5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558685, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.f1906a;
    }
}
