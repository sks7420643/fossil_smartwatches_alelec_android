package com.fossil;

import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c97 {
    @DexIgnore
    public static final gj4 a(List<f97> list) {
        pq7.c(list, "$this$toPhotosJsonObject");
        gj4 gj4 = new gj4();
        bj4 bj4 = new bj4();
        for (T t : list) {
            gj4 gj42 = new gj4();
            gj42.n("id", t.a());
            gj42.n("image", t.b());
            bj4.k(gj42);
        }
        gj4.k(CloudLogWriter.ITEMS_PARAM, bj4);
        return gj4;
    }
}
