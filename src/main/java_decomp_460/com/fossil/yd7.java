package com.fossil;

import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yd7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ boolean f4305a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public SQLiteDatabase e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements DatabaseErrorHandler {
        @DexIgnore
        public a(yd7 yd7) {
        }

        @DexIgnore
        public void onCorruption(SQLiteDatabase sQLiteDatabase) {
        }
    }

    @DexIgnore
    public yd7(String str, int i, boolean z, int i2) {
        this.b = str;
        this.f4305a = z;
        this.c = i;
        this.d = i2;
    }

    @DexIgnore
    public static void b(String str) {
        SQLiteDatabase.deleteDatabase(new File(str));
    }

    @DexIgnore
    public void a() {
        this.e.close();
    }

    @DexIgnore
    public SQLiteDatabase c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return "[" + e() + "] ";
    }

    @DexIgnore
    public String e() {
        Thread currentThread = Thread.currentThread();
        return "" + this.c + "," + currentThread.getName() + "(" + currentThread.getId() + ")";
    }

    @DexIgnore
    public SQLiteDatabase f() {
        return this.e;
    }

    @DexIgnore
    public void g() {
        this.e = SQLiteDatabase.openDatabase(this.b, null, net.sqlcipher.database.SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    @DexIgnore
    public void h() {
        this.e = SQLiteDatabase.openDatabase(this.b, null, 1, new a(this));
    }
}
