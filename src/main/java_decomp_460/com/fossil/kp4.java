package com.fossil;

import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp4 implements Factory<GuestApiService> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f1943a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public kp4(uo4 uo4, Provider<on5> provider) {
        this.f1943a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static kp4 a(uo4 uo4, Provider<on5> provider) {
        return new kp4(uo4, provider);
    }

    @DexIgnore
    public static GuestApiService c(uo4 uo4, on5 on5) {
        GuestApiService r = uo4.r(on5);
        lk7.c(r, "Cannot return null from a non-@Nullable @Provides method");
        return r;
    }

    @DexIgnore
    /* renamed from: b */
    public GuestApiService get() {
        return c(this.f1943a, this.b.get());
    }
}
