package com.fossil;

import com.fossil.xw7;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lu7<T> extends yv7<T> implements ku7<T>, do7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater g; // = AtomicIntegerFieldUpdater.newUpdater(lu7.class, "_decision");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater h; // = AtomicReferenceFieldUpdater.newUpdater(lu7.class, Object.class, "_state");
    @DexIgnore
    public volatile int _decision; // = 0;
    @DexIgnore
    public volatile Object _parentHandle; // = null;
    @DexIgnore
    public volatile Object _state; // = bu7.b;
    @DexIgnore
    public /* final */ tn7 e;
    @DexIgnore
    public /* final */ qn7<T> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.qn7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public lu7(qn7<? super T> qn7, int i) {
        super(i);
        this.f = qn7;
        this.e = qn7.getContext();
    }

    @DexIgnore
    public final void A(Throwable th) {
        if (!m(th)) {
            l(th);
            p();
        }
    }

    @DexIgnore
    public final boolean B() {
        if (nv7.a()) {
            if (!(s() != lx7.b)) {
                throw new AssertionError();
            }
        }
        Object obj = this._state;
        if (nv7.a() && !(!(obj instanceof mx7))) {
            throw new AssertionError();
        } else if (obj instanceof xu7) {
            o();
            return false;
        } else {
            this._decision = 0;
            this._state = bu7.b;
            return true;
        }
    }

    @DexIgnore
    public final ou7 C(Object obj, int i) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof mx7)) {
                if (obj2 instanceof ou7) {
                    ou7 ou7 = (ou7) obj2;
                    if (ou7.c()) {
                        return ou7;
                    }
                }
                k(obj);
                throw null;
            }
        } while (!h.compareAndSet(this, obj2, obj));
        p();
        q(i);
        return null;
    }

    @DexIgnore
    public final void D(dw7 dw7) {
        this._parentHandle = dw7;
    }

    @DexIgnore
    public final void E() {
        xw7 xw7;
        if (!n() && s() == null && (xw7 = (xw7) this.f.getContext().get(xw7.r)) != null) {
            xw7.start();
            dw7 d = xw7.a.d(xw7, true, false, new pu7(xw7, this), 2, null);
            D(d);
            if (v() && !w()) {
                d.dispose();
                D(lx7.b);
            }
        }
    }

    @DexIgnore
    public final boolean F() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!g.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean G() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!g.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    @Override // com.fossil.yv7
    public void a(Object obj, Throwable th) {
        if (obj instanceof yu7) {
            try {
                ((yu7) obj).b.invoke(th);
            } catch (Throwable th2) {
                tn7 context = getContext();
                fv7.a(context, new av7("Exception in cancellation handler for " + this, th2));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ku7
    public Object b(T t, Object obj) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof mx7)) {
                if (obj2 instanceof xu7) {
                    xu7 xu7 = (xu7) obj2;
                    if (xu7.f4183a == obj) {
                        if (nv7.a()) {
                            if (!(xu7.b == t)) {
                                throw new AssertionError();
                            }
                        }
                        return mu7.f2424a;
                    }
                }
                return null;
            }
        } while (!h.compareAndSet(this, obj2, obj == null ? t : new xu7(obj, t)));
        p();
        return mu7.f2424a;
    }

    @DexIgnore
    @Override // com.fossil.yv7
    public final qn7<T> c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ku7
    public void e(rp7<? super Throwable, tl7> rp7) {
        Object obj;
        Throwable th = null;
        iu7 iu7 = null;
        do {
            obj = this._state;
            if (obj instanceof bu7) {
                if (iu7 == null) {
                    iu7 = x(rp7);
                }
            } else if (obj instanceof iu7) {
                y(rp7, obj);
                throw null;
            } else if (!(obj instanceof ou7)) {
                return;
            } else {
                if (((ou7) obj).b()) {
                    try {
                        vu7 vu7 = (vu7) (!(obj instanceof vu7) ? null : obj);
                        if (vu7 != null) {
                            th = vu7.f3837a;
                        }
                        rp7.invoke(th);
                        return;
                    } catch (Throwable th2) {
                        fv7.a(getContext(), new av7("Exception in cancellation handler for " + this, th2));
                        return;
                    }
                } else {
                    y(rp7, obj);
                    throw null;
                }
            }
        } while (!h.compareAndSet(this, obj, iu7));
    }

    @DexIgnore
    @Override // com.fossil.ku7
    public void f(dv7 dv7, T t) {
        dv7 dv72 = null;
        qn7<T> qn7 = this.f;
        if (!(qn7 instanceof vv7)) {
            qn7 = null;
        }
        vv7 vv7 = (vv7) qn7;
        if (vv7 != null) {
            dv72 = vv7.h;
        }
        C(t, dv72 == dv7 ? 2 : this.d);
    }

    @DexIgnore
    @Override // com.fossil.ku7
    public void g(Object obj) {
        if (nv7.a()) {
            if (!(obj == mu7.f2424a)) {
                throw new AssertionError();
            }
        }
        q(this.d);
    }

    @DexIgnore
    @Override // com.fossil.do7
    public do7 getCallerFrame() {
        qn7<T> qn7 = this.f;
        if (!(qn7 instanceof do7)) {
            qn7 = null;
        }
        return (do7) qn7;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public tn7 getContext() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.do7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.yv7
    public <T> T h(Object obj) {
        return obj instanceof xu7 ? (T) ((xu7) obj).b : obj instanceof yu7 ? (T) ((yu7) obj).f4380a : obj;
    }

    @DexIgnore
    @Override // com.fossil.ku7
    public boolean isActive() {
        return u() instanceof mx7;
    }

    @DexIgnore
    @Override // com.fossil.yv7
    public Object j() {
        return u();
    }

    @DexIgnore
    public final void k(Object obj) {
        throw new IllegalStateException(("Already resumed, but proposed with update " + obj).toString());
    }

    @DexIgnore
    public boolean l(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof mx7)) {
                return false;
            }
            z = obj instanceof iu7;
        } while (!h.compareAndSet(this, obj, new ou7(this, th, z)));
        if (z) {
            try {
                ((iu7) obj).a(th);
            } catch (Throwable th2) {
                tn7 context = getContext();
                fv7.a(context, new av7("Exception in cancellation handler for " + this, th2));
            }
        }
        p();
        q(0);
        return true;
    }

    @DexIgnore
    public final boolean m(Throwable th) {
        if (this.d != 0) {
            return false;
        }
        qn7<T> qn7 = this.f;
        if (!(qn7 instanceof vv7)) {
            qn7 = null;
        }
        vv7 vv7 = (vv7) qn7;
        if (vv7 != null) {
            return vv7.p(th);
        }
        return false;
    }

    @DexIgnore
    public final boolean n() {
        boolean z;
        Throwable k;
        boolean v = v();
        if (this.d != 0) {
            return v;
        }
        qn7<T> qn7 = this.f;
        if (!(qn7 instanceof vv7)) {
            qn7 = null;
        }
        vv7 vv7 = (vv7) qn7;
        if (vv7 == null || (k = vv7.k(this)) == null) {
            z = v;
        } else {
            if (!v) {
                l(k);
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final void o() {
        dw7 s = s();
        if (s != null) {
            s.dispose();
        }
        D(lx7.b);
    }

    @DexIgnore
    public final void p() {
        if (!w()) {
            o();
        }
    }

    @DexIgnore
    public final void q(int i) {
        if (!F()) {
            zv7.a(this, i);
        }
    }

    @DexIgnore
    public Throwable r(xw7 xw7) {
        return xw7.k();
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void resumeWith(Object obj) {
        C(wu7.c(obj, this), this.d);
    }

    @DexIgnore
    public final dw7 s() {
        return (dw7) this._parentHandle;
    }

    @DexIgnore
    public final Object t() {
        xw7 xw7;
        E();
        if (G()) {
            return yn7.d();
        }
        Object u = u();
        if (u instanceof vu7) {
            Throwable th = ((vu7) u).f3837a;
            if (nv7.d()) {
                throw uz7.a(th, this);
            }
            throw th;
        } else if (this.d != 1 || (xw7 = (xw7) getContext().get(xw7.r)) == null || xw7.isActive()) {
            return h(u);
        } else {
            CancellationException k = xw7.k();
            a(u, k);
            if (nv7.d()) {
                throw uz7.a(k, this);
            }
            throw k;
        }
    }

    @DexIgnore
    public String toString() {
        return z() + '(' + ov7.c(this.f) + "){" + u() + "}@" + ov7.b(this);
    }

    @DexIgnore
    public final Object u() {
        return this._state;
    }

    @DexIgnore
    public boolean v() {
        return !(u() instanceof mx7);
    }

    @DexIgnore
    public final boolean w() {
        qn7<T> qn7 = this.f;
        return (qn7 instanceof vv7) && ((vv7) qn7).o();
    }

    @DexIgnore
    public final iu7 x(rp7<? super Throwable, tl7> rp7) {
        return rp7 instanceof iu7 ? (iu7) rp7 : new uw7(rp7);
    }

    @DexIgnore
    public final void y(rp7<? super Throwable, tl7> rp7, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + rp7 + ", already has " + obj).toString());
    }

    @DexIgnore
    public String z() {
        return "CancellableContinuation";
    }
}
