package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum oh5 {
    Device("Device"),
    User("User"),
    Mock("Mock");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public oh5(String str) {
        this.value = str;
    }

    @DexIgnore
    public static oh5 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            oh5[] values = values();
            for (oh5 oh5 : values) {
                if (str.equalsIgnoreCase(oh5.value)) {
                    return oh5;
                }
            }
        }
        return Device;
    }

    @DexIgnore
    public String getName() {
        return this.value;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
