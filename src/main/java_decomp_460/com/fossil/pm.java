package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm extends qq7 implements rp7<mw, Boolean> {
    @DexIgnore
    public static /* final */ pm b; // = new pm();

    @DexIgnore
    public pm() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public Boolean invoke(mw mwVar) {
        return Boolean.valueOf(mwVar.d == lw.o);
    }
}
