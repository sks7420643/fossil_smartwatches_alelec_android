package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov3 implements Parcelable.Creator<nv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ nv3 createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        int i = 0;
        String str = null;
        byte[] bArr = null;
        String str2 = null;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                i = ad2.v(parcel, t);
            } else if (l == 3) {
                str2 = ad2.f(parcel, t);
            } else if (l == 4) {
                bArr = ad2.b(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                str = ad2.f(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new nv3(i, str2, bArr, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ nv3[] newArray(int i) {
        return new nv3[i];
    }
}
