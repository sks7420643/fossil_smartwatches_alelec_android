package com.fossil;

import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r51 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Drawable f3081a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public r51(Drawable drawable, boolean z) {
        pq7.c(drawable, ResourceManager.DRAWABLE);
        this.f3081a = drawable;
        this.b = z;
    }

    @DexIgnore
    public final Drawable a() {
        return this.f3081a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof r51) {
                r51 r51 = (r51) obj;
                if (!pq7.a(this.f3081a, r51.f3081a) || this.b != r51.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Drawable drawable = this.f3081a;
        int hashCode = drawable != null ? drawable.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "DecodeResult(drawable=" + this.f3081a + ", isSampled=" + this.b + ")";
    }
}
