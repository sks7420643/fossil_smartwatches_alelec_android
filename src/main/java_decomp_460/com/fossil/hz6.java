package com.fossil;

import com.portfolio.platform.data.model.PermissionData;
import dagger.internal.Factory;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz6 implements Factory<List<PermissionData>> {
    @DexIgnore
    public static List<PermissionData> a(gz6 gz6) {
        List<PermissionData> a2 = gz6.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
