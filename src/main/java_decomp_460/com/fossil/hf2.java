package com.fossil;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hf2 implements ef2 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ hf2 f1473a; // = new hf2();

    @DexIgnore
    public static ef2 d() {
        return f1473a;
    }

    @DexIgnore
    @Override // com.fossil.ef2
    public long a() {
        return System.nanoTime();
    }

    @DexIgnore
    @Override // com.fossil.ef2
    public long b() {
        return System.currentTimeMillis();
    }

    @DexIgnore
    @Override // com.fossil.ef2
    public long c() {
        return SystemClock.elapsedRealtime();
    }
}
