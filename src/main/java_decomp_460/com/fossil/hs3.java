package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.r62;
import com.fossil.yb2;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hs3 extends ec2<ms3> implements ys3 {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ ac2 F;
    @DexIgnore
    public /* final */ Bundle G;
    @DexIgnore
    public Integer H;

    @DexIgnore
    public hs3(Context context, Looper looper, boolean z, ac2 ac2, Bundle bundle, r62.b bVar, r62.c cVar) {
        super(context, looper, 44, ac2, bVar, cVar);
        this.E = true;
        this.F = ac2;
        this.G = bundle;
        this.H = ac2.f();
    }

    @DexIgnore
    public hs3(Context context, Looper looper, boolean z, ac2 ac2, gs3 gs3, r62.b bVar, r62.c cVar) {
        this(context, looper, true, ac2, t0(ac2), bVar, cVar);
    }

    @DexIgnore
    public static Bundle t0(ac2 ac2) {
        gs3 k = ac2.k();
        Integer f = ac2.f();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", ac2.a());
        if (f != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", f.intValue());
        }
        if (k != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", k.i());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", k.h());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", k.f());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", k.g());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", k.c());
            bundle.putString("com.google.android.gms.signin.internal.logSessionId", k.d());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", k.j());
            if (k.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", k.a().longValue());
            }
            if (k.e() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", k.e().longValue());
            }
        }
        return bundle;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public Bundle F() {
        if (!E().getPackageName().equals(this.F.i())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.i());
        }
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.ys3
    public final void b() {
        l(new yb2.d());
    }

    @DexIgnore
    @Override // com.fossil.ys3
    public final void d(jc2 jc2, boolean z) {
        try {
            ((ms3) I()).F0(jc2, this.H.intValue(), z);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    @DexIgnore
    @Override // com.fossil.ys3
    public final void e(ks3 ks3) {
        rc2.l(ks3, "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.F.c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = o42.b(E()).c();
            }
            ((ms3) I()).Q1(new ss3(new sc2(c, this.H.intValue(), googleSignInAccount)), ks3);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                ks3.Q2(new us3(8));
            } catch (RemoteException e2) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ys3
    public final void o() {
        try {
            ((ms3) I()).K(this.H.intValue());
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public String p() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        return queryLocalInterface instanceof ms3 ? (ms3) queryLocalInterface : new ps3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.ec2, com.fossil.yb2
    public int s() {
        return h62.f1430a;
    }

    @DexIgnore
    @Override // com.fossil.m62.f, com.fossil.yb2
    public boolean v() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public String x() {
        return "com.google.android.gms.signin.service.START";
    }
}
