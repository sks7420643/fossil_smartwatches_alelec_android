package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np4 implements Factory<ct0> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ uo4 f2555a;

    @DexIgnore
    public np4(uo4 uo4) {
        this.f2555a = uo4;
    }

    @DexIgnore
    public static np4 a(uo4 uo4) {
        return new np4(uo4);
    }

    @DexIgnore
    public static ct0 c(uo4 uo4) {
        ct0 u = uo4.u();
        lk7.c(u, "Cannot return null from a non-@Nullable @Provides method");
        return u;
    }

    @DexIgnore
    /* renamed from: b */
    public ct0 get() {
        return c(this.f2555a);
    }
}
