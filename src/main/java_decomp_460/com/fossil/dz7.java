package com.fossil;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Method f863a;

    /*
    static {
        Method method;
        try {
            method = ScheduledThreadPoolExecutor.class.getMethod("setRemoveOnCancelPolicy", Boolean.TYPE);
        } catch (Throwable th) {
            method = null;
        }
        f863a = method;
    }
    */

    @DexIgnore
    public static final boolean a(Executor executor) {
        Method method;
        try {
            ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService) (!(executor instanceof ScheduledExecutorService) ? null : executor);
            if (scheduledExecutorService == null || (method = f863a) == null) {
                return false;
            }
            method.invoke(scheduledExecutorService, Boolean.TRUE);
            return true;
        } catch (Throwable th) {
            return true;
        }
    }
}
