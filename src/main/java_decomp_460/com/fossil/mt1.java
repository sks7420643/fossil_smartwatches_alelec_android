package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mt1 extends ox1 implements Parcelable {
    @DexIgnore
    public /* final */ jq1 b;
    @DexIgnore
    public /* final */ nt1 c;

    @DexIgnore
    public mt1(jq1 jq1, nt1 nt1) {
        this.b = jq1;
        this.c = nt1;
    }

    @DexIgnore
    public JSONObject a() {
        return new JSONObject();
    }

    @DexIgnore
    public abstract byte[] a(short s, ry1 ry1);

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            mt1 mt1 = (mt1) obj;
            return !(pq7.a(this.b, mt1.b) ^ true) && !(pq7.a(this.c, mt1.c) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.DeviceData");
    }

    @DexIgnore
    public final nt1 getDeviceMessage() {
        return this.c;
    }

    @DexIgnore
    public final jq1 getDeviceRequest() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        jq1 jq1 = this.b;
        int hashCode = jq1 != null ? jq1.hashCode() : 0;
        nt1 nt1 = this.c;
        if (nt1 != null) {
            i = nt1.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        Object obj;
        Object obj2;
        JSONObject jSONObject = new JSONObject();
        try {
            jd0 jd0 = jd0.o;
            jq1 jq1 = this.b;
            if (jq1 == null || (obj = jq1.toJSONObject()) == null) {
                obj = JSONObject.NULL;
            }
            g80.k(jSONObject, jd0, obj);
            jd0 jd02 = jd0.p;
            nt1 nt1 = this.c;
            if (nt1 == null || (obj2 = nt1.toJSONObject()) == null) {
                obj2 = JSONObject.NULL;
            }
            g80.k(jSONObject, jd02, obj2);
            g80.k(jSONObject, jd0.Y2, a());
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
