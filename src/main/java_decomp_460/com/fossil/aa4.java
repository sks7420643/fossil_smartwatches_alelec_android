package com.fossil;

import com.fossil.ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aa4 extends ta4.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f232a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ta4.b.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f233a;
        @DexIgnore
        public String b;

        @DexIgnore
        @Override // com.fossil.ta4.b.a
        public ta4.b a() {
            String str = "";
            if (this.f233a == null) {
                str = " key";
            }
            if (this.b == null) {
                str = str + " value";
            }
            if (str.isEmpty()) {
                return new aa4(this.f233a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.ta4.b.a
        public ta4.b.a b(String str) {
            if (str != null) {
                this.f233a = str;
                return this;
            }
            throw new NullPointerException("Null key");
        }

        @DexIgnore
        @Override // com.fossil.ta4.b.a
        public ta4.b.a c(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null value");
        }
    }

    @DexIgnore
    public aa4(String str, String str2) {
        this.f232a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // com.fossil.ta4.b
    public String b() {
        return this.f232a;
    }

    @DexIgnore
    @Override // com.fossil.ta4.b
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ta4.b)) {
            return false;
        }
        ta4.b bVar = (ta4.b) obj;
        return this.f232a.equals(bVar.b()) && this.b.equals(bVar.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((this.f232a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CustomAttribute{key=" + this.f232a + ", value=" + this.b + "}";
    }
}
