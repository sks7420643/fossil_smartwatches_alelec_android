package com.fossil;

import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ck0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public HashSet<ck0> f623a; // = new HashSet<>(2);
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public void a(ck0 ck0) {
        this.f623a.add(ck0);
    }

    @DexIgnore
    public void b() {
        this.b = 1;
        Iterator<ck0> it = this.f623a.iterator();
        while (it.hasNext()) {
            it.next().f();
        }
    }

    @DexIgnore
    public void c() {
        this.b = 0;
        Iterator<ck0> it = this.f623a.iterator();
        while (it.hasNext()) {
            it.next().c();
        }
    }

    @DexIgnore
    public boolean d() {
        return this.b == 1;
    }

    @DexIgnore
    public void e() {
        this.b = 0;
        this.f623a.clear();
    }

    @DexIgnore
    public void f() {
    }
}
