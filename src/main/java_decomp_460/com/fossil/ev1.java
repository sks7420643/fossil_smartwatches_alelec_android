package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ io1[] b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ev1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ev1 createFromParcel(Parcel parcel) {
            Object[] createTypedArray = parcel.createTypedArray(io1.CREATOR);
            if (createTypedArray != null) {
                pq7.b(createTypedArray, "parcel.createTypedArray(\u2026tificationReplyMessage)!!");
                return new ev1((io1[]) createTypedArray);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ev1[] newArray(int i) {
            return new ev1[i];
        }
    }

    @DexIgnore
    public ev1(io1[] io1Arr) {
        Object[] copyOf = Arrays.copyOf(io1Arr, io1Arr.length);
        pq7.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : copyOf) {
            if (hashSet.add(Byte.valueOf(((io1) obj).getMessageId()))) {
                arrayList.add(obj);
            }
        }
        Object[] array = pm7.b0(arrayList.subList(0, Math.min(10, arrayList.size())), new sb0()).toArray(new io1[0]);
        if (array != null) {
            this.b = (io1[]) array;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (io1 io1 : this.b) {
                byteArrayOutputStream.write(io1.b());
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            pq7.b(byteArray, "entriesData.toByteArray()");
            this.c = byteArray;
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final byte[] a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(ev1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((ev1) obj).b);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageGroup");
    }

    @DexIgnore
    public final io1[] getReplyMessages() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return bm7.a(this.b);
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(new JSONObject(), jd0.n, px1.a(this.b));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.b, i);
    }
}
