package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu7 extends zw7<xw7> {
    @DexIgnore
    public /* final */ lu7<?> f;

    @DexIgnore
    public pu7(xw7 xw7, lu7<?> lu7) {
        super(xw7);
        this.f = lu7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.rp7
    public /* bridge */ /* synthetic */ tl7 invoke(Throwable th) {
        w(th);
        return tl7.f3441a;
    }

    @DexIgnore
    @Override // com.fossil.lz7
    public String toString() {
        return "ChildContinuation[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.zu7
    public void w(Throwable th) {
        lu7<?> lu7 = this.f;
        lu7.A(lu7.r(this.e));
    }
}
