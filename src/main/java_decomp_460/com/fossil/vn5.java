package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn5 {
    @DexIgnore
    public static /* final */ String g;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public GoogleSignInOptions f3794a;
    @DexIgnore
    public j42 b;
    @DexIgnore
    public /* final */ String c; // = "23412525";
    @DexIgnore
    public WeakReference<ls5> d;
    @DexIgnore
    public yn5 e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<TResult> implements ht3<Void> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ a f3795a; // = new a();

        @DexIgnore
        @Override // com.fossil.ht3
        public final void onComplete(nt3<Void> nt3) {
            pq7.c(nt3, "it");
            FLogger.INSTANCE.getLocal().d(vn5.g, "Log out google account completely");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements it3 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ b f3796a; // = new b();

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            pq7.c(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = vn5.g;
            local.d(str, "Could not log out google account, error = " + exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    /*
    static {
        String canonicalName = vn5.class.getCanonicalName();
        if (canonicalName != null) {
            pq7.b(canonicalName, "MFLoginGoogleManager::class.java.canonicalName!!");
            g = canonicalName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public final void b() {
        j42 j42;
        FLogger.INSTANCE.getLocal().d(g, "connectNewAccount");
        WeakReference<ls5> weakReference = this.d;
        if (weakReference != null) {
            if (weakReference == null) {
                pq7.i();
                throw null;
            } else if (!(weakReference.get() == null || (j42 = this.b) == null)) {
                if (j42 != null) {
                    Intent s = j42.s();
                    pq7.b(s, "googleSignInClient!!.signInIntent");
                    WeakReference<ls5> weakReference2 = this.d;
                    if (weakReference2 != null) {
                        ls5 ls5 = weakReference2.get();
                        if (ls5 != null) {
                            ls5.startActivityForResult(s, 922);
                            return;
                        }
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
        }
        yn5 yn5 = this.e;
        if (yn5 != null) {
            yn5.b(600, null, "");
        }
    }

    @DexIgnore
    public final void c() {
        f(this.d);
        b();
    }

    @DexIgnore
    public final void d() {
        f(this.d);
    }

    @DexIgnore
    public final void e(l42 l42) {
        int i = 2;
        if (l42 == null) {
            d();
            yn5 yn5 = this.e;
            if (yn5 != null) {
                yn5.b(600, null, "");
            }
        } else if (l42.c()) {
            GoogleSignInAccount b2 = l42.b();
            if (b2 == null) {
                yn5 yn52 = this.e;
                if (yn52 != null) {
                    yn52.b(600, null, "");
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(g, "Step 1: Login using google success");
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String f2 = b2.f();
                if (f2 == null) {
                    f2 = "";
                }
                signUpSocialAuth.setEmail(f2);
                FLogger.INSTANCE.getLocal().d(g, "Google user email is " + b2.f());
                String k = b2.k();
                if (k == null) {
                    k = "";
                }
                signUpSocialAuth.setFirstName(k);
                FLogger.INSTANCE.getLocal().d(g, "Google user first name is " + b2.k());
                String h = b2.h();
                if (h == null) {
                    h = "";
                }
                signUpSocialAuth.setLastName(h);
                FLogger.INSTANCE.getLocal().d(g, "Google user last name is " + b2.h());
                String F = b2.F();
                if (F == null) {
                    F = "";
                }
                signUpSocialAuth.setToken(F);
                signUpSocialAuth.setClientId(dk5.g.a(""));
                signUpSocialAuth.setService("google");
                yn5 yn53 = this.e;
                if (yn53 != null) {
                    yn53.a(signUpSocialAuth);
                }
                this.f = 0;
                d();
            }
        } else {
            Status a2 = l42.a();
            pq7.b(a2, "result.status");
            int f3 = a2.f();
            FLogger.INSTANCE.getLocal().d(g, "login result code from google: " + f3);
            if (f3 == 12502) {
                int i2 = this.f;
                if (i2 < 2) {
                    this.f = i2 + 1;
                    b();
                    return;
                }
                this.f = 0;
                FLogger.INSTANCE.getLocal().d(g, "why the login session always end up here, bug from Google!!");
                d();
                yn5 yn54 = this.e;
                if (yn54 != null) {
                    yn54.b(600, null, "");
                    return;
                }
                return;
            }
            if (f3 != 12501) {
                i = 600;
            }
            d();
            yn5 yn55 = this.e;
            if (yn55 != null) {
                yn55.b(i, null, "");
            }
        }
    }

    @DexIgnore
    public final void f(WeakReference<Activity> weakReference) {
        if ((weakReference != null ? weakReference.get() : null) != null && h42.b(weakReference.get()) != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "inside .logOut(), googleSignInClient=" + this.b);
            j42 j42 = this.b;
            if (j42 == null) {
                return;
            }
            if (j42 != null) {
                nt3<Void> t = j42.t();
                if (t != null) {
                    t.b(a.f3795a);
                }
                if (t != null) {
                    t.d(b.f3796a);
                    return;
                }
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void g(WeakReference<ls5> weakReference, yn5 yn5) {
        String str;
        pq7.c(weakReference, Constants.ACTIVITY);
        pq7.c(yn5, Constants.CALLBACK);
        Access c2 = SoLibraryLoader.f().c(PortfolioApp.h0.c());
        if (c2 == null || (str = c2.getA()) == null) {
            str = this.c;
        }
        if (str != null) {
            String obj = wt7.u0(str).toString();
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(GoogleSignInOptions.v);
            aVar.d(obj);
            aVar.g(obj);
            aVar.b();
            aVar.e();
            GoogleSignInOptions a2 = aVar.a();
            pq7.b(a2, "GoogleSignInOptions.Buil\u2026\n                .build()");
            this.f3794a = a2;
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            GoogleSignInOptions googleSignInOptions = this.f3794a;
            if (googleSignInOptions != null) {
                this.b = h42.a(applicationContext, googleSignInOptions);
                this.e = yn5;
                this.d = weakReference;
                c();
                return;
            }
            pq7.n("gso");
            throw null;
        }
        throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final boolean h(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Inside .onActivityResult requestCode=" + i + ", resultCode=" + i2);
        if (i != 922) {
            return true;
        }
        if (intent != null) {
            l42 a2 = d42.f.a(intent);
            e(a2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = g;
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult googleSignInResult=");
            pq7.b(a2, Constants.RESULT);
            sb.append(a2.a());
            local2.d(str2, sb.toString());
            return true;
        }
        e(null);
        return true;
    }
}
