package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ih5 implements Factory<hh5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<UserRepository> f1629a;

    @DexIgnore
    public ih5(Provider<UserRepository> provider) {
        this.f1629a = provider;
    }

    @DexIgnore
    public static ih5 a(Provider<UserRepository> provider) {
        return new ih5(provider);
    }

    @DexIgnore
    public static hh5 c(UserRepository userRepository) {
        return new hh5(userRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public hh5 get() {
        return c(this.f1629a.get());
    }
}
