package com.fossil;

import android.os.Build;
import androidx.work.ListenableWorker;
import androidx.work.OverwritingInputMerger;
import com.fossil.h11;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z01 extends h11 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends h11.a<a, z01> {
        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            super(cls);
            this.c.d = OverwritingInputMerger.class.getName();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.h11$a' to match base method */
        @Override // com.fossil.h11.a
        public /* bridge */ /* synthetic */ a d() {
            g();
            return this;
        }

        @DexIgnore
        /* renamed from: f */
        public z01 c() {
            if (!this.f1408a || Build.VERSION.SDK_INT < 23 || !this.c.j.h()) {
                o31 o31 = this.c;
                if (!o31.q || Build.VERSION.SDK_INT < 23 || !o31.j.h()) {
                    return new z01(this);
                }
                throw new IllegalArgumentException("Cannot run in foreground with an idle mode constraint");
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
        }

        @DexIgnore
        public a g() {
            return this;
        }
    }

    @DexIgnore
    public z01(a aVar) {
        super(aVar.b, aVar.c, aVar.d);
    }

    @DexIgnore
    public static z01 e(Class<? extends ListenableWorker> cls) {
        return (z01) new a(cls).b();
    }
}
