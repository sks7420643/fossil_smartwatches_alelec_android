package com.fossil;

import com.fossil.h34;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r44<E> extends h34.b<E> {
    @DexIgnore
    public static /* final */ r44<Object> EMPTY; // = new r44<>(h44.f1427a, 0, null, 0);
    @DexIgnore
    public /* final */ transient Object[] c;
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ transient Object[] table;

    @DexIgnore
    public r44(Object[] objArr, int i, Object[] objArr2, int i2) {
        this.c = objArr;
        this.table = objArr2;
        this.d = i2;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean contains(Object obj) {
        Object[] objArr = this.table;
        if (obj == null || objArr == null) {
            return false;
        }
        int c2 = r24.c(obj);
        while (true) {
            int i = c2 & this.d;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            c2 = i + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.u24
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.c;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return this.c.length + i;
    }

    @DexIgnore
    @Override // com.fossil.h34, com.fossil.h34.b
    public y24<E> createAsList() {
        return this.table == null ? y24.of() : new m44(this, this.c);
    }

    @DexIgnore
    @Override // com.fossil.h34.b
    public E get(int i) {
        return (E) this.c[i];
    }

    @DexIgnore
    @Override // com.fossil.h34
    public int hashCode() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.h34
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.u24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.c.length;
    }
}
