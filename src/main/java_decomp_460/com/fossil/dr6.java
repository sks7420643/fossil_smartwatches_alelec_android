package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr6 implements Factory<cr6> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<ThemeRepository> f826a;

    @DexIgnore
    public dr6(Provider<ThemeRepository> provider) {
        this.f826a = provider;
    }

    @DexIgnore
    public static dr6 a(Provider<ThemeRepository> provider) {
        return new dr6(provider);
    }

    @DexIgnore
    public static cr6 c(ThemeRepository themeRepository) {
        return new cr6(themeRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public cr6 get() {
        return c(this.f826a.get());
    }
}
