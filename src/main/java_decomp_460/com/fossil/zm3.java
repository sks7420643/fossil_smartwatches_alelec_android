package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm3 implements Callable<List<xr3>> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ /* synthetic */ String f4499a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ qm3 d;

    @DexIgnore
    public zm3(qm3 qm3, String str, String str2, String str3) {
        this.d = qm3;
        this.f4499a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<xr3> call() throws Exception {
        this.d.b.d0();
        return this.d.b.U().j0(this.f4499a, this.b, this.c);
    }
}
