package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iz3 extends Transition {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ TextView f1699a;

        @DexIgnore
        public a(iz3 iz3, TextView textView) {
            this.f1699a = textView;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.f1699a.setScaleX(floatValue);
            this.f1699a.setScaleY(floatValue);
        }
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public void n(wy0 wy0) {
        r0(wy0);
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public void q(wy0 wy0) {
        r0(wy0);
    }

    @DexIgnore
    public final void r0(wy0 wy0) {
        View view = wy0.b;
        if (view instanceof TextView) {
            wy0.f4017a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public Animator u(ViewGroup viewGroup, wy0 wy0, wy0 wy02) {
        ValueAnimator valueAnimator;
        float f = 1.0f;
        if (wy0 == null || wy02 == null || !(wy0.b instanceof TextView)) {
            valueAnimator = null;
        } else {
            View view = wy02.b;
            if (!(view instanceof TextView)) {
                valueAnimator = null;
            } else {
                TextView textView = (TextView) view;
                Map<String, Object> map = wy0.f4017a;
                Map<String, Object> map2 = wy02.f4017a;
                float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
                if (map2.get("android:textscale:scale") != null) {
                    f = ((Float) map2.get("android:textscale:scale")).floatValue();
                }
                if (floatValue == f) {
                    return null;
                }
                ValueAnimator ofFloat = ValueAnimator.ofFloat(floatValue, f);
                ofFloat.addUpdateListener(new a(this, textView));
                valueAnimator = ofFloat;
            }
        }
        return valueAnimator;
    }
}
