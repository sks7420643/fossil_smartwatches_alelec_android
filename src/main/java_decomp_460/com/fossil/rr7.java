package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rr7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ rr7 f3149a; // = mo7.f2407a.b();
    @DexIgnore
    public static /* final */ b b; // = new b(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends rr7 {
        @DexIgnore
        public static /* final */ a c; // = new a();

        @DexIgnore
        @Override // com.fossil.rr7
        public int b(int i) {
            return rr7.b.b(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends rr7 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.rr7
        public int b(int i) {
            return rr7.f3149a.b(i);
        }

        @DexIgnore
        @Override // com.fossil.rr7
        public int c() {
            return rr7.f3149a.c();
        }
    }

    /*
    static {
        a aVar = a.c;
    }
    */

    @DexIgnore
    public abstract int b(int i);

    @DexIgnore
    public int c() {
        return b(32);
    }
}
