package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c71 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static final a f572a = a.f573a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ /* synthetic */ a f573a; // = new a();

        @DexIgnore
        public final c71 a(s61 s61, int i) {
            pq7.c(s61, "referenceCounter");
            return i > 0 ? new f71(s61, i) : u61.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Bitmap f574a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(Bitmap bitmap, boolean z, int i) {
            pq7.c(bitmap, "bitmap");
            this.f574a = bitmap;
            this.b = z;
            this.c = i;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.f574a;
        }

        @DexIgnore
        public final int b() {
            return this.c;
        }

        @DexIgnore
        public final boolean c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!(pq7.a(this.f574a, bVar.f574a) && this.b == bVar.b && this.c == bVar.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Bitmap bitmap = this.f574a;
            int hashCode = bitmap != null ? bitmap.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (((hashCode * 31) + i) * 31) + this.c;
        }

        @DexIgnore
        public String toString() {
            return "Value(bitmap=" + this.f574a + ", isSampled=" + this.b + ", size=" + this.c + ")";
        }
    }

    @DexIgnore
    void a(int i);

    @DexIgnore
    b b(String str);

    @DexIgnore
    void c(String str, Bitmap bitmap, boolean z);
}
