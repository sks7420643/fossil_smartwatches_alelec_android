package com.fossil;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import com.fossil.tx3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ux3 extends tx3.a {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements TypeEvaluator<e> {
        @DexIgnore
        public static /* final */ TypeEvaluator<e> b; // = new b();

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ e f3656a; // = new e();

        @DexIgnore
        /* renamed from: a */
        public e evaluate(float f, e eVar, e eVar2) {
            this.f3656a.b(lz3.d(eVar.f3659a, eVar2.f3659a, f), lz3.d(eVar.b, eVar2.b, f), lz3.d(eVar.c, eVar2.c, f));
            return this.f3656a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Property<ux3, e> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ Property<ux3, e> f3657a; // = new c("circularReveal");

        @DexIgnore
        public c(String str) {
            super(e.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public e get(ux3 ux3) {
            return ux3.getRevealInfo();
        }

        @DexIgnore
        /* renamed from: b */
        public void set(ux3 ux3, e eVar) {
            ux3.setRevealInfo(eVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends Property<ux3, Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ Property<ux3, Integer> f3658a; // = new d("circularRevealScrimColor");

        @DexIgnore
        public d(String str) {
            super(Integer.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Integer get(ux3 ux3) {
            return Integer.valueOf(ux3.getCircularRevealScrimColor());
        }

        @DexIgnore
        /* renamed from: b */
        public void set(ux3 ux3, Integer num) {
            ux3.setCircularRevealScrimColor(num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public float f3659a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public e(float f, float f2, float f3) {
            this.f3659a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public e(e eVar) {
            this(eVar.f3659a, eVar.b, eVar.c);
        }

        @DexIgnore
        public boolean a() {
            return this.c == Float.MAX_VALUE;
        }

        @DexIgnore
        public void b(float f, float f2, float f3) {
            this.f3659a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public void c(e eVar) {
            b(eVar.f3659a, eVar.b, eVar.c);
        }
    }

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    int getCircularRevealScrimColor();

    @DexIgnore
    e getRevealInfo();

    @DexIgnore
    void setCircularRevealOverlayDrawable(Drawable drawable);

    @DexIgnore
    void setCircularRevealScrimColor(int i);

    @DexIgnore
    void setRevealInfo(e eVar);
}
