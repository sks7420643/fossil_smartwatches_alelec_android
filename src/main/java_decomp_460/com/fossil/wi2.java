package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fossil.pc2;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wi2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wi2> CREATOR; // = new aj2();
    @DexIgnore
    public static /* final */ TimeUnit f; // = TimeUnit.MILLISECONDS;
    @DexIgnore
    public /* final */ zh2 b;
    @DexIgnore
    public /* final */ List<DataSet> c;
    @DexIgnore
    public /* final */ List<DataPoint> d;
    @DexIgnore
    public /* final */ mo2 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public zh2 f3943a;
        @DexIgnore
        public List<DataSet> b; // = new ArrayList();
        @DexIgnore
        public List<DataPoint> c; // = new ArrayList();
        @DexIgnore
        public List<uh2> d; // = new ArrayList();

        @DexIgnore
        public a a(DataSet dataSet) {
            rc2.b(dataSet != null, "Must specify a valid data set.");
            uh2 A = dataSet.A();
            rc2.p(!this.d.contains(A), "Data set for this data source %s is already added.", A);
            rc2.b(!dataSet.k().isEmpty(), "No data points specified in the input data set.");
            this.d.add(A);
            this.b.add(dataSet);
            return this;
        }

        @DexIgnore
        public wi2 b() {
            boolean z = true;
            rc2.o(this.f3943a != null, "Must specify a valid session.");
            if (this.f3943a.f(TimeUnit.MILLISECONDS) == 0) {
                z = false;
            }
            rc2.o(z, "Must specify a valid end time, cannot insert a continuing session.");
            for (DataSet dataSet : this.b) {
                for (DataPoint dataPoint : dataSet.k()) {
                    g(dataPoint);
                }
            }
            for (DataPoint dataPoint2 : this.c) {
                g(dataPoint2);
            }
            return new wi2(this);
        }

        @DexIgnore
        public a c(zh2 zh2) {
            this.f3943a = zh2;
            return this;
        }

        @DexIgnore
        public final void g(DataPoint dataPoint) {
            long A = this.f3943a.A(TimeUnit.NANOSECONDS);
            long f = this.f3943a.f(TimeUnit.NANOSECONDS);
            long F = dataPoint.F(TimeUnit.NANOSECONDS);
            if (F != 0) {
                long a2 = (F < A || F > f) ? fp2.a(F, TimeUnit.NANOSECONDS, wi2.f) : F;
                rc2.p(a2 >= A && a2 <= f, "Data point %s has time stamp outside session interval [%d, %d]", dataPoint, Long.valueOf(A), Long.valueOf(f));
                if (dataPoint.F(TimeUnit.NANOSECONDS) != a2) {
                    Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", Long.valueOf(dataPoint.F(TimeUnit.NANOSECONDS)), Long.valueOf(a2), wi2.f));
                    dataPoint.p0(a2, TimeUnit.NANOSECONDS);
                }
            }
            long A2 = this.f3943a.A(TimeUnit.NANOSECONDS);
            long f2 = this.f3943a.f(TimeUnit.NANOSECONDS);
            long D = dataPoint.D(TimeUnit.NANOSECONDS);
            long k = dataPoint.k(TimeUnit.NANOSECONDS);
            if (D != 0 && k != 0) {
                if (k > f2) {
                    k = fp2.a(k, TimeUnit.NANOSECONDS, wi2.f);
                }
                rc2.p(D >= A2 && k <= f2, "Data point %s has start and end times outside session interval [%d, %d]", dataPoint, Long.valueOf(A2), Long.valueOf(f2));
                if (k != dataPoint.k(TimeUnit.NANOSECONDS)) {
                    Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", Long.valueOf(dataPoint.k(TimeUnit.NANOSECONDS)), Long.valueOf(k), wi2.f));
                    dataPoint.o0(D, k, TimeUnit.NANOSECONDS);
                }
            }
        }
    }

    @DexIgnore
    public wi2(a aVar) {
        this(aVar.f3943a, aVar.b, aVar.c, (mo2) null);
    }

    @DexIgnore
    public wi2(wi2 wi2, mo2 mo2) {
        this(wi2.b, wi2.c, wi2.d, mo2);
    }

    @DexIgnore
    public wi2(zh2 zh2, List<DataSet> list, List<DataPoint> list2, IBinder iBinder) {
        this.b = zh2;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
        this.e = oo2.e(iBinder);
    }

    @DexIgnore
    public wi2(zh2 zh2, List<DataSet> list, List<DataPoint> list2, mo2 mo2) {
        this.b = zh2;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
        this.e = mo2;
    }

    @DexIgnore
    public List<DataPoint> c() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof wi2)) {
                return false;
            }
            wi2 wi2 = (wi2) obj;
            if (!(pc2.a(this.b, wi2.b) && pc2.a(this.c, wi2.c) && pc2.a(this.d, wi2.d))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public List<DataSet> f() {
        return this.c;
    }

    @DexIgnore
    public zh2 h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return pc2.b(this.b, this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        pc2.a c2 = pc2.c(this);
        c2.a(Constants.SESSION, this.b);
        c2.a("dataSets", this.c);
        c2.a("aggregateDataPoints", this.d);
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, h(), i, false);
        bd2.y(parcel, 2, f(), false);
        bd2.y(parcel, 3, c(), false);
        mo2 mo2 = this.e;
        bd2.m(parcel, 4, mo2 == null ? null : mo2.asBinder(), false);
        bd2.b(parcel, a2);
    }
}
