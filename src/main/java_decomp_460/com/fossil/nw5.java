package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw5 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<cl7<ShineDevice, String>> f2589a; // = new ArrayList();
    @DexIgnore
    public /* final */ wa1 b;
    @DexIgnore
    public a c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void p1(View view, b bVar, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ ImageView d;
        @DexIgnore
        public /* final */ View e;
        @DexIgnore
        public ShineDevice f;
        @DexIgnore
        public /* final */ /* synthetic */ nw5 g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f2590a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(b bVar) {
                this.f2590a = bVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                pq7.c(str, "serial");
                pq7.c(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "set image for serial=" + str + " path " + str2);
                this.f2590a.g.b.t(str2).d(((fj1) new fj1().c0(2131231334)).n()).F0(this.f2590a.d);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nw5$b$b")
        /* renamed from: com.fossil.nw5$b$b  reason: collision with other inner class name */
        public static final class C0176b implements CloudImageHelper.OnImageCallbackListener {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f2591a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public C0176b(b bVar) {
                this.f2591a = bVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                pq7.c(str, "fastPairId");
                pq7.c(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "setImageCallback() for wearOS, fastPairId=" + str);
                this.f2591a.g.b.t(str2).d(((fj1) new fj1().c0(2131231082)).n()).F0(this.f2591a.d);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(nw5 nw5, View view) {
            super(view);
            pq7.c(view, "view");
            this.g = nw5;
            View findViewById = view.findViewById(2131362414);
            pq7.b(findViewById, "view.findViewById(R.id.ftv_device_serial)");
            this.b = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362413);
            pq7.b(findViewById2, "view.findViewById(R.id.ftv_device_name)");
            this.c = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362692);
            pq7.b(findViewById3, "view.findViewById(R.id.iv_device_image)");
            this.d = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(2131363079);
            pq7.b(findViewById4, "view.findViewById(R.id.scan_device_container)");
            this.e = findViewById4;
            findViewById4.setOnClickListener(this);
        }

        @DexIgnore
        public final void b(ShineDevice shineDevice, String str) {
            boolean z = true;
            pq7.c(shineDevice, "shineDevice");
            pq7.c(str, "deviceName");
            this.f = shineDevice;
            this.c.setText(str);
            String serial = shineDevice.getSerial();
            pq7.b(serial, "shineDevice.serial");
            if (serial.length() > 0) {
                FlexibleTextView flexibleTextView = this.b;
                hr7 hr7 = hr7.f1520a;
                Locale locale = Locale.US;
                pq7.b(locale, "Locale.US");
                String c2 = um5.c(PortfolioApp.h0.c(), 2131887221);
                pq7.b(c2, "LanguageHelper.getString\u2026 R.string.Serial_pattern)");
                String format = String.format(locale, c2, Arrays.copyOf(new Object[]{shineDevice.getSerial()}, 1));
                pq7.b(format, "java.lang.String.format(locale, format, *args)");
                flexibleTextView.setText(format);
                this.b.setVisibility(0);
            } else {
                this.b.setVisibility(4);
            }
            String serial2 = shineDevice.getSerial();
            pq7.b(serial2, "shineDevice.serial");
            if (serial2.length() == 0) {
                String fastPairId = shineDevice.getFastPairId();
                pq7.b(fastPairId, "shineDevice.fastPairId");
                if (fastPairId.length() != 0) {
                    z = false;
                }
                if (z) {
                    this.d.setImageDrawable(PortfolioApp.h0.c().getDrawable(2131231082));
                    return;
                }
            }
            c(shineDevice);
        }

        @DexIgnore
        public final void c(ShineDevice shineDevice) {
            String serial = shineDevice.getSerial();
            pq7.b(serial, "shineDevice.serial");
            if (serial.length() > 0) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                String serial2 = shineDevice.getSerial();
                pq7.b(serial2, "shineDevice.serial");
                CloudImageHelper.ItemImage type = with.setSerialNumber(serial2).setSerialPrefix(nk5.o.m(shineDevice.getSerial())).setType(Constants.DeviceType.TYPE_LARGE);
                ImageView imageView = this.d;
                nk5.a aVar = nk5.o;
                String serial3 = shineDevice.getSerial();
                pq7.b(serial3, "shineDevice.serial");
                type.setPlaceHolder(imageView, aVar.i(serial3, nk5.b.LARGE)).setImageCallback(new a(this)).download();
                return;
            }
            CloudImageHelper.ItemImage with2 = CloudImageHelper.Companion.getInstance().with();
            String fastPairId = shineDevice.getFastPairId();
            pq7.b(fastPairId, "shineDevice.fastPairId");
            with2.setFastPairId(fastPairId).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder(this.d, 2131231082).setImageCallback(new C0176b(this)).downloadForWearOS();
        }

        @DexIgnore
        public void onClick(View view) {
            a aVar;
            pq7.c(view, "view");
            if (this.g.getItemCount() > getAdapterPosition() && getAdapterPosition() != -1 && (aVar = this.g.c) != null) {
                aVar.p1(view, this, getAdapterPosition());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<cl7<? extends ShineDevice, ? extends String>> {
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        /* renamed from: a */
        public final int compare(cl7<ShineDevice, String> cl7, cl7<ShineDevice, String> cl72) {
            int i;
            int i2 = -1;
            if (cl7 == null || cl72 == null) {
                int i3 = cl7 == null ? -1 : 1;
                if (cl72 != null) {
                    i2 = 1;
                }
                i = i3 - i2;
            } else {
                i = cl7.getFirst().getRssi() - cl72.getFirst().getRssi();
            }
            return -i;
        }
    }

    @DexIgnore
    public nw5(wa1 wa1, a aVar) {
        pq7.c(wa1, "mRequestManager");
        this.b = wa1;
        this.c = aVar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f2589a.size();
    }

    @DexIgnore
    /* renamed from: i */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "viewHolder");
        bVar.b(this.f2589a.get(i).getFirst(), this.f2589a.get(i).getSecond());
    }

    @DexIgnore
    /* renamed from: j */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558714, viewGroup, false);
        pq7.b(inflate, "v");
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.width = (int) (((float) viewGroup.getMeasuredWidth()) * 0.5f);
        inflate.setLayoutParams(layoutParams);
        return new b(this, inflate);
    }

    @DexIgnore
    public final void k(List<cl7<ShineDevice, String>> list) {
        pq7.c(list, "data");
        this.f2589a.clear();
        lm7.r(list, c.b);
        this.f2589a.addAll(list);
        notifyDataSetChanged();
    }
}
