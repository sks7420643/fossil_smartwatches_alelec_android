package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class rg4 implements Runnable {
    @DexIgnore
    public /* final */ tg4 b;

    @DexIgnore
    public rg4(tg4 tg4) {
        this.b = tg4;
    }

    @DexIgnore
    public static Runnable a(tg4 tg4) {
        return new rg4(tg4);
    }

    @DexIgnore
    public void run() {
        this.b.g(false);
    }
}
