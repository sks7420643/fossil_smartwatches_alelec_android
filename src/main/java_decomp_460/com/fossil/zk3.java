package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk3<V> {
    @DexIgnore
    public static /* final */ Object h; // = new Object();

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4486a;
    @DexIgnore
    public /* final */ xk3<V> b;
    @DexIgnore
    public /* final */ V c;
    @DexIgnore
    public /* final */ V d;
    @DexIgnore
    public /* final */ Object e;
    @DexIgnore
    public volatile V f;
    @DexIgnore
    public volatile V g;

    @DexIgnore
    public zk3(String str, V v, V v2, xk3<V> xk3) {
        this.e = new Object();
        this.f = null;
        this.g = null;
        this.f4486a = str;
        this.c = v;
        this.d = v2;
        this.b = xk3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r2 = com.fossil.xg3.f4117a.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0032, code lost:
        if (r2.hasNext() == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0034, code lost:
        r0 = (com.fossil.zk3) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003e, code lost:
        if (com.fossil.yr3.a() != false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0040, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0043, code lost:
        if (r0.b == null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0045, code lost:
        r1 = r0.b.zza();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0064, code lost:
        throw new java.lang.IllegalStateException("Refreshing flag cache must be done on a worker thread.");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V a(V r5) {
        /*
            r4 = this;
            java.lang.Object r1 = r4.e
            monitor-enter(r1)
            monitor-exit(r1)     // Catch:{ all -> 0x0072 }
            if (r5 == 0) goto L_0x0007
        L_0x0006:
            return r5
        L_0x0007:
            com.fossil.yr3 r0 = com.fossil.al3.f285a
            if (r0 != 0) goto L_0x000e
            V r5 = r4.c
            goto L_0x0006
        L_0x000e:
            java.lang.Object r1 = com.fossil.zk3.h
            monitor-enter(r1)
            boolean r0 = com.fossil.yr3.a()     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x0025
            V r0 = r4.g     // Catch:{ all -> 0x001f }
            if (r0 != 0) goto L_0x0022
            V r5 = r4.c     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            goto L_0x0006
        L_0x001f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            throw r0
        L_0x0022:
            V r5 = r4.g
            goto L_0x001d
        L_0x0025:
            monitor-exit(r1)
            java.util.List r0 = com.fossil.xg3.z0()     // Catch:{ SecurityException -> 0x0055 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ SecurityException -> 0x0055 }
        L_0x002e:
            boolean r0 = r2.hasNext()     // Catch:{ SecurityException -> 0x0055 }
            if (r0 == 0) goto L_0x0056
            java.lang.Object r0 = r2.next()     // Catch:{ SecurityException -> 0x0055 }
            com.fossil.zk3 r0 = (com.fossil.zk3) r0     // Catch:{ SecurityException -> 0x0055 }
            boolean r1 = com.fossil.yr3.a()     // Catch:{ SecurityException -> 0x0055 }
            if (r1 != 0) goto L_0x005d
            r1 = 0
            com.fossil.xk3<V> r3 = r0.b     // Catch:{ IllegalStateException -> 0x0075 }
            if (r3 == 0) goto L_0x004b
            com.fossil.xk3<V> r3 = r0.b     // Catch:{ IllegalStateException -> 0x0075 }
            java.lang.Object r1 = r3.zza()     // Catch:{ IllegalStateException -> 0x0075 }
        L_0x004b:
            java.lang.Object r3 = com.fossil.zk3.h
            monitor-enter(r3)
            r0.g = r1     // Catch:{ all -> 0x0052 }
            monitor-exit(r3)     // Catch:{ all -> 0x0052 }
            goto L_0x002e
        L_0x0052:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0052 }
            throw r0
        L_0x0055:
            r0 = move-exception
        L_0x0056:
            com.fossil.xk3<V> r0 = r4.b
            if (r0 != 0) goto L_0x0065
            V r5 = r4.c
            goto L_0x0006
        L_0x005d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Refreshing flag cache must be done on a worker thread."
            r0.<init>(r1)
            throw r0
        L_0x0065:
            java.lang.Object r5 = r0.zza()     // Catch:{ SecurityException -> 0x006e, IllegalStateException -> 0x006a }
            goto L_0x0006
        L_0x006a:
            r0 = move-exception
            V r5 = r4.c
            goto L_0x0006
        L_0x006e:
            r0 = move-exception
            V r5 = r4.c
            goto L_0x0006
        L_0x0072:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0075:
            r3 = move-exception
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zk3.a(java.lang.Object):java.lang.Object");
    }

    @DexIgnore
    public final String b() {
        return this.f4486a;
    }
}
