package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz6 implements Factory<pz6> {
    @DexIgnore
    public static pz6 a(rz6 rz6) {
        pz6 b = rz6.b();
        lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
