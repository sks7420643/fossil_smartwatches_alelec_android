package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f92 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ d92 f1087a;

    @DexIgnore
    public f92(d92 d92) {
        this.f1087a = d92;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b(c92 c92) {
        c92.b.lock();
        try {
            if (c92.l == this.f1087a) {
                a();
                c92.b.unlock();
            }
        } finally {
            c92.b.unlock();
        }
    }
}
