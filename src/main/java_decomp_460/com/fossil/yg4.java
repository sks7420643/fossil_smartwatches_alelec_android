package com.fossil;

import com.fossil.ng4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yg4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract yg4 a();

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(long j);

        @DexIgnore
        public abstract a d(long j);
    }

    @DexIgnore
    public static a a() {
        return new ng4.b();
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract long d();
}
