package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface r41<TTaskResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(t41<TTaskResult> t41) throws Exception;
}
