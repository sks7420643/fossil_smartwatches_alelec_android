package com.fossil;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ne1<Data> implements af1<Uri, Data> {
    @DexIgnore
    public static /* final */ int c; // = 22;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ AssetManager f2505a;
    @DexIgnore
    public /* final */ a<Data> b;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        wb1<Data> a(AssetManager assetManager, String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements bf1<Uri, ParcelFileDescriptor>, a<ParcelFileDescriptor> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AssetManager f2506a;

        @DexIgnore
        public b(AssetManager assetManager) {
            this.f2506a = assetManager;
        }

        @DexIgnore
        @Override // com.fossil.ne1.a
        public wb1<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new ac1(assetManager, str);
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, ParcelFileDescriptor> b(ef1 ef1) {
            return new ne1(this.f2506a, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements bf1<Uri, InputStream>, a<InputStream> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ AssetManager f2507a;

        @DexIgnore
        public c(AssetManager assetManager) {
            this.f2507a = assetManager;
        }

        @DexIgnore
        @Override // com.fossil.ne1.a
        public wb1<InputStream> a(AssetManager assetManager, String str) {
            return new gc1(assetManager, str);
        }

        @DexIgnore
        @Override // com.fossil.bf1
        public af1<Uri, InputStream> b(ef1 ef1) {
            return new ne1(this.f2507a, this);
        }
    }

    @DexIgnore
    public ne1(AssetManager assetManager, a<Data> aVar) {
        this.f2505a = assetManager;
        this.b = aVar;
    }

    @DexIgnore
    /* renamed from: c */
    public af1.a<Data> b(Uri uri, int i, int i2, ob1 ob1) {
        return new af1.a<>(new yj1(uri), this.b.a(this.f2505a, uri.toString().substring(c)));
    }

    @DexIgnore
    /* renamed from: d */
    public boolean a(Uri uri) {
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }
}
