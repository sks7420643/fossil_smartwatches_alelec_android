package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import com.facebook.internal.Utility;
import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j37 {
    @DexIgnore
    public static final String a(byte[] bArr) {
        pq7.c(bArr, "$this$toBase64String");
        String encodeToString = Base64.encodeToString(bArr, 0);
        pq7.b(encodeToString, "Base64.encodeToString(this, Base64.DEFAULT)");
        return encodeToString;
    }

    @DexIgnore
    public static final byte[] b(File file) {
        pq7.c(file, "$this$toBinary");
        return z58.c(file);
    }

    @DexIgnore
    public static final Bitmap c(byte[] bArr) {
        return BitmapFactory.decodeByteArray(bArr, 0, bArr != null ? bArr.length : 0);
    }

    @DexIgnore
    public static final String d(byte[] bArr) {
        pq7.c(bArr, "$this$toMd5");
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
            instance.update(bArr, 0, bArr.length);
            String bigInteger = new BigInteger(1, instance.digest()).toString(16);
            pq7.b(bigInteger, "BigInteger(1, mdEnc.digest()).toString(16)");
            while (bigInteger.length() < 32) {
                bigInteger = '0' + bigInteger;
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }
}
