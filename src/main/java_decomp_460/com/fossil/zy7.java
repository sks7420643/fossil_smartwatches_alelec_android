package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zy7<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public Object[] f4558a; // = new Object[16];
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public final void a(T t) {
        Object[] objArr = this.f4558a;
        int i = this.c;
        objArr[i] = t;
        int length = (objArr.length - 1) & (i + 1);
        this.c = length;
        if (length == this.b) {
            b();
        }
    }

    @DexIgnore
    public final void b() {
        Object[] objArr = this.f4558a;
        int length = objArr.length;
        Object[] objArr2 = new Object[(length << 1)];
        dm7.j(objArr, objArr2, 0, this.b, 0, 10, null);
        Object[] objArr3 = this.f4558a;
        int length2 = objArr3.length;
        int i = this.b;
        dm7.j(objArr3, objArr2, length2 - i, 0, i, 4, null);
        this.f4558a = objArr2;
        this.b = 0;
        this.c = length;
    }

    @DexIgnore
    public final boolean c() {
        return this.b == this.c;
    }

    @DexIgnore
    public final T d() {
        int i = this.b;
        if (i == this.c) {
            return null;
        }
        Object[] objArr = this.f4558a;
        T t = (T) objArr[i];
        objArr[i] = null;
        this.b = (i + 1) & (objArr.length - 1);
        if (t != null) {
            return t;
        }
        throw new il7("null cannot be cast to non-null type T");
    }
}
