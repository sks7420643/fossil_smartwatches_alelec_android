package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bb7 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f411a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ za7 d;

    @DexIgnore
    public bb7(String str, String str2, String str3, za7 za7) {
        pq7.c(str, "complicationId");
        pq7.c(str2, "data");
        this.f411a = str;
        this.b = str2;
        this.c = str3;
        this.d = za7;
    }

    @DexIgnore
    public final String a() {
        return this.f411a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final za7 c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof bb7) {
                bb7 bb7 = (bb7) obj;
                if (!pq7.a(this.f411a, bb7.f411a) || !pq7.a(this.b, bb7.b) || !pq7.a(this.c, bb7.c) || !pq7.a(this.d, bb7.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f411a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        za7 za7 = this.d;
        if (za7 != null) {
            i = za7.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationWrapper(complicationId=" + this.f411a + ", data=" + this.b + ", setting=" + this.c + ", dimension=" + this.d + ")";
    }
}
