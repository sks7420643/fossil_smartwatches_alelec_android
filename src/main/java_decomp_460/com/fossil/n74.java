package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class n74 implements Runnable {
    @DexIgnore
    public /* final */ Map.Entry b;
    @DexIgnore
    public /* final */ de4 c;

    @DexIgnore
    public n74(Map.Entry entry, de4 de4) {
        this.b = entry;
        this.c = de4;
    }

    @DexIgnore
    public static Runnable a(Map.Entry entry, de4 de4) {
        return new n74(entry, de4);
    }

    @DexIgnore
    public void run() {
        ((ee4) this.b.getKey()).a(this.c);
    }
}
