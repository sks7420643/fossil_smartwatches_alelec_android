package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj0 {
    @DexIgnore
    public static final <T> aj0<T> a(T... tArr) {
        pq7.c(tArr, "values");
        aj0<T> aj0 = new aj0<>(tArr.length);
        for (T t : tArr) {
            aj0.add(t);
        }
        return aj0;
    }
}
