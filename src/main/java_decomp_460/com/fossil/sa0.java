package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa0 extends va0 {
    @DexIgnore
    public static /* final */ ra0 CREATOR; // = new ra0(null);
    @DexIgnore
    public /* final */ y90 c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public /* synthetic */ sa0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = y90.values()[parcel.readInt()];
        this.d = parcel.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort(this.c.b);
        order.put((byte) ((this.c.c.b << 7) | (this.d ? 1 : 0)));
        byte[] array = order.array();
        pq7.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(sa0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            sa0 sa0 = (sa0) obj;
            if (this.c != sa0.c) {
                return false;
            }
            return this.d == sa0.d;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.HIDInstr");
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int hashCode() {
        return (this.c.hashCode() * 31) + Boolean.valueOf(this.d).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.va0, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.X3, ey1.a(this.c)), jd0.Y3, Boolean.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
