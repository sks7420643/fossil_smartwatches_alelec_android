package com.fossil;

import com.fossil.dl7;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yv7<T> extends n08 {
    @DexIgnore
    public int d;

    @DexIgnore
    public yv7(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
    }

    @DexIgnore
    public abstract qn7<T> c();

    @DexIgnore
    public final Throwable d(Object obj) {
        vu7 vu7 = (vu7) (!(obj instanceof vu7) ? null : obj);
        if (vu7 != null) {
            return vu7.f3837a;
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public <T> T h(Object obj) {
        return obj;
    }

    @DexIgnore
    public final void i(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                tk7.a(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            String str = "Fatal exception in coroutines machinery for " + this + ". Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (th != null) {
                fv7.a(c().getContext(), new mv7(str, th));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public abstract Object j();

    @DexIgnore
    public final void run() {
        Object r1;
        Object r12;
        o08 o08 = this.c;
        try {
            qn7<T> c = c();
            if (c != null) {
                vv7 vv7 = (vv7) c;
                qn7<T> qn7 = vv7.i;
                tn7 context = qn7.getContext();
                Object j = j();
                Object c2 = zz7.c(context, vv7.g);
                try {
                    Throwable d2 = d(j);
                    xw7 xw7 = zv7.b(this.d) ? (xw7) context.get(xw7.r) : null;
                    if (d2 == null && xw7 != null && !xw7.isActive()) {
                        CancellationException k = xw7.k();
                        a(j, k);
                        dl7.a aVar = dl7.Companion;
                        qn7.resumeWith(dl7.m1constructorimpl(el7.a(nv7.d() ? !(qn7 instanceof do7) ? k : uz7.j(k, (do7) qn7) : k)));
                    } else if (d2 != null) {
                        dl7.a aVar2 = dl7.Companion;
                        qn7.resumeWith(dl7.m1constructorimpl(el7.a(d2)));
                    } else {
                        T h = h(j);
                        dl7.a aVar3 = dl7.Companion;
                        qn7.resumeWith(dl7.m1constructorimpl(h));
                    }
                    tl7 tl7 = tl7.f3441a;
                    try {
                        dl7.a aVar4 = dl7.Companion;
                        o08.h();
                        r12 = dl7.m1constructorimpl(tl7.f3441a);
                    } catch (Throwable th) {
                        dl7.a aVar5 = dl7.Companion;
                        r12 = dl7.m1constructorimpl(el7.a(th));
                    }
                    i(null, dl7.m4exceptionOrNullimpl(r12));
                    return;
                } finally {
                    zz7.a(context, c2);
                }
            } else {
                throw new il7("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
            }
        } catch (Throwable th2) {
            dl7.a aVar6 = dl7.Companion;
            r1 = dl7.m1constructorimpl(el7.a(th2));
        }
        i(th, dl7.m4exceptionOrNullimpl(r1));
    }
}
