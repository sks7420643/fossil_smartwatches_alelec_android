package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.fossil.vg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc2 extends vg2<oc2> {
    @DexIgnore
    public static /* final */ vc2 c; // = new vc2();

    @DexIgnore
    public vc2() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    @DexIgnore
    public static View c(Context context, int i, int i2) throws vg2.a {
        return c.e(context, i, i2);
    }

    @DexIgnore
    /* renamed from: d */
    public final oc2 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        return queryLocalInterface instanceof oc2 ? (oc2) queryLocalInterface : new vd2(iBinder);
    }

    @DexIgnore
    public final View e(Context context, int i, int i2) throws vg2.a {
        try {
            uc2 uc2 = new uc2(i, i2, null);
            return (View) tg2.i(((oc2) b(context)).P0(tg2.n(context), uc2));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new vg2.a(sb.toString(), e);
        }
    }
}
