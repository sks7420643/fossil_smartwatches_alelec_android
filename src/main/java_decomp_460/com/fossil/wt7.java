package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wt7 extends vt7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 implements vp7<CharSequence, Integer, cl7<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ char[] $delimiters;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(char[] cArr, boolean z) {
            super(2);
            this.$delimiters = cArr;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final cl7<Integer, Integer> invoke(CharSequence charSequence, int i) {
            pq7.c(charSequence, "$receiver");
            int H = wt7.H(charSequence, this.$delimiters, i, this.$ignoreCase);
            if (H < 0) {
                return null;
            }
            return hl7.a(Integer.valueOf(H), 1);
        }

        @DexIgnore
        @Override // com.fossil.vp7
        public /* bridge */ /* synthetic */ cl7<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
            return invoke(charSequence, num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qq7 implements vp7<CharSequence, Integer, cl7<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ List $delimitersList;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(List list, boolean z) {
            super(2);
            this.$delimitersList = list;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final cl7<Integer, Integer> invoke(CharSequence charSequence, int i) {
            pq7.c(charSequence, "$receiver");
            cl7 y = wt7.y(charSequence, this.$delimitersList, i, this.$ignoreCase, false);
            if (y != null) {
                return hl7.a(y.getFirst(), Integer.valueOf(((String) y.getSecond()).length()));
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.vp7
        public /* bridge */ /* synthetic */ cl7<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
            return invoke(charSequence, num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<wr7, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        public final String invoke(wr7 wr7) {
            pq7.c(wr7, "it");
            return wt7.f0(this.$this_splitToSequence, wr7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements rp7<wr7, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        public final String invoke(wr7 wr7) {
            pq7.c(wr7, "it");
            return wt7.f0(this.$this_splitToSequence, wr7);
        }
    }

    @DexIgnore
    public static final int A(CharSequence charSequence) {
        pq7.c(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    @DexIgnore
    public static final int B(CharSequence charSequence, char c2, int i, boolean z) {
        pq7.c(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c2, i);
        }
        return H(charSequence, new char[]{c2}, i, z);
    }

    @DexIgnore
    public static final int C(CharSequence charSequence, String str, int i, boolean z) {
        pq7.c(charSequence, "$this$indexOf");
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        return (z || !(charSequence instanceof String)) ? E(charSequence, str, i, charSequence.length(), z, false, 16, null) : ((String) charSequence).indexOf(str, i);
    }

    @DexIgnore
    public static final int D(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        ur7 wr7 = !z2 ? new wr7(bs7.d(i, 0), bs7.g(i2, charSequence.length())) : bs7.k(bs7.g(i, A(charSequence)), bs7.d(i2, 0));
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a2 = wr7.a();
            int b2 = wr7.b();
            int c2 = wr7.c();
            if (c2 < 0 ? a2 >= b2 : a2 <= b2) {
                while (!T(charSequence2, 0, charSequence, a2, charSequence2.length(), z)) {
                    if (a2 != b2) {
                        a2 += c2;
                    }
                }
                return a2;
            }
        } else {
            int a3 = wr7.a();
            int b3 = wr7.b();
            int c3 = wr7.c();
            if (c3 < 0 ? a3 >= b3 : a3 <= b3) {
                while (!vt7.m((String) charSequence2, 0, (String) charSequence, a3, charSequence2.length(), z)) {
                    if (a3 != b3) {
                        a3 += c3;
                    }
                }
                return a3;
            }
        }
        return -1;
    }

    @DexIgnore
    public static /* synthetic */ int E(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        return D(charSequence, charSequence2, i, i2, z, (i3 & 16) != 0 ? false : z2);
    }

    @DexIgnore
    public static /* synthetic */ int F(CharSequence charSequence, char c2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return B(charSequence, c2, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int G(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return C(charSequence, str, i, z);
    }

    @DexIgnore
    public static final int H(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        pq7.c(charSequence, "$this$indexOfAny");
        pq7.c(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int d2 = bs7.d(i, 0);
            int A = A(charSequence);
            if (d2 <= A) {
                while (true) {
                    char charAt = charSequence.charAt(d2);
                    int length = cArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            z2 = false;
                            break;
                        } else if (dt7.d(cArr[i2], charAt, z)) {
                            z2 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (!z2) {
                        if (d2 == A) {
                            break;
                        }
                        d2++;
                    } else {
                        return d2;
                    }
                }
            }
            return -1;
        }
        return ((String) charSequence).indexOf(em7.W(cArr), i);
    }

    @DexIgnore
    public static final int I(CharSequence charSequence, char c2, int i, boolean z) {
        pq7.c(charSequence, "$this$lastIndexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c2, i);
        }
        return M(charSequence, new char[]{c2}, i, z);
    }

    @DexIgnore
    public static final int J(CharSequence charSequence, String str, int i, boolean z) {
        pq7.c(charSequence, "$this$lastIndexOf");
        pq7.c(str, LegacyTokenHelper.TYPE_STRING);
        return (z || !(charSequence instanceof String)) ? D(charSequence, str, i, 0, z, true) : ((String) charSequence).lastIndexOf(str, i);
    }

    @DexIgnore
    public static /* synthetic */ int K(CharSequence charSequence, char c2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = A(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return I(charSequence, c2, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int L(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = A(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return J(charSequence, str, i, z);
    }

    @DexIgnore
    public static final int M(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        pq7.c(charSequence, "$this$lastIndexOfAny");
        pq7.c(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            for (int g = bs7.g(i, A(charSequence)); g >= 0; g--) {
                char charAt = charSequence.charAt(g);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z2 = false;
                        break;
                    } else if (dt7.d(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return g;
                }
            }
            return -1;
        }
        return ((String) charSequence).lastIndexOf(em7.W(cArr), i);
    }

    @DexIgnore
    public static final CharSequence N(CharSequence charSequence, int i, char c2) {
        int i2 = 1;
        pq7.c(charSequence, "$this$padStart");
        if (i < 0) {
            throw new IllegalArgumentException("Desired length " + i + " is less than zero.");
        } else if (i <= charSequence.length()) {
            return charSequence.subSequence(0, charSequence.length());
        } else {
            StringBuilder sb = new StringBuilder(i);
            int length = i - charSequence.length();
            if (1 <= length) {
                while (true) {
                    sb.append(c2);
                    if (i2 == length) {
                        break;
                    }
                    i2++;
                }
            }
            sb.append(charSequence);
            return sb;
        }
    }

    @DexIgnore
    public static final String O(String str, int i, char c2) {
        pq7.c(str, "$this$padStart");
        return N(str, i, c2).toString();
    }

    @DexIgnore
    public static final ts7<wr7> P(CharSequence charSequence, char[] cArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new ft7(charSequence, i, i2, new a(cArr, z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static final ts7<wr7> Q(CharSequence charSequence, String[] strArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new ft7(charSequence, i, i2, new b(dm7.d(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ ts7 R(CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return P(charSequence, cArr, i, z, i2);
    }

    @DexIgnore
    public static /* synthetic */ ts7 S(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return Q(charSequence, strArr, i, z, i2);
    }

    @DexIgnore
    public static final boolean T(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        pq7.c(charSequence, "$this$regionMatchesImpl");
        pq7.c(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!dt7.d(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static final String U(String str, CharSequence charSequence) {
        pq7.c(str, "$this$removePrefix");
        pq7.c(charSequence, "prefix");
        if (!e0(str, charSequence, false, 2, null)) {
            return str;
        }
        String substring = str.substring(charSequence.length());
        pq7.b(substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    @DexIgnore
    public static final String V(String str, CharSequence charSequence, CharSequence charSequence2) {
        pq7.c(str, "$this$removeSurrounding");
        pq7.c(charSequence, "prefix");
        pq7.c(charSequence2, "suffix");
        if (str.length() < charSequence.length() + charSequence2.length() || !e0(str, charSequence, false, 2, null) || !x(str, charSequence2, false, 2, null)) {
            return str;
        }
        String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final List<String> W(CharSequence charSequence, String[] strArr, boolean z, int i) {
        boolean z2 = true;
        pq7.c(charSequence, "$this$split");
        pq7.c(strArr, "delimiters");
        if (strArr.length == 1) {
            String str = strArr[0];
            if (str.length() != 0) {
                z2 = false;
            }
            if (!z2) {
                return X(charSequence, str, z, i);
            }
        }
        Iterable<wr7> e = at7.e(S(charSequence, strArr, 0, z, i, 2, null));
        ArrayList arrayList = new ArrayList(im7.m(e, 10));
        for (wr7 wr7 : e) {
            arrayList.add(f0(charSequence, wr7));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<String> X(CharSequence charSequence, String str, boolean z, int i) {
        int length;
        int i2 = 10;
        int i3 = 0;
        if (i >= 0) {
            int C = C(charSequence, str, 0, z);
            if (C == -1 || i == 1) {
                return gm7.b(charSequence.toString());
            }
            boolean z2 = i > 0;
            if (z2) {
                i2 = bs7.g(i, 10);
            }
            ArrayList arrayList = new ArrayList(i2);
            int i4 = C;
            while (true) {
                arrayList.add(charSequence.subSequence(i3, i4).toString());
                length = str.length() + i4;
                if ((!z2 || arrayList.size() != i - 1) && (i4 = C(charSequence, str, length, z)) != -1) {
                    i3 = length;
                }
            }
            arrayList.add(charSequence.subSequence(length, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ List Y(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return W(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final ts7<String> Z(CharSequence charSequence, char[] cArr, boolean z, int i) {
        pq7.c(charSequence, "$this$splitToSequence");
        pq7.c(cArr, "delimiters");
        return at7.o(R(charSequence, cArr, 0, z, i, 2, null), new d(charSequence));
    }

    @DexIgnore
    public static final ts7<String> a0(CharSequence charSequence, String[] strArr, boolean z, int i) {
        pq7.c(charSequence, "$this$splitToSequence");
        pq7.c(strArr, "delimiters");
        return at7.o(S(charSequence, strArr, 0, z, i, 2, null), new c(charSequence));
    }

    @DexIgnore
    public static /* synthetic */ ts7 b0(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return Z(charSequence, cArr, z, i);
    }

    @DexIgnore
    public static /* synthetic */ ts7 c0(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a0(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final boolean d0(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        pq7.c(charSequence, "$this$startsWith");
        pq7.c(charSequence2, "prefix");
        return (z || !(charSequence instanceof String) || !(charSequence2 instanceof String)) ? T(charSequence, 0, charSequence2, 0, charSequence2.length(), z) : vt7.s((String) charSequence, (String) charSequence2, false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ boolean e0(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return d0(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final String f0(CharSequence charSequence, wr7 wr7) {
        pq7.c(charSequence, "$this$substring");
        pq7.c(wr7, "range");
        return charSequence.subSequence(wr7.h().intValue(), wr7.g().intValue() + 1).toString();
    }

    @DexIgnore
    public static final String g0(String str, char c2, String str2) {
        pq7.c(str, "$this$substringAfter");
        pq7.c(str2, "missingDelimiterValue");
        int F = F(str, c2, 0, false, 6, null);
        if (F == -1) {
            return str2;
        }
        String substring = str.substring(F + 1, str.length());
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String h0(String str, String str2, String str3) {
        pq7.c(str, "$this$substringAfter");
        pq7.c(str2, "delimiter");
        pq7.c(str3, "missingDelimiterValue");
        int G = G(str, str2, 0, false, 6, null);
        if (G == -1) {
            return str3;
        }
        String substring = str.substring(G + str2.length(), str.length());
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String i0(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return g0(str, c2, str2);
    }

    @DexIgnore
    public static /* synthetic */ String j0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return h0(str, str2, str3);
    }

    @DexIgnore
    public static final String k0(String str, char c2, String str2) {
        pq7.c(str, "$this$substringAfterLast");
        pq7.c(str2, "missingDelimiterValue");
        int K = K(str, c2, 0, false, 6, null);
        if (K == -1) {
            return str2;
        }
        String substring = str.substring(K + 1, str.length());
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String l0(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return k0(str, c2, str2);
    }

    @DexIgnore
    public static final String m0(String str, char c2, String str2) {
        pq7.c(str, "$this$substringBefore");
        pq7.c(str2, "missingDelimiterValue");
        int F = F(str, c2, 0, false, 6, null);
        if (F == -1) {
            return str2;
        }
        String substring = str.substring(0, F);
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String n0(String str, String str2, String str3) {
        pq7.c(str, "$this$substringBefore");
        pq7.c(str2, "delimiter");
        pq7.c(str3, "missingDelimiterValue");
        int G = G(str, str2, 0, false, 6, null);
        if (G == -1) {
            return str3;
        }
        String substring = str.substring(0, G);
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String o0(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return m0(str, c2, str2);
    }

    @DexIgnore
    public static /* synthetic */ String p0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return n0(str, str2, str3);
    }

    @DexIgnore
    public static final String q0(String str, char c2, String str2) {
        pq7.c(str, "$this$substringBeforeLast");
        pq7.c(str2, "missingDelimiterValue");
        int K = K(str, c2, 0, false, 6, null);
        if (K == -1) {
            return str2;
        }
        String substring = str.substring(0, K);
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String r0(String str, String str2, String str3) {
        pq7.c(str, "$this$substringBeforeLast");
        pq7.c(str2, "delimiter");
        pq7.c(str3, "missingDelimiterValue");
        int L = L(str, str2, 0, false, 6, null);
        if (L == -1) {
            return str3;
        }
        String substring = str.substring(0, L);
        pq7.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String s0(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return q0(str, c2, str2);
    }

    @DexIgnore
    public static /* synthetic */ String t0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return r0(str, str2, str3);
    }

    @DexIgnore
    public static final boolean u(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        pq7.c(charSequence, "$this$contains");
        pq7.c(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (charSequence2 instanceof String) {
            if (G(charSequence, (String) charSequence2, 0, z, 2, null) >= 0) {
                return true;
            }
        } else if (E(charSequence, charSequence2, 0, charSequence.length(), z, false, 16, null) >= 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static final CharSequence u0(CharSequence charSequence) {
        pq7.c(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        boolean z = false;
        int i = 0;
        while (i <= length) {
            boolean c2 = ct7.c(charSequence.charAt(!z ? i : length));
            if (!z) {
                if (!c2) {
                    z = true;
                } else {
                    i++;
                }
            } else if (!c2) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    @DexIgnore
    public static /* synthetic */ boolean v(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return u(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final String v0(String str, char... cArr) {
        CharSequence charSequence;
        pq7.c(str, "$this$trimEnd");
        pq7.c(cArr, "chars");
        int length = str.length();
        while (true) {
            length--;
            if (length >= 0) {
                if (!em7.y(cArr, str.charAt(length))) {
                    charSequence = str.subSequence(0, length + 1);
                    break;
                }
            } else {
                charSequence = "";
                break;
            }
        }
        return charSequence.toString();
    }

    @DexIgnore
    public static final boolean w(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        pq7.c(charSequence, "$this$endsWith");
        pq7.c(charSequence2, "suffix");
        return (z || !(charSequence instanceof String) || !(charSequence2 instanceof String)) ? T(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z) : vt7.i((String) charSequence, (String) charSequence2, false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ boolean x(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return w(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final cl7<Integer, String> y(CharSequence charSequence, Collection<String> collection, int i, boolean z, boolean z2) {
        T t;
        T t2;
        if (z || collection.size() != 1) {
            ur7 wr7 = !z2 ? new wr7(bs7.d(i, 0), charSequence.length()) : bs7.k(bs7.g(i, A(charSequence)), 0);
            if (charSequence instanceof String) {
                int a2 = wr7.a();
                int b2 = wr7.b();
                int c2 = wr7.c();
                if (c2 < 0 ? a2 >= b2 : a2 <= b2) {
                    while (true) {
                        Iterator<T> it = collection.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t2 = null;
                                break;
                            }
                            T next = it.next();
                            T t3 = next;
                            if (vt7.m(t3, 0, (String) charSequence, a2, t3.length(), z)) {
                                t2 = next;
                                break;
                            }
                        }
                        T t4 = t2;
                        if (t4 == null) {
                            if (a2 == b2) {
                                break;
                            }
                            a2 += c2;
                        } else {
                            return hl7.a(Integer.valueOf(a2), t4);
                        }
                    }
                }
            } else {
                int a3 = wr7.a();
                int b3 = wr7.b();
                int c3 = wr7.c();
                if (c3 < 0 ? a3 >= b3 : a3 <= b3) {
                    while (true) {
                        Iterator<T> it2 = collection.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            T next2 = it2.next();
                            T t5 = next2;
                            if (T(t5, 0, charSequence, a3, t5.length(), z)) {
                                t = next2;
                                break;
                            }
                        }
                        T t6 = t;
                        if (t6 == null) {
                            if (a3 == b3) {
                                break;
                            }
                            a3 += c3;
                        } else {
                            return hl7.a(Integer.valueOf(a3), t6);
                        }
                    }
                }
            }
            return null;
        }
        String str = (String) pm7.Y(collection);
        int G = !z2 ? G(charSequence, str, i, false, 4, null) : L(charSequence, str, i, false, 4, null);
        if (G < 0) {
            return null;
        }
        return hl7.a(Integer.valueOf(G), str);
    }

    @DexIgnore
    public static final wr7 z(CharSequence charSequence) {
        pq7.c(charSequence, "$this$indices");
        return new wr7(0, charSequence.length() - 1);
    }
}
