package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rg7 extends ng7 {
    @DexIgnore
    public Long m; // = null;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;

    @DexIgnore
    public rg7(Context context, String str, String str2, int i, Long l, jg7 jg7) {
        super(context, i, jg7);
        this.o = str;
        this.n = str2;
        this.m = l;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public og7 a() {
        return og7.f2678a;
    }

    @DexIgnore
    @Override // com.fossil.ng7
    public boolean b(JSONObject jSONObject) {
        ji7.d(jSONObject, "pi", this.n);
        ji7.d(jSONObject, "rf", this.o);
        Long l = this.m;
        if (l == null) {
            return true;
        }
        jSONObject.put("du", l);
        return true;
    }
}
