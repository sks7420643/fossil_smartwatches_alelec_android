package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nz0 {
    @DexIgnore
    public static Method b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Field d;
    @DexIgnore
    public static boolean e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float[] f2596a;

    @DexIgnore
    public void a(View view) {
        if (view.getVisibility() == 0) {
            view.setTag(ny0.save_non_transition_alpha, null);
        }
    }

    @DexIgnore
    @SuppressLint({"PrivateApi"})
    public final void b() {
        if (!c) {
            try {
                Method declaredMethod = View.class.getDeclaredMethod("setFrame", Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE);
                b = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewUtilsBase", "Failed to retrieve setFrame method", e2);
            }
            c = true;
        }
    }

    @DexIgnore
    public float c(View view) {
        Float f = (Float) view.getTag(ny0.save_non_transition_alpha);
        return f != null ? view.getAlpha() / f.floatValue() : view.getAlpha();
    }

    @DexIgnore
    public void d(View view) {
        if (view.getTag(ny0.save_non_transition_alpha) == null) {
            view.setTag(ny0.save_non_transition_alpha, Float.valueOf(view.getAlpha()));
        }
    }

    @DexIgnore
    public void e(View view, Matrix matrix) {
        if (matrix == null || matrix.isIdentity()) {
            view.setPivotX((float) (view.getWidth() / 2));
            view.setPivotY((float) (view.getHeight() / 2));
            view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setRotation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float[] fArr = this.f2596a;
        if (fArr == null) {
            fArr = new float[9];
            this.f2596a = fArr;
        }
        matrix.getValues(fArr);
        float f = fArr[3];
        float sqrt = ((float) (fArr[0] < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? -1 : 1)) * ((float) Math.sqrt((double) (1.0f - (f * f))));
        float f2 = fArr[0] / sqrt;
        float f3 = fArr[4] / sqrt;
        float f4 = fArr[2];
        float f5 = fArr[5];
        view.setPivotX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setPivotY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationX(f4);
        view.setTranslationY(f5);
        view.setRotation((float) Math.toDegrees(Math.atan2((double) f, (double) sqrt)));
        view.setScaleX(f2);
        view.setScaleY(f3);
    }

    @DexIgnore
    public void f(View view, int i, int i2, int i3, int i4) {
        b();
        Method method = b;
        if (method != null) {
            try {
                method.invoke(view, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
            } catch (IllegalAccessException e2) {
            } catch (InvocationTargetException e3) {
                throw new RuntimeException(e3.getCause());
            }
        }
    }

    @DexIgnore
    public void g(View view, float f) {
        Float f2 = (Float) view.getTag(ny0.save_non_transition_alpha);
        if (f2 != null) {
            view.setAlpha(f2.floatValue() * f);
        } else {
            view.setAlpha(f);
        }
    }

    @DexIgnore
    public void h(View view, int i) {
        if (!e) {
            try {
                Field declaredField = View.class.getDeclaredField("mViewFlags");
                d = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.i("ViewUtilsBase", "fetchViewFlagsField: ");
            }
            e = true;
        }
        Field field = d;
        if (field != null) {
            try {
                d.setInt(view, (field.getInt(view) & -13) | i);
            } catch (IllegalAccessException e3) {
            }
        }
    }

    @DexIgnore
    public void i(View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            i(view2, matrix);
            matrix.preTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        }
        matrix.preTranslate((float) view.getLeft(), (float) view.getTop());
        Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            matrix.preConcat(matrix2);
        }
    }

    @DexIgnore
    public void j(View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            j(view2, matrix);
            matrix.postTranslate((float) view2.getScrollX(), (float) view2.getScrollY());
        }
        matrix.postTranslate((float) (-view.getLeft()), (float) (-view.getTop()));
        Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            Matrix matrix3 = new Matrix();
            if (matrix2.invert(matrix3)) {
                matrix.postConcat(matrix3);
            }
        }
    }
}
