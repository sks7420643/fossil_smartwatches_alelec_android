package com.fossil;

import android.os.Parcelable;
import com.fossil.sx1;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v9 extends vx1<el1[], el1[]> {
    @DexIgnore
    public static /* final */ qx1<el1[]>[] b; // = {new d9(), new g9(), new j9(new ry1(3, 0))};
    @DexIgnore
    public static /* final */ rx1<el1[]>[] c; // = {new m9(ob.ALARM.c), new p9(ob.ALARM.c), new s9(ob.ALARM.c, (byte) 255, new ry1(3, 0))};
    @DexIgnore
    public static /* final */ v9 d; // = new v9();

    @DexIgnore
    @Override // com.fossil.vx1
    public qx1<el1[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.vx1
    public rx1<el1[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] h(el1[] el1Arr) {
        gl1 gl1;
        ByteBuffer allocate = ByteBuffer.allocate(el1Arr.length * 3);
        for (el1 el1 : el1Arr) {
            gl1[] b2 = el1.b();
            int length = b2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    gl1 = null;
                    break;
                }
                gl1 = b2[i];
                if (gl1 instanceof hl1) {
                    break;
                }
                i++;
            }
            if (gl1 != null) {
                allocate.put(gl1.b());
            }
        }
        byte[] array = allocate.array();
        pq7.b(array, "entryDataBuffer.array()");
        return array;
    }

    @DexIgnore
    public final el1[] j(byte[] bArr) throws sx1 {
        byte[] a2 = vx1.f3851a.a(bArr);
        if (a2.length % 3 == 0) {
            ArrayList arrayList = new ArrayList();
            ur7 l = bs7.l(bs7.m(0, a2.length), 3);
            int a3 = l.a();
            int b2 = l.b();
            int c2 = l.c();
            if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
                while (true) {
                    hl1 a4 = hl1.CREATOR.a(dm7.k(a2, a3, a3 + 3));
                    Parcelable jl1 = a4 instanceof kl1 ? new jl1((kl1) a4, null, null, 6, null) : a4 instanceof ol1 ? new nl1((ol1) a4, null, null, 6, null) : null;
                    if (jl1 != null) {
                        arrayList.add(jl1);
                    }
                    if (a3 == b2) {
                        break;
                    }
                    a3 += c2;
                }
            }
            Object[] array = arrayList.toArray(new el1[0]);
            if (array != null) {
                return (el1[]) array;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new sx1(sx1.a.INVALID_FILE_DATA, e.b(e.e("Size("), a2.length, ") not divide to 3."), null, 4, null);
    }

    @DexIgnore
    public final byte[] k(el1[] el1Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (el1 el1 : el1Arr) {
            byteArrayOutputStream.write(el1.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        pq7.b(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final el1[] l(byte[] bArr) throws sx1 {
        try {
            return el1.CREATOR.a(vx1.f3851a.a(bArr));
        } catch (IllegalArgumentException e) {
            throw new sx1(sx1.a.INVALID_FILE_DATA, e.getLocalizedMessage(), e);
        }
    }
}
