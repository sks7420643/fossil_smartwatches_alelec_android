package com.fossil;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ld6 extends rv5<kd6> {
    @DexIgnore
    void A(boolean z);

    @DexIgnore
    Object G0();  // void declaration

    @DexIgnore
    void J1(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z);

    @DexIgnore
    Object Q5();  // void declaration

    @DexIgnore
    void R1(String str, String str2);

    @DexIgnore
    Object U0();  // void declaration

    @DexIgnore
    void Z1(boolean z);

    @DexIgnore
    void g5(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void o0(int i);

    @DexIgnore
    void q1(boolean z, boolean z2, boolean z3);

    @DexIgnore
    Object q6();  // void declaration

    @DexIgnore
    void r2(Date date);

    @DexIgnore
    Object t1();  // void declaration

    @DexIgnore
    Object t6();  // void declaration

    @DexIgnore
    void x3(boolean z);
}
