package com.fossil;

import com.fossil.xa1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xa1<CHILD extends xa1<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    @DexIgnore
    public uj1<? super TranscodeType> b; // = sj1.b();

    @DexIgnore
    /* renamed from: d */
    public final CHILD clone() {
        try {
            return (CHILD) ((xa1) super.clone());
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @DexIgnore
    public final uj1<? super TranscodeType> e() {
        return this.b;
    }
}
