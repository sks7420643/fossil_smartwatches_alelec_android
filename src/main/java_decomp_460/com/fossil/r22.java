package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.j32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r22 implements j32.b {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ long f3069a;
    @DexIgnore
    public /* final */ h02 b;

    @DexIgnore
    public r22(long j, h02 h02) {
        this.f3069a = j;
        this.b = h02;
    }

    @DexIgnore
    public static j32.b a(long j, h02 h02) {
        return new r22(j, h02);
    }

    @DexIgnore
    @Override // com.fossil.j32.b
    public Object apply(Object obj) {
        return j32.g0(this.f3069a, this.b, (SQLiteDatabase) obj);
    }
}
