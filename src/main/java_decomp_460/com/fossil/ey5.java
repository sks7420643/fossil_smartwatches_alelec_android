package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey5 implements Factory<cy5> {
    @DexIgnore
    public static cy5 a(dy5 dy5) {
        cy5 a2 = dy5.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
