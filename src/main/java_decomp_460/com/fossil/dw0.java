package com.fossil;

import android.os.Build;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dw0 extends cw0 {
    @DexIgnore
    public tv0 e;

    @DexIgnore
    public dw0(long j, RenderScript renderScript) {
        super(j, renderScript);
    }

    @DexIgnore
    public static dw0 k(RenderScript renderScript, vv0 vv0) {
        if (vv0.q(vv0.k(renderScript)) || vv0.q(vv0.j(renderScript))) {
            boolean z = renderScript.i() && Build.VERSION.SDK_INT < 19;
            dw0 dw0 = new dw0(renderScript.C(5, vv0.c(renderScript), z), renderScript);
            dw0.h(z);
            dw0.n(5.0f);
            return dw0;
        }
        throw new yv0("Unsupported element type.");
    }

    @DexIgnore
    public void l(tv0 tv0) {
        if (tv0.l().k() != 0) {
            f(0, null, tv0, null);
            return;
        }
        throw new yv0("Output is a 1D Allocation");
    }

    @DexIgnore
    public void m(tv0 tv0) {
        if (tv0.l().k() != 0) {
            this.e = tv0;
            j(1, tv0);
            return;
        }
        throw new yv0("Input set to a 1D Allocation");
    }

    @DexIgnore
    public void n(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 25.0f) {
            throw new yv0("Radius out of range (0 < r <= 25).");
        }
        i(0, f);
    }
}
