package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w23 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ u23 f3869a; // = c();
    @DexIgnore
    public static /* final */ u23 b; // = new x23();

    @DexIgnore
    public static u23 a() {
        return f3869a;
    }

    @DexIgnore
    public static u23 b() {
        return b;
    }

    @DexIgnore
    public static u23 c() {
        try {
            return (u23) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
