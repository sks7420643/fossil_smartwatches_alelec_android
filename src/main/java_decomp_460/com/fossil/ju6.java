package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.hq4;
import com.fossil.hu6;
import com.fossil.mu6;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju6 extends qv5 implements View.OnClickListener, t47.g, hu6.a {
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public g37<rc5> h;
    @DexIgnore
    public hu6 i;
    @DexIgnore
    public mu6 j;
    @DexIgnore
    public po4 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ju6 a() {
            return new ju6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<mu6.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ju6 f1809a;

        @DexIgnore
        public b(ju6 ju6) {
            this.f1809a = ju6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mu6.a aVar) {
            FragmentActivity activity = this.f1809a.getActivity();
            if (activity != null) {
                if (aVar != null) {
                    int i = ku6.f2098a[aVar.ordinal()];
                    if (i == 1) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "Data is changed, wait for user confirm to save");
                        s37 s37 = s37.c;
                        FragmentManager childFragmentManager = this.f1809a.getChildFragmentManager();
                        pq7.b(childFragmentManager, "childFragmentManager");
                        s37.y0(childFragmentManager);
                        return;
                    } else if (i == 2 || i == 3) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "close view");
                        activity.finish();
                        return;
                    } else if (i == 4) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "FAIL_DEVICE_DISCONNECT");
                        s37 s372 = s37.c;
                        FragmentManager childFragmentManager2 = this.f1809a.getChildFragmentManager();
                        pq7.b(childFragmentManager2, "childFragmentManager");
                        s372.y(childFragmentManager2);
                        return;
                    }
                }
                FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "unknown error");
                String J = PortfolioApp.h0.c().J();
                TroubleshootingActivity.a aVar2 = TroubleshootingActivity.B;
                Context requireContext = this.f1809a.requireContext();
                pq7.b(requireContext, "requireContext()");
                TroubleshootingActivity.a.c(aVar2, requireContext, J, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<hq4.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ju6 f1810a;

        @DexIgnore
        public c(ju6 ju6) {
            this.f1810a = ju6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.c cVar) {
            if (!cVar.a().isEmpty()) {
                ju6 ju6 = this.f1810a;
                Object[] array = cVar.a().toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    ju6.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<hq4.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ju6 f1811a;

        @DexIgnore
        public d(ju6 ju6) {
            this.f1811a = ju6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(hq4.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutSettingFragment", "loadingState start " + bVar.a() + " stop " + bVar.b());
                if (bVar.b()) {
                    this.f1811a.a();
                } else if (bVar.a()) {
                    this.f1811a.b();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<List<? extends WorkoutSetting>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ju6 f1812a;

        @DexIgnore
        public e(ju6 ju6) {
            this.f1812a = ju6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WorkoutSetting> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutSettingFragment", "update workoutSettingList, " + list);
            hu6 K6 = ju6.K6(this.f1812a);
            pq7.b(list, "it");
            K6.k(list);
        }
    }

    @DexIgnore
    public static final /* synthetic */ hu6 K6(ju6 ju6) {
        hu6 hu6 = ju6.i;
        if (hu6 != null) {
            return hu6;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        mu6 mu6 = this.j;
        if (mu6 != null) {
            hu6 hu6 = this.i;
            if (hu6 != null) {
                mu6.q(hu6.h());
                return true;
            }
            pq7.n("mAdapter");
            throw null;
        }
        pq7.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hu6.a
    public void M3(WorkoutSetting workoutSetting, boolean z) {
        pq7.c(workoutSetting, "workout");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingFragment", "onConfirmationChange(), workout = " + workoutSetting + ", value=" + z);
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363373 && (activity = getActivity()) != null) {
                activity.finish();
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363291) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363373) {
                mu6 mu6 = this.j;
                if (mu6 != null) {
                    hu6 hu6 = this.i;
                    if (hu6 != null) {
                        mu6.u(hu6.h());
                    } else {
                        pq7.n("mAdapter");
                        throw null;
                    }
                } else {
                    pq7.n("mViewModel");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.hu6.a
    public void m5(WorkoutSetting workoutSetting, boolean z) {
        pq7.c(workoutSetting, "workout");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingFragment", "onEnableChange(), workout = " + workoutSetting + ", value=" + z);
    }

    @DexIgnore
    public void onClick(View view) {
        if (isActive() && view != null && view.getId() == 2131361851) {
            mu6 mu6 = this.j;
            if (mu6 != null) {
                hu6 hu6 = this.i;
                if (hu6 != null) {
                    mu6.q(hu6.h());
                } else {
                    pq7.n("mAdapter");
                    throw null;
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        rc5 rc5 = (rc5) aq0.f(layoutInflater, 2131558645, viewGroup, false, A6());
        this.h = new g37<>(this, rc5);
        RTLImageView rTLImageView = rc5.q;
        pq7.b(rTLImageView, "binding.acivBack");
        nl5.a(rTLImageView, this);
        this.i = new hu6();
        RecyclerView recyclerView = rc5.s;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        hu6 hu6 = this.i;
        if (hu6 != null) {
            recyclerView.setAdapter(hu6);
            hu6 hu62 = this.i;
            if (hu62 != null) {
                hu62.l(this);
                g37<rc5> g37 = this.h;
                if (g37 != null) {
                    rc5 a2 = g37.a();
                    if (a2 != null) {
                        pq7.b(a2, "mBinding.get()!!");
                        return a2.n();
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
            pq7.n("mAdapter");
            throw null;
        }
        pq7.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.h0.c().M().U().a(this);
        FragmentActivity requireActivity = requireActivity();
        po4 po4 = this.k;
        if (po4 != null) {
            ts0 a2 = vs0.f(requireActivity, po4).a(mu6.class);
            pq7.b(a2, "ViewModelProviders.of(re\u2026ingViewModel::class.java)");
            mu6 mu6 = (mu6) a2;
            this.j = mu6;
            if (mu6 != null) {
                mu6.r().h(getViewLifecycleOwner(), new b(this));
                mu6 mu62 = this.j;
                if (mu62 != null) {
                    mu62.l().h(getViewLifecycleOwner(), new c(this));
                    mu6 mu63 = this.j;
                    if (mu63 != null) {
                        mu63.j().h(getViewLifecycleOwner(), new d(this));
                        mu6 mu64 = this.j;
                        if (mu64 != null) {
                            mu64.s().h(getViewLifecycleOwner(), new e(this));
                        } else {
                            pq7.n("mViewModel");
                            throw null;
                        }
                    } else {
                        pq7.n("mViewModel");
                        throw null;
                    }
                } else {
                    pq7.n("mViewModel");
                    throw null;
                }
            } else {
                pq7.n("mViewModel");
                throw null;
            }
        } else {
            pq7.n("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
