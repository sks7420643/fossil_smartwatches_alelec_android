package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface t93 extends IInterface {
    @DexIgnore
    void beginAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException;

    @DexIgnore
    void endAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void generateEventId(u93 u93) throws RemoteException;

    @DexIgnore
    void getAppInstanceId(u93 u93) throws RemoteException;

    @DexIgnore
    void getCachedAppInstanceId(u93 u93) throws RemoteException;

    @DexIgnore
    void getConditionalUserProperties(String str, String str2, u93 u93) throws RemoteException;

    @DexIgnore
    void getCurrentScreenClass(u93 u93) throws RemoteException;

    @DexIgnore
    void getCurrentScreenName(u93 u93) throws RemoteException;

    @DexIgnore
    void getGmpAppId(u93 u93) throws RemoteException;

    @DexIgnore
    void getMaxUserProperties(String str, u93 u93) throws RemoteException;

    @DexIgnore
    void getTestFlag(u93 u93, int i) throws RemoteException;

    @DexIgnore
    void getUserProperties(String str, String str2, boolean z, u93 u93) throws RemoteException;

    @DexIgnore
    void initForTests(Map map) throws RemoteException;

    @DexIgnore
    void initialize(rg2 rg2, xs2 xs2, long j) throws RemoteException;

    @DexIgnore
    void isDataCollectionEnabled(u93 u93) throws RemoteException;

    @DexIgnore
    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException;

    @DexIgnore
    void logEventAndBundle(String str, String str2, Bundle bundle, u93 u93, long j) throws RemoteException;

    @DexIgnore
    void logHealthData(int i, String str, rg2 rg2, rg2 rg22, rg2 rg23) throws RemoteException;

    @DexIgnore
    void onActivityCreated(rg2 rg2, Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void onActivityDestroyed(rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivityPaused(rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivityResumed(rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivitySaveInstanceState(rg2 rg2, u93 u93, long j) throws RemoteException;

    @DexIgnore
    void onActivityStarted(rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivityStopped(rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void performAction(Bundle bundle, u93 u93, long j) throws RemoteException;

    @DexIgnore
    void registerOnMeasurementEventListener(us2 us2) throws RemoteException;

    @DexIgnore
    void resetAnalyticsData(long j) throws RemoteException;

    @DexIgnore
    void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void setCurrentScreen(rg2 rg2, String str, String str2, long j) throws RemoteException;

    @DexIgnore
    void setDataCollectionEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setDefaultEventParameters(Bundle bundle) throws RemoteException;

    @DexIgnore
    void setEventInterceptor(us2 us2) throws RemoteException;

    @DexIgnore
    void setInstanceIdProvider(vs2 vs2) throws RemoteException;

    @DexIgnore
    void setMeasurementEnabled(boolean z, long j) throws RemoteException;

    @DexIgnore
    void setMinimumSessionDuration(long j) throws RemoteException;

    @DexIgnore
    void setSessionTimeoutDuration(long j) throws RemoteException;

    @DexIgnore
    void setUserId(String str, long j) throws RemoteException;

    @DexIgnore
    void setUserProperty(String str, String str2, rg2 rg2, boolean z, long j) throws RemoteException;

    @DexIgnore
    void unregisterOnMeasurementEventListener(us2 us2) throws RemoteException;
}
