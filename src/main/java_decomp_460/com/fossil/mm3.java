package com.fossil;

import android.os.Process;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm3 extends Thread {
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ BlockingQueue<nm3<?>> c;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public /* final */ /* synthetic */ im3 e;

    @DexIgnore
    public mm3(im3 im3, String str, BlockingQueue<nm3<?>> blockingQueue) {
        this.e = im3;
        rc2.k(str);
        rc2.k(blockingQueue);
        this.b = new Object();
        this.c = blockingQueue;
        setName(str);
    }

    @DexIgnore
    public final void a() {
        synchronized (this.b) {
            this.b.notifyAll();
        }
    }

    @DexIgnore
    public final void b(InterruptedException interruptedException) {
        this.e.d().I().b(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }

    @DexIgnore
    public final void c() {
        synchronized (this.e.i) {
            if (!this.d) {
                this.e.j.release();
                this.e.i.notifyAll();
                if (this == this.e.c) {
                    this.e.c = null;
                } else if (this == this.e.d) {
                    this.e.d = null;
                } else {
                    this.e.d().F().a("Current scheduler thread is neither worker nor network");
                }
                this.d = true;
            }
        }
    }

    @DexIgnore
    public final void run() {
        boolean z = false;
        while (!z) {
            try {
                this.e.j.acquire();
                z = true;
            } catch (InterruptedException e2) {
                b(e2);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                nm3<?> poll = this.c.poll();
                if (poll != null) {
                    Process.setThreadPriority(poll.c ? threadPriority : 10);
                    poll.run();
                } else {
                    synchronized (this.b) {
                        if (this.c.peek() == null && !(this.e.k)) {
                            try {
                                this.b.wait(30000);
                            } catch (InterruptedException e3) {
                                b(e3);
                            }
                        }
                    }
                    synchronized (this.e.i) {
                        if (this.c.peek() == null) {
                            if (this.e.m().s(xg3.y0)) {
                            }
                            c();
                            return;
                        }
                    }
                }
            }
        } finally {
            c();
        }
    }
}
