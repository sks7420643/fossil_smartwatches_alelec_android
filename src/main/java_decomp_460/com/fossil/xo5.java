package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo5 implements wo5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ qw0 f4151a;
    @DexIgnore
    public /* final */ jw0<no5> b;
    @DexIgnore
    public /* final */ po5 c; // = new po5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jw0<no5> {
        @DexIgnore
        public a(qw0 qw0) {
            super(qw0);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(px0 px0, no5 no5) {
            if (no5.g() == null) {
                px0.bindNull(1);
            } else {
                px0.bindString(1, no5.g());
            }
            if (no5.e() == null) {
                px0.bindNull(2);
            } else {
                px0.bindString(2, no5.e());
            }
            if (no5.f() == null) {
                px0.bindNull(3);
            } else {
                px0.bindString(3, no5.f());
            }
            String a2 = xo5.this.c.a(no5.a());
            if (a2 == null) {
                px0.bindNull(4);
            } else {
                px0.bindString(4, a2);
            }
            px0.bindLong(5, no5.i() ? 1 : 0);
            if (no5.d() == null) {
                px0.bindNull(6);
            } else {
                px0.bindString(6, no5.d());
            }
            if (no5.b() == null) {
                px0.bindNull(7);
            } else {
                px0.bindString(7, no5.b());
            }
            if (no5.c() == null) {
                px0.bindNull(8);
            } else {
                px0.bindString(8, no5.c());
            }
            if (no5.h() == null) {
                px0.bindNull(9);
            } else {
                px0.bindString(9, no5.h());
            }
        }

        @DexIgnore
        @Override // com.fossil.xw0
        public String createQuery() {
            return "INSERT OR REPLACE INTO `diana_recommended_preset` (`serial`,`id`,`name`,`button`,`isDefault`,`faceUrl`,`checkSum`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore
    public xo5(qw0 qw0) {
        this.f4151a = qw0;
        this.b = new a(qw0);
    }

    @DexIgnore
    @Override // com.fossil.wo5
    public Long[] insert(List<no5> list) {
        this.f4151a.assertNotSuspendingTransaction();
        this.f4151a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.f4151a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.f4151a.endTransaction();
        }
    }
}
