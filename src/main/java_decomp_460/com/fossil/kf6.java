package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kf6 implements Factory<nf6> {
    @DexIgnore
    public static nf6 a(if6 if6) {
        nf6 b = if6.b();
        lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
