package com.fossil;

import android.bluetooth.BluetoothDevice;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Hashtable<String, k5> f4037a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ x4 b; // = new x4();

    @DexIgnore
    public final k5 a(BluetoothDevice bluetoothDevice) {
        k5 k5Var;
        m80.c.a("Peripheral.Factory", "getPeripheral: bluetoothDevice=%s.", bluetoothDevice.getAddress());
        synchronized (f4037a) {
            k5Var = f4037a.get(bluetoothDevice.getAddress());
            if (k5Var == null) {
                k5Var = new k5(bluetoothDevice, null);
                f4037a.put(k5Var.A.getAddress(), k5Var);
            }
        }
        return k5Var;
    }
}
