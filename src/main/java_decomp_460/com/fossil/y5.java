package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y5 extends u5 {
    @DexIgnore
    public /* final */ n5 k; // = n5.VERY_HIGH;
    @DexIgnore
    public f5 l; // = f5.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean m;

    @DexIgnore
    public y5(boolean z, n4 n4Var) {
        super(v5.c, n4Var);
        this.m = z;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void d(k5 k5Var) {
        k5Var.t(this.m);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void f(h7 h7Var) {
        s5 a2;
        k(h7Var);
        g7 g7Var = h7Var.f1435a;
        if (g7Var.b == f7.SUCCESS) {
            a2 = this.l == f5.CONNECTED ? s5.a(this.e, null, r5.b, null, 5) : s5.a(this.e, null, r5.e, null, 5);
        } else {
            s5 a3 = s5.e.a(g7Var);
            a2 = s5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public n5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public boolean i(h7 h7Var) {
        f5 f5Var;
        return (h7Var instanceof z6) && ((f5Var = ((z6) h7Var).b) == f5.CONNECTED || f5Var == f5.DISCONNECTED);
    }

    @DexIgnore
    @Override // com.fossil.u5
    public fd0<h7> j() {
        return this.j.e;
    }

    @DexIgnore
    @Override // com.fossil.u5
    public void k(h7 h7Var) {
        this.l = ((z6) h7Var).b;
    }
}
