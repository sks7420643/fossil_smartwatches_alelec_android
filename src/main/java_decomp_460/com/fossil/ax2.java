package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ax2<E> extends bz2<E> {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public ax2(int i, int i2) {
        sw2.g(i2, i);
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public abstract E a(int i);

    @DexIgnore
    public final boolean hasNext() {
        return this.c < this.b;
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.c > 0;
    }

    @DexIgnore
    @Override // java.util.Iterator, java.util.ListIterator
    public final E next() {
        if (hasNext()) {
            int i = this.c;
            this.c = i + 1;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.c;
    }

    @DexIgnore
    @Override // java.util.ListIterator
    public final E previous() {
        if (hasPrevious()) {
            int i = this.c - 1;
            this.c = i;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.c - 1;
    }
}
