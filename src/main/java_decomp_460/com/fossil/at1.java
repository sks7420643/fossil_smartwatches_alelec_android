package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class at1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ g90 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<at1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public at1 createFromParcel(Parcel parcel) {
            g90 g90 = g90.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = f90.f1085a[g90.ordinal()];
            if (i == 1) {
                return i90.CREATOR.a(parcel);
            }
            if (i == 2) {
                return ct1.CREATOR.a(parcel);
            }
            if (i == 3) {
                return bt1.CREATOR.a(parcel);
            }
            throw new al7();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public at1[] newArray(int i) {
            return new at1[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public at1(Parcel parcel) {
        this(g90.values()[parcel.readInt()], parcel.readInt() != 1 ? false : true);
    }

    @DexIgnore
    public at1(g90 g90, boolean z) {
        this.b = g90;
        this.c = z;
    }

    @DexIgnore
    public /* synthetic */ at1(g90 g90, boolean z, int i) {
        z = (i & 2) != 0 ? false : z;
        this.b = g90;
        this.c = z;
    }

    @DexIgnore
    public boolean a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            at1 at1 = (at1) obj;
            return this.b == at1.b && this.c == at1.c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + Boolean.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("enable_goal_ring", this.c);
        pq7.b(put, "JSONObject()\n           \u2026 mEnablePercentageCircle)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }
}
