package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ys4 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    int b(String str);

    @DexIgnore
    LiveData<List<xs4>> c();

    @DexIgnore
    List<xs4> d();

    @DexIgnore
    int e();

    @DexIgnore
    List<xs4> f();

    @DexIgnore
    Object g();  // void declaration

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    List<xs4> i();

    @DexIgnore
    Long[] insert(List<xs4> list);

    @DexIgnore
    Object j();  // void declaration

    @DexIgnore
    List<xs4> k();

    @DexIgnore
    List<xs4> l();

    @DexIgnore
    int m(String[] strArr);

    @DexIgnore
    List<xs4> n();

    @DexIgnore
    long o(xs4 xs4);

    @DexIgnore
    xs4 p(String str);
}
