package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j46 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ h46 f1709a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LoaderManager c;

    @DexIgnore
    public j46(h46 h46, int i, LoaderManager loaderManager) {
        pq7.c(h46, "mView");
        pq7.c(loaderManager, "mLoaderManager");
        this.f1709a = h46;
        this.b = i;
        this.c = loaderManager;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager b() {
        return this.c;
    }

    @DexIgnore
    public final h46 c() {
        return this.f1709a;
    }
}
