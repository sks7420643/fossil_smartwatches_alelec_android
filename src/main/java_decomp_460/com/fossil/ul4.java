package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul4 extends zl4 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public ul4(zl4 zl4, int i, int i2) {
        super(zl4);
        this.c = (short) ((short) i);
        this.d = (short) ((short) i2);
    }

    @DexIgnore
    @Override // com.fossil.zl4
    public void c(am4 am4, byte[] bArr) {
        int i = 0;
        while (true) {
            short s = this.d;
            if (i < s) {
                if (i == 0 || (i == 31 && s <= 62)) {
                    am4.g(31, 5);
                    short s2 = this.d;
                    if (s2 > 62) {
                        am4.g(s2 - 31, 16);
                    } else if (i == 0) {
                        am4.g(Math.min((int) s2, 31), 5);
                    } else {
                        am4.g(s2 - 31, 5);
                    }
                }
                am4.g(bArr[this.c + i], 8);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(SimpleComparison.LESS_THAN_OPERATION);
        sb.append((int) this.c);
        sb.append("::");
        sb.append((this.c + this.d) - 1);
        sb.append('>');
        return sb.toString();
    }
}
