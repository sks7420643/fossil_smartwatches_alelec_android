package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v32 implements Factory<t32> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ v32 f3701a; // = new v32();

    @DexIgnore
    public static v32 a() {
        return f3701a;
    }

    @DexIgnore
    public static t32 b() {
        t32 a2 = u32.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    /* renamed from: c */
    public t32 get() {
        return b();
    }
}
