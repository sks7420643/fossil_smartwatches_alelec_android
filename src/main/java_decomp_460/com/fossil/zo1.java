package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo1 extends hv1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ vw1 c; // = vw1.TOP_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ vw1 d; // = vw1.MIDDLE_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ vw1 e; // = vw1.BOTTOM_SHORT_PRESS_RELEASE;
    @DexIgnore
    public /* final */ HashSet<yo1> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zo1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public zo1 a(Parcel parcel) {
            return new zo1(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zo1 createFromParcel(Parcel parcel) {
            return new zo1(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zo1[] newArray(int i) {
            return new zo1[i];
        }
    }

    @DexIgnore
    public zo1(Parcel parcel) {
        super(parcel);
        HashSet<yo1> hashSet = new HashSet<>();
        this.b = hashSet;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(yo1.class.getClassLoader());
        if (readParcelableArray != null) {
            mm7.t(hashSet, (yo1[]) readParcelableArray);
            return;
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchApp>");
    }

    @DexIgnore
    public zo1(yo1 yo1, yo1 yo12, yo1 yo13) {
        this.b = new HashSet<>();
        yo1.a(c);
        yo12.a(d);
        yo13.a(e);
        this.b.add(yo1);
        this.b.add(yo12);
        this.b.add(yo13);
    }

    @DexIgnore
    public zo1(yo1[] yo1Arr) {
        HashSet<yo1> hashSet = new HashSet<>();
        this.b = hashSet;
        mm7.t(hashSet, yo1Arr);
    }

    @DexIgnore
    @Override // com.fossil.hv1
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (T t : this.b) {
            jSONArray.put(t.toJSONObject());
            gy1.b(jSONObject, t.a(), false, 2, null);
        }
        jSONObject.put("master._.config.buttons", jSONArray);
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(zo1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(pq7.a(this.b, ((zo1) obj).b) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchAppConfig");
    }

    @DexIgnore
    public final yo1 getBottomButton() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next.getButtonEvent() == e) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        if (t != null) {
            return t;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final yo1 getMiddleButton() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next.getButtonEvent() == d) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        if (t != null) {
            return t;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final yo1 getTopButton() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next.getButtonEvent() == c) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        if (t != null) {
            return t;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    @Override // com.fossil.hv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.b.toArray(new yo1[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
