package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o34 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends k24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;
        @DexIgnore
        public /* final */ /* synthetic */ j14 d;

        @DexIgnore
        public a(Iterable iterable, j14 j14) {
            this.c = iterable;
            this.d = j14;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return p34.k(this.c.iterator(), this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends k24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;
        @DexIgnore
        public /* final */ /* synthetic */ b14 d;

        @DexIgnore
        public b(Iterable iterable, b14 b14) {
            this.c = iterable;
            this.d = b14;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return p34.w(this.c.iterator(), this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends k24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        public c(List list, int i) {
            this.c = list;
            this.d = i;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            int min = Math.min(this.c.size(), this.d);
            List list = this.c;
            return list.subList(min, list.size()).iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends k24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<T> {
            @DexIgnore
            public boolean b; // = true;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator c;

            @DexIgnore
            public a(d dVar, Iterator it) {
                this.c = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.c.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public T next() {
                T t = (T) this.c.next();
                this.b = false;
                return t;
            }

            @DexIgnore
            public void remove() {
                a24.c(!this.b);
                this.c.remove();
            }
        }

        @DexIgnore
        public d(Iterable iterable, int i) {
            this.c = iterable;
            this.d = i;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            Iterator it = this.c.iterator();
            p34.b(it, this.d);
            return new a(this, it);
        }
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> boolean a(Collection<T> collection, Iterable<? extends T> iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll(b24.a(iterable));
        }
        i14.l(iterable);
        return p34.a(collection, iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean b(Iterable<T> iterable, j14<? super T> j14) {
        return p34.c(iterable.iterator(), j14);
    }

    @DexIgnore
    public static <E> Collection<E> c(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : t34.i(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> d(Iterable<T> iterable, j14<? super T> j14) {
        i14.l(iterable);
        i14.l(j14);
        return new a(iterable, j14);
    }

    @DexIgnore
    public static <T> T e(Iterable<? extends T> iterable, T t) {
        return (T) p34.n(iterable.iterator(), t);
    }

    @DexIgnore
    public static <T> T f(Iterable<T> iterable) {
        return (T) p34.o(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> g(Iterable<T> iterable, int i) {
        i14.l(iterable);
        i14.e(i >= 0, "number to skip cannot be negative");
        return iterable instanceof List ? new c((List) iterable, i) : new d(iterable, i);
    }

    @DexIgnore
    public static Object[] h(Iterable<?> iterable) {
        return c(iterable).toArray();
    }

    @DexIgnore
    public static <T> T[] i(Iterable<? extends T> iterable, T[] tArr) {
        return (T[]) c(iterable).toArray(tArr);
    }

    @DexIgnore
    public static String j(Iterable<?> iterable) {
        return p34.v(iterable.iterator());
    }

    @DexIgnore
    public static <F, T> Iterable<T> k(Iterable<F> iterable, b14<? super F, ? extends T> b14) {
        i14.l(iterable);
        i14.l(b14);
        return new b(iterable, b14);
    }
}
