package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.lifecycle.Lifecycle;
import coil.target.ImageViewTarget;
import com.fossil.p18;
import com.fossil.w71;
import com.fossil.x71;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u71 extends y71<u71> {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public Drawable C;
    @DexIgnore
    public /* final */ Context t;
    @DexIgnore
    public j81 u; // = null;
    @DexIgnore
    public Lifecycle v; // = null;
    @DexIgnore
    public n81 w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u71(Context context, z41 z41) {
        super(z41, null);
        pq7.c(context, "context");
        pq7.c(z41, Constants.DEFAULTS);
        this.t = context;
        this.w = z41.h();
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.A = z41.f();
        this.B = z41.d();
        this.C = z41.e();
    }

    @DexIgnore
    public final u71 A(j81 j81) {
        this.u = j81;
        return this;
    }

    @DexIgnore
    public final t71 w() {
        Context context = this.t;
        Object f = f();
        j81 j81 = this.u;
        Lifecycle lifecycle = this.v;
        n81 n81 = this.w;
        String k = k();
        List<String> a2 = a();
        x71.a l = l();
        g81 r = r();
        e81 q = q();
        d81 p = p();
        u51 g = g();
        dv7 i = i();
        List<m81> s = s();
        Bitmap.Config d = d();
        ColorSpace e = e();
        p18.a j = j();
        w71 w71 = null;
        p18 s2 = w81.s(j != null ? j.e() : null);
        pq7.b(s2, "headers?.build().orEmpty()");
        w71.a o = o();
        if (o != null) {
            w71 = o.a();
        }
        return new t71(context, f, j81, lifecycle, n81, k, a2, l, r, q, p, g, i, s, d, e, s2, w81.r(w71), n(), h(), m(), b(), c(), this.x, this.y, this.z, this.A, this.B, this.C);
    }

    @DexIgnore
    public final u71 x(Object obj) {
        u(obj);
        return this;
    }

    @DexIgnore
    public final u71 y(Drawable drawable) {
        this.A = drawable;
        this.x = 0;
        return this;
    }

    @DexIgnore
    public final u71 z(ImageView imageView) {
        pq7.c(imageView, "imageView");
        A(new ImageViewTarget(imageView));
        return this;
    }
}
