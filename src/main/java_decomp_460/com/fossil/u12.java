package com.fossil;

import android.app.job.JobParameters;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class u12 implements Runnable {
    @DexIgnore
    public /* final */ JobInfoSchedulerService b;
    @DexIgnore
    public /* final */ JobParameters c;

    @DexIgnore
    public u12(JobInfoSchedulerService jobInfoSchedulerService, JobParameters jobParameters) {
        this.b = jobInfoSchedulerService;
        this.c = jobParameters;
    }

    @DexIgnore
    public static Runnable a(JobInfoSchedulerService jobInfoSchedulerService, JobParameters jobParameters) {
        return new u12(jobInfoSchedulerService, jobParameters);
    }

    @DexIgnore
    public void run() {
        this.b.jobFinished(this.c, false);
    }
}
