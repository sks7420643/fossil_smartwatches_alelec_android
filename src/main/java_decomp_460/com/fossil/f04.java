package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f04 extends zz3 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public float f1015a; // = -1.0f;

    @DexIgnore
    @Override // com.fossil.zz3
    public void a(i04 i04, float f, float f2, float f3) {
        i04.n(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3 * f2, 180.0f, 180.0f - f);
        float f4 = 2.0f * f3 * f2;
        i04.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4, f4, 180.0f, f);
    }
}
