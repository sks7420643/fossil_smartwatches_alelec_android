package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e90 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f893a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;

    @DexIgnore
    public /* synthetic */ e90(String str, String str2, String str3, int i, String str4, String str5, int i2) {
        str5 = (i2 & 32) != 0 ? "android" : str5;
        this.f893a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
        this.e = str4;
        this.f = str5;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof e90) {
                e90 e90 = (e90) obj;
                if (!pq7.a(this.f893a, e90.f893a) || !pq7.a(this.b, e90.b) || !pq7.a(this.c, e90.c) || this.d != e90.d || !pq7.a(this.e, e90.e) || !pq7.a(this.f, e90.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.f893a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        int i2 = this.d;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.f;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i2) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder e2 = e.e("SystemInformation(osVersion=");
        e2.append(this.f893a);
        e2.append(", phoneModel=");
        e2.append(this.b);
        e2.append(", userId=");
        e2.append(this.c);
        e2.append(", timezoneOffset=");
        e2.append(this.d);
        e2.append(", sdkVersion=");
        e2.append(this.e);
        e2.append(", osName=");
        e2.append(this.f);
        e2.append(")");
        return e2.toString();
    }
}
