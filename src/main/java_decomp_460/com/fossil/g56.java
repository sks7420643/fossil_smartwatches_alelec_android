package com.fossil;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g56 implements Factory<ArrayList<String>> {
    @DexIgnore
    public static ArrayList<String> a(f56 f56) {
        ArrayList<String> b = f56.b();
        lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
