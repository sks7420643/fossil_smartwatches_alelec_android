package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l73 implements m73 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2154a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;
    @DexIgnore
    public static /* final */ xv2<Boolean> c;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f2154a = hw2.d("measurement.client.global_params.dev", false);
        b = hw2.d("measurement.service.global_params_in_payload", true);
        c = hw2.d("measurement.service.global_params", false);
        hw2.b("measurement.id.service.global_params", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.m73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m73
    public final boolean zzb() {
        return f2154a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.m73
    public final boolean zzc() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.m73
    public final boolean zzd() {
        return c.o().booleanValue();
    }
}
