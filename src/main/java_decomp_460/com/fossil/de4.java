package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class de4<T> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Class<T> f776a;
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public T a() {
        return this.b;
    }

    @DexIgnore
    public Class<T> b() {
        return this.f776a;
    }

    @DexIgnore
    public String toString() {
        return String.format("Event{type: %s, payload: %s}", this.f776a, this.b);
    }
}
