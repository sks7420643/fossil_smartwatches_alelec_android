package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt5 implements Factory<ot5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<DeviceRepository> f2868a;
    @DexIgnore
    public /* final */ Provider<on5> b;

    @DexIgnore
    public pt5(Provider<DeviceRepository> provider, Provider<on5> provider2) {
        this.f2868a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static pt5 a(Provider<DeviceRepository> provider, Provider<on5> provider2) {
        return new pt5(provider, provider2);
    }

    @DexIgnore
    public static ot5 c(DeviceRepository deviceRepository, on5 on5) {
        return new ot5(deviceRepository, on5);
    }

    @DexIgnore
    /* renamed from: b */
    public ot5 get() {
        return c(this.f2868a.get(), this.b.get());
    }
}
