package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.StressWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz4 {
    @DexIgnore
    public static /* final */ String b;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f3860a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<RestingWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<SleepSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<ArrayList<RestingWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<ArrayList<SleepSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends TypeToken<ArrayList<WorkoutSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends TypeToken<ArrayList<WorkoutSessionWrapper>> {
    }

    /*
    static {
        String simpleName = d05.class.getSimpleName();
        pq7.b(simpleName, "JsonObjectConverter::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public vz4() {
        zi4 zi4 = new zi4();
        zi4.f(DateTime.class, new GsonConvertDateTime());
        Gson d2 = zi4.d();
        pq7.b(d2, "GsonBuilder().registerTy\u2026nvertDateTime()).create()");
        this.f3860a = d2;
    }

    @DexIgnore
    public final String a(ActiveMinuteWrapper activeMinuteWrapper) {
        pq7.c(activeMinuteWrapper, "activeMinute");
        try {
            String u = this.f3860a.u(activeMinuteWrapper, ActiveMinuteWrapper.class);
            pq7.b(u, "mGson.toJson(activeMinut\u2026inuteWrapper::class.java)");
            return u;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("activeMinuteToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String b(CalorieWrapper calorieWrapper) {
        pq7.c(calorieWrapper, "calorie");
        try {
            String u = this.f3860a.u(calorieWrapper, CalorieWrapper.class);
            pq7.b(u, "mGson.toJson(calorie, CalorieWrapper::class.java)");
            return u;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("calorieToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String c(DistanceWrapper distanceWrapper) {
        pq7.c(distanceWrapper, "distance");
        try {
            String u = this.f3860a.u(distanceWrapper, DistanceWrapper.class);
            pq7.b(u, "mGson.toJson(distance, D\u2026tanceWrapper::class.java)");
            return u;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("distanceToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String d(HeartRateWrapper heartRateWrapper) {
        if (heartRateWrapper == null) {
            return null;
        }
        try {
            return this.f3860a.u(heartRateWrapper, HeartRateWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("heartRateToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String e(List<RestingWrapper> list) {
        pq7.c(list, "resting");
        try {
            return this.f3860a.u(list, new a().getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("restingToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String f(List<SleepSessionWrapper> list) {
        pq7.c(list, "sleepSessions");
        try {
            String u = this.f3860a.u(list, new b().getType());
            pq7.b(u, "mGson.toJson(sleepSessions, listType)");
            return u;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("sleepSessionsToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String g(StepWrapper stepWrapper) {
        pq7.c(stepWrapper, "step");
        try {
            String u = this.f3860a.u(stepWrapper, StepWrapper.class);
            pq7.b(u, "mGson.toJson(step, StepWrapper::class.java)");
            return u;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("stepToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String h(StressWrapper stressWrapper) {
        if (stressWrapper == null) {
            return null;
        }
        try {
            return this.f3860a.u(stressWrapper, StressWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("stressToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final ActiveMinuteWrapper i(String str) {
        pq7.c(str, "data");
        try {
            Object k = this.f3860a.k(str, ActiveMinuteWrapper.class);
            pq7.b(k, "mGson.fromJson(data, Act\u2026inuteWrapper::class.java)");
            return (ActiveMinuteWrapper) k;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toActiveMinute exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new ActiveMinuteWrapper(0, new ArrayList(), 0);
        }
    }

    @DexIgnore
    public final CalorieWrapper j(String str) {
        pq7.c(str, "data");
        try {
            Object k = this.f3860a.k(str, CalorieWrapper.class);
            pq7.b(k, "mGson.fromJson(data, CalorieWrapper::class.java)");
            return (CalorieWrapper) k;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toCalorie exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new CalorieWrapper(0, new ArrayList(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final DistanceWrapper k(String str) {
        pq7.c(str, "data");
        try {
            Object k = this.f3860a.k(str, DistanceWrapper.class);
            pq7.b(k, "mGson.fromJson(data, DistanceWrapper::class.java)");
            return (DistanceWrapper) k;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toDistance exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new DistanceWrapper(0, new ArrayList(), 0.0d);
        }
    }

    @DexIgnore
    public final HeartRateWrapper l(String str) {
        HeartRateWrapper heartRateWrapper;
        if (str == null) {
            return null;
        }
        try {
            heartRateWrapper = (HeartRateWrapper) this.f3860a.k(str, HeartRateWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toDistance exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            heartRateWrapper = null;
        }
        return heartRateWrapper;
    }

    @DexIgnore
    public final List<RestingWrapper> m(String str) {
        try {
            Object l = this.f3860a.l(str, new c().getType());
            pq7.b(l, "mGson.fromJson(data, listType)");
            return (List) l;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toResting exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final List<SleepSessionWrapper> n(String str) {
        pq7.c(str, "data");
        try {
            Object l = this.f3860a.l(str, new d().getType());
            pq7.b(l, "mGson.fromJson(data, listType)");
            return (List) l;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toSleepSessions exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final StepWrapper o(String str) {
        pq7.c(str, "data");
        try {
            Object k = this.f3860a.k(str, StepWrapper.class);
            pq7.b(k, "mGson.fromJson(data, StepWrapper::class.java)");
            return (StepWrapper) k;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toStep exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new StepWrapper(0, new ArrayList(), 0);
        }
    }

    @DexIgnore
    public final StressWrapper p(String str) {
        StressWrapper stressWrapper;
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            stressWrapper = (StressWrapper) this.f3860a.k(str, StressWrapper.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toStress exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            stressWrapper = null;
        }
        return stressWrapper;
    }

    @DexIgnore
    public final List<WorkoutSessionWrapper> q(String str) {
        pq7.c(str, "data");
        try {
            Object l = this.f3860a.l(str, new e().getType());
            pq7.b(l, "mGson.fromJson(data, listType)");
            return (List) l;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSession exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final String r(List<WorkoutSessionWrapper> list) {
        pq7.c(list, "workoutSessions");
        try {
            String u = this.f3860a.u(list, new f().getType());
            pq7.b(u, "mGson.toJson(workoutSessions, listType)");
            return u;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("workoutSessionToString exception=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.d(str, sb.toString());
            return "";
        }
    }
}
