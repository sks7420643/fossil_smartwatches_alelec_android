package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs extends ss {
    @DexIgnore
    public /* final */ n6 A;
    @DexIgnore
    public /* final */ boolean B;

    @DexIgnore
    public vs(n6 n6Var, boolean z, k5 k5Var) {
        super(hs.f, k5Var);
        this.A = n6Var;
        this.B = z;
    }

    @DexIgnore
    @Override // com.fossil.ns
    public u5 D() {
        return new i6(this.A, this.B, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.R0, this.A.b), jd0.F, Boolean.valueOf(this.B));
    }
}
