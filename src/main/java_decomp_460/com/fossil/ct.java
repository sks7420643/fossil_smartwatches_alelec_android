package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct extends bt {
    @DexIgnore
    public long K;
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ct(k5 k5Var, long j) {
        super(k5Var, ut.PROCESS_USER_AUTHORIZATION, hs.f0, 0, 8);
        boolean z = false;
        this.L = j;
        if (j >= 0 && j <= hy1.b(oq7.f2710a)) {
            z = true;
        }
        if (z) {
            this.K = this.L;
            return;
        }
        StringBuilder e = e.e("timeoutInMs (");
        e.append(this.L);
        e.append(") must be in [0, [");
        e.append(hy1.b(oq7.f2710a));
        e.append("]].");
        throw new IllegalArgumentException(e.toString().toString());
    }

    @DexIgnore
    @Override // com.fossil.ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        pq7.b(array, "ByteBuffer.allocate(4)\n \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.K = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(super.z(), jd0.K4, Long.valueOf(this.L));
    }
}
