package com.fossil;

import android.os.Parcel;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g2 extends c2 {
    @DexIgnore
    public static /* final */ f2 CREATOR; // = new f2(null);
    @DexIgnore
    public /* final */ v8[] e;
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore
    public g2(byte b, v8[] v8VarArr, byte[] bArr) {
        super(lt.BACKGROUND_SYNC_EVENT, b, false, 4);
        this.e = v8VarArr;
        this.f = bArr;
    }

    @DexIgnore
    public /* synthetic */ g2(Parcel parcel, kq7 kq7) {
        super(parcel);
        Object[] createTypedArray = parcel.createTypedArray(v8.CREATOR);
        if (createTypedArray != null) {
            this.e = (v8[]) createTypedArray;
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                this.f = createByteArray;
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c2
    public byte[] a() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(g2.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            g2 g2Var = (g2) obj;
            return this.c == g2Var.c && Arrays.equals(this.e, g2Var.e) && Arrays.equals(this.f, g2Var.f);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        byte b = this.c;
        return (((b * 31) + this.e.hashCode()) * 31) + Arrays.hashCode(this.f);
    }

    @DexIgnore
    @Override // com.fossil.c2, com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (v8 v8Var : this.e) {
            jSONArray.put(v8Var.toJSONObject());
        }
        return g80.k(g80.k(super.toJSONObject(), jd0.N1, jSONArray), jd0.O1, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    @Override // com.fossil.c2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.e, i);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
    }
}
