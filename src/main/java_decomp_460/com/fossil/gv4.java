package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv4 extends pv5 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public po4 g;
    @DexIgnore
    public g37<j85> h;
    @DexIgnore
    public iv4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public long k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public String m;
    @DexIgnore
    public kv4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return gv4.u;
        }

        @DexIgnore
        public final gv4 b(String str, long j, int i, String str2) {
            gv4 gv4 = new gv4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id_extra", str);
            bundle.putLong("challenge_start_time_extra", j);
            bundle.putInt("challenge_target_extra", i);
            bundle.putString("challenge_status_extra", str2);
            gv4.setArguments(bundle);
            return gv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gv4 b;

        @DexIgnore
        public b(gv4 gv4) {
            this.b = gv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ j85 f1371a;
        @DexIgnore
        public /* final */ /* synthetic */ gv4 b;

        @DexIgnore
        public c(j85 j85, gv4 gv4) {
            this.f1371a = j85;
            this.b = gv4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.f1371a.q;
            pq7.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            String str = this.b.j;
            if (str != null) {
                gv4.O6(this.b).m(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gv4 b;

        @DexIgnore
        public d(gv4 gv4) {
            this.b = gv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            BCFindFriendsActivity.a aVar = BCFindFriendsActivity.A;
            gv4 gv4 = this.b;
            aVar.a(gv4, gv4.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<List<? extends Object>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv4 f1372a;

        @DexIgnore
        public e(gv4 gv4) {
            this.f1372a = gv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            kv4 M6 = gv4.M6(this.f1372a);
            pq7.b(list, "it");
            M6.g(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv4 f1373a;

        @DexIgnore
        public f(gv4 gv4) {
            this.f1373a = gv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            j85 j85 = (j85) gv4.K6(this.f1373a).a();
            if (j85 != null) {
                SwipeRefreshLayout swipeRefreshLayout = j85.w;
                pq7.b(swipeRefreshLayout, "swipe");
                pq7.b(bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<cl7<? extends Boolean, ? extends ServerError>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv4 f1374a;

        @DexIgnore
        public g(gv4 gv4) {
            this.f1374a = gv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cl7<Boolean, ? extends ServerError> cl7) {
            if (cl7.getFirst().booleanValue()) {
                j85 j85 = (j85) gv4.K6(this.f1374a).a();
                if (j85 != null) {
                    FlexibleTextView flexibleTextView = j85.q;
                    pq7.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            FragmentActivity activity = this.f1374a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, um5.c(activity, 2131886231), 1).show();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gv4 f1375a;

        @DexIgnore
        public h(gv4 gv4) {
            this.f1375a = gv4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            j85 j85;
            ImageView imageView;
            if (pq7.a("completed", str) && (j85 = (j85) gv4.K6(this.f1375a).a()) != null && (imageView = j85.s) != null) {
                imageView.setVisibility(0);
            }
        }
    }

    /*
    static {
        String simpleName = gv4.class.getSimpleName();
        pq7.b(simpleName, "BCLeaderBoardFragment::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(gv4 gv4) {
        g37<j85> g37 = gv4.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ kv4 M6(gv4 gv4) {
        kv4 kv4 = gv4.s;
        if (kv4 != null) {
            return kv4;
        }
        pq7.n("leaderBoardAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ iv4 O6(gv4 gv4) {
        iv4 iv4 = gv4.i;
        if (iv4 != null) {
            return iv4;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        this.s = new kv4(this.k, this.l);
        g37<j85> g37 = this.h;
        if (g37 != null) {
            j85 a2 = g37.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.v;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                kv4 kv4 = this.s;
                if (kv4 != null) {
                    recyclerView.setAdapter(kv4);
                    a2.t.setOnClickListener(new b(this));
                    a2.w.setOnRefreshListener(new c(a2, this));
                    a2.s.setOnClickListener(new d(this));
                    return;
                }
                pq7.n("leaderBoardAdapter");
                throw null;
            }
            return;
        }
        pq7.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        iv4 iv4 = this.i;
        if (iv4 != null) {
            iv4.i().h(getViewLifecycleOwner(), new e(this));
            iv4 iv42 = this.i;
            if (iv42 != null) {
                iv42.h().h(getViewLifecycleOwner(), new f(this));
                iv4 iv43 = this.i;
                if (iv43 != null) {
                    iv43.g().h(getViewLifecycleOwner(), new g(this));
                    iv4 iv44 = this.i;
                    if (iv44 != null) {
                        iv44.f().h(getViewLifecycleOwner(), new h(this));
                    } else {
                        pq7.n("viewModel");
                        throw null;
                    }
                } else {
                    pq7.n("viewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModel");
                throw null;
            }
        } else {
            pq7.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        String str;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 16 && i3 == -1 && (str = this.j) != null) {
            iv4 iv4 = this.i;
            if (iv4 == null) {
                pq7.n("viewModel");
                throw null;
            } else if (str != null) {
                iv4.m(str);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        String str = null;
        super.onCreate(bundle);
        PortfolioApp.h0.c().M().C0().a(this);
        po4 po4 = this.g;
        if (po4 != null) {
            ts0 a2 = vs0.d(this, po4).a(iv4.class);
            pq7.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
            this.i = (iv4) a2;
            Bundle arguments = getArguments();
            this.j = arguments != null ? arguments.getString("challenge_id_extra") : null;
            Bundle arguments2 = getArguments();
            this.k = arguments2 != null ? arguments2.getLong("challenge_start_time_extra") : new Date().getTime();
            Bundle arguments3 = getArguments();
            this.l = arguments3 != null ? arguments3.getInt("challenge_target_extra") : -1;
            Bundle arguments4 = getArguments();
            if (arguments4 != null) {
                str = arguments4.getString("challenge_status_extra");
            }
            this.m = str;
            return;
        }
        pq7.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        j85 j85 = (j85) aq0.f(layoutInflater, 2131558581, viewGroup, false, A6());
        this.h = new g37<>(this, j85);
        pq7.b(j85, "binding");
        return j85.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ck5 g2 = ck5.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_full_leader_board", activity);
            return;
        }
        throw new il7("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        iv4 iv4 = this.i;
        if (iv4 != null) {
            iv4.k(this.j, this.m);
            P6();
            Q6();
            return;
        }
        pq7.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
