package com.fossil;

import androidx.work.ListenableWorker;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h11 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public UUID f1407a;
    @DexIgnore
    public o31 b;
    @DexIgnore
    public Set<String> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<B extends a<?, ?>, W extends h11> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f1408a; // = false;
        @DexIgnore
        public UUID b; // = UUID.randomUUID();
        @DexIgnore
        public o31 c;
        @DexIgnore
        public Set<String> d; // = new HashSet();

        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            this.c = new o31(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.h11$a<B extends com.fossil.h11$a<?, ?>, W extends com.fossil.h11> */
        /* JADX WARN: Multi-variable type inference failed */
        public final B a(String str) {
            this.d.add(str);
            d();
            return this;
        }

        @DexIgnore
        public final W b() {
            W c2 = c();
            this.b = UUID.randomUUID();
            o31 o31 = new o31(this.c);
            this.c = o31;
            o31.f2626a = this.b.toString();
            return c2;
        }

        @DexIgnore
        public abstract W c();

        @DexIgnore
        public abstract B d();

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.h11$a<B extends com.fossil.h11$a<?, ?>, W extends com.fossil.h11> */
        /* JADX WARN: Multi-variable type inference failed */
        public final B e(p01 p01) {
            this.c.j = p01;
            d();
            return this;
        }
    }

    @DexIgnore
    public h11(UUID uuid, o31 o31, Set<String> set) {
        this.f1407a = uuid;
        this.b = o31;
        this.c = set;
    }

    @DexIgnore
    public UUID a() {
        return this.f1407a;
    }

    @DexIgnore
    public String b() {
        return this.f1407a.toString();
    }

    @DexIgnore
    public Set<String> c() {
        return this.c;
    }

    @DexIgnore
    public o31 d() {
        return this.b;
    }
}
