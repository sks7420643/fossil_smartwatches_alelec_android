package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar extends zj {
    @DexIgnore
    public /* final */ ou1 T;

    @DexIgnore
    public ar(k5 k5Var, i60 i60, ou1 ou1) {
        super(k5Var, i60, yp.L0, true, 1797, ou1.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = ou1;
    }

    @DexIgnore
    @Override // com.fossil.lp, com.fossil.ro, com.fossil.mj
    public JSONObject C() {
        return g80.k(super.C(), jd0.T5, this.T.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.lp
    public Object x() {
        return this.T.getFileVersion();
    }
}
