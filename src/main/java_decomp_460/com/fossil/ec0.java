package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ec0 {
    STATIC("static"),
    COMPLICATIONS("complications"),
    GOAL_RINGS("goal_rings");
    
    @DexIgnore
    public static /* final */ dc0 g; // = new dc0(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ec0(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
