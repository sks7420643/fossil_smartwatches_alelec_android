package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.m47;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp6 extends pv5 implements op6, View.OnClickListener, t47.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public g37<j75> g;
    @DexIgnore
    public np6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return pp6.j;
        }

        @DexIgnore
        public final pp6 b() {
            return new pp6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public b(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public c(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String N6;
            if (wr4.f3989a.a().f()) {
                N6 = m47.a(m47.c.FAQ, null);
                pq7.b(N6, "URLHelper.buildStaticPag\u2026per.StaticPage.FAQ, null)");
            } else {
                N6 = this.b.N6("https://support.fossil.com/hc/%s/categories/360000064626-Smartwatch-FAQ");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pp6.k.a();
            local.d(a2, "FAQ URL = " + N6);
            this.b.P6(N6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public d(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String format;
            if (wr4.f3989a.a().f()) {
                format = m47.a(m47.c.REPAIR_CENTER, null);
                pq7.b(format, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
            } else {
                hr7 hr7 = hr7.f1520a;
                Locale a2 = um5.a();
                pq7.b(a2, "LanguageHelper.getLocale()");
                format = String.format("https://c.fossil.com/web/service_centers", Arrays.copyOf(new Object[]{a2.getLanguage()}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
            }
            FLogger.INSTANCE.getLocal().d(pp6.k.a(), "Repair Center URL = https://c.fossil.com/web/service_centers");
            this.b.P6(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public e(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (wr4.f3989a.a().n()) {
                String a2 = m47.a(m47.c.REPAIR_CENTER, null);
                pp6 pp6 = this.b;
                pq7.b(a2, "url");
                pp6.P6(a2);
                return;
            }
            this.b.M6().o();
            this.b.M6().p("Contact Us - From app [Fossil] - [Android]");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public f(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String N6 = this.b.N6("https://support.fossil.com/hc/%s?wearablesChat=true");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pp6.k.a();
            local.d(a2, "Chat URL = " + N6);
            this.b.P6(N6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public g(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String format;
            if (wr4.f3989a.a().f()) {
                format = m47.a(m47.c.CALL, null);
                pq7.b(format, "URLHelper.buildStaticPag\u2026er.StaticPage.CALL, null)");
            } else {
                hr7 hr7 = hr7.f1520a;
                Locale a2 = um5.a();
                pq7.b(a2, "LanguageHelper.getLocale()");
                format = String.format("https://c.fossil.com/web/call", Arrays.copyOf(new Object[]{a2.getLanguage()}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = pp6.k.a();
            local.d(a3, "Call Us URL = " + format);
            this.b.P6(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp6 b;

        @DexIgnore
        public h(pp6 pp6, String str) {
            this.b = pp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                DeleteAccountActivity.a aVar = DeleteAccountActivity.B;
                FragmentActivity requireActivity = this.b.requireActivity();
                pq7.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    /*
    static {
        String simpleName = pp6.class.getSimpleName();
        if (simpleName != null) {
            pq7.b(simpleName, "HelpFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        pq7.i();
        throw null;
    }
    */

    @DexIgnore
    public final np6 M6() {
        np6 np6 = this.h;
        if (np6 != null) {
            return np6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final String N6(String str) {
        pq7.c(str, "featureLink");
        Locale a2 = um5.a();
        pq7.b(a2, "LanguageHelper.getLocale()");
        String language = a2.getLanguage();
        Locale a3 = um5.a();
        pq7.b(a3, "LanguageHelper.getLocale()");
        String country = a3.getCountry();
        if (pq7.a(language, "zh")) {
            if (pq7.a(country, "tw")) {
                hr7 hr7 = hr7.f1520a;
                Locale locale = Locale.US;
                pq7.b(locale, "Locale.US");
                language = String.format(locale, "%s-%s", Arrays.copyOf(new Object[]{language, "tw"}, 2));
                pq7.b(language, "java.lang.String.format(locale, format, *args)");
            } else {
                hr7 hr72 = hr7.f1520a;
                Locale locale2 = Locale.US;
                pq7.b(locale2, "Locale.US");
                language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, "cn"}, 2));
                pq7.b(language, "java.lang.String.format(locale, format, *args)");
            }
        }
        hr7 hr73 = hr7.f1520a;
        String format = String.format(str, Arrays.copyOf(new Object[]{language}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    /* renamed from: O6 */
    public void M5(np6 np6) {
        pq7.c(np6, "presenter");
        i14.l(np6);
        pq7.b(np6, "Preconditions.checkNotNull(presenter)");
        this.h = np6;
    }

    @DexIgnore
    public final void P6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), j);
    }

    @DexIgnore
    @Override // com.fossil.t47.g
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = wt6.x.a();
        local.d(a2, "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof ls5)) {
            activity = null;
        }
        ls5 ls5 = (ls5) activity;
        if (ls5 != null) {
            ls5.R5(str, i2, intent);
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.op6
    public void j0(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        pq7.c(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            np6 np6 = this.h;
            if (np6 != null) {
                np6.n();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        pq7.c(view, "v");
        if (view.getId() == 2131361851) {
            e0();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        j75 j75 = (j75) aq0.f(LayoutInflater.from(getContext()), R.layout.fragment_help, null, false, A6());
        this.g = new g37<>(this, j75);
        pq7.b(j75, "binding");
        return j75.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        np6 np6 = this.h;
        if (np6 != null) {
            np6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        np6 np6 = this.h;
        if (np6 != null) {
            np6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        String P = PortfolioApp.h0.c().P();
        g37<j75> g37 = this.g;
        if (g37 != null) {
            j75 a2 = g37.a();
            if (a2 != null) {
                String d2 = qn5.l.a().d("nonBrandSeparatorLine");
                if (!TextUtils.isEmpty(d2)) {
                    int parseColor = Color.parseColor(d2);
                    a2.J.setBackgroundColor(parseColor);
                    a2.K.setBackgroundColor(parseColor);
                }
                FlexibleTextView flexibleTextView = a2.B;
                pq7.b(flexibleTextView, "binding.tvAppVersion");
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131887105);
                pq7.b(c2, "LanguageHelper.getString\u2026w_Text__AppVersionNumber)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{P}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.setOnClickListener(new b(this, P));
                a2.r.setOnClickListener(new c(this, P));
                a2.s.setOnClickListener(new d(this, P));
                a2.M.setOnClickListener(new e(this, P));
                a2.N.setOnClickListener(new f(this, P));
                a2.L.setOnClickListener(new g(this, P));
                a2.E.setOnClickListener(new h(this, P));
                if (!wr4.f3989a.a().c()) {
                    FlexibleTextView flexibleTextView2 = a2.H;
                    pq7.b(flexibleTextView2, "binding.tvLiveChat");
                    flexibleTextView2.setVisibility(8);
                    CustomizeWidget customizeWidget = a2.N;
                    pq7.b(customizeWidget, "binding.wcLiveChat");
                    customizeWidget.setVisibility(8);
                }
                if (!wr4.f3989a.a().a()) {
                    FlexibleTextView flexibleTextView3 = a2.D;
                    pq7.b(flexibleTextView3, "binding.tvContacts");
                    flexibleTextView3.setVisibility(4);
                    ConstraintLayout constraintLayout = a2.u;
                    pq7.b(constraintLayout, "binding.clContacts");
                    constraintLayout.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
