package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp1 extends vp1 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ bo1 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qp1 createFromParcel(Parcel parcel) {
            return new qp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qp1[] newArray(int i) {
            return new qp1[i];
        }
    }

    @DexIgnore
    public qp1(byte b, int i, bo1 bo1) {
        super(np1.APP_NOTIFICATION_CONTROL, b);
        this.d = bo1;
        this.e = i;
    }

    @DexIgnore
    public /* synthetic */ qp1(Parcel parcel, kq7 kq7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(bo1.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (bo1) readParcelable;
            this.e = parcel.readInt();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(qp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            qp1 qp1 = (qp1) obj;
            if (!pq7.a(this.d, qp1.d)) {
                return false;
            }
            return this.e == qp1.e;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.AppNotificationControlNotification");
    }

    @DexIgnore
    public final bo1 getAction() {
        return this.d;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.d.hashCode()) * 31) + Integer.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.mp1, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(super.toJSONObject(), jd0.v0, this.d.toJSONObject()), jd0.d4, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
