package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr extends lp {
    @DexIgnore
    public /* final */ fv1[] C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ pr(k5 k5Var, i60 i60, fv1[] fv1Arr, String str, int i) {
        super(k5Var, i60, yp.D0, (i & 8) != 0 ? e.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = fv1Arr;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        boolean z = false;
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (fv1 fv1 : this.C) {
            io1[] replyMessages = fv1.getReplyMessageGroup().getReplyMessages();
            for (io1 io1 : replyMessages) {
                oo1 messageIcon = io1.getMessageIcon();
                if (!messageIcon.d()) {
                    copyOnWriteArrayList.addIfAbsent(messageIcon);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new qu1[0]);
        if (array != null) {
            qu1[] qu1Arr = (qu1[]) array;
            if (qu1Arr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    xa xaVar = xa.d;
                    ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.NOTIFICATION.b));
                    if (ry1 == null) {
                        ry1 = hd0.y.d();
                    }
                    lp.h(this, new zj(this.w, this.x, yp.E0, true, 1795, xaVar.h(qu1Arr, 1795, ry1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new zo(this), new mp(this), new zp(this), null, null, 48, null);
                } catch (sx1 e) {
                    d90.i.i(e);
                    l(nr.a(this.v, null, zq.INCOMPATIBLE_FIRMWARE, null, null, 13));
                }
            } else {
                H();
            }
        } else {
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.h5, px1.a(this.C));
    }

    @DexIgnore
    public final void H() {
        short b = ke.b.b(this.w.x, ob.REPLY_MESSAGES_FILE);
        try {
            ka kaVar = ka.d;
            ry1 ry1 = this.x.a().h().get(Short.valueOf(ob.REPLY_MESSAGES_FILE.b));
            if (ry1 == null) {
                ry1 = new ry1(2, 0);
            }
            lp.h(this, new zj(this.w, this.x, yp.F0, true, b, kaVar.a(b, ry1, this.C), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new nq(this), new br(this), null, null, null, 56, null);
        } catch (sx1 e) {
            d90.i.i(e);
            l(nr.a(this.v, null, zq.UNSUPPORTED_FORMAT, null, null, 13));
        }
    }
}
