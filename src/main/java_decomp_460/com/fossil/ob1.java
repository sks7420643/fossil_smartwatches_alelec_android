package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ob1 implements mb1 {
    @DexIgnore
    public /* final */ zi0<nb1<?>, Object> b; // = new ak1();

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public static <T> void f(nb1<T> nb1, Object obj, MessageDigest messageDigest) {
        nb1.g(obj, messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public void a(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            f(this.b.j(i), this.b.n(i), messageDigest);
        }
    }

    @DexIgnore
    public <T> T c(nb1<T> nb1) {
        return this.b.containsKey(nb1) ? (T) this.b.get(nb1) : nb1.c();
    }

    @DexIgnore
    public void d(ob1 ob1) {
        this.b.k(ob1.b);
    }

    @DexIgnore
    public <T> ob1 e(nb1<T> nb1, T t) {
        this.b.put(nb1, t);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public boolean equals(Object obj) {
        if (obj instanceof ob1) {
            return this.b.equals(((ob1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.mb1
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Options{values=" + this.b + '}';
    }
}
