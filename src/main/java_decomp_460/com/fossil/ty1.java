package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3490a;

    @DexIgnore
    public ty1(String str) {
        if (str != null) {
            this.f3490a = str;
            return;
        }
        throw new NullPointerException("name is null");
    }

    @DexIgnore
    public static ty1 b(String str) {
        return new ty1(str);
    }

    @DexIgnore
    public String a() {
        return this.f3490a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ty1)) {
            return false;
        }
        return this.f3490a.equals(((ty1) obj).f3490a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f3490a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Encoding{name=\"" + this.f3490a + "\"}";
    }
}
