package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class db0 extends va0 {
    @DexIgnore
    public static /* final */ cb0 CREATOR; // = new cb0(null);
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    public /* synthetic */ db0(Parcel parcel, kq7 kq7) {
        super(parcel);
        this.c = parcel.readByte();
    }

    @DexIgnore
    @Override // com.fossil.va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        pq7.b(order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c);
        byte[] array = order.array();
        pq7.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(db0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((db0) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.StreamingInstr");
    }

    @DexIgnore
    @Override // com.fossil.va0
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.va0, com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(super.toJSONObject(), jd0.V3, Byte.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
    }
}
