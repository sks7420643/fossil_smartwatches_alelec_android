package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u57 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public short f3517a;
    @DexIgnore
    public short b;

    @DexIgnore
    public final short a() {
        return this.f3517a;
    }

    @DexIgnore
    public final short b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof u57) {
                u57 u57 = (u57) obj;
                if (!(this.f3517a == u57.f3517a && this.b == u57.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.f3517a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateDWMModel(minValue=" + ((int) this.f3517a) + ", maxValue=" + ((int) this.b) + ")";
    }
}
