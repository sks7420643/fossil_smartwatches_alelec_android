package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rp0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<rp0> CREATOR; // = new b();
    @DexIgnore
    public static /* final */ rp0 c; // = new a();
    @DexIgnore
    public /* final */ Parcelable b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends rp0 {
        @DexIgnore
        public a() {
            super((a) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.ClassLoaderCreator<rp0> {
        @DexIgnore
        /* renamed from: a */
        public rp0 createFromParcel(Parcel parcel) {
            return createFromParcel(parcel, null);
        }

        @DexIgnore
        /* renamed from: b */
        public rp0 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return rp0.c;
            }
            throw new IllegalStateException("superState must be null");
        }

        @DexIgnore
        /* renamed from: c */
        public rp0[] newArray(int i) {
            return new rp0[i];
        }
    }

    @DexIgnore
    public rp0() {
        this.b = null;
    }

    @DexIgnore
    public rp0(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.b = readParcelable == null ? c : readParcelable;
    }

    @DexIgnore
    public rp0(Parcelable parcelable) {
        if (parcelable != null) {
            this.b = parcelable == c ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    @DexIgnore
    public /* synthetic */ rp0(a aVar) {
        this();
    }

    @DexIgnore
    public final Parcelable a() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.b, i);
    }
}
