package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka3 extends zc2 implements z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ka3> CREATOR; // = new va3();
    @DexIgnore
    public /* final */ Status b;
    @DexIgnore
    public /* final */ la3 c;

    @DexIgnore
    public ka3(Status status) {
        this(status, null);
    }

    @DexIgnore
    public ka3(Status status, la3 la3) {
        this.b = status;
        this.c = la3;
    }

    @DexIgnore
    @Override // com.fossil.z62
    public final Status a() {
        return this.b;
    }

    @DexIgnore
    public final la3 c() {
        return this.c;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.t(parcel, 1, a(), i, false);
        bd2.t(parcel, 2, c(), i, false);
        bd2.b(parcel, a2);
    }
}
