package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl6 implements Factory<pl6> {
    @DexIgnore
    public static pl6 a(rl6 rl6) {
        pl6 a2 = rl6.a();
        lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
