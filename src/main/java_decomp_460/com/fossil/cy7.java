package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cy7 extends dv7 {
    @DexIgnore
    public static /* final */ cy7 c; // = new cy7();

    @DexIgnore
    @Override // com.fossil.dv7
    public void M(tn7 tn7, Runnable runnable) {
        ey7 ey7 = (ey7) tn7.get(ey7.c);
        if (ey7 != null) {
            ey7.b = true;
            return;
        }
        throw new UnsupportedOperationException("Dispatchers.Unconfined.dispatch function can only be used by the yield function. If you wrap Unconfined dispatcher in your code, make sure you properly delegate isDispatchNeeded and dispatch calls.");
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public boolean Q(tn7 tn7) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.dv7
    public String toString() {
        return "Unconfined";
    }
}
