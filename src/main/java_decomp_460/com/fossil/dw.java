package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw extends fs {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ long C; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ gp7<tl7> E;
    @DexIgnore
    public bw F;
    @DexIgnore
    public /* final */ short G;
    @DexIgnore
    public /* final */ vr H;

    @DexIgnore
    public dw(short s, vr vrVar, k5 k5Var) {
        super(hs.P, k5Var, 0, 4);
        this.G = (short) s;
        this.H = vrVar;
        this.E = new cw(this, k5Var);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject A() {
        return g80.k(super.A(), jd0.o3, Long.valueOf(this.A));
    }

    @DexIgnore
    public final void D() {
        bw bwVar = this.F;
        if (bwVar != null) {
            this.j.removeCallbacks(bwVar);
        }
        bw bwVar2 = this.F;
        if (bwVar2 != null) {
            bwVar2.b = true;
        }
        this.F = null;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void f(long j) {
        this.B = j;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void s(o7 o7Var) {
        if (o7Var.f2638a == n6.FTC) {
            byte[] bArr = o7Var.b;
            if (bArr.length >= 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                if (this.G != order.getShort(1)) {
                    return;
                }
                if (sv.LEGACY_PUT_FILE_EOF.b == b) {
                    D();
                    mw b2 = mw.g.b(kt.k.a(order.get(3)));
                    this.v = mw.a(this.v, null, null, b2.d, null, b2.f, 11);
                    this.A = hy1.o(order.getInt(5));
                    this.g.add(new hw(0, o7Var.f2638a, o7Var.b, g80.k(new JSONObject(), jd0.I0, Long.valueOf(this.A)), 1));
                    m(this.v);
                    return;
                }
                this.g.add(new hw(0, o7Var.f2638a, o7Var.b, null, 9));
                m(mw.a(this.v, null, null, lw.e, null, null, 27));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void v(u5 u5Var) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.v = mw.a(this.v, null, null, mw.g.a(u5Var.e).d, u5Var.e, null, 19);
        a90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        a90 a902 = this.f;
        if (!(a902 == null || (jSONObject2 = a902.n) == null)) {
            g80.k(jSONObject2, jd0.k, ey1.a(lw.b));
        }
        lw lwVar = this.v.d;
        lw lwVar2 = lw.b;
        n(this.p);
        a90 a903 = this.f;
        if (!(a903 == null || (jSONObject = a903.n) == null)) {
            g80.k(jSONObject, jd0.q3, Integer.valueOf(this.H.c()));
        }
        float min = Math.min((((float) this.H.c()) * 1.0f) / ((float) this.H.c), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.H.c() >= this.H.c) {
            this.D = min;
            e(min);
        }
        q();
    }

    @DexIgnore
    @Override // com.fossil.fs
    public u5 w() {
        if (!(this.H.b.remaining() > 0)) {
            return null;
        }
        byte[] a2 = this.H.a();
        if (this.s) {
            a2 = jx.b.c(this.y.x, this.H.f, a2);
        }
        return new j6(this.H.f, a2, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.fs
    public long x() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.fs
    public void y() {
        B();
        C();
        D();
        bw bwVar = new bw(this, this.E);
        this.F = bwVar;
        if (bwVar != null) {
            t().postDelayed(bwVar, this.C);
        }
    }

    @DexIgnore
    @Override // com.fossil.fs
    public JSONObject z() {
        return g80.k(g80.k(super.z(), jd0.q3, Integer.valueOf(this.H.c)), jd0.p3, Integer.valueOf(((wr) this.H).g));
    }
}
