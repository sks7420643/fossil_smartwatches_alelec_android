package com.fossil;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t42 implements i42 {
    @DexIgnore
    @Override // com.fossil.i42
    public final l42 a(Intent intent) {
        return v42.a(intent);
    }

    @DexIgnore
    @Override // com.fossil.i42
    public final t62<Status> b(r62 r62) {
        return v42.f(r62, r62.l(), false);
    }
}
