package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ke2 extends ql2 implements le2 {
    @DexIgnore
    public ke2() {
        super("com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    public static le2 e(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ICertData");
        return queryLocalInterface instanceof le2 ? (le2) queryLocalInterface : new me2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.ql2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            rg2 zzb = zzb();
            parcel2.writeNoException();
            sl2.c(parcel2, zzb);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            int zzc = zzc();
            parcel2.writeNoException();
            parcel2.writeInt(zzc);
            return true;
        }
    }
}
