package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import net.sqlcipher.database.SQLiteDatabase;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class jd0 extends Enum<jd0> {
    @DexIgnore
    public static /* final */ jd0 A;
    @DexIgnore
    public static /* final */ jd0 A0;
    @DexIgnore
    public static /* final */ jd0 A1;
    @DexIgnore
    public static /* final */ jd0 A2;
    @DexIgnore
    public static /* final */ jd0 A3;
    @DexIgnore
    public static /* final */ jd0 A4;
    @DexIgnore
    public static /* final */ jd0 A5;
    @DexIgnore
    public static /* final */ jd0 B;
    @DexIgnore
    public static /* final */ jd0 B0;
    @DexIgnore
    public static /* final */ jd0 B1;
    @DexIgnore
    public static /* final */ jd0 B2;
    @DexIgnore
    public static /* final */ jd0 B3;
    @DexIgnore
    public static /* final */ jd0 B4;
    @DexIgnore
    public static /* final */ jd0 B5;
    @DexIgnore
    public static /* final */ jd0 C;
    @DexIgnore
    public static /* final */ jd0 C0;
    @DexIgnore
    public static /* final */ jd0 C1;
    @DexIgnore
    public static /* final */ jd0 C2;
    @DexIgnore
    public static /* final */ jd0 C3;
    @DexIgnore
    public static /* final */ jd0 C4;
    @DexIgnore
    public static /* final */ jd0 C5;
    @DexIgnore
    public static /* final */ jd0 D;
    @DexIgnore
    public static /* final */ jd0 D0;
    @DexIgnore
    public static /* final */ jd0 D1;
    @DexIgnore
    public static /* final */ jd0 D2;
    @DexIgnore
    public static /* final */ jd0 D3;
    @DexIgnore
    public static /* final */ jd0 D4;
    @DexIgnore
    public static /* final */ jd0 D5;
    @DexIgnore
    public static /* final */ jd0 E;
    @DexIgnore
    public static /* final */ jd0 E0;
    @DexIgnore
    public static /* final */ jd0 E1;
    @DexIgnore
    public static /* final */ jd0 E2;
    @DexIgnore
    public static /* final */ jd0 E3;
    @DexIgnore
    public static /* final */ jd0 E4;
    @DexIgnore
    public static /* final */ jd0 E5;
    @DexIgnore
    public static /* final */ jd0 F;
    @DexIgnore
    public static /* final */ jd0 F0;
    @DexIgnore
    public static /* final */ jd0 F1;
    @DexIgnore
    public static /* final */ jd0 F2;
    @DexIgnore
    public static /* final */ jd0 F3;
    @DexIgnore
    public static /* final */ jd0 F4;
    @DexIgnore
    public static /* final */ jd0 F5;
    @DexIgnore
    public static /* final */ jd0 G;
    @DexIgnore
    public static /* final */ jd0 G0;
    @DexIgnore
    public static /* final */ jd0 G1;
    @DexIgnore
    public static /* final */ jd0 G2;
    @DexIgnore
    public static /* final */ jd0 G3;
    @DexIgnore
    public static /* final */ jd0 G4;
    @DexIgnore
    public static /* final */ jd0 G5;
    @DexIgnore
    public static /* final */ jd0 H;
    @DexIgnore
    public static /* final */ jd0 H0;
    @DexIgnore
    public static /* final */ jd0 H1;
    @DexIgnore
    public static /* final */ jd0 H2;
    @DexIgnore
    public static /* final */ jd0 H3;
    @DexIgnore
    public static /* final */ jd0 H4;
    @DexIgnore
    public static /* final */ jd0 H5;
    @DexIgnore
    public static /* final */ jd0 I;
    @DexIgnore
    public static /* final */ jd0 I0;
    @DexIgnore
    public static /* final */ jd0 I1;
    @DexIgnore
    public static /* final */ jd0 I2;
    @DexIgnore
    public static /* final */ jd0 I3;
    @DexIgnore
    public static /* final */ jd0 I4;
    @DexIgnore
    public static /* final */ jd0 I5;
    @DexIgnore
    public static /* final */ jd0 J;
    @DexIgnore
    public static /* final */ jd0 J0;
    @DexIgnore
    public static /* final */ jd0 J1;
    @DexIgnore
    public static /* final */ jd0 J2;
    @DexIgnore
    public static /* final */ jd0 J3;
    @DexIgnore
    public static /* final */ jd0 J4;
    @DexIgnore
    public static /* final */ jd0 J5;
    @DexIgnore
    public static /* final */ jd0 K;
    @DexIgnore
    public static /* final */ jd0 K0;
    @DexIgnore
    public static /* final */ jd0 K1;
    @DexIgnore
    public static /* final */ jd0 K2;
    @DexIgnore
    public static /* final */ jd0 K3;
    @DexIgnore
    public static /* final */ jd0 K4;
    @DexIgnore
    public static /* final */ jd0 K5;
    @DexIgnore
    public static /* final */ jd0 L;
    @DexIgnore
    public static /* final */ jd0 L0;
    @DexIgnore
    public static /* final */ jd0 L1;
    @DexIgnore
    public static /* final */ jd0 L2;
    @DexIgnore
    public static /* final */ jd0 L3;
    @DexIgnore
    public static /* final */ jd0 L4;
    @DexIgnore
    public static /* final */ jd0 L5;
    @DexIgnore
    public static /* final */ jd0 M;
    @DexIgnore
    public static /* final */ jd0 M0;
    @DexIgnore
    public static /* final */ jd0 M1;
    @DexIgnore
    public static /* final */ jd0 M2;
    @DexIgnore
    public static /* final */ jd0 M3;
    @DexIgnore
    public static /* final */ jd0 M4;
    @DexIgnore
    public static /* final */ jd0 M5;
    @DexIgnore
    public static /* final */ jd0 N;
    @DexIgnore
    public static /* final */ jd0 N0;
    @DexIgnore
    public static /* final */ jd0 N1;
    @DexIgnore
    public static /* final */ jd0 N2;
    @DexIgnore
    public static /* final */ jd0 N3;
    @DexIgnore
    public static /* final */ jd0 N4;
    @DexIgnore
    public static /* final */ jd0 N5;
    @DexIgnore
    public static /* final */ jd0 O;
    @DexIgnore
    public static /* final */ jd0 O0;
    @DexIgnore
    public static /* final */ jd0 O1;
    @DexIgnore
    public static /* final */ jd0 O2;
    @DexIgnore
    public static /* final */ jd0 O3;
    @DexIgnore
    public static /* final */ jd0 O4;
    @DexIgnore
    public static /* final */ jd0 O5;
    @DexIgnore
    public static /* final */ jd0 P;
    @DexIgnore
    public static /* final */ jd0 P0;
    @DexIgnore
    public static /* final */ jd0 P1;
    @DexIgnore
    public static /* final */ jd0 P2;
    @DexIgnore
    public static /* final */ jd0 P3;
    @DexIgnore
    public static /* final */ jd0 P4;
    @DexIgnore
    public static /* final */ jd0 P5;
    @DexIgnore
    public static /* final */ jd0 Q;
    @DexIgnore
    public static /* final */ jd0 Q0;
    @DexIgnore
    public static /* final */ jd0 Q1;
    @DexIgnore
    public static /* final */ jd0 Q2;
    @DexIgnore
    public static /* final */ jd0 Q3;
    @DexIgnore
    public static /* final */ jd0 Q4;
    @DexIgnore
    public static /* final */ jd0 Q5;
    @DexIgnore
    public static /* final */ jd0 R;
    @DexIgnore
    public static /* final */ jd0 R0;
    @DexIgnore
    public static /* final */ jd0 R1;
    @DexIgnore
    public static /* final */ jd0 R2;
    @DexIgnore
    public static /* final */ jd0 R3;
    @DexIgnore
    public static /* final */ jd0 R4;
    @DexIgnore
    public static /* final */ jd0 R5;
    @DexIgnore
    public static /* final */ jd0 S;
    @DexIgnore
    public static /* final */ jd0 S0;
    @DexIgnore
    public static /* final */ jd0 S1;
    @DexIgnore
    public static /* final */ jd0 S2;
    @DexIgnore
    public static /* final */ jd0 S3;
    @DexIgnore
    public static /* final */ jd0 S4;
    @DexIgnore
    public static /* final */ jd0 S5;
    @DexIgnore
    public static /* final */ jd0 T;
    @DexIgnore
    public static /* final */ jd0 T0;
    @DexIgnore
    public static /* final */ jd0 T1;
    @DexIgnore
    public static /* final */ jd0 T2;
    @DexIgnore
    public static /* final */ jd0 T3;
    @DexIgnore
    public static /* final */ jd0 T4;
    @DexIgnore
    public static /* final */ jd0 T5;
    @DexIgnore
    public static /* final */ jd0 U;
    @DexIgnore
    public static /* final */ jd0 U0;
    @DexIgnore
    public static /* final */ jd0 U1;
    @DexIgnore
    public static /* final */ jd0 U2;
    @DexIgnore
    public static /* final */ jd0 U3;
    @DexIgnore
    public static /* final */ jd0 U4;
    @DexIgnore
    public static /* final */ jd0 U5;
    @DexIgnore
    public static /* final */ jd0 V;
    @DexIgnore
    public static /* final */ jd0 V0;
    @DexIgnore
    public static /* final */ jd0 V1;
    @DexIgnore
    public static /* final */ jd0 V2;
    @DexIgnore
    public static /* final */ jd0 V3;
    @DexIgnore
    public static /* final */ jd0 V4;
    @DexIgnore
    public static /* final */ jd0 V5;
    @DexIgnore
    public static /* final */ jd0 W;
    @DexIgnore
    public static /* final */ jd0 W0;
    @DexIgnore
    public static /* final */ jd0 W1;
    @DexIgnore
    public static /* final */ jd0 W2;
    @DexIgnore
    public static /* final */ jd0 W3;
    @DexIgnore
    public static /* final */ jd0 W4;
    @DexIgnore
    public static /* final */ jd0 W5;
    @DexIgnore
    public static /* final */ jd0 X;
    @DexIgnore
    public static /* final */ jd0 X0;
    @DexIgnore
    public static /* final */ jd0 X1;
    @DexIgnore
    public static /* final */ jd0 X2;
    @DexIgnore
    public static /* final */ jd0 X3;
    @DexIgnore
    public static /* final */ jd0 X4;
    @DexIgnore
    public static /* final */ jd0 X5;
    @DexIgnore
    public static /* final */ jd0 Y;
    @DexIgnore
    public static /* final */ jd0 Y0;
    @DexIgnore
    public static /* final */ jd0 Y1;
    @DexIgnore
    public static /* final */ jd0 Y2;
    @DexIgnore
    public static /* final */ jd0 Y3;
    @DexIgnore
    public static /* final */ jd0 Y4;
    @DexIgnore
    public static /* final */ jd0 Y5;
    @DexIgnore
    public static /* final */ jd0 Z;
    @DexIgnore
    public static /* final */ jd0 Z0;
    @DexIgnore
    public static /* final */ jd0 Z1;
    @DexIgnore
    public static /* final */ jd0 Z2;
    @DexIgnore
    public static /* final */ jd0 Z3;
    @DexIgnore
    public static /* final */ jd0 Z4;
    @DexIgnore
    public static /* final */ jd0 Z5;
    @DexIgnore
    public static /* final */ jd0 a0;
    @DexIgnore
    public static /* final */ jd0 a1;
    @DexIgnore
    public static /* final */ jd0 a2;
    @DexIgnore
    public static /* final */ jd0 a3;
    @DexIgnore
    public static /* final */ jd0 a4;
    @DexIgnore
    public static /* final */ jd0 a5;
    @DexIgnore
    public static /* final */ jd0 a6;
    @DexIgnore
    public static /* final */ jd0 b;
    @DexIgnore
    public static /* final */ jd0 b0;
    @DexIgnore
    public static /* final */ jd0 b1;
    @DexIgnore
    public static /* final */ jd0 b2;
    @DexIgnore
    public static /* final */ jd0 b3;
    @DexIgnore
    public static /* final */ jd0 b4;
    @DexIgnore
    public static /* final */ jd0 b5;
    @DexIgnore
    public static /* final */ jd0 b6;
    @DexIgnore
    public static /* final */ jd0 c;
    @DexIgnore
    public static /* final */ jd0 c0;
    @DexIgnore
    public static /* final */ jd0 c1;
    @DexIgnore
    public static /* final */ jd0 c2;
    @DexIgnore
    public static /* final */ jd0 c3;
    @DexIgnore
    public static /* final */ jd0 c4;
    @DexIgnore
    public static /* final */ jd0 c5;
    @DexIgnore
    public static /* final */ jd0 c6;
    @DexIgnore
    public static /* final */ jd0 d;
    @DexIgnore
    public static /* final */ jd0 d0;
    @DexIgnore
    public static /* final */ jd0 d1;
    @DexIgnore
    public static /* final */ jd0 d2;
    @DexIgnore
    public static /* final */ jd0 d3;
    @DexIgnore
    public static /* final */ jd0 d4;
    @DexIgnore
    public static /* final */ jd0 d5;
    @DexIgnore
    public static /* final */ jd0 d6;
    @DexIgnore
    public static /* final */ jd0 e;
    @DexIgnore
    public static /* final */ jd0 e0;
    @DexIgnore
    public static /* final */ jd0 e1;
    @DexIgnore
    public static /* final */ jd0 e2;
    @DexIgnore
    public static /* final */ jd0 e3;
    @DexIgnore
    public static /* final */ jd0 e4;
    @DexIgnore
    public static /* final */ jd0 e5;
    @DexIgnore
    public static /* final */ jd0 e6;
    @DexIgnore
    public static /* final */ jd0 f;
    @DexIgnore
    public static /* final */ jd0 f0;
    @DexIgnore
    public static /* final */ jd0 f1;
    @DexIgnore
    public static /* final */ jd0 f2;
    @DexIgnore
    public static /* final */ jd0 f3;
    @DexIgnore
    public static /* final */ jd0 f4;
    @DexIgnore
    public static /* final */ jd0 f5;
    @DexIgnore
    public static /* final */ /* synthetic */ jd0[] f6;
    @DexIgnore
    public static /* final */ jd0 g;
    @DexIgnore
    public static /* final */ jd0 g0;
    @DexIgnore
    public static /* final */ jd0 g1;
    @DexIgnore
    public static /* final */ jd0 g2;
    @DexIgnore
    public static /* final */ jd0 g3;
    @DexIgnore
    public static /* final */ jd0 g4;
    @DexIgnore
    public static /* final */ jd0 g5;
    @DexIgnore
    public static /* final */ jd0 h;
    @DexIgnore
    public static /* final */ jd0 h0;
    @DexIgnore
    public static /* final */ jd0 h1;
    @DexIgnore
    public static /* final */ jd0 h2;
    @DexIgnore
    public static /* final */ jd0 h3;
    @DexIgnore
    public static /* final */ jd0 h4;
    @DexIgnore
    public static /* final */ jd0 h5;
    @DexIgnore
    public static /* final */ jd0 i;
    @DexIgnore
    public static /* final */ jd0 i0;
    @DexIgnore
    public static /* final */ jd0 i1;
    @DexIgnore
    public static /* final */ jd0 i2;
    @DexIgnore
    public static /* final */ jd0 i3;
    @DexIgnore
    public static /* final */ jd0 i4;
    @DexIgnore
    public static /* final */ jd0 i5;
    @DexIgnore
    public static /* final */ jd0 j;
    @DexIgnore
    public static /* final */ jd0 j0;
    @DexIgnore
    public static /* final */ jd0 j1;
    @DexIgnore
    public static /* final */ jd0 j2;
    @DexIgnore
    public static /* final */ jd0 j3;
    @DexIgnore
    public static /* final */ jd0 j4;
    @DexIgnore
    public static /* final */ jd0 j5;
    @DexIgnore
    public static /* final */ jd0 k;
    @DexIgnore
    public static /* final */ jd0 k0;
    @DexIgnore
    public static /* final */ jd0 k1;
    @DexIgnore
    public static /* final */ jd0 k2;
    @DexIgnore
    public static /* final */ jd0 k3;
    @DexIgnore
    public static /* final */ jd0 k4;
    @DexIgnore
    public static /* final */ jd0 k5;
    @DexIgnore
    public static /* final */ jd0 l;
    @DexIgnore
    public static /* final */ jd0 l0;
    @DexIgnore
    public static /* final */ jd0 l1;
    @DexIgnore
    public static /* final */ jd0 l2;
    @DexIgnore
    public static /* final */ jd0 l3;
    @DexIgnore
    public static /* final */ jd0 l4;
    @DexIgnore
    public static /* final */ jd0 l5;
    @DexIgnore
    public static /* final */ jd0 m;
    @DexIgnore
    public static /* final */ jd0 m0;
    @DexIgnore
    public static /* final */ jd0 m1;
    @DexIgnore
    public static /* final */ jd0 m2;
    @DexIgnore
    public static /* final */ jd0 m3;
    @DexIgnore
    public static /* final */ jd0 m4;
    @DexIgnore
    public static /* final */ jd0 m5;
    @DexIgnore
    public static /* final */ jd0 n;
    @DexIgnore
    public static /* final */ jd0 n0;
    @DexIgnore
    public static /* final */ jd0 n1;
    @DexIgnore
    public static /* final */ jd0 n2;
    @DexIgnore
    public static /* final */ jd0 n3;
    @DexIgnore
    public static /* final */ jd0 n4;
    @DexIgnore
    public static /* final */ jd0 n5;
    @DexIgnore
    public static /* final */ jd0 o;
    @DexIgnore
    public static /* final */ jd0 o0;
    @DexIgnore
    public static /* final */ jd0 o1;
    @DexIgnore
    public static /* final */ jd0 o2;
    @DexIgnore
    public static /* final */ jd0 o3;
    @DexIgnore
    public static /* final */ jd0 o4;
    @DexIgnore
    public static /* final */ jd0 o5;
    @DexIgnore
    public static /* final */ jd0 p;
    @DexIgnore
    public static /* final */ jd0 p0;
    @DexIgnore
    public static /* final */ jd0 p1;
    @DexIgnore
    public static /* final */ jd0 p2;
    @DexIgnore
    public static /* final */ jd0 p3;
    @DexIgnore
    public static /* final */ jd0 p4;
    @DexIgnore
    public static /* final */ jd0 p5;
    @DexIgnore
    public static /* final */ jd0 q;
    @DexIgnore
    public static /* final */ jd0 q0;
    @DexIgnore
    public static /* final */ jd0 q1;
    @DexIgnore
    public static /* final */ jd0 q2;
    @DexIgnore
    public static /* final */ jd0 q3;
    @DexIgnore
    public static /* final */ jd0 q4;
    @DexIgnore
    public static /* final */ jd0 q5;
    @DexIgnore
    public static /* final */ jd0 r;
    @DexIgnore
    public static /* final */ jd0 r0;
    @DexIgnore
    public static /* final */ jd0 r1;
    @DexIgnore
    public static /* final */ jd0 r2;
    @DexIgnore
    public static /* final */ jd0 r3;
    @DexIgnore
    public static /* final */ jd0 r4;
    @DexIgnore
    public static /* final */ jd0 r5;
    @DexIgnore
    public static /* final */ jd0 s;
    @DexIgnore
    public static /* final */ jd0 s0;
    @DexIgnore
    public static /* final */ jd0 s1;
    @DexIgnore
    public static /* final */ jd0 s2;
    @DexIgnore
    public static /* final */ jd0 s3;
    @DexIgnore
    public static /* final */ jd0 s4;
    @DexIgnore
    public static /* final */ jd0 s5;
    @DexIgnore
    public static /* final */ jd0 t;
    @DexIgnore
    public static /* final */ jd0 t0;
    @DexIgnore
    public static /* final */ jd0 t1;
    @DexIgnore
    public static /* final */ jd0 t2;
    @DexIgnore
    public static /* final */ jd0 t3;
    @DexIgnore
    public static /* final */ jd0 t4;
    @DexIgnore
    public static /* final */ jd0 t5;
    @DexIgnore
    public static /* final */ jd0 u;
    @DexIgnore
    public static /* final */ jd0 u0;
    @DexIgnore
    public static /* final */ jd0 u1;
    @DexIgnore
    public static /* final */ jd0 u2;
    @DexIgnore
    public static /* final */ jd0 u3;
    @DexIgnore
    public static /* final */ jd0 u4;
    @DexIgnore
    public static /* final */ jd0 u5;
    @DexIgnore
    public static /* final */ jd0 v;
    @DexIgnore
    public static /* final */ jd0 v0;
    @DexIgnore
    public static /* final */ jd0 v1;
    @DexIgnore
    public static /* final */ jd0 v2;
    @DexIgnore
    public static /* final */ jd0 v3;
    @DexIgnore
    public static /* final */ jd0 v4;
    @DexIgnore
    public static /* final */ jd0 v5;
    @DexIgnore
    public static /* final */ jd0 w;
    @DexIgnore
    public static /* final */ jd0 w0;
    @DexIgnore
    public static /* final */ jd0 w1;
    @DexIgnore
    public static /* final */ jd0 w2;
    @DexIgnore
    public static /* final */ jd0 w3;
    @DexIgnore
    public static /* final */ jd0 w4;
    @DexIgnore
    public static /* final */ jd0 w5;
    @DexIgnore
    public static /* final */ jd0 x;
    @DexIgnore
    public static /* final */ jd0 x0;
    @DexIgnore
    public static /* final */ jd0 x1;
    @DexIgnore
    public static /* final */ jd0 x2;
    @DexIgnore
    public static /* final */ jd0 x3;
    @DexIgnore
    public static /* final */ jd0 x4;
    @DexIgnore
    public static /* final */ jd0 x5;
    @DexIgnore
    public static /* final */ jd0 y;
    @DexIgnore
    public static /* final */ jd0 y0;
    @DexIgnore
    public static /* final */ jd0 y1;
    @DexIgnore
    public static /* final */ jd0 y2;
    @DexIgnore
    public static /* final */ jd0 y3;
    @DexIgnore
    public static /* final */ jd0 y4;
    @DexIgnore
    public static /* final */ jd0 y5;
    @DexIgnore
    public static /* final */ jd0 z;
    @DexIgnore
    public static /* final */ jd0 z0;
    @DexIgnore
    public static /* final */ jd0 z1;
    @DexIgnore
    public static /* final */ jd0 z2;
    @DexIgnore
    public static /* final */ jd0 z3;
    @DexIgnore
    public static /* final */ jd0 z4;
    @DexIgnore
    public static /* final */ jd0 z5;

    /*
    static {
        jd0 jd0 = new jd0("DEVICE_INFO", 0);
        b = jd0;
        jd0 jd02 = new jd0("RSSI", 1);
        c = jd02;
        jd0 jd03 = new jd0("NOTIFICATION", 2);
        d = jd03;
        jd0 jd04 = new jd0("TYPE", 3);
        e = jd04;
        jd0 jd05 = new jd0("UID", 4);
        f = jd05;
        jd0 jd06 = new jd0("APP_NAME", 5);
        g = jd06;
        jd0 jd07 = new jd0(ShareConstants.TITLE, 6);
        h = jd07;
        jd0 jd08 = new jd0("SENDER", 7);
        i = jd08;
        jd0 jd09 = new jd0("SENDER_ID", 8);
        j = jd09;
        jd0 jd010 = new jd0("MESSAGE", 9);
        k = jd010;
        jd0 jd011 = new jd0("FLAGS", 10);
        l = jd011;
        jd0 jd012 = new jd0("RECEIVED_TIMESTAMP_IN_SECOND", 11);
        m = jd012;
        jd0 jd013 = new jd0("REPLY_MESSAGES", 12);
        n = jd013;
        jd0 jd014 = new jd0("DEVICE_RESPONSE", 13);
        jd0 jd015 = new jd0("REQUEST", 14);
        o = jd015;
        jd0 jd016 = new jd0("RESPONSE", 15);
        p = jd016;
        jd0 jd017 = new jd0("REQUEST_ID", 16);
        q = jd017;
        jd0 jd018 = new jd0("COMPLICATION_NAME", 17);
        jd0 jd019 = new jd0("CHANCE_OF_RAIN", 18);
        r = jd019;
        jd0 jd020 = new jd0("DESTINATION", 19);
        s = jd020;
        jd0 jd021 = new jd0("MINUTES", 20);
        jd0 jd022 = new jd0("WEATHER_CONDITION", 21);
        t = jd022;
        jd0 jd023 = new jd0("TEMP_UNIT", 22);
        u = jd023;
        jd0 jd024 = new jd0("TEMP", 23);
        jd0 jd025 = new jd0("TRACK_INFO", 24);
        v = jd025;
        jd0 jd026 = new jd0("VOLUME", 25);
        w = jd026;
        jd0 jd027 = new jd0("ARTIST", 26);
        x = jd027;
        jd0 jd028 = new jd0("ALBUM", 27);
        y = jd028;
        jd0 jd029 = new jd0("ALARMS", 28);
        z = jd029;
        jd0 jd030 = new jd0("HOUR", 29);
        A = jd030;
        jd0 jd031 = new jd0("MINUTE", 30);
        B = jd031;
        jd0 jd032 = new jd0("DAYS", 31);
        C = jd032;
        jd0 jd033 = new jd0("REPEAT", 32);
        D = jd033;
        jd0 jd034 = new jd0("ALLOW_SNOOZE", 33);
        E = jd034;
        jd0 jd035 = new jd0("ENABLE", 34);
        F = jd035;
        jd0 jd036 = new jd0("BACKGROUND_IMAGE_CONFIG", 35);
        G = jd036;
        jd0 jd037 = new jd0("MAIN", 36);
        jd0 jd038 = new jd0("TOP_COMPLICATION", 37);
        jd0 jd039 = new jd0("RIGHT_COMPLICATION", 38);
        jd0 jd040 = new jd0("BOTTOM_COMPLICATION", 39);
        jd0 jd041 = new jd0("LEFT_COMPLICATION", 40);
        jd0 jd042 = new jd0("TOP", 41);
        jd0 jd043 = new jd0("RIGHT", 42);
        jd0 jd044 = new jd0("BOTTOM", 43);
        jd0 jd045 = new jd0("LEFT", 44);
        jd0 jd046 = new jd0("MIDDLE", 45);
        jd0 jd047 = new jd0("NAME", 46);
        H = jd047;
        jd0 jd048 = new jd0("FILE_SIZE", 47);
        I = jd048;
        jd0 jd049 = new jd0("FILE_CRC", 48);
        J = jd049;
        jd0 jd050 = new jd0("COMPLICATION_CONFIG", 49);
        jd0 jd051 = new jd0("LOCATION", 50);
        K = jd051;
        jd0 jd052 = new jd0("UTC_OFFSET_IN_MINUTES", 51);
        jd0 jd053 = new jd0("CONFIG", 52);
        jd0 jd054 = new jd0("INTERVAL", 53);
        L = jd054;
        jd0 jd055 = new jd0("MAX_INTERVAL", 54);
        M = jd055;
        jd0 jd056 = new jd0("MIN_INTERVAL", 55);
        N = jd056;
        jd0 jd057 = new jd0("LATENCY", 56);
        O = jd057;
        jd0 jd058 = new jd0("TIMEOUT", 57);
        P = jd058;
        jd0 jd059 = new jd0("PRIORITY", 58);
        Q = jd059;
        jd0 jd060 = new jd0("CONFIGS", 59);
        R = jd060;
        jd0 jd061 = new jd0("AFFECTED_CONFIGS", 60);
        S = jd061;
        jd0 jd062 = new jd0("GROUP_ID", 61);
        T = jd062;
        jd0 jd063 = new jd0("NOTIFICATION_FILTERS", 62);
        U = jd063;
        jd0 jd064 = new jd0("WATCH_APP_CONFIG", 63);
        V = jd064;
        jd0 jd065 = new jd0("FITNESS_DATA", 64);
        W = jd065;
        jd0 jd066 = new jd0("START_TIME", 65);
        X = jd066;
        jd0 jd067 = new jd0("END_TIME", 66);
        Y = jd067;
        jd0 jd068 = new jd0("TIMEZONE_OFFSET_IN_SECOND", 67);
        Z = jd068;
        jd0 jd069 = new jd0("TOTAL_DISTANCE", 68);
        a0 = jd069;
        jd0 jd070 = new jd0("TOTAL_CALORIES", 69);
        b0 = jd070;
        jd0 jd071 = new jd0("TOTAL_STEPS", 70);
        c0 = jd071;
        jd0 jd072 = new jd0("TOTAL_ACTIVE_MINUTES", 71);
        d0 = jd072;
        jd0 jd073 = new jd0("SLEEP_SESSION", 72);
        e0 = jd073;
        jd0 jd074 = new jd0("WORKOUT_SESSION", 73);
        f0 = jd074;
        jd0 jd075 = new jd0("HEART_RATE_RECORD", 74);
        g0 = jd075;
        jd0 jd076 = new jd0("RESTING", 75);
        h0 = jd076;
        jd0 jd077 = new jd0("GOAL_TRACKING", 76);
        i0 = jd077;
        jd0 jd078 = new jd0("CONNECTION_PARAMS", 77);
        jd0 jd079 = new jd0("EXCHANGED_CONNECTION_PARAMS", 78);
        jd0 jd080 = new jd0("DATA_TYPE", 79);
        j0 = jd080;
        jd0 jd081 = new jd0("MAC_ADDRESS", 80);
        k0 = jd081;
        jd0 jd082 = new jd0("SERIAL_NUMBER", 81);
        l0 = jd082;
        jd0 jd083 = new jd0("HARDWARE_REVISION", 82);
        m0 = jd083;
        jd0 jd084 = new jd0("FIRMWARE_VERSION", 83);
        n0 = jd084;
        jd0 jd085 = new jd0("MODEL_NUMBER", 84);
        o0 = jd085;
        jd0 jd086 = new jd0("HEART_RATE_SERIAL_NUMBER", 85);
        p0 = jd086;
        jd0 jd087 = new jd0("BOOT_LOADER_VERSION", 86);
        q0 = jd087;
        jd0 jd088 = new jd0("WATCH_APP_VERSION", 87);
        r0 = jd088;
        jd0 jd089 = new jd0("FONT_VERSION", 88);
        s0 = jd089;
        jd0 jd090 = new jd0("LUTS_VERSION", 89);
        jd0 jd091 = new jd0("SUPPORTED_DEVICE_CONFIG", 90);
        t0 = jd091;
        jd0 jd092 = new jd0("MUSIC_EVENT", 91);
        u0 = jd092;
        jd0 jd093 = new jd0(ShareConstants.ACTION, 92);
        v0 = jd093;
        jd0 jd094 = new jd0("STATUS", 93);
        w0 = jd094;
        jd0 jd095 = new jd0("SKIP_RESUME", 94);
        x0 = jd095;
        jd0 jd096 = new jd0("KEY", 95);
        y0 = jd096;
        jd0 jd097 = new jd0("VALUE", 96);
        z0 = jd097;
        jd0 jd098 = new jd0("FILE_HANDLE", 97);
        A0 = jd098;
        jd0 jd099 = new jd0("NEW_STATE", 98);
        B0 = jd099;
        jd0 jd0100 = new jd0("PREV_STATE", 99);
        C0 = jd0100;
        jd0 jd0101 = new jd0("NEW_BOND_STATE", 100);
        D0 = jd0101;
        jd0 jd0102 = new jd0("RESPONSES", 101);
        E0 = jd0102;
        jd0 jd0103 = new jd0("DATA_SIZE", 102);
        F0 = jd0103;
        jd0 jd0104 = new jd0("SOCKET_ID", 103);
        G0 = jd0104;
        jd0 jd0105 = new jd0("PROCESSED_DATA_LENGTH", 104);
        H0 = jd0105;
        jd0 jd0106 = new jd0("TRANSFERRED_DATA_CRC", 105);
        jd0 jd0107 = new jd0("WRITTEN_SIZE", 106);
        I0 = jd0107;
        jd0 jd0108 = new jd0("WRITTEN_DATA_CRC", 107);
        J0 = jd0108;
        jd0 jd0109 = new jd0("OPTIMAL_PAYLOAD", 108);
        K0 = jd0109;
        jd0 jd0110 = new jd0("VERIFIED_DATA_OFFSET", 109);
        L0 = jd0110;
        jd0 jd0111 = new jd0("VERIFIED_DATA_LENGTH", 110);
        M0 = jd0111;
        jd0 jd0112 = new jd0("VERIFIED_DATA_CRC", 111);
        N0 = jd0112;
        jd0 jd0113 = new jd0("RESULT_CODE", 112);
        O0 = jd0113;
        jd0 jd0114 = new jd0("ERROR_DETAIL", 113);
        P0 = jd0114;
        jd0 jd0115 = new jd0("TIMESTAMP", 114);
        Q0 = jd0115;
        jd0 jd0116 = new jd0("CHANNEL_ID", 115);
        R0 = jd0116;
        jd0 jd0117 = new jd0("RAW_DATA", 116);
        S0 = jd0117;
        jd0 jd0118 = new jd0("RAW_DATA_LENGTH", 117);
        T0 = jd0118;
        jd0 jd0119 = new jd0("DATA_CRC", 118);
        U0 = jd0119;
        jd0 jd0120 = new jd0("PACKAGE_COUNT", 119);
        V0 = jd0120;
        jd0 jd0121 = new jd0("MOVING_TYPE", 120);
        W0 = jd0121;
        jd0 jd0122 = new jd0("HAND_ID", 121);
        X0 = jd0122;
        jd0 jd0123 = new jd0("DEGREE", 122);
        Y0 = jd0123;
        jd0 jd0124 = new jd0("DIRECTION", 123);
        Z0 = jd0124;
        jd0 jd0125 = new jd0("SPEED", 124);
        a1 = jd0125;
        jd0 jd0126 = new jd0("HAND_CONFIGS", 125);
        b1 = jd0126;
        jd0 jd0127 = new jd0("OFFSET", 126);
        c1 = jd0127;
        jd0 jd0128 = new jd0("LENGTH", 127);
        d1 = jd0128;
        jd0 jd0129 = new jd0("TOTAL_LENGTH", 128);
        e1 = jd0129;
        jd0 jd0130 = new jd0("AUTO_CONNECT", 129);
        f1 = jd0130;
        jd0 jd0131 = new jd0("SERVICES", 130);
        g1 = jd0131;
        jd0 jd0132 = new jd0("CHARACTERISTICS", 131);
        h1 = jd0132;
        jd0 jd0133 = new jd0("PROPOSED_TIMEOUT", 132);
        i1 = jd0133;
        jd0 jd0134 = new jd0("REQUESTED_MTU", 133);
        j1 = jd0134;
        jd0 jd0135 = new jd0("EXCHANGED_MTU", 134);
        k1 = jd0135;
        jd0 jd0136 = new jd0("DEVICE_EVENT", 135);
        l1 = jd0136;
        jd0 jd0137 = new jd0("DEVICE", 136);
        m1 = jd0137;
        jd0 jd0138 = new jd0(ux6.w, 137);
        n1 = jd0138;
        jd0 jd0139 = new jd0("CURRENT_BOND_STATE", 138);
        o1 = jd0139;
        jd0 jd0140 = new jd0("CURRENT_HID_STATE", 139);
        p1 = jd0140;
        jd0 jd0141 = new jd0("NEW_HID_STATE", ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        q1 = jd0141;
        jd0 jd0142 = new jd0("IS_CACHED", 141);
        r1 = jd0142;
        jd0 jd0143 = new jd0("PERIPHERAL_CURRENT_STATE", 142);
        s1 = jd0143;
        jd0 jd0144 = new jd0("STARTED_AT", 143);
        t1 = jd0144;
        jd0 jd0145 = new jd0("COMPLETED_AT", 144);
        u1 = jd0145;
        jd0 jd0146 = new jd0("REQUEST_TIMEOUT_IN_MS", 145);
        v1 = jd0146;
        jd0 jd0147 = new jd0("FILE_LIST", 146);
        w1 = jd0147;
        jd0 jd0148 = new jd0("MESSAGE_LENGTH", 147);
        x1 = jd0148;
        jd0 jd0149 = new jd0("MESSAGE_CRC", 148);
        y1 = jd0149;
        jd0 jd0150 = new jd0("SEQUENCE", 149);
        z1 = jd0150;
        jd0 jd0151 = new jd0("HEARTBEAT_STATISTIC", 150);
        jd0 jd0152 = new jd0("HEARTBEATS_SENT", 151);
        jd0 jd0153 = new jd0("HEARTBEATS_RECEIVED", 152);
        jd0 jd0154 = new jd0("HEARTBEAT_INTERVAL_IN_MS", 153);
        A1 = jd0154;
        jd0 jd0155 = new jd0("HEARTBEAT", 154);
        jd0 jd0156 = new jd0("PHASE_RESULT", 155);
        B1 = jd0156;
        jd0 jd0157 = new jd0("CREATED_AT", 156);
        C1 = jd0157;
        jd0 jd0158 = new jd0("GATT_CONNECTED_DEVICES", 157);
        D1 = jd0158;
        jd0 jd0159 = new jd0("HID_CONNECTED_DEVICES", 158);
        E1 = jd0159;
        jd0 jd0160 = new jd0("SCAN_CALLBACK", 159);
        F1 = jd0160;
        jd0 jd0161 = new jd0("SCAN_ERROR", 160);
        G1 = jd0161;
        jd0 jd0162 = new jd0("FILTER_TYPE", 161);
        H1 = jd0162;
        jd0 jd0163 = new jd0("SERIAL_NUMBER_PREFIXES", 162);
        I1 = jd0163;
        jd0 jd0164 = new jd0("DEVICE_TYPES", 163);
        J1 = jd0164;
        jd0 jd0165 = new jd0("SERIAL_NUMBER_REGEX", 164);
        K1 = jd0165;
        jd0 jd0166 = new jd0("SCAN_FILTER", 165);
        L1 = jd0166;
        jd0 jd0167 = new jd0("DEVICE_FILTER", 166);
        M1 = jd0167;
        jd0 jd0168 = new jd0("TOGGLE_RING_MY_PHONE", 167);
        jd0 jd0169 = new jd0("MUSIC_CONTROL_NOTIFICATION", DateTimeConstants.HOURS_PER_WEEK);
        jd0 jd0170 = new jd0("BACKGROUND_SYNC", 169);
        jd0 jd0171 = new jd0("FRAMES", 170);
        N1 = jd0171;
        jd0 jd0172 = new jd0("TOTAL_FRAMES", 171);
        O1 = jd0172;
        jd0 jd0173 = new jd0("TOTAL_SLEEP_IN_MINUTE", 172);
        P1 = jd0173;
        jd0 jd0174 = new jd0("AWAKE_IN_MINUTE", 173);
        Q1 = jd0174;
        jd0 jd0175 = new jd0("LIGHT_SLEEP_IN_MINUTE", 174);
        R1 = jd0175;
        jd0 jd0176 = new jd0("DEEP_SLEEP_IN_MINUTE", 175);
        S1 = jd0176;
        jd0 jd0177 = new jd0("TEMPERATURE", 176);
        T1 = jd0177;
        jd0 jd0178 = new jd0("CALORIES", 177);
        U1 = jd0178;
        jd0 jd0179 = new jd0("DISTANCE", 178);
        V1 = jd0179;
        jd0 jd0180 = new jd0("TIME", 179);
        W1 = jd0180;
        jd0 jd0181 = new jd0("DATE", 180);
        X1 = jd0181;
        jd0 jd0182 = new jd0("CURRENT_TEMPERATURE", 181);
        Y1 = jd0182;
        jd0 jd0183 = new jd0("HIGH_TEMPERATURE", 182);
        Z1 = jd0183;
        jd0 jd0184 = new jd0("LOW_TEMPERATURE", 183);
        a2 = jd0184;
        jd0 jd0185 = new jd0("WEEKDAY", 184);
        b2 = jd0185;
        jd0 jd0186 = new jd0("CURRENT_WEATHER_INFO", 185);
        c2 = jd0186;
        jd0 jd0187 = new jd0("HOURLY_FORECAST", 186);
        d2 = jd0187;
        jd0 jd0188 = new jd0("DAILY_FORECAST", 187);
        e2 = jd0188;
        jd0 jd0189 = new jd0("WEATHER_CONFIGS", 188);
        f2 = jd0189;
        jd0 jd0190 = new jd0("QUERY_STRING", 189);
        jd0 jd0191 = new jd0("ERROR", FacebookRequestErrorClassification.EC_INVALID_TOKEN);
        jd0 jd0192 = new jd0("REQUEST_UUID", 191);
        g2 = jd0192;
        jd0 jd0193 = new jd0("CORRECT_OFFSET", 192);
        h2 = jd0193;
        jd0 jd0194 = new jd0("DEVICE_FILE", 193);
        i2 = jd0194;
        jd0 jd0195 = new jd0("FILE_VERSION", 194);
        j2 = jd0195;
        jd0 jd0196 = new jd0("CONNECT_DURATION", 195);
        k2 = jd0196;
        jd0 jd0197 = new jd0("CONNECT_COUNT", 196);
        l2 = jd0197;
        jd0 jd0198 = new jd0("DISCONNECT_COUNT", 197);
        m2 = jd0198;
        jd0 jd0199 = new jd0("MSL_STATUS_CODE", 198);
        n2 = jd0199;
        jd0 jd0200 = new jd0("PHONE_RANDOM_NUMBER", Action.Music.MUSIC_END_ACTION);
        o2 = jd0200;
        jd0 jd0201 = new jd0("DEVICE_RANDOM_NUMBER", 200);
        p2 = jd0201;
        jd0 jd0202 = new jd0("BOTH_SIDES_RANDOM_NUMBERS", 201);
        q2 = jd0202;
        jd0 jd0203 = new jd0("PHONE_PUBLIC_KEY", Action.Selfie.TAKE_BURST);
        r2 = jd0203;
        jd0 jd0204 = new jd0("DEVICE_PUBLIC_KEY", 203);
        s2 = jd0204;
        jd0 jd0205 = new jd0("SECRET_KEY_CRC", 204);
        t2 = jd0205;
        jd0 jd0206 = new jd0("SECRET_KEY", 205);
        jd0 jd0207 = new jd0("AUTHENTICATION_KEY_TYPE", 206);
        u2 = jd0207;
        jd0 jd0208 = new jd0("DEVICE_SECURITY_VERSION", 207);
        v2 = jd0208;
        jd0 jd0209 = new jd0("TIMEZONE_OFFSET_IN_MINUTE", 208);
        w2 = jd0209;
        jd0 jd0210 = new jd0("HOUR_DEGREE", 209);
        x2 = jd0210;
        jd0 jd0211 = new jd0("MINUTE_DEGREE", 210);
        y2 = jd0211;
        jd0 jd0212 = new jd0("SUBEYE_DEGREE", 211);
        z2 = jd0212;
        jd0 jd0213 = new jd0("DURATION_IN_MS", 212);
        A2 = jd0213;
        jd0 jd0214 = new jd0("HAND_MOVING_CONFIG", 213);
        B2 = jd0214;
        jd0 jd0215 = new jd0("VIBE_PATTERN", 214);
        C2 = jd0215;
        jd0 jd0216 = new jd0("CHARACTERISTIC", 215);
        D2 = jd0216;
        jd0 jd0217 = new jd0("DESCRIPTOR", 216);
        jd0 jd0218 = new jd0("PREPARED_WRITE", 217);
        jd0 jd0219 = new jd0("RESPONSE_NEEDED", 218);
        jd0 jd0220 = new jd0("NEED_CONFIRM", 219);
        jd0 jd0221 = new jd0("COMMAND_ID", 220);
        E2 = jd0221;
        jd0 jd0222 = new jd0("GATT_RESULT", 221);
        F2 = jd0222;
        jd0 jd0223 = new jd0("SERVICE", 222);
        jd0 jd0224 = new jd0("EXECUTE", 223);
        jd0 jd0225 = new jd0("SECOND", 224);
        G2 = jd0225;
        jd0 jd0226 = new jd0("MILLISECOND", 225);
        H2 = jd0226;
        jd0 jd0227 = new jd0("SUPPORTED_FILES_VERSION", 226);
        I2 = jd0227;
        jd0 jd0228 = new jd0("FILE_TYPE", 227);
        J2 = jd0228;
        jd0 jd0229 = new jd0("VERSION", 228);
        K2 = jd0229;
        jd0 jd0230 = new jd0("DEVICE_TYPE", 229);
        L2 = jd0230;
        jd0 jd0231 = new jd0("EXPIRED_TIMESTAMP_IN_SECOND", 230);
        M2 = jd0231;
        jd0 jd0232 = new jd0("IS_COMPLETED", 231);
        N2 = jd0232;
        jd0 jd0233 = new jd0("EVENT_ID", 232);
        O2 = jd0233;
        jd0 jd0234 = new jd0("FILES", 233);
        P2 = jd0234;
        jd0 jd0235 = new jd0("ACTUAL_BYTE_WRITTEN", 234);
        jd0 jd0236 = new jd0("SEGMENT_OFFSET", 235);
        Q2 = jd0236;
        jd0 jd0237 = new jd0("SEGMENT_LENGTH", 236);
        R2 = jd0237;
        jd0 jd0238 = new jd0("TOTAL_FILE_LENGTH", 237);
        S2 = jd0238;
        jd0 jd0239 = new jd0("SEGMENT_CRC", 238);
        T2 = jd0239;
        jd0 jd0240 = new jd0("PAGE_OFFSET", 239);
        U2 = jd0240;
        jd0 jd0241 = new jd0("NEW_SIZE_WRITTEN", 240);
        V2 = jd0241;
        jd0 jd0242 = new jd0("OTA_ENTER_RESPONSE", 241);
        jd0 jd0243 = new jd0("APP_BUNDLE_CRC", 242);
        W2 = jd0243;
        jd0 jd0244 = new jd0("APP_PACKAGE_NAME", 243);
        X2 = jd0244;
        jd0 jd0245 = new jd0("DATA", 244);
        Y2 = jd0245;
        jd0 jd0246 = new jd0("DEVICE_DATA", 245);
        Z2 = jd0246;
        jd0 jd0247 = new jd0("DEFAULT_ICON", 246);
        a3 = jd0247;
        jd0 jd0248 = new jd0("ICON_CONFIG", 247);
        b3 = jd0248;
        jd0 jd0249 = new jd0("NOTIFICATION_ICONS", 248);
        c3 = jd0249;
        jd0 jd0250 = new jd0("REQUEST_DATA", 249);
        d3 = jd0250;
        jd0 jd0251 = new jd0("LANGUAGE_CODE", 250);
        e3 = jd0251;
        jd0 jd0252 = new jd0("FILE_CRC_C", 251);
        f3 = jd0252;
        jd0 jd0253 = new jd0("PRESET", 252);
        g3 = jd0253;
        jd0 jd0254 = new jd0("NUMBER_OF_FILES", 253);
        h3 = jd0254;
        jd0 jd0255 = new jd0("TOTAL_FILE_SIZE", 254);
        i3 = jd0255;
        jd0 jd0256 = new jd0("SKIP_READ_ACTIVITY_FILES", 255);
        j3 = jd0256;
        jd0 jd0257 = new jd0("SKIP_ERASE_ACTIVITY_FILES", 256);
        k3 = jd0257;
        jd0 jd0258 = new jd0("RESPONSE_STATUS", 257);
        l3 = jd0258;
        jd0 jd0259 = new jd0("LOCALE", 258);
        m3 = jd0259;
        jd0 jd0260 = new jd0("LOCALIZATION_FILE", 259);
        n3 = jd0260;
        jd0 jd0261 = new jd0("ACTUAL_WRITTEN_SIZE", 260);
        o3 = jd0261;
        jd0 jd0262 = new jd0("HEADER_LENGTH", 261);
        p3 = jd0262;
        jd0 jd0263 = new jd0("TRANSFERRED_DATA_SIZE", 262);
        q3 = jd0263;
        jd0 jd0264 = new jd0("SIZE_WRITTEN", 263);
        r3 = jd0264;
        jd0 jd0265 = new jd0("BOND_REQUIRED", 264);
        s3 = jd0265;
        jd0 jd0266 = new jd0("LOCALE_VERSION", 265);
        jd0 jd0267 = new jd0("MICRO_APP_VERSION", 266);
        t3 = jd0267;
        jd0 jd0268 = new jd0("FAST_PAIR_ID_HEX_STRING", 267);
        u3 = jd0268;
        jd0 jd0269 = new jd0("MICRO_APP_EVENT", 268);
        v3 = jd0269;
        jd0 jd0270 = new jd0("SHIP_HANDS_TO_TWELVE", 269);
        w3 = jd0270;
        jd0 jd0271 = new jd0("TRAVEL_TIME_IN_MINUTE", 270);
        x3 = jd0271;
        jd0 jd0272 = new jd0("CURRENT_HEART_RATE", 271);
        y3 = jd0272;
        jd0 jd0273 = new jd0("WATCH_PARAMETERS_FILE", 272);
        z3 = jd0273;
        jd0 jd0274 = new jd0("BACKGROUND_IMAGES", 273);
        A3 = jd0274;
        jd0 jd0275 = new jd0("IS_VALID_SECRET_KEY", 274);
        B3 = jd0275;
        jd0 jd0276 = new jd0("BUTTON", 275);
        C3 = jd0276;
        jd0 jd0277 = new jd0("DECLARATIONS", 276);
        D3 = jd0277;
        jd0 jd0278 = new jd0("TOTAL_DECLARATIONS", 277);
        E3 = jd0278;
        jd0 jd0279 = new jd0("CUSTOMIZATION", 278);
        jd0 jd0280 = new jd0("MICRO_APP_ID", 279);
        F3 = jd0280;
        jd0 jd0281 = new jd0("MINOR_VERSION", 280);
        G3 = jd0281;
        jd0 jd0282 = new jd0("MAJOR_VERSION", 281);
        H3 = jd0282;
        jd0 jd0283 = new jd0("VARIATION_NUMBER", 282);
        I3 = jd0283;
        jd0 jd0284 = new jd0("VARIANT", 283);
        J3 = jd0284;
        jd0 jd0285 = new jd0("RUN_TIME", 284);
        K3 = jd0285;
        jd0 jd0286 = new jd0("HAS_CUSTOMIZATION", 285);
        L3 = jd0286;
        jd0 jd0287 = new jd0("CRC", 286);
        M3 = jd0287;
        jd0 jd0288 = new jd0("GOAL_ID", 287);
        N3 = jd0288;
        jd0 jd0289 = new jd0("CONTEXT_NUMBER", 288);
        O3 = jd0289;
        jd0 jd0290 = new jd0("ACTIVITY_ID", 289);
        P3 = jd0290;
        jd0 jd0291 = new jd0("INSTRUCTION_ID", 290);
        Q3 = jd0291;
        jd0 jd0292 = new jd0("ROTATION", 291);
        R3 = jd0292;
        jd0 jd0293 = new jd0("ANIMATIONS", 292);
        S3 = jd0293;
        jd0 jd0294 = new jd0("IS_RESUME", 293);
        T3 = jd0294;
        jd0 jd0295 = new jd0("TIMES", 294);
        U3 = jd0295;
        jd0 jd0296 = new jd0("STREAM_DATA", 295);
        V3 = jd0296;
        jd0 jd0297 = new jd0("DELAY_IN_SECOND", 296);
        W3 = jd0297;
        jd0 jd0298 = new jd0("HID_CODE", 297);
        X3 = jd0298;
        jd0 jd0299 = new jd0("IMMEDIATE_RELEASE", 298);
        Y3 = jd0299;
        jd0 jd0300 = new jd0("SYSTEM_VERSION", Action.Selfie.SELFIE_END_ACTION);
        Z3 = jd0300;
        jd0 jd0301 = new jd0("MICRO_APP_MAPPINGS", SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
        a4 = jd0301;
        jd0 jd0302 = new jd0("TOTAL_MAPPINGS", Action.Presenter.NEXT);
        b4 = jd0302;
        jd0 jd0303 = new jd0("MICRO_APP_ERROR_TYPE", Action.Presenter.PREVIOUS);
        c4 = jd0303;
        jd0 jd0304 = new jd0(Constants.NOTIFICATION_UID, Action.Presenter.BLACKOUT);
        d4 = jd0304;
        jd0 jd0305 = new jd0("APP_NOTIFICATION_EVENT", 304);
        e4 = jd0305;
        jd0 jd0306 = new jd0("SOFTWARE_REVISION", 305);
        f4 = jd0306;
        jd0 jd0307 = new jd0("FILE_HANDLE_DESCRIPTION", 306);
        g4 = jd0307;
        jd0 jd0308 = new jd0("ABSOLUTE_FILE_NUMBER", 307);
        h4 = jd0308;
        jd0 jd0309 = new jd0("PHASE_ID", 308);
        i4 = jd0309;
        jd0 jd0310 = new jd0("REQUEST_RESULT", 309);
        j4 = jd0310;
        jd0 jd0311 = new jd0("GATT_STATUS", 310);
        k4 = jd0311;
        jd0 jd0312 = new jd0("COMMAND_RESULT", 311);
        l4 = jd0312;
        jd0 jd0313 = new jd0("CURRENT_CONNECTION_PARAMS", 312);
        jd0 jd0314 = new jd0("REQUESTED_CONNECTION_PARAMS", 313);
        m4 = jd0314;
        jd0 jd0315 = new jd0("ACCEPTED_CONNECTION_PARAMS", 314);
        n4 = jd0315;
        jd0 jd0316 = new jd0("ERASE_DURATION_IN_MS", 315);
        o4 = jd0316;
        jd0 jd0317 = new jd0("TRANSFER_DURATION_IN_MS", 316);
        p4 = jd0317;
        jd0 jd0318 = new jd0("RECONNECT_DURATION_IN_MS", 317);
        q4 = jd0318;
        jd0 jd0319 = new jd0("OLD_FIRMWARE", 318);
        r4 = jd0319;
        jd0 jd0320 = new jd0("NEW_FIRMWARE", 319);
        s4 = jd0320;
        jd0 jd0321 = new jd0("TOTAL_DATA_SIZE", 320);
        jd0 jd0322 = new jd0("STACK_TRACE", 321);
        jd0 jd0323 = new jd0("FILE_INDEX", 322);
        t4 = jd0323;
        jd0 jd0324 = new jd0("ERROR_CODE", 323);
        jd0 jd0325 = new jd0("FILE", 324);
        u4 = jd0325;
        jd0 jd0326 = new jd0("ROW_ID", 325);
        v4 = jd0326;
        jd0 jd0327 = new jd0("NUMBER_OF_DELETED_ROW", 326);
        w4 = jd0327;
        jd0 jd0328 = new jd0("DEST", 327);
        jd0 jd0329 = new jd0("COMMUTE_TIME_IN_MINUTE", 328);
        x4 = jd0329;
        jd0 jd0330 = new jd0("COMMUTE_INFO", 329);
        y4 = jd0330;
        jd0 jd0331 = new jd0("COMMUTE", 330);
        jd0 jd0332 = new jd0("TRAFFIC", 331);
        z4 = jd0332;
        jd0 jd0333 = new jd0("CURRENT_FILES_VERSION", 332);
        A4 = jd0333;
        jd0 jd0334 = new jd0("CURRENT_VERSION", 333);
        B4 = jd0334;
        jd0 jd0335 = new jd0("SUPPORTED_VERSION", 334);
        C4 = jd0335;
        jd0 jd0336 = new jd0("CHALLENGE_ID", 335);
        D4 = jd0336;
        jd0 jd0337 = new jd0("REQUEST_TYPE", 336);
        jd0 jd0338 = new jd0("STEP", 337);
        E4 = jd0338;
        jd0 jd0339 = new jd0("STEP_OFFSET", 338);
        jd0 jd0340 = new jd0("CALORIES_OFFSET", 339);
        jd0 jd0341 = new jd0("USER", 340);
        F4 = jd0341;
        jd0 jd0342 = new jd0("RANK", FacebookRequestErrorClassification.EC_TOO_MANY_USER_ACTION_CALLS);
        G4 = jd0342;
        jd0 jd0343 = new jd0("DURATION_MINUTE", 342);
        jd0 jd0344 = new jd0("REMAIN_MINUTE", 343);
        jd0 jd0345 = new jd0("GOAL", 344);
        jd0 jd0346 = new jd0("PLAYER_NUM", 345);
        H4 = jd0346;
        jd0 jd0347 = new jd0("SESSION_STATE", 346);
        jd0 jd0348 = new jd0("ID", 347);
        I4 = jd0348;
        jd0 jd0349 = new jd0("INSTALLED_VERSION", 348);
        jd0 jd0350 = new jd0("UI_PACKAGE", 349);
        J4 = jd0350;
        jd0 jd0351 = new jd0("HR_BPM", 350);
        jd0 jd0352 = new jd0("CHALLENGE_METRIC", 351);
        jd0 jd0353 = new jd0("TIMEOUT_IN_MS", 352);
        K4 = jd0353;
        jd0 jd0354 = new jd0("INSTALLED_UI_PACKAGES", 353);
        L4 = jd0354;
        jd0 jd0355 = new jd0("PDK_CONFIG", 354);
        jd0 jd0356 = new jd0("BUNDLE_ID", 355);
        M4 = jd0356;
        jd0 jd0357 = new jd0("AUTO_RUN", 356);
        N4 = jd0357;
        jd0 jd0358 = new jd0("RESERVED_DATA", 357);
        O4 = jd0358;
        jd0 jd0359 = new jd0("BYTE_CODE", 358);
        jd0 jd0360 = new jd0("NODE_NAME", 359);
        P4 = jd0360;
        jd0 jd0361 = new jd0("UI_PACKAGE_INFO", 360);
        Q4 = jd0361;
        jd0 jd0362 = new jd0("UI_PACKAGE_MAIN_NODE", 361);
        jd0 jd0363 = new jd0("UI_PACKAGE_CHILD_NODES", 362);
        R4 = jd0363;
        jd0 jd0364 = new jd0("UI_PACKAGE_IMAGES", 363);
        S4 = jd0364;
        jd0 jd0365 = new jd0("UI_PACKAGE_LAYOUTS", 364);
        T4 = jd0365;
        jd0 jd0366 = new jd0("UI_PACKAGE_LOCALES", 365);
        U4 = jd0366;
        jd0 jd0367 = new jd0("UI_PACKAGE_INFOS", 366);
        V4 = jd0367;
        jd0 jd0368 = new jd0("UI_PACKAGE_CONFIGS", 367);
        W4 = jd0368;
        jd0 jd0369 = new jd0("UI_PACKAGE_TEXT", 368);
        X4 = jd0369;
        jd0 jd0370 = new jd0("UI_PACKAGE_BUNDLE_ID", 369);
        Y4 = jd0370;
        jd0 jd0371 = new jd0("UI_PACKAGE_ID", 370);
        Z4 = jd0371;
        jd0 jd0372 = new jd0("UI_PACKAGE_CRC", 371);
        a5 = jd0372;
        jd0 jd0373 = new jd0("PERCENTAGE", 372);
        b5 = jd0373;
        jd0 jd0374 = new jd0("VOLTAGE", 373);
        c5 = jd0374;
        jd0 jd0375 = new jd0("MESS", 374);
        d5 = jd0375;
        jd0 jd0376 = new jd0("CHALLENGES", 375);
        e5 = jd0376;
        jd0 jd0377 = new jd0("BLUETOOTH_DEVICE_TYPE", 376);
        f5 = jd0377;
        jd0 jd0378 = new jd0("SUB_ENTRY", 377);
        jd0 jd0379 = new jd0("ICON", 378);
        g5 = jd0379;
        jd0 jd0380 = new jd0("REPLY_MESSAGE_GROUP", 379);
        h5 = jd0380;
        jd0 jd0381 = new jd0("TIMESTAMPS", 380);
        i5 = jd0381;
        jd0 jd0382 = new jd0("TARGET_FIRMWARE", 381);
        j5 = jd0382;
        jd0 jd0383 = new jd0("ENCRYPT_METHOD", 382);
        k5 = jd0383;
        jd0 jd0384 = new jd0("KEY_TYPE", 383);
        l5 = jd0384;
        jd0 jd0385 = new jd0("BATTERY_MODE", 384);
        m5 = jd0385;
        jd0 jd0386 = new jd0("NFC_STATE", 385);
        n5 = jd0386;
        jd0 jd0387 = new jd0("ALWAYS_ON_SCREEN_STATE", 386);
        o5 = jd0387;
        jd0 jd0388 = new jd0("TOUCH_TO_WAKE_STATE", 387);
        p5 = jd0388;
        jd0 jd0389 = new jd0("TILT_TO_WAKE_STATE", 388);
        q5 = jd0389;
        jd0 jd0390 = new jd0("LOCATION_STATE", 389);
        r5 = jd0390;
        jd0 jd0391 = new jd0("VIBRATION_STATE", 390);
        s5 = jd0391;
        jd0 jd0392 = new jd0("SPEAKER_STATE", 391);
        t5 = jd0392;
        jd0 jd0393 = new jd0("WIFI_STATE", 392);
        u5 = jd0393;
        jd0 jd0394 = new jd0("GOOGLE_DETECT_STATE", 393);
        v5 = jd0394;
        jd0 jd0395 = new jd0("BLE_FROM_MINUTE_OFFET", 394);
        w5 = jd0395;
        jd0 jd0396 = new jd0("BLE_TO_MINUTE_OFFET", 395);
        x5 = jd0396;
        jd0 jd0397 = new jd0("MINIMUM_STEP_THRESHOLD", 396);
        y5 = jd0397;
        jd0 jd0398 = new jd0("XOR_KEY_FIRST_OFFSET", 397);
        z5 = jd0398;
        jd0 jd0399 = new jd0("XOR_KEY_SECOND_OFFSET", 398);
        A5 = jd0399;
        jd0 jd0400 = new jd0("DIAMETER", 399);
        B5 = jd0400;
        jd0 jd0401 = new jd0("TIRE_SIZE", MFNetworkReturnCode.BAD_REQUEST);
        C5 = jd0401;
        jd0 jd0402 = new jd0("CHAINRING", 401);
        D5 = jd0402;
        jd0 jd0403 = new jd0("COG", Action.ActivityTracker.TAG_ACTIVITY);
        E5 = jd0403;
        jd0 jd0404 = new jd0("GPS", MFNetworkReturnCode.WRONG_PASSWORD);
        F5 = jd0404;
        jd0 jd0405 = new jd0("ACTIVITY", 404);
        G5 = jd0405;
        jd0 jd0406 = new jd0("SESSION_ID", 405);
        H5 = jd0406;
        jd0 jd0407 = new jd0("WATCH_APPS", 406);
        I5 = jd0407;
        jd0 jd0408 = new jd0("INSTALLED_WATCH_APPS", 407);
        J5 = jd0408;
        jd0 jd0409 = new jd0("ROUTE", MFNetworkReturnCode.CLIENT_TIMEOUT);
        K5 = jd0409;
        jd0 jd0410 = new jd0("DURATION", MFNetworkReturnCode.ITEM_NAME_IN_USED);
        L5 = jd0410;
        jd0 jd0411 = new jd0("APP_LIST", 410);
        jd0 jd0412 = new jd0("TOTAL_DURATION", 411);
        M5 = jd0412;
        jd0 jd0413 = new jd0("ORIENTATION", FacebookRequestErrorClassification.EC_APP_NOT_INSTALLED);
        N5 = jd0413;
        jd0 jd0414 = new jd0("LAYOUT_TYPE", 413);
        O5 = jd0414;
        jd0 jd0415 = new jd0("GPS_FILE_COUNT", 414);
        P5 = jd0415;
        jd0 jd0416 = new jd0("MSL_DUR", MFNetworkReturnCode.CONTENT_TYPE_ERROR);
        Q5 = jd0416;
        jd0 jd0417 = new jd0("PAUSE_RUN_SEQ", 416);
        R5 = jd0417;
        jd0 jd0418 = new jd0("GPS_COUNT", 417);
        S5 = jd0418;
        jd0 jd0419 = new jd0("ELABEL_FILE", 418);
        T5 = jd0419;
        jd0 jd0420 = new jd0("OUTPUT_FORMAT", 419);
        U5 = jd0420;
        jd0 jd0421 = new jd0("WIDTH", 420);
        V5 = jd0421;
        jd0 jd0422 = new jd0("HEIGHT", 421);
        W5 = jd0422;
        jd0 jd0423 = new jd0("SCALED_WIDTH", 422);
        jd0 jd0424 = new jd0("SCALED_HEIGHT", HelpSearchRecyclerViewAdapter.TYPE_PADDING);
        jd0 jd0425 = new jd0("ANGLE", 424);
        jd0 jd0426 = new jd0("X", 425);
        jd0 jd0427 = new jd0("Y", 426);
        jd0 jd0428 = new jd0("SCALED_X", 427);
        X5 = jd0428;
        jd0 jd0429 = new jd0("SCALED_Y", 428);
        Y5 = jd0429;
        jd0 jd0430 = new jd0("OFFSET_IN_MINUTES", MFNetworkReturnCode.RATE_LIMIT_EXEEDED);
        jd0 jd0431 = new jd0("THEME_CLASS", 430);
        Z5 = jd0431;
        jd0 jd0432 = new jd0("OS_VERSION", 431);
        a6 = jd0432;
        jd0 jd0433 = new jd0("SYNC_MODE", 432);
        b6 = jd0433;
        jd0 jd0434 = new jd0("THEME_BUNDLE_ID", 433);
        c6 = jd0434;
        jd0 jd0435 = new jd0("HTTP_STATUS", 434);
        d6 = jd0435;
        jd0 jd0436 = new jd0("NETWORK_ERROR", 435);
        e6 = jd0436;
        f6 = new jd0[]{jd0, jd02, jd03, jd04, jd05, jd06, jd07, jd08, jd09, jd010, jd011, jd012, jd013, jd014, jd015, jd016, jd017, jd018, jd019, jd020, jd021, jd022, jd023, jd024, jd025, jd026, jd027, jd028, jd029, jd030, jd031, jd032, jd033, jd034, jd035, jd036, jd037, jd038, jd039, jd040, jd041, jd042, jd043, jd044, jd045, jd046, jd047, jd048, jd049, jd050, jd051, jd052, jd053, jd054, jd055, jd056, jd057, jd058, jd059, jd060, jd061, jd062, jd063, jd064, jd065, jd066, jd067, jd068, jd069, jd070, jd071, jd072, jd073, jd074, jd075, jd076, jd077, jd078, jd079, jd080, jd081, jd082, jd083, jd084, jd085, jd086, jd087, jd088, jd089, jd090, jd091, jd092, jd093, jd094, jd095, jd096, jd097, jd098, jd099, jd0100, jd0101, jd0102, jd0103, jd0104, jd0105, jd0106, jd0107, jd0108, jd0109, jd0110, jd0111, jd0112, jd0113, jd0114, jd0115, jd0116, jd0117, jd0118, jd0119, jd0120, jd0121, jd0122, jd0123, jd0124, jd0125, jd0126, jd0127, jd0128, jd0129, jd0130, jd0131, jd0132, jd0133, jd0134, jd0135, jd0136, jd0137, jd0138, jd0139, jd0140, jd0141, jd0142, jd0143, jd0144, jd0145, jd0146, jd0147, jd0148, jd0149, jd0150, jd0151, jd0152, jd0153, jd0154, jd0155, jd0156, jd0157, jd0158, jd0159, jd0160, jd0161, jd0162, jd0163, jd0164, jd0165, jd0166, jd0167, jd0168, jd0169, jd0170, jd0171, jd0172, jd0173, jd0174, jd0175, jd0176, jd0177, jd0178, jd0179, jd0180, jd0181, jd0182, jd0183, jd0184, jd0185, jd0186, jd0187, jd0188, jd0189, jd0190, jd0191, jd0192, jd0193, jd0194, jd0195, jd0196, jd0197, jd0198, jd0199, jd0200, jd0201, jd0202, jd0203, jd0204, jd0205, jd0206, jd0207, jd0208, jd0209, jd0210, jd0211, jd0212, jd0213, jd0214, jd0215, jd0216, jd0217, jd0218, jd0219, jd0220, jd0221, jd0222, jd0223, jd0224, jd0225, jd0226, jd0227, jd0228, jd0229, jd0230, jd0231, jd0232, jd0233, jd0234, jd0235, jd0236, jd0237, jd0238, jd0239, jd0240, jd0241, jd0242, jd0243, jd0244, jd0245, jd0246, jd0247, jd0248, jd0249, jd0250, jd0251, jd0252, jd0253, jd0254, jd0255, jd0256, jd0257, jd0258, jd0259, jd0260, jd0261, jd0262, jd0263, jd0264, jd0265, jd0266, jd0267, jd0268, jd0269, jd0270, jd0271, jd0272, jd0273, jd0274, jd0275, jd0276, jd0277, jd0278, jd0279, jd0280, jd0281, jd0282, jd0283, jd0284, jd0285, jd0286, jd0287, jd0288, jd0289, jd0290, jd0291, jd0292, jd0293, jd0294, jd0295, jd0296, jd0297, jd0298, jd0299, jd0300, jd0301, jd0302, jd0303, jd0304, jd0305, jd0306, jd0307, jd0308, jd0309, jd0310, jd0311, jd0312, jd0313, jd0314, jd0315, jd0316, jd0317, jd0318, jd0319, jd0320, jd0321, jd0322, jd0323, jd0324, jd0325, jd0326, jd0327, jd0328, jd0329, jd0330, jd0331, jd0332, jd0333, jd0334, jd0335, jd0336, jd0337, jd0338, jd0339, jd0340, jd0341, jd0342, jd0343, jd0344, jd0345, jd0346, jd0347, jd0348, jd0349, jd0350, jd0351, jd0352, jd0353, jd0354, jd0355, jd0356, jd0357, jd0358, jd0359, jd0360, jd0361, jd0362, jd0363, jd0364, jd0365, jd0366, jd0367, jd0368, jd0369, jd0370, jd0371, jd0372, jd0373, jd0374, jd0375, jd0376, jd0377, jd0378, jd0379, jd0380, jd0381, jd0382, jd0383, jd0384, jd0385, jd0386, jd0387, jd0388, jd0389, jd0390, jd0391, jd0392, jd0393, jd0394, jd0395, jd0396, jd0397, jd0398, jd0399, jd0400, jd0401, jd0402, jd0403, jd0404, jd0405, jd0406, jd0407, jd0408, jd0409, jd0410, jd0411, jd0412, jd0413, jd0414, jd0415, jd0416, jd0417, jd0418, jd0419, jd0420, jd0421, jd0422, jd0423, jd0424, jd0425, jd0426, jd0427, jd0428, jd0429, jd0430, jd0431, jd0432, jd0433, jd0434, jd0435, jd0436};
    }
    */

    @DexIgnore
    public jd0(String str, int i6) {
    }

    @DexIgnore
    public static jd0 valueOf(String str) {
        return (jd0) Enum.valueOf(jd0.class, str);
    }

    @DexIgnore
    public static jd0[] values() {
        return (jd0[]) f6.clone();
    }
}
