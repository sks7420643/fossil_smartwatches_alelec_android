package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o53 implements l53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f2632a;
    @DexIgnore
    public static /* final */ xv2<Boolean> b;

    /*
    static {
        hw2 hw2 = new hw2(yv2.a("com.google.android.gms.measurement"));
        f2632a = hw2.d("measurement.client.consent_state_v1.dev", false);
        b = hw2.d("measurement.service.consent_state_v1", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.l53
    public final boolean zza() {
        return f2632a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.l53
    public final boolean zzb() {
        return b.o().booleanValue();
    }
}
