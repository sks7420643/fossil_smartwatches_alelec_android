package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yb3 extends IInterface {
    @DexIgnore
    rg2 B2(LatLng latLng, float f) throws RemoteException;

    @DexIgnore
    rg2 C2(float f, float f2) throws RemoteException;

    @DexIgnore
    rg2 J1(CameraPosition cameraPosition) throws RemoteException;

    @DexIgnore
    rg2 Z0(float f, int i, int i2) throws RemoteException;

    @DexIgnore
    rg2 c2() throws RemoteException;

    @DexIgnore
    rg2 g0(LatLng latLng) throws RemoteException;

    @DexIgnore
    rg2 u(LatLngBounds latLngBounds, int i) throws RemoteException;

    @DexIgnore
    rg2 v2(float f) throws RemoteException;

    @DexIgnore
    rg2 x(float f) throws RemoteException;

    @DexIgnore
    rg2 z0() throws RemoteException;
}
