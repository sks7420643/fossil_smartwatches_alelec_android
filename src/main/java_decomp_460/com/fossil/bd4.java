package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bd4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f415a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ i94 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore
    public bd4(String str, String str2, String str3, String str4, i94 i94, String str5, String str6, String str7, int i2) {
        this.f415a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = i94;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = i2;
    }
}
