package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em extends lp {
    @DexIgnore
    public /* final */ ArrayList<ow> C; // = by1.a(this.i, hm7.c(ow.ASYNC));
    @DexIgnore
    public /* final */ c2 D;

    @DexIgnore
    public em(k5 k5Var, i60 i60, c2 c2Var) {
        super(k5Var, i60, yp.M, null, false, 24);
        this.D = c2Var;
    }

    @DexIgnore
    @Override // com.fossil.lp
    public void B() {
        lp.i(this, new ls(this.w, this.D), sk.b, fl.b, null, new sl(this), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.lp
    public JSONObject C() {
        return g80.k(super.C(), jd0.l1, this.D.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.lp
    public ArrayList<ow> z() {
        return this.C;
    }
}
