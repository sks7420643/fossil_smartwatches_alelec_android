package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt5 implements Factory<at5> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Provider<on5> f506a;
    @DexIgnore
    public /* final */ Provider<v36> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> d;
    @DexIgnore
    public /* final */ Provider<FileRepository> e;
    @DexIgnore
    public /* final */ Provider<uo5> f;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> g;
    @DexIgnore
    public /* final */ Provider<d26> h;
    @DexIgnore
    public /* final */ Provider<d37> i;
    @DexIgnore
    public /* final */ Provider<yt5> j;
    @DexIgnore
    public /* final */ Provider<ck5> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> m;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> n;

    @DexIgnore
    public bt5(Provider<on5> provider, Provider<v36> provider2, Provider<PortfolioApp> provider3, Provider<DeviceRepository> provider4, Provider<FileRepository> provider5, Provider<uo5> provider6, Provider<NotificationSettingsDatabase> provider7, Provider<d26> provider8, Provider<d37> provider9, Provider<yt5> provider10, Provider<ck5> provider11, Provider<AlarmsRepository> provider12, Provider<DianaAppSettingRepository> provider13, Provider<WorkoutSettingRepository> provider14) {
        this.f506a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
    }

    @DexIgnore
    public static bt5 a(Provider<on5> provider, Provider<v36> provider2, Provider<PortfolioApp> provider3, Provider<DeviceRepository> provider4, Provider<FileRepository> provider5, Provider<uo5> provider6, Provider<NotificationSettingsDatabase> provider7, Provider<d26> provider8, Provider<d37> provider9, Provider<yt5> provider10, Provider<ck5> provider11, Provider<AlarmsRepository> provider12, Provider<DianaAppSettingRepository> provider13, Provider<WorkoutSettingRepository> provider14) {
        return new bt5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14);
    }

    @DexIgnore
    public static at5 c(on5 on5, v36 v36, PortfolioApp portfolioApp, DeviceRepository deviceRepository, FileRepository fileRepository, uo5 uo5, NotificationSettingsDatabase notificationSettingsDatabase, d26 d26, d37 d37, yt5 yt5, ck5 ck5, AlarmsRepository alarmsRepository, DianaAppSettingRepository dianaAppSettingRepository, WorkoutSettingRepository workoutSettingRepository) {
        return new at5(on5, v36, portfolioApp, deviceRepository, fileRepository, uo5, notificationSettingsDatabase, d26, d37, yt5, ck5, alarmsRepository, dianaAppSettingRepository, workoutSettingRepository);
    }

    @DexIgnore
    /* renamed from: b */
    public at5 get() {
        return c(this.f506a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get());
    }
}
