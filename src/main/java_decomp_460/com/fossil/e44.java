package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e44 extends i44<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ e44 INSTANCE; // = new e44();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public transient i44<Comparable> b;
    @DexIgnore
    public transient i44<Comparable> c;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        i14.l(comparable);
        i14.l(comparable2);
        return comparable.compareTo(comparable2);
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends Comparable> i44<S> nullsFirst() {
        i44<S> i44 = (i44<S>) this.b;
        if (i44 != null) {
            return i44;
        }
        i44<S> nullsFirst = super.nullsFirst();
        this.b = nullsFirst;
        return nullsFirst;
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends Comparable> i44<S> nullsLast() {
        i44<S> i44 = (i44<S>) this.c;
        if (i44 != null) {
            return i44;
        }
        i44<S> nullsLast = super.nullsLast();
        this.c = nullsLast;
        return nullsLast;
    }

    @DexIgnore
    @Override // com.fossil.i44
    public <S extends Comparable> i44<S> reverse() {
        return t44.INSTANCE;
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural()";
    }
}
