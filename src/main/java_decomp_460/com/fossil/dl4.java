package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl4 extends hl4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public String f802a;
    @DexIgnore
    public String b;

    @DexIgnore
    public dl4() {
        c();
    }

    @DexIgnore
    @Override // com.fossil.hl4
    public /* bridge */ /* synthetic */ hl4 b(el4 el4) throws IOException {
        d(el4);
        return this;
    }

    @DexIgnore
    public dl4 c() {
        this.f802a = "";
        this.b = "";
        return this;
    }

    @DexIgnore
    public dl4 d(el4 el4) throws IOException {
        while (true) {
            int q = el4.q();
            if (q == 0) {
                break;
            } else if (q == 18) {
                this.f802a = el4.p();
            } else if (q == 26) {
                this.b = el4.p();
            } else if (q == 50) {
                el4.p();
            } else if (!jl4.e(el4, q)) {
                break;
            }
        }
        return this;
    }
}
