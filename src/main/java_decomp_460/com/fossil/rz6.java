package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rz6 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ls5 f3187a;
    @DexIgnore
    public /* final */ pz6 b;

    @DexIgnore
    public rz6(ls5 ls5, pz6 pz6) {
        pq7.c(ls5, "mContext");
        pq7.c(pz6, "mView");
        this.f3187a = ls5;
        this.b = pz6;
    }

    @DexIgnore
    public final ls5 a() {
        return this.f3187a;
    }

    @DexIgnore
    public final pz6 b() {
        return this.b;
    }
}
