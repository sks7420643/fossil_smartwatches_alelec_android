package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe3 implements Parcelable.Creator<CameraPosition> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ CameraPosition createFromParcel(Parcel parcel) {
        int C = ad2.C(parcel);
        LatLng latLng = null;
        float f = 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < C) {
            int t = ad2.t(parcel);
            int l = ad2.l(t);
            if (l == 2) {
                latLng = (LatLng) ad2.e(parcel, t, LatLng.CREATOR);
            } else if (l == 3) {
                f3 = ad2.r(parcel, t);
            } else if (l == 4) {
                f2 = ad2.r(parcel, t);
            } else if (l != 5) {
                ad2.B(parcel, t);
            } else {
                f = ad2.r(parcel, t);
            }
        }
        ad2.k(parcel, C);
        return new CameraPosition(latLng, f3, f2, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ CameraPosition[] newArray(int i) {
        return new CameraPosition[i];
    }
}
