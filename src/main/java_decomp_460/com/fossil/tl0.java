package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.fossil.kl0;
import com.fossil.zm0;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tl0 extends yl0 {
    @DexIgnore
    public static Class<?> b;
    @DexIgnore
    public static Constructor<?> c;
    @DexIgnore
    public static Method d;
    @DexIgnore
    public static Method e;
    @DexIgnore
    public static boolean f;

    @DexIgnore
    public static boolean k(Object obj, String str, int i, boolean z) {
        n();
        try {
            return ((Boolean) d.invoke(obj, str, Integer.valueOf(i), Boolean.valueOf(z))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public static Typeface l(Object obj) {
        n();
        try {
            Object newInstance = Array.newInstance(b, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) e.invoke(null, newInstance);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public static void n() {
        Method method;
        Method method2;
        Constructor<?> constructor;
        Class<?> cls;
        if (!f) {
            f = true;
            try {
                cls = Class.forName("android.graphics.FontFamily");
                constructor = cls.getConstructor(new Class[0]);
                method2 = cls.getMethod("addFontWeightStyle", String.class, Integer.TYPE, Boolean.TYPE);
                method = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass());
            } catch (ClassNotFoundException | NoSuchMethodException e2) {
                Log.e("TypefaceCompatApi21Impl", e2.getClass().getName(), e2);
                method = null;
                method2 = null;
                constructor = null;
                cls = null;
            }
            c = constructor;
            b = cls;
            d = method2;
            e = method;
        }
    }

    @DexIgnore
    public static Object o() {
        n();
        try {
            return c.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.yl0
    public Typeface b(Context context, kl0.b bVar, Resources resources, int i) {
        Object o = o();
        kl0.c[] a2 = bVar.a();
        for (kl0.c cVar : a2) {
            File e2 = zl0.e(context);
            if (e2 == null) {
                return null;
            }
            try {
                if (!zl0.c(e2, resources, cVar.b())) {
                    e2.delete();
                    return null;
                } else if (!k(o, e2.getPath(), cVar.e(), cVar.f())) {
                    return null;
                } else {
                    e2.delete();
                }
            } catch (RuntimeException e3) {
                return null;
            } finally {
                e2.delete();
            }
        }
        return l(o);
    }

    @DexIgnore
    @Override // com.fossil.yl0
    public Typeface c(Context context, CancellationSignal cancellationSignal, zm0.f[] fVarArr, int i) {
        if (fVarArr.length < 1) {
            return null;
        }
        zm0.f h = h(fVarArr, i);
        try {
            ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(h.c(), "r", cancellationSignal);
            if (openFileDescriptor != null) {
                try {
                    File m = m(openFileDescriptor);
                    if (m == null || !m.canRead()) {
                        FileInputStream fileInputStream = new FileInputStream(openFileDescriptor.getFileDescriptor());
                        try {
                            Typeface d2 = super.d(context, fileInputStream);
                            fileInputStream.close();
                            if (openFileDescriptor != null) {
                                openFileDescriptor.close();
                            }
                            return d2;
                        } catch (Throwable th) {
                            th.addSuppressed(th);
                        }
                    } else {
                        Typeface createFromFile = Typeface.createFromFile(m);
                        if (openFileDescriptor != null) {
                            openFileDescriptor.close();
                        }
                        return createFromFile;
                    }
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            } else if (openFileDescriptor == null) {
                return null;
            } else {
                openFileDescriptor.close();
                return null;
            }
        } catch (IOException e2) {
            return null;
        }
        throw th;
        throw th;
    }

    @DexIgnore
    public final File m(ParcelFileDescriptor parcelFileDescriptor) {
        try {
            String readlink = Os.readlink("/proc/self/fd/" + parcelFileDescriptor.getFd());
            if (OsConstants.S_ISREG(Os.stat(readlink).st_mode)) {
                return new File(readlink);
            }
        } catch (ErrnoException e2) {
        }
        return null;
    }
}
