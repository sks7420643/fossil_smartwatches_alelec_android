package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xy2<E> extends ay2<E> {
    @DexIgnore
    public /* final */ transient E d;
    @DexIgnore
    public transient int e;

    @DexIgnore
    public xy2(E e2) {
        sw2.b(e2);
        this.d = e2;
    }

    @DexIgnore
    public xy2(E e2, int i) {
        this.d = e2;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean contains(Object obj) {
        return this.d.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final int hashCode() {
        int i = this.e;
        if (i != 0) {
            return i;
        }
        int hashCode = this.d.hashCode();
        this.e = hashCode;
        return hashCode;
    }

    @DexIgnore
    public final int size() {
        return 1;
    }

    @DexIgnore
    public final String toString() {
        String obj = this.d.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final boolean zza() {
        return this.e != 0;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final int zzb(Object[] objArr, int i) {
        objArr[i] = this.d;
        return i + 1;
    }

    @DexIgnore
    @Override // com.fossil.tx2
    /* renamed from: zzb */
    public final cz2<E> iterator() {
        return new ky2(this.d);
    }

    @DexIgnore
    @Override // com.fossil.ay2
    public final sx2<E> zzd() {
        return sx2.zza(this.d);
    }

    @DexIgnore
    @Override // com.fossil.tx2
    public final boolean zzh() {
        return false;
    }
}
