package com.fossil;

import com.fossil.fitness.WorkoutState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class nc {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ /* synthetic */ int[] f2497a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] g;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] h;

    /*
    static {
        int[] iArr = new int[ob.values().length];
        f2497a = iArr;
        iArr[ob.HARDWARE_LOG.ordinal()] = 1;
        f2497a[ob.ALARM.ordinal()] = 2;
        f2497a[ob.ACTIVITY_FILE.ordinal()] = 3;
        f2497a[ob.NOTIFICATION_FILTER.ordinal()] = 4;
        f2497a[ob.DEVICE_CONFIG.ordinal()] = 5;
        f2497a[ob.DATA_COLLECTION_FILE.ordinal()] = 6;
        f2497a[ob.OTA.ordinal()] = 7;
        f2497a[ob.FONT.ordinal()] = 8;
        f2497a[ob.MUSIC_CONTROL.ordinal()] = 9;
        f2497a[ob.UI_SCRIPT.ordinal()] = 10;
        f2497a[ob.ASSET.ordinal()] = 11;
        f2497a[ob.NOTIFICATION.ordinal()] = 12;
        f2497a[ob.DEVICE_INFO.ordinal()] = 13;
        f2497a[ob.ALL_FILE.ordinal()] = 14;
        f2497a[ob.MICRO_APP.ordinal()] = 15;
        f2497a[ob.DEPRECATED_UI_PACKAGE_FILE.ordinal()] = 16;
        f2497a[ob.WATCH_PARAMETERS_FILE.ordinal()] = 17;
        f2497a[ob.UI_PACKAGE_FILE.ordinal()] = 18;
        f2497a[ob.LUTS_FILE.ordinal()] = 19;
        f2497a[ob.RATE_FILE.ordinal()] = 20;
        f2497a[ob.REPLY_MESSAGES_FILE.ordinal()] = 21;
        f2497a[ob.UI_ENCRYPTED_FILE.ordinal()] = 22;
        int[] iArr2 = new int[av1.values().length];
        b = iArr2;
        iArr2[av1.ETA.ordinal()] = 1;
        b[av1.TRAVEL.ordinal()] = 2;
        int[] iArr3 = new int[zu1.values().length];
        c = iArr3;
        iArr3[zu1.COMMUTE_TIME.ordinal()] = 1;
        c[zu1.RING_PHONE.ordinal()] = 2;
        int[] iArr4 = new int[lt.values().length];
        d = iArr4;
        iArr4[lt.JSON_FILE_EVENT.ordinal()] = 1;
        d[lt.HEARTBEAT_EVENT.ordinal()] = 2;
        d[lt.SERVICE_CHANGE_EVENT.ordinal()] = 3;
        d[lt.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 4;
        d[lt.APP_NOTIFICATION_EVENT.ordinal()] = 5;
        d[lt.MUSIC_EVENT.ordinal()] = 6;
        d[lt.BACKGROUND_SYNC_EVENT.ordinal()] = 7;
        d[lt.MICRO_APP_EVENT.ordinal()] = 8;
        d[lt.TIME_SYNC_EVENT.ordinal()] = 9;
        d[lt.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        d[lt.BATTERY_EVENT.ordinal()] = 11;
        d[lt.ENCRYPTED_DATA.ordinal()] = 12;
        int[] iArr5 = new int[du1.values().length];
        e = iArr5;
        iArr5[du1.START.ordinal()] = 1;
        e[du1.STOP.ordinal()] = 2;
        int[] iArr6 = new int[l90.values().length];
        f = iArr6;
        iArr6[l90.GET_CHALLENGE_INFO.ordinal()] = 1;
        f[l90.LIST_CHALLENGES.ordinal()] = 2;
        f[l90.SYNC_DATA.ordinal()] = 3;
        int[] iArr7 = new int[WorkoutState.values().length];
        g = iArr7;
        iArr7[WorkoutState.START.ordinal()] = 1;
        g[WorkoutState.RESUME.ordinal()] = 2;
        g[WorkoutState.PAUSE.ordinal()] = 3;
        g[WorkoutState.END.ordinal()] = 4;
        int[] iArr8 = new int[n90.values().length];
        h = iArr8;
        iArr8[n90.ROUTE_IMAGE.ordinal()] = 1;
        h[n90.DISTANCE.ordinal()] = 2;
    }
    */
}
