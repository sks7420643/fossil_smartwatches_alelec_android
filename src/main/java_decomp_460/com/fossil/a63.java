package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a63 implements x53 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ xv2<Boolean> f203a; // = new hw2(yv2.a("com.google.android.gms.measurement")).d("measurement.upload.file_truncate_fix", false);

    @DexIgnore
    @Override // com.fossil.x53
    public final boolean zza() {
        return f203a.o().booleanValue();
    }
}
