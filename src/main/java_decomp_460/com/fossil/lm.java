package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm extends bi {
    @DexIgnore
    public HashMap<zm1, ym1> V;

    @DexIgnore
    public lm(k5 k5Var, i60 i60, short s) {
        super(k5Var, i60, yp.w, s, true, zm7.i(hl7.a(hu1.SKIP_ERASE, Boolean.TRUE), hl7.a(hu1.NUMBER_OF_FILE_REQUIRED, 1), hl7.a(hu1.ERASE_CACHE_FILE_BEFORE_GET, Boolean.TRUE)), 1.0f, null, 128);
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public JSONObject E() {
        JSONArray jSONArray;
        Collection<ym1> values;
        JSONObject E = super.E();
        jd0 jd0 = jd0.R;
        HashMap<zm1, ym1> hashMap = this.V;
        if (hashMap == null || (values = hashMap.values()) == null) {
            jSONArray = null;
        } else {
            Object[] array = values.toArray(new ym1[0]);
            if (array != null) {
                jSONArray = px1.a((ym1[]) array);
            } else {
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return g80.k(E, jd0, jSONArray);
    }

    @DexIgnore
    @Override // com.fossil.bi
    public void M(ArrayList<j0> arrayList) {
        l(this.v);
    }

    @DexIgnore
    @Override // com.fossil.bi
    public void Q(j0 j0Var) {
        zq zqVar;
        super.Q(j0Var);
        try {
            this.V = (HashMap) ya.f.f(j0Var.f);
            zqVar = zq.SUCCESS;
        } catch (sx1 e) {
            d90.i.i(e);
            zqVar = zq.UNSUPPORTED_FORMAT;
        }
        this.v = nr.a(this.v, null, zqVar, null, null, 13);
    }

    @DexIgnore
    @Override // com.fossil.bi, com.fossil.lp
    public Object x() {
        HashMap<zm1, ym1> hashMap = this.V;
        return hashMap != null ? hashMap : new HashMap();
    }
}
