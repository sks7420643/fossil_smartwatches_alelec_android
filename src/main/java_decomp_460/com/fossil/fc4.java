package com.fossil;

import com.fossil.ec4;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fc4 implements ec4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ File f1104a;
    @DexIgnore
    public /* final */ File[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;

    @DexIgnore
    public fc4(File file) {
        this(file, Collections.emptyMap());
    }

    @DexIgnore
    public fc4(File file, Map<String, String> map) {
        this.f1104a = file;
        this.b = new File[]{file};
        this.c = new HashMap(map);
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public String b() {
        String e = e();
        return e.substring(0, e.lastIndexOf(46));
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public File c() {
        return this.f1104a;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public File[] d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public String e() {
        return c().getName();
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public ec4.a getType() {
        return ec4.a.JAVA;
    }

    @DexIgnore
    @Override // com.fossil.ec4
    public void remove() {
        x74 f = x74.f();
        f.b("Removing report at " + this.f1104a.getPath());
        this.f1104a.delete();
    }
}
