package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu3<TResult> implements hu3<TResult> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Executor f509a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public it3 c;

    @DexIgnore
    public bu3(Executor executor, it3 it3) {
        this.f509a = executor;
        this.c = it3;
    }

    @DexIgnore
    @Override // com.fossil.hu3
    public final void a(nt3<TResult> nt3) {
        if (!nt3.q() && !nt3.o()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.f509a.execute(new cu3(this, nt3));
                }
            }
        }
    }
}
