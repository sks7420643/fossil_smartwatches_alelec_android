package com.fossil;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class me4 implements Callable {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Context f2372a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public me4(Context context, Intent intent) {
        this.f2372a = context;
        this.b = intent;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Integer.valueOf(cg4.b().g(this.f2372a, this.b));
    }
}
